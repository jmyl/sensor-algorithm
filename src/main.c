/*!
*******************************************************************************

                CREATZ Vulkan API main process

    @section copyright_notice COPYRIGHT NOTICE
    Copyright (c) 2020 by Creatz Inc.
    All Rights Reserved. \n
    Do not duplicate without prior written consent of Creatz Inc.

*******************************************************************************
    @section file_information FILE CREATION INFORMATION
    @file   main.cpp
    @brief  user level API main process
    @author Original: by kj lee
    @date   2020/12/28 First Created

    @section checkin_information LATEST CHECK-IN INFORMATION
    $Rev$
    $Author$
    $Date$
*******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

#include "info.h"
#include "cr_common.h"
#include "cr_osapi.h"
#include "cr_thread.h"
#include "cr_api.h"

#include "testapp.h"

#define LOCAL_DEBUG_ON 1
#define LOCAL_ERROR_TRACE_ON 1
#include "cr_dbg_on.h"
#include "cr_cli.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define MAIN_COMM_TASK_PRIORITY 				(MIN_USER_PRIORITY + 10)


/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/
static cr_thread_t ThreadContext;

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/

// main thread - tcp/ip socket API - switch(command) --> IANA_Cmd()
void *MainCommThread(void *startcontext)
{
	cr_thread_t *pt;
	
	volatile U32	loop;
	U32				u0;
	U32 camid;


	//--------------------------------------- 
	// Change state..
	pt = &ThreadContext;
	pt->ustate = CR_THREAD_STATE_INITED;


	cr_thread_setpriority(pt->hthread, MAIN_COMM_TASK_PRIORITY);

	//---------------------------------------
	pt->ustate = CR_THREAD_STATE_RUN;
	loop = 1;


	while(loop) {
#define CHECK_PERIOD	100				// 100 Hz
		u0 = cr_event_wait (pt->hevent, CHECK_PERIOD);
		if (pt->ustate == CR_THREAD_STATE_NEEDSTOP) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%s: I would be killed...\n", __func__);
			break;
		}
	}

	pt->ustate = CR_THREAD_STATE_STOPPING;
	//--------------------------------------- 

	return 0;
}


static void PrintInfo(void)
{
	LOCAL_DEBUG(("\n#######################################\n"));
	LOCAL_DEBUG(("#    Model Name=%s\n", MODEL_NAME));		
	LOCAL_DEBUG(("#    HW Platform Version=0x%x\n", MODEL_HW_VERSION));		
	LOCAL_DEBUG(("#    SW Version=%d.%d.%d\n", MAJOR_SW_VERSION, MINOR_SW_VERSION, SW_BUILD_NUMBER));			
	LOCAL_DEBUG(("#    Max CAM count=%d\n", MAXCAMCOUNT));
	LOCAL_DEBUG(("#    Num of CAM Used=%d\n", NUM_CAM));	
	LOCAL_DEBUG(("#######################################\n\n"));

}

/*!
 ********************************************************************************
 *	@brief      Main Entry function
 *
 *	@param[in]  		argc
 *						number of argument
 *	@param[in]  		argv
 *						arguments for test applications and future use.
 *
 *				
 *  @return		SUCCESS: 0
 *
 *******************************************************************************/
int main( int argc, char* argv[] )
{		
	BOOL test_app = FALSE;
	int timeCnt = 0;
	BOOL TermProgram = FALSE;	
	
	PrintInfo();

	if(argc > 2) {
		test_app = TRUE;
	}	
	
	if(test_app == TRUE) {
		int testCaseNum;
		
		testCaseNum = atoi(argv[1]); 
		
		switch(testCaseNum)
		{
#if defined(TESTAPP_IANA_FILE)		
			case INTER_TESTAPP_IANA_FILE:
				if(argc > 4) 	{
					TESTAPP_IANA_File(argv[2], atoi(argv[3]), atoi(argv[4]));									
				} else if(argc > 3) 	{
					TESTAPP_IANA_File(argv[2], atoi(argv[3]), -1);									
				} else {
					TESTAPP_IANA_File(argv[2], 0, -1);				
				}
				break;
#endif

#if defined(TESTAPP_OPENCV)		
			case INTER_TESTAPP_OPENCV:
				TESTAPP_OpenCV(argv[2]); 			
				break;
#endif

#if defined(TESTAPP_CAM_CTRL)		
			case INTER_TESTAPP_CAM_CTRL:
				TESTAPP_CAM_Ctrl(argv[2]); 			
				break;
#endif

#if defined(TESTAPP_UNIT_INI)		
			case INTER_TESTAPP_INI:
				TESTAPP_Libini(argc-1, &argv[1]);			
				break;
#endif

			case INTER_TESTAPP_DEFAULT:
			default:
				LOCAL_DEBUG(("No test app specified\n"));
				LOCAL_DEBUG(("Usage: ./vulkan <test_app_num> <args>\n"));
				LOCAL_DEBUG(("e.g: ./vulkan 1 test.png\n"));				
				break;
		}
	}
	else {
		
		ThreadContext.ustate = CR_THREAD_STATE_NULL;
		ThreadContext.hevent = cr_event_create();
		ThreadContext.hthread = (cr_thread_t *) cr_thread_create(MainCommThread, NULL);

		CR_API_StartHostAPI();		
	}

#ifdef DEBUG_BUILD
	TOOL_CLI_Proc();		 
#endif

	if(test_app == FALSE) {
		while(1) {
			
			// TODO: 
			
			if(ThreadContext.ustate == CR_THREAD_STATE_STOPPING)
			{
				cr_event_delete(ThreadContext.hevent);
				exit(-1);
			}
			
			if(TermProgram == TRUE)
			{
				CR_API_StopHostAPI();
				ThreadContext.ustate = CR_THREAD_STATE_NEEDSTOP;
			}
		
			timeCnt++;
			cr_sleep(1000);
		}
	}

	return ( 0 );
} /* end main */

/* EOF */



