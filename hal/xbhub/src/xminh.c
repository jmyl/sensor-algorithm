#define	_XMINH_MODDULE_

#include <winsock2.h>
#include <ws2tcpip.h>

#include "xminh.h"
#include "cr_common.h"
#include "cr_dbgmsg.h"
//---------
#pragma warning(disable:4204)
#pragma warning(disable:4221)

XMINH_RET
XMINH_Transact(
	HANDLE							hXminh,
	PMBTCP_TRANSACT					pTransact
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	fd_set							fds;
	struct	timeval					tv;
	XMINH_RET						iret;
	int								sentbyte;

	if (0 == pXminh) {
		return XMINH_RET_INVALID_HANDLE;
	}

	if (INVALID_SOCKET == pXminh->Socket) {
		return XMINH_RET_INVALID_SOCKET;
	}

	if (0 == pTransact) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	if ((0 == pTransact->pSndBuf) || (0 == pTransact->iSndBytes) || (pTransact->iSndBytes > pTransact->iSndBufSiz)) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	if ((0 != pTransact->pRcvBuf) && (0 == pTransact->iSndBufSiz)) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	FD_ZERO(&fds);
#pragma warning(disable:4127)
	FD_SET(pXminh->Socket, &fds);
#pragma warning(default:4127)
	tv.tv_sec = 0;
	tv.tv_usec = 25000;
	iret = select(32, NULL, &fds, NULL, &tv);

	if (0 >= iret) {
		return XMINH_RET_TRANSMIT_FAIL;
	}

	//if (pTransact->iSndBytes != send(pXminh->Socket, (char*)pTransact->pSndBuf, pTransact->iSndBytes, 0)) 
	sentbyte = send(pXminh->Socket, (char*)pTransact->pSndBuf, pTransact->iSndBytes, 0);
	if (pTransact->iSndBytes != sentbyte) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d  snd bytes %d != %d sendbyte\n", 
				__LINE__,
				pTransact->iSndBytes, sentbyte);
		
		return XMINH_RET_TRANSMIT_FAIL;
	}

	if (0 == pTransact->pRcvBuf) {
		return XMINH_RET_OK;
	}

	FD_ZERO(&fds);
#pragma warning(disable:4127)
	FD_SET(pXminh->Socket, &fds);
#pragma warning(default:4127)
	tv.tv_sec = 0;
	tv.tv_usec = pTransact->dwTansactTimeout;
	iret = select(32, &fds, NULL, NULL, &tv);

	if (0 < iret) {
		pTransact->iRcvBytes = recv(pXminh->Socket, (char *)pTransact->pRcvBuf, pTransact->iRcvBufSiz, 0);
		return XMINH_RET_OK;
	}

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d recv timeout.. iret:%d\n", __LINE__, iret);


	pTransact->iRcvBytes = 0;

	return XMINH_RET_RECEIVE_FAIL;
}


HANDLE
XMINH_Create(
	PXMINH_CFG						pCfg
)
{
	WSADATA							wsaData;
	PXMINH							pXminh;

	if (0 == pCfg) {
		return INVALID_HANDLE_VALUE;
	}

	if (NO_ERROR != WSAStartup(MAKEWORD(2, 2), &wsaData)) {
		return INVALID_HANDLE_VALUE;
	}

	pXminh = (PXMINH) malloc(sizeof(XMINH));
	if (0 == pXminh) {
		return INVALID_HANDLE_VALUE;
	}

	InetPton(AF_INET, pCfg->szIpAddress, &pXminh->AddressInfo.sin_addr.S_un.S_addr);
	pXminh->AddressInfo.sin_family = AF_INET;
	pXminh->AddressInfo.sin_port = htons(pCfg->wIpPort);

#if 0
	pXminh->Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (INVALID_SOCKET == pXminh->Socket) {
		free(pXminh);
		WSACleanup();
		return INVALID_HANDLE_VALUE;
	}
#endif

	pXminh->Socket = INVALID_SOCKET;			// Default..
	return (HANDLE)pXminh;
}


XMINH_RET
XMINH_Delete(
		HANDLE						hXminh
)
{
	PXMINH							pXminh = (PXMINH)hXminh;

	if ((INVALID_HANDLE_VALUE == hXminh) || (0 == hXminh)) {
		return XMINH_RET_INVALID_HANDLE;
	}

	if (INVALID_SOCKET != pXminh->Socket) {
		closesocket(pXminh->Socket);
		pXminh->Socket = INVALID_SOCKET;
	}

	free((void*)hXminh);
	WSACleanup();

	return XMINH_RET_OK;
}


XMINH_RET
XMINH_Connect(
	HANDLE							hXminh
	)
{
	PXMINH							pXminh = (PXMINH)hXminh;

	if ((INVALID_HANDLE_VALUE == hXminh) || (0 == hXminh)) {
		return XMINH_RET_INVALID_HANDLE;
	}
#if 1

	if (INVALID_SOCKET != pXminh->Socket) {
		XMINH_Disconnect(pXminh);
	}

	pXminh->Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (INVALID_SOCKET == pXminh->Socket) {
		free(pXminh);
		WSACleanup();
		return XMINH_RET_CONNECT_FAIL;
	}
#endif

	if (0 != connect(pXminh->Socket, (struct sockaddr*) & pXminh->AddressInfo, sizeof(struct sockaddr_in))) {
		closesocket(pXminh->Socket);
		pXminh->Socket = INVALID_SOCKET;
		return XMINH_RET_CONNECT_FAIL;
	}

	pXminh->wTransactionId = 0;		// reset transaction id

	return XMINH_RET_OK;
}


XMINH_RET
XMINH_Disconnect(
	HANDLE							hXminh
)
{
	PXMINH							pXminh = (PXMINH)hXminh;

	if ((INVALID_HANDLE_VALUE == hXminh) || (0 == hXminh)) {
		return XMINH_RET_INVALID_HANDLE;
	}

	if (INVALID_SOCKET != pXminh->Socket) {
		closesocket(pXminh->Socket);
		pXminh->Socket = INVALID_SOCKET;
	}

	return XMINH_RET_OK;
}

void
XMINH_MBTCP_BuildMbap(
	PMBTCP_TRANSACT					pTransact
)
{
	pTransact->pSndBuf[0] = (BYTE) (pTransact->wTransactionId >> 8);
	pTransact->pSndBuf[1] = (BYTE) (pTransact->wTransactionId >> 0);
	pTransact->pSndBuf[2] = 0;
	pTransact->pSndBuf[3] = 0;
	pTransact->pSndBuf[4] = (BYTE) ((pTransact->iSndBytes - 6) >> 8);
	pTransact->pSndBuf[5] = (BYTE) ((pTransact->iSndBytes - 6) >> 0);
	pTransact->pSndBuf[6] = pTransact->bySlaveId;
}


BOOL
XMINH_MBTCP_IsValidMbap(
	PMBTCP_TRANSACT					pTransaction
)
{
	WORD							wLen;

	wLen = ((WORD)pTransaction->pRcvBuf[4] << 8) | ((WORD)pTransaction->pRcvBuf[5] << 0);

	if (2 > wLen) {
		return FALSE;
	}

	if ((6 + wLen) != pTransaction->iRcvBytes) {
		return FALSE;
	}

	if (0 != memcmp(pTransaction->pSndBuf, pTransaction->pRcvBuf, 4)) {			// check transaction id and protocol id
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "VVVVV %02x %02x %02x %02x  != %02x %02x %02x %02x\n",
			(int)pTransaction->pSndBuf[0],
			(int)pTransaction->pSndBuf[1],
			(int)pTransaction->pSndBuf[2],
			(int)pTransaction->pSndBuf[3],

			(int)pTransaction->pRcvBuf[0],
			(int)pTransaction->pRcvBuf[1],
			(int)pTransaction->pRcvBuf[2],
			(int)pTransaction->pRcvBuf[3]);
		return FALSE;
	}

	if (pTransaction->pRcvBuf[6] != pTransaction->pSndBuf[6]) {					// check slavel id
		return FALSE;
	}

	if ((0x7f & pTransaction->pRcvBuf[7]) != pTransaction->pSndBuf[7]) {		// check function code
		return FALSE;
	}

	return TRUE;
}


XMINH_RET
XMINH_MBTCP_ReadRegister(
	HANDLE							hXminh,
	PMODBUS_REGISTER_CONTEXT		pContext
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	BYTE							aTxBuf[12];
	BYTE							aRxBuf[260];
	LPBYTE							pPdu;
	MBTCP_TRANSACT					sTransact;
	XMINH_RET						iRet;

	if (0 == pContext) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	pPdu = &aTxBuf[7];
	pPdu[0] = MODBUS_FUNCTION_CODE_READ_HOLDING_REGISTERS;
	pPdu[1] = (BYTE)(pContext->wAddress >> 8);
	pPdu[2] = (BYTE)(pContext->wAddress >> 0);
	pPdu[3] = (BYTE)(pContext->wQuantity >> 8);
	pPdu[4] = (BYTE)(pContext->wQuantity >> 0);
	sTransact.iSndBufSiz = 12;
	sTransact.iSndBytes = 12;
	sTransact.pSndBuf = aTxBuf;
	sTransact.iRcvBufSiz = 260;
	sTransact.iRcvBytes = 0;
	sTransact.pRcvBuf = aRxBuf;
	sTransact.bySlaveId = (BYTE) pContext->wSlaveId;
	sTransact.wTransactionId = pXminh->wTransactionId;
	sTransact.dwTansactTimeout = pContext->dwTimeout;

	XMINH_MBTCP_BuildMbap(&sTransact);

	iRet = XMINH_Transact(pXminh, &sTransact);

	if (XMINH_RET_OK == iRet) {
		iRet = XMINH_RET_ILLEGAL_RESPONSE;
		if (TRUE == XMINH_MBTCP_IsValidMbap(&sTransact)) {
			if (sTransact.pRcvBuf[7] == sTransact.pSndBuf[7]) {			// normal response
				if ((sTransact.pRcvBuf[8] == (2 * pContext->wQuantity)) && (sTransact.iRcvBytes == (9 + sTransact.pRcvBuf[8]))) {
					WORD	i;
					WORD	val;

					for (i = 0; i < pContext->wQuantity; i++) {
						val = ((WORD)sTransact.pRcvBuf[9 + 0 + (2 * i)] << 8) | ((WORD)sTransact.pRcvBuf[9 + 1 + (2 * i)] << 0);
						pContext->pData[i] = val;
					}

					pContext->wExceptionCode = 0;
					iRet = XMINH_RET_OK;
				}
			}
			else {												// exception response
				if (9 == sTransact.iRcvBytes) {
					pContext->wExceptionCode = sTransact.pRcvBuf[8];
					iRet = XMINH_RET_OK;
				}
			}
		}
	}

	pXminh->wTransactionId++;

	if (XMINH_RET_OK == iRet) {
		iRet = pContext->wExceptionCode;
	}

	return iRet;
}


XMINH_RET
XMINH_MBTCP_WriteRegister(
	HANDLE							hXminh,
	PMODBUS_REGISTER_CONTEXT		pContext
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	BYTE							aTxBuf[12];
	BYTE							aRxBuf[260];
	LPBYTE							pPdu;
	MBTCP_TRANSACT					sTransact;
	XMINH_RET						iRet;

	if (0 == pContext) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	pPdu = &aTxBuf[7];
	pPdu[0] = MODBUS_FUNCTION_CODE_WRITE_SINGLE_REGISTER;
	pPdu[1] = (BYTE)(pContext->wAddress >> 8);
	pPdu[2] = (BYTE)(pContext->wAddress >> 0);
	pPdu[3] = (BYTE)(pContext->pData[0] >> 8);
	pPdu[4] = (BYTE)(pContext->pData[0] >> 0);
	sTransact.iSndBufSiz = 12;
	sTransact.iSndBytes = 12;
	sTransact.pSndBuf = aTxBuf;
	sTransact.iRcvBufSiz = 260;
	sTransact.iRcvBytes = 0;
	sTransact.pRcvBuf = aRxBuf;
	sTransact.bySlaveId = (BYTE)pContext->wSlaveId;
	sTransact.wTransactionId = pXminh->wTransactionId;
	sTransact.dwTansactTimeout = pContext->dwTimeout;

	XMINH_MBTCP_BuildMbap(&sTransact);

	iRet = XMINH_Transact(pXminh, &sTransact);

	if (XMINH_RET_OK == iRet) {
		iRet = XMINH_RET_ILLEGAL_RESPONSE;
		if (TRUE == XMINH_MBTCP_IsValidMbap(&sTransact)) {
			if (sTransact.pRcvBuf[7] == sTransact.pSndBuf[7]) {
				if (0 == memcmp(&sTransact.pRcvBuf[8], &sTransact.pSndBuf[8], 4)) {
					pContext->wExceptionCode = 0;
					iRet = XMINH_RET_OK;
				}
			}
			else if (sTransact.pRcvBuf[7] == (0x80 | sTransact.pSndBuf[7])) {
				pContext->wExceptionCode = sTransact.pRcvBuf[8];
				iRet = XMINH_RET_OK;
			}
			else {

			}
		}
	}

	pXminh->wTransactionId++;

	if (XMINH_RET_OK == iRet) {
		iRet = pContext->wExceptionCode;
	}

	return iRet;
}


XMINH_RET
XMINH_MBTCP_WriteMultipleRegisters(
	HANDLE							hXminh,
	PMODBUS_REGISTER_CONTEXT		pContext
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	BYTE							aTxBuf[260];
	BYTE							aRxBuf[260];
	LPBYTE							pPdu;
	MBTCP_TRANSACT					sTransact;
	XMINH_RET						iRet;
	WORD							i;

	if (0 == pContext) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	pPdu = &aTxBuf[7];
	pPdu[0] = MODBUS_FUNCTION_CODE_WRITE_MULTIPLE_REGISTERS;
	pPdu[1] = (BYTE)(pContext->wAddress >> 8);
	pPdu[2] = (BYTE)(pContext->wAddress >> 0);
	pPdu[3] = (BYTE)(pContext->wQuantity >> 8);
	pPdu[4] = (BYTE)(pContext->wQuantity >> 0);
	pPdu[5] = (BYTE)(2 * pContext->wQuantity);
	for (i = 0; i < pContext->wQuantity; i++) {
		pPdu[6 + 0 + (2 * i)] = (BYTE)(pContext->pData[i] >> 8);
		pPdu[6 + 1 + (2 * i)] = (BYTE)(pContext->pData[i] >> 0);
	}
	sTransact.iSndBufSiz = 260;
	sTransact.iSndBytes = 13 + (2 * pContext->wQuantity);	// MBAP (6) + S_ID (1) + F_CODE (1) + ADDRESS (2) + QUANTITY (2) + nBytes (1) + Data (2 * N)
	sTransact.pSndBuf = aTxBuf;
	sTransact.iRcvBufSiz = 260;
	sTransact.iRcvBytes = 0;
	sTransact.pRcvBuf = aRxBuf;
	sTransact.bySlaveId = (BYTE)pContext->wSlaveId;
	sTransact.wTransactionId = pXminh->wTransactionId;
	sTransact.dwTansactTimeout = pContext->dwTimeout;

	XMINH_MBTCP_BuildMbap(&sTransact);

	iRet = XMINH_Transact(pXminh, &sTransact);

	if (XMINH_RET_OK == iRet) {
		iRet = XMINH_RET_ILLEGAL_RESPONSE;
		if (TRUE == XMINH_MBTCP_IsValidMbap(&sTransact)) {
			if (sTransact.pRcvBuf[7] == sTransact.pSndBuf[7]) {
				if (0 == memcmp(&sTransact.pRcvBuf[8], &sTransact.pRcvBuf[8], 4)) {
					pContext->wExceptionCode = 0;
					iRet = XMINH_RET_OK;
				}
			}
			else {
				pContext->wExceptionCode = sTransact.pRcvBuf[8];
				iRet = XMINH_RET_OK;
			}
		}
	}

	pXminh->wTransactionId++;

	if (XMINH_RET_OK == iRet) {
		iRet = pContext->wExceptionCode;
	}

	return iRet;
}


XMINH_RET
XMINH_MBTCP_ReadCoil(
	HANDLE							hXminh,
	PMODBUS_REGISTER_CONTEXT		pContext
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	BYTE							aTxBuf[12];
	BYTE							aRxBuf[260];
	LPBYTE							pPdu;
	MBTCP_TRANSACT					sTransact;
	XMINH_RET						iRet;

	if (0 == pContext) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	pPdu = &aTxBuf[7];
	pPdu[0] = MODBUS_FUNCTION_CODE_READ_COILS;
	pPdu[1] = (BYTE)(pContext->wAddress >> 8);
	pPdu[2] = (BYTE)(pContext->wAddress >> 0);
	pPdu[3] = (BYTE)(pContext->wQuantity >> 8);
	pPdu[4] = (BYTE)(pContext->wQuantity >> 0);
	sTransact.iSndBufSiz = 12;
	sTransact.iSndBytes = 12;
	sTransact.pSndBuf = aTxBuf;
	sTransact.iRcvBufSiz = 260;
	sTransact.iRcvBytes = 0;
	sTransact.pRcvBuf = aRxBuf;
	sTransact.bySlaveId = (BYTE)pContext->wSlaveId;
	sTransact.wTransactionId = pXminh->wTransactionId;
	sTransact.dwTansactTimeout = pContext->dwTimeout;

	XMINH_MBTCP_BuildMbap(&sTransact);

	iRet = XMINH_Transact(pXminh, &sTransact);

	if (XMINH_RET_OK == iRet) {
		iRet = XMINH_RET_ILLEGAL_RESPONSE;
		if (TRUE == XMINH_MBTCP_IsValidMbap(&sTransact)) {
			if (sTransact.pRcvBuf[7] == sTransact.pSndBuf[7]) {			// normal response
				if ((sTransact.pRcvBuf[8] == ((7 + pContext->wQuantity) / 8)) && (sTransact.iRcvBytes == (9 + sTransact.pRcvBuf[8]))) {
					WORD	i;

					for (i = 0; i < pContext->wQuantity; i++) {
						if (0 != (sTransact.pRcvBuf[9 + (i / 8)] & (0x1 << (i % 8)))) {
							pContext->pData[i] = 1;
						}
						else {
							pContext->pData[i] = 0;
						}
					}

					pContext->wExceptionCode = 0;
					iRet = XMINH_RET_OK;
				}
			}
			else {												// exception response
				if (9 == sTransact.iRcvBytes) {
					pContext->wExceptionCode = sTransact.pRcvBuf[8];
					iRet = XMINH_RET_OK;
				}
			}
		}
	}

	pXminh->wTransactionId++;

	if (XMINH_RET_OK == iRet) {
		iRet = pContext->wExceptionCode;
	}

	return iRet;
}


XMINH_RET
XMINH_MBTCP_WriteSingleCoil(
	HANDLE							hXminh,
	PMODBUS_REGISTER_CONTEXT		pContext
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	BYTE							aTxBuf[12];
	BYTE							aRxBuf[260];
	LPBYTE							pPdu;
	MBTCP_TRANSACT					sTransact;
	XMINH_RET						iRet;

	if (0 == pContext) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	pPdu = &aTxBuf[7];
	pPdu[0] = MODBUS_FUNCTION_CODE_WRITE_SINGLE_COIL;
	pPdu[1] = (BYTE)(pContext->wAddress >> 8);
	pPdu[2] = (BYTE)(pContext->wAddress >> 0);
	if (0 == pContext->pData[0]) {
		pPdu[3] = 0x0;
		pPdu[4] = 0x0;
	}
	else {
		pPdu[3] = 0xff;
		pPdu[4] = 0x00;
	}
	sTransact.iSndBufSiz = 12;
	sTransact.iSndBytes = 12;
	sTransact.pSndBuf = aTxBuf;
	sTransact.iRcvBufSiz = 260;
	sTransact.iRcvBytes = 0;
	sTransact.pRcvBuf = aRxBuf;
	sTransact.bySlaveId = (BYTE) pContext->wSlaveId;
	sTransact.wTransactionId = pXminh->wTransactionId;
	sTransact.dwTansactTimeout = pContext->dwTimeout;

	XMINH_MBTCP_BuildMbap(&sTransact);

	iRet = XMINH_Transact(pXminh, &sTransact);

	if (XMINH_RET_OK == iRet) {
		iRet = XMINH_RET_ILLEGAL_RESPONSE;
		if (TRUE == XMINH_MBTCP_IsValidMbap(&sTransact)) {
			if (sTransact.pRcvBuf[7] == sTransact.pSndBuf[7]) {
				if (0 == memcmp(&sTransact.pRcvBuf[8], &sTransact.pSndBuf[8], 4)) {
					pContext->wExceptionCode = 0;
					iRet = XMINH_RET_OK;
				}
			}
			else if (sTransact.pRcvBuf[7] == (0x80 | sTransact.pSndBuf[7])) {
				pContext->wExceptionCode = sTransact.pRcvBuf[8];
				iRet = XMINH_RET_OK;
			}
			else {

			}
		}
	}

	pXminh->wTransactionId++;

	if (XMINH_RET_OK == iRet) {
		iRet = pContext->wExceptionCode;
	}

	return iRet;
}


XMINH_RET
XMINH_RGBLED_Set(
	HANDLE							hXminh,
	XMINH_SLAVE_ID					nId,
	LPWORD							pLedInfo
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
#define 	MODBUS_TIMEOUT_RGBLED	(100 * 1000)
	MODBUS_REGISTER_CONTEXT			sContext = { MBRTU_REGISTER_ID_RGB1_1 - 1, 12, &pLedInfo[0], nId, 0, MODBUS_TIMEOUT_RGBLED };

	if (0 == pLedInfo) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	iRet = XMINH_MBTCP_WriteMultipleRegisters(pXminh, &sContext);

	return iRet;
}


XMINH_RET
XMINH_RGBLED_Get(
	HANDLE							hXminh,
	XMINH_SLAVE_ID					nId,
	LPWORD							pLedInfo
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	MODBUS_REGISTER_CONTEXT			sContext = { MBRTU_REGISTER_ID_RGB1_1 - 1, 12, &pLedInfo[0], nId, 0, MODBUS_TIMEOUT_RGBLED};

	if (0 == pLedInfo) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	iRet = XMINH_MBTCP_ReadRegister(pXminh, &sContext);

	return iRet;
}


XMINH_RET
XMINH_IRLED_Set(
	HANDLE							hXminh,
	XMINH_SLAVE_ID					nId,
	WORD							wBrightIndex
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
//#define 	MODBUS_TIMEOUT_NORMAL	(30 * 1000)
#define 	MODBUS_TIMEOUT_NORMAL	(100 * 1000)
	MODBUS_REGISTER_CONTEXT			sContext = { MBRTU_REGISTER_ID_IRLED_BRI - 1, 1, &wBrightIndex, nId, 0, MODBUS_TIMEOUT_NORMAL };

	iRet = XMINH_MBTCP_WriteRegister(pXminh, &sContext);

	return iRet;
}


XMINH_RET
XMINH_IRLED_Get(
	HANDLE							hXminh,
	XMINH_SLAVE_ID					nId,
	LPWORD							pwBrightIndex
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	MODBUS_REGISTER_CONTEXT			sContext = { MBRTU_REGISTER_ID_IRLED_BRI - 1, 1, pwBrightIndex, nId, 0, MODBUS_TIMEOUT_NORMAL };

	if (0 == pwBrightIndex) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	iRet = XMINH_MBTCP_ReadRegister(pXminh, &sContext);

	return iRet;
}




XMINH_RET
XMINH_IRLED_GetStatus(
	HANDLE							hXminh,
	XMINH_SLAVE_ID					nId,
	PIRLED_LOAD_STATUS				pStatus
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							value;
	MODBUS_REGISTER_CONTEXT			sContext = { MBRTU_REGISTER_ID_IRLED_BRI - 1, 1, &value, nId, 0, MODBUS_TIMEOUT_NORMAL };

	if (0 == pStatus) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	iRet = XMINH_MBTCP_ReadRegister(pXminh, &sContext);
	if (XMINH_RET_OK == iRet) {
		*pStatus = value;
	}

	return iRet;
}


XMINH_RET
XMINH_CAMMODE_Set(
	HANDLE							hXminh,
	XMINH_SLAVE_ID					nId,
	XMINH_CAMMODE					nMode
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							value = (WORD)nMode;
	MODBUS_REGISTER_CONTEXT			sContext = { MBRTU_REGISTER_ID_OPMODE - 1, 1, &value, nId, 0, MODBUS_TIMEOUT_NORMAL };

	iRet = XMINH_MBTCP_WriteRegister(pXminh, &sContext);

	return iRet;
}


XMINH_RET
XMINH_CAMMODE_Get(
	HANDLE							hXminh,
	XMINH_SLAVE_ID					nId,
	PXMINH_CAMMODE					pMode
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							value;
	MODBUS_REGISTER_CONTEXT			sContext = { MBRTU_REGISTER_ID_OPMODE - 1, 1, &value, nId, 0, MODBUS_TIMEOUT_NORMAL };

	if (0 == pMode) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	iRet = XMINH_MBTCP_ReadRegister(pXminh, &sContext);
	if (XMINH_RET_OK == iRet) {
		*pMode = value;
	}

	return iRet;
}


XMINH_RET
XMINH_IRLED_Rescan(
	HANDLE							hXminh,
	XMINH_SLAVE_ID					nId
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							wCoilValue = 0xff00;
	MODBUS_REGISTER_CONTEXT			sContext = { MBRTU_COIL_ID_IRLED_SCAN - 1, 1, &wCoilValue, nId, 0, MODBUS_TIMEOUT_NORMAL };

	iRet = XMINH_MBTCP_WriteSingleCoil(pXminh, &sContext);

	return iRet;
}


XMINH_RET
XMINH_IRLED_IsRescanComplete(
	HANDLE							hXminh,
	XMINH_SLAVE_ID					nId,
	PIRLED_RESCAN_STATUS			pStatus
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							value;
	MODBUS_REGISTER_CONTEXT			sContext = { MBRTU_COIL_ID_IRLED_SCAN - 1, 1, &value, nId, 0, MODBUS_TIMEOUT_NORMAL };

	if (0 == pStatus) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	iRet = XMINH_MBTCP_ReadCoil(pXminh, &sContext);
	if (XMINH_RET_OK == iRet) {
		*pStatus = value;
	}

	return iRet;
}


XMINH_RET
XMINH_ZCAM_PowerControl(
	HANDLE							hXminh,
	XMINH_SLAVE_ID					nId,
	ZCAM_POWER_STATE				nState
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							value = nState;
	MODBUS_REGISTER_CONTEXT			sContext = { MBRTU_COIL_ID_ZCAM_PWR - 1, 1, &value, nId, 0, MODBUS_TIMEOUT_NORMAL };

	iRet = XMINH_MBTCP_WriteSingleCoil(pXminh, &sContext);

	return iRet;
}


XMINH_RET
XMINH_ZCAM_GetPowerState(
	HANDLE							hXminh,
	XMINH_SLAVE_ID					nId,
	PZCAM_POWER_STATE				pStatus
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							value;
	MODBUS_REGISTER_CONTEXT			sContext = { MBRTU_COIL_ID_ZCAM_PWR - 1, 1, &value, nId, 0, MODBUS_TIMEOUT_NORMAL };

	if (0 == pStatus) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	iRet = XMINH_MBTCP_ReadCoil(pXminh, &sContext);
	if (XMINH_RET_OK == iRet) {
		*pStatus = value;
	}

	return iRet;
}


XMINH_RET
XMINH_ResetCamera(
	HANDLE							hXminh,
	XMINH_SLAVE_ID					nId
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							wCoilValue = 0xff00;
	MODBUS_REGISTER_CONTEXT			sContext = { MBRTU_COIL_ID_SYSTEM_RESET - 1, 1, &wCoilValue, nId, 0, MODBUS_TIMEOUT_NORMAL };

	iRet = XMINH_MBTCP_WriteSingleCoil(pXminh, &sContext);

	return iRet;
}


XMINH_RET
XMINH_SetWatchdogTimeoutValue(
	HANDLE							hXminh,
	XMINH_SLAVE_ID					nId,
	WORD							wTimeout			// in [ms]
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							wCoilValue = 0xff00;
	MODBUS_REGISTER_CONTEXT			sContext = { MBRTU_REGISTER_ID_ALIVETIME - 1, 1, &wTimeout, nId, 0, MODBUS_TIMEOUT_NORMAL };

	if (XMINH_SLAVE_ID_XMINH == nId) {
		sContext.wAddress = XMINH_HOLDING_ID_KEEPALIVE_TIME - 1;
	}

	iRet = XMINH_MBTCP_WriteRegister(pXminh, &sContext);

	wCoilValue;
	return iRet;
}


XMINH_RET
XMINH_EnableWatchdog(
	HANDLE							hXminh,
	XMINH_SLAVE_ID					nId
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							wCoilValue = 0xff00;
	MODBUS_REGISTER_CONTEXT			sContext = { MBRTU_COIL_ID_WDT_ENABLE - 1, 1, &wCoilValue, nId, 0, MODBUS_TIMEOUT_NORMAL };

	if (XMINH_SLAVE_ID_XMINH == nId) {
		sContext.wAddress = XMINH_COIL_ID_WDT_ENABLE - 1;
	}

	iRet = XMINH_MBTCP_WriteSingleCoil(pXminh, &sContext);

	return iRet;
}


XMINH_RET
XMINH_DisableWatchdog(
	HANDLE							hXminh,
	XMINH_SLAVE_ID					nId
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							wCoilValue = 0x0;
	MODBUS_REGISTER_CONTEXT			sContext = { MBRTU_COIL_ID_WDT_ENABLE - 1, 1, &wCoilValue, nId, 0, MODBUS_TIMEOUT_NORMAL };

	if (XMINH_SLAVE_ID_XMINH == nId) {
		sContext.wAddress = XMINH_COIL_ID_WDT_ENABLE - 1;
	}

	iRet = XMINH_MBTCP_WriteSingleCoil(pXminh, &sContext);

	return iRet;
}


XMINH_RET	
XMINH_KickWatchdog(					
	HANDLE							hXminh,	
	PDWORD							pStatus,
	XMINH_SLAVE_ID					nId		
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	WORD							value;
	MODBUS_REGISTER_CONTEXT			sContext = { MBRTU_REGISTER_ID_OPMODE - 1, 1, (LPWORD)pStatus, nId, 0, MODBUS_TIMEOUT_NORMAL };
	XMINH_RET						iRet;


	if (XMINH_SLAVE_ID_XMINH == nId) {
		sContext.wAddress = XMINH_HOLDING_ID_CAM_SYNC_MODE - 1;
	}

	//return XMINH_MBTCP_ReadRegister(pXminh, &sContext);
	iRet = XMINH_MBTCP_ReadRegister(pXminh, &sContext);

	*pStatus = *pStatus & 0xFFFF;
	value;
	return iRet;
}


XMINH_RET
XMINH_ResetHub(
	HANDLE							hXminh
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	WORD							wCoilValue = 0xff00;
	MODBUS_REGISTER_CONTEXT			sContext = { XMINH_COIL_ID_HUB_RESET - 1, 1, &wCoilValue, XMINH_SLAVE_ID_XMINH, 0, MODBUS_TIMEOUT_NORMAL };

	return XMINH_MBTCP_WriteSingleCoil(pXminh, &sContext);
}


XMINH_RET
XMINH_ResetXminh(
	HANDLE							hXminh
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	WORD							wCoilValue = 0xff00;
	MODBUS_REGISTER_CONTEXT			sContext = { MBRTU_COIL_ID_SYSTEM_RESET - 1, 1, &wCoilValue, XMINH_SLAVE_ID_XMINH, 0, MODBUS_TIMEOUT_NORMAL };

	return XMINH_MBTCP_WriteSingleCoil(pXminh, &sContext);
}


XMINH_RET
XMINH_SetXminhMode(
	HANDLE							hXminh,
	XMINH_SYNC_MODE					nMode
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							value = (WORD)nMode;
	MODBUS_REGISTER_CONTEXT			sContext = { XMINH_HOLDING_ID_CAM_SYNC_MODE - 1, 1, &value, XMINH_SLAVE_ID_XMINH, 0, MODBUS_TIMEOUT_NORMAL };

	iRet = XMINH_MBTCP_WriteRegister(pXminh, &sContext);

	return iRet;
}


XMINH_RET
XMINH_GetXminhMode(
	HANDLE							hXminh,
	PXMINH_SYNC_MODE				pMode
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							value;
	MODBUS_REGISTER_CONTEXT			sContext = { XMINH_HOLDING_ID_CAM_SYNC_MODE - 1, 1, &value, XMINH_SLAVE_ID_XMINH, 0, MODBUS_TIMEOUT_NORMAL };

	if (0 == pMode) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	iRet = XMINH_MBTCP_ReadRegister(pXminh, &sContext);
	if (XMINH_RET_OK == iRet) {
		*pMode = value;
	}

	return iRet;
}


XMINH_RET
XMINH_ReadSecureMem(
	HANDLE							hXminh,
	LPBYTE							pData,
	WORD							wAddress,
	WORD							nBytes
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	BYTE							aBuf[64];
	MODBUS_REGISTER_CONTEXT			sContext = { 0x8000 | wAddress, (nBytes / 2), (LPWORD)aBuf, XMINH_SLAVE_ID_XMINH, 0, MODBUS_TIMEOUT_NORMAL };

	if (0 != (0x8000 & wAddress)) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	iRet = XMINH_MBTCP_ReadRegister(pXminh, &sContext);

	if (XMINH_RET_OK == iRet) {
		WORD						i;

		for (i = 0; i < (nBytes/2); i++) {
			pData[0 + (2 * i)] = aBuf[1 + (2 * i)];
			pData[1 + (2 * i)] = aBuf[0 + (2 * i)];
		}
	}

	return iRet;
}


XMINH_RET
XMINH_WriteSecureMem(
	HANDLE							hXminh,
	LPBYTE							pData,
	WORD							wAddress,
	WORD							nBytes
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	WORD							i;
	BYTE							aBuf[64];
	MODBUS_REGISTER_CONTEXT			sContext = { 0x8000 | wAddress, nBytes / 2, (LPWORD)aBuf, XMINH_SLAVE_ID_XMINH, 0, MODBUS_TIMEOUT_NORMAL };

	if ((0 != (0x8000 & wAddress)) || (64 < nBytes) || (0 != (0x3 & wAddress)) || (0 != (0x3 & nBytes))) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	for (i = 0; i < nBytes; i++) {
		if (0xff != pData[i]) {
			break;
		}
	}

	if (i >= nBytes) {
		return XMINH_RET_OK;
	}

	for (i = 0; i < (nBytes / 2); i++) {
		aBuf[0 + (2 * i)] = pData[1 + (2 * i)];
		aBuf[1 + (2 * i)] = pData[0 + (2 * i)];
	}

	return	XMINH_MBTCP_WriteMultipleRegisters(pXminh, &sContext);
}


XMINH_RET
XMINH_EraseSecureMem(
	HANDLE							hXminh,
	WORD							wAddress
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							aBuf[4] = {0xffff, 0xffff, 0xffff, 0xffff};
	MODBUS_REGISTER_CONTEXT			sContext = { 0x8000 | wAddress, 4, aBuf, XMINH_SLAVE_ID_XMINH, 0, MODBUS_TIMEOUT_NORMAL };

	if ((0 != (0x8000 & wAddress)) || (0 != (0x3 & wAddress))) {
		return XMINH_RET_INVALID_PARAMETER;
	}

	iRet = XMINH_MBTCP_WriteMultipleRegisters(pXminh, &sContext);
	return	iRet;
//	return	XMINH_MBTCP_WriteMultipleRegisters(pXminh, &sContext);
}


XMINH_RET
XMINH_SCHIP_SetPowerState(
	HANDLE							hXminh,
	SCHIP_POWER_STATE				nState
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							value = nState;
	MODBUS_REGISTER_CONTEXT			sContext = { XMINH_COIL_ID_SMEM_POWER - 1, 1, &value, XMINH_SLAVE_ID_XMINH, 0, MODBUS_TIMEOUT_NORMAL };

	iRet = XMINH_MBTCP_WriteSingleCoil(pXminh, &sContext);

	return iRet;
}


XMINH_RET
XMINH_SCHIP_GetPowerState(
	HANDLE							hXminh,
	PSCHIP_POWER_STATE				pState
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							value;
	MODBUS_REGISTER_CONTEXT			sContext = { XMINH_COIL_ID_SMEM_POWER - 1, 1, &value, XMINH_SLAVE_ID_XMINH, 0, MODBUS_TIMEOUT_NORMAL };

	iRet = XMINH_MBTCP_ReadCoil(pXminh, &sContext);
	if (XMINH_RET_OK == iRet) {
		*pState = value;
	}

	return iRet;
}



XMINH_RET
XMINH_GetCpuUniqId(
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	LPDWORD							pdwUid0,		// [OUT] CPU 고유 식별 번호 0
	LPDWORD							pdwUid1,		// [OUT] CPU 고유 식별 번호 1
	LPDWORD							pdwUid2,		// [OUT] CPU 고유 식별 번호 2
	XMINH_SLAVE_ID					nId				// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							aBuf[6];
	MODBUS_REGISTER_CONTEXT			sContext = { XMINH_HOLDING_ID_CPU_UID0 - 1, 6, aBuf, nId, 0, MODBUS_TIMEOUT_NORMAL };

	if (XMINH_SLAVE_ID_XMINH == nId) {
	}
	else if ((XMINH_SLAVE_ID_CAM1 == nId) || (XMINH_SLAVE_ID_CAM2 == nId)) {
		sContext.wAddress = MBRTU_REGISTER_ID_CPU_UID0 - 1;
	}
	else {
		return XMINH_RET_INVALID_PARAMETER;
	}

	iRet = XMINH_MBTCP_ReadRegister(pXminh, &sContext);
	if (XMINH_RET_OK == iRet) {
		*pdwUid0 = ((DWORD)(0x00ff & aBuf[0]) << 24) | ((DWORD)(0xff00 & aBuf[0]) << 8) | ((DWORD)(0x00ff & aBuf[1]) << 8) | ((DWORD)(0xff00 & aBuf[1]) >> 8);
		*pdwUid1 = ((DWORD)(0x00ff & aBuf[2]) << 24) | ((DWORD)(0xff00 & aBuf[2]) << 8) | ((DWORD)(0x00ff & aBuf[3]) << 8) | ((DWORD)(0xff00 & aBuf[3]) >> 8);
		*pdwUid2 = ((DWORD)(0x00ff & aBuf[4]) << 24) | ((DWORD)(0xff00 & aBuf[4]) << 8) | ((DWORD)(0x00ff & aBuf[5]) << 8) | ((DWORD)(0xff00 & aBuf[5]) >> 8);
	}

	return iRet;
}


XMINH_RET
XMINH_GetVersion(
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	LPWORD							pwHwVersion,	// [OUT] 하드웨어 버전 넘버
	LPWORD							pwSwVersion,	// [OUT] 소프트웨어 버전 넘버
	XMINH_SLAVE_ID					nId				// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							aBuf[2];
	MODBUS_REGISTER_CONTEXT			sContext = { XMINH_HOLDING_ID_HW_VERSION - 1, 2, aBuf, nId, 0, MODBUS_TIMEOUT_NORMAL };

	if (XMINH_SLAVE_ID_XMINH == nId) {
	}
	else if ((XMINH_SLAVE_ID_CAM1 == nId) || (XMINH_SLAVE_ID_CAM2 == nId)) {
		sContext.wAddress = MBRTU_REGISTER_ID_HW_VERSION - 1;
	}
	else {
		return XMINH_RET_INVALID_PARAMETER;
}

	iRet = XMINH_MBTCP_ReadRegister(pXminh, &sContext);
	if (XMINH_RET_OK == iRet) {
		*pwHwVersion = aBuf[0];
		*pwSwVersion = aBuf[1];
	}

	return iRet;
}


XMINH_RET
XMINH_EnableIWDG(
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId				// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
	WORD							value = 0xff00;
	MODBUS_REGISTER_CONTEXT			sContext = { XMINH_COIL_ID_IWDG_ENABLE - 1, 1, &value, XMINH_SLAVE_ID_XMINH, 0, MODBUS_TIMEOUT_NORMAL };

	if (XMINH_SLAVE_ID_XMINH == nId) {
		iRet = XMINH_MBTCP_WriteSingleCoil(pXminh, &sContext);
//		return XMINH_MBTCP_WriteSingleCoil(pXminh, &sContext);
		return iRet;
	}

	return XMINH_RET_INVALID_PARAMETER;
}

// NEW CMD for RGB Animation  20210203




XMINH_RET
XMINH_RgbAnimationControl (
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId,
	PRGB_ANIMATION_CONTROL			pControl		// [IN] 컨트롤
)
{
	PXMINH							pXminh = (PXMINH)hXminh;
	XMINH_RET						iRet;
////	WORD							value = 0x8001;
	MODBUS_REGISTER_CONTEXT			sContext = {MBRTU_REGISTER_ID_RGB_ANIMATION_CONTROL - 1, 1, (LPWORD)pControl, nId, 0, 30000 };


//	sContext.pData = &value;
	if ((XMINH_SLAVE_ID_CAM1 == nId) || (XMINH_SLAVE_ID_CAM2 == nId)) {
		iRet = XMINH_MBTCP_WriteRegister(pXminh, &sContext);
		return iRet;
	}
	iRet = XMINH_RET_INVALID_PARAMETER;
	return iRet;
}



