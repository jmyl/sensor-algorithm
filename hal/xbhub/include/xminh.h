#ifndef _XMINH_H_
#define	_XMINH_H_

#include "modbus.h"

/*
	XMINH 함수 출력 
*/
typedef	enum
{
	XMINH_RET_OK = 0,																											// 성공

	/* 아래는 모드버스 서버의 예외 응답 */
	XMINH_RET_MODBUS_EXCEPTION_CODE_ILLEGAL_FUNCTION_CODE = MODBUS_EXCEPTION_CODE_ILLEGAL_FUNCTION_CODE,						// 지원하지 않는 펑션코드 요청입니다
	XMINH_RET_MODBUS_EXCEPTION_CODE_ILLEGAL_DATA_ADDRESS = MODBUS_EXCEPTION_CODE_ILLEGAL_DATA_ADDRESS,							// 코일이나 레지스터의 어드레스가 유효하지 않습니다.
	XMINH_RET_MODBUS_EXCEPTION_CODE_ILLEGAL_DATA_VALUE = MODBUS_EXCEPTION_CODE_ILLEGAL_DATA_VALUE,								// 레지스터 쓰기 동작 중 값의 범위를 벗어난 쓰기 시도가 있었습니다.
	XMINH_RET_MODBUS_EXCEPTION_CODE_SLAVE_DEVICE_FAILURE = MODBUS_EXCEPTION_CODE_SLAVE_DEVICE_FAILURE,							// 슬레이브 디바이스에 오류가 발생하였습니다.
	XMINH_RET_MODBUS_EXCEPTION_CODE_ACKNOWLEDGE = MODBUS_EXCEPTION_CODE_ACKNOWLEDGE,											// 마스터의 요청에 슬레이브가 애크놀로지를 보냈습니다.
	XMINH_RET_MODBUS_EXCEPTION_CODE_SLAVE_DEVICE_BUSY = MODBUS_EXCEPTION_CODE_SLAVE_DEVICE_BUSY,								// 슬레이브 디바이스가 바쁩니다.
	XMINH_RET_MODBUS_EXCEPTION_CODE_MEMORY_PARITY_ERROR = MODBUS_EXCEPTION_CODE_MEMORY_PARITY_ERROR,							// 파일 입출력 중에 메모리 오류가 발생하였습니다.
	XMINH_RET_MODBUS_EXCEPTION_CODE_GATEWAY_PATH_UNAVAILABLE = MODBUS_EXCEPTION_CODE_GATEWAY_PATH_UNAVAILABLE,					// 게이트웨이 기능을 사용할 수 없습니다.
	XMINH_RET_MODBUS_EXCEPTION_CODE_GATEWAY_TARGET_DEVICE_FAILED_TO_RESPONSE = MODBUS_EXCEPTION_CODE_GATEWAY_TARGET_DEVICE_FAILED_TO_RESPONSE,	// 게이트웨이 타겟에서 응답이 없습니다.

	XMINH_RET_INVALID_HANDLE = 10000,																							// 잘못된 핸들러 호출입니다.
	XMINH_RET_INVALID_SOCKET,																									// 잘못된 소켓 핸들입니다.
	XMINH_RET_INVALID_PARAMETER,																								// 함수 파라미터 중에 널 포인터 등 잘못이 있습니다.
	XMINH_RET_CONNECT_FAIL,																										// 슬레이브 디바이스와 통신 연결에 실패하였습니다.
	XMINH_RET_TRANSMIT_FAIL,																									// 데이터 전송에 실패하였습니다.
	XMINH_RET_RECEIVE_FAIL,																										// 데이터 수신에 실패하였습니다.
	XMINH_RET_ILLEGAL_RESPONSE																									// 슬레이브 디바이스로 부터 받은 데이터에 오류가 발생하였습니다
}	XMINH_RET;


/*
	모드버스 슬레이브 디바이스 식별번호
*/
typedef	enum
{
	XMINH_SLAVE_ID_XMINH = 10,																									// XMINI-H 식별번호
	XMINH_SLAVE_ID_CAM1 = 11,																									// CAM1용 XLD3.5.1 식별번호
	XMINH_SLAVE_ID_CAM2 = 12																									// CAM2용 XLD3.5.1 식별번호
}	XMINH_SLAVE_ID;


/*
	XMINI-H의 모드버스 코일 식별번호
*/
typedef	enum
{
	XMINH_COIL_ID_SYSTEM_RESET = 1,																								// 시스템 리셋 코일
	XMINH_COIL_ID_HUB_RESET,																									// 이더넷 허브 리셋 코일
	XMINH_COIL_ID_WDT_ENABLE,																									// 와치독 기능 활성화 코일
	XMINH_COIL_ID_SMEM_POWER,																									// S-Chip 전원
	XMINH_COIL_ID_IWDG_ENABLE,																									// CPU Independent WatchDog 활성

	XMINH_COIL_ID_MAX
}	XMINH_COIL_ID;



/*
	XMINI-H의 모드버스 홀딩레지스터 식별번호
*/
typedef	enum
{
	XMINH_HOLDING_ID_KEEPALIVE_TIME = 1,																						// 와치독 타임아웃 값 (0 ~ 65536)msec
	XMINH_HOLDING_ID_CAM_SYNC_MODE,																								// 카메라 Sync Mode : 0 for reset mode, 1 for normal mode (CH1 master, CH2 slave), 2 for invert mode
	XMINH_HOLDING_ID_CPU_UID0,																									// CPU 고유 식별 아이디 0
	XMINH_HOLDING_ID_CPU_UID1,																									// CPU 고유 식별 아이디 1
	XMINH_HOLDING_ID_CPU_UID2,																									// CPU 고유 식별 아이디 2
	XMINH_HOLDING_ID_CPU_UID3,																									// CPU 고유 식별 아이디 3
	XMINH_HOLDING_ID_CPU_UID4,																									// CPU 고유 식별 아이디 4
	XMINH_HOLDING_ID_CPU_UID5,																									// CPU 고유 식별 아이디 5
	XMINH_HOLDING_ID_HW_VERSION,																								// XMINI-H 보드 하드웨어 리비전 넘버
	XMINH_HOLDING_ID_SW_VERSION,																								// XMINI-H 보드 소프트웨어 리비전 넘버


	XMINH_HOLDING_ID_MAX
}	XMINH_HOLDING_ID;


/*
	XLD3.5.1의 모드버스 코일 식별번호
*/
typedef		enum
{
	MBRTU_COIL_ID_SYSTEM_RESET = 1,																								// 리셋 코일
	MBRTU_COIL_ID_ZCAM_PWR,																										// ZCAM 파워 온오프 코일
	MBRTU_COIL_ID_IRLED_SCAN,																									// IRLED 리스캔 코일
	MBRTU_COIL_ID_WDT_ENABLE,																									// XLD3.5.1 와치독 온오프 코일
	MBRTU_COIL_ID_IWDG_ENABLE,																									// CPU 내부 와치독 활성화

	MBRTU_COIL_ID_MAX
}	MBRTU_COIL_ID;


/*
	XLD3.5.1의 모드버스 홀딩레지스터 식별번호
*/
typedef		enum
{
	MBRTU_REGISTER_ID_OPMODE = 1,																								// XLD3.5.1 동작 모드 레지스터 : 0 (리셋상태), 1 (싱크 마스터 모드), 2 (싱크 슬레이브 모드)
	MBRTU_REGISTER_ID_ALIVETIME,																								// 와치독 타이머 레지스터 : 0 (0초) ~ 65535 (65.535초)
	MBRTU_REGISTER_ID_IRLED_STATUS,																								// IRLED 상태 레지스터 : 0 (정상), 1 (무부하), 2 (저부하), 3 (과부하)
	MBRTU_REGISTER_ID_IRLED_BRI,																								// IRLED 밝기 레벨 레지스터 : 85 ~ 135, 리셋 후 디폴트 110
	MBRTU_REGISTER_ID_RGB1_1,																									// RGBLED 1번 LED의 칼라 코드 레지스터 : RGB565 포맷 [RRRRRGGGGGGBBBBB]
	MBRTU_REGISTER_ID_RGB1_2,																									// RGBLED 2번 LED의 칼라 코드 레지스터 : RGB565 포맷 [RRRRRGGGGGGBBBBB]
	MBRTU_REGISTER_ID_RGB1_3,																									// RGBLED 3번 LED의 칼라 코드 레지스터 : RGB565 포맷 [RRRRRGGGGGGBBBBB]
	MBRTU_REGISTER_ID_RGB1_4,																									// RGBLED 4번 LED의 칼라 코드 레지스터 : RGB565 포맷 [RRRRRGGGGGGBBBBB]
	MBRTU_REGISTER_ID_RGB1_5,																									// RGBLED 5번 LED의 칼라 코드 레지스터 : RGB565 포맷 [RRRRRGGGGGGBBBBB]
	MBRTU_REGISTER_ID_RGB1_6,																									// RGBLED 6번 LED의 칼라 코드 레지스터 : RGB565 포맷 [RRRRRGGGGGGBBBBB]
	MBRTU_REGISTER_ID_RGB2_1,																									// RGBLED 7번 LED의 칼라 코드 레지스터 : RGB565 포맷 [RRRRRGGGGGGBBBBB]
	MBRTU_REGISTER_ID_RGB2_2,																									// RGBLED 8번 LED의 칼라 코드 레지스터 : RGB565 포맷 [RRRRRGGGGGGBBBBB]
	MBRTU_REGISTER_ID_RGB2_3,																									// RGBLED 9번 LED의 칼라 코드 레지스터 : RGB565 포맷 [RRRRRGGGGGGBBBBB]
	MBRTU_REGISTER_ID_RGB2_4,																									// RGBLED 10번 LED의 칼라 코드 레지스터 : RGB565 포맷 [RRRRRGGGGGGBBBBB]
	MBRTU_REGISTER_ID_RGB2_5,																									// RGBLED 11번 LED의 칼라 코드 레지스터 : RGB565 포맷 [RRRRRGGGGGGBBBBB]
	MBRTU_REGISTER_ID_RGB2_6,																									// RGBLED 12번 LED의 칼라 코드 레지스터 : RGB565 포맷 [RRRRRGGGGGGBBBBB]
	MBRTU_REGISTER_ID_CPU_UID0,																									// CPU Uniq ID0
	MBRTU_REGISTER_ID_CPU_UID1,																									// CPU Uniq ID1
	MBRTU_REGISTER_ID_CPU_UID2,																									// CPU Uniq ID2
	MBRTU_REGISTER_ID_CPU_UID3,																									// CPU Uniq ID3
	MBRTU_REGISTER_ID_CPU_UID4,																									// CPU Uniq ID4
	MBRTU_REGISTER_ID_CPU_UID5,																									// CPU Uniq ID5
	MBRTU_REGISTER_ID_HW_VERSION,																								// XLD3.5.1 보드 하드웨어 리비전 넘버
	MBRTU_REGISTER_ID_SW_VERSION,																								// XLD3.5.1 보드 소프트웨어 리비전 넘버

	MBRTU_REGISTER_ID_RGB_ANIMATION_CONTROL,                 // 20210203																					// RGB Animation Control

	MBRTU_REGISTER_ID_MAX
}	MBRTU_REGISTER_ID;


/*
	RGBLED 부하 상태
*/
typedef	enum
{
	IRLED_LOAD_STATUS_NORMAL = 0,																								// 정상
	IRLED_LOAD_STATUS_NO_LOAD,																									// IRLED가 연결되지 않거나 선이 끊어져 있습니다.
	IRLED_LOAD_STATUS_UNDER_LOAD,																								// IRLED의 랭크가 저조도입니다.
	IRLED_LOAD_STATUS_OVER_LOAD																									// IRLED가 과부하이거나 단락되어 있습니다.
}	IRLED_LOAD_STATUS, * PIRLED_LOAD_STATUS;


/*
	IRLED 리스캔 상태
*/
typedef	enum
{
	IRLED_RESCAN_STATUS_COMPLETE = 0,																							// 리스캔이 종료되었습니다.
	IRLED_RESCAN_STATUS_DOING,																									// 현재 리스캔 진행주입니다.
}	IRLED_RESCAN_STATUS, * PIRLED_RESCAN_STATUS;


/*
	ZCAM 전원 상태
*/
typedef	enum
{
	ZCAM_POWER_STATE_OFF = 0,																									// ZCAM 전원이 파워오프 상태입니다.
	ZCAM_POWER_STATE_ON = 1,																									// ZCAM 전원이 파워온 상태입니다.
}	ZCAM_POWER_STATE, * PZCAM_POWER_STATE;


/*
	XLD3.5.1의 카메라 싱크 모드
*/
typedef	enum
{
	XMINH_CAMMODE_RESET = 0,																									// 리셋 상태
	XMINH_CAMMODE_MASTER = 1,																									// 카메라 싱크가 마스터 모드입니다.
	XMINH_CAMMODE_SLAVE = 2,																									// 카메라 싱크가 슬레이브 모드입니다.
}	XMINH_CAMMODE, * PXMINH_CAMMODE;



/*
	S-Chip 전원 상태
*/
typedef	enum
{
	SCHIP_POWER_STATE_OFF = 0,																									// S-Chip 전원이 파워오프 상태입니다.
	SCHIP_POWER_STATE_ON = 1,																									// S-Chip 전원이 파워온 상태입니다.
}	SCHIP_POWER_STATE, * PSCHIP_POWER_STATE;


/*
	XMINI-H 보드의 카메라 싱크 모드
*/
typedef	enum
{
	XMINH_SYNC_MODE_RESET = 0,																									// 리셋 상태
	XMINH_SYNC_MODE_NORMAL = 1,																									// CH1 master. CH2 slave
	XMINH_SYNC_MODE_INVERT = 2,																									// CH1 slave, CH2 master
}	XMINH_SYNC_MODE, * PXMINH_SYNC_MODE;



/*
	XMINI-H TCP 연결 설정
*/
typedef	struct				_XMINH_CFG_
{
	TCHAR					szIpAddress[30];																					// XMINI-H IP 어드레스																		
	WORD					wIpPort;																							// XMINI-H 포트 번호
}	XMINH_CFG,				*PXMINH_CFG;


/*
	XMINI-H 핸들
*/
typedef	struct						_XMINH_
{
	SOCKET							Socket;																						// TCP 소켓 핸들
	struct sockaddr_in				AddressInfo;																				// IP 어드레스, 포트 정보
	WORD							wTransactionId;																				// 모드버스 트랜잭션 식별번호 (각 트랜잭션마다 1씩 증가)
}	XMINH, * PXMINH;


/*
	모드버스 트랜잭션
*/
typedef	struct						_MBTCP_TRANSACT_
{
	LPBYTE							pSndBuf;																					// 송신 버퍼
	LPBYTE							pRcvBuf;																					// 수신 버퍼
	int								iSndBytes;																					// 송신할 데이터 바이트 수
	int								iRcvBytes;																					// 수신된 데이터 바이트 수
	int								iSndBufSiz;																					// 송신 버퍼의 사이즈
	int								iRcvBufSiz;																					// 수신 버퍼의 사이즈
	WORD							wTransactionId;																				// 모드버스 트랜잭션 아이디
	BYTE							bySlaveId;																					// 슬레이브 디바이스 아이디, XMINH_SLAVE_ID 참조
	DWORD							dwTansactTimeout;																			// 최대 응답대기 시간 (마이크로세컨드 단위)
}	MBTCP_TRANSACT,					*PMBTCP_TRANSACT;


#pragma warning(disable:4214)
typedef	struct				_RGB_ANIMATION_CONTROL_			// 20210203
{
	WORD					RotationType	: 	2;			// 0 (pause), 1 (CW), 2 (CCW), 3 (invalid, pause)
	WORD					GradientType	: 	2;			// future use
	WORD					AnimationSpeed	:	4;			// 0 (fast), 15 (slowest)
	WORD					reserved		:	7;			// reserved
	WORD					AnimationMode	:	1;			// 0 (no animation, direct put), 1 (animation mode)
}	RGB_ANIMATION_CONTROL,	*PRGB_ANIMATION_CONTROL;
#pragma warning(default:4214)


#ifdef __cplusplus
extern "C" {
#endif


/*
	모드버스 요청과 응답으로 이루어진 하나의 트랜잭션을 수행합니다
*/
XMINH_RET	XMINH_Transact(							// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	PMBTCP_TRANSACT					pTransact		// [IN] 트랜잭션 구조체, MBTCP_TRANSACT 참조
	);


/*
	XMINH 핸들을 생성합니다 
*/
HANDLE		XMINH_Create(							// [OUT] 성공하면 핸들 값, 실패하면 INVALID_HANDLE_VALUE
	PXMINH_CFG						pCfg			// [IN] TCP 연결을 위한 구조체, XMINH_CFG 참조
	);


/*
	XMINH_Create 함수에 의해 생성된 핸들을 제거합니다.
*/
XMINH_RET	XMINH_Delete(							// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	);


/*
	XMINI-H와 통신 연결을 시도합니다.
*/
XMINH_RET	XMINH_Connect(							// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	);


/*
	XMINI-H와 통신 연결을 분리합니다.
*/
XMINH_RET	XMINH_Disconnect(						// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	);


/*
	수신버퍼의 MBAP (MODBUS Application Protocol) 헤더의 무결성을 검사합니다.
*/
BOOL		XMINH_MBTCP_IsValidMbap(				// [OUT] : 검사 결과 오류가 없으면 TRUE, 아니면 FALSE
	PMBTCP_TRANSACT					pTransaction	// [IN] : 트랜잭션 구조체, MBTCP_TRANSACT 참조
	);


/*
	MODBUS-TCP 규격의 Read Holding Register 구현 : 슬레이브 디바이스의 홀딩레지스터의 내용을 읽어 옵니다.
*/
XMINH_RET	XMINH_MBTCP_ReadRegister(				// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	PMODBUS_REGISTER_CONTEXT		pContext		// [IN] 액세스할 레지스터를 기술, MODBUS_REGISTER_CONTEXT 참조
	);


/*
	MODBUS-TCP 규격의 Write Single Register 구현 : 슬레이브 디바이스의 하나의 홀딩레지스터에 데이터를  저장합니다.
*/
XMINH_RET	XMINH_MBTCP_WriteRegister(				// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	PMODBUS_REGISTER_CONTEXT		pContext		// [IN] 액세스할 레지스터를 기술, MODBUS_REGISTER_CONTEXT 참조
	);


/*
	MODBUS-TCP 규격의 Write Multiple Register 구현 : 슬레이브 디바이스의 여러개의 홀딩레지스터에 데이터를  저장합니다.
*/
XMINH_RET	XMINH_MBTCP_WriteMultipleRegisters(		// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	PMODBUS_REGISTER_CONTEXT		pContext		// [IN] 액세스할 레지스터를 기술, MODBUS_REGISTER_CONTEXT 참조
	);


/*
	MODBUS-TCP 규격의 Read Coil 구현 : 슬레이브 디바이스로부터 여러개의 코일 값을 읽어 옵니다.
*/
XMINH_RET	XMINH_MBTCP_ReadCoil(					// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	PMODBUS_REGISTER_CONTEXT		pContext		// [IN] 액세스할 레지스터를 기술, MODBUS_REGISTER_CONTEXT 참조
	);


/*
	MODBUS-TCP 규격의 Read Coil 구현 : 슬레이브 디바이스의 하나의 코일에 값을 저장합니다.
*/
XMINH_RET	XMINH_MBTCP_WriteSingleCoil(			// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	PMODBUS_REGISTER_CONTEXT		pContext		// [IN] 액세스할 레지스터를 기술, MODBUS_REGISTER_CONTEXT 참조
	);


/*
	RGBLED에 Color 값을 변경합니다. 
*/
XMINH_RET	XMINH_RGBLED_Set(						// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId,			// [IN] 변경할 LED 모듈의 식별변호, XMINH_SLAVE_ID 참조
	LPWORD							pLedInfo		// [IN] 색상 정보, LED 모듈에는 12개의 LED가 있으면 각 LED는 RGB565 포맷 [RRRRRGGGGGGBBBBB]
	);


/*
	RGBLED의 현재 Color 값을 읽어 옵니다.
*/
XMINH_RET	XMINH_RGBLED_Get(						// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId,			// [IN] LED 모듈의 식별변호, XMINH_SLAVE_ID 참조
	LPWORD							pLedInfo		// [OUT] 색상 정보, LED 모듈에는 12개의 LED가 있으면 각 LED는 RGB565 포맷 [RRRRRGGGGGGBBBBB]
	);

XMINH_RET	XMINH_RgbAnimationControl(
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId,
	PRGB_ANIMATION_CONTROL			pControl		// [IN] 컨트롤
);
/*
	IRLED의 밝기를 변경합니다.
*/
XMINH_RET	XMINH_IRLED_Set(						// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId,			// [IN] LED 모듈의 식별변호, XMINH_SLAVE_ID 참조
	WORD							wBrightIndex	// [IN] 밝기 인데스 (85 ~ 135)
	);


/*
	IRLED의 현재 밝기 값을 읽어 옵니다.
*/
XMINH_RET	XMINH_IRLED_Get(						// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId,			// [IN] LED 모듈의 식별변호, XMINH_SLAVE_ID 참조
	LPWORD							pwBrightIndex	// [OUT] 밝기 인데스(85 ~135)
	);


/*
	IRLED의 부하 상태를 읽어 옵니다
*/
XMINH_RET	XMINH_IRLED_GetStatus(					// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId,			// [IN] LED 모듈의 식별변호, XMINH_SLAVE_ID 참조
	PIRLED_LOAD_STATUS				pStatus			// [OUT] LED의 부하 상태, IRLED_LOAD_STATUS 참조
	);


/*
	카메라의 마스터 및 슬레이브 동작 모드 설정합니다
*/
XMINH_RET	XMINH_CAMMODE_Set(						// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId,			// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
	XMINH_CAMMODE					nMode			// [IN] 동작 모드, XMINH_CAMMODE 참조
	);


/*
	카메라의 마스터 및 슬레이브 동작 모드를 읽어 옵니다
*/
XMINH_RET	XMINH_CAMMODE_Get(						// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId,			// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
	PXMINH_CAMMODE					pMode			// [IN] 동작 모드, XMINH_CAMMODE 참조
	);


/*
	IRLED를 재스캔합니다
	재스캔 동작 시간이 2초이며 
	이 시간동안 모드버스 통신도 작동하지 않으므로
	2초 후 XMINH_IRLED_IsRescanComplete 를 사용하여 확인하는 것이 좋다
*/
XMINH_RET	XMINH_IRLED_Rescan(						// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId				// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
	);


/*
	IRLED를 재스캔이 완료되었는지 확인합니다
*/
XMINH_RET	XMINH_IRLED_IsRescanComplete(			// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId,			// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
	PIRLED_RESCAN_STATUS			pStatus			// [OUT] 리스캔 동작 상태, IRLED_RESCAN_STATUS 참조
	);


/*
	ZCAM 모듈의 전원을 설정합니다.
*/
XMINH_RET	XMINH_ZCAM_PowerControl(				// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId,			// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
	ZCAM_POWER_STATE				nState			// [IN] 전원 상태, ZCAM_POWER_STATE 참조
	);


/*
	ZCAM 모듈의 전원 상태를  확인합니다.
*/
XMINH_RET	XMINH_ZCAM_GetPowerState(				// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId,			// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
	PZCAM_POWER_STATE				pStatus			// [IN] 전원 상태, ZCAM_POWER_STATE 참조
	);


/*
	카메라 모듈 전체를 리셋합니다
*/
XMINH_RET	XMINH_ResetCamera(						// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId				// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
	);


/*
	와치독타이머 값을 변경합니다. 초기값은 65.535초입니다
*/
XMINH_RET	XMINH_SetWatchdogTimeoutValue(			// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId,			// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
	WORD							wTimeout		// in [ms]
	);


/*
	와치독 기능을 시작합니다
*/
XMINH_RET	XMINH_EnableWatchdog(					// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId				// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
	);


/*
	와치독 기능을 중지합니다
*/
XMINH_RET	XMINH_DisableWatchdog(					// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId				// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
	);


/*
	와치독 타이머가 종료되지 않도록 주기적으로 와치독을 리셋합니다.
*/
XMINH_RET	XMINH_KickWatchdog(						// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	PDWORD							pStatus,		// [OUT] Synch Mode Status
	XMINH_SLAVE_ID					nId				// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
);


/*
	XMINI-H 보드의 Ethernet HUB 칩을 리셋합니다.
	리셋 후에는 XMINH 모든 서비가 중지되므로 
	XMINH_Disconnect, XMINH_Delete 함수를 차례로 호출하여 종료 후
	XMINH_Create, XMINH_Connect 로 재연결하여야 합니다.

*/
XMINH_RET
XMINH_ResetHub(
	HANDLE							hXminh			// [IN] XMINH_Create 함수에 의해 생성된 핸들
);


/*
	XMINI-H 보드의 마이컴를 리셋합니다.
	리셋 후에는 XMINH 모든 서비가 중지되므로
	XMINH_Disconnect, XMINH_Delete 함수를 차례로 호출하여 종료 후
	XMINH_Create, XMINH_Connect 로 재연결하여야 합니다.

*/
XMINH_RET
XMINH_ResetXminh(
	HANDLE							hXminh			// [IN] XMINH_Create 함수에 의해 생성된 핸들
);


/*
	XMINI-H 보드의 카메라 싱크 모드를 설정합니다.
*/
XMINH_RET
XMINH_SetXminhMode(
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SYNC_MODE					nMode			// [IN] 카메라 싱크 모드, XMINH_SYNC_MODE 참조
);


/*
	XMINI-H 보드의 현재 카메라 싱크 모드를 확인합니다.
*/
XMINH_RET
XMINH_GetXminhMode(
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	PXMINH_SYNC_MODE				pMode			// [OUT] 카메라 싱크 모드, XMINH_SYNC_MODE 참조
);



/*
	XMINI-H 보드의 S-Chip 전원을 켜거나 끕니다.
*/
XMINH_RET
XMINH_SCHIP_SetPowerState(
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	SCHIP_POWER_STATE				nState			// [IN] S-Chip 전원상태, SCHIP_POWER_STATE 참조
);



/*
	XMINI-H 보드의 S-Chip 전원의 상태를 확인합니다.
*/
XMINH_RET
XMINH_SCHIP_GetPowerState(
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	PSCHIP_POWER_STATE				pState			// [OUT] S-Chip 전원상태, SCHIP_POWER_STATE 참조
);


/*
	XMINI-H 보드의 S-Chip 메모리 읽기입니다.
*/
XMINH_RET
XMINH_ReadSecureMem(
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	LPBYTE							pData,			// [OUT] 읽기용 버퍼
	WORD							wAddress,		// [IN] 어드레스, 0x8000 보다 작아야 하며, 4의 배수
													//      [0x0000 ~ 0x2FFF] : F/W Binary
													//		[0x3C00 ~ 0x5FFF] : Secure Code Page
	WORD							nBytes			// [IN] 써야 할 바이트 수, 최대 64바이트, 4의배수
);



/*
	XMINI-H 보드의 S-Chip 메모리 쓰기입니다.
*/
XMINH_RET
XMINH_WriteSecureMem(
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	LPBYTE							pData,			// [OUT] 쓰기용 버퍼
	WORD							wAddress,		// [IN] 어드레스, 0x8000 보다 작아야 하며, 4의 배수
													//      [0x0000 ~ 0x2FFF] : F/W Binary
													//		[0x3C00 ~ 0x5FFF] : Secure Code Page
	WORD							nBytes			// [IN] 써야 할 바이트 수, 최대 64바이트, 4의배수
);


/*
	XMINI-H 보드의 S-Chip 메모리 지우기입니다.
*/
XMINH_RET
XMINH_EraseSecureMem(
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	WORD							wAddress		// [IN] 어드레스, 0x8000 보다 작아야 하며, 4의 배수
													//      [0x0000 ~ 0x2FFF] : F/W Binary
													//		[0x3C00 ~ 0x5FFF] : Secure Code Page
);



/*
	CPU 고유식별번호를 읽어 옵니다.
*/
XMINH_RET
XMINH_GetCpuUniqId(
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	LPDWORD							pdwUid0,		// [OUT] CPU 고유 식별 번호 0
	LPDWORD							pdwUid1,		// [OUT] CPU 고유 식별 번호 1
	LPDWORD							pdwUid2,		// [OUT] CPU 고유 식별 번호 2
	XMINH_SLAVE_ID					nId				// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
);


/*
	보드의 버전 정보를 읽어 옵니다.
*/
XMINH_RET
XMINH_GetVersion(
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	LPWORD							pwHwVersion,	// [OUT] 하드웨어 버전 넘버
	LPWORD							pwSwVersion,	// [OUT] 소프트웨어 버전 넘버
	XMINH_SLAVE_ID					nId				// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
);


/*
	CPU 내부 와치독 타이머를 활성화 시킵니다.
*/
XMINH_RET
XMINH_EnableIWDG(
	HANDLE							hXminh,			// [IN] XMINH_Create 함수에 의해 생성된 핸들
	XMINH_SLAVE_ID					nId				// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
);


#ifdef __cplusplus
}
#endif


#endif // !_XMINH_H_
