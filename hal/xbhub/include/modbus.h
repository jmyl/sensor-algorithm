
#ifndef _MODBUS_H_
#define _MODBUS_H_


#include <windows.h>


/*
	모드버스 펑션 코드 리스트
*/
typedef	enum
{
	MODBUS_FUNCTION_CODE_READ_COILS = 1,							// 코일 값 읽기 (참고) 코일은 입출력이 가능한 1비트 짜리 I/O이다.
	MODBUS_FUNCTION_CODE_READ_DISCRETE_INPUTS = 2,					// 디스크리트 입력 읽기 (참고) 디스크리트 입력은 1비트 짜리 입력 전용이다. GPIO의 입력 핀이 그 예이다
	MODBUS_FUNCTION_CODE_READ_HOLDING_REGISTERS = 3,				// 홀딩 레지스터 값 읽기 (참고) 홀딩 레지스터는 입출력이 가능하다.
	MODBUS_FUNCTION_CODE_READ_INPUT_REGISTER = 4,					// 입력 레지스터 읽기 (참고) INPUT REGISTER 는 읽기만 가능한 레지스터이다, 8비트 디지털 입력 포트가 그 예이다.
	MODBUS_FUNCTION_CODE_WRITE_SINGLE_COIL = 5,						// 하나의 코일에 쓰기
	MODBUS_FUNCTION_CODE_WRITE_SINGLE_REGISTER = 6,					// 하나의 홀딩 레지스터에 쓰기
	MODBUS_FUNCTION_CODE_READ_EXCEPTION_STATUS = 7,					// 예외 상태 읽기
	MODBUS_FUNCTION_CODE_DIAGNOSTIC = 8,							// 진단 시도
	MODBUS_FUNCTION_CODE_GET_COMM_EVENT_COUNTER = 11,				// 
	MODBUS_FUNCTION_CODE_GET_COMM_EVENT_LOG = 12,					//
	MODBUS_FUNCTION_CODE_WRITE_MULTIPLE_COILS = 15,					// 여러개의 코일을 쓰기
	MODBUS_FUNCTION_CODE_WRITE_MULTIPLE_REGISTERS = 16,				// 여러개의 홀딩 레지스터에 쓰기
	MODBUS_FUNCTION_CODE_REPORT_SLAVE_ID = 17,				
	MODBUS_FUNCTION_CODE_READ_FILE_RECORD = 20,
	MODBUS_FUNCTION_CODE_WRITE_FILE_RECORD = 21,
	MODBUS_FUNCTION_CODE_MASK_WRITE_REGISTER = 22,
	MODBUS_FUNCTION_CODE_MODIFY_MULTIPLE_REGISTERS = 23,
	MODBUS_FUNCTION_CODE_READ_FIFO_QUEUE = 24,
	MODBUS_FUNCTION_CODE_READ_DEVICE_IDENTIFICATION = 43,
	MODBUS_FUNCTION_CODE_ENCAPSULATED_INTERFACE_TRANSPORT = 43

}	MODBUS_FUNCTION_CODE;


/*
	모드버스 슬레이브 디바이스에서 보내온 예외 코드 리스트
*/
typedef	enum
{
	MODBUS_EXCEPTION_CODE_ILLEGAL_FUNCTION_CODE = 1,				// 지원하지 않는 펑션코드
	MODBUS_EXCEPTION_CODE_ILLEGAL_DATA_ADDRESS = 2,					// 허용되지 않는 어드레스
	MODBUS_EXCEPTION_CODE_ILLEGAL_DATA_VALUE = 3,					// 허용되지 않는 값의 쓰기 시도
	MODBUS_EXCEPTION_CODE_SLAVE_DEVICE_FAILURE = 4,					// 슬레이브 디바이스 오류
	MODBUS_EXCEPTION_CODE_ACKNOWLEDGE = 5,							// 파일 입출력 등에서 I/O Pending을 지원하지 위해 리퀘스트에 대한 데이터 응답없이 리퀘스트를 잘 수신했다는 것을 알려서 타임아웃을 방지
	MODBUS_EXCEPTION_CODE_SLAVE_DEVICE_BUSY = 6,					// 슬레이브 디바이스가 BUSY 상태
	MODBUS_EXCEPTION_CODE_MEMORY_PARITY_ERROR = 8,					// 메모리 오류
	MODBUS_EXCEPTION_CODE_GATEWAY_PATH_UNAVAILABLE = 10,			// 게이트웨이 패스에 오류가 발생
	MODBUS_EXCEPTION_CODE_GATEWAY_TARGET_DEVICE_FAILED_TO_RESPONSE = 11,	// 게이트웨이의 타겟 디바이스에서 응답이 없음

}	MODBUS_EXCEPTION_CODE;


/*
	MODBUS 레지스터, 코일 등을 액세스하기 위해 필요한 정보
*/
typedef	struct					_MODBUS_REGISTER_CONTEXT_
{
	WORD						wAddress;					// 어드레스 : MODBUS에서 레지스터와 코일의 어드레스는 각 레지스터와 코일 아이디에서 1을 뺀 값을 사용하도록 되어 있다.
	WORD						wQuantity;					// 개수 : 액세스할 레지스터와 코일의 개수
	LPWORD						pData;						// 데이터 버퍼 : 읽기 혹은 쓰기용 데이터 버퍼
	WORD						wSlaveId;					// 타겟 슬레이브 아이디
	WORD						wExceptionCode;				// 타겟의 오류로 인한 예외 응답 코드
	DWORD						dwTimeout;					// 요청 후 응답 까지 최대 대기 시간, 단위는 마이크로 세컨드
}	MODBUS_REGISTER_CONTEXT,	*PMODBUS_REGISTER_CONTEXT;



#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif

#endif /* _MODBUS_H_ */

