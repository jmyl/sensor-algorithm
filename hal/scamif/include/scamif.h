/*!
 *******************************************************************************
                                                                                
                    Creatz SCAM Interface
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 ~ 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   scrmif.h
	 @brief  
	 @author YongHo Suk
	 @date   2015/11/30 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_SCRMIF_H_)
#define		 _SCRMIF_H_

#include "cr_common.h"

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#if defined(PCIE_CHANNEL)
#define GIGEV_CHANNEL	0
#else
#define GIGEV_CHANNEL	1
#endif

//-----
#define	SCAMIF_STATE_NULL		0x0000
#define	SCAMIF_STATE_CREATED	0x0001
#define	SCAMIF_STATE_INITED		0x0002
#define	SCAMIF_STATE_STARTED	0x0003
#define	SCAMIF_STATE_STOPPED	0x0004
#define	SCAMIF_STATE_DELETED	0x0005

//#define SCAMIF_NULL				0
//#define SCAMIF_MASTER			1
//#define SCAMIF_SLAVE			2


#define SCAMIF_MASTER			0
#define SCAMIF_SLAVE			1


//#define	SCAMIF_MASTERSLAVE_ATCAM0		SCAMIF_SLAVE
//#define	SCAMIF_MASTERSLAVE_ATCAM1		SCAMIF_SLAVE




//#if CAMSENSOR_CATEGORY == CAMSENSOR_CBALL
//#define	SCAMIF_MASTERSLAVE_CAM0		SCAMIF_SLAVE
//#define	SCAMIF_MASTERSLAVE_CAM1		SCAMIF_MASTER
//#else
//#define	SCAMIF_MASTERSLAVE_CAM0		SCAMIF_MASTER
//#define	SCAMIF_MASTERSLAVE_CAM1		SCAMIF_SLAVE
//#endif


// ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ

//#define	SCAMIF_MASTERSLAVE_CAM0		SCAMIF_MASTER
//#define	SCAMIF_MASTERSLAVE_CAM1		SCAMIF_SLAVE


//#define	SCAMIF_MASTERSLAVE_CAM0		SCAMIF_SLAVE
//#define	SCAMIF_MASTERSLAVE_CAM1		SCAMIF_MASTER

#if !defined(MAXCAMCOUNT)
#define MAXCAMCOUNT				2		/*! For Linux, this is defined in platform.mak */
#endif

#if !defined(NUM_CAM)
#define NUM_CAM				2 		/*! 실제 사용되는 캠의 개수. 4 CAM 구현이 완료되면 deprecated되고 MAXCAMCOUNT만 사용 */
#endif


#define SCAMIF_CAMSTATE_INIT			0
#define SCAMIF_CAMSTATE_START			1
#define SCAMIF_CAMSTATE_PAUSE			2
#define SCAMIF_CAMSTATE_STOP			3
#define SCAMIF_CAMSTATE_BULK			4

#define SCAMCAMSTATE_INIT				0
#define SCAMCAMSTATE_START				1
#define SCAMCAMSTATE_PAUSE				2
#define SCAMCAMSTATE_STOP				3
#define SCAMCAMSTATE_BULK				4


/*
#define SCAMBUFFERSTATE_EMPTY				0
#define SCAMBUFFERSTATE_FULL				1
#define SCAMBUFFERSTATE_BUSY				2
*/


//---
#define DEF_WIDTH						1280
//#define DEF_HEIGHT						1280
#define DEF_HEIGHT						1024

#define DEF_FRAMERATE					30
#define DEF_GAIN						(256 * 10)
#define DEF_EXPOSURE					20
#define DEF_GVSP_PORT(x) 				(15566+(x))	// Master Image receiving HOST Port
#define DEF_GVCP_CAMPORT 				3956		// Device Camera Port
#define DEF_GVCP_MSGPORT(x) 			(54377+(x))	// MSG receiving HOST port

//--
#define MAKEU64(h,l)	( ((((U64)(h))<<32) & 0xFFFFFFFF00000000LL) | (((U64)(l)) & 0x00000000FFFFFFFFLL) )
#define EXTRACTH(x)		( (U32) ((x) >> 32) & 0xFFFFFFFF )
#define EXTRACTL(x)		( (U32) ((x)      ) & 0xFFFFFFFF )

//----

// Image buffer count test... 2016/06/09
#define SCAMIF_BUFFER_COUNT1			16
#define SCAMIF_BUFFER_COUNT2			32  // (SCAMIF_BUFFER_COUNT1 * 2)
#define SCAMIF_BUFFER_COUNT4			64	// (SCAMIF_BUFFER_COUNT1 * 4)
#define SCAMIF_BUFFER_COUNT8			128 // (SCAMIF_BUFFER_COUNT1 * 8)
#define SCAMIF_BUFFER_COUNT16			256 // (SCAMIF_BUFFER_COUNT1 * 16)

//#define SCAMIF_BUFFER_COUNT1			32
//#define SCAMIF_BUFFER_COUNT2			64  // (SCAMIF_BUFFER_COUNT1 * 2)
//#define SCAMIF_BUFFER_COUNT4			128	// (SCAMIF_BUFFER_COUNT1 * 4)
//#define SCAMIF_BUFFER_COUNT8			256 // (SCAMIF_BUFFER_COUNT1 * 8)
//#define SCAMIF_BUFFER_COUNT16			512 // (SCAMIF_BUFFER_COUNT1 * 16)


// Image buffer count ORG
#define SCAMIF_BUFFER_SIZE1				(1280 * 1024)
#define SCAMIF_BUFFER_SIZE2				(SCAMIF_BUFFER_SIZE1/2)
#define SCAMIF_BUFFER_SIZE4				(SCAMIF_BUFFER_SIZE1/4)
#define SCAMIF_BUFFER_SIZE8				(SCAMIF_BUFFER_SIZE1/8)
#define SCAMIF_BUFFER_SIZE16			(SCAMIF_BUFFER_SIZE1/16)

#define SCAMIF_BUFFER_GUARD				(SCAMIF_BUFFER_SIZE1)

#define SCAMIF_BUFFER_SIZE_BUFFER		(20 * SCAMIF_BUFFER_SIZE1)					// 20191122


//--
#define TSSCALE_D 	((double) (1000000000.0))			//		1_000_000_000    10^9.   1/TSSCALE_D = 10^-9  sec = 1 nsec
#define TSSCALE 	(1000000000LL)
//---


#define SCAMIF_LOCK_NULL				0x0000
#define SCAMIF_LOCK_WRITE				0x0001
#define SCAMIF_LOCK_READ				0x0002
#define SCAMIF_LOCK_WRITE_DATAMEMONLY	0x0004
#define SCAMIF_LOCK_READ_DATAMEMONLY	0x0008


#define SCAMIF_LOCK_NORMAL_WRITE				0x0010
#define SCAMIF_LOCK_NORMAL_READ					0x0020
#define SCAMIF_LOCK_NORMAL_WRITE_DATAMEMONLY	0x0040
#define SCAMIF_LOCK_NORMAL_READ_DATAMEMONLY		0x0080

#define SCAMIF_LOCK_BULK_WRITE					0x0100
#define SCAMIF_LOCK_BULK_READ					0x0200
#define SCAMIF_LOCK_BULK_WRITE_DATAMEMONLY		0x0400
#define SCAMIF_LOCK_BULK_READ_DATAMEMONLY		0x0800








/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif


typedef struct
{
	I32 updateMode;
	I32 syncdiv; 
	I32 width; 
	I32 height;
	I32 sx; 
	I32 sy;
	I32 fps;
	I32 gain;
	I32 exposureusec;
	I32 skip;
	I32 multitude4normal;
	I32 zrot; 
	I32 autoinit;
	I32 exposure_post; 
	I32 exposure_expiate; 
} CAM_IMG_ATTR_T;

enum {
	CAM_IMAGE_PARAM_GAMMA,	// Gamma: 100 == 1.0 
	CAM_IMAGE_PARAM_CONTRAST,	// Contrast: 100 == 1.0  
	CAM_IMAGE_PARAM_BRIGHT, 	// Bright: 100 == 0 
	CAM_IMAGE_PARAM_GAIN,
	CAM_IMAGE_PARAM_DELAY_OFFSET,		// Exposure Delay offset: usec
	CAM_IMAGE_PARAM_EXPOSURE_EXPIATE,		//  Exposure Expiate time.
	CAM_IMAGE_PARAM_LED_BRIGHT, // brightness: 8bit. (0 ~ 255)
	CAM_IMAGE_PARAM_ROI_OFFSET,	
	CAM_IMAGE_PARAM_ROI_SIZE,	
};


enum {
	CAM_INFO_BOARD,
	CAM_INFO_PC,	// ???
	CAM_INFO_CPUSID,
	CAM_INFO_CPUBDSERIAL,
	CAM_INFO_RUNCODE,
	CAM_INFO_BD_DATA_RSVD,
};

/**** CAM Run State for Eyexo2 ****/
typedef enum CAM_Run_State_T_{
	CAM_RUN_STATE_NO_CHANGE	= 0,
	CAM_RUN_STATE_IDLE,
	CAM_RUN_STATE_NORMAL,
	CAM_RUN_STATE_NORMAL_CIRCLE_DETECTION,
	CAM_RUN_STATE_LAUNCH_DETECTING,
	CAM_RUN_STATE_END
} CAM_Run_State_T;


typedef I32(*CR_video_fnCB)(void *pData, I32 nSize, 
		U32 ts_high, U32 ts_low, 
		I32 size_x, I32 size_y, 
		I32 offset_x, I32 offset_y,
		U32 normalbulk, U32 skip, U32 multitude,		// 
		void *pParam);


typedef struct _camimageinfo {
	U32 width;
	U32 height;
	//
	//U32 runProccessingWidth;
	//U32 runProccessingHeight;

	U32 offset_x;
	U32 offset_y;
	U32 framerate;				// frame per sec..
	U32 gain;					// 
	U32 exposure;				// usec

	//--
	U32 skip;					// skip count
	U32 multitude;				// multitude count
	U32 syncdiv;				// Sync Divide count

	//--
	U32 ledbrgt;
	U32 lut_bright;
	U32 lut_contrast;
	U32 lut_gamma;


	U32 exposure_post;			// FW default value: 0			// 2019-11/29
	U32 exposure_expiate;		// FW default value: 34
} camimageinfo_t;

#define MAXMULT	15
typedef struct _camif_buffer_info {
//	U32 runmode;		// 		0: NULL, 1: Normal, 2: BULK
	U32 state;		
	U32 ts_h;
	U32 ts_l;
	U32 frameindex;		// Frameindex....

	U64 ts64m[MAXMULT];			
	camimageinfo_t cii;
} camif_buffer_info_t;

typedef struct _camif_buffergroup {
	HAND 	hmutex;
	U32 valid;
	U32 count;				// buffer count. Must be 2^n number.
	U32 windexb;			// next write index for buf; (0 ~ count-1)
	U32 rindexb;			// next read index for buf;  (0 ~ count-1)
	//---------

	camif_buffer_info_t	buffergroupinfo;			// Last accessed param..
	
	//
	camif_buffer_info_t bif[SCAMIF_BUFFER_COUNT16];
	U08 *pbuffer[SCAMIF_BUFFER_COUNT16];
	//U08 datamem[SCAMIF_BUFFER_COUNT1 * SCAMIF_BUFFER_SIZE1 + SCAMIF_BUFFER_GUARD];
	U08 *datamem;
} camif_buffergroup_t;

typedef struct _scamcam {
	U32 camid;
	U32 state;					// Init, Start, Pause, Stop, BULK
	U32 connected;				// 0: Not connected, 1: Connected
	U32 masterslave;
	U32 camip;
	U32 gvcpport;				// Cam-side GVCP-port
	U32 gvspport;				// PC-side GVSP-port

	U32 timetick;				// Last Success time tick.

//	HAND	hgev;				// Handle of gev
	HAND	hgecp;				// Handle of gecp
	HAND	hsci;				// Handle of scamif..
	//--
	U32 needInitBuffer;

	//--
	camimageinfo_t	imginfo;
	camimageinfo_t	imginfo_shadow;

	camimageinfo_t	imginfo_BULK;						// image information for BULK.
	camimageinfo_t	imginfo_BULK_shadow;


	U32 needCamimageInfoUpdate;

	//--	Buffer management
	camif_buffergroup_t nb;			// normal buffer
	camif_buffergroup_t bb;			// BULK buffer
} scamcam_t;


#define CAMWORKMODE_NULL		0
#define CAMWORKMODE_SENSING		1
#define CAMWORKMODE_ATTCAM		2



typedef struct _scamif
{
	HAND	hmutex;		// Mutex for SCAMIF
	HAND	hth;		// thread handler
	U32		state;		
	PARAM_T		userparam;
	U32		right0left1;			// I am right-side sensor? left-sided sensor?
	U32		streaming_channel_mode;
									//	0: use single channel. Count of camera device == camcount.  streaming count: 1
									//	1: use multi channel.  Count of camera device == 1.         streaming count: camcount
	//---
	HAND	hcbfunc;
	U32 	camcount;
	I32		workmode;				// 0: NULL, 1: SENSING, 2: Attack Camera
	I32		mastercamid;			// master cam.. (<0 : NO master.. WHAT?)
	I32		slavecamid;				// slave cam..  (<0 : NO slave)
	U32		myip;
	U32		mysubnetmask;

	HAND	hgev;
	scamcam_t	scc[MAXCAMCOUNT];

	//---
	HAND	hsec;						// Security  module for Z3.		20190512
	U32		runcode;
	U08		swspec[64+4];					// [64+4];
	U08		barcode[64];


	//U32		act_vrinitfmc;			// DO vr_init_fm with zrot
	//U32		zrot;					// 0: Normal ROT, 1: Zero ROT
	//U32		autoinit;				// Auto init in scam-side


	U32		act_vrinitfmc[MAXCAMCOUNT];			// DO vr_init_fm with zrot
	U32		zrot[MAXCAMCOUNT];					// 0: Normal ROT, 1: Zero ROT
	U32		autoinit[MAXCAMCOUNT];				// Auto init in scam-side

	U32		allmastermode;

	U32		lockcode;

	U32		virtualslavetimermode;					// 0: NOT virtual slave timer mode, 1: Virtual slave timer mode.   20200319

	/* PCIe params */	
	HAND	hPCIe;	
} scamif_t;

//typedef struct _bufinfo {
//	int fake;
//} bufinfo_t;

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/
typedef int (CALLBACK SCAMIF_CALLBACKFUNC)(HAND h, U32 camid, U32 index);

// Callback function.
typedef I32(*SCAMIF_FNCB)(void *pData, I32 nSize, 
		U32 ts_high, U32 ts_low, 
		I32 size_x, I32 size_y, 
		I32 offset_x, I32 offset_y,
		U32 normalbulk, U32 skip, U32 multitude,		
		U32 camid,						// !!! :)
		PARAM_T userparam);
//---
HAND scamif_create2(HAND hcbfunc, U32 workmode, U32 right0left1, PARAM_T userparam);
HAND scamif_create3(HAND hcbfunc, U32 workmode, U32 right0left1, U32 streaming_channel_mode, PARAM_T userparam);
I32 scamif_lock(HAND hscamif, U32 lockcode);
I32 scamif_delete(HAND hscamif);

I32 scamif_start(HAND hscamif);		// Start 
I32 scamif_start_allmaster(HAND h);	// Start with all-master mode.. :P
I32 scamif_start_SENSINGCAM(HAND hscamif);		// Start 
I32 scamif_pause(HAND hscamif);		// Pause 
I32 scamif_resume(HAND hscamif);	// resume 
I32 scamif_stop(HAND hscamif);		// Stop 
I32 scamif_update(HAND hscamif);	// Update setting


I32 scamif_camtimer_reset(HAND h);
I32 scamif_camtimer_reset2(HAND h);
I32 scamif_camtimer_reset3(HAND h);
I32 scamif_camtimer_stop(HAND h);

I32 scamif_camtimer_virtualslavetimer_set(HAND h, U32 benable);

I32 scamif_fmc_init(HAND h);
I32 scamif_check_host_cam_subnet(HAND h);


//---
I32 scamif_cam_start(HAND hscamif,
		U32 camcount,
		U08 *ip[MAXCAMCOUNT],			// camera IP
		U32 port[MAXCAMCOUNT],			// Port for Receiving Image
		U32 masterslave[MAXCAMCOUNT]	// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE
		);				// Camera Count, IP addr, My Image Port, master/slave.


I32 scamif_cam_start2(HAND h,
		U32 camcount,
		U08 *ip[MAXCAMCOUNT],			// camera IP
		U32 port[MAXCAMCOUNT],			// Port for Receiving Image
		U32 masterslave[MAXCAMCOUNT]	// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE
		, U32 streaming_channel_mode	// select streaming channel mode. 0: single mode,   1: multi mode
		);		


I32 scamif_cam_start_dummy(HAND h,
		U32 camcount,
		U32 masterslave[MAXCAMCOUNT]	// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE
		);				// Camera Count, master/slave.

I32 scamif_cam_stop(HAND hscamif);	// Stop camera operation
I32 scamif_cam_heartbeat(HAND h, U32 camid, U32 heartbeat);		// heart beat(msec.)  0: not any more.


I32 scamif_cam_myip(HAND hscamif, U08 *ip);		// My (PC) IP

I32 scamif_connection_set(HAND h, U32 camid, U32 connection);

I32 scamif_cam_ipport(HAND hscamif, 
		U32 camid,						
		U08 *ip,								// camera IP
		U32 port								// Image Port for PC
		);								
I32 scamif_cam_update(HAND hscamif, U32 camid);	// Update Camera setting
//I32 scamif_cam_start(HAND hscamif, U32 camid);	// Start camera operation
I32 scamif_cam_pause(HAND hscamif, U32 camid);	// Pause camera operation
I32 scamif_cam_resume(HAND hscamif, U32 camid);	// resume camera operation (BULK continue..)

I32 scamif_cam_connected(HAND h, U32 camid);	// Connected? or not..

//I32 scamif_cam_fmcinit_config(HAND hscamif, U32 fmcinit, U32 zrot, U32 initauto);	
I32 scamif_cam_fmcinit_config(HAND hscamif, U32 camid, U32 fmcinit, U32 zrot, U32 initauto);   // fmcinit: Do FMC Init or Not.    zrot: 1: ZROT mode,  0: NROT mode.   initauto: 1: Auto init, 0: NOT.

I32 scamif_cam_BULK(HAND h, U32 camid,   	// BULK transfer..... with 
		U32 ts_h, U32 ts_l, 
		U32 framecount, 
		U32 waittime, 
		U32 continueafterbulk,
		
		U32 multitude4Bulk,
		U32 skip4bulk,

		U32 tsshot_h, U32 tsshot_l, I32 axx1024, I32 bxx1024, I32 ayx1024, I32 byx1024);


I32 scamif_cam_BULK64(HAND h, U32 camid,   	// BULK transfer..... with 
		U64 ts64,
		U32 framecount, 
		U32 waittime, 
		U32 continueafterbulk,
		
		U32 multitude4Bulk,
		U32 skip4bulk,

		U64 ts64shot, I32 axx1024, I32 bxx1024, I32 ayx1024, I32 byx1024);


//---
//I32 scamif_image_property(HAND hscamif, U32 camid,
//		U32 width, U32 height,
//		U32 offset_x, U32 offset_y,
//		U32 framerate,
//		U32 gain,
//		U32 exposure,
//		U32 skip,
//		U32 multitude,
//		U32 syncdiv
//		);	
I32 scamif_image_property2(HAND hscamif, U32 camid,
		U32 width, U32 height,
		U32 offset_x, U32 offset_y,
		U32 framerate,
		U32 gain,
		U32 exposure,
		U32 skip,
		U32 multitude,
		U32 syncdiv,
		U32 exposure_post,
		U32 exposure_expiate
		);	


I32 scamif_image_property_BULK(HAND h, U32 camid,
		U32 width, U32 height,
		U32 offset_x, U32 offset_y);

I32 scamif_image_size(HAND hscamif, U32 camid, U32 width, U32 height);					// Size (width, height)
I32 scamif_image_offset(HAND hscamif, U32 camid, U32 offset_x, U32 offset_y);	// Offset (offset_x, offset_y)

I32 scamif_image_fps(HAND hscamif, U32 camid, U32 fps);
I32 scamif_image_exposure(HAND hscamif, U32 camid, U32 exposureUsec);
I32 scamif_image_skip(HAND hscamif, U32 camid, U32 skip);
I32 scamif_image_multitude(HAND hscamif, U32 camid, U32 multitude);
I32 scamif_image_syncdiv(HAND hscamif, U32 camid, U32 syncdiv);

I32 scamif_image_update(HAND hscamif, U32 camid, U32 forceinitbuffer );	// Update Image property, with/without buffer init
//I32 scamif_image_update2(HAND h, U32 camid, U32 forcebufferinit, U32 zrot, U32 autoinit);
I32 scamif_image_update2(HAND h, U32 camid, U32 forcebufferinit);
I32 scamif_image_update_all(HAND hscamif, U32 forceinitbuffer );


I32 scamif_image_gamma(HAND h, U32 camid, U32 gamma);
I32 scamif_image_contrast(HAND h, U32 camid, U32 contrast);
I32 scamif_image_bright(HAND h, U32 camid, U32 bright);
I32 scamif_image_LUT(HAND h, U32 camid, U32 bright, U32 contrast, U32 gamma);
I32 scamif_image_gain(HAND h, U32 camid, U32 gain);

I32 scamif_image_delayoffset(HAND h, U32 camid, U32 delayoffset);
I32 scamif_image_expiate(HAND h, U32 camid, U32 expiate);
I32 scamif_led_brightness(HAND h, U32 camid, U32 brightness);

I32 scamif_image_sync_update(HAND h, U32 camid, U32 updateme);

I32 scamif_image_property_LEDbright(HAND h, U32 camid, U32 ledbright);
I32 scamif_image_property_LUT(HAND h, U32 camid,
		U32 bright, U32 contrast, U32 gamma);
I32 scamif_image_property_gain(HAND h, U32 camid, U32 gain);

I32 scamif_image_update_LEDbright(HAND h, U32 camid);
I32 scamif_image_update_LUT(HAND h, U32 camid);
I32 scamif_image_update_gain(HAND h, U32 camid);
I32 scamif_image_update_gain_all(HAND h);
/*
I32 scamif_maskset(HAND h, U32 camid, U32 startx, U32 starty, U32 endx, U32 endy);
I32 scamif_masksave(HAND h, U32 camid);
I32 scamif_maskcalc(HAND h, U32 camid, U32 calc);
*/


//---
//I32 scamif_imagebuf_create(HAND hscamif, U32 camid );	// Buffer Create
//I32 scamif_imagebuf_delete(HAND hscamif, U32 camid );	// Buffer Delete
//I32 scamif_imagebuf_init(HAND hscamif, U32 camid );	// Buffer init.
//I32 scamif_imagebuf_latest(HAND hscamif, U32 camid, U32 *pindex);	// Get latest frame index.
//I32 scamif_imagebuf_groupinfo(HAND hscamif, U32 camid, camif_buffer_info_t *pb);	// Get image buffer information 
//I32 scamif_imagebuf_info(HAND hscamif, U32 camid, U32 index, camif_buffer_info_t *pb);	// Get image buffer information 
//I32 scamif_imagebuf_getbuf(HAND hscamif, U32 camid, U32 index, camif_buffer_info_t *pb, U08 **pbuf);	// Get image buffer and info


I32 scamif_hboardinfo_read(HAND h, U32 camid, U32 *pbuf, U32 len);
I32 scamif_hboardinfo_write(HAND h, U32 camid, U32 *pbuf, U32 len);
I32 scamif_pcinfo_read(HAND h, U32 camid, U32 *pbuf, U32 len);
I32 scamif_pcinfo_write(HAND h, U32 camid, U32 *pbuf, U32 len);
I32 scamif_runcode_read(HAND h, U32 camid, U32 *pbuf, U32 len);
I32 scamif_runcode_write(HAND h, U32 camid, U32 *pbuf, U32 len);
I32 scamif_cpusid_read(HAND h, U32 camid, U32 *pbuf, U32 len);
I32 scamif_cpusid_write(HAND h, U32 camid, U32 *pbuf, U32 len);
I32 scamif_cpubdserial_read(HAND h, U32 camid, U32 *pbuf, U32 len);
I32 scamif_bddatarsvd_read(HAND h, U32 camid, U32 *pbuf, U32 len);
I32 scamif_bddatarsvd_write(HAND h, U32 camid, U32 *pbuf, U32 len);
I32 scamif_security_update(HAND h);
I32 scamif_security_swspec(HAND h, U08 swspec[64+4]);
I32 scamif_security_barcode(HAND h, U08 barcode[64]);
I32 scamif_security_runcode(HAND h, U32 *pruncode);


I32 scamif_z3_version_get(HAND h, U32 camid, char *pversion /*U08 pversion[64]*/);
I32 scamif_z3_cache_update(HAND h, U32 camid, U32 addr, U32 len);
I32 scamif_z3_cache_commit(HAND h, U32 camid, U32 addr, U32 len);
I32 scamif_z3_flash_erase(HAND h, U32 camid, U32 addr, U32 len);
I32 scamif_z3_reboot(HAND h, U32 camid);
I32 scamif_z3_recover_gold(HAND h, U32 camid);

I32 scamif_z3_mem_write(HAND h, U32 camid, U32 addr, U32 count, U32 *data, U32 *status);
I32 scamif_z3_mem_read (HAND h, U32 camid, U32 addr, U32 count, U32 *data, U32 *status);
I32 scamif_z3_mem_checksum(HAND h, U32 camid, U32 addr, U32 count, U32 checksumtype, U32 *pchecksum, U32 *pstatus);

U32 scamif_imagebuf_zrot_barnoise_position(
		HAND hscamif, U32 camid,
		U32 normalbulk, U32 index, 
		I32 *pstartx, I32 *pendx );


#if defined(PCIE_CHANNEL)
I32 HAL_SCAMIF_SetRunState(HAND h, U32 state);
I32 HAL_SCAMIF_UpdateRoi(HAND h, U32 camid, U32 x, U32 y, U32 width, U32 heigh ); // [jmyl] test 함수에서만 호출되고 실제로 사용되지 않는 듯.
#endif



#if defined (__cplusplus)
}
#endif

#endif		// _SCRMIF_H_

