//#pragma once

#ifndef _IHEX_H_
#define	_IHEX_H_


//------------------------------------------------------------------------
#define		IHEXA_RECORD_TYPE_DATA							(0x00)
#define		IHEXA_RECORD_TYPE_EOF							(0x01)
#define		IHEXA_RECORD_TYPE_SEGMENT_ADDRESS				(0x02)
#define		IHEXA_RECORD_TYPE_START_SEGMENT_ADDRESS			(0x03)
#define		IHEXA_RECORD_TYPE_LINEAR_ADDRESS				(0x04)
#define		IHEXA_RECORD_TYPE_START_LINEAR_ADDRESS			(0x05)

#define		PIHEX_BIN_IMAGE_ECODE_SUCCESS					(0)
#define		PIHEX_BIN_IMAGE_ECODE_FILE_NOT_FOUND			(0x1)
#define		PIHEX_BIN_IMAGE_ECODE_TOO_MANY_PARTITIONS		(0x2)
#define		PIHEX_BIN_IMAGE_ECODE_ADDRESS_OUTOGRANGE		(0x3)
#define		PIHEX_BIN_IMAGE_ECODE_INVALID_RECORD			(0x80000000)






#define		ZCAM_FLASH_SEGMENT_SIZE		(64 * 1024)
#define		ZCAM_FLASH_MEMORY_SIZE		(16 * 1024 * 1024)
#define		ZCAM_FLASH_SEGMENT_COUNT	(ZCAM_FLASH_MEMORY_SIZE / ZCAM_FLASH_SEGMENT_SIZE)

#define BASEIMAGE_SILVER_OFFSET       0
#define BASEIMAGE_GOLD_OFFSET       (0x400000)
//------------------------------------------------------------------------

#if defined (__cplusplus)
extern "C" {
#endif


typedef				struct _IHEXA_RECORD_
{
	char			szTextData[1024];
	unsigned char	aBinData[512];
	unsigned char	aRecordData[512];
	unsigned int	ulRecordType;
	unsigned int	ulRecordLength;
	unsigned int	ulAddress;
}	IHEXA_RECORD, *PIHEXA_RECORD;



typedef					struct _IHEX_RECORD_
{
	unsigned char		aData[256];
	unsigned int		ulType;
	unsigned int		ulLength;
	unsigned int		ulAddress;
}	IHEX_RECORD,		*PIHEX_RECORD;


typedef					struct _IHEX_PARTITION_
{
	unsigned int		ulAddress;
	unsigned int		ulLength;
}	IHEX_PARTITION,		*PIHEX_PARTITION;


typedef					struct _IHEX_BIN_IMAGE_
{
	PIHEX_PARTITION		pPartitions;
	unsigned char*		pImgData;
	unsigned int		ulMaxPartCnt;
	unsigned int		ulMaxImageSize;
	unsigned int		ulPartCnt;
	unsigned int		ulErrorCode;
}	IHEX_BIN_IMAGE,		*PIHEX_BIN_IMAGE;

//-------------------------------------------------------------------------------
int IHEXA_Hex2Bin(
	unsigned char*	pBin,
	unsigned char*	pStr
);

int IHEXA_String2Bin(
	PIHEXA_RECORD	pRecord
);

int IHEX_StrToHexaBin(
	unsigned char*	pBin,
	unsigned char*	pStr,
	unsigned int	nStrLen
);

int IHEX_ParseData (
	PIHEX_RECORD	pRecord,
	unsigned char	*pStr
);

PIHEX_BIN_IMAGE IHEX_CreateImage(
	char*			pFileName,
	unsigned int	MaxPartitionCount,
	unsigned int	MaxImageSize
);


void IHEX_DeleteImage(
	PIHEX_BIN_IMAGE	pImg
);

#if defined (__cplusplus)
}
#endif


#endif // !_IHEX_H_

