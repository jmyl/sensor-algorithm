/*!
 *******************************************************************************
                                                                                
                    Creatz SCAM Buffer
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 ~ 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   scrmif_buffer.h
	 @brief  
	 @author YongHo Suk
	 @date   2015/12/13 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_SCRMIF_BUFFER_H_)
#define		 _SCRMIF_BUFFER_H_

#include "cr_common.h"

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#define SCAMBUFFERSTATE_EMPTY				0
#define SCAMBUFFERSTATE_FULL				1
#define SCAMBUFFERSTATE_BUSY				2

#if 0
#define SCAMIF_BUFFER_COUNT1			16
#define SCAMIF_BUFFER_COUNT2			(SCAMIF_BUFFER_COUNT1 * 2)
#define SCAMIF_BUFFER_COUNT4			(SCAMIF_BUFFER_COUNT1 * 4)
#define SCAMIF_BUFFER_COUNT8			(SCAMIF_BUFFER_COUNT1 * 8)
#define SCAMIF_BUFFER_COUNT16			(SCAMIF_BUFFER_COUNT1 * 16)

#define SCAMIF_BUFFER_SIZE1				(1280 * 1024)
#define SCAMIF_BUFFER_SIZE2				(SCAMIF_BUFFER_SIZE1/2)
#define SCAMIF_BUFFER_SIZE4				(SCAMIF_BUFFER_SIZE1/4)
#define SCAMIF_BUFFER_SIZE8				(SCAMIF_BUFFER_SIZE1/8)
#define SCAMIF_BUFFER_SIZE16			(SCAMIF_BUFFER_SIZE1/16)
#endif


//---
/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

	//I32 scamif_imagebuf_create(HAND hscamif, U32 camid );	// Buffer Create
//I32 scamif_imagebuf_delete(HAND hscamif, U32 camid );	// Buffer Delete
I32 scamif_imagebuf_init(HAND hscamif, U32 camid, U32 normalbulk, camimageinfo_t *pci0);	// Buffer init.
I32 scamif_imagebuf_groupinfo(HAND hscamif, U32 camid, U32 normalbulk, camif_buffer_info_t **ppb);	// Get image buffer information 
I32 scamif_imagebuf_groupinfo2(HAND hscamif, U32 camid, U32 normalbulk, camif_buffer_info_t **ppb, camif_buffergroup_t **ppbg);	// Get image buffer information 

I32 scamif_imagebuf_trimindex(HAND hscamif, U32 camid, U32 normalbulk, U32 index, U32 *pindex);	// Trim index and return valid index. 0: Fail, 1: Good.
I32 scamif_imagebuf_trimindex2(HAND hscamif, U32 camid, U32 normalbulk, I32 signedindex, U32 *pindex);	// Trim index and return valid index. 0: Fail, 1: Good.
I32 scamif_imagebuf_trimindexdelta(HAND hscamif, U32 camid, U32 normalbulk, U32 index, I32 indexdelta, U32 *pindex);

I32 scamif_imagebuf_getwriteindex(HAND hscamif, U32 camid, U32 normalbulk, U32 *pindex);	// Get next write index.  0: Fail, 1: Good.
I32 scamif_imagebuf_getreadindex(HAND hscamif, U32 camid, U32 normalbulk, U32 *pindex);		// Get next read index.  0: Fail, 1: Good.

I32 scamif_imagebuf_write(
		HAND hscamif, U32 camid, U32 normalbulk, 
		camif_buffer_info_t  *ppb, U08 *img, U32 *pwindex);	// Write image to buffer and update write-index, if success.

I32 scamif_imagebuf_write_raw(HAND hscamif, U32 camid, U32 normalbulk, camif_buffer_info_t  *pb, U08 *img, U32 index);

I32 scamif_imagebuf_read(
		HAND hscamif, U32 camid, U32 normalbulk, 
		camif_buffer_info_t **ppb, U08 **pbuf, U32 *prindex);	// Get image buffer and info. and update read-index, if success.

I32 scamif_imagebuf_read_latest(
		HAND hscamif, U32 camid, U32 normalbulk, 
		camif_buffer_info_t **ppb, U08 **pbuf, U32 *prindex);	// Get Latest image buffer and info. and update read-index, if success.

I32 scamif_imagebuf_peek(
		HAND hscamif, U32 camid, U32 normalbulk, 
		camif_buffer_info_t **ppb, U08 **pbuf, U32 *prindex);	// Get image buffer and info. Preserve read-index

I32 scamif_imagebuf_peek_latest(
		HAND hscamif, U32 camid, U32 normalbulk, 
		camif_buffer_info_t **ppb, U08 **pbuf, U32 *prindex);	// Get Latest image buffer and info. Preserve read-index

I32 scamif_imagebuf_read2(
		HAND hscamif, U32 camid, U32 normalbulk, U32 updateindex,
		camif_buffer_info_t **ppb, U08 **ppbuf, U32 *prindex);	// Get image buffer and info. and update read-index, if success.

I32 scamif_imagebuf_read3(
		HAND hscamif, U32 camid, U32 normalbulk, U32 updateindex,
		camif_buffer_info_t **ppb, U08 **ppbuf, U32 *prindex);	// Get Latest image buffer and info. and update read-index, if success.

I32 scamif_imagebuf_randomaccess(
		HAND hscamif, U32 camid, U32 normalbulk, U32 index, 
		camif_buffer_info_t **ppb, U08 **ppbuf);	

I32 scamif_imagebuf_randomaccess_lean(
		HAND hscamif, U32 camid, U32 normalbulk, U32 index, 
		camif_buffer_info_t **ppb, U08 **ppbuf);	




I32 scamif_imagebuf_getinfo(
		HAND hscamif, U32 camid, U32 normalbulk, U32 index, 
		camif_buffer_info_t **ppb);	// Get info

I32 scamif_imagebuf_buffercount(HAND hscamif, U32 camid, U32 normalbulk, U32 *pcount);

I32 scamif_imagebuf_timestamprange(
	HAND hscamif, U32 camid, U32 normalbulk, U32 index,
	U64 *pts64from, U64 *pts64to);

I32 scamif_imagebuf_timestamprangeEx(HAND hscamif, U32 camid, U32 normalbulk, U64 ts64,
		U32 *prindex, U64 *pts64fit, U64 *pts64from, U64 *pts64to);


I32 scamif_imagebuf_timetickupdate( HAND hscamif, U32 camid);
U32 scamif_imagebuf_timetick(HAND hscamif, U32 camid);


void scamif_datamem_create(HAND hscamif);
void scamif_datamem_release(HAND hscamif);


#if defined (__cplusplus)
}
#endif

#endif		// _SCRMIF_BUFFER_H_

