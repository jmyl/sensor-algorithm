/*!
 *******************************************************************************
                                                                                
                    CREATZ SCAM WORKING
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   scamif_work.cpp
	 @brief  SCAMFIF
	 @author Original: by yhsuk
	 @date   2015/12/01 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"
#include "cr_math.h"

#include "crgev.h"
#include "scamif.h"
#include "scamif_work.h"
#include "scamif_buffer.h"
#include "iana_security.h"
#include "iana_security_implement.h"

#if defined(PCIE_CHANNEL)
#include "pcie.h"
#endif

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define CAMCAPTURESTOP_TRY	3
#define HEARTBEAT_TRY	10

#define IMAGEUPDATE_TRY	10
#define GECP_TRY	4
#define HBOARDINFO_TRY	1
#define PCINFO_TRY	1
#define CPUSID_TRY	1
#define RUNCODE_TRY	1
#define Z3_CMD_TRY	2

#define		START_TRY 2
#define		STOP_TRY 	2

#define FMC_INIT_SLEEP	500

#define SECURITY_UPDATE	4


 //#define DELAYFORINITFMC  (10 * 1000)
//#define DELAYFORINITFMC  (4 * 1000)
//#define DELAYFORINITFMC  (4 * 100)
//#define DELAYFORINITFMC  0
#define DELAYFORINITFMC  (100)
//#define DELAYFORINITFMC  (200)
//#define DELAYFORINITFMC  (500)
//#define DELAYFORINITFMC  (500)



//#define USE_VIRTUAL_SLAVE
#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/
static int s_delayforinitfmc = DELAYFORINITFMC;

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/

I32 static video_fnCB(
		void *pData, I32 nSize, 
		U32 ts_high, U32 ts_low, 
		I32 size_x, I32 size_y, 
		I32 offset_x, I32 offset_y,
		U32 normalbulk, U32 skip, U32 multitude,		// 
		void *pParam);


/*----------------------------------------------------------------------------
 *	Description	: local functions
 -----------------------------------------------------------------------------*/
static I32 check_params(HAND h, U32 camid, CR_BOOL check_connected)
{
	scamif_t *pscamif;
	scamcam_t *psc;

	if (h == NULL) {
		return CR_FALSE;
	}

	pscamif = (scamif_t *) h;

	if (camid >= pscamif->camcount) {
		return CR_FALSE;
	}	

	if(check_connected == CR_TRUE)
	{
		psc = &pscamif->scc[camid];
		if (psc->connected) {
			return CR_TRUE;
		}
	}
	else {
		return CR_TRUE;
	}
		
	return CR_FALSE;
}



/*!
 ********************************************************************************
 *	@brief      scamif_cam start
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camcount
 *              camera count. (0/1/2)			// 0 means, use current configuration.
 *  @param[in]	ip
 *              IP address string of Camera
 *  @param[in]	port
 *              PC-side port for receiving Image from camera
 *  @param[in]	masterslave
 *              Master or slave.....
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *
 *	@author	    yhsuk
 *  @date       2012/11/19
 *******************************************************************************/

I32 scamif_cam_start(HAND h,
		U32 camcount,
		U08 *ip[MAXCAMCOUNT],			// camera IP
		U32 port[MAXCAMCOUNT],			// Port for Receiving Image
		U32 masterslave[MAXCAMCOUNT]	// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE
		)				// Camera Count, IP addr, My Image Port, master/slave.
{
	I32 res;


	res = scamif_cam_start2(h,
			camcount,
			ip,
			port,
			masterslave,
			0							// single mode.. :P
			);

	return res;
}


/*!
 ********************************************************************************
 *	@brief      scamif_cam start 2.
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camcount
 *              camera count. (0/1/2)			// 0 means, use current configuration.
 *  @param[in]	ip
 *              IP address string of Camera
 *  @param[in]	port
 *              PC-side port for receiving Image from camera
 *  @param[in]	masterslave
 *              Master or slave.....
 *  @param[in]	streaming_channel_mode
 *              0: use single channel. Count of camera device == camcount.  streaming count: 1
 *              1: use multi channel.  Count of camera device == 1.         streaming count: camcount
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *
 *	@author	    yhsuk
 *  @date       2019/02/14
 *******************************************************************************/
I32 scamif_cam_start2(HAND h,
		U32 camcount,
		U08 *ip[MAXCAMCOUNT],			// camera IP
		U32 port[MAXCAMCOUNT],			// Port for Receiving Image
		U32 masterslave[MAXCAMCOUNT]	// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE
		, U32 streaming_channel_mode	// select streaming channel mode.
		)			
{
	I32 res;
	U32 i;
	U32 usecurrentconf;

	//--
	scamif_t *pscamif;
	if (h == NULL) {
		res = 0;
		goto func_exit;
	}

	pscamif = (scamif_t *) h;
	if (camcount == 0) {
		if (pscamif->camcount == 0) {
			res = 0;
			goto func_exit;
		}
		camcount = pscamif->camcount;
		usecurrentconf = 1;
	} else {
		usecurrentconf = 0;
	}

	pscamif->streaming_channel_mode	= streaming_channel_mode;

	if (camcount > NUM_CAM || camcount == 0) {
		res = 0;
		goto func_exit;
	}

	pscamif->camcount = camcount;

	if (usecurrentconf == 0) {
		pscamif->mastercamid = -1;
		pscamif->slavecamid  = -1;
	}

#if GIGEV_CHANNEL

	for (i = 0; i < 2; i++) {
		pscamif->scc[i].hgecp = 0;
	}

	{
		HAND hsc[MAXCAMCOUNT];
		scamcam_t *psc;
		U32 runmode;

		if (pscamif->hgev != NULL) {
			GEV_delete(pscamif->hgev);
			pscamif->hgev = NULL;
		}
		for (i = 0; i < camcount; i++) {
			hsc[i] = (HAND)&pscamif->scc[i];
		}
		runmode = GEV_RUNMODE_NORMAL;
		pscamif->hgev = GEV_create2(hsc, camcount, runmode, streaming_channel_mode);

		for (i = 0; i < camcount; i++) {
			psc = &pscamif->scc[i];
			psc->hgecp = GEV_GECP_hand(pscamif->hgev, i);
		}
	}

	for (i = 0; i < camcount; i++) {
		scamcam_t *psc;

		//--
		psc = &pscamif->scc[i];

		psc->hsci = h;
		psc->camid = i;

		psc->state = SCAMCAMSTATE_INIT;

		if (usecurrentconf) {
			//--
		} else {
			psc->masterslave = masterslave[i];

			if (masterslave[i] == SCAMIF_MASTER) {
				pscamif->mastercamid = i;
			} else if (masterslave[i] == SCAMIF_SLAVE) {
				pscamif->slavecamid = i;
			}
			psc->camip 		= GECP_Ipstr2Ip(NULL, (char *)ip[i]);
			psc->gvcpport	= DEF_GVCP_CAMPORT;
			psc->gvspport 	= port[i];
		}
	}


	{
		U32 destIp;
		U32 bestInterface;
		U32 myIp, mysubnetmask;
		if (pscamif->mastercamid < 0) {
			destIp = pscamif->scc[0].camip;
		} else {
			destIp = pscamif->scc[pscamif->mastercamid].camip;
		}

		bestInterface = cr_GetBestInterface(destIp);
		if (cr_GetMacAddress (bestInterface, NULL, NULL, &myIp, &mysubnetmask)) {
			pscamif->myip =  myIp;
			pscamif->mysubnetmask =  mysubnetmask;
		} else {
			pscamif->myip = 0;
			pscamif->mysubnetmask =  0;
		}
	}
#else

	{
		HAND hsc[MAXCAMCOUNT];
		scamcam_t *psc;

		if (pscamif->hPCIe != NULL) {
			HAL_PCIE_Stop(pscamif->hPCIe);
			pscamif->hPCIe = NULL;
		}
		for (i = 0; i < camcount; i++) {
			hsc[i] = (HAND)&pscamif->scc[i];
		}

		pscamif->hPCIe = HAL_PCIE_Start(hsc, camcount, 0, streaming_channel_mode);

	}

	for (i = 0; i < camcount; i++) {
		scamcam_t *psc;

		//--
		psc = &pscamif->scc[i];

		psc->hsci = h;
		psc->camid = i;

		psc->state = SCAMCAMSTATE_INIT;

		if (usecurrentconf) {
			//--
		} else {
			psc->masterslave = masterslave[i];

			if (masterslave[i] == SCAMIF_MASTER) {
				pscamif->mastercamid = i;
			} else if (masterslave[i] == SCAMIF_SLAVE) {
				pscamif->slavecamid = i;
			}
		}
	}
#endif
	res = 1;

func_exit:
	return res;
}



I32 scamif_cam_start_dummy(HAND h,
		U32 camcount,
		U32 masterslave[MAXCAMCOUNT]	// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE
		)				// Camera Count, master/slave.
{
	I32 res;
	U32 i;

	//--
	scamif_t *pscamif;
	if (h == NULL) {
		res = 0;
		goto func_exit;
	}

	pscamif = (scamif_t *) h;

	if (camcount > NUM_CAM || camcount == 0) {
		res = 0;
		goto func_exit;
	}

	pscamif->camcount = camcount;

#if GIGEV_CHANNEL
	{
		if (pscamif->hgev != NULL) {
			GEV_delete(pscamif->hgev);
			pscamif->hgev = NULL;
		}
	}


	for (i = 0; i < camcount; i++) {
		scamcam_t *psc;

		//--
		psc = &pscamif->scc[i];

		psc->hgecp = NULL;
		psc->hsci = h;
		psc->camid = i;

		psc->state = SCAMCAMSTATE_INIT;


		psc->masterslave = masterslave[i];

		if (masterslave[i] == SCAMIF_MASTER) {
			pscamif->mastercamid = i;
		} else if (masterslave[i] == SCAMIF_SLAVE) {
			pscamif->slavecamid = i;
		}

		psc->camip 		= 0;		// fake
		psc->gvcpport	= 0;		// fake
		psc->gvspport 	= 0;		// fake
	}
#else
	if (pscamif->hPCIe != NULL) {
		HAL_PCIE_Stop(pscamif->hPCIe);
		pscamif->hPCIe = NULL;
	}

	for (i = 0; i < camcount; i++) {
		scamcam_t *psc;

		//--
		psc = &pscamif->scc[i];

		psc->hsci = h;
		psc->camid = i;

		psc->state = SCAMCAMSTATE_INIT;


		psc->masterslave = masterslave[i];

		if (masterslave[i] == SCAMIF_MASTER) {
			pscamif->mastercamid = i;
		} else if (masterslave[i] == SCAMIF_SLAVE) {
			pscamif->slavecamid = i;
		}
	}

#endif

	pscamif->myip =  0;
	res = 1;

func_exit:
	return res;
}







/*!
 ********************************************************************************
 *	@brief      scamif_cam stop
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *
 *	@author	    yhsuk
 *  @date       2016/02/19
 *******************************************************************************/
I32 scamif_cam_stop(HAND h)
{
	I32 res;
	U32 i;
	scamif_t *pscamif;
	U32 camcount;

	if (h == NULL) {
		res = 0;
		goto func_exit;
	}

	pscamif = (scamif_t *) h;
	camcount = pscamif->camcount;

	for (i = 0; i < camcount; i++) {
		scamcam_t *psc;
		psc = &pscamif->scc[i];
		if (psc->connected) {
#if GIGEV_CHANNEL			
			if (psc->hgecp != NULL) {
				int j;

				for (j = 0; j < CAMCAPTURESTOP_TRY; j++)		
				{
					res = GECP_CamCaptureStop(psc->hgecp);
					if (res == 0) {
						continue;
					}
					res = GECP_startSendStart(psc->hgecp, 0);
					if (res) {
						break;
					}
				}
			}
#else
		res = HAL_PCIE_StopCam(pscamif->hPCIe, i);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s: %d: HAL_PCIE_StopCam Error\n", __func__,  __LINE__);
			res = 0;
			goto func_exit;
		}
#endif
		}
	}

#if GIGEV_CHANNEL
	if (pscamif->hgev) {
		GEV_delete(pscamif->hgev);	
	}
	pscamif->hgev = NULL;
#else
	if (pscamif->hPCIe != NULL) {
		HAL_PCIE_Stop(pscamif->hPCIe);
		pscamif->hPCIe = NULL;
	}
#endif	
	res = 1;
func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      Camera Beat beat. 
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	heartbeat 
 *				heartbeat msec. 0 == Do not check heartbeat anymore.
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/03/05
 *******************************************************************************/
I32 scamif_cam_heartbeat(HAND h, U32 camid, U32 heartbeat)	
{
	I32 res;
	U32 i;
	HAND hDevice;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

#if GIGEV_CHANNEL
	hDevice = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {		
		for (i = 0; i < HEARTBEAT_TRY; i++) {
			res = GECP_heartbeat(hDevice,	heartbeat);
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
	}
#else
	hDevice = ((scamif_t *)h)->hPCIe;	

	res = HAL_PCIE_SetHeartBeatTimeout(hDevice,	camid, heartbeat);
	if(res != CR_OK) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s: %d: HAL_PCIE_SetHeartBeatTimeout Error\n", __func__,  __LINE__);
		res = 0;
		goto func_exit;
	}
	res = 1;
#endif

func_exit:
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}


#if GIGEV_CHANNEL
/*!
 ********************************************************************************
 *	@brief      Set My Ip
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	ip
 *              My Ip string.
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/
I32 scamif_myip(HAND h, U08 *ip)
{
	scamif_t *pscamif;

	if (h == NULL) {
		return 0;
	}

	pscamif = (scamif_t *) h;
	pscamif->myip =  GECP_Ipstr2Ip(NULL, (char *)ip);
	return 1;
}
#endif

/*!
 ********************************************************************************
 *	@brief      Set GECP connetion
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	connection
 *              
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *
 *	@author	    yhsuk
 *  @date       2019/0501
 *******************************************************************************/

I32 scamif_connection_set(HAND h, U32 camid, U32 connection)
{
	I32 res;
	scamif_t *pscamif;
	scamcam_t *psccs;

	//----
	if (h == NULL) {
		res = 0;
		goto func_exit;
	}

	pscamif = (scamif_t *) h;

	psccs = &pscamif->scc[camid];
	psccs->connected = connection;

	res = 1;
func_exit:
	return res;

}

/*!
 ********************************************************************************
 *	@brief      Start.
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/

I32 scamif_start(HAND h)
{
	I32 res;
	scamif_t *pscamif;
	U32 workmode;


	//------
	pscamif = (scamif_t *) h;
	workmode = pscamif->workmode;
	pscamif->allmastermode = 0;
	if (workmode == CAMWORKMODE_SENSING) {
		res = scamif_start_SENSINGCAM(h);
	} else {
		res = 0;
	}
	return res;
}

I32 scamif_start_allmaster(HAND h)
{
	I32 res;
	scamif_t *pscamif;
	U32 workmode;


	//------
	pscamif = (scamif_t *) h;
	workmode = pscamif->workmode;
	pscamif->allmastermode = 1;
	if (workmode == CAMWORKMODE_SENSING) {
		res = scamif_start_SENSINGCAM(h);
	} else {
		res = 0;
	}
	return res;
}

#if GIGEV_CHANNEL
I32 scamif_start_SENSINGCAM(HAND h)
{
	I32 res;
	HAND hgecpm;
	scamcam_t *psccm;
	I32 mastercamid;
	scamcam_t *psccs;
	scamif_t *pscamif;
	HAND hgecps;
	I32 slavecamid;
	U32 camid;

	U32 act_vrinitfmc[MAXCAMCOUNT];
	U32 zrot[MAXCAMCOUNT];
	U32 autoinit[MAXCAMCOUNT];

	U32 mastergood;
	U32 slavegood;

	U32 i;
	U32 streaming_channel_mode;

	//--
//__debugbreak();
	mastergood = 0;
	slavegood = 0;
	if (h == NULL) {
		res = 0;
		goto func_exit;
	}

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": START\n\n");
	pscamif = (scamif_t *) h;
	mastercamid = pscamif->mastercamid;
	slavecamid  = pscamif->slavecamid;


	streaming_channel_mode = pscamif->streaming_channel_mode;

	if (/*mastercamid < 0 || */
		mastercamid >= (I32) pscamif->camcount || pscamif->camcount <= 0) {
		res = 0;
		goto func_exit;
	}

	for (camid = 0; camid < NUM_CAM; camid++) {
		act_vrinitfmc[camid] = pscamif->act_vrinitfmc[camid];
		zrot[camid] = pscamif->zrot[camid];
		autoinit[camid] = pscamif->autoinit[camid];
	}
	//--


	if (mastercamid >= 0) {
		psccm = &pscamif->scc[mastercamid];
		hgecpm = psccm->hgecp;
		
		psccm->connected = 0;
		mastergood = 1;
	} else {
		psccm = NULL;
		hgecpm = NULL;
		mastergood = 0;
	}
	if (slavecamid >= 0 || slavecamid < (I32) pscamif->camcount) {
		psccs = &pscamif->scc[slavecamid];
		psccs->connected = 0;
		hgecps = psccs->hgecp;
		slavegood = 1;
	} else {
		hgecps = NULL;
		psccs = NULL;
		slavegood = 0;
	}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mid: %d, sid: %d, camcount: %d\n", mastercamid, slavecamid, pscamif->camcount);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "hgecpm: %08x,  hgecps: %08x\n", hgecpm, hgecps);


	//---
	//pscamif->myip =  cr_GetBestInterface(destIp);

	//---------------------------------------------------------
	// For MasterCam,  Set Host IP, MasterCam IP and Timer Stop

	res = 0;
	i = 0;

	while(mastergood) {
		res = GECP_HostIp(hgecpm, pscamif->myip);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}
		//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= GECP_CamIp\n");
		res = GECP_CamIp(hgecpm, psccm->camip);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}
		//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= GECP_CamGvcpPort\n");
		res = GECP_CamGvcpPort(hgecpm, psccm->gvcpport);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}
		//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= GECP_CamGvcpStart\n");
		res = GECP_CamGvcpStart(hgecpm);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}
		//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= GECP_CamGvspPort\n");
		res = GECP_CamGvspPort(hgecpm, psccm->gvspport);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}
		//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= GECP_CamTimerMode\n");
		for (i = 0; i < START_TRY; i++) {
			res = GECP_CamCaptureStop(hgecpm);
			if (res > 0) {
				res = GECP_CamGvspStop(hgecpm);
			}
			if (res > 0) {
				if (streaming_channel_mode == 0) {
					res = GECP_CamTimerMode(hgecpm, GVCP_VR_TIMER_CMD_SET_MASTER_STOP);				// Master Timer Stop
				}
				break;
			} 
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"#4   %d:%d res: %d\n", __LINE__,i, res);
			cr_sleep(20);
		}
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d:%d res: %d\n", __LINE__,i, res); mastergood = 0; break;}
		break;
	}

	//---------------------------------------------------------
	// For SlaveCam,  Set Host IP, SlaveCam IP and Timer Stop


	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d:%d res: %d mastargood: %d, slavegood: %d\n", __LINE__,i, res, mastergood, slavegood);

	while(slavegood) {
		res = GECP_HostIp(hgecps, pscamif->myip);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); slavegood = 0; break;}
		res = GECP_CamIp(hgecps, psccs->camip);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); slavegood = 0; break;}
		res = GECP_CamGvcpPort(hgecps, psccs->gvcpport);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); slavegood = 0; break;}
		res = GECP_CamGvcpStart(hgecps);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); slavegood = 0; break;}
		//<--- REBOOT
		res = GECP_CamGvspPort(hgecps, psccs->gvspport);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); slavegood = 0; break;}


		for (i = 0; i < START_TRY; i++) {
			res = GECP_CamCaptureStop(hgecps);
			if (res > 0) {
				res = GECP_CamGvspStop(hgecps);
			}

			if (res > 0) {
				break;
			}
			cr_sleep(20);
		}
		
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d SLAVE... res: %d\n", __LINE__, res); slavegood = 0; break;}
		break;
	}

	while(slavegood) {
		//<--- REBOOT

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "----- GECP_CamTimerMode\n");
		//res = GECP_CamTimerMode(hgecps, GVCP_VR_TIMER_CMD_SET_SLAVE_RUN);					// Slave Timer ready to Run..

		if (streaming_channel_mode == 0) {
			if (pscamif->virtualslavetimermode) {
				res = GECP_CamTimerMode(hgecps, GVCP_VR_TIMER_CMD_SET_VIRTUAL_SLAVE_RUN_1);			// Virtual Slave.. ready to Run.
			} else {
				//res = GECP_CamTimerMode(hgecps, GVCP_VR_TIMER_CMD_SET_MASTER_RUN);					// ONLY MASTER!
				res = GECP_CamTimerMode(hgecps, GVCP_VR_TIMER_CMD_SET_SLAVE_RUN);					// Slave Timer ready to Run..
			}


			if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); slavegood = 0; break;}
			//GECP_CamTimerMode(hgecps, GVCP_VR_TIMER_CMD_SET_MASTER_RUN);					

			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "----- GECP_CamExposureMode\n");
		}
//#define FAKEMASTER0
#if defined(FAKEMASTER0)
		res = GECP_CamExposureMode(hgecps, GVCP_VR_EXPOSURE_MODE_MASTER);
#else
		if (pscamif->allmastermode) {
			res = GECP_CamExposureMode(hgecps, GVCP_VR_EXPOSURE_MODE_MASTER);
		} else {
			res = GECP_CamExposureMode(hgecps, GVCP_VR_EXPOSURE_MODE_SLAVE);
		}
#endif

		
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); slavegood = 0; break;}
		 //GECP_CamExposureMode(hgecps, GVCP_VR_TIMER_CMD_SET_MASTER_RUN);

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "----- GECP_CamGvspCB\n");
		res = GECP_CamGvspCB(hgecps, video_fnCB);										// Callback..
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); slavegood = 0; break;}
		//SetImage();
		
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-----GECP_CamCaptureStop\n");
		res = GECP_CamCaptureStop(hgecps);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); slavegood = 0; break;}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "----- GECP_CamGvspStop\n");
		res = GECP_CamGvspStop(hgecps);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); slavegood = 0; break;}

		//<--- REBOOT
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "----- scamif_image_update\n");

	{
		I32 connected = psccs->connected;
		psccs->connected = 1;
		res = scamif_image_update2(pscamif, pscamif->slavecamid, 1); // Update2 Image property, with/without buffer init
		psccs->connected = connected;
	}
	if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); slavegood = 0; break;}

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "----- GECP_CamGvspStart\n");
		res = GECP_CamGvspStart(hgecps);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); slavegood = 0; break;}
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); slavegood = 0; break;}
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); slavegood = 0; break;}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-----= GECP_startCapture\n");
		res = GECP_startCapture(hgecps, 1);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-----= After GECP_startCapture\n");

		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); slavegood = 0; break;}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "----- GECP_startSendStart\n");
		res = GECP_startSendStart(hgecps, 1);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); slavegood = 0; break;}
		
		psccs->connected = 1;
		break;
	}

	//---------------------------------------------------------
	while(mastergood) {
		if (streaming_channel_mode == 0) {
			res = GECP_CamTimerMode(hgecpm, GVCP_VR_TIMER_CMD_SET_MASTER_STOP);				// Master Timer Stop
			if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}

			if (slavegood) {
				//<--- REBOOT
				//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "----- GECP_CamTimerMode\n");
				//res = GECP_CamTimerMode(hgecps, GVCP_VR_TIMER_CMD_SET_SLAVE_RUN);					// Slave Timer ready to Run..

			if (pscamif->virtualslavetimermode) {
				res = GECP_CamTimerMode(hgecps, GVCP_VR_TIMER_CMD_SET_VIRTUAL_SLAVE_RUN_1);					// Virtual Slave Timer ready to Run..
			} else {
				//res = GECP_CamTimerMode(hgecps, GVCP_VR_TIMER_CMD_SET_MASTER_RUN);					// ONLY MASTER!!
				res = GECP_CamTimerMode(hgecps, GVCP_VR_TIMER_CMD_SET_SLAVE_RUN);					// Slave Timer ready to Run..
			}
				if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}
			}
		}

		//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= GECP_CamExposureMode\n");
//#define FAKESLAVE0
#if defined(FAKESLAVE0)
		res = GECP_CamExposureMode(hgecpm, GVCP_VR_EXPOSURE_MODE_SLAVE);
#else
		res = GECP_CamExposureMode(hgecpm, GVCP_VR_EXPOSURE_MODE_MASTER);
#endif

		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= GECP_CamGvspCB\n");
		res = GECP_CamGvspCB(hgecpm, video_fnCB);										// Callback..
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}

		if (streaming_channel_mode == 0) {
			//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= GECP_CamTimerMode\n");
			res = GECP_CamTimerMode(hgecpm, GVCP_VR_TIMER_CMD_SET_MASTER_RUN);
			if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}
		}
		//	cr_sleep(100);
		//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= GECP_CamCaptureStop\n");
		res = GECP_CamCaptureStop(hgecpm);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}
		//	cr_sleep(100);
		//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= GECP_CamGvspStop\n");
		res = GECP_CamGvspStop(hgecpm);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}
		//	cr_sleep(100);
		//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= GECP_HostIp\n");
//// {1} FAIL.
//cr_sleep(1000);
//res = GECP_CamFmcInit(hgecpm, zrot[mastercamid], autoinit[mastercamid]);
//if (res > 0) {
//	cr_sleep(1000);
//}

		///// res = scamif_image_update(pscamif, pscamif->mastercamid, 1);	// Update Image property, with/without buffer init
//		res = scamif_image_update2(pscamif, pscamif->mastercamid, 1, zrot[mastercamid], autoinit[mastercamid]); // Update2 Image property, with/without buffer init

		
		{
			I32 connected = psccm->connected;
			psccm->connected = 1;
			res = scamif_image_update2(pscamif, pscamif->mastercamid, 1); // Update2 Image property, with/without buffer init
			psccm->connected = connected;
		}
		
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}

// {2} FAIL.
//cr_sleep(1000);
//res = GECP_CamFmcInit(hgecpm, zrot[mastercamid], autoinit[mastercamid]);
//if (res > 0) {
//	cr_sleep(1000);
//}
//res = GECP_CamFmcInit(hgecpm, zrot[mastercamid], autoinit[mastercamid]);




		//	cr_sleep(100);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= GECP_CamGvspStart\n");
		res = GECP_CamGvspStart(hgecpm);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}
		//	cr_sleep(100);
		//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= GECP_CamCaptureStart\n");
		//res = GECP_CamCaptureStart(hgecpm);
		//if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}
		//	cr_sleep(100);
		//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= GECP_ImageMultitude4Normal\n");
/////////////////		res = GECP_ImageMultitude4Normal(hgecpm, psccm->imginfo.multitude);
/////////////////		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}
		//	cr_sleep(100);

///res = scamif_image_update(pscamif, pscamif->mastercamid, 1);	// Update Image property, with/without buffer init
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= GECP_startCapture\n");
		res = GECP_startCapture(hgecpm, 1);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= After GECP_startCapture\n");
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}
		//cr_sleep(200);
//res = scamif_image_update(pscamif, pscamif->mastercamid, 1);	// Update Image property, with/without buffer init
		//	cr_sleep(100);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= GECP_startSendStart\n");
// {3}
//cr_sleep(1000);
//res = GECP_CamFmcInit(hgecpm, zrot[mastercamid], autoinit[mastercamid]);
//if (res > 0) {
//	cr_sleep(1000);
//}
		res = GECP_startSendStart(hgecpm, 1);
		if (res == 0 ||res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); mastergood = 0; break;}
		psccm->connected = 1;
		//	cr_sleep(100);
		//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= \n");
		break;
	}

	if (res < 0) {
		res = 0;
	}
#if 0					///////////////////
	if (res) {
		// act_vrinitfmc = 0;		// Prevent insane operation

		if (act_vrinitfmc[mastercamid]) {				// Master CAM, init FMC with zrot mode.				2016/11/02
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "init fmc, Master\n");
			if (mastergood) {
				//// res = scamif_image_update(pscamif, pscamif->mastercamid, 1);	// Update Image property, with/without buffer init

				res = GECP_CamFmcInit(hgecpm, zrot[mastercamid], autoinit[mastercamid]);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "#1 master zrot: %d, res: %d\n", zrot[mastercamid], res);
			}
		}
		if (act_vrinitfmc[slavecamid]) {				// Master CAM, init FMC with zrot mode.				2016/11/02
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "init fmc, Slave\n");
			if (slavegood) {
				//// res = scamif_image_update(pscamif, pscamif->slavecamid, 1);	// Update Image property, with/without buffer init
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "#1 slave zrot: %d, res: %d\n", zrot[slavecamid], res);
				res = GECP_CamFmcInit(hgecps, zrot[slavecamid], autoinit[slavecamid]);
			}
		}
	}
#endif
	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d    mastergood: %d, slavegood: %d\n\n", res, mastergood, slavegood);
	return res;
}
#else
I32 scamif_start_SENSINGCAM(HAND h)
{
	I32 res;
	HAND hPCIe;
	scamcam_t *psccm;
	I32 mastercamid;
	scamcam_t *psccs;
	scamif_t *pscamif;

	I32 slavecamid;
	U32 camid;

	U32 act_vrinitfmc[2];
	U32 zrot[2];
	U32 autoinit[2];

	U32 mastergood;
	U32 slavegood;

	U32 streaming_channel_mode;

	//--
//__debugbreak();
	mastergood = 0;
	slavegood = 0;
	if (h == NULL) {
		res = 0;
		goto func_exit;
	}

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%s: START\n", __func__);
	pscamif = (scamif_t *) h;
	mastercamid = pscamif->mastercamid;
	slavecamid  = pscamif->slavecamid;


	streaming_channel_mode = pscamif->streaming_channel_mode;

	if (	mastercamid >= (I32) pscamif->camcount || pscamif->camcount <= 0) {
		res = 0;
		goto func_exit;
	}

	for (camid = 0; camid < NUM_CAM; camid++) {
		act_vrinitfmc[camid] = pscamif->act_vrinitfmc[camid];
		zrot[camid] = pscamif->zrot[camid];
		autoinit[camid] = pscamif->autoinit[camid];
	}

	hPCIe = pscamif->hPCIe;

	if (mastercamid >= 0) {
		psccm = &pscamif->scc[mastercamid];
		psccm->connected = 0;
		mastergood = 1;
	} else {
		psccm = NULL;
		mastergood = 0;
	}
	if (slavecamid >= 0 || slavecamid < (I32) pscamif->camcount) {
		psccs = &pscamif->scc[slavecamid];
		psccs->connected = 0;
		slavegood = 1;
	} else {
		psccs = NULL;
		slavegood = 0;
	}
	
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mid: %d, sid: %d, camcount: %d\n", mastercamid, slavecamid, pscamif->camcount);

	//---------------------------------------------------------
	// For MasterCam

	res = CR_ERROR;

	if(mastergood) {

		res = HAL_PCIE_StopCam(hPCIe, mastercamid);	
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:%d HAL_PCIE_StopCam res: %d\n", __func__, __LINE__, res); 
			mastergood = 0; 
		}

	}

	//---------------------------------------------------------
	// For SlaveCam, 
	if(slavegood) {
		res = HAL_PCIE_StopCam(hPCIe, slavecamid);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:%d HAL_PCIE_StopCam res: %d\n", __func__, __LINE__, res); 
			slavegood = 0; 
		}



		res = HAL_PCIE_SetCallBackFunc(hPCIe, slavecamid, video_fnCB);										// Callback..
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:%d HAL_PCIE_SetCallBackFunc res: %d\n", __func__, __LINE__, res); 
			slavegood = 0; 
		}


	{
		I32 connected = psccs->connected;
		psccs->connected = 1;
		res = scamif_image_update2(pscamif, pscamif->slavecamid, 1); // Update2 Image property, with/without buffer init
		psccs->connected = connected;
	}


		res = HAL_PCIE_StartCam(hPCIe, slavecamid);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:%d HAL_PCIE_StartCam res: %d\n", __func__, __LINE__, res); 
			slavegood = 0; 
		}
		
		psccs->connected = 1;		
	}


	//---------------------------------------------------------
	if(mastergood) {

		res = HAL_PCIE_SetCallBackFunc(hPCIe, mastercamid, video_fnCB);									// Callback..
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:%d HAL_PCIE_SetCallBackFunc res: %d\n", __func__, __LINE__, res); 
			mastergood = 0; 
		}

		
		{
			I32 connected = psccm->connected;
			psccm->connected = 1;
			res = scamif_image_update2(pscamif, pscamif->mastercamid, 1); // Update2 Image property, with/without buffer init
			psccm->connected = connected;
		}
		

		res = HAL_PCIE_StartCam(hPCIe, mastercamid);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:%d HAL_PCIE_StartCam res: %d\n", __func__, __LINE__, res); 
			mastergood = 0; 
		}

		psccm->connected = 1;
	}

	if (res == CR_OK ) {
		res = 1;
	} else {
		res = 0;	
	}	

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d    mastergood: %d, slavegood: %d\n\n", res, mastergood, slavegood);
	return res;
}

#endif

/*!
 ********************************************************************************
 *	@brief      STOP
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/
I32 scamif_stop(HAND h)
{
	I32 res;
	HAND hMasterDevice;
	scamcam_t *psccm;
	I32 mastercamid;
	scamcam_t *psccs;
	scamif_t *pscamif;
	HAND hSlaveDevice;
	I32 slavecamid;
	U32 i;

	//--
	if (h == NULL) {
		res = 0;
		goto func_exit;
	}
	pscamif = (scamif_t *) h;
	mastercamid = pscamif->mastercamid;
	slavecamid  = pscamif->slavecamid;
	if (
		/*mastercamid < 0 ||  */
		mastercamid >= (I32) pscamif->camcount || pscamif->camcount <= 0) {
		res = 0;
		goto func_exit;
	}

	//--
	
	if (mastercamid >= 0) {
		psccm = &pscamif->scc[mastercamid];
#if GIGEV_CHANNEL		
		hMasterDevice = psccm->hgecp;
#else
		hMasterDevice = pscamif->hPCIe;
#endif

	} else {
		psccm = NULL;
		hMasterDevice = NULL;
	}

	if (slavecamid >= 0 && slavecamid < (I32) pscamif->camcount) {
		psccs = &pscamif->scc[slavecamid];
#if GIGEV_CHANNEL		
		hSlaveDevice = psccs->hgecp;
#else
		hSlaveDevice = pscamif->hPCIe;
#endif		
	} else {
		psccs = NULL;
		hSlaveDevice = NULL;
	}
#if GIGEV_CHANNEL
	if (hMasterDevice != NULL) {
		if (psccm->connected) {
			for (i = 0; i < STOP_TRY; i++) {
				res = GECP_CamCaptureStop(hMasterDevice);
				if (res >= 0) break;
			}
			for (i = 0; i < STOP_TRY; i++) {
				res = GECP_startSendStart(hMasterDevice, 0);
				if (res >= 0) break;
			}
		}
	}
	if (hSlaveDevice != NULL) {
		if (psccs->connected) {
			for (i = 0; i < STOP_TRY; i++) {
				res = GECP_CamCaptureStop(hSlaveDevice);
				if (res >= 0) break;
			}
			for (i = 0; i < STOP_TRY; i++) {
				res = GECP_startSendStart(hSlaveDevice, 0);
				if (res >= 0) break;
			}
		}
	}
#else
	if (hMasterDevice != NULL) {
		if (psccm->connected) {
			res = HAL_PCIE_StopCam(hMasterDevice, mastercamid);
			if (res != CR_OK) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:%d HAL_PCIE_StopCam res: %d\n", __func__, __LINE__, res); 

			}
		}
	}
	if (hSlaveDevice != NULL) {
		if (psccs->connected) {
			res = HAL_PCIE_StopCam(hSlaveDevice, slavecamid);
			if (res != CR_OK) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:%d HAL_PCIE_StopCam res: %d\n", __func__, __LINE__, res); 

			}
		}	
	}

#endif
	res = 1;

func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      PAUSE
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/
I32 scamif_pause(HAND h)
{
	I32 res;
	HAND hMasterDevice;
	scamcam_t *psccm;
	I32 mastercamid;
	scamcam_t *psccs;
	scamif_t *pscamif;
	HAND hSlaveDevice;
	I32 slavecamid;
	U32 i;
	//--
	if (h == NULL) {
		res = 0;
		goto func_exit;
	}
	pscamif = (scamif_t *) h;
	mastercamid = pscamif->mastercamid;
	slavecamid  = pscamif->slavecamid;
	if (	mastercamid >= (I32) pscamif->camcount || pscamif->camcount <= 0) {
		res = 0;
		goto func_exit;
	}

	//--
	if (mastercamid >= 0) {
		psccm = &pscamif->scc[mastercamid];
		hMasterDevice = psccm->hgecp;
	} else {
		psccm = NULL;
		hMasterDevice = NULL;
	}
	if (slavecamid >= 0 && slavecamid < (I32) pscamif->camcount) {
		psccs = &pscamif->scc[slavecamid];
		hSlaveDevice = psccs->hgecp;
	} else {
		hSlaveDevice = NULL;
		psccs = NULL;
	}

#if GIGEV_CHANNEL
	if (hMasterDevice != NULL) {
		if (psccm->connected) {
			for (i = 0; i < GECP_TRY; i++) {
				res = GECP_startSendStart(hMasterDevice, 0);
				if (res >= 0) break;
			}
		}
	}
	if (hSlaveDevice != NULL) {
		if (psccs->connected) {
			for (i = 0; i < GECP_TRY; i++) {
				res = GECP_startSendStart(hSlaveDevice, 0);
				if (res >= 0) break;
			}
		}
	}
#else
 	// TODO: SCAMIF if necessary
#endif
	res = 1;

func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      RESUME
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/
I32 scamif_resume(HAND h)
{
	I32 res;
	HAND hgecpm;
	scamcam_t *psccm;
	I32 mastercamid;
	scamcam_t *psccs;
	scamif_t *pscamif;
	HAND hgecps;
	I32 slavecamid;
	U32 i;
	//--
	if (h == NULL) {
		res = 0;
		goto func_exit;
	}
	pscamif = (scamif_t *) h;
	mastercamid = pscamif->mastercamid;
	slavecamid  = pscamif->slavecamid;
	if (	mastercamid >= (I32) pscamif->camcount || pscamif->camcount <= 0) {
		res = 0;
		goto func_exit;
	}

	//--
	if (mastercamid >= 0) {
		psccm = &pscamif->scc[mastercamid];
		hgecpm = psccm->hgecp;
	} else {
		hgecpm = NULL;
		psccm = NULL;
	}
	if (slavecamid >= 0 || slavecamid < (I32) pscamif->camcount) {
		psccs = &pscamif->scc[slavecamid];
		hgecps = psccs->hgecp;
	} else {
		hgecps = NULL;
		psccs = NULL;
	}

#if GIGEV_CHANNEL
	if (hgecpm != NULL) {
		if (psccm->connected) {
			for (i = 0; i < GECP_TRY; i++) {
				res = GECP_startSendStart(hgecpm, 1);
				if (res >= 0) break;
			}
		}
	}
	if (hgecps != NULL) {
		if (psccs->connected) {
			for (i = 0; i < GECP_TRY; i++) {
				res = GECP_startSendStart(hgecps, 1);
				if (res >= 0) break;
			}
		}
	}
#else
 	// TODO: SCAMIF if necessary
#endif	
	res = 1;

func_exit:
	return res;
}

#if GIGEV_CHANNEL
/*!
 ********************************************************************************
 *	@brief      Set Camera IP and my Image data receiving port
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	ip
 *              camera ip string
 *  @param[in]	port
 *              Image Port for PC
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/
I32 scamif_cam_ipport(HAND h, 
		U32 camid,						
		U08 *ipstr,								// camera IP
		U32 port								// Image Port for PC
		)								
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;

	//--
	res = check_params(h, camid, CR_FALSE);
	if (res <= 0) {
		goto func_exit;
	}

	pscamif = (scamif_t *) h;

	psc = &pscamif->scc[camid];
	psc->camip = GECP_Ipstr2Ip(NULL, (char *)ipstr);
	psc->gvspport = port;
	
	res = 1;
func_exit:
	return res;
}



/*!
 ********************************************************************************
 *	@brief      Pause Camera capture..
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/
I32 scamif_cam_pause(HAND h, U32 camid)	// Pause camera operation
{
	I32 res;
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_FALSE);
	if (res <= 0) {
		goto func_exit;
	}	

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	for (i = 0; i < GECP_TRY; i++) {
		res = GECP_CamGvspStop(hgecp);
		if (res >= 0) break;
	}

	res = 1;

func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Resume Camera capture..
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/
I32 scamif_cam_resume(HAND h, U32 camid)	// Resume camera operation
{
	I32 res;
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_FALSE);
	if (res <= 0) {
		goto func_exit;
	}	

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	for (i = 0; i < GECP_TRY; i++) {
		res = GECP_CamGvspStart(hgecp);
		if (res >= 0) break;
	}

	res = 1;

func_exit:
	return res;
}
#endif


/*!
 ********************************************************************************
 *	@brief      Connected ?
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: connected. 0: Not connected
 * 
 *	@author	    yhsuk
 *  @date       2016/11/08
 *******************************************************************************/
I32 scamif_cam_connected(HAND h, U32 camid)	
{
	return check_params(h, camid, CR_TRUE);
}


/*!
 ********************************************************************************
 *	@brief      Set fmcinit config
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	fmcinit
 *              1: Do FMC Init   0: Not
 *  @param[in]	zrot
 *              1: ZROT mode,  0: NROT mode
 *  @return		1: Success, 0: Fail.
 *
 *	@author	    yhsuk
 *  @date       2016/11/02
 *******************************************************************************/
I32 scamif_cam_fmcinit_config(HAND hscamif, U32 camid, U32 fmcinit, U32 zrot, U32 autoinit)
{
	I32 res;
	scamif_t *pscamif;

	//--
	if (hscamif == NULL) {
		res = 0;
		goto func_exit;
	}

	pscamif = (scamif_t *)hscamif;
	pscamif->act_vrinitfmc[camid] = fmcinit;
	pscamif->zrot[camid] = zrot;
	pscamif->autoinit[camid] = autoinit;

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" cam [%d] zrot: %d, act_vrinitfmc: %d\n", camid, zrot, fmcinit);
	res = 1;

func_exit:
	return res;
}





/*!
 ********************************************************************************
 *	@brief      Start BULK transfer with ts64
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	ts_h
 *              start frame timestamp (ns), higher 32bit
 *  @param[in]	ts_l
 *              start frame timestamp (ns), lower 32bit
 *  @param[in]	framecount
 *              transfer frame count
 *  @param[in]	waittime
 *              waittime between sending image frame (2^waittime usec)
 *  @param[in]	continueafterbulk
 *              0: pause after BULK transfer, 1: continue after BULK transfer
 *  @param[in]	multitude4Bulk
 *              multitude for BULK transfer
 *  @param[in]	skip4bulk
 *              skip for BULK transfer
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/
I32 scamif_cam_BULK64(HAND h, U32 camid,   	// BULK transfer..... with 
		U64 ts64,
		U32 framecount, 
		U32 waittime, 
		U32 continueafterbulk,
		U32 multitude4Bulk,
		U32 skip4bulk,

		U64 ts64shot, I32 axx1024, I32 bxx1024, I32 ayx1024, I32 byx1024)
{
	I32 res;
	U32 ts_h;
	U32 ts_l;
	U32 tsshot_h;
	U32 tsshot_l;

	//--
	ts_h = EXTRACTH(ts64);
	ts_l = EXTRACTL(ts64);

	tsshot_h = EXTRACTH(ts64shot);
	tsshot_l = EXTRACTL(ts64shot);


	res = scamif_cam_BULK(h, camid,   	
		ts_h, ts_l, 
		framecount, 
		waittime, 
		continueafterbulk,
		multitude4Bulk,
		skip4bulk,

		tsshot_h, tsshot_l, axx1024, bxx1024, ayx1024, byx1024
		);

	return res;

}

/*!
 ********************************************************************************
 *	@brief      Start BULK transfer
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	ts_h
 *              start frame timestamp (ns), higher 32bit
 *  @param[in]	ts_l
 *              start frame timestamp (ns), lower 32bit
 *  @param[in]	framecount
 *              transfer frame count
 *  @param[in]	waittime
 *              waittime between sending image frame (2^waittime usec)
 *  @param[in]	continueafterbulk
 *              0: pause after BULK transfer, 1: continue after BULK transfer
 *  @param[in]	multitude4Bulk
 *              multitude for BULK transfer
 *  @param[in]	skip4bulk
 *              skip for BULK transfer
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/
I32 scamif_cam_BULK(HAND h, U32 camid,   	// BULK transfer..... with 
		U32 ts_h, U32 ts_l, 
		U32 framecount, 
		U32 waittime, 
		U32 continueafterbulk,
		U32 multitude4Bulk,
		U32 skip4bulk,

		U32 tsshot_h, U32 tsshot_l, I32 axx1024, I32 bxx1024, I32 ayx1024, I32 byx1024)
{

	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;
	camimageinfo_t	*pimi_BULK;
	U32 i;
	U32 width, height;
	U32 offset_x, offset_y;
	U32 skip;
	U32 multitude;

	//--
	res = 0;
	if (h == NULL) {
		goto func_exit;
	}
	pscamif = (scamif_t *) h;
	if (camid >= pscamif->camcount) {
		goto func_exit;
	}

	psc = &pscamif->scc[camid];
	pimi_BULK = &psc->imginfo_BULK;

	//-
	width		= pimi_BULK->width;
	height 		= pimi_BULK->height;
	offset_x 	= pimi_BULK->offset_x;
	offset_y 	= pimi_BULK->offset_y;

	skip		= skip4bulk;
	multitude	= multitude4Bulk;

#if GIGEV_CHANNEL

	for (i = 0; i < GECP_TRY; i++) {
		res = GECP_ImageBulkRequest(psc->hgecp, ts_h, ts_l, framecount, waittime, continueafterbulk, multitude, skip,
			width, height, offset_x, offset_y, tsshot_h, tsshot_l, axx1024, bxx1024, ayx1024, byx1024);
		if (res >= 0) break;
	}

#else
	res = HAL_PCIE_RequestBulkImage(pscamif->hPCIe, camid, ts_h, ts_l, framecount, waittime, continueafterbulk, multitude, skip,
		width, height, offset_x, offset_y, tsshot_h, tsshot_l, axx1024, bxx1024, ayx1024, byx1024);
	if (res != CR_OK) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:%d HAL_PCIE_RequestBulkImage res: %d\n", __func__, __LINE__, res); 
		res = 0;
	}


#endif

func_exit:
	return res;
}



I32 scamif_image_property2(HAND h, U32 camid,
		U32 width, U32 height,
		U32 offset_x, U32 offset_y,
		U32 framerate,
		U32 gain,
		U32 exposure,
		U32 skip,
		U32 multitude,
		U32 syncdiv,
		U32 exposure_post, 
		U32 exposure_expiate
		)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;

	camimageinfo_t	*pimi_s;
	//--
	if (h == NULL) {
		res = 0;
		goto func_exit;
	}
	pscamif = (scamif_t *) h;
	if (camid >= pscamif->camcount) {
		res = 0;
		goto func_exit;
	}

	psc 	= &pscamif->scc[camid];
	pimi_s	= &psc->imginfo_shadow;

	//-
	pimi_s->width		= width;
	pimi_s->height		= height;
	pimi_s->offset_x	= offset_x;
	pimi_s->offset_y	= offset_y;
	pimi_s->framerate	= framerate;
	pimi_s->gain		= gain;
	pimi_s->exposure	= exposure;
	pimi_s->skip		= skip;
	pimi_s->multitude	= multitude;
	pimi_s->syncdiv		= syncdiv;

	pimi_s->exposure_post = exposure_post;
	pimi_s->exposure_expiate = exposure_expiate;

	res = 1;
func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      set image properties.. 
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	bright
 *              image LUT bright
 *  @param[in]	contrast
 *              image LUT contrast
 *  @param[in]	gamma
 *              image LUT gamma
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0307
 *******************************************************************************/

I32 scamif_image_property_LUT(HAND h, U32 camid,
		U32 bright, U32 contrast, U32 gamma)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;

	camimageinfo_t	*pimi_s;
	//--
	res = check_params(h, camid, CR_FALSE);
	if (res <= 0) {
		goto func_exit;
	}
	
	pscamif = (scamif_t *) h;


	psc 	= &pscamif->scc[camid];
	pimi_s	= &psc->imginfo_shadow;

	//-
	pimi_s->lut_bright		= bright;
	pimi_s->lut_contrast	= contrast;
	pimi_s->lut_gamma		= gamma;


	res = 1;
func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      set gain..
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	gain
 *              image gain
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2020/1002
 *******************************************************************************/

I32 scamif_image_property_gain(HAND h, U32 camid, U32 gain)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;

	camimageinfo_t	*pimi_s;
	//--
	res = check_params(h, camid, CR_FALSE);
	if (res <= 0) {
		goto func_exit;
	}

	pscamif = (scamif_t *) h;

	psc 	= &pscamif->scc[camid];
	pimi_s	= &psc->imginfo_shadow;

	//-
	pimi_s->gain		= gain;

	res = 1;
func_exit:
	return res;
}






/*!
 ********************************************************************************
 *	@brief      set image LED bright 
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	ledbright
 *              image LED bright
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0307
 *******************************************************************************/

I32 scamif_image_property_LEDbright(HAND h, U32 camid,
		U32 ledbright)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;

	camimageinfo_t	*pimi_s;
	//--
	res = check_params(h, camid, CR_FALSE);
	if (res <= 0) {
		goto func_exit;
	}

	pscamif = (scamif_t *) h;

	psc 	= &pscamif->scc[camid];
	pimi_s	= &psc->imginfo_shadow;

	//-
	pimi_s->ledbrgt		= ledbright;
	res = 1;
func_exit:
	return res;
}

I32 scamif_image_property_BULK(HAND h, U32 camid,
		U32 width, U32 height,
		U32 offset_x, U32 offset_y)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;

	camimageinfo_t	*pimi_s;
	camimageinfo_t	*pimi_BULK_s;
	camimageinfo_t	*pimi_BULK;
	//--
	res = check_params(h, camid, CR_FALSE);
	if (res <= 0) {
		goto func_exit;
	}	
	pscamif = (scamif_t *) h;

	psc 	= &pscamif->scc[camid];
	pimi_s	= &psc->imginfo_shadow;
	pimi_BULK_s	= &psc->imginfo_BULK_shadow;
	pimi_BULK	= &psc->imginfo_BULK;

	//-
	memcpy(pimi_BULK_s, pimi_s, sizeof(camimageinfo_t));

	pimi_BULK_s->width		= width;
	pimi_BULK_s->height		= height;
	pimi_BULK_s->offset_x	= offset_x;
	pimi_BULK_s->offset_y	= offset_y;


	memcpy(pimi_BULK, pimi_BULK_s, sizeof(camimageinfo_t));
	res = 1;
func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      set image size..
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	width
 *              image width
 *  @param[in]	height
 *              image height
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/
I32 scamif_image_size(HAND h, U32 camid, U32 width, U32 height)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;

	camimageinfo_t	*pimi_s;
	//--
	res = check_params(h, camid, CR_FALSE);
	if (res <= 0) {
		goto func_exit;
	}

	pscamif = (scamif_t *) h;

	psc 	= &pscamif->scc[camid];
	pimi_s	= &psc->imginfo_shadow;

	//-
	pimi_s->width		= width;
	pimi_s->height		= height;
	res = 1;
func_exit:
	return res;
}
/*!
 ********************************************************************************
 *	@brief      set image position offset
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	offset_x
 *  @param[in]	offset_y
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/

I32 scamif_image_offset(HAND h, U32 camid, U32 offset_x, U32 offset_y)	// Offset (offset_x, offset_y)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;

	camimageinfo_t	*pimi_s;
	//--
	res = check_params(h, camid, CR_FALSE);
	if (res <= 0) {
		goto func_exit;
	}
	pscamif = (scamif_t *) h;

	psc 	= &pscamif->scc[camid];
	pimi_s	= &psc->imginfo_shadow;

	//-
	pimi_s->offset_x	= offset_x;
	pimi_s->offset_y	= offset_y;

	res = 1;
func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      set image frame rate ( frame per sec)
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	framerate
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/

I32 scamif_image_fps(HAND h, U32 camid, U32 fps)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;

	camimageinfo_t	*pimi_s;
	//--
	res = check_params(h, camid, CR_FALSE);
	if (res <= 0) {
		goto func_exit;
	}

	pscamif = (scamif_t *) h;

	psc 	= &pscamif->scc[camid];
	pimi_s	= &psc->imginfo_shadow;

	//-
	pimi_s->framerate	= fps;

	res = 1;
func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      set image capture exposure time (usec)
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	exposure
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/

I32 scamif_image_exposure(HAND h, U32 camid, U32 exposureUsec)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;

	camimageinfo_t	*pimi_s;
	//--
	res = check_params(h, camid, CR_FALSE);
	if (res <= 0) {
		goto func_exit;
	}

	pscamif = (scamif_t *) h;

	psc 	= &pscamif->scc[camid];
	pimi_s	= &psc->imginfo_shadow;

	//-
	pimi_s->exposure	= exposureUsec;

	res = 1;
func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      set image capture skip
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	skip
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/

I32 scamif_image_skip(HAND h, U32 camid, U32 skip)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;

	camimageinfo_t	*pimi_s;
	//--
	res = check_params(h, camid, CR_FALSE);
	if (res <= 0) {
		goto func_exit;
	}
	pscamif = (scamif_t *) h;

	psc 	= &pscamif->scc[camid];
	pimi_s	= &psc->imginfo_shadow;

	//-
	pimi_s->skip		= skip;

	res = 1;
func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      set image capture multitude
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	multitude
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/
I32 scamif_image_multitude(HAND h, U32 camid, U32 multitude)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;

	camimageinfo_t	*pimi_s;
	//--
	res = check_params(h, camid, CR_FALSE);
	if (res <= 0) {
		goto func_exit;
	}

	pscamif = (scamif_t *) h;

	psc 	= &pscamif->scc[camid];
	pimi_s	= &psc->imginfo_shadow;

	//-
	pimi_s->multitude	= multitude;

	res = 1;
func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      set image capture syncdiv for Slave cam
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	syncdiv
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/

I32 scamif_image_syncdiv(HAND h, U32 camid, U32 syncdiv)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;

	camimageinfo_t	*pimi_s;
	//--
	res = check_params(h, camid, CR_FALSE);
	if (res <= 0) {
		goto func_exit;
	}
	pscamif = (scamif_t *) h;

	psc 	= &pscamif->scc[camid];
	pimi_s	= &psc->imginfo_shadow;

	//-
	pimi_s->syncdiv		= syncdiv;

	res = 1;
func_exit:
	return res;
}



I32 scamif_image_update(HAND h, U32 camid, U32 forcebufferinit)	// Update Image property, with/without buffer init
{
	I32 res;
	U32 i;
	
	scamif_t *pscamif;
	scamcam_t *psc;
	HAND hgecp;
	camimageinfo_t	*pimi;
	camimageinfo_t	*pimi_s;

	U32 needbufferinit;



	//------
	res = 0;
	if (h == NULL) {
		goto func_exit;
	}

	pscamif = (scamif_t *) h;

	if (camid >= pscamif->camcount) {
		goto func_exit;
	}

	psc = &pscamif->scc[camid];
	hgecp = psc->hgecp;

	pimi	= &psc->imginfo;
	pimi_s	= &psc->imginfo_shadow;

	if (forcebufferinit) {
		needbufferinit = 1;
	} else {
		if ((pimi->width != pimi_s->width)||(pimi->height != pimi_s->height)) {
			needbufferinit = 1;
		} else {
			needbufferinit = 0;
		}
	}

	memcpy(pimi, pimi_s, sizeof(camimageinfo_t));
	if (needbufferinit) {
		// BUFFER-INIT
		scamif_imagebuf_init(h, camid, 1, NULL);
	}


	if(psc->connected) {
		res = 1;
	} else {
		res = 0;
	}

	if (res > 0) {
		CAM_IMG_ATTR_T gvcpImgAttr;
		
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" CAM: [%d]--------------------\n", camid);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" \n\tsyncdiv: %d\n\twidth: %d height:%d\n\toffset_x; %d offsety: %d\n\tframerate: %d\n\tgain: %d\n\texposure: %d\n\tskip: %d\n\tmultitude: %d\n\n",
					pimi_s->syncdiv, 
					pimi_s->width,		pimi_s->height,
					pimi_s->offset_x,	pimi_s->offset_y,
					pimi_s->framerate,
					pimi_s->gain,
					pimi_s->exposure,
					pimi_s->skip,
					pimi_s->multitude);

		gvcpImgAttr.updateMode = GECP_UPDATE_MODE_DEFAULT;
		gvcpImgAttr.syncdiv = pimi_s->syncdiv;		
		gvcpImgAttr.width = pimi_s->width;				
		gvcpImgAttr.height = pimi_s->height;		
		gvcpImgAttr.sx = pimi_s->offset_x;
		gvcpImgAttr.sy = pimi_s->offset_y;		
		gvcpImgAttr.fps = pimi_s->framerate;
		gvcpImgAttr.gain = pimi_s->gain;		
		gvcpImgAttr.exposureusec = pimi_s->exposure;
		gvcpImgAttr.skip = pimi_s->skip;		
		gvcpImgAttr.multitude4normal = pimi_s->multitude;
		gvcpImgAttr.zrot = 0;		
		gvcpImgAttr.autoinit = 0;
		gvcpImgAttr.exposure_post = 0;		
		gvcpImgAttr.exposure_expiate = 0;				

		if (pscamif->scc[camid].connected) {
#if GIGEV_CHANNEL			
			for (i = 0; i < IMAGEUPDATE_TRY; i++) {
				res = GECP_ImageUpdate(hgecp, &gvcpImgAttr);				

				if (res > 0) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" camid [%d] success. %d res: %d\n\n", camid, i, res);
					break;
				}
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" camid [%d] fail. %d res: %d\n\n", camid, i, res);

			}
#else
			res = HAL_PCIE_UpdateImageParamAll(pscamif->hPCIe, camid, &gvcpImgAttr);				

			if (res != CR_OK) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n\n", __func__, camid, res);	
				res = 0;
			}
#endif
		} else {
			res = 0;
		}
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Update image parameter.
 *                ( imginfo_shadow -> imginfo)
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	forcebufferinit
 *              Always Buffer init.
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/
I32 scamif_image_update2(HAND h, U32 camid, U32 forcebufferinit)	// Update Image property, with/without buffer init
{
	I32 res;
	U32 i;
	
	scamif_t *pscamif;
	scamcam_t *psc;
	HAND hgecp;
	camimageinfo_t	*pimi;
	camimageinfo_t	*pimi_s;

	U32 needbufferinit;

	U32 zrot;
	U32 autoinit;

	//------
	res = 0;
	if (h == NULL) {
		goto func_exit;
	}

	pscamif = (scamif_t *) h;

	if (camid >= pscamif->camcount) {
		goto func_exit;
	}


	psc = &pscamif->scc[camid];
	if (psc->connected == 0) {
		goto func_exit;
	}
	hgecp = psc->hgecp;

	pimi	= &psc->imginfo;
	pimi_s	= &psc->imginfo_shadow;

	if (forcebufferinit) {
		needbufferinit = 1;
	} else {
		if ((pimi->width != pimi_s->width)||(pimi->height != pimi_s->height)) {
			needbufferinit = 1;
		} else {
			needbufferinit = 0;
		}
	}

	memcpy(pimi, pimi_s, sizeof(camimageinfo_t));
	if (needbufferinit) {
		// BUFFER-INIT
		scamif_imagebuf_init(h, camid, 1, NULL);
	}

	zrot = pscamif->zrot[camid];
	autoinit = pscamif->autoinit[camid];
	
	res = 1;

	if (psc->connected) {
		res = 1;
	} else {
		res = 0;
	}

	if (res > 0) {
		CAM_IMG_ATTR_T gvcpImgAttr;									
		
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" update2, CAM: [%d]--------------------\n", camid);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" \n\tsyncdiv: %d\n\twidth: %d height:%d\n\toffset_x; %d offsety: %d\n\tframerate: %d\n\tgain: %d\n\texposure: %d\n\tskip: %d\n\tmultitude: %d\n\tzrot: %d\n\tautoinit:%d\n\texposure_post:%d\n\texposure_expiate:%d \n\n",
					pimi_s->syncdiv, 
					pimi_s->width,		pimi_s->height,
					pimi_s->offset_x,	pimi_s->offset_y,
					pimi_s->framerate,
					pimi_s->gain,
					pimi_s->exposure,
					pimi_s->skip,
					pimi_s->multitude,
					zrot,
					autoinit

					,pimi_s->exposure_post,
					pimi_s->exposure_expiate
					);

		gvcpImgAttr.updateMode = GECP_UPDATE_MODE_EXT;
		gvcpImgAttr.syncdiv = pimi_s->syncdiv;		
		gvcpImgAttr.width = pimi_s->width;				
		gvcpImgAttr.height = pimi_s->height;		
		gvcpImgAttr.sx = pimi_s->offset_x;
		gvcpImgAttr.sy = pimi_s->offset_y;		
		gvcpImgAttr.fps = pimi_s->framerate;
		gvcpImgAttr.gain = pimi_s->gain;		
		gvcpImgAttr.exposureusec = pimi_s->exposure;
		gvcpImgAttr.skip = pimi_s->skip;		
		gvcpImgAttr.multitude4normal = pimi_s->multitude;
		gvcpImgAttr.zrot = zrot;		
		gvcpImgAttr.autoinit = autoinit;
		gvcpImgAttr.exposure_post = pimi_s->exposure_post;		
		gvcpImgAttr.exposure_expiate = pimi_s->exposure_expiate;		

#if GIGEV_CHANNEL	
		for (i = 0; i < IMAGEUPDATE_TRY; i++) {
			res = GECP_ImageUpdate(hgecp, &gvcpImgAttr);
			
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
#else
		res = HAL_PCIE_UpdateImageParamAll(pscamif->hPCIe, camid, &gvcpImgAttr);				

		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n\n", __func__, camid, res);	
			res = 0;
		}
#endif		
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Update image parameter all..
 *                ( imginfo_shadow -> imginfo)
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	forcebufferinit
 *              Always Buffer init.
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/04/12
 *******************************************************************************/
I32 scamif_image_update_all(HAND h, U32 forcebufferinit)	// Update Image property, with/without buffer init
{
	I32 res;
	U32 i;
	
	scamif_t *pscamif;

	scamcam_t *psc0;
	HAND hgecp0;
	camimageinfo_t	*pimi0;
	camimageinfo_t	*pimi_s0;

	scamcam_t *psc1;
	HAND hgecp1;
	camimageinfo_t	*pimi1;
	camimageinfo_t	*pimi_s1;

	U32 needbufferinit;

	U32 zrot0;
	U32 autoinit0;

	U32 zrot1;
	U32 autoinit1;

	//------
	res = 0;
	if (h == NULL) {
		goto func_exit;
	}

	pscamif = (scamif_t *) h;

	psc0 = &pscamif->scc[0];
	if (psc0->connected == 0) {
		goto func_exit;
	}
	hgecp0 = psc0->hgecp;

	pimi0	= &psc0->imginfo;
	pimi_s0	= &psc0->imginfo_shadow;

	psc1 = &pscamif->scc[1];
	if (psc1->connected == 0) {
		goto func_exit;
	}
	hgecp1 = psc1->hgecp;

	pimi1	= &psc1->imginfo;
	pimi_s1	= &psc1->imginfo_shadow;

	if (forcebufferinit) {
		needbufferinit = 1;
	} else {
		needbufferinit = 0;
		if ((pimi0->width != pimi_s0->width)||(pimi0->height != pimi_s0->height)) {
			needbufferinit = 1;
		}
		
		if ((pimi1->width != pimi_s1->width)||(pimi1->height != pimi_s1->height)) {
			needbufferinit = 1;
		}
		
	}
	memcpy(pimi0, pimi_s0, sizeof(camimageinfo_t));
	memcpy(pimi1, pimi_s1, sizeof(camimageinfo_t));

	if (needbufferinit) {
		// BUFFER-INIT
		scamif_imagebuf_init(h, 0, 1, NULL);
		scamif_imagebuf_init(h, 1, 1, NULL);
	}

	zrot0 = pscamif->zrot[0];
	autoinit0 = pscamif->autoinit[0];

	zrot1 = pscamif->zrot[1];
	autoinit1 = pscamif->autoinit[1];
	
	res = 1;

	if (psc0->connected && psc1->connected) {
		res = 1;
	} else {
		res = 0;
	}

	if (res > 0) {
		CAM_IMG_ATTR_T pGvcpImgAttr[2];
		
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" update both\n");
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__"CAM0 \tsyncdiv: %d\n\twidth: %d height:%d\n\toffset_x; %d offsety: %d\n\tframerate: %d\n\tgain: %d\n\texposure: %d\n\tskip: %d\n\tmultitude: %d\n\texposure_post: %d\n\texposure_expiate: %d\n\tzrot: %d\n\tautoinit:%d\n\n",
					pimi_s0->syncdiv, 
					pimi_s0->width,		pimi_s0->height,
					pimi_s0->offset_x,	pimi_s0->offset_y,
					pimi_s0->framerate,
					pimi_s0->gain,
					pimi_s0->exposure,
					pimi_s0->skip,
					pimi_s0->multitude,
					pimi_s0->exposure_post,
					pimi_s0->exposure_expiate,

					zrot0,
					autoinit0
					);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__"CAM1 \tsyncdiv: %d\n\twidth: %d height:%d\n\toffset_x; %d offsety: %d\n\tframerate: %d\n\tgain: %d\n\texposure: %d\n\tskip: %d\n\tmultitude: %d\n\texposure_post: %d\n\texposure_expiate: %d\n\tzrot: %d\n\tautoinit:%d\n\n",
					pimi_s1->syncdiv, 
					pimi_s1->width,		pimi_s1->height,
					pimi_s1->offset_x,	pimi_s1->offset_y,
					pimi_s1->framerate,
					pimi_s1->gain,
					pimi_s1->exposure,
					pimi_s1->skip,
					pimi_s1->multitude,
					pimi_s1->exposure_post,
					pimi_s1->exposure_expiate,
					zrot1,
					autoinit1
					);

		pGvcpImgAttr[0].updateMode = GECP_UPDATE_MODE_EXT;
		pGvcpImgAttr[0].syncdiv = pimi_s0->syncdiv;		
		pGvcpImgAttr[0].width = pimi_s0->width;				
		pGvcpImgAttr[0].height = pimi_s0->height;		
		pGvcpImgAttr[0].sx = pimi_s0->offset_x;
		pGvcpImgAttr[0].sy = pimi_s0->offset_y;		
		pGvcpImgAttr[0].fps = pimi_s0->framerate;
		pGvcpImgAttr[0].gain = pimi_s0->gain;		
		pGvcpImgAttr[0].exposureusec = pimi_s0->exposure;
		pGvcpImgAttr[0].skip = pimi_s0->skip;		
		pGvcpImgAttr[0].multitude4normal = pimi_s0->multitude;
		pGvcpImgAttr[0].zrot = zrot0;		
		pGvcpImgAttr[0].autoinit = autoinit0;
		pGvcpImgAttr[0].exposure_post = pimi_s0->exposure_post;		
		pGvcpImgAttr[0].exposure_expiate = pimi_s0->exposure_expiate;	

		pGvcpImgAttr[1].updateMode = GECP_UPDATE_MODE_EXT;
		pGvcpImgAttr[1].syncdiv = pimi_s1->syncdiv;		
		pGvcpImgAttr[1].width = pimi_s1->width;				
		pGvcpImgAttr[1].height = pimi_s1->height;		
		pGvcpImgAttr[1].sx = pimi_s1->offset_x;
		pGvcpImgAttr[1].sy = pimi_s1->offset_y;		
		pGvcpImgAttr[1].fps = pimi_s1->framerate;
		pGvcpImgAttr[1].gain = pimi_s1->gain;		
		pGvcpImgAttr[1].exposureusec = pimi_s1->exposure;
		pGvcpImgAttr[1].skip = pimi_s1->skip;		
		pGvcpImgAttr[1].multitude4normal = pimi_s1->multitude;
		pGvcpImgAttr[1].zrot = zrot1;		
		pGvcpImgAttr[1].autoinit = autoinit1;
		pGvcpImgAttr[1].exposure_post = pimi_s1->exposure_post;		
		pGvcpImgAttr[1].exposure_expiate = pimi_s1->exposure_expiate;				

#if GIGEV_CHANNEL	
		for (i = 0; i < IMAGEUPDATE_TRY; i++) {
			res = GECP_ImageUpdate_both(hgecp0,	pGvcpImgAttr);
	
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
#else
		res = HAL_PCIE_UpdateImageParamAll(pscamif->hPCIe, 0, &pGvcpImgAttr[0]);						
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail. res: %d\n", __func__, 0,  res);	
			res = 0;
			goto func_exit;
		}

		res = HAL_PCIE_UpdateImageParamAll(pscamif->hPCIe, 1, &pGvcpImgAttr[1]);						
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail. res: %d\n", __func__, 1,  res);	
			res = 0;
			goto func_exit;			
		}		
#endif

	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}




/*!
 ********************************************************************************
 *	@brief      Update LUT
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0307
 *******************************************************************************/
I32 scamif_image_update_LUT(HAND h, U32 camid)
{
	I32 res;
	HAND hgecp;
	camimageinfo_t	*pimi;
	camimageinfo_t	*pimi_s;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	pimi	= &(((scamif_t *)h)->scc[camid].imginfo);
	pimi_s	= &(((scamif_t *)h)->scc[camid].imginfo_shadow);

	memcpy(pimi, pimi_s, sizeof(camimageinfo_t));

	if (res > 0) {
		res = scamif_image_LUT(h,  camid, pimi_s->lut_bright, pimi_s->lut_contrast, pimi_s->lut_gamma);	
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Update LED Bright
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0307
 *******************************************************************************/
I32 scamif_image_update_LEDbright(HAND h, U32 camid)
{
	I32 res;
	HAND hgecp;
	camimageinfo_t	*pimi;
	camimageinfo_t	*pimi_s;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	pimi	= &(((scamif_t *)h)->scc[camid].imginfo);
	pimi_s	= &(((scamif_t *)h)->scc[camid].imginfo_shadow);

	memcpy(pimi, pimi_s, sizeof(camimageinfo_t));


	if (res > 0) {
		res = scamif_led_brightness(h, camid, pimi_s->ledbrgt);
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}



/*!
 ********************************************************************************
 *	@brief      Update gain
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2020/1002
 *******************************************************************************/
I32 scamif_image_update_gain(HAND h, U32 camid)
{
	I32 res;
	HAND hgecp;
	camimageinfo_t	*pimi;
	camimageinfo_t	*pimi_s;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	pimi	= &(((scamif_t *)h)->scc[camid].imginfo);
	pimi_s	= &(((scamif_t *)h)->scc[camid].imginfo_shadow);

	memcpy(pimi, pimi_s, sizeof(camimageinfo_t));

	if (res > 0) {
		res = scamif_image_gain(h,  camid, pimi_s->gain);
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}




/*!
 ********************************************************************************
 *	@brief      Update gain, both cam.
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: GOOD. 0: FAIL.
 *
 *	@author	    yhsuk
 *  @date       2020/1002
 *******************************************************************************/
I32 scamif_image_update_gain_all(HAND h)
{
	I32 res;
	
	res = scamif_image_update_gain(h, 0);
	if (res) {
		res = scamif_image_update_gain(h, 1);
	}
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Gamma correction. using LUT
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	gamma    ( 100: 1.0)
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/01/31
 *******************************************************************************/
I32 scamif_image_gamma(HAND h, U32 camid, U32 gamma)	
{
	I32 res;
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

#if GIGEV_CHANNEL
	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {		
		for (i = 0; i < IMAGEUPDATE_TRY; i++) {
			res = GECP_ImageGamma(hgecp,	gamma);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
	}
#else	

	if (res) {		
		hgecp = ((scamif_t *)h)->hPCIe;
		res = HAL_PCIE_UpdateImageParam(hgecp,	camid, CAM_IMAGE_PARAM_GAMMA, gamma);		
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
			res = 0;
			goto func_exit;
		}
	}
#endif

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}



/*!
 ********************************************************************************
 *	@brief      Contras using LUT
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	contrast    ( 100: 1.0)
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/03/04
 *******************************************************************************/
I32 scamif_image_contrast(HAND h, U32 camid, U32 contrast)	
{
	I32 res;
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

#if GIGEV_CHANNEL

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {
		for (i = 0; i < IMAGEUPDATE_TRY; i++) {
			res = GECP_ImageContrast(hgecp,	contrast);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
	}

#else	

	if (res) {		
		hgecp = ((scamif_t *)h)->hPCIe;
		res = HAL_PCIE_UpdateImageParam(hgecp, camid, CAM_IMAGE_PARAM_CONTRAST, 	contrast);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
			res = 0;
			goto func_exit;
		}
	}
#endif


func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}



/*!
 ********************************************************************************
 *	@brief      Bright using LUT
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	bright    (100:... )
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/03/04
 *******************************************************************************/
I32 scamif_image_bright(HAND h, U32 camid, U32 bright)	
{
	I32 res;
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

#if GIGEV_CHANNEL

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {
		for (i = 0; i < IMAGEUPDATE_TRY; i++) {
			res = GECP_ImageBright(hgecp,	bright);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
	}
#else	

	if (res) {		
		hgecp = ((scamif_t *)h)->hPCIe;
		res = HAL_PCIE_UpdateImageParam(hgecp, camid, CAM_IMAGE_PARAM_BRIGHT, 	bright);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
			res = 0;
			goto func_exit;
		}
	}
#endif


func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}




/*!
 ********************************************************************************
 *	@brief      LUT
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	bright
 *  @param[in]	contrast
 *  @param[in]	gamma
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/03/04
 *******************************************************************************/
I32 scamif_image_LUT(HAND h, U32 camid, U32 bright, U32 contrast, U32 gamma)	
{
	I32 res;
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

#if GIGEV_CHANNEL

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {
		for (i = 0; i < IMAGEUPDATE_TRY; i++) {
			res = GECP_ImageLUT(hgecp,	bright, contrast, gamma);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
	}
#else	

	if (res) {		
		hgecp = ((scamif_t *)h)->hPCIe;
		res = HAL_PCIE_UpdateImageParam(hgecp, camid, CAM_IMAGE_PARAM_GAMMA, 	gamma);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
			res = 0;
			goto func_exit;
		}
		res = HAL_PCIE_UpdateImageParam(hgecp, camid, CAM_IMAGE_PARAM_CONTRAST, 	contrast);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
			res = 0;
			goto func_exit;
		}
		res = HAL_PCIE_UpdateImageParam(hgecp, camid, CAM_IMAGE_PARAM_BRIGHT, 	bright);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
			res = 0;
			goto func_exit;
		}		
	}
#endif	

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}


/*!
 ********************************************************************************
 *	@brief      gain
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	gain
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2020/1002
 *******************************************************************************/
I32 scamif_image_gain(HAND h, U32 camid, U32 gain)
{
	I32 res;
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

#if GIGEV_CHANNEL

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {
		for (i = 0; i < IMAGEUPDATE_TRY; i++) {
			res = GECP_ImageGain(hgecp,	gain);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d gain: %d,  res: %d\n", __LINE__, i, gain, res);
			if (res > 0) {
				break;
			}
		}
	}

#else	
	if (res) {		
		hgecp = ((scamif_t *)h)->hPCIe;
		res = HAL_PCIE_UpdateImageParam(hgecp, camid, CAM_IMAGE_PARAM_GAIN, 	gain);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
			res = 0;
			goto func_exit;
		}		
	}
#endif


func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}




/*!
 ********************************************************************************
 *	@brief      gain, cam0 and cam1
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	gain0
 *  @param[in]	gain1
 *
 *  @return		1: GOOD. 0: FAIL.
 *
 *	@author	    yhsuk
 *  @date       2020/1002
 *******************************************************************************/
I32 scamif_image_gain_all(HAND h, U32 gain0, U32 gain1)
{
	I32 res;

	res = scamif_image_gain(h, 0, gain0);
	if (res > 0) {
		res = scamif_image_gain(h, 1, gain1);
	}
	return res;
}





/*!
 ********************************************************************************
 *	@brief      Exposure Delay offset. 
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	delayoffset 
 *				exposure delay offset. usec.
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/02/23
 *******************************************************************************/
I32 scamif_image_delayoffset(HAND h, U32 camid, U32 delayoffset)	
{
	I32 res;
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

#if GIGEV_CHANNEL

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {
		for (i = 0; i < IMAGEUPDATE_TRY; i++) {
			res = GECP_ImageDelayoffset(hgecp,	delayoffset);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
	}
#else	
	if (res) {		
		hgecp = ((scamif_t *)h)->hPCIe;
		res = HAL_PCIE_UpdateImageParam(hgecp, camid, CAM_IMAGE_PARAM_DELAY_OFFSET, 	delayoffset);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
			res = 0;
			goto func_exit;
		}		
	}
#endif	

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Exposure Expiate. 
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	expiate 
 *				exposure expiate. usec.
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/02/23
 *******************************************************************************/
I32 scamif_image_expiate(HAND h, U32 camid, U32 expiate)	
{
	I32 res;
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

#if GIGEV_CHANNEL

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {
		for (i = 0; i < IMAGEUPDATE_TRY; i++) {
			res = GECP_ImageExposureExpiate(hgecp,	expiate);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
	}
#else	
	if (res) {		
		hgecp = ((scamif_t *)h)->hPCIe;
		res = HAL_PCIE_UpdateImageParam(hgecp, camid, CAM_IMAGE_PARAM_EXPOSURE_EXPIATE, 	expiate);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
			res = 0;
			goto func_exit;
		}		
	}
#endif	

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}


/*!
 ********************************************************************************
 *	@brief      LED Brightness
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	gamma    ( 100: 1.0)
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0219
 *******************************************************************************/
I32 scamif_led_brightness(HAND h, U32 camid, U32 brightness)	
{
	I32 res;
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

#if GIGEV_CHANNEL

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {
		for (i = 0; i < IMAGEUPDATE_TRY; i++) {
			res = GECP_LED_Brightness(hgecp,	brightness);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
	}
#else	
	if (res) {		
		hgecp = ((scamif_t *)h)->hPCIe;
		res = HAL_PCIE_UpdateImageParam(hgecp, camid, CAM_IMAGE_PARAM_LED_BRIGHT, 	brightness);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
			res = 0;
			goto func_exit;
		}		
	}
#endif	

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}




/*!
 ********************************************************************************
 *	@brief      Cam timer Stop.
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2017/06/15
 *******************************************************************************/
I32 scamif_camtimer_stop(HAND h)
{
	I32 res;
	U32 i;

	I32 mastercamid;
	I32 slavecamid;

	scamif_t *pscamif;

	HAND hgecpm;
	HAND hgecps;

	//------
	res = 0;
	if (h == NULL) {
		goto func_exit;
	}

	pscamif = (scamif_t *) h;
	if (pscamif->streaming_channel_mode != 0) {
		goto func_exit;
	}
	mastercamid = pscamif->mastercamid;
	slavecamid  = pscamif->slavecamid;

	if (/*mastercamid < 0 || */
			mastercamid >= (I32) pscamif->camcount || pscamif->camcount <= 0) {
		res = 0;
		goto func_exit;
	}


	if (mastercamid >= 0) {
		hgecpm = pscamif->scc[mastercamid].hgecp;
	} else {
		hgecpm = NULL;
	}

	if (slavecamid >= 0) {
		hgecps = pscamif->scc[slavecamid].hgecp;
	} else {
		hgecps = NULL;
	}

#if GIGEV_CHANNEL

	if (hgecpm != NULL) {
		if (pscamif->scc[mastercamid].connected) {
			for (i = 0; i < GECP_TRY; i++) {
				res = GECP_CamTimerMode(hgecpm, GVCP_VR_TIMER_CMD_SET_MASTER_STOP);	// Master Timer Stop
				if (res >= 0) break;
			}
		}
	}

	if (hgecps != NULL) {
		if (pscamif->scc[slavecamid].connected) {
			for (i = 0; i < GECP_TRY; i++) {
				res = GECP_CamTimerMode(hgecps, GVCP_VR_TIMER_CMD_SET_MASTER_STOP);	// Master Timer Stop
				if (res >= 0) break;
			}
		}
	}

#else	
	// TODO: SCAMIF
#endif		


func_exit:
	return res;
}




/*!
 ********************************************************************************
 *	@brief      Reset Cam timer, Virtual slave mode.
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2017/06/15
 *******************************************************************************/
I32 scamif_camtimer_reset3(HAND h)
{
	I32 res;
	U32 i;

	I32 mastercamid;
	I32 slavecamid;

	scamif_t *pscamif;

	HAND hgecpm;
	HAND hgecps;

	//------
	res = 0;
	if (h == NULL) {
		goto func_exit;
	}

	pscamif = (scamif_t *) h;

	if (pscamif->streaming_channel_mode != 0) {
		res = 1;
		goto func_exit;
	}


	mastercamid = pscamif->mastercamid;
	slavecamid  = pscamif->slavecamid;

	if (/*mastercamid < 0 || */
		mastercamid >= (I32) pscamif->camcount || pscamif->camcount <= 0) {
		res = 0;
		goto func_exit;
	}



	if (mastercamid >= 0) {
		if (pscamif->scc[mastercamid].connected) {
			hgecpm = pscamif->scc[mastercamid].hgecp;
		} else {
			hgecpm = NULL;
		}
	} else {
		hgecpm = NULL;
	}

	if (slavecamid >= 0) {
		if (pscamif->scc[slavecamid].connected) {
			hgecps = pscamif->scc[slavecamid].hgecp;
		} else {
			hgecps = NULL;
		}
	} else {
		hgecps = NULL;
	}

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" CAMTimer... mcamid: %d, scamid: %d\n", mastercamid, slavecamid);

#if GIGEV_CHANNEL	
	for (i = 0; i < GECP_TRY; i++) {
		if (hgecps) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" s, STOP\n");
			res = GECP_CamTimerMode(hgecps, GVCP_VR_TIMER_CMD_SET_MASTER_STOP);	// Master Timer Stop
		} else {
			res = 1;
		}
		if (res > 0) {
			if (hgecpm) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" m, STOP\n");
				res = GECP_CamTimerMode(hgecpm, GVCP_VR_TIMER_CMD_SET_MASTER_STOP);	// Master Timer Stop
			} else {
				res = 1;
			}
		} else {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__"%d res: %d\n", __LINE__, res);
		}

		if (res > 0) {
			if (hgecps) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" s, S_R_1\n");
			if (pscamif->virtualslavetimermode) {
				res = GECP_CamTimerMode(hgecps, GVCP_VR_TIMER_CMD_SET_VIRTUAL_SLAVE_RUN_1);		// Slave..... VIRTUAL SLAVE...
			} else {
				res = GECP_CamTimerMode(hgecps, GVCP_VR_TIMER_CMD_SET_SLAVE_RUN);					// Slave Timer ready to Run..
			}

			} else {
				res = 1;
			}
		} else {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__"%d res: %d\n", __LINE__, res);
		}

		if (res > 0) {
			if (hgecpm) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" m, M R\n");
				res = GECP_CamTimerMode(hgecpm, GVCP_VR_TIMER_CMD_SET_MASTER_RUN);	// Master Timer Run.

			} else {
				res = 1;
			}
		} else {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__"%d res: %d\n", __LINE__, res);
		} 
		
		if (res > 0) {
			break;
		}
	}
	if (res < 0) {
		res = 0;
	} 

#else
	// TODO: SCAMIF
	res = 1;
#endif
	
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" CAMTimer...  res: %d\n", res);


func_exit:
	return res;
}




I32 static video_fnCB(
		void *pData, I32 nSize, 
		U32 ts_high, U32 ts_low, 
		I32 size_x, I32 size_y, 
		I32 offset_x, I32 offset_y,
		U32 normalbulk, U32 skip, U32 multitude,		// 
		void *pParam)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;
	camif_buffer_info_t  cifb;
	U32 camid;
	U32 windex;

	//--
	res = 0;
	if (pParam == NULL) {
		goto func_exit;
	}

	psc 	= (scamcam_t *) pParam;
	pscamif	= (scamif_t *) psc->hsci;


	if (nSize != (size_x * size_y)) {
		goto func_exit;
	}

	if ((pscamif->lockcode & SCAMIF_LOCK_WRITE) != 0) {
		goto func_exit;
	}
	if (
		(normalbulk == 1) && 
		((pscamif->lockcode & SCAMIF_LOCK_NORMAL_WRITE) != 0)
		) 
	{
			goto func_exit;
	}

	if (
		(normalbulk == 2) && 
		((pscamif->lockcode & SCAMIF_LOCK_BULK_WRITE) != 0)
		) 
	{
			goto func_exit;
	}

	camid = psc->camid;
	memset(&cifb, 0, sizeof(camif_buffer_info_t));

	cifb.state 			= 0;		// discard..
	cifb.ts_h 			= ts_high;
	cifb.ts_l 			= ts_low;
	cifb.frameindex 	= 0;		// discard..

	cifb.cii.width		= size_x;
	cifb.cii.height 	= size_y;
	cifb.cii.offset_x 	= offset_x;
	cifb.cii.offset_y 	= offset_y;
	cifb.cii.skip 		= skip;
	cifb.cii.multitude 	= multitude;

	//-
	cifb.cii.framerate	= pscamif->scc[camid].imginfo.framerate;		
	cifb.cii.gain		= pscamif->scc[camid].imginfo.gain;			
	cifb.cii.exposure	= pscamif->scc[camid].imginfo.exposure;
	cifb.cii.syncdiv	= pscamif->scc[camid].imginfo.syncdiv;


	if (normalbulk == 2 && multitude >= 2) {					// Bulk.. :)
		U32 i;
		U32 d0;
		U08 *ptr;
		U32 w, h;
		U32 sx, sy;
		U32 sizeOne;
		U64 ts64;

		ptr = (U08 *)pData;					// U64 ts64, U16 w, U16 h, U16 sx, U16 sy
		for (i = 0; i < multitude; i++) {
				// U64 ts64, U16 w, U16 h, U16 sx, U16 sy
			ts64 = *(U64 *)ptr;
			ts_high = EXTRACTH(ts64);
			ts_low = EXTRACTL(ts64);

			d0 = *(U32 *)(ptr + 8);
			w = (d0 >> 16) & 0xFFFF;
			h = (d0 >>  0) & 0xFFFF;

			d0 = *(U32 *)(ptr + 12);
			sx = (d0 >> 16) & 0xFFFF;
			sy = (d0 >>  0) & 0xFFFF;


			cifb.ts_h 			= ts_high;
			cifb.ts_l 			= ts_low;

			cifb.cii.width		= w;
			cifb.cii.height 	= h;
			cifb.cii.offset_x 	= sx;
			cifb.cii.offset_y 	= sy;
			cifb.cii.multitude 	= 1;			// :)

			sizeOne = w * h;
			res = scamif_imagebuf_write(
					(HAND) pscamif, 
					camid, 
					normalbulk, 
					&cifb,						// camif_buffer_info_t  *pb, 
					(U08 *)ptr,
					&windex);


			scamif_imagebuf_timetickupdate(
				(HAND) pscamif, 
				camid);

			if (pscamif->hcbfunc != NULL) {
				((SCAMIF_FNCB)(pscamif->hcbfunc))(
				ptr, sizeOne, 
				ts_high, ts_low, 
				w, h, 
				sx, sy,
				normalbulk, skip, cifb.cii.multitude,		// 
				camid,			// !!!!!
				pscamif->userparam);
			}
			ptr += sizeOne;
		}
	} else {
		res = scamif_imagebuf_write(
				(HAND) pscamif, 
				camid, 
				normalbulk, 
				&cifb,						// camif_buffer_info_t  *pb, 
				(U08 *)pData, 
				&windex);
		UNUSED(nSize);

		scamif_imagebuf_timetickupdate(
				(HAND) pscamif, 
				camid);

		if (pscamif->hcbfunc != NULL) {
			((SCAMIF_FNCB)(pscamif->hcbfunc))(
			pData, nSize, 
			ts_high, ts_low, 
			size_x, size_y, 
			offset_x, offset_y,
			normalbulk, skip, multitude,		// 
			camid,			// !!!!!
			pscamif->userparam);
		}
	}


func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      CAM FMC init.
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2016/12/15
 *******************************************************************************/
I32 scamif_fmc_init(HAND h)
{
	I32 res;
	I32 res01[2];
	U32 i;

	scamif_t *pscamif;
	HAND hgecp;


	//0--
	res01[0] = res01[1] = 0;

	pscamif = (scamif_t *) h;


#if GIGEV_CHANNEL
	for (i = 0; i < 2; i++) {
		hgecp = pscamif->scc[i].hgecp;
		res01[i] = GECP_CamFmcInit(hgecp, pscamif->zrot[i], pscamif->autoinit[i]);
	}

	for (i = 0; i < 2; i++) {
		hgecp = pscamif->scc[i].hgecp;
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "#2 Before GECP_SendOnOff()\n");

		GECP_SendOnOff(hgecp, 1);	
	}
#else	
	hgecp = ((scamif_t *)h)->hPCIe;
	for (i = 0; i < 2; i++) {
		res01[i] = HAL_PCIE_InitCam(hgecp, i, pscamif->zrot[i]);		
		if (res01[i] != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s: %d:%d fail res: %d\n", __func__, __LINE__, i, res01[i]);
		}
	}
		
#endif	


	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"res[0]: %d, res[1]: %d\n", res01[0], res01[1]);

	res = 1;
	return res;
}

/*!
 ********************************************************************************
 *	@brief      Virtual slave timer mode..
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *
 *  @param[in]	benable
 *  	        Virtual slave timer mode.  (0: NO, 1: YES)
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2020/0319
 *******************************************************************************/
I32 scamif_camtimer_virtualslavetimer_set(HAND h, U32 benable)
{
	I32 res;
	scamif_t *pscamif;

	//--


	if (h == NULL) {
		res = 0;
	} else {
		pscamif = (scamif_t *) h;

		if (benable) {
			pscamif->virtualslavetimermode = 1;
		} else {
			pscamif->virtualslavetimermode = 0;
		}

		res = 1;
	}
	return res;
}




/*!
 ********************************************************************************
 *	@brief      CAM SYNC update
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              Camera id.
 *  @param[in]	updateme
 *              update me.  (0: NO, 1: YES..  0xFF: update all)
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2016/12/15
 *******************************************************************************/
I32 scamif_image_sync_update(HAND h, U32 camid, U32 updateme)
{
	I32 res;
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

#if GIGEV_CHANNEL

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {
		for (i = 0; i < IMAGEUPDATE_TRY; i++) {
			res = GECP_SyncUpdate(hgecp, updateme);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
	}
#else	
	hgecp = ((scamif_t *)h)->hPCIe;
	if (res) {
		res = HAL_PCIE_SetSyncUpdate(hgecp, camid, updateme);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
			res = 0;
		}
	}		
#endif		

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;

}



/*!
 ********************************************************************************
 *	@brief      Check if HOST IP and cam IP are in same subnet...
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2016/12/15
 *******************************************************************************/
I32 scamif_check_host_cam_subnet(HAND h)
{
#if GIGEV_CHANNEL
	I32 res;
	U32 camcount;
	U32 hostIp;
	U32 camIp;
	U32 subnetmask;
	U32 i;


	scamif_t *pscamif;

	//----
	if (h == NULL) {
		res = 0;
		goto func_exit;
	}

	pscamif = (scamif_t *) h;
	camcount = pscamif->camcount;
	if (camcount == 0) {
		res = 0;
		goto func_exit;
	}

	hostIp 		= pscamif->myip;
	subnetmask 	= pscamif->mysubnetmask;
	if (hostIp == 0 || subnetmask == 0) {
		res = 0;
		goto func_exit;
	}

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" hostIp   : %08x subnetmask: %08x\n", hostIp, subnetmask);
	res = 1;
	for (i = 0; i < camcount; i++) {
		camIp = pscamif->scc[i].camip;
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" camIp[%d]: %08x\n", i, camIp);
		if ((hostIp & subnetmask) !=  (camIp & subnetmask)) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" campare fail..\n");
			// Fail.. 
			res = 0;
			break;
		}
	}
func_exit:
	return res;
#else
	return 1;
#endif
}



/*!
 ********************************************************************************
 *	@brief      hboardinfo read
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[out]	pbuf  
 *  			hboardinfo_t 
 *  @param[in]	len  
 *  			length of data
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0409
 *******************************************************************************/
I32 scamif_hboardinfo_read(HAND h, U32 camid, U32 *pbuf, U32 len)
{
	I32 res;
	U32 i;
	HAND hgecp;
	hboardinfo_t	hbdi;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

#if GIGEV_CHANNEL
	hgecp = ((scamif_t *)h)->scc[camid].hgecp;
#endif

	if (len == 0) {
		len = sizeof(hboardinfo_t);
	} else if (len != sizeof(hboardinfo_t)) {
		res = 0;
		goto func_exit;
	}

#if GIGEV_CHANNEL
	if (res) {
		for (i = 0; i < HBOARDINFO_TRY; i++) {
			res = GECP_hboardinfo_read(hgecp, (U32 *) &hbdi, len);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
	}
#else	
	hgecp = ((scamif_t *)h)->hPCIe;
	if (res) {
		res = HAL_PCIE_ReadInfo(hgecp, camid, CAM_INFO_BOARD, (U32 *) &hbdi, len);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
			res = 0;
		}
	}		
#endif


	if (res > 0) {
		U32 crcvalue;
		crcvalue = cr_crc32(0, (U08 *)&hbdi, sizeof(hboardinfo_t) - sizeof(int));
		if (hbdi.crc == crcvalue) {
			res = 1;
			memcpy(pbuf, &hbdi, sizeof(hboardinfo_t));
		} else {
			res = 0;
		}
	}


	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}

/*!
 ********************************************************************************
 *	@brief      hboardinfo write
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[out]	pbuf  
 *  			hboardinfo_t 
 *  @param[in]	len  
 *  			length of data
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0409
 *******************************************************************************/
I32 scamif_hboardinfo_write(HAND h, U32 camid, U32 *pbuf, U32 len)
{
	I32 res;
	U32 i;
	HAND hgecp;
	hboardinfo_t	hbdi;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

#if GIGEV_CHANNEL
	hgecp = ((scamif_t *)h)->scc[camid].hgecp;
#endif
	if (len == 0) { 
		len = sizeof(hboardinfo_t);
	} else if (len != sizeof(hboardinfo_t)) {
		res = 0;
		goto func_exit;
	}

	if (res) {
		U32 crcvalue;
		memcpy(&hbdi, pbuf, len);
		crcvalue = cr_crc32(0, (U08 *)&hbdi, sizeof(hboardinfo_t) - sizeof(int));

		hbdi.crc = crcvalue;

#if GIGEV_CHANNEL
		for (i = 0; i < HBOARDINFO_TRY; i++) {
			res = GECP_hboardinfo_write(hgecp, (U32 *) &hbdi, len);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
#else	
		hgecp = ((scamif_t *)h)->hPCIe;
		if (res) {
			res = HAL_PCIE_WriteInfo(hgecp, camid, CAM_INFO_BOARD, (U32 *) &hbdi, len);
			if (res != CR_OK) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
				res = 0;
			}
		}		
#endif


	}

	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}


/*!
 ********************************************************************************
 *	@brief      pcinfo read
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[out]	pbuf  
 *  			pcinfocode_t 
 *  @param[in]	len  
 *  			length of data
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0409
 *******************************************************************************/
I32 scamif_pcinfo_read(HAND h, U32 camid, U32 *pbuf, U32 len)
{
	I32 res;
	U32 i;
	HAND hgecp;
	pcinfocode_t	pif;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

#if GIGEV_CHANNEL
	hgecp = ((scamif_t *)h)->scc[camid].hgecp;
#endif

	if (len == 0) {
		len = sizeof(pcinfocode_t);
	} else if (len != sizeof(pcinfocode_t)) {
		res = 0;
		goto func_exit;
	}

	if (res) {
#if GIGEV_CHANNEL		
		for (i = 0; i < PCINFO_TRY; i++) {
			res = GECP_pcinfo_read(hgecp, (U32 *) &pif, len);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}

#else	
		hgecp = ((scamif_t *)h)->hPCIe;
		if (res) {
			res = HAL_PCIE_ReadInfo(hgecp, camid, CAM_INFO_PC, (U32 *) &pif, len);
			if (res != CR_OK) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
				res = 0;
			}
		}		
#endif
	}

	if (res > 0) {
		U32 crcvalue;
		crcvalue = cr_crc32(0, (U08 *)&pif, sizeof(pcinfocode_t) - sizeof(int));
		if (pif.crc == crcvalue) {
			res = 1;
			memcpy(pbuf, &pif, sizeof(pcinfocode_t));
		} else {
			res = 0;
		}
	}


	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}

/*!
 ********************************************************************************
 *	@brief      pcinfo write
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[out]	pbuf  
 *  			pcinfocode_t 
 *  @param[in]	len  
 *  			length of data
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0409
 *******************************************************************************/
I32 scamif_pcinfo_write(HAND h, U32 camid, U32 *pbuf, U32 len)
{
	I32 res;
	U32 i;
	HAND hgecp;
	pcinfocode_t	pif;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

#if GIGEV_CHANNEL
	hgecp = ((scamif_t *)h)->scc[camid].hgecp;
#else

#endif

	if (len == 0) { 
		len = sizeof(pcinfocode_t);
	} else if (len != sizeof(pcinfocode_t)) {
		res = 0;
		goto func_exit;
	}

	if (res) {
		U32 crcvalue;
		memcpy(&pif, pbuf, len);
		crcvalue = cr_crc32(0, (U08 *)&pif, sizeof(pcinfocode_t) - sizeof(int));

		pif.crc = crcvalue;

#if GIGEV_CHANNEL
		for (i = 0; i < PCINFO_TRY; i++) {
			res = GECP_pcinfo_write(hgecp, (U32 *) &pif, len);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}

#else	
		hgecp = ((scamif_t *)h)->hPCIe;
		if (res) {
			res = HAL_PCIE_WriteInfo(hgecp, camid, CAM_INFO_PC, (U32 *) &pif, len);
			if (res != CR_OK) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
				res = 0;
			}
		}		
#endif
	}

	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}





/*!
 ********************************************************************************
 *	@brief      cpusid read
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[out]	pbuf  
 *  			cpusid_t 
 *  @param[in]	len  
 *  			length of data
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0409
 *******************************************************************************/
I32 scamif_cpusid_read(HAND h, U32 camid, U32 *pbuf, U32 len)
{
	I32 res;
	U32 i;	
	HAND hgecp;
	U32 psi[132];

	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

#if GIGEV_CHANNEL
	hgecp = ((scamif_t *)h)->scc[camid].hgecp;
#endif

	if (len == 0) { 
		len = sizeof(cpusid_t);
	} else if (len == sizeof(cpusid_t)) {
		// good.
	} else if (len == sizeof(cpusid_t) + sizeof(int)) {
		// with CRC value..
	} else {
		res = 0;
		goto func_exit;
	}

	if (res) {
#if GIGEV_CHANNEL		
		for (i = 0; i < CPUSID_TRY; i++) {
			res = GECP_cpusid_read(hgecp, (U32 *) &psi[0], len);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				memcpy(pbuf, &psi[0], len);
				break;
			}
		}
#else	
		hgecp = ((scamif_t *)h)->hPCIe;
		if (res) {
			res = HAL_PCIE_ReadInfo(hgecp, camid, CAM_INFO_CPUSID,  (U32 *) &psi[0], len);
			if (res != CR_OK) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
				res = 0;
			}
		}		
#endif

	}

	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}

/*!
 ********************************************************************************
 *	@brief      cpusid write
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[out]	pbuf  
 *  			cpusid_t 
 *  @param[in]	len  
 *  			length of data
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0409
 *******************************************************************************/
I32 scamif_cpusid_write(HAND h, U32 camid, U32 *pbuf, U32 len)
{
	I32 res;
	U32 i;
	HAND hgecp;
	U32 psi[132];

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}

#if GIGEV_CHANNEL
	hgecp = ((scamif_t *)h)->scc[camid].hgecp;
#else

#endif

	if (len == 0) { 
		len = sizeof(cpusid_t);
	} else if (len == sizeof(cpusid_t)) {
		// good.
	} else if (len == sizeof(cpusid_t) + sizeof(int)) {
		// with CRC value..
	} else {
		res = 0;
		goto func_exit;
	}
	

	if (res) {
		memcpy(&psi[0], pbuf, len);
#if GIGEV_CHANNEL		
		for (i = 0; i < CPUSID_TRY; i++) {
			res = GECP_cpusid_write(hgecp, (U32 *) &psi[0], len);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
#else	
		hgecp = ((scamif_t *)h)->hPCIe;
		if (res) {
			res = HAL_PCIE_WriteInfo(hgecp, camid, CAM_INFO_CPUSID,	(U32 *) &psi[0], len);
			if (res != CR_OK) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
				res = 0;
			}
		}		
#endif

	}

	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Board Serial read
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[out]	pbuf  
 *  			CPU board serial. U08 [32]
 *  @param[in]	len  
 *  			length of data
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0502
 *******************************************************************************/
I32 scamif_cpubdserial_read(HAND h, U32 camid, U32 *pbuf, U32 len)
{
	I32 res;
	U32 i;
	HAND hgecp;
	U32 psi[132];

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}
#if GIGEV_CHANNEL
	hgecp = ((scamif_t *)h)->scc[camid].hgecp;
#endif

	if (len == 0) { 
		len = 32;
	} else if (len > 32) {
		res = 0;
		goto func_exit;
	}

	if (res) {
#if GIGEV_CHANNEL		
		for (i = 0; i < CPUSID_TRY; i++) {
			res = GECP_cpubdserial_read(hgecp, (U32 *) &psi[0], len);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				memcpy(pbuf, &psi[0], len);
				break;
			}
		}
#else	
		hgecp = ((scamif_t *)h)->hPCIe;
		if (res) {
			res = HAL_PCIE_ReadInfo(hgecp, camid, CAM_INFO_CPUBDSERIAL,	(U32 *) &psi[0], len);
			if (res != CR_OK) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
				res = 0;
			}
		}		
#endif

	}

	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}


/*!
 ********************************************************************************
 *	@brief      runcode read
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[out]	pbuf  
 *  			runcode_t 
 *  @param[in]	len  
 *  			length of data
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0409
 *******************************************************************************/
I32 scamif_runcode_read(HAND h, U32 camid, U32 *pbuf, U32 len)
{
	I32 res;
	U32 i;

	HAND hgecp;
	runcode_t	rcd;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}

#if GIGEV_CHANNEL
	hgecp = ((scamif_t *)h)->scc[camid].hgecp;
#endif

	if (len == 0) {
		len = sizeof(runcode_t);
	} else if (len != sizeof(runcode_t)) {
		res = 0;
		goto func_exit;
	}

	if (res) {
#if GIGEV_CHANNEL		
		for (i = 0; i < RUNCODE_TRY; i++) {
			res = GECP_runcode_read(hgecp, (U32 *) &rcd, len);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
#else	
		hgecp = ((scamif_t *)h)->hPCIe;
		if (res) {
			res = HAL_PCIE_ReadInfo(hgecp, camid, CAM_INFO_RUNCODE, (U32 *) &rcd, len);
			if (res != CR_OK) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
				res = 0;
			}
		}		
#endif
	}

	if (res > 0) {
		U32 crcvalue;
		crcvalue = cr_crc32(0, (U08 *)&rcd, sizeof(runcode_t) - sizeof(int));
		if (rcd.crc == crcvalue) {
			res = 1;
			memcpy(pbuf, &rcd, sizeof(runcode_t));
		} else {
			res = 0;
		}
	}


	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}

/*!
 ********************************************************************************
 *	@brief      runcode write
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[out]	pbuf  
 *  			runcode_t 
 *  @param[in]	len  
 *  			length of data
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0409
 *******************************************************************************/
I32 scamif_runcode_write(HAND h, U32 camid, U32 *pbuf, U32 len)
{
	I32 res;
	U32 i;
	HAND hgecp;
	runcode_t	rcd;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}

#if GIGEV_CHANNEL
	hgecp = ((scamif_t *)h)->scc[camid].hgecp;
#endif

	if (len == 0) { 
		len = sizeof(runcode_t);
	} else if (len != sizeof(runcode_t)) {
		res = 0;
		goto func_exit;
	}

	if (res) {
		U32 crcvalue;
		memcpy(&rcd, pbuf, len);
		crcvalue = cr_crc32(0, (U08 *)&rcd, sizeof(runcode_t) - sizeof(int));
		rcd.crc = crcvalue;
#if GIGEV_CHANNEL		
		for (i = 0; i < RUNCODE_TRY; i++) {
			res = GECP_runcode_write(hgecp, (U32 *) &rcd, len);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
#else	
		hgecp = ((scamif_t *)h)->hPCIe;
		if (res) {
			res = HAL_PCIE_WriteInfo(hgecp, camid, CAM_INFO_RUNCODE, (U32 *) &rcd, len);
			if (res != CR_OK) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
				res = 0;
			}
		}		
#endif

	}

	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}

/*!
 ********************************************************************************
 *	@brief      bddatarsvd read
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[out]	pbuf  
 *  			boarddatarsvd_t 
 *  @param[in]	len  
 *  			length of data
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0508
 *******************************************************************************/
I32 scamif_bddatarsvd_read(HAND h, U32 camid, U32 *pbuf, U32 len)
{
	I32 res;
	U32 i;
	HAND hgecp;
	boarddatarsvd_t	bdrsvd;

	//------
	memset(pbuf, 0, len);
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	
#if GIGEV_CHANNEL
	hgecp = ((scamif_t *)h)->scc[camid].hgecp;
#endif

	if (len == 0) {
		len = sizeof(boarddatarsvd_t);
	} else if (len != sizeof(boarddatarsvd_t)) {
		res = 0;
		goto func_exit;
	}

	if (res) {
		memset(&bdrsvd, 0, sizeof(boarddatarsvd_t));
#if GIGEV_CHANNEL		
		for (i = 0; i < RUNCODE_TRY; i++) {
			res = GECP_bddatarsvd_read(hgecp, (U32 *) &bdrsvd, len);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
#else	
		hgecp = ((scamif_t *)h)->hPCIe;
		if (res) {
			res = HAL_PCIE_ReadInfo(hgecp, camid, CAM_INFO_BD_DATA_RSVD, (U32 *) &bdrsvd, len);
			if (res != CR_OK) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
				res = 0;
			}
		}		
#endif

		if (res > 0) {
			memcpy(pbuf, &bdrsvd, sizeof(boarddatarsvd_t));
		}
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}

/*!
 ********************************************************************************
 *	@brief      bddatarsvd write
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[out]	pbuf  
 *  			runcode_t 
 *  @param[in]	len  
 *  			length of data
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0508
 *******************************************************************************/
I32 scamif_bddatarsvd_write(HAND h, U32 camid, U32 *pbuf, U32 len)
{
	I32 res;
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	
#if GIGEV_CHANNEL
	hgecp = ((scamif_t *)h)->scc[camid].hgecp;
#endif

	if (len == 0) { 
		len = sizeof(boarddatarsvd_t);
	} else if (len != sizeof(boarddatarsvd_t)) {
		res = 0;
		goto func_exit;
	}

	if (res) {
#if GIGEV_CHANNEL		
		for (i = 0; i < RUNCODE_TRY; i++) {
			res = GECP_bddatarsvd_write(hgecp, (U32 *) pbuf, len);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" %d:%d res: %d\n", __LINE__, i, res);
			if (res > 0) {
				break;
			}
		}
#else	
		hgecp = ((scamif_t *)h)->hPCIe;
		if (res) {
			res = HAL_PCIE_WriteInfo(hgecp, camid, CAM_INFO_BD_DATA_RSVD,  (U32 *) pbuf, len);
			if (res != CR_OK) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  camid [%d] fail.  res: %d\n", __func__, camid, res);	
				res = 0;
			}
		}		
#endif
	}

	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}



/*!
 ********************************************************************************
 *	@brief      Security infom update
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0512
 *******************************************************************************/
I32 scamif_security_update(HAND h)
{
	I32 res;
	U32 runcode;
	scamif_t *pscamif;
	hboardinfo_t	hbdi;
	U08	buf[128];

	U32 iter;
	//--
	if (h == NULL) {
		res = 0;
		goto func_exit;
	}
	pscamif = (scamif_t *) h;



	res = 0;
	memset(pscamif->swspec, 0, (64+4));
	memset(pscamif->barcode, 0, 64);

	for (iter = 0; iter < SECURITY_UPDATE; iter++) {
		res = iana_security_runcode_check(pscamif->hsec, &runcode);
		if (res) {
			pscamif->runcode = runcode;
		} 

		res = scamif_bddatarsvd_read(h, 0, (U32 *) buf, 92);
		if (res) {
			memcpy(pscamif->swspec, buf, (64+4));
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" swspec success.\n");
		} else {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" swspec read..%d fail %d\n", iter, res);

			continue;
		}

		res = scamif_hboardinfo_read(h, 0, (U32 *)&hbdi, 0);
		if (res) {
			memcpy(pscamif->barcode,  hbdi.barcode, 32);
			pscamif->barcode[31] = 0;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" barcode read success. %s\n", pscamif->barcode);
		} else {

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" barcode read..%d fail %d\n", iter, res);
			continue;
		}
		break;
	}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" -------------- update result: %d\n", res);

	if (res > 0) 
	{
		int i;
		U32 *pbuf;

		pbuf = (U32 *)pscamif->swspec;
		for (i = 0; i < (64+4)/4; i++) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%08x ", pbuf[i]);
		}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");


		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "barcode: %s\n", pscamif->barcode);
	} else {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" swspec, barcode read. fail\n");
		memset(pscamif->swspec, 0, (64+4));
		memset(pscamif->barcode, 0, 64);
	}

func_exit:
	return res;
}

I32 scamif_security_swspec(HAND h, U08 swspec[64+4])
{
	I32 res;
	scamif_t *pscamif;

	//--
	if (h == NULL) {
		res = 0;
		goto func_exit;
	}
	pscamif = (scamif_t *) h;

	memcpy(swspec, pscamif->swspec, (64+4));

	res = 1;

func_exit:
	return res;
}

I32 scamif_security_barcode(HAND h, U08 barcode[64])
{
	I32 res;
	scamif_t *pscamif;

	//--
	if (h == NULL) {
		res = 0;
		goto func_exit;
	}
	pscamif = (scamif_t *) h;

	memcpy(barcode, pscamif->barcode, 64);

	res = 1;

func_exit:
	return res;

}

I32 scamif_security_runcode(HAND h, U32 *pruncode)
{
	I32 res;
	scamif_t *pscamif;

	//--
	if (h == NULL) {
		res = 0;
		goto func_exit;
	}
	pscamif = (scamif_t *) h;

	*pruncode = pscamif->runcode;

	res = 1;

func_exit:
	return res;

}


/*!
 ********************************************************************************
 *	@brief      z3 BOARD memory write
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	addr
 *              Address
 *  @param[in]	count  
 *  			data length. Byte length
 *  @param[in]	data  
 *  			data buffer
 *  @param[out]	status  
 *  			operation result status
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0428
 *******************************************************************************/
I32 scamif_z3_mem_write(HAND h, U32 camid, U32 addr, U32 count, U32 *data, U32 *status)
{
	I32 res = 1;
#if GIGEV_CHANNEL	
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {
		for (i = 0; i < Z3_CMD_TRY; i++) {
			res = GECP_memWrite(hgecp, addr, count, data, status);
			if (res > 0) {
				break;
			}
		}
	}
	if (res < 0) {
		res = 0;
	}

func_exit:
#endif	
//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}


/*!
 ********************************************************************************
 *	@brief      z3 BOARD memory read
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	addr
 *              Address
 *  @param[in]	count  
 *  			data length. Byte length
 *  @param[out]	data  
 *  			data buffer
 *  @param[out]	status  
 *  			operation result status
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0428
 *******************************************************************************/
I32 scamif_z3_mem_read(HAND h, U32 camid, U32 addr, U32 count, U32 *data, U32 *pstatus)
{
	I32 res=1;
#if GIGEV_CHANNEL	
	U32 i;
	HAND hgecp;
	U32 status0;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	status0 = (U32)-1;
	if (res) {

//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "hgcep: %p addr: %p count: %d data: %p\n",
//			(int)hgecp, (int)addr, count, (int)data);

		for (i = 0; i < Z3_CMD_TRY; i++) {
			res = GECP_memRead(hgecp, addr, count, data, &status0);
			if (res > 0) {
				break;
			}
		}
	}

	if (pstatus) {
		*pstatus = status0;
	}
	
	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
#endif		
	return res;
}



/*!
 ********************************************************************************
 *	@brief      z3 BOARD memory checksum
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	addr
 *              Address
 *  @param[in]	count  
 *  			data length. Byte length
 *  @param[in]	count  
 *  			data length. Byte length
 *  @param[in]	checksumtype  
 *  			checksum type.  0: CRC,   other: NOT YET.. :P
 *  @param[out]	pchecksum  
 *  			checksum result
 *  @param[out]	pstatus  
 *  			operation result status
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0501
 *******************************************************************************/
I32 scamif_z3_mem_checksum(HAND h, U32 camid, U32 addr, U32 count, U32 checksumtype, U32 *pchecksum, U32 *pstatus)
{
	I32 res=1;
#if GIGEV_CHANNEL	
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {

		for (i = 0; i < Z3_CMD_TRY; i++) {
			res = GECP_cache_checksum(hgecp, addr, count, checksumtype, pchecksum, pstatus);
			if (res > 0) {
				break;
			}
			cr_sleep(10);
		}
	}

	if (res < 0) {
		res = 0;
	}

func_exit:
//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
#endif
	return res;
}

/*!
 ********************************************************************************
 *	@brief      get z3 BOARD version
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[out]	pversion  
 *  			version
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0428
 *******************************************************************************/
I32 scamif_z3_version_get(HAND h, U32 camid, char *pversion /*U08 pversion[64]*/)
{
	I32 res=1;
#if GIGEV_CHANNEL	
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {

		for (i = 0; i < Z3_CMD_TRY; i++) {
			res = GECP_version_get(hgecp, (char *)pversion);
			if (res > 0) {
				break;
			}
			cr_sleep(50);
		}
	}

	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
#endif	
	return res;
}


/*!
 ********************************************************************************
 *	@brief      z3 cache update 
 *              FLASH to Cache memory
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	addr
 *              address of cache operation
 *  @param[in]	len
 *              len of cache operation
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0428
 *******************************************************************************/
I32 scamif_z3_cache_update(HAND h, U32 camid, U32 addr, U32 len)
{
	I32 res=1;
#if GIGEV_CHANNEL	
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {

		for (i = 0; i < Z3_CMD_TRY; i++) {
			res = GECP_cache_update(hgecp, addr, len);
			if (res > 0) {
				break;
			}
		}
	}

	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
#endif	
	return res;
}


/*!
 ********************************************************************************
 *	@brief      z3 cache commit 
 *              Cache memory to FLASH
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	addr
 *              address of cache operation
 *  @param[in]	len
 *              len of cache operation
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0428
 *******************************************************************************/
I32 scamif_z3_cache_commit(HAND h, U32 camid, U32 addr, U32 len)
{
	I32 res=1;
#if GIGEV_CHANNEL	
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {

		for (i = 0; i < Z3_CMD_TRY; i++) {
			res = GECP_cache_commit(hgecp, addr, len);
			if (res > 0) {
				break;
			}
		}
	}

	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
#endif	
	return res;
}

/*!
 ********************************************************************************
 *	@brief      z3 Flash erase
 *              Flash memory erase
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	addr
 *              address of erase operation
 *  @param[in]	len
 *              len of erase operation
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0428
 *******************************************************************************/
I32 scamif_z3_flash_erase(HAND h, U32 camid, U32 addr, U32 len)
{
	I32 res=1;
#if GIGEV_CHANNEL	
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {

		for (i = 0; i < Z3_CMD_TRY; i++) {
			res = GECP_flash_erase(hgecp, addr, len);
			if (res > 0) {
				break;
			}
		}
	}

	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
#endif	
	return res;
}


/*!
 ********************************************************************************
 *	@brief      z3 Reboot
 *              rebooting z3 
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0428
 *******************************************************************************/
I32 scamif_z3_reboot(HAND h, U32 camid)
{
	I32 res=1;
#if GIGEV_CHANNEL	
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {

		for (i = 0; i < Z3_CMD_TRY; i++) {
			res = GECP_reboot(hgecp);
			if (res > 0) {
				break;
			}
		}
	}

	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
#endif	
	return res;
}


/*!
 ********************************************************************************
 *	@brief      z3 Gold Recover
 *              Disable Silver FW and make GOLD FW work.
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2019/0428
 *******************************************************************************/
I32 scamif_z3_recover_gold(HAND h, U32 camid)
{
	I32 res=1;
#if GIGEV_CHANNEL	
	U32 i;
	HAND hgecp;

	//------
	res = check_params(h, camid, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	

	hgecp = ((scamif_t *)h)->scc[camid].hgecp;

	if (res) {

		for (i = 0; i < Z3_CMD_TRY; i++) {
			res = GECP_gold_recover(hgecp);
			if (res > 0) {
				break;
			}
		}
	}

	if (res < 0) {
		res = 0;
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
#endif	
	return res;
}


#if defined(PCIE_CHANNEL)
/*!
 ********************************************************************************
 *	@brief      set running stage
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	state - CAM run state
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	   
 *  @date       
 *******************************************************************************/
I32 HAL_SCAMIF_SetRunState(HAND h, U32 state)
{
	I32 res;
	HAND hDevice;

	//------
	res = check_params(h, 0, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	
	
	if (res) {		
		hDevice = ((scamif_t *)h)->hPCIe;
		res = HAL_PCIE_SetRunState(hDevice, (CAM_Run_State_T)state);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s:  HAL_PCIE_SetRunState fail.  res: %d\n", __func__,  res);	
			res = 0;
		}		
	}
	
func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      set new ROI
 *                
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	x, y, w, h
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	   
 *  @date       
 *******************************************************************************/
I32 HAL_SCAMIF_UpdateRoi(HAND h, U32 camid, U32 x, U32 y, U32 width, U32 height )
{
	I32 res;
	HAND hDevice;
	I32 offset, size;

	//------
	res = check_params(h, 0, CR_TRUE);
	if (res <= 0) {
		goto func_exit;
	}	
	
	if (res) {		
		hDevice = ((scamif_t *)h)->hPCIe;
		offset = x | (y<<16);
		res = HAL_PCIE_UpdateImageParam(hDevice, camid, ROI_OFFSET_OFFSET, offset);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s: %d HAL_PCIE_UpdateImageParam fail.  res: %d\n", __func__, __LINE__,  res);	
			res = 0;
		}
		size = width | (height<<16);		
		res = HAL_PCIE_UpdateImageParam(hDevice, camid, ROI_SIZE_OFFSET, size);
		if (res != CR_OK) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR,"%s: %d HAL_PCIE_UpdateImageParam fail.  res: %d\n", __func__, __LINE__,  res);	
			res = 0;
		}		
	}
	
func_exit:
	return res;

}

#endif

#if defined (__cplusplus)
}
#endif

