/*!
 *******************************************************************************
                                                                                
                    CREATZ SCAM IF
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   scamif_main.cpp
	 @brief  SCAMFIF main thread
	 @author Original: by yhsuk
	 @date   2015/11/30 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/



/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"

#include "crgev.h"
#include "scamif.h"
#include "scamif_main.h"


#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
//THREAD_PRIORITY_NORMAL
//THREAD_PRIORITY_ABOVE_NORMAL
//THREAD_PRIORITY_HIGHEST
//THREAD_PRIORITY_TIME_CRITICAL


#if defined(_WIN32)
#define		THREADPRIORITY_SCAMIF			THREAD_PRIORITY_TIME_CRITICAL
#else
#define		THREADPRIORITY_SCAMIF			MAX_USER_PRIORITY
#endif


/*----------------------------------------------------------------------------
 *	Description	: defines datatype
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/



/*!
 ********************************************************************************
 *	@brief      SCAMIF thead main function
 *
 *  @return		trivial..
 *
 *	@author	    yhsuk
 *  @date       2015/11/30
 *******************************************************************************/
void *scamif_mainfunc(void *startcontext)
{
	scamif_t *pscamif;	
	cr_thread_t *pt;
	
	volatile U32	loop;
	U32				u0;
	U32 camid;

	//--------------------------------------- 
	// Check handle of Sensor
	pscamif = (scamif_t *)startcontext;
	if (pscamif == NULL) {
		// TODO... what?
		goto func_exit;
	}


	//--------------------------------------- 
	// Change state..
	pt = (cr_thread_t *) pscamif->hth;
	pt->ustate = CR_THREAD_STATE_INITED;


	cr_thread_setpriority(pt->hthread, THREADPRIORITY_SCAMIF);

	//---------------------------------------
	pt->ustate = CR_THREAD_STATE_RUN;
	loop = 1;

	//---------------------------------------
	pscamif->camcount = 0;
	pscamif->mastercamid = -1;

	//-----------------------------------------
	//pscamif->act_vrinitfmc	= 0;					// NO re-init
	for(camid = 0; camid < MAXCAMCOUNT; camid++) {
		pscamif->act_vrinitfmc[camid]	= 1;					// re-init
		pscamif->zrot[camid]			= 1;					// zrot! 
		//pscamif->zrot			= 0;					// nrot! 
		pscamif->autoinit[camid]		= 1;					// Auto init in SCAM-side
	}

	//pscamif->zrot = 0;							// nrot! 


	// get my ip..

	while(loop) {
#define CHECK_PERIOD	10				// 100 Hz
		u0 = cr_event_wait (pt->hevent, CHECK_PERIOD);
		if (pt->ustate == CR_THREAD_STATE_NEEDSTOP) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" I would be killed...\n");
			break;
		}
	}

	pt->ustate = CR_THREAD_STATE_STOPPING;
	//--------------------------------------- 

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"--\n");
	return 0;
}


#if defined (__cplusplus)
}
#endif

