/*!
 *******************************************************************************
                                                                                
                    CREATZ SCAM IF
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   scamif.cpp
	 @brief  SCAMFIF
	 @author Original: by yhsuk
	 @date   2015/11/30 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"

#include "crgev.h"
#include "scamif.h"
#include "scamif_main.h"
#include "scamif_buffer.h"
#include "iana_security.h"
#include <cstring>
/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define SCAMIF_WAIT	1000

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
void camimageinfo_default(scamif_t *pscamif);



/*!
 ********************************************************************************
 *	@brief      Create SCAMIF module. Deprecated.
 *							Call scamif_create2 with right-side work mode
 *
 *  @param[in]	hcbfunc
 *              SCAMIF callback function for new image reception
 *  @param[in]	workmode
 *              Camera mode. Currently CAMWORKMODE_SENSING supported only
 *  @param[in]	userparam
 *              User parameter for callback function
 * 
 *  @return		SCAMIF module handle
 *
 *	@author	    yhsuk
 *  @date       2015/11/30
 *******************************************************************************/
HAND scamif_create(HAND hcbfunc, U32 workmode, PARAM_T userparam)
{
	HAND res;

	res = scamif_create2(hcbfunc, workmode, 0, userparam);				// RIGHT-SIDED.. :P

	return res;
}


/*!
 ********************************************************************************
 *	@brief      Create SCAMIF module.
 *							Call scamif_create3 with single channel 
 *
 *  @param[in]	hcbfunc
 *              SCAMIF callback function for new image reception
 *  @param[in]	workmode
 *              Camera mode
 *  @param[in]	right0left1
 *              right-side(0) or left-side(1)
 *  @param[in]	userparam
 *              User parameter for callback function
 * 
 *  @return		SCAMIF module handle
 *
 *	@author	    yhsuk
 *  @date       2015/11/30
 *******************************************************************************/
HAND scamif_create2(HAND hcbfunc, U32 workmode, U32 right0left1, PARAM_T userparam)
{

	HAND res;

	res = scamif_create3(hcbfunc, workmode, right0left1,
		0, // streaming_channel_mode
		userparam);

	return res;
}

/*!
 ********************************************************************************
 *	@brief      Create SCAMIF module.
 *
 *  @param[in]	hcbfunc
 *              SCAMIF callback function for new image reception
 *  @param[in]	workmode
 *              Camera mode
 *  @param[in]	right0left1
 *              right-side(0) or left-side(1)
 *  @param[in]	streaming_channel_mode
 *              streaming mode - 0: use single channel	1: use multi channel.
 *  @param[in]	userparam
 *              User parameter for callback function
 * 
 *  @return		SCAMIF module handle
 *
 *	@author	    yhsuk
 *  @date       2015/11/30
 *******************************************************************************/
HAND scamif_create3(HAND hcbfunc, U32 workmode, U32 right0left1, U32 streaming_channel_mode, PARAM_T userparam)
{
	U32 i;
	scamif_t *pscamif;

	pscamif = (scamif_t *) malloc(sizeof(scamif_t));
	if (pscamif == NULL) {
		goto func_exit;
	}
	//-------------
	memset(pscamif, 0, sizeof(scamif_t));
	pscamif->hmutex = cr_mutex_create();

	pscamif->state = SCAMIF_STATE_CREATED;
	pscamif->workmode = workmode;
	pscamif->userparam = userparam;

	pscamif->hcbfunc = (HAND) hcbfunc;


	pscamif->allmastermode = 0;
	pscamif->lockcode = SCAMIF_LOCK_NULL;				// default lock is NULL.. :P

	pscamif->right0left1 = right0left1;

	pscamif->streaming_channel_mode = streaming_channel_mode;

	for (i = 0; i < MAXCAMCOUNT; i++) {
		scamcam_t *psc;

		psc = &pscamif->scc[i];
		psc->state = SCAMIF_STATE_NULL;
		psc->connected = 0;
		psc->timetick = 0;
		psc->nb.hmutex = cr_mutex_create();
		psc->bb.hmutex = cr_mutex_create();
	}

	scamif_datamem_create(pscamif);

//	pscamif->hsec = iana_security_create((HAND)pscamif);
	pscamif->hsec = NULL;
	pscamif->runcode = 0;

	pscamif->virtualslavetimermode = 0;				// DEFAULT: NO virtual slave timer mode..:P
	//-------------
//	camimageinfo_default(pscamif);
//	cambuffer_clear(pscamif);

	//------ Make module thread.
	{
		cr_thread_t *pt;
		
		pt = (cr_thread_t *) malloc(sizeof(cr_thread_t));
		if (pt == NULL) {
			// TODO...WHAT?
			goto func_exit;
		}
		memset(pt, 0, sizeof(cr_thread_t));

		pscamif->hth = (HAND) pt;					// Preserve Thread handle.

		pt->ustate = CR_THREAD_STATE_NULL;
		pt->hevent = cr_event_create();
		pt->hthread = (cr_thread_t *) cr_thread_create(scamif_mainfunc, (HAND)pscamif);

		if (pt->hthread == NULL) {
			goto func_exit;
		}

		for (i = 0; i < SCAMIF_WAIT; i++) {
			if (
					pt->ustate == CR_THREAD_STATE_RUN 
					|| pt->ustate == CR_THREAD_STATE_STOPPING) 
			{
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_INFO, "scamif thread create done!\n");
				break;
			}
			cr_sleep(10);
		}

		if ( pt->ustate == CR_THREAD_STATE_RUN || pt->ustate == CR_THREAD_STATE_STOPPING)  {
			//good.
		} else {
			// bad
		}
	}
	
	pscamif->state = SCAMIF_STATE_INITED;

	pscamif->hsec = iana_security_create((HAND)pscamif);				// 20190512, yhsuk.

func_exit:
	return (HAND) pscamif;
}

/*!
 ********************************************************************************
 *	@brief      Delete SCAMIF module
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @param[in]	lockcode
 *              lock code.
 *  @return		0: bad, 1: Good.
 *
 *	@author	    yhsuk
 *  @date       2018/08/24
 *******************************************************************************/
I32 scamif_lock(HAND h, U32 lockcode) 
{
	I32 res;
	scamif_t *pscamif;

	//--
	if (h == NULL) {
		res = 0;
		goto func_exit;
	}

	pscamif = (scamif_t *) h;
	pscamif->lockcode = lockcode;

	res = 1;

func_exit:
	return res;

}
/*!
 ********************************************************************************
 *	@brief      Delete SCAMIF module
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @return		Terminate result. 
 *
 *	@author	    yhsuk
 *  @date       2012/11/19
 *******************************************************************************/
I32 scamif_delete(HAND h) 
{
	I32 res;
	U32 i;
	scamif_t *pscamif;
	cr_thread_t *pt;
	
	//---
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++ (h: %p)\n",  h);
	res = CR_OK;
	if (h) {
		pscamif = (scamif_t *) h;
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #1\n");
		scamif_cam_stop(h);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #2\n");

		pt = (cr_thread_t *) pscamif->hth;
		if (pt == NULL) {
			// what?
			goto func_exit;
		} else {
			pt->ustate = CR_THREAD_STATE_NEEDSTOP;		// You shall die...
			cr_event_set(pt->hevent);
			
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #3\n");
			cr_thread_join(pt->hthread);					// Wait for termincation..

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #4\n");
			cr_event_delete(pt->hevent);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #5\n");

			free(pt);
			pscamif->hth = NULL;

			cr_mutex_delete(pscamif->hmutex);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #6\n");
		}

		for (i = 0; i < MAXCAMCOUNT; i++) {
			scamcam_t *psc;

			psc = &pscamif->scc[i];
			psc->connected = 0;
			psc->timetick = 0;
			cr_mutex_delete(psc->nb.hmutex);
			cr_mutex_delete(psc->bb.hmutex);
		}

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #7\n");
		scamif_datamem_release(pscamif);

		iana_security_delete(pscamif->hsec);			// 20190512

		free(pscamif);

		// free(h);
	} else {
		// what!
	}

	res = CR_OK;
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"--\n");

func_exit:
	return res;
}



/*!
 ********************************************************************************
 *	@brief      Default value of CAM Image info 
 *
 *  @param[in]	h
 *              SCAMIF module handle
 *  @return		none
 *
 *	@author	    yhsuk
 *  @date       2012/12/01
 *******************************************************************************/
void scamcam_default(scamif_t *pscamif)
{
	U32 i;
	scamcam_t *psc;
	camimageinfo_t *pimi;
	camimageinfo_t *pimi_s;

	for (i = 0; i < NUM_CAM; i++) {

		//---
		psc = &pscamif->scc[i];

		psc->camid 			= i;					// My camid
		psc->state 			= SCAMCAMSTATE_INIT;
		psc->masterslave 	= SCAMIF_MASTER;		// Default
		psc->camip			= 0;
		psc->gvspport		= DEF_GVSP_PORT(i);
		psc->gvcpport		= DEF_GVCP_CAMPORT;

//		psc->hgev			= NULL;
		psc->hgecp			= NULL;
		psc->hsci			= (HAND) pscamif;		// Parent..

		psc->needInitBuffer = 1;

		psc->needCamimageInfoUpdate = 1;

		//---
		pimi 			= &psc->imginfo;
		pimi_s 			= &psc->imginfo_shadow;

		pimi->width 	= DEF_WIDTH;
		pimi->height 	= DEF_HEIGHT;
		pimi->offset_x 	= 0;
		pimi->offset_x 	= 0;

		pimi->framerate	= DEF_FRAMERATE;
		pimi->gain		= DEF_GAIN;
		pimi->exposure	= DEF_EXPOSURE;

		pimi->skip		= 1;		// 0: STOP, 1: send all frame, 2: skip 1 frame, 3: skip 2 frames.....
		pimi->multitude	= 1;
		pimi->syncdiv	= 1;

		memcpy(pimi_s, pimi, sizeof(camimageinfo_t));

		//---
		pimi 			= &psc->imginfo_BULK;
		pimi_s 			= &psc->imginfo_BULK_shadow;

		pimi->width 	= DEF_WIDTH;
		pimi->height 	= DEF_HEIGHT;
		pimi->offset_x 	= 0;
		pimi->offset_x 	= 0;

		pimi->framerate	= DEF_FRAMERATE;
		pimi->gain		= DEF_GAIN;
		pimi->exposure	= DEF_EXPOSURE;

		pimi->skip		= 1;		// 0: STOP, 1: send all frame, 2: skip 1 frame, 3: skip 2 frames.....
		pimi->multitude	= 1;
		pimi->syncdiv	= 1;

		memcpy(pimi_s, pimi, sizeof(camimageinfo_t));
	}
}

//	cambuffer_clear(pscamif);



#if defined (__cplusplus)
}
#endif


