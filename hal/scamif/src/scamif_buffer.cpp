/*!
 *******************************************************************************
                                                                                
                    CREATZ SCAM IF buffer management
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   scamif_buffer.cpp
	 @brief  SCAMFIF buffer management
	 @author by yhsuk
	 @date   2015/12/01 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"

#include "crgev.h"
#include "scamif.h"
#include "scamif_buffer.h"

#if defined(PCIE_CHANNEL)
#include "pcie.h"
#endif

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

#define MUTEXTIMEOUT_INIT	1000
#define MUTEXTIMEOUT_READ	1000
#define MUTEXTIMEOUT_WRITE_RAW	1000

#define INDEXCOUNTMULT	10

#define MAXTS64MDIFF		(0x1000)
#define MUTEXTIMEOUT_RANDOMACCESS	1000

#define ZROT_BARNOISE_STARTX	136
#define ZROT_BARNOISE_ENDX		143




/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

typedef struct _scamif_datamem_manager {
	I32 refcount;
	U08 *nbmem[MAXCAMCOUNT];
	U08 *bbmem[MAXCAMCOUNT];
} scamif_datamem_manager_t;

enum {
	SCAMIF_INDEX_DEFAULT = 0,
	SCAMIF_INDEX_SIGNED,		
	SCAMIF_INDEX_DELTA,	
	SCAMIF_INDEX_MAX_NUM	
};

typedef struct {
	U32  index_mode;
    union {
        U32  index;
        I32  signed_index;
    };
    I32  index_delta;	
} Trim_Index_t;



/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/
static volatile HAND s_sdm_hmutex = NULL;
static volatile scamif_datamem_manager_t *s_psdm;

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
I32 scamif_imagebuf_updatets64m(HAND hscamif, U32 camid, camif_buffer_info_t  *pb, U08 *img);



/*----------------------------------------------------------------------------
 *	Description	: local functions
 -----------------------------------------------------------------------------*/
static I32 trim_index(HAND hscamif, U32 camid, U32 normalbulk, Trim_Index_t index_data, U32 *pindex)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;
	camif_buffergroup_t *pbg; 		// 	buffer group
	U32 count;

	//--
	res = 0;
	if (hscamif == NULL) {
		goto func_exit;
	}
	pscamif = (scamif_t *) hscamif;
	if (camid >= pscamif->camcount) {
		goto func_exit;
	}

	psc = &pscamif->scc[camid];
	
	//--
	if (normalbulk == NORMALBULK_NORMAL) { // normal
		pbg = &psc->nb;		// 
	} else if (normalbulk == NORMALBULK_BULK) {	// 0x01: Normal, 0x02: Bulk
		pbg = &psc->bb;		 
	} else {
		goto func_exit;
	}

//	cr_mutex_wait(pbg->hmutex, MUTEXTIMEOUT);
	count = pbg->count;
	if (count >= SCAMIF_BUFFER_COUNT1 && count <= SCAMIF_BUFFER_COUNT16) {
		if(index_data.index_mode == SCAMIF_INDEX_DEFAULT)	{
			*pindex = (index_data.index + count * INDEXCOUNTMULT) % count;
		}
		else if(index_data.index_mode == SCAMIF_INDEX_SIGNED)	{
			I32 itmp;
			
			if (index_data.signed_index >= 0) {
							//
			} else {
				itmp = (((-index_data.signed_index) / count) + 3) * count;
				index_data.signed_index = index_data.signed_index + itmp;
			}
			*pindex = index_data.signed_index % count;
		}
		else if(index_data.index_mode == SCAMIF_INDEX_DELTA)	{
			I32 itmp;
			U32 index2;
			I32 index3;		
			
			index3 = (I32)index_data.index + index_data.index_delta;
			if (index3 >= 0) {
				index2 = (U32)(index3);
			} else {
				itmp = (((-index3) / count) + 3) * count;
				index2 = index3 + itmp;
			}
			
			*pindex = index2 % count;
		}		

		res = 1;
	} else {
		res = 0;
	}
//	cr_mutex_release (pbg->hmutex);


func_exit:
	return res;
}

static I32  get_wr_index(HAND hscamif, U32 camid, U32 normalbulk, CR_BOOL write_index, U32 *pindex)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;
	//camimageinfo_t	*pci;
	camif_buffergroup_t *pbg; 		// 	buffer group

	//--
	res = 0;
	if (hscamif == NULL) {
		goto func_exit;
	}
	pscamif = (scamif_t *) hscamif;
	if (camid >= pscamif->camcount) {
		goto func_exit;
	}

	psc = &pscamif->scc[camid];
	//pci  = &psc->imginfo;

	//--
	if (normalbulk == NORMALBULK_NORMAL) { // normal
		pbg = &psc->nb;		// 
	} else if (normalbulk == NORMALBULK_BULK) {	// 0x01: Normal, 0x02: Bulk
		pbg = &psc->bb;		 
	} else {
		goto func_exit;
	}

	if (pbg->valid) {
		if(write_index) {
			*pindex = pbg->windexb;
		}
		else {
			*pindex = pbg->rindexb;
		}
			
		
		res = 1;
	}

func_exit:
	return res;
}


static I32 read_imagebuf(
		HAND hscamif, U32 camid, U32 normalbulk, CR_BOOL updateindex, CR_BOOL latest,
		camif_buffer_info_t **ppb, U08 **ppbuf, U32 *prindex)	// Get image buffer and info. and update read-index, if success.

{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;
	//camimageinfo_t	*pci;
	camif_buffergroup_t *pbg; 		// 	buffer group
	U32 utmp;
	U32 rindex, windex;
	U32 mres;

	//--
	res = 0;
	if (hscamif == NULL) {
		goto func_exit;
	}
	pscamif = (scamif_t *) hscamif;
	if (camid >= pscamif->camcount) {
		goto func_exit;
	}

	psc = &pscamif->scc[camid];
	//pci  = &psc->imginfo;

	//--
	if (normalbulk == NORMALBULK_NORMAL) {	// 0x01: Normal, 0x02: Bulk
		pbg = &psc->nb;		// 
	} else if (normalbulk == NORMALBULK_BULK) {
		pbg = &psc->bb;		// 
	} else {
		res = 0;
		goto func_exit;
	}

	mres = cr_mutex_wait(pbg->hmutex, MUTEXTIMEOUT_READ);
	if (mres == 0) {
		res = 0;
		goto func_exit;
	}

	windex = pbg->windexb;

	if(latest)
	{
		utmp = windex-1;
		res = scamif_imagebuf_trimindex(hscamif, camid, normalbulk, utmp, &rindex);
	}
	else
	{
		rindex = pbg->rindexb;
		res = 1;
	}
	
	if (res == 1) {	// sucess..
		if ( (pbg->bif[rindex].state == SCAMBUFFERSTATE_FULL) && (rindex != windex) ) 
		//if (rindex != windex) 
		{				// rindex == windex: EMPTY.. :)
			if (ppb) {
				*ppb = &pbg->bif[rindex];
			}

			if (ppbuf) {
				*ppbuf = pbg->pbuffer[rindex];
			}

			if (prindex) {
				*prindex = rindex;
			}
			if (updateindex) {
				//pbg->bif[rindex].state = SCAMBUFFERSTATE_EMPTY;			// 2017_0110
				utmp = rindex+1;
				res = scamif_imagebuf_trimindex(hscamif, camid, normalbulk, utmp, &rindex);
				if (res) {
					pbg->rindexb = rindex;
				}
			} else {
				res = 1;
			}
		}	else {
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "pscamif: %08x pbg: %08x  pbg->bif[%d].state %d\n", 
			//		(U32) pscamif, (U32) pbg, rindex, pbg->bif[rindex].state);

			res = 0;
		}
	} else {
		// what?
	}
	cr_mutex_release (pbg->hmutex);

func_exit:
	return res;
}



static I32 get_group_info(HAND hscamif, U32 camid, U32 normalbulk, camif_buffer_info_t **ppb, camif_buffergroup_t **ppbg)	// Get image buffer information 
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;
	//camimageinfo_t	*pci;
	camif_buffergroup_t *pbg; 		// 	buffer group

	//--
	res = 0;
	if (hscamif == NULL) {
		goto func_exit;
	}
	pscamif = (scamif_t *) hscamif;
	if (camid >= pscamif->camcount) {
		goto func_exit;
	}

	psc = &pscamif->scc[camid];
	//pci  = &psc->imginfo;

	//--
	if (normalbulk == NORMALBULK_NORMAL) {	// 0x01: Normal, 0x02: Bulk
		pbg = &psc->nb;		// 
	} else if (normalbulk == NORMALBULK_BULK) {
		pbg = &psc->bb;		// 
	} else {
		res = 0;
		goto func_exit;
	}

	if (ppb != NULL) {
		*ppb = &pbg->buffergroupinfo;
	}

	if (ppbg != NULL) {
		*ppbg = pbg;
	}

	res = 1;

func_exit:
	return res;
}





/*!
 ********************************************************************************
 *	@brief      Initialize Buffer with.. and save to buffer group info
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK, 3: Both
 *  @param[in]	pci0
 *              camimageinfo_t		
 *              Use imginfo in ssc if pci0 is NULL
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/
I32 scamif_imagebuf_init(HAND hscamif, U32 camid, U32 normalbulk, camimageinfo_t *pci0)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;
	camimageinfo_t	*pci;
	camif_buffergroup_t *pbg; 		// 	buffer group
	camif_buffer_info_t *pbibg;
	U32 i, j;
	U32 mres;

	//--
	res = 0;
	if (hscamif == NULL) {
		goto func_exit;
	}
	pscamif = (scamif_t *) hscamif;
	if (camid >= pscamif->camcount) {
		goto func_exit;
	}

	psc = &pscamif->scc[camid];
	//--
	for (i = 1; i < 3; i++) {
		if ((normalbulk & i) == NORMALBULK_NORMAL) {	// 0x01: Normal, 0x02: Bulk
			pbg = &psc->nb;		// 
			if (pci0 == NULL) {
				pci  = &psc->imginfo;		// Normal.
			} else {
				pci = pci0;
			}
		} else if ((normalbulk & i) == NORMALBULK_BULK) {	// 0x01: Normal, 0x02: Bulk
			pbg = &psc->bb;		// 
			if (pci0 == NULL) {
				pci  = &psc->imginfo_BULK;		// Normal.
			} else {
				pci = pci0;
			}
		} else {
			continue;
		}
		mres = cr_mutex_wait(pbg->hmutex, MUTEXTIMEOUT_INIT);
		if (mres == 0) {
			continue;
		}
		// buffer group
		pbg->valid 		= 0;
//		pbg->count		= 0;			// buffer count. Must be 2^n number.
//		pbg->windexb	= 0;			// next write index for buf; (0 ~ count-1)
//		pbg->rindexb	= 0;			// next read index for buf;  (0 ~ count-1)

		// buffergroupinfo
		pbibg 			= &pbg->buffergroupinfo;
		pbibg->state 	= SCAMBUFFERSTATE_EMPTY;
		pbibg->ts_h 	= 0;			//
		pbibg->ts_l 	= 0;			// nsec
		pbibg->frameindex = 0;

		memcpy(&pbibg->cii, pci, sizeof(camimageinfo_t)); 

		// Buffer...
		{
			U32 pixelcount;
			U32 datamemsize;
			U32 buffercount;
			U08 *pbuf;

			pixelcount = pci->width * pci->height;
			//datamemsize = SCAMIF_BUFFER_COUNT1 * SCAMIF_BUFFER_SIZE1;
			datamemsize = SCAMIF_BUFFER_SIZE_BUFFER;		//20191122


			buffercount = SCAMIF_BUFFER_COUNT1;

			for (buffercount = SCAMIF_BUFFER_COUNT1; buffercount <= SCAMIF_BUFFER_COUNT16; buffercount *=2) {
				if (pixelcount * buffercount * 2 > datamemsize) {
					break;
				}
			}
			if (buffercount > SCAMIF_BUFFER_COUNT16) {
				buffercount = SCAMIF_BUFFER_COUNT16;
			}
			pbg->count = buffercount;


			//pbuf = &pbg->datamem[0];
			pbuf = pbg->datamem;
			for (j = 0; j < buffercount; j++) {		// reset buffer memory pointer and buffer info
				pbg->pbuffer[j] = pbuf;
				pbuf = pbuf + pixelcount;
				pbg->bif[j].state = SCAMBUFFERSTATE_EMPTY;
			}
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%d:%d> buffer count: %d.  (%4dx%4d) datamemsize: %d (use: %d)      init windexb and rindexb \n", camid, i, buffercount, pci->width, pci->height,
				datamemsize, pixelcount * buffercount);
			//pbg->count		= 0;			// buffer count. Must be 2^n number.
			pbg->windexb	= 0;			// next write index for buf; (0 ~ count-1)
			pbg->rindexb	= 0;			// next read index for buf;  (0 ~ count-1)
		}
		pbg->valid 	= 1;
		cr_mutex_release (pbg->hmutex);
		res = 1;

		scamif_imagebuf_timetickupdate(hscamif, camid);
	}
func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Get Group info
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[out]	ppb
 *            	Buffer info of group  
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/13
 *******************************************************************************/
I32 scamif_imagebuf_groupinfo(HAND hscamif, U32 camid, U32 normalbulk, camif_buffer_info_t **ppb)	// Get image buffer information 
{
	return get_group_info(hscamif, camid, normalbulk, ppb, NULL);
}


/*!
 ********************************************************************************
 *	@brief      Get Group info... :P
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[out]	ppb
 *            	Buffer info of group  
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/13
 *******************************************************************************/
I32 scamif_imagebuf_groupinfo2(HAND hscamif, U32 camid, U32 normalbulk, camif_buffer_info_t **ppb, camif_buffergroup_t **ppbg)	// Get image buffer information 
{
	return get_group_info(hscamif, camid, normalbulk, ppb, ppbg);
}


/*!
 ********************************************************************************
 *	@brief      Get Trim'ed index 
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[in]	index
 *  			Index  (May be larger than count.)
 *  @param[out]	pindex
 *  			Trim'ed Index 
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/13
 *******************************************************************************/
I32 scamif_imagebuf_trimindex(HAND hscamif, U32 camid, U32 normalbulk, U32 index, U32 *pindex)
{
	Trim_Index_t index_data;

	index_data.index_mode = SCAMIF_INDEX_DEFAULT;
	index_data.index = index;

	return trim_index(hscamif, camid, normalbulk, index_data, pindex);
}


/*!
 ********************************************************************************
 *	@brief      Get Trim'ed index, signed input
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[in]	index
 *  			Index  (May be larger than count.. .May be minus )
 *  @param[out]	pindex
 *  			Trim'ed Index 
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/13
 *******************************************************************************/
I32 scamif_imagebuf_trimindex2(HAND hscamif, U32 camid, U32 normalbulk, I32 signedindex, U32 *pindex)
{
	Trim_Index_t index_data;

	index_data.index_mode = SCAMIF_INDEX_SIGNED;
	index_data.signed_index = signedindex;

	return trim_index(hscamif, camid, normalbulk, index_data, pindex);
}



/*!
 ********************************************************************************
 *	@brief      Get Trim'ed index, with delta index. 
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[in]	index
 *  			Index  (May be larger than count.. .May be minus )
 *  @param[in]	indexdelta
 *  			delta of Index
 *  @param[out]	pindex
 *  			Trim'ed Index 
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2017/02/09
 *******************************************************************************/
I32 scamif_imagebuf_trimindexdelta(HAND hscamif, U32 camid, U32 normalbulk, U32 index, I32 indexdelta, U32 *pindex)
{
	Trim_Index_t index_data;

	index_data.index_mode = SCAMIF_INDEX_DELTA;
	index_data.index = index;
	index_data.index_delta = indexdelta;	

	return trim_index(hscamif, camid, normalbulk, index_data, pindex);
}



/*!
 ********************************************************************************
 *	@brief      Get next write index
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[out]	pindex
 *  			Index of next write index
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/13
 *******************************************************************************/
I32 scamif_imagebuf_getwriteindex(HAND hscamif, U32 camid, U32 normalbulk, U32 *pindex)
{
	return get_wr_index(hscamif, camid, normalbulk, CR_TRUE, pindex);
}


/*!
 ********************************************************************************
 *	@brief      Get next read index
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[out]	pindex
 *  			Index of next read index
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/13
 *******************************************************************************/
I32 scamif_imagebuf_getreadindex(HAND hscamif, U32 camid, U32 normalbulk, U32 *pindex)
{
	return get_wr_index(hscamif, camid, normalbulk, CR_FALSE, pindex);
}

/*!
 ********************************************************************************
 *	@brief      Write Image.. with info and index
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[in]	ppb
 *            	Buffer info of image
 *  @param[in]	img
 *            	image buffer
 *  @param[in]	index
 *            	Index of frame 
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2016/07/03
 *******************************************************************************/
I32 scamif_imagebuf_write_raw(
		HAND hscamif, U32 camid, U32 normalbulk, 
		camif_buffer_info_t  *pb, U08 *img, U32 index)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;
	//camimageinfo_t	*pci;
	camif_buffergroup_t *pbg; 		// 	buffer group
	camif_buffer_info_t *pbibg;
	U32 windex;
	U32 mres;

	//--
	res = 0;
	if (hscamif == NULL) {
		goto func_exit;
	}
	pscamif = (scamif_t *) hscamif;
	if (camid >= pscamif->camcount) {
		goto func_exit;
	}

	if ((pscamif->lockcode & SCAMIF_LOCK_WRITE) != 0) {
		goto func_exit;
	}

	psc = &pscamif->scc[camid];
	//pci  = &psc->imginfo;

	//--
	if (normalbulk == NORMALBULK_NORMAL) {	// 0x01: Normal, 0x02: Bulk
		if ((pscamif->lockcode & SCAMIF_LOCK_NORMAL_WRITE) != 0) {		// 20191112
			goto func_exit;
		}
		pbg = &psc->nb;		// 
	} else if (normalbulk == NORMALBULK_BULK) {
		if ((pscamif->lockcode & SCAMIF_LOCK_BULK_WRITE) != 0) {		// 20191112
			goto func_exit;
		}
		pbg = &psc->bb;		// 
	} else {
		res = 0;
		goto func_exit;
	}


	pbibg 	= &pbg->buffergroupinfo;

	mres = cr_mutex_wait(pbg->hmutex, MUTEXTIMEOUT_WRITE_RAW);
	if (mres == 0) {
		res = 0;
		goto func_exit;
	}
	
	// Check image image size and offset
	if (
			(pbibg->cii.width 	== pb->cii.width)
			&& (pbibg->cii.height 	== pb->cii.height)
			//&& (pbibg->cii.offset_x == pb->cii.offset_x)
			//&& (pbibg->cii.offset_y == pb->cii.offset_y)
			)
	{
		U32 pixelcount;
		res = scamif_imagebuf_trimindex(hscamif, camid, normalbulk, index, &windex);
		if (res) {
			pixelcount =    pbibg->cii.width * pbibg->cii.height;
			memcpy(&pbg->bif[windex], pb, sizeof(camif_buffer_info_t));	// Buffer informatin.

			if ((pscamif->lockcode & SCAMIF_LOCK_WRITE_DATAMEMONLY) == 0) {
				memcpy(pbg->pbuffer[windex], img, pixelcount);				// Image..
			} else {
				// do not copy -_-;
			}
			pbg->bif[windex].state = SCAMBUFFERSTATE_FULL;

			windex++;
			scamif_imagebuf_trimindex(hscamif, camid, normalbulk, windex, &windex);

			scamif_imagebuf_updatets64m(hscamif, camid, pb, img);

			pbg->windexb = windex;
			res = 1;

		}
	}

	cr_mutex_release (pbg->hmutex);

func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Write Image.. with info
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[in]	ppb
 *            	Buffer info of image
 *  @param[in]	img
 *            	image buffer
 *  @param[out]	pwindex
 *            	Index of written frame (And updated internally, if writing was successful.)
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/13
 *******************************************************************************/
I32 scamif_imagebuf_write(
		HAND hscamif, U32 camid, U32 normalbulk, 
		camif_buffer_info_t  *pb, U08 *img, U32 *pwindex)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;
	//camimageinfo_t	*pci;
	camif_buffergroup_t *pbg; 		// 	buffer group
	camif_buffer_info_t *pbibg;
	U32 utmp;
	U32 rindex, windex;
	U32 mres;

	//--
	res = 0;
	if (hscamif == NULL) {
		goto func_exit;
	}
	pscamif = (scamif_t *) hscamif;
	if (camid >= pscamif->camcount) {
		goto func_exit;
	}

	if ((pscamif->lockcode & SCAMIF_LOCK_WRITE) != 0) {
		goto func_exit;
	}

	psc = &pscamif->scc[camid];
	//pci  = &psc->imginfo;

	//--
	if (normalbulk == NORMALBULK_NORMAL) {	// 0x01: Normal, 0x02: Bulk
		if ((pscamif->lockcode & SCAMIF_LOCK_NORMAL_WRITE) != 0) {		// 20191112
			goto func_exit;
		}
		pbg = &psc->nb;		// 
	} else if (normalbulk == NORMALBULK_BULK) {
		if ((pscamif->lockcode & SCAMIF_LOCK_BULK_WRITE) != 0) {		// 20191112
			goto func_exit;
		}
		pbg = &psc->bb;		// 
	} else {
		res = 0;
		goto func_exit;
	}

//#define BULK_DEBUG

#if defined(BULK_DEBUG)
	if (normalbulk == NORMALBULK_BULK) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #1\n");
	}
#endif

	pbibg 	= &pbg->buffergroupinfo;
//#define MUTEXTIMEOUT_WRITE	1000
#if defined(MUTEXTIMEOUT_WRITE)
	mres = cr_mutex_wait(pbg->hmutex, MUTEXTIMEOUT_WRITE);
	if (mres == 0) {
		res = 0;
		goto func_exit;
	}
#endif
	UNUSED(mres);

#if defined(BULK_DEBUG)
	if (normalbulk == NORMALBULK_BULK) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #2\n");
	}
#endif
	
	// Check image image size and offset
	if (
			(pbibg->cii.width 	== pb->cii.width)
			&& (pbibg->cii.height 	== pb->cii.height)
			//&& (pbibg->cii.offset_x == pb->cii.offset_x)
			//&& (pbibg->cii.offset_y == pb->cii.offset_y)
		)
	{
		U32 pixelcount;

		windex = pbg->windexb;
		pixelcount =    pbibg->cii.width * pbibg->cii.height;

		memcpy(&pbg->bif[windex], pb, sizeof(camif_buffer_info_t));	// Buffer informatin.
		if ((pscamif->lockcode & SCAMIF_LOCK_WRITE_DATAMEMONLY) == 0) {
			memcpy(pbg->pbuffer[windex], img, pixelcount);				// Image..
		} else {
			// do not copy -_-;
		}

		pbg->bif[windex].state = SCAMBUFFERSTATE_FULL;

		utmp = windex+1;
#if defined(BULK_DEBUG)
	if (normalbulk == NORMALBULK_BULK) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #3\n");
	}
#endif
		res = scamif_imagebuf_trimindex(hscamif, camid, normalbulk, utmp, &windex);
		if (res) {
			pbg->windexb = windex;
		}
		rindex = pbg->rindexb;
#if 1
		if (rindex == windex) {				// TODO: Need MUTEX...... 
			utmp = rindex+1;
			res = scamif_imagebuf_trimindex(hscamif, camid, normalbulk, utmp, &rindex);
			if (res) {
				pbg->rindexb = rindex;
			}
		}
#endif
		scamif_imagebuf_updatets64m(hscamif, camid, pb, img);


		*pwindex = windex;

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%d> wi: %d, (%d, %d)\n", camid, windex, pb->cii.width, pb->cii.height);
		res = 1;
	} else {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"configued width: %d, height: %d    !=   Image packet input width:%d, height: %d\n", 
			pbibg->cii.width,  pbibg->cii.height,  pb->cii.width, pb->cii.height);
		res = 0;
	}
#if defined(MUTEXTIMEOUT_WRITE)
	cr_mutex_release (pbg->hmutex);
#endif
func_exit:
#if defined(BULK_DEBUG)
	if (normalbulk == NORMALBULK_BULK) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #4\n");
	}
#endif

	return res;
}


I32 scamif_imagebuf_updatets64m(HAND hscamif, U32 camid, camif_buffer_info_t  *pb, U08 *img)
{
	U64 ts64;
	U64 period;
	U64 ts64delta;
	U32 syncdiv;
	U32 multitude;
	U32 skip;
	U32 width, height;
	U32 ts_high, ts_low;
	
	U64 ts64m0[MAXMULT];				// calcurated TS64 for each multi.
	U64 ts64m1[MAXMULT];				// from Image..
	scamif_t *pscamif;

	//--
	pscamif = (scamif_t *) hscamif;
	period = (1000000000LL) / (U64)(pb->cii.framerate);

	syncdiv 	= pb->cii.syncdiv;
	multitude 	= pb->cii.multitude;
	ts_high 	= pb->ts_h;
	ts_low 		= pb->ts_l;
	skip 		= pb->cii.skip;
	width 		= pb->cii.width;
	height 		= pb->cii.height;
	

	if (multitude == 0) {
		ts64delta = period;
	}	else {
		//			ts64delta = period * multitude;
		ts64delta = period;
	}


	//if (camid == 0 || syncdiv == 0) {			// 0: master
	//	syncdiv = 1;
	//}

	if (camid == (U32)pscamif->mastercamid || syncdiv == 0) {			// 0: master
		syncdiv = 1;
	}



	if (syncdiv != 0) {
		ts64delta = ts64delta * syncdiv;
	}
	ts64delta = ts64delta * skip;

	ts64 = MAKEU64(ts_high, ts_low);


	// fill ts64 for multitude images.
	if (multitude == 0) {
		pb->ts64m[0] = ts64;
	}
	else {
		U32 i;
		U32 imageoffset_line;
		U32 imageoffset;
		U08 *pbuf;
		I64 ts64diff;

		pbuf = (U08 *) img;


		for (i = 0; i < multitude; i++) {
			double d0;
			double d1;

			//--
			imageoffset_line = (height * i) / multitude;
			imageoffset = imageoffset_line * width;

			ts64m0[i] = ts64 - (multitude - i - 1) * ts64delta;
			ts64m0[i] &= ~((U64)15);
			ts64m1[i] = *(U64 *)(pbuf + imageoffset);
			ts64m1[i] &= ~((U64)15);

			ts64diff = ts64m1[i] - ts64m0[i];

			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%d/%d]: %08x-%08x vs %08x_%08x\n", camid, i, multitude, 
			//		(U32)(ts64m0[i] >> 32), (U32)(ts64m0[i] >> 0),
			//		(U32)(ts64m1[i] >> 32), (U32)(ts64m1[i] >> 0)
			//		);	

			if (i != 0) {
				d0 = (ts64m0[i] - ts64m0[i - 1]) / 1000000000.0;
				d1 = (ts64m1[i] - ts64m1[i - 1]) / 1000000000.0;
//				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%d/%d]: %lf  vs %lf\n", camid, i, multitude, d0, d1);
			}

			if (ts64diff < -MAXTS64MDIFF || ts64diff > MAXTS64MDIFF) {
				ts64m1[i] = ts64m0[i];
			}
		}

		for (i = 0; i < multitude; i++) {
			pb->ts64m[i] = ts64m1[i];
		}
	}
//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]   ts64 : %08x-%08x\n", camid, (U32)(ts64 >> 32), (U32)(ts64 >> 0));

	UNUSED(hscamif);

	return 1;
}



/*!
 ********************************************************************************
 *	@brief      Read Image and info. and Update frame index
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[out]	ppb
 *            	Buffer info of group  
 *  @param[out]	ppbuf
 *            	image buffer
 *  @param[out]	prindex
 *            	Index of read frame (And updated internally, if reading was successful.)
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/13
 *******************************************************************************/
I32 scamif_imagebuf_read(
		HAND hscamif, U32 camid, U32 normalbulk,
		camif_buffer_info_t **ppb, U08 **ppbuf, U32 *prindex)
{
	// Update index, if reading is successful.
	return read_imagebuf(hscamif, camid, normalbulk, CR_TRUE, CR_FALSE, ppb, ppbuf, prindex );
}

/*!
 ********************************************************************************
 *	@brief      Read Latest Image and info. and Update frame index
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[out]	ppb
 *            	Buffer info of group  
 *  @param[out]	ppbuf
 *            	image buffer
 *  @param[out]	prindex
 *            	Index of read frame (And updated internally, if reading was successful.)
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/13
 *******************************************************************************/
I32 scamif_imagebuf_read_latest(
		HAND hscamif, U32 camid, U32 normalbulk,
		camif_buffer_info_t **ppb, U08 **ppbuf, U32 *prindex)
{
	// Update index, if reading is successful.
	return read_imagebuf(hscamif, camid, normalbulk, CR_TRUE, CR_TRUE, ppb, ppbuf, prindex );
}



/*!
 ********************************************************************************
 *	@brief      Peek Image and info. Don't update frame index
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[out]	ppb
 *            	Buffer info of group  
 *  @param[out]	ppbuf
 *            	image buffer
 *  @param[out]	prindex
 *            	Index of read frame (And updated internally, if reading was successful.)
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/13
 *******************************************************************************/
I32 scamif_imagebuf_peek(
		HAND hscamif, U32 camid, U32 normalbulk,
		camif_buffer_info_t **ppb, U08 **ppbuf, U32 *prindex)
{
	// Don't update index
	return read_imagebuf(hscamif, camid, normalbulk, CR_FALSE, CR_FALSE, ppb, ppbuf, prindex );
}

/*!
 ********************************************************************************
 *	@brief      Peek Image and info. Don't update frame index
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[out]	ppb
 *            	Buffer info of group  
 *  @param[out]	ppbuf
 *            	image buffer
 *  @param[out]	prindex
 *            	Index of read frame (And updated internally, if reading was successful.)
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/13
 *******************************************************************************/
I32 scamif_imagebuf_peek_latest(
		HAND hscamif, U32 camid, U32 normalbulk,
		camif_buffer_info_t **ppb, U08 **ppbuf, U32 *prindex)
{
	// Don't update index
	return read_imagebuf(hscamif, camid, normalbulk, CR_FALSE, CR_TRUE, ppb, ppbuf, prindex );
}




/*!
 ********************************************************************************
 *	@brief      Random access and Read Image and info
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[in]	index
 *              Frame index
 *  @param[out]	ppb
 *            	Buffer info of group  
 *  @param[out]	ppbuf
 *            	image buffer
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/13
 *******************************************************************************/
I32 scamif_imagebuf_randomaccess(
		HAND hscamif, U32 camid, U32 normalbulk, U32 index, 
		camif_buffer_info_t **ppb, U08 **ppbuf)	
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;
	//camimageinfo_t	*pci;
	camif_buffergroup_t *pbg; 		// 	buffer group
	U32 trindex;
	U32 mres;

	//--
//*(char *) 0 = 0;
	res = 0;
	if (hscamif == NULL) {
		goto func_exit;
	}
	pscamif = (scamif_t *) hscamif;
	if (camid >= pscamif->camcount) {
		goto func_exit;
	}

	psc = &pscamif->scc[camid];
	//pci  = &psc->imginfo;

	//--
	if (normalbulk == NORMALBULK_NORMAL) {	// 0x01: Normal, 0x02: Bulk
		pbg = &psc->nb;		// 
	} else if (normalbulk == NORMALBULK_BULK) {
		pbg = &psc->bb;		// 
	} else {
		res = 0;
		goto func_exit;
	}

	mres = cr_mutex_wait(pbg->hmutex, MUTEXTIMEOUT_RANDOMACCESS);
	if (mres == 0) {
		res = 0;
		goto func_exit;
	}


	res = scamif_imagebuf_trimindex(hscamif, camid, normalbulk, index, &trindex);
	if (res) {
		if (pbg->bif[trindex].state == SCAMBUFFERSTATE_FULL) {
			if (ppb) {
				*ppb = &pbg->bif[trindex];
			}

			if (ppbuf) {
				*ppbuf = pbg->pbuffer[trindex];
			}
			res = 1;
		}
		else res = 0;
	}
	cr_mutex_release (pbg->hmutex);

func_exit:
	return res;
}




/*!
 ********************************************************************************
 *	@brief      Get info 
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	index
 *              frame index
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[out]	ppb
 *            	Buffer info of group  
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/13
 *******************************************************************************/
I32 scamif_imagebuf_getinfo(HAND hscamif, U32 camid, U32 normalbulk, U32 index, camif_buffer_info_t **ppb)	// Get image buffer and info
{
	I32 res;

	res = scamif_imagebuf_randomaccess(hscamif, camid, normalbulk, index, ppb, NULL);	
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Get buffer count
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[out]	pcount
 *            	buffer count
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2016/02/24
 *******************************************************************************/
I32 scamif_imagebuf_buffercount(HAND hscamif, U32 camid, U32 normalbulk, U32 *pcount)	
{
	I32 res;
	scamif_t *pscamif;
	scamcam_t *psc;
//	camimageinfo_t	*pci;
	camif_buffergroup_t *pbg; 		// 	buffer group

	//--
	if (hscamif == NULL) {
		res = 0;
		goto func_exit;
	}

	pscamif = (scamif_t *) hscamif;
	if (camid >= pscamif->camcount) {
		res = 0;
		goto func_exit;
	}

	psc = &pscamif->scc[camid];
	if (normalbulk == NORMALBULK_NORMAL) {	// 0x01: Normal, 0x02: Bulk
		pbg = &psc->nb;		// 
		*pcount = pbg->count;
		res = 1;
	} else if (normalbulk == NORMALBULK_BULK) {
		pbg = &psc->bb;		// 
		*pcount = pbg->count;
		res = 1;
	} else {
		res = 0;
	}

func_exit:
	return res;
}



/*!
 ********************************************************************************
 *	@brief      Get time-stamp64 range of this buffer 
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	index
 *              frame index
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[out]	ptsfrom_h
 *            	from timeindex, higher 32bit
 *  @param[out]	ptsfrom_l
 *            	from timeindex, lower 32bit
 *  @param[out]	ptsto_h
 *            	to timeindex, higher 32bit
 *  @param[out]	ptsto_l
 *            	to timeindex, lower 32bit
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/13
 *******************************************************************************/
I32 scamif_imagebuf_timestamprange(HAND hscamif, U32 camid, U32 normalbulk, U32 index, 
		U64 *pts64from, U64 *pts64to)
{
	I32 res;
	U32 ts_h, ts_l;				// nsec
	camif_buffer_info_t *pb;
	camimageinfo_t     *pcii;

	U32 framerate;				// frame per sec..
	//U32 gain;					// 
	//U32 exposure;				// usec

	//--
	U32 skip;					// skip count
	U32 multitude;				// multitude count
	U32 syncdiv;				// Sync Divide count

	U64 period;					// 64bit, frame period, nsec 
	U64 ts64;
	U64 tsfrom64, tsto64;

	//---
	res = scamif_imagebuf_getinfo(hscamif, camid, normalbulk, index, &pb);
	if (res == 0) {
		goto func_exit;
	}

	if (pb->state != SCAMBUFFERSTATE_FULL) {
		res = 0;
		goto func_exit;
	}

	//---
	ts_h = pb->ts_h;
	ts_l = pb->ts_l;

	ts64 = MAKEU64(ts_h,ts_l);

	pcii = &pb->cii;


	framerate 	= pcii->framerate;
	skip 		= pcii->skip;
	multitude 	= pcii->multitude;
	syncdiv 	= pcii->syncdiv;

	//period = (1000000000LL) / (U64) framerate;			// nsec
	period = (TSSCALE) / (U64) framerate;			// nsec

	if (multitude == 0) {
		multitude = 1;
	}

	if (camid == 0) {
		syncdiv = 1;								// I am  master... syncdiv is useless.
	}
	//tsfrom64 	= ts64 - ((U64) (((multitude - 1) + 0.5) * period)) * skip * syncdiv;
	//tsto64 		= ts64 + ((U64) ((                  0.5) * period)) * skip * syncdiv;

	tsfrom64 	= ts64 - ((U64) (((multitude - 1) + 0.0) * period)) * skip * syncdiv;
	tsto64 		= ts64 + ((U64) ((                  0.0) * period)) * skip * syncdiv;

	*pts64from	= tsfrom64;

	*pts64to	= tsto64;

func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      Get time-stamp64 range of this buffer, Extended
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[in]	ts64
 *            	time stamp for search
 *  @param[out]	*prindex
 *              frame index contains ts
 *  @param[out]	*pts64fit
 *              best fit ts64
 *  @param[out]	pts64from
 *            	from timeindex
 *  @param[out]	ptsto
 *            	to timeindex
 *
 *  @return		1: Found. 
 *             -1: Not Yet. 
 *             -2: Don't contain. 
 *
 *             0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2016/02/24
 *******************************************************************************/
I32 scamif_imagebuf_timestamprangeEx(HAND hscamif, U32 camid, U32 normalbulk, U64 ts64,
		U32 *prindex, U64 *pts64fit, U64 *pts64from, U64 *pts64to)
{
	I32 res;
	U32 i;
	camif_buffer_info_t *pb;
	U32 rindex;
	U32 foundit;
	U32 count;

	U64 ts640, ts641;
	U64 ts64from, ts64to;
	U64 ts64fit;

	//---
	ts64fit = 0;
	ts64from = 0;
	ts64to = 0;


	count = 0;
	ts641 = 0;

	res = scamif_imagebuf_buffercount(hscamif, camid, normalbulk, &count);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] buffer count: %d\n", camid, count);

	res = scamif_imagebuf_peek_latest(hscamif, camid, normalbulk, &pb, NULL, &rindex);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] peek res: %d\n", camid, res);
	if (res != 0) {
		res = scamif_imagebuf_buffercount(hscamif, camid, normalbulk, &count);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] buffer count: %d\n", camid, count);
		if (count == 0) res = 0;
	}
	if (res == 0) {
		goto func_exit;			// FAIL.
	}

	ts640 = MAKEU64(pb->ts_h, pb->ts_l);	

	if (ts640 < ts64) {			// Not yet.
		res = -1;
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] not yet.. (%llu < %llu)\n",camid,
				ts640, ts64);
		goto func_exit;
	}

	foundit = 0;
	rindex--;
	for (i = 0; i < count; i++) {
		res = scamif_imagebuf_getinfo(hscamif, camid, normalbulk, rindex, &pb);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d/%d] rindex: %d, res: %d\n", i, count, rindex, res);

		if (res == 0) {
			break;
		}

		ts641 = MAKEU64(pb->ts_h, pb->ts_l);	
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ts640: %llu(%llx), ts641: %llu(%llx)\n", ts640,ts640, ts641,ts641);
		if (ts641 >= ts640) {		// Round... fail.
			break;
		}

		if (ts641 <= ts64) {		// Good!
			foundit = 1;
			if (ts641 != ts64) {
				rindex++;				// progress.. :)
				ts641 = ts640;			// Previous value.
			}
			break;
		}
		rindex--;
		ts640 = ts641;
	}

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] foundit: %d, i: %d\n",camid, foundit, i);
	if (foundit) {
		camimageinfo_t     *pcii;
		U32 framerate;				// frame per sec..
		U32 skip;					// skip count
		U32 multitude;				// multitude count
		U32 syncdiv;				// Sync Divide count
		U64 period;					// 64bit, frame period, nsec 

		//--
		pcii 		= &pb->cii;
		framerate 	= pcii->framerate;
		skip 		= pcii->skip;
		multitude 	= pcii->multitude;
		syncdiv 	= pcii->syncdiv;
		period 		= (TSSCALE) / (U64) framerate;			// nsec

		if (multitude == 0) {
			multitude = 1;
		}
		if (camid == 0) {
			syncdiv = 1;								// I am the master... syncdiv is useless.
		}
//		ts64from 	= ts641 - ((U64) (((multitude - 1) + 0) * period)) * skip * syncdiv;
		ts64from 	= ts641 - ((U64) (((multitude - 0) + 0) * period)) * skip * syncdiv;
		ts64to 		= ts641 + ((U64) ((                  0) * period)) * skip * syncdiv;

		ts64fit = ts64from;
		for (i = 0; i < (multitude * skip * syncdiv); i++) {
			if (ts64fit >= ts64) {
				break;
			}
			ts64fit += period;
		}

		*prindex 	= rindex;
		*pts64fit 	= ts64fit;
		*pts64from 	= ts64from;
		*pts64to 	= ts64to;
		res = 1;			// good.
	} else {
		res = -2;			// Cannot found..
	}

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "res: %d, rindex: %d ts64: %lf, ts64fit: %lf, ts64from: %lf, ts64to: %lf\n", res, rindex,
		ts64/TSSCALE_D,
		ts64fit/TSSCALE_D,
		ts64from/TSSCALE_D,
		ts64to/TSSCALE_D);
func_exit:
	return res;
}




/*!
 ********************************************************************************
 *	@brief      TimeTick update to current tick.
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2016/09/03
 *******************************************************************************/
I32 scamif_imagebuf_timetickupdate(
		HAND hscamif, U32 camid)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;

	//--
	res = 0;
	if (hscamif == NULL) {
		goto func_exit;
	}
	pscamif = (scamif_t *) hscamif;
	if (camid >= pscamif->camcount) {
		goto func_exit;
	}

	psc = &pscamif->scc[camid];
	psc->timetick = cr_gettickcount();
	res = 1;

func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Get Last successful TimeTick
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *
 *  @return		timetick of last successful operation.
 * 
 *	@author	    yhsuk
 *  @date       2016/09/03
 *******************************************************************************/
U32 scamif_imagebuf_timetick(
		HAND hscamif, U32 camid)
{
	U32 tick;
	scamcam_t *psc;
	scamif_t *pscamif;

	//--
	tick = 0;
	if (hscamif == NULL) {
		goto func_exit;
	}
	pscamif = (scamif_t *) hscamif;
	if (camid >= pscamif->camcount) {
		goto func_exit;
	}

	psc = &pscamif->scc[camid];
	tick = psc->timetick;

func_exit:
	return tick;
}

void scamif_datamem_create(HAND hscamif)
{
	I32 camid;
	U32 workmode;
	scamif_t *pscamif;
	scamcam_t *psc;

	//---
	if (hscamif == NULL) {
		return;
	}
	pscamif = (scamif_t *) hscamif;
	workmode = pscamif->workmode;

	if (s_sdm_hmutex == NULL) {
		s_sdm_hmutex = cr_mutex_create();	
	}
	
	cr_mutex_wait(s_sdm_hmutex, MUTEXTIMEOUT_INIT);
	if (s_sdm_hmutex == NULL) {				// Check once more.. :P
		s_sdm_hmutex = cr_mutex_create();	
	}
	if (s_psdm == NULL) {
		U32 bufsize;
		s_psdm = (scamif_datamem_manager_t *) malloc(sizeof(scamif_datamem_manager_t));
		memset((void *)s_psdm, 0, sizeof(scamif_datamem_manager_t));

		//bufsize = sizeof(U08) * (SCAMIF_BUFFER_COUNT1 * SCAMIF_BUFFER_SIZE1 + SCAMIF_BUFFER_GUARD);
		// bufzie = 22282240  ( ~= 21.25 Mbyte)

		bufsize = sizeof(U08) * (SCAMIF_BUFFER_SIZE_BUFFER + SCAMIF_BUFFER_GUARD);						// 20191123

		for (camid = 0; camid < MAXCAMCOUNT; camid++) {
			s_psdm->nbmem[camid] = (U08 *) malloc(bufsize); memset(s_psdm->nbmem[camid], 0, bufsize);
			s_psdm->bbmem[camid] = (U08 *) malloc(bufsize); memset(s_psdm->bbmem[camid], 0, bufsize);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "bufsize: %d\n", bufsize);
		}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Total bufsize: %d Byte (%d MByte)\n", 
				bufsize * 4 * MAXCAMCOUNT, (bufsize * 4 * MAXCAMCOUNT)/(1024*1024));
		// Total Image Buffer size : 170 Mbyte.. -_-;  (21.25 * 4(buffer kind) * 2(cam count))
		s_psdm->refcount = 0;
	}

	for (camid = 0; camid < MAXCAMCOUNT; camid++) {
		psc = &pscamif->scc[camid];
		psc->nb.datamem = s_psdm->nbmem[camid];
		psc->bb.datamem = s_psdm->bbmem[camid];
		s_psdm->refcount++;

	}

#if defined(PCIE_CHANNEL)
	HAL_PCIE_Init();	
#endif

	cr_mutex_release (s_sdm_hmutex);
}

void scamif_datamem_release(HAND hscamif)
{
	I32 camid;
	U32 workmode;
	scamif_t *pscamif;

	if (hscamif == NULL) {
		return;
	}
	pscamif = (scamif_t *) hscamif;
	workmode = pscamif->workmode;

	if (s_sdm_hmutex != NULL) {
		cr_mutex_wait(s_sdm_hmutex, MUTEXTIMEOUT_INIT);
		for (camid = 0; camid < MAXCAMCOUNT; camid++) {
			if (s_psdm->refcount > 0) {
				s_psdm->refcount--;
			}
		}

		if (s_psdm->refcount <= 0) {			// release me..:P
			HAND sdm_hmutex = s_sdm_hmutex;
			for (camid = 0; camid < MAXCAMCOUNT; camid++) {
				free(s_psdm->nbmem[camid]);
				free(s_psdm->bbmem[camid]);

				s_psdm->nbmem[camid] = NULL;
				s_psdm->bbmem[camid] = NULL;
			}
			free((void *)s_psdm);
			s_psdm = NULL;
			s_sdm_hmutex = NULL;				// NULL first... 

			cr_mutex_release (sdm_hmutex);		// saved mutex..
			cr_mutex_delete (sdm_hmutex);
		} else {
			cr_mutex_release (s_sdm_hmutex);		// saved mutex..
		}
	}

#if defined(PCIE_CHANNEL)
	HAL_PCIE_Terminate();	
#endif	
}

/*!
 ********************************************************************************
 *	@brief      Get ZROT BAR-NOISE position
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[in]	index
 *              Frame index
 *  @param[out]	pstartx
 *            	start position of bar noise
 *  @param[out]	pendx
 *            	end position of bar noise
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2020/0810
 *******************************************************************************/
U32 scamif_imagebuf_zrot_barnoise_position(
		HAND hscamif, U32 camid,
		U32 normalbulk, U32 index, 
		I32 *pstartx, I32 *pendx )
{
	U32 res;
	scamif_t *pscamif;
	camif_buffer_info_t *pb, *pbbulk;
	U32 width;
	I32 startx, endx;

	//---
	if (hscamif == NULL) {
		res = 0;
		goto func_exit;
	}
	pscamif = (scamif_t *) hscamif;
	if (camid >= pscamif->camcount) {
		res = 0;
		goto func_exit;
	}

	if (pscamif->zrot[camid] == 0) {
		res = 0;
		goto func_exit;
	}

	res = scamif_imagebuf_peek_latest(hscamif, camid, NORMALBULK_NORMAL, &pb, NULL, NULL);
	if (res == 0 || pb == NULL) {
		goto func_exit;	
	}
	width = pb->cii.width;

	if (width < ZROT_BARNOISE_STARTX) {
		res = 0;
		goto func_exit;
	}

	startx = ZROT_BARNOISE_STARTX;
	if (width < ZROT_BARNOISE_ENDX) {
		endx = width;
	} else {
		endx = ZROT_BARNOISE_ENDX;
	}

	if (normalbulk == NORMALBULK_BULK) {
		I32 deltax;
		I32 w;

		res = scamif_imagebuf_randomaccess(hscamif, camid, NORMALBULK_BULK, index, &pbbulk, NULL);
		if (res == 0 || pbbulk == NULL) {
			goto func_exit;
		}

		deltax = pbbulk->cii.offset_x - pb->cii.offset_x;
		w = pbbulk->cii.width;

		startx = -999;
		endx = -999;
		if (deltax <= ZROT_BARNOISE_STARTX) {
			startx = ZROT_BARNOISE_STARTX - deltax;
		} else if (deltax < ZROT_BARNOISE_ENDX) {
			startx = deltax;
		}

		if (deltax+w < ZROT_BARNOISE_STARTX) {
			// endx = -999;			// fail.
		} else if (deltax+w < ZROT_BARNOISE_ENDX) {
			endx = w-1;
		} else {
			endx = ZROT_BARNOISE_ENDX - deltax;
		}

		if (startx < 0 || endx < 0) {
			res = 0;
			goto func_exit;
		}
	}
	*pstartx = startx;
	*pendx = endx;
	res = 1;

func_exit:
	return res;
}





#if defined (__cplusplus)
}
#endif


/************** Not used  *******************/
#if 0
 ********************************************************************************
 *	@brief      Initialize Buffer...
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK, 3: Both
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2015/12/01
 *******************************************************************************/
I32 scamif_imagebuf_init(HAND hscamif, U32 camid, U32 normalbulk)
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;
	camimageinfo_t	*pci;
	camif_buffergroup_t *pbg; 		// 	buffer group
	camif_buffer_info_t *pbibg;
	U32 i;

	//--
	res = 0;
	if (hscamif == NULL) {
		goto func_exit;
	}
	pscamif = (scamif_t *) hscamif;
	if (camid >= pscamif->camcount) {
		goto func_exit;
	}

	psc = &pscamif->scc[camid];
	pci  = &psc->imginfo;

	//--
	for (i = 1; i < 3; i++) {
		if ((normalbulk & i) == 0x01) {	// 0x01: Normal, 0x02: Bulk
			pbg = &psc->nb;		// 
		} else if ((normalbulk & i) == 0x02) {	// 0x01: Normal, 0x02: Bulk
			pbg = &psc->bb;		// 
		} else {
			continue;
		}
#define MUTEXTIMEOUT	100
		cr_mutex_wait(pbg->hmutex, MUTEXTIMEOUT);
		// buffer group
		pbg->valid 		= 0;
//		pbg->count		= 0;			// buffer count. Must be 2^n number.
//		pbg->windexb	= 0;			// next write index for buf; (0 ~ count-1)
//		pbg->rindexb	= 0;			// next read index for buf;  (0 ~ count-1)

		// buffergroupinfo
		pbibg 			= &pbg->buffergroupinfo;
		pbibg->state 	= SCAMBUFFERSTATE_EMPTY;
		pbibg->ts_h 	= 0;			//
		pbibg->ts_l 	= 0;			// nsec
		pbibg->frameindex = 0;

		memcpy(&pbibg->cii, pci, sizeof(camimageinfo_t)); 

		// Buffer...
		{
			U32 pixelcount;
			U32 datamemsize;
			U32 buffercount;
			U08 *pbuf;

			pixelcount = pci->width * pci->height;
			//datamemsize = SCAMIF_BUFFER_COUNT1 * SCAMIF_BUFFER_SIZE1;
			datamemsize = SCAMIF_BUFFER_SIZE_BUFFER;


			buffercount = SCAMIF_BUFFER_COUNT1;

			for (buffercount = SCAMIF_BUFFER_COUNT1; buffercount <= SCAMIF_BUFFER_COUNT16; buffercount *=2) {
				if (pixelcount * buffercount * 2 > datamemsize) {
					break;
				}
			}
			pbg->count = buffercount;


			pbuf = &pbg->datamem[0];
			for (i = 0; i < buffercount; i++) {		// reset buffer memory pointer and buffer info
				pbg->pbuffer[i] = pbuf;
				pbuf = pbuf + pixelcount;
				pbg->bif[i].state = SCAMBUFFERSTATE_EMPTY;
			}
//			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "buffer count: %d\n", buffercount);
			//pbg->count		= 0;			// buffer count. Must be 2^n number.
			pbg->windexb	= 0;			// next write index for buf; (0 ~ count-1)
			pbg->rindexb	= 0;			// next read index for buf;  (0 ~ count-1)
		}
		pbg->valid 	= 1;
		cr_mutex_release (pbg->hmutex);
		res = 1;
	}
func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Random access and Read Image and info, without mutex lock.
 *
 *  @param[in]	hscamif
 *              SCAMIF module handle
 *  @param[in]	camid
 *              camera id
 *  @param[in]	normalbulk
 *              0: NONE, 1: NORMAL, 2: BULK
 *  @param[in]	index
 *              Frame index
 *  @param[out]	ppb
 *            	Buffer info of group  
 *  @param[out]	ppbuf
 *            	image buffer
 *
 *  @return		1: GOOD. 0: FAIL.
 * 
 *	@author	    yhsuk
 *  @date       2016/11/24
 *******************************************************************************/
I32 scamif_imagebuf_randomaccess_lean(
		HAND hscamif, U32 camid, U32 normalbulk, U32 index, 
		camif_buffer_info_t **ppb, U08 **ppbuf)	
{
	I32 res;
	scamcam_t *psc;
	scamif_t *pscamif;
	//camimageinfo_t	*pci;
	camif_buffergroup_t *pbg; 		// 	buffer group
	U32 trindex;

	//--
	res = 0;
	if (hscamif == NULL) {
		goto func_exit;
	}
	pscamif = (scamif_t *) hscamif;
	if (camid >= pscamif->camcount) {
		goto func_exit;
	}

	psc = &pscamif->scc[camid];
	//pci  = &psc->imginfo;

	//--
	if (normalbulk == 0x01) {	// 0x01: Normal, 0x02: Bulk
		pbg = &psc->nb;		// 
	} else if (normalbulk == 0x02) {
		pbg = &psc->bb;		// 
	} else {
		res = 0;
		goto func_exit;
	}

	//cr_mutex_wait(pbg->hmutex, MUTEXTIMEOUT);
	res = scamif_imagebuf_trimindex(hscamif, camid, normalbulk, index, &trindex);
	if (res) {
		if (pbg->bif[trindex].state == SCAMBUFFERSTATE_FULL) {
			if (ppb) {
				*ppb = &pbg->bif[trindex];
			}

			if (ppbuf) {
				*ppbuf = pbg->pbuffer[trindex];
			}
			res = 1;
		}
		else res = 0;
	}
	//cr_mutex_release (pbg->hmutex);

func_exit:
	return res;
}

#endif

