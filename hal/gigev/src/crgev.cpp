/*!
 *******************************************************************************
                                                                                
                    CREATZ GEV
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   crgev.cpp
	 @brief  GEV
	 @author Original: by hgmoon, modified by yhsuk
	 @date   2015/11/16 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#if defined(_WIN32)
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <Winsock2.h>
#endif
#include <stdio.h>
#include <stdlib.h>


#include "crgev.h"
#include "crgev_message.h"

#include "cr_osapi.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
/*
#define DEFAULT_GVSP_HOSTPORT 15566
#define DEFAULT_GVSP_CAMPORT 3956
//#define DEFAULT_GVSP_MSGPORT 54377
#define DEFAULT_GVSP_MSGPORT 58280
*/

#define PACKET_SIZE	1456
#define RECEVE_PACKET_SIZE	1202

//#define THREADSTOPTRY	10
//#define THREADSTOPTRY	100
#define THREADSTOPTRY	500
#define THREADSTOPSLEEP	10

//#define SLEEP_SOCKET_CLOSE	50
#define SLEEP_SOCKET_CLOSE	10

#define MAXEXPOSURENUSEC		(100       * 1000 )	// 0.1sec
//#define MINEXPOSURENUSEC		(5                )	// 5 usec
#define MINEXPOSURENUSEC		(1                )	// ZERO .... -_-;

#define MAXDURATIONUSEC		(1 * (1000 * 1000))				// 1sec
#define MINDURATIONUSEC		(100)							// 100 usec -> 10 KHz

#define MAXFPS	10000					// 10000 fps? ..-_-;
#define MINFPS	1

#define MAXGAIN		(1000 * 1000)	// 1000_000
#define MINGAIN		(1          )	//

#define MAXWIDTH	1280
#define MAXHEIGHT	1024

#define INIT_FMC_ITER	4

//#define MAXBSIZE		(1280 * 1024 * 3)				// max 3 frames... 
#define MAXBSIZE		(1280 * 1024 * 16)				// max 32 frames... 
//#define MAXBSIZE		(1280 * 1024 * 32)				// max 32 frames... 

#define MUTEXTIMEOUT_GECP	1000

#define GUARDMEM	(64*1024)

#define USE_SEND_CTRL

#define GEV_REG_NUM 13


#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

//#define  VIDEOCB_THREAD
typedef struct _videocbcontext {
	CR_video_fnCB videoCB;

	void *pData;
	I32 nSize;
	U32 ts_high;
	U32 ts_low;
	I32 size_x;
	I32 size_y;
	I32 offset_x;
	I32 offset_y;
	U32 normalbulk;
	U32 skip;
	U32 multitude;
	void *pParam;
} videocbcontext_t;


enum {
	GECP_REG_CONTRAST	=	0x2800,
	GECP_REG_BRIGHT,	
	GECP_REG_GAMMA,
	GECP_REG_LUT,
	GECP_REG_DELAY_OFFSET,
	GECP_REG_LED_BRIGHT,
	GECP_REG_SKIP,
	GECP_REG_MULTITUDE,
	GECP_REG_SYNC_UPDATE,
	GECP_REG_HEARTBEAT_TIMEOUT,
	GECP_REG_GAIN,
	GECP_REG_EXPOSURE,	
	GECP_REG_EXPOSURE_MODE,
	GECP_REG_EXPOSURE_EXPIATE,
	GECP_REG_EXPOSURE_POST,
	GECP_REG_DURATION,
	GECP_REG_POSITION,
	GECP_REG_SIZE,
	GECP_REG_TIMER_MODE,
	GECP_REG_SYNC_DIV,
	GECP_REG_MAX_NUM
};



/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/
static U32 DummyRdBuf[16];

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/
//int gReqID = 1;


#pragma warning(disable:4505)
static void* GECP_create(HAND hgev, void *pParent);
static U32 GECP_create2(HAND hgev, void *pParent[], U32 streamid, U32 streamcount);
static void GECP_destroy(void *pHand);

static void *CR_ImgThreadFn(void *pParam);
static void *CR_MsgThreadFn(void *pParam);
/*static*/ void *CR_videoCbThreadFn(void *pParam);
#pragma warning(default:4505)

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

//int SendPacket(BYTE *sBuff, int sLen, BYTE *rBuff);
/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: local functions
 -----------------------------------------------------------------------------*/

static I32 writeResister(void *pHand, U32 regName, U32 *value)
{
	I32 res = 0;
	BYTE *rBuff = NULL;
	T_GVCP_WRITE_LIST regList[4];
	T_GVCP_MAIN *pMainHand;
	T_GVCP_STREAM *pgvcpstr;
	U32			streamid;
	I32 regCount;

	//---
	if (pHand == NULL) {
		res = 0;
		goto func_exit;
	}

	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;
	if (pMainHand == NULL) {
		return 0;
	}

	streamid = pgvcpstr->m_streamid;

	rBuff = new BYTE[1202];

	regCount = 1;

	switch(regName)
	{
		case GECP_REG_CONTRAST:
			regList[0].addr = GVCP_VR_IMG_CONTRAST_C(streamid);
			regList[0].value = value[0];
			break;

		case GECP_REG_BRIGHT:
			regList[0].addr = GVCP_VR_IMG_BRIGHT_C(streamid);
			regList[0].value = value[0];
			break;

		case GECP_REG_GAMMA:
			regList[0].addr = GVCP_VR_IMG_GAMMA_C(streamid);
			regList[0].value = value[0];
			break;

		case GECP_REG_LUT:
			regCount = 3;
			regList[0].addr = GVCP_VR_IMG_BRIGHT_C(streamid);
			regList[0].value = value[0];
			regList[1].addr = GVCP_VR_IMG_CONTRAST_C(streamid);
			regList[1].value = value[1];
			regList[2].addr = GVCP_VR_IMG_GAMMA_C(streamid);
			regList[2].value = value[2];
			break;				

		case GECP_REG_DELAY_OFFSET:
			regList[0].addr = GVCP_VR_EXPOSURE_DELAYOFFSET_C(streamid);
			regList[0].value = value[0];
			break;		

		case GECP_REG_LED_BRIGHT:
			regList[0].addr = GVCP_VR_LED_BRIGHTNESS_C(streamid);
			regList[0].value = value[0];
			break;		

		case GECP_REG_SKIP:
			regList[0].addr = GVCP_VR_SKIP_C(streamid);;
			regList[0].value = value[0];
			break;	

		case GECP_REG_MULTITUDE:
			regList[0].addr = GVCP_VR_MULTITUDE;
			regList[0].value = value[0];
			break;	

		case GECP_REG_SYNC_UPDATE:
			regList[0].addr = GVCP_VR_SYNC_UPDATE_C(0);			
			regList[0].value = value[0];
			break;	

	
		case GECP_REG_HEARTBEAT_TIMEOUT:
			regList[0].addr = GVCP_HEARTBEAT_TIMEOUT;			
			regList[0].value = value[0];
			break;	

		case GECP_REG_GAIN:			
			regList[0].addr = GVCP_VR_IMG_GAIN_C(streamid);
			regList[0].value = value[0];
			break;	

		case GECP_REG_EXPOSURE: 		
			regList[0].addr = GVCP_VR_SHUTTER_C(streamid);
			regList[0].value = value[0];
			break;

		case GECP_REG_EXPOSURE_MODE: 		
			regList[0].addr = GVCP_VR_EXPOSURE_MOD_C(streamid);
			regList[0].value = value[0];
			break;

		case GECP_REG_EXPOSURE_EXPIATE:			
			regList[0].addr = GVCP_VR_EXPOSURE_EXPIATE_C(streamid);;
			regList[0].value = value[0];
			break;

		case GECP_REG_EXPOSURE_POST: 		
			regList[0].addr = GVCP_VR_EXPOSURE_POST_C(streamid);
			regList[0].value = value[0];
			break;			

		case GECP_REG_DURATION: 		
			regList[0].addr = GVCP_VR_DURATION_C(streamid);
			regList[0].value = value[0];
			break;			
			
		case GECP_REG_POSITION: 		
			regCount = 2;
		
			regList[0].addr = GVCP_VR_IMG_OFFX_C(streamid);
			regList[0].value = value[0];
			
			regList[1].addr = GVCP_VR_IMG_OFFY_C(streamid);
			regList[1].value = value[1];
			break;

		case GECP_REG_SIZE: 		
			regCount = 2;

			regList[0].addr = GVCP_VR_IMG_WIDTH_C(streamid);
			regList[0].value = value[0]; // Width

			regList[1].addr = GVCP_VR_IMG_HEIGHT_C(streamid);
			regList[1].value = value[1];	// Height
			break;

		case GECP_REG_TIMER_MODE: 		
			//regList[0].addr = GVCP_VR_TIMER_CMD;
			regList[0].addr = GVCP_VR_TIMER_CMD_C(streamid);
			regList[0].value = value[0];
			break;	

		case GECP_REG_SYNC_DIV:		
			regList[0].addr = GVCP_VR_EXT_DIVIDER_C(streamid);
			regList[0].value = value[0];
			break;				

		default:
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" regName[0x%x] is not supported!\n", regName);
			goto func_exit;
	}


	res = GECP_regWrite(pHand, regList, regCount, rBuff, 1202);

func_exit:
	if(rBuff) {
		delete rBuff;
	}
	
	return res;
}

 

static I32 readCommand(void *pHand, U32 *rdata, U32 count, U32 command)
{
	I32 res;
	U32 sbuf[512];
	U32 rbuf[512];
	//---
	if (pHand == NULL) {
		res = 0;
		goto func_exit;
	}

	res = GECP_crcmdWrite(pHand, command, count, sbuf, rbuf, NULL);

	if (count >  CRCMD_PARAMCOUNT*sizeof(U32)) {
		count =  CRCMD_PARAMCOUNT*sizeof(U32);
	}	

	memcpy(rdata, &rbuf[0], count);

func_exit:
	return res;
}

static I32 writeCommand(void *pHand, U32 *sdata, U32 count, U32 command, U32 *rdata, U32 *pStatus)
{
	I32 res;
	U32 sbuf[512];
	U32 rbuf[512];
	
	//---
	if (pHand == NULL) {
		res = 0;
		goto func_exit;
	}

	if (count >  CRCMD_PARAMCOUNT*sizeof(U32)) {
		count =  CRCMD_PARAMCOUNT*sizeof(U32);
	}
	
	if(count > 0) {
		memcpy(&sbuf[0], sdata, count);
	}
	if(rdata != NULL) {
		res = GECP_crcmdWrite(pHand, command, count, sbuf, rbuf, pStatus);
		if(command == CRCMD_CACHE_CHECMSUM) {
			rdata[0] = rbuf[0];
		}
	} else {
		res = GECP_crcmdWrite(pHand, command, count, sbuf, NULL, pStatus);	// rbuf == NULL : Do not wait for ACK.
	}

func_exit:
	return res;
}



/*!
 ********************************************************************************
 *	@brief      Thread function of 
 *
 *  @return		TRUE if successful, FALSE otherwise
 *
 *	@note This code uses IP helper functions from Microsoft. This function must be
 *	               called before calling _SetIpAddress().
 *	@author	    yhsuk
 *  @date       2015/1116
 *******************************************************************************/

void *CR_MsgThreadFn(void *pParam)
{
	SOCKET soGVSP = INVALID_SOCKET;	// Stream channel socket
	struct sockaddr_in serveraddr, cliaddr;	
	////////////	BYTE p_fmt;
	BYTE tmpBuff[RECEVE_PACKET_SIZE];
	I32 pid;
	I32 bsize = 0;
	I32 len;

	//
	//////////////////	T_GVSP_DATA_LEADER *header;
	//	String str;
	//	WSADATA wsaData;
	SOCKET RecvSocket;

	T_GVCP_MAIN *pMainHand;

	pMainHand = (T_GVCP_MAIN *)pParam;
	__try { 				// __try / __except().. 20180912

		soGVSP = socket(PF_INET, SOCK_DGRAM, 0);

		//	memset(gImage.rcv_img, 0x80, 1280 * 1024);
		//	bsize = 1280 * 1024 * 3;
		//	setsockopt(soGVSP, SOL_SOCKET, SO_RCVBUF, (char *)&bsize, 4);

		serveraddr.sin_family = AF_INET;
		serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
		serveraddr.sin_port = htons((u_short)pMainHand->m_msgPort);

		bind(soGVSP, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
		len = sizeof(cliaddr);


		{
			//#define WAITTIME_MSG_MS	10
#define WAITTIME_MSG_MS	100
			I32 optval = WAITTIME_MSG_MS;
			setsockopt(soGVSP, SOL_SOCKET, SO_RCVTIMEO, (char *)&optval, sizeof(I32));
		}

		while (pMainHand->m_nMsgStop == 0)
		{
			if (pMainHand->m_msgPort != 0) {
				bsize = recvfrom(soGVSP, (char *)tmpBuff, RECEVE_PACKET_SIZE, 0, (struct sockaddr *)&cliaddr, &len);


				if (pMainHand->m_msgCB != NULL)
					pMainHand->m_msgCB(tmpBuff, bsize, pMainHand->m_pMsgParam);
			}
			if (pMainHand->m_nMsgStop)
				break;

			////		GECP_heartbeat(pMainHand, 3000);
			////		GECP_heartbeat(pMainHand, 1000000);
			//#define SCAMIF_HEARTBEATTIME_MS	(10 * 1000)
			//		GECP_heartbeat(pMainHand, SCAMIF_HEARTBEATTIME_MS);

			//{
			//	int i;
			//	I32 res;
			//	T_GVCP_READ_LIST regList[32];
			//	U08 versionStr[32+1];
			//	U32 itmp;

			//	for (i = 0; i < 32/sizeof(U32); i++) {
			//		regList[i].addr = GVCP_DEVICE_VERSION + i*sizeof(U32);
			//	}

			//	res = GECP_regRead(pMainHand, &regList[0], 32/sizeof(U32));
			//	if (res > 0) {

			//		for (i = 0; i < 32/sizeof(U32); i++) {
			//			itmp = regList[i].value;
			//			versionStr[i*4+0] = (U08) ((itmp >>  0) &0xFF);
			//			versionStr[i*4+1] = (U08) ((itmp >>  8) &0xFF);
			//			versionStr[i*4+2] = (U08) ((itmp >> 16) &0xFF);
			//			versionStr[i*4+3] = (U08) ((itmp >> 24) &0xFF);
			//		}
			//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "version:  %s\n", versionStr);
			//	} else {
			//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "version RegRead Fail..\n");
			//	}
			//}

			cr_sleep(100);
		}

		closesocket(soGVSP);
		pMainHand->m_hMsgThread = NULL;
	} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
			|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
			){
		// .... :)
	}


	{
		pid;
		RecvSocket;
	}
	return NULL;
}




void *CR_ImgThreadFn(void *pParam)
{
	SOCKET soGvsp = INVALID_SOCKET;	// Stream channel socket
	//struct sockaddr_in serveraddr, cliaddr;
	struct sockaddr_in cliaddr;
	BYTE p_fmt;
	BYTE tmpBuff[PACKET_SIZE + 100];
	I32 pid;
	//I32 bsize = 0;
	I32 len, n, tPack;
	long long rTime;				// , diff, tol;
	T_RCV_CONTROL *Image;
	//
	T_GVSP_DATA_LEADER *header;
	T_GVCP_STREAM 	*pgvcpstr;
	T_GVCP_MAIN 	*pMainHand;
	T_GVSP			*pgvsp;
	U32 			streamid;
	I32 continueloop;
	BYTE *prcv_img;
	U32 rcvFrameindex;
	U32 lcount;

//	String str;
//	WSADATA wsaData;
/////////////////	SOCKET RecvSocket;


	pgvcpstr 	= (T_GVCP_STREAM *)pParam;
	streamid 	= pgvcpstr->m_streamid;
	pMainHand 	= pgvcpstr->m_pgvcpm;
	pgvsp		= &pMainHand->m_gvsp[streamid];

	Image = &(pgvsp->m_Image);
	//-----------------------------------------------
	// Initialize Winsock
//	WSAStartup(MAKEWORD(2, 2), &wsaData);


	//soGVSP = socket(PF_INET, SOCK_DGRAM, 0);

//	memset(gImage.rcv_img, 0x80, 1280 * 1024);
	tPack = 0;
//#define LCOUNT 10
	lcount = 0;
	while (pgvsp->m_nCapStop == 0) {
		/*
		if (Image->normalbulk == 2) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "tPack: %d    #1\n", tPack);
		}
		*/
#if defined(LCOUNT)
		if (!(lcount++%LCOUNT)) {
			cr_sleep(1);
		}
#endif
		rcvFrameindex 	= Image->rcvFrame % RCV_IMG_COUNT;
		prcv_img = &Image->rcv_img[rcvFrameindex][0];


		continueloop = 1;
		n = 0;
		if (pgvsp->m_isGvspActive) {
			soGvsp = pgvsp->m_soGvsp;					// GVSP host socket
			if (soGvsp == INVALID_SOCKET) {
#pragma warning(disable:4456)
				int len;
#pragma warning(default:4456)
				struct sockaddr_in  *pAddrGvsp;
				I32 bsize = 0;
				I32 res;

				soGvsp = socket(PF_INET, SOCK_DGRAM, 0);
				if (soGvsp == INVALID_SOCKET) {
					continue;
				}

				len = sizeof(bsize);
				getsockopt(soGvsp, SOL_SOCKET, SO_RCVBUF, (char *)&bsize, &len);
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] rcvbuf size: %d\n", soGvsp, bsize);

				bsize = MAXBSIZE;
				//setsockopt(soGvsp, SOL_SOCKET, SO_RCVBUF, (char *)&bsize, 4);
				setsockopt(soGvsp, SOL_SOCKET, SO_RCVBUF, (char *)&bsize, sizeof(bsize));

				len = sizeof(bsize);
				getsockopt(soGvsp, SOL_SOCKET, SO_RCVBUF, (char *)&bsize, &len);
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] --> rcvbuf size: %d\n", soGvsp, bsize);
				len;


				bsize = 1;          // SO_REUSEADDR 의 옵션 값을 TRUE 로
				setsockopt(soGvsp, SOL_SOCKET, SO_REUSEADDR, (char *)&bsize, sizeof(bsize));

				pAddrGvsp 					= &pgvsp->m_sAddrGvsp;
				pAddrGvsp->sin_family 		= AF_INET;
				pAddrGvsp->sin_port 		= htons((u_short) pgvsp->m_hostGvspPort);
				pAddrGvsp->sin_addr.s_addr 	= htonl(INADDR_ANY);

				res = bind(soGvsp, (struct sockaddr *)pAddrGvsp, sizeof(struct sockaddr_in));

				pgvsp->m_soGvsp = soGvsp;					// GVSP host socket
				{
#define WAITTIME_VS_MS	100
//#define WAITTIME_VS_MS	10
//#define WAITTIME_VS_MS	1
					I32 optval = WAITTIME_VS_MS;
					setsockopt(soGvsp, SOL_SOCKET, SO_RCVTIMEO, (char *)&optval, sizeof(I32));
				}
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "INVALID_SOCKET\n");
			}

			/*
			if (Image->normalbulk == 2) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "tPack: %d    #2\n", tPack);
			}
			*/
			if (soGvsp != INVALID_SOCKET) {
				len = sizeof(struct sockaddr);
				n = recvfrom(soGvsp, (char *)tmpBuff, PACKET_SIZE + 100, 0, (struct sockaddr *)&cliaddr, &len);
				//n = recvfrom(soGvsp, (char *)tmpBuff, PACKET_SIZE, 0, (struct sockaddr *)&cliaddr, &len);
				/*
				if (Image->normalbulk == 2) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "recvfrom: %d vs n %d\n", PACKET_SIZE + 100, n);
				}
				*/
				if (n < 0) {
					int ecode;
					ecode = WSAGetLastError();
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ecode: %d\n", ecode);
#if 0
					if (soGvsp != INVALID_SOCKET) {
						closesocket(soGvsp);
						pgvsp->m_soGvsp = INVALID_SOCKET;
						soGvsp = INVALID_SOCKET;
					}
#endif
					///////   printf("ecode: %d", ecode);
				}
				continueloop = 0;
			}
		} else {
			soGvsp = pgvsp->m_soGvsp;					// GVSP host socket
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": GVSP socket close... %d\n", soGvsp);

			if (soGvsp != INVALID_SOCKET) {
				closesocket(soGvsp);
				pgvsp->m_soGvsp = INVALID_SOCKET;
				soGvsp = INVALID_SOCKET;
			}
		}
		
		if (pgvsp->m_nCapStop) {
			break;
		}

		if (continueloop) {
#define SLEEP_GVSP0		10					// sleep and continue.. :)
			cr_sleep(SLEEP_GVSP0);
			continue;
		}

		/*
		if (Image->normalbulk == 2) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "tPack: %d    #2-1\n", tPack);
		}
		*/
		if (n > 0) {
			p_fmt = tmpBuff[4];

			pid = tmpBuff[5];
			pid = (pid << 8) | tmpBuff[6];
			pid = (pid << 8) | tmpBuff[7];
			//str = "F:"+IntToStr(p_fmt)+", P:"+IntToStr(pid)+", L:"+IntToStr(AData.Length); Memo1->Lines->Add(str);

			if (p_fmt == GVSP_PACKET_LEADER) // leader
			{
				header = (T_GVSP_DATA_LEADER *)tmpBuff;

				Image->ts_high = ntohl(header->ts_high);
				Image->ts_low = ntohl(header->ts_low);

				Image->size_x = ntohl(header->size_x);
				Image->size_y = ntohl(header->size_y);

				Image->offset_x = ntohl(header->offset_x);
				Image->offset_y = ntohl(header->offset_y);


				{
					U16 pad_x, pad_y;
					U32 normalbulk;
					U32 skip;
					U32 multitude;

					pad_x = ntohs(header->pad_x);
					pad_y = ntohs(header->pad_y);


					// (multitudeImage & 0x0F) | ((gVideoCntr.sendSkip << 4) & 0xF0) | ((gVideoCntr.runmode == 2) ? (1 << 8) : 0);

					multitude	= pad_x & 0x0F;
					skip		= (pad_x >> 4) & 0x0F;
					normalbulk	= (pad_x >> 8) & 0x01;		// bulk?
					if (normalbulk) {
						normalbulk = 2;						// BULK mode: 2
					} else {
						normalbulk = 1;						// normal mode: 1
					}
					Image->normalbulk	= normalbulk;
					Image->multitude	= multitude;
					Image->skip			= skip;
				}

				Image->bID = ntohs(header->header.block_id);

				rTime = ntohl(header->ts_high);
				rTime = (rTime << 32) | ntohl(header->ts_low);
				/*
				if (cbDur->Checked && (Image->prcvTime != 0))
				{
					diff = (rTime - Image->prcvTime);
					//str = "T:"+IntToStr(diff); Memo1->Lines->Add(str);

					tol = 1e9 / edFPS->Value;
					if ((diff > (tol + 100000)) || (diff <(tol - 100000)))
					{
						str = "T:" + IntToStr(diff); Memo1->Lines->Add(str);
					}

				}
				*/

				Image->prcvTime = rTime;
				if ((Image->prv_bID + 1) != Image->bID)
				{
					//str = "H:" + IntToStr(Image->prv_bID) + "," + IntToStr(Image->bID); Memo1->Lines->Add(str);
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "streamid: %d,   prev bID:  %d,  bID: %d\n", streamid, Image->prv_bID,  Image->bID);
				}

				if (Image->bID == 0xFFFF) Image->prv_bID = 0;
				else Image->prv_bID = Image->bID;

				tPack = 1;
				/*
				if (Image->normalbulk == 2) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "tPack: %d    #3\n", tPack);
				}
				*/
			}

			else if (p_fmt == GVSP_PACKET_PAYLOAD) // data
			{
				int readsize;
				if ((Image->prv_bID != Image->bID) && (Image->bID != 0xFFFF))
				{
					//str = "D:" + IntToStr(Image->prv_bID) + ",:" + IntToStr(Image->bID); Memo1->Lines->Add(str);
				}

				readsize = n - 8;
				if (readsize > PACKET_SIZE - 8) {
					readsize = PACKET_SIZE - 8;
				}
				//memcpy(&Image->rcv_img[(pid - 2)*(PACKET_SIZE - 8)], &tmpBuff[8], readsize);
				memcpy(&prcv_img[(pid - 2)*(PACKET_SIZE - 8)], &tmpBuff[8], readsize);
				//				memcpy(&Image->rcv_img[(pid - 2)*(PACKET_SIZE - 8)], &tmpBuff[8], PACKET_SIZE - 8);


				/*
				if (Image->normalbulk == 2) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "tPack: %d, readsize: %d\n", tPack, readsize);
				}
				*/


				tPack++;
#if 0
				if ((Image->prv_bID != Image->bID) && (Image->bID != 0xFFFF))
				{
					//str = "D:" + IntToStr(Image->prv_bID) + ",:" + IntToStr(Image->bID); Memo1->Lines->Add(str);
				}
				memcpy(&Image->rcv_img[(pid - 2)*(PACKET_SIZE - 8)], &tmpBuff[8], PACKET_SIZE - 8);
				tPack++;
#endif
				/*
				if (Image->normalbulk == 2) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "tPack: %d    #4\n", tPack);
				}
				*/
			}
			else if (p_fmt == GVSP_PACKET_TRAILER) // trailer
			{
				Image->pID = pid;
				tPack++;
				Image->tPack = tPack;
				Image->rcvFrame++;
				//str = "F:"+IntToStr(p_fmt)+", P:"+IntToStr(pid)+", L:"+IntToStr(gImage.tPack); Memo1->Lines->Add(str);
				//str = "R:"+IntToStr(gImage.rcvFrame); Memo1->Lines->Add(str);
//				if (Image->rcv == 0)
				{
//					Image->rcv = 1;

					if (pgvsp->m_nCapStop)
						break;

					if (pgvsp->m_videoCB != NULL) {
						//pMainHand->m_videoCB(Image->rcv_img, 1280 * 1024, Image->size_x, Image->size_y, pgvsp->m_hParent);
#if defined(VIDEOCB_THREAD)
						videocbcontext_t *pvct;
						
						//--
						pvct = (videocbcontext_t *) malloc(sizeof(videocbcontext_t));

						pvct->videoCB 		= pgvsp->m_videoCB;
						//pvct->pData 		= Image->rcv_img;
						pvct->pData 		= prcv_img;
						pvct->nSize 		= (Image->size_x * Image->size_y);
						pvct->ts_high 		= Image->ts_high;
						pvct->ts_low 		= Image->ts_low;

						pvct->size_x 		= Image->size_x;
						pvct->size_y 		= Image->size_y;
						pvct->offset_x 		= Image->offset_x;
						pvct->offset_y 		= Image->offset_y;

						pvct->normalbulk	= Image->normalbulk;
						pvct->skip 			= Image->skip;
						pvct->multitude 	= Image->multitude;
						pvct->pParam 		= pMainHand->m_hParent[streamid];

						CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)CR_videoCbThreadFn, pvct, 0, NULL);
						//SetThreadPriority();
#else

						/*
						if (Image->normalbulk == 2) {
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "tPack: %d, nsize: %d (%d * %d)\n", tPack, 
								(Image->size_x * Image->size_y),
								Image->size_x, Image->size_y					
							);
						}
						*/
						pgvsp->m_videoCB(
								prcv_img,										// Image->rcv_img, 
								(Image->size_x * Image->size_y),				// (1280 * 1024),
								Image->ts_high,		Image->ts_low,
								Image->size_x,		Image->size_y,
								Image->offset_x,	Image->offset_y,

								Image->normalbulk,	Image->skip, Image->multitude,
								pMainHand->m_hParent[streamid]);
#endif
						//memcpy(Image->disp_img, gImage.rcv_img, 1280 * 1024);
						//PostMessage(Handle, APPM_NOTIFY_DISPLAY, 0, 0);
					}
				}
				/*
				if (Image->normalbulk == 2) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "tPack: %d    #5\n", tPack);
					//cr_sleep(5);
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "After Sleep\n");
				}
				*/
			}
		} else {
//			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "_b s\n");
			cr_sleep(1);				// Zero sleep :)
//			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "_a s\n");
		}
	}
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" m_nCapStop: %d\n",  pgvsp->m_nCapStop);
			
	if (soGvsp != INVALID_SOCKET) {
		closesocket(soGvsp);
		pgvsp->m_soGvsp = INVALID_SOCKET;
	}
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" After closesocket\n");

	pgvsp->m_hCaptureThread = NULL;
	return NULL;
}


I32 GECP_regWrite(void *pHand, T_GVCP_WRITE_LIST *regList, I32 regCount, BYTE *rBuff, I32 rSize)
{

	BYTE *sBuff;
	T_GVCP_HEADER *header;
	I32 rLen, i;
	//I32 i;
	U32 *rData;

	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;
	I32 res;

	//--
	if (pHand == NULL) {
//		return 0;
		return -1;
	}

	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;
	if (pMainHand == NULL) {
		return -1;
	}
	if (pMainHand->m_soGvcp == INVALID_SOCKET) {
		return -1;
	}

//	pMainHand = (T_GVCP_MAIN *)pHand;

	sBuff = new BYTE[sizeof(T_GVCP_HEADER) + regCount * 8];

	rData = (U32 *)&sBuff[sizeof(T_GVCP_HEADER)];

	header = (T_GVCP_HEADER *)sBuff;
	header->hard_key = GVCP_KEY;
	header->flag = 0x80;
	header->command = htons(GVCP_WRITEREG_CMD);
	header->length = htons((u_short)(regCount * 8));

//	pMainHand = (T_GVCP_MAIN *)pHand;
	cr_mutex_wait(pMainHand->m_hmutex, MUTEXTIMEOUT_GECP);

	header->req_id = htons((u_short)(pMainHand->m_nReqId));


//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-------------\n");
//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "pHand: %08x\n", (U32) pHand);
	for (i = 0; i < regCount; i++)
	{
		rData[i * 2 + 0] = htonl(regList[i].addr);
		rData[i * 2 + 1] = htonl(regList[i].value);

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%08x:%08x\n", (int)rData[i * 2 + 0], (int)rData[i * 2 + 1]);

	}
//__debugbreak();

	if (++pMainHand->m_nReqId == 0x0) pMainHand->m_nReqId = 1;
	if (((u_short)(++pMainHand->m_nReqId)) == 0x0) pMainHand->m_nReqId = 1;

	//	rLen = SendPacket(sBuff, sizeof(T_GVCP_HEADER) + regCount * 8, rBuff);
	//	GevSendMessage(UINT8 *pDatagram, int size, SOCKET *pSocket, struct sockaddr_in *pDest, BOOL showData);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Before GevSendMessage \n", (U32)pHand);
	rLen = GevSendMessage(sBuff, sizeof(T_GVCP_HEADER) + regCount * 8, &(pMainHand->m_soGvcp), &(pMainHand->m_sAddrGvcp), true, rBuff, rSize);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "After  GevSendMessage \n", (U32)pHand);
	cr_mutex_release (pMainHand->m_hmutex);
	//ShowMessage(IntToStr(rLen));



	if (rLen == 12) {
		res = 1;
	}
	else {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"  regCount: %d,  %08x-%08x, regList[0] %08x-%08xrLen: %d\n", regCount, rData[0], rData[1], regList[0].addr, regList[0].value, rLen);
		res = -1;
	}

	delete sBuff;
	//#define WRITE_SLEEP	20

#if defined(WRITE_SLEEP)
	cr_sleep(WRITE_SLEEP);
#endif


#if 0
	{
		I32 res0;
		T_GVCP_WRITE_LIST rregList[1024];
		memset(&rregList, 0, sizeof(rregList));
		for (i = 0; i < regCount; i++)
		{
			rregList[i].addr = regList[i].addr;
			rregList[i].value = 0x00;
		}

		res0 = GECP_regRead(pHand, &rregList[0], regCount);

		for (i = 0; i < regCount; i++)
		{
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d] (0x%08x, 0x%08x) -> (0x%08x, 0x%08x)\n",
				i,
				regList[i].addr,
				regList[i].value,
				rregList[i].addr,
				rregList[i].value);
		}



		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "regCount: %d, res0: %d\n", regCount, res0);

	}
#endif
	return res;
}

I32 GECP_regRead(void *pHand, T_GVCP_READ_LIST *regList, I32 regCount)
{
	BYTE *sBuff,*rBuff;
	T_GVCP_HEADER *header;
	I32 rLen,i;
	U32 *rData;
	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;

	I32 rSize;

	//--
//	__debugbreak();
	if (pHand == NULL) {
		return -1;
	}

	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;
	if (pMainHand == NULL) {
		return -1;
	}
	if (pMainHand->m_soGvcp == INVALID_SOCKET) {
		return -1;
	}


	sBuff = new BYTE[sizeof(T_GVCP_HEADER)+regCount*4];
	rBuff = new BYTE[1202];


	rData = (U32 *)&sBuff[sizeof(T_GVCP_HEADER)];

	rSize =  (I32) (sizeof(T_GVCP_HEADER)+regCount*4);
	for (i = 0; i < regCount; i++) {
		rData[i] = htonl(regList[i].addr);
	}


	header =(T_GVCP_HEADER *)sBuff;
	header->hard_key = GVCP_KEY;
	header->flag = 0x80;
	header->command = htons(GVCP_READREG_CMD);
	header->length =  htons((u_short) (regCount*4));

	pMainHand = (T_GVCP_MAIN *)pHand;
	cr_mutex_wait(pMainHand->m_hmutex, MUTEXTIMEOUT_GECP);
	header->req_id = htons((u_short) pMainHand->m_nReqId);

	if (++pMainHand->m_nReqId == 0x0) pMainHand->m_nReqId = 1;

//	rLen = SendPacket(sBuff,sizeof(T_GVCP_HEADER)+regCount*4,rBuff);
//	GevSendMessage(UINT8 *pDatagram, int size, SOCKET *pSocket, struct sockaddr_in *pDest, BOOL showData);
	rLen = GevSendMessage(sBuff, sizeof(T_GVCP_HEADER) + regCount * 4, &(pMainHand->m_soGvcp), &(pMainHand->m_sAddrGvcp), true, rBuff, rSize);
	cr_mutex_release(pMainHand->m_hmutex);


	delete sBuff;

	if (rLen !=  rSize) {
		delete rBuff;
		return -1;
	}

	rData = (U32 *)&rBuff[sizeof(T_GVCP_HEADER)];
	for (i = 0; i < regCount; i++)
	{
		regList[i].value= htonl(rData[i]);
	}

	delete rBuff;
	return rLen;
}


I32 GECP_crcmdWrite(void *pHand, U32 cmd, U32 count, U32 *data, U32 *rdata, U32 *status)
		// rdata == NULL: DO NOT wait nor receive response..
{
	I32 res;
	U32 i;
	U32 status0;
	U08 *sBuff, *rBuff;
	T_GVCP_HEADER *header;
	I32 rLen;
	U32 *pbuf;
	U08 *pd8;
	I32 len;
	I32 len2;

	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;

//	U32 cmd2;

	//--
	if (pHand == NULL) {
		return -1;
	}

	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;
	if (pMainHand == NULL) {
		status0 = GEV_STATUS_ERROR;
		res = -1;
		goto func_exit;
	}
	if (pMainHand->m_soGvcp == INVALID_SOCKET) {
		status0 = GEV_STATUS_ERROR; 
		res = -1;
		goto func_exit;
	}

	len = (CRCMD_PARAMCOUNT+1)*sizeof(U32);				// contains cmd.
	len2 = sizeof(T_GVCP_HEADER) + len;
		
	sBuff = (U08 *) malloc(len2);
	rBuff = (U08 *) malloc(1202);		// what?
//	sBuff = new BYTE[len2];
//	rBuff = new BYTE[1202];

	pbuf = (U32 *)&sBuff[sizeof(T_GVCP_HEADER)];
	pd8 = (U08 *)pbuf;

	header = (T_GVCP_HEADER *)sBuff;
	header->hard_key = GVCP_KEY;
	header->flag = 0x80;
	header->command = htons(GVCP_CRCMD_CMD);
	header->length = htons((u_short)(len));
				
	cr_mutex_wait(pMainHand->m_hmutex, MUTEXTIMEOUT_GECP);
	header->req_id = htons((u_short)(pMainHand->m_nReqId));
	if (++pMainHand->m_nReqId == 0x0) pMainHand->m_nReqId = 1;
	if (((u_short)(++pMainHand->m_nReqId)) == 0x0) pMainHand->m_nReqId = 1;

	// Make Payload of CRMCD
	//cmd2 = (count << 16 | cmd);
	//*pdata = htonl(cmd2);
	*(U16 *)pd8 		= htons((U16)count);			// data count
	*(U16 *)(pd8+2) 	= htons((U16)cmd);			// sub command.
	//memcpy((pdata+1), data, sizeof(U32)*CRCMD_PARAMCOUNT);

	for (i = 0; i < (count+3)/4; i++) {
		U32 itmp;
		itmp = data[i];
		*(pd8 + 4 + i*4 + 0) = (U08) (itmp >> 24);
		*(pd8 + 4 + i*4 + 1) = (U08) (itmp >> 16);
		*(pd8 + 4 + i*4 + 2) = (U08) (itmp >>  8);
		*(pd8 + 4 + i*4 + 3) = (U08) (itmp >>  0);
	}
	
	if (rdata) {
		rLen = GevSendMessage(sBuff, sizeof(T_GVCP_HEADER) + len, &(pMainHand->m_soGvcp), &(pMainHand->m_sAddrGvcp), true, rBuff, len2);
	} else {
		rLen = GevSendMessageOnly(sBuff, sizeof(T_GVCP_HEADER) + len, &(pMainHand->m_soGvcp), &(pMainHand->m_sAddrGvcp), true);
	}
	cr_mutex_release (pMainHand->m_hmutex);

	if (rdata) {
		pbuf = (U32 *)(&rBuff[0] + sizeof(T_GVCP_HEADER) + sizeof(U32));
		pd8 = (U08 *)pbuf;
		if (rLen == len2) {
			header = (T_GVCP_HEADER *) rBuff;
			if (header->command == htons(GVCP_CRCMD_ACK)) { // good.
				res = 1;
				status0 = htons(*(U16 *)rBuff);			// must check status..
				if (status0 == GEV_STATUS_SUCCESS) {
					for (i = 0; i < (count+3)/4; i++) {
						U32 itmp;
						//itmp  = *(pd8 + 4 + i*4 + 0);
						//itmp <<=8;
						//itmp |= *(pd8 + 4 + i*4 + 1);
						//itmp <<=8;
						//itmp |= *(pd8 + 4 + i*4 + 2);
						//itmp <<=8;
						//itmp |= *(pd8 + 4 + i*4 + 3);

						itmp  = *(pd8 + i*4 + 0);
						itmp <<=8;
						itmp |= *(pd8 + i*4 + 1);
						itmp <<=8;
						itmp |= *(pd8 + i*4 + 2);
						itmp <<=8;
						itmp |= *(pd8 + i*4 + 3);

						rdata[i] = itmp;
					}
					//memcpy(rdata, pbuf, len - sizeof(U32));
				}
			} else {
				res = -1;
				status0 = GEV_STATUS_ERROR;
			}
		} else {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"  rLen: %d   != %d\n", rLen, len2);
			status0 = GEV_STATUS_ERROR;
			res = -1;
		}

	} else {
		if (rLen > 0) {
			res = 1;
			status0 = GEV_STATUS_SUCCESS;
		} else {
			res = 0;
			status0 = GEV_STATUS_ERROR;
		}
	}
	free(sBuff);
	free(rBuff);
//	delete sBuff;
//	delete rBuff;

func_exit:
	if (status) {
		*status = status0;
	}
	return res;
}
//------------------------------------------------
I32 GECP_memWrite(void *pHand, U32 addr, U32 count, U32 *data, U32 *status)
{
	I32 res;

	U32 i;
	I32 len, len2;
	I32 rlen, rlen2, rlenRes;

	U08 *sBuff, *rBuff;
	U08 *pd8;
	U32 itmp;
	
	T_GVCP_HEADER *header;

	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;
	//----
	if (pHand == NULL) {
		*status = GEV_STATUS_ERROR;
		res = -1;
		goto func_exit;
	}
	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;
	if (pMainHand == NULL) {
		*status = GEV_STATUS_ERROR;
		res = -1;
		goto func_exit;
	}
	if (pMainHand->m_soGvcp == INVALID_SOCKET) {
		*status = GEV_STATUS_ERROR;
		res = -1;
		goto func_exit;
	}

	// Make Buffer.
	len = sizeof(U32) + count;				// address
	len2 = sizeof(T_GVCP_HEADER) + len;

	rlen = sizeof(U32);
	rlen2 = sizeof(T_GVCP_HEADER) + rlen;

	sBuff = (U08 *) malloc(((len2+3)/4)*4  +1024*1024);
	if (sBuff == NULL) {
		*status = GEV_STATUS_ERROR;
		res = -1;
		goto func_exit;
	}
	pd8 = sBuff + sizeof(T_GVCP_HEADER);

	rBuff = (U08 *) malloc(rlen2+1024*1024);
	if (rBuff == NULL) {
		free(sBuff);
		*status = GEV_STATUS_ERROR;
		res = -1;
		goto func_exit;
	}


	// Make Header
	header = (T_GVCP_HEADER *)sBuff;
	header->hard_key = GVCP_KEY;
	header->flag = 0x80;			// ACKNOWLEDGE flag (bit7) should be set.
	header->command = htons(GVCP_WRITEMEM_CMD);
	header->length = htons((u_short)(len));

	cr_mutex_wait(pMainHand->m_hmutex, MUTEXTIMEOUT_GECP);
	header->req_id = htons((u_short)(pMainHand->m_nReqId));
	if (++pMainHand->m_nReqId == 0x0) pMainHand->m_nReqId = 1;
	if (((u_short)(++pMainHand->m_nReqId)) == 0x0) pMainHand->m_nReqId = 1;

	*(U32 *)(pd8 + 0) = htonl(addr);
//	for (i = 0; i < ((count+3)/4)*4; i++) 
	for (i = 0; i < ((count+3)/4); i++) 
	{
		itmp = *( ((U32 *) data) + i );
		*(pd8 + 4 + i*4 + 0) = (U08) (itmp >> 24);
		*(pd8 + 4 + i*4 + 1) = (U08) (itmp >> 16);
		*(pd8 + 4 + i*4 + 2) = (U08) (itmp >>  8);
		*(pd8 + 4 + i*4 + 3) = (U08) (itmp >>  0);
	}

	//
	rlenRes = GevSendMessage(sBuff, len2, &(pMainHand->m_soGvcp), &(pMainHand->m_sAddrGvcp), true, rBuff, rlen2);
	cr_mutex_release (pMainHand->m_hmutex);

	header = (T_GVCP_HEADER *) rBuff;
	pd8 = rBuff + sizeof(T_GVCP_HEADER);
	if (header->command == htons(GVCP_WRITEMEM_ACK)) { // good.
		res = 1;
		if (status) {
			*status = htons(*(U16 *)rBuff);			// must check status..
		}
		if (*status == GEV_STATUS_SUCCESS) {
			// if (*(U16 *)(pd8 + 2) == htons(count)) 	// 
			// good. 
		}
	} else {
		res = -1;
		if (status) {
			*status = GEV_STATUS_ERROR;
		}
	}

	free(sBuff);
	free(rBuff);
func_exit:
	return res;
}

I32 GECP_memRead(void *pHand, U32 addr, U32 count, U32 *rdata, U32 *status)
{
	I32 res;
	I32 len, len2;
	I32 rlen, rlen2, rlenRes;

	U08 *sBuff, *rBuff;
	U08 *pd8;
	
	T_GVCP_HEADER *header;

	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;
	//----
	if (pHand == NULL) {
		*status = GEV_STATUS_ERROR;
		res = -1;
		goto func_exit;
	}
	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;
	if (pMainHand == NULL) {
		*status = GEV_STATUS_ERROR;
		res = -1;
		goto func_exit;
	}
	if (pMainHand->m_soGvcp == INVALID_SOCKET) {
		*status = GEV_STATUS_ERROR;
		res = -1;
		goto func_exit;
	}

	// Make Buffer.
	len = 2*sizeof(U32);				// address , count
	len2 = sizeof(T_GVCP_HEADER) + len;

	rlen = sizeof(U32) + ((count+3)/4)*4;
	rlen2 = sizeof(T_GVCP_HEADER) + rlen;

//	sBuff = (U08 *) malloc(len2);
	sBuff = (U08 *) malloc(len2+GUARDMEM);
	if (sBuff == NULL) {
		*status = GEV_STATUS_ERROR;
		res = -1;
		goto func_exit;
	}
//	rBuff = (U08 *) malloc(rlen2);
	rBuff = (U08 *) malloc(rlen2+GUARDMEM);
	if (rBuff == NULL) {
		free(sBuff);
		*status = GEV_STATUS_ERROR;
		res = -1;
		goto func_exit;
	}
	pd8 = sBuff + sizeof(T_GVCP_HEADER);


	// Make Header
	header = (T_GVCP_HEADER *)sBuff;
	header->hard_key = GVCP_KEY;
	header->flag = 0x80;			// ACKNOWLEDGE flag (bit7) should be set.
	header->command = htons(GVCP_READMEM_CMD);
	header->length = htons((u_short)(len));

	cr_mutex_wait(pMainHand->m_hmutex, MUTEXTIMEOUT_GECP);
	header->req_id = htons((u_short)(pMainHand->m_nReqId));
	if (++pMainHand->m_nReqId == 0x0) pMainHand->m_nReqId = 1;
	if (((u_short)(++pMainHand->m_nReqId)) == 0x0) pMainHand->m_nReqId = 1;

	*(U32 *)(pd8 + 0) = htonl(addr);
	*(U16 *)(pd8 + 4) = 0x0000;
	*(U16 *)(pd8 + 6) = htons((U16)count);


	rlenRes = GevSendMessage(sBuff, len2, &(pMainHand->m_soGvcp), &(pMainHand->m_sAddrGvcp), true, rBuff, rlen2);
	cr_mutex_release (pMainHand->m_hmutex);


	header = (T_GVCP_HEADER *) rBuff;
	pd8 = rBuff + sizeof(T_GVCP_HEADER);
	if (header->command == ntohs(GVCP_READMEM_ACK)) { // good.
		res = 1;
		*status = ntohs(*(U16 *)rBuff);			// must check status..
		if (*status == GEV_STATUS_SUCCESS) {
			U32 itmp;
			U32 i;
			// if (*(U32 *)(pd8 + 0) == htonl(addr)) 	// 

//			*(U32 *)(pd8 + 0) = htonl(addr);
			//memcpy(rdata, pd8 + sizeof(U32), count);
			//for (i = 0; i < ((count+3)/4)*4; i++) 
			for (i = 0; i < ((count+3)/4); i++) 
			{
				itmp =*(U32 *)(pd8 + 4 + i*4);
				rdata[i] = ntohl(itmp);
#if 0
				itmp  = *(pd8 + 4 + i*4 + 0);
				itmp <<=8;
				itmp |= *(pd8 + 4 + i*4 + 1);
				itmp <<=8;
				itmp |= *(pd8 + 4 + i*4 + 2);
				itmp <<=8;
				itmp |= *(pd8 + 4 + i*4 + 3);

				rdata[i] = itmp;
#endif
			}
		}
	} else {
		res = -1;
		*status = GEV_STATUS_ERROR;
	}

	free(sBuff);
	free(rBuff);
func_exit:
	return res;
}


//------------------------------------------------
I32 GECP_startCapture(void *pHand, I32 bStart)
{
	T_GVCP_STREAM *pgvcpstr;
	U32			streamid;

	BYTE *rBuff;
	T_GVCP_WRITE_LIST regList[1];
	//I32 rLen;
	I32 res;

//	HANDLE hthread = NULL;
//	HAND hthread = NULL;
	//UL32 threadid;

	res = 0;
	streamid = (U32)-999;
	if (pHand == NULL) {
		goto func_exit;
	}
	pgvcpstr = (T_GVCP_STREAM *)pHand;
	streamid = pgvcpstr->m_streamid;

	rBuff = new BYTE[1202];


	regList[0].addr = GVCP_VR_GRAB_CTRL_C(streamid);
//	regList[0].addr = GVCP_DISCOVERY_CMD;
	regList[0].value = bStart;

	//rLen = GECP_regWrite(pHand, regList, 1, rBuff, 1202);
	res = GECP_regWrite(pHand, regList, 1, rBuff, 1202);

//	// check OK
//	if (rLen != 12) {
//		//MessageBox(NULL, L"GVCP_VR_GRAB_CTRL err");
//	}

	delete rBuff;
func_exit:

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" streamid: %d,  bStart: %d, res: %d\n", streamid, bStart, res);
	return res;
}


/*!
 ********************************************************************************
 *	@brief      GEV module create
 *
 *  @return		HANDLE.  (NULL: FAIL)
 *
 *	@note 
 *	@author	    yhsuk
 *  @date       2016/0811
 *******************************************************************************/
void *GEV_create(HAND pP[], U32 camcount, U32 runmode)
{
	void *hgev;

	hgev = GEV_create2(pP, camcount, runmode, GEV_STREAMING_CHANNEL_SINGLE);

	return hgev;
}
void *GEV_create2(HAND pP[], U32 camcount, U32 runmode, U32 stchannelmode)
{
	U32 i;
	T_GEV	*pgev;
	//T_GVCP_MAIN *pgvcpm;

	//-
	pgev = NULL;
	if (camcount > GEV_MAXCAM) {
		goto func_exit;
	}

	pgev = (T_GEV *) malloc(sizeof(T_GEV));

	memset(pgev, 0, sizeof(T_GEV));
//	pgev->hParent = pParent;
	pgev->runmode = runmode;
	pgev->stchannelmode = stchannelmode;				// streaming channel mode. 0: SINGLE, 1: MULTI.
	for (i = 0; i < camcount; i++) {
		pgev->hParent[i] = pP[i];
	}

	if (stchannelmode == GEV_STREAMING_CHANNEL_SINGLE) {
		pgev->devicecount = camcount;
		for (i = 0; i < camcount; i++) {
			//pgvcpm = (T_GVCP_MAIN *)GECP_create(pgev, &pP[i]);
			//pgev->gecpo[i].m_streamid = i;
			//pgev->gvcpstr[i].m_streamid = i;

			GECP_create2(pgev, &pP[0], i, 1);

//			pgev->gecpo[i].m_camid = 0;
//			pgev->gecpo[i].m_stcahnnelmode = stchannelmode;
//			pgev->gecpo[i].m_pgvcpm = pgvcpm;

#if 0
			pgvcpm = (T_GVCP_MAIN *)GECP_create(pgev, &pP[i]);
//			pgvcpm->m_camcount = 1;						// GEV_STREAMING_CHANNEL_SINGLE.. :P
//			pgvcpm->m_stchannelmode = stchannelmode;						// GEV_STREAMING_CHANNEL_SINGLE.. :P
			pgvcpm->m_hgvcpo[0] = &pgev->gecpo[i];

			pgev->gecpo[i].m_camid = 0;
			pgev->gecpo[i].m_stcahnnelmode = stchannelmode;
			pgev->gecpo[i].m_pgvcpm = pgvcpm;
#endif
		}
	} else if (stchannelmode == GEV_STREAMING_CHANNEL_MULTI) {
		pgev->devicecount = 1;
		//GECP_create2(pgev, &pgev->gvcpstr[0], &pP[0], camcount);
		GECP_create2(pgev, &pP[0], 0, camcount);

		/*
		for (i = 0; i < camcount; i++) {
			pgvcpm->m_hgvcpo[i] = &pgev->gecpo[i];

			pgev->gecpo[i].m_camid = i;
			pgev->gecpo[i].m_stcahnnelmode = stchannelmode;
			pgev->gecpo[i].m_pgvcpm = pgvcpm;
		}
		*/
	} else {
		//WHAT?
	}

	if (runmode == GEV_RUNMODE_IOCP) {
		// Create IOCP
	}


func_exit:
	return (void *)pgev;
}


/*!
 ********************************************************************************
 *	@brief      Delete GEV module
 *
 *  @return		
 *
 *	@note 
 *	@author	    yhsuk
 *  @date       2016/0811
 *******************************************************************************/
void GEV_delete(void *pHand)
{
	U32 i;
	T_GEV	*pgev;
	U32 devicecount;
	U32 stchannelmode;
	//--
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++\n");

	if (pHand == NULL) {
		goto func_exit;
	}

	pgev = (T_GEV *) pHand;

	devicecount = pgev->devicecount;
	if (devicecount > GEV_MAXCAM) {
		devicecount = GEV_MAXCAM;
	}

	stchannelmode = pgev->stchannelmode;
	if (pgev->runmode == GEV_RUNMODE_IOCP) {
		// Destroy IOCP
	}

	for (i = 0; i < devicecount; i++) {
		GECP_destroy(&pgev->gvcpstr[i]);
	}
	/*
	if (stchannelmode == GEV_STREAMING_CHANNEL_SINGLE) {
		for (i = 0; i < devicecount; i++) {
			GECP_destroy(&pgev->gvcpstr[i]);
		}
	} else {
		GECP_destroy(&pgev->gvcpstr[0]);
	}
	*/

	free(pgev);
func_exit:
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"--\n");
	return;
}

/*!
 ********************************************************************************
 *	@brief      Get GECP hand
 *
 *  @return		
 *
 *	@note 
 *	@author	    yhsuk
 *  @date       2016/0811
 *******************************************************************************/
HAND GEV_GECP_hand(HAND hgev, U32 camid)
{
	T_GEV	*pgev;
	HAND	hgecp;

	//--
	hgecp = NULL;
	if (hgev == NULL) {
		goto func_exit;
	}
	pgev = (T_GEV *) hgev;

	if (camid >= GEV_MAXCAM) {
		goto func_exit;
	}
	hgecp = (HAND)&pgev->gvcpstr[camid];
	
func_exit:
	return hgecp;
}


/*
static void* GECP_create(HAND hgev, void *pParent[])
{
	void* hgecp;
	
	hgecp = GECP_create2(HAND hgev, pParent, 1, GEV_STREAMING_CHANNEL_SINGLE);

	return hgecp;
}
*/

//static U32 GECP_create2(HAND hgev, T_GVCP_STREAM gvcpstr[], void *pParent[], U32 streamcount)
//static U32 GECP_create2(HAND hgev, void *pParent[], U32 streamid, U32 streamcount)
static U32 GECP_create2(HAND hgev, void *pParent[], U32 camid, U32 streamcount)
{
	U32 res;
	T_GVCP_MAIN *pMainHand;
	T_GEV	*pgev;
	T_GVSP  *pgvsp;
	T_GVCP_STREAM *pgvcpstr;
	U32 stchannelmode;
	U32 i;

	//-------------
	if (hgev == NULL) {
		res = 0;
		goto func_exit;
	}
	pgev = (T_GEV *)hgev;
	stchannelmode = pgev->stchannelmode;
	//gvcpstr[GEV_MAXCAM];			// Object of Stream..

	pMainHand = (T_GVCP_MAIN *)malloc(sizeof(T_GVCP_MAIN));
	if (pMainHand == NULL) {
		res = 0;
		goto func_exit;
	}



	memset(pMainHand,0, sizeof(T_GVCP_MAIN));
	pMainHand->m_soGvcp 		= INVALID_SOCKET;
	pMainHand->m_isGvcpActive 	= 0;

	pMainHand->m_streamcount 	= streamcount;
	pMainHand->m_stchannelmode 	= stchannelmode;
	

	//for (i = 0; i < GEV_MAXCAM; i++) {
	//	pgvsp = &pMainHand->m_gvsp[i];
	//	pgvsp->m_soGvsp 		= INVALID_SOCKET;
	//	pgvsp->m_isGvspActive 	= 0;
	//	pgvsp->m_nCapStop = 1;
	//}

	pgvsp = &pMainHand->m_gvsp[camid];
	pgvsp->m_soGvsp 		= INVALID_SOCKET;
	pgvsp->m_isGvspActive 	= 0;
	pgvsp->m_nCapStop = 1;
	

	pMainHand->m_hgev 		= hgev;
	pMainHand->m_nMsgStop 	= 1;

	cr_InitSocketAPI();
	if (stchannelmode == GEV_STREAMING_CHANNEL_SINGLE) {
		pMainHand->m_streamcount 	= streamcount = 1;

		pMainHand->m_hParent[0]	= pParent[camid];
		pgvsp = &pMainHand->m_gvsp[0];
		pgvsp->m_nCapStop = 0;

		pgvcpstr = &pgev->gvcpstr[camid];
		pgvcpstr->m_pgvcpm 			= pMainHand;
		//pgvcpstr->m_streamid 		= streamid;
		pgvcpstr->m_streamid 		= 0;
		pgvcpstr->m_stchannelmode 	= stchannelmode;

		pMainHand->m_hgvcpstr[0] = pgvcpstr;
	} else if (stchannelmode == GEV_STREAMING_CHANNEL_MULTI) {
		for (i = 0; i < streamcount; i++) {
			pMainHand->m_hParent[i]	= pParent[i];
			pgvsp = &pMainHand->m_gvsp[i];
			pgvsp->m_nCapStop = 0;

			pgvcpstr = &pgev->gvcpstr[i];
			pgvcpstr->m_pgvcpm 			= pMainHand;
			pgvcpstr->m_streamid 		= i;
			pgvcpstr->m_stchannelmode 	= stchannelmode;
			pMainHand->m_hgvcpstr[i] = pgvcpstr;
		}
	} else {
		// WHAT?
	}

	pMainHand->m_nReqId = 1;

	//-------------

	pMainHand->m_hmutex = cr_mutex_create();	

	if (pgev->runmode == GEV_RUNMODE_NORMAL) {
		if (stchannelmode == GEV_STREAMING_CHANNEL_SINGLE) {
			pgvsp 		= &pMainHand->m_gvsp[0];				// 0 is good for GEV_STREAMING_CHANNEL_SINGLE
			pgvcpstr 	= &pgev->gvcpstr[camid];

			pgvsp->m_hCaptureThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)CR_ImgThreadFn, pgvcpstr, 0, NULL);  
			//SetThreadPriority(pMainHand->m_hCaptureThread, 2);		// THREAD PRIORITY.. -_-; 20160717
			//	SetThreadPriority(pMainHand->m_hCaptureThread, THREAD_PRIORITY_TIME_CRITICAL);		// THREAD PRIORITY.. -_-; 20160717
			SetThreadPriority(pgvsp->m_hCaptureThread, THREADPRIORITY_GEV_CAPTURE);
		} else if (stchannelmode == GEV_STREAMING_CHANNEL_MULTI) {
			for (i = 0; i < streamcount; i++) {
				pgvsp 		= &pMainHand->m_gvsp[i];
				pgvcpstr 	= &pgev->gvcpstr[i];

				pgvsp->m_hCaptureThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)CR_ImgThreadFn, pgvcpstr, 0, NULL);  
				SetThreadPriority(pgvsp->m_hCaptureThread, THREADPRIORITY_GEV_CAPTURE);
			}
		} else {
			// WHAT?
		}
	}

	pMainHand->m_nMsgStop = 0;
	//pMainHand->m_hMsgThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)CR_MsgThreadFn, pMainHand, 0, NULL);
	pMainHand->m_hMsgThread = CreateThread(NULL, THREADPRIORITY_GEV_MSG, (LPTHREAD_START_ROUTINE)CR_MsgThreadFn, pMainHand, 0, NULL);
	SetThreadPriority(pMainHand->m_hMsgThread, 1);			// THREAD PRIORITY.. -_-; 20160717

	res = 1;
func_exit:

	return res;
}

static void GECP_destroy(void *pHand)
{
	U32 i;
	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;
	U32 stchannelmode;
	T_GEV	*pgev;
	//struct sockaddr_in  *pAddrGvcp;
	//I32 nPort;
	
	//---
	if (pHand == NULL) {
		goto func_exit;
	}

	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;

	if (pMainHand == NULL) {
	//	cr_CloseSocketAPI();
		goto func_exit;
	}

	pgev = (T_GEV *)pMainHand->m_hgev;

	stchannelmode = pgvcpstr->m_stchannelmode;

	GECP_stop(pHand);

	pMainHand->m_nMsgStop = 1;

	for (i = 0; i < pMainHand->m_streamcount; i++) {
		T_GVSP  *pgvsp;
		pgvsp = &pMainHand->m_gvsp[i];
		pgvsp->m_nCapStop = 1;

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "before GvspClose \n");
		if (pgvsp->m_soGvsp != INVALID_SOCKET) {
			SOCKET	s;			
			s = pgvsp->m_soGvsp;
			pgvsp->m_soGvsp = INVALID_SOCKET;

			cr_sleep(SLEEP_SOCKET_CLOSE);
			closesocket(s);
		}
		pgvsp->m_isGvspActive   = 0;

		WaitForSingleObject(pgvsp->m_hCaptureThread, INFINITE);
	}

	WaitForSingleObject(pMainHand->m_hMsgThread, INFINITE);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "after  m_hMsgThread DESTROY\n");
	cr_mutex_delete(pMainHand->m_hmutex);
	pMainHand->m_hmutex = NULL;
	free(pMainHand);

	pgvcpstr->m_pgvcpm = NULL;

	if (stchannelmode == GEV_STREAMING_CHANNEL_MULTI) {
		for (i = 0; i < GEV_MAXCAM; i++) {				// Clear.. :)
			pgev->gvcpstr[i].m_pgvcpm = NULL;
		}
	}

func_exit:
	return;
}



I32 GECP_stop(void *pHand)
{
	I32 res;

	U32 i;
	T_GVCP_STREAM 		*pgvcpstr;
	U32		stchannelmode;
	
	//--

	if (pHand == NULL) {
		return 0;
	}
	
	res = GECP_CamGvcpStop(pHand);

	pgvcpstr 		= (T_GVCP_STREAM *)pHand;
	stchannelmode 	= pgvcpstr->m_stchannelmode;

	if (stchannelmode == GEV_STREAMING_CHANNEL_SINGLE) {
		res = GECP_CamGvspStop(pHand);
	} else if (stchannelmode == GEV_STREAMING_CHANNEL_MULTI) {
//		T_GVCP_STREAM *pgvcpstr;
		T_GVCP_MAIN *pMainHand;
		T_GEV	*pgev;

		res = 1;
		//if (pHand == NULL) {
		//	res = 0;
		//}

		if (res) {
//			pgvcpstr = (T_GVCP_STREAM *)pHand;
			pMainHand = pgvcpstr->m_pgvcpm;
			pgev = (T_GEV *)pMainHand->m_hgev;

			for (i = 0; i < GEV_MAXCAM; i++) {
				res = GECP_CamGvspStop(&pgev->gvcpstr[i]);
			}
		}
	}

	return res;
}

int GECP_CamIpstr(void *pHand, char *pIP)
{
	//T_GVCP_OBJ *pgvcpo;
	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;

	if (pHand == NULL) {
		return 0;
	}

	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;

	if (pMainHand == NULL) {
		return 0;
	}
	memcpy(pMainHand->m_camIPstr, pIP, 64);
	pMainHand->m_camIP = ntohl(inet_addr(pIP));
	return 1;
}

int GECP_CamIp(void *pHand, U32 Ip)
{
	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;

	if (pHand == NULL) {
		return 0;
	}

	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;

	if (pMainHand == NULL) {
		return 0;
	}

	pMainHand->m_camIP = Ip;
	return 1;
}

I32 GECP_HostIpstr(void *pHand, char *pIP)
{
	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;

	//--
	if (pHand == NULL) {
		return 0;
	}
	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;
	if (pMainHand == NULL) {
		return 0;
	}
	
	memcpy(pMainHand->m_hostIPstr, pIP, 64);
	pMainHand->m_hostIP = ntohl(inet_addr(pIP));
	return 1;
}

I32 GECP_HostIp(void *pHand, U32 Ip)
{
	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;

	//--
	if (pHand == NULL) {
		return 0;
	}
	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;
	if (pMainHand == NULL) {
		return 0;
	}

	pMainHand->m_hostIP = Ip;
	return 1;
}


I32 GECP_CamGvcpPort(void *pHand, I32 nPort)
{
	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;

	//--
	if (pHand == NULL) {
		return 0;
	}
	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;
	if (pMainHand == NULL) {
		return 0;
	}

	if (nPort != 0) {
		pMainHand->m_camGvcpPort = nPort;							// GVCP cam port number. (3956)
	}

	return 1;
}

I32 GECP_CamGvspPort(void *pHand, I32 nPort)
{
	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;
	T_GVSP  *pgvsp;

	U32 streamid;

	//--
	if (pHand == NULL) {
		return 0;
	}
	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;
	if (pMainHand == NULL) {
		return 0;
	}

	//pgvcpstr->m_streamid 		= streamid;

	streamid = pgvcpstr->m_streamid;

	pgvsp = &pMainHand->m_gvsp[streamid];
	if (nPort != 0) {
		//pMainHand->m_camGvcpPort = nPort;							// GVCP cam port number. (3956)
		pgvsp->m_hostGvspPort = nPort;							// GVCP cam port number. (3956)
	}

	return 1;
}



I32 GECP_CamGvspCB(void *pHand, CR_video_fnCB fnCB)
{
	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;
	T_GVSP  *pgvsp;

	U32 streamid;

	//--
	if (pHand == NULL) {
		return 0;
	}
	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;
	if (pMainHand == NULL) {
		return 0;
	}

	streamid = pgvcpstr->m_streamid;
	pgvsp = &pMainHand->m_gvsp[streamid];


	pgvsp->m_videoCB = fnCB;

	return 1;
}

I32 GECP_CamGvcpStart(void *pHand)
{
	I32 res;
	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;
	//T_GVSP  *pgvsp;

	struct sockaddr_in  *pAddrGvcp;
	I32 nPort;
	
	//---
	nPort = 0;
	if (pHand == NULL) {
		res = 0;
		goto func_exit;
	}
	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;
	if (pMainHand == NULL) {
		res = 0;
		goto func_exit;
	}
	nPort = pMainHand->m_camGvcpPort;

	if (nPort == 0) {
		res = 0;
		goto func_exit;
	}

	if (pMainHand->m_isGvcpActive) {				// Already running..
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"Gvcp Already Run..\n");
		if (pMainHand->m_stchannelmode == 0) {
			res = 0;
		} else {
			res = 1;
		}
		goto func_exit;
	}

	if (pMainHand->m_soGvcp == INVALID_SOCKET) {
		pMainHand->m_soGvcp = socket(PF_INET, SOCK_DGRAM, 0);
	}

	if (pMainHand->m_soGvcp == INVALID_SOCKET) {
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"Gvcp socket open fail..\n");
		res = 0;
		goto func_exit;
	}

	pAddrGvcp 					= &pMainHand->m_sAddrGvcp;
	pAddrGvcp->sin_family 		= AF_INET;
	pAddrGvcp->sin_port 		= htons((u_short) nPort);
	//	pAddrGvcp->sin_addr.s_addr 	= inet_addr(pMainHand->m_camIPstr);
	pAddrGvcp->sin_addr.s_addr = htonl(pMainHand->m_camIP);

	pMainHand->m_isGvcpActive   = 1;
	res = 1;
func_exit:
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" port: %d, res: %d\n", nPort, res);
	return res;
}

I32 GECP_CamGvcpStop(void *pHand)
{
	I32 res;
	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;
	//struct sockaddr_in  *pAddrGvcp;
	//I32 nPort;
	
	//---
	if (pHand == NULL) {
		res = 0;
		goto func_exit;
	}

	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;

	if (pMainHand == NULL) {
		res = 0;
		goto func_exit;
	}
	//pMainHand = (T_GVCP_MAIN *)pHand;
	if (pMainHand->m_soGvcp != INVALID_SOCKET) {
		SOCKET	s;			
		s = pMainHand->m_soGvcp;
		pMainHand->m_soGvcp = INVALID_SOCKET;

		cr_sleep(SLEEP_SOCKET_CLOSE);
		closesocket(s);
	}
	pMainHand->m_isGvcpActive   = 0;
	res = 1;
func_exit:
	return res;
}

I32 GECP_CamGvspStart(void *pHand)
{
	I32 res;

	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;
	T_GVSP  *pgvsp;

	U32 streamid;

	//---
	if (pHand == NULL) {
		return 0;
	}
	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;
	if (pMainHand == NULL) {
		return 0;
	}

	streamid = pgvcpstr->m_streamid;
	pgvsp = &pMainHand->m_gvsp[streamid];

	//pMainHand->m_isGvspActive   = 1;
	pgvsp->m_isGvspActive   = 1;
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": m_isGvspActive: %d\n", pMainHand->m_isGvspActive);
	res = 1;
//func_exit:

	return res;
}

I32 GECP_CamGvspStop(void *pHand)
{
	I32 res;
	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;
	T_GVSP		*pgvsp;
	U32 streamid;
	//U32 stchannelmode;
	//struct sockaddr_in  *pAddrGvsp;
//	I32 nPort;
	
	//---
	if (pHand == NULL) {
		res = 0;
		goto func_exit;
	}

	pgvcpstr = (T_GVCP_STREAM *)pHand;
	streamid = pgvcpstr->m_streamid;

	pMainHand = pgvcpstr->m_pgvcpm;

	if (pMainHand == NULL) {
		res = 0;
		goto func_exit;
	}

	pgvsp = &pMainHand->m_gvsp[streamid];
	pgvsp->m_isGvspActive   = 0;


#if 0

	stchannelmode = pgvcpstr->stchannelmode;
#if 0
	if (pMainHand->m_soGvsp != INVALID_SOCKET) {
		SOCKET	s;			
		s = pMainHand->m_soGvsp;
		pMainHand->m_soGvsp = INVALID_SOCKET;
//#define SLEEP_SOCKET_CLOSE	50
		cr_sleep(SLEEP_SOCKET_CLOSE);
		closesocket(s);
	}
#endif
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": 0x%08x\n", (U32) pHand);
	if (stchannelmode == GEV_STREAMING_CHANNEL_SINGLE) {
		pgvsp = &pMainHand->m_gvsp[0];
		pgvsp->m_isGvspActive   = 0;
	} else {
		for (i = 0; i < pMainHand->m_streamcount; i++) {
			pgvsp = &pMainHand->m_gvsp[i];
			pgvsp->m_isGvspActive   = 0;
		}
	}
#endif
	res = 1;
func_exit:
	return res;
}

I32 GECP_CamTimerMode(void *pHand, U32 mode)
{
	U32 value = mode;

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" mode: %d\n", mode);
	
	return writeResister(pHand, GECP_REG_TIMER_MODE, &value);			
}

I32 GECP_CamExposureMode(void *pHand, U32 mode)
{
	U32 value = mode;

	if ((mode != GVCP_VR_EXPOSURE_MODE_MASTER) &&
		(mode != GVCP_VR_EXPOSURE_MODE_SLAVE)) 
	{
		return 0;
	}

	return writeResister(pHand, GECP_REG_EXPOSURE_MODE, &value);		
}

//----------------------------------------------------------------
I32 GECP_ImageSize(void *pHand, I32 width, I32 height)
{
	U32 value[2];

	if ( (width > MAXWIDTH || width <= 0) || (height > MAXHEIGHT || height <= 0) ) {
		return 0;
	}	

	value[0] = width;
	value[1] = height;

	return writeResister(pHand, GECP_REG_SIZE, value);	
}


I32 GECP_ImagePosition(void *pHand, I32 sx, I32 sy)
{
	U32 value[2];

	if ( (sx >= MAXWIDTH || sx < 0) || (sy >= MAXHEIGHT || sy < 0) ) {
		return 0;
	}	

	value[0] = sx;
	value[1] = sy;

	return writeResister(pHand, GECP_REG_POSITION, value);	
}

I32 GECP_ImageFps(void *pHand, I32 fps)
{
	I32 res;

	I32 durationUsec;

	if (fps > MAXFPS || fps < MINFPS) {
		res = 0;
		goto func_exit;
	}

	durationUsec = (I32) (1e6 / fps);		// 1000_000 / fps

	res = GECP_ImageDuration(pHand, durationUsec);

func_exit:
	return res;
}

I32 GECP_ImageDuration(void *pHand, I32 usec)
{	
	U32 value = usec;

	if (usec > MAXDURATIONUSEC || usec < MINDURATIONUSEC) {
		return 0;
	}	
	return writeResister(pHand, GECP_REG_DURATION, &value);			
}

I32 GECP_ImageExposure(void *pHand, I32 usec)	// with LED On.
{	
	U32 value = usec;

	if (usec > MAXEXPOSURENUSEC) {
		return 0;
	}
	
	if (usec < MINEXPOSURENUSEC) {
		usec = MINEXPOSURENUSEC;
	}
	
	return writeResister(pHand, GECP_REG_EXPOSURE, &value);			
}

I32 GECP_ImageExposurePost(void *pHand, I32 usec)	// Set Post Exposure time.  20170325
{	
	U32 value = usec;
	
	return writeResister(pHand, GECP_REG_EXPOSURE_POST, &value);		
}

I32 GECP_ImageExposureExpiate(void *pHand, I32 usec)	// Set Exposure Expiate time.  20170325
{	
	U32 value = usec;
	
	return writeResister(pHand, GECP_REG_EXPOSURE_EXPIATE, &value);		
}

I32 GECP_ImageGain(void *pHand, I32 gain)		// gain value
{
	U32 value = gain;

	if (gain > MAXEXPOSURENUSEC || gain < MINEXPOSURENUSEC) {
		return 0;
	}		
	return writeResister(pHand, GECP_REG_GAIN, &value);
}

I32 GECP_ImageGamma(void *pHand, I32 gamma)		// Gamma: 100 == 1.0     2019/0131
{
	U32 value = gamma;
	
	return writeResister(pHand, GECP_REG_GAMMA, &value);
}


//AAA

I32 GECP_ImageContrast(void *pHand, I32 contrast)		// Contrast: 100 == 1.0  
{
	U32 value = contrast;

	return writeResister(pHand, GECP_REG_CONTRAST, &value);
}
I32 GECP_ImageBright(void *pHand, I32 bright)		// Bright: 100 == 0 
{
	U32 value = bright;

	return writeResister(pHand, GECP_REG_BRIGHT, &value);
}



I32 GECP_ImageLUT(void *pHand, I32 bright, I32 contrast, I32 gamma)
{
	U32 value[3];

	value[0] = bright;
	value[1] = contrast;
	value[2] = gamma;

	return writeResister(pHand, GECP_REG_LUT, value);

}


//ZZZ

I32 GECP_ImageDelayoffset(void *pHand, I32 delayoffset)		// Exposure Delay offset: usec     2019/0223
{
	U32 value = delayoffset;

	return writeResister(pHand, GECP_REG_DELAY_OFFSET, &value);
}

I32 GECP_LED_Brightness(void *pHand, I32 brightness)		// brightness: 8bit. (0 ~ 255)
{
	U32 value = brightness;

	return writeResister(pHand, GECP_REG_LED_BRIGHT, &value);
}


I32 GECP_ImageSkip(void *pHand, I32 skip)		// 0: Don't send, 1: Don't skip, 2: skip1, 3: skip2, ..., N: skip(N-1)
{
	U32 value = skip;

	if (skip < 0 ) {
		return 0;
	}

	return writeResister(pHand, GECP_REG_SKIP, &value);
}


I32 GECP_ImageMultitude4Normal(void *pHand, I32 multitude4normal)
{
	U32 value = multitude4normal;

	if (multitude4normal < 0) {
		return 0;
	}
	return writeResister(pHand, GECP_REG_MULTITUDE, &value);
}



I32 GECP_ImageBulkRequest64(void *pHand, U64 ts, U32 framecount, U32 waittime, U32 continueafterbulk, U32 multitude4Bulk, U32 skip4bulk
							, U32 width4bulk, U32 height4bulk, U32 offset_x4bulk, U32 offset_y4bulk, U64 ts64shot, I32 axx1024, I32 bxx1024, I32 ayx1024, I32 byx1024)
{
	U32 ts_h;
	U32 ts_l;

	U32 tsshot_h;
	U32 tsshot_l;

	ts_h = (U32) (ts >> 32);
	ts_l = (U32) ts;


	tsshot_h = (U32) (ts64shot >> 32);
	tsshot_l = (U32) ts64shot;

	return  GECP_ImageBulkRequest(pHand, ts_h, ts_l, framecount, waittime, continueafterbulk, multitude4Bulk, skip4bulk
							,width4bulk, height4bulk, offset_x4bulk, offset_y4bulk,
							tsshot_h, tsshot_l, axx1024, bxx1024, ayx1024, byx1024);
}

I32 GECP_ImageBulkRequest(void *pHand, U32 ts_h, U32 ts_l, U32 framecount, U32 waittime, U32 continueafterbulk, 
	U32 multitude4Bulk, U32 skip4bulk
	, U32 width4bulk, U32 height4bulk, U32 offset_x4bulk, U32 offset_y4bulk, U32 tsshot_h, U32 tsshot_l,I32 axx1024, I32 bxx1024, I32 ayx1024, I32 byx1024)
{
	I32 res;
	BYTE *rBuff;
	T_GVCP_WRITE_LIST regList[14];
	T_GVCP_STREAM *pgvcpstr;
	U32			streamid;
	
	
	//I32 rLen;
	I32 regCount;
	U32 data;
	

	//---
	if (pHand == NULL) {
		res = 0;
		goto func_exit;
	}
	pgvcpstr = (T_GVCP_STREAM *)pHand;
	streamid = pgvcpstr->m_streamid;

	rBuff = new BYTE[1202];

	regCount = 13;

	//--
	//regList[0].addr = GVCP_VR_BULK_TIMESTAMP_HIGH;
	//regList[0].value = ts_h;

	//regList[1].addr = GVCP_VR_BULK_TIMESTAMP_LOW;
	//regList[1].value = ts_l;


	//data  = (framecount & 0xFF);					// b7 ~ b0
	//data |= (waittime << 8) & 0x7F00;				// b14 ~ b8
	//data |= continueafterbulk?(0x8000):(0x0000);	// b15
	//data |= (multitude4Bulk << 16) &  0xF0000;	// b19 ~ b16   
	//data |= (skip4bulk << 20) & 0xF00000;	// b23 ~ b20   

	//regList[2].addr = GVCP_VR_BULK_REQUEST;
	//regList[2].value = data;

	//res = GECP_regWrite(pHand, regList, regCount, rBuff, 1202);


	regList[0].addr = GVCP_VR_BULK_TIMESTAMP_HIGH_C(streamid);	
	regList[0].value = ts_h;

	regList[1].addr = GVCP_VR_BULK_TIMESTAMP_LOW_C(streamid);
	regList[1].value = ts_l;


	regList[2].addr = GVCP_VR_BULK_IMG_WIDTH_C(streamid);
	regList[2].value = width4bulk;

	regList[3].addr = GVCP_VR_BULK_IMG_HEIGHT_C(streamid);
	regList[3].value = height4bulk;


	regList[4].addr = GVCP_VR_BULK_IMG_OFFX_C(streamid);
	regList[4].value = offset_x4bulk;

	regList[5].addr = GVCP_VR_BULK_IMG_OFFY_C(streamid);
	regList[5].value = offset_y4bulk;


	regList[6].addr = GVCP_VR_BULK_TIMESTAMP_SHOT_HIGH_C(streamid);
	regList[6].value = tsshot_h;

	regList[7].addr = GVCP_VR_BULK_TIMESTAMP_SHOT_LOW_C(streamid);
	regList[7].value = tsshot_l;

	regList[8].addr = GVCP_VR_BULK_STARTX_A_C(streamid);
	regList[8].value = axx1024;

	regList[9].addr = GVCP_VR_BULK_STARTX_B_C(streamid);
	regList[9].value = bxx1024;

	regList[10].addr = GVCP_VR_BULK_STARTY_A_C(streamid);
	regList[10].value = ayx1024;

	regList[11].addr = GVCP_VR_BULK_STARTY_B_C(streamid);
	regList[11].value = byx1024;

	data  = (framecount & 0xFF);					// b7 ~ b0
	data |= (waittime << 8) & 0x7F00;				// b14 ~ b8
	data |= continueafterbulk?(0x8000):(0x0000);	// b15
	data |= (multitude4Bulk << 16) &  0xF0000;	// b19 ~ b16   
	data |= (skip4bulk << 20) & 0xF00000;	// b23 ~ b20   

	regList[12].addr = GVCP_VR_BULK_REQUEST_C(streamid);
	regList[12].value = data;

	res = GECP_regWrite(pHand, regList, regCount, rBuff, 1202);
	delete rBuff;
func_exit:
	return res;
}


I32 GECP_ImageSyncDiv(void *pHand, I32 syncdiv)
{
	U32 value = syncdiv;

	return writeResister(pHand, GECP_REG_SYNC_DIV, &value);
}




I32 GECP_CamFmcInit(void *pHand, I32 zrot, I32 autoinit)							// 2016/11/02
{
	I32 res;
	BYTE *rBuff;
	T_GVCP_WRITE_LIST regList[1];
	T_GVCP_STREAM *pgvcpstr;
	U32			streamid;
	//I32 rLen;
	I32 regCount;
	
	U32 initcode;
	int i;

	//---
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" pHand: 0x%08x with zrot: %d, autoinit: %d\n", pHand, zrot, autoinit);
	if (pHand == NULL) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" pHand NULL!!!  zrot xxx\n");
		res = 0;
		goto func_exit;
	}

	pgvcpstr = (T_GVCP_STREAM *)pHand;
	streamid = pgvcpstr->m_streamid;
	rBuff = new BYTE[1202];

	regCount = 1;

	//--
	initcode = 0x00000001;					//b0:  Init FMC before start or NOT.
	if (zrot) {
		initcode |= 0x00000002;				//    b1:           1: ZROT
	}

	if (autoinit) {
		initcode |= 0x00000004;				//    b2:           1: Autoinit
	}

// //	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [0x%08x] zrot: %d\n", (U32) pHand, zrot);

	regList[0].addr = GVCP_VR_INIT_FMC_C(streamid);
	regList[0].value = initcode;

	res = 0;

	for (i = 0; i < INIT_FMC_ITER; i++) {
		res = GECP_regWrite(pHand, regList, regCount, rBuff, 1202);
		if (res > 0) {
			break;
		}
	//	cr_sleep(20);
	}
	// check OK

	delete rBuff;
func_exit:
	return res;
}


I32 GECP_closeSendSocket(void *pHand)
{
	T_GVCP_MAIN *pMainHand;
	T_GVCP_STREAM *pgvcpstr;
	U32			streamid;

	if (pHand == NULL)
		return 0;

	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;
	if (pMainHand == NULL) {
		return 0;
	}

	streamid = pgvcpstr->m_streamid;

	if (pMainHand->m_soGvcp != INVALID_SOCKET)
	{
		closesocket(pMainHand->m_soGvcp);
		pMainHand->m_soGvcp = INVALID_SOCKET;
	}

	return 1;


}

//int GECP_startSendStart(void *pHand, char *pIP, int nPort, int bStart)
//I32 GECP_startSendStart(void *pHand, U32 uIP, I32 nPort, I32 bStart)
//I32 GECP_startSendStart(void *pHand, I32 nPort, I32 bStart)
I32 GECP_startSendStart(void *pHand, I32 bStart)
{
	BYTE *rBuff;
	T_GVCP_WRITE_LIST regList[3];
	T_GVCP_MAIN 	*pMainHand;
	T_GVCP_STREAM 	*pgvcpstr;
	T_GVSP			*pgvsp;
	U32				streamid;
	I32 rLen;

	//---
	if (pHand == NULL) {
		return 0;
	}

	pgvcpstr 	= (T_GVCP_STREAM *)pHand;
	streamid 	= pgvcpstr->m_streamid;
	pMainHand 	= pgvcpstr->m_pgvcpm;
	pgvsp		= &pMainHand->m_gvsp[streamid];


	if (pMainHand == NULL) {
		return 0;
	}

	streamid = pgvcpstr->m_streamid;	

	//frmControl->IdUDPClient1->Host = edIP->Text;
	rBuff = new BYTE[1202];

	if (bStart == 1) {
		int regcount;
		regList[0].addr = GVCP_SC_DESTINATION_ADDRESS(streamid);
		regList[0].value = pMainHand->m_hostIP;

		regList[1].addr = GVCP_SC_DESTINATION_PORT(streamid);
		//regList[1].value = pMainHand->m_hostGvspPort;				// GVSP shot port number.
		regList[1].value = pgvsp->m_hostGvspPort;

#if defined(USE_SEND_CTRL)
		regList[2].addr = GVCP_VR_SEND_CTRL_C(streamid);			// 2019/0214
		regList[2].value = 1;										

		regcount = 3;
#else
		regcount = 2;
#endif
		rLen = GECP_regWrite(pHand, regList, regcount, rBuff, 1202);
	} else {
		int regcount;
		regList[0].addr = GVCP_SC_DESTINATION_PORT(streamid);
		regList[0].value = 0;
#if defined(USE_SEND_CTRL)
		regList[1].addr = GVCP_VR_SEND_CTRL_C(streamid);			// 2019/0214
		regList[1].value = 0;	

		regcount = 2;
#else
		regcount = 1;
#endif
		rLen = GECP_regWrite(pHand, regList, regcount, rBuff, 1202);
	}
	// check OK
	if (rLen != 12)
	{
	//	ShowMessage("Respone is no valid : " + IntToStr(rLen));
	}

	//ShowMessage(IntToStr(rLen));

	delete rBuff;

	return 1;
}


I32 GECP_SendOnOff(void *pHand, I32 bStart)				// 20190214
{
	BYTE *rBuff;
	T_GVCP_WRITE_LIST regList[2];
	T_GVCP_MAIN 	*pMainHand;
	T_GVCP_STREAM 	*pgvcpstr;
	U32				streamid;
	I32 rLen;

	//---
	if (pHand == NULL) {
		return 0;
	}

	pgvcpstr 	= (T_GVCP_STREAM *)pHand;
	streamid 	= pgvcpstr->m_streamid;
	pMainHand 	= pgvcpstr->m_pgvcpm;


	if (pMainHand == NULL) {
		return 0;
	}

	streamid = pgvcpstr->m_streamid;	

	//frmControl->IdUDPClient1->Host = edIP->Text;
	rBuff = new BYTE[1202];

	regList[0].addr = GVCP_VR_SEND_CTRL_C(streamid);			// 2019/0214

	if (bStart == 1) {
		regList[0].value = 1;										
	} else {
		regList[0].value = 0;	
	}
	rLen = GECP_regWrite(pHand, regList, 1, rBuff, 1202);
	// check OK
	if (rLen != 12)
	{
	//	ShowMessage("Respone is no valid : " + IntToStr(rLen));
	}

	//ShowMessage(IntToStr(rLen));

	delete rBuff;

	return 1;
}



I32 GECP_setMsgCB(void *pHand, CR_msg_fnCB fnCB, I32 nPort, void *pParam)
{

	T_GVCP_MAIN *pMainHand;
	T_GVCP_STREAM *pgvcpstr;

	//---
	if (pHand == NULL)
		return 0;

	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;
	if (pMainHand == NULL) {
		return 0;
	}

	pMainHand->m_msgPort = nPort;

	pMainHand->m_msgCB = fnCB;
	pMainHand->m_pMsgParam = pParam;


	/*
	if (pMainHand->m_hMsgThread == NULL && pMainHand->m_nMsgStop == 1)
	{
		pMainHand->m_nMsgStop = 0;
		pMainHand->m_hMsgThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)CR_MsgThreadFn, pMainHand, 0, NULL);
						//SetThreadPriority();
	}
	*/

	return 1;
}


int GECP_Ipstr2Ip(void *pHand, char *pIP)
{
	int res;

	res = ntohl(inet_addr(pIP));
	pHand;
	return res;
}


I32 GECP_CamCaptureStart(void *pHand)
{
	return GECP_startCapture(pHand, 1);
}
I32 GECP_CamCaptureStop(void *pHand)
{
	return GECP_startCapture(pHand, 0);
}


I32 GECP_ImageUpdate(void *pHand, CAM_IMG_ATTR_T * pGvcpImgAttr)
{
	I32 res;
	BYTE *rBuff;
	T_GVCP_WRITE_LIST regList[32];
	T_GVCP_STREAM *pgvcpstr;
	U32			streamid;
	I32 regCount;

	I32 durationUsec;

	//---
	if (pHand == NULL) {
		res = 0;
		goto func_exit;
	}
	pgvcpstr = (T_GVCP_STREAM *)pHand;
	streamid = pgvcpstr->m_streamid;
	rBuff = new BYTE[1202];

	regCount = 0;

	//-- 
	
	// GECP_ImageSyncDiv
	regList[0].addr = GVCP_VR_EXT_DIVIDER_C(streamid);
	regList[0].value = pGvcpImgAttr->syncdiv;

	if ( (pGvcpImgAttr->width > MAXWIDTH || pGvcpImgAttr->width <= 0) || (pGvcpImgAttr->height > MAXHEIGHT || pGvcpImgAttr->height <= 0) ) {
		res = 0;
		goto func_exit;
	}

	// GECP_ImageSize
	regList[1].addr = GVCP_VR_IMG_WIDTH_C(streamid);
	regList[1].value = pGvcpImgAttr->width;

	regList[2].addr = GVCP_VR_IMG_HEIGHT_C(streamid);
	regList[2].value = pGvcpImgAttr->height;

	// GECP_ImagePosition
	if ( (pGvcpImgAttr->sx >= MAXWIDTH || pGvcpImgAttr->sx < 0) || (pGvcpImgAttr->sy >= MAXHEIGHT || pGvcpImgAttr->sy < 0) ) {
		res = 0;
		goto func_exit;
	}

	regList[3].addr = GVCP_VR_IMG_OFFX_C(streamid);
	regList[3].value = pGvcpImgAttr->sx;

	regList[4].addr = GVCP_VR_IMG_OFFY_C(streamid);
	regList[4].value = pGvcpImgAttr->sy;

	// GECP_ImageFps
	if (pGvcpImgAttr->fps > MAXFPS || pGvcpImgAttr->fps < MINFPS) {
		res = 0;
		goto func_exit;
	}

	durationUsec = (I32) (1e6 / pGvcpImgAttr->fps);		// 1000_000 / fps

	regList[5].addr = GVCP_VR_DURATION_C(streamid);
	regList[5].value = durationUsec;

	// GECP_ImageGain
	if (pGvcpImgAttr->gain > MAXGAIN || pGvcpImgAttr->gain < MINGAIN) {
		res = 0;
		goto func_exit;
	}
	regList[6].addr = GVCP_VR_IMG_GAIN_C(streamid);
	regList[6].value = pGvcpImgAttr->gain;

	// GECP_ImageExposure
	if (pGvcpImgAttr->exposureusec > MAXEXPOSURENUSEC) {
		pGvcpImgAttr->exposureusec = MAXEXPOSURENUSEC;
	}
	if (pGvcpImgAttr->exposureusec < MINEXPOSURENUSEC)
	{
		pGvcpImgAttr->exposureusec = MINEXPOSURENUSEC;
	}

	regList[7].addr = GVCP_VR_SHUTTER_C(streamid);
	regList[7].value = pGvcpImgAttr->exposureusec;


	//GECP_ImageSkip
	if (pGvcpImgAttr->skip < 0 ) {
		pGvcpImgAttr->skip = 0;
	}
	regList[8].addr = GVCP_VR_SKIP_C(streamid);
	regList[8].value = pGvcpImgAttr->skip;


	// GECP_ImageMultitude4Normal
	if (pGvcpImgAttr->multitude4normal < 0) {
		pGvcpImgAttr->multitude4normal = 0;
	}
	regList[9].addr = GVCP_VR_MULTITUDE_C(streamid);
	regList[9].value = pGvcpImgAttr->multitude4normal;


	// GECP_CamFmcInit
	if(pGvcpImgAttr->updateMode == GECP_UPDATE_MODE_EXT)
	{
		U32 initcode;

		initcode = 0x00000001;					//b0:  Init FMC before start or NOT.
		if (pGvcpImgAttr->zrot) {
			initcode |= 0x00000002;				//    b1:           1: ZROT
		}

		if (pGvcpImgAttr->autoinit) {
			initcode |= 0x00000004;				//    b2:           1: Autoinit
		}

		regList[10].addr = GVCP_VR_INIT_FMC_C(streamid);;
		regList[10].value = initcode;

		regList[11].addr = GVCP_VR_EXPOSURE_POST_C(streamid);;
		regList[11].value = pGvcpImgAttr->exposure_post;

		regList[12].addr = GVCP_VR_EXPOSURE_EXPIATE_C(streamid);;
		regList[12].value = pGvcpImgAttr->exposure_expiate;

		regCount = 13;		
	}
	else 
	{
		regCount = 10;		
	}

	res = GECP_regWrite(pHand, regList, regCount, rBuff, 1202);	

	delete rBuff;

func_exit:
	return res;
}



I32 GECP_ImageUpdate_both(void *pHand, CAM_IMG_ATTR_T  pGvcpImgAttr[2])		
{
	I32 res;
	BYTE *rBuff;
	T_GVCP_WRITE_LIST regList[32];
	T_GVCP_STREAM *pgvcpstr;
	U32			streamid;
	I32 regCount;

	I32 durationUsec;
	I32 i;

	//---
	if (pHand == NULL) {
		res = 0;
		goto func_exit;
	}

	pgvcpstr = (T_GVCP_STREAM *)pHand;
	//streamid = pgvcpstr->m_streamid;
	rBuff = new BYTE[1202];

	regCount = 0;

	for(i=0; i<2; i++)
	{
		//-- 
		streamid = i;
		regList[i*GEV_REG_NUM+0].addr = GVCP_VR_EXT_DIVIDER_C(streamid);
		regList[i*GEV_REG_NUM+0].value = pGvcpImgAttr[i].syncdiv;

		if ( (pGvcpImgAttr[i].width > MAXWIDTH || pGvcpImgAttr[i].width <= 0) || (pGvcpImgAttr[i].height > MAXHEIGHT || pGvcpImgAttr[i].height <= 0) ) {
			res = 0;
			goto func_exit;
		}

		// GECP_ImageSize
		regList[i*GEV_REG_NUM+1].addr = GVCP_VR_IMG_WIDTH_C(streamid);;
		regList[i*GEV_REG_NUM+1].value = pGvcpImgAttr[i].width;

		regList[i*GEV_REG_NUM+2].addr = GVCP_VR_IMG_HEIGHT_C(streamid);;
		regList[i*GEV_REG_NUM+2].value = pGvcpImgAttr[i].height;

		// GECP_ImagePosition
		if ( (pGvcpImgAttr[i].sx >= MAXWIDTH || pGvcpImgAttr[i].sx < 0) || (pGvcpImgAttr[i].sy >= MAXHEIGHT || pGvcpImgAttr[i].sy < 0) ) {
			res = 0;
			goto func_exit;
		}

		regList[i*GEV_REG_NUM+3].addr = GVCP_VR_IMG_OFFX_C(streamid);;
		regList[i*GEV_REG_NUM+3].value = pGvcpImgAttr[i].sx;

		regList[i*GEV_REG_NUM+4].addr = GVCP_VR_IMG_OFFY_C(streamid);;
		regList[i*GEV_REG_NUM+4].value = pGvcpImgAttr[i].sy;

		// GECP_ImageFps
		if (pGvcpImgAttr[i].fps > MAXFPS || pGvcpImgAttr[i].fps < MINFPS) {
			res = 0;
			goto func_exit;
		}

		durationUsec = (I32) (1e6 / pGvcpImgAttr[i].fps);		// 1000_000 / fps

		regList[i*GEV_REG_NUM+5].addr = GVCP_VR_DURATION_C(streamid);;
		regList[i*GEV_REG_NUM+5].value = durationUsec;

		// GECP_ImageGain
		if (pGvcpImgAttr[i].gain > MAXGAIN || pGvcpImgAttr[i].gain < MINGAIN) {
			res = 0;
			goto func_exit;
		}
		regList[i*GEV_REG_NUM+6].addr = GVCP_VR_IMG_GAIN_C(streamid);;
		regList[i*GEV_REG_NUM+6].value = pGvcpImgAttr[i].gain;

		// GECP_ImageExposure
		if (pGvcpImgAttr[i].exposureusec > MAXEXPOSURENUSEC) {
			pGvcpImgAttr[i].exposureusec = MAXEXPOSURENUSEC;
		}
		if (pGvcpImgAttr[i].exposureusec < MINEXPOSURENUSEC)
		{
			pGvcpImgAttr[i].exposureusec = MINEXPOSURENUSEC;
		}

		regList[i*GEV_REG_NUM+7].addr = GVCP_VR_SHUTTER_C(streamid);;
		regList[i*GEV_REG_NUM+7].value = pGvcpImgAttr[i].exposureusec;


		//GECP_ImageSkip
		if (pGvcpImgAttr[i].skip < 0 ) {
			pGvcpImgAttr[i].skip = 0;
		}
		regList[i*GEV_REG_NUM+8].addr = GVCP_VR_SKIP_C(streamid);;
		regList[i*GEV_REG_NUM+8].value = pGvcpImgAttr[i].skip;


		// GECP_ImageMultitude4Normal
		if (pGvcpImgAttr[i].multitude4normal < 0) {
			pGvcpImgAttr[i].multitude4normal = 0;
		}
		regList[i*GEV_REG_NUM+9].addr = GVCP_VR_MULTITUDE_C(streamid);;
		regList[i*GEV_REG_NUM+9].value = pGvcpImgAttr[i].multitude4normal;



		// GECP_CamFmcInit

		{
			U32 initcode;

			initcode = 0x00000001;					//b0:  Init FMC before start or NOT.
			if (pGvcpImgAttr[i].zrot) {
				initcode |= 0x00000002;				//    b1:           1: ZROT
			}

			if (pGvcpImgAttr[i].autoinit) {
				initcode |= 0x00000004;				//    b2:           1: Autoinit
			}

			regList[i*GEV_REG_NUM+10].addr = GVCP_VR_INIT_FMC_C(streamid);;
			regList[i*GEV_REG_NUM+10].value = initcode;
		}

		regList[i*GEV_REG_NUM+11].addr = GVCP_VR_EXPOSURE_POST_C(streamid);;
		regList[i*GEV_REG_NUM+11].value = pGvcpImgAttr[i].exposure_post;

		regList[i*GEV_REG_NUM+12].addr = GVCP_VR_EXPOSURE_EXPIATE_C(streamid);;
		regList[i*GEV_REG_NUM+12].value = pGvcpImgAttr[i].exposure_expiate;		
		
	}

	regCount = GEV_REG_NUM*2;	
	

	res = GECP_regWrite(pHand, regList, regCount, rBuff, 1202);

#if 0
	regCount = 10;
	res = GECP_regWrite(pHand, regList, regCount, rBuff, 1202);

	zrot;
	autoinit;


#endif
	{
		regList[0].addr = GVCP_VR_EXPOSURE_POST_C(0);
		regList[0].value = pGvcpImgAttr[0].exposure_post;
		regList[1].addr = GVCP_VR_EXPOSURE_EXPIATE_C(0);
		regList[1].value = pGvcpImgAttr[0].exposure_expiate;

		regList[2].addr = GVCP_VR_EXPOSURE_POST_C(1);
		regList[2].value = pGvcpImgAttr[1].exposure_post;
		regList[3].addr = GVCP_VR_EXPOSURE_EXPIATE_C(1);
		regList[3].value = pGvcpImgAttr[1].exposure_expiate;
		
		regCount = 4;
		res = GECP_regWrite(pHand, regList, regCount, rBuff, 1202);
	}

	delete rBuff;

func_exit:
	return res;
}


I32 GECP_heartbeat(void *pHand, 
	U32 heartbeattimeout			// msec.  0: Do not check heartbeat any more.
				   )	
{
	U32 value = heartbeattimeout;

	return writeResister(pHand, GECP_REG_HEARTBEAT_TIMEOUT, &value);
}



I32 GECP_SyncUpdate(void *pHand, U32 updateme // 0: NO update, 1: yes.. update me
					)	
{		

	U32 value = updateme;

	return writeResister(pHand, GECP_REG_SYNC_UPDATE, &value);
}

//-- 20190409, yhsuk.
I32 GECP_hboardinfo_read(void *pHand, U32 *rdata, U32 count)
{
	return readCommand(pHand, rdata, count, CRCMD_HBOARDINFO_READ);
}



I32 GECP_hboardinfo_write(void *pHand, U32 *sdata, U32 count)
{
	return writeCommand(pHand, sdata, count, CRCMD_HBOARDINFO_WRITE, DummyRdBuf, NULL);
}

I32 GECP_pcinfo_read(void *pHand, U32 *rdata, U32 count)
{
	return readCommand(pHand, rdata, count, CRCMD_PCINFOCODE_READ);
}



I32 GECP_pcinfo_write(void *pHand, U32 *sdata, U32 count)
{
	return writeCommand(pHand, sdata, count, CRCMD_PCINFOCODE_WRITE, DummyRdBuf, NULL);
}

I32 GECP_runcode_read(void *pHand, U32 *rdata, U32 count)
{
	return readCommand(pHand, rdata, count, CRCMD_RUNCODE_READ);
}

I32 GECP_runcode_write(void *pHand, U32 *sdata, U32 count)
{
	return writeCommand(pHand, sdata, count, CRCMD_RUNCODE_WRITE, DummyRdBuf, NULL);
}


I32 GECP_bddatarsvd_read(void *pHand, U32 *rdata, U32 count)
{
	return readCommand(pHand, rdata, count, CRCMD_BDDATARSVD_READ);
}

I32 GECP_bddatarsvd_write(void *pHand, U32 *sdata, U32 count)
{
	return writeCommand(pHand, sdata, count, CRCMD_BDDATARSVD_WRITE, DummyRdBuf, NULL);		
}


I32 GECP_cpusid_read(void *pHand, U32 *rdata, U32 count)
{
	return readCommand(pHand, rdata, count, CRCMD_CPUSID_READ);
}

I32 GECP_cpusid_write(void *pHand, U32 *sdata, U32 count)
{
	return writeCommand(pHand, sdata, count, CRCMD_CPUSID_WRITE, DummyRdBuf, NULL);
}

I32 GECP_cpubdserial_read(void *pHand, U32 *rdata, U32 count)
{
	return readCommand(pHand, rdata, count, CRCMD_CPUBDSERIAL_READ);
}


//GVCP_CRCMD_GetVersion
I32 GECP_version_get(void *pHand, char *pVersion)		// by ywchoi code..
	// sucess: 1,  fail: -1
{
	I32 res;
	U32 count;
	U32 rbuf[512];

	count = 64;
	res = readCommand(pHand, rbuf, count, CRCMD_GETVERSION);	
	if (res == 1) {
		memcpy(&pVersion[0], rbuf, count);
	}

	return res;
}

//GVCP_CRCMD_UpdateCache
I32 GECP_cache_update(void *pHand, U32 addr, U32 len)		// by ywchoi code..
{
	I32 res;
	U32 count;
	U32 sbuf[512];

	count = sizeof(U32) * 2;

	sbuf[0] = addr;
	sbuf[1] = len;

	res = writeCommand(pHand, sbuf, count, CRCMD_CACHE_UPDATE, DummyRdBuf, NULL);

	return res;	
}

//GVCP_CRCMD_CommitCache
I32 GECP_cache_commit(void *pHand, U32 addr, U32 len)		// by ywchoi code..
{
	I32 res;
	U32 count;
	U32 sbuf[512];

	count = sizeof(U32) * 2;

	sbuf[0] = addr;
	sbuf[1] = len;

	res = writeCommand(pHand, sbuf, count, CRCMD_CACHE_COMMIT, NULL, NULL);		

	return res; 
}

//GVCP_CRCMD_EraseFlash
I32 GECP_flash_erase(void *pHand, U32 addr, U32 len)		// by ywchoi code..
{
	I32 res;
	U32 count;
	U32 sbuf[512];

	count = sizeof(U32) * 2;

	sbuf[0] = addr;
	sbuf[1] = len;

	res = writeCommand(pHand, sbuf, count, CRCMD_FLASH_ERASE, NULL, NULL);		

	return res; 
}


//GVCP_CRCMD_Reboot
I32 GECP_reboot(void *pHand)		// by ywchoi code..
{
	I32 res;
	U32 count;
	U32 sbuf[512];

	count = 0;

	res = writeCommand(pHand, sbuf, count, CRCMD_REBOOT, NULL, NULL);		

	return res; 
}

//GVCP_CRCMD_RecoverGold 
I32 GECP_gold_recover(void *pHand)		// by ywchoi code..
{
	I32 res;
	U32 count;
	U32 sbuf[512];

	count = 0;
	
	res = writeCommand(pHand, sbuf, count, CRCMD_RECOVER_GOLD, NULL, NULL); 	

	return res; 
}


I32 GECP_cache_checksum(void *pHand, U32 addr, U32 len, U32 checksumtype, U32 *pchecksumresult, U32 *pstatus)
{
	I32 res;
	U32 count;
	U32 sbuf[512];

	count = sizeof(U32) * 3;

	sbuf[0] = addr;
	sbuf[1] = len;
	sbuf[2] = checksumtype;

	res = writeCommand(pHand, sbuf, count, CRCMD_CACHE_CHECMSUM, pchecksumresult, pstatus); 	
	
	return res;
}


/*
I32 GECP_MaskSet(void *pHand, U32 startx, U32 starty, U32 endx, U32 endy)
{
	BYTE *rBuff;
	T_GVCP_WRITE_LIST regList[2];
	T_GVCP_MAIN *pMainHand;
	//I32 rLen;
	I32 regCount;
	I32 res;
	U32 xy;

	//---
	res = 0;
	if (pHand == NULL) {
		goto func_exit;
	}


	pMainHand = (T_GVCP_MAIN *)pHand;
	rBuff = new BYTE[1202];

	regCount = 0;

	regList[regCount].addr = GVCP_VR_MASK_STARTPOS;
	xy = (startx & 0xFFFF) | ((starty << 16) & 0xFFFF0000);
	regList[regCount].value = xy;
	regCount++;


	regList[regCount].addr = GVCP_VR_MASK_ENDPOS;
	xy = (endx & 0xFFFF) | ((endy << 16) & 0xFFFF0000);
	regList[regCount].value = xy;
	regCount++;

	res = GECP_regWrite(pHand, regList, regCount, rBuff, 1202);

	delete rBuff;
func_exit:
	return res;
}



I32 GECP_MaskSave(void *pHand)
{
	BYTE *rBuff;
	T_GVCP_WRITE_LIST regList[1];
	T_GVCP_MAIN *pMainHand;
	//I32 rLen;
	I32 regCount;
	I32 res;

	//---
	res = 0;
	if (pHand == NULL) {
		goto func_exit;
	}


	pMainHand = (T_GVCP_MAIN *)pHand;
	rBuff = new BYTE[1202];

	regCount = 0;

	regList[regCount].addr = GVCP_VR_MASK_SAVE;
	regList[regCount].value = 1;				// 0: NOT save... trivial. 1: Save me.
	regCount++;


	res = GECP_regWrite(pHand, regList, regCount, rBuff, 1202);

	delete rBuff;
func_exit:
	return res;
}


I32 GECP_MaskCalc(void *pHand, U32 calc)
{
	BYTE *rBuff;
	T_GVCP_WRITE_LIST regList[1];
	T_GVCP_MAIN *pMainHand;
	I32 regCount;
	I32 res;

	//---
	res = 0;
	if (pHand == NULL) {
		goto func_exit;
	}


	pMainHand = (T_GVCP_MAIN *)pHand;
	rBuff = new BYTE[1202];

	regCount = 0;

	regList[regCount].addr = GVCP_VR_MASK_CALC;
	regList[regCount].value = calc;				// 0: STOP ... 1: START.. other? IDONNO.
	regCount++;


	res = GECP_regWrite(pHand, regList, regCount, rBuff, 1202);

	delete rBuff;
func_exit:
	return res;
}
*/







/*!
 ********************************************************************************
 *	@brief      Thread function of video cb. oneshot.
 *
 *  @return		TRUE if successful, FALSE otherwise
 *
 *	@note 
 *
 *	@author	    yhsuk
 *  @date       2015/1222
 *******************************************************************************/

#pragma warning(disable:4505)
/*static*/ void *CR_videoCbThreadFn(void *pParam)
{
	videocbcontext_t *pvct;

	//-- 
	if (pParam == NULL) {
		return 0;
	}
	pvct = (videocbcontext_t *) pParam;

	pvct->videoCB(
			pvct->pData,
			pvct->nSize,
			pvct->ts_high,
			pvct->ts_low,
			pvct->size_x,
			pvct->size_y,
			pvct->offset_x,
			pvct->offset_y,
			pvct->normalbulk,
			pvct->skip,
			pvct->multitude,
			pvct->pParam);

	free(pvct);
	return (void *)1;
}

#pragma warning(default:4505)

#if defined (__cplusplus)
}
#endif

/************** Not used  *******************/

#if 0
I32 GECP_start(void *pHand)
{
	I32 res;
	T_GVCP_STREAM *pgvcpstr;
	T_GVCP_MAIN *pMainHand;

	//--
	pgvcpstr = (T_GVCP_STREAM *)pHand;
	pMainHand = pgvcpstr->m_pgvcpm;

	res = GECP_CamGvcpStart(pHand);
	res = GECP_CamGvspStart(pHand);
	return res;
}

I32 GECP_setCamGvspCB(void *pHand, CR_video_fnCB fnCB, I32 nPort)
{

	T_GVCP_MAIN *pMainHand;

	if (pHand == NULL)
		return 0;
	pMainHand = (T_GVCP_MAIN *)pHand;

//	pMainHand->m_camPort = nPort;

	pMainHand->m_videoCB = fnCB;
	pMainHand->m_hostPort = nPort;

	return 1;
}

I32 GECP_setCamCB(void *pHand, CR_video_fnCB fnCB, I32 nPort)
{

	T_GVCP_MAIN *pMainHand;

	if (pHand == NULL)
		return 0;
	pMainHand = (T_GVCP_MAIN *)pHand;

//	pMainHand->m_camPort = nPort;

	pMainHand->m_videoCB = fnCB;
	pMainHand->m_hostPort = nPort;

	return 1;
}



#endif



