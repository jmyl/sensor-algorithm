/*!
 *******************************************************************************
                                                                                
                    CREATZ GEV message
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   crgev_message.cpp
	 @brief  GEV Message functions
	 @author Original: by hgmoon, modified by yhsuk
	 @date   2015/11/19 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>

#include "crgev.h"
#include "crgev_message.h"

#include "cr_osapi.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

//int SendPacket(BYTE *sBuff, int sLen, BYTE *rBuff);
/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
#if defined (__cplusplus)
extern "C" {
#endif

/*!
 ********************************************************************************
 *	@brief      Send a message using socket API
 *
 *  @param [in] pDatagram Pointer to datagram message to send
 *  @param [in] size Size of message to send (in bytes)
 *  @param [in] pSocket Pointer to socket for stream channel
 *  @param [in] pDest Destination address of the message
 *  @param [in] showData Flag to indicate if we want to dump datagram content to console
 *
 *  @return Received reply data  Length (byte)
 *
 *	@note 		This function is used to send a message to the specified destination
 *	@author	    hgmoon, yhsuk
 *  @date       2015/1119
 *******************************************************************************/

I32 GevSendMessage(U08 *pDatagram, I32 size, SOCKET *pSocket, struct sockaddr_in *pDest, BOOL showData, U08 *rBuff, I32 rSize)
{
	I32 addrLen = sizeof(struct sockaddr_in);
	I32 cnt;		// number of bytes sent
	I32 cnt2;
	I32 tmpcnt;
	I32 offset;
	GEV_STATUS status = GEV_STATUS_SUCCESS;

//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++\n");
	// Send datagram
//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%p]\n", (int)*pSocket);
	cnt2 = 0;

	if (*pSocket == INVALID_SOCKET) {
		cnt2 = -1;
		goto func_exit;
	}
#if 1
	{
#define WAITTIME_MSG_MS_CLEAR 1
//#define DUMMYREAD_LEN	256
#define DUMMYREAD_LEN	512
#define DUMMYREAD_COUNT	16
		DWORD optval = WAITTIME_MSG_MS_CLEAR;
		DWORD optval2[32];
		U08   readbuf[DUMMYREAD_LEN];
		int ret;
		int sizesize;
		int Err;
		int i;


		//--
		ret = setsockopt(*pSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&optval, sizeof(DWORD));
		if (ret == -1) {
			Err = WSAGetLastError();
		}

		sizesize = 32 * sizeof(DWORD);
		ret = getsockopt(*pSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&optval2[0], &sizesize);
		if (ret == -1) {
			Err = WSAGetLastError();
		}

//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" DUMMY clear..\n");
		for (i = 0; i < DUMMYREAD_COUNT; i++) {
			int bytes;
//			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" DUMMY read. %d\n", i);
			if(
					//ioctl
					ioctlsocket
					(*pSocket, FIONREAD, (u_long*)&bytes) != -1) {
//				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" bytes available.\n", bytes);
			}
			if (bytes <= 0) {
				break;
			}
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d bytes available. CLEAR!\n", bytes);

			if (bytes > DUMMYREAD_LEN) {
				bytes = DUMMYREAD_LEN;
			}
			cnt2 = recvfrom(*pSocket, (char *)readbuf, bytes, 0, NULL, NULL);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"  dummy read: %d,  result read: %d!\n", bytes, cnt2);
			//if (cnt2 <= 0) {
			//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"%d read %d\n", i, bytes);
			//	break;
			//}
		}
	}
#endif







	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%p], sendto\n", (int)*pSocket);
	cnt = sendto(*pSocket, (const char *)pDatagram, size, 0, (struct sockaddr *)pDest, addrLen);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%p], After sendto\n", (int)*pSocket);
	if (cnt < 0)
	{
		//printf("ERROR: Error in sendto (error %d)\n\n\b", cr_GetSocketError());		// XXX!!!!
		status = GEV_STATUS_ERROR;
		cnt2 = -1;
	}
	else
	{
		I32 t0, t1;
		{
#define WAITTIME_MSG_MS	10
//#define WAITTIME_MSG_MS2	1
//#define WAITTIME_MSG_MS2	10
//#define WAITTIME_MSG_MS2	50			// for windows... 
//#define WAITTIME_MSG_MS2	100			// for windows... 
//#define WAITTIME_MSG_MS2	20
/////#define WAITTIME_MSG_MS2	200			// for windows... 
#define WAITTIME_MSG_MS2	500			// for windows... 
			DWORD optval = WAITTIME_MSG_MS2;
			DWORD optval2[32];
			int ret;
			int sizesize;
			int Err;
//			optval = 10000;
			ret = setsockopt(*pSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&optval, sizeof(DWORD));
			if (ret == -1) {
				Err = WSAGetLastError();
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" setsockopt, err: %d\n", Err);
			}

			sizesize = 32 * sizeof(DWORD);
			ret = getsockopt(*pSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&optval2[0], &sizesize);
			if (ret == -1) {
				Err = WSAGetLastError();
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" getsockopt, err: %d\n", Err);
			}
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%p], timeout: %d -> %d (%d)\n", (int)*pSocket, optval, optval2[0], sizesize);
		}
		/*
		struct timeval  tv; 

		tv.tv_sec = 0; 
		tv.tv_usec = (100 * 1000);
		setsockopt(*pSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(tv)); 
		*/
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%p], recvfrom\n", (int)*pSocket);
		t0 = cr_gettickcount();
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" read. \n");
		cnt2 = recvfrom(*pSocket, (char *)rBuff, rSize, 0, NULL, NULL);
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"   DONE\n");
#if 0
		cnt = recvfrom(*pSocket, (char *)rBuff, rSize, 0, NULL, NULL);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"   DONE2\n");
#endif
		if (cnt2 >= 0) {
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%p] recvfrom, done (%d)\n", (int)*pSocket, cnt2);
		} else {
			t1 = cr_gettickcount();
			int Err;
			Err = WSAGetLastError();
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" recvfrom, err: %d, time diff: %d\n", Err, t1-t0);
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%p]------------------:recvfrom, done (%d)   \n",(int)*pSocket, cnt2);
		}

		if (showData)
		{
			// print destination socket
			//printf("-> Sending a message to "); PrintIP(ntohl(pDest->sin_addr.s_addr)); printf(" port %d:\n", ntohs(pDest->sin_port));
			// print message content
			tmpcnt = size / sizeof(U32);
			offset = 0;

			//while (tmpcnt--)
			//{
			//	mprj_print(_T("\t0x%04X => %02X "), offset, GET_BYTE(pDatagram, offset++));
			//	mprj_print(_T("%02X "), GET_BYTE(pDatagram, offset++));
			//	mprj_print(_T("%02X "), GET_BYTE(pDatagram, offset++));
			//	mprj_print(_T("%02X\n"), GET_BYTE(pDatagram, offset++));
			//}
		}
	}
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"--\n");
func_exit:
	return cnt2;
}




/*!
 ********************************************************************************
 *	@brief      Send a message using socket API. And Without reply.
 *
 *  @param [in] pDatagram Pointer to datagram message to send
 *  @param [in] size Size of message to send (in bytes)
 *  @param [in] pSocket Pointer to socket for stream channel
 *  @param [in] pDest Destination address of the message
 *  @param [in] showData Flag to indicate if we want to dump datagram content to console
 *
 *  @return 1: GOOD send. 0: BAD send.
 *
 *	@note 		This function is used to send a message to the specified destination
 *	@author	    hgmoon, yhsuk
 *  @date       2019/0428
 *******************************************************************************/

I32 GevSendMessageOnly(U08 *pDatagram, I32 size, SOCKET *pSocket, struct sockaddr_in *pDest, BOOL showData)
{
	I32 res;
	I32 addrLen = sizeof(struct sockaddr_in);
	I32 cnt;		// number of bytes sent

	//--

	if (*pSocket == INVALID_SOCKET) {
		res = 0;
		goto func_exit;
	}

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%p], sendto\n", (int)*pSocket);
	cnt = sendto(*pSocket, (const char *)pDatagram, size, 0, (struct sockaddr *)pDest, addrLen);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%p], After sendto\n", (int)*pSocket);
	if (cnt <= 0)
	{
		res = 0;
	} else {
		res = 1;
	}
	showData;
func_exit:
	return res;
}





/*!
 ********************************************************************************
 *	@brief      Broadcast a message using socket API
 *
 *  @param [in] pDatagram Pointer to datagram message to send
 *  @param [in] size Size of message to send (in bytes)
 *  @param [in] pSocket Pointer to socket for stream channel
 *  @param [in] pDest Destination address of the message
 *  @param [in] showData Flag to indicate if we want to dump datagram content to console
 *
 *  @return GEV status code
 *
 *	@note 		This function is used to broadcast a message
 *	@author	    hgmoon, yhsuk
 *  @date       2015/1119
 *******************************************************************************/

GEV_STATUS GevBroadcastMessage(U08 *pDatagram, int size, SOCKET *pSocket, struct sockaddr_in *pDest, BOOL showData)
{
	GEV_STATUS status = GEV_STATUS_SUCCESS;
	int option;		// used to hold the broadcast option
	struct sockaddr_in destination;

	//-------

	if (*pSocket == INVALID_SOCKET) {
		status = GEV_STATUS_ERROR;
		goto func_exit;
	}
	
	
	// Activate broadcast on this socket
	option = 1;
	setsockopt(*pSocket, SOL_SOCKET, SO_BROADCAST, (char *)&option, sizeof(option));
	destination = *pDest;
	destination.sin_addr.s_addr = INADDR_BROADCAST;	// enforce broadcast destination

	// Send the message using the regular function
	//status = GevSendMessage(pDatagram, size, pSocket, &destination, showData);

	// Deactivate broadcast
	option = 0;
	setsockopt(*pSocket, SOL_SOCKET, SO_BROADCAST, (char *)&option, sizeof(option));


	{
		showData;
		size;
		pDatagram;
	}

func_exit:
	return (GEV_STATUS) status;
}


/*!
 ********************************************************************************
 *	@brief      Receive a message using socket API
 *
 *  @param [in] pDatagram Pointer to datagram message to receive
 *  @param [in] maxSize Maximum size of message to receive (in bytes)
 *  @param [in] pSocket Pointer to socket for stream channel
 *  @param [in] pTimeout Timeout for select() operation
 *  @param [out] pAddr Pointer to IP address of the datagram
 *  @param [out] pPort Pointer to UDP port of the datagram
 *  @param [in] showData Flag to indicate if we want to dump datagram content to console
 *
 *  @return GEV status code
 *	@note 		This function is used to receive a message
 *	@author	    hgmoon, yhsuk
 *  @date       2015/1119
 *******************************************************************************/
GEV_STATUS GevReceiveMessage(U08 *pDatagram, int maxSize, SOCKET *pSocket, struct timeval *pTimeout, U32 *pAddr, U16 *pPort, BOOL showData)
{
	fd_set setRead;
	int notused = 0;
	int cnt;			// number of instance
//	int offset;
	struct sockaddr_in sa;
	int sa_len = sizeof(sa);
	GEV_STATUS status = GEV_STATUS_SUCCESS;

	//------------------

	if (*pSocket == INVALID_SOCKET) {
		status = GEV_STATUS_ERROR;
		goto func_exit;
	}
	
	// Add application socket to list
	FD_ZERO(&setRead);
#pragma warning(disable:4127)
	FD_SET(*pSocket, &setRead);
#pragma warning(default:4127)
	// Perform select operation to block until a port is ready
	cnt = select(notused, &setRead, NULL, NULL, pTimeout);
	if (cnt == SOCKET_ERROR)
	{
		printf("ERROR: select returned an error (error %d)\n\n\b", cr_GetSocketError());
	}

	// Check if at least one port has valid data
	if (cnt > 0)
	{
		// Check if this socket is ready
		if (FD_ISSET(*pSocket, &setRead))
		{
			// Read datagram
			memset(&sa, 0, sizeof(struct sockaddr_in));

			{
#define WAITTIME_MSG_MS	10
				I32 optval = WAITTIME_MSG_MS;
				setsockopt(*pSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&optval, sizeof(I32));
			}

			cnt = recvfrom(*pSocket, (char *)pDatagram, maxSize, 0, (struct sockaddr *)&sa, &sa_len);
			if (cnt > 0)
			{
				if (pAddr != NULL)	// only perform assignment if a valid buffer was provided
				{
					*pAddr = ntohl(sa.sin_addr.s_addr);
				}
				if (pPort != NULL)	// only perform assignment if a valid buffer was provided
				{
					*pPort = ntohs(sa.sin_port);
				}

				// Check if user wants to dump datagram content
				if (showData)
				{
					//// print source socket
					////printf("-> Received a message from "); PrintIP(ntohl(sa.sin_addr.s_addr)); printf(" port %d:\n", ntohs(sa.sin_port));
					//// print message content. Cannot use GVCP length field because this function is also
					//// called by GVSP.
					////					size = (U16)(GET_WORD( pDatagram, GVCP_LENGTH_OFFSET) + GVCP_HEADER_SIZE);
					////					cnt = size / sizeof (U32);
					//cnt /= sizeof(U32);
					//offset = 0;
					//while (cnt--)
					//{
					//	printf("\t0x%04X => %02X ", offset, GET_BYTE(pDatagram, offset++));
					//	printf("%02X ", GET_BYTE(pDatagram, offset++));
					//	printf("%02X ", GET_BYTE(pDatagram, offset++));
					//	printf("%02X\n", GET_BYTE(pDatagram, offset++));
					//	//printf ("\t0x%04X => 0x%08X\n", offset, htonl( GET_DWORD( pDatagram, offset)));
					//	//offset += 4;
					//}
				}
				status = GEV_STATUS_SUCCESS;
			}
		}
	}
	else
	{
		// No message!
		status = GEV_STATUS_NO_MSG;
	}
func_exit:
	return status;
}

#if defined (__cplusplus)
}
#endif



