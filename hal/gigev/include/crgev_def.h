/*!
 *******************************************************************************
                                                                                
                    CREATZ Definition for GEV
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   crgev_def.h
	 @brief  Socket for GEV
	 @author Orignal: TeddyBoy,  Modified by yhsuk
	 @date   2015/11/16 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_CRGEV_DEF_H_)
#define      _CRGEV_DEF_H_
/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define	GVCP_SUPPORTED_VERSION					0x00010000	// version 1.0
#define GVCP_KEY								0x42

/*
Bootstrap registers addresses
copy from epan-dissectors-packet-gvcp.c (wireShark)
*/
#define	GVCP_BOOTSTRAP_SIZE 					(0x0000A000)
#define GVCP_USER_REG_START 					GVCP_BOOTSTRAP_SIZE

#define GVCP_VERSION 							(0x00000000)
#define GVCP_DEVICE_MODE 						(0x00000004)
#define GVCP_DEVICE_MAC_HIGH_0 					(0x00000008)
#define GVCP_DEVICE_MAC_LOW_0 					(0x0000000c)
#define GVCP_SUPPORTED_IP_CONFIGURATION_0 		(0x00000010)
#define GVCP_CURIPCFG_0 						(0x00000014)
#define GVCP_CURRENT_IP_ADDRESS_0 				(0x00000024)
#define GVCP_CURRENT_SUBNET_MASK_0 				(0x00000034)
#define GVCP_CURRENT_DEFAULT_GATEWAY_0 			(0x00000044)
#define GVCP_MANUFACTURER_NAME 					(0x00000048)
#define GVCP_MODEL_NAME 						(0x00000068)
#define GVCP_DEVICE_VERSION 					(0x00000088)
#define GVCP_MANUFACTURER_INFO 					(0x000000a8)
#define GVCP_SERIAL_NUMBER 						(0x000000d8)
#define GVCP_USER_DEFINED_NAME 					(0x000000e8)
#define GVCP_FIRST_URL 							(0x00000200)
#define GVCP_SECOND_URL 						(0x00000400)
#define GVCP_NUMBER_OF_NETWORK_INTERFACES 		(0x00000600)
#define GVCP_PERSISTENT_IP_ADDRESS_0 			(0x0000064C)
#define GVCP_PERSISTENT_SUBNET_MASK_0 			(0x0000065C)
#define GVCP_PERSISTENT_DEFAULT_GATEWAY_0 		(0x0000066C)
#define GVCP_LINK_SPEED_0 						(0x00000670)
/*#define GVCP_DEVICE_MAC_HIGH_1 					(0x00000680)
#define GVCP_DEVICE_MAC_LOW_1 					(0x00000684)
#define GVCP_SUPPORTED_IP_CONFIGURATION_1 		(0x00000688)
#define GVCP_CURIPCFG_1 						(0x0000068C)
#define GVCP_CURRENT_IP_ADDRESS_1  				(0x0000069C)
#define GVCP_CURRENT_SUBNET_MASK_1 				(0x000006AC)
#define GVCP_CURRENT_DEFAULT_GATEWAY_1 			(0x000006BC)
#define GVCP_PERSISTENT_IP_ADDRESS_1 			(0x000006CC)
#define GVCP_PERSISTENT_SUBNET_MASK_1 			(0x000006DC)
#define GVCP_PERSISTENT_DEFAULT_GATEWAY_1 		(0x000006EC)
#define GVCP_LINK_SPEED_1 						(0x000006F0)
#define GVCP_DEVICE_MAC_HIGH_2 					(0x00000700)
#define GVCP_DEVICE_MAC_LOW_2 					(0x00000704)
#define GVCP_SUPPORTED_IP_CONFIGURATION_2 		(0x00000708)
#define GVCP_CURIPCFG_2 						(0x0000070C)
#define GVCP_CURRENT_IP_ADDRESS_2 				(0x0000071C)
#define GVCP_CURRENT_SUBNET_MASK_2 				(0x0000072C)
#define GVCP_CURRENT_DEFAULT_GATEWAY_2 			(0x0000073C)
#define GVCP_PERSISTENT_IP_ADDRESS_2 			(0x0000074C)
#define GVCP_PERSISTENT_SUBNET_MASK_2 			(0x0000075C)
#define GVCP_PERSISTENT_DEFAULT_GATEWAY_2 		(0x0000076C)
#define GVCP_LINK_SPEED_2 						(0x00000770)
#define GVCP_DEVICE_MAC_HIGH_3 					(0x00000780)
#define GVCP_DEVICE_MAC_LOW_3 					(0x00000784)
#define GVCP_SUPPORTED_IP_CONFIGURATION_3 		(0x00000788)
#define GVCP_CURIPCFG_3 						(0x0000078C)
#define GVCP_CURRENT_IP_ADDRESS_3 				(0x0000079C)
#define GVCP_CURRENT_SUBNET_MASK_3 				(0x000007AC)
#define GVCP_CURRENT_DEFAULT_GATEWAY_3 			(0x000007BC)
#define GVCP_PERSISTENT_IP_ADDRESS_3 			(0x000007CC)
#define GVCP_PERSISTENT_SUBNET_MASK_3 			(0x000007DC)
#define GVCP_PERSISTENT_DEFAULT_GATEWAY_3 		(0x000007EC)
#define GVCP_LINK_SPEED_3 						(0x000007F0)
#define GVCP_NUMBER_OF_MESSAGE_CHANNELS 		(0x00000900)
#define GVCP_NUMBER_OF_STREAM_CHANNELS 			(0x00000904)
#define GVCP_NUMBER_OF_ACTION_SIGNALS 			(0x00000908)
#define GVCP_ACTION_DEVICE_KEY 					(0x0000090C)
#define GVCP_NUMBER_OF_ACTIVE_LINKS 			(0x00000910)
#define GVCP_SC_CAPS 							(0x0000092C)
#define GVCP_MESSAGE_CHANNEL_CAPS 				(0x00000930)
#define GVCP_CAPABILITY 						(0x00000934)
*/
#define GVCP_HEARTBEAT_TIMEOUT 					(0x00000938)
/*#define GVCP_TIMESTAMP_TICK_FREQUENCY_HIGH 		(0x0000093c)
#define GVCP_TIMESTAMP_TICK_FREQUENCY_LOW 		(0x00000940)
#define GVCP_TIMESTAMP_CONTROL 					(0x00000944)
*/
#define GVCP_TIMESTAMP_VALUE_HIGH 				(0x00000948)
#define GVCP_TIMESTAMP_VALUE_LOW 				(0x0000094c)
/*#define GVCP_DISCOVERY_ACK_DELAY 				(0x00000950)
#define GVCP_CONFIGURATION 						(0x00000954)
#define GVCP_PENDING_TIMEOUT 					(0x00000958)
#define GVCP_CONTROL_SWITCHOVER_KEY 			(0x0000095C)
#define GVCP_GVSCP_CONFIGURATION 				(0x00000960)
#define GVCP_PHYSICAL_LINK_CAPABILITY 			(0x00000964)
#define GVCP_PHYSICAL_LINK_CONFIGURATION 		(0x00000968)
#define GVCP_IEEE_1588_STATUS 					(0x0000096C)
#define GVCP_SCHEDULED_ACTION_COMMAND_QUEUE_SIZE (0x00000970)
#define GVCP_CCP 								(0x00000a00)
#define GVCP_PRIMARY_APPLICATION_PORT 			(0x00000A04)
#define GVCP_PRIMARY_APPLICATION_IP_ADDRESS 	(0x00000A14)
#define GVCP_MC_DESTINATION_PORT 				(0x00000b00)
#define GVCP_MC_DESTINATION_ADDRESS 			(0x00000b10)
#define GVCP_MC_TIMEOUT 						(0x00000b14)
#define GVCP_MC_RETRY_COUNT 					(0x00000b18)
#define GVCP_MC_SOURCE_PORT 					(0x00000b1c)*/

/*
#define GVCP_MANIFEST_TABLE 					(0x00009000)
*/
#define GVCP_SC_DESTINATION_PORT(I)  			(0x0d00+(0x40*(I)))
/*
#define GVCP_SC_PACKET_SIZE(I) 					(0x0d04+(0x40*(I)))
#define GVCP_SC_PACKET_DELAY(I) 				(0x0d08+(0x40*(I)))
*/
#define GVCP_SC_DESTINATION_ADDRESS(I) 			(0x0d18+(0x40*(I)))
/*
#define GVCP_SC_SOURCE_PORT(I) 					(0x0d1c+(0x40*(I)))
#define GVCP_SC_CAPABILITY(I) 					(0x0d20+(0x40*(I)))
#define GVCP_SC_CONFIGURATION(I) 				(0x0d24+(0x40*(I)))
#define GVCP_SC_ZONE(I) 						(0x0d28+(0x40*(I)))
#define GVCP_SC_ZONE_DIRECTION(I) 				(0x0d2c+(0x40*(I)))

#define GVCP_ACTION_GROUP_KEY(I)  				(0x9800+(0x10*(I)))
#define GVCP_ACTION_GROUP_MASK(I) 				(0x9804+(0x10*(I)))
*/
/*
User Register
*/


/*
Command and acknowledge IDs
*/

#define GVCP_DISCOVERY_CMD 						(0x0002)
#define GVCP_DISCOVERY_ACK 						(0x0003)
#define GVCP_FORCEIP_CMD 						(0x0004)
#define GVCP_FORCEIP_ACK 						(0x0005)
#define GVCP_PACKETRESEND_CMD 					(0x0040)
#define GVCP_PACKETRESEND_ACK 					(0x0041)
#define GVCP_READREG_CMD 						(0x0080)
#define GVCP_READREG_ACK 						(0x0081)
#define GVCP_WRITEREG_CMD 						(0x0082)
#define GVCP_WRITEREG_ACK 						(0x0083)
#define GVCP_READMEM_CMD 						(0x0084)
#define GVCP_READMEM_ACK 						(0x0085)
#define GVCP_WRITEMEM_CMD 						(0x0086)
#define GVCP_WRITEMEM_ACK 						(0x0087)
#define GVCP_PENDING_ACK 						(0x0089)
#define GVCP_CRCMD_CMD							(0x00A0)			// by yhsuk. 20190409
#define GVCP_CRCMD_ACK							(0x00A1)			// by yhsuk. 20190409
#define GVCP_EVENT_CMD 							(0x00C0)
#define GVCP_EVENT_ACK 							(0x00C1)
#define GVCP_EVENTDATA_CMD 						(0x00C2)
#define GVCP_EVENTDATA_ACK 						(0x00C3)
#define GVCP_ACTION_CMD 						(0x0100)
#define GVCP_ACTION_ACK 						(0x0101)


#define GVCP_CURIPCFG_MASK_LLA					(0x00000004)
#define GVCP_CURIPCFG_MASK_DHCP					(0x00000002)
#define GVCP_CURIPCFG_MASK_STATIC				(0x00000001)


#define GEV_REGISTER_READ						0x00000001
#define GEV_REGISTER_WRITE						0x00000002
#define GEV_REGISTER_ALL						(GEV_REGISTER_READ | GEV_REGISTER_WRITE)

#define GEV_REGISTER_FORCE						0x00000004
#define GEV_REGISTER_STRING						0x00000080
//#define GEV_REGISTER_STRING64		0x00000090
//#define GEV_REGISTER_STRING512	0x000000A0


/*
GVCP statuses
*/
//! GVCP status code
typedef unsigned short							GEV_STATUS;

#define GEV_STATUS_SUCCESS 						(0x0000)
#define GEV_STATUS_PACKET_RESEND 				(0x0100)
#define GEV_STATUS_NOT_IMPLEMENTED 				(0x8001)
#define GEV_STATUS_INVALID_PARAMETER 			(0x8002)
#define GEV_STATUS_INVALID_ADDRESS 				(0x8003)
#define GEV_STATUS_WRITE_PROTECT 				(0x8004)
#define GEV_STATUS_BAD_ALIGNMENT 				(0x8005)
#define GEV_STATUS_ACCESS_DENIED 				(0x8006)
#define GEV_STATUS_BUSY 						(0x8007)
#define GEV_STATUS_LOCAL_PROBLEM 				(0x8008)  /* deprecated */
#define GEV_STATUS_MSG_MISMATCH 				(0x8009) /* deprecated */
#define GEV_STATUS_INVALID_PROTOCOL 			(0x800A) /* deprecated */
#define GEV_STATUS_NO_MSG 						(0x800B) /* deprecated */
#define GEV_STATUS_PACKET_UNAVAILABLE 			(0x800C)
#define GEV_STATUS_DATA_OVERRUN 				(0x800D)
#define GEV_STATUS_INVALID_HEADER 				(0x800E)
#define GEV_STATUS_WRONG_CONFIG 				(0x800F) /* deprecated */
#define GEV_STATUS_PACKET_NOT_YET_AVAILABLE 	(0x8010)
#define GEV_STATUS_PACKET_AND_PREV_REMOVED_FROM_MEMORY (0x8011)
#define GEV_STATUS_PACKET_REMOVED_FROM_MEMORY 	(0x8012)
#define GEV_STATUS_ERROR 						(0x8FFF)

/*
Link configurations
*/

#define GEV_LINKCONFIG_SINGLELINK 				(0x00)
#define GEV_LINKCONFIG_MULTIPLELINKS 			(0x01)
#define GEV_LINKCONFIG_STATICLAG 				(0x02)
#define GEV_LINKCONFIG_DYNAMICLAG 				(0x03)

/*
  Payload types
 */

#define GVSP_PAYLOAD_IMAGE 						(0x0001)
#define GVSP_PAYLOAD_RAWDATA 					(0x0002)
#define GVSP_PAYLOAD_FILE 						(0x0003)
#define GVSP_PAYLOAD_CHUNKDATA 					(0x0004)
#define GVSP_PAYLOAD_EXTENDEDCHUNKDATA 			(0x0005) /* Deprecated */
#define GVSP_PAYLOAD_JPEG 						(0x0006)
#define GVSP_PAYLOAD_JPEG2000 					(0x0007)
#define GVSP_PAYLOAD_H264 						(0x0008)
#define GVSP_PAYLOAD_MULTIZONEIMAGE 			(0x0009)
#define GVSP_PAYLOAD_DEVICEPSECIFICSTART 		(0x8000)


/*
   GVSP packet types
 */

#define GVSP_PACKET_LEADER 						( 1 )
#define GVSP_PACKET_TRAILER 					( 2 )
#define GVSP_PACKET_PAYLOAD 					( 3 )
#define GVSP_PACKET_ALLIN 						( 4 )
#define GVSP_PACKET_PAYLOAD_H264 				( 5 )
#define GVSP_PACKET_PAYLOAD_MULTIZONE 			( 6 )

/*
   Pixel type color
 */

#define GVSP_PIX_MONO             				(0x01000000)
#define GVSP_PIX_RGB              				(0x02000000)
#define GVSP_PIX_COLOR            				(0x02000000)
#define GVSP_PIX_CUSTOM           				(0x80000000)
#define GVSP_PIX_COLOR_MASK       				(0xFF000000)


/*
   Pixel type size
 */

#define GVSP_PIX_OCCUPY1BIT      				(0x00010000)
#define GVSP_PIX_OCCUPY2BIT      				(0x00020000)
#define GVSP_PIX_OCCUPY4BIT      				(0x00040000)
#define GVSP_PIX_OCCUPY8BIT      				(0x00080000)
#define GVSP_PIX_OCCUPY12BIT     				(0x000C0000)
#define GVSP_PIX_OCCUPY16BIT     				(0x00100000)
#define GVSP_PIX_OCCUPY24BIT     				(0x00180000)
#define GVSP_PIX_OCCUPY32BIT     				(0x00200000)
#define GVSP_PIX_OCCUPY36BIT     				(0x00240000)
#define GVSP_PIX_OCCUPY48BIT     				(0x00300000)


/*
   Pixel type masks, shifts
 */
#define GVSP_PIX_EFFECTIVE_PIXEL_SIZE_MASK 		(0x00FF0000)
#define GVSP_PIX_EFFECTIVE_PIXEL_SIZE_SHIFT 	(16)

#define GVSP_PIX_ID_MASK 						(0x0000FFFF)


/*
   Pixel types
 */

#define GVSP_PIX_COUNT (0x46)

#define GVSP_PIX_MONO1P                 	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY1BIT  | 0x0037)
#define GVSP_PIX_MONO2P                 	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY2BIT  | 0x0038)
#define GVSP_PIX_MONO4P                 	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY4BIT  | 0x0039)
#define GVSP_PIX_MONO8                  	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY8BIT  | 0x0001)
#define GVSP_PIX_MONO8S                 	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY8BIT  | 0x0002)
#define GVSP_PIX_MONO10                 	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY16BIT | 0x0003)
#define GVSP_PIX_MONO10_PACKED          	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY12BIT | 0x0004)
#define GVSP_PIX_MONO12                 	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY16BIT | 0x0005)
#define GVSP_PIX_MONO12_PACKED          	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY12BIT | 0x0006)
#define GVSP_PIX_MONO14                 	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY16BIT | 0x0025)
#define GVSP_PIX_MONO16                 	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY16BIT | 0x0007)

#define GVSP_PIX_BAYGR8                 	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY8BIT  | 0x0008)
#define GVSP_PIX_BAYRG8                 	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY8BIT  | 0x0009)
#define GVSP_PIX_BAYGB8                 	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY8BIT  | 0x000A)
#define GVSP_PIX_BAYBG8                 	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY8BIT  | 0x000B)
#define GVSP_PIX_BAYGR10                	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY16BIT | 0x000C)
#define GVSP_PIX_BAYRG10                	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY16BIT | 0x000D)
#define GVSP_PIX_BAYGB10                	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY16BIT | 0x000E)
#define GVSP_PIX_BAYBG10                	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY16BIT | 0x000F)
#define GVSP_PIX_BAYGR12                	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY16BIT | 0x0010)
#define GVSP_PIX_BAYRG12                	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY16BIT | 0x0011)
#define GVSP_PIX_BAYGB12                	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY16BIT | 0x0012)
#define GVSP_PIX_BAYBG12                	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY16BIT | 0x0013)
#define GVSP_PIX_BAYGR10_PACKED         	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY12BIT | 0x0026)
#define GVSP_PIX_BAYRG10_PACKED         	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY12BIT | 0x0027)
#define GVSP_PIX_BAYGB10_PACKED         	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY12BIT | 0x0028)
#define GVSP_PIX_BAYBG10_PACKED         	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY12BIT | 0x0029)
#define GVSP_PIX_BAYGR12_PACKED         	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY12BIT | 0x002A)
#define GVSP_PIX_BAYRG12_PACKED         	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY12BIT | 0x002B)
#define GVSP_PIX_BAYGB12_PACKED         	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY12BIT | 0x002C)
#define GVSP_PIX_BAYBG12_PACKED         	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY12BIT | 0x002D)
#define GVSP_PIX_BAYGR16                	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY16BIT | 0x002E)
#define GVSP_PIX_BAYRG16                	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY16BIT | 0x002F)
#define GVSP_PIX_BAYGB16                	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY16BIT | 0x0030)
#define GVSP_PIX_BAYBG16                	  	(GVSP_PIX_MONO  | GVSP_PIX_OCCUPY16BIT | 0x0031)

#define GVSP_PIX_RGB8                   		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY24BIT | 0x0014)
#define GVSP_PIX_BGR8                   		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY24BIT | 0x0015)
#define GVSP_PIX_RGBA8                  		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY32BIT | 0x0016)
#define GVSP_PIX_BGRA8                  		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY32BIT | 0x0017)
#define GVSP_PIX_RGB10                  		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY48BIT | 0x0018)
#define GVSP_PIX_BGR10                  		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY48BIT | 0x0019)
#define GVSP_PIX_RGB12                  		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY48BIT | 0x001A)
#define GVSP_PIX_BGR12                  		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY48BIT | 0x001B)
#define GVSP_PIX_RGB16                  		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY48BIT | 0x0033)
#define GVSP_PIX_RGB10V1_PACKED         		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY32BIT | 0x001C)
#define GVSP_PIX_RGB10P32               		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY32BIT | 0x001D)
#define GVSP_PIX_RGB12V1_PACKED         		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY36BIT | 0x0034)
#define GVSP_PIX_RGB565P                		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY16BIT | 0x0035)
#define GVSP_PIX_BGR565P                		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY16BIT | 0x0036)

#define GVSP_PIX_YUV411_8_UYYVYY        		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY12BIT | 0x001E)
#define GVSP_PIX_YUV422_8_UYVY          		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY16BIT | 0x001F)
#define GVSP_PIX_YUV422_8               		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY16BIT | 0x0032)
#define GVSP_PIX_YUV8_UYV               		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY24BIT | 0x0020)
#define GVSP_PIX_YCBCR8_CBYCR           		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY24BIT | 0x003A)
#define GVSP_PIX_YCBCR422_8             		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY16BIT | 0x003B)
#define GVSP_PIX_YCBCR422_8_CBYCRY      		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY16BIT | 0x0043)
#define GVSP_PIX_YCBCR422_8_CBYYCRYY    		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY12BIT | 0x003C)
#define GVSP_PIX_YCBCR601_8_CBYCR       		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY24BIT | 0x003D)
#define GVSP_PIX_YCBCR601_422_8         		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY16BIT | 0x003E)
#define GVSP_PIX_YCBCR601_422_8_CBYCRY  		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY16BIT | 0x0044)
#define GVSP_PIX_YCBCR601_411_8_CBYYCRYY		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY12BIT | 0x003F)
#define GVSP_PIX_YCBCR709_411_8_CBYCR   		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY24BIT | 0x0040)
#define GVSP_PIX_YCBCR709_422_8         		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY16BIT | 0x0041)
#define GVSP_PIX_YCBCR709_422_8_CBYCRY  		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY16BIT | 0x0045)
#define GVSP_PIX_YCBCR709_411_8_CBYYCRYY		(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY12BIT | 0x0042)

#define GVSP_PIX_RGB8_PLANAR         			(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY24BIT | 0x0021)
#define GVSP_PIX_RGB10_PLANAR        			(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY48BIT | 0x0022)
#define GVSP_PIX_RGB12_PLANAR        			(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY48BIT | 0x0023)
#define GVSP_PIX_RGB16_PLANAR        			(GVSP_PIX_COLOR | GVSP_PIX_OCCUPY48BIT | 0x0024)

/*
 * Vensor Registers
 */
#define GVCP_VR_OFFSET							0xA000
#define GVCP_VR_CAMOFFSET						0x0400
#define GVCP_VR_SENSOR_TYPE						(GVCP_VR_OFFSET +0x0000)
#define GVCP_VR_PIXEL_TYPE						(GVCP_VR_OFFSET +0x0004)
#define GVCP_VR_SENSOR_WIDTH					(GVCP_VR_OFFSET +0x0008)
#define GVCP_VR_SENSOR_HEIGH					(GVCP_VR_OFFSET +0x000C)

#define GVCP_VR_IMG_OFFX						(GVCP_VR_OFFSET +0x0010)
#define GVCP_VR_IMG_OFFY						(GVCP_VR_OFFSET +0x0014)
#define GVCP_VR_IMG_WIDTH						(GVCP_VR_OFFSET +0x0018)
#define GVCP_VR_IMG_HEIGHT						(GVCP_VR_OFFSET +0x001C)

#define GVCP_VR_IMG_BRIGHT						(GVCP_VR_OFFSET +0x0020)
#define GVCP_VR_IMG_CONTRAST					(GVCP_VR_OFFSET +0x0024)
#define GVCP_VR_IMG_GAMMA						(GVCP_VR_OFFSET +0x0028)
#define GVCP_VR_IMG_GAIN						(GVCP_VR_OFFSET +0x002C)


#define GVCP_VR_EXPOSURE_MOD					(GVCP_VR_OFFSET +0x0030)
#define GVCP_VR_DURATION						(GVCP_VR_OFFSET +0x0034)	// Frame duration (nSec)
#define GVCP_VR_SHUTTER							(GVCP_VR_OFFSET +0x0038)	// Shutter time (nSec)
#define GVCP_VR_LED_PULSE						(GVCP_VR_OFFSET +0x003C)	// LED on time (nSec)


#define GVCP_VR_GRAB_CTRL						(GVCP_VR_OFFSET +0x0040)
#define GVCP_VR_REQUEST_FRAM					(GVCP_VR_OFFSET +0x0044)	// hi16 : start frame. lo16 : end frame

//#define GVCP_VR_SKIP							(GVCP_VR_OFFSET +0x0070)				// yhsuk.. 2015/11/25
#define GVCP_VR_SKIP							(GVCP_VR_OFFSET +0x0048)				// yhsuk.. 2015/11/25

#define GVCP_VR_SEND_CTRL						(GVCP_VR_OFFSET +0x04C)	// 2019/01/24

#define GVCP_VR_SW_TRIGGER						(GVCP_VR_OFFSET +0x0050)
#define GVCP_VR_EXT_DIVIDER						(GVCP_VR_OFFSET +0x0054)

#define GVCP_VR_EXPOSURE_POST					(GVCP_VR_OFFSET +0x0058)		// Post Exposure SYNO time.  yhsuk  2017/0325
#define GVCP_VR_EXPOSURE_EXPIATE				(GVCP_VR_OFFSET +0x005C)	// Expiate Post Exposure SYNO time.  yhsuk  2017/0325



#define GVCP_VR_TIMER_CMD						(GVCP_VR_OFFSET +0x0060)
#define GVCP_VR_TIMER_CMD_SET_MASTER_STOP			1
#define GVCP_VR_TIMER_CMD_SET_SLAVE_RUN				2
#define GVCP_VR_TIMER_CMD_SET_MASTER_RUN			3
#define GVCP_VR_TIMER_CMD_SET_VIRTUAL_SLAVE_RUN_1	4		//  Timer tick 이 0가 아닐 경우 Master 로 전환
#define GVCP_VR_TIMER_CMD_SET_VIRTUAL_SLAVE_RUN_2	5		//  최초로 Image Capture 발생시 Master 로 전환
#define GVCP_VR_TIMER_CMD_SET_VIRTUAL_SLAVE_RUN_3	6		//  VIRTUAL_SLAVE_RUN_1, _2 의 OR 조건.


#define GVCP_VR_EXPOSURE_MODE_MASTER			1
#define GVCP_VR_EXPOSURE_MODE_SLAVE				0


#define GVCP_VR_EXPOSURE_DELAYOFFSET			(GVCP_VR_OFFSET +0x006C)		// Exposure delay offset.  yhsuk  20190213
//---------------------------------------------
#define GVCP_VR_BULK_TIMESTAMP_HIGH 	(GVCP_VR_OFFSET + 0x70)		// BULK sending, from TIMESTAMP, high 32bit.   	yhsuk 2015/11/26
#define GVCP_VR_BULK_TIMESTAMP_LOW 		(GVCP_VR_OFFSET + 0x74)		// BULK sending, from TIMESTAMP, low  32bit.   	yhsuk 2015/11/26
#define GVCP_VR_BULK_REQUEST 			(GVCP_VR_OFFSET + 0x78)		// BULK sending request
// b31 ~ b24: Reserved,
// b23 ~ b20: skip4bulk
// b19 ~ b16: multitude4bulk
// b15      : continue after BULK Xfer. 0: stop, 1: continue
// b14 ~ b08: Wait_time. Sleep time = (1 <<(wait_time)) usec
// b07 ~ b00: Frame count.
#define GVCP_VR_MULTITUDE 				(GVCP_VR_OFFSET + 0x7c)		// MULTITUDE.    b3 ~ b0: multitude value for normal operation.  b31 ~ b4: Reserved.
#define GVCP_VR_INIT_FMC 				(GVCP_VR_OFFSET + 0x80)		// INIT FMC.    b0:  Init FMC before start or NOT   b1: 0: NZROT, 1: ZROT


//-------------
#define GVCP_VR_BULK_IMG_WIDTH			(GVCP_VR_OFFSET + 0x84)
#define GVCP_VR_BULK_IMG_HEIGHT			(GVCP_VR_OFFSET + 0x88)
#define GVCP_VR_BULK_IMG_OFFX			(GVCP_VR_OFFSET + 0x8C)
#define GVCP_VR_BULK_IMG_OFFY			(GVCP_VR_OFFSET + 0x90)
#define GVCP_VR_BULK_TIMESTAMP_SHOT_HIGH (GVCP_VR_OFFSET + 0x94)
#define GVCP_VR_BULK_TIMESTAMP_SHOT_LOW	(GVCP_VR_OFFSET + 0x98)
#define GVCP_VR_BULK_STARTX_A			(GVCP_VR_OFFSET + 0x9C)
#define GVCP_VR_BULK_STARTX_B			(GVCP_VR_OFFSET + 0xA0)
#define GVCP_VR_BULK_STARTY_A			(GVCP_VR_OFFSET + 0xA4)
#define GVCP_VR_BULK_STARTY_B			(GVCP_VR_OFFSET + 0xA8)

/*
#define GVCP_VR_MASK_SAVE				(GVCP_VR_OFFSET + 0xB0)
#define GVCP_VR_MASK_STARTPOS			(GVCP_VR_OFFSET + 0xB4)
#define GVCP_VR_MASK_ENDPOS				(GVCP_VR_OFFSET + 0xB8)
#define GVCP_VR_MASK_CALC				(GVCP_VR_OFFSET + 0xBC)
*/


//------------------------------------------------------------------------------------------------
#define GVCP_VR_SENSOR_TYPE_C(C)				(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0000)
#define GVCP_VR_PIXEL_TYPE_C(C)						(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0004)
#define GVCP_VR_SENSOR_WIDTH_C(C)					(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0008)
#define GVCP_VR_SENSOR_HEIGH_C(C)					(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x000C)

#define GVCP_VR_IMG_OFFX_C(C)						(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0010)
#define GVCP_VR_IMG_OFFY_C(C)						(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0014)
#define GVCP_VR_IMG_WIDTH_C(C)						(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0018)
#define GVCP_VR_IMG_HEIGHT_C(C)						(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x001C)

#define GVCP_VR_IMG_BRIGHT_C(C)						(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0020)
#define GVCP_VR_IMG_CONTRAST_C(C)					(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0024)
#define GVCP_VR_IMG_GAMMA_C(C)						(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0028)
#define GVCP_VR_IMG_GAIN_C(C)						(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x002C)


#define GVCP_VR_EXPOSURE_MOD_C(C)					(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0030)
#define GVCP_VR_DURATION_C(C)						(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0034)	// Frame duration (nSec)
#define GVCP_VR_SHUTTER_C(C)							(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0038)	// Shutter time (nSec)
#define GVCP_VR_LED_PULSE_C(C)						(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x003C)	// LED on time (nSec)


#define GVCP_VR_GRAB_CTRL_C(C)						(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0040)
#define GVCP_VR_REQUEST_FRAM_C(C)					(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0044)	// hi16 : start frame. lo16 : end frame

//#define GVCP_VR_SKIP							(GVCP_VR_OFFSET +0x0070)				// yhsuk.. 2015/11/25
#define GVCP_VR_SKIP_C(C)							(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0048)				// yhsuk.. 2015/11/25

#define GVCP_VR_SEND_CTRL_C(C)						(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x004C)				// 2019/01/24


#define GVCP_VR_SW_TRIGGER_C(C)						(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0050)
#define GVCP_VR_EXT_DIVIDER_C(C)					(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0054)

#define GVCP_VR_EXPOSURE_POST_C(C)					(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0058)		// Post Exposure SYNO time.  yhsuk  2017/0325
#define GVCP_VR_EXPOSURE_EXPIATE_C(C)				(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x005C)	// Expiate Post Exposure SYNO time.  yhsuk  2017/0325



#define GVCP_VR_TIMER_CMD_C(C)						(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0060)
#define GVCP_VR_TIMER_CMD_SET_MASTER_STOP			1
#define GVCP_VR_TIMER_CMD_SET_SLAVE_RUN				2
#define GVCP_VR_TIMER_CMD_SET_MASTER_RUN			3
#define GVCP_VR_TIMER_CMD_SET_VIRTUAL_SLAVE_RUN_1	4		//  Timer tick 이 0가 아닐 경우 Master 로 전환
#define GVCP_VR_TIMER_CMD_SET_VIRTUAL_SLAVE_RUN_2	5		//  최초로 Image Capture 발생시 Master 로 전환
#define GVCP_VR_TIMER_CMD_SET_VIRTUAL_SLAVE_RUN_3	6		//  VIRTUAL_SLAVE_RUN_1, _2 의 OR 조건.


#define GVCP_VR_EXPOSURE_MODE_MASTER			1
#define GVCP_VR_EXPOSURE_MODE_SLAVE				0


#define GVCP_VR_EXPOSURE_DELAYOFFSET_C(C)		(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x006C)		// Exposure delay offset.  yhsuk  20190213

//---------------------------------------------
#define GVCP_VR_BULK_TIMESTAMP_HIGH_C(C) 	(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) + 0x70)		// BULK sending, from TIMESTAMP, high 32bit.   	yhsuk 2015/11/26
#define GVCP_VR_BULK_TIMESTAMP_LOW_C(C) 		(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) + 0x74)		// BULK sending, from TIMESTAMP, low  32bit.   	yhsuk 2015/11/26
#define GVCP_VR_BULK_REQUEST_C(C) 			(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) + 0x78)		// BULK sending request
// b31 ~ b24: Reserved,
// b23 ~ b20: skip4bulk
// b19 ~ b16: multitude4bulk
// b15      : continue after BULK Xfer. 0: stop, 1: continue
// b14 ~ b08: Wait_time. Sleep time = (1 <<(wait_time)) usec
// b07 ~ b00: Frame count.
#define GVCP_VR_MULTITUDE_C(C) 				(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) + 0x7c)		// MULTITUDE.    b3 ~ b0: multitude value for normal operation.  b31 ~ b4: Reserved.
#define GVCP_VR_INIT_FMC_C(C) 				(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) + 0x80)		// INIT FMC.    b0:  Init FMC before start or NOT   b1: 0: NZROT, 1: ZROT


//-------------
#define GVCP_VR_BULK_IMG_WIDTH_C(C)			(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) + 0x84)
#define GVCP_VR_BULK_IMG_HEIGHT_C(C)			(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) + 0x88)
#define GVCP_VR_BULK_IMG_OFFX_C(C)			(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) + 0x8C)
#define GVCP_VR_BULK_IMG_OFFY_C(C)			(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) + 0x90)
#define GVCP_VR_BULK_TIMESTAMP_SHOT_HIGH_C(C) (GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) + 0x94)
#define GVCP_VR_BULK_TIMESTAMP_SHOT_LOW_C(C)	(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) + 0x98)
#define GVCP_VR_BULK_STARTX_A_C(C)			(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) + 0x9C)
#define GVCP_VR_BULK_STARTX_B_C(C)			(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) + 0xA0)
#define GVCP_VR_BULK_STARTY_A_C(C)			(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) + 0xA4)
#define GVCP_VR_BULK_STARTY_B_C(C)			(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) + 0xA8)

#define GVCP_VR_SYNC_UPDATE_C(C)			(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0E4)// Sync update..
#define GVCP_VR_LED_BRIGHTNESS_C(C)			(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0E8)		// For LED Brightness control
#define GVCP_VR_SYNC_EDGE_PERIOD_C(C)		(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0EC)		// Period of sync pattern
#define GVCP_VR_SYNC_EDGE_A_NUM_C(C)			(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0F0)		// Number of pair of Sync Rising/Falling edge for Camera
#define GVCP_VR_SYNC_EDGE_B_NUM_C(C)			(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0F4)		// Number of pair of Sync Rising/Falling edge for LED
#define GVCP_VR_SYNC_EDGE_A_ENABLE_C(C)			(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0F8)		// Enable Camera sync pulse
#define GVCP_VR_SYNC_EDGE_B_ENABLE_C(C)			(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x0FC)		// Enable LED sync pulse
#define GVCP_VR_SYNC_EDGE_A_C(C,I)				(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x100+(0x04*I))	// Rising/Falling edge timing for CAM sync pulse, I:0~15
#define GVCP_VR_SYNC_EDGE_B_C(C,I)				(GVCP_VR_OFFSET+((C)*GVCP_VR_CAMOFFSET) +0x140+(0x04*I))	// Rising/Falling edge timing for LED sync pulse, I:0~15




//------------------------------------------------------------------------------
// CRCMD.  2019/0409
#define CRCMD_PARAMCOUNT	32
//#define CRCMD_PARAMCOUNT	16

#define CRCMD_NULL			0x0000
#define CRCMD_HBOARDINFO_READ		0x00000010
#define CRCMD_HBOARDINFO_WRITE		0x00000011
#define CRCMD_PCINFOCODE_READ		0x00000012
#define CRCMD_PCINFOCODE_WRITE		0x00000013
#define CRCMD_RUNCODE_READ			0x00000014
#define CRCMD_RUNCODE_WRITE			0x00000015
#define CRCMD_CPUSID_READ			0x00000016
#define CRCMD_CPUSID_WRITE			0x00000017
#define CRCMD_CPUBDSERIAL_READ		0x00000018
#define CRCMD_BDDATARSVD_READ		0x00000019
#define CRCMD_BDDATARSVD_WRITE		0x0000001A


#define	CRCMD_CACHE_UPDATE			(0x00000020)
#define	CRCMD_CACHE_COMMIT			(0x00000021)
#define	CRCMD_FLASH_ERASE			(0x00000022)
#define	CRCMD_GETVERSION			(0x00000023)
#define	CRCMD_REBOOT				(0x00000024)
#define	CRCMD_RECOVER_GOLD			(0x00000025)
#define	CRCMD_CACHE_CHECMSUM		(0x00000026)

//------------------------------------------------------------------------------


/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif
//#define ZHUANG_EXTENDED_BLOCKID						// 2015/11/27
#define EI_ENABLE						(1)
typedef struct _T_GVSP_HEADER
{
	U16 status;
	U16 block_id;
	U08  p_fmt;
	U08 packet_id[3];
#if defined(ZHUANG_EXTENDED_BLOCKID)				// Fake.. for ZHUANG-defined parameter.. :)
	u32 block_id64_h;
	u32 block_id64_l;
	u32 block_id32;
#endif
} T_GVSP_HEADER;

typedef struct _T_GVSP_DATA_LEADER
{
	T_GVSP_HEADER header;
	U16	type_spec;
	U16	payload_type;
	U32 ts_high;
	U32 ts_low;
	U32 pix_fmt;
	U32 size_x;
	U32 size_y;
	U32 offset_x;
	U32 offset_y;
	U16 pad_x;
	U16 pad_y;
} T_GVSP_DATA_LEADER;

typedef struct _T_GVSP_DATA_TRAILER
{
	T_GVSP_HEADER header;
	U16 res;
	U16 payload_type;
	U32 size_y;
} T_GVSP_DATA_TRAILER;


typedef struct _T_GVCP_HEADER
{
	U08  hard_key; // 0x42
	U08  flag;
	U16 command;
	U16 length;
	U16 req_id;
} T_GVCP_HEADER;


typedef struct _T_GVCP_HEADER_ACK
{
	U16 status;
	U16 answer;
	U16 length;
	U16 ack_id;
} T_GVCP_HEADER_ACK;

typedef struct _T_GVCP_DISCOVERY_ACK
{
	T_GVCP_HEADER_ACK header;
	//U16 ver_major;
	//U16 ver_minor;
	U32 version;
	U32 device_mode;
	U16 res1;
	U16 mac_high;
	U32 mac_low;
	U32 ip_config_opt;
	U32 ip_config_curr;
	U32 res2[3];
	U32 ip_curr;
	U32 res3[3];
	U32 ip_mask;
	U32 res4[3];
	U32 ip_gw;
	char mf_name[32];
	char model_name[32];
	char device_ver[32];
	char mf_spec[48];
	char mf_sn[16];
	char user_name[16];
} T_GVCP_DISCOVERY_ACK;

#if defined (__cplusplus)
}
#endif



#endif // __GIGE_V_DEF_H_
