/*!
 *******************************************************************************
                                                                                
                    CREATZ GEV
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   crgev.h
	 @brief  GEV
	 @author Original: by hgmoon, modified by yhsuk
	 @date   2015/11/16 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_CRGEV_H_)
#define		 _CRGEV_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>

#if defined(_WIN32)
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <winsock2.h>

#include <windows.h>
#endif

#include "cr_common.h"
#include "crgev_def.h"
#include "cr_network.h"
#include "crgev_information.h"
#include "cr_thread.h"
#include "scamif.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

#if defined(_WIN32)
#define		THREADPRIORITY_GEV_CAPTURE		THREAD_PRIORITY_TIME_CRITICAL
#define		THREADPRIORITY_GEV_MSG			THREAD_PRIORITY_NORMAL
#else
#define		THREADPRIORITY_GEV_CAPTURE		MAX_USER_PRIORITY
#define		THREADPRIORITY_GEV_MSG			(MIN_USER_PRIORITY  + 10)
#endif

#define		GEV_MAXCAM		4

enum	_enum_gev_runmode {
	GEV_RUNMODE_NORMAL	= 		0x0000,
	GEV_RUNMODE_IOCP	=		0x0001,
	GEV_RUNMODE_EPOLL	=	0x0002
};

#if !defined(NORMALBULK_NONE)
enum	_enum_normalbulk {
	NORMALBULK_NONE		= 	0,
	NORMALBULK_NORMAL	=	1,
	NORMALBULK_BULK		=	2
};
#endif
enum	_enum_gvsp_mode {
	GVSP_MODE_NULL		= 	0,
	GVSP_MODE_SINGLE	=	1,
	GVSP_MODE_MULT		=	2
};



enum	_enum_gev_streaming_channel {
	GEV_STREAMING_CHANNEL_SINGLE		= 	0,
	GEV_STREAMING_CHANNEL_MULTI	=	1,
};


enum
{
	GECP_UPDATE_MODE_DEFAULT = 0,
	GECP_UPDATE_MODE_EXT,		
};


/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/
#if defined (__cplusplus)
extern "C" {
#endif

typedef struct
{
	U32 addr;
	U32 value;
} T_GVCP_REG_LIST, T_GVCP_WRITE_LIST, T_GVCP_READ_LIST;

#define RCV_IMG_COUNT	1
//#define RCV_IMG_COUNT	2
//#define RCV_IMG_COUNT	4
typedef struct
{
	//	BYTE rcv_img[1280 * 1024];
	BYTE rcv_img[RCV_IMG_COUNT][1280 * 1024];

	//	BYTE disp_img[1280 * 1024];
	U32 ts_high;				// TimeStamp High 32bit
	U32 ts_low;				// TimeStamp Low 32bit
	I32 size_x;
	I32 size_y;
	I32 offset_x;
	I32 offset_y;

	volatile I32 rcv;
	I32 bID;
	I32 pID;
	I32 tPack;

	I32 prv_bID;
	I32 dispFrame;
	I32 rcvFrame;
	U64 prcvTime;


	U32	normalbulk;	// 0: NONE, 1: NORMAL, 2: BULK
	U32 skip;		// skip count   		추가 필요.
	U32 multitude;	// multitude count	추가 필요.

} T_RCV_CONTROL;


// Callback function.

typedef I32(*CR_msg_fnCB)(void *pData, I32 nSize, void *pParam);


typedef struct
{
	I32					m_hostGvspPort;				// GVSP shot port number.
	SOCKET				m_soGvsp;					// GVSP host socket
	struct sockaddr_in  m_sAddrGvsp;
	I32					m_isGvspActive;

	CR_video_fnCB		m_videoCB;					// GVSP, streaming function CB

	T_RCV_CONTROL		m_Image;


	HANDLE				m_hCaptureThread;

	I32					m_nCapStop;

} T_GVSP;

typedef struct
{
//	CR_THREAD			m_hAppThread;
//	HAND				m_hAppThread;
//	ApplicationInfo		m_appInfo;
	//-----------------
	HAND 				m_hmutex;

	char				m_hostIPstr[64];				// Application (== my) Ip addresss :)
	U32					m_hostIP;						// host...

	char				m_camIPstr[64];				// Device (== Camera) Ip address
	U32					m_camIP;

	I32					m_camGvcpPort;				// GVCP cam port number. (3956)
	SOCKET				m_soGvcp;					// GVCP cam socket
	struct sockaddr_in  m_sAddrGvcp;
	I32					m_isGvcpActive;
//	T_GVSP_MAIN			m_gvsp;
	U32					m_streamcount;					// 
	U32					m_stchannelmode;			// streaming channel mode.. single? multi?
	HAND				m_hgvcpstr[GEV_MAXCAM];				//   gvcpobject... cross reference.. :P

	T_GVSP				m_gvsp[GEV_MAXCAM];					// Max channel.. :P

	void				*m_hgev;
	void				*m_hParent[GEV_MAXCAM];


	CR_msg_fnCB			m_msgCB;
	I32					m_msgPort;

	void				*m_pMsgParam;
	HANDLE				m_hMsgThread;
	I32					m_nMsgStop;

	I32 				m_nReqId;
} T_GVCP_MAIN;

typedef struct 
{
	T_GVCP_MAIN *m_pgvcpm;
	U32			m_streamid;
	U32			m_stchannelmode;			// streaming channel mode.. single? multi?
} T_GVCP_STREAM;

typedef struct
{
	U32		runmode;
	U32		stchannelmode;
	U32		devicecount;

	HAND	hParent[GEV_MAXCAM];

	T_GVCP_STREAM gvcpstr[GEV_MAXCAM];			// Object of Stream..
//	T_GVCP_MAIN	*gecp[GEV_MAXCAM];				// Handle of Device Count
} T_GEV;


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/

//void *GEV_create(void *pParent, U32 camcount, U32 runmode);
void *GEV_create(HAND pP[], U32 camcount, U32 runmode);
void *GEV_create2(HAND pP[], U32 camcount, U32 runmode, U32 stchannelmode);


void GEV_delete(void *pHand);
//void* GECP_create(void *pParent);
//void GECP_destroy(void *pHand);

HAND GEV_GECP_hand(HAND hgev, U32 camid);
I32 GECP_start(void *pParent);
I32 GECP_stop(void *pHand);

I32 GECP_regWrite(void *pHand, T_GVCP_WRITE_LIST *regList, I32 regCount, BYTE *rBuff, I32 rSize);
I32 GECP_regRead(void *pHand, T_GVCP_READ_LIST *regList, I32 regCount);
I32 GECP_crcmdWrite(void *pHand, U32 cmd, U32 count, U32 *data, U32 *rdata, U32 *status);

I32 GECP_memWrite(void *pHand, U32 addr, U32 count, U32 *data, U32 *status);				// return <=0 : error,   >0 : good.
I32 GECP_memRead(void *pHand, U32 addr, U32 count, U32 *rdata, U32 *status);				// return <=0 : error,   >0 : good.

// I32 GECP_setCam(void *pHand, char *pIP /*, I32 nPort*/ );
//I32 GECP_setCamCB(void *pHand, CR_video_fnCB fnCB, I32 nPort);


//I32 GECP_setCam(void *pHand, char *pIP);
//I32 GECP_setCamGvcp(void *pHand, I32 nPort);
//I32 GECP_setCamGvsp(void *pHand, I32 nPort);
//I32 GECP_setCamGvspCB(void *pHand, CR_video_fnCB fnCB, I32 nPort);

//--
I32 GECP_CamIpstr(void *pHand, char *pIP);
I32 GECP_CamIp(void *pHand, U32 Ip);

I32 GECP_HostIpstr(void *pHand, char *pIP);
I32 GECP_HostIp(void *pHand, U32 Ip);

I32 GECP_CamGvcpPort(void *pHand, I32 nPort);
I32 GECP_CamGvspPort(void *pHand, I32 nPort);
//I32 GECP_CamGvspCB(void *pHand, CR_video_fnCB fnCB, I32 nPort);
I32 GECP_CamGvspCB(void *pHand, CR_video_fnCB fnCB);

I32 GECP_CamGvcpStart(void *pHand);
I32 GECP_CamGvcpStop(void *pHand);

I32 GECP_CamGvspStart(void *pHand);
I32 GECP_CamGvspStop(void *pHand);

I32 GECP_CamTimerMode(void *pHand, U32 mode);
I32 GECP_CamExposureMode(void *pHand, U32 mode);

//--

I32 GECP_ImageSize(void *pHand, I32 width, I32 height);
I32 GECP_ImagePosition(void *pHand, I32 sx, I32 sy);
I32 GECP_ImageFps(void *pHand, I32 fps);		
I32 GECP_ImageDuration(void *pHand, I32 usec);		
I32 GECP_ImageExposure(void *pHand, I32 usec);	// with LED On..
I32 GECP_ImageExposurePost(void *pHand, I32 usec);	// Set Post Exposure time.  20170325
I32 GECP_ImageExposureExpiate(void *pHand, I32 usec);	// Set Exposure Expiate time.  20170325
I32 GECP_ImageGain(void *pHand, I32 gain);		// gain value
I32 GECP_ImageGamma(void *pHand, I32 Gamma);		// Gamma: 100 == 1.0     2019/0131
I32 GECP_ImageContrast(void *pHand, I32 Contrast);		// Contrast: 100 == 1.0
I32 GECP_ImageBright(void *pHand, I32 Bright);		// Bright: 100 == 0     
I32 GECP_ImageLUT(void *pHand, I32 Bright, I32 Contrast, I32 Gamma);	



I32 GECP_ImageDelayoffset(void *pHand, I32 delayoffset);		// Exposure Delay offset: usec     2019/0223
I32 GECP_LED_Brightness(void *pHand, I32 brightness);		// brightness: 8bit. (0 ~ 255)  2019/0219

I32 GECP_ImageSkip(void *pHand, I32 skip);		// 0: Don't send, 1: Don't skip, 2: skip1, 3: skip2, ..., N: skip(N-1)
I32 GECP_ImageMultitude4Normal(void *pHand, I32 multitude4normal);

I32 GECP_ImageSizePos(void *pHand, I32 width, I32 height,I32 sx, I32 sy);		
I32 GECP_ImageDurationExposure(void *pHand, I32 durationUsec, I32 exposureUsec);		
I32 GECP_ImageFpsExposure(void *pHand, I32 fps, I32 exposureUsec);		


I32 GECP_ImageBulkRequest(void *pHand, U32 ts_h, U32 ts_l, U32 framecount, U32 waittime, U32 continueafterbulk,	U32 multitude4Bulk, U32 skip4bulk,
							U32 width4bulk, U32 height4bulk, U32 offset_x4bulk, U32 offset_y4bulk, U32 tsshot_h, U32 tsshot_l, I32 axx1024, I32 bxx1024, I32 ayx1024, I32 byx1024);
I32 GECP_ImageBulkRequest64(void *pHand, U64 ts, U32 framecount, U32 waittime, U32 continueafterbulk, U32 multitude4Bulk, U32 skip4bulk,
							U32 width4bulk, U32 height4bulk, U32 offset_x4bulk, U32 offset_y4bulk, U64 ts64shot, I32 axx1024, I32 bxx1024, I32 ayx1024, I32 byx1024);

I32 GECP_ImageSyncDiv(void *pHand, I32 syncdiv);


I32 GECP_CamFmcInit(void *pHand, I32 zrot, I32 autoinit);		// 2016/11/02

//--
I32 GECP_closeSendSocket(void *pHand);
I32 GECP_startCapture(void *pHand, I32 bStart);
I32 GECP_CamCaptureStart(void *pHand);
I32 GECP_CamCaptureStop(void *pHand);

I32 GECP_ImageUpdate(void *pHand, CAM_IMG_ATTR_T * pGvcpImgAttr);

I32 GECP_ImageUpdate_both(void *pHand, CAM_IMG_ATTR_T  pGvcpImgAttr[2]);

I32 GECP_heartbeat(void *pHand, U32 heartbeattimeout);			// msec.  0: Do not check heartbeat any more.

I32 GECP_SyncUpdate(void *pHand, U32 updateme);					 // 0: NO update, 1: yes.. update me

I32 GECP_SendOnOff(void *pHand, I32 bStart);				// 20190214
#if 0
// Mask Operation...        2018/0425, yhsuk
I32 GECP_MaskCalc(void *pHand, U32 calc);
I32 GECP_MaskSave(void *pHand);
I32 GECP_MaskSet(void *pHand, U32 startx, U32 starty, U32 endx, U32 endy);
#endif


//I32 GECP_startSendStart(void *pHand, U32 uIP, I32 nPort, I32 bStart);
I32 GECP_startSendStart(void *pHand, I32 bStart);
I32 GECP_setMsgCB(void *pHand, CR_msg_fnCB fnCB, I32 nPort, void *pParam);

//-------
int GECP_Ipstr2Ip(void *pHand, char *pIP);







//-- 20190409, yhsuk.
I32 GECP_hboardinfo_read(void *pHand, U32 *rdata, U32 len);
I32 GECP_hboardinfo_write(void *pHand, U32 *sdata, U32 len);
I32 GECP_pcinfo_read(void *pHand, U32 *rdata, U32 len);
I32 GECP_pcinfo_write(void *pHand, U32 *sdata, U32 len);
I32 GECP_runcode_read(void *pHand, U32 *rdata, U32 len);
I32 GECP_runcode_write(void *pHand, U32 *sdata, U32 len);
I32 GECP_cpusid_read(void *pHand, U32 *rdata, U32 len);
I32 GECP_cpusid_write(void *pHand, U32 *sdata, U32 len);
I32 GECP_cpubdserial_read(void *pHand, U32 *rdata, U32 count);
I32 GECP_bddatarsvd_read(void *pHand, U32 *rdata, U32 count);
I32 GECP_bddatarsvd_write(void *pHand, U32 *sdata, U32 count);


I32 GECP_version_get(void *pHand, char *pVersion);		// by ywchoi code..
	// sucess: 1,  fail: -1
I32 GECP_cache_update(void *pHand, U32 addr, U32 len);		// by ywchoi code..
I32 GECP_cache_commit(void *pHand, U32 addr, U32 len);		// by ywchoi code..
I32 GECP_flash_erase(void *pHand, U32 addr, U32 len);		// by ywchoi code..
I32 GECP_reboot(void *pHand);		// by ywchoi code..
I32 GECP_gold_recover(void *pHand);		// by ywchoi code..
I32 GECP_cache_checksum(void *pHand, U32 addr, U32 len, U32 checksumtype, U32 *pchecksum, U32 *pstatus);  // checksumtype 0: CRC32  


#if defined (__cplusplus)
}
#endif

#endif			// #if !defined(_CRGEV_H_)
