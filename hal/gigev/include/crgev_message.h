/*!
 *******************************************************************************
                                                                                
                    CREATZ GEV
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   crgev_message.h
	 @brief  GEV Message API
	 @author Original: by hgmoon, modified by yhsuk
	 @date   2015/11/19 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_CRGEV_MESSAGE_H_)
#define      _CRGEV_MESSAGE_H_	

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/
#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/

/*GEV_STATUS*/   I32 GevSendMessage(U08 *pDatagram, I32 size, SOCKET *pSocket, struct sockaddr_in *pDest, 
		BOOL showData, U08 *rBuff, I32 rSize);


I32 GevSendMessageOnly(U08 *pDatagram, I32 size, SOCKET *pSocket, struct sockaddr_in *pDest, BOOL showData);

GEV_STATUS GevBroadcastMessage(U08 *pDatagram, int size, SOCKET *pSocket, struct sockaddr_in *pDest, 
		BOOL showData);
GEV_STATUS GevReceiveMessage(U08 *pDatagram, int maxSize, SOCKET *pSocket, struct timeval *pTimeout, 
		U32 *pAddr, U16 *pPort, BOOL showData);
	
#if defined (__cplusplus)
}
#endif

#endif			// #if !defined(_CRGEV_MESSAGE_H_)

