/*!
 *******************************************************************************
                                                                                
                    CREATZ GEV
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   crgev_information.h
	 @brief  GEV
	 @author Original: by hgmoon, modified by yhsuk
	 @date   2015/11/19 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_CRGEV_INFORMATION_H_)
#define      _CRGEV_INFORMATION_H_	

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
// INI sections
#define	TYPE_SECTION							L"Type"
#define	IP_SECTION								L"IP"
#define	IMAGE_SECTION							L"Image"
#define	DISCOVERY_SECTION						L"Discovery"
#define	CONTROL_SECTION							L"Control"
#define	STREAMING_SECTION						L"Streaming"
#define	MESSAGING_SECTION						L"Messaging"

// INI keys
#define	DEVICE_KEY								L"device"
#define	DEVICE_CAMERA							1
#define	DEVICE_APPLICATION					2

#define	NETWORK_CARD_KEY						L"network_card"
#define	CAMERA_IP_KEY							L"camera_ip"
#define	CAMERA_MAC_HIGH_KEY					L"camera_mac_high"
#define	CAMERA_MAC_LOW_KEY					L"camera_mac_low"

#define	PERSISTENT_ENABLE_KEY				L"persistent_enable"
#define	PERSISTENT_IP_KEY						L"persistent_ip"
#define	PERSISTENT_MASK_KEY					L"persistent_mask"
#define	DHCP_ENABLE_KEY						L"dhcp_enable"

#define	CONTROL_CAMERA_PORT_KEY				L"control_camera_port"
#define	CONTROL_APPLICATION_PORT_KEY		L"control_application_port"
#define	DISCOVERY_TIMEOUT_KEY				L"discovery_timeout"
#define	HEARTBEAT_TIMEOUT_KEY				L"heartbeat_timeout"
#define	COMMAND_TIMEOUT_KEY					L"command_timeout"
#define	COMMAND_RETRY_KEY						L"command_retry"

#define	STREAMING_CAMERA_PORT_KEY			L"streaming_camera_port"
#define	STREAMING_APPLICATION_PORT_KEY	L"streaming_application_port"
#define	STREAMING_PACKET_SIZE_KEY			L"streaming_packet_size"
#define	STREAMING_PACKET_DELAY_KEY			L"streaming_packet_delay"

#define	MESSAGING_CAMERA_PORT_KEY			L"messaging_camera_port"
#define	MESSAGING_APPLICATION_PORT_KEY	L"messaging_application_port"

#define	IMAGE_WIDTH_KEY						L"image_width"
#define	IMAGE_HEIGHT_KEY						L"image_height"
#define	PIXEL_TYPE_KEY							L"pixel_type"
#define	IMAGE_DELAY_KEY						L"image_delay"
#define	IMAGE_COUNT_KEY						L"image_count"
#define	NB_SAVE_IMAGE_KEY						L"nb_save_image"

#define FPS_KEY							L"fps"
#define BRIGHTNESS_KEY					L"brightness"
#define CONTRAST_KEY					L"contrast"
#define SATURATION_KEY					L"saturation"
#define HUE_KEY							L"hue"
#define AUTO_WHITE_BALANCE_KEY			L"auto_white_balance"
#define GAIN_KEY						L"gain"
#define AUTO_GAIN_KEY					L"auto_gain"
#define SHARPNESS_KEY					L"sharpness"
#define HFLIP_KEY						L"hflip"
#define VFLIP_KEY						L"vflip"
#define EXPOSURE_KEY					L"exposure"
#define AUTO_EXPOSURE_KEY				L"auto_exposure"

#define AOI_OFFSETX_KEY					L"aoi_offsetx"
#define AOI_OFFSETY_KEY					L"aoi_offsety"
#define AOI_WIDTH_KEY					L"aoi_width"
#define AOI_HEIGHT_KEY					L"aoi_height"

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/
#if defined (__cplusplus)
extern "C" {
#endif

// Structures to hold INI information

typedef struct
{
	U32	device;								// 1 -> camera, 2 -> application

	// network card info
	U32	network_card;						// index of network card to use

	// Device Discovery
	U32	persistent_enable;				// indicate if persistentIP is enabled
	U32	persistent_ip;						// persistent IP address
	U32	persistent_mask;					// persistent IP mask
	U32	dhcp_enable;						// indicate if DHCP is enabled

	// GigE Vision Control Protocol
	U32	control_camera_port;				// camera control port (GVCP port)

	// GigE Vision Streaming Protocol
	U32	streaming_camera_port;			// camera streaming port (source)

	// GigE Vision Messaging
	U32	messaging_camera_port;			// camera messaging port (source)

	// Image info
	U32	image_count;						// number of different images to send to application

} CameraInfo;

typedef struct
{
	U32	device;								// 1 -> camera, 2 -> application

	// network card info
	U32	network_card;						// index of network card to use

	// IP information
	U32	camera_ip;							// IP address of camera
	U32	camera_mac_high;					// MAC address (most significant bits), upper 16-bit always 0x0000
	U32	camera_mac_low;					// MAC address (least significant bits)

	// GigE Vision Control Protocol
	U32	control_camera_port;				// camera control port (GVCP port, destination)
	U32	control_application_port;		// application control port (source)
	U32	discovery_timeout;				// number of ms to wait for device discovery
	U32	heartbeat_timeout;				// heartbeat watchdog timeout (in seconds)
	U32	command_timeout;					// timeout for ACK message (in ms)
	U32	command_retry;						// number of retries for ACK message

	// GigE Vision Streaming Protocol
	U32	streaming_camera_port;			// camera streaming port (source)
	U32	streaming_application_port;	// application streaming port (destination)
	U32	streaming_packet_size;			// size in bytes of streaming packets
	U32	streaming_packet_delay;			// delay in us between each streaming packet

	// GigE Vision Messaging
	U32	messaging_camera_port;			// camera messaging port (source)
	U32	messaging_application_port;	// application messaging port (destination)

	// Image info
	U32	image_width;						// width of image generated by simulator
	U32	image_height;						// height of image generated by simulator
	U32	pixel_type;							// type of pixel in the image
	U32	image_delay;						// delay in ms between images
	U32	image_count;						// number of image buffers
	U32	nb_save_image;						// indicate number ofimages to save to disk

	U32 fps;
	U32 brightness;
	U32 contrast;
	U32 saturation;
	U32 hue;
	U32 auto_white_balance;
	U32 gain;
	U32 auto_gain;
	U32 sharpness;
	U32 hflip;
	U32 vflip;
	U32 exposure;
	U32 auto_exposure;
	U32 grab;

	U32 aoi_offsetx;
	U32 aoi_offsety;
	U32 aoi_width;
	U32 aoi_height;

} ApplicationInfo;

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
}
#endif

#endif			// #if !defined(_CRGEV_INFORMATION_H_)

