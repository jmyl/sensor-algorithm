#if defined(_DEBUG)
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif
#include <windows.h>
#include "SerialPort.h"

CSerialPort::CSerialPort()
{
	m_hComm	 = INVALID_HANDLE_VALUE;  
	m_bPortReady = false;;  
	m_bWriteRC = false;  
	m_bReadRC = false;  
	m_iBytesWritten = 0;  
	m_iBytesRead = 0;  
	m_dwBytesRead = 0;  
	m_portnum = -1;		// 2013/10/23
}

CSerialPort::~CSerialPort()
{
}


bool CSerialPort::CheckPortExist(int portnum)
{
	HANDLE hc;
    bool bSuccess = false;
	WCHAR buf[128];

	//------------------
	wsprintf(buf, TEXT("//./COM%d"), portnum);

	hc = CreateFile(buf,  // L"//./" + portname,
			GENERIC_READ | GENERIC_WRITE,
			0,
			0,
			OPEN_EXISTING,
			0,
			0);
	
	if(hc == INVALID_HANDLE_VALUE) {
		DWORD dwError = GetLastError();

		//Check to see if the error was because some other app had the port open or a general failure
		if (dwError == ERROR_ACCESS_DENIED || dwError == ERROR_GEN_FAILURE || dwError == ERROR_SHARING_VIOLATION || dwError == ERROR_SEM_TIMEOUT) {
			bSuccess = true;    // NOT usable, but EXIST!!!
		} else {
			bSuccess = false;	
		}
	} else {
		bSuccess = true;
		CloseHandle(hc);
	}
	
	return bSuccess;
}


bool CSerialPort::CheckPortUsable(int portnum)
{
	HANDLE hc;
    bool bSuccess = false;
	WCHAR buf[128];

	//------------------
	wsprintf(buf, TEXT("//./COM%d"), portnum);

	hc = CreateFile(buf,  // L"//./" + portname,
			GENERIC_READ | GENERIC_WRITE,
			0,
			0,
			OPEN_EXISTING,
			0,
			0);
	
	if(hc == INVALID_HANDLE_VALUE) {
		bSuccess = false;
	} else {
		bSuccess = true;
		CloseHandle(hc);
	}
	
	return bSuccess;
}


bool CSerialPort::OpenPort(int portnum)
{
	WCHAR buf[128];

	if (m_hComm != INVALID_HANDLE_VALUE) {
		ClosePort();
	}
	
	wsprintf(buf, TEXT("//./COM%d"), portnum);
	m_hComm = CreateFile(buf,  // L"//./" + portname,
			GENERIC_READ | GENERIC_WRITE,
			0,
			0,
			OPEN_EXISTING,
			0,
			0);
		if(m_hComm == INVALID_HANDLE_VALUE)
		{
			return false;
		}
		else{
			m_portnum = portnum;
			return true;
		}
}


bool CSerialPort::ReOpenPort()
{
	WCHAR buf[128];

	if (m_hComm != INVALID_HANDLE_VALUE) {
		ClosePort();
	}
	
	wsprintf(buf, TEXT("//./COM%d"), m_portnum);
	m_hComm = CreateFile(buf,  // L"//./" + portname,
			GENERIC_READ | GENERIC_WRITE,
			0,
			0,
			OPEN_EXISTING,
			0,
			0);
		if(m_hComm == INVALID_HANDLE_VALUE)
		{
			return false;
		}
		else {
			ReConfigurePort();
			return true;
		}
}




bool CSerialPort::ConfigurePort(DWORD BaudRate, BYTE ByteSize, DWORD fParity, 
		BYTE Parity, BYTE StopBits)
{
	if(false == (m_bPortReady = !!GetCommState(m_hComm, &m_dcb)))
	{
//		MessageBox(L"GetCommState Error", L"Error", MB_OK + MB_ICONERROR);
		ClosePort();
		return false;
	}

	m_dcb.BaudRate			= BaudRate;
	m_dcb.ByteSize			= ByteSize;
	m_dcb.Parity			= Parity ;
	m_dcb.StopBits			= StopBits;
	m_dcb.fBinary			= true;
	m_dcb.fDsrSensitivity	= false;
	m_dcb.fParity			= fParity;
	m_dcb.fOutX				= false;
	m_dcb.fInX				= false;
	m_dcb.fNull				= false;
	m_dcb.fAbortOnError		= true;
	m_dcb.fOutxCtsFlow		= false;
	m_dcb.fOutxDsrFlow		= false;
	m_dcb.fDtrControl		= DTR_CONTROL_DISABLE;
	m_dcb.fDsrSensitivity	= false;
	m_dcb.fRtsControl		= RTS_CONTROL_DISABLE;
	m_dcb.fOutxCtsFlow		= false;
	m_dcb.fOutxCtsFlow		= false;

	m_bPortReady = !!SetCommState(m_hComm, &m_dcb);

	if(m_bPortReady == 0)
	{
	//	MessageBox(L"SetCommState Error", L"Error", MB_OK + MB_ICONERROR);
		//CloseHandle(m_hComm);
		ClosePort();
		return false;
	}

	return true;
}


bool CSerialPort::ReConfigurePort()
{
	if(m_hComm == INVALID_HANDLE_VALUE) {
		return false;
	}

	m_bPortReady = !!SetCommState(m_hComm, &m_dcb);

	if(m_bPortReady == 0)
	{
	//	MessageBox(L"SetCommState Error", L"Error", MB_OK + MB_ICONERROR);
		//CloseHandle(m_hComm);
		ClosePort();
		return false;
	}

	return true;
}







bool CSerialPort::SetCommunicationTimeouts(DWORD ReadIntervalTimeout,
		DWORD ReadTotalTimeoutMultiplier, DWORD ReadTotalTimeoutConstant,
		DWORD WriteTotalTimeoutMultiplier, DWORD WriteTotalTimeoutConstant)
{
	if(false == (m_bPortReady = !!GetCommTimeouts(m_hComm, &m_CommTimeouts)))
		return false;

	m_CommTimeouts.ReadIntervalTimeout			= ReadIntervalTimeout;
	m_CommTimeouts.ReadTotalTimeoutConstant		= ReadTotalTimeoutConstant;
	m_CommTimeouts.ReadTotalTimeoutMultiplier	= ReadTotalTimeoutMultiplier;
	m_CommTimeouts.WriteTotalTimeoutConstant	= WriteTotalTimeoutConstant;
	m_CommTimeouts.WriteTotalTimeoutMultiplier	= WriteTotalTimeoutMultiplier;
	
	m_bPortReady = !!SetCommTimeouts(m_hComm, &m_CommTimeouts);
		
	if(m_bPortReady == 0)
	{
	//	MessageBox(L"StCommTimeouts function failed",L"Com Port Error",MB_OK+MB_ICONERROR);
		//CloseHandle(m_hComm);
		ClosePort();
		return false;
	}

	return true;
}

bool CSerialPort::WriteByte(BYTE bybyte)
{
	m_iBytesWritten=0;
	if(WriteFile(m_hComm, &bybyte, 1, &m_iBytesWritten, NULL) == 0)
		return false;
	else
		return true;
}

bool CSerialPort::ReadByte(BYTE *presp/*BYTE &resp*/ )
{
	BYTE rx;
	*presp = 0;
	//resp=0;

	DWORD dwBytesTransferred=0;

	if(ReadFile(m_hComm, &rx, 1, &dwBytesTransferred, 0))
	{
		if(dwBytesTransferred == 1)
		{
			*presp = rx; //resp=rx;
			return true;
		}
	} else {
		ReOpenPort();
	}
			  
	return false;
}

DWORD CSerialPort::ReadByte(BYTE* presp /*BYTE* &resp*/, UINT size)
{
	DWORD dwBytesTransferred=0;
	int success;

	success = ReadFile(m_hComm, presp, size, &dwBytesTransferred, 0);
	if (!success)  {
		ReOpenPort();
	}
	return dwBytesTransferred;
}

DWORD CSerialPort::WriteByte(BYTE *buf, UINT size)
{
	m_iBytesWritten=0;
	WriteFile(m_hComm, buf, size, &m_iBytesWritten, NULL);
	return m_iBytesWritten;
}

void CSerialPort::ClosePort()
{
	CloseHandle(m_hComm);
	m_hComm = INVALID_HANDLE_VALUE;
	return;
}

