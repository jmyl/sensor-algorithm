#if !defined(SERIALPORT_H_)

#define      SERIALPORT_H_
class CSerialPort  
{  
	public:  
		static bool CheckPortExist(int port);
		static bool CheckPortUsable(int port);
		CSerialPort(void);  
		virtual ~CSerialPort(void);  

	private:  
		HANDLE  m_hComm;  
		DCB     m_dcb;  
		COMMTIMEOUTS m_CommTimeouts;  
		bool    m_bPortReady;  
		bool    m_bWriteRC;  
		bool    m_bReadRC;  
		DWORD   m_iBytesWritten;  
		DWORD   m_iBytesRead;  
		DWORD   m_dwBytesRead;  
		int	m_portnum;

	public:  
		void ClosePort();  
	//	bool ReadByte(BYTE &resp);  
		bool ReadByte(BYTE *presp);  
		//bool ReadByte(BYTE* &resp, UINT size);  
		DWORD ReadByte(BYTE* presp, UINT size);  
		bool WriteByte(BYTE bybyte);  
		DWORD WriteByte(BYTE *buf, UINT size);  
//		bool OpenPort(CString portname);  
		bool OpenPort(int portnum);
		bool ReOpenPort();
		bool SetCommunicationTimeouts(DWORD ReadIntervalTimeout,  
				DWORD ReadTotalTimeoutMultiplier, DWORD ReadTotalTimeoutConstant,  
				DWORD WriteTotalTimeoutMultiplier,DWORD WriteTotalTimeoutConstant);  
		bool ConfigurePort(DWORD BaudRate, BYTE ByteSize, DWORD fParity,   
				BYTE  Parity,BYTE StopBits);  
		bool ReConfigurePort();
};  

#endif