#if 1
/*!
 *******************************************************************************
                                                                                
                    Creatz XBRD
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana.h
	 @brief  
	 @author YongHo Suk
	 @date   2016/12/19 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if defined (__cplusplus)
extern "C" {
#endif


//=============================================================================
//	 xbrd.h
//=============================================================================

#ifndef	_XBRD_H_
#define	_XBRD_H_


typedef	enum
{
	XBRD_RET_NOT_AVAILABLE_RECEIVER = -7,
	XBRD_RET_TRANSMITTER_BUSY = -6,
	XBRD_RET_NOT_AVAILABLE_TRANSMITTER = -5,
	XBRD_RET_CONNECT_FAIL = -4,
	XBRD_RET_INVALID_SOCKET = -3,
	XBRD_RET_INVALID_PARAMETER = -2,
	XBRD_RET_INVALID_HANDLE = -1,
	XBRD_RET_OK	= 0,
	XBRD_RET_TRANSMIT_FAILED,			// 1
	XBRD_RET_RECEIVER_FAILED,			// 2
	XBRD_RET_RECEIVER_TIMEOUT,			// 3
	XBRD_RET_ILLEGAL_RX_HEADER,			// 4
	XBRD_RET_ILLEGAL_RX_CHECKSUM,		// 5
	XBRD_RET_INVALID_CONTROL,			// 6
	XBRD_RET_PARAMETER_OUTOFRANGE,		// 7
	XBRD_RET_MEMORY_INVALID_ADDRESS,	// 8
	XBRD_RET_MEMORY_WRITE_FAIL,			// 9
	XBRD_RET_FAILED_REQUEST,			//10
}	XBRD_RET;



typedef	enum
{
	XBRD_CONTROL = 0,
	XBRD_MEMORY_READ = 0x20,
	XBRD_MEMORY_WRITE,
	XBRD_MEMORY_PROTECT,
	XBRD_MEMORY_UNPROTECT,
	XBRD_MEMORY_ERAZE,
	XBRD_SET_JUMP,
	XBRD_SET_NET_INFO,
	XBRD_GET_VERSION_INFO,
	XBRD_RESET_SYSTEM,

	XBRD_REQUESTCPUID = 0x60,
	XBRD_MEMORY_NEO_ERAZE = 0x61,
	XBRD_MEMORY_NEO_READ = 0x62,
	XBRD_MEMORY_NEO_WRITE = 0x63

}	XBRD_COMMAND;


#define	XBRD_MAGIC_NUMBER				(0xfa)
#define	XBRD_TRANSFER_BUFFER_SIZE		(64)
#define	XBRD_TRANSFER_BIG_BUFFER_NETSIZE	(1024)
#define	XBRD_TRANSFER_BIG_BUFFER_SIZE		(XBRD_TRANSFER_BIG_BUFFER_NETSIZE + 8)
//#define	XBRD_TRANSFER_TIMEOUT			(200*1000)		// 200msec
#define	XBRD_TRANSFER_TIMEOUT			(60*1000)		// 200msec


typedef	struct		_XBRD_REQUEST_
{
	unsigned long	dwTimeKeepAlive;	// system reset delay in [mssec] : 0 ~ 0xfffffffe, 0xffffffff : infinite
	unsigned long	dwTimeHubReset;		// HUB reset delay in [mssec] : 0 ~ 0xfffffffe, 0xffffffff : infinite
	unsigned long	dwCamStatus1;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamStatus2;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamLightStatus;	// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwIndicatorStatus;	// 1 for ON, 2 for OFF, other for no change

//---   2017/4/10
	unsigned long	dwHubResetDuration;	// [msec]
	unsigned long	dwCamStatus3;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamIRLED1;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamIRLED2;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamIRLED3;		// 1 for ON, 2 for OFF, other for no change

}	XBRD_REQUEST,	*PXBRD_REQUEST;


typedef	struct		_XBRD_STATUS_
{
	unsigned long	dwResetType;		// 0 : for power on reset, 1 : watchdog reset
	unsigned long	dwTimeKeepAlive;	// system reset delay in [mssec] : 0 ~ 0xfffffffe, 0xffffffff : infinite
	unsigned long	dwTimeHubReset;		// HUB reset delay in [mssec] : 0 ~ 0xfffffffe, 0xffffffff : infinite
	unsigned long	dwCamStatus1;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamStatus2;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamLightStatus;	// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwIndicatorStatus;	// 1 for ON, 2 for OFF, other for no change

//---   2017/4/10
	unsigned long	dwHubResetDuration;	// [msec]
	unsigned long	dwCamStatus3;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamIRLED1;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamIRLED2;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamIRLED3;		// 1 for ON, 2 for OFF, other for no change

}	XBRD_STATUS,	*PXBRD_STATUS;


#define	XBRD_INVALID_HANDLE		((XBRD_HANDLE) ~0)


typedef	void*		XBRD_HANDLE;



XBRD_HANDLE
	XBRD_Create (
	unsigned char*				puchNetAddress,
	unsigned short				usPortNumber
	);


XBRD_RET
	XBRD_Delete (
	XBRD_HANDLE					hXBRD
	);


XBRD_RET
	XBRD_Connect (
	XBRD_HANDLE						hXBRD
	);


XBRD_RET
	XBRD_Disconnect (
	XBRD_HANDLE						hXBRD
	);


XBRD_RET
	XBRD_Control (
	XBRD_HANDLE				hXBRD,
	PXBRD_REQUEST			pRequest,
	PXBRD_STATUS			pStatus
	);


XBRD_RET
	XBRD_ReadMemory (
	XBRD_HANDLE				hXBRD,
	unsigned char*			pData,
	unsigned long			ulAddress,
	DWORD					dwLength
	);


XBRD_RET
	XBRD_WriteMemory (
	XBRD_HANDLE				hXBRD,
	unsigned char*			pData,
	unsigned char*			pMem,
	unsigned long			ulAddress,
	DWORD					dwLength
	);


XBRD_RET
	XBRD_ProtectMemory (
	XBRD_HANDLE				hXBRD,
	unsigned long			ulAddress
	);


XBRD_RET
	XBRD_UnprotectMemory (
	XBRD_HANDLE				hXBRD,
	unsigned long			ulAddress
	);


XBRD_RET
	XBRD_ErazeMemory (
	XBRD_HANDLE				hXBRD,
	unsigned long			ulAddress,
	DWORD					dwLength
	);


XBRD_RET
	XBRD_SetJump (
	XBRD_HANDLE				hXBRD,
	unsigned long			ulAddress
	);


XBRD_RET
	XBRD_SetNetInfo (
	XBRD_HANDLE				hXBRD,
	unsigned char*			pIpAddress,
	unsigned char*			pMacAddress,
	unsigned short			usPort
	);


XBRD_RET
	XBRD_GetVersionInfo (
	XBRD_HANDLE				hXBRD,
	unsigned char*			pVersion
	);




XBRD_RET
	XBRD_GetCPUID (						// 2019/0117
	XBRD_HANDLE				hXBRD,
	unsigned int*			pbuf
	);
	

#endif	//_XBRD_H_

//=============================================================================
//	 xbrd.h
//=============================================================================

#if defined (__cplusplus)
}
#endif



#endif
