/*!
 *******************************************************************************
                                                                                
                    Creatz XMBD
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana.h
	 @brief  
	 @author YongHo Suk
	 @date   2016/12/19 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/


#ifndef	_XMBD_H_
#define	_XMBD_H_

#include	<winsock2.h>



#ifdef __cplusplus
extern "C" {
#endif


#define	XMBD_PREDEFINED_TIMEOUT

#ifdef	XMBD_PREDEFINED_TIMEOUT
#define	XMBD_CONNECTION_TIMEOUT						(5000)
#define	XMBD_PROTOCOL_TIMEOUT						(2000)
#endif

#define	XMBD_BUFFER_SIZE							(1024 + 16)

#define	XMBD_INVALID_HANDLE							((XMBD_HANDLE) -1)

#define	XMBD_CPUID_LENGTH							(12)


typedef	void*										XMBD_HANDLE;

typedef	enum
{
	XMBD_ERROR_NO_ERROR								= 0,
	XMBD_ERROR_ILLEGAL_HANDLE						= 0x10000,
	XMBD_ERROR_SOCKET_FAILED,
	XMBD_ERROR_SOCKET_TIMEOUT,

	XMBD_ERROR_SEND_FAILED,
	XMBD_ERROR_RECEIVE_FAILED,
	XMBD_ERROR_UNEXPECTED_RESPONSE,
	XMBD_ERROR_ILLEGAL_PARAMETER_VALUE,
}	XMBD_ERROR;


typedef	enum
{
	XMBD_CONTROL = 0,
	XMBD_MEMORY_READ = 0x20,
	XMBD_MEMORY_WRITE,
	XMBD_MEMORY_PROTECT,
	XMBD_MEMORY_UNPROTECT,
	XMBD_MEMORY_ERAZE,
	XMBD_SET_JUMP,
	XMBD_SET_NET_INFO,
	XMBD_GET_VERSION_INFO,
	XMBD_RESET_SYSTEM,
	XMBD_GET_CPUID		= 0x60,
	XMBD_ERAZE_FLASH,
	XMBD_READ_FLASH,
	XMBD_WRITE_FLASH,
}	XMBD_COMMAND;


#define	XMBD_MAGIC_NUMBER				(0xfa)
#define	XMBD_TRANSFER_BUFFER_SIZE		(16 + 1024)
#define	XMBD_TRANSFER_TIMEOUT			(60*1000)		// 200msec


typedef	struct		_XMBD_REQUEST_
{
	unsigned long	dwTimeKeepAlive;	// system reset delay in [mssec] : 0 ~ 0xfffffffe, 0xffffffff : infinite
	unsigned long	dwTimeHubReset;		// HUB reset delay in [mssec] : 0 ~ 0xfffffffe, 0xffffffff : infinite
	unsigned long	dwCamStatus1;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamStatus2;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamLightStatus;	// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwIndicatorStatus;	// 1 for ON, 2 for OFF, other for no change

//---   2017/4/10
	unsigned long	dwHubResetDuration;	// [msec]
	unsigned long	dwCamStatus3;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamIRLED1;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamIRLED2;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamIRLED3;		// 1 for ON, 2 for OFF, other for no change

}	XMBD_REQUEST,	*PXMBD_REQUEST;


typedef	struct		_XMBD_STATUS_
{
	unsigned long	dwResetType;		// 0 : for power on reset, 1 : watchdog reset
	unsigned long	dwTimeKeepAlive;	// system reset delay in [mssec] : 0 ~ 0xfffffffe, 0xffffffff : infinite
	unsigned long	dwTimeHubReset;		// HUB reset delay in [mssec] : 0 ~ 0xfffffffe, 0xffffffff : infinite
	unsigned long	dwCamStatus1;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamStatus2;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamLightStatus;	// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwIndicatorStatus;	// 1 for ON, 2 for OFF, other for no change

//---   2017/4/10
	unsigned long	dwHubResetDuration;	// [msec]
	unsigned long	dwCamStatus3;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamIRLED1;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamIRLED2;		// 1 for ON, 2 for OFF, other for no change
	unsigned long	dwCamIRLED3;		// 1 for ON, 2 for OFF, other for no change

}	XMBD_STATUS,	*PXMBD_STATUS;



typedef	struct		_XMBD_CPUID_
{
	unsigned char	szId[XMBD_CPUID_LENGTH];
}	XMBD_CPUID,		*PXMBD_CPUID;




XMBD_HANDLE
	XMBD_Create (										// create and connect X-board's communication handle
	PCHAR							pszIpAddress,		// IP address in format "xxx.xxx.xxx.xxx"
	USHORT							usPort,				// IP Port Number
	DWORD							dwTimeout			// timeout for connecting in msec
	);


XMBD_ERROR
	XMBD_Delete (										// disconnect and delete X-board's communication handle
	XMBD_HANDLE						hClient				// handle created by XMBD_Create function
	);


XMBD_ERROR
	XMBD_ReadFlash (									// read X-board's flash memory
	XMBD_HANDLE						hClient,			// handle created by XMBD_Create function
	PBYTE							pData,				// data to be read by this function
	USHORT							usAddress,			// flash memory address to be read
	USHORT							usLength,			// flash memory size to be read
	DWORD							dwTimeout			// timeout to be waiting for server response in msec
	);


XMBD_ERROR
	XMBD_WriteFlash (									// write X-board's flash memory
	XMBD_HANDLE						hClient,			// handle created by XMBD_Create function
	PBYTE							pData,				// data to be written by this function
	USHORT							usAddress,			// flash memory address to be written
	USHORT							usLength,			// flash memory size to be written
	DWORD							dwTimeout			// timeout to be waiting for server response in msec
	);


XMBD_ERROR
	XMBD_ErazeFlash (									// earse X-board's flash memory
	XMBD_HANDLE						hClient,			// handle created by XMBD_Create function
	USHORT							usAddress,			// flash memory address to be erased
	USHORT							usLength,			// flash memory size to be erased
	DWORD							dwTimeout			// timeout to be waiting for server response in msec
	);


XMBD_ERROR
	XMBD_GetCpuid (										// retrieve X-board CPU's unique ID
	XMBD_HANDLE						hClient,			// handle created by XMBD_Create function
	PXMBD_CPUID						pId,				// CPU's unique ID to be read
	DWORD							dwTimeout			// timeout to be waiting for server response in msec
	);


XMBD_ERROR
	XMBD_Control (										// send request and receive status of X-board
	XMBD_HANDLE						hClient,			// handle created by XMBD_Create function
	PXMBD_REQUEST					pRequest,			// request data
	PXMBD_STATUS					pStatus,			// response status
	DWORD							dwTimeout			// timeout to be waiting for server response in msec
	);


XMBD_ERROR
	XMBD_GetLastTransact (								// retrieve last transaction data
	XMBD_HANDLE						hClient,			// handle created by XMBD_Create function
	PBYTE							pSent,				// sent data
	PBYTE							pReceived,			// received data
	PUSHORT							pSentBytes,			// sent data length
	PUSHORT							pReceivedBytes		// received data length
	);



#ifdef __cplusplus
}
#endif


#endif	//_XMBD_H_

