#include <stdio.h>
#include "xmbd.h"
#include "cr_common.h"
#include "cr_network.h"

typedef	struct						_XMBD_
{
	SOCKET							Socket;
	BYTE							SndBuf[XMBD_BUFFER_SIZE];
	BYTE							RcvBuf[XMBD_BUFFER_SIZE];
	USHORT							nToBeSend;
	USHORT							nSent;
	USHORT							nExpected;
	USHORT							nReceived;
	USHORT							nTransactionId;
}	XMBD,							*PXMBD;


XMBD_HANDLE
	XMBD_Create (
	PCHAR							pszIpAddress,
	USHORT							usPort,
	DWORD							dwTimeout
	)
{
	PXMBD							pXmbd;
	WSADATA							wsaData;
	int								iMode = 1;		// non-block mode
	struct sockaddr_in				sSvrAddr;
	fd_set							fdset;  
	struct timeval					tvout;
	int								iRet;

	if (NO_ERROR != WSAStartup (MAKEWORD(2, 2), &wsaData)) {
		return (XMBD_HANDLE) XMBD_INVALID_HANDLE;
	}

	pXmbd = malloc (sizeof (XMBD));
	if (0 != pXmbd) {
		memset (pXmbd, 0, sizeof (XMBD));
		pXmbd->Socket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (INVALID_SOCKET != pXmbd->Socket) {
			if (NO_ERROR == ioctlsocket (pXmbd->Socket, FIONBIO, (u_long *)&iMode)) {
				memset (&sSvrAddr, 0, sizeof (sSvrAddr));
				sSvrAddr.sin_family = AF_INET;
				sSvrAddr.sin_port = htons (usPort);
#pragma warning(disable:4996)
				sSvrAddr.sin_addr.s_addr = inet_addr (pszIpAddress);
#pragma warning(default:4996)
				tvout.tv_sec = dwTimeout / 1000;
				tvout.tv_usec = (dwTimeout % 1000) * 1000;

				if (0 == connect (pXmbd->Socket, (struct sockaddr *) &sSvrAddr, sizeof (sSvrAddr))) {
					return (XMBD_HANDLE) pXmbd;
				}

				iRet = cr_GetSocketError ();
				if ((WSAEWOULDBLOCK == iRet) || (WSAEINPROGRESS == iRet)) {
					FD_ZERO (&fdset);  
#pragma warning(disable:4127)
					FD_SET (pXmbd->Socket, &fdset);  
#pragma warning(default:4127)
					iRet = select (0, NULL, &fdset, NULL, &tvout);
					if (0 < iRet) {
						return (XMBD_HANDLE) pXmbd;
					}
				}

			}

			shutdown (pXmbd->Socket, SD_BOTH); 
			closesocket (pXmbd->Socket);
		}

		free (pXmbd);
	}

	WSACleanup ();

	return (XMBD_HANDLE) XMBD_INVALID_HANDLE;
}



XMBD_ERROR
	XMBD_Delete (
	XMBD_HANDLE						hClient
	)
{
	PXMBD							pXmbd = (PXMBD) hClient;

	if (XMBD_INVALID_HANDLE == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}

	if (NULL == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}


	shutdown (pXmbd->Socket, SD_BOTH); 
	closesocket (pXmbd->Socket);

	free (pXmbd);

//	WSACleanup ();

	return XMBD_ERROR_NO_ERROR;
}


XMBD_ERROR
	XMBD_IsReadyToTransmit (
	XMBD_HANDLE						hClient,
	DWORD							dwTimeout
	)
{
	PXMBD							pXmbd = (PXMBD) hClient;
	fd_set							fds;
	struct	timeval					tv;
	int								iRet;

	if (XMBD_INVALID_HANDLE == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}

	FD_ZERO (&fds);
#pragma warning(disable:4127)
	FD_SET (pXmbd->Socket, &fds);
#pragma warning(default:4127)

	tv.tv_sec = dwTimeout / 1000;
	tv.tv_usec = (dwTimeout % 1000) * 1000;

	iRet = select (0, NULL, &fds, NULL, &tv);
	if (0 > iRet) {							// socket error
		return XMBD_ERROR_SOCKET_FAILED;
	} else if (0 == iRet) {					// transmit buffer full
		return XMBD_ERROR_SOCKET_TIMEOUT;
	} else {
		return XMBD_ERROR_NO_ERROR;
	}
}


XMBD_ERROR
	XMBD_IsReceivedData (
	XMBD_HANDLE						hClient,
	DWORD							dwTimeout
	)
{
	PXMBD							pXmbd = (PXMBD) hClient;
	fd_set							fds;
	struct	timeval					tv;
	int								iRet;

	if (XMBD_INVALID_HANDLE == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}

	FD_ZERO (&fds);
#pragma warning(disable:4127)
	FD_SET (pXmbd->Socket, &fds);
#pragma warning(default:4127)

	tv.tv_sec = dwTimeout / 1000;
	tv.tv_usec = (dwTimeout % 1000) * 1000;

	iRet = select (0, &fds, NULL, NULL, &tv);
	if (0 > iRet) {
		return XMBD_ERROR_SOCKET_FAILED;
	}

	if (0 == iRet) {					// timeout
		return XMBD_ERROR_SOCKET_TIMEOUT;
	}

	return XMBD_ERROR_NO_ERROR;
}


XMBD_ERROR
	XMBD_Send (
	XMBD_HANDLE						hClient
	)
{
	PXMBD							pXmbd = (PXMBD) hClient;
	int								iRet;

	if (XMBD_INVALID_HANDLE == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}

	iRet = send (pXmbd->Socket, (const char *)pXmbd->SndBuf, pXmbd->nToBeSend, 0);
	if (SOCKET_ERROR == iRet) {
		return XMBD_ERROR_SOCKET_FAILED;
	}

	pXmbd->nSent = (USHORT)iRet;

	return XMBD_ERROR_NO_ERROR;
}


XMBD_ERROR
	XMBD_Receive (
	XMBD_HANDLE						hClient
	)
{
	PXMBD							pXmbd = (PXMBD) hClient;
	int								iRet;

	if (XMBD_INVALID_HANDLE == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}

	iRet = recv (pXmbd->Socket, (char *)pXmbd->RcvBuf, XMBD_BUFFER_SIZE, 0);
	if (SOCKET_ERROR == iRet) {
		pXmbd->nReceived = 0;
		return XMBD_ERROR_RECEIVE_FAILED;
	}

	pXmbd->nReceived = (USHORT)iRet;
	return XMBD_ERROR_NO_ERROR;
}


XMBD_ERROR
	XMBD_Transact (
	XMBD_HANDLE						hClient,
	DWORD							dwTimeout
	)
{
	PXMBD							pXmbd = (PXMBD) hClient;
	int								iRet;

	iRet = XMBD_IsReadyToTransmit (hClient, 2000);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	iRet = XMBD_Send (hClient);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	if (pXmbd->nToBeSend != pXmbd->nSent) {
		return XMBD_ERROR_SEND_FAILED;
	}

	iRet = XMBD_IsReceivedData (hClient, dwTimeout);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	return XMBD_Receive (hClient);
}


XMBD_ERROR
	XMBD_ReadFlash (
	XMBD_HANDLE						hClient,
	PBYTE							pData,
	USHORT							usAddress,
	USHORT							usLength,
	DWORD							dwTimeout
	)
{
	PXMBD							pXmbd = (PXMBD) hClient;
	int								iRet;
	XMBD_ERROR						error;

	if (XMBD_INVALID_HANDLE == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}

	if ((0 == pData) || (8 * 1024 < usAddress) || (0 == usLength) || (1024 < usLength)) {
		return XMBD_ERROR_ILLEGAL_PARAMETER_VALUE;
	}

	pXmbd->nExpected = 6 + 2 + usLength;
	pXmbd->nToBeSend = 6;

	pXmbd->SndBuf[0] = XMBD_MAGIC_NUMBER;
	pXmbd->SndBuf[1] = XMBD_READ_FLASH;
	pXmbd->SndBuf[2] = (BYTE) (usAddress >> 0);
	pXmbd->SndBuf[3] = (BYTE) (usAddress >> 8);
	pXmbd->SndBuf[4] = (BYTE) (usLength >> 0);
	pXmbd->SndBuf[5] = (BYTE) (usLength >> 8);

	iRet = XMBD_Transact (hClient, dwTimeout);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	if (0 != memcmp (pXmbd->SndBuf, pXmbd->RcvBuf, 6)) {
		return XMBD_ERROR_UNEXPECTED_RESPONSE;
	}

	error = (XMBD_ERROR) (((USHORT) pXmbd->RcvBuf[6] << 0) | ((USHORT) pXmbd->RcvBuf[7] << 8));
	if (XMBD_ERROR_NO_ERROR == error) {
		memcpy (pData, &pXmbd->RcvBuf[8], usLength);
	}

	return error;
}


XMBD_ERROR
	XMBD_WriteFlash (
	XMBD_HANDLE						hClient,
	PBYTE							pData,
	USHORT							usAddress,
	USHORT							usLength,
	DWORD							dwTimeout
	)
{
	PXMBD							pXmbd = (PXMBD) hClient;
	int								iRet;
	XMBD_ERROR						error;

	if (XMBD_INVALID_HANDLE == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}

	if ((0 == pData) || (8 * 1024 < usAddress) || (0 == usLength) || (1024 < usLength)) {
		return XMBD_ERROR_ILLEGAL_PARAMETER_VALUE;
	}

	pXmbd->nExpected = 6 + 2;
	pXmbd->nToBeSend = 6 + usLength;

	pXmbd->SndBuf[0] = XMBD_MAGIC_NUMBER;
	pXmbd->SndBuf[1] = XMBD_WRITE_FLASH;
	pXmbd->SndBuf[2] = (BYTE) (usAddress >> 0);
	pXmbd->SndBuf[3] = (BYTE) (usAddress >> 8);
	pXmbd->SndBuf[4] = (BYTE) (usLength >> 0);
	pXmbd->SndBuf[5] = (BYTE) (usLength >> 8);
	memcpy (&pXmbd->SndBuf[6], pData, usLength);

	iRet = XMBD_Transact (hClient, dwTimeout);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	if (0 != memcmp (pXmbd->SndBuf, pXmbd->RcvBuf, 6)) {
		return XMBD_ERROR_UNEXPECTED_RESPONSE;
	}

	error = (XMBD_ERROR) (((USHORT) pXmbd->RcvBuf[6] << 0) | ((USHORT) pXmbd->RcvBuf[7] << 8));

	return error;
}


XMBD_ERROR
	XMBD_ErazeFlash (
	XMBD_HANDLE						hClient,
	USHORT							usAddress,
	USHORT							usLength,
	DWORD							dwTimeout
	)
{
	PXMBD							pXmbd = (PXMBD) hClient;
	int								iRet;
	XMBD_ERROR						error;

	if (XMBD_INVALID_HANDLE == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}

	if ((8 * 1024 < usAddress) || (0 == usLength) || (1024 < usLength)) {
		return XMBD_ERROR_ILLEGAL_PARAMETER_VALUE;
	}

	pXmbd->nExpected = 6 + 2;
	pXmbd->nToBeSend = 6;

	pXmbd->SndBuf[0] = XMBD_MAGIC_NUMBER;
	pXmbd->SndBuf[1] = XMBD_ERAZE_FLASH;
	pXmbd->SndBuf[2] = (BYTE) (usAddress >> 0);
	pXmbd->SndBuf[3] = (BYTE) (usAddress >> 8);
	pXmbd->SndBuf[4] = (BYTE) (usLength >> 0);
	pXmbd->SndBuf[5] = (BYTE) (usLength >> 8);

	iRet = XMBD_Transact (hClient, dwTimeout);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	if (0 != memcmp (pXmbd->SndBuf, pXmbd->RcvBuf, 6)) {
		return XMBD_ERROR_UNEXPECTED_RESPONSE;
	}

	error = (XMBD_ERROR) (((USHORT) pXmbd->RcvBuf[6] << 0) | ((USHORT) pXmbd->RcvBuf[7] << 8));

	return error;
}


XMBD_ERROR
	XMBD_GetCpuid (
	XMBD_HANDLE						hClient,
	PXMBD_CPUID						pId,
	DWORD							dwTimeout
	)
{
	PXMBD							pXmbd = (PXMBD) hClient;
	int								iRet;
//	XMBD_ERROR						error;

	if (XMBD_INVALID_HANDLE == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}

	if (0 == pId) {
		return XMBD_ERROR_ILLEGAL_PARAMETER_VALUE;
	}

	pXmbd->nToBeSend = 2;
	pXmbd->nExpected = XMBD_CPUID_LENGTH + pXmbd->nToBeSend;

	pXmbd->SndBuf[0] = XMBD_MAGIC_NUMBER;
	pXmbd->SndBuf[1] = XMBD_GET_CPUID;

	iRet = XMBD_Transact (hClient, dwTimeout);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	if (0 != memcmp (pXmbd->SndBuf, pXmbd->RcvBuf, 2)) {
		return XMBD_ERROR_UNEXPECTED_RESPONSE;
	}

	memcpy (pId, &pXmbd->RcvBuf[2], XMBD_CPUID_LENGTH);

	return XMBD_ERROR_NO_ERROR;
}


XMBD_ERROR
	XMBD_Control (
	XMBD_HANDLE							hClient,
	PXMBD_REQUEST						pRequest,
	PXMBD_STATUS						pStatus,
	DWORD								dwTimeout
	)
{
	PXMBD								pXmbd = (PXMBD) hClient;
	int									iRet;
//	XMBD_ERROR							error;

	if (XMBD_INVALID_HANDLE == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}

	if ((0 == pRequest) || (0 == pStatus)) {
		return XMBD_ERROR_ILLEGAL_PARAMETER_VALUE;
	}

	pXmbd->nToBeSend = 2 + sizeof (XMBD_REQUEST);
	pXmbd->nExpected = 2 + sizeof (XMBD_STATUS);

	pXmbd->SndBuf[0] = XMBD_MAGIC_NUMBER;
	pXmbd->SndBuf[1] = XMBD_CONTROL;

	memcpy (&pXmbd->SndBuf[2], pRequest, sizeof (XMBD_REQUEST));

	iRet = XMBD_Transact (hClient, dwTimeout);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	if (0 != memcmp (pXmbd->SndBuf, pXmbd->RcvBuf, 2)) {
		return XMBD_ERROR_UNEXPECTED_RESPONSE;
	}

	memcpy (pStatus, &pXmbd->RcvBuf[2], sizeof (XMBD_STATUS));

	return XMBD_ERROR_NO_ERROR;
}


XMBD_ERROR
	XMBD_GetLastTransact (
	XMBD_HANDLE					hClient,
	PBYTE							pSent,
	PBYTE							pReceived,
	PUSHORT							pSentBytes,
	PUSHORT							pReceivedBytes
	)
{
	PXMBD							pXmbd = (PXMBD) hClient;

	if (XMBD_INVALID_HANDLE == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}

	memcpy (pSent, pXmbd->SndBuf, pXmbd->nSent);
	memcpy (pReceived, pXmbd->RcvBuf, pXmbd->nReceived);
	*pSentBytes = pXmbd->nSent;
	*pReceivedBytes = pXmbd->nReceived;

	return XMBD_ERROR_NO_ERROR;
}


#ifdef XMBD_TEST

#define	XMBD_TEST_IP_ADDRESS			("172.16.1.50")
#define	XMBD_TEST_IP_PORT				(502)
#define	XMBD_TEST_CONNECTION_TIMEOUT	(2000)


XMBD_HANDLE						ghClient;
BYTE							TestRxData[8 * 1024];
BYTE							TestTxData[8 * 1024];
XMBD_CPUID						TestCpuId;
XMBD_REQUEST					TestRequest;
XMBD_STATUS						TestStatus;

void
	xmbdflash_test (
	XMBD_HANDLE hClient
	)
{
	XMBD_ERROR						error;
	USHORT							usAddress;
	USHORT							usLength;

	usAddress = 0;
	usLength = 1024;

	error = XMBD_GetCpuid (hClient, &TestCpuId, 2000);
	printf ("XMBD_GetCpuid :  => ");
	{
		int *ptmp;
		ptmp = (int *)&TestCpuId;

		printf("%08x %08x %08x\n", *ptmp, *(ptmp+1), *(ptmp+2));
	}
	if (XMBD_ERROR_NO_ERROR == error) {
		printf ("success\n");
	} else {
		printf ("failed with [%d]\n", error);
	}

	error = XMBD_ErazeFlash (hClient, usAddress, usLength, 2000);
	printf ("XMBD_ErazeFlash : address[%04X], length[%d] => ", usAddress, usLength);
	if (XMBD_ERROR_NO_ERROR == error) {
		printf ("success\n");
	} else {
		printf ("failed with [%d]\n", error);
	}

	memset (TestTxData, 0xff, 8 * 1024);	// a5

	error = XMBD_WriteFlash (hClient, TestTxData, usAddress, usLength, 2000);
	printf ("XMBD_WriteFlash : address[%04X], length[%d] => ", usAddress, usLength);
	if (XMBD_ERROR_NO_ERROR == error) {
		printf ("success\n");
	} else {
		printf ("failed with [%d]\n", error);
	}

	error = XMBD_ReadFlash (hClient, TestRxData, usAddress, usLength, 2000);
	printf ("XMBD_ReadFlash : address[%04X], length[%d] => ", usAddress, usLength);
	if (XMBD_ERROR_NO_ERROR == error) {
		printf ("success\n");
	} else {
		printf ("failed with [%d]\n", error);
	}

	memset (TestTxData, 0x00, 8 * 1024);	// 00

	error = XMBD_WriteFlash (hClient, TestTxData, usAddress, usLength, 2000);
	printf ("XMBD_WriteFlash : address[%04X], length[%d] => ", usAddress, usLength);
	if (XMBD_ERROR_NO_ERROR == error) {
		printf ("success\n");
	} else {
		printf ("failed with [%d]\n", error);
	}

	error = XMBD_ReadFlash (hClient, TestRxData, usAddress, usLength, 2000);
	printf ("XMBD_ReadFlash : address[%04X], length[%d] => ", usAddress, usLength);
	if (XMBD_ERROR_NO_ERROR == error) {
		printf ("success\n");
	} else {
		printf ("failed with [%d]\n", error);
	}

}


void
	xmbd_control_test (
	XMBD_HANDLE hClient
	)
{
	XMBD_ERROR						error;

	TestRequest.dwTimeKeepAlive = (unsigned long)-1;
	TestRequest.dwTimeHubReset = (unsigned long)-1;

	TestRequest.dwTimeKeepAlive = (unsigned long)5000;
	TestRequest.dwTimeHubReset = (unsigned long)50000;

	TestRequest.dwHubResetDuration = 500;
	
	TestRequest.dwCamIRLED1 = 1;
	TestRequest.dwCamIRLED2 = 1;
	TestRequest.dwCamIRLED3 = 1;
	TestRequest.dwCamLightStatus = 1;
	TestRequest.dwCamStatus1 = 1;
	TestRequest.dwCamStatus3 = 1;
	TestRequest.dwCamStatus2 = 1;
	TestRequest.dwIndicatorStatus = 1;

//	XMBD_ERROR
	error = XMBD_Control (hClient, &TestRequest, &TestStatus, 2000);
	if (XMBD_ERROR_NO_ERROR == error) {
		printf ("success     [%d]\n", error);
	} else {
		printf ("failed with [%d]\n", error);
	}
}



void
	xmbd_test (
	void
	)
{
	XMBD_ERROR						error;

	int i;
	XMBD_HANDLE						hClient;

	i;
	hClient = XMBD_Create (XMBD_TEST_IP_ADDRESS, XMBD_TEST_IP_PORT, XMBD_TEST_CONNECTION_TIMEOUT);
	if (XMBD_INVALID_HANDLE == hClient) {
		printf ("XMBD_Create : failed\n");
		exit (0);
	} else {
		printf ("XMBD_Create : success\n");
	}

	//xmbdflash_test (hClient);
	for (i = 0; i < 100; i++) {
		printf ("xmbd_control TEST.. %d\n", i);
		//XMBD_Delete (hClient);
		//hClient = XMBD_Create (XMBD_TEST_IP_ADDRESS, XMBD_TEST_IP_PORT, XMBD_TEST_CONNECTION_TIMEOUT);
		if (XMBD_INVALID_HANDLE == hClient) {
			printf("INVALID_HANDLE\n");
			continue;	
		}
		xmbd_control_test(hClient);

		cr_sleep(100);
//		cr_sleep(1000);
	}
	error = XMBD_Delete (hClient);
	if (XMBD_ERROR_NO_ERROR == error) {
		printf ("XMBD_Delete : success\n");
	} else {
		printf ("XMBD_Delete : failed [%d]\n", error);
	}
}



void
	xmbd_test2(
	XMBD_HANDLE	hClient
	)
{

	int i;
	XMBD_HANDLE						hmyClient;
	XMBD_ERROR						error;

	i;
	if (hClient == NULL) {
		hmyClient = hClient = XMBD_Create (XMBD_TEST_IP_ADDRESS, XMBD_TEST_IP_PORT, XMBD_TEST_CONNECTION_TIMEOUT);
	} else {
		hmyClient = NULL;
	}

	if (XMBD_INVALID_HANDLE == hClient) {
		printf ("XMBD_Create : failed\n");
		exit (0);
	} else {
		printf ("XMBD_Create : success\n");
	}

	xmbdflash_test (hClient);
	for (i = 0; i < 100; i++) {
		printf ("xmbd_control TEST.. %d\n", i);
//		XMBD_Delete (hClient);
		//hClient = XMBD_Create (XMBD_TEST_IP_ADDRESS, XMBD_TEST_IP_PORT, XMBD_TEST_CONNECTION_TIMEOUT);
		//if (XMBD_INVALID_HANDLE == hClient) {
		//	printf("INVALID_HANDLE\n");
		//	continue;	
		//}
		xmbd_control_test(hClient);

		cr_sleep(100);
//		cr_sleep(1000);
	}

	if (hmyClient != NULL) {
		error = XMBD_Delete (hClient);
		if (XMBD_ERROR_NO_ERROR == error) {
			printf ("XMBD_Delete : success\n");
		} else {
			printf ("XMBD_Delete : failed [%d]\n", error);
		}
	}
}


#endif

/************** Not used  *******************/
#if 0

XMBD_ERROR
	XMBD_ReadCoils (
	XMBD_HANDLE					hClient,
	PBYTE							pCoils,
	BYTE							ubUnitId,
	USHORT							usAddress,
	USHORT							usNumCoils,
	DWORD							dwTimeout
	)
{
	PXMBD							pXmbd = (PXMBD) hClient;
	int								iRet;

	if (XMBD_INVALID_HANDLE == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}

	if ((0 == pCoils) || (1 > usNumCoils) || (2000 < usNumCoils)) {
		return XMBD_ERROR_ILLEGAL_PARAMETER_VALUE;
	}

	pXmbd->nExpected = 9 + ((7 + usNumCoils) / 8);
	pXmbd->nToBeSend = 12;

	XMBD_BuildHeader (pXmbd->SndBuf, pXmbd->nTransactionId++, XMBD_FCODE_READCOILS, ubUnitId, pXmbd->nToBeSend - 6);
	pXmbd->SndBuf[8] = (BYTE) (usAddress >> 8);
	pXmbd->SndBuf[9] = (BYTE) (usAddress >> 0);
	pXmbd->SndBuf[10] = (BYTE) (usNumCoils >> 8);
	pXmbd->SndBuf[11] = (BYTE) (usNumCoils >> 0);

	iRet = XMBD_Transact (hClient, dwTimeout);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	iRet = XMBD_CheckHeader (pXmbd);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	if (pXmbd->RcvBuf[7] == (0x80 | pXmbd->SndBuf[7])) {
		if (9 == pXmbd->nReceived) {
			return (XMBD_ERROR) pXmbd->RcvBuf[8];
		}
	} else if (pXmbd->RcvBuf[7] == pXmbd->SndBuf[7]) {
		if (pXmbd->RcvBuf[8] == ((7 + usNumCoils) / 8)) {
			memcpy (pCoils, &pXmbd->RcvBuf[9], pXmbd->RcvBuf[8]);
			return XMBD_ERROR_NO_ERROR;
		}
	} else {
	}

	return XMBD_ERROR_UNEXPECTED_RESPONSE;
}


XMBD_ERROR
	XMBD_ReadDiscreteInputs (
	XMBD_HANDLE					hClient,
	PBYTE							pDiscretes,
	BYTE							ubUnitId,
	USHORT							usAddress,
	USHORT							usNumDiscretes,
	DWORD							dwTimeout
	)
{
	PXMBD							pXmbd = (PXMBD) hClient;
	int								iRet;

	if (XMBD_INVALID_HANDLE == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}

	if ((0 == pDiscretes) || (1 > usNumDiscretes) || (2000 < usNumDiscretes)) {
		return XMBD_ERROR_ILLEGAL_PARAMETER_VALUE;
	}

	pXmbd->nExpected = 9 + ((7 + usNumDiscretes) / 8);
	pXmbd->nToBeSend = 12;

	XMBD_BuildHeader (pXmbd->SndBuf, pXmbd->nTransactionId++, XMBD_FCODE_READDISCRETEINPUTS, ubUnitId, pXmbd->nToBeSend - 6);
	pXmbd->SndBuf[8] = (BYTE) (usAddress >> 8);
	pXmbd->SndBuf[9] = (BYTE) (usAddress >> 0);
	pXmbd->SndBuf[10] = (BYTE) (usNumDiscretes >> 8);
	pXmbd->SndBuf[11] = (BYTE) (usNumDiscretes >> 0);

	iRet = XMBD_Transact (hClient, dwTimeout);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	iRet = XMBD_CheckHeader (pXmbd);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	if (pXmbd->RcvBuf[7] == (0x80 | pXmbd->SndBuf[7])) {
		if (9 == pXmbd->nReceived) {
			return (XMBD_ERROR) pXmbd->RcvBuf[8];
		}
	} else if (pXmbd->RcvBuf[7] == pXmbd->SndBuf[7]) {
		if (pXmbd->RcvBuf[8] == ((7 + usNumDiscretes) / 8)) {
			memcpy (pDiscretes, &pXmbd->RcvBuf[9], pXmbd->RcvBuf[8]);
			return XMBD_ERROR_NO_ERROR;
		}
	} else {
	}

	return XMBD_ERROR_UNEXPECTED_RESPONSE;
}


void
	XMBD_RegisterCopy (
	PUSHORT							pRegister,
	PBYTE							pData,
	USHORT							nRegisterNum
	)
{
	USHORT							i;

	for (i=0; i<nRegisterNum; i++) {
		pRegister[i] = ((USHORT) pData[0 + (2 * i)] << 8) | ((USHORT) pData[1 + (2 * i)] << 0);
	}
}


XMBD_ERROR
	XMBD_ReadHoldingRegisters (
	XMBD_HANDLE					hClient,
	PUSHORT							pRegisters,
	BYTE							ubUnitId,
	USHORT							usAddress,
	USHORT							usNumRegisters,
	DWORD							dwTimeout
	)
{
	PXMBD							pXmbd = (PXMBD) hClient;
	int								iRet;

	if (XMBD_INVALID_HANDLE == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}

	if ((0 == pRegisters) || (1 > usNumRegisters) || (125 < usNumRegisters)) {
		return XMBD_ERROR_ILLEGAL_PARAMETER_VALUE;
	}

	pXmbd->nExpected = 9 + (2 * usNumRegisters);
	pXmbd->nToBeSend = 12;

	XMBD_BuildHeader (pXmbd->SndBuf, pXmbd->nTransactionId++, XMBD_FCODE_READHOLDINGREGISTERS, ubUnitId, pXmbd->nToBeSend - 6);
	pXmbd->SndBuf[8] = (BYTE) (usAddress >> 8);
	pXmbd->SndBuf[9] = (BYTE) (usAddress >> 0);
	pXmbd->SndBuf[10] = (BYTE) (usNumRegisters >> 8);
	pXmbd->SndBuf[11] = (BYTE) (usNumRegisters >> 0);

	iRet = XMBD_Transact (hClient, dwTimeout);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	iRet = XMBD_CheckHeader (pXmbd);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	if (pXmbd->RcvBuf[7] == (0x80 | pXmbd->SndBuf[7])) {
		if (9 == pXmbd->nReceived) {
			return (XMBD_ERROR) pXmbd->RcvBuf[8];
		}
	} else if (pXmbd->RcvBuf[7] == pXmbd->SndBuf[7]) {
		if (pXmbd->RcvBuf[8] == (2 * usNumRegisters)) {
			XMBD_RegisterCopy (pRegisters, &pXmbd->RcvBuf[9], usNumRegisters);
			return XMBD_ERROR_NO_ERROR;
		}
	} else {
	}

	return XMBD_ERROR_UNEXPECTED_RESPONSE;
}


XMBD_ERROR
	XMBD_ReadInputRegisters (
	XMBD_HANDLE					hClient,
	PUSHORT							pRegisters,
	BYTE							ubUnitId,
	USHORT							usAddress,
	USHORT							usNumRegisters,
	DWORD							dwTimeout
	)
{
	PXMBD							pXmbd = (PXMBD) hClient;
	int								iRet;

	if (XMBD_INVALID_HANDLE == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}

	if ((0 == pRegisters) || (1 > usNumRegisters) || (125 < usNumRegisters)) {
		return XMBD_ERROR_ILLEGAL_PARAMETER_VALUE;
	}

	pXmbd->nExpected = 9 + (2 * usNumRegisters);
	pXmbd->nToBeSend = 12;

	XMBD_BuildHeader (pXmbd->SndBuf, pXmbd->nTransactionId++, XMBD_FCODE_READINPUTREGISTERS, ubUnitId, pXmbd->nToBeSend - 6);
	pXmbd->SndBuf[8] = (BYTE) (usAddress >> 8);
	pXmbd->SndBuf[9] = (BYTE) (usAddress >> 0);
	pXmbd->SndBuf[10] = (BYTE) (usNumRegisters >> 8);
	pXmbd->SndBuf[11] = (BYTE) (usNumRegisters >> 0);

	iRet = XMBD_Transact (hClient, dwTimeout);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	iRet = XMBD_CheckHeader (pXmbd);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	if (pXmbd->RcvBuf[7] == (0x80 | pXmbd->SndBuf[7])) {
		if (9 == pXmbd->nReceived) {
			return (XMBD_ERROR) pXmbd->RcvBuf[8];
		}
	} else if (pXmbd->RcvBuf[7] == pXmbd->SndBuf[7]) {
		if (pXmbd->RcvBuf[8] == (2 * usNumRegisters)) {
			XMBD_RegisterCopy (pRegisters, &pXmbd->RcvBuf[9], usNumRegisters);
			return XMBD_ERROR_NO_ERROR;
		}
	} else {
	}

	return XMBD_ERROR_UNEXPECTED_RESPONSE;
}


XMBD_ERROR
	XMBD_WriteSingleCoil (
	XMBD_HANDLE					hClient,
	USHORT							usCoilValue,
	BYTE							ubUnitId,
	USHORT							usAddress,
	DWORD							dwTimeout
	)
{
	PXMBD							pXmbd = (PXMBD) hClient;
	int								iRet;

	if (0 == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}

	pXmbd->nExpected = 12;
	pXmbd->nToBeSend = 12;

	XMBD_BuildHeader (pXmbd->SndBuf, pXmbd->nTransactionId++, XMBD_FCODE_WRITESINGLECOIL, ubUnitId, pXmbd->nToBeSend - 6);
	pXmbd->SndBuf[8] = (BYTE) (usAddress >> 8);
	pXmbd->SndBuf[9] = (BYTE) (usAddress >> 0);
	pXmbd->SndBuf[10] = (0 == usCoilValue) ? 0x00 : 0xff;
	pXmbd->SndBuf[11] = (BYTE) 0x00;

	iRet = XMBD_Transact (hClient, dwTimeout);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	if (12 == pXmbd->nReceived) {
		if (0 == memcmp (pXmbd->SndBuf, pXmbd->RcvBuf, 12)) {
			return XMBD_ERROR_NO_ERROR;
		}
	} else if (9 == pXmbd->nReceived) {
		if ((XMBD_ERROR_NO_ERROR == XMBD_CheckHeader (pXmbd)) && (9 == pXmbd->nReceived)) {
			return (XMBD_ERROR) pXmbd->RcvBuf[8];
		}
	} else {
	}

	return XMBD_ERROR_UNEXPECTED_RESPONSE;
}


XMBD_ERROR
	XMBD_WriteSingleRegister (
	XMBD_HANDLE					hClient,
	USHORT							usRegisterValue,
	BYTE							ubUnitId,
	USHORT							usAddress,
	DWORD							dwTimeout
	)
{
	PXMBD							pXmbd = (PXMBD) hClient;
	int								iRet;

	if (0 == hClient) {
		return XMBD_ERROR_ILLEGAL_HANDLE;
	}

	pXmbd->nExpected = 12;
	pXmbd->nToBeSend = 12;

	XMBD_BuildHeader (pXmbd->SndBuf, pXmbd->nTransactionId++, XMBD_FCODE_WRITESINGLEREGISTER, ubUnitId, pXmbd->nToBeSend - 6);
	pXmbd->SndBuf[8] = (BYTE) (usAddress >> 8);
	pXmbd->SndBuf[9] = (BYTE) (usAddress >> 0);
	pXmbd->SndBuf[10] = (BYTE) (usRegisterValue >> 8);
	pXmbd->SndBuf[11] = (BYTE) (usRegisterValue >> 0);

	iRet = XMBD_Transact (hClient, dwTimeout);
	if (XMBD_ERROR_NO_ERROR != iRet) {
		return iRet;
	}

	if (12 == pXmbd->nReceived) {
		if (0 == memcmp (pXmbd->SndBuf, pXmbd->RcvBuf, 12)) {
			return XMBD_ERROR_NO_ERROR;
		}
	} else if (9 == pXmbd->nReceived) {
		if ((XMBD_ERROR_NO_ERROR == XMBD_CheckHeader (pXmbd)) && (9 == pXmbd->nReceived)) {
			return (XMBD_ERROR) pXmbd->RcvBuf[8];
		}
	} else {
	}

	return XMBD_ERROR_UNEXPECTED_RESPONSE;
}


#endif

