/*!
 *******************************************************************************
                                                                                
                    CREATZ XB
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   xbrd.cpp
	 @brief  XBoard 
	 @author Original: by ywchoi
	 @date   2016/12/19 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#pragma warning(disable:4456)

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/

//=============================================================================
//	 xbrd.c
//=============================================================================

#include <winsock2.h>

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"
#include "cr_network.h"


#include "xbrd.h"


typedef	struct						_XBRD_
{
	SOCKET							socket;
	unsigned long					address;
	unsigned short					port;
	//unsigned char					szSndBuf[XBRD_TRANSFER_BUFFER_SIZE];
	//unsigned char					szRcvBuf[XBRD_TRANSFER_BUFFER_SIZE];
	unsigned char					szSndBuf[XBRD_TRANSFER_BIG_BUFFER_SIZE];
	unsigned char					szRcvBuf[XBRD_TRANSFER_BIG_BUFFER_SIZE];
}	XBRD,							*PXBRD;



XBRD_RET XBRD_TransferPacket0 (
	SOCKET					socket,
	unsigned char*			pSndPkt,
	unsigned char*			pRcvPkt,
	unsigned long			nSndPktLen,
	unsigned long			nRcvPktLen,
	unsigned long			checkChecksum
	);





//--------------------------------------------------
XBRD_RET
	XBRD_Connect (
	XBRD_HANDLE						hXBRD
	)
{
	PXBRD							pXBRD = (PXBRD) hXBRD;
	struct sockaddr_in				stAddressInfo;
	ULONG NonBlk = 1;
	int Ret;
	DWORD Err;

	XBRD_RET xret;
	

	//-------
	if (XBRD_INVALID_HANDLE == hXBRD) {
		xret = XBRD_RET_INVALID_HANDLE;
		goto func_exit;
	}
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #1\n");
	//-- Open Socket.
	pXBRD->socket = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #2\n");

	if (INVALID_SOCKET == pXBRD->socket) {
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" socket open error!\n");
		xret = XBRD_RET_INVALID_SOCKET;
		goto func_exit;
	}

	// Set to Non-blocking mode
	ioctlsocket(pXBRD->socket, FIONBIO, &NonBlk);

	stAddressInfo.sin_addr.S_un.S_addr = pXBRD->address;
	stAddressInfo.sin_family = AF_INET;
	stAddressInfo.sin_port = htons (pXBRD->port);

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #3\n");

	// This call will return immediately, coz our socket is non-blocking
	Ret = connect (pXBRD->socket, (struct sockaddr *) &stAddressInfo, sizeof (struct sockaddr_in));

	// If connected, it will return 0, or error
	if (Ret == SOCKET_ERROR) {
		Err = cr_GetSocketError();
		// Check if the error was WSAEWOULDBLOCK, where we'll wait.
		if (Err == WSAEWOULDBLOCK) {
			fd_set       Write, Err;
			TIMEVAL      Timeout;

			//--
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" Connect() returned WSAEWOULDBLOCK. Need to Wait..");
			FD_ZERO(&Write);
			FD_ZERO(&Err);
#pragma warning(disable:4127)
			FD_SET(pXBRD->socket, &Write);
			FD_SET(pXBRD->socket, &Err);
#pragma warning(default:4127)

#define TIMEOUTSEC	0
//#define TIMEOUTUSEC	(100*1000)
#define TIMEOUTUSEC	(20*1000)
			Timeout.tv_sec  = TIMEOUTSEC;
			Timeout.tv_usec = TIMEOUTUSEC;

			Ret = select (0,                // ignored
					NULL,           // read,
					&Write,        // Write Check
					&Err,            // Error check
					&Timeout);

			if (Ret == 0) { 
				shutdown(pXBRD->socket, SD_BOTH);
				closesocket (pXBRD->socket);
				pXBRD->socket = INVALID_SOCKET;

				xret = XBRD_RET_CONNECT_FAIL;
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__ ": Connect Timeout..\n");
				goto func_exit;
			} else {
				if (FD_ISSET(pXBRD->socket, &Write)) {
					xret = XBRD_RET_OK;
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__ ": Connected..\n");
					goto func_exit;
				} else {
					//if (FD_ISSET(pXBRD->socket, &Err)) {
					//}
					shutdown(pXBRD->socket, SD_BOTH);
					closesocket (pXBRD->socket);

					pXBRD->socket = INVALID_SOCKET;
					xret = XBRD_RET_CONNECT_FAIL;
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__ ": Fail.\n");
					goto func_exit;
				}
			}
		} else {
			xret = XBRD_RET_CONNECT_FAIL;
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__ ": Connection with 0x%08x\n", WSAGetLastError());
			goto func_exit;
		}
	} else {
		xret = XBRD_RET_OK;
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__ ": Connection with NO timeout!\n");
		goto func_exit;
	}
	
func_exit:
	return xret;
}


XBRD_RET
	XBRD_Disconnect (
	XBRD_HANDLE						hXBRD
	)
{
	PXBRD							pXBRD = (PXBRD) hXBRD;

	if (XBRD_INVALID_HANDLE == hXBRD) {
		return XBRD_RET_INVALID_HANDLE;
	}

	if (INVALID_SOCKET == pXBRD->socket) {
		return XBRD_RET_INVALID_SOCKET;
	}

	closesocket (pXBRD->socket);
	pXBRD->socket = INVALID_SOCKET;

	return XBRD_RET_OK;
}


XBRD_HANDLE
	XBRD_Create (
	unsigned char*					puchNetAddress,
	unsigned short					usPortNumber
	)
{
    WSADATA							wsaData;
	PXBRD							pXBRD;

	if (0 == puchNetAddress) {
		return (XBRD_HANDLE) XBRD_INVALID_HANDLE;
	}

	if (NO_ERROR != WSAStartup (MAKEWORD(2, 2), &wsaData)) {
		return (XBRD_HANDLE) XBRD_INVALID_HANDLE;
	}

	pXBRD = (PXBRD)malloc (sizeof (XBRD));
	if (0 == pXBRD) {
		return (XBRD_HANDLE) XBRD_INVALID_HANDLE;
	}
#pragma warning(disable:4996)
	pXBRD->address = inet_addr ((const char *)puchNetAddress);
#pragma warning(disable:4996)

	pXBRD->port = usPortNumber;

	return (XBRD_HANDLE) pXBRD;
}


XBRD_RET
	XBRD_Delete (
	XBRD_HANDLE					hXBRD
	)
{
	if (XBRD_INVALID_HANDLE == hXBRD) {
		return XBRD_RET_INVALID_HANDLE;
	}

	XBRD_Disconnect (hXBRD);
	free ((void *) hXBRD);
	WSACleanup ();

	return XBRD_RET_OK;
}


static XBRD_RET
	XBRD_Transmit (
	SOCKET					socket,
	PBYTE					pData,
	DWORD					dwLength
	)
{
	fd_set					fds;
	struct	timeval			tv;
	int						iret;

	if (INVALID_SOCKET == socket) {
		return XBRD_RET_INVALID_SOCKET;
	}

	if ((0 == pData) || (0 == dwLength)) {
		return XBRD_RET_INVALID_PARAMETER;
	}
#if 1
	FD_ZERO (&fds);
#pragma warning(disable:4127)
	FD_SET (socket, &fds);
#pragma warning(default:4127)
	tv.tv_sec = 0;
	tv.tv_usec = 20000;
	iret = select (32, NULL, &fds, NULL, &tv);
	if (0 > iret) {
		return XBRD_RET_NOT_AVAILABLE_TRANSMITTER;
	}

	if (0 == iret) {
		return XBRD_RET_TRANSMITTER_BUSY;
	}
#endif
	if (dwLength != (DWORD)send (socket, (char *) pData, dwLength, 0)) {
		return XBRD_RET_TRANSMIT_FAILED;
	}

	return XBRD_RET_OK;
}


static XBRD_RET
	XBRD_Receive (
	SOCKET					socket,
	PBYTE					pData,
	DWORD					dwLength
	)
{
	fd_set					fds;
	struct	timeval			tv;
	int						iret;
	int						rcvlength;
	int i;

	if (INVALID_SOCKET == socket) {
		return XBRD_RET_INVALID_SOCKET;
	}

	if ((0 == pData) || (0 == dwLength)) {
		return XBRD_RET_INVALID_PARAMETER;
	}
#if 1
	FD_ZERO (&fds);
#pragma warning(disable:4127)
	FD_SET (socket, &fds);
#pragma warning(default:4127)

	tv.tv_sec = 0;
	tv.tv_usec = XBRD_TRANSFER_TIMEOUT;
	iret = select (32, &fds, NULL, NULL, &tv);
	if (0 > iret) {
		return XBRD_RET_NOT_AVAILABLE_RECEIVER;
	}

	if (0 == iret) {
		return XBRD_RET_RECEIVER_TIMEOUT;
	}
#endif
	memset (pData, 0xff, dwLength);
//	if (dwLength != recv (socket, pData, dwLength, 0)) 
	rcvlength = 0;
#define TRYCOUNT 10
	for (i = 0; i < TRYCOUNT; i++) { 
		rcvlength = recv (socket, (char *)pData, dwLength, 0);
		if (dwLength == (DWORD)rcvlength) { 
			break;
		}
		cr_sleep(100);
	}
	if (dwLength != (DWORD)rcvlength) 
	{
		return XBRD_RET_RECEIVER_FAILED;
	}

	return XBRD_RET_OK;
}


static unsigned char
	XBRD_CalcChecksum (
	unsigned char*			pData,
	unsigned long			dwLength
	)
{
	unsigned long			i;
	unsigned char			bChecksum = 0;

	for (i=0; i<dwLength; i++) {
//		bChecksum += (unsigned char) *pData++;
		bChecksum = bChecksum + (unsigned char) *pData++;
	}

	return bChecksum;
}


XBRD_RET
	XBRD_TransferPacket (
	SOCKET					socket,
	unsigned char*			pSndPkt,
	unsigned char*			pRcvPkt,
	unsigned long			nSndPktLen,
	unsigned long			nRcvPktLen
	)
{

	return XBRD_TransferPacket0 (
			socket,
			pSndPkt,
			pRcvPkt,
			nSndPktLen,
			nRcvPktLen,
			0
			);
}

XBRD_RET
	XBRD_TransferPacket0 (
	SOCKET					socket,
	unsigned char*			pSndPkt,
	unsigned char*			pRcvPkt,
	unsigned long			nSndPktLen,
	unsigned long			nRcvPktLen,
	unsigned long			checkChecksum
	)
{
	XBRD_RET				iret;
	BYTE					bChecksum = 0;

	iret = XBRD_Transmit (socket, pSndPkt, nSndPktLen);
	if (XBRD_RET_OK != iret) {
		return iret;
	}

	iret = XBRD_Receive (socket, pRcvPkt, nRcvPktLen);
	if (XBRD_RET_OK != iret) {
		return iret;
	}

	if (checkChecksum) {
		bChecksum = XBRD_CalcChecksum(pRcvPkt, XBRD_TRANSFER_BUFFER_SIZE-1);

		if (pRcvPkt[XBRD_TRANSFER_BUFFER_SIZE-1] != bChecksum) {
			return XBRD_RET_ILLEGAL_RX_CHECKSUM;
		}
	}

	return XBRD_RET_OK;
}


XBRD_RET
	XBRD_Control (
	XBRD_HANDLE				hXBRD,
	PXBRD_REQUEST			pRequest,
	PXBRD_STATUS			pStatus
	)
{
	PXBRD					pXBRD = (PXBRD) hXBRD;
	XBRD_RET				iret;

	if (XBRD_INVALID_HANDLE == hXBRD) {
		return XBRD_RET_INVALID_HANDLE;
	}

	if ((0 == pStatus) || (0 == pRequest)) {
		return XBRD_RET_INVALID_PARAMETER;
	}

	pXBRD->szSndBuf[0] = XBRD_MAGIC_NUMBER;
	pXBRD->szSndBuf[1] = XBRD_CONTROL;

	//{
	//	static int xxx = 0;
	//	pXBRD->szSndBuf[1] = pXBRD->szSndBuf[1] + (unsigned char) (xxx & 0x01);
	//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #0   xxx: %d\n", xxx & 0x1);
	//	xxx++;


	//}
//	memcpy (&pXBRD->szSndBuf[2], pRequest, sizeof (XBRD));
	memcpy (&pXBRD->szSndBuf[2], pRequest, sizeof (XBRD_REQUEST));
	pXBRD->szSndBuf[XBRD_TRANSFER_BUFFER_SIZE-1] = XBRD_CalcChecksum (pXBRD->szSndBuf, XBRD_TRANSFER_BUFFER_SIZE-1);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #1\n");
	iret = XBRD_TransferPacket (pXBRD->socket, pXBRD->szSndBuf, pXBRD->szRcvBuf, XBRD_TRANSFER_BUFFER_SIZE, XBRD_TRANSFER_BUFFER_SIZE);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #2, %d\n", iret);
	
	if (XBRD_RET_OK == iret) {
		if (
//			(XBRD_MAGIC_NUMBER == pXBRD->szRcvBuf[XBRD_TRANSFER_BUFFER_SIZE-1])	
			(XBRD_MAGIC_NUMBER == pXBRD->szRcvBuf[0])	
			&& 
			(pXBRD->szSndBuf[1] == pXBRD->szRcvBuf[1])) {
			if (pXBRD->szRcvBuf[XBRD_TRANSFER_BUFFER_SIZE-1] == XBRD_CalcChecksum (pXBRD->szRcvBuf, XBRD_TRANSFER_BUFFER_SIZE-1)) {
				memcpy (pStatus, &pXBRD->szRcvBuf[2], sizeof (XBRD_STATUS));
				return XBRD_RET_OK;
			}
			return XBRD_RET_ILLEGAL_RX_CHECKSUM;
		}
		return XBRD_RET_ILLEGAL_RX_HEADER;
	}
	
	return iret;
}


XBRD_RET
	XBRD_ReadMemory (
	XBRD_HANDLE				hXBRD,
	unsigned char*			pData,
	unsigned long			ulAddress,
	DWORD					dwLength
	)
{
	PXBRD					pXBRD = (PXBRD) hXBRD;
	XBRD_RET				iret;

	if (XBRD_INVALID_HANDLE == hXBRD) {
		return XBRD_RET_INVALID_HANDLE;
	}

	if (0 == pData) {
		return XBRD_RET_INVALID_PARAMETER;
	}

	if (XBRD_TRANSFER_BIG_BUFFER_NETSIZE < dwLength) {
		return XBRD_RET_PARAMETER_OUTOFRANGE;
	}


	pXBRD->szSndBuf[0] = XBRD_MAGIC_NUMBER;
	pXBRD->szSndBuf[1] = (BYTE) XBRD_MEMORY_NEO_READ;			// 0x62

	pXBRD->szSndBuf[2] = (BYTE) ((ulAddress 	>> 0) & 0xFF);			// L
	pXBRD->szSndBuf[3] = (BYTE) ((ulAddress 	>> 8) & 0xFF);			// H

	pXBRD->szSndBuf[4] = (BYTE) ((dwLength 		>> 0) & 0xFF);			// L
	pXBRD->szSndBuf[5] = (BYTE) ((dwLength 		>> 8) & 0xFF);			// H

	memset(&pXBRD->szRcvBuf[0], 0x00, XBRD_TRANSFER_BIG_BUFFER_SIZE);
	iret = XBRD_TransferPacket (pXBRD->socket, pXBRD->szSndBuf, pXBRD->szRcvBuf, 6, 8 + dwLength);

	if (XBRD_RET_OK == iret) {
		int n;
		int myerrorcode;
		n=memcmp (&pXBRD->szSndBuf[0], &pXBRD->szRcvBuf[0], 6);
		if (n == 0) {
			myerrorcode = (pXBRD->szRcvBuf[6] & 0xFF) | (((int)(pXBRD->szRcvBuf[7]) << 8) & 0xFF00);
			if (myerrorcode == 0) {
				memcpy (pData, &pXBRD->szRcvBuf[8], dwLength);
				return XBRD_RET_OK;
			} else {
				XBRD_RET_FAILED_REQUEST;
			}
		}
		return XBRD_RET_ILLEGAL_RX_HEADER;
	}
	
	return iret;
}


XBRD_RET
	XBRD_WriteMemory (
	XBRD_HANDLE				hXBRD,
	unsigned char*			pData,
	unsigned char*			pMem,			// wright-back memory. NULL: discard it.
	unsigned long			ulAddress,
	DWORD					dwLength
	)
{
	PXBRD					pXBRD = (PXBRD) hXBRD;
	XBRD_RET				iret;

	if (XBRD_INVALID_HANDLE == hXBRD) {
		return XBRD_RET_INVALID_HANDLE;
	}

	if (0 == pData) {
		return XBRD_RET_INVALID_PARAMETER;
	}

	if (XBRD_TRANSFER_BIG_BUFFER_NETSIZE < dwLength) {
		return XBRD_RET_PARAMETER_OUTOFRANGE;
	}

	pXBRD->szSndBuf[0] = XBRD_MAGIC_NUMBER;
	pXBRD->szSndBuf[1] = (BYTE) XBRD_MEMORY_NEO_WRITE;					// 0x63

	pXBRD->szSndBuf[2] = (BYTE) ((ulAddress 	>> 0) & 0xFF);			// L
	pXBRD->szSndBuf[3] = (BYTE) ((ulAddress 	>> 8) & 0xFF);			// H

	pXBRD->szSndBuf[4] = (BYTE) ((dwLength 		>> 0) & 0xFF);			// L
	pXBRD->szSndBuf[5] = (BYTE) ((dwLength 		>> 8) & 0xFF);			// H

	memcpy (&pXBRD->szSndBuf[6], pData, dwLength);

	iret = XBRD_TransferPacket (pXBRD->socket, pXBRD->szSndBuf, pXBRD->szRcvBuf, 6 + dwLength, 8 + dwLength);


	if (XBRD_RET_OK == iret) {
		int n;
		int myerrorcode;
		n=memcmp (&pXBRD->szSndBuf[0], &pXBRD->szRcvBuf[0], 6);
		if (n == 0) {
			myerrorcode = (pXBRD->szRcvBuf[6] & 0xFF) | (((int)(pXBRD->szRcvBuf[7]) << 8) & 0xFF00);
			if (myerrorcode == 0) {
				if (pMem) {
					memcpy (pMem, &pXBRD->szRcvBuf[8], dwLength);
				}
				return XBRD_RET_OK;
			} else {
				XBRD_RET_FAILED_REQUEST;
			}
		}
		return XBRD_RET_ILLEGAL_RX_HEADER;
	}
	return iret;
}



XBRD_RET
	XBRD_ErazeMemory (
	XBRD_HANDLE				hXBRD,
	unsigned long			ulAddress,
	DWORD					dwLength
	)
{
	PXBRD					pXBRD = (PXBRD) hXBRD;
	XBRD_RET				iret;

	if (XBRD_INVALID_HANDLE == hXBRD) {
		return XBRD_RET_INVALID_HANDLE;
	}


	if (dwLength > XBRD_TRANSFER_BIG_BUFFER_NETSIZE) {
		return XBRD_RET_INVALID_PARAMETER;
	}


	pXBRD->szSndBuf[0] = XBRD_MAGIC_NUMBER;
	pXBRD->szSndBuf[1] = (BYTE) XBRD_MEMORY_NEO_ERAZE;			// 0x61

	pXBRD->szSndBuf[2] = (BYTE) ((ulAddress 	>> 0) & 0xFF);			// L
	pXBRD->szSndBuf[3] = (BYTE) ((ulAddress 	>> 8) & 0xFF);			// H

	pXBRD->szSndBuf[4] = (BYTE) ((dwLength 		>> 0) & 0xFF);			// L
	pXBRD->szSndBuf[5] = (BYTE) ((dwLength 		>> 8) & 0xFF);			// H

	//iret = XBRD_TransferPacket (pXBRD->socket, pXBRD->szSndBuf, pXBRD->szRcvBuf, XBRD_TRANSFER_BUFFER_SIZE, 8);
	iret = XBRD_TransferPacket (pXBRD->socket, pXBRD->szSndBuf, pXBRD->szRcvBuf, 6, 8);

	if (XBRD_RET_OK == iret) {
		int n;
		int myerrorcode;
		n=memcmp (&pXBRD->szSndBuf[0], &pXBRD->szRcvBuf[0], 6);
		if (n == 0) {
			myerrorcode = (pXBRD->szRcvBuf[6] & 0xFF) | (((int)(pXBRD->szRcvBuf[7]) << 8) & 0xFF00);
			if (myerrorcode == 0) {
				return XBRD_RET_OK;
			} else {
				return XBRD_RET_MEMORY_WRITE_FAIL; 
			}
		}
		return XBRD_RET_ILLEGAL_RX_HEADER;
	}
	return iret;
}


XBRD_RET
	XBRD_SetNetInfo (
	XBRD_HANDLE				hXBRD,
	unsigned char*			pIpAddress,
	unsigned char*			pMacAddress,
	unsigned short			usPort
	)
{
	PXBRD					pXBRD = (PXBRD) hXBRD;
	XBRD_RET				iret;

	if (XBRD_INVALID_HANDLE == hXBRD) {
		return XBRD_RET_INVALID_HANDLE;
	}

	pXBRD->szSndBuf[0] = XBRD_MAGIC_NUMBER;
	pXBRD->szSndBuf[1] = (BYTE) XBRD_SET_NET_INFO;
	memcpy (&pXBRD->szSndBuf[2], pIpAddress, 4);
	memcpy (&pXBRD->szSndBuf[6], pMacAddress, 6);
	pXBRD->szSndBuf[12] = (unsigned char) (usPort >> 8);
	pXBRD->szSndBuf[13] = (unsigned char) (usPort >> 0);

	iret = XBRD_TransferPacket (pXBRD->socket, pXBRD->szSndBuf, pXBRD->szRcvBuf, XBRD_TRANSFER_BUFFER_SIZE, XBRD_TRANSFER_BUFFER_SIZE);

	if (XBRD_RET_OK == iret) {
		if (XBRD_MAGIC_NUMBER == pXBRD->szRcvBuf[XBRD_TRANSFER_BUFFER_SIZE-1]) {
			if (pXBRD->szRcvBuf[XBRD_TRANSFER_BUFFER_SIZE-1] == XBRD_CalcChecksum (pXBRD->szSndBuf, XBRD_TRANSFER_BUFFER_SIZE-1)) {
				if (0 == pXBRD->szRcvBuf[1]) {
					return XBRD_RET_OK;
				}
				return XBRD_RET_FAILED_REQUEST;
			}
			return XBRD_RET_ILLEGAL_RX_CHECKSUM;
		}
		return XBRD_RET_ILLEGAL_RX_HEADER;
	}
	
	return iret;
}


XBRD_RET
	XBRD_GetVersionInfo (
	XBRD_HANDLE				hXBRD,
	unsigned char*			pVersion
	)
{
	PXBRD					pXBRD = (PXBRD) hXBRD;
	XBRD_RET				iret;
	unsigned char*			pBuf;

	if (XBRD_INVALID_HANDLE == hXBRD) {
		return XBRD_RET_INVALID_HANDLE;
	}

	pBuf = ((PXBRD) hXBRD)->szSndBuf;
	memset (pBuf, 0, XBRD_TRANSFER_BUFFER_SIZE);
	pXBRD->szSndBuf[0] = XBRD_MAGIC_NUMBER;
	pXBRD->szSndBuf[1] = (BYTE) XBRD_GET_VERSION_INFO;

	iret = XBRD_TransferPacket (pXBRD->socket, pXBRD->szSndBuf, pXBRD->szRcvBuf, XBRD_TRANSFER_BUFFER_SIZE, XBRD_TRANSFER_BUFFER_SIZE);

	if (XBRD_RET_OK == iret) {
		if (XBRD_MAGIC_NUMBER == pXBRD->szRcvBuf[XBRD_TRANSFER_BUFFER_SIZE-1]) {
			if (pXBRD->szRcvBuf[XBRD_TRANSFER_BUFFER_SIZE-1] == XBRD_CalcChecksum (pXBRD->szSndBuf, XBRD_TRANSFER_BUFFER_SIZE-1)) {
				memcpy (pVersion, &pXBRD->szRcvBuf[1], XBRD_TRANSFER_BUFFER_SIZE-2);
				return XBRD_RET_OK;
			}
			return XBRD_RET_ILLEGAL_RX_CHECKSUM;
		}
		return XBRD_RET_ILLEGAL_RX_HEADER;
	}
	
	return iret;
}



// 20190117.. 
XBRD_RET
	XBRD_GetCPUID (
	XBRD_HANDLE				hXBRD,
	unsigned int*			pbuf
	)
{
	PXBRD					pXBRD = (PXBRD) hXBRD;
	XBRD_RET				iret;

	if (XBRD_INVALID_HANDLE == hXBRD) {
		return XBRD_RET_INVALID_HANDLE;
	}

	pXBRD->szSndBuf[0] = XBRD_MAGIC_NUMBER;
	pXBRD->szSndBuf[1] = (BYTE) XBRD_REQUESTCPUID;

	iret = XBRD_TransferPacket (pXBRD->socket, pXBRD->szSndBuf, pXBRD->szRcvBuf, XBRD_TRANSFER_BUFFER_SIZE, 14);

	if (XBRD_RET_OK == iret) {
		if (XBRD_MAGIC_NUMBER == pXBRD->szRcvBuf[0]) {
			if (XBRD_REQUESTCPUID == pXBRD->szRcvBuf[1]) {
				memcpy(pbuf, &pXBRD->szRcvBuf[2], sizeof(BYTE) * 12);
				return XBRD_RET_OK;
			}
		}
		return XBRD_RET_ILLEGAL_RX_HEADER;
	}
	
	return iret;
}


//=============================================================================
//	 xbrd.c
//=============================================================================

#if defined (__cplusplus)
}
#endif

#pragma warning(default:4456)

/************** Not used  *******************/

#if 0
XBRD_RET
	XBRD_ProtectMemory (
	XBRD_HANDLE				hXBRD,
	unsigned long			ulAddress
	)
{
	PXBRD					pXBRD = (PXBRD) hXBRD;
	XBRD_RET				iret;

	if (XBRD_INVALID_HANDLE == hXBRD) {
		return XBRD_RET_INVALID_HANDLE;
	}

	pXBRD->szSndBuf[0] = XBRD_MAGIC_NUMBER;
	pXBRD->szSndBuf[1] = (BYTE) XBRD_MEMORY_PROTECT;
	pXBRD->szSndBuf[2] = (BYTE) (ulAddress >> 24);
	pXBRD->szSndBuf[3] = (BYTE) (ulAddress >> 16);
	pXBRD->szSndBuf[4] = (BYTE) (ulAddress >>  8);
	pXBRD->szSndBuf[5] = (BYTE) (ulAddress >>  0);

	iret = XBRD_TransferPacket (pXBRD->socket, pXBRD->szSndBuf, pXBRD->szRcvBuf, XBRD_TRANSFER_BUFFER_SIZE, XBRD_TRANSFER_BUFFER_SIZE);

	if (XBRD_RET_OK == iret) {
		if (XBRD_MAGIC_NUMBER == pXBRD->szRcvBuf[XBRD_TRANSFER_BUFFER_SIZE-1]) {
			if (pXBRD->szRcvBuf[XBRD_TRANSFER_BUFFER_SIZE-1] == XBRD_CalcChecksum (pXBRD->szSndBuf, XBRD_TRANSFER_BUFFER_SIZE-1)) {
				if (0 == pXBRD->szRcvBuf[1]) {
					return XBRD_RET_OK;
				}
				return XBRD_RET_FAILED_REQUEST;
			}
			return XBRD_RET_ILLEGAL_RX_CHECKSUM;
		}
		return XBRD_RET_ILLEGAL_RX_HEADER;
	}

	return iret;
}


XBRD_RET
	XBRD_UnprotectMemory (
	XBRD_HANDLE				hXBRD,
	unsigned long			ulAddress
	)
{
	PXBRD					pXBRD = (PXBRD) hXBRD;
	XBRD_RET				iret;

	if (XBRD_INVALID_HANDLE == hXBRD) {
		return XBRD_RET_INVALID_HANDLE;
	}

	pXBRD->szSndBuf[0] = XBRD_MAGIC_NUMBER;
	pXBRD->szSndBuf[1] = (BYTE) XBRD_MEMORY_UNPROTECT;
	pXBRD->szSndBuf[2] = (BYTE) (ulAddress >> 24);
	pXBRD->szSndBuf[3] = (BYTE) (ulAddress >> 16);
	pXBRD->szSndBuf[4] = (BYTE) (ulAddress >>  8);
	pXBRD->szSndBuf[5] = (BYTE) (ulAddress >>  0);

	iret = XBRD_TransferPacket (pXBRD->socket, pXBRD->szSndBuf, pXBRD->szRcvBuf, XBRD_TRANSFER_BUFFER_SIZE, XBRD_TRANSFER_BUFFER_SIZE);

	if (XBRD_RET_OK == iret) {
		if (XBRD_MAGIC_NUMBER == pXBRD->szRcvBuf[XBRD_TRANSFER_BUFFER_SIZE-1]) {
			if (pXBRD->szRcvBuf[XBRD_TRANSFER_BUFFER_SIZE-1] == XBRD_CalcChecksum (pXBRD->szSndBuf, XBRD_TRANSFER_BUFFER_SIZE-1)) {
				if (0 == pXBRD->szRcvBuf[1]) {
					return XBRD_RET_OK;
				}
				return XBRD_RET_FAILED_REQUEST;
			}
			return XBRD_RET_ILLEGAL_RX_CHECKSUM;
		}
		return XBRD_RET_ILLEGAL_RX_HEADER;
	}
	
	return iret;
}

XBRD_RET
	XBRD_SetJump (
	XBRD_HANDLE				hXBRD,
	unsigned long			ulAddress
	)
{
	PXBRD					pXBRD = (PXBRD) hXBRD;
	XBRD_RET				iret;

	if (XBRD_INVALID_HANDLE == hXBRD) {
		return XBRD_RET_INVALID_HANDLE;
	}

	pXBRD->szSndBuf[0] = XBRD_MAGIC_NUMBER;
	pXBRD->szSndBuf[1] = (BYTE) XBRD_SET_JUMP;
	pXBRD->szSndBuf[2] = (BYTE) (ulAddress >> 24);
	pXBRD->szSndBuf[3] = (BYTE) (ulAddress >> 16);
	pXBRD->szSndBuf[4] = (BYTE) (ulAddress >>  8);
	pXBRD->szSndBuf[5] = (BYTE) (ulAddress >>  0);

	iret = XBRD_TransferPacket (pXBRD->socket, pXBRD->szSndBuf, pXBRD->szRcvBuf, XBRD_TRANSFER_BUFFER_SIZE, XBRD_TRANSFER_BUFFER_SIZE);

	if (XBRD_RET_OK == iret) {
		if (XBRD_MAGIC_NUMBER == pXBRD->szRcvBuf[XBRD_TRANSFER_BUFFER_SIZE-1]) {
			if (pXBRD->szRcvBuf[XBRD_TRANSFER_BUFFER_SIZE-1] == XBRD_CalcChecksum (pXBRD->szSndBuf, XBRD_TRANSFER_BUFFER_SIZE-1)) {
				if (0 == pXBRD->szRcvBuf[1]) {
					return XBRD_RET_OK;
				}
				return XBRD_RET_FAILED_REQUEST;
			}
			return XBRD_RET_ILLEGAL_RX_CHECKSUM;
		}
		return XBRD_RET_ILLEGAL_RX_HEADER;
	}
	
	return iret;
}


#endif


