/*!
 *******************************************************************************
                                                                                
                    CREATZ PCIe Driver
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2021 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   pcie_mem.cpp
	 @brief  PCIe memory and buffer management
	 @author Original: by Lee KJ
	 @date   2021/01/25 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <fcntl.h>
#include <string.h> 
#include <stdio.h>


#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"


#include "scamif.h"
#include "scamif_main.h"
#include "scamif_buffer.h"

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h> 
#include <sys/poll.h> 


#include "pcie.h"
#include "vulkan_ioctl.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define LOCAL_DEBUG_ON 1
#define LOCAL_ERROR_TRACE_ON 1
#include "cr_dbg_on.h"
	


#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#define DMA_TIMEOUT		100 /* msec */

#define VULKAN_DEV		"/dev/vulkan"

/*----------------------------------------------------------------------------
 *	Description : static variable declaration  
 -----------------------------------------------------------------------------*/ 
static PCIE_Mem_T PCIe_MemInfo;
static HAND 			DmaMutex;



/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/

 

/*!
 ********************************************************************************
 *	@brief      PCIe device file을 열고 각각의 BAR영역에 대한 memory-mapped pool을 생성한다.
 *							DMA를 포함한 FPGA 제어를 위한 control device 파일을 열어 준비한다.
 *	@param  		None
 *	  
 *  @return			SUCCESS: CR_OK  / FAIL: other values including CR_ERROR 
 *******************************************************************************/
I32 HAL_PCIE_OpenDevice(void)
{
	int i;

	if(access(PCIE_DEVICE_BAR_0, F_OK) != 0)
	{
		LOCAL_TRACE_ERR(("%s:[%d] %s not exist!!\n", __func__, __LINE__, PCIE_DEVICE_BAR_0));
		return CR_ERROR;
	}
	if(access(PCIE_DEVICE_BAR_2, F_OK) != 0)
	{
		LOCAL_TRACE_ERR(("%s:[%d] %s not exist!!\n", __func__, __LINE__, PCIE_DEVICE_BAR_0));
		return CR_ERROR;
	}
	if(access(PCIE_DEVICE_BAR_4, F_OK) != 0)
	{
		LOCAL_TRACE_ERR(("%s:[%d] %s not exist!!\n", __func__, __LINE__, PCIE_DEVICE_BAR_0));
		return CR_ERROR;
	}	

	if(PCIe_MemInfo.fd_bar0 == 0) {
		PCIe_MemInfo.fd_bar0 = open(PCIE_DEVICE_BAR_0, O_RDWR | O_SYNC);
		if(PCIe_MemInfo.fd_bar0 != 0) {
			PCIe_MemInfo.bar0_addr = (U08 *)mmap(0, BAR_0_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, PCIe_MemInfo.fd_bar0, 0 );
		} else {
			LOCAL_TRACE_ERR(("%s:[%d] %s open error\n", __func__, __LINE__, PCIE_DEVICE_BAR_0));
			return CR_ERROR;
		}
	} 
	
	LOCAL_DEBUG(("BAR0 mapping address=0x%lx\n", PCIe_MemInfo.bar0_addr));
	
	if(PCIe_MemInfo.fd_bar2 == 0) {
		PCIe_MemInfo.fd_bar2 = open(PCIE_DEVICE_BAR_2, O_RDWR | O_SYNC);
		if(PCIe_MemInfo.fd_bar2 != 0) {
			PCIe_MemInfo.bar2_addr = (U08 *)mmap(0, BAR_2_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, PCIe_MemInfo.fd_bar2, 0 );
		} else {
			LOCAL_TRACE_ERR(("%s:[%d] %s open error\n", __func__, __LINE__, PCIE_DEVICE_BAR_2));
			return CR_ERROR;
		}			
	}

	LOCAL_DEBUG(("BAR2 mapping address=0x%lx\n", PCIe_MemInfo.bar2_addr));

	if(PCIe_MemInfo.fd_bar4 == 0) {
		PCIe_MemInfo.fd_bar4 = open(PCIE_DEVICE_BAR_4, O_RDWR | O_SYNC);
		if(PCIe_MemInfo.fd_bar4 != 0) {
			PCIe_MemInfo.bar4_addr = (U08 *)mmap(0, BAR_4_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, PCIe_MemInfo.fd_bar4, 0 );
		} else {
			LOCAL_TRACE_ERR(("%s:[%d] %s open error\n", __func__, __LINE__, PCIE_DEVICE_BAR_4));
			return CR_ERROR;
		}					
	}

	LOCAL_DEBUG(("BAR4 mapping address=0x%lx\n", PCIe_MemInfo.bar4_addr));

	/* Open device file for system call */
	PCIe_MemInfo.fd_device = open(VULKAN_DEV, O_RDWR|O_NDELAY);
	if(PCIe_MemInfo.fd_device <= 0 ) {
		LOCAL_TRACE_ERR(("%s:[%d] %s open error\n", __func__, __LINE__, VULKAN_DEV));
		return CR_ERROR;
	}

	DmaMutex = cr_mutex_create();

	PCIe_MemInfo.init_done = CR_TRUE;

	for(i = 0; i< NUM_CAM ; i++)
	{
		PCIe_MemInfo.dram_start_addr[DRAM0_IDX][i] = HAL_PCIE_ReadMem_INT32(BAR_2, CAM_REG_OFFSET(i, DRAM0_START_OFFSET));
		PCIe_MemInfo.dram_size[DRAM0_IDX][i] = HAL_PCIE_ReadMem_INT32(BAR_2, CAM_REG_OFFSET(i, DRAM0_SIZE_OFFSET));		
		PCIe_MemInfo.dram_stride[DRAM0_IDX][i] = HAL_PCIE_ReadMem_INT32(BAR_2, CAM_REG_OFFSET(i, DRAM0_STRIDE_OFFSET));	
		
		PCIe_MemInfo.dram_start_addr[DRAM1_IDX][i] = HAL_PCIE_ReadMem_INT32(BAR_2, CAM_REG_OFFSET(i, DRAM1_START_OFFSET));
		PCIe_MemInfo.dram_size[DRAM1_IDX][i] = HAL_PCIE_ReadMem_INT32(BAR_2, CAM_REG_OFFSET(i, DRAM1_SIZE_OFFSET));				
		PCIe_MemInfo.dram_stride[DRAM1_IDX][i] = HAL_PCIE_ReadMem_INT32(BAR_2, CAM_REG_OFFSET(i, DRAM1_STRIDE_OFFSET));						
	}	

	return CR_OK;
}

/*!
 ********************************************************************************
 *	@brief      Close PCIe device files and terminate device connections
 *
 *	@param  None  
 *
 *  @return			SUCCESS: CR_OK  / FAIL: other values including CR_ERROR 
 *******************************************************************************/
I32 HAL_PCIE_CloseDevice(void)
{
	if(PCIe_MemInfo.fd_bar0 != 0) {
		munmap(PCIe_MemInfo.bar0_addr, BAR_0_SIZE);
		close(PCIe_MemInfo.fd_bar0);
		PCIe_MemInfo.fd_bar0 = 0;
	}

	if(PCIe_MemInfo.fd_bar2 != 0) {
		munmap(PCIe_MemInfo.bar2_addr, BAR_2_SIZE);
		close(PCIe_MemInfo.fd_bar2);
		PCIe_MemInfo.fd_bar2 = 0;
	}

	if(PCIe_MemInfo.fd_bar4 != 0) {
		munmap(PCIe_MemInfo.bar4_addr, BAR_4_SIZE);
		close(PCIe_MemInfo.fd_bar4);
		PCIe_MemInfo.fd_bar4 = 0;
	}
	
	if(PCIe_MemInfo.fd_device != 0) {
		close(PCIe_MemInfo.fd_device);
		PCIe_MemInfo.fd_device = 0;
	}

	cr_mutex_delete (DmaMutex);	
	PCIe_MemInfo.init_done = CR_FALSE;

	return CR_OK;
}

/*!
 ********************************************************************************
 *	@brief      기초기화된 PCIe context 에 생성된 메모리 구조체 포인터를 연결한다
 *
 *	@param[in]  pcie_context
 *							PCIe context 
 *
 *  @return			None 
 *******************************************************************************/
void HAL_PCIE_AddMemInfo2Context(HAL_PCIE_Context_T *pcie_context)
{
	pcie_context->pcie_mem = &PCIe_MemInfo;
}

/*!
 ********************************************************************************
 *	@brief      새로운 이미지프레임이 발생하는지 확인한다
 *
 *	@param[in]  timeout
 *							polling timeout for new image frame
 *
 *  @return			CR_TRUE   
 *							새로운 이미지 프레임이 들어온 경우
 *  @return			CR_FALSE   
 *							입력타임아웃값동안 신규 이미지 프레임이 들어오지 않거나, 디바이스파일을 읽는데 실패한 경우
 *******************************************************************************/
CR_BOOL HAL_PCIE_WaitFrame(U32 timeout)
{
	CR_BOOL ret = CR_FALSE;	
	struct pollfd event;

	if(!PCIe_MemInfo.init_done) {
		LOCAL_TRACE_ERR(("%s:[%d] PCIe mem NOT intialized !!\n", __func__, __LINE__));
		return CR_FALSE;
	}
	
	event.fd = PCIe_MemInfo.fd_device;
	event.events = POLLIN | POLLERR;
	ret = poll((struct pollfd *)&event, 1, timeout);
	if(ret > 0) {
		if(event.revents & POLLIN) {
			ret = CR_TRUE;	
		} else {
			ret = CR_FALSE;
		}
	} else if(ret == 0) {
		LOCAL_TRACE_ERR(("%s:[%d] poll error !!\n", __func__, __LINE__));
		ret = CR_FALSE;
	} else {
		LOCAL_TRACE_ERR(("%s:[%d] poll timeout error !!\n", __func__, __LINE__));
		ret = CR_FALSE;
	}

	return ret;
}

/*!
 ********************************************************************************
 *	@brief      write 32bit integer value to the offset in PCIe BAR
 *
 *	@param[in]  bar
 *							PCIe BAR number
 *	@param[in]  offset
 *							data offset
 *	@param[in]  value
 *	 						32bit integer
 *  @return			SUCCESS: CR_OK  / FAIL: other values including CR_ERROR
 *
 *******************************************************************************/
I32 HAL_PCIE_WriteMem_INT32(int bar, U32 offset, I32 value)
{
	volatile I32 *bar_addr;

	if(!PCIe_MemInfo.init_done) {
		LOCAL_TRACE_ERR(("%s:[%d] PCIe mem NOT intialized !!\n", __func__, __LINE__));
		return CR_ERROR;
	}
	
	if(bar == BAR_0) {
		bar_addr = (volatile I32 *)PCIe_MemInfo.bar0_addr; 
	} else if(bar == BAR_2) {
		bar_addr = (volatile I32 *)PCIe_MemInfo.bar2_addr; 
	} else {
		LOCAL_TRACE_ERR(("%s: BAR %d is read only!\n", __func__, bar));
		return CR_ERROR;
	}

	*bar_addr = value;
	
	
	return CR_OK;	
}

/*!
 ********************************************************************************
 *	@brief      write 64bit integer value to the offset in PCIe BAR
 *
 *	@param[in]  bar
 *							PCIe BAR number
 *	@param[in]  offset
 *							data offset
 *	@param[in]  value
 *	 						64bit integer
 *  @return			SUCCESS: CR_OK  / FAIL: other values including CR_ERROR
 *
 *******************************************************************************/
I32 HAL_PCIE_WriteMem_INT64(int bar, U32 offset, I64 value)
{
	volatile I64 *bar_addr;

	if(!PCIe_MemInfo.init_done) {
		LOCAL_TRACE_ERR(("%s:[%d] PCIe mem NOT intialized !!\n", __func__, __LINE__));
		return CR_ERROR;
	}
	
	if(bar == BAR_0) {
		bar_addr = (volatile I64 *)PCIe_MemInfo.bar0_addr; 
	} else if(bar == BAR_2) {
		bar_addr = (volatile I64 *)PCIe_MemInfo.bar2_addr; 
		return CR_ERROR;		
	} else {
		LOCAL_TRACE_ERR(("%s: BAR %d is read only!\n", __func__, bar));
		return CR_ERROR;
	}

	*bar_addr = value;
	
	
	return CR_OK;	
}



/*!
 ********************************************************************************
 *	@brief      write data to the offset in PCIe BAR.
 *	@brief      Not supported now
 *	@param[in]  bar
 *							PCIe BAR number
 *	@param[in]  offset
 *							data offset
 *	@param[in]  buf
 *	 						data buffer 
 *	@param[in]  size
 *	 						data size  
 *  @return			SUCCESS: CR_OK  / FAIL: other values including CR_ERROR
 *
 *******************************************************************************/
I32 HAL_PCIE_WriteMem_Buffer(int bar, U32 offset, U08 *buf, U32 size)
{
	/* Not supported */
	return CR_OK;	
}



/*!
 ********************************************************************************
 *	@brief      read 32bit integer value from the offset in PCIe BAR
 *
 *	@param[in]  bar
 *							PCIe BAR number
 *	@param[in]  offset
 *							data offset
 *
 *  @return			32bit integer. If any error happens, 0 is returned.
 *
 *******************************************************************************/
I32 HAL_PCIE_ReadMem_INT32(int bar, U32 offset)
{
	volatile I32 *bar_addr;

	if(!PCIe_MemInfo.init_done) {
		LOCAL_TRACE_ERR(("%s:[%d] PCIe mem NOT intialized !!\n", __func__, __LINE__));
		return 0;
	}
	
	if(bar == BAR_0) {
		bar_addr = (volatile I32 *)PCIe_MemInfo.bar0_addr; 
	} else if(bar == BAR_2) {
		bar_addr = (volatile I32 *)PCIe_MemInfo.bar2_addr; 
	} else {
		bar_addr = (volatile I32 *)PCIe_MemInfo.bar4_addr; 
	}

	return *(bar_addr+offset);	
}

/*!
 ********************************************************************************
 *	@brief      read 64bit integer value from the offset in PCIe BAR
 *
 *	@param[in]  bar
 *							PCIe BAR number
 *	@param[in]  offset
 *							data offset
 *
 *  @return			64bit integer. If any error happens, 0 is returned.
 *
 *******************************************************************************/
I64 HAL_PCIE_ReadMem_INT64(int bar, U32 offset)
{
	volatile I64 *bar_addr;

	if(!PCIe_MemInfo.init_done) {
		LOCAL_TRACE_ERR(("%s:[%d] PCIe mem NOT intialized !!\n", __func__, __LINE__));
		return 0;
	}	
	
	if(bar == BAR_0) {
		bar_addr = (volatile I64 *)PCIe_MemInfo.bar0_addr; 
	} else if(bar == BAR_2) {
		bar_addr = (volatile I64 *)PCIe_MemInfo.bar2_addr; 
	} else {
		bar_addr = (volatile I64 *)PCIe_MemInfo.bar4_addr; 
	}

	return *(bar_addr+offset);	
}

/*!
 ********************************************************************************
 *	@brief      read data from the offset in PCIe BAR
 *
 *	@param[in]  bar
 *							PCIe BAR number
 *	@param[in]  offset
 *							data offset 
 *	@param[in]  size
 *							data size
 *	@param[out]  buf
 *							read buffer
 * 
 *  @return			32bit integer. If any error happens, 0 is returned.
 *
 *******************************************************************************/
I32 HAL_PCIE_ReadMem_Buffer(int bar, U32 offset, U08 *buf, U32 size)
{
	I32 timeout = DMA_TIMEOUT;
	I32 dma_status = DMA_STATUS_STOP;
	int ret = CR_OK;	
#if defined(USE_SYSCALL) && VULKAN_DMA_IOCTL
	struct pollfd event;
#endif

	if(!PCIe_MemInfo.init_done) {
		LOCAL_TRACE_ERR(("%s:[%d] PCIe mem NOT intialized !!\n", __func__, __LINE__));
		return CR_ERROR;
	}


	if(bar == BAR_4) {
		// TODO: necessary code later	
	} else {
		LOCAL_TRACE_ERR(("%s: Not supported BAR %d !\n", __func__, bar));
		return CR_ERROR;
	}
	
	cr_mutex_wait(DmaMutex, INFINITE);


	/* Set Source(FPGA DRAM address) */ 
	*(volatile I64 *)(PCIe_MemInfo.bar0_addr + BAR_0_DMA_SRC_ADDR_OFFSET) = offset; // TODO: if needed, you can implement Seek instead

#if VULKAN_DMA_IOCTL	
	/* Set Destination(Linux user space address) */
	ioctl(PCIe_MemInfo.fd_device, SET_USR_BUF_ADDR, (I64)buf);

	/* Read Size */
	*(volatile I64 *)(PCIe_MemInfo.bar0_addr + BAR_0_DMA_SIZE_OFFSET) = size; 

	/* Start DMA */
	*(volatile I32 *)(PCIe_MemInfo.bar0_addr + BAR_0_DMA_CTRL_OFFSET) = DMA_RUN; 
#endif	

	
#ifdef USE_SYSCALL
#if VULKAN_DMA_IOCTL
		event.fd = PCIe_MemInfo.fd_device;
		event.events = POLLIN | POLLERR;
		ret = poll((struct pollfd *)&event, 1, timeout);
		if(ret > 0) {
			if(event.revents & POLLIN) {
				dma_status = *(volatile I32 *)(PCIe_MemInfo.bar0_addr + BAR_0_DMA_STATUS_OFFSET);
				if(dma_status == DMA_STATUS_DONE) { 			
					ioctl(PCIe_MemInfo.fd_device, COPY_DMA_BUF, size);	
				} else if(dma_status == DMA_STATUS_ERR) {
					LOCAL_TRACE_ERR(("%s:[%d] poll DMA status error !!\n", __func__, __LINE__));
					ret = CR_ERROR;;
				}
			} else {
				LOCAL_TRACE_ERR(("%s:[%d] poll DMA error !!\n", __func__, __LINE__));		
				ret = CR_ERROR;;
			}
		} else if(ret == 0) {
			LOCAL_TRACE_ERR(("%s:[%d] poll error !!\n", __func__, __LINE__));
			ret = CR_ERROR;;
		} else {
			LOCAL_TRACE_ERR(("%s:[%d] poll timeout error !!\n", __func__, __LINE__));
			ret = CR_ERROR;;
		}
#else
		ret = read(PCIe_MemInfo.fd_device, buf, size);
		if(ret > 0) {
		} else {
			LOCAL_TRACE_ERR(("%s:[%d] read error !!\n", __func__, __LINE__));
			ret = CR_ERROR;;		
		}
#endif
	
#else
		while(timeout-- > 0)
		{
			dma_status = *(volatile I32 *)(PCIe_MemInfo.bar0_addr + BAR_0_DMA_STATUS_OFFSET);
			if(dma_status == DMA_STATUS_DONE) {
				
				ioctl(PCIe_MemInfo.fd_device, COPY_DMA_BUF, size);	
				break;
			} else if(dma_status == DMA_STATUS_ERR) {
				LOCAL_TRACE_ERR(("%s:[%d] error !!\n", __func__, __LINE__));
				ret = CR_ERROR;;
			}
	
			OSAL_SYS_MicroSleep(1);
		}
	
		if(timeout <=0 ) {
			LOCAL_TRACE_ERR(("%s:[%d] timeout error !!\n", __func__, __LINE__));
			ret = CR_ERROR;;
		}		
#endif
	
		cr_mutex_release(DmaMutex); 
	
		return ret;
	



}



#if defined (__cplusplus)
}
#endif

