/*!
 *******************************************************************************
                                                                                
                    CREATZ PCIe driver 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2021 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   pcie_drv.cpp
	 @brief  user level PCIe device driver
	 @author Original: by Lee KJ
	 @date   2021/01/22 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <string.h> 

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"


#include "scamif.h"
#include "scamif_main.h"
#include "scamif_buffer.h"


#include "pcie.h"
#include "cr_cli.h"

#include "iana.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define LOCAL_DEBUG_ON 1
#define LOCAL_ERROR_TRACE_ON 1
#include "cr_dbg_on.h"


#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/
#define CAM_INIT_TIMEOUT			100	/* msec */
#define FRAME_TIMEOUT				100	/* msec */
/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/
static cr_thread_t PCIeCommThreadContext;


/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
#ifdef DEBUG_BUILD 
static BOOL TT_CLI_Write_PCIe_INT32_BAR( CLI_Parse_t *pars_p, char *result_sym_p );	
static BOOL TT_CLI_Read_PCIe_INT32_BAR( CLI_Parse_t *pars_p, char *result_sym_p );
#endif

static void ResetFrameNumber(HAL_PCIE_Context_T *pcie_context, int camid)
{
	cr_mutex_wait(pcie_context->hMutex, INFINITE);
	HAL_PCIE_WriteMem_INT32(BAR_2, BAR_2_CAM_CTRL_OFFSET, CAM_CMD_CTRL_CLEAR_FRAME_NUM<<(camid*CAM_CMD_CTRL_SHIFT));
	pcie_context->current_frame_num[camid] = 0;
	cr_mutex_release(pcie_context->hMutex);	
}

static I32 ReadCamStatus(int cam_id)
{
	I32 cam_status = CAM_CMD_STATUS_IDLE;

	cam_status = HAL_PCIE_ReadMem_INT32(BAR_2, BAR_2_CAM_STATUS_OFFSET);
	cam_status = (cam_status>>(cam_id*CAM_CMD_CTRL_SHIFT)) & 0xF;

	return cam_status;
}

// TODO: mutex

// TODO: cur frame num reset control w/mutex

// TODO: stop &\\ start  sync

/* Resized image streaming */
void *PCIeImageStreamingThread(void *startcontext)
{
	cr_thread_t *pt;
	HAL_PCIE_Context_T *pcie_context;	
	volatile U32	loop;
	U32				u0;
	U32 camid;
	U32 cur_frame_num[MAXCAMCOUNT];
	U08 *imageBuf;
	U32 read_frame_addr;
	U32 dram_idx ;


	pcie_context = (HAL_PCIE_Context_T *)startcontext;

	//--------------------------------------- 
	// Change state..
	pt = &PCIeCommThreadContext;
	pt->ustate = CR_THREAD_STATE_INITED;	


	//---------------------------------------
	pt->ustate = CR_THREAD_STATE_RUN;
	loop = 1;

	imageBuf = (U08*)cr_malloc(PCIE_IMG_BUF_SIZE);


	while(loop) {

		cr_mutex_wait(pcie_context->hMutex, INFINITE);

		if(HAL_PCIE_WaitFrame(FRAME_TIMEOUT)) {
			if(pcie_context->bulk_mode == CR_TRUE) {
				dram_idx = DRAM0_IDX;						
			} else {
				dram_idx = DRAM1_IDX;						
			}
			for(camid= 0 ; camid <NUM_CAM; camid++)
			{
				cur_frame_num[camid] = HAL_PCIE_ReadMem_INT32(BAR_2, CAM_REG_OFFSET(camid, CUR_FRAME_NUM_OFFSET));
				while(cur_frame_num[camid] != pcie_context->current_frame_num[camid]) {					

					if (pcie_context->imageCallbackFunc[camid] != NULL && pcie_context->status[camid] >=	PCIE_STATE_START) {

						PCIeImageHeader_T imageHeader;
						
						read_frame_addr = pcie_context->pcie_mem->dram_start_addr[dram_idx][camid] + 
										(pcie_context->current_frame_num[camid] % pcie_context->max_frame_num[dram_idx][camid])*pcie_context->frame_size[dram_idx][camid];
						
						HAL_PCIE_ReadMem_Buffer(BAR_4, read_frame_addr, imageBuf, pcie_context->frame_size[dram_idx][camid]);
						memcpy((U08*)&imageHeader, imageBuf, PCIE_IMG_HEADER_SIZE);

						// TODO: improve buffer management later - e.g. direct memory writing wo/intermediate buffer	 if possible					
						pcie_context->imageCallbackFunc[camid](
									&imageBuf[PCIE_IMG_HEADER_SIZE],										
									(imageHeader.size_x * imageHeader.size_y),				
									imageHeader.ts_high,		imageHeader.ts_low,
									imageHeader.size_x,		imageHeader.size_y,
									imageHeader.offset_x,	imageHeader.offset_y,
									pcie_context->bulk_mode? NORMALBULK_BULK: NORMALBULK_NORMAL,	
									0 /*imageHeader.skip*/, 
									1 /*imageHeader.multitude*/,
									pcie_context->hParent[camid]);

					}
					
					pcie_context->current_frame_num[camid]++;
					if(pcie_context->current_frame_num[camid] >= pcie_context->max_frame_num[dram_idx][camid]) {
						pcie_context->current_frame_num[camid] = 0;	
					}
				}
			}
		}
		else cr_sleep(1000); // just for test

		cr_mutex_release(pcie_context->hMutex);		
		
#define CHECK_PERIOD	(-1)				
		u0 = cr_event_wait (pt->hevent, CHECK_PERIOD);
		if (pt->ustate == CR_THREAD_STATE_NEEDSTOP) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%s: I would be killed...\n", __func__);
			break;
		}
	}

	cr_free(imageBuf);
	pt->ustate = CR_THREAD_STATE_STOPPING;
	//--------------------------------------- 

	return 0;
}



/*!
 ********************************************************************************
 *	@brief      start PCIe thread
 *
 *	@param[in]  pcie_context
 *							PCIe context pointer
 *  @return			If no error happens, 0 is returned
 *
 *******************************************************************************/
I32 HAL_PCIE_StartThread(void *pcie_context)
{
	PCIeCommThreadContext.ustate = CR_THREAD_STATE_NULL;
	PCIeCommThreadContext.hevent = cr_event_create();
	PCIeCommThreadContext.hthread = (cr_thread_t *) cr_thread_create(PCIeImageStreamingThread, pcie_context);

	cr_thread_setpriority(PCIeCommThreadContext.hthread, PCIE_COMM_TASK_PRIORITY);	

	return 0;
}


/*!
 ********************************************************************************
 *	@brief      stop PCIe thread
 *
 *	@param			None
 *						
 *  @return			If no error happens, 0 is returned
 *
 *******************************************************************************/
I32 HAL_PCIE_StopThread(void)
{	
	PCIeCommThreadContext.ustate = CR_THREAD_STATE_NEEDSTOP;	
	cr_event_set(PCIeCommThreadContext.hevent);
	
	cr_thread_join(PCIeCommThreadContext.hthread); 
	cr_event_delete(PCIeCommThreadContext.hevent);

	return 0;
}



/*!
 ********************************************************************************
 *	@brief      set SCAMIF callback
 *
 *	@param[in]  hand
 *							PCIe context pointer
 *	@param[in]  cam_id
 *							CAM number
 *	@param[in]  fnCB
 *							SCAMIF callback for image buffering
 *  @return			If no error happens, 0 is returned
 *
 *******************************************************************************/
I32 HAL_PCIE_SetCallBackFunc(HAND hand, int cam_id, CR_video_fnCB fnCB)
{
	HAL_PCIE_Context_T *pcie_context;

	pcie_context = (HAL_PCIE_Context_T *)hand;

	pcie_context->imageCallbackFunc[cam_id] = fnCB;

	return CR_OK;
}


/*!
 ********************************************************************************
 *	@brief      high level PCIe API to start PCIe operation
 *
 *	@param[in]  pP
 *							SCAMIF context
 *	@param[in]  runmode
 *							running mode - not used yet
 *	@param[in]  stchannelmode
 *							streaming mode - not used yet
 *  @return			If no error happens, 0 is returned
 *
 *******************************************************************************/
HAND HAL_PCIE_Start(HAND pP[], U32 camcount, U32 runmode, U32 stchannelmode)
{
	HAL_PCIE_Context_T *pcie_context;
	int i;

	pcie_context = (HAL_PCIE_Context_T *)cr_malloc(sizeof(HAL_PCIE_Context_T));

	pcie_context->hMutex = cr_mutex_create();	

	for(i=0; i< NUM_CAM; i++)
	{
		pcie_context->hParent[i] = (void*)pP[i];
		pcie_context->status[i] = 	PCIE_STATE_CREATE;
	}

	HAL_PCIE_AddMemInfo2Context(pcie_context);

	HAL_PCIE_StartThread((void*)pcie_context);	

	return (HAND)pcie_context;	
}

/*!
 ********************************************************************************
 *	@brief      high level PCIe API to stop PCIe operation
 *
 *	@param[in]  hand
 *							PCIe context pointer
 *  @return			If no error happens, 0 is returned
 *
 *******************************************************************************/
I32 HAL_PCIE_Stop(HAND hand)
{	
	HAL_PCIE_Context_T *pcie_context;
	int i;			

	if(hand == NULL) {
		return CR_ERROR;
	}

	pcie_context = (HAL_PCIE_Context_T *)hand;

	HAL_PCIE_StopThread();
	
	cr_mutex_delete (pcie_context->hMutex);	
		
	for(i=0; i < NUM_CAM ; i++)
	{
		pcie_context->imageCallbackFunc[i] = NULL;
		pcie_context->status[i] = 	PCIE_STATE_NONE;
	}		

	cr_free(hand);

	return CR_OK;
}

/*!
 ********************************************************************************
 *	@brief      Intialize CAM before the use
 *
 *	@param[in]  hand
 *							PCIe context pointer
 *	@param[in]  cam_id
 *							CAM number
 *	@param[in]  zrot
 *							if 1, CAM is running with ZROT mode(High speed). Otherwise, NROT mode(normal mode)
 *  @return			If no error happens, 0(CR_OK) is returned
 *
 *******************************************************************************/
I32 HAL_PCIE_InitCam(HAND hand, int cam_id, CR_BOOL zrot)
{	
	HAL_PCIE_Context_T *pcie_context;
	int timeout = CAM_INIT_TIMEOUT; // msec
	I32 cam_status = CAM_CMD_STATUS_IDLE;

	if(hand == NULL) {
		return CR_ERROR;
	}

	pcie_context = (HAL_PCIE_Context_T *)hand;	
	  
	if(zrot) {
		HAL_PCIE_WriteMem_INT32(BAR_2, BAR_2_CAM_CTRL_OFFSET, CAM_CMD_CTRL_INIT_ZROT<<(cam_id*CAM_CMD_CTRL_SHIFT));
	} else {
		HAL_PCIE_WriteMem_INT32(BAR_2, BAR_2_CAM_CTRL_OFFSET, CAM_CMD_CTRL_INIT_NROT<<(cam_id*CAM_CMD_CTRL_SHIFT));
	}
	
	while(timeout) {
		cam_status = ReadCamStatus(cam_id);
		if(cam_status == CAM_CMD_STATUS_INIT_OK) {
			break;
		}
		timeout -= 10;
		cr_sleep(10);
	}
	
	if(timeout <= 0) {
		LOCAL_TRACE_ERR(("%s: %d status read timeout!!\n", __func__, __LINE__));
		return CR_ERROR;
	}	


	pcie_context->status[cam_id] = 	PCIE_STATE_INIT;
	
	return CR_OK;
}

/*!
 ********************************************************************************
 *	@brief      Set CAM run state
 *
 *	@param[in]  hand
 *							PCIe context pointer
 *	@param[in]  cam_id
 *							CAM number
 *	@param[in]  state
 *							CAM_Run_State_T
 *  @return			If no error happens, 0(CR_OK) is returned
 *
 *******************************************************************************/
I32 HAL_PCIE_SetRunState(HAND hand, CAM_Run_State_T state)
{
	HAL_PCIE_Context_T *pcie_context;
	int i;
	
	if(hand == NULL) {
		return CR_ERROR;
	}

	pcie_context = (HAL_PCIE_Context_T *)hand;	

	HAL_PCIE_WriteMem_INT32(BAR_2, BAR_2_RUN_STATE_OFFSET, state);

	for(i=0; i < NUM_CAM ; i++)
	{
		pcie_context->status[i] = 	(state + PCIE_STATE_START);
	}	

	return CR_OK;
}

/*!
 ********************************************************************************
 *	@brief      Start CAM operation(Image frame transfer)
 *
 *	@param[in]  hand
 *							PCIe context pointer
 *	@param[in]  cam_id
 *							CAM number
 *
 *  @return			If no error happens, 0(CR_OK) is returned
 *
 *******************************************************************************/
I32 HAL_PCIE_StartCam(HAND hand, int cam_id)
{
	HAL_PCIE_Context_T *pcie_context;
	
	if(hand == NULL) {
		return CR_ERROR;
	}

	pcie_context = (HAL_PCIE_Context_T *)hand;	

	HAL_PCIE_WriteMem_INT32(BAR_2, BAR_2_CAM_CTRL_OFFSET, CAM_CMD_CTRL_START<<(cam_id*CAM_CMD_CTRL_SHIFT));

	pcie_context->status[cam_id] = 	PCIE_STATE_START;

	pcie_context->frame_size[DRAM0_IDX][cam_id] = HAL_PCIE_ReadMem_INT32(BAR_2, CAM_REG_OFFSET(cam_id, DRAM0_STRIDE_OFFSET));	
	pcie_context->frame_size[DRAM1_IDX][cam_id] = HAL_PCIE_ReadMem_INT32(BAR_2, CAM_REG_OFFSET(cam_id, DRAM1_STRIDE_OFFSET));		
	pcie_context->max_frame_num[DRAM0_IDX][cam_id] = pcie_context->pcie_mem->dram_size[DRAM0_IDX][cam_id] / pcie_context->frame_size[DRAM0_IDX][cam_id];
	pcie_context->max_frame_num[DRAM1_IDX][cam_id] = pcie_context->pcie_mem->dram_size[DRAM1_IDX][cam_id] / pcie_context->frame_size[DRAM1_IDX][cam_id];	
	
	return CR_OK;
}

/*!
 ********************************************************************************
 *	@brief      Stop CAM operation(Image frame transfer)
 *
 *	@param[in]  hand
 *							PCIe context pointer
 *	@param[in]  cam_id
 *							CAM number
 *
 *  @return			If no error happens, 0(CR_OK) is returned
 *
 *******************************************************************************/
I32 HAL_PCIE_StopCam(HAND hand, int cam_id)
{

	HAL_PCIE_Context_T *pcie_context;
	int i;
	
	if(hand == NULL) {
		return CR_ERROR;
	}

	pcie_context = (HAL_PCIE_Context_T *)hand;	

	HAL_PCIE_WriteMem_INT32(BAR_2, BAR_2_CAM_CTRL_OFFSET, CAM_CMD_CTRL_PAUSE<<(cam_id*CAM_CMD_CTRL_SHIFT));
	ResetFrameNumber((HAL_PCIE_Context_T *)hand, cam_id);

	pcie_context->status[cam_id] = 	PCIE_STATE_STOP;	

	return CR_OK;
}


static U32 GetFrameIndex(int cam_id, U32 ts_h, U32 ts_l, U32 tsshot_h, U32 tsshot_l)
{
	U32 frame_start_idx = 0;


	// TODO: find bulk image index from parameters - how to get the index??
	

	return frame_start_idx;
}

/*!
 ********************************************************************************
 *	@brief      Get Bulk Images
 *
 *	@param[in]  
 *
 *  @return			If no error happens, 0(CR_OK) is returned
 *
 *******************************************************************************/
I32 HAL_PCIE_RequestBulkImage(HAND hand, int cam_id,  U32 ts_h, U32 ts_l, U32 framecount, U32 waittime, U32 continueafterbulk, 
	U32 multitude4Bulk, U32 skip4bulk
	, U32 width4bulk, U32 height4bulk, U32 offset_x4bulk, U32 offset_y4bulk, U32 tsshot_h, U32 tsshot_l,I32 axx1024, I32 bxx1024, I32 ayx1024, I32 byx1024)
{

	HAL_PCIE_Context_T *pcie_context;
	U32 i;
	PCIeImageHeader_T imageHeader;
	U32 read_frame_addr;
	U32 frame_start_idx;
	U08 imageBuf[PCIE_IMG_BUF_SIZE];
	
	if(hand == NULL) {
		return CR_ERROR;
	}

	pcie_context = (HAL_PCIE_Context_T *)hand;	

	frame_start_idx = GetFrameIndex(cam_id,ts_h, ts_l, tsshot_h, tsshot_l);

	read_frame_addr = pcie_context->pcie_mem->dram_start_addr[DRAM0_IDX][cam_id] + frame_start_idx*pcie_context->frame_size[DRAM0_IDX][cam_id];

	for(i=0; i < framecount; i++) 
	{					
		HAL_PCIE_ReadMem_Buffer(BAR_4, read_frame_addr+i*pcie_context->frame_size[DRAM0_IDX][cam_id], imageBuf, pcie_context->frame_size[DRAM0_IDX][cam_id]);
		memcpy((U08*)&imageHeader, imageBuf, PCIE_IMG_HEADER_SIZE);

		if (pcie_context->imageCallbackFunc[cam_id] != NULL) {

			pcie_context->imageCallbackFunc[cam_id](
								&imageBuf[PCIE_IMG_HEADER_SIZE],										
								(imageHeader.size_x * imageHeader.size_y),				
								imageHeader.ts_high,		imageHeader.ts_low,
								imageHeader.size_x,		imageHeader.size_y,
								imageHeader.offset_x,	imageHeader.offset_y,
								imageHeader.normalbulk,	imageHeader.skip, imageHeader.multitude,
								pcie_context->hParent[cam_id]);

		}
	}

	return CR_OK;
}

/*!
 ********************************************************************************
 *	@brief      Update all arrtibutes for CAM image processing at once
 *
 *	@param[in]  hand
 *							PCIe context pointer
 *	@param[in]  cam_id
 *							CAM number 
 *	@param[in]  pImgAttr
 *							Image attributes
 * 
 *  @return			If no error happens, 0(CR_OK) is returned
 *
 *******************************************************************************/
I32 HAL_PCIE_UpdateImageParamAll(HAND hand, int cam_id, CAM_IMG_ATTR_T * pImgAttr)
{
	HAL_PCIE_Context_T *pcie_context;
	I32 resizeOpt = CAM_RESIZE_OPTION_OFF;
	U32		rot_size, roi_offset;

	if(hand == NULL) {
		return CR_ERROR;
	}

	pcie_context = (HAL_PCIE_Context_T *)hand;	

// TODO: para setting accoridng to scenario 3 	
	
	if ( (pImgAttr->width > IMAGE_MAX_WIDTH || pImgAttr->width <= 0) || (pImgAttr->height > IMAGE_MAX_HEIGHT || pImgAttr->height <= 0) ) {
		return CR_ERROR;
	}

//	pcie_context->frame_size[cam_id] = pImgAttr->width * pImgAttr->height + PCIE_IMG_HEADER_SIZE;

	rot_size = (pImgAttr->width & ROI_W_MASK) | ((pImgAttr->height & ROI_H_MASK)>>16);

	HAL_PCIE_WriteMem_INT32(BAR_2, CAM_REG_OFFSET(cam_id, ROI_SIZE_OFFSET), rot_size);

	// GECP_ImagePosition
	if ( (pImgAttr->sx >= IMAGE_MAX_WIDTH || pImgAttr->sx < 0) || (pImgAttr->sy >= IMAGE_MAX_HEIGHT || pImgAttr->sy < 0) ) {
		return CR_ERROR;
	}

	roi_offset = (pImgAttr->sx & ROI_X_MASK) | ((pImgAttr->sy & ROI_Y_MASK)>>16);

	HAL_PCIE_WriteMem_INT32(BAR_2, CAM_REG_OFFSET(cam_id, ROI_OFFSET_OFFSET), roi_offset);


	if (pImgAttr->gain > IMAGE_MAX_GAIN || pImgAttr->gain < IMAGE_MIN_GAIN) {
		return CR_ERROR;
	}

	HAL_PCIE_WriteMem_INT32(BAR_2, CAM_REG_OFFSET(cam_id, ANALOG_GAIN_OFFSET), pImgAttr->gain);
	// TODO: Digital gain??	
	


	if (pImgAttr->multitude4normal > 0) {
		resizeOpt = pImgAttr->multitude4normal - 1;
	}
	
	HAL_PCIE_WriteMem_INT32(BAR_2, CAM_REG_OFFSET(cam_id, RESIZE_OPTION_OFFSET), resizeOpt);

	return CR_OK;
}

/*!
 ********************************************************************************
 *	@brief      Update each arrtibutes for CAM image processing
 *
 *	@param[in]  hand
 *							PCIe context pointer
 *	@param[in]  cam_id
 *							CAM number 
 *	@param[in]  type
 *							attribute type
 *	@param[in]  value
 *							attribute value
 *
 *  @return			If no error happens, 0(CR_OK) is returned
 *
 *******************************************************************************/
I32 HAL_PCIE_UpdateImageParam(HAND hand, int cam_id, U32 type, I32 value)		
{	
	I32 tmp_value = 0;
	
	switch(type) {
		case CAM_IMAGE_PARAM_GAMMA:
			tmp_value = HAL_PCIE_ReadMem_INT32(BAR_2, CAM_REG_OFFSET(cam_id,LUT_SETTING_OFFSET));
			tmp_value = (tmp_value & (~LUT_GAMMA_MASK)) | (value <<LUT_GAMMA_SHIFT);
			HAL_PCIE_WriteMem_INT32(BAR_2, CAM_REG_OFFSET(cam_id,LUT_SETTING_OFFSET), tmp_value);
		case CAM_IMAGE_PARAM_CONTRAST:			
			tmp_value = HAL_PCIE_ReadMem_INT32(BAR_2, CAM_REG_OFFSET(cam_id,LUT_SETTING_OFFSET));
			tmp_value = (tmp_value & (~LUT_CONTRAST_MASK)) | (value <<LUT_CONTRAST_SHIFT);
			HAL_PCIE_WriteMem_INT32(BAR_2, CAM_REG_OFFSET(cam_id,LUT_SETTING_OFFSET), tmp_value);			
			break;						
		case CAM_IMAGE_PARAM_BRIGHT:
			tmp_value = HAL_PCIE_ReadMem_INT32(BAR_2, CAM_REG_OFFSET(cam_id,LUT_SETTING_OFFSET));
			tmp_value = (tmp_value & (~LUT_BRIGHTNESS_MASK)) | (value <<LUT_BRIGHTNESS_SHIFT);
			HAL_PCIE_WriteMem_INT32(BAR_2, CAM_REG_OFFSET(cam_id,LUT_SETTING_OFFSET), tmp_value);				
			break;			
		case CAM_IMAGE_PARAM_GAIN:
			HAL_PCIE_WriteMem_INT32(BAR_2, CAM_REG_OFFSET(cam_id,ANALOG_GAIN_OFFSET), value);
		case CAM_IMAGE_PARAM_DELAY_OFFSET:
		case CAM_IMAGE_PARAM_EXPOSURE_EXPIATE:		
			break;						
		case CAM_IMAGE_PARAM_LED_BRIGHT:					
			HAL_PCIE_WriteMem_INT32(BAR_2, LED_0_BRIGHTNESS_OFFSET+(cam_id*4), value);
			break;
		case CAM_IMAGE_PARAM_ROI_OFFSET:					
			HAL_PCIE_WriteMem_INT32(BAR_2, CAM_REG_OFFSET(cam_id,ROI_OFFSET_OFFSET), value);
			break;
		case CAM_IMAGE_PARAM_ROI_SIZE:					
			HAL_PCIE_WriteMem_INT32(BAR_2, CAM_REG_OFFSET(cam_id,ROI_SIZE_OFFSET), value);
			break;
		default:
			break;
	}

	return CR_OK;
}



/*!
 ********************************************************************************
 *	@brief      Get current timestamp value from FPGA
 *
 *	@param[in]  hand
 *							PCIe context pointer
 *
 *  @return			64bit timestamp 
 *
 *******************************************************************************/
U64 HAL_PCIE_GetTimeStamp(HAND hand)
{
	HAL_PCIE_Context_T *pcie_context;
	
	if(hand == NULL) {
		return CR_ERROR;
	}

	return (U64)HAL_PCIE_ReadMem_INT64(BAR_2, BAR_2_TIMESTAMP_LOW_OFFSET);
}

/*!
 ********************************************************************************
 *	@brief      set launch detection capture time
 *
 *	@param[in]  hand
 *							PCIe context pointer
 *	@param[in]  capture_time1
 *							Capture Time #1 after Launch Detection (unit: 0.1 us)
 *	@param[in]  capture_time2
 *							Capture Time #2 after Capture Time #1 (unit: 0.1 us)
 *
 *  @return			If no error happens, 0(CR_OK) is returned
 *
 *******************************************************************************/
I32 HAL_PCIE_SetLaunchDetectionCaptureTime(HAND hand, int capture_time1, int capture_time2) 
{
	HAL_PCIE_Context_T *pcie_context;
	
	if(hand == NULL) {
		return CR_ERROR;
	}

	if(capture_time1 >= 0) {
		HAL_PCIE_WriteMem_INT32(BAR_2, BAR_2_LAUNCH_DETECTION_CAPTURE_TIME_1_OFFSET, capture_time1);
	}
	if(capture_time2 >= 0) {
		HAL_PCIE_WriteMem_INT32(BAR_2, BAR_2_LAUNCH_DETECTION_CAPTURE_TIME_2_OFFSET, capture_time2);
	}

	return CR_OK;
}



I32 HAL_PCIE_SetSyncUpdate(HAND hand, int cam_id, U32 updateme) // 0: NO update, 1: yes.. update me
{
	// TODO:
	return CR_OK;
}

I32 HAL_PCIE_ReadInfo(HAND hand, int cam_id, U32 type, U32 *rdata, U32 count)
{

	switch(type) {

		case CAM_INFO_BOARD:
		case CAM_INFO_PC:			
		case CAM_INFO_CPUSID:
		case CAM_INFO_CPUBDSERIAL:
		case CAM_INFO_RUNCODE:
		case CAM_INFO_BD_DATA_RSVD:		
			// TODO: if needed
			break;
		default:
			break;
	}

	return CR_OK;
}


I32 HAL_PCIE_WriteInfo(HAND hand, int cam_id, U32 type, U32 *wdata, U32 count)
{

	switch(type) {

		case CAM_INFO_BOARD:
		case CAM_INFO_PC:			
		case CAM_INFO_CPUSID:
		case CAM_INFO_CPUBDSERIAL:
		case CAM_INFO_RUNCODE:
		case CAM_INFO_BD_DATA_RSVD:		
			// TODO: if needed
			break;
		default:
			break;
	}

	return CR_OK;
}


/*!
 ********************************************************************************
 *	@brief      CAM spin check
 *
 *  @param[in]	hand
 *              PCIE driver handle
 *  @param[in]	heartbeattimeout
 *              msec.  0: Do not check heartbeat any more
 *
 *  @return		CR_OK: good, Else no-good
 *
 *	@author	    
 *  @date       
 *******************************************************************************/
int HAL_PCIE_SetHeartBeatTimeout(HAND hand, int cam_id,	U32 heartbeattimeout) 
{
	// TODO: if needed

	return CR_OK;
}

/*!
 ********************************************************************************
 *	@brief      high level PCIe API to initialize PCIe operation
 *
 *	@param			None
 *
 *  @return			If no error happens, 0 is returned
 *
 *******************************************************************************/
int HAL_PCIE_Init(void)
{
	HAL_PCIE_OpenDevice();
#ifdef DEBUG_BUILD
	TOOL_CLI_Command("wbar",    TT_CLI_Write_PCIe_INT32_BAR, "write 32bit integer to PCIe BAR");
	TOOL_CLI_Command("rbar",	TT_CLI_Read_PCIe_INT32_BAR, "read 32bit integer from PCIe BAR");
#endif
	return CR_OK;
}


/*!
 ********************************************************************************
 *	@brief      high level PCIe API to terminate PCIe operation
 *
 *	@param			None
 *
 *  @return			If no error happens, 0 is returned
 *
 *******************************************************************************/
int HAL_PCIE_Terminate(void)
{
	HAL_PCIE_CloseDevice();
	return CR_OK;
}


#ifdef DEBUG_BUILD
static BOOL TT_CLI_Write_PCIe_INT32_BAR( CLI_Parse_t *pars_p, char *result_sym_p )
{
	long	bar;
	long	offset;	
	long	value;	
	I32 err;

	TOOL_CLI_GetInteger( pars_p, 0, &bar );
	TOOL_CLI_GetInteger( pars_p, 0, &offset );
	TOOL_CLI_GetInteger( pars_p, 0, &value );	

	err= HAL_PCIE_WriteMem_INT32((int)bar, (U32)offset, (I32)value);

	LOCAL_DEBUG(("%s: bar%d offset=0x%x result=0x%x\n", __func__, bar, offset, err));

	return FALSE;
}

static BOOL TT_CLI_Read_PCIe_INT32_BAR( CLI_Parse_t *pars_p, char *result_sym_p )
{
	long	bar;
	long	offset;	
	I32 value;

	TOOL_CLI_GetInteger( pars_p, 0, &bar );
	TOOL_CLI_GetInteger( pars_p, 0, &offset );

	value= HAL_PCIE_ReadMem_INT32((int)bar, (U32)offset);

	LOCAL_DEBUG(("%s: bar%d offset=0x%x value=0x%x\n", __func__, bar, offset, value));

	return FALSE;
}

#endif


#if defined (__cplusplus)
}
#endif





