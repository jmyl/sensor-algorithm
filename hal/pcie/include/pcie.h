#if !defined(_PCIE_H_)
#define		 _PCIE_H_


#if defined (__cplusplus)
extern "C" {
#endif

#include "vulkan_pcie_map.h"

#define DRAM0_IDX		0
#define DRAM1_IDX		1

typedef struct PCIE_MEM_T_ {
	CR_BOOL	init_done;
	int fd_bar0;
	int fd_bar2;
	int fd_bar4;
	int fd_device;
	
	unsigned char *bar0_addr;
	unsigned char *bar2_addr;	
	unsigned char *bar4_addr;	

	/* array 0 =  DRAM0, Original image buffer address / array 1 = DRAM1, Resized image buffer address */
	U64 	dram_start_addr[2][MAXCAMCOUNT];		
	U64 	dram_size[2][MAXCAMCOUNT];		
	U64 	dram_stride[2][MAXCAMCOUNT];					
} PCIE_Mem_T;


/**** CAM Run State for Eyexo2 ****/
enum {
	PCIE_STATE_NONE = 0,
	PCIE_STATE_CREATE,			
	PCIE_STATE_INIT,		
	PCIE_STATE_STOP,
	PCIE_STATE_START,		
	PCIE_STATE_SEARCH,
	PCIE_STATE_READY,
	PCIE_STATE_LAUNCH,
	PCIE_STATE_AUTO_LAUNCH,	
};


typedef struct HAL_PCIE_Context_T_{
	I32				status[MAXCAMCOUNT];	
	HAND 			hMutex;
	void			*hParent[MAXCAMCOUNT];
	U32 			current_frame_num[MAXCAMCOUNT];	
	U32 			max_frame_num[2][MAXCAMCOUNT];	
	U32 			frame_size[2][MAXCAMCOUNT];	
	CR_BOOL 		bulk_mode;
	CR_video_fnCB imageCallbackFunc[MAXCAMCOUNT];
	PCIE_Mem_T 	*pcie_mem;
} HAL_PCIE_Context_T;


typedef struct PCIeImageHeader_T_
{
	/* Metadata */
	U32 sync_code;
	U32 frame_num;
	U32 ts_low;				// TimeStamp Low 32bit
	U32 ts_high;				// TimeStamp High 32bit

	U16 offset_y;
	U16 offset_x;	
	U16 size_y;
	U16 size_x;

	U32 reserved1[2];

	/* Circle info */

	/* Deprecated - old params */
	U32	normalbulk;	// 0: NONE, 1: NORMAL, 2: BULK
	U32 skip;		// skip count   		추가 필요.
	U32 multitude;	// multitude count	추가 필요.

	U08 padding[PCIE_IMG_HEADER_SIZE]; // TODO: for 256 bytes alignment
} PCIeImageHeader_T;


#define PCIE_COMM_TASK_PRIORITY 				MAX_USER_PRIORITY
#define USE_SYSCALL
 

// TODO: adjust value or remove unused one
#define IMAGE_MAX_WIDTH	1280
#define IMAGE_MAX_HEIGHT	1024

#define IMAGE_MAX_FPS	10000					
#define IMAGE_MIN_FPS	1

#define IMAGE_MAX_GAIN		(1000 * 1000)	
#define IMAGE_MIN_GAIN		(1          )	


#define PCIE_DEVICE_BAR_0	"/sys/bus/pci/devices/0000:01:00.0/resource0"
#define PCIE_DEVICE_BAR_2	"/sys/bus/pci/devices/0000:01:00.0/resource2"
#define PCIE_DEVICE_BAR_4	"/sys/bus/pci/devices/0000:01:00.0/resource4"


/* pcie_drv */

int HAL_PCIE_Init(void);
int HAL_PCIE_Terminate(void);

HAND HAL_PCIE_Start(HAND pP[], U32 camcount, U32 runmode, U32 stchannelmode);
I32 HAL_PCIE_Stop(HAND hand);

I32 HAL_PCIE_StartThread(void *pcie_context);
I32 HAL_PCIE_StopThread(void);

I32 HAL_PCIE_InitCam(HAND hand, int cam_id, CR_BOOL zrot);
I32 HAL_PCIE_SetRunState(HAND hand, CAM_Run_State_T state);

I32 HAL_PCIE_StartCam(HAND hand, int cam_id);
I32 HAL_PCIE_StopCam(HAND hand, int cam_id);

I32 HAL_PCIE_SetCallBackFunc(HAND hand, int cam_id, CR_video_fnCB fnCB);

I32 HAL_PCIE_RequestBulkImage(HAND hand, int cam_id,  U32 ts_h, U32 ts_l, U32 framecount, U32 waittime, U32 continueafterbulk, 
	U32 multitude4Bulk, U32 skip4bulk
	, U32 width4bulk, U32 height4bulk, U32 offset_x4bulk, U32 offset_y4bulk, U32 tsshot_h, U32 tsshot_l,I32 axx1024, I32 bxx1024, I32 ayx1024, I32 byx1024);

I32 HAL_PCIE_UpdateImageParamAll(HAND hand, int cam_id, CAM_IMG_ATTR_T * pImgAttr);
I32 HAL_PCIE_UpdateImageParam(HAND hand, int cam_id, U32 type, I32 value)	;
U64 HAL_PCIE_GetTimeStamp(HAND hand);
I32 HAL_PCIE_SetLaunchDetectionCaptureTime(HAND hand, int capture_time1, int capture_time2) ;
I32 HAL_PCIE_SetSyncUpdate(HAND hand, int cam_id, U32 updateme); // 0: NO update, 1: yes.. update me
I32 HAL_PCIE_ReadInfo(HAND hand, int cam_id, U32 type, U32 *rdata, U32 count);
I32 HAL_PCIE_WriteInfo(HAND hand, int cam_id, U32 type, U32 *wdata, U32 count); 
int HAL_PCIE_SetHeartBeatTimeout(HAND hand, int cam_id,	U32 heartbeattimeout);



/* pcie_mem */
I32 HAL_PCIE_OpenDevice(void);
I32 HAL_PCIE_CloseDevice(void);
void HAL_PCIE_AddMemInfo2Context(HAL_PCIE_Context_T *pcie_context);
CR_BOOL HAL_PCIE_WaitFrame(U32 timeout);
I32 HAL_PCIE_WriteMem_INT32(int bar, U32 offset, I32 value);
I32 HAL_PCIE_WriteMem_INT64(int bar, U32 offset, I64 value);
I32 HAL_PCIE_WriteMem_Buffer(int bar, U32 offset, U08 *buf, U32 size);
I32 HAL_PCIE_ReadMem_INT32(int bar, U32 offset);
I64 HAL_PCIE_ReadMem_INT64(int bar, U32 offset);
I32 HAL_PCIE_ReadMem_Buffer(int bar, U32 offset, U08 *buf, U32 size);


#if defined (__cplusplus)
}
#endif

#endif

