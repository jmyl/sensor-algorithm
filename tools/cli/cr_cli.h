

#ifndef _CLI__H_
#define _CLI__H_

#if defined (__cplusplus)
extern "C" {
#endif

#define SPACE_CHAR          0x20
#define TAB_CHAR            0x09
#define ESCAPE_CHAR         '\\'
#define NL_CHAR             '\n'
#define CR_CHAR             '\r'
#define COMMENT_CHAR        ';' 
#define BAD_INTEGER_VAL     (-1)

#define NO_CONST            0
#define DEFINE_CONST        1
#define IF_CONST            2
#define ELSE_CONST          3
#define WHILE_CONST         4
#define FOR_CONST           5

#define MAX_LINE_LEN        255
#define NO_TOKEN            0

#define NO_SYMBOL           0
#define INT_SYMBOL          1       // integer symbol 
#define FLT_SYMBOL          2       // floating point symbol 
#define STR_SYMBOL          4       // string symbol 
#define COM_SYMBOL          8       // command symbol 
#define MAC_SYMBOL          16      // macro symbol 
#define ANY_SYMBOL          0xff    // matches all symbol types 
#define MAX_SYMBOL_CNT      1000
#define MAX_TOK_LEN 		32


typedef struct
{
  char    *line_p;   /* string under examination                            */
  short   par_pos;   /* index of current position, at delimiter or EOL      */
  short   par_sta;   /* index of start position for last operation          */
  short   tok_len;   /* length of identified token                          */
  char    tok_del;   /* delimiter of current token (not part of token)      */
  char    token[MAX_TOK_LEN];  /* actual token found, in upper case         */
}CLI_Parse_t;

typedef struct mwMacro_s
{
    struct mwMacro_s   	*line_p;
    char        		line[MAX_LINE_LEN];
}CLI_Macro_t;

typedef struct
{
    char     		*name_p;               	// symbol id 
    short    		type;                  	// type of symbol 
    short    		name_len;              	// length of symbol name 

    union    
    {
        long    	int_val;
        double  	flt_val;
        char    	*str_val;
        BOOL 	(*com_val)(CLI_Parse_t*, char*);
        CLI_Macro_t 	*mac_val;
    } value;                   			// value of symbol 
    
    BOOL  		fixed;                 	// flag for symbol 
    short    		depth;                 	// nesting depth at which declaration made 
    char     		*info_p;               	// informational string 
}CLI_SymTable_t;


int 	TOOL_CLI_Proc( void );

int 	TOOL_CLI_Command( char *token_p, BOOL (*action)(CLI_Parse_t*, char*), char *help_p );
int 	TOOL_CLI_GetInteger( CLI_Parse_t *pars_p, long def_int, long *result_p );
int 	TOOL_CLI_GetFloat( CLI_Parse_t *pars_p, double *result_p );
int		TOOL_CLI_GetString( CLI_Parse_t *pars_p, char *default_p, char *result_p, short max_len );

#if defined (__cplusplus)
}
#endif

#endif /* _CLI__H_ */


