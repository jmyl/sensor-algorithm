/*!
 *******************************************************************************
	 @file   cli_main.cpp
	 @brief  command line interface testtool
 *******************************************************************************/


/********************************************************************
 Header Files
 ********************************************************************/
#include <string.h> 
#include <ctype.h> 
#include "cr_common.h"
#include "cr_osapi.h"
#include "cr_cli.h"


#if defined (__cplusplus)
extern "C" {
#endif


/********************************************************************
 Definitions
 ********************************************************************/
#define	MAX_CMD_LEN						100

/********************************************************************
 Local Variables
 ********************************************************************/
static BOOL			pwd_command		= FALSE;
#ifdef DEBUG_BUILD
static char				*ToolString		= "Testtool>";
#endif
static CLI_SymTable_t		*My_Symbol[MAX_SYMBOL_CNT + 1];
static CLI_SymTable_t		**symtab_p		= My_Symbol; 
static long				max_symbol_cnt	= MAX_SYMBOL_CNT; 
static long				symbol_cnt		= 0;				  
static short			macro_depth		= 0;				 
static short			number_base		= 16;   			 
static char				delim_set[]		= " ,\\\t"; 	   
static BOOL			cli_init_flag	= TRUE;
static short			prev_base;

/********************************************************************
 Local Functions
 ********************************************************************/
static BOOL	command_execute( char *input_line );
static void free_resources( void );
static long		conv_int( char *token_p, short default_base );
static BOOL	conv_flt( char *token_p, double *value_p );
static void		init_pars( CLI_Parse_t *pars_p, char *new_line_p );
static short	get_tok( CLI_Parse_t *pars_p, char *delim_p );
static CLI_SymTable_t *look_for( char *token_p, short type );
static CLI_SymTable_t *insert_symbol( char *token_p, short type );
static BOOL	Evaluate_Integer( char *token_p, long *value_p, short default_base );
static BOOL	Evaluate_Float( char *token_p, double *value_p );
static BOOL	Evaluate_String( char *token_p, char *string_p, short max_len );
static BOOL	Evaluate_Integer_Expr( CLI_Parse_t *pars_p, long *value_p, short default_base );
static BOOL	Evaluate_Float_Expr( CLI_Parse_t *pars_p, double *value_p );
static BOOL 	Do_help( CLI_Parse_t *pars_p, char *result_sym_p );
static BOOL 	Do_BasePrev( CLI_Parse_t *pars_p, char *result_sym_p );
static BOOL 	Do_Base10( CLI_Parse_t *pars_p, char *result_sym_p );
static BOOL 	Do_Base16( CLI_Parse_t *pars_p, char *result_sym_p );




/*!
 ********************************************************************************
 *	@brief      Command Line Interface Process
 *
 *	@param  		None
 *				
 *  @return		SUCCESS: 0
 *
 *******************************************************************************/
int TOOL_CLI_Proc( void )
{	
#ifdef DEBUG_BUILD
	unsigned char 	ch;
	char	buf[2];
	int		cnt = 0, i = 0;
	char	cmd_buf[MAX_CMD_LEN + 1]		= " ";
	char	old_cmd_buf[MAX_CMD_LEN + 1]	= " ";
	char	pwd[4]						= "0705";
	unsigned int	num;

	buf[0] = buf[1] = 0;

#ifdef USE_PWD_TO_START_CLI
	while ( 1 )
	{
		if ( (ch = getchar()) == EOF )
		{
			cr_sleep( 1000 );
			continue;
		}

		if ( ch == pwd[i] )
		{
			i++;
			if ( i == 4 )
			{
				i = 0;
				pwd_command = TRUE;
				break;
			}
		}		
		else
		{
			i = 0;
		}
	}	
#endif

    TOOL_CLI_Command("HELP",    Do_help, "help");
    TOOL_CLI_Command("BASEPREV",Do_BasePrev, "Base Base Prev");
    TOOL_CLI_Command("BASE10",  Do_Base10, "Base 10");  
    TOOL_CLI_Command("BASE16",  Do_Base16, "Base 16");  

	printf( "\nWelcome to Creatz CLI tool V1.00\n" );

	while ( 1 )
	{

		printf( ToolString );
		cnt = 0;

		do
		{
			ch = 0;
			if ((ch = getchar()) == EOF )
			{
				cr_sleep( 1000 );
				continue;
			}

			buf[0] = ( char ) ch;
			if ( isprint( buf[0] ) )
			{
				cmd_buf[cnt] = buf[0];
				printf( buf );
				if ( cnt++ >= MAX_CMD_LEN )
				{
					cmd_buf[0] = '\0';
					cnt = 0;
					printf( "\nTool>" );
				}
			}
			else
			{
				if ( ( buf[0] == '\n' ) || ( buf[0] == '\r' ) )
				{
					cmd_buf[cnt] = '\0';
					printf( "\n" );
					strcpy( old_cmd_buf, cmd_buf );
					break;
				}
				else if ( buf[0] == '\b' )
				{
					if ( cnt > 0 )
					{
						printf( "\b \b" );
						cnt--;
					}
				}
				else if ( buf[0] == 0x1b )  // ESC Key
				{
					printf( ToolString );
					strcpy( cmd_buf, old_cmd_buf );
					cnt = strlen( cmd_buf );
					printf( cmd_buf );
				}
			}	/* control key Inputed */
		}
		while ( 1 );

		if ( cnt != 0 )
		{
			if(strcmp(cmd_buf,"quit") == 0) {
				printf("Quit CLI\n");
				break;
			}
			command_execute( cmd_buf );
		}
	}
	free_resources();
#endif	
	return 0;
}

/*!
 ********************************************************************************
 *	@brief      install new CLI command
 *
 *	@param[in]  		token_p
 *						command string	
 *	@param[in]  		action
 *						test function
 *	@param[in]  		help_p
 *				 		message explaining the CLI command(to be displayed for "help" command)
 *
 *  @return		SUCCESS: TRUE
 *
 *******************************************************************************/
int TOOL_CLI_Command( char *token_p, BOOL( *action )( CLI_Parse_t*, char * ), char *help_p )
{
	CLI_SymTable_t*symbol_p;
	BOOL	error	= FALSE;

	if ( cli_init_flag == FALSE )
	{
		return FALSE;
	}

	symbol_p = look_for( token_p, ( COM_SYMBOL | MAC_SYMBOL ) );
	if ( symbol_p != ( CLI_SymTable_t * ) NULL )
	{
		error = TRUE;
#ifdef DEBUG_BUILD
		printf( "Name clash when registering command \"%s\"\n", token_p );
#endif
	}
	else
	{
		symbol_p = insert_symbol( token_p, COM_SYMBOL );

		if ( symbol_p == ( CLI_SymTable_t * ) NULL )
		{
			error = TRUE;
		}
		else
		{
			symbol_p->fixed = TRUE;
			symbol_p->info_p = help_p;
			symbol_p->value.com_val = action;
		}
	}

	return( error );
}


/*!
 ********************************************************************************
 *	@brief      Get integer input in CLI console
 *
 *	@param[in]  		pars_p
 *						CLI_Parse_t pointer	
 *	@param[in]  		def_int
 *						default value when there is no input
 *	@param[out]  		result_p
 *						output value 
 *				
 *  @return		SUCCESS: TRUE
 *
 *******************************************************************************/
int TOOL_CLI_GetInteger( CLI_Parse_t *pars_p, long def_int, long *result_p )
{
	BOOL	error	= FALSE;

	get_tok( pars_p, delim_set );
	if ( pars_p->tok_len == 0 )
	{
		*result_p = def_int;
	}
	else
	{
		error = Evaluate_Integer( pars_p->token, result_p, number_base );
		if ( error )
		{
			*result_p = def_int;
		}
	}
	return( error );
}

/*!
 ********************************************************************************
 *	@brief      Get float input in CLI console
 *
 *	@param[in]  		pars_p
 *						CLI_Parse_t pointer	
 *	@param[out]  		result_p
 *						output value 
 *				
 *  @return		SUCCESS: TRUE
 *
 *******************************************************************************/
int TOOL_CLI_GetFloat( CLI_Parse_t *pars_p, double *result_p )
{
	BOOL	error	= FALSE;

	get_tok( pars_p, delim_set );
	if ( pars_p->tok_len == 0 )
	{
		*result_p = 0;
	}
	else
	{
		error = Evaluate_Float( pars_p->token, result_p );
		if ( error )
		{
			*result_p = 0;
		}
	}
	return( error );
}


/*!
 ********************************************************************************
 *	@brief      Get string input in CLI console
 *
 *	@param[in]  		pars_p
 *						CLI_Parse_t pointer	
 *	@param[in]  		default_p
 *						default string when there is no input
 *	@param[out]  		result_p
 *						output value 
 *	@param[in]  		max_len
 *						max string size 
 *				
 *  @return		SUCCESS: TRUE
 *
 *******************************************************************************/
int TOOL_CLI_GetString( CLI_Parse_t *pars_p, char *default_p, char *result_p, short max_len )
{
	BOOL	error	= FALSE;

	get_tok( pars_p, delim_set );
	if ( pars_p->tok_len == 0 )
	{
		strncpy( result_p, default_p, max_len );
	}
	else
	{
		error = Evaluate_String( pars_p->token, result_p, max_len );
		if ( error )
		{
			strncpy( result_p, default_p, max_len );
		}
	}
	return ( FALSE );
}


/******************************************************************************
	ROUTINE 	: Local Functions
	DESCRIPTION : 
*******************************************************************************/

static void free_resources( void )
{	
	int i;
	
	for(i=0; i<symbol_cnt ; i++)
	{
		if(symtab_p[i] != NULL) 
		{
			if(symtab_p[i]->name_p != NULL) 
			{
				cr_free(symtab_p[i]->name_p);
				symtab_p[i]->name_p = NULL;
			}
			cr_free(symtab_p[i]);
			symtab_p[i] = NULL;
		}
	}
	
	symbol_cnt = 0;	
}

static BOOL is_delim( char character, char *delim_p )
{
	short	delim_count	= 0;

	while ( ( delim_p[delim_count] != '\0' ) && ( character != delim_p[delim_count] ) )
	{
		delim_count++;
	}
	return ( ( character == delim_p[delim_count] ) || ( character == NL_CHAR ) || ( character == CR_CHAR ) || ( character == '\0' ) );
}

static BOOL is_matched( char *tested_p, char *definition_p, short minlen )
{
	BOOL	match	= TRUE;
	short	cnt		= 0;

	while ( ( ( tested_p[cnt] == definition_p[cnt] ) || ( ( tested_p[cnt] & 0xdf ) == ( definition_p[cnt] & 0xdf ) ) ) && ( tested_p[cnt] != '\0' ) && ( definition_p[cnt] != '\0' ) )
	{
		cnt++;
	}

	if ( ( tested_p[cnt] != '\0' ) || ( ( cnt < minlen ) && ( definition_p[cnt] != '\0' ) ) )
	{
		match = FALSE;
	}
	return( match );
}

long conv_int( char *token_p, short default_base )
{
	long		value;
	short		base, cnt;
	BOOL		negative;
	static  char*conv	= "0123456789ABCDEF";

	negative = FALSE;
	if ( token_p[0] == '#' )
	{
		token_p++;
		base = 16;
	}
	else if ( token_p[0] == '$' )
	{
		token_p++;
		base = 2;
	}
	else if ( token_p[0] == 'o' )
	{
		token_p++;
		base = 8;
	}
	else if ( token_p[0] == 'O' )
	{
		token_p++;
		base = 8;
	}
	else if ( token_p[0] == '-' )
	{
		token_p++;
		negative = TRUE;
		base = 10;
	}
	else if ( token_p[0] == '+' )
	{
		token_p++;
		base = 10;
	}
	else
	{
		base = default_base;
	}

	/* convert by comparison to ordered string array of numeric characters */

	value = 0;
	cnt = 0;
	while ( ( token_p[cnt] != '\0' ) && ( value > BAD_INTEGER_VAL ) )
	{
		short	i	= 0;
		while ( ( conv[i] != ( char ) toupper( token_p[cnt] ) ) && ( i < base ) )
		{
			i++;
		}
		if ( ( i >= base ) || ( value >= ( long ) 0x7fffffffffffffff ) /*||( ( value >= 214748364 ) && ( i >= 8 ) )*/ )
		{
			value = BAD_INTEGER_VAL;
		}
		else
		{
			value = ( value * base ) + i;
		}
		cnt++;
	}
	if ( negative && ( value > BAD_INTEGER_VAL ) )
	{
		value = -value;
	}

	return( value );
}

static CLI_SymTable_t *look_for( char *token_p, short type )
{
	short	cnt;
	BOOL	found	= FALSE;
	CLI_SymTable_t*symbol_p;
	CLI_SymTable_t*shortest_p;
	short	short_len;

	short_len = MAX_TOK_LEN;
	shortest_p = ( CLI_SymTable_t * ) NULL;

	/* point to last symbol in the table */
	if ( symbol_cnt != 0 )
	{
		cnt = 1;
		while ( cnt <= symbol_cnt )
		{
			symbol_p = symtab_p[symbol_cnt - cnt];
			/* protect against deleted symbols */
			if ( symbol_p->name_p != NULL )
			{
				/* look for a name match of at least two characters and
				   shortest matching definition of search type */
				found = ( ( is_matched( token_p, symbol_p->name_p, 2 ) && ( ( symbol_p->type & type ) > 0 ) ) );
				if ( found && ( symbol_p->name_len < short_len ) )
				{
					shortest_p = symbol_p;
					short_len = symbol_p->name_len;
				}
			}
			cnt++;
		}
	}
	return( shortest_p );
}

static void init_pars( CLI_Parse_t *pars_p, char *new_line_p )
{
	pars_p->line_p = new_line_p;
	pars_p->par_pos = 0;
	pars_p->par_sta = 0;
	pars_p->tok_del = '\0';
	pars_p->token[0] = '\0';
	pars_p->tok_len = 0;
}

static short get_tok( CLI_Parse_t *pars_p, char *delim_p )
{
	short	par_sta	= pars_p->par_pos;
	short	par_pos	= par_sta;
	short	tok_len	= 0;
	short	quotes	= 0;

	/* check that we are not already at the end of a line due to
	   a previous call (or a null input) - if so return a null token
		End of line now includes finding comment character! */
	if ( ( pars_p->line_p[par_pos] == '\0' ) || ( pars_p->line_p[par_pos] == COMMENT_CHAR ) )
	{
		pars_p->token[0] = '\0';
		pars_p->tok_del = '\0';
	}
	else
	{
		/* attempt to find start of a token, noting special case of first call,
		   incrementing past last delimiter and checking for end of line
		   on the way */
		if ( par_pos != 0 )
		{
			par_pos++;
		}
		while ( ( ( pars_p->line_p[par_pos] == SPACE_CHAR ) || ( pars_p->line_p[par_pos] == TAB_CHAR ) ) && ( pars_p->line_p[par_pos] != '\0' ) && ( pars_p->line_p[par_pos] != COMMENT_CHAR ) )
		{
			par_pos++;
		}
		/* if we find a delimiter before anything else, return a null token
		   also deal with special case of a comment character ending a line */
		if ( is_delim( pars_p->line_p[par_pos], delim_p ) || ( pars_p->line_p[par_pos] == COMMENT_CHAR ) )
		{
			pars_p->token[0] = '\0';
			if ( pars_p->line_p[par_pos] != COMMENT_CHAR )
			{
				pars_p->tok_del = pars_p->line_p[par_pos];
			}
			else
			{
				pars_p->tok_del = '\0';
			}
		}
		else
		{
			/* copy token from line into token string
			   until next delimiter found. Note that delimiters found within
			   pairs of double quotes will not be considered significant. Quotes
			   can be embedded within strings using '\' however. Note also that
			   we have to copy the '\' escape char where it is used, it can
			   be taken out when the string is evaluated */
			while ( ( !is_delim( pars_p->line_p[par_pos], delim_p ) || ( quotes > 0 ) ) && ( tok_len < MAX_TOK_LEN ) && ( pars_p->line_p[par_pos] != '\0' ) )
			{
				pars_p->token[tok_len] = pars_p->line_p[par_pos++];
				if ( ( pars_p->token[tok_len] == '"' ) && ( tok_len == 0 ) )
				{
					quotes++;
				}
				if ( ( pars_p->token[tok_len] == '"' ) && ( tok_len > 0 ) )
				{
					if ( pars_p->token[tok_len - 1] != ESCAPE_CHAR )
					{
						if ( quotes > 0 )
						{
							quotes--;
						}
						else
						{
							quotes++;
						}
					}
				}
				tok_len++;
			}
			/* if we ran out of token space before copy ended, move up to delimiter */
			while ( !is_delim( pars_p->line_p[par_pos], delim_p ) )
			{
				par_pos++;
			}
			/* tidy up the rest of the data */
			pars_p->tok_del = pars_p->line_p[par_pos];
			pars_p->token[tok_len] = '\0';
		}
	}
	pars_p->par_pos = par_pos;
	pars_p->par_sta = par_sta;
	pars_p->tok_len = tok_len;
	return( tok_len );
}

static CLI_SymTable_t * insert_symbol( char *token_p, short type )
{
	CLI_SymTable_t*symbol_p;
	CLI_SymTable_t*oldsym_p;
	short	cnt;
	BOOL	valid;

	/* check that symbol table has a spare slot and issue a warning if close */
	symbol_p = ( CLI_SymTable_t * ) NULL;
	if ( symbol_cnt >= ( max_symbol_cnt - 1 ) )
	{
#ifdef DEBUG_BUILD
		printf( "Cannot insert \"%s\" in symbol table - table is full!\n", token_p );
#endif
	}
	else
	{
		valid = TRUE;
		cnt = 0;
		while ( token_p[cnt] != '\0' )
		{
			if ( ( ( token_p[cnt] < 'A' ) || ( token_p[cnt] > 'Z' ) ) && ( ( token_p[cnt] < 'a' ) || ( token_p[cnt] > 'z' ) ) && ( ( token_p[cnt] < '0' ) || ( token_p[cnt] > '9' ) || ( cnt == 0 ) ) && ( ( token_p[cnt] != '_' ) || ( cnt == 0 ) ) )
			{
				valid = FALSE;
			}
			cnt++;
		}
		if ( !valid )
		{
#ifdef DEBUG_BUILD
			printf( "Cannot insert \"%s\" in symbol table - invalid symbol name\n", token_p );
#endif
		}
		else
		{
			/* carry on with insertion process, checking for scoped name clashes*/
			/* look for a symbol of the same type and matching name. This can
			   be ok if it was declared in a macro level less than the current one*/
			oldsym_p = look_for( token_p, ANY_SYMBOL );
			if ( ( oldsym_p != ( CLI_SymTable_t * ) NULL ) && ( oldsym_p->depth >= macro_depth ) )
			{
#ifdef DEBUG_BUILD
				printf( "Cannot insert \"%s\" in symbol table - name clash within current scope\n", token_p );
#endif
			}
			else
			{
				symbol_p = ( CLI_SymTable_t * ) cr_malloc( sizeof( CLI_SymTable_t ) );
				symbol_p->name_p = cr_malloc( MAX_TOK_LEN );
				if ( ( symbol_p == NULL ) || ( symbol_p->name_p == NULL ) )
				{
#ifdef DEBUG_BUILD
					printf( "Cannot insert \"%s\" in symbol table - no memory available\n", token_p );
#endif
					symbol_p = NULL;
				}
				else
				{
					strcpy( symbol_p->name_p, token_p );
					symbol_p->name_len = strlen( token_p );
					symbol_p->type = type;
					symbol_p->depth = macro_depth;
					/* insert new structure in table and warn if near full */
					symtab_p[symbol_cnt] = symbol_p;
					symbol_cnt++;
					if ( symbol_cnt >= ( max_symbol_cnt - 10 ) )
					{
#ifdef DEBUG_BUILD
						printf( "Warning: Symbol table nearly full - (%ld of %ld entries)\n", symbol_cnt, max_symbol_cnt );
#endif
					}
				}
			}
		}
	}
	return( symbol_p );
}

static BOOL Evaluate_Integer( char *token_p, long *value_p, short default_base )
{
	CLI_Parse_t	pars;
	CLI_SymTable_t	*symbol_p;
	BOOL		error	= FALSE;

	/* eliminate null strings and then look for unary operators */
	if ( token_p[0] == '\0' )
	{
		error = TRUE;
	}
	else if ( token_p[0] == '-' )
	{
		/* unary negative */
		token_p++;
		error = Evaluate_Integer( token_p, value_p, default_base );
		if ( !error )
		{
			*value_p = -( *value_p );
		}
	}
	else if ( token_p[0] == '~' )
	{
		/* unary bitflip */
		token_p++;
		error = Evaluate_Integer( token_p, value_p, default_base );
		if ( !error )
		{
			*value_p = ~( *value_p );
		}
	}
	else if ( token_p[0] == '!' )
	{
		/* unary NOT */
		token_p++;
		error = Evaluate_Integer( token_p, value_p, default_base );
		if ( !error )
		{
			*value_p = !( *value_p );
		}
	}
	else
	{
		/* Try to convert number or expression*/
		/*   First look for a simple number, then try a symbol reference. If this
		 all fails, the value may be due to an expression, so try and evaluate it*/
		*value_p = conv_int( token_p, default_base );
		if ( *value_p == BAD_INTEGER_VAL )
		{
			symbol_p = look_for( token_p, INT_SYMBOL );
			if ( symbol_p == ( CLI_SymTable_t * ) NULL )
			{
				init_pars( &pars, token_p );
				error = Evaluate_Integer_Expr( &pars, value_p, default_base );
			}
			else
			{
				error = FALSE;
				*value_p = symbol_p->value.int_val;
			}
		}
	}
	return( error );
}

static BOOL Evaluate_Integer_Expr( CLI_Parse_t *pars_p, long *value_p, short default_base )
{
	BOOL			error		= FALSE;
	short			brackets	= 0;
	long			value1, value2;
	short			ptr_index, i;
	char			sub_expr[MAX_TOK_LEN];
	char			operation;
	static   char	*delim		= "*/+-|&^%";   /* recognised arithmetic operators */

	/* pair up a leading bracket */
	if ( pars_p->line_p[0] == '(' )
	{
		brackets = 1;
		ptr_index = 1;
		while ( ( pars_p->line_p[ptr_index] != '\0' ) && ( brackets > 0 ) )
		{
			if ( pars_p->line_p[ptr_index] == ')' )
			{
				brackets--;
			}
			if ( pars_p->line_p[ptr_index] == '(' )
			{
				brackets++;
			}
			ptr_index++;
		}
		if ( brackets != 0 )
		{
			error = TRUE;
		}
		else
		{
			/* copy substring without enclosing brackets and evaluate it.
			if this is ok, then update parsing start position */
			for ( i = 1; i < ptr_index; i++ )
			{
				sub_expr[i - 1] = pars_p->line_p[i];
			}
			sub_expr[ptr_index - 2] = '\0';
			error = Evaluate_Integer( sub_expr, &value1, default_base );
			if ( !error )
			{
				pars_p->par_pos = ptr_index - 1;
			}
		}
	}
	if ( !error )
	{
		/* look for a token and check for significance */
		get_tok( pars_p, delim );
		if ( pars_p->tok_del == '\0' )
		{
			/* no operator seen*/
			if ( pars_p->par_sta > 0 )  	 /* bracket removal code used */
			{
				*value_p = value1;
			}
			else
			{
				error = TRUE;
			}   		 /* not a valid expression! */
		}
		else
		{
			/* have found an operator. If we stripped brackets in the first
			half of this code, then the interesting part of the line is
			now after the operator. If we did not, then evaluate both
			sides of operator */
			operation = pars_p->tok_del;
			if ( pars_p->par_sta == 0 )
			{
				error = Evaluate_Integer( pars_p->token, &value1, default_base );
			}
			if ( !error )
			{
				get_tok( pars_p, "" );   /* all the way to the end of this line */
				error = Evaluate_Integer( pars_p->token, &value2, default_base );
			}
			if ( !error )
			{
				switch ( operation )
				{
					case '+':
						*value_p = value1 + value2; break;
					case '-':
						*value_p = value1 - value2; break;
					case '*':
						*value_p = value1 * value2; break;
					case '/':
						*value_p = value1 / value2; break;
					case '&':
						*value_p = value1 & value2; break;
					case '|':
						*value_p = value1 | value2; break;
					case '^':
						*value_p = value1 ^ value2; break;
					case '%':
						*value_p = value1 % value2; break;
					default:
						error = TRUE; break;
				}
			}
		}
	}
	return( error );
}

static BOOL conv_flt( char *token_p, double *value_p )
{
	short	cnt		= 0;
	BOOL	error	= FALSE;
	BOOL	seen_dp	= FALSE;

	while ( ( token_p[cnt] != '\0' ) && !error )
	{
		/* check for silly characters */
		if ( ( token_p[cnt] != '-' ) && ( token_p[cnt] != '+' ) && ( token_p[cnt] != '.' ) && ( token_p[cnt] != 'E' ) && ( token_p[cnt] != 'e' ) && ( ( token_p[cnt] > '9' ) || ( token_p[cnt] < '0' ) ) )
		{
			error = TRUE;
		}
		/* check for more than one decimal point */
		if ( token_p[cnt] == '.' )
		{
			if ( seen_dp )
			{
				error = TRUE;
			}
			else
			{
				seen_dp = TRUE;
			}
		}
		/* check for sign after decimal point not associated with exponent */
		if ( ( ( token_p[cnt] == '+' ) || ( token_p[cnt] == '-' ) ) && seen_dp )
		{
			if ( ( token_p[cnt - 1] != 'E' ) && ( token_p[cnt - 1] != 'e' ) )
			{
				error = TRUE;
			}
		}
		/* check for sign before a decimal point but not at start of token */
		if ( ( ( token_p[cnt] == '+' ) || ( token_p[cnt] == '-' ) ) && !seen_dp )
		{
			if ( cnt > 0 )
			{
				error = TRUE;
			}
		}
		cnt++;
	}
	if ( error || ( sscanf( token_p, "%lg", value_p ) == 0 ) )
	{
		*value_p = BAD_INTEGER_VAL;
	}
	return( error );
}

static BOOL Evaluate_Float( char *token_p, double *value_p )
{
	CLI_Parse_t	pars;
	CLI_SymTable_t	*symbol_p;
	BOOL		error	= FALSE;

	/* this will only be relevant for non null strings w/o leading white space.
	   First look for a simple number, then try a symbol reference. If this
	   all fails, the value may be due to an expression, so try and evaluate it*/
	/* printf("float evaluation of %s \n",token_p); */
	if ( token_p[0] == '\0' )
	{
		error = TRUE;
	}
	else
	{
		error = conv_flt( token_p, value_p );
		if ( error )
		{
			/* a symbol would be legal if it were an integer, but have to float it */
			symbol_p = look_for( token_p, FLT_SYMBOL | INT_SYMBOL );
			if ( symbol_p == ( CLI_SymTable_t * ) NULL )
			{
				init_pars( &pars, token_p );
				error = Evaluate_Float_Expr( &pars, value_p );
			}
			else
			{
				error = FALSE;
				if ( symbol_p->type == FLT_SYMBOL )
				{
					*value_p = symbol_p->value.flt_val;
				}
				else
				{
					*value_p = ( double ) symbol_p->value.int_val;
				}
			}
		}
	}
	return( error );
}

static BOOL Evaluate_Float_Expr( CLI_Parse_t *pars_p, double *value_p )
{
	BOOL			error		= FALSE;
	short			brackets	= 0;
	double			value1, value2;
	short			ptr_index, i;
	char			sub_expr[MAX_TOK_LEN];
	char			operation;
	static   char	*delim		= "*/+-";   /* recognised arithmetic operators */

	/* pair up a leading bracket */
	if ( pars_p->line_p[0] == '(' )
	{
		brackets = 1;
		ptr_index = 1;
		while ( ( pars_p->line_p[ptr_index] != '\0' ) && ( brackets > 0 ) )
		{
			if ( pars_p->line_p[ptr_index] == ')' )
			{
				brackets--;
			}
			if ( pars_p->line_p[ptr_index] == '(' )
			{
				brackets++;
			}
			ptr_index++;
		}
		if ( brackets != 0 )
		{
			error = TRUE;
		}
		else
		{
			/* copy substring without enclosing brackets and evaluate it.
			   if this is ok, then update parsing start position */
			for ( i = 1; i < ptr_index; i++ )
			{
				sub_expr[i - 1] = pars_p->line_p[i];
			}
			sub_expr[ptr_index - 2] = '\0';
			error = Evaluate_Float( sub_expr, &value1 );
			if ( !error )
			{
				pars_p->par_pos = ptr_index - 1;
			}
		}
	}
	if ( !error )
	{
		/* look for a token and check for significance */
		get_tok( pars_p, delim );
		if ( pars_p->tok_del == '\0' )
		{
			/* no operator seen*/
			if ( pars_p->par_sta > 0 )  	 /* bracket removal code used */
			{
				*value_p = value1;
			}
			else
			{
				error = TRUE;
			}   		 /* not a valid expression! */
		}
		else
		{
			/* have found an operator. If we stripped brackets in the first
			   half of this code, then the interesting part of the line is
			   now after the operator. If we did not, then evaluate both
			   sides of operator */
			operation = pars_p->tok_del;
			if ( pars_p->par_sta == 0 )
			{
				error = Evaluate_Float( pars_p->token, &value1 );
			}
			if ( !error )
			{
				get_tok( pars_p, "" );   /* all the way to the end of this line */
				error = Evaluate_Float( pars_p->token, &value2 );
			}
			if ( !error )
			{
				switch ( operation )
				{
					case '+':
						*value_p = value1 + value2; break;
					case '-':
						*value_p = value1 - value2; break;
					case '*':
						*value_p = value1 * value2; break;
					case '/':
						*value_p = value1 / value2; break;
					default:
						error = TRUE;
				}
			}
		}
	}
	return( error );
}

static BOOL Evaluate_String( char *token_p, char *string_p, short max_len )
{
	CLI_SymTable_t	*symbol_p;
	BOOL		error	= FALSE;
	short		ptr_index, i, len;
	CLI_Parse_t	pars;

	/* this will only be relevant for non null strings w/o leading white space
	   and enclosed in a pair of double quotes '"'. Concatenation of strings
	   is dealt with using recursion*/
	string_p[0] = '\0';
	/* printf("string evaluation of %s \n",token_p); */
	if ( token_p[0] == '\0' )
	{
		error = TRUE;
	}
	else
	{
		/* look for concatenation function */
		init_pars( &pars, token_p );
		get_tok( &pars, "+" );
		if ( pars.tok_del == '+' )
		{
			/* call routine on halves of passed token */
			error = Evaluate_String( pars.token, string_p, max_len );
			if ( !error )
			{
				get_tok( &pars, "" );  /* rest of line */
				len = strlen( string_p );
				error = Evaluate_String( pars.token, &string_p[len], max_len - len );
			}
		}
		else
		{
			/* examine given token for string contents*/
			symbol_p = look_for( token_p, STR_SYMBOL );
			if ( symbol_p != ( CLI_SymTable_t * ) NULL )
			{
				strcpy( string_p, symbol_p->value.str_val );
			}
			else
			{
				/* try to extract quoted character sequence*/
				/* we assume since this is already been passed through the tokeniser that this
				   is a valid string - starting with a quote mark and ending with a quote mark
				   only containing escaped quote marks within its body. All that is needed
				   for evaluation is to strip out the escapes */
				len = strlen( token_p );
				if ( ( token_p[0] == '"' ) && ( token_p[len - 1] == '"' ) )
				{
					/* copy substring without enclosing quotes, preserving case */
					ptr_index = 1;
					i = 0;
					while ( ( token_p[ptr_index] != '\0' ) && ( ptr_index < ( len - 1 ) ) )
					{
						if ( ( token_p[ptr_index + 1] == '"' ) && ( token_p[ptr_index] == ESCAPE_CHAR ) )
						{
							string_p[i++] = token_p[ptr_index + 1];
							ptr_index++;
						}
						else
						{
							string_p[i++] = token_p[ptr_index];
						}
						ptr_index++;
					}
					string_p[i] = '\0';
					len = i;
				}
				else
				{
					error = TRUE;
				}
			}
		}
	}
	return( error );
}


static BOOL execute_command_line( char *line_p, char *target_p, BOOL *result_p )
{
	CLI_SymTable_t	*command_p;
	CLI_Parse_t	pars;
	BOOL		error;

	error = FALSE;
	init_pars( &pars, line_p );
	get_tok( &pars, delim_set );
	command_p = look_for( pars.token, COM_SYMBOL );
	if ( command_p != ( CLI_SymTable_t * ) NULL )
	{
		*result_p = command_p->value.com_val( &pars, target_p );
	}
	else
	{
		error = TRUE;
	}
	return( error );
}

#ifdef DEBUG_BUILD
static BOOL Do_help( CLI_Parse_t *pars_p, char *result_sym_p )
{
	BOOL	error	= FALSE;
	CLI_SymTable_t*symbol_p;
	short	cnt		= 0;
	unsigned char	Highch,Lowch;

	if ( get_tok( pars_p, delim_set ) == 0 )
	{
		/* no items on a line */
		/* display list of commands and macros plus general help*/
		printf( "The following commands and macros are defined:\n\n" );

		Highch = 'A';
		Lowch = 'a';
		for ( ; Highch < ( 'Z' + 1 ); )
		{
			cnt = 0;
			while ( cnt < symbol_cnt )
			{
				symbol_p = symtab_p[cnt++];
				if ( ( symbol_p->name_p[0] != Highch ) && ( symbol_p->name_p[0] != Lowch ) )
				{
					continue;
				}
				switch ( symbol_p->type )
				{
					case COM_SYMBOL:
						printf( "%-12s - %s\n", symbol_p->name_p, symbol_p->info_p );
						break;
					case MAC_SYMBOL:
						printf( "%s ", symbol_p->name_p );
						printf( "(%s) - %s\n", symbol_p->value.mac_val->line, symbol_p->info_p );
						break;
					default:
						break;
				}
			}
			Highch++;
			Lowch++;
		}

		printf( "\n" );
	}
	else
	{
		/* display individual information*/
		symbol_p = look_for( pars_p->token, ( MAC_SYMBOL | COM_SYMBOL ) );
		if ( symbol_p != ( CLI_SymTable_t * ) NULL )
		{
			switch ( symbol_p->type )
			{
				case COM_SYMBOL:
					printf( "%-12s - %s\n", symbol_p->name_p, symbol_p->info_p );
					break;
				case MAC_SYMBOL:
					printf( "%s ", symbol_p->name_p );
					printf( "(%s) - %s\n", symbol_p->value.mac_val->line, symbol_p->info_p );
					break;
				default:
					break;
			}
		}
		else
		{
			printf( "unrecognised command or macro\n" );
			error = TRUE;
		}
	}
	return( error );
}

static BOOL Do_BasePrev( CLI_Parse_t *pars_p, char *result_sym_p )
{
	number_base = prev_base;
	printf( "Numerical Base %d\n", number_base );	
	return FALSE;
}

static BOOL Do_Base10( CLI_Parse_t *pars_p, char *result_sym_p )
{
	prev_base = number_base;
	number_base = 10;
	printf( "Numerical Base %d\n", number_base );		
	return FALSE;
}

static BOOL Do_Base16( CLI_Parse_t *pars_p, char *result_sym_p )
{
	prev_base = number_base;
	number_base = 16;
	printf( "Numerical Base %d\n", number_base );		
	return FALSE;
}
#endif

static BOOL command_execute( char *input_line )
{
	BOOL		not_done	= TRUE;
	CLI_Parse_t	pars;
	static char	cmd_delim[]	= "= \\";
	BOOL		if_flag		= FALSE;
	BOOL		error		= FALSE;
	int			i;

	init_pars( &pars, input_line );
	get_tok( &pars, cmd_delim );
	if ( pars.tok_len <= 0 )
	{
		return FALSE;
	}
	if ( pars.tok_del == ' ' )
	{
		i = pars.par_pos;
		while ( input_line[i] == ' ' )
		{
			i++;
		}
		if ( input_line[i] == '=' )
		{
			pars.tok_del = '=';
			pars.par_pos = i;
		}
	}
	if ( pars.tok_del == '=' )
	{
	}
	/* attempt to execute a command or macro statement */
	else
	{
		if_flag = FALSE;
		not_done = execute_command_line( input_line, "", &error );
		if ( not_done )
		{
#ifdef DEBUG_BUILD
			printf( "Unrecognised command statement\n" );
#endif
			error = TRUE;
		}
	}
	return error;
}

#if defined (__cplusplus)
}
#endif


