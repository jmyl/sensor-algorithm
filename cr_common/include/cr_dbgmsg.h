/*!
 *******************************************************************************
                                                                                
                    CREATZ Debug message.
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_dbgmsg.h
	 @brief  
	 @author YongHo Suk
	 @date   2011/03/14 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_CR_DBGMSG_H_)
#define		 _CR_DBGMSG_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

// definition of debug levels

#define CR_MSG_LEVEL_NONE            0
#define CR_MSG_LEVEL_ERR             1
#define CR_MSG_LEVEL_TOP             2
#define CR_MSG_LEVEL_WARN            3
#define CR_MSG_LEVEL_TG_DEBUG        4
#define CR_MSG_LEVEL_TRACE           5
#define CR_MSG_LEVEL_INFO            6
#define CR_MSG_LEVEL_VERB            7
#define CR_MSG_LEVEL_ALL			0x7FFFFFFF


// definition of debug areas
#define CR_MSG_AREA_GENERAL		(1 << 0)
#define CR_MSG_AREA_GIGEV		(1 << 1)
#define CR_MSG_AREA_ALL            0xFFFFFFFF




#define DBGSTAMP cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_INFO, __FUNCTION__"[%d]: %d\n", __LINE__,	cr_gettickcount())



/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

void cr_trace(unsigned int area, unsigned int level, const char* szFormat, ...);
void cr_trace_enable(void);
void cr_trace_disable(void);
void cr_trace_config(unsigned int area, unsigned int level);

#if defined (__cplusplus)
}
#endif

#endif		// _CR_DBGMSG_H_

