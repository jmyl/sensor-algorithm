/*!
 *******************************************************************************
                                                                                
                   Creatz  Camera 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_chessmatch.h
	 @brief  Chessboard result match.
	 @author YongHo Suk                                 
	 @date   2011/03/12 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_CR_CHESSMATCH_H_)
#define		 _CR_CHESSMATCH_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

void cr_chessboardreorder(double *data, U32 column, U32 row, double *odata);


#if defined (__cplusplus)
}
#endif


#endif		// _CR_CHESSMATCH_H_

