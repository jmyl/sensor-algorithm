/*!
 *******************************************************************************

                   Creatz Matrix Module

     @section copyright_notice COPYRIGHT NOTICE
     Copyright (c) 2021 by Creatz Inc.
     All Rights Reserved. \n
     Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
     @section file_information FILE CREATION INFORMATION
     @file      cr_matrix.h
     @brief     This defines matrix operations
     @author    YongHo Suk, Hyeonseok Choi
     @date      2021/02/15 First Created

     @section checkin_information LATEST CHECK-IN INFORMATION
     $Rev$
     $Author$
     $Date$
 *******************************************************************************/
#if !defined(_CR_MATRIX_H_)
#define		 _CR_MATRIX_H_

/*----------------------------------------------------------------------------
    Description	: defines referenced header files
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
    Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
    Description	: Type definition of structures and data type
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
    Description	: static variable declaration
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
    Description	: external and internal global variable
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*	Description	: declares the dll function prototype
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*	Description	: declares the external function prototype
-----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

double mat3inv(
    double A[3][3], 		// input
    double X[3][3]		// output
);

double mat3det(double A[3][3]);

void mat3scalar(double A[3][3], double s, double B[3][3]);

void mat3trans(
    double A[3][3], 		// input
    double X[3][3]		// output
);

void mat33mult(double ma[3][3], double mb[3][3], double mc[3][3]);
void mat3v3mult(double ma[3][3], double va[3], double vb[3]);


#if defined (__cplusplus)
}
#endif
#endif // _CR_MATRIX_H_