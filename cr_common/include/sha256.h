
#if !defined(_SHA256_H_DSVLJKSVDLKJ)
#define		 _SHA256_H_DSVLJKSVDLKJ

#include "cr_common.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#ifdef _MSC_VER

#include <stdlib.h>
#define rotlFixed(x, n) _rotl((x), (n))
#define rotrFixed(x, n) _rotr((x), (n))

#else

#define rotlFixed(x, n) (((x) << (n)) | ((x) >> (32 - (n))))
#define rotrFixed(x, n) (((x) >> (n)) | ((x) << (32 - (n))))

#endif

#define SHA256_DIGEST_SIZE 32

typedef struct
{
	U32 state[8];
	U32 count;
	U08 buffer[64];
} CSha256;







#define S0(x) (rotrFixed(x, 2) ^ rotrFixed(x,13) ^ rotrFixed(x, 22))
#define S1(x) (rotrFixed(x, 6) ^ rotrFixed(x,11) ^ rotrFixed(x, 25))
#define s0(x) (rotrFixed(x, 7) ^ rotrFixed(x,18) ^ (x >> 3))
#define s1(x) (rotrFixed(x,17) ^ rotrFixed(x,19) ^ (x >> 10))

#define blk0(i) (W[i] = data[i])
#define blk2(i) (W[i&15] += s1(W[(i-2)&15]) + W[(i-7)&15] + s0(W[(i-15)&15]))

#define Ch(x,y,z) (z^(x&(y^z)))
#define Maj(x,y,z) ((x&y)|(z&(x|y)))

#define a(i) T[(0-(i))&7]
#define b(i) T[(1-(i))&7]
#define c(i) T[(2-(i))&7]
#define d(i) T[(3-(i))&7]
#define e(i) T[(4-(i))&7]
#define f(i) T[(5-(i))&7]
#define g(i) T[(6-(i))&7]
#define h(i) T[(7-(i))&7]


#ifdef _SHA256_UNROLL2

#define R(a,b,c,d,e,f,g,h, i) h += S1(e) + Ch(e,f,g) + K[i+j] + (j?blk2(i):blk0(i));\
	d += h; h += S0(a) + Maj(a, b, c)

#define RX_8(i) \
	R(a,b,c,d,e,f,g,h, i); \
	R(h,a,b,c,d,e,f,g, i+1); \
	R(g,h,a,b,c,d,e,f, i+2); \
	R(f,g,h,a,b,c,d,e, i+3); \
	R(e,f,g,h,a,b,c,d, i+4); \
	R(d,e,f,g,h,a,b,c, i+5); \
	R(c,d,e,f,g,h,a,b, i+6); \
	R(b,c,d,e,f,g,h,a, i+7)

#else

#define R(i) h(i) += S1(e(i)) + Ch(e(i),f(i),g(i)) + K[i+j] + (j?blk2(i):blk0(i));\
	d(i) += h(i); h(i) += S0(a(i)) + Maj(a(i), b(i), c(i))

#ifdef _SHA256_UNROLL

#define RX_8(i) R(i+0); R(i+1); R(i+2); R(i+3); R(i+4); R(i+5); R(i+6); R(i+7);

#endif

#endif


/*----------------------------------------------------------------------------
 	Description	: Functions..
-----------------------------------------------------------------------------*/
HAND Sha256_create();
void Sha256_delete(HAND h);

void Sha256_Init(HAND h);
void Sha256_Update(HAND h, const U08 *data, U32 size);
void Sha256_Final(HAND h, U08 *digest);

#endif		// _SHA256_H_DSVLJKSVDLKJ

