#if !defined(ELLIPSECALC_H_)
#define      ELLIPSECALC_H_

#if defined (__cplusplus)
extern "C" {
#endif


//#define MAXDATACOUNT	1024
int ellipsecalc(
		double x[],
		double y[],
		int count,
		double *pmx,
		double *pmy,
		double *prx,
		double *pry,
		double *ptheta);


#if defined (__cplusplus)
}
#endif

#endif

