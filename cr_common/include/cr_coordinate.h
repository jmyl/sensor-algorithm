/*!
 *******************************************************************************
                                                                                
                   Creatz Coordinate  
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_coordinate.h
	 @brief  Coordinate transform 
	 @author YongHo Suk                                 
	 @date   2011/08/09 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_CR_COORDINATE_H_)
#define		 _CR_COORDINATE_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

//#define COORD_MAXPOINT	128
//#define COORD_MAXPOINT	1024
//#define COORD_MAXPOINT	4096
#define COORD_MAXPOINT	(1024*10)

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

typedef struct _point {
	double x;
	double y;
} point_t;

typedef struct _biquad {
	double mat[6*2];
} biquad_t;

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

typedef signed int I32;
typedef unsigned int U32;

I32 cr_bi_quadratic_matrix(
	biquad_t 	*bqmat, 		// bi-quad transform matrix (6x2)
	point_t *frompoints,	// from point data
	point_t *topoints,		// to point data
	U32 count);			// count of point

I32 cr_bi_quadratic(
	biquad_t 	*bqmat, 		// bi-quad transform matrix (6x2)
	point_t *frompoints,	// from point data
	point_t *topoints);		// to point data

#if defined (__cplusplus)
}
#endif
#endif		// _CR_CAM_H_


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#if 0 // not used
typedef struct _bilinear {
    double mat[3*2];
} bilinear_t;


typedef struct _hmg34 {					// 4 to 3
    double mat[3*4];
} hmg34_t;

typedef struct _hmg43 {					// 3 to 4
    double mat[4*3];
} hmg43_t;




I32 cr_bi_linear_matrix(
    bilinear_t 	*bqmat, 		// bi-linear transform matrix (3x2)
    point_t *frompoints,	// from point data
    point_t *topoints,		// to point data
    U32 count);			// count of point

I32 cr_bi_linear(
    bilinear_t 	*bqmat, 		// bi-linear transform matrix (3x2)
    point_t *frompoints,	// from point data
    point_t *topoints);		// to point data

I32 cr_homography_matrix(
    double 	*transmat, 	// transform matrix
    double  *fromv,
    double  *tov,
    U32		dimf,
    U32		dimt,
    U32 count);			// count of point

I32 cr_homography(
    double 	*transmat, 	// transform matrix
    double  *fromv,
    double  *tov,
    U32		dimf,
    U32		dimt);

#endif

