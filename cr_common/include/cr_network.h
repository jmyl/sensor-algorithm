/*!
 *******************************************************************************
                                                                                
                    CREATZ Network API
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_network.h
	 @brief  Network APIs for all modules
	 @author YongHo Suk                                 
	 @date   2015/11/16 First Created
	 			2020/12/24 File name and location Changed

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/

#if !defined(_CRGEV_SOCKET_H_)
#define		 _CRGEV_SOCKET_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>

#if defined(_WIN32)
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <winsock2.h>
#include <windows.h>

#elif defined(__linux__)
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <ifaddrs.h>
#include <string.h> 
#include <linux/if_packet.h>
#include <net/ethernet.h> 
#endif
/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

#if defined(_WIN32)
// Windows Socket version
#define WS_VERSION		2		//!< Winsock version
#define WS_SUBVERSION	2		//!< Winsock subversion
#else
#define INVALID_SOCKET (~0)
#define SOCKET	int
#define SOCKET_ERROR	(~0)
#define SOCKADDR_IN struct sockaddr_in
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif
CR_BOOL cr_InitSocketAPI();
CR_BOOL cr_CloseSocketAPI();
CR_BOOL cr_GetMacAddress(I32 indexAdapter, U16 *pMacHigh, U32 *pMacLow, U32 *pIpAddr, U32 *pSubnetMask);
CR_BOOL cr_SetIPAddress(U32 ipCamera, U32 ipMask);
I32  cr_GetSocketError();
I32 cr_CloseSocket(SOCKET hSocket);
U32  cr_GetBestInterface_str(char *destIpAddr_str);
U32  cr_GetBestInterface(U32 destAddr);

#if defined (__cplusplus)
}
#endif


#endif		// _CRGEV_SOCKET_H_

