/*!
 *******************************************************************************
                                                                                
                   Creatz Camera 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_cam_jpeg.h
	 @brief  JPEG
	 @author YongHo Suk                                 
	 @date   2011/10/31 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_CR_CAM_JPEG_H_)
#define		 _CR_CAM_JPEG_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif
HAND cr_cam_jpeg_create(void);

void cr_cam_jpeg_delete(HAND hjpg);

I32 cr_cam_jpeg_encode(	// 0: success, -1: fail.
		HAND hjpg,
		U08 *jpgbuf,	// Output, Jpeg bitstream
		U32 *jpglen,	// Output, Jpeg bitstream length (0: Fail encoding)
		U08 *ybuf,		// Input, Y, 411 (or 420)
		U08 *ubuf,		// Input, U(Cb)
		U08 *vbuf,		// Input, V(Cr)
		U32 width,		// Image width
		U32 height,		// Image height
		U32 qvalue		// Compression qvalue
		);


U08 *cr_cam_jpeg_Y_encode(	// NULL: Fail, NotNull: Sucess and result buffer ptr
		HAND hjpg,
		U32 *jpglen,	// Output, Jpeg bitstream length (0: Fail encoding)
		U08 *ybuf,		// Input, Y, 411 (or 420)
		U32 width,		// Image width
		U32 height,		// Image height
		U32 qvalue		// Compression qvalue
		); 

I32 cr_cam_jpeg_RGB_encode(	// 0: success, -1: fail.
		HAND hjpg,
		U08 *jpgbuf,	// Output, Jpeg bitstream
		U32 *jpglen,	// Output, Jpeg bitstream length (0: Fail encoding)
		U08 *rgbbuf,	// Input, RGB
		U32 width,		// Image width
		U32 height,		// Image height
		U32 qvalue		// Compression qvalue
		);

I32 cr_cam_jpeg_YUV_encode(	// 0: success, -1: fail.
		HAND hjpg,
		U08 *jpgbuf,	// Output, Jpeg bitstream
		U32 *jpglen,	// Output, Jpeg bitstream length (0: Fail encoding)
		U08 *ybuf,		// Input, Y, 411 (or 420)
		U08 *ubuf,		// Input, U(Cb)
		U08 *vbuf,		// Input, V(Cr)
		U32 width,		// Image width
		U32 height,		// Image height
		U32 qvalue		// Compression qvalue
		);

I32 cr_cam_jpeg_headerparse(
		HAND hjpg,
		U08 *jpgbuf,	// input, Jpeg bitstream
		U32 jpglen,		// input, Jpeg bitstream length (0: Fail encoding)
		U32 *pwidth,	// output, Image width
		U32 *pheight	// output, Image height
		);

I32 cr_cam_jpeg_decompress(
		HAND hjpg,
		U08 *yuvbuf,	// output, YUV buffer
		U08 *jpgbuf,	// input, Jpeg bitstream
		U32 jpglen		// input, Jpeg bitstream length (0: Fail encoding)
		);

#if defined (__cplusplus)
}
#endif


#endif		// _CR_CAM_JPEG_H_

