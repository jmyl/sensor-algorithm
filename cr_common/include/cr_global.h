/*!
 *******************************************************************************
                                                                                
                    Creatz Camera 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 ~ 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_global.h
	 @brief  
	 @author YongHo Suk
	 @date   2011/03/14 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_CR_GLOBAL_H_)
#define		 _CR_GLOBAL_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#if !defined(M_PI)
#define M_PI       3.14159265358979323846
#endif

#define RADIAN2DEGREE(x)	((x) * 180. / M_PI)
#define DEGREE2RADIAN(x)	((x) * M_PI / 180.)

#define RADIAN2RPM(x)		((x) * 60./ (2. * M_PI))
#define RPM2RADIAN(x)		((x) * (2. * M_PI) / 60.)

#define KMH2MS(x)			((x) / 3.6)
#define MS2KMH(x)			((x) * 3.6)


/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/


//----------------------------------------------------------------------------------


#endif		// _CR_GLOBAL_H_

