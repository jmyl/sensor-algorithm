/*!
 *******************************************************************************
                                                                                
                    CREATZ NANO XML 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2014 ~ 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_nanoxml.h
	 @brief  NANO XML. very very very small xml writer
	 @author YongHo Suk                                 
	 @date   2014/11/08 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/

#if !defined(_CR_NANOXML_H_)
#define		 _CR_NANOXML_H_

#include "cr_common.h"

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

#define NANOXML_BUFSIZE		(256*1024)
#define NAMOXML_BUFSIZE_GUARD	(4*1024)
typedef struct _nanoxml {
	FILE *fp;

	U32 encryption;			// 0: Plain text, 1: Encrypted binary
	U32		length;			
	HAND	hrr;

	I32 tabcount;

	U08 buf[NANOXML_BUFSIZE + NAMOXML_BUFSIZE_GUARD];
	U32 len;

} nanoxml_t;

//---
HAND cr_nanoxml_create(char *fn, U32 encryption);
void cr_nanoxml_create2(char *fn, U32 encryption);
void cr_nanoxml_delete(HAND hnx, U32 encryption);
//void cr_nanoxml_insertelement(HAND hnx, U32 encryption, I32 addtab, char *szFormat, ...);
void cr_nanoxml_insertelement(HAND hnx, U32 encryption, I32 addtab, char *szFormat, va_list ap);
void nxmlie(I32 addtab, char *szFormat, ...);
void cr_nanoxml_encryptout(HAND hnx, char *buf, U32 datalen);
void cr_nanoxml_bufinfo(HAND hnx, U08 **pbuf, U32 *pdatalen);



#if defined (__cplusplus)
}
#endif


#endif		// _CR_LOG_H_

