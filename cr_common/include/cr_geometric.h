/*!
 *******************************************************************************
                                                                                
                   Creatz  Camera 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2012 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_geometric.h
	 @brief  Geometric library
	 @author YongHo Suk                                 
	 @date   2012/03/13 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_CR_GEOMETRIC_H_)
#define		 _CR_GEOMETRIC_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include "cr_common.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

#define MIN_GEOMETRIC_VALUE		(1.e-100)
// min float (32bit) : 1.175494351e-38F
// min double (64bit): 2.2250738585072014e-308
// max double (64bit): 1.7976931348623158e+308
#define GEOMETRIC_SMALL_VALUE		(1.e-20)

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

typedef struct _point {
    double x;
    double y;
} point_t;

typedef struct _biquad {
    double mat[6*2];
} biquad_t;



typedef struct _cr_vector {
	union {
		double v[3];		// x0, y0, z0, component element for vector
		double p[3];		// x0, y0, z0, component element for point
		struct {
			double x;
			double y;
			double z;
		};
	};
} cr_vector_t, cr_point_t;

typedef struct _cr_line {
	 cr_point_t		p0;
	 cr_vector_t	m;
} cr_line_t;

typedef struct _cr_plane {
	 cr_point_t		p0;
	 cr_vector_t	n;
} cr_plane_t;

typedef struct _cr_sphere {
	 cr_point_t		p0;				// Center
	 double			r;				// radius
} cr_sphere_t;

typedef struct _cr_rect {
    long    left;
    long    top;
    long    right;
    long    bottom;
} cr_rect_t;

typedef struct _cr_rectL {
    double    sx;
    double    sy;
    double    ex;
    double    ey;
} cr_rectL_t;

#if defined (__cplusplus)
}
#endif

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

// basic functions: vector ---
void 	cr_vector_make(cr_point_t *pp, cr_vector_t *pvo);
void	cr_vector_make2(cr_point_t *pp0, cr_point_t *pp1, cr_vector_t *pvo);
void 	cr_vector_add(cr_vector_t *pv1, cr_vector_t *pv2, cr_vector_t *pvo);
void 	cr_vector_sub(cr_vector_t *pv1, cr_vector_t *pv2, cr_vector_t *pvo);
void 	cr_vector_scalar(cr_vector_t *pv, double s, cr_vector_t *pvo);
double 	cr_vector_norm(cr_vector_t *pv);
double  cr_vector_normalization(cr_vector_t *pv, cr_vector_t *pvn);

double  cr_vector_distance(cr_vector_t *pv1,	cr_vector_t *pv2);
double  cr_vector_angle(cr_vector_t *pv1, cr_vector_t *pv2);

double 	cr_vector_inner(cr_vector_t *pv1, cr_vector_t *pv2);
void 	cr_vector_cross(cr_vector_t *pv1, cr_vector_t *pv2, cr_vector_t *pvo);

// basic functions: line ---
U32 	cr_line_p0p1(cr_point_t *pp0, cr_point_t *pp1, cr_line_t *pl);
void 	cr_line_p0m(cr_point_t *pp0, cr_vector_t *pm, cr_line_t *pl);
U32 	cr_line_v0v1p0(cr_vector_t *pv0, cr_vector_t *pv1, cr_point_t *pp0, cr_line_t  *pl);

// basic functions: plane ---
void 	cr_plane_p0n(cr_point_t *pp0, cr_vector_t *pn, cr_plane_t *pplane);
U32 	cr_plane_p0p1p2(cr_point_t *pp0, cr_point_t *pp1, cr_point_t *pp2, cr_plane_t *pplane);
void    cr_plane_p0line(cr_point_t *pp0, cr_line_t *pline, cr_plane_t *pplane);
void    cr_plane_line0line1(cr_line_t *pline0, cr_line_t *pline1, cr_plane_t *pplane0, cr_plane_t *pplane1);

// rename? : project point to plane
U32 cr_plane_xy2z(cr_plane_t *pplane, double x, double y, double *pz);
U32 cr_plane_yz2x(cr_plane_t *pplane, double y, double z, double *px);
U32 cr_plane_zx2y(cr_plane_t *pplane, double z, double x, double *py);

U32 cr_angle_line_line(cr_line_t  *pl0, cr_line_t  *pl1, double *pangle);
U32 cr_angle_plane_plane(cr_plane_t *pP0, cr_plane_t *pP1, double *pangle);

// distances, and check whether one is on the other or not
U32	    cr_footPerpendicular_Line(cr_point_t *pP, cr_line_t *pL, cr_point_t *pfP);
double  cr_distance_P_Line(cr_point_t *pP, cr_line_t *pL);		// 20190421
double  cr_check_P_on_Line(cr_point_t *pP, cr_line_t *pL);

U32     cr_footPerpendicular_Plane(cr_point_t *pP,	cr_plane_t *ppl, cr_point_t *pfP);
double  cr_distance_P_Plane(cr_point_t *pP, cr_plane_t *ppl);
double  cr_check_P_on_Plane(cr_point_t *pP, cr_plane_t *pPL);

double  cr_check_Line_on_Plane(cr_line_t *pL, cr_plane_t *pPL);

// intersection of two objects
U32 cr_point_line_plane(cr_line_t  *pl, cr_plane_t *pplane, cr_point_t *pp2);
U32 cr_point_line_line(cr_line_t  *pl0, cr_line_t  *pl1, cr_point_t *pp0, cr_point_t *pp1);

U32 cr_line_plane_plane(
		cr_plane_t *ppl0,
		cr_plane_t *ppl1,
		cr_line_t  *plo
		);

// sphere related functions
void	cr_sphere_make(cr_point_t *pp, double r, cr_sphere_t *psp);
U32		cr_line_sphere(cr_line_t *pl, cr_sphere_t *psp, cr_point_t pp[2], U32 *pcount, double *pD);	// Line-sphere interconnection point
U32		cr_line_sphere_mild(cr_line_t *pl, cr_sphere_t *psp, cr_point_t pp[2], U32 *pcount, double *pD);	// Line-sphere interconnection point

// split distort/undistort functions to another file?
U32 cr_intrinsic_normalize(
		point_t *psrc,
		point_t *pdst,
		double cameraParameter[4]		// fx, cx, fy, cy
		);

U32 cr_intrinsic_denormalize(
		point_t *psrc,
		point_t *pdst,
		double cameraParameter[4]		// fx, cx, fy, cy
		);

U32 cr_undistortPoint(
		point_t *psrc,
		point_t *pdst,
		double cameraParameter[4],		// fx, cx, fy, cy
		double distCoeffs[5]			// k1, k2, p1, p2, k3
		);

U32 cr_undistortPoint_normalized (
	point_t *psrc,
	point_t *pdst,
	double distCoeffs[5]			// k1, k2, p1, p2, k3
	);

U32 cr_distortPoint(
		point_t *psrc,
		point_t *pdst,
		double cameraParameter[4],		// fx, cx, fy, cy
		double distCoeffs[5]			// k1, k2, p1, p2, k3
		);

U32 cr_distortPoint_normalized(
		point_t *psrc,
		point_t *pdst,
		double distCoeffs[5]			// k1, k2, p1, p2, k3
		);

U32 cr_undistort_image(
		U08 *pbufDist,		// source
		U08 *pbufUndist,	// Destination
		U32 width,
		U32 height,
		U32 sx, U32 sy,
		U32 ex, U32 ey,
		U32 pixelbyte,
		
		double cameraParameter[4],		// fx, cx, fy, cy
		double distCoeffs[5]			// k1, k2, p1, p2, k3
		);


I32 cr_bi_quadratic_matrix(
    biquad_t 	*bqmat, 		// bi-quad transform matrix (6x2)
    point_t *frompoints,	// from point data
    point_t *topoints,		// to point data
    U32 count);			// count of point

I32 cr_bi_quadratic(
    biquad_t 	*bqmat, 		// bi-quad transform matrix (6x2)
    point_t *frompoints,	// from point data
    point_t *topoints);		// to point data


#if defined (__cplusplus)
}
#endif


#endif		// _CR_GEOMETRIC_H_

