/*!
*******************************************************************************
                                                                                
                Creatz  Camera 
                                                                                
  	@section copyright_notice COPYRIGHT NOTICE
    Copyright (c) 2011 by Creatz Inc. 
    All Rights Reserved. \n
    Do not duplicate without prior written consent of Creatz Inc.

*******************************************************************************
  	@section file_information FILE CREATION INFORMATION
	@file   cr_interpolation.h
	@brief  Computes the evalutation at a given point form sevral known reference values by interpolation
	@author YongHo Suk                                 
	@date   2015/09/10 First Created

	@section checkin_information LATEST CHECK-IN INFORMATION
    $Rev$ 
    $Author$
    $Date$
*******************************************************************************/

#if !defined(_CR_INTERPOLATION_H_)
#define		 _CR_INTERPOLATION_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

#define INTERPOLN	20
#define INTERPOLM	20

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

typedef struct _interpol {
	unsigned int n;					// 0, 1, 2, ..., n   
	double xi[INTERPOLN+1];
	double yi[INTERPOLN+1];
} interpol_t;

typedef struct _interpol2d {
	unsigned int m;					// 0, 1, 2, ..., M   
	double     z[INTERPOLM+1];	// z0, z1, ..., zM
	interpol_t lip[INTERPOLM+1];
} interpol2d_t;


#if defined (__cplusplus)
}
#endif


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

double linearip(double x, interpol_t *plip);
double linearip2d(double x, double z, interpol2d_t *plip2);

#if defined (__cplusplus)
}
#endif


#endif		// _CR_INTERPOLATION_H_

