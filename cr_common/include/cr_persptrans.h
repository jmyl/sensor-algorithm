/*!
 *******************************************************************************
                                                                                
                   Creatz Camera 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2018 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_persptrans.h
	 @brief  Coordinate transform 
	 @author YongHo Suk                                 
	 @date   2018/07/14 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_CR_PERSPTRANS_H_)
#define		 _CR_PERSPTRANS_H_

#include "cr_common.h"

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

I32 cr_getPerspectiveTransform( point_t src[4],	point_t dst[4],	double perspMat[3][3]);
void cr_perspectiveTransform( point_t *psrc, point_t *pdst, double perspMat[3][3]);
void cr_perspectiveTransformInv( point_t *psrc, point_t *pdst, double perspMat[3][3]);
I32 cr_doperspectiveTransform( U08 *inbuf, U08 *outbuf, U32 widh, U32 height, U32 pixelbyte, double perspMat[3][3]);
I32 cr_doperspectiveTransform2( U08 *inbuf, U08 *outbuf, U32 width, U32 height, U32 pixelbyte, double perspMat[3][3]);

#if defined (__cplusplus)
}
#endif

#endif		// _CR_PERSPTRANS_H_

