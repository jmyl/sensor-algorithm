/*!
 *******************************************************************************
                                                                                
                   Creatz Regression Module
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
     Copyright (c) 2011 by Creatz Inc. 
     All Rights Reserved. \n
     Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file      cr_regression.h
	 @brief     Polynomial and circular regressions. Some algorithms such as RANSAC, constrainted regression are also included.
	 @author    YongHo Suk, Hyeonseok Choi
	 @date      2011/08/09 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
     $Rev$ 
     $Author$
     $Date$
 *******************************************************************************/
#if !defined(_CR_REGRESSION_H_)
#define		 _CR_REGRESSION_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

#include "cr_common.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

	
int check_circle(		// 1: success, 0: fail.
		double *x,		// X
		double *y,		// Y
		int points,		// count of pixel
		double *pcx,	// OUTPUT, x of center
		double *pcy,	// OUTPUT, y of center
		double *pcr,	// OUTPUT, radius of circle 
		double *per		// OUTPUT, normalized error
);

int check_circle_woIPP(		// 1: success, 0: fail.
    double *x,		// X
    double *y,		// Y
    int points,		// count of pixel
    double *pcx,	// OUTPUT, x of center
    double *pcy,	// OUTPUT, y of center
    double *pcr,	// OUTPUT, radius of circle 
    double *per		// OUTPUT, normalized error
);

I32 check_circle_RANSAC(		// 1: success, 0: fail.
		double *x,		// X
		double *y,		// Y
		int points,		// count of pixel
		double minr,	// Minimum r, maximum r
		double maxr,

		double *pcx,	// OUTPUT, x of center
		double *pcy,	// OUTPUT, y of center
		double *pcr,	// OUTPUT, radius of circle 
		double *per	// OUTPUT, normalized error
		);

I32 set_check_circle_RANSAC_id(U32 ransac_id);

int make_circle_w3pts(									// 1: success, 0: fail.
    double x[3], double y[3],				// 3 points, input
    double *pcx, double *pcy, double *pr);	// circle, output


// split regression functions to another file?
int cr_regression(
	double x[], double y[], int count,
	double *pm, double *pb);

int cr_regression2(
	double x[], double y[], int count,
	double *pm, double *pb, double *pr2);


int  caw_quadraticregression(
		double x[], double y[], int count,
		double *pa2, double *pa1, double *pa0,
		double *pr2);			// y = a2 x^2 + a1 x + a0


I32 caw_quadraticregression_a2_constraints(
		double x[], double y[], int count,
		double alpha,
		double *pa2, double *pa1, double *pa0,
		double *pr2);


double cr_quadregression_errcalc(double xx[], double yy[], U32 count, double ma[3], double *pmaxerr, U32 *pmaxerrindex);

#if defined (__cplusplus)
}
#endif


#endif		// _CR_REGRESSION_H_

#if 0


// deprecated
int check_circle2(		// 1: success, 0: fail.
    double *x,		// X
    double *y,		// Y
    int points,		// count of pixel
    double *pcx,	// OUTPUT, x of center
    double *pcy,	// OUTPUT, y of center
    double *pcr,	// OUTPUT, radius of circle 
    double *per,		// OUTPUT, normalized error
    double *pm,		// Output, coefficient for y = m x + b.		NOT return, if NULL.
    double *pb		// Output, coefficient for y = m x + b.		NOT return, if NULL.
);

// deprecated
int check_circle3(		// 1: success, 0: fail.
    double *x,		// X
    double *y,		// Y
    int points,		// count of pixel
    double *pcx,	// OUTPUT, x of center
    double *pcy,	// OUTPUT, y of center
    double *pcr,	// OUTPUT, radius of circle 
    double *per,		// OUTPUT, normalized error
    double *pm,		// Output, coefficient for y = m x + b. (after y ratio)		NOT return, if NULL.
    double *pb,		// Output, coefficient for y = m x + b.	(after y ratio)	NOT return, if NULL.
    double *pry	// Output, coefficient for y ratio..		NOT return, if NULL
);

// deprecated
int check_circle3D(		// 1: success, 0: fail.
    double *x,		// X
    double *y,		// Y
    double *z,		// Z
    int points,		// count of pixel
    double *pcx,	// OUTPUT, x of center
    double *pcy,	// OUTPUT, y of center
    double *pcz,	// OUTPUT, y of center
    double *pcr,	// OUTPUT, radius of circle 
    double *per		// OUTPUT, normalized error
);

int getplanabc(		// 1: success, 0: fail.
    double *x,		// X
    double *y,		// Y
    double *z,		// Z
    int points,		// count of pixel
    double *pa,		// OUTPUT, a
    double *pb,		// OUTPUT, b
    double *pc		// OUTPUT, c
);
#endif 