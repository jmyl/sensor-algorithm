/*!
 *******************************************************************************
                                                                                
                    CREATZ Log 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2014 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_log.h
	 @brief  Debug Message module
	 @author YongHo Suk                                 
	 @date   2014/10/08 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/

#if !defined(_CR_LOG_H_)
#define		 _CR_LOG_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define LOGSTATICOBJECT
/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif


HAND cr_log_create(U32 logid);
void cr_log_delete(HAND hcl);
void cr_log_enable(HAND hcl, int enable);
void cr_log(HAND hcl, U32 mode, char *strMsg, ... );
void cr_log_binlog(HAND hcl, char *buf);
void cr_log_decrypt(char *fn, FILE *fpout);
char *cr_log_getfn(HAND hcl);

#if defined (__cplusplus)
}
#endif


#endif		// _CR_LOG_H_

