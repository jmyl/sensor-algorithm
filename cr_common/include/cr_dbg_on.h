#if !defined(__CR_DBG_ON__)
#define __CR_DBG_ON__
/* LOCAL debug print */
#ifndef DEBUG_BUILD
#ifdef LOCAL_DEBUG_ON
#undef LOCAL_DEBUG_ON
#define LOCAL_DEBUG_ON 0
#endif
#ifdef LOCAL_ERROR_TRACE_ON
#undef LOCAL_ERROR_TRACE_ON
#define LOCAL_ERROR_TRACE_ON 0
#endif
#endif
	
#if (LOCAL_DEBUG_ON == 1)
#if (_WIN32) 
#define LOCAL_DEBUG(x)		OutputDebugStringA(x) 
#else
#define LOCAL_DEBUG(x)		printf x
#endif

#else
#define LOCAL_DEBUG(x)
#endif
	
#if (LOCAL_ERROR_TRACE_ON == 1)
#undef	LOCAL_TRACE_ERR
#if (_WIN32) 
#define LOCAL_TRACE_ERR(x)		OutputDebugStringA(x) 
#else
#define LOCAL_TRACE_ERR(x)		printf x
#endif
#else
#define LOCAL_TRACE_ERR(x)	
#endif

#endif

