/*!
 *******************************************************************************
                                                                                
                   Creatz Camera Sensor 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2010, 2011 by Creatz Inc.
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc. \n

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_bugtrap.h
	 @brief  Creatz Camera Golf Sensor Bugtrap 
	 @author YongHo Suk                                 
	 @date   2015/07/06 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_IANA_BUGTRAP_H_)
#define		 _IANA_BUGTRAP_H_


/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines datatype
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

HAND iana_bugtrap_create(char *ipaddr, I32 portnum, I32 vmajor, I32 vminor, I32 vbuild, char *logfn);
void iana_bugtrap_installSehFilter(void);
void iana_bugtrap_delete(HAND hcbgt, I32 sendsnapshot);



#endif			// _IANA_BUGTRAP_H_


