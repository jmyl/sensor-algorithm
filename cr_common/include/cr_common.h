/*!
 *******************************************************************************
                                                                                
                    Creatz Camera COMMON data type
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 ~ 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_common.h
	 @brief  
	 @author YongHo Suk
	 @date   2011/03/14 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_CR_COMMON_H_)
#define		 _CR_COMMON_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#if defined(_DEBUG)
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>


#if defined(_WIN32)
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <Winsock2.h>


#if defined( _MSC_VER ) && (_MSC_VER <= 1200)    // 1200 = MSVC 6.0.
#pragma warning(disable:4115)
#endif
#include <windows.h>
#if defined( _MSC_VER ) && (_MSC_VER <= 1200)    // 1200 = MSVC 6.0.
#pragma warning(default:4115)
#endif

#endif

#include "cr_global.h"

#include "cr_dbgmsg.h"




/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

#define		CR_OK						0x00000000

#define		CR_ERR_GENERAL				0x80000000
#define		CR_ERR_BADHANDLE			0x80000001
#define		CR_ERR_UNSUPPORTED_CMD		0x80000010
#define		CR_ERR_BADPARAM				0x80000011

#define		CR_ERROR					CR_ERR_GENERAL


#define		CR_IS_RESULT_ERROR(x)		(CR_ERR_GENERAL & (x))

#define		CR_TRUE						1
#define		CR_FALSE						0


/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/
typedef	  signed char	I08;
typedef	  signed short	I16;
typedef	  signed int	I32;
typedef	  signed long	L32;
typedef	unsigned char	U08;
typedef	unsigned short	U16;
typedef	unsigned int	U32;
typedef	unsigned long	UL32;

typedef	long long				I64;
typedef	unsigned long long		U64;

typedef void*			HAND;

typedef unsigned int CR_BOOL;

typedef struct _CR_GUID {
    unsigned int  Data1;
    unsigned short Data2;
    unsigned short Data3;
    unsigned char  Data4[8];
} CR_GUID, *PCR_GUID;


//---
#if defined(_WIN32)
#if defined(_WIN64)
typedef	I64 PARAM_T;				// 64bit..
#else
typedef	I32 PARAM_T;				// 32bit..
#endif

typedef	TCHAR	CHR;

//---
#elif defined(__linux__)
#if defined(__GNUC__)

#if defined(LINUX_32BIT)
typedef	I32 PARAM_T;				// 32bit..

#else
typedef	I64 PARAM_T;				// 64bit..
#endif

typedef	char	CHR;

typedef	unsigned char	BYTE;
typedef	unsigned short	WORD;
typedef	unsigned long	DWORD;
typedef	long	LONG;
typedef unsigned int BOOL, BOOLEAN;
#define TRUE		1
#define FALSE		0


/* Shared Directories */
#define SHARED_DIR		"/home/pi/shared"
#define CONFIG_DIR		SHARED_DIR"/config"
#define IMAGE_DIR		SHARED_DIR"/image"
#define DATA_DIR		SHARED_DIR"/data"
#define RESULT_DIR		SHARED_DIR"/result"


/* Window specific */
#define CALLBACK
#define __FUNCTION__
#define TEXT(x)		x
#define _T(x)		x


#else
#error WHAT COMPILER?...
#endif

typedef HAND HANDLE;
#endif
//---

#ifndef UNUSED
#define UNUSED(x) 	(void)(x)
#endif

//---------------------------
#include "cr_log.h"

#endif		// _CR_COMMON_H_

