/*!
 *******************************************************************************
                                                                                
                   Creatz Camera 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2012 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_math.h
	 @brief  Math tools
	 @author YongHo Suk                                 
	 @date   2014/1002 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_CR_MATH_H_)
#define		 _CR_MATH_H_

#include "cr_common.h"

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define PHI 0x9e3779b9
typedef struct _cr_rand {
	U32 Q[4096];
	U32 ccc;
	U32 iii;
	U32 seed;
	U32 residualdata;
	U32 residualcount;
} cr_rand_t;

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

U32 cr_crc32(U32 crc, U08 *buf, U32 len);
U32 crc32g(U32 crc, U08 *buf, U32 size);
U32 crc32X(U32 crc, U08 *buf, U32 len);
HAND cr_rand_create(U32 seed);
void cr_rand_delete(HAND hrr);
void cr_rand_init(HAND hrr, U32 seed) ;
U32  cr_rand_gendata(HAND hrr);
U08  cr_rand_gendataBYTE(HAND hrr);
U32  cr_rand_mkseed(U32 p0, U32 p1, U32 p2, U32 p3);


#if defined (__cplusplus)
}
#endif


#endif		// _CR_MATH_H_

