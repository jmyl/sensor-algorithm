/*!
 *******************************************************************************
                                                                                
                   Creatz  Camera Jpeg codec.
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_cam_jpeg.c
	 @brief  Jpeg Codec
	 @author YongHo Suk                                 
	 @date   2011/10/31 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#if defined(_DEBUG)
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif
#include <string.h>

#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#else
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#endif

#include "cr_common.h"
#include "cr_osapi.h"

#include "turbojpeg.h"

#include "cr_cam_jpeg.h"


/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

typedef struct _cam_jpeg {
	HAND tjhc;
	HAND tjhd;
	U08 *rgbbuf;
	U08 *binbuf;
	U08 *ubuf;
	U08 *vbuf;
	
} cam_jpeg_t;


U08 *cr_cam_jpeg_Y_encode_withRGB(	// NULL: Fail, NotNull: Sucess and result buffer ptr
		HAND hjpg,
		U32 *jpglen,	// Output, Jpeg bitstream length (0: Fail encoding)
		U08 *ybuf,		// Input, Y
		U32 width,		// Image width
		U32 height,		// Image height
		U32 qvalue		// Compression qvalue
	);

U08 *cr_cam_jpeg_Y_encode_withYUV(	// NULL: Fail, NotNull: Sucess and result buffer ptr
		HAND hjpg,
		U32 *jpglen,	// Output, Jpeg bitstream length (0: Fail encoding)
		U08 *ybuf,		// Input, Y
		U32 width,		// Image width
		U32 height,		// Image height
		U32 qvalue		// Compression qvalue
	); 



/*!
 ********************************************************************************
 *	@brief      Jpeg Encoder
 *  @return		1: Good. 0: Fail.
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/
//#define JPEG_YUV
#define JPEG_RGB

HAND cr_cam_jpeg_create()
{
	cam_jpeg_t *pjpg;
	U32 uvlen;
	pjpg = (cam_jpeg_t *) malloc(sizeof(cam_jpeg_t));
#define MAXWIDTH	2048
#define MAXHEIGHT	2048
	//#define MAXBINLEN	(256 * 1024)
#define MAXBINLEN	(1024 * 1024)
	uvlen = (MAXWIDTH/2) * (MAXHEIGHT/2);

	if (pjpg != NULL) {
		pjpg->tjhc = tjInitCompress();
		pjpg->tjhd = tjInitDecompress();

		pjpg->rgbbuf = (U08*)malloc(MAXWIDTH*MAXHEIGHT * 3);
		pjpg->binbuf = (U08*)malloc(MAXBINLEN);
		pjpg->ubuf = (U08*)malloc(uvlen);
		pjpg->vbuf = (U08*)malloc(uvlen);

		memset(pjpg->ubuf, 0x80, uvlen);
		memset(pjpg->vbuf, 0x80, uvlen);
	}

	return (HAND) pjpg;
}

void cr_cam_jpeg_delete(HAND hjpg)
{
	cam_jpeg_t *pjpg;

	pjpg = (cam_jpeg_t *) hjpg;

	if (pjpg != NULL) {
		//pjpg->rgbbuf = malloc(640*480 * 3);
		tjDestroy(pjpg->tjhc);
		tjDestroy(pjpg->tjhd);
		free(pjpg->rgbbuf);
		free(pjpg->binbuf);

		free(pjpg->ubuf);
		free(pjpg->vbuf);
		free(pjpg);
	}

}


U08 *cr_cam_jpeg_Y_encode(	// NULL: Fail, NotNull: Sucess and result buffer ptr
		HAND hjpg,
		U32 *jpglen,	// Output, Jpeg bitstream length (0: Fail encoding)
		U08 *ybuf,		// Input, Y
		U32 width,		// Image width
		U32 height,		// Image height
		U32 qvalue		// Compression qvalue
	) 
{
	U08 *resbuf;

	// use cr_cam_jpeg_Y_encode_withRGB(). better result.
	resbuf =  cr_cam_jpeg_Y_encode_withRGB(	// NULL: Fail, NotNull: Sucess and result buffer ptr
		hjpg,
		jpglen,	// Output, Jpeg bitstream length (0: Fail encoding)
		ybuf,		// Input, Y
		width,		// Image width
		height,		// Image height
		qvalue		// Compression qvalue
	); 

	return resbuf;
}


U08 *cr_cam_jpeg_Y_encode_withRGB(	// NULL: Fail, NotNull: Sucess and result buffer ptr
		HAND hjpg,
		U32 *jpglen,	// Output, Jpeg bitstream length (0: Fail encoding)
		U08 *ybuf,		// Input, Y
		U32 width,		// Image width
		U32 height,		// Image height
		U32 qvalue		// Compression qvalue
	) 
{

	I32 res;
	U08 *resbuf;
	cam_jpeg_t *pjpg;

	pjpg = (cam_jpeg_t *) hjpg;

	{
		U32 i;
		U08 *rgbbuf;

		rgbbuf = pjpg->rgbbuf;
		for (i = 0; i < width*height; i++) {
			*(rgbbuf + i * 3 + 0) = *(ybuf+i);			// R
			*(rgbbuf + i * 3 + 1) = *(ybuf+i);			// G
			*(rgbbuf + i * 3 + 2) = *(ybuf+i);			// B
		}
	}

	res = cr_cam_jpeg_RGB_encode(	// 0: success, -1: fail.
			pjpg,
			pjpg->binbuf,
			jpglen,
			pjpg->rgbbuf,

			width,
			height,
			qvalue
			);
	if (res == 0) {  // success
		resbuf = pjpg->binbuf;
	} else {
		resbuf = NULL;
	}

	return resbuf;
}


U08 *cr_cam_jpeg_Y_encode_withYUV(	// NULL: Fail, NotNull: Sucess and result buffer ptr
		HAND hjpg,
		U32 *jpglen,	// Output, Jpeg bitstream length (0: Fail encoding)
		U08 *ybuf,		// Input, Y
		U32 width,		// Image width
		U32 height,		// Image height
		U32 qvalue		// Compression qvalue
	) 
{

	I32 res;
	U08 *resbuf;
	cam_jpeg_t *pjpg;

	pjpg = (cam_jpeg_t *) hjpg;

	res = cr_cam_jpeg_YUV_encode(
			pjpg,
			pjpg->binbuf,
			jpglen,
			ybuf,
			pjpg->ubuf,
			pjpg->vbuf,
			width,
			height,
			qvalue
		);

	if (res == 0) {  // success
		resbuf = pjpg->binbuf;
	} else {
		resbuf = NULL;
	}

	return resbuf;
}


I32 cr_cam_jpeg_YUV_encode(	// 0: success, -1: fail.
		HAND hjpg,
		U08 *jpgbuf,	// Output, Jpeg bitstream
		U32 *jpglen,	// Output, Jpeg bitstream length (0: Fail encoding)
		U08 *ybuf,		// Input, Y, 411 (or 420)
		U08 *ubuf,		// Input, U(Cb)
		U08 *vbuf,		// Input, V(Cr)
		U32 width,		// Image width
		U32 height,		// Image height
		U32 qvalue		// Compression qvalue
		)
{
#ifdef IPP_SUPPORT
	I32 res;
	IppStatus ippstatus;
	const Ipp8u* pSrc[3];
	int srcStep[3];
	IppiSize roisize;
	unsigned long jpsize;
	int flag;


	//---
	cam_jpeg_t *pjpg;

	if (hjpg == NULL) {
		res = -1;
		goto func_exit;
	}

	pjpg = (cam_jpeg_t *) hjpg;

	pSrc[0] = ybuf;
	pSrc[1] = ubuf;
	pSrc[2] = vbuf;

	srcStep[0] = width;
	srcStep[1] = width/2;
	srcStep[2] = width/2;

	roisize.width = width;
	roisize.height = height;

	ippstatus = ippiYCbCr420ToRGB_8u_P3C3R(
			pSrc, srcStep,
			pjpg->rgbbuf, width * 3,
			roisize);


	flag = 0 
//		| TJ_BOTTOMUP 
//		| TJ_FASTUPSAMPLE 
		;


	res =  tjCompress(
				pjpg->tjhc,
				pjpg->rgbbuf, 
				width, 0,
				height, 
				3,
				jpgbuf, 
				&jpsize,
				TJ_420 /*TJ_GRAYSCALE*/ /*TJ_420*/,
				qvalue, 
				flag);

	if (res == 0 && jpsize > 0) {
		*jpglen = jpsize;
	} else {
		*jpglen = 0;
	}

func_exit:
	return res;
#else
    I32 res;
    U08* imgbuf;
    cv::Mat cvImgYUV, cvImgRGB;
    cv::Mat cvImgChU, cvImgChV;
    cv::Size size;
    unsigned long jpsize;
    int flag;


    //---
    cam_jpeg_t *pjpg;

    if (hjpg == NULL) {
        res = -1;
        goto func_exit;
    }

    pjpg = (cam_jpeg_t *)hjpg;
    imgbuf = (U08*)malloc(width*height*3);
    
    size.width = width;
    size.height = height;

    cv::resize(cv::Mat(size/2, CV_8UC1, ubuf), cvImgChU, size, 2.0, 2.0, cv::INTER_LINEAR);
    cv::resize(cv::Mat(size/2, CV_8UC1, vbuf), cvImgChV, size, 2.0, 2.0, cv::INTER_LINEAR);

    memcpy(imgbuf, ybuf, sizeof(U08) * width * height);
    memcpy(imgbuf + sizeof(U08) * width * height, cvImgChU.data, sizeof(U08) * width * height);
    memcpy(imgbuf + sizeof(U08) * width * height * 2, cvImgChV.data, sizeof(U08) * width * height);

    cvImgYUV = cv::Mat(size, CV_8UC3, imgbuf);
    cv::cvtColor(cvImgYUV, cvImgRGB, cv::COLOR_YUV2RGBA_I420);
    memcpy(pjpg->rgbbuf, cvImgRGB.data, sizeof(U08) * width * height * 3);

    flag = 0
        //		| TJ_BOTTOMUP 
        //		| TJ_FASTUPSAMPLE 
        ;
    
    res =  tjCompress(
        pjpg->tjhc,
        pjpg->rgbbuf,
        width, 0,
        height,
        3,
        jpgbuf,
        &jpsize,
        TJ_420 /*TJ_GRAYSCALE*/ /*TJ_420*/,
        qvalue,
        flag);

    if (res == 0 && jpsize > 0) {
        *jpglen = jpsize;
    } else {
        *jpglen = 0;
}

func_exit:
    return res;
#endif
}


I32 cr_cam_jpeg_RGB_encode(	// 0: success, -1: fail.
		HAND hjpg,
		U08 *jpgbuf,	// Output, Jpeg bitstream
		U32 *jpglen,	// Output, Jpeg bitstream length (0: Fail encoding)
		U08 *rgbbuf,	// Input, RGB
		U32 width,		// Image width
		U32 height,		// Image height
		U32 qvalue		// Compression qvalue
		)
{
	I32 res;
	unsigned long jpsize;
	int flag;


	//---
	cam_jpeg_t *pjpg;

	if (hjpg == NULL) {
		res = -1;
		goto func_exit;
	}

	pjpg = (cam_jpeg_t *) hjpg;

	flag = 0 
		//		| TJ_BOTTOMUP 
		//| TJ_FASTUPSAMPLE 
		//| TJ_BGR
		;


	res =  tjCompress(
				pjpg->tjhc,
				rgbbuf, //pjpg->rgbbuf, 
				width, 0,
				height, 
				3,
				jpgbuf, 
				&jpsize,
				TJ_444,  // TJ_420, /*TJ_GRAYSCALE*/ /*TJ_420*/
				qvalue, 
				flag);

	if (res == 0 && jpsize > 0) {
		*jpglen = jpsize;
	} else {
		*jpglen = 0;
	}

func_exit:
	return res;
}





I32 cr_cam_jpeg_headerparse(
		HAND hjpg,
		U08 *jpgbuf,	// input, Jpeg bitstream
		U32 jpglen,		// input, Jpeg bitstream length (0: Fail encoding)
		U32 *pwidth,	// output, Image width
		U32 *pheight	// output, Image height
		)
{
	I32 res;
	cam_jpeg_t *pjpg;

	if (hjpg == NULL) {
		res = -1;
		goto func_exit;
	}

	pjpg = (cam_jpeg_t *) hjpg;

	res = tjDecompressHeader(
			pjpg->tjhd,
			jpgbuf, jpglen,
			(I32 *)pwidth, (I32 *)pheight);

func_exit:
	return res;
}


I32 cr_cam_jpeg_decompress(
		HAND hjpg,
		U08 *yuvbuf,	// output, YUV buffer
		U08 *jpgbuf,	// input, Jpeg bitstream
		U32 jpglen		// input, Jpeg bitstream length (0: Fail encoding)
		)
{
	I32 res;
	cam_jpeg_t *pjpg;
	I32 flag;

	if (hjpg == NULL) {
		res = -1;
		goto func_exit;
	}

	pjpg = (cam_jpeg_t *) hjpg;

	flag = 0;

	res = tjDecompressToYUV(
			pjpg->tjhd,
			jpgbuf, jpglen,
			yuvbuf,
			flag);

func_exit:
	return res;
}



#if defined (__cplusplus)
}
#endif

