/*!
*******************************************************************************

                Creatz  Camera

    @section copyright_notice COPYRIGHT NOTICE
    Copyright (c) 2011 by Creatz Inc.
    All Rights Reserved. \n
    Do not duplicate without prior written consent of Creatz Inc.

*******************************************************************************
    @section file_information FILE CREATION INFORMATION
    @file   cr_interpolation.c
    @brief  Computes the evalutation at a given point form sevral known reference values by interpolation
    @author YongHo Suk
    @date   2015/09/10 First Created

    @section checkin_information LATEST CHECK-IN INFORMATION
    $Rev$
    $Author$
    $Date$
*******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/

#include "cr_common.h"
#include "cr_osapi.h"
#include "cr_interpolation.h"

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: type definition
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*!
 ********************************************************************************
 *	@brief      Linear Interpolation
 *	@param[in]  x
 *	            independent variable
 *	@param[in]  plip 
 *         		linear interpolatin fixed points
 *  @return		interpolated value
 *
 *	@author	    yhsuk
 *  @date       2015/0910
 *******************************************************************************/
double linearip(double x, interpol_t *plip)
{
	double y;

	double mi, bi;
	U32 n;
	U32 i;

	//---
	if (plip == NULL) {
		y = 0.0;
		goto func_exit;
	}
	n = plip->n;
	if (n < 1 || n > INTERPOLN-1) {
		y = 0.0;
		goto func_exit;
	}



	//--
	if (n == 1) {
		y = plip->yi[0];
		goto func_exit;
	}

	//--
	if (n == 2) {
		if ( x < plip->xi[0]) {
			y = plip->yi[0];
		} else if ( x < plip->xi[1]) {
			mi = (plip->yi[1] - plip->yi[0]) / (plip->xi[1] - plip->xi[0]);
			bi = plip->yi[1] - mi * plip->xi[1];
			y = mi * x + bi;
		} else {
			y = plip->yi[1];
		}

		goto func_exit;
	}


	//--
	for (i = 0; i <= n; i++) {
		if (x <= plip->xi[i]) {
			break;
		}
	}

	if (i == 0) {				// Lower Boundary area,
		mi = 0.0;
		bi = plip->yi[0];
	} else if (i == n+1) {		// Upper Boundary area,
		mi = 0.0;
		bi = plip->yi[n];
	} else {
		double xi1, xi;
		double yi1, yi;

		xi1 = plip->xi[i-1];
		xi  = plip->xi[i];

		yi1 = plip->yi[i-1];
		yi  = plip->yi[i];


		mi = (yi - yi1) / (xi - xi1);
		bi = yi - mi * xi;
	}

	y = mi * x + bi;

func_exit:
	return y;
}



/*!
 ********************************************************************************
 *	@brief      2D Linear Interpolation
 *	@param[in]  x
 *	            independent variable
 *	@param[in]  z
 *	            2nd independent variable
 *	@param[in]  plip 
 *  			array of linear interpolatin fixed points
 *  @return		interpolated value
 *
 *	@author	    yhsuk
 *  @date       2015/0910
 *******************************************************************************/
double linearip2d(double x, double z, interpol2d_t *plip2)
{
	double y;
	int m;
	int i;

	if (plip2 == NULL) {
		y = 0.0;
		goto func_exit;
	}

	m = plip2->m;
	if (m < 1 || m > INTERPOLM-1) {
		y = 0.0;
		goto func_exit;
	}

	for (i = 0; i <= m; i++) {
		if (z <= plip2->z[i]) {
			break;
		}
	}

	if (i == 0) {				// Lower Boundary area,
		y = linearip(x, &plip2->lip[0]); 		// 0
	} else if (i == m+1) {		// Upper Boundary area,
		y = linearip(x, &plip2->lip[m]); 		// M+1
	} else {
		double yi, yi1;
		double zi, zi1;
		double p, q;
		yi  = linearip(x, &plip2->lip[i]); 		// i
		yi1 = linearip(x, &plip2->lip[i-1]); 	// i-1

		zi  = plip2->z[i];
		zi1 = plip2->z[i-1];

		p   = (zi - z) / (zi - zi1);

		q   = 1.0 - p;

		y   = p * yi1 + q * yi;
	}

func_exit:
	return y;
}


#if defined (__cplusplus)
}
#endif

