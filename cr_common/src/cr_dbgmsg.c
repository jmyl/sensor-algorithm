/*!
 *******************************************************************************
                                                                                
                    CREATZ Debug message.
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 ~ 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_dbgmsg.c
	 @brief  Debug Message module
	 @author YongHo Suk                                 
	 @date   2011/03/14 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/

#if defined(_WIN32)
#if defined( _MSC_VER ) && (_MSC_VER <= 1200)    // 1200 = MSVC 6.0.
#pragma warning(disable:4115)
#endif
#include <stdio.h> 
#include <stdarg.h>


#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <winsock2.h>
#include <windows.h>

#if defined( _MSC_VER ) && (_MSC_VER <= 1200)    // 1200 = MSVC 6.0.
#pragma warning(default:4115)
#endif

#elif defined(__linux__) 
#include <stdio.h> 
#include <stdarg.h>
#endif




#include "cr_common.h"
#include "cr_dbgmsg.h"
#include "cr_osapi.h"


/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/
U32 s_cr_debugtrace = 1;

U32 s_cr_debugtrace_area =	
//								CR_MSG_AREA_ALL;


								(0
								 |CR_MSG_AREA_GENERAL 
								 | CR_MSG_AREA_GIGEV
								 );

U32 s_cr_debugtrace_level = CR_MSG_LEVEL_WARN;
//U32 s_cr_debugtrace_level = CR_MSG_LEVEL_VERB;


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif


//////////////////
//// _TRACE_()
//
// 아래의 define 에 대해서는 그대로 유지해주세요. [yhsuk. 20201113]
#define DEBUGOUT                
#define DBGVIEW
//#define FILEOUT

#if defined(FILEOUT)
static FILE *s_fp = NULL;
#define LOGFILE_NAME	"scam.log"
#endif

void cr_trace(
		unsigned int area,
		unsigned int level,
		const char* szFormat, ...)
{
#if defined (DEBUGOUT)
    char szTempBuf[2048] ;
    va_list vlMarker ;

	if (!s_cr_debugtrace) {
		goto func_exit;
	}

	if ((s_cr_debugtrace_area & area) && (level <= s_cr_debugtrace_level)) {
		va_start(vlMarker,szFormat) ;
		vsprintf(szTempBuf,szFormat,vlMarker) ;
		va_end(vlMarker) ;

		//    OutputDebugString((LPCWSTR)szTempBuf) ;
#if defined(DBGVIEW)
#if (_WIN32) 
		OutputDebugStringA(szTempBuf) ;
#else
		printf(szTempBuf);
#endif
#endif
#if defined(FILEOUT)
		if (s_fp == NULL) {
			s_fp = cr_fopen(LOGFILE_NAME, "a");
		}

		if (s_fp != NULL) {
			cr_fprintf(s_fp, szTempBuf);
			cr_fflush(s_fp);
		}

#endif
	}
	func_exit:
#else

	{
		area = area;
		level = level;
		szFormat = szFormat;
	}
#endif

	return;
}


void cr_trace_enable(void)
{
#if defined(FILEOUT)
	if (s_fp == NULL) {
		s_fp = cr_fopen(LOGFILE_NAME, "a");
	}
#endif
	s_cr_debugtrace = 1;
//	s_cr_debugtrace = 0;
}

void cr_trace_disable(void)
{
	s_cr_debugtrace = 0;
#if defined(FILEOUT)
		if (s_fp != NULL) {
			cr_fclose(s_fp);
			s_fp = NULL;
		}
#endif
}

void cr_trace_config(
		unsigned int area,
		unsigned int level)
{
	s_cr_debugtrace_area = area;
	s_cr_debugtrace_level = level;
}

#if defined (__cplusplus)
}
#endif

