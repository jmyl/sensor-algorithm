/*!
*******************************************************************************

                Creatz Matrix Module

    @section copyright_notice COPYRIGHT NOTICE
    Copyright (c) 2021 by Creatz Inc.
    All Rights Reserved. \n
    Do not duplicate without prior written consent of Creatz Inc.

*******************************************************************************
    @section file_information FILE CREATION INFORMATION
    @file      cr_matrix.h
    @brief     This defines matrix operations
    @author    YongHo Suk, Hyeonseok Choi
    @date      2021/02/15 First Created

    @section checkin_information LATEST CHECK-IN INFORMATION
    $Rev$
    $Author$
    $Date$
*******************************************************************************/

/*----------------------------------------------------------------------------
    Description	: defines referenced header files
-----------------------------------------------------------------------------*/

#include "cr_common.h"
#include "cr_matrix.h"

/*----------------------------------------------------------------------------
*	Description	: defines Macros and definitions
-----------------------------------------------------------------------------*/

#define EPSILON_MIN (1.e-20)

/*----------------------------------------------------------------------------
*	Description	: type definition
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*	Description	: static variable declaration
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*	Description	: external and internal global & static variable declaration
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*	Description	: declares the external function prototype
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*	Description	: declares the static function prototype
-----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*!
********************************************************************************
*	@brief      Computes the determinant of 3*3 matrix
*	@param[in]  A
                The given 3*3 matrix
*
*   @return		determinant of the input matrix A
*
*   @author	    yhsuk
*   @date       Unknown
*   @rev        2020.11.12 by Hyeonseok Choi
*******************************************************************************/
double mat3det(double A[3][3])
{
    double det;

    //-----
    det = 0.;

    det  = A[0][0]*A[0+1][0+1]*A[0+2][0+2];
    det += A[0][1]*A[0+1][1+1]*A[0+2][0];
    det += A[0][2]*A[0+1][0]  *A[0+2][1];

    det -= A[2][0]*A[2-1][0+1]*A[2-2][0+2];		// i = 2, j = 0
    det -= A[2][1]*A[2-1][1+1]*A[2-2][0];		// i = 2, j = 1
    det -= A[2][2]*A[2-1][0]  *A[2-2][1];		// i = 2, j = 2

    return det;
}


/*!
********************************************************************************
*	@brief      perform a scalar multiplication to a 3*3 matrix
*	@param[in]  A
                3*3 input matrix
*	@param[in]  s
                scalar to multiply
*	@param[out] B
                3*3 output matrix
*
*   @return		None
*
*	@author	    yhsuk
*   @date       Unknown
*   @rev        2020.11.12 by Hyeonseok Choi
*******************************************************************************/
void mat3scalar(double A[3][3], double s, double B[3][3])
{
    U32 i, j;


    //-----

    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            B[i][j] = s * A[i][j];
        }
    }

    return;
}




/*!
********************************************************************************
*	@brief      computes the inverse of a 3*3 matrix
*	@param[in]  A
                3*3 input matrix
*	@param[out] X
                3*3 output matrix, inversed
*
*   @return		The determinant of input matrix A
*
*	@author	    yhsuk
*   @date       Unknown
*   @rev        2020.11.12 by Hyeonseok Choi
*******************************************************************************/
double mat3inv(
    double A[3][3], 		// input
    double X[3][3]		// output
)
{
    double B[3][3];			// transpose of A
    double C[3][3];			// adj of A
    double i_det, det;//det is the determinant of A
    int i, j;

    det = 0.;

    //-----
    det  = A[0][0]*A[0+1][0+1]*A[0+2][0+2];
    det += A[0][1]*A[0+1][1+1]*A[0+2][0];
    det += A[0][2]*A[0+1][0]  *A[0+2][1];

    det -= A[2][0]*A[2-1][0+1]*A[2-2][0+2];		// i = 2, j = 0
    det -= A[2][1]*A[2-1][1+1]*A[2-2][0];		// i = 2, j = 1
    det -= A[2][2]*A[2-1][0]  *A[2-2][1];		// i = 2, j = 2

#ifdef DEBUG_CIRCLE
    printf("=====================================================================\n\n");
    printf("The determinant of matrix A is %.2f ", det);
    printf("\n\n=====================================================================\n");
#endif

    if (det > -EPSILON_MIN && det < EPSILON_MIN) {
        det = 0.;

        for (i=0; i<3; i++) {
            for (j=0; j<3; j++) {
                X[i][j]=0;
            }
        }

        goto func_exit;
    }
    i_det = 1./det;


    mat3trans(A, B);

    C[0][0]=B[1][1]*B[2][2]-(B[2][1]*B[1][2]);
    C[0][1]=(-1)*(B[1][0]*B[2][2]-(B[2][0]*B[1][2]));
    C[0][2]=B[1][0]*B[2][1]-(B[2][0]*B[1][1]);

    C[1][0]=(-1)*(B[0][1]*B[2][2]-B[2][1]*B[0][2]);
    C[1][1]=B[0][0]*B[2][2]-B[2][0]*B[0][2];
    C[1][2]=(-1)*(B[0][0]*B[2][1]-B[2][0]*B[0][1]);

    C[2][0]=B[0][1]*B[1][2]-B[1][1]*B[0][2];
    C[2][1]=(-1)*(B[0][0]*B[1][2]-B[1][0]*B[0][2]);
    C[2][2]=B[0][0]*B[1][1]-B[1][0]*B[0][1];


    for (i=0; i<3; i++) {
        for (j=0; j<3; j++) {
            X[i][j]=C[i][j]*i_det;
        }
    }

func_exit:
    return det;
}

/*!
********************************************************************************
*	@brief      performes transpose to a 3*3 matrix
*	@param[in]  A
                3*3 input matrix
*	@param[out] X
                3*3 output matrix, transposed
*
*   @return		None
*
*	@author	    yhsuk
*   @date       Unknown
*   @rev        2020.11.12 by Hyeonseok Choi
*******************************************************************************/
void mat3trans(
    double A[3][3], 		// input
    double X[3][3]		// output
)
{
    int i, j;

    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            X[i][j] = A[j][i];
        }
    }
}


/*!
********************************************************************************
*	@brief      performs the matrix multiplication of two 3*3 matrix
*	@param[in]  ma
                a 3*3 input matrix
*	@param[in]  mb
                another 3*3 input matrix
*	@param[out] mc
                resulted 3*3 output matrix, multiplied
*
*   @return		None
*
*	@author	    yhsuk
*   @date       Unknown
*   @rev        2020.11.12 by Hyeonseok Choi
*******************************************************************************/
void mat33mult(
    double ma[3][3], double mb[3][3], double mc[3][3]
)
{
    int i, j, k;
    double dtmp;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            dtmp = 0;
            for (k = 0; k < 3; k++) {
                dtmp += ma[i][k] * mb[k][j];
            }
            mc[i][j] = dtmp;
        }
    }
}


/*!
********************************************************************************
*	@brief      performs the multiplication of 3*3 matrix to a 3 vector
*	@param[in]  ma
                a 3*3 matrix to multiply
*	@param[in]  va
                a 3 vector to multiply
*	@param[out] vb
                resulted 3 vector, multiplied
*
*   @return		None
*
*	@author	    yhsuk
*   @date       Unknown
*   @rev        2020.11.12 by Hyeonseok Choi
*******************************************************************************/
void mat3v3mult(
    double ma[3][3], double va[3], double vb[3]
)
{
    int i, j;
    double dtmp;

    for (i = 0; i < 3; i++) {
        dtmp = 0;
        for (j = 0; j < 3; j++) {
            dtmp += ma[i][j] * va[j];
        }
        vb[i] = dtmp;
    }
}

#if defined (__cplusplus)
}
#endif