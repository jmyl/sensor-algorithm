/*!
 *******************************************************************************
                                                                                
                   Creatz Camera 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 ~ 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_pixeltrans.c
	 @brief  Pixel Coordinate transform 
	 @author YongHo Suk                                 
	 @date   2012/03/12 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/

#include "cr_common.h"
#include "cr_osapi.h"
#include "cr_math.h"

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: type definition
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif



/* ========================================================================= */
/* ========================================================================= */

/* ========================================================================
 * Table of CRC-32's of all single-byte values 
 */
static const U32 crc_table[256] = {
  0x00000000L, 0x77073096L, 0xee0e612cL, 0x990951baL, 0x076dc419L,
  0x706af48fL, 0xe963a535L, 0x9e6495a3L, 0x0edb8832L, 0x79dcb8a4L,
  0xe0d5e91eL, 0x97d2d988L, 0x09b64c2bL, 0x7eb17cbdL, 0xe7b82d07L,
  0x90bf1d91L, 0x1db71064L, 0x6ab020f2L, 0xf3b97148L, 0x84be41deL,
  0x1adad47dL, 0x6ddde4ebL, 0xf4d4b551L, 0x83d385c7L, 0x136c9856L,
  0x646ba8c0L, 0xfd62f97aL, 0x8a65c9ecL, 0x14015c4fL, 0x63066cd9L,
  0xfa0f3d63L, 0x8d080df5L, 0x3b6e20c8L, 0x4c69105eL, 0xd56041e4L,
  0xa2677172L, 0x3c03e4d1L, 0x4b04d447L, 0xd20d85fdL, 0xa50ab56bL,
  0x35b5a8faL, 0x42b2986cL, 0xdbbbc9d6L, 0xacbcf940L, 0x32d86ce3L,
  0x45df5c75L, 0xdcd60dcfL, 0xabd13d59L, 0x26d930acL, 0x51de003aL,
  0xc8d75180L, 0xbfd06116L, 0x21b4f4b5L, 0x56b3c423L, 0xcfba9599L,
  0xb8bda50fL, 0x2802b89eL, 0x5f058808L, 0xc60cd9b2L, 0xb10be924L,
  0x2f6f7c87L, 0x58684c11L, 0xc1611dabL, 0xb6662d3dL, 0x76dc4190L,
  0x01db7106L, 0x98d220bcL, 0xefd5102aL, 0x71b18589L, 0x06b6b51fL,
  0x9fbfe4a5L, 0xe8b8d433L, 0x7807c9a2L, 0x0f00f934L, 0x9609a88eL,
  0xe10e9818L, 0x7f6a0dbbL, 0x086d3d2dL, 0x91646c97L, 0xe6635c01L,
  0x6b6b51f4L, 0x1c6c6162L, 0x856530d8L, 0xf262004eL, 0x6c0695edL,
  0x1b01a57bL, 0x8208f4c1L, 0xf50fc457L, 0x65b0d9c6L, 0x12b7e950L,
  0x8bbeb8eaL, 0xfcb9887cL, 0x62dd1ddfL, 0x15da2d49L, 0x8cd37cf3L,
  0xfbd44c65L, 0x4db26158L, 0x3ab551ceL, 0xa3bc0074L, 0xd4bb30e2L,
  0x4adfa541L, 0x3dd895d7L, 0xa4d1c46dL, 0xd3d6f4fbL, 0x4369e96aL,
  0x346ed9fcL, 0xad678846L, 0xda60b8d0L, 0x44042d73L, 0x33031de5L,
  0xaa0a4c5fL, 0xdd0d7cc9L, 0x5005713cL, 0x270241aaL, 0xbe0b1010L,
  0xc90c2086L, 0x5768b525L, 0x206f85b3L, 0xb966d409L, 0xce61e49fL,
  0x5edef90eL, 0x29d9c998L, 0xb0d09822L, 0xc7d7a8b4L, 0x59b33d17L,
  0x2eb40d81L, 0xb7bd5c3bL, 0xc0ba6cadL, 0xedb88320L, 0x9abfb3b6L,
  0x03b6e20cL, 0x74b1d29aL, 0xead54739L, 0x9dd277afL, 0x04db2615L,
  0x73dc1683L, 0xe3630b12L, 0x94643b84L, 0x0d6d6a3eL, 0x7a6a5aa8L,
  0xe40ecf0bL, 0x9309ff9dL, 0x0a00ae27L, 0x7d079eb1L, 0xf00f9344L,
  0x8708a3d2L, 0x1e01f268L, 0x6906c2feL, 0xf762575dL, 0x806567cbL,
  0x196c3671L, 0x6e6b06e7L, 0xfed41b76L, 0x89d32be0L, 0x10da7a5aL,
  0x67dd4accL, 0xf9b9df6fL, 0x8ebeeff9L, 0x17b7be43L, 0x60b08ed5L,
  0xd6d6a3e8L, 0xa1d1937eL, 0x38d8c2c4L, 0x4fdff252L, 0xd1bb67f1L,
  0xa6bc5767L, 0x3fb506ddL, 0x48b2364bL, 0xd80d2bdaL, 0xaf0a1b4cL,
  0x36034af6L, 0x41047a60L, 0xdf60efc3L, 0xa867df55L, 0x316e8eefL,
  0x4669be79L, 0xcb61b38cL, 0xbc66831aL, 0x256fd2a0L, 0x5268e236L,
  0xcc0c7795L, 0xbb0b4703L, 0x220216b9L, 0x5505262fL, 0xc5ba3bbeL,
  0xb2bd0b28L, 0x2bb45a92L, 0x5cb36a04L, 0xc2d7ffa7L, 0xb5d0cf31L,
  0x2cd99e8bL, 0x5bdeae1dL, 0x9b64c2b0L, 0xec63f226L, 0x756aa39cL,
  0x026d930aL, 0x9c0906a9L, 0xeb0e363fL, 0x72076785L, 0x05005713L,
  0x95bf4a82L, 0xe2b87a14L, 0x7bb12baeL, 0x0cb61b38L, 0x92d28e9bL,
  0xe5d5be0dL, 0x7cdcefb7L, 0x0bdbdf21L, 0x86d3d2d4L, 0xf1d4e242L,
  0x68ddb3f8L, 0x1fda836eL, 0x81be16cdL, 0xf6b9265bL, 0x6fb077e1L,
  0x18b74777L, 0x88085ae6L, 0xff0f6a70L, 0x66063bcaL, 0x11010b5cL,
  0x8f659effL, 0xf862ae69L, 0x616bffd3L, 0x166ccf45L, 0xa00ae278L,
  0xd70dd2eeL, 0x4e048354L, 0x3903b3c2L, 0xa7672661L, 0xd06016f7L,
  0x4969474dL, 0x3e6e77dbL, 0xaed16a4aL, 0xd9d65adcL, 0x40df0b66L,
  0x37d83bf0L, 0xa9bcae53L, 0xdebb9ec5L, 0x47b2cf7fL, 0x30b5ffe9L,
  0xbdbdf21cL, 0xcabac28aL, 0x53b39330L, 0x24b4a3a6L, 0xbad03605L,
  0xcdd70693L, 0x54de5729L, 0x23d967bfL, 0xb3667a2eL, 0xc4614ab8L,
  0x5d681b02L, 0x2a6f2b94L, 0xb40bbe37L, 0xc30c8ea1L, 0x5a05df1bL,
  0x2d02ef8dL
};


/* ========================================================================= */
#define DO1(buf) crc = crc_table[((U32)crc ^ (*buf++)) & 0xff] ^ (crc >> 8);
#define DO2(buf)  DO1(buf); DO1(buf);
#define DO4(buf)  DO2(buf); DO2(buf);
#define DO8(buf)  DO4(buf); DO4(buf);

/*!
 ********************************************************************************
 *	@brief      Get CRC32
 *	@param[in]  crc
 *				initial CRC value. 
 *	@param[in]  buf
 *				data for crc
 *	@param[in] len
 *				length for crc
 *  @return		CRC value
 *
 *	@author	    yhsuk
 *  @date       2014/1002
 *******************************************************************************/

U32 cr_crc32(U32 crc, U08 *buf, U32 len)
{
    if (buf == NULL) return 0L;

    crc = crc ^ 0xffffffffU;
    while (len >= 8)
    {
      DO8(buf);
      len -= 8;
    }
    if (len) do {
      DO1(buf);
    } while (--len);
    return crc ^ 0xffffffffU;
}

U32 crc32X(U32 crc, U08 *buf, U32 len)
{
    if (buf == NULL) return 0L;

//    crc = crc ^ 0xffffffffU;
    while (len >= 8)
    {
      DO8(buf);
      len -= 8;
    }
    if (len) do {
      DO1(buf);
    } while (--len);
//   return crc ^ 0xffffffffU;
   return crc;
}




//---------------------------------------------------------------------
/*-
 *  COPYRIGHT (C) 1986 Gary S. Brown.  You may use this program, or
 *  code or tables extracted from it, as desired without restriction.
 *
 *  First, the polynomial itself and its table of feedback terms.  The
 *  polynomial is
 *  X^32+X^26+X^23+X^22+X^16+X^12+X^11+X^10+X^8+X^7+X^5+X^4+X^2+X^1+X^0
 *
 *  Note that we take it "backwards" and put the highest-order term in
 *  the lowest-order bit.  The X^32 term is "implied"; the LSB is the
 *  X^31 term, etc.  The X^0 term (usually shown as "+1") results in
 *  the MSB being 1
 *
 *  Note that the usual hardware shift register implementation, which
 *  is what we're using (we're merely optimizing it by doing eight-bit
 *  chunks at a time) shifts bits into the lowest-order term.  In our
 *  implementation, that means shifting towards the right.  Why do we
 *  do it this way?  Because the calculated CRC must be transmitted in
 *  order from highest-order term to lowest-order term.  UARTs transmit
 *  characters in order from LSB to MSB.  By storing the CRC this way
 *  we hand it to the UART in the order low-byte to high-byte; the UART
 *  sends each low-bit to hight-bit; and the result is transmission bit
 *  by bit from highest- to lowest-order term without requiring any bit
 *  shuffling on our part.  Reception works similarly
 *
 *  The feedback terms table consists of 256, 32-bit entries.  Notes
 *
 *      The table can be generated at runtime if desired; code to do so
 *      is shown later.  It might not be obvious, but the feedback
 *      terms simply represent the results of eight shift/xor opera
 *      tions for all combinations of data and CRC register values
 *
 *      The values must be right-shifted by eight bits by the "updcrc
 *      logic; the shift must be unsigned (bring in zeroes).  On some
 *      hardware you could probably optimize the shift in assembler by
 *      using byte-swap instructions
 *      polynomial $edb88320
 *
 *
 * CRC32 code derived from work by Gary S. Brown.
 */

static U32 crc32_tab[] = {
	0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f,
	0xe963a535, 0x9e6495a3,	0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,
	0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2,
	0xf3b97148, 0x84be41de,	0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
	0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec,	0x14015c4f, 0x63066cd9,
	0xfa0f3d63, 0x8d080df5,	0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
	0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b,	0x35b5a8fa, 0x42b2986c,
	0xdbbbc9d6, 0xacbcf940,	0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
	0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423,
	0xcfba9599, 0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
	0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d,	0x76dc4190, 0x01db7106,
	0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
	0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d,
	0x91646c97, 0xe6635c01, 0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,
	0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
	0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
	0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7,
	0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
	0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa,
	0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
	0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81,
	0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
	0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683, 0xe3630b12, 0x94643b84,
	0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
	0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb,
	0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,
	0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 0xa1d1937e,
	0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
	0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55,
	0x316e8eef, 0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
	0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28,
	0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
	0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f,
	0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,
	0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242,
	0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
	0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69,
	0x616bffd3, 0x166ccf45, 0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,
	0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc,
	0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
	0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605, 0xcdd70693,
	0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
	0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};

U32 crc32g(U32 crc, U08 *buf, U32 size)
{
	U08 *p;

	//---
	p = buf;
	crc = crc ^ ~0U;

	while (size--)
		crc = crc32_tab[(crc ^ *p++) & 0xFF] ^ (crc >> 8);

	return crc ^ ~0U;
}



/* ========================================================================= */
/* ========================================================================= */
HAND cr_rand_create(U32 seed)
{
	cr_rand_t	*prr;
	//---

	prr = (cr_rand_t *) malloc(sizeof(cr_rand_t));
	cr_rand_init(prr, seed);

	return (HAND) prr;
}

void cr_rand_delete(HAND hrr)
{
	if (hrr) {
		free(hrr);
	}
}


void cr_rand_init(HAND hrr, U32 seed) 
{
    int i;
	cr_rand_t	*prr;

	//---
	if (hrr == NULL) {
		return ;
	}

	prr = (cr_rand_t *) hrr;


	prr->ccc 	= 362436;
	prr->iii 	= 4095;
	prr->seed = seed;


    prr->Q[0] = seed;
    prr->Q[1] = seed + PHI;
    prr->Q[2] = seed + PHI + PHI;

    for (i = 3; i < 4096; i++)
            prr->Q[i] = prr->Q[i - 3] ^ prr->Q[i - 2] ^ PHI ^ i;


	prr->residualdata = 0;
	prr->residualcount = 0;
	return;
}


U32 cr_rand_mkseed(U32 p0, U32 p1, U32 p2, U32 p3)
{
	U32 seed;

	U32 u0, u1, u2, u3;
	U32 v0, v1;


#define CONST0		0x99887766
#define CONST1		0x1294a0df
#define CONST2		0xad0f8790
#define CONST3		0x325abab1

#define SUFFLE1(x,y)		((((x)      ) & 0x0000FFFF) | (((y)      ) & 0xFFFF0000))
#define SUFFLE2(x,y)		((((x) >> 16) & 0x0000FFFF) | (((y) << 16) & 0xFFFF0000))
#define SUFFLE3(x,y)		((((x)      ) & 0xFF00FF00) | (((y)      ) & 0x00FF00FF))
#define SUFFLE4(x,y)		((((x)      ) & 0xF0F0F0F0) | (((y)      ) & 0x0F0F0F0F))
#define ROTATE_LEFT(x, n)   (( ((U32)(x)) << (n)) | ( ((U32) (x)) >> (32 - (n))))



	u0 = p0 ^ SUFFLE4(p0, ~ROTATE_LEFT(p1, 17));
	u1 = (p3 << 12) ^SUFFLE4(p1, ROTATE_LEFT(p2, 17));
	u2 = ((p1 << 24) ^ p3) ^ SUFFLE4(p2, ~ROTATE_LEFT(p3, 7));
	u3 = (p2 ^ u0) ^ SUFFLE4(p3, ROTATE_LEFT(p1, 13));

	u0 = u0 ^ (CONST0 ^ (p0 + ~CONST1));
	u1 = u1 ^ (CONST1 ^ (p1 + ~CONST2));
	u2 = u2 ^ (CONST2 ^ (p2 + ~CONST3));
	u3 = u3 ^ (CONST3 ^ (p3 + ~CONST0));

	u0 = SUFFLE4(u0, ~ROTATE_LEFT(u1, 17));
	u1 = SUFFLE4(u1, ROTATE_LEFT(u2, 17));
	u2 = SUFFLE4(u2, ~ROTATE_LEFT(u3, 7));
	u3 = SUFFLE4(u3, ROTATE_LEFT(u1, 13));

	u0 ^= (p1 * p3 * p2 * p0 + p1 * p1 * p1 * p1 + 1) ^ CONST3;
	u1 ^= (p2 * p2 * p3 * p0 + p2 * p2 * p2 * p2 + 2) ^ CONST2;
	u2 ^= (p3 * p2 * p0 * p0 + p3 * p3 * p3 * p3 + 3) ^ CONST1;
	u3 ^= (p0 * p2 * p1 * p0 + p0 * p0 * p0 * p0 + 4) ^ CONST0;

	u0 = SUFFLE3(~ROTATE_LEFT(u0,  1), u1) ^ u2;
	u1 = SUFFLE3(ROTATE_LEFT(u1, 13), u2) ^ u3;
	u2 = SUFFLE3(~ROTATE_LEFT(u2, 15), u3) ^ u0;
	u3 = SUFFLE3(ROTATE_LEFT(u3, 17), u0) ^ u1;

	v0 = u0 ^ ~ROTATE_LEFT(u1, 1);
	v1 = u2 ^ ~ROTATE_LEFT(u3, 17);
	
	seed = ROTATE_LEFT(v0, 13) ^ ROTATE_LEFT(v1, 1);

	seed = (((seed ^ (CONST0 +  p0)) ^ (CONST1 + p1)) ^ (CONST2 + ~p2)) ^ (CONST3 + ~p3);

	return seed;
}


U32 cr_rand_gendata(HAND hrr)
{
	cr_rand_t *prr;
    U64 t, a = 18782LL;
//    static unsigned int iii = 4095;
    U32 x, r = 0xfffffffe;

	//--
	if (hrr == NULL) {
		return 0;
	}

	prr = (cr_rand_t *) hrr;


    prr->iii = (prr->iii + 1) & 4095;
    t = a * prr->Q[prr->iii] + prr->ccc;
    prr->ccc = (unsigned int) ((t >> 32) & 0xFFFFFFFF);
    x = (unsigned int) ((t + prr->ccc) & 0xFFFFFFFF);
    if (x < prr->ccc) {
		x++;
		prr->ccc++;
    }
    return (prr->Q[prr->iii] = r - x);
}



U08 cr_rand_gendataBYTE(HAND hrr)
{
	U08 data;
	U32 rcount;
	cr_rand_t *prr;
	if (hrr == NULL) {
		return (U08) 0;
	}

	prr = (cr_rand_t *) hrr;

	rcount = prr->residualcount;

	if (rcount == 0) {
		prr->residualdata 	= cr_rand_gendata(hrr);
		prr->residualcount 	= 4;
	}
	data = (U08) (prr->residualdata & 0xFF);
	prr->residualdata >>= 8;
	prr->residualcount--;

	return data;
}


#if defined (__cplusplus)
}
#endif

