/*!
 *******************************************************************************

                   Creatz Regression Module

     @section copyright_notice COPYRIGHT NOTICE
     Copyright (c) 2011 by Creatz Inc.
     All Rights Reserved. \n
     Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
     @section file_information FILE CREATION INFORMATION
     @file      cr_regression.cpp
     @brief     Polynomial and circular regressions. Some algorithms such as RANSAC, constrainted regression are also included.
     @author    YongHo Suk, Hyeonseok Choi
     @date      2011/08/09 First Created

     @section checkin_information LATEST CHECK-IN INFORMATION
     $Rev$
     $Author$
     $Date$
 *******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#endif

#include "cr_common.h"
#include "cr_osapi.h"
#include "cr_math.h"
#include "cr_matrix.h"

#include "cr_regression.h"

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define POINTS_MAX		2048
#define EPSILON_MIN (1.e-20)

#define MAXINLIER_MINRATE	(0.3) // 0.4, 0.5, 0.6, 0.7
#define MIN_MAXINLIER	40
#define MIN_MAXINLIER0	30						// for test..
// #define DEBUG_CIRCLE
//#define 	CR_CIRCLE_TEST
//#define Z3_25mmTEST


//#define MIN_DENUMERATOR	(1e-6)
#define MIN_DENUMERATOR	(1e-8)
#define LARGE_MVALUE	1e5

#define DENOMINATOR_MIN		(1e-30)

#define MAXRANSACITER	64

#define R2MINRATIO	(0.93*0.93)
#define R2MAXRATIO	(1.07*1.07)

#define MIN_IN_CLASS_RATIO	(0.4)

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/
static int s_min_maxinlier_0 = 1;
static int s_RRR = 6;
int check_circle_woIPP(		// 1: success, 0: fail.
    double *x,		// X
    double *y,		// Y
    int points,		// count of pixel
    double *pcx,	// OUTPUT, x of center
    double *pcy,	// OUTPUT, y of center
    double *pcr,	// OUTPUT, radius of circle 
    double *per		// OUTPUT, normalized error
);

//static int regression(double x[], double y[], int count, double *pm, double *pb);

//---------------------------------

#if defined(CR_CIRCLE_TEST)
int main()
{
	double A[3][3];// the matrix that is entered by user
	double X[3][3];//the inverse
	int i, j;
	double det;

	while(1) {
		printf("========== Enter matrix A =============================================\n");
		for(i=0;i<3;i++) {
			for(j=0;j<3;j++) {     
				printf(" A[%d][%d]= ",i,j);
				scanf("%lf", &A[i][j]);
			}
		}

		printf("---------\n");
		printf("A: \n");
		for (i = 0; i < 3; i++) {
			for (j = 0; j < 3; j++) {
				printf("%10lf ", A[i][j]);
			}
			printf("\n");
		}
		printf("---------\n");

		det = mat3inv(A, X);


		printf("---------\n");
		printf("det: %lf\n", det);
		printf("Inv(A):\n");
		for (i = 0; i < 3; i++) {
			for (j = 0; j < 3; j++) {
				printf("%10lf ", X[i][j]);
			}
			printf("\n");
		}
	}

	return 0;
}
#endif			// defined(CR_CIRCLE_TEST)


/*!
 ********************************************************************************
 *	@brief      Check whether the given set of pixel points form a circle or not.
                If they form a circle, return the center and radius of the best circle, and error.
 *	@param[in]  x
                x coordinates
 *	@param[in]  y
                y coordinates
 *	@param[in]  points
                number of points, expected to form a circle
 *	@param[out] pcx
                pointer to the resulted x coordinate of the best circle
 *	@param[out] pcy
                pointer to the resulted y coordinate of the best circle
 *	@param[out] pcr
                pointer to the resulted radius of the best circle
 *	@param[out] per
                pointer to the error.
 *
 *  @return		1: success, 0: fail.
 *
 *	@author	    yhsuk
 *  @date       Unknown
 *  @rev        2020.11.12 by Hyeonseok Choi
 *******************************************************************************/
int check_circle(		// 1: success, 0: fail.
		double *x,		// X
		double *y,		// Y
		int points,		// count of pixel
		double *pcx,	// OUTPUT, x of center
		double *pcy,	// OUTPUT, y of center
		double *pcr,	// OUTPUT, radius of circle 
		double *per		// OUTPUT, normalized error
)
{
#ifdef IPP_SUPPORT
	int i;
	int ret;

	Ipp32f *pAN;		// count x 3
	int srcStride2;		//  = sizeof(Ipp32f);
	int srcStride1;		//  = 3 * sizeof(Ipp32f);				// x, y, 1
	Ipp32f *pBN;		// 1 x count

	int src2Stride2;	//  = sizeof(Ipp32f);

	Ipp32f *pDecomp;			// Decomposed matrix.    count x 3

	int decompoStride2;	//  = sizeof(Ipp32f);
	int decompoStride1;	//  = 3 * sizeof(Ipp32f); 				// x, y, 1

	Ipp32f *pX;			// 3 x 1

	int dstStride2;		//  = sizeof(Ipp32f);

	int width;			//  = 3;
	int height;			//  = count;

	Ipp32f *pBuffer;

	IppStatus status;

	Ipp32f xi, yi;
	Ipp32f cx, cy, w0;
	Ipp32f dtmp;
	Ipp32f en;
	Ipp32f r2;
	//------
	width 		= 3;
	height 		= points;
	srcStride2 	= sizeof(Ipp32f);
	srcStride1 	= width * sizeof(Ipp32f);				// x, y, 1
	src2Stride2 = sizeof(Ipp32f);
	decompoStride2 = sizeof(Ipp32f);
	decompoStride1 = width * sizeof(Ipp32f);
	dstStride2 	= sizeof(Ipp32f);

	pAN 		= (Ipp32f *) malloc(sizeof(Ipp32f) * points * width);
	pBN 		= (Ipp32f *) malloc(sizeof(Ipp32f) * points);
	pDecomp 	= (Ipp32f *) malloc(sizeof(Ipp32f) * points * width);
	pX  		= (Ipp32f *) malloc(sizeof(Ipp32f) * width);
	pBuffer 	= (Ipp32f *) malloc(sizeof(Ipp32f) * points);

	//--
	for (i = 0; i < points; i++) {
		xi = (Ipp32f) x[i];
		yi = (Ipp32f) y[i];
		//---
		*(pAN + width*i + 0) = (Ipp32f) xi;			// AN[i][0]
		*(pAN + width*i + 1) = (Ipp32f) yi;			// AN[i][1]
		*(pAN + width*i + 2) = (Ipp32f) 1;			// AN[i][2]

		//---
		*(pBN + i) =(Ipp32f) ((xi*xi + yi * yi)/2.);
	}

	status = ippmQRDecomp_m_32f((const Ipp32f*)pAN,
			srcStride1, srcStride2, pBuffer,
			pDecomp, decompoStride1, decompoStride2, width, height);

	status = ippmQRBackSubst_mv_32f(pDecomp, decompoStride1, decompoStride2 , pBuffer, pBN, src2Stride2, pX, dstStride2, width, height);

	cx = (Ipp32f) pX[0];
	cy = (Ipp32f) pX[1];
	w0 = (Ipp32f) pX[2];

	r2 = (Ipp32f) (2. * w0 + cx * cx + cy * cy);

	if (r2 <= 0.) {
		ret = 0;
		goto func_exit;
	}

	en = 0.;

	//---------------- For least square error
	for (i = 0; i < points; i++) {
		xi = *(pAN + width*i + 0);
		yi = *(pAN + width*i + 1);

		dtmp = (xi - cx) * (xi - cx) + (yi - cy) * (yi - cy);
		dtmp = (1.0f - dtmp/r2);
		dtmp = dtmp * dtmp;
		en += dtmp;
#ifdef DEBUG_CIRCLE
		   cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%d: ei: %lf (en: %lf)\n",
		   i,
		   dtmp,
		   en);
#endif
	}
	en = (Ipp32f)sqrt(en / (Ipp32f) points);

	//--
	*pcx = cx;
	*pcy = cy;
	*pcr = sqrt(r2);

	*per = en;
	ret = 1;
#ifdef DEBUG_CIRCLE
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "====> (%f, %f:%f), e: %f\n",  
			cx, cy, sqrt(r2), en);
#endif
	free(pAN);
	free(pBN);
	free(pDecomp);
	free(pX);
	free(pBuffer);

func_exit:
    return ret;
#else
    return check_circle_woIPP(x, y, points, pcx, pcy, pcr, per);
#endif
}


/*!
 ********************************************************************************
 *	@brief      Check whether the given set of pixel points form a circle or not. with no use of IPP
                If they form a circle, return the center and radius of the best circle, and error.
 *	@param[in]  x
                x coordinates
 *	@param[in]  y
                y coordinates
 *	@param[in]  points
                number of points, expected to form a circle
 *	@param[out] pcx
                pointer to the resulted x coordinate of the best circle
 *	@param[out] pcy
                pointer to the resulted y coordinate of the best circle
 *	@param[out] pcr
                pointer to the resulted radius of the best circle
 *	@param[out] per
                pointer to the error.
 *
 *  @return		1: success, 0: fail.
 *
 *	@author	    yhsuk
 *  @date       Unknown
 *  @rev        2020.11.12 by Hyeonseok Choi
 *******************************************************************************/
int check_circle_woIPP(		// 1: success, 0: fail.
    double *x,		// X
    double *y,		// Y
    int points,		// count of pixel
    double *pcx,	// OUTPUT, x of center
    double *pcy,	// OUTPUT, y of center
    double *pcr,	// OUTPUT, radius of circle 
    double *per		// OUTPUT, normalized error
)
{	
int i, j, k;
	double *AN;
	double *BN;
	double ANT_AN[3][3];
	double ANT_BN[3][1];
	double X[3][1];
	double ATAI[3][3];
	double xi, yi;
	double cx, cy, w0;
	double dtmp;
	double det;
	double en;
	double r2;

	double s_AN[POINTS_MAX*3];
	double s_BN[POINTS_MAX*1];

	int ret;
	//-------------

	AN = NULL;
	BN = NULL;

	if (points <= 0) {
		ret = 0;				// fail.
		goto func_exit;
	}

	AN = s_AN; 
	BN = s_BN;

	for (i = 0; i < points; i++) {
		xi = (double) x[i];
		yi = (double) y[i];
		//---
		*(AN + 3*i + 0) = xi;			// AN[i][0]
		*(AN + 3*i + 1) = yi;			// AN[i][1]
		*(AN + 3*i + 2) = 1;			// AN[i][2]

		//---
		*(BN + i) = (xi*xi + yi * yi)/2.;
	}

	//-- ANT AN
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			ANT_AN[i][j] = 0.;
			for (k = 0; k < points; k++) {
				ANT_AN[i][j] += *(AN + 3*k + i) * (*(AN + 3*k + j));
			}
		}
	}

	det = mat3inv(
			ANT_AN,
			ATAI
			);

	if (det == 0.) {
		ret = 0;
		goto func_exit;
	}



	//-- ANT BN
	for (i = 0; i < 3; i++) {
		ANT_BN[i][0] = 0.;
		for (k = 0; k < points; k++) {
			ANT_BN[i][0] += *(AN + 3*k + i) * (*(BN + k + 0));
		}
	}


	// (ANT AN)^-1 * ANT BN 
	for (i = 0; i < 3; i++) {
		X[i][0] = 0.;
		for (k = 0; k < 3; k++) {
			X[i][0] += ATAI[i][k]  * ANT_BN[k][0];
		}
	}

	//--------------------------------
	cx = X[0][0];
	cy = X[1][0];
	w0 = X[2][0];

	r2 = 2. * w0 + cx * cx + cy * cy;

	if (r2 <= 0.) {
		ret = 0;
		goto func_exit;
	}


	*pcx = cx;
	*pcy = cy;
	*pcr = sqrt(r2);

	en = 0.;

	//---------------- For least square error
	for (i = 0; i < points; i++) {
		xi = *(AN + 3*i + 0);
		yi = *(AN + 3*i + 1);

		dtmp = (xi - cx) * (xi - cx) + (yi - cy) * (yi - cy);
		dtmp = (1. - dtmp/r2);
		dtmp = dtmp * dtmp;
		en += dtmp;
#ifdef DEBUG_CIRCLE
		   cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%d: ei: %lf (en: %lf)\n",
		   i,
		   dtmp,
		   en);
#endif
	}

	en = sqrt(en / (double) points);

	*per = en;

	ret = 1;
#ifdef DEBUG_CIRCLE
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "(%lf, %lf:%lf), e: %lf\n",  
			cx, cy, sqrt(r2), en);
#endif
func_exit:
	return ret;
}


/*!
 ********************************************************************************
 *	@brief      Get the circle passing 3 given points
                see https://math.stackexchange.com/questions/827072/finding-an-equation-of-circle-which-passes-through-three-points?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
                and http://www.ambrsoft.com/TrigoCalc/Circle3D.htm
 *	@param[in]  x
                x coordinates of s points
 *	@param[in]  y
                y coordinates of s points
 *	@param[out] pcx
                a pointer to the x coordinate of the center of the circle
 *	@param[out] pcy
                a pointer to the y coordinate of the center of the circle
 *	@param[out] pr
                a pointer to the length of the radius of the circle
 *
 *  @return		1: success, 0: fail
 *
 *	@author	    yhsuk
 *  @date       Unknown
 *  @rev        2020.11.12 by Hyeonseok Choi
 *******************************************************************************/
int make_circle_w3pts(									// 1: success, 0: fail.
    double x[3], double y[3],				// 3 points, input
    double *pcx, double *pcy, double *pr	// circle, output
) {
    int res;
    double x1, x2, x3;
    double y1, y2, y3;
    double x12Py12, x22Py22, x32Py32;
    double A, B, C, D;
    double cx, cy;
    double r2;

    //--
    x1 = x[0]; x2 = x[1]; x3 = x[2];
    y1 = y[0]; y2 = y[1]; y3 = y[2];

    x12Py12 = x1 * x1 + y1 * y1;
    x22Py22 = x2 * x2 + y2 * y2;
    x32Py32 = x3 * x3 + y3 * y3;

    A = x1 * y2 + x2 * y3 + x3 * y1 - (y1*x2 + y2 * x3 + y3 * x1);
    if (A > -EPSILON_MIN && A < EPSILON_MIN) {
        res = 0;
        goto func_exit;
    }
    B = x12Py12 * (y3 - y2) + x22Py22 * (y1 - y3) + x32Py32 * (y2 - y1); 				B /= A;
    C = x12Py12 * (x2 - x3) + x22Py22 * (x3 - x1) + x32Py32 * (x1 - x2); 				C /= A;
    D = x12Py12 * (x3*y2 - x2 * y3) + x22Py22 * (x1*y3 - x3 * y1) + x32Py32 * (x2*y1 - x1 * y2); 	D /= A;

    cx = -B / 2.0;
    cy = -C / 2.0;

    r2 = B * B / 4.0 + C * C / 4.0 - D;

    if (r2 <= 0) {
        res = 0;
        goto func_exit;
    }

    *pr = sqrt(r2);
    *pcx = cx;
    *pcy = cy;
    res = 1;
func_exit:
    return res;
}


// split regression functions to another file?
/*!
 ********************************************************************************
 *	@brief      Regression. get coefficient m, b of  y = m x + b
 *
 *	@param[in] x
 *					independent variable. array.
 *	@param[in] y
 *					dependent variable. array.
 *	@param[in] count
 *					array count.
 *
 *	@param[out]  pm
 *				 m value.
 *
 *	@param[out]  pb
 *				 b value.
 *
 *  @return		0: Fail. Numerical calculation error.
 *  			1: Success.
 *
 *	@author	    yhsuk
 *  @date       2014/02/28
 *******************************************************************************/
int cr_regression(
		double x[], double y[], int count,
		double *pm, double *pb)

{
	int res;

	res = cr_regression2(
		x, y, count, pm, pb, 
		NULL);

	return res;
}


/*!
 ********************************************************************************
 *	@brief      Regression2. get coefficient m, b of  y = m x + b and r2
 *
 *	@param[in] x
 *					independent variable. array.
 *	@param[in] y
 *					dependent variable. array.
 *	@param[in] count
 *					array count.
 *
 *	@param[out]  pm
 *				 m value.
 *
 *	@param[out]  pb
 *				 b value.
 *
 *	@param[out]  pr2
 *				 r2 value
 *
 *  @return		0: Fail. Numerical calculation error.
 *  			1: Success.
 *
 *	@author	    yhsuk
 *  @date       2016/02/11
 *******************************************************************************/
int cr_regression2(
		double x[], double y[], int count,
		double *pm, double *pb, double *pr2)
{
	int i;
	double sx, sxx;
	double sy, syy;
	double sxy;
	double x_, y_;
	double N;
	double m_, b_;
	double d0, d1;
	double sstot, ssres;
	double f_;
	double my_;
	//---

	N = (double) count;
	sx = sxx = sy = syy = sxy = 0.0;

	for (i = 0; i < count; i++) {
		x_ = x[i];
		y_ = y[i];

		sx  += x_;
		sy  += y_;
		sxx += x_ * x_;
		syy += y_ * y_;
		sxy += x_ * y_;
	}

	d0 = (N * sxy - sx * sy);
	d1 = (N * sxx - sx*sx);

	if (d1 > -MIN_DENUMERATOR && d1 < MIN_DENUMERATOR) {
		if (d0 * d1 > 0) {
			m_ = LARGE_MVALUE;
		} else {
			m_ = -LARGE_MVALUE;
		}
	}
	else {
		m_ = d0 / d1;
//		m_ = (N * sxy - sx * sy) / (N * sxx - sx*sx);
	}

//	y_ = m x_ + b
//	b = y_ - m x_
	b_ = (sy - m_ * sx) / N; 

	*pm = m_;
	*pb = b_;

	if (pr2 != NULL) {
		sstot = 0.0; ssres = 0.0;
		my_ = sy / N;
		for (i = 0; i < count; i++) {
			x_ = x[i];
			y_ = y[i];

			f_ = m_ * x_ + b_;

			sstot += (y_ - my_) * (y_ - my_);
			ssres += (y_ - f_) * (y_ - f_);
		}

		*pr2 = 1 - ssres / sstot;
	}
	return 1;
}


/*!
 ********************************************************************************
 *	@brief      Quadratic (2nd order) Regression. get coefficient a0, a1, a2 of  yi = a0 + a1 xi + a2 xi^2
 *
 *	@param[in] x
 *					independent variable. array.
 *	@param[in] y
 *					dependent variable. array.
 *	@param[in] count
 *					array count.
 *
 *	@param[out]  a2
 *				 a2 value.
 *
 *	@param[out]  a1
 *				 a1 value.
 *
 *	@param[out]  a0
 *				 a0 value.
 *
 *	@param[out]  r2
 *				 r-square value.
 *
 *  @return		0: Fail. Numerical calculation error.
 *  			1: Success.
 *
 *	@author	    yhsuk
 *  @date       2016/06/08
 *******************************************************************************/
// http://adnoctum.tistory.com/104   
I32 caw_quadraticregression(
		double x[], double y[], int count,
		double *pa2, double *pa1, double *pa0,
		double *pr2)
{
    double alpha;
    alpha = 0;

    return caw_quadraticregression_a2_constraints(
        x, y, count, 
        alpha, 
        pa2, pa1, pa0, 
        pr2);
}


/*!
********************************************************************************
*	@brief      Quadratic (2nd order) Regression. get coefficient a0, a1, a2 of  yi = a0 + a1 xi + a2 xi^2
*	            with constrains (minimize (a2)^2 value)
*
*	@param[in]  x
*				independent variable. array.
*	@param[in]  y
*				dependent variable. array.
*	@param[in]  count
*				array count.
*	@param[in]  alpha
*				alpha value for (a2)^2
*   @param[out] a2
*				a2 value.
*	@param[out] a1
*				a1 value.
*	@param[out] a0
*				a0 value.
*	@param[out] r2
*				r-square value.
*
*   @return		0: Fail. Numerical calculation error.
*  			    1: Success.
*
*	@author	    yhsuk
*   @date       20200827
*******************************************************************************/
I32 caw_quadraticregression_a2_constraints(
		double x[], double y[], int count,
		double alpha,
		double *pa2, double *pa1, double *pa0,
		double *pr2)
{
    // http://adnoctum.tistory.com/104   
    // 
    // ei = (y_ - yi)^2
    // ei~ = ei + alpha * (a2)^2
    //
    // Minimize E~ = sum_i (ei~) = sum_i (ei) + alpha * (a2^2)
    //
    // d E~ / d(a2) = d E / d(a2) + 2N * alpha * a2
    //              = a2 (sum_i(xi^4) + 2*alpha) + ....)
    //   -> let's HACK. replace sum_i(xi^4) with (sum_i(xi^4) + 2*alpha).. :) 
    //

	I32 res;
	int i;
	double sx, sx2, sx3, sx4;
	double sy;
	double syx, syx2;
	double a2, a1, a0;
	double N;
	double x_, x2_, x3_, x4_, y_;
	double denominator;
	double a0p, a1p, a2p;
	//---

	N = (double) count;
	sx  = 0; sx2 = 0; sx3 = 0; sx4 = 0; sy  = 0; syx = 0; syx2= 0;

	for (i = 0; i < count; i++) {
		x_ = x[i];
		y_ = y[i];

		x2_ = x_ * x_;
		x3_ = x2_ * x_;
		x4_ = x2_ * x2_;

		sx  += x_;
		sx2 += x2_;
		sx3 += x3_;
		sx4 += x4_;

		sy  += y_;

		syx += y_ * x_;
		syx2+= y_ * x2_;
	}

	sx4 += (2*alpha);				// HACK.. :P


	//--
//	denominator = -N*sx4*sx2 + sx4*sx*sx + sx2*sx2*sx2 + sx3*sx3*N - 2*sx3*sx*sx2; 
//	a0p 		= -(sy*sx4*sx2 - sy*sx3*sx3 - sx*syx*sx4 + sx*sx3*syx2 - sx2*sx2*syx2 + sx2*sx3*syx);

//  L = syx; K = sy2; 
//	denominator = -N*X4*X2 +X4*X*X + X2*X2*X2 + X3*X3*N - 2*X3*X*X2;
//	a0p 		= -(Y*X4*X2 - Y*X3*X3 - X*L*X4 + X*X3*K - X2*X2*K + X2*X3*L);
//	a1p			= X*Y*X4 - X*K*X2 - L*N*X4 +X3*N*K - Y*X2*X3 +X2*X2*L;
//	a2p			= -(K*N*X2 - K*X*X - X2*X2*Y - X3*N*L + X3*X*Y + X*X2*L);
	{
		double X, X2, X3, X4;
		double Y, L, K;


		X = sx; X2 = sx2; X3 = sx3; X4 = sx4;
		Y = sy; L  = syx;  K  = syx2;

		denominator = -N*X4*X2 +X4*X*X + X2*X2*X2 + X3*X3*N - 2*X3*X*X2;
		a0p 		= -(Y*X4*X2 - Y*X3*X3 - X*L*X4 + X*X3*K - X2*X2*K + X2*X3*L);
		a1p			= X*Y*X4 - X*K*X2 - L*N*X4 +X3*N*K - Y*X2*X3 +X2*X2*L;
		a2p			= -(K*N*X2 - K*X*X - X2*X2*Y - X3*N*L + X3*X*Y + X*X2*L);

	}

	if (denominator < DENOMINATOR_MIN && denominator > -DENOMINATOR_MIN) {
		res = 0;
		goto func_exit;
	}

	a0 = a0p / denominator;
	a1 = a1p / denominator;
	a2 = a2p / denominator;

	{
		double my;
		double SStot, SSreg, SSres;
		double r2;

		SStot = 0; SSreg = 0; SSres = 0;
		my = sy/(double)count;
		for (i = 0; i < count; i++) {
			y_ = a0 + a1 * x[i] + a2 * x[i] * x[i];

			SStot += (y[i] - my) * (y[i] - my);
			SSreg += (y_ - my) * (y_ - my);
			SSres += (y[i] - y_) * (y[i] - y_);
		}
		r2 = 1 - SSres / SStot;
		*pr2 = r2;
	}

	*pa0 = a0;
	*pa1 = a1;
	*pa2 = a2;

	res = 1;
func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Error of Quadratic (2nd order) Regression. 
 *
 *	@param[in] x
 *					independent variable. array.
 *	@param[in] y
 *					dependent variable. array.
 *	@param[in] count
 *					array count.
 *	@param[in]  ma
 *				 coefficients.
 *	@param[out]  
 *				 coefficients.
 *	@param[in]  ma
 *				 coefficients.
 
 
 *
 *  @return		0: Fail. Numerical calculation error.
 *  			1: Success.
 *
 *	@author	    yhsuk
 *  @date       2020/0326
 *******************************************************************************/
double cr_quadregression_errcalc(double xx[], double yy[], U32 count, double ma[3], double *pmaxerr, U32 *pmaxerrindex)
{
	double meanerr;
	double yy_;
	double ey_;
	double ey_2;

	double maxerr2;
	U32    maxerr2index;
	U32 i;

	maxerr2 = 0;
	maxerr2index = 0;
	meanerr = 0;
	for (i = 0; i < count; i++) {
		yy_ = ma[2]*xx[i]*xx[i] + ma[1]*xx[i] + ma[0];
		ey_ = yy[i] - yy_;
		ey_2 = ey_*ey_;
		if (ey_2 > maxerr2) {
			maxerr2 = ey_2;
			maxerr2index = i;
		}
		meanerr += ey_2;
	}

	meanerr = (meanerr / count);
	meanerr = sqrt(meanerr);
	if (pmaxerr != NULL && pmaxerrindex != NULL) {
		*pmaxerr = sqrt(maxerr2);
		*pmaxerrindex = maxerr2index;
	}

	return meanerr;
}



I32 set_check_circle_RANSAC_id(U32 ransac_id)
{
	s_RRR = ransac_id;

	return 1;
}


I32 check_circle_RANSAC(		// 1: success, 0: fail.
		double *x,		// X
		double *y,		// Y
		int points,		// count of pixel
		double minr,	// Minimum r, maximum r
		double maxr,

		double *pcx,	// OUTPUT, x of center
		double *pcy,	// OUTPUT, y of center
		double *pcr,	// OUTPUT, radius of circle 
		double *per	// OUTPUT, normalized error
)
{
	I32 res;
	I32 i, j, k;
	I32 n;
	I32 halfpoints;
	double xv[3], yv[3];
	double cx, cy;
	double r;

	double rsx;
	double rsy;
	double rex;
	double rey;

	I32 count;

	I32 inlier[MAXRANSACITER];
	I32 maxinlier;
	I32 maxinlierid;
	double cxi, cyi;
	double ri;

	int itercount;

	I32 min_maxinlier;
	//---


// For Debugging: need to fix ransac seed. we don't want the randomness 
// #define RAND_FIX_TEST		1234
#if defined(RAND_FIX_TEST)
	srand(RAND_FIX_TEST);
	//srand(cr_gettickcount());
#endif

	res = 0;
	memset(inlier, 0, sizeof(I32) * MAXRANSACITER);
	
	halfpoints = points/2;


	rsx = 1e100;
	rsy = 1e100;

	rex = -1e100;
	rey = -1e100;

	cxi = 1e100;
	cyi = 1e100;
	ri  = 1e100;
	for (i = 0; i < points; i++) {
		if (rsx > x[i]) {rsx = x[i];}
		if (rex < x[i]) {rex = x[i];}
		if (rsy > y[i]) {rsy = y[i];}
		if (rey < y[i]) {rey = y[i];}
	}

	count = 0;
	maxinlier = 0;
	maxinlierid = 0;

	if (s_RRR == 1 || 
        s_RRR == 2 || 
        s_RRR == 3 || 
        s_RRR == 4 || 
        s_RRR == 5 || 
        s_RRR == 6)  {
		itercount = MAXRANSACITER;
	} else {
		itercount = points/3;
	}

	for (n = 0; n < itercount; n++) 
	{
		if (s_RRR == 1) {
			i = n%points;
			j = (rand() * points) / RAND_MAX;
			if (j >= points) { j = points-1;}

			k = (rand() * points) / RAND_MAX;
			if (k >= points) { k = points-1;}
		} else if (s_RRR == 2) {
			i = n%points;
			j = i + points/3;
			j += (int) ((rand() / (double)RAND_MAX) * 6 - 3);
			k = j + points/3;
			k += (int) ((rand() / (double)RAND_MAX) * 6 - 3);

			j = j % points;
			k = k % points;
		} else if (s_RRR == 3) {
			i = n%points;
			j = i + points/4;
			k = j + points/4;

			j = j % points;
			k = k % points;
		} else if (s_RRR == 4) {
			i = (rand() * points) / RAND_MAX;
			if (i >= points) { i = points-1;}

			j = (rand() * points) / RAND_MAX;
			if (j >= points) { j = points-1;}

			k = (rand() * points) / RAND_MAX;
			if (k >= points) { k = points-1;}
		} else 	if (s_RRR == 5) {
			i = n%points;
			j = i + (rand() * points) / RAND_MAX;
			j = j % points;

			k = i + (rand() * points) / RAND_MAX;
			k = k % points;
		} else if (s_RRR == 6) {
			i = (rand() * rand());
			i = i % points;

			j = (rand() * rand());
			j = j % points;
			if (i == j) {
				j = j + 1;
				j = j % points;
			}

			k = (rand() * rand());
			k = k % points;
			if (i == k || j == k) {
				continue;
			}
		} else {				// deterministic
			i = n%points;
			j = i + points/3;
			k = j + points/3;

			j = j % points;
			k = k % points;
		}

		if (i == j || j == k) {
			continue;
		}

		xv[0] = x[i]; yv[0] = y[i];
		xv[1] = x[j]; yv[1] = y[j];
		xv[2] = x[k]; yv[2] = y[k];

		res = make_circle_w3pts(xv, yv, &cx, &cy, &r);

		if (res != 0) {
			if ((cx > rsx && cx < rex) && (cy > rsy && cy < rey) && (r > minr && r < maxr)) {
				I32 m;
				double xm, ym;
				double r2;
				double r2min, r2max;
				double d2;

				// check 

				r2 = r*r;

				r2min = r2 * R2MINRATIO;
				r2max = r2 * R2MAXRATIO;
				for (m = 0; m < points; m++) {		// get inlier points
					xm = x[m]; ym = y[m];
					d2 = (xm-cx)*(xm-cx) + (ym-cy)*(ym-cy);

					if (d2 > r2min && d2 < r2max) {
						inlier[count]++;
					}
				}
				if (maxinlier < inlier[count]) {
					maxinlier = inlier[count];
					maxinlierid = count;
					cxi = cx;
					cyi = cy;
					ri = r;
				}
				count++;
			}
		}
		if (count >= MAXRANSACITER) {
			break;
		}
	}


	min_maxinlier = MIN_MAXINLIER;
	if (s_min_maxinlier_0) {
		min_maxinlier = MIN_MAXINLIER0;
	}


	if (maxinlier > (int)(points * MAXINLIER_MINRATE)
			&& maxinlier > min_maxinlier) 
	{
		double xi, yi;
		double r2;
		double r2min, r2max;
		double dtmp;
		double xx[POINTS_MAX];
		double yy[POINTS_MAX];

        //---------------- For least square error
		count = 0;

		r2 = ri * ri;
		r2min = r2 * R2MINRATIO;
		r2max = r2 * R2MAXRATIO;

		r2min *= 0.95;
		r2max *= 1.05;

		for (i = 0; i < points; i++) {
			xi = x[i];
			yi = y[i];
			dtmp = (xi - cxi) * (xi - cxi) + (yi - cyi) * (yi - cyi);

			if (dtmp > r2min && dtmp < r2max) {
				xx[count] = xi;
				yy[count] = yi;
				count++;
				if (count >= POINTS_MAX) {
					break;
				}
			}
		}

		if (count >= (I32)(MIN_IN_CLASS_RATIO*points)) {
			res = check_circle(		// 1: success, 0: fail.
				xx,
				yy,
				count,
				pcx,
				pcy,
				pcr,
				per);
		}
		else {
			res = 0;

			*pcx = 0;
			*pcy = 0;
			*pcr = 9999;

			*per = 1;
		}
	} else {
		res = 0;

		*pcx = 0;
		*pcy = 0;
		*pcr = 9999;

		*per = 1;
	}

	if (res != 0) {
		I32 res1;
		double cr;
		double er;

		double dx, dy;
		double dvalue;
		res1 = check_circle(		// 1: success, 0: fail.
				x,
				y,
				count,
				&cx,
				&cy,
				&cr,
				&er);

		dx = cx-*pcx;
		dy = cy-*pcy;

		dvalue = sqrt(dx*dx+dy*dy);
#ifdef DEBUG_CIRCLE
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" (%10lf %10lf : %10lf, %10lf) <-- (%10lf %10lf : %10lf, %10lf)    DELTA: (%10lf %10lf : %10lf)\n",
				*pcx, *pcy, *pcr, *per,
				cx, cy, cr, er,
				dx, dy, dvalue );
#else
		UNUSED(dvalue);
#endif
	}

	return res;
}

#undef EPSILON_MIN
#ifdef DEBUG_CIRCLE
#undef DEBUG_CIRCLE
#endif

#if defined (__cplusplus)
}
#endif
