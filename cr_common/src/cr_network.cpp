/*!
 *******************************************************************************
                                                                                
                    CREATZ Network API
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_network.cpp
	 @brief  Network API
	 @author YongHo Suk                                 
	 @date   2015/11/16 First Created
	 			2020/12/24 File name and location Changed

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/



/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>

#if defined(_WIN32)
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <winsock2.h>
#include <process.h>
#include <tchar.h>

#include <iphlpapi.h>	// IP Helper library (requires Microsoft SDK)
#else
#include <errno.h>
#endif

//-----------------
#include "cr_common.h"
#include "cr_osapi.h"
#include "cr_thread.h"

#include "cr_network.h"

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#if defined(_WIN32)
#pragma comment(lib, "iphlpapi.lib")
#pragma comment(lib, "ws2_32.lib")
#endif
/*----------------------------------------------------------------------------
 *	Description	: type definition
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/
#if defined(_WIN32)
static DWORD gAdapterIndex = 0;		// network adapter to use
#endif
/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/
#if defined (__cplusplus)
extern "C" {
#endif

/*!
 ********************************************************************************
 *	@brief      Init Socket API
 *  @return		Success / fail.
 *
 *	@author	    yhsuk
 *  @date       2015/1116
 *******************************************************************************/
CR_BOOL cr_InitSocketAPI ()
{
	CR_BOOL fStatus = CR_TRUE;
#if defined(_WIN32)		
	WORD wVersionRequested;
	WSADATA wsaData;

	// Set requested version of Windows socket API
	wVersionRequested = MAKEWORD( WS_VERSION, WS_SUBVERSION );

	// Initialize Windows socket API
	if (WSAStartup( wVersionRequested, &wsaData ) != 0)
	{
		//printf ("\nCannot initialize Windows Socket API\n\b");
		fStatus = CR_FALSE;
	}
#endif
	return fStatus;
}

/*!
 ********************************************************************************
 *	@brief      Close Socket API
 *  @return		Success / fail.
 *
 *	@author	    yhsuk
 *  @date       2015/1116
 *******************************************************************************/
CR_BOOL cr_CloseSocketAPI ()
{
	CR_BOOL fStatus = CR_TRUE;
#if defined(_WIN32)	
	if (WSACleanup() != 0)
	{
		//printf ("\nCannot close Windows Socket API\n\b");
		fStatus = CR_FALSE;
	}
#endif
	return fStatus;
}





/*!
 ********************************************************************************
 *	@brief      get the best interface by asking for any internet IP address string.

 *	@param [in] 	destipaddr_str: String of destination ip address 
 *  @return		    Index of network adapter (0..N-1)
 *
 *	@note 
 *
 *	@author	    yhsuk
 *  @date       2015/1123
 *******************************************************************************/
U32 cr_GetBestInterface_str(char *destIpAddr_str)
{
	U32 BestIfIndex = 0;
#if defined(_WIN32)	
	U32 DestAddr = ntohl(inet_addr(destIpAddr_str));   

	BestIfIndex = cr_GetBestInterface(DestAddr);
#else
	// TODO: LINUX_PORT: if needed
#endif	
	return BestIfIndex;
}

/*!
 ********************************************************************************
 *	@brief      get the best interface by asking for any internet IP address

 *	@param [in] 	destipaddr_str: String of destination ip address 
 *  @return		    Index of network adapter (0..N-1)
 *
 *	@note 
 *
 *	@author	    yhsuk
 *  @date       2015/1123
 *******************************************************************************/
U32 cr_GetBestInterface(U32 destAddr)
{
	unsigned long dwBestIfIndex = 0;

#if defined(_WIN32)
	if (GetBestInterface(htonl(destAddr), &dwBestIfIndex) != NO_ERROR) {
		dwBestIfIndex = 0xFFFF;
	}
#else
	// TODO: LINUX_PORT: if needed
#endif
	return (U32) dwBestIfIndex;
}
/*!
 ********************************************************************************
 *	@brief      Get MAC address and IP address
 *	This function is used to obtain the MAC and IP address of the network adapter
 *	@param [in] 	indexAdapter: Index of network adapter (0..N-1)
 *	@param [out] 	pMacHigh: Pointer to upper 16-bit of MAC address
 *	@param [out] 	pMacLow: Pointer to upper 32-bit of MAC address
 *	@param [out] 	pIpAddr: Pointer to IP address associated with selected network adapter
 *
 *  @return		CR_TRUE if successful, CR_FALSE otherwise
 *
 *	@note This code uses IP helper functions from Microsoft. This function must be
 *	               called before calling _SetIpAddress().
 *	@author	    yhsuk
 *  @date       2015/1116
 *******************************************************************************/
CR_BOOL cr_GetMacAddress (I32 indexAdapter, U16 *pMacHigh, U32 *pMacLow, U32 *pIpAddr, U32 *pSubnetMask)
{
	CR_BOOL fStatus = CR_TRUE;
#if defined(_WIN32)	
	PIP_ADAPTER_INFO pAdapterInfo = NULL;
	PIP_ADAPTER_INFO pAdapter = NULL;
	DWORD dwRetVal = 0;
	DWORD ulOutBufLen;
//	int index;

	// The following piece of code comes from MSDN, IP Helper, GetAdaptersInfo()
	// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/iphlp/iphlp/ip_helper_start_page.asp

	pAdapterInfo = (IP_ADAPTER_INFO *) malloc( sizeof(IP_ADAPTER_INFO) );
	ulOutBufLen = sizeof(IP_ADAPTER_INFO);

	// Make an initial call to GetAdaptersInfo to get
	// the necessary size into the ulOutBufLen variable

	//	NOTE: This call to the function is meant to fail, 
	// and is used to ensure that the ulOutBufLen variable 
	// specifies a size sufficient for holding all the 
	// information returned to pAdapterInfo. This is a 
	// common programming model for data structures and 
	// functions of this type.

	if (GetAdaptersInfo( pAdapterInfo, &ulOutBufLen) != ERROR_SUCCESS) 
	{
		free (pAdapterInfo);
		pAdapterInfo = (IP_ADAPTER_INFO *) malloc (ulOutBufLen);
	}

	if ((dwRetVal = GetAdaptersInfo( pAdapterInfo, &ulOutBufLen)) == NO_ERROR) 
	{
		pAdapter = pAdapterInfo;

		// Iterate to find proper adapter
//		index = indexAdapter;
		while (pAdapter != NULL)
		{
			if (indexAdapter == (I32)pAdapter->Index) {
				break;
			}
			pAdapter = pAdapter->Next;
		}

		if (pAdapter) 
		{
			//printf("Adapter #%d description = %s\n", indexAdapter, pAdapter->Description);
			//printf("\tOriginal IP Address: %s\n", pAdapter->IpAddressList.IpAddress.String);
			//printf("\tOriginal IP Mask: %s\n", pAdapter->IpAddressList.IpMask.String);
			//printf("\tadaptor index: 0x%08X\n", pAdapter->Index);

			// Keep adapter info
			gAdapterIndex = pAdapter->Index;
//			memcpy (pMAC, &pAdapter->Address, MAC_ADDRESS_LENGTH);	// copy MAC address
			if (pMacLow != NULL && pMacHigh != NULL) {
				*pMacLow = *(UINT32 *)(&pAdapter->Address[2]);
				*pMacHigh = *(UINT16 *)(&pAdapter->Address[0]) & 0x0000FFFF;
//				printf("\tMAC %02X.%02X.%02X.%02X.%02X.%02X\n", (*pMacHigh >> 8)&0xFF, (*pMacHigh)&0xFF, (*pMacLow >> 24)&0xFF, (*pMacLow >> 16)&0xFF, (*pMacLow >> 8)&0xFF, (*pMacLow)&0xFF);
			}
			if (pIpAddr != NULL)
			{
				*pIpAddr = ntohl( inet_addr( (const char *)( pAdapter->IpAddressList.IpAddress.String)));
			}
			if (pSubnetMask != NULL)
			{
				*pSubnetMask = ntohl( inet_addr( (const char *)( pAdapter->IpAddressList.IpMask.String)));
			}
		}
		else
		{
			//printf ("No Adapter found!\n\b");
			fStatus = CR_FALSE;
		}
	}
	else 
	{
		//printf("Call to GetAdaptersInfo failed.\n\b");
		fStatus = CR_FALSE;
	}

	// Release memory
	if (pAdapterInfo != NULL)
	{
		free( pAdapterInfo);
		pAdapterInfo = NULL;
	}
#else

	// TODO: LINUX_PORT: if needed

#endif
	return fStatus;
}	


#if 0 // not used
/*!
 ********************************************************************************
 *	@brief      Platform specific call to set the IP address of the device
 *				This function is used to assign an IP address to the requested
 *				network card.
 *	@param [in] ipCamera IP address to use for this camera
 *	@param [in] ipMask Subnet mask associated with IP address
 *
 *	@return CR_TRUE if successful, CR_FALSE otherwise
 *
 *	@note Must first have called _GetMacAddress to specify the network adapter to use
 *
 *	@author	    yhsuk
 *  @date       2015/1116
 *******************************************************************************/
CR_BOOL cr_SetIPAddress (U32 ipCamera, U32 ipMask)
{
	// Before calling AddIPAddress we use GetIpAddrTable to get
	// an adapter to which we can add the IP.
	PMIB_IPADDRTABLE pIPAddrTable = NULL;
	DWORD dwSize = 0;
	U32 dwRetVal;
	DWORD index;
	CR_BOOL fStatus = CR_TRUE;
	CR_BOOL fAddAddress = CR_TRUE;	// indicate if we need to add IP address
	static ULONG m_NTEContext = 0;		// used to add IP address to network interface

	// Variables where handles to the added IP will be returned
	ULONG NTEInstance = 0;
	LPVOID lpMsgBuf;

	// This is not a valid address under Windows.
	if (ipCamera == 0)
	{
		// Special case to recover pre-existing IP address
		if (m_NTEContext != 0)
		{
			// Delete the IP we  added using the NTEContext
			// variable where the handle was returned	
			dwRetVal = DeleteIPAddress(m_NTEContext);
			if (dwRetVal == NO_ERROR) 
			{
				//printf("Camera IP Address Deleted.\n");
			}
			else 
			{		
				//printf("ERROR: Call to DeleteIPAddress failed (0x%08X).\n\b", dwRetVal);
			}
			m_NTEContext = 0;
		}
		return CR_TRUE;
	}


	// Must check if the camera IP address to set already exists on this PC.
	// Otherwise, this generates an error. To do this, scan the list
	// of all IP address.
	pIPAddrTable = (MIB_IPADDRTABLE*) malloc( sizeof( MIB_IPADDRTABLE) );

	// Make an initial call to GetIpAddrTable to get the
	// necessary size into the dwSize variable
	if (GetIpAddrTable(pIPAddrTable, &dwSize, 0) == ERROR_INSUFFICIENT_BUFFER) 
	{
		free( pIPAddrTable );
		pIPAddrTable = (MIB_IPADDRTABLE *) malloc ( dwSize );
	}

	// Make a second call to GetIpAddrTable to get the
	// actual data we want
	if ( (dwRetVal = GetIpAddrTable( pIPAddrTable, &dwSize, 0 )) == NO_ERROR ) 
	{ 
		//printf("Number of IP address = %d\n", pIPAddrTable->dwNumEntries);
		for (index = 0; index < pIPAddrTable->dwNumEntries; index++)
		{
#if 0
// Following prints might be useful to list all network adpaters
// in the PC.
printf("IP address %d\n", index);
printf("\tIP Addr: 0x%08X\n", ntohl( pIPAddrTable->table[index].dwAddr));
printf("\tIP Mask: 0x%08X\n", ntohl( pIPAddrTable->table[index].dwMask));
printf("\tIndex:   0x%08X\n", pIPAddrTable->table[index].dwIndex);
printf("\tBCast:   0x%08X\n", ntohl( pIPAddrTable->table[index].dwBCastAddr));
printf("\tReasm:   %ld\n", pIPAddrTable->table[index].dwReasmSize);
printf("\tType:    %d\n", pIPAddrTable->table[index].wType);
#endif

			// Check if the camera IP address already exists in the list. If so, don't add it.
			if (ntohl( pIPAddrTable->table[index].dwAddr) == ipCamera)
			{
				fAddAddress = CR_FALSE;
			}

		}
	}
	else 
	{
		//printf("Call to GetIpAddrTable failed.\n");
	}

	// Check if we want to allocate the current IP address
	if (fAddAddress == CR_TRUE)
	{
		if ( (dwRetVal = AddIPAddress(htonl (ipCamera), htonl(ipMask), gAdapterIndex /*pIPAddrTable->table[indexAdapter].dwIndex*/, &m_NTEContext, &NTEInstance) ) == NO_ERROR) 
		{
			//printf("IP address added.\n");
		}
		else 
		{
			//printf("Error adding IP address (status = 0x%08X).\n", dwRetVal);
			if (FormatMessage( 
				FORMAT_MESSAGE_ALLOCATE_BUFFER | 
				FORMAT_MESSAGE_FROM_SYSTEM | 
				FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,
				dwRetVal,
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
				(LPTSTR) &lpMsgBuf,
				0,
				NULL )) 
			{
				//printf("\tError: %s", lpMsgBuf);
			}
			LocalFree( lpMsgBuf );
			fStatus = CR_FALSE;		// unable to add address
		}
	}

	// Release memory
	if (pIPAddrTable != NULL)
	{
		free( pIPAddrTable);
		pIPAddrTable = NULL;
	}

	return fStatus;
}
#endif

//! Return last socket error
/*!
	This function is used retreive the last socket error value.
	\return Socket status code
	\note None
*/
/*!
 ********************************************************************************
 *	@brief      Return last socket error
 *				This function is used retreive the last socket error value.
 *
 *	@return Socket status code
 *
 *	@author	    yhsuk
 *  @date       2015/1116
 *******************************************************************************/
I32 cr_GetSocketError () 
{
#if defined(_WIN32)
	return WSAGetLastError();
#else
	return errno;
#endif
}

/*!
 ********************************************************************************
 *	@brief      Return last socket error
 *				This function is used retreive the last socket error value.
 *
 *	@return Socket status code
 *
 *	@author	    yhsuk
 *  @date       2015/1116
 *******************************************************************************/
I32 cr_CloseSocket(SOCKET hSocket) 
{
#if defined(_WIN32)
	return closesocket(hSocket);
#else
	return close(hSocket);
#endif
}


#if defined (__cplusplus)
}
#endif
