/*!
 *******************************************************************************
                                                                                
                   Creatz Camera Sensor 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2010, 2011 by Creatz Inc.
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc. \n

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_bugtrap.cpp
	 @brief  Creatz Camera Golf Sensor Bugtrap 
	 @author YongHo Suk                                 
	 @date   2015/07/06 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/



/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#if defined(_DEBUG)
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

//#include <windows.h>
//#include <tchar.h>
////#include <atlstr.h> 
////#include <direct.h>

#include "cr_common.h"
#include "cr_osapi.h"

#include <windows.h>
#include <tchar.h>
#include <atlstr.h> 
#include <direct.h>

#if defined(_DEBUG)
//#define USE_BUGTRAP
//#define USE_BUGTRAP_SEH
#else
//#define USE_BUGTRAP			
//#define USE_BUGTRAP_SEH
#endif
//

#include "BugTrap.h"
#pragma warning(disable:4091)

#include "dbghelp.h"
#pragma warning(default:4091)

#if defined(USE_BUGTRAP) || defined(USE_BUGTRAP_SEH)
//#include "BugTrap.h"
//#include "dbghelp.h"
#if defined(_WIN64)
#pragma comment(lib, "BugTrapU-x64.lib")
#else
#pragma comment(lib, "BugTrapU.lib") // Link to Unicode DLL
#endif
#endif

/*----------------------------------------------------------------------------
 *	Description	: defines datatype
 -----------------------------------------------------------------------------*/
typedef struct _iana_bugtrap {
	TCHAR	vdstr[128];				// version/date string
	TCHAR	ipaddr[128];			// IP address
	TCHAR	creulogfile[256];		// CREU log file (Full path)
	I32		portnum;				// Port number


	INT_PTR	hBtLog;


	//---- Teamviewer Data..
	TCHAR	regpathh[256];			// Reg Path for HKLM, teamviewer
	I32		regpathugood;
	TCHAR	regpathu[256];			// Reg Path for HK_USER, teamviewer
	DWORD	clientid;

} iana_bugtrap_t;



/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

#if !defined(USE_BUGTRAP) && !defined(USE_BUGTRAP_SEH)
HAND iana_bugtrap_create(char *ipaddr, I32 portnum, I32 vmajor, I32 vminor, I32 vbuild, char *logfn)
{
	ipaddr; portnum; vmajor; vminor; vbuild; logfn;

	return NULL;
}
void iana_bugtrap_installSehFilter(void)
{
}
void iana_bugtrap_delete(HAND hcbgt, I32 sendsnapshot)
{
	hcbgt; 
	sendsnapshot;
}

#else
void iana_bugtrap_getTVinfo(HAND hcbgt);
void iana_bugtrap_getTVreg(HAND hcbgt);

void CALLBACK iana_bugtrap_MyErrHandler(INT_PTR nErrHandlerParam);

static BOOL Is64BitWindows();
static BOOL IsCurrentProcess64bit();
static BOOL IsCurrentProcessWow64();
static TCHAR *FindRegKey(HKEY rootKey, TCHAR *pSubKeyName,TCHAR *pFindKey);





HAND iana_bugtrap_create(char *ipaddr, I32 portnum, I32 vmajor, I32 vminor, I32 vbuild, char *logfn)
{
	iana_bugtrap_t *pcbgt;
//	SYSTEMTIME st;
	TCHAR	tstr0[1024];
	TCHAR	fn[1024];
//	char *creulogfn;

//--
//__debugbreak();
	pcbgt = (iana_bugtrap_t *)malloc(sizeof(iana_bugtrap_t));
	if (pcbgt == NULL) goto func_exit;

	memset(pcbgt, 0, sizeof(iana_bugtrap_t));
	iana_bugtrap_getTVinfo(pcbgt);

#if defined(USE_BUGTRAP)
//	GetLocalTime( &st);
#if defined(_DEBUG)
//	wsprintf(&pcbgt->vdstr[0], _T("V%d-%d-%d_D%4d%02d%02d-DEBUGMODE"), vmajor, vminor, vbuild, st.wYear, st.wMonth, st.wDay);
	wsprintf(&pcbgt->vdstr[0], _T("T%09d_V%d-%d-%d-DEBUGMODE") , pcbgt->clientid, vmajor, vminor, vbuild);
	
#else
//	wsprintf(&pcbgt->vdstr[0], _T("V%d-%d-%d_D%4d%02d%02d"), vmajor, vminor, vbuild, st.wYear, st.wMonth, st.wDay);
	wsprintf(&pcbgt->vdstr[0], _T("T%09d_V%d-%d-%d") , pcbgt->clientid, vmajor, vminor, vbuild);
#endif
	wsprintf(tstr0, _T("%s-CRASH"), &pcbgt->vdstr[0]);

	wsprintf(&pcbgt->ipaddr[0], _T("%s"), CA2W(ipaddr).m_psz);
	if (portnum <= 0) {
		portnum =9999;					// Default port number for BugTrap 
	}
	pcbgt->portnum = portnum;

	BT_SetAppVersion(tstr0);

//	BT_SetAppName(_T("SCAM_XYZ"));
	BT_SetAppName(_T("ZCAM3_EYEXO_CORE"));

//	BT_SetSupportEMail(_T("your@email.com"));
	//BT_SetFlags(BTF_DETAILEDMODE | BTF_EDITMAIL | BTF_ATTACHREPORT| BTF_SCREENCAPTURE);
	BT_SetFlags(BTF_DETAILEDMODE | BTF_ATTACHREPORT | BTF_SCREENCAPTURE);
	//BT_SetFlags(BTF_DETAILEDMODE);
//	BT_SetSupportURL(_T("http://www.www.www"));

	// = BugTrapServer ===========================================
	BT_SetDumpType( MiniDumpWithDataSegs | MiniDumpWithIndirectlyReferencedMemory );

//	BT_SetDumpType(MiniDumpNoDump);				// 하하핫.


	//wsprintf(tstr1, _T("%s\\%s"), tstr0, CA2W(creulogfn).m_psz);
	BT_SetSupportServer(pcbgt->ipaddr, (SHORT)portnum);
//	BT_SetSupportServer(_T("123.123.123.123"), (SHORT)portnum);
	BT_SetExitMode(BTEM_EXECUTEHANDLER);

	// Force BugTrap to submit reports to support server w/o GUI
	BT_SetActivityType(BTA_SENDREPORT);
	
//	BT_InstallSehFilter();

	//--- Logfile
	pcbgt->hBtLog = BT_OpenLogFile(NULL, BTLF_TEXT);
	BT_SetLogSizeInEntries(pcbgt->hBtLog, 100);
	BT_SetLogFlags(pcbgt->hBtLog, BTLF_SHOWLOGLEVEL | BTLF_SHOWTIMESTAMP);
	BT_SetLogEchoMode(pcbgt->hBtLog, BTLE_STDERR | BTLE_DBGOUT);
	BT_SetLogLevel(pcbgt->hBtLog, BTLL_VERBOSE);

	BT_AddLogFile(BT_GetLogFileName(pcbgt->hBtLog));

	{ //----- Additional Files... :)
		GetCurrentDirectory(MAX_PATH, tstr0); 
		//-- CRLOG
		wsprintf(&pcbgt->creulogfile[0], _T("%s\\%s"), tstr0, CA2W(logfn).m_psz);
		BT_AddLogFile(&pcbgt->creulogfile[0]);

		_mkdir("MetaData");
		//-- Cam files
		wsprintf(&fn[0], _T("%s\\MetaData\\%s"), tstr0, _T("img_c.jpg")); BT_AddLogFile(&fn[0]);
		wsprintf(&fn[0], _T("%s\\MetaData\\%s"), tstr0, _T("img_s.jpg")); BT_AddLogFile(&fn[0]);
		wsprintf(&fn[0], _T("%s\\MetaData\\%s"), tstr0, _T("img_c1.jpg")); BT_AddLogFile(&fn[0]);
		wsprintf(&fn[0], _T("%s\\MetaData\\%s"), tstr0, _T("img_s1.jpg")); BT_AddLogFile(&fn[0]);
		wsprintf(&fn[0], _T("%s\\MetaData\\%s"), tstr0, _T("img_LS_c.jpg")); BT_AddLogFile(&fn[0]);
		wsprintf(&fn[0], _T("%s\\MetaData\\%s"), tstr0, _T("img_LS_s.jpg")); BT_AddLogFile(&fn[0]);
		wsprintf(&fn[0], _T("%s\\MetaData\\%s"), tstr0, _T("img_LS_c1.jpg")); BT_AddLogFile(&fn[0]);
		wsprintf(&fn[0], _T("%s\\MetaData\\%s"), tstr0, _T("img_LS_s1.jpg")); BT_AddLogFile(&fn[0]);

		//-- MetaFiles
		wsprintf(&fn[0], _T("%s\\%s"), tstr0, _T("coordtrans_C.txt")); BT_AddLogFile(&fn[0]);
		wsprintf(&fn[0], _T("%s\\%s"), tstr0, _T("coordtrans_L.txt")); BT_AddLogFile(&fn[0]);
		wsprintf(&fn[0], _T("%s\\%s"), tstr0, _T("coordtrans_LS_C.txt")); BT_AddLogFile(&fn[0]);
		wsprintf(&fn[0], _T("%s\\%s"), tstr0, _T("coordtrans_LS_L.txt")); BT_AddLogFile(&fn[0]);
		wsprintf(&fn[0], _T("%s\\%s"), tstr0, _T("kea.ini")); BT_AddLogFile(&fn[0]);
		//wsprintf(&fn[0], _T("%s\\%s"), tstr0, _T("kea.log")); BT_AddLogFile(&fn[0]);
		
		wsprintf(&fn[0], _T("%s\\%s"), tstr0, _T("ct_0.txt")); BT_AddLogFile(&fn[0]);
		wsprintf(&fn[0], _T("%s\\%s"), tstr0, _T("ct_1.txt")); BT_AddLogFile(&fn[0]);
		wsprintf(&fn[0], _T("%s\\%s"), tstr0, _T("scam.ini")); BT_AddLogFile(&fn[0]);

		//-- TV Files
		wsprintf(&fn[0], _T("%s\\MetaData\\%s"), tstr0, _T("th.txt")); BT_AddLogFile(&fn[0]);			// HKLM TV file
		wsprintf(&fn[0], _T("%s\\MetaData\\%s"), tstr0, _T("tu.txt")); BT_AddLogFile(&fn[0]);			// H_USER TV file
	}
	//-- 

	{	// log TV id..
		unsigned int n2, n1, n0;

		n0 = (unsigned int)pcbgt->clientid % 1000;
		n1 = ((unsigned int)pcbgt->clientid / 1000) % 1000;
		n2 = ((unsigned int)pcbgt->clientid / 1000000) % 1000;
		//creu_log(NULL, 3,  "TV ClientId: %09d (%03d %03d %03d)", pcbgt->clientid, n2, n1, n0);
	}
	BT_SetPreErrHandler((BT_ErrHandler)iana_bugtrap_MyErrHandler, (INT_PTR)pcbgt);
#else

//	BT_SetFlags(BTF_DETAILEDMODE | BTF_ATTACHREPORT);
		ipaddr; portnum; vmajor; vminor; vbuild; logfn;

		//st; 
		tstr0; fn;
#endif

func_exit:
	return (HAND)pcbgt;
}

void iana_bugtrap_installSehFilter(void)
{
#if defined(USE_BUGTRAP_SEH)
	BT_SetDumpType( MiniDumpWithDataSegs | MiniDumpWithIndirectlyReferencedMemory );		// Crash Report Enable 시에는 Minidump 를 정상적으로..
	BT_InstallSehFilter();
#endif
}

void iana_bugtrap_delete(HAND hcbgt, I32 sendsnapshot)
{
	TCHAR	tstrbuf[128];
	iana_bugtrap_t *pcbgt;

	//--
	if (hcbgt == NULL) goto func_exit;

	pcbgt = (iana_bugtrap_t *) hcbgt;
#if defined(USE_BUGTRAP)
	if (sendsnapshot) {
		{
			// 1) Delete th/tu file 
			DeleteFile(_T("MetaData\\th.txt"));
			DeleteFile(_T("MetaData\\tu.txt"));

			// 2) Get th/tu file
			iana_bugtrap_getTVreg(hcbgt);
		}
		wsprintf(tstrbuf, _T("%s-LOG"), &pcbgt->vdstr[0]);
		BT_SetFlags(BTF_DETAILEDMODE);
		BT_SetAppVersion(tstrbuf);

		BT_SendSnapshot();					// Send snapshot!


		{
			// 1) Delete 
			DeleteFile(_T("MetaData\\th.txt"));
			DeleteFile(_T("MetaData\\tu.txt"));

			DeleteFile(_T("MetaData\\img_c1.jpg"));
			DeleteFile(_T("MetaData\\img_s1.jpg"));
			DeleteFile(_T("MetaData\\img_LS_c.jpg"));
			DeleteFile(_T("MetaData\\img_LS_s.jpg"));
			DeleteFile(_T("MetaData\\img_LS_c1.jpg"));
			DeleteFile(_T("MetaData\\img_LS_s1.jpg"));
		}

	}
#else
	sendsnapshot;
	tstrbuf;
#endif

	free(pcbgt);
func_exit:
	return;
}

void iana_bugtrap_getTVinfo(HAND hcbgt)
{
	iana_bugtrap_t *pcbgt;
	TCHAR buf[2048];

	//--
	if (hcbgt == NULL) goto func_exit;

	pcbgt = (iana_bugtrap_t *) hcbgt;

	if (Is64BitWindows()) {
		wsprintf(buf, _T("SOFTWARE\\Wow6432Node\\TeamViewer"));
	} else {
		wsprintf(buf, _T("SOFTWARE\\TeamViewer"));
	}

	wsprintf(pcbgt->regpathh, _T("HKEY_LOCAL_MACHINE\\%s") , buf);
	{
		 HKEY hkey;
		 DWORD clientid;
		 DWORD Size;

		 Size = sizeof(LONG);
		 if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, buf, 0, KEY_ALL_ACCESS, &hkey) == ERROR_SUCCESS) {
			  RegQueryValueEx(hkey, _T("ClientID"), 0, NULL, (LPBYTE)&clientid, (LPDWORD)&Size);
			  RegCloseKey(hkey);
		  } else {
			  clientid = 0;			// -_-;
		  }

		 pcbgt->clientid = clientid;
	}


	{

		HKEY hKey;
		TCHAR *pPath;

		DWORD dwIndex = 0;
		LONG lRet;
		DWORD cbName = MAX_PATH;
		TCHAR szSubKeyName[MAX_PATH];

		pcbgt->regpathugood = 0;
		if( RegOpenKeyEx( HKEY_USERS, NULL, 0, KEY_READ, &hKey ) != ERROR_SUCCESS ) {
			//
		} else {
			while(( lRet = RegEnumKeyEx( hKey, dwIndex, szSubKeyName, &cbName, NULL, NULL, NULL, NULL )) != ERROR_NO_MORE_ITEMS ) {
				if( lRet == ERROR_SUCCESS ) {
					if ((_tcsstr(szSubKeyName, _T("_Classes")) == NULL)
							&& (_tcsstr(szSubKeyName, _T("_CLASSES")) == NULL)
							&& (_tcsstr(szSubKeyName, _T("_classes")) == NULL)) {
						pPath = FindRegKey(HKEY_USERS, szSubKeyName, _T("TeamViewer"));
						if (pPath != NULL) {
							wsprintf(pcbgt->regpathu, _T("HKEY_USERS\\%s") , pPath);
							pcbgt->regpathugood = 1;
							break;
						}
					}
				}
				dwIndex++;
				cbName = MAX_PATH;
			}
			RegCloseKey(hKey);
		}
	}
func_exit:
	return;
}


void iana_bugtrap_getTVreg(HAND hcbgt)
{
	TCHAR	cmd[128];
	iana_bugtrap_t *pcbgt;

	//--
	if (hcbgt == NULL) goto func_exit;

	pcbgt = (iana_bugtrap_t *) hcbgt;

	if (pcbgt->clientid != 0) {
//		TCHAR	regpath[256];			// Reg Path for HKLM, teamviewer
		wsprintf(cmd, _T("export %s MetaData\\th.txt /Y"), pcbgt->regpathh);
		ShellExecute(NULL, _T("open"), _T("reg.exe"), cmd, NULL, SW_HIDE ) ;

		wsprintf(cmd, _T("export %s MetaData\\tu.txt /Y"), pcbgt->regpathu);
		ShellExecute(NULL, _T("open"), _T("reg.exe"), cmd, NULL, SW_HIDE ) ;

		cr_sleep(50);
	}
	
func_exit:
	return;
}



void CALLBACK iana_bugtrap_MyErrHandler(INT_PTR nErrHandlerParam)
{
	//iana_bugtrap_t *pcbgt;

	//pcbgt = (iana_bugtrap_t *)nErrHandlerParam;

	iana_bugtrap_getTVreg((HAND)nErrHandlerParam);

}



//-----
static BOOL IsCurrentProcess64bit()
{
#if defined(_WIN64)
    return TRUE;
#else
    return FALSE;
#endif
}

static BOOL IsCurrentProcessWow64()
{
    BOOL bIsWow64 = FALSE;
    typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS)(HANDLE, PBOOL);
    LPFN_ISWOW64PROCESS fnIsWow64Process;

    fnIsWow64Process = (LPFN_ISWOW64PROCESS)
                                       GetProcAddress(GetModuleHandle(TEXT("kernel32")), "IsWow64Process");
    if (!fnIsWow64Process)
        return FALSE;

    return fnIsWow64Process(GetCurrentProcess(), &bIsWow64) && bIsWow64;
}


static BOOL Is64BitWindows()
{
    if (IsCurrentProcess64bit())
        return TRUE;

    return IsCurrentProcessWow64();
}

static TCHAR *FindRegKey(HKEY rootKey, TCHAR *pSubKeyName,TCHAR *pFindKey)
{
	HKEY hKey;
//	DWORD dwIndex = 0;

	//DWORD cbName = MAX_PATH;
	
	TCHAR *pKeyPath;
	TCHAR szSubKeyFullPath[2048];
	static TCHAR sSubKeyFullPath[2048];


	if((wcslen(pSubKeyName) > MAX_PATH) || ( ::RegOpenKeyEx( HKEY_USERS, pSubKeyName, 0, KEY_READ, &hKey ) != ERROR_SUCCESS) )
	{
		return NULL;
	}
	else
	{
		 
		
		DWORD dwIndex = 0;
		LONG lRet;
//		DWORD cbName = MAX_PATH;
//		TCHAR szSubKeyName[MAX_PATH];
		DWORD cbName = 2048;
		TCHAR szSubKeyName[2048];

		while(( lRet = ::RegEnumKeyEx( hKey, dwIndex, szSubKeyName, &cbName, NULL, NULL, NULL, NULL )) != ERROR_NO_MORE_ITEMS )
		{
			if( lRet == ERROR_SUCCESS )
			{
				memset(szSubKeyFullPath, 0, 2048);
#pragma warning(disable:4996)
				swprintf(szSubKeyFullPath, L"%s\\%s",pSubKeyName,szSubKeyName);
				if (wcscmp(szSubKeyName,pFindKey) == 0)
				{
					swprintf(sSubKeyFullPath, L"%s\\%s",pSubKeyName,szSubKeyName);
					return sSubKeyFullPath;
				}

				
				pKeyPath = FindRegKey(rootKey, szSubKeyFullPath,pFindKey);
				if (pKeyPath != NULL)
					return pKeyPath;
#pragma warning(default:4996)

			}

			dwIndex++;
			cbName = MAX_PATH;
		}
		::RegCloseKey(hKey);
		
	}


	return NULL;
}
#endif

