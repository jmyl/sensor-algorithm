/*!
 *******************************************************************************
                                                                                
                   Creatz  Camera 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_chessmatch.c
	 @brief  Chessboard match
	 @author YongHo Suk                                 
	 @date   2012/03/12 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#endif

#include "cr_common.h"
#include "cr_chessmatch.h"

#include "assert.h"

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#define MAXCOL	32
#define MAXROW	32

/*----------------------------------------------------------------------------
 *	Description	: type definition
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/
static double chessdata[MAXCOL * MAXROW * 2];


/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/



/*!
 ********************************************************************************
 *	@brief      Re-order Chessboard result.
 *	            (x0, y0) (x1, y0), ..., (xM-1, y0)
 *              (x0, y1) (x1, y1), ..., (xM-1, y1)
 *              ...
 *              (x0, yN-1), ...,        (xM-1, yN-1)
 *	@param[in]  data
 *				un-ordered data pair  (xm, yn)
 *
 *	@param[in]  width
 *	@param[in]  height
 *	@param[out] odata
 *	            ordered data (x, y)  (caller should alloc memory)
 *  @return		none.
 *
 *	@author	    yhsuk
 *  @date       2012/03/12
 *******************************************************************************/
void cr_chessboardreorder2(
		double *data,
		U32 width,
		U32 height,
		double *odata)
{
	U32 i, j, k;
	double x_, y_;

	double fv;
	double minv;
	U32 minindex;

//	double maxv;
//	U32 maxindex;
	U32 count;
	double *meansw;
//	U32 alloced;

	count = width * height;
	/*
	len = column * row;

	if (len > (MAXCOL * MAXROW)) {
		pd = (float *) malloc(count * 2 * sizeof(double));
		alloced = 1;
	} else {
		pd = (double *) chessdata;
		alloced = 0;
	}
	*/

	meansw = (double *) malloc(sizeof(double) * height);
#if 0
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-------------------------\n");
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-------------------------\n");
	for (i = 0; i < height; i++) {
		for (j = 0; j < width; j++) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d, %2d]: (%10f, %10f)\n",
				j, i,
				*(data + (i * width + j) * 2),
				*(data + (i * width + j) * 2 + 1));
		}
		//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
	}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "--\n");
#endif



	memcpy(odata, data, width * height * 2 * sizeof(double));
	
	//--- y-based sort.  use selection sort. -_-;
	for (i = 0; i < height; i++) {						// mean of y-values;
		double mv;

		mv = 0.0;
		for (j = 0; j < width; j++) {
			mv += *(data + (i * width + j)*2 + 1);			// 
		}
		
		*(meansw+i)= mv/width;
	}

	for (i = 0; i < height - 1; i++) {
		minv = *(meansw+i);
		minindex = i;
		for (j = i+1; j < height; j++) {
			fv = *(meansw+j);
			if (minv > fv) {							// get min value.
				minindex = j;
				minv = fv;
			}
		}
		if (minindex != i) {						// exchange..
			for (j = 0; j < width; j++) {
				x_ = *(odata + (i*width + j) * 2 + 0);
				y_ = *(odata + (i*width + j) * 2 + 1);


				*(odata + (i*width + j) * 2 + 0) = *(odata + (minindex*width + j) * 2 + 0);
				*(odata + (i*width + j) * 2 + 1) = *(odata + (minindex*width + j) * 2 + 1);


				*(odata + (minindex*width + j) * 2 + 0) = x_;
				*(odata + (minindex*width + j) * 2 + 1) = y_;
			}
		}
	}

#if 0
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------------------------- 1st result\n");
	for (i = 0; i < height; i++) {
		for (j = 0; j < width; j++) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d, %2d]: (%10f, %10f)\n",
				j, i,
				*(odata + (i * width + j) * 2),
				*(odata + (i * width + j) * 2 + 1));
		}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
	}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
#endif


	//---- x-based sort for each y... 
	for (k = 0; k < height ; k++)
	{
		for (i = 0; i < width - 1; i++)
		{
			minv = *(odata + (k * width + i) * 2 + 0);					// <x, y>, get x
			minindex = i;

			for (j = i + 1; j < width; j++) {
				fv = *(odata + (k * width + j) * 2 + 0);				// <x, y>, get x
							
				if (minv > fv) {							// get mac value.
					minindex = j;
					minv = fv;
				}
				
			}
			if (minindex != i) {						// exchange..
				x_ = *(odata + (k*width + i) * 2);
				y_ = *(odata + (k*width + i) * 2 + 1);

				*(odata + (k*width + i) * 2) = *(odata + (k*width + minindex) * 2);
				*(odata + (k*width + i) * 2 + 1) = *(odata + (k*width + minindex) * 2 + 1);

				*(odata + (k*width + minindex) * 2) = x_;
				*(odata + (k*width + minindex) * 2 + 1) = y_;
			}
		}
	} 

#if 0
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------------------------- 2nd result\n");
	for (i = 0; i < height; i++) {
		for (j = 0; j < width; j++) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d, %2d]: (%10f, %10f)\n",
				j, i,
				*(odata + (i * width + j) * 2),
				*(odata + (i * width + j) * 2 + 1));
		}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
	}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
#endif

	free(meansw);
}

#if 0								// works pretty well for CIRCLE..
void cr_chessboardreorder(
		double *data,
		U32 width,
		U32 height,
		double *odata)
{
	U32 i, j, k;
	double x_, y_;

	double fv;
	double minv;
	U32 minindex;

	double maxv;
	U32 maxindex;
	U32 count;
//	U32 alloced;

	count = width * height;
	/*
	len = column * row;

	if (len > (MAXCOL * MAXROW)) {
		pd = (float *) malloc(count * 2 * sizeof(double));
		alloced = 1;
	} else {
		pd = (double *) chessdata;
		alloced = 0;
	}
	*/

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-------------------------\n");
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-------------------------\n");
	for (i = 0; i < height; i++) {
		for (j = 0; j < width; j++) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d, %2d]: (%10f, %10f)\n",
				j, i,
				*(data + (i * width + j) * 2),
				*(data + (i * width + j) * 2 + 1));
		}
		//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
	}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "--\n");



	memcpy(odata, data, width * height * 2 * sizeof(double));
	//--- y-based sort.  use selection sort. -_-;
	for (i = 0; i < count - 1; i++) {
		minv = *(odata + i * 2+1);					// <x, y>, get y
		minindex = i;
		for (j = i+1; j < count; j++) {
			fv = *(odata + j * 2+1);					// <x, y>, get y
			if (minv > fv) {							// get min value.
				minindex = j;
				minv = fv;
			}
		}
		if (minindex != i) {						// exchange..
			x_ = *(odata + i * 2);
			y_ = *(odata + i * 2 + 1);

//			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "(%d <-> %d), <%f, %f> <-> <%f ,%f>\n", i, minindex, x_, y_, *(odata + minindex * 2), *(odata + minindex * 2+1));

			*(odata + i * 2)     = *(odata + minindex * 2);
			*(odata + i * 2 + 1) = *(odata + minindex * 2 + 1);

			*(odata + minindex * 2)     = x_;
			*(odata + minindex * 2 + 1) = y_;
		}
	}

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------------------------- 1st result\n");
	for (i = 0; i < height; i++) {
		for (j = 0; j < width; j++) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d, %2d]: (%10f, %10f)\n",
				j, i,
				*(odata + (i * width + j) * 2),
				*(odata + (i * width + j) * 2 + 1));
		}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
	}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");


	//---- x-based sort for each y... 
	for (k = 0; k < height ; k++)
	{
		for (i = 0; i < width - 1; i++)
		{
			maxv = *(odata + (k * width + i) * 2 + 0);					// <x, y>, get x
			maxindex = i;

			for (j = i + 1; j < width; j++) {
				fv = *(odata + (k * width + j) * 2 + 0);				// <x, y>, get x
							
				if (maxv < fv) {							// get mac value.
					maxindex = j;
					maxv = fv;
				}
				
			}
			if (maxindex != i) {						// exchange..
				x_ = *(odata + (k*width + i) * 2);
				y_ = *(odata + (k*width + i) * 2 + 1);

				*(odata + (k*width + i) * 2) = *(odata + (k*width + maxindex) * 2);
				*(odata + (k*width + i) * 2 + 1) = *(odata + (k*width + maxindex) * 2 + 1);

				*(odata + (k*width + maxindex) * 2) = x_;
				*(odata + (k*width + maxindex) * 2 + 1) = y_;
			}
		}
	} 


	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------------------------- 2nd result\n");
	for (i = 0; i < height; i++) {
		for (j = 0; j < width; j++) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d, %2d]: (%10f, %10f)\n",
				j, i,
				*(odata + (i * width + j) * 2),
				*(odata + (i * width + j) * 2 + 1));
		}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
	}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
}
#endif

/*!
 ********************************************************************************
 *	@brief      Re-order Chessboard result.
 *	            (x0, y0) (x0, y1), ..., (x0, yN-1)
 *              (x1, y0) (x1, y1), ..., (x1, yN-1)
 *              ...
 *              (xM-1, y0), ...,        (xM-1, yN-1)
 *	@param[in]  data
 *				un-ordered data pair  (xm, yn)
 *
 *	@param[in]  column
 *	            column of matrix
 *	@param[in]  row
 *				row of matrix
 *	@param[out] odata
 *	            ordered data (x, y)  (caller should alloc memory)
 *  @return		none.
 *
 *	@author	    yhsuk
 *  @date       2012/03/12
 *******************************************************************************/
void cr_chessboardreorder(
		double *data,
		U32 column,
		U32 row,
		double *odata)
{
	U32 i, j, k;
	double x_, y_;

	double fv;
	double minv;
	U32 minindex;

	double maxv;
	U32 maxindex;
	U32 count;
//	U32 alloced;

	count = column * row;
	/*
	len = column * row;

	if (len > (MAXCOL * MAXROW)) {
		pd = (float *) malloc(count * 2 * sizeof(double));
		alloced = 1;
	} else {
		pd = (double *) chessdata;
		alloced = 0;
	}
	*/

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-------------------------\n");
	for (i = 0; i < row; i++) {
		for (j = 0; j < column; j++) {

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d, %2d]: (%10f, %10f)\n",
				i, j,
				*(data + (i * column + j) * 2),
				*(data + (i * column + j) * 2 + 1));
		}
		//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
	}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "--\n");



	memcpy(odata, data, column * row * 2 * sizeof(double));
	//--- x-based sort.  use selection sort. -_-;
	for (i = 0; i < count - 1; i++) {
		minv = *(odata + i * 2);					// <x, y>, get x
		minindex = i;
		for (j = i+1; j < count; j++) {
			fv = *(odata + j * 2);					// <x, y>, get x
			if (minv > fv) {							// get min value.
				minindex = j;
				minv = fv;
			}
		}
		if (minindex != i) {						// exchange..
			x_ = *(odata + i * 2);
			y_ = *(odata + i * 2 + 1);

//			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "(%d <-> %d), <%f, %f> <-> <%f ,%f>\n", i, minindex, x_, y_, *(odata + minindex * 2), *(odata + minindex * 2+1));

			*(odata + i * 2)     = *(odata + minindex * 2);
			*(odata + i * 2 + 1) = *(odata + minindex * 2 + 1);

			*(odata + minindex * 2)     = x_;
			*(odata + minindex * 2 + 1) = y_;
		}
	}

//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------------------------- 1st result\n");
	
	for (i = 0; i < row; i++) {
		for (j = 0; j < column; j++) {
			/*
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
				"[%2d, %2d]: (%10f, %10f)\n", 
				i, j, 
				*(odata + (i * column + j) * 2),
				*(odata + (i * column + j) * 2+1));
				*/
		}
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
	}

	//---- y-based sort for each column. 
	//	for (k = 0; k < row; k ++) 
	for (k = 0; k <column ; k++)
	{
		for (i = 0; i < row - 1; i++)
		{
			maxv = *(odata + (k * row + i) * 2 + 1);					// <x, y>, get y
			maxindex = i;

			for (j = i + 1; j < row; j++) {
				fv = *(odata + (k * row + j) * 2 + 1);				// <x, y>, get y
							
				if (maxv < fv) {							// get mac value.
					maxindex = j;
					maxv = fv;
				}
				
			}
			if (maxindex != i) {						// exchange..
				x_ = *(odata + (k*row + i) * 2);
				y_ = *(odata + (k*row + i) * 2 + 1);

				*(odata + (k*row + i) * 2) = *(odata + (k*row + maxindex) * 2);
				*(odata + (k*row + i) * 2 + 1) = *(odata + (k*row + maxindex) * 2 + 1);

				*(odata + (k*row + maxindex) * 2) = x_;
				*(odata + (k*row + maxindex) * 2 + 1) = y_;
			}
		}
	} 


//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------------------------- 2nd result\n");
	for (i = 0; i < row; i++) {
		for (j = 0; j < column; j++) {

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d, %2d]: (%10f, %10f)\n", 
				i, j,
				*(odata + (i * column + j) * 2),
				*(odata + (i * column + j) * 2+1));
		}
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
	}

}

#if defined (__cplusplus)
}
#endif

