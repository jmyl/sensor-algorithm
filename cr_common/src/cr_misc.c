// TODO: confused name with cr_misc diectory!!
/*!
 *******************************************************************************
                                                                                
                    CREATZ Common Misc
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2014, 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_misc.c
	 @brief  Miscellaneous functions used for all modules
	 @author 
	 @date   

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#include "cr_common.h"
#include "cr_math.h"
#include "cr_osapi.h"
#include "cr_misc.h"


/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif


/*!
 ********************************************************************************
 *	@brief      Make header 
 *	@param[out] pHeader 
 *	             buffer pounter
 *
 *
 *	@author	    
 *  @date       
 *******************************************************************************/
void cr_make_header(U32 *pHeader)
{
	CR_Time_t st;			
	U32 p0, p1, p2, p3;
	
	//--- Make Seed and header
	OSAL_SYS_GetLocalTime( &st);
	srand(cr_gettickcount());
	
	p0 = (rand() & 0xFF) ;
	p1 = st.wMonth;
	p2 = st.wDay;
	p3 = st.wHour;

	pHeader[0] = cr_rand_mkseed(p0, p1, p2, p3);

	pHeader[1] = ((p0 << 24) & 0xFF000000)					// RAND
				| ((p1 << 16) & 0x00FF0000) 					// month
				| ((p2 <<  8) & 0x0000FF00) 					// date
				| ((p3 <<  0) & 0x000000FF);					// hour
	pHeader[2] = (~pHeader[1]) ^ pHeader[0];				// check..
	
}


#if defined (__cplusplus)
extern "C" {
#endif


