/*!
 *******************************************************************************
                                                                                
                    CREATZ Log 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2014 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_log.c
	 @brief  Debug Message module
	 @author YongHo Suk                                 
	 @date   2014/10/08 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#if defined(_DEBUG)
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif


#include <stdio.h> 
#include <stdarg.h>

#if defined(_WIN32)
//#include <windows.h>
#include <direct.h>
#include <tchar.h>

#include <winsock2.h>
#include <iphlpapi.h>
#pragma comment(lib, "Iphlpapi.lib")

#elif defined(__linux__)
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <ifaddrs.h>
#include <string.h> 
#include <linux/if_packet.h>
#include <net/ethernet.h> 
#endif


#include "cr_common.h"
#include "cr_osapi.h"
#include "cr_math.h"
#include "cr_misc.h"

#include "cr_log.h"


/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
// 아래의 2개의 define 은 유지해주세요. [20201113, yhsuk]
#define CRLOG
//#define CRLOG_BIN
/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

	


typedef struct _cr_log
{
	HAND 	m_hmutex;
	FILE 	*m_fp;
	FILE 	*m_fp2;			// for txt log
	U32 	enable;
	U32		length;
	HAND	hrr;
	char	fn[1024];
	char	fn2[1024];		// for txt log
	U32		logid;
	CR_GUID guid;
} cr_log_t;

static cr_log_t *s_pcl = NULL;


#if !defined(CRLOG)
//--
HAND cr_log_create(U32 logid)
{
	logid;
	return NULL;
}
void cr_log_decrypt(char *fn, FILE *fpout)
{
	fn;
	fpout;
}

void cr_log_delete(HAND hcl)
{
	hcl;
}
void cr_log_enable(HAND hcl, int enable)
{
	hcl;
	enable;
}

void cr_log(HAND hcl, U32 mode, char *strMsg, ... )
{
	hcl;
	strMsg;
	mode;
}
void cr_log_binlog(HAND hcl, char *buf)
{
	hcl;
	buf;
}

char *cr_log_getfn(HAND hcl)
{
	hcl;
	return NULL;
}
#else
#define BUFLEN		2048
#define BUFLEN2		(BUFLEN + 64)

static void saveMacAddressInfo(cr_log_t *pcl)
{		// Store MAC address.. :)
#if defined(_WIN32)
	PIP_ADAPTER_INFO p = NULL;
	PIP_ADAPTER_INFO pp = NULL;
	ULONG len1 = 0; 
	DWORD dw = 0;
	IP_ADDR_STRING * ns = NULL;
	char buf1[ 101 ];
	char imsi[ 10 ];
	I32 i;

	len1 = sizeof( IP_ADAPTER_INFO );
	p = (IP_ADAPTER_INFO*) malloc( sizeof( IP_ADAPTER_INFO ) );

	dw = GetAdaptersInfo( p, &len1 );

	if( dw == ERROR_BUFFER_OVERFLOW ) {
		free( p );
		p = (IP_ADAPTER_INFO*) malloc( len1 );
	}

	pp = p;
	if( (dw = GetAdaptersInfo( p, &len1 )) == ERROR_SUCCESS ) {
		while( p ) {
			cr_log(pcl, 3, "Adapter [ %02d ]--------------------", p->Index );
			cr_log(pcl, 3, "Name: %s", p->AdapterName );
			cr_log(pcl, 3, "Description: %s", p->Description  );
			cr_log(pcl, 3, "Type: %d", p->Type );

			memset( buf1, 0, sizeof( buf1 ) );
			memset( buf1, 0, sizeof( imsi ) );
			for(i = 0 ; i < (int)p->AddressLength ; i++ ) {
				if( i == (int) p->AddressLength -1 )
					sprintf( imsi, "%02x", p->Address[ i ] );
				else
					sprintf( imsi, "%02x-", p->Address[ i ] );
				strcat( buf1, imsi );
			}
			cr_log(pcl, 3, "Hardware Address: %s", buf1 );

			ns  = &p->IpAddressList;
			while( ns )
			{
				cr_log(pcl, 3, "Ip : %s" , ns->IpAddress.String );
				cr_log(pcl, 3, "Mask: %s", ns->IpMask.String );
				ns = ns->Next;
			}
			p = p->Next;
		}
	}
	else
	{
		LPVOID lpMsgBuf;
		cr_log(pcl, 3, "Call to GetAdaptersInfo failed.");
		if (FormatMessage( 
					FORMAT_MESSAGE_ALLOCATE_BUFFER | 
					FORMAT_MESSAGE_FROM_SYSTEM | 
					FORMAT_MESSAGE_IGNORE_INSERTS,
					NULL,
					dw,
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
					(LPTSTR) &lpMsgBuf,
					0,
					NULL )) {
			cr_log(pcl, 3, "\tError: %s", lpMsgBuf);
		}
		free( lpMsgBuf );
	}

	free( pp );
	
#else	

	struct ifaddrs *ifaddr=NULL;
	struct ifaddrs *ifa = NULL;

	if (getifaddrs(&ifaddr) == -1)
	{
	   perror("getifaddrs");
	}
	else
	{
		for ( ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
		{
			char ip[128];
			char mac[128];			
			socklen_t salen;
			
			if ( (ifa->ifa_addr) && (ifa->ifa_addr->sa_family == AF_PACKET) )
			{
				int i = 0;			
				struct sockaddr_ll *s = (struct sockaddr_ll*)ifa->ifa_addr;
//				printf("%-8s ", ifa->ifa_name);
				cr_log(pcl, 3, "Name: %s", ifa->ifa_name );
				memset(mac, 0, 128);
				for (i=0; i <s->sll_halen; i++)
				{
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wrestrict"		
				
//					printf("%02x%c", (s->sll_addr[i]), (i+1!=s->sll_halen)?':':'\n');
					sprintf(mac, "%s%02x%c", mac, (s->sll_addr[i]), (i+1!=s->sll_halen)?':':'\0');
#pragma GCC diagnostic pop

				}
				cr_log(pcl, 3, "Hardware Address: %s", mac );
			}

			if (ifa->ifa_addr->sa_family == AF_INET) {
            	salen = sizeof (struct sockaddr_in);
				if (getnameinfo (ifa->ifa_addr, salen, ip, sizeof(ip), NULL, 0, NI_NUMERICHOST) < 0) {
					perror ("getnameinfo");
				}
//				 printf ("IP: %s\n", ip);
				cr_log(pcl, 3, "Ip : %s" , ip );
			}
		}    		
		freeifaddrs(ifaddr);
	} 
#endif	
}

 
//-----------------------------------
HAND cr_log_create(U32 logid)
{
	I32 len;
#if defined(CRLOG_BIN)	
	U32 seed;
#endif
	I32 newfile;
	FILE *fp;
	FILE *fp2;
	char buf[BUFLEN];
	CR_Time_t st;
	cr_log_t *pcl;
	
	//---
	//ShellExecute(NULL, _T("open"), _T("cmd.exe"), _T("/c mkdir CRLog"), NULL, SW_HIDE ) ;
	//ShellExecuteA(NULL, "open", "cmd.exe", "/c mkdir CRLog", NULL, SW_HIDE ) ;
	cr_mkdir( "CRLogV2" );
	if (logid == 0) {
		cr_log_delete(s_pcl);
		s_pcl = NULL;
	} else {
#if defined(_WIN32)
		sprintf(buf, "CRLogV2\\%02d", logid);
#else
		sprintf(buf, "CRLogV2/%02d", logid);
#endif
		cr_mkdir(buf);
	}

	pcl = (cr_log_t *) malloc(sizeof(cr_log_t));

	if (pcl == NULL) {
		goto func_exit;
	}
	memset(pcl, 0, sizeof(cr_log_t));

	OSAL_SYS_GetLocalTime( &st);
	if (logid == 0) {
#if defined(_WIN32)
		sprintf(pcl->fn,  "CRLogV2\\\\log%.2d.cl2", (int) st.wDay);
		sprintf(pcl->fn2, "CRLogV2\\\\log%.2d.txt", (int) st.wDay);
#else		
		sprintf(pcl->fn,  "CRLogV2/log%.2d.cl2", (int) st.wDay);
		sprintf(pcl->fn2, "CRLogV2/log%.2d.txt", (int) st.wDay);
#endif		
	} else {
#if defined(_WIN32)
		sprintf(pcl->fn,  "CRLogV2\\%02d\\\\log%.2d.cl2", logid, (int) st.wDay);
		sprintf(pcl->fn2, "CRLogV2\\%02d\\\\log%.2d.txt", logid, (int) st.wDay);
#else	
		sprintf(pcl->fn,  "CRLogV2/%02d/log%.2d.cl2", logid, (int) st.wDay);
		sprintf(pcl->fn2, "CRLogV2/%02d/log%.2d.txt", logid, (int) st.wDay);
#endif		
	}

	pcl->m_hmutex = cr_mutex_create();	
	cr_mutex_wait(pcl->m_hmutex, INFINITE);

	OSAL_SYS_GenerateGUID((char *)&pcl->guid);
	
	//----------------------
#if defined(CRLOG_BIN)
	seed = 0;
	fp = cr_fopen(pcl->fn, "rb");

	newfile = 1;
	if (fp != NULL) {				// Alread file exists.
		U32 header[3];					// [seed] [r|m|d|h] [~1]
		// 1) Check file size
		// 2) if larger .... rotate file names
		//
		fseek(fp, 0L, SEEK_END);
		len = ftell(fp);
		fseek(fp, 0L, SEEK_SET);

		//-

		if (len >= (3 * 4)) {
			U32 mmm, ddd;
			cr_fread (&header[0] , sizeof(U32), 3, fp);
			if (header[1] == (~header[2] ^ header[0])) {			// good.
				seed = header[0];
				mmm  = (header[1] >> 16) & 0xFF;	// month
				ddd  = (header[1] >>  8) & 0xFF;	// day

				if (mmm == st.wMonth) {
					if (ddd == st.wDay) {
						newfile = 0;				// Yes!!!
					}
				}
			}
		} 
		cr_fclose(fp);
	}

	if (newfile) {
		U32 header[3];					// [seed] [r|m|d|h] [~1]

		cr_make_header(header);

		fp = cr_fopen(pcl->fn, "wb");
		if (fp == NULL) {
			int iii;
			for (iii = 0; iii < 100; iii++) {
				fp = cr_fopen(pcl->fn, "wb");
				if (fp) {
					break;
				}
				cr_sleep(10);
			}
			if (fp == NULL) {
				cr_mutex_release(pcl->m_hmutex);
				cr_log_delete(pcl);
				pcl = NULL;
				goto func_exit;
			}
		}
		cr_fwrite(&header[0] , sizeof(U32), 3, fp);
		cr_fclose(fp);
		seed = header[0];
	} 

	//----
	fp = cr_fopen(pcl->fn, "ab+");
	if (fp == NULL) {
		// WHAT?
		// xxx... =_-;
	}

	pcl->hrr = cr_rand_create(seed);
	if (pcl->hrr == NULL) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s[%d] ERROR!! cr_rand_create failed!!\n", __func__, __LINE__);
	}

	fseek(fp, 0L, SEEK_END);
	len = ftell(fp);

	for (i = 0; i < len; i++) {
		cr_rand_gendataBYTE(pcl->hrr);					// discard... :P
	}

#else
	fp = NULL;
	len = 0;

	{
		I32 itemcount;
		I32 year_, month_, day_;
		FILE *fp2_;
		fp2_ = cr_fopen(pcl->fn2, "r");						// New file for text log

		newfile = 1;
		if (fp2_ != NULL) {
			itemcount = fscanf(fp2_, "[%4d/%2d/%2d", &year_, &month_, &day_);
			if (itemcount == 3) {
				if (year_ == st.wYear && month_ == st.wMonth && day_ == st.wDay) {
					newfile = 0;	// Preserve... 
				}
			}
			cr_fclose(fp2_);
		}
	}
#endif

	pcl->m_fp = fp;

	pcl->length = len;
	pcl->enable = 1;

	//--
	if (newfile) {
		fp2 = cr_fopen(pcl->fn2, "w");						// New file for text log
	} else {
		fp2 = cr_fopen(pcl->fn2, "a");	
	}
	pcl->m_fp2 = fp2;

	pcl->logid = logid;

	cr_mutex_release(pcl->m_hmutex);

	cr_log(pcl, 3, "[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[");
	cr_log(pcl, 3, "                  CR_LOG CREATED.    ");
	cr_log(pcl, 3, " LOG GUID: %08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X",
				(U32) pcl->guid.Data1,  
				(U32) pcl->guid.Data2,  
				(U32) pcl->guid.Data3,  
				(U32) pcl->guid.Data4[0],   
				(U32) pcl->guid.Data4[1],   
				(U32) pcl->guid.Data4[2],   
				(U32) pcl->guid.Data4[3],   
				(U32) pcl->guid.Data4[4],   
				(U32) pcl->guid.Data4[5],   
				(U32) pcl->guid.Data4[6],   
				(U32) pcl->guid.Data4[7]
			   );
	cr_log(pcl, 3, "[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[");

	saveMacAddressInfo(pcl);

func_exit:
	if (logid == 0) {
		s_pcl = pcl;
	}
	return (HAND) pcl;
}

void cr_log_decrypt(char *fn, FILE *fpout)										// For Decryption.
{
	U32 i;
	I32 len;
	U32 seed;
	I32 goodfile;
	HAND hrr;
	FILE *fp;
    char buf[BUFLEN2];
	
	//---

	//----------------------
	fp = cr_fopen(fn, "rb");

	goodfile = 0;
	if (fp == NULL) {
		goto func_exit;
	}


	// 1) Check file size.
	// 2) Check data sanity.
	fseek(fp, 0L, SEEK_END);
	len = ftell(fp);
	fseek(fp, 0L, SEEK_SET);

	//-
	goodfile = 0;
	seed = 0;
	if (len >= (3 * 4)) {					// Check header
//		U32 mmm, ddd;
		U32 *pu32;
		cr_fread (&buf[0] , sizeof(U32), 3, fp);
		pu32 = (U32 *) buf;
		if (pu32[1] == (~pu32[2] ^ pu32[0])) {			// good.
			seed = pu32[0];
//			mmm  = (pu32[1] >> 16) & 0xFF;	// month
//			ddd  = (pu32[1] >>  8) & 0xFF;	// day
			goodfile = 1;				// Yes!!!
		}
	} 

	if (!goodfile) {
		goto func_exit;
	}

	hrr = cr_rand_create(seed);
	for (i = 0; i < (3*4); i++) {
		cr_rand_gendataBYTE(hrr);					// discard... :P
	}


	if (fpout == NULL) {
		fpout = stdout;
	}


	while(1) {
		U32 readlen;
		U08 xdata;
		readlen = (U32)fread(buf, 1, (size_t)BUFLEN, fp);
		if (readlen > 0) {
			for (i = 0; i < readlen; i++) {
				xdata = cr_rand_gendataBYTE(hrr);
				buf[i] = buf[i] ^ xdata;
			}
			cr_fwrite(buf, 1, readlen, fpout);
		}
		if (readlen != BUFLEN) {
			break;
		}
	}

	cr_rand_delete(hrr);
func_exit:
	return;
}




//--
void cr_log_delete(HAND hcl)
{
	cr_log_t *pcl;

	if (hcl != NULL) {
		pcl = (cr_log_t *) hcl;
	} else {
		pcl = s_pcl;
	}

	if (pcl) {
		cr_log(hcl, 3, "]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]");
		cr_log(hcl, 3, "                  CR_LOG DELETED.");
		cr_log(hcl, 3, " LOG GUID: %08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X",
				(U32) pcl->guid.Data1,  
				(U32) pcl->guid.Data2,  
				(U32) pcl->guid.Data3,  
				(U32) pcl->guid.Data4[0],   
				(U32) pcl->guid.Data4[1],   
				(U32) pcl->guid.Data4[2],   
				(U32) pcl->guid.Data4[3],   
				(U32) pcl->guid.Data4[4],   
				(U32) pcl->guid.Data4[5],   
				(U32) pcl->guid.Data4[6],   
				(U32) pcl->guid.Data4[7]
			  );
		cr_log(hcl, 3, "]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]\n");

		cr_mutex_delete (pcl->m_hmutex);

		if (pcl->m_fp != NULL) {
			cr_fclose(pcl->m_fp);
			pcl->m_fp = NULL;
		}

		if (pcl->m_fp2 != NULL) {
			cr_fclose(pcl->m_fp2);
			pcl->m_fp2 = NULL;
		}

		cr_rand_delete(pcl->hrr);
		free(pcl);
		s_pcl = NULL;
	}
}
void cr_log_enable(HAND hcl, int enable)
{
	cr_log_t *pcl;

	if (hcl != NULL) {
		pcl = (cr_log_t *) hcl;
	} else {
		pcl = s_pcl;
	}

	if (pcl) {
		cr_mutex_wait(pcl->m_hmutex, INFINITE);

		if (enable) {
				pcl->enable = 1;
		} else {
				pcl->enable = 0;
		}
		cr_mutex_release(pcl->m_hmutex);
	}
}


void cr_log(HAND hcl, U32 mode, char *strMsg, ... )
{
	CR_Time_t st;
    char strBuffer[BUFLEN2];
    char strBuffer2[BUFLEN2];
	cr_log_t *pcl;
	
    va_list args;

	//----
	if (hcl != NULL) {
		pcl = (cr_log_t *) hcl;
	} else {
		pcl = s_pcl;
	}
	if (pcl == NULL) {
		goto func_exit;
	}


	if (pcl->enable == 0) {
		goto func_exit;
	}

	//--

	memset(strBuffer, 0, BUFLEN2);   
	memset(strBuffer2,0, BUFLEN2);   
	OSAL_SYS_GetLocalTime( &st);


	cr_mutex_wait(pcl->m_hmutex, INFINITE);
	//if (pcl->m_fp) 
	{
		va_start(args, strMsg);
#if defined(_WIN32)
		_vsnprintf( strBuffer, BUFLEN, strMsg, args );
#else
		vsnprintf( strBuffer, BUFLEN, strMsg, args );
#endif
		va_end(args);
		strBuffer[BUFLEN] = '\0';

		//                     y    m   d   h   m   s
		sprintf(strBuffer2, "[%04d/%02d/%02d-%02d:%02d:%02d] %s\n", 
				(int) st.wYear,
				(int) st.wMonth,
				(int) st.wDay,
				(int) st.wHour,
				(int) st.wMinute,
				(int) st.wSecond,
				strBuffer);


		//- binary log
		if (mode & 0x01) {
			cr_log_binlog(pcl, strBuffer2);
		} 

		//- text log
		if (mode & 0x02) {
			if (pcl->m_fp2 != NULL) {
				cr_fprintf(pcl->m_fp2, "%s", strBuffer2);
				cr_fflush(pcl->m_fp2);
			}
		}
	}
	cr_mutex_release(pcl->m_hmutex);

func_exit:
	return;
}


void cr_log_binlog(HAND hcl, char *buf)
{
	char binbuf[BUFLEN2];
	U32 slen;
	U08 xdata;
	U32 i;
	cr_log_t *pcl;
	

	//--
	if (hcl != NULL) {
		pcl = (cr_log_t *) hcl;
	} else {
		pcl = s_pcl;
	}
	if (pcl == NULL) {
		goto func_exit;
	}
	if (pcl->m_fp == NULL) {
		goto func_exit;
	}

	slen = (U32)strlen(buf);

	if (slen > BUFLEN) {
		slen = BUFLEN;
	}
	memset(binbuf, 0, BUFLEN2);


	//--
	for (i = 0; i < slen; i++) {
		xdata = cr_rand_gendataBYTE(pcl->hrr);
		binbuf[i] = buf[i] ^ xdata;
	}
	pcl->length += slen;

	cr_fwrite(&binbuf[0] , sizeof(U08), slen, pcl->m_fp);
	cr_fflush(pcl->m_fp);
func_exit:
	return;
}


char *cr_log_getfn(HAND hcl)
{
	char *fn;
	cr_log_t *pcl;

	fn = NULL;
	if (hcl != NULL) {
		pcl = (cr_log_t *) hcl;
	} else {
		pcl = s_pcl;
	}


	if (pcl == NULL) {
		goto func_exit;
	}
	
	fn = &pcl->fn[0];

func_exit:
	return fn;
}



#if defined(CR_LOG_TEST)
//-------------------------------------------------------------------------------------
void testlog(void)
{
	HAND hcl;
	U32 i;

	hcl = cr_log_create(0);

	if (hcl == NULL) {
		printf("WHAT?\n");
		return;
	}

	cr_log(hcl, 3, "Good.");
	cr_log(hcl, 3, "Good. %s", "Very....lsdkjvasfldkj\n");

	for (i = 0; i < 100; i++) {
		cr_log(hcl, 3, "[%.4d] Count Up...", i);
	}
	cr_log_delete(hcl);
}

void testlogdecrypt(char *fn, FILE *fp)
{
	cr_log_decrypt(fn, fp);
}

int main(int argc, char *argv[]) 
{
	FILE *fp;
	if (argc == 1) { 
		testlog();
		cr_sleep(1000);
		testlog();
		cr_sleep(5000);
		testlog();
	} else {
		fp = NULL;
		if (argc == 2) {
			fp = stdout;
		} else if (argc == 3) {
			fp = cr_fopen(argv[2], "wt");
		} else {
			printf("Usage: %s [enclogfn [outfn]]\n", argv[0]);
			return 0;
		}

		testlogdecrypt(argv[1], fp);
	}
}


//---------------------------------------------------------------------------------------
#endif
#endif


#if defined (__cplusplus)
}
#endif
