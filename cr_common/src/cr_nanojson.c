/*!
 *******************************************************************************
                                                                                
                    CREATZ NANO JSON 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2014, 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_nanojson.c
	 @brief  NANO JAON. very very very small json writer
	 @author YongHo Suk                                 
	 @date   2019/12/16 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#include "cr_common.h"
#include "cr_math.h"
#include "cr_osapi.h"
#include "cr_nanojson.h"
#include "cr_misc.h"


/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

nanojson_t *s_pnj;
nanojson_t *s_pnj_encryption;

//---
HAND cr_nanojson_create(char *fn, U32 encryption)
{
	FILE *fp;
	nanojson_t *pnj;
	
	pnj = (nanojson_t *) malloc(sizeof(nanojson_t));

	if (pnj != NULL) {
		memset(pnj, 0, sizeof(nanojson_t));
		pnj->tabcount = 0;
		pnj->len = 0;

		if (encryption) {
			fp = cr_fopen(fn, "wb");
		} else {
			//pnj->fp = fopen(fn, "w");
			fp = cr_fopen(fn, "w");
		}

		if (fp == NULL) {			// -_-;
			free(pnj);
			pnj = NULL;
		} else {
			if (encryption) {
				U32 i;
				U32 seed;
				
				U32 header[3];					// [seed] [r|m|d|h] [~1]

				cr_make_header(header);

				cr_fwrite(&header[0] , sizeof(U32), 3, fp);
				pnj->length = (sizeof(U32) * 3);

				seed = header[0];

				pnj->hrr = cr_rand_create(seed);
				for (i = 0; i < pnj->length; i++) {
					cr_rand_gendataBYTE(pnj->hrr);		// discard... :P
				}
			} else {
				pnj->length = 0;
				pnj->hrr = NULL;
			}
			pnj->fp = fp;
			pnj->encryption = encryption;
		}
	}

	return (HAND) pnj;
}


void cr_nanojson_create2(char *fn, U32 encryption)
{
	if (encryption) {
		if (s_pnj_encryption) {
			cr_nanojson_delete(s_pnj, encryption);
			s_pnj = NULL;
		}
		s_pnj_encryption = (nanojson_t *)cr_nanojson_create(fn, encryption);
	} else {
		if (s_pnj) {
			cr_nanojson_delete(s_pnj, encryption);
			s_pnj = NULL;
		}
		s_pnj = (nanojson_t *)cr_nanojson_create(fn, encryption);
	}
}


void cr_nanojson_delete(HAND hnx, U32 encryption)
{
	nanojson_t *pnj;
	
	if (hnx) {
		pnj = (nanojson_t *)hnx;
	} else {
		if (encryption) {
			pnj = s_pnj_encryption;
		} else {
			pnj = s_pnj;
		}
	}
			
	if (pnj) {
		if (pnj->fp) {
			cr_fclose(pnj->fp);
		}

		if (pnj->hrr) {
			cr_rand_delete(pnj->hrr);
		}
		free(pnj);
	}

	if (hnx == NULL) {
		if (encryption) {
			s_pnj_encryption = NULL;
		} else {
			s_pnj = NULL;
		}
	}
}


//void cr_nanojson_insertelement(HAND hnx, U32 encryption, I32 addtab, char *szFormat, ...)
void cr_nanojson_insertelement(HAND hnx, U32 encryption, I32 addtab, char *szFormat, va_list vlMarker)
{
//    va_list vlMarker ;

	I32 i;
#define NANOJSON_BUFLEN	10240
    char szTempBuf[NANOJSON_BUFLEN];

	nanojson_t *pnj;


	if (hnx != NULL) {
		pnj = (nanojson_t *) hnx;
	} else {
		if (encryption) {
			pnj = s_pnj_encryption;
		} else {
			pnj = s_pnj;
		}
	}
	if (pnj == NULL) {
		goto func_exit;
	}

	if (pnj->encryption != encryption) {
		goto func_exit;
	}


	memset(szTempBuf, 0, NANOJSON_BUFLEN);
	if (addtab < 0) {
		pnj->tabcount += addtab;
		if (pnj->tabcount < 0) {
			pnj->tabcount = 0;
		}
	}


#if 1
//	va_start(vlMarker,szFormat) ;
	{
		U32 datalen;
		for (i = 0; i < pnj->tabcount; i++) {
			*(szTempBuf+i) = '\t';
		}
		vsprintf((szTempBuf + pnj->tabcount),szFormat,vlMarker) ;

		datalen = (U32)strlen(szTempBuf);
		*(szTempBuf+datalen) = '\n';
		datalen++;


		if (encryption) {
			cr_nanojson_encryptout(pnj, szTempBuf, datalen+1);
		} else {
			cr_fprintf(pnj->fp, szTempBuf);
		}
		if (pnj->len + datalen < NANOJSON_BUFSIZE) {
			memcpy(pnj->buf + pnj->len, szTempBuf, datalen);
			pnj->len += datalen;
		}
	}
//	va_end(vlMarker) ;

#endif


	if (addtab > 0) {
		pnj->tabcount += addtab;
	}
func_exit:
	return;
}




void njsonie(I32 addtab, char *szFormat, ...)
{
    va_list vlMarker ;
	nanojson_t *pnj;

	//---- PLAIN TEXT
	if (s_pnj) {
		pnj = s_pnj;
		{
			va_start(vlMarker,szFormat) ;
			cr_nanojson_insertelement(pnj, 0, addtab, szFormat, vlMarker);
			va_end(vlMarker) ;
		}
	}

	//---- Encryption TEXT
	if (s_pnj_encryption) {
		pnj = s_pnj_encryption;
		{
			va_start(vlMarker,szFormat) ;
			cr_nanojson_insertelement(pnj, 1, addtab, szFormat, vlMarker);
			va_end(vlMarker) ;
		}
	}
}

void cr_nanojson_encryptout(HAND hnx, char *buf, U32 datalen)
{
	
	if (hnx) {
		U32 i;
		U08 xdata;
		char buf1[NANOJSON_BUFLEN];
		nanojson_t *pnj;

		//---
		pnj = (nanojson_t *)hnx;
		if (pnj->encryption) {
			if (datalen > NANOJSON_BUFLEN) {
				datalen = NANOJSON_BUFLEN;
			}
			for (i = 0; i < datalen; i++) {
				xdata = cr_rand_gendataBYTE(pnj->hrr);
				buf1[i] = buf[i] ^ xdata;
			}
			pnj->length += datalen;

			cr_fwrite(&buf1[0] , sizeof(U08), datalen, pnj->fp);
			cr_fflush(pnj->fp);
		}
	}
}

void cr_nanojson_bufinfo(HAND hnx, U08 **pbuf, U32 *pdatalen)
{
	nanojson_t *pnj;
	
	if (hnx) {
		pnj = (nanojson_t *) hnx;
	} else {
		pnj = s_pnj;
	}

	if (pnj) {
		*pbuf = pnj->buf;
		*pdatalen = pnj->len;
	} else {
		*pbuf = NULL;
		*pdatalen = 0;
	}
}


#if defined (__cplusplus)
extern "C" {
#endif

