#if defined(_DEBUG)
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#include "sha256.h"



//------------------------------------
void Sha256_Init(CSha256 *p);
void Sha256_Update(CSha256 *p, const U08 *data, U32 size);
void Sha256_Final(CSha256 *p, U08 *digest);


/*----------------------------------------------------------------------------
 	Description	: Functions..
-----------------------------------------------------------------------------*/
HAND Sha256_create()
{ 
	CSha256 *p;

	p = (CSha256 *) malloc(sizeof(CSha256));
	if (p == NULL) {
		return NULL;
	}
	Sha256_Init((HAND)p);

	return (HAND)p;
}

void Sha256_delete(HAND h) 
{
	free(h);
}

void Sha256_Init(HAND h)
{
	CSha256 *p;

	if (h == NULL) {
		return ;
	}

	p = (CSha256 *) h;

	p->state[0] = 0x6a09e667;
	p->state[1] = 0xbb67ae85;
	p->state[2] = 0x3c6ef372;
	p->state[3] = 0xa54ff53a;
	p->state[4] = 0x510e527f;
	p->state[5] = 0x9b05688c;
	p->state[6] = 0x1f83d9ab;
	p->state[7] = 0x5be0cd19;
	p->count = 0;
}



static const U32 K[64] = {
	0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
	0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
	0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
	0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
	0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
	0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
	0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
	0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
	0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
	0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
	0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
	0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
	0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
	0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
	0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
	0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
};

static void Sha256_Transform(U32 *state, const U32 *data)
{
	U32 W[16];
	U32 j;
#ifdef _SHA256_UNROLL2
	U32 a,b,c,d,e,f,g,h;
	a = state[0];
	b = state[1];
	c = state[2];
	d = state[3];
	e = state[4];
	f = state[5];
	g = state[6];
	h = state[7];
#else
	U32 T[8];
	for (j = 0; j < 8; j++)
		T[j] = state[j];
#endif

	for (j = 0; j < 64; j += 16)
	{
#if defined(_SHA256_UNROLL) || defined(_SHA256_UNROLL2)
		RX_8(0); RX_8(8);
#else
		U32 i;
		for (i = 0; i < 16; i++) { R(i); }
#endif
	}

#ifdef _SHA256_UNROLL2
	state[0] += a;
	state[1] += b;
	state[2] += c;
	state[3] += d;
	state[4] += e;
	state[5] += f;
	state[6] += g;
	state[7] += h;
#else
	for (j = 0; j < 8; j++)
		state[j] += T[j];
#endif

	/* Wipe variables */
	/* memset(W, 0, sizeof(W)); */
	/* memset(T, 0, sizeof(T)); */
}


static void Sha256_WriteByteBlock(CSha256 *p)
{
	U32 data32[16];
	U32 i;
	for (i = 0; i < 16; i++)
		data32[i] =
		((U32)(p->buffer[i * 4    ]) << 24) +
		((U32)(p->buffer[i * 4 + 1]) << 16) +
		((U32)(p->buffer[i * 4 + 2]) <<  8) +
		((U32)(p->buffer[i * 4 + 3]));
	Sha256_Transform(p->state, data32);
}

void Sha256_Update(HAND h, const U08 *data, U32 size)
{
	U32 curBufferPos;
	CSha256 *p;

	//--
	if (h == NULL) {
		return ;
	}

	p = (CSha256 *) h;

	curBufferPos = p->count & 0x3F;
	while (size > 0)
	{
		p->buffer[curBufferPos++] = *data++;
		p->count++;
		size--;
		if (curBufferPos == 64)
		{
			curBufferPos = 0;
			Sha256_WriteByteBlock(p);
		}
	}
}

void Sha256_Final(HAND h, U08 *digest)
{
	U64 lenInBits;
	U32 curBufferPos;
	unsigned i;
	CSha256 *p;

	//--
	if (h == NULL) {
		return ;
	}

	p = (CSha256 *) h;

	lenInBits = (p->count << 3);
	curBufferPos = p->count & 0x3F;

	p->buffer[curBufferPos++] = 0x80;
	while (curBufferPos != (64 - 8))
	{
		curBufferPos &= 0x3F;
		if (curBufferPos == 0)
			Sha256_WriteByteBlock(p);
		p->buffer[curBufferPos++] = 0;
	}
	for (i = 0; i < 8; i++)
	{
		p->buffer[curBufferPos++] = (U08)(lenInBits >> 56);
		lenInBits <<= 8;
	}
	Sha256_WriteByteBlock(p);

	for (i = 0; i < 8; i++)
	{
		*digest++ = (U08)(p->state[i] >> 24);
		*digest++ = (U08)(p->state[i] >> 16);
		*digest++ = (U08)(p->state[i] >> 8);
		*digest++ = (U08)(p->state[i]);
	}
	Sha256_Init((HAND)p);
}

#undef S0
#undef S1
#undef s0
#undef s1
