/*!
 *******************************************************************************
                   Creatz  Camera 
                                                                                
  	@section copyright_notice COPYRIGHT NOTICE
  	Copyright (c) 2012 by Creatz Inc. 
  	All Rights Reserved. \n
  	Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	@section file_information FILE CREATION INFORMATION
	@file   cr_geometric.c
	@brief  Geometric library
	@author YongHo Suk                                 
	@date   2012/03/13 First Created

	@section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/

#include <math.h>
#include <string.h>

#include "cr_common.h"
#include "cr_osapi.h"
#include "cr_geometric.h"

#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#else
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#endif

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#define KITER	50

/*----------------------------------------------------------------------------
 *	Description	: type definition
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/
static int s_KITER = KITER;

/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/

double cr_distance_P_Line_legacy(cr_point_t *pP, cr_line_t *pL);


/*----------------------------------------------------------------------------
 *	Description	: local functions
 -----------------------------------------------------------------------------*/
static U32 get_vector_angle(cr_vector_t *pn0, cr_vector_t *pn1, double *pangle)
{
	U32 res;
	double l0, l1, adb;
	double theta, angle;

	l0 = cr_vector_norm(pn0);
	l1 = cr_vector_norm(pn1);

	if (l0 < MIN_GEOMETRIC_VALUE || l1 < MIN_GEOMETRIC_VALUE) {
		res = 0;
		goto func_exit;
	}

	adb = -cr_vector_inner(pn0, pn1);

	theta = acos(adb / (l0 * l1));


	if (theta > M_PI/2) {
		theta = M_PI - theta;
	}

	angle = RADIAN2DEGREE(theta);

	*pangle = angle;
	res = 1;

func_exit:
	return res;
}

// basic functions: vector ---

/*!
 ********************************************************************************
 *	@brief      Make vector
 *	@param[in]  pp
 *				point
 *	@param[out] pvo
 *				Vector
 *
 *  @return		none
 *
 *	@author	    yhsuk
 *  @date       2012/03/13
 *******************************************************************************/
void cr_vector_make(
		cr_point_t *pp,
		cr_vector_t *pvo)
{
	pvo->x = pp->x;
	pvo->y = pp->y;
	pvo->z = pp->z;
}

/*!
 ********************************************************************************
 *	@brief      Make vector with two points
 *	@param[in]  pp0		
 *				start point
 *	@param[in]  pp1
 *				end point
 *	@param[out] pvo
 *				Vector
 *
 *  @return		none
 *
 *	@author	    yhsuk
 *  @date       2012/03/13
 *******************************************************************************/
void cr_vector_make2(
		cr_point_t *pp0,
		cr_point_t *pp1,
		cr_vector_t *pvo)
{
	cr_vector_sub( pp1, pp0, pvo);
}

/*!
 ********************************************************************************
 *	@brief      Make Zero vector
 *	@param[in]  pp
 *				point
 *
 *  @return		none
 *
 *	@author	    yhsuk
 *  @date       2014/02/21
 *******************************************************************************/
void cr_vector_makeZero(
		cr_vector_t *pvo)
{
	pvo->x = 0.0;
	pvo->y = 0.0;
	pvo->z = 0.0;
}


/*!
 ********************************************************************************
 *	@brief      vector addition
 *	@param[in]  pv1
 *				First vector
 *	@param[in]  pv2
 *				2nd vector
 *	@param[out] pvo
 *				v1 + v2
 *
 *  @return		none
 *
 *	@author	    yhsuk
 *  @date       2012/03/13
 *******************************************************************************/
void cr_vector_add(
		cr_vector_t *pv1,
		cr_vector_t *pv2,
		cr_vector_t *pvo)
{
	pvo->x = pv1->x + pv2->x;
	pvo->y = pv1->y + pv2->y;
	pvo->z = pv1->z + pv2->z;
}


/*!
 ********************************************************************************
 *	@brief      vector subtraction
 *	@param[in]  pv1
 *				First vector
 *	@param[in]  pv2
 *				2nd vector
 *	@param[out] pvo
 *				v1 - v2
 *
 *  @return		none
 *
 *	@author	    yhsuk
 *  @date       2012/03/13
 *******************************************************************************/
void cr_vector_sub(
		cr_vector_t *pv1,
		cr_vector_t *pv2,
		cr_vector_t *pvo)
{
	pvo->x = pv1->x - pv2->x;
	pvo->y = pv1->y - pv2->y;
	pvo->z = pv1->z - pv2->z;
}


/*!
 ********************************************************************************
 *	@brief      vector-scalar product
 *	@param[in]  pv
 *				vector
 *	@param[in]  s
 *				scalar value
 *	@param[out] pvo
 *				s * v
 *
 *  @return		none
 *
 *	@author	    yhsuk
 *  @date       2012/03/13
 *******************************************************************************/
void cr_vector_scalar(
		cr_vector_t *pv,
		double		s,
		cr_vector_t *pvo)
{
	pvo->x = pv->x * s;
	pvo->y = pv->y * s;
	pvo->z = pv->z * s;
}


/*!
 ********************************************************************************
 *	@brief      vector-norm
 *	@param[in]  pv
 *				vector
 *
 *  @return		norm value.
 *
 *	@author	    yhsuk
 *  @date       2012/03/13
 *******************************************************************************/
double cr_vector_norm(
		cr_vector_t *pv)
{
	double norm;

	norm = pv->x * pv->x + pv->y * pv->y + pv->z * pv->z;
	norm = sqrt(norm);

	return norm;
}


/*!
 ********************************************************************************
 *	@brief      vector-normalization
 *	@param[in]  pv
 *				vector
 *	@param[out]  pvn
 *				normalizaed vector
 *
 *  @return		norm value.
 *
 *	@author	    yhsuk
 *  @date       2014/02/21
 *******************************************************************************/
double cr_vector_normalization(
		cr_vector_t *pv,
		cr_vector_t *pvn
		)
{
	double norm;
	double invnorm;

	norm = cr_vector_norm(pv);

	if (norm > MIN_GEOMETRIC_VALUE) {
		invnorm = 1.0 / norm;
		cr_vector_scalar(pv, invnorm, pvn);
	} else {
		norm = 0.0;
		cr_vector_makeZero(pvn);
	}
	return norm;
}


/*!
 ********************************************************************************
 *	@brief      vector distance
 *	@param[in]  pv1
 *				First vector
 *	@param[in]  pv2
 *				2nd vector
 *
 *  @return		vector distance = |v1-v2|
 *
 *	@author	    yhsuk
 *  @date       2012/12/07
 *******************************************************************************/
double cr_vector_distance(
		cr_vector_t *pv1,
		cr_vector_t *pv2)
{
	double distance;
	double dx, dy, dz;

	dx = pv1->x - pv2->x;
	dy = pv1->y - pv2->y;
	dz = pv1->z - pv2->z;

	distance = sqrt(dx*dx + dy*dy + dz*dz);

	return distance;
}

/*!
 ********************************************************************************
 *	@brief      angle between two vectorss
 *	@param[in]  pv1
 *				First vector
 *	@param[in]  pv2
 *				2nd vector
 *
 *  @return		radian angle between two vectors.
 *              (zero vector: return zero)
 *	@author	    yhsuk
 *  @date       2020/0424
 *******************************************************************************/
double cr_vector_angle(
		cr_vector_t *pv1,
		cr_vector_t *pv2)
{
	double res;
	double l1, l2;
	double dotprod;
	double cosv;


	//--
	l1 = cr_vector_norm(pv1);
	l2 = cr_vector_norm(pv2);

	if (l1 < GEOMETRIC_SMALL_VALUE || l1 < GEOMETRIC_SMALL_VALUE) {
		res = 0;		// too small length..
		goto func_exit;
	}

	dotprod = cr_vector_inner(pv1, pv2);

	cosv = dotprod / (l1*l2);

	if (cosv >= 1.0) {
		cosv = 1.0;
	}
	if (cosv <= -1.0) {
		cosv = -1.0;
	}

	res = acos(cosv);

func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      vector inner product
 *	@param[in]  pv1
 *				First vector
 *	@param[in]  pv2
 *				2nd vector
 *
 *  @return		Inner product value v1 * v2
 *
 *	@author	    yhsuk
 *  @date       2012/03/13
 *******************************************************************************/
double cr_vector_inner(
		cr_vector_t *pv1,
		cr_vector_t *pv2)
{
	double inner;

	inner = pv1->x * pv2->x + pv1->y * pv2->y + pv1->z * pv2->z;

	return inner;
}

/*!
 ********************************************************************************
 *	@brief      vector cross product
 *	@param[in]  pv1
 *				First vector
 *	@param[in]  pv2
 *				2nd vector
 *	@param[out] pvo
 *				v1 x v2
 *
 *  @return		none
 *
 *	@author	    yhsuk
 *  @date       2012/03/13
 *******************************************************************************/
void cr_vector_cross(
		cr_vector_t *pv1,
		cr_vector_t *pv2,
		cr_vector_t *pvo)
{
	double x;
	double y;
	double z;


	/*
	c = a x b
	 --> 	x = ay bz - az by;
			y = az bx - ax bz;
			z = ax by - ay bx;
	*/

	x = pv1->y * pv2->z - pv1->z * pv2->y;
	y = pv1->z * pv2->x - pv1->x * pv2->z;
	z = pv1->x * pv2->y - pv1->y * pv2->x;

	pvo->x = x;
	pvo->y = y;
	pvo->z = z;
}


// basic functions: line ---
/*!
 ********************************************************************************
 *	@brief      Make line with two point
 *	@param[in]  pp0
 *				first point
 *	@param[in]  pp1
 *				second point
 *	@param[out] pl
 *				maked line.
 *
 *  @return		0: fail.		(p0 is too close to p1)
 *  			1: Success.
 *
 *	@author	    yhsuk
 *  @date       2012/03/13
 *******************************************************************************/
U32 cr_line_p0p1(
		cr_point_t *pp0,
		cr_point_t *pp1,
		cr_line_t *pl)
{
	double dx;
	double dy;
	double dz;

	double norm;

	U32 res;

	//-----
	dx = pp1->x - pp0->x;
	dy = pp1->y - pp0->y;
	dz = pp1->z - pp0->z;

	norm = dx * dx + dy * dy + dz * dz;
	norm = sqrt(norm);


	if (norm < MIN_GEOMETRIC_VALUE) {
		res = 0;
		goto func_exit;
	}

	dx = dx / norm;					// normalization.
	dy = dy / norm;					// now parallel vecor m is unit vector.
	dz = dz / norm;					// 

	pl->p0.x = pp0->x;				// p0_x
	pl->p0.y = pp0->y;				// p0_y
	pl->p0.z = pp0->z;				// p0_z

	pl->m.x = dx;
	pl->m.y = dy;
	pl->m.z = dz;

	res = 1;
func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      Make line with p0 and m
 *	@param[in]  p0
 *				pfixed point
 *	@param[in]  pm
 *				parallel vector
 *	@param[out] pl
 *				maked line.
 *
 *  @return		none.
 *
 *	@author	    yhsuk
 *  @date       2012/03/13
 *******************************************************************************/
void cr_line_p0m(
		cr_point_t *pp0,
		cr_vector_t *pm,
		cr_line_t *pl)
{
	pl->p0.x = pp0->x;				// p0_x
	pl->p0.y = pp0->y;				// p0_y
	pl->p0.z = pp0->z;				// p0_z

	pl->m.x = pm->x;
	pl->m.y = pm->y;
	pl->m.z = pm->z;
}

/*!
 ********************************************************************************
 *	@brief      Make line prependicular to two vectors and crosses fixed point p0
 *	@param[in]  pv0
 *				1st vector
 *	@param[in]  pv1
 *				2nd vector
 *	@param[in]  pp0
 *				fixed point
 *	@param[out] pl
 *				maked line
 *
 *  @return		0: fail, 1: success
 *
 *	@author	    yhsuk
 *  @date       2012/03/13
 *******************************************************************************/
U32 cr_line_v0v1p0(
    cr_vector_t *pv0,
    cr_vector_t *pv1,
    cr_point_t *pp0,
    cr_line_t  *pl) {
    U32 res;
    cr_vector_t vm;
    double norm;
    // 1) make parallel vector m = v0 x v1
    cr_vector_cross(pv0, pv1, &vm);

    norm = cr_vector_norm(&vm);

    if (norm < MIN_GEOMETRIC_VALUE) {
        res = 0;
        memset(pl, 0, sizeof(cr_line_t));
        goto func_exit;
    }

    // 2) make line with m and pp0

    cr_line_p0m(pp0, &vm, pl);
    res = 1;

func_exit:
    return res;
}


// basic functions: plane ---
/*!
 ********************************************************************************
 *	@brief      Make plane with p0 and n
 *	@param[in]  pp0
 *				pfixed point
 *	@param[in]  pn
 *				normal vector
 *	@param[out] pplane
 *				maked plane.
 *
 *  @return		none.
 *
 *	@author	    yhsuk
 *  @date       2012/03/13
 *******************************************************************************/
void cr_plane_p0n(
		cr_point_t *pp0,
		cr_vector_t *pn,
		cr_plane_t *pplane)
{
	pplane->p0.x 	= pp0->x;				// p0_x
	pplane->p0.y 	= pp0->y;				// p0_y
	pplane->p0.z 	= pp0->z;				// p0_z

	pplane->n.x 		= pn->x;
	pplane->n.y 		= pn->y;
	pplane->n.z 		= pn->z;
}

/*!
 ********************************************************************************
 *	@brief      Make plane with three points p0, p1, p2
 *	@param[in]  pp0
 *				1st fixed point
 *	@param[in]  pp1
 *				2nd fixed point
 *	@param[in]  pp2
 *				3rd fixed point
 *	@param[out] pplane
 *				maked plane
 *
 *  @return		0: fail, 1: success
 *
 *	@author	    yhsuk
 *  @date       2012/03/13
 *******************************************************************************/
U32 cr_plane_p0p1p2(
		cr_point_t *pp0,
		cr_point_t *pp1,
		cr_point_t *pp2,
		cr_plane_t *pplane)
{
	U32 res;
	double norm;

	cr_vector_t v0;
	cr_vector_t v1;
	cr_vector_t n;

	// v0 = p0 - p1
	v0.x = pp0->x - pp1->x;
	v0.y = pp0->y - pp1->y;
	v0.z = pp0->z - pp1->z;


	// v1 = p0 - p2
	v1.x = pp0->x - pp2->x;
	v1.y = pp0->y - pp2->y;
	v1.z = pp0->z - pp2->z;

	// n = v1 x v2 = (p0 - p1) x (p0 - p2)
	cr_vector_cross(&v0, &v1, &n);

	norm = cr_vector_norm(&n);
	
	if (norm < MIN_GEOMETRIC_VALUE) {
		res = 0;
		memset(pplane, 0, sizeof(cr_plane_t));
		goto func_exit;
	}


	// make plane with p0 and n
	cr_plane_p0n(pp0, &n, pplane);
	res = 1;
func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Make plane with p0 and line
 *	@param[in]  pp0
 *				pfixed point
 *	@param[in]  pline
 *				line
 *	@param[out] pplane
 *				maked plane.
 *
 *  @return		none.
 *
 *	@author	    yhsuk
 *  @date       2014/0408
 *******************************************************************************/
void cr_plane_p0line(
    cr_point_t *pp0,
    cr_line_t *pline,
    cr_plane_t *pplane) {
    cr_point_t p1, p2;

    //--

    memcpy(&p1, &pline->p0, sizeof(cr_point_t));			// make 2 points in the line.
    //p2 = p1 + 1 * pline->m0;
    memcpy(&p2, &pline->m, sizeof(cr_vector_t));
#define PLANE_P2_SCALAR	10.0
    cr_vector_scalar(&p2, PLANE_P2_SCALAR, &p2);

    cr_plane_p0p1p2(
        pp0,
        &p1,
        &p2,
        pplane);
}

/*!
 ********************************************************************************
 *	@brief      Make plane with two lines (l0 and l1)
 *	@param[in]  pline0
 *				line0
 *	@param[in]  pline1
 *				line0
 *	@param[out] pplane0
 *				maked plane, contains pline0.
 *	@param[out] pplane1
 *				maked plane, contains pline1.
 *
 *  @return		none.
 *
 *	@author	    yhsuk
 *  @date       2020/0508
 *******************************************************************************/
void cr_plane_line0line1(
    cr_line_t *pline0,
    cr_line_t *pline1,
    cr_plane_t *pplane0,
    cr_plane_t *pplane1
) {
    U32 i;
    cr_point_t p0, p1;
    cr_point_t p_0;
    cr_vector_t vtmp;

    cr_line_t *pline_0, *pline_1;
    cr_plane_t *pplane_1;
    double dist0, dist1;

    for (i = 0; i < 2; i++) {
        if (i == 0) {
            pline_0 = pline0;
            pline_1 = pline1;

            pplane_1 = pplane1;
        } else {
            pline_0 = pline1;
            pline_1 = pline0;

            pplane_1 = pplane0;
        }

        // 1) With fixed point, get another point of each lines
#define LINELINE_PLANE_DELTA	(0.1)
        memcpy(&p0, &pline_0->p0, sizeof(cr_point_t));		// fixed point of line
        cr_vector_scalar(&pline_0->m, LINELINE_PLANE_DELTA, &vtmp);
        cr_vector_add(&p0, &vtmp, &p1);	// another point

        // 2) select far point 
        dist0 = cr_distance_P_Line(&p0, pline_1);
        dist1 = cr_distance_P_Line(&p1, pline_1);

        if (dist0 > GEOMETRIC_SMALL_VALUE || dist1 > GEOMETRIC_SMALL_VALUE) {
            if (dist0 > dist1) {
                // use p0  with pline1 
                memcpy(&p_0, &p0, sizeof(cr_point_t));
            } else {
                // use p1  with pline1 
                memcpy(&p_0, &p1, sizeof(cr_point_t));
            }
        } else {
            // what? almost same line?
            memcpy(&p_0, &p0, sizeof(cr_point_t));
            p_0.x = p_0.x + 0.01;		// fake.. :P
        }

        // 3) make plane with p0 and line1
        cr_plane_p0line(&p_0, pline_1, pplane_1);
    }
}


// rename? : project point to plane
/*!
 ********************************************************************************
 *	@brief      with x and y, get z location on the given plane
 *	@param[in] pplane
 *				plane.
 *	@param[in]  x
 *				x location
 *	@param[in]  y
 *				y location
 *	@param[out]  pz
 *				z location result
 *
 *  @return		1: good, 0: bad
 *
 *	@author	    yhsuk
 *  @date       2017/08/03
 *******************************************************************************/
U32 cr_plane_xy2z(
		cr_plane_t *pplane,
		double x,
		double y,
		double *pz
		)
{
	U32 res;
	double nx, ny, nz;
	double x0, y0, z0;

	//--------------
	x0 = pplane->p0.x;
	y0 = pplane->p0.y;
	z0 = pplane->p0.z;

	nx = pplane->n.x;
	ny = pplane->n.y;
	nz = pplane->n.z;


	if (nz > - MIN_GEOMETRIC_VALUE && nz < MIN_GEOMETRIC_VALUE) {
		res = 0;
		goto func_exit;
	}

	*pz = z0 - (1.0/nz) * (nx * (x-x0) + ny*(y-y0));

	res = 1;
func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      with y and z, get x location on the given plane
 *	@param[in] pplane
 *				plane.
 *	@param[in]  y
 *				y location
 *	@param[in]  z
 *				z location
 *	@param[out] px
 *				x location result
 *
 *  @return		1: good, 0: bad
 *
 *	@author	    yhsuk
 *  @date       2017/08/03
 *******************************************************************************/
U32 cr_plane_yz2x(
		cr_plane_t *pplane,
		double y,
		double z,
		double *px
		)
{
	U32 res;
	double nx, ny, nz;
	double x0, y0, z0;

	//--------------
	x0 = pplane->p0.x;
	y0 = pplane->p0.y;
	z0 = pplane->p0.z;

	nx = pplane->n.x;
	ny = pplane->n.y;
	nz = pplane->n.z;


	if (nx > - MIN_GEOMETRIC_VALUE && nx < MIN_GEOMETRIC_VALUE) {
		res = 0;
		goto func_exit;
	}

	*px = x0 - (1.0/nx) * (ny * (y-y0) + nz*(z-z0));

	res = 1;
func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      with z and x, get y location on the given plane
 *	@param[in] pplane
 *				plane.
 *	@param[in]  z
 *				z location
 *	@param[in]  x
 *				x location
 *	@param[out] py
 *				y location result
 *
 *  @return		1: good, 0: bad
 *
 *	@author	    yhsuk
 *  @date       2017/08/03
 *******************************************************************************/
U32 cr_plane_zx2y(
		cr_plane_t *pplane,
		double z,
		double x,
		double *py
		)
{
	U32 res;
	double nx, ny, nz;
	double x0, y0, z0;

	//--------------
	x0 = pplane->p0.x;
	y0 = pplane->p0.y;
	z0 = pplane->p0.z;

	nx = pplane->n.x;
	ny = pplane->n.y;
	nz = pplane->n.z;


	if (ny > - MIN_GEOMETRIC_VALUE && ny < MIN_GEOMETRIC_VALUE) {
		res = 0;
		goto func_exit;
	}

	*py = y0 - (1.0/ny) * (nz * (z-z0) + nx*(x-x0));

	res = 1;
func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Line - Line angle   (They could not interconnect... no problem.)
 *	@param[in]  pl0
 *				1st line.
 *	@param[in]  pl1
 *				2nd line.
 *	@param[out]  pangle
 *              0=< angle <= 90. (degree)
 *
 *  @return		0: fail, 1: success
 *	@author	    yhsuk
 *  @date       2018/04/27
 *******************************************************************************/
U32 cr_angle_line_line(
		cr_line_t  *pl0,
		cr_line_t  *pl1,
		double *pangle)
{
	return get_vector_angle(&pl0->m, &pl1->m, pangle);
}


/*!
 ********************************************************************************
 *	@brief      Plane - Plane angle   (They could not interconnect... no problem.)
 *	@param[in]  pl0
 *				1st line.
 *	@param[in]  pl1
 *				2nd line.
 *	@param[out]  pangle
 *              0=< angle <= 90. (degree)
 *
 *  @return		0: fail, 1: success
 *	@author	    yhsuk
 *  @date       2018/04/27
 *******************************************************************************/
U32 cr_angle_plane_plane(
		cr_plane_t *pP0,
		cr_plane_t *pP1,
		double *pangle)
{
	return get_vector_angle(&pP0->n, &pP1->n, pangle);	
}



// distances, and check whether one is on the other or not
/*!
 ********************************************************************************
 *	@brief      Get foot of perpendicular on the line
 *	            See https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
 *	@param[in]  pP
 *				given point
 *	@param[in]  pL
 *				line.
 *	@param[out] pfP
 *				foot of perpendicular on the line
 *
 *  @return		good.
 *
 *	@author	    yhsuk
 *  @date       2020/0423
 *
 *******************************************************************************/
U32  cr_footPerpendicular_Line(
    cr_point_t *pP,
    cr_line_t *pL,
    cr_point_t *pfP
) {

    double res;
    cr_vector_t a;
    cr_vector_t a_p;
    cr_vector_t nn;
    cr_vector_t apnn;
    double apn;
    cr_vector_t b;

    //--
    // Foot of perpendicular = a - ((a-p) * n)n
    //               a: point on the line,   n: unit-vector, direction of line

    memcpy(&a, &pL->p0, sizeof(cr_vector_t));		// a
    cr_vector_sub(&a, pP, &a_p);					// (a-p)
    res = cr_vector_normalization(&pL->m, &nn);		// n 

    apn = cr_vector_inner(&a_p, &nn);				// (a-p) * n
    cr_vector_scalar(&nn, apn, &apnn);				// ((a-p)*n)n
    cr_vector_sub(&a, &apnn, &b);					// a - ((a-p)*n)n

    memcpy(pfP, &b, sizeof(cr_vector_t));
    return 1;
}


/*!
 ********************************************************************************
 *	@brief      Distance between point and line.
 *	            See https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
 *	@param[in]  pP
 *				given point
 *	@param[in]  pL
 *				line.
 *
 *  @return		distance.
 *
 *	@author	    yhsuk
 *  @date       2019/04/21
 *
 *******************************************************************************/
double cr_distance_P_Line(
    cr_point_t *pP,
    cr_line_t *pL) {
    double res;
    cr_point_t fP;
    cr_point_t P2fP;

    //--

    cr_footPerpendicular_Line(
        pP,
        pL,
        &fP
    );
    cr_vector_sub(&fP, pP, &P2fP);
    res = cr_vector_norm(&P2fP);


    //{
    //	double res2;
    //	res2 = cr_distance_P_Line_legacy(pP, pL);
    ////	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %le vs %le  (%le)\n", res, res2, res - res2);
    //}
    return res;
}

/*!
 ********************************************************************************
 *	@brief      Check if a given point is on the line.
 *	@param[in]  pP
 *				given point
 *	@param[in]  pL
 *				line.
 *
 *  @return		smaller, on the line. None-negative.
 *
 *	@author	    yhsuk
 *  @date       2014/02/21
 *******************************************************************************/
double cr_check_P_on_Line(
    cr_point_t *pP,
    cr_line_t *pL) {
    double res;

    res = cr_distance_P_Line(pP, pL);

    return res;
}

/*!
 ********************************************************************************
 *	@brief      Get foot of perpendicular on the plane
 *	            See https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_plane
 *	@param[in]  pP
 *				given point
 *	@param[in]  ppl
 *				plane
 *	@param[out] pfP
 *				foot of perpendicular on the plane
 *
 *  @return		good.
 *
 *	@author	    yhsuk
 *  @date       2020/0507
 *
 *******************************************************************************/
U32  cr_footPerpendicular_Plane(
    cr_point_t *pP,
    cr_plane_t *ppl,
    cr_point_t *pfP
) {
    U32  res;
    cr_vector_t p;
    cr_vector_t y_p;

    cr_vector_t nn;
    cr_vector_t apnn;
    double apn;
    cr_vector_t x;

    //--
    // Foot of perpendicular x = y - (((y-p) * n) / (n*n)) n
    //               y: point on the line,   n: normal vector of the plane. p: (fixed) point on the plane.

    memcpy(&p, &ppl->p0, sizeof(cr_vector_t));		// p
    cr_vector_sub(pP, &p, &y_p);					// (y-p)
    cr_vector_normalization(&ppl->n, &nn);			// n, with normalization. (unit-vector)

    apn = cr_vector_inner(&y_p, &nn);				// (y-p) * n
    cr_vector_scalar(&nn, apn, &apnn);				// ((y-p)*n)n / (n*n)      -> (n*n) = 1 
    cr_vector_sub(pP, &apnn, &x);					// y - ((y-p)*n)n / (n*n)

    memcpy(pfP, &x, sizeof(cr_vector_t));

    res = 1;
    return res;
}

/*!
 ********************************************************************************
 *	@brief      Get the distance between a point and a plane
 *
 *	@param[in]  pP
 *				given point
 *	@param[in]  ppl
 *				given plane
 *
 *  @return		distance.
 *
 *	@author	    yhsuk
 *  @date       2020/0423
 *******************************************************************************/
double cr_distance_P_Plane(
    cr_point_t *pP,
    cr_plane_t *ppl) {
    double res;
    cr_point_t fP;
    cr_point_t P2fP;

    //--

    cr_footPerpendicular_Plane(
        pP,
        ppl,
        &fP
    );
    cr_vector_sub(&fP, pP, &P2fP);
    res = cr_vector_norm(&P2fP);

    return res;
}


/*!
 ********************************************************************************
 *	@brief      Check if a given point is on the Plane
 *	@param[in]  pP
 *				given point
 *	@param[in]  pPL
 *				plane.
 *
 *  @return		smaller, on the Plane. None-negative
 *
 *	@author	    yhsuk
 *  @date       2014/02/21
 *******************************************************************************/
double cr_check_P_on_Plane(
    cr_point_t *pP,
    cr_plane_t *pPL) {
    double res;

    res = cr_distance_P_Plane(pP, pPL);

    return res;
}


/*!
 ********************************************************************************
 *	@brief      Check if a given line is on the Plane
 *	@param[in]  pL
 *				given line
 *	@param[in]  pPL
 *				plane.
 *
 *  @return		smaller, on the Plane. non-negative
 *
 *	@author	    yhsuk
 *  @date       2014/02/21
 *******************************************************************************/
double cr_check_Line_on_Plane(
    cr_line_t *pL,
    cr_plane_t *pPL) {

    double res, res1, res2;
    cr_point_t P1, P2;

    //---
    P1.x = pL->p0.x;				// P1.
    P1.y = pL->p0.y;
    P1.z = pL->p0.z;


    cr_vector_add(&P1, &pL->m, &P2);	// P2


    res1 = cr_check_P_on_Plane(&P1, pPL);
    res2 = cr_check_P_on_Plane(&P2, pPL);

    res = res1 + res2;

    return res;
}


// intersection of two objects
/*!
 ********************************************************************************
 *	@brief      Line-plane intersection point
 *	@param[in]  pl
 *				line.
 *	@param[in]  pplane
 *				plane
 *	@param[out] ppo
 *				intersection point
 *
 *  @return		0: fail, 1: success
 *
 *	@author	    yhsuk
 *  @date       2012/03/13
 *******************************************************************************/
U32 cr_point_line_plane(
    cr_line_t  *pl,
    cr_plane_t *pplane,
    cr_point_t *pp2) {
    U32 res;
    double d0, d1, d2;
    cr_vector_t v0;

    //------------------
///////	//d = (l0 - p0) * n / (l*n)
    //d = (p0 - l0) * n / (l*n)
    // -> p = d l + l0

    //// v0 = l0 - p0
    //v0.x = pl->p0.x - pplane->p0.x;
    //v0.y = pl->p0.y - pplane->p0.y;
    //v0.z = pl->p0.z - pplane->p0.z;


    // v0 = p0 - l0
    v0.x = pplane->p0.x - pl->p0.x;
    v0.y = pplane->p0.y - pl->p0.y;
    v0.z = pplane->p0.z - pl->p0.z;



    // d0 = v0 * n
    d0 = cr_vector_inner(&v0, &pplane->n);

    // d1 = l * n
    d1 = cr_vector_inner(&pl->m, &pplane->n);

    if (d1 > -MIN_GEOMETRIC_VALUE && d1 < MIN_GEOMETRIC_VALUE) {		// fail..
        res = 0;
        goto func_exit;
    }

    d2 = d0 / d1;

    // d * l
    cr_vector_scalar(&pl->m, d2, &v0);

    // p = (d * l) + l0
//	cr_vector_add(&v0, &pl->p0, v1);

    pp2->x = v0.x + pl->p0.x;
    pp2->y = v0.y + pl->p0.y;
    pp2->z = v0.z + pl->p0.z;
    res = 1;

func_exit:
    return res;
}

/*!
 ********************************************************************************
 *	@brief      Line - Line intersection point
 *	@param[in]  pl0
 *				1st line.
 *	@param[in]  pl1
 *				2nd line.
 *	@param[out] ppo0
 *				intersection point, on the 1st line.
 *	@param[out] ppo1
 *				intersection point, on the 2nd line.
 *  @return		0: fail, 1: success
 *
 *	@author	    yhsuk
 *  @date       2016/07/05
 *******************************************************************************/
U32 cr_point_line_line(
    cr_line_t  *pl0,
    cr_line_t  *pl1,
    cr_point_t *pp0,
    cr_point_t *pp1) {
    U32 res;

    cr_vector_t n0;
    cr_point_t  p0;
    cr_vector_t n1;
    cr_point_t  p1;
    cr_vector_t p0_p1;

    double t, s;
    double a, b, e;
    double c, d, f;
    double determin;

    //--

    memcpy(&n0, &pl0->m, sizeof(cr_vector_t));
    memcpy(&p0, &pl0->p0, sizeof(cr_point_t));

    memcpy(&n1, &pl1->m, sizeof(cr_vector_t));
    memcpy(&p1, &pl1->p0, sizeof(cr_point_t));

    cr_vector_sub(&p0, &p1, &p0_p1);


    //  a t + b s = e;
    //  c t + d s = f;
    // -> t = ( d e - b f) / (a d - b c)
    // -> s = (-c e + a f) / (a d - b c)

    a = cr_vector_inner(&n0, &n0);
    b = -cr_vector_inner(&n0, &n1);
    c = b;
    d = cr_vector_inner(&n1, &n1);

    e = -cr_vector_inner(&n0, &p0_p1);
    f = +cr_vector_inner(&n1, &p0_p1);

    determin = a * d - b * c;

    if (determin > -GEOMETRIC_SMALL_VALUE && determin < GEOMETRIC_SMALL_VALUE) {
        res = 0;
        goto func_exit;
    }

    t = (d * e - b * f) / determin;
    s = (-c * e + a * f) / determin;

    // pp0 = p0 + n0 * t
    cr_vector_scalar(&n0, t, pp0);		// n0 * t
    cr_vector_add(&p0, pp0, pp0);		// p0 + (n0 * t)

    // pp1 = p1 + n1 * s
    cr_vector_scalar(&n1, s, pp1);		// n1 * s
    cr_vector_add(&p1, pp1, pp1);		// p1 + (n1 * s)

    res = 1;

func_exit:
    return res;
}


/*!
 ********************************************************************************
 *	@brief      Plane - Plane intersection line 
 *	@param[in]  ppl0
 *				1st plane.
 *	@param[in]  ppl1
 *				2nd plane
 *	@param[out] plo
 *				intersection line
 *  @return		0: fail, 1: success
 *
 *	@author	    yhsuk
 *  @date       2019/12/27
 *******************************************************************************/
U32 cr_line_plane_plane(
		cr_plane_t *ppl0,
		cr_plane_t *ppl1,
		cr_line_t  *plo)
{
	U32 res;

	cr_point_t p0;
	cr_point_t p00;
	cr_point_t p01;
	cr_vector_t m;

	cr_plane_t	npl0;		// normalized plane 0
	cr_plane_t	npl1;		// normalized plane 1

	double norm0, norm1;	// norm of plane0, 1
	double h0, h1;			// normalized h.

	double c0, c1;
	double dotpvalue;

	//--
	//---------------------------------------------------------
	//-- (A-1) Get normalized plane 0, plane 1

	norm0 = cr_vector_norm(&ppl0->n);
	norm1 = cr_vector_norm(&ppl1->n);

#define TOO_SMALL_NORM	(1e-5)
	if (norm0 < TOO_SMALL_NORM || norm1 < TOO_SMALL_NORM) {
		res = 0;
		goto func_exit;
	}
	cr_vector_scalar(&ppl0->n,  1./norm0, &npl0.n);	// Normalized plane.
	cr_vector_scalar(&ppl0->p0, 1.0, &npl0.p0);
	h0 = cr_vector_inner(&npl0.n, &npl0.p0);
	// n * (r - r0) = 0  -> n * r = n * r0 = h   -> h = n * r0

	cr_vector_scalar(&ppl1->n,  1./norm1, &npl1.n);	// Normalized plane.
	cr_vector_scalar(&ppl1->p0, 1.0, &npl1.p0);
	h1 = cr_vector_inner(&npl1.n, &npl1.p0);

	{
		double th;
		th = acos(h1);
		th = th * 180 / 3.141592;
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "h1: %lf th: %lf\n", h1, th);
	}

	//-- (A-2) get direction vector of intersection line.  m = n_p0 x n_p1
	cr_vector_cross(&npl0.n, &npl1.n, &m);

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "m: %10lf %10lf %10lf\n", m.x, m.y, m.z);
	//-- (A-3) get c0 and c1   (See plane (geometry) of wikipedia.. :)
//	c0 = (h0 - h1 (n0 * n1)) / (1 - (n0 * n1)^2)
	dotpvalue = cr_vector_inner(&npl0.n, &npl1.n);
	c0 = (h0 - h1 * dotpvalue) / ( 1. - dotpvalue * dotpvalue);

	c1 = (h1 - h0 * dotpvalue) / ( 1. - dotpvalue * dotpvalue);

	//-- (A-4) Fixed point p0 = c0 * n0 + c1 * n1

	cr_vector_scalar(&npl0.n, c0, &p00);
	cr_vector_scalar(&npl1.n, c1, &p01);
	
	cr_vector_add(&p00, &p01, &p0);
	
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "p: %10lf %10lf %10lf\n", p0.x, p0.y, p0.z);
	//-- Line!
	cr_line_p0m(&p0, &m, plo);
	res = 1;

func_exit:
	return res;
}



// sphere related functions
/*!
 ********************************************************************************
 *	@brief      Make sphere
 *	@param[in] pp
 *				center point
 *	@param[in] r
 *				radius
 *	@param[out] psp
 *				sphere
 *  @return		none
 *
 *	@author	    yhsuk
 *  @date       2017/0711
 *******************************************************************************/
void	cr_sphere_make(cr_point_t *pp, double r, cr_sphere_t *psp)
{
	memcpy(&psp->p0, pp, sizeof(cr_point_t));
	psp->r = r;
}


/*!
 ********************************************************************************
 *	@brief      Get line - sphere intersection point, strict.
 *	@param[in] pl
 *				line
 *	@param[in] psp
 *				sphere
 *	@param[out] pp
 *				intersection point
 *	@param[out] pcount
 *				intersection count, max 2.
 *	@param[out] pD
 *				Determinant value. (+: 2 points, 0: 1 point, -: no)
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2017/0711
 *******************************************************************************/
U32		cr_line_sphere( cr_line_t *pl, cr_sphere_t *psp, cr_point_t pp[2], U32 *pcount, double *pD)
{
	U32 res;

	res = cr_line_sphere_mild(pl, psp, pp, pcount, pD);


	if (res == 0) {
		*pcount = 0;
		*pD = -1;
	} else if (*pD < 0) {
		res = 0;
		*pcount = 0;
	}

	return res;
}

/*!
 ********************************************************************************
 *	@brief      Get line - sphere intersection point, mild condition
 *	@param[in] pl
 *				line
 *	@param[in] psp
 *				sphere
 *	@param[out] pp
 *				intersection point
 *	@param[out] pcount
 *				intersection count, max 2.
 *	@param[out] pD
 *				Determinant value. (+: 2 points, 0: 1 point, -: no... but pseudo-interconnected point)
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2017/0711
 *******************************************************************************/
U32		cr_line_sphere_mild( cr_line_t *pl, cr_sphere_t *psp, cr_point_t pp[2], U32 *pcount, double *pD)
{
	U32 res;

	cr_point_t o;
	cr_vector_t l;
	cr_point_t Ct;
	double r;

	cr_vector_t o_Ct;

	double det;
	double d0, d1;

	double a, b, c;

	cr_point_t x0, x1;
	//------------------------------------------------------------
	// See https://en.wikipedia.org/wiki/Line-sphere_intersection
	// point x = o + d l   
	//      where   o: fixed point of line
	//      		l: direction vector
	//      		d: multiply scalar
	// ->  a d^2 + b d + c = 0
	//      where 	a = l dot l = |l|^2
	//      		b = 2 (l dot (o - Ct)),    Ct: Center of Sphere
	//      		c = (o-Ct) dot (o-Ct) - r^2 = |o-Ct|^2 - r^2,     r: radius of sphere
	//             

	memcpy(&o, &pl->p0, sizeof(cr_point_t));
	memcpy(&l, &pl->m, sizeof(cr_vector_t));
	
	memcpy(&Ct, &psp->p0, sizeof(cr_point_t));

	r = psp->r;
	a = cr_vector_inner(&l, &l);

	if (a < GEOMETRIC_SMALL_VALUE) {	
		res = 0;
		goto func_exit;
	}
	cr_vector_sub(&o, &Ct, &o_Ct);			// o_Ct = o - Ct
	b = 2 * cr_vector_inner(&l, &o_Ct);
	c = cr_vector_inner(&o_Ct, &o_Ct) - r*r;
	det = b*b - 4*a*c;

	*pD = det;
	if (det < GEOMETRIC_SMALL_VALUE) {	
		if (det >= 0) {
			*pD = 0;
		}
		det = 0;

		d0 = -b / (2*a);

		d1 = 0;				// Fake.
		*pcount = 1;
	} else {
		d0 = (-b - sqrt(det)) / (2*a);
		d1 = (-b + sqrt(det)) / (2*a);
		*pcount = 2;
	}

	//		x0 = o + d0 * l;
	cr_vector_scalar(&l, d0, &x0);
	cr_vector_add(&o, &x0, &pp[0]);

	//		x1 = o + d1 * l;
	cr_vector_scalar(&l, d1, &x1);
	cr_vector_add(&o, &x1, &pp[1]);

	res = 1;
func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      normalizes point(converts from pixel coord to normal-plane coordinate)
 *	@param[in]  psrc
 *				input point, which is a pixel coordinated point
 *	@param[out] pdst
 *				output point, which is a converted normal-plane coordianted point
 *	@param[in]  cameraParameter
 *				the intrinsic camera parameters
 *
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       unknown
 *******************************************************************************/
U32 cr_intrinsic_normalize(
		point_t *psrc,
		point_t *pdst,
		double cameraParameter[4]		// fx, cx, fy, cy
		)
{
	U32 res;
	double fx, cx, fy, cy;

	//---

	fx = cameraParameter[0];
	cx = cameraParameter[1];
	fy = cameraParameter[2];
	cy = cameraParameter[3];


#define MINFX	10
#define MINFY	10
	if (fx < MINFX || fy < MINFY) {
		res = 0;
	} else {
		pdst->x = (psrc->x - cx) / fx;
		pdst->y = (psrc->y - cy) / fy;

		res = 1;
	}
	return res;
}


/*!
 ********************************************************************************
 *	@brief      denormalizes point(converts from normal-plane coordinate to pixel coord)
 *	@param[in]  psrc
 *				input point, which is a normal-plane coordianted point
 *	@param[out] pdst
 *				output point, which is a converted pixel coordinated point
 *	@param[in]  cameraParameter
 *				the intrinsic camera parameters
 *
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       unknown
 *******************************************************************************/
U32 cr_intrinsic_denormalize(
		point_t *psrc,
		point_t *pdst,
		double cameraParameter[4]		// fx, cx, fy, cy
		)
{
	U32 res;
	double fx, cx, fy, cy;

	//---

	fx = cameraParameter[0];
	cx = cameraParameter[1];
	fy = cameraParameter[2];
	cy = cameraParameter[3];


#define MINFX	10
#define MINFY	10
	if (fx < MINFX || fy < MINFY) {
		res = 0;
	} else {
		pdst->x = fx * psrc->x + cx;
		pdst->y = fy * psrc->y + cy;
		res = 1;
	}
	return res;
}

/*!
 ********************************************************************************
 *	@brief      Distort image point(pixel point) with camera intrinsic parameter and distort coefficient
 *	@param[in]  psrc
 *				Undistorted point
 *	@param[out] pdst
 *				distorted point
 *	@param[in]  cameraParameter
 *				camera intrinsic parameter. fx, cx, fy, cy
 *	@param[in]  distCoeffs
 *				camera distortion parameter. k1, k2, p1, p2, k3
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2017/0711
 *******************************************************************************/
U32 cr_distortPoint
(
		point_t *psrc,
		point_t *pdst,
		double cameraParameter[4],		// fx, cx, fy, cy
		double distCoeffs[5]			// k1, k2, p1, p2, k3
		)
{
#if 0
	U32 res;
	double fx, cx, fy, cy;
	double k1, k2, p1, p2, k3;

	double xd, yd;
	double xd_, yd_;
	double xc, yc;
	double xc_, yc_;

	double r2, kkk;


	//---

	fx = cameraParameter[0];
	cx = cameraParameter[1];
	fy = cameraParameter[2];
	cy = cameraParameter[3];

	k1 = distCoeffs[0];
	k2 = distCoeffs[1];
	p1 = distCoeffs[2];
	p2 = distCoeffs[3];
	k3 = distCoeffs[4];
#define MINFX	10
#define MINFY	10
	if (fx < MINFX || fy < MINFY) {
		res = 0;
		goto func_exit;
	}

	xd = psrc->x;
	yd = psrc->y;

	xd_ = (xd - cx) / fx;
	yd_ = (yd - cy) / fy;

	r2 = xd_*xd_ + yd_*yd_;

	kkk = (1 + k1*r2 + k2*r2*r2 + k3*r2*r2*r2);

	xc_ =  kkk*xd_ + (2*p1*xd_*yd_ + p2*(r2 + 2*xd_*xd_));
	yc_ =  kkk*yd_ + (2*p2*xd_*yd_ + p1*(r2 + 2*yd_*yd_));

	xc = xc_ * fx + cx;
	yc = yc_ * fy + cy;

	pdst->x = xc;
	pdst->y = yc;


	res = 1;

func_exit:
	return res;
#endif

#if 1
	U32 res;
	point_t nsrc;
	point_t ndst;

	//point_t *psrc,
	//point_t *pdst,

	cr_intrinsic_normalize(psrc, &nsrc, cameraParameter);
	res = cr_distortPoint_normalized (&nsrc, &ndst, distCoeffs);
	cr_intrinsic_denormalize(&ndst, pdst, cameraParameter);

	return res;
#endif

}

/*!
 ********************************************************************************
 *	@brief      Distort normalized point with distort coefficients. Intrinsic parameters are not needed b/c the input point is alredy normalized
 *	@param[in]  psrc
 *				Undistorted point
 *	@param[out] pdst
 *				distorted point
 *	@param[in] distCoeffs
 *				camera distortion parameter. k1, k2, p1, p2, k3
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2017/0711
 *******************************************************************************/
U32 cr_distortPoint_normalized (
		point_t *psrc,					// normalized.
		point_t *pdst,					// normalized.
		double distCoeffs[5]			// k1, k2, p1, p2, k3
		)
{
	U32 res;
	double k1, k2, p1, p2, k3;

	double xd_, yd_;
	double xc_, yc_;

	double r2, kkk;


	//---


	k1 = distCoeffs[0];
	k2 = distCoeffs[1];
	p1 = distCoeffs[2];
	p2 = distCoeffs[3];
	k3 = distCoeffs[4];

	xd_ = psrc->x;
	yd_ = psrc->y;

	r2 = xd_*xd_ + yd_*yd_;

	kkk = (1 + k1*r2 + k2*r2*r2 + k3*r2*r2*r2);

	xc_ =  kkk*xd_ + (2*p1*xd_*yd_ + p2*(r2 + 2*xd_*xd_));
	yc_ =  kkk*yd_ + (2*p2*xd_*yd_ + p1*(r2 + 2*yd_*yd_));

	pdst->x = xc_;
	pdst->y = yc_;

	res = 1;

	return res;
}


/*!
 ********************************************************************************
 *	@brief      Undistort image point(pixel point) with camera intrinsic parameter and distort coefficient
 *	@param[in]  psrc
 *				distorted point
 *	@param[out] pdst
 *				undistorted point
 *	@param[in]  cameraParameter
 *				camera intrinsic parameter. fx, cx, fy, cy
 *	@param[in]  distCoeffs
 *				camera distortion parameter. k1, k2, p1, p2, k3
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2017/0711
 *******************************************************************************/
U32 cr_undistortPoint
(
		point_t *psrc,
		point_t *pdst,
		double cameraParameter[4],		// fx, cx, fy, cy
		double distCoeffs[5]			// k1, k2, p1, p2, k3
		)
{
#if 0
	U32 res;
	I32 n;
	double fx, cx, fy, cy;
	double k1, k2, p1, p2, k3;

	double xd, yd;
	double xd_, yd_;
	double xc, yc;
	double xc_, yc_;

	double xn_, yn_;
	double xnp_, ynp_;

	double rn2, kkk;

	double xn_0, yn_0;
	double xn_d, yn_d;
	double xyn_d;
	U32 xyn_d_small;
	//----
	fx = cameraParameter[0];
	cx = cameraParameter[1];
	fy = cameraParameter[2];
	cy = cameraParameter[3];

	k1 = distCoeffs[0];
	k2 = distCoeffs[1];
	p1 = distCoeffs[2];
	p2 = distCoeffs[3];
	k3 = distCoeffs[4];
#define MINFX	10
#define MINFY	10
	if (fx < MINFX || fy < MINFY) {
		res = 0;
		goto func_exit;
	}

	xc = psrc->x;
	yc = psrc->y;

	xc_ = (xc - cx) / fx;
	yc_ = (yc - cy) / fy;

	//---
	xn_ = xc_; yn_ = yc_;
	rn2 = xn_*xn_ + yn_*yn_;

	xn_0 = 999;
	yn_0 = 999;
	xyn_d = 0;
	xyn_d_small = 0;
	for (n = 0; n < s_KITER; n++) {
		kkk = (1+k1*rn2 + k2*rn2*rn2 + k3*rn2*rn2*rn2);

		xnp_ = xc_ - (2*p1*xn_*yn_ + p2*(rn2 + 2*xn_*xn_));
		xnp_ = xnp_ / kkk;

		ynp_ = yc_ - (2*p2*xn_*yn_ + p1*(rn2 + 2*yn_*yn_));
		ynp_ = ynp_ / kkk;

		xn_  = xnp_;
		yn_  = ynp_;
		rn2 = xn_*xn_ + yn_*yn_;

		xn_d = xn_ - xn_0;
		yn_d = yn_ - yn_0;

		xyn_d = sqrt(xn_d*xn_d + yn_d * yn_d);

#define XYN_D (1e-4)
		if (xyn_d < XYN_D) {
			xyn_d_small++;
#define XYN_D_SMALL 5
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d]xyn_d: %lf, xyn_d_small: %d\n", n, xyn_d, xyn_d_small);

			if (xyn_d_small > XYN_D_SMALL) {
				break;
			}
		} else {
			xyn_d_small = 0;
		}
//		xd_ = xn_;
//		yd_ = yn_;

//		xd = xd_ * fx + cx;
//		yd = yd_ * fy + cy;
		xn_0 = xn_;
		yn_0 = yn_;
	}

	xd_ = xn_;
	yd_ = yn_;

	xd = xd_ * fx + cx;
	yd = yd_ * fy + cy;

	pdst->x = xd;
	pdst->y = yd;


	res = 1;
func_exit:
	return res;

#endif

#if 1
	U32 res;
	point_t nsrc;
	point_t ndst;

	//point_t *psrc,
	//point_t *pdst,

	cr_intrinsic_normalize(psrc, &nsrc, cameraParameter);
	res = cr_undistortPoint_normalized (&nsrc, &ndst, distCoeffs);
	cr_intrinsic_denormalize(&ndst, pdst, cameraParameter);

	return res;
#endif

}

/*!
 ********************************************************************************
 *	@brief      Undistort normalized point with distort coefficients. Intrinsic parameters are not needed b/c the input point is alredy normalized
 *	@param[in]  psrc
 *				Distorted point
 *	@param[out] pdst
 *				Undistorted point
 *	@param[in]  distCoeffs
 *				camera distortion parameter. k1, k2, p1, p2, k3
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2017/0711
 *******************************************************************************/
U32 cr_undistortPoint_normalized (
	point_t *psrc,
	point_t *pdst,
	double distCoeffs[5]			// k1, k2, p1, p2, k3
)
{
	U32 res;
	I32 n;
	double k1, k2, p1, p2, k3;

	double xd_, yd_;
	double xc_, yc_;

	double xn_, yn_;
	double xnp_, ynp_;

	double rn2, kkk;
	//----
	k1 = distCoeffs[0];
	k2 = distCoeffs[1];
	p1 = distCoeffs[2];
	p2 = distCoeffs[3];
	k3 = distCoeffs[4];

	//#define KITER	10
#define KITER	50
	xc_ = psrc->x;
	yc_ = psrc->y;

	//---
	xn_ = xc_; yn_ = yc_;
	rn2 = xn_*xn_ + yn_*yn_;

	for (n = 0; n < KITER; n++) {
		kkk = (1+k1*rn2 + k2*rn2*rn2 + k3*rn2*rn2*rn2);

		xnp_ = xc_ - (2*p1*xn_*yn_ + p2*(rn2 + 2*xn_*xn_));
		xnp_ = xnp_ / kkk;

		ynp_ = yc_ - (2*p2*xn_*yn_ + p1*(rn2 + 2*yn_*yn_));
		ynp_ = ynp_ / kkk;

		xn_  = xnp_;
		yn_  = ynp_;
		rn2 = xn_*xn_ + yn_*yn_;
	}

	xd_ = xn_;
	yd_ = yn_;

	pdst->x = xd_;
	pdst->y = yd_;

	res = 1;
	return res;

}

/*!
 ********************************************************************************
 *	@brief      Undistort Image with camera intrinsic parameter and distort coefficient
 *	@param[in]  pbufDist
 *				distorted image
 *	@param[out] pbufUndist
 *				undistorted image
 *	@param[in]  width
 *				width of the image
 *	@param[in]  height
 *				height of the image
 *	@param[in]  sx
 *				x coordnate of beginning(starting) point, in pixel, to undistort
 *	@param[in]  sy
 *				y coordnate of beginning(starting) point, in pixel, to undistort
 *	@param[in]  ex
 *				x coordnate of ending point, in pixel, to undistort
 *	@param[in]  ey
 *				y coordnate of ending point, in pixel, to undistort
 *	@param[in]  pixelbyte
 *				number of channels(not sure), only 1C or 3C is allowed
 *	@param[in]  cameraParameter
 *				camera intrinsic parameter. fx, cx, fy, cy
 *	@param[in]  distCoeffs
 *				camera distortion parameter. k1, k2, p1, p2, k3
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2017/0912
 *******************************************************************************/
U32 cr_undistort_image(
		U08 *pbufDist,		// source
		U08 *pbufUndist,	// Destination
		U32 width,
		U32 height,
		U32 sx, U32 sy,
		U32 ex, U32 ey,
		U32 pixelbyte,
		
		double cameraParameter[4],		// fx, cx, fy, cy
		double distCoeffs[5]			// k1, k2, p1, p2, k3
		)
{
	U32 res;
	I32 x, y;
	I32 xDist, yDist;
	I32 b;
	point_t PtDist, PtUndist;
	U08 *psrc, *pdst;

	//---
	if (pixelbyte != 1 && pixelbyte != 3) {
		res = 0;
		goto func_exit;
	}

	if (sx >= width || ex >= width || sy >= height || ey >= height || sx > ex || sy > ey) {
		res = 0;
		goto func_exit;
	}

	//memset(pbufUndist, 0, width*height*pixelbyte);
	memset(pbufUndist, 0xFF, width*height*pixelbyte);
    for (y = sy; y <= (I32)ey; y++) {
		PtUndist.y = y;
		for (x = sx; x <= (I32)ex; x++) {
			PtUndist.x = x;

			cr_distortPoint(&PtUndist, &PtDist, cameraParameter, distCoeffs);
			xDist = (I32) (PtDist.x + 0.1);
			yDist = (I32) (PtDist.y + 0.1);

			if (xDist >= 0 && xDist < (I32) width && yDist >= 0 && yDist < (I32)height) {
				psrc = pbufDist 	+ (xDist + yDist * width) * pixelbyte;
				pdst = pbufUndist 	+ (x + y * width) * pixelbyte;
				for (b = 0; b < (I32) pixelbyte; b++) {
					*(pdst+b) = *(psrc+b);
				}
			}
		}
	}

	res = 1;

func_exit:
	return res;
}



/*!
 ********************************************************************************
 *	@brief      get bivariate quadratic transform matrix
 *	@param[out] bqmat
 *					bi-quad transform matrix (6x2)
 *	@param[in]  frompoints
 *					from points
 *	@param[in]  topoints
 *					to points
 *	@param[in]  count
 *					count of points
 *
 *  @return		0: Fail. Numerical calculation error.
 *  			1: Success.
 *
 *	@author	    yhsuk
 *  @date       2011/08/09
 *******************************************************************************/
I32 cr_bi_quadratic_matrix(
    biquad_t 	*bqmat, 		// bi-quad transform matrix (6x2)
    point_t *frompoints,	// from point data
    point_t *topoints,		// to point data
    U32 count)			// count of point
{
#ifdef IPP_SUPPORT
    I32 res;
    U32 i;
    double xp, xxp;
    double yp, yyp;
    double xyp;

    double xl;
    double yl;

    IppStatus ippstatus;
    Ipp64f *pmat;
    Ipp64f *lmat;
    Ipp64f PpTPp[6*6];
    Ipp64f invPpTPp[6*6];
    Ipp64f PpTPl[6*2];
    Ipp64f Ap2lMat[6*2];
    Ipp64f pBuffer[6*6 + 6];

    int src1Stride1;
    int src1Stride2;

    int src2Stride1;
    int src2Stride2;

    int dstStride1;
    int dstStride2;

    pmat = (Ipp64f *)malloc(sizeof(Ipp64f) * (count * 6));
    lmat = (Ipp64f *)malloc(sizeof(Ipp64f) * (count * 2));

    for (i = 0; i < count; i++) {
        // from points
        xp 	= frompoints[i].x;
        xxp = xp * xp;
        yp 	= frompoints[i].y;
        yyp = yp * yp;
        xyp = xp * yp;

        pmat[i*6 + 0] = (Ipp64f)xp;
        pmat[i*6 + 1] = (Ipp64f)yp;
        pmat[i*6 + 2] = (Ipp64f)xyp;
        pmat[i*6 + 3] = (Ipp64f)xxp;
        pmat[i*6 + 4] = (Ipp64f)yyp;
        pmat[i*6 + 5] = (Ipp64f) 1.;

        // to points
        xl 	= topoints[i].x;
        yl 	= topoints[i].y;

        lmat[i*2 + 0] = (Ipp64f)xl;
        lmat[i*2 + 1] = (Ipp64f)yl;
    }

    //--------------------------------
    // A = (PpT * Pp) ^-1 * PpT * Pl

    //  (PpT * Pp)
    src1Stride1 = sizeof(Ipp64f) * 6;
    src1Stride2 = sizeof(Ipp64f);

    src2Stride1 = sizeof(Ipp64f) * 6;
    src2Stride2 = sizeof(Ipp64f);

    dstStride1 = sizeof(Ipp64f) * 6;
    dstStride2 = sizeof(Ipp64f);

#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---Pp\n");
    for (i = 0; i < 6; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Pp Pp[%d]: %10f %10f %10f %10f %10f %10f \n",
            i,
            pmat[i*6+0],
            pmat[i*6+1],
            pmat[i*6+2],
            pmat[i*6+3],
            pmat[i*6+4],
            pmat[i*6+5]);
    }
#endif

    ippstatus = ippmMul_tm_64f(
        pmat,					// PpT,    	6xN
        src1Stride1,
        src1Stride2,
        6,
        count,

        pmat,					// Pp,		Nx6
        src2Stride1,
        src2Stride2,
        6,
        count,

        PpTPp,					// PpT * Pp, 6x6
        dstStride1,
        dstStride2);

#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---PpT * Pp\n");
    for (i = 0; i < 6; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "PpT Pp[%d]: %20lf %20lf %20lf %20lf %20lf %20lf \n",
            i,
            PpTPp[i * 6 + 0],
            PpTPp[i * 6 + 1],
            PpTPp[i * 6 + 2],
            PpTPp[i * 6 + 3],
            PpTPp[i * 6 + 4],
            PpTPp[i * 6 + 5]);
    }
#endif


    //#define PFACTOR		10000.
    //#define PFACTOR		1.
#if defined(PFACTOR)
    for (i = 0; i < 6 * 6; i++) {
        PpTPp[i] /= PFACTOR;
    }
#endif

    //  (PpT * Pp) ^-1
    src1Stride1 = sizeof(Ipp64f) * 6;
    src1Stride2 = sizeof(Ipp64f);

    dstStride1 = sizeof(Ipp64f) * 6;
    dstStride2 = sizeof(Ipp64f);

    ippstatus = ippmInvert_m_64f(
        PpTPp,
        src1Stride1,
        src1Stride2,
        pBuffer, 		// ( 6 * 6 + 6)
        invPpTPp,
        dstStride1,
        dstStride2,
        6);
#if defined(PFACTOR)
    for (i = 0; i < 6 * 6; i++) {
        invPpTPp[i] /= PFACTOR;
    }
#endif

#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---invPpTPp\n");
    for (i = 0; i < 6; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "invPpTPp[%d]: %20lf %20lf %20lf %20lf %20lf %20lf \n",
            i,
            invPpTPp[i * 6 + 0],
            invPpTPp[i * 6 + 1],
            invPpTPp[i * 6 + 2],
            invPpTPp[i * 6 + 3],
            invPpTPp[i * 6 + 4],
            invPpTPp[i * 6 + 5]);
    }
#endif

    //  (PpT * Pl)
    src1Stride1 = sizeof(Ipp64f) * 6;
    src1Stride2 = sizeof(Ipp64f);

    src2Stride1 = sizeof(Ipp64f) * 2;
    src2Stride2 = sizeof(Ipp64f);

    dstStride1 = sizeof(Ipp64f) * 2;
    dstStride2 = sizeof(Ipp64f);

    ippstatus = ippmMul_tm_64f(
        pmat,					// PpT,		6xN
        src1Stride1,
        src1Stride2,
        6,
        count,

        lmat,					// Pl,		Nx2
        src2Stride1,
        src2Stride2,
        2,
        count,

        PpTPl,					// PpT * Pl,	6x2
        dstStride1,
        dstStride2);

    // (PpT * Pp) ^-1 * (PpT * Pl)
    src1Stride1 = sizeof(Ipp64f) * 6;
    src1Stride2 = sizeof(Ipp64f);

    src2Stride1 = sizeof(Ipp64f) * 2;
    src2Stride2 = sizeof(Ipp64f);

    dstStride1 = sizeof(Ipp64f) * 2;
    dstStride2 = sizeof(Ipp64f);

    ippstatus = ippmMul_mm_64f(
        invPpTPp,
        src1Stride1,
        src1Stride2,
        6,
        6,
        PpTPl,
        src2Stride1,
        src2Stride2,
        2,
        6,
        Ap2lMat,
        dstStride1,
        dstStride2);

    //
    //t_p2l 	*Ap2l, 			// Pixel to Local transform Matrix A (6x2)
    for (i = 0; i < 6*2; i++) {
        bqmat->mat[i] = (double)Ap2lMat[i];
    }

    free(pmat);
    free(lmat);

    res = 1;

#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---------mat: \n");

    for (i = 0; i < 6 * 2; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "m[%d]: %lf\n", i, bqmat->mat[i]);
    }
#endif

    return res;
#else

    double xp, yp, xyp, xxp, yyp, xl, yl;
    U32 i;
    I32 res;

    cv::Mat cvPointsFrom, cvPointsto;
    cv::Mat pmat(cv::Size(6, count), CV_64FC1), lmat(cv::Size(6, count), CV_64FC1),
        pmatT, pTp, ptpinv, ptpinv_pT_lmat;

    pmat = 0.0;
    lmat = 0.0;

    for (i = 0; i < count; i++) {
        // from points
        xp 	= frompoints[i].x;
        xxp = xp * xp;
        yp 	= frompoints[i].y;
        yyp = yp * yp;
        xyp = xp * yp;

        pmat.at<double>(i, 0) = xp;
        pmat.at<double>(i, 1) = yp;
        pmat.at<double>(i, 2) = xyp;
        pmat.at<double>(i, 3) = xxp;
        pmat.at<double>(i, 4) = yyp;
        pmat.at<double>(i, 5) = 1.;

        // to points
        xl 	= topoints[i].x;
        yl 	= topoints[i].y;

        lmat.at<double>(i, 0) = xl;
        lmat.at<double>(i, 1) = yl;
    }

    //--------------------------------
    // A = (PpT * Pp) ^-1 * PpT * Pl

    //  (PpT * Pp)
#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---Pp\n");
    for (i = 0; i < 6; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Pp Pp[%d]: %10f %10f %10f %10f %10f %10f \n",
            i,
            pmat.at<double>(i, 0),
            pmat.at<double>(i, 1),
            pmat.at<double>(i, 2),
            pmat.at<double>(i, 3),
            pmat.at<double>(i, 4),
            pmat.at<double>(i, 5);
    }
#endif

    cv::transpose(pmat, pmatT);
    pTp = pmatT * pmat;

#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---PpT * Pp\n");
    for (i = 0; i < 6; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "PpT Pp[%d]: %20lf %20lf %20lf %20lf %20lf %20lf \n",
            i,
            pTp.at<double>(i, 0),
            pTp.at<double>(i, 1),
            pTp.at<double>(i, 2),
            pTp.at<double>(i, 3),
            pTp.at<double>(i, 4),
            pTp.at<double>(i, 5));
    }
#endif

    //  (PpT * Pp) ^-1

    cv::invert(pTp, ptpinv);

#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---invPpTPp\n");
    for (i = 0; i < 6; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "invPpTPp[%d]: %20lf %20lf %20lf %20lf %20lf %20lf \n",
            i,
            ptpinv.at<double>(i, 0),
            ptpinv.at<double>(i, 1),
            ptpinv.at<double>(i, 2),
            ptpinv.at<double>(i, 3),
            ptpinv.at<double>(i, 4),
            ptpinv.at<double>(i, 5));
    }
#endif

    ptpinv_pT_lmat = ptpinv * pmatT * lmat;

    memcpy(&(bqmat->mat[0]), ptpinv_pT_lmat.data, sizeof(double) * 6 * 2);

    res = 1;

#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---------mat: \n");

    for (i = 0; i < 6 * 2; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "m[%d]: %lf\n", i, bqmat->mat[i]);
    }
#endif

    return res;
#endif
}


/*!
 ********************************************************************************
 *	@brief      bi-quadratic transfer
 *	@param[in] bqmat
 *					bi-quad transform matrix (6x2)
 *	@param[in]  frompoints
 *					from points
 *	@param[out]  topoints
 *					to points
 *
 *  @return		0: Fail. Numerical calculation error.
 *  			1: Success.
 *
 *	@author	    yhsuk
 *  @date       2011/08/10
 *******************************************************************************/
I32 cr_bi_quadratic(
    biquad_t 	*bqmat, 		// bi-quad transform matrix (6x2)
    point_t *frompoints,	// from point data
    point_t *topoints)		// to point data
{
    double xp, xxp;
    double yp, yyp;
    double xyp;

    double xl;
    double yl;


    xp 	= frompoints->x;
    xxp = xp * xp;
    yp 	= frompoints->y;
    yyp = yp * yp;
    xyp = xp * yp;


    xl =  xp  * bqmat->mat[0]				// p(1,1) * A(1,1)
        + yp  * bqmat->mat[2]				// p(1,2) * A(2,1)
        + xyp * bqmat->mat[4]				// p(1,2) * A(3,1)
        + xxp * bqmat->mat[6]				// p(1,2) * A(4,1)
        + yyp * bqmat->mat[8]				// p(1,2) * A(5,1)
        +       bqmat->mat[10];				// p(1,2) * A(6,1)

    yl =  xp  * bqmat->mat[1]				// p(1,1) * A(1,2)
        + yp  * bqmat->mat[3]				// p(1,2) * A(2,2)
        + xyp * bqmat->mat[5]				// p(1,2) * A(3,2)
        + xxp * bqmat->mat[7]				// p(1,2) * A(4,2)
        + yyp * bqmat->mat[9]				// p(1,2) * A(5,2)
        +       bqmat->mat[11];				// p(1,2) * A(6,2)

    topoints->x = xl;
    topoints->y = yl;

    return 1;
}

#if defined (__cplusplus)
}
#endif

#if 0 // deprecated
double cr_check_P_on_Plane_Legacy
(
    cr_point_t *pP,
    cr_plane_t *pPL)

{
    double res;
    cr_vector_t an;
    cr_vector_t nn;

    //--
    cr_vector_make2(pP, &pPL->p0, &an);			// AN = line(p, PL->p0)

    res = cr_vector_normalization(&an, &an);
    if (res > GEOMETRIC_SMALL_VALUE) {			// On the p0..
        cr_vector_normalization(&pPL->n, &nn);
        res = cr_vector_inner(&an, &nn);

        if (res < 0) {
            res = -res;
        }
    }

    return res;
}

double cr_check_P_on_Line_Legacy
(
    cr_point_t *pP,
    cr_line_t *pL)

{

    double res;
    cr_vector_t an;
    cr_vector_t mn;

    //--
    cr_vector_make2(pP, &pL->p0, &an);			// 

    res = cr_vector_normalization(&an, &an);
    if (res > GEOMETRIC_SMALL_VALUE) {			// On the p0..
        cr_vector_normalization(&pL->m, &mn);

        res = cr_vector_inner(&an, &mn);

        res = 1.0 - res * res;					// 

        if (res < 0) {
            res = -res;
        }
    }

    return res;
}

double cr_distance_P_Line_legacy(
    cr_point_t *pP,
    cr_line_t *pL) {

    double res;
    cr_vector_t a_p;
    cr_vector_t nn;
    cr_vector_t apnn;
    double apn;
    cr_vector_t b;

    //--
    // distance = || (a-p) - ((a-p) * n)n||
    //               a: point on the line,   n: unit-vector, direction of line

    cr_vector_sub(&pL->p0, pP, &a_p);			// (a-p)
    res = cr_vector_normalization(&pL->m, &nn);	// n, 


    apn = cr_vector_inner(&a_p, &nn);
    cr_vector_scalar(&nn, apn, &apnn);		// ((a-p)*n)n
    cr_vector_sub(&a_p, &apnn, &b);

    res = cr_vector_norm(&b);
    return res;
}
#endif