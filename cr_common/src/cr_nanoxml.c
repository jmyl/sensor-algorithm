/*!
 *******************************************************************************
                                                                                
                    CREATZ NANO XML 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2014, 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_nanoxml.c
	 @brief  NANO XML. very very very small xml writer
	 @author YongHo Suk                                 
	 @date   2014/11/08 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#include <string.h>
#include <stdarg.h>

#include "cr_common.h"
#include "cr_math.h"
#include "cr_osapi.h"
#include "cr_nanoxml.h"
#include "cr_misc.h"


/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

nanoxml_t *s_pnx;
nanoxml_t *s_pnx_encryption;

//---
HAND cr_nanoxml_create(char *fn, U32 encryption)
{
	FILE *fp;
	nanoxml_t *pnx;
	
	pnx = (nanoxml_t *) malloc(sizeof(nanoxml_t));

	if (pnx != NULL) {
		memset(pnx, 0, sizeof(nanoxml_t));
		pnx->tabcount = 0;
		pnx->len = 0;

		if (encryption) {
			fp = cr_fopen(fn, "wb");
		} else {
			//pnx->fp = fopen(fn, "w");
			fp = cr_fopen(fn, "w");
		}

		if (fp == NULL) {			// -_-;
			free(pnx);
			pnx = NULL;
		} else {
			if (encryption) {
				U32 i;
				U32 seed;
				
				U32 header[3];					// [seed] [r|m|d|h] [~1]

				cr_make_header(header);

				cr_fwrite(&header[0] , sizeof(U32), 3, fp);
				pnx->length = (sizeof(U32) * 3);

				seed = header[0];

				pnx->hrr = cr_rand_create(seed);
				for (i = 0; i < pnx->length; i++) {
					cr_rand_gendataBYTE(pnx->hrr);		// discard... :P
				}
			} else {
				pnx->length = 0;
				pnx->hrr = NULL;
			}
			pnx->fp = fp;
			pnx->encryption = encryption;
		}
	}

	return (HAND) pnx;
}


void cr_nanoxml_create2(char *fn, U32 encryption)
{
	if (encryption) {
		if (s_pnx_encryption) {
			cr_nanoxml_delete(s_pnx, encryption);
			s_pnx = NULL;
		}
		s_pnx_encryption = (nanoxml_t *)cr_nanoxml_create(fn, encryption);
	} else {
		if (s_pnx) {
			cr_nanoxml_delete(s_pnx, encryption);
			s_pnx = NULL;
		}
		s_pnx = (nanoxml_t *)cr_nanoxml_create(fn, encryption);
	}
}


void cr_nanoxml_delete(HAND hnx, U32 encryption)
{
	nanoxml_t *pnx;
	
	if (hnx) {
		pnx = (nanoxml_t *)hnx;
	} else {
		if (encryption) {
			pnx = s_pnx_encryption;
		} else {
			pnx = s_pnx;
		}
	}
			
	if (pnx) {
		if (pnx->fp) {
			cr_fclose(pnx->fp);
		}

		if (pnx->hrr) {
			cr_rand_delete(pnx->hrr);
		}
		free(pnx);
	}

	if (hnx == NULL) {
		if (encryption) {
			s_pnx_encryption = NULL;
		} else {
			s_pnx = NULL;
		}
	}
}


void cr_nanoxml_insertelement(HAND hnx, U32 encryption, I32 addtab, char *szFormat, va_list vlMarker)
{
//    va_list vlMarker ;

	I32 i;
#define NANOXML_BUFLEN	10240
    char szTempBuf[NANOXML_BUFLEN];

	nanoxml_t *pnx;
	
	if (hnx != NULL) {
		pnx = (nanoxml_t *) hnx;
	} else {
		if (encryption) {
			pnx = s_pnx_encryption;
		} else {
			pnx = s_pnx;
		}
	}
	if (pnx == NULL) {
		goto func_exit;
	}

	if (pnx->encryption != encryption) {
		goto func_exit;
	}


	memset(szTempBuf, 0, NANOXML_BUFLEN);
	if (addtab < 0) {
		pnx->tabcount += addtab;
		if (pnx->tabcount < 0) {
			pnx->tabcount = 0;
		}
	}


	{
		U32 datalen;
		for (i = 0; i < pnx->tabcount; i++) {
			*(szTempBuf+i) = '\t';
		}
		vsprintf((szTempBuf + pnx->tabcount),szFormat,vlMarker) ;

		datalen = (U32)strlen(szTempBuf);
		*(szTempBuf+datalen) = '\n';
		datalen++;


		if (encryption) {
			cr_nanoxml_encryptout(pnx, szTempBuf, datalen+1);
		} else {
			cr_fprintf(pnx->fp, szTempBuf);
		}
		if (pnx->len + datalen < NANOXML_BUFSIZE) {
			memcpy(pnx->buf + pnx->len, szTempBuf, datalen);
			pnx->len += datalen;
		}
	}

	if (addtab > 0) {
		pnx->tabcount += addtab;
	}
func_exit:
	return;
}


void nxmlie(I32 addtab, char *szFormat, ...)
{
    va_list vlMarker ;
	nanoxml_t *pnx;

	//---- PLAIN TEXT
	if (s_pnx) {
		pnx = s_pnx;
		{
			va_start(vlMarker,szFormat) ;
			cr_nanoxml_insertelement(pnx, 0, addtab, szFormat, vlMarker);
			va_end(vlMarker) ;
		}
	}

	//---- Encryption TEXT
	if (s_pnx_encryption) {
		pnx = s_pnx_encryption;
		{
			va_start(vlMarker,szFormat) ;
			cr_nanoxml_insertelement(pnx, 1, addtab, szFormat, vlMarker);
			va_end(vlMarker) ;
		}
	}
}

void cr_nanoxml_encryptout(HAND hnx, char *buf, U32 datalen)
{
	
	if (hnx) {
		U32 i;
		U08 xdata;
		char buf1[NANOXML_BUFLEN];
		nanoxml_t *pnx;

		//---
		pnx = (nanoxml_t *)hnx;
		if (pnx->encryption) {
			if (datalen > NANOXML_BUFLEN) {
				datalen = NANOXML_BUFLEN;
			}
			for (i = 0; i < datalen; i++) {
				xdata = cr_rand_gendataBYTE(pnx->hrr);
				buf1[i] = buf[i] ^ xdata;
			}
			pnx->length += datalen;

			cr_fwrite(&buf1[0] , sizeof(U08), datalen, pnx->fp);
			cr_fflush(pnx->fp);
		}
	}
}

void cr_nanoxml_bufinfo(HAND hnx, U08 **pbuf, U32 *pdatalen)
{
	nanoxml_t *pnx;
	
	if (hnx) {
		pnx = (nanoxml_t *) hnx;
	} else {
		pnx = s_pnx;
	}

	if (pnx) {
		*pbuf = pnx->buf;
		*pdatalen = pnx->len;
	} else {
		*pbuf = NULL;
		*pdatalen = 0;
	}
}


#if defined (__cplusplus)
extern "C" {
#endif

