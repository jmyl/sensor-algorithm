/*!
 *******************************************************************************
                                                                                
                   Creatz Camera 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2018 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_persptrans.c
	 @brief  Coordinate transform 
	 @author YongHo Suk                                 
	 @date   2018/07/14 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#if defined(_DEBUG)
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#endif

#include "cr_common.h"
#include "cr_osapi.h"
#include "cr_geometric.h"

#include "cr_persptrans.h"
#include "assert.h"

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: type definition
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/* Not used */
#if 0
/*!
 ********************************************************************************
 *	@brief      Get perspective Transform matrix
 *	@param[in] point_t src[4]
 *				4 source points
 *	@param[in] point_t dst[4]
 *				4 destination points
 *	@param[out] perspMat[3][3]
 *				Perspective matrix
 *
 *  @return		0: Fail. Numerical calculation error.
 *  			1: Success.
 *
 *	@author	    yhsuk
 *  @date       2018/07/14
 *******************************************************************************/

I32 cr_getPerspectiveTransform(
	point_t src[4],	
	point_t dst[4],	
	double perspMat[3][3])
{
	I32 res;


/* Calculates coefficients of perspective transformation
 * which maps (xi,yi) to (ui,vi), (i=1,2,3,4):
 *
 *      c00*xi + c01*yi + c02
 * ui = ---------------------
 *      c20*xi + c21*yi + c22
 *
 *      c10*xi + c11*yi + c12
 * vi = ---------------------
 *      c20*xi + c21*yi + c22
 *
 * Coefficients are calculated by solving linear system:
 * / x0 y0  1  0  0  0 -x0*u0 -y0*u0 \ /c00\ /u0\
 * | x1 y1  1  0  0  0 -x1*u1 -y1*u1 | |c01| |u1|
 * | x2 y2  1  0  0  0 -x2*u2 -y2*u2 | |c02| |u2|
 * | x3 y3  1  0  0  0 -x3*u3 -y3*u3 |.|c10|=|u3|,
 * |  0  0  0 x0 y0  1 -x0*v0 -y0*v0 | |c11| |v0|
 * |  0  0  0 x1 y1  1 -x1*v1 -y1*v1 | |c12| |v1|
 * |  0  0  0 x2 y2  1 -x2*v2 -y2*v2 | |c20| |v2|
 * \  0  0  0 x3 y3  1 -x3*v3 -y3*v3 / \c21/ \v3/
 *
 * where:
 *   cij - matrix coefficients, c22 = 1
 */
//	Ipp64f M[3,3];
 //   Mat M(3, 3, CV_64F), X(8, 1, CV_64F, M.data);
 //   Ipp64f a[8][8], b[8];
//    Mat A(8, 8, CV_64F, a), B(8, 1, CV_64F, b);
	IppStatus status;
	Ipp64f x, y;
	Ipp64f u, v;
	Ipp64f a[8*8];
	Ipp64f aInv[8*8];
	Ipp64f b[8];
	Ipp64f c[8];
	I32 i, j;

	//-- Make linear system
    for(i = 0; i < 4; ++i )
    {
		x = src[i].x;
		y = src[i].y;
		u = dst[i].x;
		v = dst[i].y;

		//-- 0 to 3
		a[i*8+0] = x;
		a[i*8+1] = y;
		a[i*8+2] = 1;
		a[i*8+3] = 0;
		a[i*8+4] = 0;
		a[i*8+5] = 0;

		a[i*8+6] = -x*u;
		a[i*8+7] = -y*u;

		b[i] = u;


		//-- 4 to 7
 		// 0  0  0 x0 y0  1 -x0*v0 -y0*v0 | |c11| |v0|
		a[(i+4)*8 + 0] = 0;
		a[(i+4)*8 + 1] = 0;
		a[(i+4)*8 + 2] = 0;
		a[(i+4)*8 + 3] = x;
		a[(i+4)*8 + 4] = y;
		a[(i+4)*8 + 5] = 1;
		a[(i+4)*8 + 6] = -x*v;
		a[(i+4)*8 + 7] = -y*v;

		b[i+4] = v;
    }

	//--- Solve Linear system..
	{
		Ipp64f pBuffer[8*8+8];
		int widthHeight = 8;
		int srcStride2 = sizeof(Ipp64f);
		int srcStride1 = 8*sizeof(Ipp64f);
		int dstStride2 = sizeof(Ipp64f);
		int dstStride1 = 8*sizeof(Ipp64f);

		status = ippmInvert_m_64f(
				a,
				srcStride1,
				srcStride2, 
				pBuffer, 
				aInv, 
				dstStride1, 
				dstStride2, widthHeight);
		if (status == ippStsNoErr) {
			// c = aInv * b;
			for(i = 0; i < 8; i++) {
				c[i] = 0;
				for (j = 0; j < 8; j++) {
					c[i] += aInv[i*8+j] * b[j];
				}
			}
			perspMat[0][0] = c[0]; // c00;		// 0	
			perspMat[0][1] = c[1]; // c01;		// 1
			perspMat[0][2] = c[2]; // c02;		// 2

			perspMat[1][0] = c[3]; // c10;		// 3
			perspMat[1][1] = c[4]; // c11;		// 4
			perspMat[1][2] = c[5]; // c12;		// 5

			perspMat[2][0] = c[6]; // c20;		// 6
			perspMat[2][1] = c[7]; // c21;		// 7
			perspMat[2][2] = 1.0;

			res = 1;
		} else {
			res = 0;
		}
	}

		
/*	
 * / x0 y0  1  0  0  0 -x0*u0 -y0*u0 \ /c00\ /u0\			0
 * | x1 y1  1  0  0  0 -x1*u1 -y1*u1 | |c01| |u1|			1
 * | x2 y2  1  0  0  0 -x2*u2 -y2*u2 | |c02| |u2|			2
 * | x3 y3  1  0  0  0 -x3*u3 -y3*u3 |.|c10|=|u3|,			3
 * |  0  0  0 x0 y0  1 -x0*v0 -y0*v0 | |c11| |v0|			4
 * |  0  0  0 x1 y1  1 -x1*v1 -y1*v1 | |c12| |v1|			5
 * |  0  0  0 x2 y2  1 -x2*v2 -y2*v2 | |c20| |v2|			6
 * \  0  0  0 x3 y3  1 -x3*v3 -y3*v3 / \c21/ \v3/			7
 *
 */
		
	return res;
}



/*!
 ********************************************************************************
 *	@brief      perspective Transform 
 *	@param[in] point_t *psrc
 *				source points
 *	@param[out] point_t *pdst
 *				destination points
 *	@param[in] perspMat[3][3]
 *				Perspective matrix
 *
 *  @return		NONE
 *
 *	@author	    yhsuk
 *  @date       2018/07/14
 *******************************************************************************/

void cr_perspectiveTransform(
		point_t *psrc,
		point_t *pdst,
		double perspMat[3][3])
{
	double  m11, m12, m13;
	double  m21, m22, m23;
	double  m31, m32, m33;
	double  x, y;
	double  u, v;

	//--
	m11 = perspMat[0][0];
	m12 = perspMat[0][1];
	m13 = perspMat[0][2];

	m21 = perspMat[1][0];
	m22 = perspMat[1][1];
	m23 = perspMat[1][2];

	m31 = perspMat[2][0];
	m32 = perspMat[2][1];
	m33 = perspMat[2][2];

	x = psrc->x;
	y = psrc->y;

	u = (m11*x + m12*y + m13)/(m31*x + m32*y + m33); 
	v = (m21*x + m22*y + m23)/(m31*x + m32*y + m33);

	pdst->x = u;
	pdst->y = v;
}

/*!
 ********************************************************************************
 *	@brief      Inverse perspective Transform 
 *	@param[in] point_t *psrc
 *				source points
 *	@param[out] point_t *pdst
 *				destination points
 *	@param[in] perspMat[3][3]
 *				Perspective matrix
 *
 *  @return		NONE
 *
 *	@author	    yhsuk
 *  @date       2018/07/14
 *******************************************************************************/

void cr_perspectiveTransformInv(
		point_t *psrc,
		point_t *pdst,
		double perspMat[3][3])
{
	double  m11, m12, m13;
	double  m21, m22, m23;
	double  m31, m32, m33;
	double  U, V;
	double  x, y;
	double t2, t7;

	//--
	m11 = perspMat[0][0];
	m12 = perspMat[0][1];
	m13 = perspMat[0][2];

	m21 = perspMat[1][0];
	m22 = perspMat[1][1];
	m23 = perspMat[1][2];

	m31 = perspMat[2][0];
	m32 = perspMat[2][1];
	m33 = perspMat[2][2];

	U = psrc->x;
	V = psrc->y;

	x = (-V * m33 * m12 + V * m32 * m13 + m23 * m12 - m22 * m13 + m22 * U * m33 - m23 * U * m32) / (m11 * m22 - m11 * V * m32 - m12 * m21 + m12 * V * m31 - U * m31 * m22 + U * m32 * m21);
	t2 = m11 * V;
	t7 = U * m31;
	y = -(m11 * m23 - t2 * m33 - m13 * m21 + m13 * V * m31 - t7 * m23 + U * m33 * m21) / (m11 * m22 - t2 * m32 - m12 * m21 + m12 * V * m31 - t7 * m22 + U * m32 * m21); 




	pdst->x = x;
	pdst->y = y;
}


/*!
 ********************************************************************************
 *	@brief      do perspective Transform with image
 *	@param[in] U08 *inbuf
 *				Original image buffer
 *	@param[out] U08 *outbuf
 *				Output image buffer. transformed.. 
 *	@param[in] U32 width
 *				Image width
 *	@param[in] U32 height
 *				Image height
 *  @return		NONE
 *
 *	@author	    yhsuk
 *  @date       2018/07/14
 *******************************************************************************/
I32 cr_doperspectiveTransform(
	U08 *inbuf,
	U08 *outbuf,
	U32 width, U32 height,
	U32 pixelbyte,
	double perspMat[3][3]
	)
{
	I32 res;
	//double coeffs[3][3];
	IppStatus status;
	IppiSize srcSize;
	IppiRect srcRoi;
	IppiRect dstRoi;

	int i;
	point_t src[4];
	point_t dst[4];
	double inquad[4][2];
	double quad[4][2];
	//---
	if (pixelbyte != 1 && pixelbyte != 3) {
		res = 0;
		goto func_exit;
	}

	// 1) Calc destination coordinate with Full image width/height
	src[0].x = 0; 		src[0].y = 0;
	src[1].x = width-1; src[1].y = 0;
	src[2].x = width-1; src[2].y = height-1;
	src[3].x = 0; 		src[3].y = height-1;

	for (i = 0; i < 4; i++) {
		cr_perspectiveTransform(
				&src[i],
				&dst[i],
				perspMat);
		inquad[i][0] = src[i].x;
		inquad[i][1] = src[i].y;
		quad[i][0] = dst[i].x;
		quad[i][1] = dst[i].y;
	}


	
	srcSize.width = width;
	srcSize.height = height;
	srcRoi.x = 0;
	srcRoi.y = 0;
	srcRoi.width = width;
	srcRoi.height = height;

	dstRoi.x = 0;
	dstRoi.y = 0;
	dstRoi.width = width;
	dstRoi.height = height;


	status = ippiWarpPerspectiveQuad_8u_C3R(
			inbuf, 
			srcSize, 
			width*pixelbyte, 
			srcRoi, 
			inquad,
			outbuf,
			width*pixelbyte, 
			dstRoi, 
			quad,
			IPPI_INTER_CUBIC);

	if (status == ippStsNoErr) {
		res = 1;
	} else {
		res = cr_doperspectiveTransform2( inbuf, outbuf, width, height, pixelbyte, perspMat);
	}
#if 0
	status = ippiWarpPerspectiveQuad_8u_C3R(
			const Ipp<datatype>* pSrc, 
			IppiSize srcSize, 
			int srcStep, 
			IppiRect srcRoi, 
			const double srcQuad[4][2],
			Ipp<datatype>* pDst, 
			int dstStep, 
			IppiRect dstRoi, 
			const double dstQuad[4][2],
			int interpolation);
#endif


#if 0

	// 2) Calc perspective transform matrix with Full image ROI and destination...
	srcSize.width = width;
	srcSize.height = height;
	srcRoi.x = 0;
	srcRoi.y = 0;
	srcRoi.width = width;
	srcRoi.height = height;

	dstRoi.x = 0;
	dstRoi.y = 0;
	dstRoi.width = width;
	dstRoi.height = height;

	status = ippiGetPerspectiveTransform(
			srcRoi, 
			quad,
			coeffs);
			
	

	// 3) Perspective transform with IPP coeffs.. :P
	status = ippiWarpPerspective_8u_C3R(
			inbuf, 
			srcSize, 
			width*3, 
			srcRoi, 
			outbuf, 
			width*3,
			dstRoi, 
			coeffs,					// perspMat,
			IPPI_INTER_CUBIC);

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" persp status: %d\n", (int) status);
	
#endif
	//status = ippiWarpPerspective_8u_C3R(
	//	const Ipp<datatype>* pSrc, 
	//	IppiSize srcSize, 
	//	int srcStep, 
	//	IppiRect srcRoi, 
	//	Ipp<datatype>* pDst, 
	//	int dstStep,
	//	IppiRect dstRoi, 
	//	const double coeffs[3][3], 
	//	int interpolation);
func_exit:
	return res;
}

I32 cr_doperspectiveTransform2(
	U08 *inbuf,
	U08 *outbuf,
	U32 width, U32 height,
	U32 pixelbyte,
	double perspMat[3][3])
{
	I32 res;
	I32 x, y;
	//I32 xDist, yDist;
	I32 x1, y1;
	I32 b;
	//point_t PtDist, PtUndist;
	point_t ptsrc, ptdst;
	U08 *psrc, *pdst;

	//---
	if (pixelbyte != 1 && pixelbyte != 3) {
		res = 0;
		goto func_exit;
	}

//	memset(pbufimgUndist, 0, width*height*pixelbyte);
	memset(outbuf, 0, width*height*pixelbyte);
    for (y = 0; y < (I32)height; y++) {
		//PtUndist.y = y;
		ptdst.y = y;
		for (x = 0; x < (I32)width; x++) {
			//PtUndist.x = x;
			ptdst.x = x;

			//cam_distortPoint(&PtUndist, &PtDist, cameraParameter, distCoeffs);
			cr_perspectiveTransformInv(
					&ptdst,
					&ptsrc,
					perspMat);
			
			//xDist = (I32) (PtDist.x + 0.1);
			//yDist = (I32) (PtDist.y + 0.1);

			x1 = (I32) (ptsrc.x + 0.1);
			y1 = (I32) (ptsrc.y + 0.1);

			//if (xDist >= 0 && xDist < (I32) width && yDist >= 0 && yDist < (I32)height) 
			if (x1 >= 0 && x1 < (I32) width && y1 >= 0 && y1 < (I32)height) 
			{
				psrc = inbuf 	+	(I32) (x1 + y1 * width) * pixelbyte;
				pdst = outbuf 	+	(x + y * width) * pixelbyte;
				for (b = 0; b < (I32) pixelbyte; b++) {
					*(pdst+b) = *(psrc+b);
				}
			}
		}
	}

	res = 1;

func_exit:
	return res;
}

#endif



#if defined (__cplusplus)
}
#endif

