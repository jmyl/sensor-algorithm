/*!
 *******************************************************************************
                                                                                
                   Creatz Coordinate 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_coordinate.c
	 @brief  Coordinate transform 
	 @author YongHo Suk                                 
	 @date   2011/08/09 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#else
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#endif

#include "cr_common.h"
#include "cr_osapi.h"
#include "cr_coordinate.h"

#include "assert.h"

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: type definition
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

// #define DEBUG_COORDINATE

/*!
 ********************************************************************************
 *	@brief      get bivariate quadratic transform matrix
 *	@param[out] bqmat
 *					bi-quad transform matrix (6x2)
 *	@param[in]  frompoints
 *					from points
 *	@param[in]  topoints
 *					to points
 *	@param[in]  count
 *					count of points
 *
 *  @return		0: Fail. Numerical calculation error.
 *  			1: Success.
 *
 *	@author	    yhsuk
 *  @date       2011/08/09
 *******************************************************************************/
I32 cr_bi_quadratic_matrix(
	biquad_t 	*bqmat, 		// bi-quad transform matrix (6x2)
	point_t *frompoints,	// from point data
	point_t *topoints,		// to point data
	U32 count)			// count of point
{
#ifdef IPP_SUPPORT
	I32 res;
	U32 i;
	double xp, xxp;
	double yp, yyp;
	double xyp;

	double xl;
	double yl;

	IppStatus ippstatus;
	Ipp64f *pmat;
	Ipp64f *lmat;
	Ipp64f PpTPp[6*6];
	Ipp64f invPpTPp[6*6];
	Ipp64f PpTPl[6*2];
	Ipp64f Ap2lMat[6*2];
	Ipp64f pBuffer[6*6 + 6];

	int src1Stride1;
	int src1Stride2;

	int src2Stride1;
	int src2Stride2;

	int dstStride1;
	int dstStride2;

	pmat = (Ipp64f *) malloc(sizeof(Ipp64f) * (count * 6));
	lmat = (Ipp64f *) malloc(sizeof(Ipp64f) * (count * 2));

	for (i = 0; i < count; i++) {
		// from points
		xp 	= frompoints[i].x;
		xxp = xp * xp;
		yp 	= frompoints[i].y;
		yyp = yp * yp;
		xyp = xp * yp;

		pmat[i*6 + 0] = (Ipp64f) xp;
		pmat[i*6 + 1] = (Ipp64f) yp;
		pmat[i*6 + 2] = (Ipp64f) xyp;
		pmat[i*6 + 3] = (Ipp64f) xxp;
		pmat[i*6 + 4] = (Ipp64f) yyp;
		pmat[i*6 + 5] = (Ipp64f) 1.;

		// to points
		xl 	= topoints[i].x;
		yl 	= topoints[i].y;

		lmat[i*2 + 0] = (Ipp64f) xl;
		lmat[i*2 + 1] = (Ipp64f) yl;
	}

	//--------------------------------
	// A = (PpT * Pp) ^-1 * PpT * Pl

	//  (PpT * Pp)
	src1Stride1 = sizeof(Ipp64f) * 6;
	src1Stride2 = sizeof(Ipp64f);

	src2Stride1 = sizeof(Ipp64f) * 6;
	src2Stride2 = sizeof(Ipp64f);

	dstStride1 = sizeof(Ipp64f) * 6;
	dstStride2 = sizeof(Ipp64f);

#ifdef DEBUG_COORDINATE
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---Pp\n");
	for (i = 0; i < 6; i++) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Pp Pp[%d]: %10f %10f %10f %10f %10f %10f \n", 
				i, 
				pmat[i*6+0],
				pmat[i*6+1],
				pmat[i*6+2],
				pmat[i*6+3],
				pmat[i*6+4],
				pmat[i*6+5]);
	}
#endif

	ippstatus = ippmMul_tm_64f(
			pmat,					// PpT,    	6xN
			src1Stride1, 
			src1Stride2, 
			6,
			count,

			pmat,					// Pp,		Nx6
			src2Stride1, 
			src2Stride2, 
			6,
			count,

			PpTPp,					// PpT * Pp, 6x6
			dstStride1, 
			dstStride2);

#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---PpT * Pp\n");
    for (i = 0; i < 6; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "PpT Pp[%d]: %20lf %20lf %20lf %20lf %20lf %20lf \n",
            i,
            PpTPp[i * 6 + 0],
            PpTPp[i * 6 + 1],
            PpTPp[i * 6 + 2],
            PpTPp[i * 6 + 3],
            PpTPp[i * 6 + 4],
            PpTPp[i * 6 + 5]);
    }
#endif

	
//#define PFACTOR		10000.
//#define PFACTOR		1.
#if defined(PFACTOR)
	for (i = 0; i < 6 * 6; i++) {
		PpTPp[i] /= PFACTOR;
	}
#endif
	
	//  (PpT * Pp) ^-1
	src1Stride1 = sizeof(Ipp64f) * 6;
	src1Stride2 = sizeof(Ipp64f);	

	dstStride1 = sizeof(Ipp64f) * 6;
	dstStride2 = sizeof(Ipp64f);
	
	ippstatus = ippmInvert_m_64f(
		PpTPp,
		src1Stride1, 
		src1Stride2,
		pBuffer, 		// ( 6 * 6 + 6)
		invPpTPp,
		dstStride1, 
		dstStride2, 
		6);
#if defined(PFACTOR)
	for (i = 0; i < 6 * 6; i++) {
		invPpTPp[i] /= PFACTOR;
	}
#endif

#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---invPpTPp\n");
    for (i = 0; i < 6; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "invPpTPp[%d]: %20lf %20lf %20lf %20lf %20lf %20lf \n",
            i,
            invPpTPp[i * 6 + 0],
            invPpTPp[i * 6 + 1],
            invPpTPp[i * 6 + 2],
            invPpTPp[i * 6 + 3],
            invPpTPp[i * 6 + 4],
            invPpTPp[i * 6 + 5]);
    }
#endif

	//  (PpT * Pl)
	src1Stride1 = sizeof(Ipp64f) * 6;
	src1Stride2 = sizeof(Ipp64f);

	src2Stride1 = sizeof(Ipp64f) * 2;
	src2Stride2 = sizeof(Ipp64f);

	dstStride1 = sizeof(Ipp64f) * 2;
	dstStride2 = sizeof(Ipp64f);

	ippstatus = ippmMul_tm_64f(
			pmat,					// PpT,		6xN
			src1Stride1, 
			src1Stride2, 
			6,
			count,

			lmat,					// Pl,		Nx2
			src2Stride1, 
			src2Stride2, 
			2,
			count,

			PpTPl,					// PpT * Pl,	6x2
			dstStride1, 
			dstStride2);

	// (PpT * Pp) ^-1 * (PpT * Pl)
	src1Stride1 = sizeof(Ipp64f) * 6;
	src1Stride2 = sizeof(Ipp64f);

	src2Stride1 = sizeof(Ipp64f) * 2;
	src2Stride2 = sizeof(Ipp64f);

	dstStride1 = sizeof(Ipp64f) * 2;
	dstStride2 = sizeof(Ipp64f);
	
	ippstatus = ippmMul_mm_64f(
			invPpTPp,
			src1Stride1, 
			src1Stride2, 
			6,
			6,
			PpTPl,
			src2Stride1, 
			src2Stride2, 
			2,
			6,
			Ap2lMat,
			dstStride1, 
			dstStride2);

	//
	//t_p2l 	*Ap2l, 			// Pixel to Local transform Matrix A (6x2)
	for (i = 0; i < 6*2; i++) {
		bqmat->mat[i] = (double) Ap2lMat[i];
	}

	free(pmat);
	free(lmat);

	res = 1;

#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---------mat: \n");

    for (i = 0; i < 6 * 2; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "m[%d]: %lf\n", i, bqmat->mat[i]);
    }
#endif

	return res;
#else

    double xp, yp, xyp, xxp, yyp, xl, yl;
    U32 i;
    I32 res;

    cv::Mat cvPointsFrom, cvPointsto;
    cv::Mat pmat(cv::Size(6, count), CV_64FC1), lmat(cv::Size(6, count), CV_64FC1),
        pmatT, pTp, ptpinv, ptpinv_pT_lmat;

    pmat = 0.0;
    lmat = 0.0;

    for (i = 0; i < count; i++) {
        // from points
        xp 	= frompoints[i].x;
        xxp = xp * xp;
        yp 	= frompoints[i].y;
        yyp = yp * yp;
        xyp = xp * yp;

        pmat.at<double>(i, 0) = xp;
        pmat.at<double>(i, 1) = yp;
        pmat.at<double>(i, 2) = xyp;
        pmat.at<double>(i, 3) = xxp;
        pmat.at<double>(i, 4) = yyp;
        pmat.at<double>(i, 5) = 1.;

        // to points
        xl 	= topoints[i].x;
        yl 	= topoints[i].y;

        lmat.at<double>(i, 0) = xl;
        lmat.at<double>(i, 1) = yl;
    }

    //--------------------------------
    // A = (PpT * Pp) ^-1 * PpT * Pl

    //  (PpT * Pp)
#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---Pp\n");
    for (i = 0; i < 6; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Pp Pp[%d]: %10f %10f %10f %10f %10f %10f \n",
            i,
            pmat.at<double>(i, 0),
            pmat.at<double>(i, 1),
            pmat.at<double>(i, 2),
            pmat.at<double>(i, 3),
            pmat.at<double>(i, 4),
            pmat.at<double>(i, 5);
    }
#endif
    
    cv::transpose(pmat, pmatT);
    pTp = pmatT * pmat;

#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---PpT * Pp\n");
    for (i = 0; i < 6; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "PpT Pp[%d]: %20lf %20lf %20lf %20lf %20lf %20lf \n",
            i,
            pTp.at<double>(i, 0),
            pTp.at<double>(i, 1),
            pTp.at<double>(i, 2),
            pTp.at<double>(i, 3),
            pTp.at<double>(i, 4),
            pTp.at<double>(i, 5));
    }
#endif

    //  (PpT * Pp) ^-1

    cv::invert(pTp, ptpinv);

#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---invPpTPp\n");
    for (i = 0; i < 6; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "invPpTPp[%d]: %20lf %20lf %20lf %20lf %20lf %20lf \n",
            i,
            ptpinv.at<double>(i, 0),
            ptpinv.at<double>(i, 1),
            ptpinv.at<double>(i, 2),
            ptpinv.at<double>(i, 3),
            ptpinv.at<double>(i, 4),
            ptpinv.at<double>(i, 5));
    }
#endif

    ptpinv_pT_lmat = ptpinv * pmatT * lmat;

    memcpy(&(bqmat->mat[0]), ptpinv_pT_lmat.data, sizeof(double) * 6 * 2);

    res = 1;

#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---------mat: \n");

    for (i = 0; i < 6 * 2; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "m[%d]: %lf\n", i, bqmat->mat[i]);
    }
#endif

    return res;
#endif
}


/*!
 ********************************************************************************
 *	@brief      bi-quadratic transfer
 *	@param[in] bqmat 
 *					bi-quad transform matrix (6x2)
 *	@param[in]  frompoints
 *					from points
 *	@param[out]  topoints 
 *					to points
 *
 *  @return		0: Fail. Numerical calculation error.
 *  			1: Success.
 *
 *	@author	    yhsuk
 *  @date       2011/08/10
 *******************************************************************************/
I32 cr_bi_quadratic(
	biquad_t 	*bqmat, 		// bi-quad transform matrix (6x2)
	point_t *frompoints,	// from point data
	point_t *topoints)		// to point data
{
	double xp, xxp;
	double yp, yyp;
	double xyp;

	double xl;
	double yl;


	xp 	= frompoints->x;
	xxp = xp * xp;
	yp 	= frompoints->y;
	yyp = yp * yp;
	xyp = xp * yp;


	xl =  xp  * bqmat->mat[0]				// p(1,1) * A(1,1)
		+ yp  * bqmat->mat[2]				// p(1,2) * A(2,1)
		+ xyp * bqmat->mat[4]				// p(1,2) * A(3,1)
		+ xxp * bqmat->mat[6]				// p(1,2) * A(4,1)
		+ yyp * bqmat->mat[8]				// p(1,2) * A(5,1)
		+       bqmat->mat[10];				// p(1,2) * A(6,1)
		
	yl =  xp  * bqmat->mat[1]				// p(1,1) * A(1,2)
		+ yp  * bqmat->mat[3]				// p(1,2) * A(2,2)
		+ xyp * bqmat->mat[5]				// p(1,2) * A(3,2)
		+ xxp * bqmat->mat[7]				// p(1,2) * A(4,2)
		+ yyp * bqmat->mat[9]				// p(1,2) * A(5,2)
		+       bqmat->mat[11];				// p(1,2) * A(6,2)

	topoints->x = xl;
	topoints->y = yl;

	return 1;
}

#if defined (__cplusplus)
}
#endif

#if 0 // not used
/*!
 ********************************************************************************
 *	@brief      get bivariate (homogeneous) linear transform matrix
 *	@param[out] blmat
 *					bi-linear transform matrix (3x2)
 *	@param[in]  frompoints
 *					from points
 *	@param[in]  topoints
 *					to points
 *	@param[in]  count
 *					count of points
 *
 *  @return		0: Fail. Numerical calculation error.
 *  			1: Success.
 *
 *	@author	    yhsuk
 *  @date       2011/08/10
 *******************************************************************************/
I32 cr_bi_linear_matrix(
    bilinear_t 	*blmat, 		// bi-linear transform matrix (3x2)
    point_t *frompoints,	// from point data
    point_t *topoints,		// to point data
    U32 count)			// count of point
{
#ifdef IPP_SUPPORT
    I32 res;
    U32 i;
    double xp;
    double yp;

    double xl;
    double yl;

    IppStatus ippstatus;
    //Ipp64f pmat[COORD_MAXPOINT * 3];
    //Ipp64f lmat[COORD_MAXPOINT * 2];
    Ipp64f *pmat;
    Ipp64f *lmat;
    Ipp64f PpTPp[3*3];
    Ipp64f invPpTPp[3*3];
    Ipp64f PpTPl[3*2];
    Ipp64f Ap2lMat[3*2];
    Ipp64f pBuffer[3*3 + 3];

    int src1Stride1;
    int src1Stride2;

    int src2Stride1;
    int src2Stride2;

    int dstStride1;
    int dstStride2;



    assert(count <= COORD_MAXPOINT);



    pmat = (Ipp64f *)malloc(sizeof(Ipp64f) * (COORD_MAXPOINT * 3));
    lmat = (Ipp64f *)malloc(sizeof(Ipp64f) * (COORD_MAXPOINT * 2));

    for (i = 0; i < count; i++) {
        // from points
        xp 	= frompoints[i].x;
        yp 	= frompoints[i].y;

        pmat[i*3 + 0] = (Ipp64f)xp;
        pmat[i*3 + 1] = (Ipp64f)yp;
        pmat[i*3 + 2] = (Ipp64f) 1.;

        // to points
        xl 	= topoints[i].x;
        yl 	= topoints[i].y;

        lmat[i*2 + 0] = (Ipp64f)xl;
        lmat[i*2 + 1] = (Ipp64f)yl;
    }

    //--------------------------------
    // A = (PpT * Pp) ^-1 * PpT * Pl

    //  (PpT * Pp)
    src1Stride1 = sizeof(Ipp64f) * 3;
    src1Stride2 = sizeof(Ipp64f);

    src2Stride1 = sizeof(Ipp64f) * 3;
    src2Stride2 = sizeof(Ipp64f);

    dstStride1 = sizeof(Ipp64f) * 3;
    dstStride2 = sizeof(Ipp64f);

    ippstatus = ippmMul_tm_64f(
        pmat,					// PpT,    	3xN
        src1Stride1,
        src1Stride2,
        3,
        count,

        pmat,					// Pp,		Nx3
        src2Stride1,
        src2Stride2,
        3,
        count,

        PpTPp,					// PpT * Pp, 3x3
        dstStride1,
        dstStride2);

    //  (PpT * Pp) ^-1
    src1Stride1 = sizeof(Ipp64f) * 3;
    src1Stride2 = sizeof(Ipp64f);

    dstStride1 = sizeof(Ipp64f) * 3;
    dstStride2 = sizeof(Ipp64f);

    ippstatus = ippmInvert_m_64f(
        PpTPp,
        src1Stride1,
        src1Stride2,
        pBuffer, 		// ( 3 * 3 + 3)
        invPpTPp,
        dstStride1,
        dstStride2,
        3);

    //  (PpT * Pl)
    src1Stride1 = sizeof(Ipp64f) * 3;
    src1Stride2 = sizeof(Ipp64f);

    src2Stride1 = sizeof(Ipp64f) * 2;
    src2Stride2 = sizeof(Ipp64f);

    dstStride1 = sizeof(Ipp64f) * 2;
    dstStride2 = sizeof(Ipp64f);

    ippstatus = ippmMul_tm_64f(
        pmat,					// PpT,		3xN
        src1Stride1,
        src1Stride2,
        3,
        count,

        lmat,					// Pl,		Nx2
        src2Stride1,
        src2Stride2,
        2,
        count,

        PpTPl,					// PpT * Pl,	3x2
        dstStride1,
        dstStride2);

    // (PpT * Pp) ^-1 * (PpT * Pl)
    src1Stride1 = sizeof(Ipp64f) * 3;
    src1Stride2 = sizeof(Ipp64f);

    src2Stride1 = sizeof(Ipp64f) * 2;
    src2Stride2 = sizeof(Ipp64f);

    dstStride1 = sizeof(Ipp64f) * 2;
    dstStride2 = sizeof(Ipp64f);


    ippstatus = ippmMul_mm_64f(
        invPpTPp,
        src1Stride1,
        src1Stride2,
        3,
        3,
        PpTPl,
        src2Stride1,
        src2Stride2,
        2,
        3,
        Ap2lMat,
        dstStride1,
        dstStride2);

    //
    //t_p2l 	*Ap2l, 			// Pixel to Local transform Matrix A (3x2)
    for (i = 0; i < 3*2; i++) {
        blmat->mat[i] = (double)Ap2lMat[i];
    }

    res = 1;
    free(pmat);
    free(lmat);

    return res;
#else
    I32 res;
    U32 i;
    double xp;
    double yp;

    double xl;
    double yl;

    double *pmat, *lmat;
    cv::Mat cvPMat, cvLMat, PT, PTP, PTPinv, PTL, cvBiMat;

    assert(count <= COORD_MAXPOINT);

    pmat = (double*)malloc(sizeof(double) * (count * 3));
    lmat = (double*)malloc(sizeof(double) * (count * 2));

    for (i = 0; i < count; i++) {
        // from points
        xp 	= frompoints[i].x;
        yp 	= frompoints[i].y;

        pmat[i*3 + 0] = xp;
        pmat[i*3 + 1] = yp;
        pmat[i*3 + 2] = 1.;

        // to points
        xl 	= topoints[i].x;
        yl 	= topoints[i].y;

        lmat[i*2 + 0] = xl;
        lmat[i*2 + 1] = yl;
    }
    cvPMat = cv::Mat(cv::Size(3, count), CV_64FC1, pmat);
    cvLMat = cv::Mat(cv::Size(2, count), CV_64FC1, lmat);

    cv::transpose(cvPMat, PT);
    PTP = PT * cvPMat;
    cv::invert(PTP, PTPinv);
    PTL = PT * cvLMat;
    cvBiMat = PTPinv * PTL;
    memcpy(&(blmat->mat[0]), cvBiMat.data, sizeof(double) * 3 * 2);

    res = 1;

    return res;
#endif
}



/*!
 ********************************************************************************
 *	@brief      bi-linear transfer
 *	@param[in] blmat
 *					bi-linear transform matrix (3x2)
 *	@param[in]  frompoints
 *					from points
 *	@param[out]  topoints
 *					to points
 *
 *  @return		0: Fail. Numerical calculation error.
 *  			1: Success.
 *
 *	@author	    yhsuk
 *  @date       2011/08/10
 *******************************************************************************/
I32 cr_bi_linear(
    bilinear_t 	*blmat, 		// bi-linear transform matrix (3x2)
    point_t *frompoints,	// from point data
    point_t *topoints)		// to point data
{
    double xp;
    double yp;

    double xl;
    double yl;


    xp 	= frompoints->x;
    yp 	= frompoints->y;


    xl =  xp  * blmat->mat[0]				// p(1,1) * A(1,1)
        + yp  * blmat->mat[2]				// p(1,2) * A(2,1)
        +       blmat->mat[4];				// p(1,3) * A(3,1)

    yl =  xp  * blmat->mat[1]				// p(1,1) * A(1,2)
        + yp  * blmat->mat[3]				// p(1,2) * A(2,2)
        +       blmat->mat[5];				// p(1,3) * A(3,2)

    topoints->x = xl;
    topoints->y = yl;

    return 1;
}




/* Source matrix with width=4 and height=3
 *
    Ipp32f pSrc[3*4] = { 	1, 2, 3, 4,
                            5, 6, 7, 8,
                            9, 0, 1, 2 };
 *
 */
 /*!
  ********************************************************************************
  *	@brief      get homography transform matrix
  *	@param[out] mat
  *					transform matrix  ((dimt+1) x (dimf+1))
  *	@param[in]  frompoints
  *					from points
  *	@param[in]  topoints
  *					to points
  *	@param[in]  dimf
  *					vector dimension, from.  (dimf * count, double)
  *	@param[in]  dimt
  *					vector dimension, to.    (dimt * count, double)
  *	@param[in]  count
  *					count of point
  *
  *  @return		0: Fail. Numerical calculation error.
  *  			1: Success.
  *
  *	@author	    yhsuk
  *  @date       2016/10/07
  *******************************************************************************/
I32 cr_homography_matrix(
    double 	*transmat, 	// transform matrix
    double  *fromv,
    double  *tov,
    U32		dimf,
    U32		dimt,
    U32 count)			// count of point
{
#ifdef IPP_SUPPORT
    I32 res;
    U32 i, j;

    IppStatus ippstatus;
    Ipp64f *agmMatrixFrom;
    Ipp64f *agmMatrixTo;

    Ipp64f *AAT;			// {(dimf+1)x(dimf+1)}

    Ipp64f *invAAT;			// {(dimf+1)x(dimf+1)}

    Ipp64f *BAT;			// {(dimt+1)x(dimf+1)}
    Ipp64f *pBuffer;		// (dimf+1) * (dimf+1) + (dimf+1))	= (dimf+1)*(dimf+1 + 1)

    Ipp64f *mat0, *mat1;

    int src1Stride1;
    int src1Stride2;

    int src2Stride1;
    int src2Stride2;

    int dstStride1;
    int dstStride2;

    //-----
    //__debugbreak();
    agmMatrixFrom 	= (Ipp64f *)malloc(sizeof(Ipp64f) * (count * (dimf+1)));
    agmMatrixTo   	= (Ipp64f *)malloc(sizeof(Ipp64f) * (count * (dimt+1)));

    AAT 			= (Ipp64f *)malloc(sizeof(Ipp64f) * (dimf+1)*(dimf+1));
    invAAT 			= (Ipp64f *)malloc(sizeof(Ipp64f) * (dimf+1)*(dimf+1));


    BAT 			= (Ipp64f *)malloc(sizeof(Ipp64f) * (dimt+1)*(dimf+1));
    pBuffer			= (Ipp64f *)malloc(sizeof(Ipp64f) * (dimf+1)*(dimf+1 + 1));

    //-- Make augmented matrix, from
    mat0 = (Ipp64f *)fromv;
    mat1 = agmMatrixFrom;
    for (i = 0; i < count; i++) {
        for (j = 0; j < dimf; j++) {
            *(mat1 + j*count) = *mat0++;
        }
        *(mat1 + dimf*count) = 1.0;
        *mat1++;
    }


    //-- Make augmented matrix, to
    mat0 = (Ipp64f *)tov;
    mat1 = agmMatrixTo;
    for (i = 0; i < count; i++) {
        for (j = 0; j < dimt; j++) {
            *(mat1 + j*count) = *mat0++;
        }
        *(mat1 + dimt*count) = 1.0;
        *mat1++;
    }

    //--------------------------------
    // B{(dimt+1)xN} = H{(dimt+1)x(dimf+1)} A{(dimf+1)xN}
    // H~ = BxA# = Bx(AT*(A AT)^-1)

    //  (A * AT)
    src1Stride1 = sizeof(Ipp64f) * count;
    src1Stride2 = sizeof(Ipp64f);

    src2Stride1 = sizeof(Ipp64f) * count;
    src2Stride2 = sizeof(Ipp64f);

    dstStride1 = sizeof(Ipp64f) * (dimf+1);
    dstStride2 = sizeof(Ipp64f);

    ippstatus = ippmMul_mt_64f(
        agmMatrixFrom,					// A,    	(dimf+1)xN
        src1Stride1,
        src1Stride2,
        count,
        (dimf+1),

        agmMatrixFrom,					// AT,		Nx(dimf+1)
        src2Stride1,
        src2Stride2,
        count,
        (dimf+1),

        AAT,					// AxAT, 	(dimf+1)x(dimf+1)
        dstStride1,
        dstStride2);


    //#define PFACTOR		10000.
    //#define PFACTOR		1.
#if defined(PFACTOR)
    for (i = 0; i < 6 * 6; i++) {
        AAT[i] /= PFACTOR;
    }
#endif

    //  (AAT) ^-1   (dimf+1)x(dimf+1)
    src1Stride1 = sizeof(Ipp64f) * (dimf+1);
    src1Stride2 = sizeof(Ipp64f);

    dstStride1 = sizeof(Ipp64f) * (dimf+1);
    dstStride2 = sizeof(Ipp64f);

    ippstatus = ippmInvert_m_64f(
        AAT,
        src1Stride1,
        src1Stride2,
        pBuffer, 		// (dimf+1) * (dimf+1) + (dimf+1))	= (dimf+1)*(dimf+1 + 1)
        invAAT,
        dstStride1,
        dstStride2,
        (dimf+1));
#if defined(PFACTOR)
    for (i = 0; i < 6 * 6; i++) {
        invAAT[i] /= PFACTOR;
    }
#endif

#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---invAAT\n");
    for (i = 0; i < 6; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "invPpTPp[%d]: %20lf %20lf %20lf %20lf %20lf %20lf \n",
            i,
            invPpTPp[i * 6 + 0],
            invPpTPp[i * 6 + 1],
            invPpTPp[i * 6 + 2],
            invPpTPp[i * 6 + 3],
            invPpTPp[i * 6 + 4],
            invPpTPp[i * 6 + 5]);
    }
#endif

    //  (BxAT)
    src1Stride1 = sizeof(Ipp64f) * count;
    src1Stride2 = sizeof(Ipp64f);

    src2Stride1 = sizeof(Ipp64f) * count;
    src2Stride2 = sizeof(Ipp64f);

    dstStride1 = sizeof(Ipp64f) * (dimf+1);
    dstStride2 = sizeof(Ipp64f);

    ippstatus = ippmMul_mt_64f(
        agmMatrixTo,					// B,		(dimt+1)xN
        src1Stride1,
        src1Stride2,
        count,
        (dimt+1),

        agmMatrixFrom,					// A,		(dimf+1)xN
        src2Stride1,
        src2Stride2,
        count,
        (dimf+1),

        BAT,							// BxAT,	(dimt+1)x(dimf+1)
        dstStride1,
        dstStride2);

    // (BxAT)x((AxAT)^-1) = BAT x invAAT   (dimt+1)x(dimf+1)
    src1Stride1 = sizeof(Ipp64f) * (dimf+1);
    src1Stride2 = sizeof(Ipp64f);

    src2Stride1 = sizeof(Ipp64f) * (dimf+1);
    src2Stride2 = sizeof(Ipp64f);

    dstStride1 	= sizeof(Ipp64f) * (dimf+1);
    dstStride2 	= sizeof(Ipp64f);


    ippstatus = ippmMul_mm_64f(
        BAT,
        src1Stride1,
        src1Stride2,
        (dimf+1),
        (dimt+1),

        invAAT,
        src2Stride1,
        src2Stride2,
        (dimf+1),
        (dimf+1),

        transmat,
        dstStride1,
        dstStride2);



    free(agmMatrixFrom);
    free(agmMatrixTo);
    free(AAT);
    free(invAAT);
    free(pBuffer);
    res = 1;

#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---------mat: \n");

    for (i = 0; i < 6*2; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "m[%d]: %lf\n", i, bqmat->mat[i]);
    }
#endif
    return res;
#else
    I32 res;

    //-- Make augmented matrix, from
    cv::Mat augmMatFromTemp, augmMatFrom, augmMatToTemp, augmMatTo, lastrow(cv::Size(count, 1), CV_64FC1),
        AT, AAT, invAAT, BAT, transMat;

    lastrow = 1.0;
    cv::transpose(cv::Mat(cv::Size(dimf, count), CV_64FC1, fromv), augmMatFromTemp);
    cv::transpose(cv::Mat(cv::Size(dimf, count), CV_64FC1, tov), augmMatToTemp);

    cv::vconcat(std::vector<cv::Mat>({ augmMatFromTemp, lastrow }), augmMatFrom);
    cv::vconcat(std::vector<cv::Mat>({ augmMatToTemp, lastrow }), augmMatTo);

    cv::transpose(augmMatFrom, AT);
    AAT = augmMatFrom * AT;

    cv::invert(AAT, invAAT);

    BAT = augmMatTo * AT;

    transMat = BAT * invAAT;

    memcpy(transmat, transMat.data, sizeof(double) * (dimt+1) * (dimf+1));

    res = 1;

#ifdef DEBUG_COORDINATE
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---------mat: \n");

    for (i = 0; i < 6*2; i++) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "m[%d]: %lf\n", i, bqmat->mat[i]);
    }
#endif
    return res;
#endif
}


/*!
 ********************************************************************************
 *	@brief      homography transform
 *	@param[in] mat
 *					transform matrix  (dimt x dimf)
 *	@param[in]  frompoints
 *					from points
 *	@param[to]  topoints
 *					to points,  (dimt+1)
 *	@param[in]  dimf
 *					vector dimension, from
 *	@param[in]  dimt
 *					vector dimension, to
 *
 *  @return		0: Fail. Numerical calculation error.
 *  			1: Success.
 *
 *	@author	    yhsuk
 *  @date       2016/10/07
 *******************************************************************************/
I32 cr_homography(
    double 	*transmat, 	// transform matrix
    double  *fromv,
    double  *tov,
    U32		dimf,
    U32		dimt)
{
#ifdef IPP_SUPPORT
    I32 res;
    U32 i;

    IppStatus ippstatus;
    Ipp64f *agmVectorFrom;
    Ipp64f *agmVectorTo;

    Ipp64f *mat1;

    int src1Stride1;
    int src1Stride2;

    int src2Stride2;

    int dstStride2;


    //-----
    agmVectorFrom 	= (Ipp64f *)malloc(sizeof(Ipp64f) * (dimf+1));
    agmVectorTo   	= (Ipp64f *)malloc(sizeof(Ipp64f) * (dimt+1));

    mat1 = agmVectorFrom;
    for (i = 0; i < dimf; i++) {
        *mat1++ = *fromv++;
    }
    *mat1 = 1.0;

    src1Stride1 = sizeof(Ipp64f) * (dimf+1);
    src1Stride2 = sizeof(Ipp64f);

    src2Stride2 = sizeof(Ipp64f);

    dstStride2 = sizeof(Ipp64f);

    ippstatus = ippmMul_mv_64f(
        transmat,						// H ((dimt+1)x(dimf+1))
        src1Stride1,
        src1Stride2,
        (dimf+1),						// Width,  colume
        (dimt+1),						// Height, Row

        agmVectorFrom,					// vectA, (dimf+1)
        src2Stride2,
        (dimf+1),

        agmVectorTo,					// vectB, (dimt+1)
        dstStride2);

    for (i = 0; i < dimt+1; i++) {
        *(tov+i) = *(agmVectorTo+i);
    }

    free(agmVectorFrom);
    free(agmVectorTo);

    res = 1;
    return res;
#else
    I32 res;
    double *fromv_last1, *tov_last1;
    cv::Mat vectorsFrom,
        vectorsTo,
        transMat(cv::Size(dimf+1, dimt+1), CV_64FC1, transmat);

    //-----
    fromv_last1 	= (double *)malloc(sizeof(double) * (dimf+1));
    tov_last1   	= (double *)malloc(sizeof(double) * (dimt+1));

    memcpy(fromv_last1, fromv, sizeof(double) * dimf);
    fromv_last1[dimf] = 1.0;

    vectorsFrom = cv::Mat(cv::Size(1, dimf+1), CV_64FC1, fromv_last1);

    vectorsTo = transMat * vectorsFrom;

    memcpy(tov, vectorsTo.data, sizeof(double) * dimt);

    res = 1;
    return res;
#endif
}
#endif