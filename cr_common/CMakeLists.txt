add_library(cr_common STATIC
    src/cr_log.c
    src/cr_dbgmsg.c
    src/cr_regression.cpp
    src/cr_matrix.cpp
    src/cr_math.c
    src/cr_misc.c
    src/cr_nanoxml.c
    src/cr_interpolation.c
    src/cr_geometric.cpp
    src/cr_cam_jpeg.cpp
    src/cr_coordinate.cpp
    src/cr_network.cpp
    src/cr_bugtrap_linux.cpp
    src/sha256.cpp
    )

target_include_directories(cr_common
    PUBLIC
        include/
    )

find_package( OpenCV REQUIRED )

target_link_libraries(cr_common
    PRIVATE
        cr_api
        # iana
        spincalc
        # gigev
        # pcie
        scamif
        cr_osal
        tinyxml2
        tool_cli
        extlibs::jpeg
        ${OpenCV_LIBS}
    # PRIVATE
)
