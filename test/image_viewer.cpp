#include "image_viewer.h"
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
using namespace cv;

static int pic_idx = 0;
static int DisplayImg(unsigned char* img, int width, int height, int sx, int sy, int colorspace);

void ImageViewer::init() {
  pic_idx = 0;
}

void ImageViewer::fin() {
  destroyAllWindows();
}

int ImageViewer::DisplayCallback(char *img, int width, int height, int p0, int p1, int p2, int p3, PARAM_T userparam) 		// p1, 1: GRAY, 2: YUV422, 3: YUV420, 4: RGB
{
  int sx, sy;
  int colorspace;

  //LOCAL_DEBUG(("[%s]%s\n",TESTAPP_NAME,  __func__));	

  sx = 0; sy = 0;
  if (p0 == 0) {
    sx= 0;
    sy= 0;
  } else if (p0 == 1) {
    sx = 512;
    sy = 0;
  } else if (p0 == 2) {
    sx = 0;
    sy = 256;
  } else if (p0 == 3) {
    sx = 512;
    sy = 256;
  } else if (p0 == 4) {
    sx = 0;
    sy = 512;
  } else if (p0 == 5) {
    sx = 512;
    sy = 512;
  }

  if (p1 >= 1 && p1 <= 4) { 	// p1, 1: GRAY, 2: YUV422, 3: YUV420, 4: RGB
    colorspace = p1;
  } else {
    colorspace = 1;
  }

  DisplayImg((unsigned char*) img,
      width, height,
      sx, sy,
      colorspace				// 1: GRAY, 2: YUV422, 3: YUV420, 4: RGB
  );

  return 1;
}

// Display callback example - openCV
static int DisplayImg(unsigned char* img, int width, int height, int sx, int sy, int colorspace)
{
  char window_name[64];
  Mat image(height,width, CV_8UC1 , img);
  
    if(image.empty())
    {
        std::cout << "Could not read the image: "  << std::endl;
        return 1;
    }

  memset(window_name, 0 , 64);
#if 0		// TODO: if you want, implement multiple display/window, but you should consider system load and memory
  sprintf(window_name, "disp window %d", pic_idx++);
#else
  sprintf(window_name, "disp window");
#endif
  
    imshow(window_name, image);

  // TODO: if you want, implement display position control
  // TODO: if you want, implement window position control
  namedWindow(window_name, WINDOW_NORMAL);
  resizeWindow(window_name, 512, 512);

  // TODO: if you want, implement display period control
  waitKey(1);	// TODO: by default for initial test

  pic_idx++; 
  
  return 0;
}
