#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <algorithm>
#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "cr_common.h"
#include "cr_osapi.h"
#include "cr_thread.h"
#include "iana.h"
#include "iana_adapt.h"
#include "iana_implement.h"
#include "iana_tool.h"
#include "image_viewer.h"

#define LOCAL_DEBUG_ON 1
#define LOCAL_ERROR_TRACE_ON 1
#include "cr_dbg_on.h"
#include "tinyxml2.h"
namespace xml = tinyxml2;

/*----------------------------------------------------------------------------
Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define TESTAPP_NAME "IANA_FILE_TESTAPP"
#define RESULT_FILE_PATH

namespace fs = std::filesystem;

const char* info_file = "info.xml";
const char* ref_file = "reference.xml";
const char* result_file = "result.xml";

class IanaSim
    : public testing::TestWithParam<std::tuple<fs::path, std::string>> {
 public:
  enum class TestStatus { idle, run, end, error };
  static void* hIana;
  static iana_t iana;
  static TestStatus test_status;
  static U32 cb_sensor_status;
  static IANA_shotdata_t cb_shotdata;
  static I32 cb_userparam;
  static void SetUpTestSuite() {
    hIana = IANA_create(IANA_OPMODE_FILE, 0, 0, 0, 0);
    test_status = TestStatus::idle;
  }

  static void TeatDownTestSuite() {
    if (hIana != NULL) {
      IANA_cmd(hIana, IANA_CMD_OPERATION_STOP, 0, 0, 0, 0);
      IANA_delete(hIana);
      hIana = NULL;
    }
    test_status = TestStatus::idle;
  }

  static int ResultCallback(HAND piana, U32 sensor_status, IANA_shotdata_t* psd,
                            I32 userparam);
  static int run(fs::path source, int image_display, int club);
  static int single_run(const char* info_file, const char* result_file,
                        int image_display, int club);

  // Create a free inline friend function.
  friend std::ostream& operator<<(std::ostream& os, const IanaSim& shot) {
    return os << "test string !!!!!!!!!!!!!!!!!!!!!!!!!!"
              << std::endl;  // bar.DebugString();  // whatever needed to
                             // print bar to os
  }
};

void* IanaSim::hIana;
iana_t IanaSim::iana;
IanaSim::TestStatus IanaSim::test_status;
U32 IanaSim::cb_sensor_status;
IANA_shotdata_t IanaSim::cb_shotdata;
I32 IanaSim::cb_userparam;

class GeneratedXML {
 protected:
  xml::XMLDocument* pdoc;
  xml::XMLElement* root;

 public:
  GeneratedXML(std::string resultfile) {
    xml::XMLError xres;
    pdoc = new xml::XMLDocument();
    xres = pdoc->LoadFile(resultfile.c_str());
  }
  double get(std::string f) {
    return std::stod(root->FirstChildElement(f.c_str())->GetText());
  }
  ~GeneratedXML() { delete pdoc; }
};

class ResultXML : public GeneratedXML {
 public:
  ResultXML(std::string resultfile) : GeneratedXML(resultfile) {
    root = pdoc->FirstChildElement("simulation_result")
               ->FirstChildElement("balldata");
  }
  double get(std::string f) {
    if (f.rfind("club", 0) == 0) {
      xml::XMLElement* tmp;
      tmp = pdoc->FirstChildElement("simulation_result")
                ->FirstChildElement("clubdata");
      return std::stod(tmp->FirstChildElement(f.c_str())->GetText());
    } else
      return GeneratedXML::get(f);
  }
};

class ReferenceXML : public GeneratedXML {
 public:
  ReferenceXML(std::string resultfile, bool use_spincalc)
      : GeneratedXML(resultfile) {
    root = pdoc->FirstChildElement("SHOTINFO");
    // TODO: error handling
    if (use_spincalc)
      root = root->FirstChildElement("shotresult2");
    else
      root = root->FirstChildElement("shotresult");
  }

  double get(std::string f) {
    if (f.compare("clubspeed") == 0) return GeneratedXML::get("clubspeed_B");
    return GeneratedXML::get(f);
  }
};

int IanaSim::ResultCallback(HAND piana, U32 sensor_status, IANA_shotdata_t* psd,
                            I32 userparam) {
  printf("[Shot Reslt] \n");
  printf("\tstatus:\t\t0x%0x\n", (U32)sensor_status);
  printf("\tuserparam:\t0x%0x\n", (U32)userparam);

  IanaSim::iana = *((iana_t*)piana);
  IanaSim::cb_sensor_status = sensor_status;
  IanaSim::cb_shotdata = *psd;
  IanaSim::cb_userparam = userparam;

  if (sensor_status == IANA_SENSOR_STATE_GOODSHOT) {
    printf("Good shot..\n");

    printf(
        " vmag %lf, inc %lf, azimuth %lf, %lf, %lf, %lf, %lf, backspin %d, "
        "sidespin %d, %lf, %lf, %lf, %lf\n",
        psd->ballspeedx1000 / 1000.0, psd->inclinex1000 / 1000.0,
        psd->azimuthx1000 / 1000.0,

        psd->clubpathx1000 / 1000.0, psd->clubfaceanglex1000 / 1000.0,
        psd->clubspeed_Bx1000 / 1000.0, psd->clubspeed_Ax1000 / 1000.0,
        psd->backspin, psd->sidespin,

        psd->spinmag,

        psd->axisx, psd->axisy, psd->axisz);

  } else {
    printf("Bad shot..\n");
  }

  test_status = TestStatus::end;
  return 0;
}

// OK: return 0
int IanaSim::single_run(const char* info_file, const char* result_file,
                        int image_display, int club) {
  LOCAL_DEBUG(("[%s]%s: BEGIN\n", TESTAPP_NAME, __func__));

  if (hIana == NULL) return 1;

  iana_readinfo_filename(hIana, info_file);     // input  (ex: info.xml)
  iana_simresult_filename(hIana, result_file);  // result (ex: result.xml)

  IANA_cmd(hIana, IANA_CMD_OPERATION_START, (I64)ResultCallback, 0, 0, 0);
  if (image_display) {
    IANA_cmd(hIana, IANA_CMD_EXC_IMAGE_CALLBACK,
             (I64)ImageViewer::DisplayCallback, 0, 0, 0);
  } else {
    IANA_cmd(hIana, IANA_CMD_EXC_IMAGE_CALLBACK, 0, 0, 0, 0);  // NO DISPLAY
  }
  test_status = TestStatus::run;
  LOCAL_DEBUG(("[%s]%s: END\n", TESTAPP_NAME, __func__));
  return 0;
}

int IanaSim::run(fs::path source, int image_display, int club) {
  int result;
  int test_timeout = 3000;  // msec

  if (image_display) ImageViewer::init();

  fs::path info_file_path = source / info_file;
  fs::path result_file_path = source / result_file;

  result = single_run(info_file_path.c_str(), result_file_path.c_str(),
                      image_display, club);
  /* result = single_run(info_file_path, result_file_path, image_display, club);
   */

  while (test_timeout) {
    if (test_status == TestStatus::end || test_status == TestStatus::idle) {
      LOCAL_DEBUG(("TESTAPP_IANA_File: Test completed\n"));
      break;
    }
    cr_sleep(100);
    test_timeout -= 100;
  }

  test_status = TestStatus::end;

  if (image_display) ImageViewer::fin();

  return result;
}

TEST_P(IanaSim, IntegTest) {
  fs::path sources_path = std::get<0>(GetParam());
  std::string field = std::get<1>(GetParam());
  fs::path result_path = sources_path / result_file;
  fs::path ref_path = sources_path / ref_file;

  if (field == "CONTROL::RUN") {
    const testing::TestInfo* const test_info =
        testing::UnitTest::GetInstance()->current_test_info();

    /* std::cout << "info file name: " << info_path << std::endl; */
    /* printf("We are in test %s, test suite %s.\n", test_info->name(), */
    /*        test_info->test_suite_name()); */
    int ret = run(sources_path.c_str(), 0, 0);
    /* ASSERT_EQ(ret, 0); */

    /* ASSERT_EQ(IanaSim::iana.runstate, IANA_RUN_GOODSHOT); */
    /* ASSERT_EQ(IanaSim::iana.shotresultflag, SHOTRESULT_GOODSHOT); */
  } else {
    ResultXML result = ResultXML(result_path);
    ReferenceXML ref = ReferenceXML(
        ref_path, IanaSim::iana.camsensor_category == CAMSENSOR_EYEXO);
    /* ASSERT_GE(IanaSim::cb_sensor_status, IANA_SENSOR_STATE_GOODSHOT); */
    /* ASSERT_LE(IanaSim::cb_sensor_status, IANA_SENSOR_STATE_SWING); */

    static std::map<std::string, double> spec = {
        {"vmag", 1},       {"incline", 5},   {"azmuth", 5},
        {"clubpath", 5},   {"clubspeed", 1}, {"backspin", 500},
        {"sidespin", 300}, {"spinmag", 500}};

    EXPECT_GT(result.get(field), ref.get(field) - spec[field]);
    EXPECT_LT(result.get(field), ref.get(field) + spec[field]);
  }
  /* IanaSim::cb_userparam = userparam; */

  /* using ::testing::StartsWith; */
  /* EXPECT_THAT(sources_path, StartsWith("/home/")); */
}

static std::string expand_tilda(std::string s) {
  static std::string HOME = std::getenv("HOME");
  if (s.rfind("~", 0) == 0) {
    s.replace(0, 1, HOME);
  }
  return s;
}

static std::vector<fs::path> get_sources(std::vector<fs::path> vp) {
  std::vector<fs::path> sources;
  for (const auto& p : vp) {
    std::string sources_path = expand_tilda(p);
    for (const auto& entry : fs::directory_iterator(sources_path)) {
      fs::path source = entry.path();
      std::string stem = entry.path().stem();
      if (fs::exists(source / info_file) && fs::exists(source / ref_file)) {
        sources.push_back(source);
      }
      /* std::cout << entry.path() << std::endl; */
      /* std::cout << references / entry.path().stem() << std::endl; */
    }
  }
  return sources;
}

INSTANTIATE_TEST_SUITE_P(
    Dataset, IanaSim,
    testing::Combine(testing::ValuesIn(get_sources(std::vector<fs::path>{
                         "~/shot-data/routinetest/EYEXO/",
                         "~/shot-data/routinetest/ZCAM3/"})),
                     testing::Values("CONTROL::RUN", "vmag", "incline",
                                     /* "azmuth", */
                                     "clubpath", "clubspeed", "backspin",
                                     "sidespin", "spinmag")));

