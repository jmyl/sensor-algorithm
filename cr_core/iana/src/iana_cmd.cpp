/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA Command Processing
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_cmd.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2016/02/20 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <string.h>

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"

#include "scamif.h"
#include "scamif_buffer.h"

#include "iana.h"
#include "iana_implement.h"
#include "iana_adapt.h"
#include "iana_core.h"
#include "iana_cam.h"
#include "iana_work.h"
#include "iana_run.h"
#include "iana_traj.h"
#include "iana_xbrd.h"
#include "iana_rgbled.h"
#include "scamif.h"

#include "info.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define TRAJMODENFILE	"trajmode.txt"

#if defined(_WIN32)
#define VER_MAJOR	0
#define VER_MINOR	2
#define VER_BUILD	1
#else
#define VER_MAJOR	MAJOR_SW_VERSION
#define VER_MINOR	MINOR_SW_VERSION
#define VER_BUILD	SW_BUILD_NUMBER
#endif

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/

/*!
 ********************************************************************************
 *	@brief      IANA Command 
 *
 *
 *	@author	    yhsuk
 *  @date       2015/11/30
 *******************************************************************************/

IANA_EXPORT IANA_result_t IANA_cmd(HAND h, U32 cmd, PARAM_T p0, PARAM_T p1, PARAM_T p2, PARAM_T p3)
{

	IANA_result_t res;
	iana_t *piana;

	//--------
	piana = (iana_t *) h;

	if (piana == NULL) {
		res = IANA_ERR_BADHANDLE;
		goto func_exit;
	}

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"() command 0x%08x with p: %08x %08x %08x %08x\n",
	//	cmd, p0, p1, p2, p3);



	//--------
	switch (cmd) {
		case IANA_CMD_OPERATION_SET_AIRPRESSURE_PA:
			{
				double airpressure_pa;
				airpressure_pa = (double) p0;

				iana_traj_scapegoat_airpressure_Pa(airpressure_pa);

				res = IANA_OK;
				break;
			}

		case IANA_CMD_OPERATION_SET_ALTITUDE:
			{
				double altitude_m;
				altitude_m = (double) p0;

				iana_traj_scapegoat_airpressure_altitude(altitude_m);

				res = IANA_OK;
				break;
			}

		case IANA_CMD_OPERATION_SET_CLUBMODE:
			{
				U32 clubcalc_mode;
				clubcalc_mode = (U32) p0;

				piana->clubcalc_method_required = clubcalc_mode;

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"() IANA_CMD_OPERATION_SET_CLUBMODE. clubcalc_method_required: %d\n", clubcalc_mode);
				res = IANA_OK;
				break;
			}



		case IANA_CMD_CALC_TRAJECTORY:
			{
#if 1
				U32 i;
				U32 count;
				HAND htraj;
				iana_traj_data_t   td, *ptd;
				iana_traj_result_t *ptr, tr;

				IANA_shotdata_t *pcsd;
				IANA_trajectory_t *pctj;

				U32 trajmode;

				//----------------------------
				pcsd 	= (IANA_shotdata_t *) p0;
				pctj 	= (IANA_trajectory_t *) p1;



				////trajmode = 1;
				//trajmode = 3;

				trajmode = piana->trajmode;

#if defined(TRAJMODENFILE)
				{
					FILE *fp;
					int mode;
					mode = 1;
					fp = cr_fopen(TRAJMODENFILE, "r");

					if (fp) {
						fscanf(fp, "%d", &mode);
						if (mode == 2) {
							trajmode = 2;
						} else if (mode == 3) {
							trajmode = 3;
						}
						cr_fclose(fp);
					}
				}
#endif


				ptd = &td;
				ptr = &tr;
				memset(ptr, 0, sizeof(iana_traj_result_t));
				if (trajmode == 2) {
					htraj = iana_traj_create_2();
				} else if (trajmode == 3) {
					htraj = iana_traj_scapegoat_create();
				} else {
					htraj = iana_traj_create();
				}

				ptd->vmag 		= (double) pcsd->ballspeedx1000 / 1000.0;	// [m/s]
				ptd->azimuth	= (double) pcsd->azimuthx1000   / 1000.0;	// [degree]
				ptd->incline	= (double) pcsd->inclinex1000   / 1000.0;	// [degree]
				ptd->hitheight	= (double) pcsd->hitheightx1000 / 1000.0;	// [m]
				ptd->backspin	= (double) pcsd->backspin;	// [rpm]
				ptd->sidespin	= (double) pcsd->sidespin;	// [rpm]

				ptd->windmag	= (double) piana->windspeedx1000 / 1000.0;
				ptd->windangle	= (double) piana->winddirectionx1000 / 1000.0;


#define IANA_TRAJ_TIMEDELTA		(0.02)			// use this... 
				if (trajmode == 2) {
					iana_traj_2(htraj,	
							IANA_TRAJ_TIMEDELTA, 
							ptd,
							ptr);
					iana_traj_delete_2(htraj);
				} else if (trajmode == 3) {
					iana_traj_scapegoat(htraj,	
							IANA_TRAJ_TIMEDELTA, //(0.01),			// IANA_TRAJ_TIMEDELTA, 
							ptd,
							ptr);
					iana_traj_scapegoat_delete(htraj);
				} else {
					iana_traj(htraj,	
							IANA_TRAJ_TIMEDELTA, 
							ptd,
							ptr);
					iana_traj_delete(htraj);
				}

				count = ptr->count;
				if (count > IANA_RESULT_COUNT_MAX) {
					count = IANA_RESULT_COUNT_MAX;
				}

				//--
				pctj->count 				= (int) (ptr->count);

				pctj->peakheightx1000 		= (int) (ptr->height * 1000.0);
				pctj->carrydistancex1000 	= (int) (ptr->distance * 1000.0);
				pctj->sidedistancex1000 	= (int) (ptr->side * 1000.0);
				pctj->flighttimeX1000 		= (int) (ptr->flighttime * 1000);

				//	iana_point_t pts[CRGTRAJECTORYMAXCOUNT];	// Unit of x, y, z: [cm],   t: [0.001sec == msec]
				for (i = 0; i < count; i++) {
					pctj->pts_mm[i].x 	= (int) (ptr->x[i] * 1000.0);		// mm
					pctj->pts_mm[i].y 	= (int) (ptr->y[i] * 1000.0);		// mm
					pctj->pts_mm[i].z 	= (int) (ptr->z[i] * 1000.0);		// mm


					pctj->pts_mm[i].t =		(int) (ptr->t[i] * 1000.0);

					pctj->vs[i].t		= (int) (ptr->t[i] * 1000.0);
					pctj->vs[i].vx 		= (int) (ptr->vx[i] * 1000.0);		// mm/s
					pctj->vs[i].vy 		= (int) (ptr->vy[i] * 1000.0);		// mm/s
					pctj->vs[i].vz 		= (int) (ptr->vz[i] * 1000.0);		// mm/s

					pctj->vs[i].backspin	= (int) (ptr->backspin[i]); // rpm
					pctj->vs[i].sidespin 	= (int) (ptr->sidespin[i]); // rpm
				}

#endif
				res = IANA_OK;
				break;
			}

		case IANA_CMD_DLLVERSION:
			{
				U32 *pmajor;
				U32 *pminor;
				U32 *pbuildnum;

				pmajor = (U32 *) p0;
				pminor = (U32 *) p1;
				pbuildnum = (U32 *) p2;

				*pmajor = VER_MAJOR;
				*pminor = VER_MINOR;
				*pbuildnum = VER_BUILD;

				res = IANA_OK;
				break;
			}
		case IANA_CMD_OPERATION_START:
			{
				I32 ires;
				piana->hcbfunc 		= (HAND) p0;
				piana->userparam 	=  p1;
				piana->cbfunc1_id 	= 0;

				piana->hcbfunc1 	= (HAND) NULL;
//				piana->hcbfunc2 	= (HAND) NULL;
				//piana->userparam2 	= (I32)  NULL;

				ires = iana_run_start(piana);
				if (ires < 0) {
					piana->needrestart = 1;

					cr_log(piana->hcl, 3, "needrestart %s %d", "IANA_CMD_OPERATION_START", __LINE__);
				}
				//piana->activated = 1;
				res = IANA_OK;
				break;
			}


		case IANA_CMD_OPERATION_START1:
			{
				I32 ires;
				piana->hcbfunc 		= (HAND) NULL;
				piana->userparam 	= (PARAM_T) p2;

				if (p3 < 2) {
					piana->cbfunc1_id	= (U32) p1;
					piana->hcbfunc1 	= (HAND) p0;
				} else {
					piana->cbfunc1_id	= (U32) 0;
					piana->hcbfunc1 	= (HAND) NULL;
				}

//				piana->hcbfunc2 	= (HAND) NULL;

				ires = iana_run_start(piana);
				if (ires < 0) {
					piana->needrestart = 1;

					cr_log(piana->hcl, 3, "needrestart %s %d", "IANA_CMD_OPERATION_START3", __LINE__);
				}
				//piana->activated = 1;
				res = IANA_OK;
				break;
			}


		case IANA_CMD_OPERATION_STOP:
			{
				
				iana_run_stop(piana);
				piana->activated = 0;

				res = IANA_OK;
				break;
			}


		case IANA_CMD_OPERATION_PAUSE:
			{
				iana_run_pause(piana);
				//piana->activated = 0;
				
				res = IANA_OK;
				break;
			}

		case IANA_CMD_OPERATION_RESTART:
			{
				iana_run_needrestart2(piana);
				piana->activated = 1;

				res = IANA_OK;
				break;
			}

		case IANA_CMD_OPERATION_SETAREA:
			{
				U32 area;
				area = (U32)p0;						// 0: TEE, 1: IRON, 2: Putter
				iana_setarea(piana, area);

//				piana->state 		= IANA_STATE_STARTED;
				res = IANA_OK;
				break;
			}

		case IANA_CMD_OPERATION_SET_CALLBACK:
			{
/*
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" SET_CALLBACK. hcbfunc: %08x, cbfunc1_id: %d, hcbfunc1: %08x, userparam: %08x\n", 
					(U32)piana->hcbfunc,
					(U32)piana->cbfunc1_id,
					(U32)piana->hcbfunc1,
					(U32)piana->userparam
					);
*/
				if (p1 == 0) {		// original call back function
					piana->hcbfunc 		= (HAND) p0;
					piana->cbfunc1_id 	= 0;
					piana->hcbfunc1 	= (HAND) NULL;
				} else {
					//piana->hcbfunc 		= (HAND) p0;
					piana->cbfunc1_id 	= (U32)p1;
					piana->hcbfunc1 	= (HAND) p0;
				}
/*
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" SET_CALLBACK. cmd: %08x p0: %08x p1: %08x    cbfunc1: %d\n", (U32) cmd, (U32) p0, (U32) p1, piana->cbfunc1_id);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"            ==> hcbfunc: %08x, cbfunc1_id: %d, hcbfunc1: %08x, userparam: %08x\n", 
					(U32)piana->hcbfunc,
					(U32)piana->cbfunc1_id,
					(U32)piana->hcbfunc1,
					(U32)piana->userparam
					);
*/
				res = IANA_OK;
				break;
			}


		case IANA_CMD_OPERATION_AREARECT_SET:
			{
				res = IANA_OK;
				break;
			}


		case IANA_CMD_OPERATION_AREARECT_GET:
			{
				res = IANA_OK;
				break;
			}

		case IANA_CMD_OPERATION_ACTIVATE:
			{
//#define TEST_TEST

#if defined(TEST_TEST)
				piana->activated = 1;
#else
				piana->activated = (U32)p0;
#endif
				// TODO
				res = IANA_OK;
				break;
			}


		case IANA_CMD_OPERATION_ACTIVATEVALID:
			{
				// TODO
				res = IANA_OK;
				break;
			}
		case IANA_CMD_OPERATION_AREAALLOW:
			{
				U32 allowTee;
				U32 allowIron;
				U32 allowPutter;

				//---
				allowTee 	= (U32)p0;
				allowIron 	= (U32)p1;
				allowPutter	= (U32)p2;
				iana_setareaallow(piana, allowTee, allowIron, allowPutter);
				res = IANA_OK;
				break;
			}

		case IANA_CMD_OPERATION_BALLPOSITION:
			{
				if (p3 == 0) {
					if (iana_getballposition(piana, (HAND) p0, (HAND) p1, (HAND) p2, CR_FALSE)) {
						res = IANA_OK;
					} else {
						res = IANA_ERR_GENERAL;
					}
				} else if (p3 == 1) {
					if (iana_getballposition(piana, (HAND) p0, (HAND) p1, (HAND) p2, CR_TRUE)) {
						res = IANA_OK;
					} else {
						res = IANA_ERR_GENERAL;
					}
				} else {
					res = IANA_ERR_GENERAL;
				}
				break;
			}

		case IANA_CMD_OPERATION_SHOTRESULTEX:
			{
				if (iana_shotdata_ballEx(piana, (U32) p0, (HAND) p1)) {
					res = IANA_OK;
				} else {
					res = IANA_ERR_GENERAL;
				}
				break;
			}


		case IANA_CMD_OPERATION_SHOTDATAEX:		// 20200409
			{
				if (iana_shotdataEx(piana, (HAND) p0)) {
					res = IANA_OK;
				} else {
					res = IANA_ERR_GENERAL;
				}
				break;
			}

		case IANA_CMD_OPERATION_SERIAL_GET:
			{
				PARAM_T *pvalid;
				char *pbuf;

				//---
				pbuf = (char *) p0;
				pvalid = (PARAM_T *) p1;

				*pvalid = 1;

				strcpy(pbuf, "Very Good");

				res = IANA_OK;
				break;
			}

		case IANA_CMD_OPERATION_MATRECT_SET:
			{
				res = IANA_OK;
				break;
			}

		case IANA_CMD_OPERATION_MAT2RECT_SET:
			{
				res = IANA_OK;
				break;
			}

		case IANA_CMD_OPERATION_MATUSE_SET:
			{
				res = IANA_OK;
				break;
			}

		case IANA_CMD_OPERATION_SETWIND:
			{
				//windspeedx1000		= p0;
				//winddirectionx1000 	= p1;

				piana->windspeedx1000	= (I32)p0; 
				piana->winddirectionx1000 = (I32)p1;
				res = IANA_OK;
				break;
			}

		case IANA_CMD_OPERATION_GETSTATUS:
			{
				U32 *psstatus = (U32 *) p0;

				if (psstatus) {
					*psstatus = piana->runstate;
				}

				res = IANA_OK;
				break;
			}


		case IANA_CMD_OPERATION_TEESTATE_SET:
			{
#if 0
// p0, Tee state is "DOWN" = 0 / "UP" = 1,   p1, 0: NO ball , 1: Ball on the TEE
				if (p0 == 0) { // TEE DOWN
					pcaw->teestate = 0;
				} else if (p0 == 1) { // TEE UP
					pcaw->teestate = 1;
				} else {
					// what?
				}

				if (p1 == 0) { // NOBALL
					pcaw->teeball = 0;
				} else if (p1 == 1) { // BALL
					pcaw->teeball = 1;
				} else {
					// what?
				}
				cr_trace(CREU_MSG_AREA_CHECKIT, CREU_MSG_LEVEL_WARN, "{CMDCMD} TEESTATE: %d, TEEBALL: %d\n",
					pcaw->teestate, pcaw->teeball);

				res = CRKEA_OK;
				break;
#endif
				res = IANA_OK;
				break;
			}

		case IANA_CMD_OPERATION_RIGHTLEFT_SET:
			{
				iana_setrightleft(piana, (U32)p0);
				res = IANA_OK;
				break;
			}




		case IANA_CMD_OPERATION_GETSWSPEC:						// 20190511
			{
				if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
					U08 swspec[64+4];
					U08 barcode[64];
					U32 blen;

					res = scamif_security_barcode(piana->hscamif, barcode);
					blen = (U32)strlen((char *)barcode);
					if (res < 0 || blen == 0) {
						scamif_security_update(piana->hscamif);
					}

					res = scamif_security_swspec(piana->hscamif, swspec);
					if (res) {
						memcpy((void *)p0, swspec, 64+4);
					} else {
						memset((void *)p0, 0, 64+4);
					}
				}
				{
					int i;
					U32 *pbuf;

					pbuf = (U32 *)p0;
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"  SWSPEC.");
					for (i = 0; i < (64+4)/4; i++) {
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%08x ", pbuf[i]);
					}
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
				}


				res = IANA_OK;
				break;
			}

		case IANA_CMD_OPERATION_GETBARCODE:						// 20190511
			{
				if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
					U08 barcode[64];
					U32 blen;
					int iter;

					for (iter = 0; iter < 2; iter++) {
						memset(barcode, 0, 64);
						res = scamif_security_barcode(piana->hscamif, barcode);
						blen = (U32)strlen((char *)barcode);
						if (res && blen > 0) {
							memcpy((void *)p0, barcode, 64);
							break;
						} 

						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" Reread barcode\n");
						memset((void *)p0, 0, 64);
						scamif_security_update(piana->hscamif);
					}
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" barcode: %s\n", barcode);
				}
				res = IANA_OK;
				break;
			}

		case IANA_CMD_EXC_IMAGE_CALLBACK:
			{
				HAND hfunc;

				hfunc = (HAND) p0;

				iana_image_callback_set(piana, hfunc, p1);
				res = IANA_OK;
				break;
			}

		case IANA_CMD_EXC_XBOARD_CPUSID_READ:
			{
				U32 *buf;
				buf = (U32 *) p0;

				res = iana_xboard_cpusid_read(piana, buf);
				if (res == 1) {
					res = IANA_OK;
				} else {
					res = IANA_ERR_GENERAL;
				}

				break;
			}


		case IANA_CMD_EXC_XBOARD_FLASH_READ:
			{
				U08 *buf;
				U32 addr, length;

				buf		= (U08 *) p0;
				addr	= (U32) p1;
				length	= (U32) p2;

				res = iana_xboard_flash_read(piana, buf, addr, length);
				if (res == 1) {
					res = IANA_OK;
				} else {
					res = IANA_ERR_GENERAL;
				}

				break;
			}


		case IANA_CMD_EXC_XBOARD_FLASH_WRITE:
		case IANA_CMD_EXC_XBOARD_FLASH_WRITE_WITHERASE:
			{
				U08 *buf;
				U08 *writebackbuf;
				U32 addr, length;

				buf		= (U08 *) p0;
				writebackbuf		= (U08 *) p1;
				addr	= (U32) p2;
				length	= (U32) p3;

				if (cmd == IANA_CMD_EXC_XBOARD_FLASH_WRITE) {
					res = iana_xboard_flash_write(piana, buf, writebackbuf, addr, length);
				} else if (cmd == IANA_CMD_EXC_XBOARD_FLASH_WRITE_WITHERASE) {
					res = iana_xboard_flash_write_witherase(piana, buf, writebackbuf, addr, length);
				} else {
					res = 0;
				}
				if (res == 1) {
					res = IANA_OK;
				} else {
					res = IANA_ERR_GENERAL;
				}

				break;
			}



		case IANA_CMD_EXC_XBOARD_FLASH_ERASE:
			{
				U32 addr, length;

				addr	= (U32) p0;
				length	= (U32) p1;

				res = iana_xboard_flash_erase(piana, addr, length);
				if (res == 1) {
					res = IANA_OK;
				} else {
					res = IANA_ERR_GENERAL;
				}

				break;
			}



		case IANA_CMD_OPERATION_RGB0_RGB:
			{
	// Set RGB code. p0:  0x01: CAM1, 0x02: CAM2, 0x03; CAM1 and CAM2. both. right Sensor  
	//                    0x10: CAM1, 0x20: CAM2, 0x30; CAM1 and CAM2. both. LEFT Sensor   
	//				 p1: (((R << 16) & 0x00FF0000) | ((G << 8) & 0x0000FF00) | (B & 0x000000FF))
				U32 rightcamcode;
				U32 leftcamcode;
				U08 r_, g_, b_;

				rightcamcode = (U32)((p0 >> 0) & 0x03);
				leftcamcode  = (U32)((p0 >> 4) & 0x03);

				r_ = (U08)((p1 >> 16) & 0xFF);
				g_ = (U08)((p1 >>  8) & 0xFF);
				b_ = (U08)((p1 >>  0) & 0xFF);

				if (rightcamcode) {
					iana_rgbled_rgb0(piana, 0, rightcamcode, r_, g_, b_);
				}
				if (leftcamcode) {
					iana_rgbled_rgb0(piana, 1, leftcamcode, r_, g_, b_);
				}
				res = IANA_OK;
				break;
			}

		case IANA_CMD_OPERATION_RGB1_RGB:
			{
	// Set RGB code. p0:  0x01: CAM1, 0x02: CAM2, 0x03; CAM1 and CAM2. both. right Sensor  
	//                    0x10: CAM1, 0x20: CAM2, 0x30; CAM1 and CAM2. both. LEFT Sensor
	//				 p1: (((R << 16) & 0x00FF0000) | ((G << 8) & 0x0000FF00) | (B & 0x000000FF))
	//               p2: ledid (0 ~ 11)
				U32 rightcamcode;
				U32 leftcamcode;
				U32 ledid;
				U08 r_, g_, b_;

				rightcamcode = (U32)((p0 >> 0) & 0x03);
				leftcamcode = (U32)((p0 >> 4) & 0x03);
				ledid = (U32)p2;

				r_ = (U08) ((p1 >> 16) & 0xFF);
				g_ = (U08) ((p1 >>  8) & 0xFF);
				b_ = (U08) ((p1 >>  0) & 0xFF);
				if (rightcamcode) {
					iana_rgbled_rgb1(piana, 0, rightcamcode, ledid, r_, g_, b_);
				}
				if (leftcamcode) {
					iana_rgbled_rgb1(piana, 1, leftcamcode, ledid, r_, g_, b_);
				}
				res = IANA_OK;
				break;
			}

		case IANA_CMD_OPERATION_RGB2_RGB:
			{
			U32 rightcamcode;
			U32 leftcamcode;
				IANA_rgb2_t *prgb2;

				//----
				rightcamcode = (U32)((p0 >> 0) & 0x03);
				leftcamcode = (U32)((p0 >> 4) & 0x03);

				prgb2 = (IANA_rgb2_t*) p1;

				if (prgb2) {
					if (rightcamcode) {
						iana_rgbled_rgb2(piana, 0, rightcamcode, &prgb2->r[0], &prgb2->g[0], &prgb2->b[0]);
					}
					if (leftcamcode) {
						iana_rgbled_rgb2(piana, 1, leftcamcode, &prgb2->r[0], &prgb2->g[0], &prgb2->b[0]);
					}					
				}
				res = IANA_OK;
				break;
			}
		case IANA_CMD_OPERATION_RGB3_RGB:
			{
				U32 rightcamcode;
				U32 leftcamcode;
				U32 tablecode;		// table code
				U32 itercount;		// iteration count
				U32 direction;		// 0: Normal, 1: Reverse

				//----
				rightcamcode = (U32)((p0 >> 0) & 0x03);
				leftcamcode = (U32)((p0 >> 4) & 0x03);
				tablecode = (U32)p1;
				itercount = (U32)p2;
				direction = (U32)p3;

				if (rightcamcode) {
					iana_rgbled_rgb3(piana, 0, rightcamcode, tablecode, itercount, direction);
				}
				if (leftcamcode) {
					iana_rgbled_rgb3(piana, 1, leftcamcode, tablecode, itercount, direction);
				}

				res = IANA_OK;
				break;
			}
		case IANA_CMD_OPERATION_RGB4_RGB:
			{
				U32 rightcamcode;
				U32 leftcamcode;
				U32 rotationtype;
				U32 gradianttype;
				U32 animationspeed;
				U32 animationmode;
				//----
				rightcamcode = (U32)((p0 >> 0) & 0x03);
				leftcamcode = (U32)((p0 >> 4) & 0x03);
				rotationtype = (U32)p1;
				gradianttype = 0;
				animationspeed = (U32)p2;
				animationmode = 1;

				if (rightcamcode) {
					iana_rgbled_rgb4(piana, 0, rightcamcode, rotationtype, gradianttype, animationspeed, animationmode);
				}
				if (leftcamcode) {
					iana_rgbled_rgb4(piana, 1, leftcamcode, rotationtype, gradianttype, animationspeed, animationmode);
				}

				res = IANA_OK;
				break;
			}
		default:
			{
				// DO something
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "unsupported command 0x%08x with p: %08x %08x %08x %08x\n",
					cmd, p0, p1, p2, p3);

				res = IANA_ERR_UNSUPPORTED_CMD;
				break;
			}
	}

func_exit:
	return res;
}

#if defined (__cplusplus)
}
#endif


