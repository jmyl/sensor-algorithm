/********************************************************************************
                                                                                
                   Creatz Golf sensor 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 by Creatz Inc.
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc. \n

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_traj.c
	 @brief  Golf Trajectory.
	 @author YongHo Suk                                 
	 @date   2011/10/05 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#include <string.h>


#include "cr_common.h"

#define _USE_MATH_DEFINES								// for using M_PI
#include <math.h>

#include "iana_adapt.h"

#include "cr_osapi.h"
#include "cr_interpolation.h"

#include "iana_traj.h"





/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#if defined( _MSC_VER ) && (_MSC_VER <= 1200)    // 1200 = MSVC 6.0.
#define __FUNCTION__
#endif

#if defined(TASUK_CAM)
#define	TRAJTYPE	1

#if defined(RUNGOLF_H600C)
#error USE iana_traj-324.c... -_-;
#undef	TRAJTYPE	
//#define	TRAJTYPE	1
#define	TRAJTYPE	0
#endif

#else
#define TRAJTYPE	0
#endif

#define SIDESPIN_PLUS_SLICE__MINUS_HOOK					// 2010/11/09

#define VISCOSITY					(0.0000182)			// kg / (m sec)

#define CATEGORY_INCLINE_LOWLOW			0x0001
#define CATEGORY_INCLINE_LOWMID			0x0002
#define CATEGORY_INCLINE_LOWHI			0x0003
#define CATEGORY_INCLINE_MIDLOW			0x0004
#define CATEGORY_INCLINE_MIDMID			0x0005
#define CATEGORY_INCLINE_MIDHI			0x0006
#define CATEGORY_INCLINE_HI00			0x0007
#define CATEGORY_INCLINE_HI01			0x0008
#define CATEGORY_INCLINE_HI02			0x0009
#define CATEGORY_INCLINE_HI03			0x000a
#define CATEGORY_INCLINE_HI04			0x000b


#define CATEGORY_INCLINE(x)				((x) & 0x000F)


/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*

             ^               
             |Z               
             |              .
             |          .       .
             |       .            .
             |     .               .
             |   .                  .
             | .                     .
             |------------------------.----->		Y
            /                          .
           /
		  /
		 V  X

*/


/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/
#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/
#if 0 // not used 
#define BACKSPIN_V

#if defined(BACKSPIN_V)
static double refine_backspin(double backspin, double vmag);
//static double revive_backspin(double backspin, double vmag);
static double revive_backspin(double backspin);
#else
static double refine_backspin(double backspin);
static double revive_backspin(double backspin);
#endif
#define SIDE_A
#if defined(SIDE_A)
static double refine_sidespin(double sidespin, double azimuth);
static double revive_sidespin(double sidespin, double azimuth);
#else
static double refine_sidespin(double sidespin);
static double revive_sidespin(double sidespin);
#endif
#endif

//double calcfmxTrate(double t, double vmag);
double calcfmxTrate(double t);
//static void traj_rungekutta4(HAND htraj, double timedelta, I32 category);
//static void traj_RK_RightHandSide(HAND htraj,
 //                            double *q, 
//							 double *deltaQ, 
//							 double td, 
 //                            double qScale, 
//							 double *dq,
//							 double t,
//							 I32 category
//							 );
static void traj_rungekutta4_2(HAND htraj, double timedelta, I32 category, double spin_halflife, double fm_mult, double fmy_positive_mult);	
static void traj_RK_RightHandSide_2(HAND htraj,
                             double *q, 
							 double *deltaQ, 
							 double td, 
                             double qScale, 
							 double *dq,
							 double t,
							 I32 category
							 , double spin_halflife, double fm_mult, double fmy_positive_mult);	

static int calcw(double vmag, double incline, double spinmag, double wc[4]);
double iana_traj_spinmag_calc(HAND htraj, double t);



/*!
 ********************************************************************************
 *	@brief      create trajectory object
 *  @return		handle of trajectory object
 *
 *	@author	    yhsuk
 *  @date       2009/11/08
 *******************************************************************************/
iana_traj_t *iana_traj_create_2(void)
{
	iana_traj_t	      *ptraj;
	iana_traj_basic_t *ptrajb;

	ptraj = (iana_traj_t *) malloc (sizeof(iana_traj_t));
	if (ptraj == NULL) {
		goto func_exit;
	}

	memset(ptraj, 0, sizeof(iana_traj_t));

	ptrajb = &ptraj->trajb;


	ptrajb->ballmass = 0.0459;				// Org... 
	ptrajb->ballarea = 0.001432;
	ptrajb->ballradius= (0.0428/2.);

	ptrajb->maxz = 0.0;

#define AIRDENSITY 1.225
	ptrajb->airdensity = AIRDENSITY;
func_exit:
	return ptraj;
}



/*!
 ********************************************************************************
 *	@brief      delete trajectory object
 *	@param[in]  htraj  handle of trajectory object
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2009/11/08
 *******************************************************************************/
void iana_traj_delete_2(HAND htraj)
{
	if (htraj) {
		free(htraj);
	}
}

/*!
 ********************************************************************************
 *	@brief      get trajectory
 *	@param[in]  htraj		handle of trajectory object
 *	@param[in]  timedelta	time resolution for trajectory (sec)
 *	@param[in]  ptd			input data for trajectory calculation
 *	@param[out]  ptr		result of trajectory calculation
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2009/11/08
 *******************************************************************************/
void iana_traj_2(
		HAND htraj,
		double  timedelta,
		iana_traj_data_t *ptd,
		iana_traj_result_t *ptr)
{
	U32		i;
	U32		flagu;
	I32 	category;
	double 	spin_halflife;
	double 	fm_mult;
	double 	fmy_positive_mult;
	iana_traj_t	*ptraj; 
	double maxheight;

	iana_traj_data_t *ptdORG;
	iana_traj_data_t td;

	//------------------------------------
	//
	//------------------------------------

	memcpy(&td, ptd, sizeof(iana_traj_data_t));
	ptdORG = ptd;
	ptd = &td;
	{
		double vmag, incline;
		double sidespin, backspin, rollspin, spinmag;

		//--
		vmag = ptd->vmag;
		sidespin = ptd->sidespin;
		backspin = ptd->backspin;
		rollspin = 0;
		incline =  ptd->incline;

		ptraj = (iana_traj_t *) htraj; 

		spinmag = sqrt(backspin*backspin + sidespin*sidespin + rollspin*rollspin);
		calcw(vmag, incline, spinmag, ptraj->wc);
	}



#define LINEAR_SCALE(x,x0,y0,x1,y1) ( ( ((double) (((y1) - (y0)))) / ((double) ((x1) - (x0))) )*(x-x0) + y0 )

//#define G_SENSOR
#if defined(G_SENSOR)			// 20190607.  5th try.. :)
	{
		double vmag, sidespin;
		double vmag0, sidespin0;
		double vmult0, smult0;
		double vmult1;
		double backspin;

		double incline;
		//---

		vmag = ptd->vmag;
		sidespin = ptd->sidespin;
		backspin = ptd->backspin;

		incline =  ptd->incline;

		if (vmag < 15) {
			vmult0 = LINEAR_SCALE(vmag, 	 0.0,	1.0,	15.0,	1.0);
		} else if (vmag < 20.0) {
			vmult0 = LINEAR_SCALE(vmag, 	15.0,	1.0,	20.0,	1.0/1.03);
		} else if (vmag < 30.0) {
			vmult0 = LINEAR_SCALE(vmag, 	20.0,	1.0/1.03, 	30.0,	1.0/1.0);
		} else if (vmag < 40.0) {
			if (incline < 10) {
				vmult0 = LINEAR_SCALE(vmag, 	30.0,	1.0/1.0, 	40.0,	1.0/0.99);
			} else if (incline < 12) {
				vmult0 = LINEAR_SCALE(vmag, 	30.0,	1.0/1.0, 	40.0,	1.0/0.98);
			} else if (incline < 16) {
				vmult0 = LINEAR_SCALE(vmag, 	30.0,	1.0/1.0, 	40.0,	1.0/0.96);
			} else {
				vmult0 = LINEAR_SCALE(vmag, 	30.0,	1.0/1.0, 	40.0,	1.0/0.95);
			}
		} else if (vmag < 50.0) {
			if (incline < 10) {
				vmult0 = LINEAR_SCALE(vmag, 	40.0,	1.0/0.99, 	50.0,	1.0/0.97);
			} else if (incline < 12) {
				vmult0 = LINEAR_SCALE(vmag, 	40.0,	1.0/0.98, 	50.0,	1.0/0.95);
			} else if (incline < 16) {
				vmult0 = LINEAR_SCALE(vmag, 	40.0,	1.0/0.96, 	50.0,	1.0/0.95);
			} else {
				vmult0 = LINEAR_SCALE(vmag, 	40.0,	1.0/0.95, 	50.0,	1.0/0.91);
			}
		} else if (vmag < 60.0) {
			if (incline < 10) {
				vmult0 = LINEAR_SCALE(vmag, 	50.0,	1.0/0.97, 	60.0,	1.0/0.93);
			} else if (incline < 12) {
				vmult0 = LINEAR_SCALE(vmag, 	50.0,	1.0/0.95, 	60.0,	1.0/0.93);
			} else if (incline < 16) {
				vmult0 = LINEAR_SCALE(vmag, 	50.0,	1.0/0.95, 	60.0,	1.0/0.92);
			} else {
				vmult0 = LINEAR_SCALE(vmag, 	50.0,	1.0/0.91, 	60.0,	1.0/0.91);
			}
		} else if (vmag < 70.0) {
			if (incline < 10) {
				vmult0 = LINEAR_SCALE(vmag, 	60.0,	1.0/0.93, 	70.0,	1.0/0.93);
			} else if (incline < 12) {
				vmult0 = LINEAR_SCALE(vmag, 	60.0,	1.0/0.93, 	70.0,	1.0/0.93);
			} else if (incline < 16) {
				vmult0 = LINEAR_SCALE(vmag, 	60.0,	1.0/0.92, 	70.0,	1.0/0.93);
			} else {
				vmult0 = LINEAR_SCALE(vmag, 	60.0,	1.0/0.91, 	70.0,	1.0/0.93);
			}
		} else {
			vmult0 = 1.0/0.93;
		}


		if (backspin < 500) {
			vmult1 = 1/0.97;
		} else if (backspin < 1000) {
			vmult1 = LINEAR_SCALE(backspin, 	 500.0, (1/0.98), 1000.0, (1/0.96));
		} else if (backspin < 2000) {
			vmult1 = LINEAR_SCALE(backspin, 	1000.0, (1/0.96), 2000.0, (1/0.98));
		} else if (backspin < 3000) {
			vmult1 = LINEAR_SCALE(backspin, 	2000.0, (1/0.98), 3000.0, 1.0);
		} else {
			vmult1 = 1.0;
		}



		vmult0 = vmult1 * vmult0;

		//--

		vmult0 = sqrt(vmult0);
		vmag0 = vmag*vmult0;

		smult0 = 1.5;
		sidespin0 = sidespin * smult0;

		ptd->vmag 		= vmag0;
		ptd->sidespin 	= (int)sidespin0;
	}
#endif

#define G_SENSOR2
#if defined(G_SENSOR2)				// 20190804
	{
		double vmag, sidespin;
		double vmag0;
		double vmult0;
		double vmult1;
		double backspin;
		double incline;
		//---

		vmag = ptd->vmag;
		sidespin = ptd->sidespin;
		backspin = ptd->backspin;
		incline =  ptd->incline;


		vmult0 = 1.0;
		vmult1 = 1.0;

		if (vmag < 30.0) {	
			vmult0 = 1.0;
		} else if (vmag < 50.0) {
			vmult0 = 1.0;
			if (backspin < 1000.0) {
				vmult1 = 1.0/0.88;
			} else if (backspin < 3000.0) {
				vmult1 = LINEAR_SCALE(backspin, 1000.0, (1.0/0.88), 3000.0, (1.0/0.99));
			} else if (backspin < 4000.0) {
				vmult1 = LINEAR_SCALE(backspin, 3000.0, (1.0/0.99), 4000.0, (1.0/0.99));
			} else if (backspin < 5000.0) {
				vmult1 = 1.0/0.99;
			} else if (backspin < 6000.0) {
				vmult1 = LINEAR_SCALE(backspin, 5000.0, (1.0/0.98), 6000.0, (1.0/0.95));
			} else if (backspin < 8000.0) {
				vmult1 = LINEAR_SCALE(backspin, 6000.0, (1.0/0.96), 8000.0, (1.0/0.93));
			} else {
				vmult1 = (1.0/0.93);
			}
			vmult0 = vmult0 * vmult1;

			if (incline < 15) {
				vmult1 = 1.0/0.95;
			} else if (incline < 20.0) {
				vmult1 = LINEAR_SCALE(incline, 10.0, (1.0/0.95), 20.0, (1.0));
			} else {
				vmult1 = 1.0;
			}
			vmult0 = vmult0 * vmult1;

		} else if (vmag < 60.0) {
			vmult0 = 1.0;
			if (backspin < 1000.0) {
				vmult1 = 1.0/0.84;
			} else if (backspin < 3000.0) {
				vmult1 = LINEAR_SCALE(backspin, 1000.0, (1.0/0.84), 3000.0, (1.0/0.98));
			} else if (backspin < 4000.0) {
				vmult1 = LINEAR_SCALE(backspin, 3000.0, (1.0/0.98), 4000.0, (1.0/1.005));
			} else if (backspin < 5000.0) {
				vmult1 = 1.0/1.005;
			} else if (backspin < 6000.0) {
				vmult1 = LINEAR_SCALE(backspin, 5000.0, (1.0/1.005), 6000.0, (1.0/0.985));
			} else {
				vmult1 = (1.0/0.985);
			}
			vmult0 = vmult0 * vmult1;


			if (incline < 10.0) {
				vmult1 = 1.0/0.95;
			} else if (incline < 20.0) {
				vmult1 = LINEAR_SCALE(incline, 10.0, (1.0/0.95), 20.0, (1.0));
			} else {
				vmult1 = 1.0;
			}
			vmult0 = vmult0 * vmult1;

		} else {
			if (vmag < 62.0) {
				vmult0 = 1.0;
			} else if (vmag < 70.0) {
				vmult0 = LINEAR_SCALE(vmag, 62.0, 1.0, 70.0, (1.0/0.98));
			} else {
				vmult0 = (1.0/0.98);
			}

			if (backspin < 500.0) {
				vmult1 = 1.0/0.85;
			} else if (backspin < 1500.0) {
				vmult1 = LINEAR_SCALE(backspin, 500.0, (1.0/0.85), 1500.0, (1.0/0.85));
			} else if (backspin < 3000.0) {
				vmult1 = LINEAR_SCALE(backspin, 1500.0, (1.0/0.82), 3000.0, (1.0/0.97));
			} else {
				vmult1 = 1.0/0.95;
			}
			vmult0 = vmult0 * vmult1;
		}
		vmult0 = sqrt(vmult0);
		vmag0 = vmag*vmult0;

//#define G_SENSOR2_BASEMULT	1.03
#if defined(G_SENSOR2_BASEMULT)
		vmag0 = vmag0 * G_SENSOR2_BASEMULT;
#endif

		ptd->vmag 		= vmag0;
	}
#endif

#define TRAJMULT
#if defined(TRAJMULT)			// 20190702
	{
		double vmag;
		double vmult;
		
		//---
		vmag = ptd->vmag;

		vmult = 1.0;
		//vmult = 1.03;		// 20190711. 
		{
			FILE *fp;
			int mode;
			fp = cr_fopen("trajmult.txt", "r");
			if (fp) {
				fscanf(fp, "%d", &mode);
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

				if (mode == 2) {
					fscanf(fp, "%lf", &vmult);
				}
				cr_fclose(fp);
			}
		}
#if 0
		{	// after V1.3.8.  20190705
			double vmult0;

			if (vmag < 65.0) {
				vmult0 = 1.0;
			} else if (vmag < 70.0) {
				vmult0 = LINEAR_SCALE(vmag, 65.0, 1.0, 70.0, 1.02);
			} else {
				vmult0 = 1.02;
			}
			vmult = vmult * vmult0;
		}
#endif

		ptd->vmag 		= vmag * vmult;
	}
#endif

	//---------------------------------------------
	// set initial values
	ptraj = (iana_traj_t *) htraj; 
	{
		double wx, wy, wz;

		double vx;
		double vy;
		double vz;

		double w;
		double rx;
		double ry;
		double rz;

		double windx;
		double windy;
		double vmag1;


		//-------------------------------------------------
		// convert velocity to vector form
		vmag1 = ptd->vmag;

		{
			double inc, azi;
			double alpha;
			double lxy, lx, ly, lz;

		

			inc = DEGREE2RADIAN(ptd->incline);
			azi = DEGREE2RADIAN(ptd->azimuth);

			alpha = 1./sqrt(1 + cos(azi) * cos(azi) * tan(inc) * tan(inc));
			//lxy = alpha * ptd->vmag;
			lxy = alpha * vmag1;
			lx = lxy * sin(azi);
			ly = lxy * cos(azi);
			lz = ly * tan(inc);

			vx = lx;
			vy = ly;
			vz = lz;
		}

//-----------------------------------------------------------------
//-----------------------------------------------------------------
		// wind velocity
		windx = ptd->windmag * sin(DEGREE2RADIAN(ptd->windangle));
		windy = ptd->windmag * cos(DEGREE2RADIAN(ptd->windangle));

#define WINDMAG_MULT		1
#if defined(WINDMAG_MULT)
		windx *= WINDMAG_MULT;
		windy *= WINDMAG_MULT;
#endif


//-----------------------------------------------------------------
		wx = RPM2RADIAN(ptd->backspin);  // Backspin
		wy = 0.;					// trivial..

		wz = RPM2RADIAN(ptd->sidespin); // sidespin, 2009/11/12

#if defined(SIDESPIN_PLUS_SLICE__MINUS_HOOK)			// 2010/11/09
		wz *= -1;
#endif

#define VERYSMALL_OFFSET	(1.0e-10)
		w  = sqrt(wx * wx + wy * wy + wz * wz) + VERYSMALL_OFFSET;		// non-zero.. -_-;

		rx = wx / w;
		ry = wy / w;
		rz = wz / w;
	

		//-------------------------------------------------
		// store to trajectory object
		ptraj->q[0] = vx;			// vx
		ptraj->q[1] = 0.;			//  x
		ptraj->q[2] = vy;			// vy
		ptraj->q[3] = 0.;			//  y
		ptraj->q[4] = vz;			// vz
		ptraj->q[5] = 0.;			//  z

		ptraj->r[0] = rx;			// backspin.. unit vector
		ptraj->r[1] = ry;			// 
		ptraj->r[2] = rz;			// sidespin
		ptraj->w    = w;			//  magnitude
		ptraj->current_w    = w;			//  current magnitude

		ptraj->windx= windx;
		ptraj->windy= windy;

		ptraj->t = 0.;

	}

	//---------------------------------------------
	// Init result.
	{
		memset(ptr, 0, sizeof(iana_traj_result_t));
		ptr->distance		= 0.;
		ptr->height			= 0.;
		ptr->side			= 0.;
		ptr->flighttime		= 0.;
		ptr->timedelta		= timedelta;
		ptr->count			= 0;
	}

	//-------------------------------------------
	// Get trajectory using 4th order RungeKutta method.
	maxheight = 0.;																// 이제부터 VMAG1 이 더 중요!!! 
	flagu = 0;


	{	// FAKE..
		fmy_positive_mult = 1.0;
		fm_mult = 1.0;
		spin_halflife = 2.0;
		category = 1;
	}
	for (i = 0; i < IANA_TRAJ_MAXCOUNT; i++) {
		//ptraj->t = i * timedelta;				// current flight time
		traj_rungekutta4_2(ptraj, timedelta, category, spin_halflife, fm_mult, fmy_positive_mult);

		// store current result
		{
			//ptr->t[i] = ptraj->t;	// comment out.. yhsuk. 2010/04/28
			ptr->t[i] = i * timedelta;		// current flight time, yhsuk 2010/04/28
			// OK
			ptr->x[i] = ptraj->q[1];		// x (side)
			ptr->y[i] = ptraj->q[3];		// y (target)
			ptr->z[i] = ptraj->q[5];		// z (height)

			ptr->vx[i] = ptraj->q[0];		// vx (side)
			ptr->vy[i] = ptraj->q[2];		// vy (target)
			ptr->vz[i] = ptraj->q[4];		// vz (height)

			ptr->sidespin[i] = RADIAN2RPM(ptraj->r[2] * ptraj->w);			// rz 

#if defined(SIDESPIN_PLUS_SLICE__MINUS_HOOK)			// 2010/11/09
			ptr->sidespin[i] *= -1;
#endif

			ptr->backspin[i] = RADIAN2RPM(ptraj->r[0] * ptraj->current_w);			// rx
		}

		if (ptraj->q[5] > maxheight) {
			maxheight = ptraj->q[5];
		}
//#define TRAJ_UNDER_ZERO		(-20.)
#define TRAJ_UNDER_ZERO		(-60.0)
		if (ptraj->q[5] < 0 && flagu == 0) {
			//---------------------------------------------
			// Store result..
			double x, y, z;
			double vx, vy, vz;
			double vxy;

			vx = ptraj->q[0];
			x  = ptraj->q[1];
			vy = ptraj->q[2];
			y  = ptraj->q[3];
			vz = ptraj->q[4];
			z  = ptraj->q[5];


			//---- Result..
			ptr->distance	= sqrt(x*x + y*y + z*z);
			ptr->height	= maxheight;
			ptr->side		= x;

			ptr->last_vmag			= sqrt(vx*vx + vy*vy + vz*vz) + VERYSMALL_OFFSET;
			vxy					= sqrt(vx*vx + vy*vy) + VERYSMALL_OFFSET;
			ptr->last_azimuth		= RADIAN2DEGREE(atan2(vx, vy));
			ptr->last_incline		= RADIAN2DEGREE(atan2(vz, vxy));

			ptr->last_sidespin		=  RADIAN2RPM(ptraj->current_w * ptraj->r[2]);	// rz
#if defined(SIDESPIN_PLUS_SLICE__MINUS_HOOK)			// 2010/11/09
			ptr->last_sidespin		*= -1;
#endif

			ptr->last_backspin		=  RADIAN2RPM(ptraj->current_w * ptraj->r[0]);  // rx
			ptr->flighttime		= ptraj->t;

			flagu = 1;
		}

		if (ptraj->q[5] < TRAJ_UNDER_ZERO) {
			break;
		}
	}

	ptr->count = i-1;

	ptd = ptdORG;
}


/*!
 ********************************************************************************
 *	@brief      4th order RungeKutta method for trajectory
 *	@param[in]  htraj		handle of trajectory object (for RK)
 *	@param[in]  timedelta	time resolution for trajectory (sec)
 *
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2009/11/08
 *******************************************************************************/
//static void traj_rungekutta4(HAND htraj, double timedelta, I32 category)
static void traj_rungekutta4_2(HAND htraj, double timedelta, I32 category, double spin_halflife, double fm_mult, double fmy_positive_mult)
{
	iana_traj_t	*ptraj; 
	U32 i;
	double t;
	double td;
	double q0[IANA_TRAJ_RK_EQ_NUM];				
	double q1[IANA_TRAJ_RK_EQ_NUM];				
	double q2[IANA_TRAJ_RK_EQ_NUM];				
	double q3[IANA_TRAJ_RK_EQ_NUM];				
	double q4[IANA_TRAJ_RK_EQ_NUM];				

	//---------------------------------------------
	// set initial values
	ptraj = (iana_traj_t *) htraj; 


	//---------------------------------------------
	//  Retrieve the current values of the dependent
	//  and independent variables.
	t = ptraj->t;
	td = timedelta;
	memcpy(&q0[0], &ptraj->q[0], sizeof(double) * IANA_TRAJ_RK_EQ_NUM);

	//---------------------------------------------
	// Compute the four Runge-Kutta steps, The return 
	// value of projectileRightHandSide method is an array
	// of delta-q values for each of the four steps.
//	traj_RK_RightHandSide(ptraj, q0, q0, td, 0.0, q1, t, category);
//	traj_RK_RightHandSide(ptraj, q0, q1, td, 0.5, q2, t, category);
//	traj_RK_RightHandSide(ptraj, q0, q2, td, 0.5, q3, t, category);
//	traj_RK_RightHandSide(ptraj, q0, q3, td, 1.0, q4, t, category);

	traj_RK_RightHandSide_2(ptraj, q0, q0, td, 0.0, q1, t, category, spin_halflife, fm_mult, fmy_positive_mult);
	traj_RK_RightHandSide_2(ptraj, q0, q1, td, 0.5, q2, t, category, spin_halflife, fm_mult, fmy_positive_mult);
	traj_RK_RightHandSide_2(ptraj, q0, q2, td, 0.5, q3, t, category, spin_halflife, fm_mult, fmy_positive_mult);
	traj_RK_RightHandSide_2(ptraj, q0, q3, td, 1.0, q4, t, category, spin_halflife, fm_mult, fmy_positive_mult);

	//---------------------------------------------
	//  Update the dependent and independent variable values
	//  at the new dependent variable location and store the
	//  values in the ODE object arrays.
	//cr_trace(CR_MSG_AREA_SENSOR, CR_MSG_LEVEL_WARN, "t: %lf, td: %lf\n ", ptraj->t, td);
	ptraj->t = ptraj->t + td;

	for(i = 0; i < IANA_TRAJ_RK_EQ_NUM; ++i) {
		q0[i] = q0[i] + (q1[i] + 2.0*q2[i] + 2.0*q3[i] + q4[i])/6.0;
		ptraj->q[i] = q0[i];
	}     
}



/*!
 ********************************************************************************
 *	@brief      Right-Hand sides for Trajectory
 *	@param[in]  htraj		handle of trajectory object (for RK)
 *	@param[in]  timedelta	time resolution for trajectory (sec)
 *
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2009/11/08
 *******************************************************************************/

//*************************************************************
//  This method loads the right-hand sides for the spring ODEs
//*************************************************************
static void traj_RK_RightHandSide_2(HAND htraj,
                             double *q, 
							 double *deltaQ, 
							 double td, 
                             double qScale, 
							 double *dq,
							 double t,
							 I32 category
							 ,double spin_halflife, double fm_mult, double fmy_positive_mult)
{
	
	//  q[0] = vx = dx/dt
	//  q[1] = x
	//  q[2] = vy = dy/dt
	//  q[3] = y
	//  q[4] = vz = dz/dt
	//  q[5] = z
	iana_traj_t	*ptraj = (iana_traj_t	*) htraj;
	//iana_traj_basic_t *ptrajb;

	double newQ[6]; // intermediate dependent variable values.
	double mass;
	double area;
	double density;
	double Cd;
	double vx;
	double vy;
	double vz;
	double v;
	double Fd;
	double Fdx;
	double Fdy;
	double Fdz;
	double vax;
	double vay;
	double vaz;
	double va;
	double windVx;
	double windVy;
	double rx;     
	double ry;     
	double rz;     
	double omega;  
	double current_omega;  
	double radius; 
	double Cl;
	double Fm;
	double Fmx;
	double Fmy;
	double Fmz;

	double G = -9.81;

	double Sp;			// Dimensionless value for Cd and Cl. 2009/11/08, yhsuk
	double Sp2, Sp3, Sp4;

	double Re;			// Reynolds number


//	double spin_halflife;
//	double fm_mult;
//	double fmy_positive_mult;

	int i;

	int usetheta;
	double theta;
	double vax2 = 0.0, 	vay2 = 0.0;
	double vx2 = 0.0, 	vy2 = 0.0;

	double fmxTrate;
	//-------------------------------------------------

	mass		= ptraj->trajb.ballmass;
	area		= ptraj->trajb.ballarea;
	density		= ptraj->trajb.airdensity;
	/*
	Cd			= ptraj->Cd;
	*/
	windVx		= ptraj->windx;
	windVy		= ptraj->windy;
	rx			= ptraj->r[0];
	ry			= ptraj->r[1];
	rz			= ptraj->r[2];
	omega		= ptraj->w;
	radius		= ptraj->trajb.ballradius;

	//--------------------------------------------------
	//	2009/11/08, yhsuk
#define SPIN_HALFLIFE		(20.0)
//#define SPIN_HALFLIFE		(10.0)
#define SPINMAG_CALC

#if defined(SPINMAG_CALC)
	current_omega = iana_traj_spinmag_calc(htraj, ptraj->t);
#else
	current_omega = omega * pow(2.0, -(ptraj->t / spin_halflife));
#endif



	ptraj->current_w = current_omega;
	
	//  Compute the intermediate values of the 
	//  dependent variables.
	for(i=0; i<6; ++i) {
		newQ[i] = q[i] + qScale*deltaQ[i];
	}

	//  Declare some convenience variables representing
	//  the intermediate values of velocity.
	vx = newQ[0];
	vy = newQ[2];
	vz = newQ[4];

	//  Compute the apparent velocities by subtracting
	//  the wind velocity components from the projectile
	//  velocity components.
	vax = vx - windVx;
	vay = vy - windVy;
	vaz = vz;

	//  Compute the apparent velocity magnitude. The 1.0e-10 term
	//  ensures there won't be a divide by zero later on
	//  if all of the velocity components are zero.
	va = sqrt(vax*vax + vay*vay + vaz*vaz) + VERYSMALL_OFFSET;



	//-------
    Sp = current_omega * radius / va;   // 2009/11/02, yhsuk

//#define SP_MULT		1.2
//#define SP_MULT		1.5
//#define SP_MULT		1.3
#if defined(SP_MULT)
    Sp *= SP_MULT;
#endif

//#define CR_IP_MAX_SP	(0.95)
//#define CR_IP_MAX_SP	(1.1)
#define CR_IP_MAX_SP	2

#if defined (CR_IP_MAX_SP)
	if (Sp > CR_IP_MAX_SP) {
		Sp = CR_IP_MAX_SP;
	}
#endif
//	cr_trace(CR_MSG_AREA_RECOG, CR_MSG_LEVEL_WARN, "----> Sp: %lf\n", Sp);
//	cr_trace(CR_MSG_AREA_RECOG, CR_MSG_LEVEL_TOP, "----> Sp: %lf\n", Sp);
	Sp2 = Sp * Sp;
	Sp3 = Sp * Sp2;
	Sp4 = Sp2 * Sp2;

	Cd = 0.7510 * Sp4 - 1.760 * Sp3 + 1.098 * Sp2 + 0.2148 * Sp + 0.2049;	

	Re = 0;
	//  Compute the total drag force.
	Fd = 0.5*density*area*Cd*va*va;



	Fdx = -Fd*vax/va;
	Fdy = -Fd*vay/va;
	Fdz = -Fd*vaz/va;

	//  Compute the velocity magnitude
	v = sqrt(vx*vx + vy*vy + vz*vz) + 1.0e-8;

	//  Evaluate the Magnus force terms.
//	Cl = radius*omega/v;
	Cl = -0.2158 * Sp4 + 1.006 * Sp3 -1.644 * Sp2 + 1.250 * Sp + 0.0616;

	Fm = 0.5*density*area*Cl*va*va;

#define VERYSMALL_V		(1.0e-3)
	theta = 0.0;
	if (
			//theta > 10.0 && 
			( vay > VERYSMALL_V || vay < - VERYSMALL_V)
	   ) {
	//	double theta;
	//	double fmxx, fmxy;
		vx2 = 0.0;
		vy2 = 0.0;
		vax2 = 0.0;
		vay2 = 0.0;

		theta = atan(vx / vy);
		usetheta = 1;

		vx2 = vx;
		vy2 = vy;

		vx = cos(-theta) * vx2  + sin(-theta) * vy2;
		vy = -sin(-theta) * vx2 + cos(-theta) * vy2;
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "theta: %lf(%lf), vaxy: (%lf, %lf) -> (%lf, %lf)\n",
//				theta,
//			RADIAN2DEGREE(theta),
//				vax2, vay2,
//				vax, vay);

	} else {
//		printf("vay: %lf\n", vay);
		usetheta = 0;
	}


	{
		double rz1 = rz;
		Fmx =  -(vy*rz1 - ry*vz)*Fm/v;
		Fmy =  -(vz*rx - rz1*vx)*Fm/v;
		Fmz =  -(vx*ry - rx*vy)*Fm/v;
	}


	//-----------------    2015/0405
#if 1
	//fmxTrate = calcfmxTrate(t);
	fmxTrate = 1.0;
	Fmx *= fmxTrate;
#endif
	//----------------------------------------------------------------

	if (Fmy > 0) {
		Fmy *= fmy_positive_mult;
	} else {
//		printf("Fmy ---\n");
	}

	if (vaz < 0.0) {			// 
		double maxz;
		double nz;
		double nz_;
		double fmxmult;

		maxz = ptraj->trajb.maxz;
#define MMMZ	1.0
		if (maxz > MMMZ) {
			nz = ptraj->q[5] / maxz;
			nz_ = nz;
			if (nz > 1.0) {
				nz = 1.0;
			}
			if (nz < 0.0) {
				nz = 0.0;
			}
			fmxmult = LINEAR_SCALE(nz, 0.0, 2.0, 1.0, 1.0);
//			Fmx *= fmxmult;

//			printf("nz: %lf, nz_: %lf, maxz: %lf, q5: %lf, fmxmult: %lf, Fmx: %lf\n",
//					nz, nz_,
//					maxz, ptraj->q[5],
//					fmxmult, Fmx
//					);

		}
//		printf("vaz---\n");




	} else {
		ptraj->trajb.maxz = ptraj->q[5];			// z
	}
//	Fmx *= fm_mult;
//	Fmx *= fm_mult;								// Sidespin: NO mult.. 2015/0405
	Fmy *= fm_mult;
	Fmz *= fm_mult;

	if (usetheta) {
		double fmx_, fmy_; 
		double th;
		double vx_, vy_;
		fmx_ = Fmx;
		fmy_ = Fmy;

		Fmx = cos(theta) * fmx_  + sin(theta) * fmy_;
		Fmy = -sin(theta) * fmx_ + cos(theta) * fmy_;


		vx_ = vx;
		vy_ = vy;

		vx =  cos(theta) * vx_  + sin(theta) * vy_;
		vy = -sin(theta) * vx_  + cos(theta) * vy_;

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Fmxy: (%lf, %lf) -> (%lf, %lf)\n",
		//		fmx_, fmy_, Fmx, Fmy);
		th = atan(fmy_/fmx_);
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "theta: %lf(%lf)\n", th, RADIAN2DEGREE(th));
	}
	//  Compute right-hand side values.
	dq[0] = td*(Fdx + Fmx)/mass;
	dq[1] = td*vx;
	dq[2] = td*(Fdy + Fmy)/mass;
	dq[3] = td*vy;
	dq[4] = td*(G + (Fdz + Fmz)/mass);
	dq[5] = td*vz;


	UNUSED(	spin_halflife);
	UNUSED(	fmy_positive_mult);
	UNUSED(	t );
	UNUSED(	category);

	return;
}



//-----------------------
#if 1			// 20190722

static const double w3c[20] = {
	0.00015159797503054286, 
	-2.953424087093212e-05, -2.3028396725348174e-05, -1.235678229737904e-07, 
	5.687183462056416e-07, 6.729147022167669e-07, 1.0682255809543922e-09, -1.981159464538998e-09, -4.610844416897642e-11, 1.3819983178422839e-11, 
	-3.7729309113847014e-09, -4.042482748103242e-09, -5.407209364288176e-12, 
	-6.771939349569807e-10, 1.6197781838620879e-12, -4.2641813994715064e-14, 
	-2.558812310818973e-10, -1.6484728020073268e-12, 2.655864354231008e-15, -5.549213560855302e-16};

static const double w2c[20] = {
	-0.006531151968132984, 
	0.00036688460379707684, 9.561632946599908e-05, 9.12826681314246e-07, 
	-5.522883140619469e-06, 3.7950980343072474e-07, 8.409769924708697e-09, -2.332144776990428e-07, -4.815055831044552e-09, -1.3315117832401155e-10, 
	3.604577850148617e-08, -2.004760355175773e-08, -1.8296172923189196e-11, 
	6.1147542967000075e-09, -6.378450101509928e-11, -3.371619519558528e-13, 
	3.084060069335135e-09, 1.5718887115379183e-11, 3.8226917464476745e-13, 6.130791084688886e-15};

static const double w1c[20] = {
	0.004535734160165342, 
	-0.0010862641262107463, -8.003228518755945e-05, -9.484123654045178e-07, 
	1.1096722491490399e-06, -1.8007068581437894e-06, 1.009719266341997e-08, 2.607194118550543e-06, 1.2429038156919096e-08, 8.301786420125014e-11, 
	-4.513516259828337e-09, 6.162577828314034e-08, 2.9306012569926474e-10, 
	-2.1724658390643305e-08, 1.3826738082547896e-10, -1.8385725176162653e-12, 
	-2.295318651426035e-08, -5.557292024825292e-11, -9.085779827064427e-13, -3.499191776081719e-16};

static const double w0c[20] = {
	-0.0023636879720684966, 
	-3.689568908555197e-05, 4.869417321336948e-05, 1.0282310004877904e-06, 
	1.8741986124628368e-06, 1.8212860116171194e-06, -4.350671841514615e-09, -1.6701269333075438e-06, -7.721939359456477e-09, -1.2027327482941054e-10, 
	-1.2864222753951497e-08, -4.125641926668278e-08, -1.2525391912830667e-10, 
	3.380518572963899e-09, -7.606172025542078e-11, 9.118047272940405e-13, 
	1.4299755631623279e-08, 3.982685114047399e-11, 5.362918443100824e-13, 3.503287665034061e-15};
#endif
//----------
static int calcw(double vmag, double incline, double spinmag, double wc[4])
{
	int i;
	double x, y, z;
	double xx,xy,xz,yy,yz,zz;

	double xxx, xxy, xxz;
	double xyy, xyz, xzz;
	double yyy, yyz, yzz, zzz;

	const double *pwc;

	
	double d0, d1, d2;
	double d30, d31, d32;
	
	//---

	x 	= vmag;
	y 	= incline;
	z 	= spinmag;

	xx 	= x*x;
	yy 	= y*y;
	zz 	= z*z;

	xx 	= x*x;
	xy 	= x*y;
	xz 	= x*z;
	yy 	= y*y;
	yz 	= y*z;
	zz 	= z*z;
	xxx = x*x*x;
	xxy = x*x*y;
	xxz = x*x*z;
	xyy = x*y*y;
	xyz = x*y*z;
	xzz = x*z*z;
	yyy = y*y*y;
	yyz = y*y*z;
	yzz = y*z*z;
	zzz = z*z*z;

	for (i = 0; i < 4; i++) {
		if (i == 0) { 			pwc = &w0c[0];
		} else if (i == 1) { 	pwc = &w1c[0];
		} else if (i == 2) { 	pwc = &w2c[0];
		} else if (i == 3) { 	pwc = &w3c[0];
		} else { break; }


		d0 = pwc[0];			// 1
		d1 = pwc[1] * x + pwc[2] * y + pwc[3] * z;				// x y z
		d2 = pwc[4] * xx + pwc[5] * xy + pwc[6] * xz + pwc[7] * yy + pwc[8] * yz + pwc[9] * zz;	// xx xy xz yy yz zz
		d30 = pwc[10] * xxx + pwc[11] * xxy + pwc[12] * xxz;					// xxx xxy xxz
		d31 = pwc[13] * xyy + pwc[14] * xyz + pwc[15] * xzz;					// xyy xyz xzz
		d32 = pwc[16] * yyy + pwc[17] * yyz + pwc[18] * yzz + pwc[19] * zzz;	// yyy yyz yzz zzz

		wc[i] = d0 + d1 + d2 + d30 + d31 + d32;
	}
	// //	vmag 60, inc 12, wmag 3000
	//wc[3] = -0.0002;
	//wc[2] = 0.0029;
	//wc[1] = -0.0237;
	//wc[0] = 0;


	/*
	 //	vmag 50, inc 12, wmag 3000
	//y = -0.0002x3 + 0.0025x2 - 0.0201x + 3.477
	wc[3] = -0.0002;
	wc[2] = 0.0025;
	wc[1] = -0.0201;
	wc[0] = 0;
	*/
	
	return 1;
}



/*!
 ********************************************************************************
 *	@brief      get spin velocity magnitude [rad/s]
 *	@param[in]  htraj	handle of trajectory object
 *	@param[in]  t		current time
 *	@return		w mag.
 *
 *	@author	    yhsuk
 *  @date       2019/0719
 *******************************************************************************/
double iana_traj_spinmag_calc(HAND htraj, double t)
{
	double wmag;
	iana_traj_t	*ptraj;

	//-------------------------------------------------------------
	ptraj = (iana_traj_t	*) htraj;
	wmag = 	ptraj->wc[3] * t*t*t + 
			ptraj->wc[2] * t*t +
			ptraj->wc[1] * t + 
			ptraj->wc[0];

//	wmag = pow(10.0, wmag);
	wmag = exp(wmag);
	wmag = wmag * ptraj->w;

	return wmag;
}


#if defined (__cplusplus)
}
#endif

