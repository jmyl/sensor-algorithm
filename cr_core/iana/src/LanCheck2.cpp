#if defined(_WIN32)

#define _WINSOCK_DEPRECATED_NO_WARNINGS
//#include <winsock2.h>
//#include <iphlpapi.h>
//#include <icmpapi.h>
#endif
#include "cr_common.h"
#include "LanCheck2.h"
#include <stdio.h>
#include <string.h>
#include "cr_osapi.h"

#define MAX_LAN_ERR	3

#if defined(_WIN32)
#pragma comment(lib, "iphlpapi.lib")
#pragma comment(lib, "ws2_32.lib")
#endif



CLanCheck2::CLanCheck2(void)
{
	m_pThread = NULL;
	for (int i=0;i<MAX_CHECK_IP;i++)
	{
		m_bIsConnect[i] = CR_FALSE;
		m_iLanSpeed[i] = 0;
		m_bCheck[i] = CR_FALSE;
		m_nErrCnt[i] = 0;
	}
	m_iEnd = 0;

	//m_nErrCnt = 0;
}

CLanCheck2::~CLanCheck2(void)
{
	stop();
}

CR_BOOL	CLanCheck2::setCheckIP(int index, char *strIP)
{
	if (index < MAX_CHECK_IP)
	{
		strcpy(&m_strIP[index][0], strIP);
		//m_strIP[index] = strIP;
		m_bCheck[index] = CR_TRUE;
		return CR_TRUE;
	}
	return CR_FALSE;
}

char* CLanCheck2::getCheckIP(int index)
{
	char *strIp;

	strIp = NULL;
	if (index < MAX_CHECK_IP)
	{
		if (m_bCheck[index]) {
			strIp = m_strIP[index];
		}
	}
	return strIp;
}

CR_BOOL	CLanCheck2::isrun(int index)
{
	CR_BOOL res;

	res = CR_FALSE;
	if (index < MAX_CHECK_IP)
	{
		if (m_bCheck[index]) {
			res = CR_TRUE;
		}
	}
	return res;
}

CR_BOOL	CLanCheck2::getLanInfo(int index, int *pLan, int *pSpeed)
{
	if (index < MAX_CHECK_IP)
	{
		*pLan = m_bIsConnect[index];
		*pSpeed = m_iLanSpeed[index];
		return CR_TRUE;
	}
	return CR_FALSE;
}

CR_BOOL CLanCheck2::removeCheckIP(int index)
{

	if (index < MAX_CHECK_IP)
	{
		m_bIsConnect[index] = CR_FALSE;
		m_iLanSpeed[index] = 0;
		m_bCheck[index] = CR_FALSE;
		m_nErrCnt[index] = 0;
	}
	return CR_FALSE;
}


CR_BOOL CLanCheck2::removeCheckIPAll()
{
	for (int i=0;i<MAX_CHECK_IP;i++)
	{
		m_bIsConnect[i] = CR_FALSE;
		m_iLanSpeed[i] = 0;
		m_bCheck[i] = CR_FALSE;
		m_nErrCnt[i] = 0;
	}
	return CR_TRUE;
}

void CLanCheck2::start()
{
	m_iEnd = 0;
	if (m_pThread == NULL) {
		//m_pThread = AfxBeginThread(checkThread,this);
		m_pThread = cr_thread_create (checkThread, (void *)this);


	}

}

void CLanCheck2::stop()
{
	int nCnt;

	nCnt = 100;
	if (m_pThread != NULL)
	{
		m_iEnd = 1;
		while((m_iEnd != 2) && (nCnt > 0))
		{
			cr_sleep(10);
			nCnt--;
		}
		if (m_iEnd != 2)
		{
			OSAL_THREAD_Delete(m_pThread);
		}

		cr_sleep(100);
	}
	//m_iEnd = 0;
	m_pThread = NULL;
}

void* CLanCheck2::checkThread(void *pParam)
{
	int nRet;
	CLanCheck2 *pLanchk = (CLanCheck2 *)pParam;
	while (pLanchk->m_iEnd == 0)
	{
		for (int i=0;i<MAX_CHECK_IP; i++)
		{
			if (pLanchk->m_bCheck[i] == CR_TRUE)
			{
				nRet = isConnectLan(pLanchk->m_strIP[i]);
				if (nRet == 1)
				{
					pLanchk->m_bIsConnect[i] = nRet;
					pLanchk->m_iLanSpeed[i] = getLanSpeed(pLanchk->m_strIP[i]);
					pLanchk->m_nErrCnt[i] = 0;
				}
				else
				{
					pLanchk->m_nErrCnt[i]++;
					if (pLanchk->m_nErrCnt[i] > MAX_LAN_ERR)
					{
						pLanchk->m_bIsConnect[i] = 0;
						pLanchk->m_iLanSpeed[i] = -1;

						pLanchk->m_nErrCnt[i] = MAX_LAN_ERR+1;
					}

				}
				cr_sleep(10);
			}
		}
		if (pLanchk->m_iEnd == 0)
			cr_sleep(500);
	}

	pLanchk->m_iEnd = 2;

	return 0;
}

int	CLanCheck2::isConnectLan(char *strIP)
{
#if defined(_WIN32)
	CR_BOOL	bConnect;
	HANDLE hIcmpFile;
	unsigned long ipaddr = INADDR_NONE;
	DWORD dwRetVal = 0;
	char SendData[32] = "Data Buffer";
	LPVOID ReplyBuffer = NULL;
	DWORD ReplySize = 0;

	// Validate the parameters


	//ipaddr = inet_addr(CHECK_IP);
//	USES_CONVERSION;
	ipaddr = inet_addr(strIP);	
	hIcmpFile = IcmpCreateFile();
	if (hIcmpFile == INVALID_HANDLE_VALUE) {
		printf("\tUnable to open handle.\n");
		printf("IcmpCreatefile returned error: %ld\n", GetLastError() );
		return 0;
	}    

	ReplySize = sizeof(ICMP_ECHO_REPLY) + sizeof(SendData);
	ReplyBuffer = (VOID*) malloc(ReplySize);
	if (ReplyBuffer == NULL) {
		printf("\tUnable to allocate memory\n");
		return 0;
	}    
	/////////////////////////////

	dwRetVal = IcmpSendEcho(hIcmpFile, ipaddr, SendData, sizeof(SendData), NULL, ReplyBuffer, ReplySize, 1000);
	if (dwRetVal != 0) 
	{
		PICMP_ECHO_REPLY pEchoReply = (PICMP_ECHO_REPLY)ReplyBuffer;
		struct in_addr ReplyAddr;
		ReplyAddr.S_un.S_addr = pEchoReply->Address;
		if (dwRetVal > 1) {
		}    
		else {    
		}    

		bConnect = CR_TRUE;
	}
	else {
		bConnect = CR_FALSE;

	}

	free(ReplyBuffer);
	IcmpCloseHandle(hIcmpFile);
	if (bConnect == CR_TRUE)
		return 1;

	return 0;
	
#else
	// TODO: LINUX_PORT
	return 0;
	
#endif
}

int	CLanCheck2::getLanSpeed(char *strIP)
{
#if defined(_WIN32)
	// get the best interface by asking for any internet IP address
	unsigned long dwBestIfIndex = 0;   
	//IPAddr dwDestAddr = (IPAddr)inet_addr(CHECK_IP);   

//	USES_CONVERSION;
	//IPAddr dwDestAddr = (IPAddr)inet_addr(W2A(strIP));   
	IPAddr dwDestAddr = (IPAddr)inet_addr(strIP);   
	if(GetBestInterface(dwDestAddr,&dwBestIfIndex) != NO_ERROR)
	return 0;

	// get information for the interface index found above
	PMIB_IFTABLE pMIFT = NULL;
	MIB_IFROW *pIFR = NULL;
	ULONG cbMIFT = 0;
	GetIfTable(NULL, &cbMIFT, CR_TRUE);
	if(cbMIFT > 0)
	{
		pMIFT = (PMIB_IFTABLE)GlobalAlloc(GMEM_ZEROINIT, cbMIFT);
		if(pMIFT)
		{
			GetIfTable(pMIFT, &cbMIFT, CR_TRUE);
			for(DWORD i = 0; pIFR == NULL && i < pMIFT->dwNumEntries; ++i)
			{
				if(pMIFT->table[i].dwIndex == dwBestIfIndex)
				{
					pIFR = &pMIFT->table[i];
				}
			}
		}
	}

	// get the interface speed if it is at least in the connected state
	DWORD r = pIFR && pIFR->dwOperStatus >= MIB_IF_OPER_STATUS_CONNECTED ? pIFR->dwSpeed : 0;

	if(pMIFT)
	{
		GlobalFree(pMIFT);
		pMIFT = NULL;
	}

	return r;
#else
	// TODO: LINUX_PORT

	return 0;
#endif
}




