/********************************************************************************
                                                                                
                   Creatz Golf sensor 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 by Creatz Inc.
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc. \n

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_traj.c
	 @brief  Golf Trajectory.
	 @author YongHo Suk                                 
	 @date   2011/10/05 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#include <string.h>


#include "cr_common.h"

#define _USE_MATH_DEFINES								// for using M_PI
#include <math.h>

#include "iana_adapt.h"

#include "cr_osapi.h"
#include "cr_interpolation.h"

#include "iana_traj.h"





/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#if defined( _MSC_VER ) && (_MSC_VER <= 1200)    // 1200 = MSVC 6.0.
#define __FUNCTION__
#endif

#if defined(TASUK_CAM)
#define	TRAJTYPE	1

#if defined(RUNGOLF_H600C)
#error USE iana_traj-324.c... -_-;
#undef	TRAJTYPE	
//#define	TRAJTYPE	1
#define	TRAJTYPE	0
#endif

#else
#define TRAJTYPE	0
#endif

#define SIDESPIN_PLUS_SLICE__MINUS_HOOK					// 2010/11/09

#define VISCOSITY					(0.0000182)			// kg / (m sec)

#define CATEGORY_INCLINE_LOWLOW			0x0001
#define CATEGORY_INCLINE_LOWMID			0x0002
#define CATEGORY_INCLINE_LOWHI			0x0003
#define CATEGORY_INCLINE_MIDLOW			0x0004
#define CATEGORY_INCLINE_MIDMID			0x0005
#define CATEGORY_INCLINE_MIDHI			0x0006
#define CATEGORY_INCLINE_HI00			0x0007
#define CATEGORY_INCLINE_HI01			0x0008
#define CATEGORY_INCLINE_HI02			0x0009
#define CATEGORY_INCLINE_HI03			0x000a
#define CATEGORY_INCLINE_HI04			0x000b


#define CATEGORY_INCLINE(x)				((x) & 0x000F)


/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*

             ^               
             |Z               
             |              .
             |          .       .
             |       .            .
             |     .               .
             |   .                  .
             | .                     .
             |------------------------.----->		Y
            /                          .
           /
		  /
		 V  X

*/


/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/
#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/
#define BACKSPIN_V

#if defined(BACKSPIN_V)
static double refine_backspin(double backspin, double vmag);
//static double revive_backspin(double backspin, double vmag);
static double revive_backspin(double backspin);
#else
static double refine_backspin(double backspin);
static double revive_backspin(double backspin);
#endif
#define SIDE_A
#if defined(SIDE_A)
static double refine_sidespin(double sidespin, double azimuth);
static double revive_sidespin(double sidespin, double azimuth);
#else
static double refine_sidespin(double sidespin);
static double revive_sidespin(double sidespin);
#endif


//double calcfmxTrate(double t, double vmag);
double calcfmxTrate(double t);
//static void traj_rungekutta4(HAND htraj, double timedelta, I32 category);
//static void traj_RK_RightHandSide(HAND htraj,
 //                            double *q, 
//							 double *deltaQ, 
//							 double td, 
 //                            double qScale, 
//							 double *dq,
//							 double t,
//							 I32 category
//							 );
static void traj_rungekutta4(HAND htraj, double timedelta, I32 category, double spin_halflife, double fm_mult, double fmy_positive_mult);	
static void traj_RK_RightHandSide(HAND htraj,
                             double *q, 
							 double *deltaQ, 
							 double td, 
                             double qScale, 
							 double *dq,
							 double t,
							 I32 category
							 , double spin_halflife, double fm_mult, double fmy_positive_mult);	



static double s_tr[6];
static double s_fr[6];
static int s_TTT = 1;


/*!
 ********************************************************************************
 *	@brief      create trajectory object
 *  @return		handle of trajectory object
 *
 *	@author	    yhsuk
 *  @date       2009/11/08
 *******************************************************************************/
iana_traj_t *iana_traj_create(void)
{
	iana_traj_t	      *ptraj;
	iana_traj_basic_t *ptrajb;

	ptraj = (iana_traj_t *) malloc (sizeof(iana_traj_t));
	if (ptraj == NULL) {
		goto func_exit;
	}

	memset(ptraj, 0, sizeof(iana_traj_t));

	ptrajb = &ptraj->trajb;


	ptrajb->ballmass = 0.0459;				// Org... 
	ptrajb->ballarea = 0.001432;
	ptrajb->ballradius= (0.0428/2.);

	ptrajb->maxz = 0.0;

#define AIRDENSITY 1.225
	ptrajb->airdensity = AIRDENSITY;
func_exit:
	return ptraj;
}



/*!
 ********************************************************************************
 *	@brief      delete trajectory object
 *	@param[in]  htraj  handle of trajectory object
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2009/11/08
 *******************************************************************************/
void iana_traj_delete(HAND htraj)
{
	if (htraj) {
		free(htraj);
	}
}

/*!
 ********************************************************************************
 *	@brief      get trajectory
 *	@param[in]  htraj		handle of trajectory object
 *	@param[in]  timedelta	time resolution for trajectory (sec)
 *	@param[in]  ptd			input data for trajectory calculation
 *	@param[out]  ptr		result of trajectory calculation
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2009/11/08
 *******************************************************************************/
void iana_traj(		
		HAND htraj,
		double  timedelta,
		iana_traj_data_t *ptd,
		iana_traj_result_t *ptr)
{
	U32		i;
	U32		flagu;
	I32 	category;
	double 	spin_halflife;
	double 	fm_mult;
	double 	fmy_positive_mult;
	iana_traj_t	*ptraj; 
	double maxheight;

	iana_traj_data_t *ptdORG;
	iana_traj_data_t td;

	//------------------------------------
	//
	//------------------------------------
#define LINEAR_SCALE(x,x0,y0,x1,y1) ( ( ((double) (((y1) - (y0)))) / ((double) ((x1) - (x0))) )*(x-x0) + y0 )

//#define G_SENSOR
#if defined(G_SENSOR)			// 20190607.  5th try.. :)
	{
		double vmag, sidespin;
		double vmag0, sidespin0;
		double vmult0, smult0;
		double vmult1;
		double backspin;

		double incline;
		//---
		memcpy(&td, ptd, sizeof(iana_traj_data_t));
		ptdORG = ptd;
		ptd = &td;

		vmag = ptd->vmag;
		sidespin = ptd->sidespin;
		backspin = ptd->backspin;

		incline =  ptd->incline;

		if (vmag < 15) {
			vmult0 = LINEAR_SCALE(vmag, 	 0.0,	1.0,	15.0,	1.0);
		} else if (vmag < 20.0) {
			vmult0 = LINEAR_SCALE(vmag, 	15.0,	1.0,	20.0,	1.0/1.03);
		} else if (vmag < 30.0) {
			vmult0 = LINEAR_SCALE(vmag, 	20.0,	1.0/1.03, 	30.0,	1.0/1.0);
		} else if (vmag < 40.0) {
			if (incline < 10) {
				vmult0 = LINEAR_SCALE(vmag, 	30.0,	1.0/1.0, 	40.0,	1.0/0.99);
			} else if (incline < 12) {
				vmult0 = LINEAR_SCALE(vmag, 	30.0,	1.0/1.0, 	40.0,	1.0/0.98);
			} else if (incline < 16) {
				vmult0 = LINEAR_SCALE(vmag, 	30.0,	1.0/1.0, 	40.0,	1.0/0.96);
			} else {
				vmult0 = LINEAR_SCALE(vmag, 	30.0,	1.0/1.0, 	40.0,	1.0/0.95);
			}
		} else if (vmag < 50.0) {
			if (incline < 10) {
				vmult0 = LINEAR_SCALE(vmag, 	40.0,	1.0/0.99, 	50.0,	1.0/0.97);
			} else if (incline < 12) {
				vmult0 = LINEAR_SCALE(vmag, 	40.0,	1.0/0.98, 	50.0,	1.0/0.95);
			} else if (incline < 16) {
				vmult0 = LINEAR_SCALE(vmag, 	40.0,	1.0/0.96, 	50.0,	1.0/0.95);
			} else {
				vmult0 = LINEAR_SCALE(vmag, 	40.0,	1.0/0.95, 	50.0,	1.0/0.91);
			}
		} else if (vmag < 60.0) {
			if (incline < 10) {
				vmult0 = LINEAR_SCALE(vmag, 	50.0,	1.0/0.97, 	60.0,	1.0/0.93);
			} else if (incline < 12) {
				vmult0 = LINEAR_SCALE(vmag, 	50.0,	1.0/0.95, 	60.0,	1.0/0.93);
			} else if (incline < 16) {
				vmult0 = LINEAR_SCALE(vmag, 	50.0,	1.0/0.95, 	60.0,	1.0/0.92);
			} else {
				vmult0 = LINEAR_SCALE(vmag, 	50.0,	1.0/0.91, 	60.0,	1.0/0.91);
			}
		} else if (vmag < 70.0) {
			if (incline < 10) {
				vmult0 = LINEAR_SCALE(vmag, 	60.0,	1.0/0.93, 	70.0,	1.0/0.93);
			} else if (incline < 12) {
				vmult0 = LINEAR_SCALE(vmag, 	60.0,	1.0/0.93, 	70.0,	1.0/0.93);
			} else if (incline < 16) {
				vmult0 = LINEAR_SCALE(vmag, 	60.0,	1.0/0.92, 	70.0,	1.0/0.93);
			} else {
				vmult0 = LINEAR_SCALE(vmag, 	60.0,	1.0/0.91, 	70.0,	1.0/0.93);
			}
		} else {
			vmult0 = 1.0/0.93;
		}


		if (backspin < 500) {
			vmult1 = 1/0.97;
		} else if (backspin < 1000) {
			vmult1 = LINEAR_SCALE(backspin, 	 500.0, (1/0.98), 1000.0, (1/0.96));
		} else if (backspin < 2000) {
			vmult1 = LINEAR_SCALE(backspin, 	1000.0, (1/0.96), 2000.0, (1/0.98));
		} else if (backspin < 3000) {
			vmult1 = LINEAR_SCALE(backspin, 	2000.0, (1/0.98), 3000.0, 1.0);
		} else {
			vmult1 = 1.0;
		}



		vmult0 = vmult1 * vmult0;

		//--

		vmult0 = sqrt(vmult0);
		vmag0 = vmag*vmult0;

		smult0 = 1.5;
		sidespin0 = sidespin * smult0;

		ptd->vmag 		= vmag0;
		ptd->sidespin 	= (int)sidespin0;
	}
#else
	UNUSED(ptdORG);
	UNUSED(td);
#endif

#define TRAJMULT
#if defined(TRAJMULT)			// 20190702
	{
		double vmag;
		double vmult;
		
		//---
		memcpy(&td, ptd, sizeof(iana_traj_data_t));
		ptdORG = ptd;
		ptd = &td;

		vmag = ptd->vmag;

		//vmult = 1.0;
		vmult = 1.03;		// 20190711. 
		{
			FILE *fp;
			int mode;
			fp = cr_fopen("trajmult.txt", "r");
			if (fp) {
				fscanf(fp, "%d", &mode);
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

				if (mode == 1) {
					fscanf(fp, "%lf", &vmult);
				}
				cr_fclose(fp);
			}
		}

		{	// after V1.3.8.  20190705
			double vmult0;

			if (vmag < 65.0) {
				vmult0 = 1.0;
			} else if (vmag < 70.0) {
				vmult0 = LINEAR_SCALE(vmag, 65.0, 1.0, 70.0, 1.02);
			} else {
				vmult0 = 1.02;
			}
			vmult = vmult * vmult0;
		}

		ptd->vmag 		= vmag * vmult;
	}
#else
	ptdORG;
	td;
#endif


	//------------------------------------
	// Check Parameter and..
#define INCLINE_MIN	2.
#define INCLINE_MAX 70.
#define AZIMUTH_MIN -60
#define AZIMUTH_MAX 60.
#define SIDESPING_MIN -8485
#define SIDESPING_MAX  8431
#define BACKSPING_MIN	0.
//#define BACKSPING_MAX  12378.
#define BACKSPING_MAX  8000

#define VMAX_MIN 1.
#define VMAX_MAX 200.

	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Traj\n ");
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
				"vmag = %lf,  azimuth = %lf, incline = %lf, backspin = %lf, sidespin = %lf, windmag = %lf, windangle = %lf \n"
				, ptd->vmag
				, ptd->azimuth
				, ptd->incline
				, ptd->backspin
				, ptd->sidespin
				, ptd->windmag
				, ptd->windangle
				);	
		if (ptd->vmag == ptd->vmag) {
			if (ptd->vmag < VMAX_MIN) {		// 
				ptd->vmag = VMAX_MIN;
			}
			if (ptd->vmag > VMAX_MAX) {
				ptd->vmag = VMAX_MAX;
			}
		} else {
			ptd->vmag = 0.;
		}

		if (ptd->incline == ptd->incline) {
			if (ptd->incline < INCLINE_MIN) {		// 
				ptd->incline = INCLINE_MIN;
			}
			if (ptd->incline > INCLINE_MAX) {
				ptd->incline = INCLINE_MAX;
			}
		} else {
			ptd->incline = 0.;
		}


		if (ptd->azimuth == ptd->azimuth) {
			if (ptd->azimuth < AZIMUTH_MIN) {		// 
				ptd->azimuth = AZIMUTH_MIN;
			}
			if (ptd->azimuth > AZIMUTH_MAX) {
				ptd->azimuth = AZIMUTH_MAX;
			}
		} else {
			ptd->azimuth = 0.;
		}


		if (ptd->sidespin == ptd->sidespin) {
			if (ptd->sidespin < SIDESPING_MIN) {		// 
				ptd->sidespin = SIDESPING_MIN;
			}
			if (ptd->sidespin > SIDESPING_MAX) {
				ptd->sidespin = SIDESPING_MAX;
			}
		} else {
			ptd->sidespin = 0.;
		}


		if (ptd->backspin == ptd->backspin) {
			if (ptd->backspin < BACKSPING_MIN) {		// 
				ptd->backspin = BACKSPING_MIN;
			}
			if (ptd->backspin > BACKSPING_MAX) {
				ptd->backspin = BACKSPING_MAX;
			}
		} else {
			ptd->backspin = 0.;
		}
	}


	//---------------------------------------------
	// set initial values
	ptraj = (iana_traj_t *) htraj; 
	{
		double wx, wy, wz;

		double vx;
		double vy;
		double vz;

		double w;
		double rx;
		double ry;
		double rz;

		double windx;
		double windy;
		double vmag1;


		//-------------------------------------------------
		// convert velocity to vector form
		vmag1 = ptd->vmag;

		{
			double inc, azi;
			double alpha;
			double lxy, lx, ly, lz;

		

			inc = DEGREE2RADIAN(ptd->incline);
			azi = DEGREE2RADIAN(ptd->azimuth);

//#define LINEAR_SCALE(x,x0,y0,x1,y1) ( ( ((double) (((y1) - (y0)))) / ((double) ((x1) - (x0))) )*(x-x0) + y0 )
#define SGPG_PRO_0317
#if defined(SGPG_PRO_0317)
			{
				double vmult;
				double backspin;

				vmult = 1.0;

				backspin = ptd->backspin;
				{ 				// if (ptd->incline > 28.0-0.1 && ptd->incline < 45.0) 
#if 1
					if (vmag1 < 10.0) {
						//vmult = 1.0;
						vmult = 1.05;
					} else if (vmag1 < 20.0) {
						//vmult = LINEAR_SCALE(vmag1, 10.0, 1.0, 20.0, (1.0/1.01));
						vmult = LINEAR_SCALE(vmag1, 10.0, 1.05, 20.0, 1.05);
					} else if (vmag1 < 30.0) {
						vmult = LINEAR_SCALE(vmag1, 20.0, 1.05, 30.0, 1.00);
					} else if (vmag1 < 35.0) {
						vmult = LINEAR_SCALE(vmag1, 30.0, 1.00, 35, 1.00);
					} else if (vmag1 < 40.0) {
						vmult = LINEAR_SCALE(vmag1, 35.0, 1.00, 40, 1.00);
					} else if (vmag1 < 45.0) {
						vmult = LINEAR_SCALE(vmag1, 40.0, 1.00, 45, 1.00);
					} else if (vmag1 < 50.0) {
						vmult = LINEAR_SCALE(vmag1, 45.0, 1.00, 50, 0.98);
					} else if (vmag1 < 55.0) {
						vmult = LINEAR_SCALE(vmag1, 50.0, 0.98, 55.0, 0.98);
					} else if (vmag1 < 58.0) {
						vmult = LINEAR_SCALE(vmag1, 55.0, 0.98, 58.0, 0.98);
					} else if (vmag1 < 60.0) {
						vmult = LINEAR_SCALE(vmag1, 58.0, 0.98, 60.0, 0.99);
					} else if (vmag1 < 65.0) {
						vmult = LINEAR_SCALE(vmag1, 60.0, 0.99, 65.0, 1.00);
					} else {
						vmult = 1.00;
					}
#endif
					//---- 2015/0910
					{
						double vvmm2;
						interpol2d_t lip2;
						interpol_t   *plip;

						{ 				// vvmm2 = linearip2d(vmag1, ptd->incline, &lip2);
							//lip2.m = 6;
							//lip2.m = 9;			// -> 20151013
							lip2.m = 10;			// -> 20151203
							{
								lip2.z[0] = 12.0;
								plip = &lip2.lip[0];
								plip->n = 4;
								plip->xi[0] = 20.0; plip->yi[0] =  1.0;
								plip->xi[1] = 30.0; plip->yi[1] =  1.025;
								plip->xi[2] = 34.0; plip->yi[2] =  1.04;
								plip->xi[3] = 40.0; plip->yi[3] =  1.01;
								plip->xi[4] = 50.0; plip->yi[4] =  1.00;
							}

							{
								lip2.z[1] = 14.0;
								plip = &lip2.lip[1];
								plip->n = 4;
								plip->xi[0] = 20.0; plip->yi[0] =  1.0;
								plip->xi[1] = 30.0; plip->yi[1] =  1.025;
								plip->xi[2] = 35.0; plip->yi[2] =  1.03;
								plip->xi[3] = 40.0; plip->yi[3] =  1.01;
								plip->xi[4] = 50.0; plip->yi[4] =  1.00;
							}
							{
								lip2.z[2] = 16.0;
								plip = &lip2.lip[2];
								plip->n = 4;
								plip->xi[0] = 20.0; plip->yi[0] =  1.0;
								plip->xi[1] = 30.0; plip->yi[1] =  1.01;
								plip->xi[2] = 35.0; plip->yi[2] =  1.03;
								plip->xi[3] = 40.0; plip->yi[3] =  1.01;
								plip->xi[4] = 45.0; plip->yi[4] =  1.00;
							}
							{
								lip2.z[3] = 18.0;
								plip = &lip2.lip[3];
								plip->n = 4;
								plip->xi[0] = 20.0; plip->yi[0] =  1.0;
								plip->xi[1] = 30.0; plip->yi[1] =  1.01;
								plip->xi[2] = 35.0; plip->yi[2] =  1.025;
								plip->xi[3] = 40.0; plip->yi[3] =  1.01;
								plip->xi[4] = 45.0; plip->yi[4] =  1.00;
							}
							{
								lip2.z[4] = 25.0;
								plip = &lip2.lip[4];
								plip->n = 3;
								plip->xi[0] = 20.0; plip->yi[0] =  1.0;
								plip->xi[1] = 40.0; plip->yi[1] =  1.02;
								plip->xi[2] = 45.0; plip->yi[2] =  1.02;
								plip->xi[3] = 50.0; plip->yi[3] =  1.0;
							}

							{
								lip2.z[5] = 30.0;
								plip = &lip2.lip[5];
								plip->n = 3;
								plip->xi[0] = 20.0; plip->yi[0] =  1.0;
								plip->xi[1] = 40.0; plip->yi[1] =  1.01;
								plip->xi[2] = 45.0; plip->yi[2] =  1.01;
								plip->xi[3] = 50.0; plip->yi[3] =  1.0;
							}

							{
								lip2.z[6] = 35.0;
								plip = &lip2.lip[6];
								plip->n = 1;
								plip->xi[0] = 20.0; plip->yi[0] =  1.0;
							}

							{
								lip2.z[7] = 50.0;
								plip = &lip2.lip[7];
								plip->n = 1;
								plip->xi[0] = 20.0; plip->yi[0] =  1.0;
							}

							{
								lip2.z[8] = 55.0;
								plip = &lip2.lip[8];
								plip->n = 4;
								
								plip->xi[0] =  1.0; plip->yi[0] = 1.0;
								plip->xi[1] =  5.0; plip->yi[1] = 1.0;
								plip->xi[2] =  7.0; plip->yi[2] = 1.0;
								plip->xi[3] = 10.0; plip->yi[3] = 1.0;
								plip->xi[4] = 50.0; plip->yi[4] = 1.0;

							}

							{
								lip2.z[9] = 60.0;
								plip = &lip2.lip[9];
								plip->n = 4;
								plip->xi[0] =  1.0; plip->yi[0] =  1.07;
								plip->xi[1] =  5.0; plip->yi[1] =  1.07;
								plip->xi[2] =  7.0; plip->yi[2] =  1.05;
								plip->xi[3] = 10.0; plip->yi[3] =  1.03;
								plip->xi[4] = 50.0; plip->yi[4] =  1.02;
							}
							{
								lip2.z[10] = 70.0;
								plip = &lip2.lip[10];
								plip->n = 4;
								plip->xi[0] =  1.0; plip->yi[0] =  1.08;
								plip->xi[1] =  5.0; plip->yi[1] =  1.08;
								plip->xi[2] =  7.0; plip->yi[2] =  1.06;
								plip->xi[3] = 10.0; plip->yi[3] =  1.04;
								plip->xi[4] = 50.0; plip->yi[4] =  1.03;
							}


						}
						vvmm2 = linearip2d(vmag1, ptd->incline, &lip2);
						vmult *= vvmm2;

					}
					//-----
				}

				{
					double vm;

					// X: backspin, y: vm, z: vmag1
					interpol2d_t lip2;
					interpol_t   *plip;
					lip2.m = 7;
					{
						lip2.z[0] = 20.0;
						plip = &lip2.lip[0];
						plip->n = 1;
						plip->xi[0] = 0;  	plip->yi[0] =  (1/1.03);
					}

					{
						lip2.z[1] = 30.0;
						plip = &lip2.lip[1];
						plip->n = 1;
						plip->xi[0] = 0;  	plip->yi[0] =  (1/0.995);
					}

					{
						lip2.z[2] = 35.0;
						plip = &lip2.lip[2];
						plip->n = 1;
						plip->xi[0] = 0;  	plip->yi[0] =  1.0;
					}

					{
						lip2.z[3] = 40.0;
						plip = &lip2.lip[3];
						plip->n = 3;
						plip->xi[0] = 3000;  	plip->yi[0] =  1.0/0.995;
						plip->xi[1] = 4000;  	plip->yi[1] =  1.0/1.05;
						plip->xi[2] = 8000;  	plip->yi[2] =  1.0/1.015;
						plip->xi[3] = 9000;  	plip->yi[3] =  1.0;
					}

					{
						lip2.z[4] = 50.0;
						plip = &lip2.lip[4];
						plip->n = 2;
						plip->xi[0] = 3000;  	plip->yi[0] =  1.0/0.99;
						plip->xi[1] = 4000;  	plip->yi[1] =  1.0/1.01;
						plip->xi[2] = 7000;  	plip->yi[2] =  1.0/0.99;
					}

					{
						lip2.z[5] = 55.0;
						plip = &lip2.lip[5];
						plip->n = 3;
						plip->xi[0] = 2000;  	plip->yi[0] =  1.0/0.995;
						plip->xi[1] = 3000;  	plip->yi[1] =  1.0/1.03;
						plip->xi[2] = 4000;  	plip->yi[2] =  1.0/1.03;
						plip->xi[3] = 6000;  	plip->yi[3] =  1.0/0.995;
					}

					{
						lip2.z[6] = 60.0;
						plip = &lip2.lip[6];
						plip->n = 6;
						plip->xi[0] = 2000;  	plip->yi[0] =  1.0;
						plip->xi[1] = 2500;  	plip->yi[1] =  1.0/1.01;
						plip->xi[2] = 3000;  	plip->yi[2] =  1.0/1.02;
						plip->xi[3] = 4000;  	plip->yi[3] =  1.0/0.99;
						plip->xi[4] = 4500;  	plip->yi[4] =  1.0/1.00;
						plip->xi[5] = 5000;  	plip->yi[5] =  1.0/1.00;
						plip->xi[6] = 6000;  	plip->yi[6] =  1.0/1.00;


					}

					{
						lip2.z[7] = 70.0;
						plip = &lip2.lip[7];
						plip->n = 4;

						plip->xi[0] = 2000;  	plip->yi[0] =  1.0;
						plip->xi[1] = 2500;  	plip->yi[1] =  1.0/1.01;
						plip->xi[2] = 3000;  	plip->yi[2] =  1.0/1.02;
						plip->xi[3] = 4000;  	plip->yi[3] =  1.0/1.01;
						plip->xi[4] = 5000;  	plip->yi[4] =  1.0/1.00;
					}

					vm =  linearip2d( ptd->backspin, vmag1, &lip2);
					vmult *= vm;
					//printf("vmag1: %lf, backspin: %lf, vm: %lf\n", vmag1, (double)ptd->backspin, (double)vm);
				}
				vmag1 = vmag1 * vmult;
			}
#endif



			alpha = 1./sqrt(1 + cos(azi) * cos(azi) * tan(inc) * tan(inc));
			//lxy = alpha * ptd->vmag;
			lxy = alpha * vmag1;
			lx = lxy * sin(azi);
			ly = lxy * cos(azi);
			lz = ly * tan(inc);

			vx = lx;
			vy = ly;
			vz = lz;
		}

//-----------------------------------------------------------------
//-----------------------------------------------------------------
		// wind velocity
		windx = ptd->windmag * sin(DEGREE2RADIAN(ptd->windangle));
		windy = ptd->windmag * cos(DEGREE2RADIAN(ptd->windangle));

//#define WINDMAG_MULT		0.7
#define WINDMAG_MULT		1
#if defined(WINDMAG_MULT)
		windx *= WINDMAG_MULT;
		windy *= WINDMAG_MULT;
#endif


#if 0	//  Sidespin/Backspin-based wind effect. 20150829

/**************************************************************
 * AhnHW, 20150829
기본로직은.. 
뒷바람은 백스핀량 감소, 사이드스핀량 감소
맞바람은 백스핀량 증가, 사이드스핀량 증가
슬라이스바람은 슬라이스성이 훅에 비해 더 증가
훅바람은 기본으로 그 성질이 감소

슬라이스바람 +
훅바람 -

------------------------------------------------------------------
뒷바람 백스핀량 감소..사이드스핀량 감소
Backspin = Backspin - 바람크기 m/s * 100 RPM
Sidespin = Sidepin * 0.9
------------------------------------------------------------------
맞바람 백스핀량 증가..사이드스핀량 증가
Backspin = Backspin + 바람크기 m/s * 150 RPM
if (SideSpin > 0)
{
Sidespin = Sidepin + 바람크기 m/s * 15 RPM
}
else
{
Sidespin = SideSpin - 바람크기 m/s * 10 RPM
}
------------------------------------------------------------------
슬라이스 바람
if (SideSpin > 0)
{
Sidespin = Sidepin + 바람크기 m/s * 35 RPM
}
else
{
Sidespin = Sidepin + 바람크기 m/s * 17.5 RPM
}
offline = (바람크기 m/s * 0.003)*Carry
------------------------------------------------------------------
훅바람
if (SideSpin > 0)
{
Sidespin = Sidepin - 바람크기 m/s * 25 RPM
}
else
{
Sidespin = Sidepin - 바람크기 m/s * 12.5 RPM
}
offline = (바람크기 m/s * 0.003)*Carry
------------------------------------------------------------------
**************************************************************/

		{
			double backspin;
			double sidespin;

			double backspin1;
			double sidespin1;

			double bwbm;
			double bwsm;


			double fwbm;
			double fwssm;
			double fwshm;

			double swsm;
			double swhm;

			double hwsm;
			double hwhm;

			//------------
			backspin = backspin1 = ptd->backspin;
			sidespin = sidespin1 = ptd->sidespin;

			bwbm = -100.0;			
			bwsm =  0.9;

			fwbm = 150.0;
			fwssm = 15.0;
			fwshm = -10.0;

			swsm = 35.0;
			swhm = 17.5;

			hwsm = -25.0;
			hwhm = -12.5;

			{

				FILE *fp;
				int mode;
				fp = fopen("windconfig.txt", "r");
				if (fp) {
					fscanf(fp, "%d", &mode);
					fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

					if (mode == 1) {
						fscanf(fp, "%lf", &bwbm);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
						fscanf(fp, "%lf", &bwsm);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
						fscanf(fp, "%lf", &fwbm);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'


						fscanf(fp, "%lf", &fwssm);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
						fscanf(fp, "%lf", &fwshm);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
						fscanf(fp, "%lf", &swsm);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
						fscanf(fp, "%lf", &swhm);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
						fscanf(fp, "%lf", &hwsm);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
						fscanf(fp, "%lf", &hwhm);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
					}
					fclose(fp);
				}

			}
#define SMALLWIND		0.05
#define SMALLSIDESPIN	100
			//--- windy..
			if (windy > SMALLWIND) {		// 뒷바람.  백스핀량 감소..사이드스핀량 감소
//Backspin = Backspin - 바람크기 m/s * 100 RPM
//Sidespin = Sidepin * 0.9
				backspin1 = backspin1 + windy * bwbm;
				sidespin1 = sidespin * bwsm;
			} else if (windy < -SMALLWIND) { 				// 맞바람. 백스핀량 증가..사이드스핀량 증가
//Backspin = Backspin + 바람크기 m/s * 150 RPM
				backspin1 = backspin1 + (-windy) * fwbm;

				if (sidespin > SMALLSIDESPIN) {
//Sidespin = Sidepin + 바람크기 m/s * 15 RPM
					sidespin1 = sidespin1 + (-windy) * fwssm;
				} else if (sidespin < -SMALLSIDESPIN) {
//Sidespin = SideSpin - 바람크기 m/s * 10 RPM
					sidespin1 = sidespin1 + (-windy) * fwshm;
				}
			} 

			//--- windx..
			if (windx > SMALLWIND) {			// 슬라이스바람? 오른쪽으로 휜다.
				if (sidespin > 0) {		// Original sidespin is slice-like..
//Sidespin = Sidepin + 바람크기 m/s * 35 RPM
					sidespin1 = sidespin1 + windx * swsm;
				} else {
//Sidespin = Sidepin + 바람크기 m/s * 17.5 RPM
					sidespin1 = sidespin1 + windx * swhm;
				}
			} else if (windx < -SMALLWIND) {					// 훅바람? 왼쪽으로 휜다.
				if (sidespin > SMALLSIDESPIN) {		// Original sidespin is slice-like..
//Sidespin = Sidepin - 바람크기 m/s * 25 RPM
					sidespin1 = sidespin1 + (-windx) * hwsm;
				} else if (sidespin < -SMALLSIDESPIN) {
//Sidespin = Sidepin + 바람크기 m/s * 17.5 RPM
					sidespin1 = sidespin1 + (-windx) * hwhm;
				}

			}

			ptd->backspin = backspin1;
			ptd->sidespin = sidespin1;
			windx = windx * 0.18;
			//windy = 0.0;
			if (windy > 0) {
				windy *= 0.3;
			} else {
				windy *= 0.1;
			}
		}

#endif		//  Sidespin/Backspin-based wind effect. 20150829

//-----------------------------------------------------------------
//-----------------------------------------------------------------

		// convert spin to vector form
#define BACKSPIN_REFINE
#define SIDESPIN_REFINE

#if defined(BACKSPIN_REFINE)
#if defined(BACKSPIN_V)
		wx = RPM2RADIAN(refine_backspin(ptd->backspin, vmag1));  // Backspin
#else
		wx = RPM2RADIAN(refine_backspin(ptd->backspin));  // Backspin
#endif
#else
		wx = RPM2RADIAN(ptd->backspin);  // Backspin
#endif
		wy = 0.;					// trivial..
		//--
		//wy = RPM2RADIAN(ptd->sidespin);  // FAKE, but VERY GOOD. -_-; 2010/0520, yhsuk
		//--

#if defined(SIDESPIN_REFINE)
#if defined(SIDE_A)
		wz = RPM2RADIAN(refine_sidespin(ptd->sidespin, ptd->azimuth));
#else
		wz = RPM2RADIAN(refine_sidespin(ptd->sidespin));
#endif
#else
		wz = RPM2RADIAN(ptd->sidespin); // sidespin, 2009/11/12
#endif

#if defined(SIDESPIN_PLUS_SLICE__MINUS_HOOK)			// 2010/11/09
		wz *= -1;
#endif


#define VERYSMALL_OFFSET	(1.0e-10)
		w  = sqrt(wx * wx + wy * wy + wz * wz) + VERYSMALL_OFFSET;		// non-zero.. -_-;

		rx = wx / w;
		ry = wy / w;
		rz = wz / w;
	



		//-------------------------------------------------
		// store to trajectory object
		ptraj->q[0] = vx;			// vx
		ptraj->q[1] = 0.;			//  x
		ptraj->q[2] = vy;			// vy
		ptraj->q[3] = 0.;			//  y
		ptraj->q[4] = vz;			// vz
		ptraj->q[5] = 0.;			//  z

		ptraj->r[0] = rx;			// backspin.. unit vector
		ptraj->r[1] = ry;			// 
		ptraj->r[2] = rz;			// sidespin
		ptraj->w    = w;			//  magnitude
		ptraj->current_w    = w;			//  current magnitude

		ptraj->windx= windx;
		ptraj->windy= windy;

		ptraj->t = 0.;

	}

	//---------------------------------------------
	// Init result.
	{
		memset(ptr, 0, sizeof(iana_traj_result_t));
		ptr->distance		= 0.;
		ptr->height			= 0.;
		ptr->side			= 0.;
		ptr->flighttime		= 0.;
		ptr->timedelta		= timedelta;
		ptr->count			= 0;
	}

	//-------
	{
		double incline;

		incline = ptd->incline;
		if (incline < 3.) {
			spin_halflife 		= LINEAR_SCALE(incline, 0.0, 20.0, 	3, 	20.0);
			fm_mult 			= LINEAR_SCALE(incline, 0.0,  1.0, 	3, 	1.0);
			fmy_positive_mult 	= LINEAR_SCALE(incline, 0.0,  0.5, 	3, 	0.4);
			category = CATEGORY_INCLINE_LOWLOW;					//

		} else if (incline < 4.) {
			spin_halflife 		= LINEAR_SCALE(incline, 3.0, 20.0, 	4, 	20.0);
			fm_mult 			= LINEAR_SCALE(incline, 3.0,  1.0, 	4, 	1.1);
			fmy_positive_mult 	= LINEAR_SCALE(incline, 3.0,  0.4, 	4, 	0.5);
			category = CATEGORY_INCLINE_LOWLOW;					//
		} else if (incline < 7.) {
			spin_halflife 		= LINEAR_SCALE(incline, 4, 	20.0,	 7, 20.0);
			fm_mult 			= LINEAR_SCALE(incline, 4, 	1.1,	 7,  1.2);
///////////////////			fmy_positive_mult 	= LINEAR_SCALE(incline, 4, 	0.5,	 7,  0.3);
			fmy_positive_mult 	= LINEAR_SCALE(incline, 4, 	0.5,	 7,  0.5);			// 150910
			category = CATEGORY_INCLINE_LOWMID;					//

		} else if (incline < 8.) {
			spin_halflife 		= LINEAR_SCALE(incline, 7, 20.0,	9, 	20.0);
			fm_mult 			= LINEAR_SCALE(incline, 7,  1.2,	9,	1.20);
//////////////////			fmy_positive_mult 	= LINEAR_SCALE(incline, 7,  0.3,	9,	0.3);
			fmy_positive_mult 	= LINEAR_SCALE(incline, 7,  0.5,	9,	0.5);			// 150910
			category = CATEGORY_INCLINE_LOWMID;					//
		} else if (incline < 10.) {						//
			spin_halflife 		= LINEAR_SCALE(incline, 9,  20.0,	10,	15.0);
			fm_mult 			= LINEAR_SCALE(incline, 9,  1.20,	10,	1.2);			// 
///////////////////////			fmy_positive_mult 	= LINEAR_SCALE(incline, 9,  0.3,	10,	0.7);
			fmy_positive_mult 	= LINEAR_SCALE(incline, 9,  0.5,	10,	0.7);
			category = CATEGORY_INCLINE_LOWHI;				
		} else if (incline < 12.) {						//
			spin_halflife 		= LINEAR_SCALE(incline, 10, 15.0,	12, 17.0);
			fm_mult 			= LINEAR_SCALE(incline, 10, 1.2,	12, 1.20);
			//fmy_positive_mult 	= LINEAR_SCALE(incline, 10, 0.6,	12, 0.92);
//////////////////			fmy_positive_mult 	= LINEAR_SCALE(incline, 10, 0.6,	12, 0.72);
			fmy_positive_mult 	= LINEAR_SCALE(incline, 10, 0.7,	12, 0.72);
			category = CATEGORY_INCLINE_MIDLOW;
		} else if (incline < 15.) {						//
			spin_halflife 		= LINEAR_SCALE(incline, 12, 17.0,	15, 14.0);
			//fm_mult 			= LINEAR_SCALE(incline, 12, 1.20,	15, 1.025);
			fm_mult 			= LINEAR_SCALE(incline, 12, 1.20,	15, 1.1);
			fmy_positive_mult 	= LINEAR_SCALE(incline, 12, 0.72,	15, 0.72);
			category = CATEGORY_INCLINE_MIDMID;
		} else if (incline < 20.) {						//
			spin_halflife 		= LINEAR_SCALE(incline, 15, 17.0,	20, 12.0);
			fm_mult 			= LINEAR_SCALE(incline, 15, 1.1,	20, 1.0);
			fmy_positive_mult 	= LINEAR_SCALE(incline, 15, 0.72,	20, 1.0);
			category = CATEGORY_INCLINE_MIDMID;
		} else if (incline < 30.) {						//
			spin_halflife 		= LINEAR_SCALE(incline, 20, 12.0,	30, 12.0);
			fm_mult 			= LINEAR_SCALE(incline, 20, 1.0,	30, 1.0);
			fmy_positive_mult 	= LINEAR_SCALE(incline, 20, 1.0,	30, 1.0);
			category = CATEGORY_INCLINE_MIDHI;
		} else if (incline < 35.) {						//
			spin_halflife 		= LINEAR_SCALE(incline, 30, 12.0,	35, 10.0);
			fm_mult 			= LINEAR_SCALE(incline, 30, 1.0,	35, 0.9);
			fmy_positive_mult 	= LINEAR_SCALE(incline, 30, 1.0,	35, 1.0);
			category = CATEGORY_INCLINE_HI00;
		} else if (incline < 40.) {						//
			spin_halflife 		= 10.0;
			fm_mult 			= 0.9;
			fmy_positive_mult 	= 1.0;
			category = CATEGORY_INCLINE_HI01;
		} else if (incline < 45.) {						//
			spin_halflife 		= 10.0;
			fm_mult 			= 0.9;
			fmy_positive_mult 	= 1.0;
			category = CATEGORY_INCLINE_HI02;
		} else if (incline < 50.) {						//
			spin_halflife 		= 10.0;
			fm_mult 			= 0.9;
			fmy_positive_mult 	= 1.0;
			category = CATEGORY_INCLINE_HI03;
		} else {
			spin_halflife 		= 10.0;
			fm_mult 			= 0.9;
			fmy_positive_mult 	= 1.0;
			category = CATEGORY_INCLINE_HI04;
		}
	}

#if TRAJTYPE == 0
	s_TTT = 0;
#endif

	//-------------------------------------------
	if (s_TTT == 1) 
	{
#define FR_MULT		1.2

		s_tr[0] = 0.5;	s_fr[0] = 0.5 * FR_MULT;
		s_tr[0] = 1.0;	s_fr[0] = 0.9 * FR_MULT;
		s_tr[1] = 2.0;	s_fr[1] = 1.0 * FR_MULT;
		s_tr[2] = 3.0;	s_fr[2] = 1.1 * FR_MULT;
		s_tr[3] = 5.0;	s_fr[3] = 1.2 * FR_MULT;
		s_tr[4] = 6.0;	s_fr[4] = 1.3 * FR_MULT;
		s_tr[5] = 7.0;	s_fr[5] = 1.4 * FR_MULT;

		if (ptd->vmag < 30) {
			//s_tr[0] = 0.1;	s_fr[0] = 0.5 * FR_MULT;
			//s_tr[1] = 0.2;	s_fr[1] = 0.6 * FR_MULT;
			s_tr[0] = 0.1;	s_fr[0] = 0.1 * FR_MULT;
			s_tr[1] = 0.2;	s_fr[1] = 0.2 * FR_MULT;
			s_tr[2] = 0.5;	s_fr[2] = 0.6 * FR_MULT;

			s_tr[3] = 0.9;	s_fr[3] = 1.0 * FR_MULT;
			s_tr[4] = 2.0;	s_fr[4] = 1.5 * FR_MULT;
			s_tr[5] = 3.0;	s_fr[5] = 2.0 * FR_MULT;

		} else if (ptd->vmag < 40) {
			//s_tr[0] = 0.5;	s_fr[0] = 0.5 * FR_MULT;
			//s_tr[1] = 0.7;	s_fr[1] = 0.6 * FR_MULT;
			s_tr[0] = 0.5;	s_fr[0] = 0.1 * FR_MULT;
			s_tr[1] = 0.7;	s_fr[1] = 0.2 * FR_MULT;
			s_tr[2] = 0.9;	s_fr[2] = 0.6 * FR_MULT;
			s_tr[3] = 1.3;	s_fr[3] = 1.0 * FR_MULT;
			s_tr[4] = 2.0;	s_fr[4] = 1.5 * FR_MULT;
			s_tr[5] = 3.0;	s_fr[5] = 2.0 * FR_MULT;

		} else if (ptd->vmag < 50) {
			//s_tr[0] = 0.5;	s_fr[0] = 0.5 * FR_MULT;
			//s_tr[1] = 1.0;	s_fr[1] = 0.6 * FR_MULT;
			s_tr[0] = 0.5;	s_fr[0] = 0.1 * FR_MULT;
			s_tr[1] = 1.0;	s_fr[1] = 0.2 * FR_MULT;
			s_tr[2] = 1.3;	s_fr[2] = 0.6 * FR_MULT;

			s_tr[3] = 1.9;	s_fr[3] = 1.0 * FR_MULT;
			s_tr[4] = 3.0;	s_fr[4] = 1.5 * FR_MULT;
			s_tr[5] = 4.5;	s_fr[5] = 2.0 * FR_MULT;

//			s_tr[3] = 1.9;	s_fr[3] = 2.5 * FR_MULT;
//			s_tr[4] = 5.0;	s_fr[4] = 2.5 * FR_MULT;
//			s_tr[5] = 6.5;	s_fr[5] = 2.5 * FR_MULT;

		} else if (ptd->vmag < 55) {
			//s_tr[0] = 0.5;	s_fr[0] = 0.5 * FR_MULT;
			//s_tr[1] = 1.0;	s_fr[1] = 0.6 * FR_MULT;
			s_tr[0] = 0.5;	s_fr[0] = 0.1 * FR_MULT;
			s_tr[1] = 1.0;	s_fr[1] = 0.2 * FR_MULT;
			s_tr[2] = 2.0;	s_fr[2] = 0.6 * FR_MULT;
			s_tr[3] = 2.2;	s_fr[3] = 1.0 * FR_MULT;
			s_tr[4] = 3.0;	s_fr[4] = 1.5 * FR_MULT;
			s_tr[5] = 4.5;	s_fr[5] = 2.0 * FR_MULT;
		} else {
			double m;
			m = (ptd->vmag / 55.0);
			s_tr[0] = 0.5 * m;	s_fr[0] = 0.1 * FR_MULT;
			s_tr[1] = 1.0 * m;	s_fr[1] = 0.2 * FR_MULT;
			s_tr[2] = 1.5 * m;	s_fr[2] = 1.0 * FR_MULT;

			s_tr[3] = 2.2 * m;	s_fr[3] = 1.2 * FR_MULT;
			s_tr[4] = 3.0 * m;	s_fr[4] = 1.2 * FR_MULT;
			s_tr[5] = 4.5 * m;	s_fr[5] = 1.4 * FR_MULT;



			s_tr[0] = 0.5 * m;	s_fr[0] = 0.2 * FR_MULT;
			s_tr[1] = 1.0 * m;	s_fr[1] = 0.4 * FR_MULT;
			s_tr[2] = 1.5 * m;	s_fr[2] = 0.4 * FR_MULT;
//			s_tr[3] = 2.2 * m;	s_fr[3] = 1.4 * FR_MULT;
//			s_tr[4] = 3.0 * m;	s_fr[4] = 1.4 * FR_MULT;
//			s_tr[5] = 4.5 * m;	s_fr[5] = 1.4 * FR_MULT;

			s_tr[3] = 2.2 * m;	s_fr[3] = 1.4 * FR_MULT;
			s_tr[4] = 3.0 * m;	s_fr[4] = 1.4 * FR_MULT;
			s_tr[5] = 4.5 * m;	s_fr[5] = 1.4 * FR_MULT;

		}
	} else 
	{
#if TRAJTYPE == 1
#undef FR_MULT
#define FR_MULT 0.8
#endif
		s_tr[0] = 0.5;	s_fr[0] = 1.0 * FR_MULT;
		s_tr[0] = 1.0;	s_fr[0] = 1.1 * FR_MULT;
		s_tr[1] = 2.0;	s_fr[1] = 1.2 * FR_MULT;
		s_tr[2] = 3.0;	s_fr[2] = 1.4 * FR_MULT;
		s_tr[3] = 5.0;	s_fr[3] = 1.5 * FR_MULT;
		s_tr[4] = 6.0;	s_fr[4] = 1.6 * FR_MULT;
		s_tr[5] = 7.0;	s_fr[5] = 1.7 * FR_MULT;

		if (ptd->vmag < 30) {
			s_tr[0] = 0.2;	s_fr[0] = 1.5 * FR_MULT;
			s_tr[1] = 0.5;	s_fr[1] = 1.5 * FR_MULT;
			s_tr[2] = 0.7;	s_fr[2] = 1.5 * FR_MULT;

			s_tr[3] = 1.0;	s_fr[3] = 1.5 * FR_MULT;
			s_tr[4] = 2.0;	s_fr[4] = 1.7 * FR_MULT;
			s_tr[5] = 5.0;	s_fr[5] = 2.0 * FR_MULT;
		} else if (ptd->vmag < 40) {
			s_tr[0] = 0.5;	s_fr[0] = 1.5 * FR_MULT;
			s_tr[1] = 1.0;	s_fr[1] = 1.5 * FR_MULT;
			s_tr[2] = 1.5;	s_fr[2] = 1.5 * FR_MULT;
			s_tr[3] = 2.0;	s_fr[3] = 1.5 * FR_MULT;
			s_tr[4] = 3.0;	s_fr[4] = 1.5 * FR_MULT;
			s_tr[5] = 5.0;	s_fr[5] = 1.5 * FR_MULT;
		} else if (ptd->vmag < 50) {
			s_tr[0] = 0.5;	s_fr[0] = 1.5 * FR_MULT;
			s_tr[1] = 1.0;	s_fr[1] = 1.5 * FR_MULT;
			s_tr[2] = 1.5;	s_fr[2] = 1.5 * FR_MULT;
			s_tr[3] = 2.0;	s_fr[3] = 1.5 * FR_MULT;
			s_tr[4] = 3.0;	s_fr[4] = 1.5 * FR_MULT;
			s_tr[5] = 5.0;	s_fr[5] = 1.5 * FR_MULT;
		} else if (ptd->vmag < 55) {
			s_tr[0] = 0.5;	s_fr[0] = 1.5 * FR_MULT;
			s_tr[1] = 1.0;	s_fr[1] = 1.5 * FR_MULT;
			s_tr[2] = 1.5;	s_fr[2] = 1.5 * FR_MULT;
			s_tr[3] = 2.0;	s_fr[3] = 1.5 * FR_MULT;
			s_tr[4] = 4.0;	s_fr[4] = 1.5 * FR_MULT;
			s_tr[5] = 6.0;	s_fr[5] = 1.5 * FR_MULT;
		} else {
			s_tr[0] = 0.5;	s_fr[0] = 1.5 * FR_MULT;
			s_tr[1] = 1.0;	s_fr[1] = 1.5 * FR_MULT;
			s_tr[2] = 2.0;	s_fr[2] = 1.5 * FR_MULT;
			s_tr[3] = 3.0;	s_fr[3] = 1.5 * FR_MULT;
			s_tr[4] = 5.0;	s_fr[4] = 1.5 * FR_MULT;
			s_tr[5] = 6.5;	s_fr[5] = 1.5 * FR_MULT;
		}
	}

	//---------------------------------------------
	// Get trajectory using 4th order RungeKutta method.
	maxheight = 0.;																// 이제부터 VMAG1 이 더 중요!!! 
	flagu = 0;
	for (i = 0; i < IANA_TRAJ_MAXCOUNT; i++) {
		//ptraj->t = i * timedelta;				// current flight time
		traj_rungekutta4(ptraj, timedelta, category, spin_halflife, fm_mult, fmy_positive_mult);

		// store current result
		{
			//ptr->t[i] = ptraj->t;	// comment out.. yhsuk. 2010/04/28
			ptr->t[i] = i * timedelta;		// current flight time, yhsuk 2010/04/28
			// OK
			ptr->x[i] = ptraj->q[1];		// x (side)
			ptr->y[i] = ptraj->q[3];		// y (target)
			ptr->z[i] = ptraj->q[5];		// z (height)

			ptr->vx[i] = ptraj->q[0];		// vx (side)
			ptr->vy[i] = ptraj->q[2];		// vy (target)
			ptr->vz[i] = ptraj->q[4];		// vz (height)


#if defined(SIDESPIN_REFINE)
#if defined(SIDE_A)
			ptr->sidespin[i] = RADIAN2RPM(revive_sidespin(ptraj->r[2] * ptraj->w, ptd->azimuth));			// rz 
#else
			ptr->sidespin[i] = RADIAN2RPM(revive_sidespin(ptraj->r[2] * ptraj->w));
#endif
#else
			ptr->sidespin[i] = RADIAN2RPM(ptraj->r[2] * ptraj->w);			// rz 
#endif
#if defined(SIDESPIN_PLUS_SLICE__MINUS_HOOK)			// 2010/11/09
			ptr->sidespin[i] *= -1;
#endif

#if defined(BACKSPIN_REFINE)
			ptr->backspin[i] = revive_backspin(RADIAN2RPM(ptraj->r[0] * ptraj->current_w));			// rx
#else
			ptr->backspin[i] = RADIAN2RPM(ptraj->r[0] * ptraj->current_w);			// rx
#endif
		}

		if (ptraj->q[5] > maxheight) {
			maxheight = ptraj->q[5];
		}
//#define TRAJ_UNDER_ZERO		(-20.)
#define TRAJ_UNDER_ZERO		(-60.0)
		if (ptraj->q[5] < 0 && flagu == 0) {
			//---------------------------------------------
			// Store result..
			double x, y, z;
			double vx, vy, vz;
			double vxy;

			vx = ptraj->q[0];
			x  = ptraj->q[1];
			vy = ptraj->q[2];
			y  = ptraj->q[3];
			vz = ptraj->q[4];
			z  = ptraj->q[5];


			//---- Result..
			ptr->distance	= sqrt(x*x + y*y + z*z);
			ptr->height	= maxheight;
			ptr->side		= x;

			ptr->last_vmag			= sqrt(vx*vx + vy*vy + vz*vz) + VERYSMALL_OFFSET;
			vxy					= sqrt(vx*vx + vy*vy) + VERYSMALL_OFFSET;
			ptr->last_azimuth		= RADIAN2DEGREE(atan2(vx, vy));
			ptr->last_incline		= RADIAN2DEGREE(atan2(vz, vxy));


#if defined(SIDESPIN_REFINE)
#if defined(SIDE_A)
			ptr->last_sidespin		=  revive_sidespin(RADIAN2RPM(ptraj->current_w * ptraj->r[2]), ptd->azimuth);  // rz
#else
			ptr->last_sidespin		=  revive_sidespin(RADIAN2RPM(ptraj->current_w * ptraj->r[2]));
#endif
#else
			ptr->last_sidespin		=  RADIAN2RPM(ptraj->current_w * ptraj->r[2]);	// rz
#endif
#if defined(SIDESPIN_PLUS_SLICE__MINUS_HOOK)			// 2010/11/09
			ptr->last_sidespin		*= -1;
#endif

#if defined(BACKSPIN_REFINE)
			ptr->last_backspin		=  revive_backspin(RADIAN2RPM(ptraj->current_w * ptraj->r[0]));  // rx
#else
			ptr->last_backspin		=  RADIAN2RPM(ptraj->current_w * ptraj->r[0]);  // rx
#endif
			ptr->flighttime		= ptraj->t;

			flagu = 1;
		}

		if (ptraj->q[5] < TRAJ_UNDER_ZERO) {
			break;
		}
	}

//#define MAXHEIGHT_BOONGA_BOONGA

#if defined(MAXHEIGHT_BOONGA_BOONGA)
	{
		double hrate;
		int j;

		if (maxheight > 10.0) {
#define MAXHEIGHT_COEFFICIENT	0.8919
//#define MAXHEIGHT_COEFFICIENT	1
			hrate = MAXHEIGHT_COEFFICIENT;

			for (j = 0; j < IANA_TRAJ_MAXCOUNT; j++) {
				ptr->z[j] 	*= hrate; 		// z (height)
				ptr->vz[j] 	*= hrate;		// vz (height)
			}
		}
	}
#endif


#define MAXHEIGHT_BOONGA_BOONGA2

#if defined(MAXHEIGHT_BOONGA_BOONGA2)						// 20150603
	{
		double hrate;
		double vmag1;
		int j;


		vmag1 = ptd->vmag;
		if (vmag1 < 20) {
			hrate = 1.1;
		} else if (vmag1 < 30) {
			hrate = LINEAR_SCALE(vmag1, 20.0, 1.2, 30.0, 1.1);
		} else if (vmag1 < 40) {
			hrate = LINEAR_SCALE(vmag1, 30.0, 1.1, 40.0, 1.0);
		} else if (vmag1 < 50) {
			hrate = LINEAR_SCALE(vmag1, 40.0, 1.0, 50.0, 0.9);
		} else if (vmag1 < 60) {
			hrate = LINEAR_SCALE(vmag1, 50.0, 0.9, 60.0, 0.8);
		} else {
			hrate = 0.8;
		}
		for (j = 0; j < IANA_TRAJ_MAXCOUNT; j++) {
			ptr->z[j] 	*= hrate; 		// z (height)
			ptr->vz[j] 	*= hrate;		// vz (height)
		}
	}
#endif


#if 0
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Traj dist: %lf, HIT ground incline: %lf\n", ptr->distance);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---------- HIT ground data\n");
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Distance: %lf\n", ptr->distance);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "side:     %lf\n", ptr->side);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "incidence:  %lf\n", ptr->last_incline);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "vmag:     %lf\n", ptr->last_vmag);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "time:     %lf\n", ptr->flighttime);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "height:    %lf\n", ptr->height);
#endif


	ptr->count = i-1;

#if defined(G_SENSOR) || defined(TRAJMULT)
	ptd = ptdORG;
#endif

}


/*!
 ********************************************************************************
 *	@brief      4th order RungeKutta method for trajectory
 *	@param[in]  htraj		handle of trajectory object (for RK)
 *	@param[in]  timedelta	time resolution for trajectory (sec)
 *
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2009/11/08
 *******************************************************************************/
//static void traj_rungekutta4(HAND htraj, double timedelta, I32 category)
static void traj_rungekutta4(HAND htraj, double timedelta, I32 category, double spin_halflife, double fm_mult, double fmy_positive_mult)
{
	iana_traj_t	*ptraj; 
	U32 i;
	double t;
	double td;
	double q0[IANA_TRAJ_RK_EQ_NUM];				
	double q1[IANA_TRAJ_RK_EQ_NUM];				
	double q2[IANA_TRAJ_RK_EQ_NUM];				
	double q3[IANA_TRAJ_RK_EQ_NUM];				
	double q4[IANA_TRAJ_RK_EQ_NUM];				

	//---------------------------------------------
	// set initial values
	ptraj = (iana_traj_t *) htraj; 


	//---------------------------------------------
	//  Retrieve the current values of the dependent
	//  and independent variables.
	t = ptraj->t;
	td = timedelta;
	memcpy(&q0[0], &ptraj->q[0], sizeof(double) * IANA_TRAJ_RK_EQ_NUM);

	//---------------------------------------------
	// Compute the four Runge-Kutta steps, The return 
	// value of projectileRightHandSide method is an array
	// of delta-q values for each of the four steps.
//	traj_RK_RightHandSide(ptraj, q0, q0, td, 0.0, q1, t, category);
//	traj_RK_RightHandSide(ptraj, q0, q1, td, 0.5, q2, t, category);
//	traj_RK_RightHandSide(ptraj, q0, q2, td, 0.5, q3, t, category);
//	traj_RK_RightHandSide(ptraj, q0, q3, td, 1.0, q4, t, category);

	traj_RK_RightHandSide(ptraj, q0, q0, td, 0.0, q1, t, category, spin_halflife, fm_mult, fmy_positive_mult);
	traj_RK_RightHandSide(ptraj, q0, q1, td, 0.5, q2, t, category, spin_halflife, fm_mult, fmy_positive_mult);
	traj_RK_RightHandSide(ptraj, q0, q2, td, 0.5, q3, t, category, spin_halflife, fm_mult, fmy_positive_mult);
	traj_RK_RightHandSide(ptraj, q0, q3, td, 1.0, q4, t, category, spin_halflife, fm_mult, fmy_positive_mult);

	//---------------------------------------------
	//  Update the dependent and independent variable values
	//  at the new dependent variable location and store the
	//  values in the ODE object arrays.
	//cr_trace(CR_MSG_AREA_SENSOR, CR_MSG_LEVEL_WARN, "t: %lf, td: %lf\n ", ptraj->t, td);
	ptraj->t = ptraj->t + td;

	for(i = 0; i < IANA_TRAJ_RK_EQ_NUM; ++i) {
		q0[i] = q0[i] + (q1[i] + 2.0*q2[i] + 2.0*q3[i] + q4[i])/6.0;
		ptraj->q[i] = q0[i];
	}     
}



/*!
 ********************************************************************************
 *	@brief      Right-Hand sides for Trajectory
 *	@param[in]  htraj		handle of trajectory object (for RK)
 *	@param[in]  timedelta	time resolution for trajectory (sec)
 *
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2009/11/08
 *******************************************************************************/

//*************************************************************
//  This method loads the right-hand sides for the spring ODEs
//*************************************************************
static void traj_RK_RightHandSide(HAND htraj,
                             double *q, 
							 double *deltaQ, 
							 double td, 
                             double qScale, 
							 double *dq,
							 double t,
							 I32 category
							 ,double spin_halflife, double fm_mult, double fmy_positive_mult)
{
	
	//  q[0] = vx = dx/dt
	//  q[1] = x
	//  q[2] = vy = dy/dt
	//  q[3] = y
	//  q[4] = vz = dz/dt
	//  q[5] = z
	iana_traj_t	*ptraj = (iana_traj_t	*) htraj;
	//iana_traj_basic_t *ptrajb;

	double newQ[6]; // intermediate dependent variable values.
	double mass;
	double area;
	double density;
	double Cd;
	double vx;
	double vy;
	double vz;
	double v;
	double Fd;
	double Fdx;
	double Fdy;
	double Fdz;
	double vax;
	double vay;
	double vaz;
	double va;
	double windVx;
	double windVy;
	double rx;     
	double ry;     
	double rz;     
	double omega;  
	double current_omega;  
	double radius; 
	double Cl;
	double Fm;
	double Fmx;
	double Fmy;
	double Fmz;

	double G = -9.81;

	double Sp;			// Dimensionless value for Cd and Cl. 2009/11/08, yhsuk
	double Sp2, Sp3, Sp4;

	double Re;			// Reynolds number


//	double spin_halflife;
//	double fm_mult;
//	double fmy_positive_mult;

	int i;

	int usetheta;
	double theta;
	double vax2 = 0.0, 	vay2 = 0.0;
	double vx2 = 0.0, 	vy2 = 0.0;

	double fmxTrate;
	//-------------------------------------------------

	mass		= ptraj->trajb.ballmass;
	area		= ptraj->trajb.ballarea;
	density		= ptraj->trajb.airdensity;
	/*
	Cd			= ptraj->Cd;
	*/
	windVx		= ptraj->windx;
	windVy		= ptraj->windy;
	rx			= ptraj->r[0];
	ry			= ptraj->r[1];
	rz			= ptraj->r[2];
	omega		= ptraj->w;
	radius		= ptraj->trajb.ballradius;

	//--------------------------------------------------
	//	2009/11/08, yhsuk
#define SPIN_HALFLIFE		(20.0)
//#define SPIN_HALFLIFE		(10.0)
/*
	cr_trace(CR_MSG_AREA_RECOG, CR_MSG_LEVEL_WARN, "category: 0x%0x (0x%0x)\n",
			category, CATEGORY_INCLINE(category));
*/
	current_omega = omega * pow(2.0, -(ptraj->t / spin_halflife));
//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "t: %10lf, omega: %10lf, co: %lf\n", 
	//ptraj->t,
	//omega,
	//current_omega);


	ptraj->current_w = current_omega;
	
	//  Compute the intermediate values of the 
	//  dependent variables.
	for(i=0; i<6; ++i) {
		newQ[i] = q[i] + qScale*deltaQ[i];
	}

	//  Declare some convenience variables representing
	//  the intermediate values of velocity.
	vx = newQ[0];
	vy = newQ[2];
	vz = newQ[4];

	//  Compute the apparent velocities by subtracting
	//  the wind velocity components from the projectile
	//  velocity components.
	vax = vx - windVx;
	vay = vy - windVy;
	vaz = vz;

	//  Compute the apparent velocity magnitude. The 1.0e-10 term
	//  ensures there won't be a divide by zero later on
	//  if all of the velocity components are zero.
	va = sqrt(vax*vax + vay*vay + vaz*vaz) + VERYSMALL_OFFSET;



	//-------
    Sp = current_omega * radius / va;   // 2009/11/02, yhsuk

//#define SP_MULT		1.2
//#define SP_MULT		1.5
//#define SP_MULT		1.3
#if defined(SP_MULT)
    Sp *= SP_MULT;
#endif

//#define CR_IP_MAX_SP	(0.95)
//#define CR_IP_MAX_SP	(1.1)
#define CR_IP_MAX_SP	2

#if defined (CR_IP_MAX_SP)
	if (Sp > CR_IP_MAX_SP) {
		Sp = CR_IP_MAX_SP;
	}
#endif
//	cr_trace(CR_MSG_AREA_RECOG, CR_MSG_LEVEL_WARN, "----> Sp: %lf\n", Sp);
//	cr_trace(CR_MSG_AREA_RECOG, CR_MSG_LEVEL_TOP, "----> Sp: %lf\n", Sp);
	Sp2 = Sp * Sp;
	Sp3 = Sp * Sp2;
	Sp4 = Sp2 * Sp2;

	Cd = 0.7510 * Sp4 - 1.760 * Sp3 + 1.098 * Sp2 + 0.2148 * Sp + 0.2049;	

	Re = 0;
	//  Compute the total drag force.
	Fd = 0.5*density*area*Cd*va*va;



	Fdx = -Fd*vax/va;
	Fdy = -Fd*vay/va;
	Fdz = -Fd*vaz/va;

	//  Compute the velocity magnitude
	v = sqrt(vx*vx + vy*vy + vz*vz) + 1.0e-8;

	//  Evaluate the Magnus force terms.
//	Cl = radius*omega/v;
	Cl = -0.2158 * Sp4 + 1.006 * Sp3 -1.644 * Sp2 + 1.250 * Sp + 0.0616;

	Fm = 0.5*density*area*Cl*va*va;

#define VERYSMALL_V		(1.0e-3)
	theta = 0.0;
	if (
			//theta > 10.0 && 
			( vay > VERYSMALL_V || vay < - VERYSMALL_V)
	   ) {
	//	double theta;
	//	double fmxx, fmxy;
		vx2 = 0.0;
		vy2 = 0.0;
		vax2 = 0.0;
		vay2 = 0.0;

		theta = atan(vx / vy);
		usetheta = 1;

		vx2 = vx;
		vy2 = vy;

		vx = cos(-theta) * vx2  + sin(-theta) * vy2;
		vy = -sin(-theta) * vx2 + cos(-theta) * vy2;
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "theta: %lf(%lf), vaxy: (%lf, %lf) -> (%lf, %lf)\n",
//				theta,
//			RADIAN2DEGREE(theta),
//				vax2, vay2,
//				vax, vay);

	} else {
//		printf("vay: %lf\n", vay);
		usetheta = 0;
	}

#if 1					// 
	{
		double rz1 = rz;
		Fmx =  -(vy*rz1 - ry*vz)*Fm/v;
		Fmy =  -(vz*rx - rz1*vx)*Fm/v;
		Fmz =  -(vx*ry - rx*vy)*Fm/v;
	}
#endif
#if 0
	{						// Curve at end of line... Fake.. :P
		//double rz1 = rz * pow(2.0, (ptraj->t / spin_halflife));
		double rz1 = rz;
		Fmx =  -(vay*rz1 - ry*vaz)*Fm/va;
		Fmy =  -(vaz*rx - rz1*vax)*Fm/va;
		Fmz =  -(vax*ry - rx*vay)*Fm/va;
	}
#endif


	//-----------------    2015/0405
#if 1
	fmxTrate = calcfmxTrate(t);
	Fmx *= fmxTrate;
#endif
	//----------------------------------------------------------------


#if 0
	Fmx =  -(vay*rz - ry*vaz)*Fm/va;
	Fmy =  -(vaz*rx - rz*vax)*Fm/va;
	Fmz =  -(vax*ry - rx*vay)*Fm/va;
#endif

	if (Fmy > 0) {
		Fmy *= fmy_positive_mult;
	} else {
//		printf("Fmy ---\n");
	}

	if (vaz < 0.0) {			// 
		double maxz;
		double nz;
		double nz_;
		double fmxmult;

		maxz = ptraj->trajb.maxz;
#define MMMZ	1.0
		if (maxz > MMMZ) {
			nz = ptraj->q[5] / maxz;
			nz_ = nz;
			if (nz > 1.0) {
				nz = 1.0;
			}
			if (nz < 0.0) {
				nz = 0.0;
			}
			fmxmult = LINEAR_SCALE(nz, 0.0, 2.0, 1.0, 1.0);
//			Fmx *= fmxmult;

//			printf("nz: %lf, nz_: %lf, maxz: %lf, q5: %lf, fmxmult: %lf, Fmx: %lf\n",
//					nz, nz_,
//					maxz, ptraj->q[5],
//					fmxmult, Fmx
//					);

		}
//		printf("vaz---\n");




	} else {
		ptraj->trajb.maxz = ptraj->q[5];			// z
	}
//	Fmx *= fm_mult;
//	Fmx *= fm_mult;								// Sidespin: NO mult.. 2015/0405
	Fmy *= fm_mult;
	Fmz *= fm_mult;

	if (usetheta) {
		double fmx_, fmy_; 
		double th;
		double vx_, vy_;
		fmx_ = Fmx;
		fmy_ = Fmy;

		Fmx = cos(theta) * fmx_  + sin(theta) * fmy_;
		Fmy = -sin(theta) * fmx_ + cos(theta) * fmy_;


		vx_ = vx;
		vy_ = vy;

		vx =  cos(theta) * vx_  + sin(theta) * vy_;
		vy = -sin(theta) * vx_  + cos(theta) * vy_;

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Fmxy: (%lf, %lf) -> (%lf, %lf)\n",
		//		fmx_, fmy_, Fmx, Fmy);
		th = atan(fmy_/fmx_);
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "theta: %lf(%lf)\n", th, RADIAN2DEGREE(th));
	}
	//  Compute right-hand side values.
	dq[0] = td*(Fdx + Fmx)/mass;
	dq[1] = td*vx;
	dq[2] = td*(Fdy + Fmy)/mass;
	dq[3] = td*vy;
	dq[4] = td*(G + (Fdz + Fmz)/mass);
	dq[5] = td*vz;

	
	UNUSED(spin_halflife);
	UNUSED(fmy_positive_mult);

	UNUSED(t);
        UNUSED(category);
	
	return;
}



#if 1											// 2015/0318
#if defined(BACKSPIN_V)
static double refine_backspin(double backspin, double vmag)
#else
static double refine_backspin(double backspin)
#endif
{
	/*
	0  -> 0		
	1K -> 1.5K
	2K -> 2.2K
	3k -> 3.0K		
	4k -> 3.5K			
	5K -> 4.6K
	6K -> 6.0K
	7K -> 7.0K
	8K -> 7.4K
	10K-> 8K
	.. *0.8
	*/

#if defined(BACKSPIN_V)
//	if (vmag > 40.0 && vmag < 75.0) 
	
	if (vmag > 20.0 && vmag < 75.0) 
	{
		double bsmult;

		bsmult = 1.0;
#if 1
		if (backspin < 800.0) {
			backspin = 800.0;
			bsmult = 1.0;
		} else if (backspin < 2000.0) {
			bsmult = 1.0;
		} else if (backspin < 2500) {
			bsmult = LINEAR_SCALE(backspin,   2000.0,    1.0,  	2500.0,	1.3);
		} else if (backspin < 3900) {
			bsmult = LINEAR_SCALE(backspin,   2500.0,	1.3,	3900.0, 1.3);
		} else if (backspin < 4000) {
			bsmult = LINEAR_SCALE(backspin,   3900.0, 	1.3,	4000.0, 1.1);
		} else if (backspin < 4100) {
			bsmult = LINEAR_SCALE(backspin,   4000.0, 	1.1,	4100.0,	1.3);
		} else if (backspin < 6000) {
			bsmult = LINEAR_SCALE(backspin,   4100.0, 	1.3,	6000.0,	1.1);
		} else if (backspin < 7000) {
			bsmult = LINEAR_SCALE(backspin,   6000.0, 	1.1,	7000.0,	1.0);
		} else {
			bsmult = 1.0;
		}
#endif
		backspin *= bsmult;
	}
#endif


	if (backspin < 1000) {
		backspin = LINEAR_SCALE(backspin,   0,    0,  1000,  1000);
	} else if (backspin < 2000) {
		backspin = LINEAR_SCALE(backspin, 1000, 1000, 2000, 1800);
	} else if (backspin < 3000) {
		backspin = LINEAR_SCALE(backspin, 2000, 1800, 3000, 2500);
	} else if (backspin < 4000) {                               
		backspin = LINEAR_SCALE(backspin, 3000, 2500, 4000, 3000);
	} else if (backspin < 5000) {                               
		backspin = LINEAR_SCALE(backspin, 4000, 3000, 5000, 4000);
	} else if (backspin < 6000) {                               
		backspin = LINEAR_SCALE(backspin, 5000, 4000, 6000, 5000);
	} else if (backspin < 7000) {                               
		backspin = LINEAR_SCALE(backspin, 6000, 5000, 7000, 6000);
	} else if (backspin < 8000) {                               
		backspin = LINEAR_SCALE(backspin, 7000, 6000, 8000, 7000);
	} else if (backspin < 10000) {
		backspin = LINEAR_SCALE(backspin, 8000, 7000, 10000, 8000);
	} else {
		backspin *= 0.8;
	}

	return backspin;
}

static double revive_backspin(double backspin)
{
	if (backspin < 1500) {
		backspin = LINEAR_SCALE(backspin,    0,    0,   1500,  1000);
	} else if (backspin < 2200) {                                  
		backspin = LINEAR_SCALE(backspin,  1500,  1000, 2200,  2000);
	} else if (backspin < 3000) {                                  
		backspin = LINEAR_SCALE(backspin,  2200,  2000, 3000,  3000);
	} else if (backspin < 3500) {                                  
		backspin = LINEAR_SCALE(backspin,  3000,  3000, 3500,  4000);
	} else if (backspin < 4600) {                                  
		backspin = LINEAR_SCALE(backspin,  3500,  4000, 4600,  5000);
	} else if (backspin < 6000) {                                  
		backspin = LINEAR_SCALE(backspin,  4600,  5000, 6000,  6000);
	} else if (backspin < 7000) {                                  
		backspin = LINEAR_SCALE(backspin,  6000,  6000, 7000,  7000);
	} else if (backspin < 7400) {                                  
		backspin = LINEAR_SCALE(backspin,  7000,  7000, 7400,  8000);
	} else if (backspin < 8000) {                                  
		backspin = LINEAR_SCALE(backspin,  7400,  8000, 8000,  10000);
	} else {
		backspin /= 0.8;
	}

	return backspin;
}
#endif


#if !defined(SIDE_A)
#if 1			// Comment out [SGPanGyo PRO result. 2015/0317]
static double refine_sidespin(double sidespin)
{
	int sign;
	double ss_;
	/*
	0  -> 0				(0, 0)
	0.7K -> 0.4K		(700, 450)
	1.0K -> 0.6K		(1000,  600)
	2.0K -> 1.0K		(2000, 1000)
	4.0K -> 2.0K		(4000, 2000)
	*/

	ss = sidespin;
	if (sidespin < 0.0) {
		sign = -1;
		sidespin *= -1;
	} else {
		sign = 1;
	}

	if (sidespin < 400) {
		sidespin = LINEAR_SCALE(sidespin,   0,    0,  400,  200);
	} else if (sidespin < 800) {
		sidespin = LINEAR_SCALE(sidespin, 400,  200,  800,  600);
	} else if (sidespin < 1000) {
		sidespin = LINEAR_SCALE(sidespin, 800,  600, 1000,  800);
	} else if (sidespin < 2000) {
		sidespin = LINEAR_SCALE(sidespin, 1000, 800, 2000, 1200);
	} else {
		sidespin *= (1200.0 /2000.0);
	}

	sidespin *= sign;
//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ss: %lf -> %lf\n", ss_, sidespin);
	return sidespin;
}

	/*
	0  -> 0				(   0,    0)
	0.4K -> 0.7K		( 400,  700)
	0.6K -> 1.0K		( 600, 1000)
	1.0K -> 2.0K		(1000, 2000)
	2.0K -> 4.0K		(2000, 4000)
	*/
static double revive_sidespin(double sidespin)
{
	int sign;

	if (sidespin < 0.0) {
		sign = -1;
		sidespin *= -1;
	} else {
		sign = 1;
	}

	if (sidespin < 400) {
		sidespin = LINEAR_SCALE(sidespin,   0,    0,  400,  700);
	} else if (sidespin < 600) {
		sidespin = LINEAR_SCALE(sidespin, 400,  700, 600, 1000);
	} else if (sidespin < 1000) {
		sidespin = LINEAR_SCALE(sidespin, 600,  1000, 1000, 2000);
	} else {
		sidespin *= 2.0;
	}

	sidespin *= sign;
	return sidespin;
}
#endif
#endif 			// #if !defined(SIDE_A)

#if defined(SIDE_A)
#if 1			// [SGPanGyo PRO result. 2015/0317]
static double refine_sidespin(double sidespin, double azimuth)
{
	int sign;
	int signmatch;
	double ss_;
	/*
	0  -> 0				(0, 0)
	0.7K -> 0.4K		(700, 450)
	1.0K -> 0.6K		(1000,  600)
	2.0K -> 1.0K		(2000, 1000)
	4.0K -> 2.0K		(4000, 2000)
	*/

	ss_ = sidespin;
	if (sidespin < 0.0) {
		sign = -1;
		sidespin *= -1;
	} else {
		sign = 1;
	}

	if (sidespin * azimuth >= 0.0) {
		signmatch = 1;
	} else {
		signmatch = 0;
	}

	if (sidespin < 300) {
		sidespin = LINEAR_SCALE(sidespin,   0,    0,  300,  100);
	} else if (sidespin < 700) {
		sidespin = LINEAR_SCALE(sidespin, 300, 100,  700, 400);
	} else if (sidespin < 1000) {
		sidespin = LINEAR_SCALE(sidespin, 700,  400, 1000,  600);
	} else if (sidespin < 1500) {
		sidespin = LINEAR_SCALE(sidespin, 1000, 600, 1500,  900);
	} else if (sidespin < 2000) {
		sidespin = LINEAR_SCALE(sidespin, 1500, 900, 2000, 1400);
	} else {
		sidespin *= (1400.0/2000.0);
	}

	sidespin *= sign;

//#define SIDESPIN_BOONGA		0.95
#define SIDESPIN_BOONGA		0.7
#if defined(SIDESPIN_BOONGA)
	sidespin *= SIDESPIN_BOONGA;
#endif

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ss: %lf -> %lf\n", ss_, sidespin);
	return sidespin;
}

	/*
	0  -> 0				(   0,    0)
	0.4K -> 0.7K		( 400,  700)
	0.6K -> 1.0K		( 600, 1000)
	1.0K -> 2.0K		(1000, 2000)
	2.0K -> 4.0K		(2000, 4000)
	*/
static double revive_sidespin(double sidespin, double azimuth)
{
	int sign;
	int signmatch;

	if (sidespin < 0.0) {
		sign = -1;
		sidespin *= -1;
	} else {
		sign = 1;
	}

	if (sidespin * azimuth >= 0.0) {
		signmatch = 1;
	} else {
		signmatch = 0;
	}

#if defined(SIDESPIN_BOONGA)
	sidespin /= SIDESPIN_BOONGA;
#endif

	if (signmatch) {
		if (sidespin < 700) {
			sidespin = LINEAR_SCALE(sidespin,   0,    0,  700,  700);
		} else if (sidespin < 900) {
			sidespin = LINEAR_SCALE(sidespin, 700,  700, 900, 1000);
		} else if (sidespin < 1500) {
			sidespin = LINEAR_SCALE(sidespin, 900,  1000, 1500, 2000);
		} else {
			sidespin *= (2000.0 / 1500.0);
		}
	} else {
		if (sidespin < 800) {
			sidespin = LINEAR_SCALE(sidespin,   0,    0, 800,  700);
		} else if (sidespin < 1000) {
			sidespin = LINEAR_SCALE(sidespin, 800,  700, 1000, 1000);
		} else if (sidespin < 1500) {
			sidespin = LINEAR_SCALE(sidespin, 1000, 1000, 2000, 1500);
		} else {
			sidespin *= 1.0;
		}
	}

	sidespin *= sign;
	return sidespin;
}
#endif
#endif 			// #if defined(SIDE_A)
#if 1
double calcfmxTrate(double t)
{
	int i;
	double fmxTrate;

	//fmxTrate = 1.0;
	fmxTrate = s_fr[5];
	for (i = 0; i < 6; i++) {
		if (t < s_tr[i]) {
			if (i == 0) {
				fmxTrate = s_fr[0];
			} else {
				fmxTrate = LINEAR_SCALE(t, s_tr[i-1], s_fr[i-1], s_tr[i], s_fr[i]);
			}
			break;
		}
	}
//	printf("fmtrate: %lf\n", fmxTrate);
#if 0
	if (t < tr[0]) {
		fmxTrate = LINEAR_SCALE(t, 0.0, tr[0], 0.2, tr[0]);
	} else if (t < tr[1]) {
		fmxTrate = LINEAR_SCALE(t, 0.2, tr[0], 0.5, tr[1]);
	} else if (t < tr[2]) {
		fmxTrate = LINEAR_SCALE(t, 0.5, tr[1], 1.0, tr[2]);
	} else if (t < tr[3]) {
		fmxTrate = LINEAR_SCALE(t, 1.0, tr[2], 1.2, tr[3]);
	} else if (t < tr[4]) {
		fmxTrate = LINEAR_SCALE(t, 1.2, tr[3], 1.6, tr[4]);
	} else if (t < tr[5]) {
		fmxTrate = LINEAR_SCALE(t, 1.6, tr[4], 2.0, tr[5]);
	} else {
		fmxTrate = 2.0;
	}
#endif

	return fmxTrate;
}
#endif


#if defined (__cplusplus)
}
#endif

// Not used 
#if 0
static double refine_backspin(double backspin)
{
	/*
	0  -> 0				(0, 0)
	5k -> 5k			(5000, 5000)
	6k -> 5.8k			(6000, 5800)
	7k -> 6.3k			(7000, 6300)
	8k -> 6.5k			(8000, 6500)
	9k -> 6.7k			(9000, 6700)
	*/
	if (backspin < 5000) {
		backspin = LINEAR_SCALE(backspin, 0, 0, 5000, 5000);
	} else if (backspin < 6000) {
		backspin = LINEAR_SCALE(backspin, 5000, 5000, 6000, 5800);
	} else if (backspin < 7000) {
		backspin = LINEAR_SCALE(backspin, 6000, 5800, 7000, 6300);
	} else if (backspin < 8000) {
		backspin = LINEAR_SCALE(backspin, 7000, 6300, 8000, 6500);
	} else {
		backspin = LINEAR_SCALE(backspin, 8000, 6500, 9000, 6700);
	}

	return backspin;
}

	/*
	0  -> 0				(0, 0)
	5k -> 5k			(5000, 5000)
	5.8k -> 6k 			(5800, 6000)
	6.3k -> 7k 			(6300, 7000)
	6.5k -> 8k 			(6500, 8000)
	6.7k -> 9k 			(6700, 9000)
	*/
static double revive_backspin(double backspin)
{
	if (backspin < 5000) {
		backspin = LINEAR_SCALE(backspin, 0, 0, 5000, 5000);
	} else if (backspin < 5800) {
		backspin = LINEAR_SCALE(backspin, 5000, 5000, 5800, 6000);
	} else if (backspin < 6300) {
		backspin = LINEAR_SCALE(backspin, 5800, 6000, 6300, 7000);
	} else if (backspin < 6500) {
		backspin = LINEAR_SCALE(backspin, 6300, 7000, 6500, 8000);
	} else {
		backspin = LINEAR_SCALE(backspin, 6500, 8000, 6700, 9000);
	}

	return backspin;
}
#endif

#if 0											// 2014/1225... Yes.. It's Chrismas!! X_X
static double refine_backspin(double backspin)
{
	/*
	0  -> 0				(0, 0)
	0.5K -> 0.8K		(500, 800)
	3k -> 3k			(3000, 3000)
	5k -> 5k			(5000, 5000)
	6k -> 5.8k			(6000, 5800)
	7k -> 6.3k			(7000, 6300)
	8k -> 6.5k			(8000, 6500)
	9k -> 6.7k			(9000, 6700)
	*/
	if (backspin < 500) {
		backspin = LINEAR_SCALE(backspin,   0,    0,  500,  800);
	} else if (backspin < 3000) {
		backspin = LINEAR_SCALE(backspin, 500,  800, 3000, 3000);
	} else if (backspin < 5000) {
		backspin = LINEAR_SCALE(backspin, 3000, 3000, 5000, 5000);
	} else if (backspin < 6000) {
		backspin = LINEAR_SCALE(backspin, 5000, 5000, 6000, 5800);
	} else if (backspin < 7000) {
		backspin = LINEAR_SCALE(backspin, 6000, 5800, 7000, 6300);
	} else if (backspin < 8000) {
		backspin = LINEAR_SCALE(backspin, 7000, 6300, 8000, 6500);
	} else {
		backspin = LINEAR_SCALE(backspin, 8000, 6500, 9000, 6700);
	}

	return backspin;
}

	/*
	0  -> 0				(0, 0)
	0.8k -> 0.5K		( 800,  500)
	3k -> 3K			(3000, 3000)
	5k -> 5k			(5000, 5000)
	5.8k -> 6k 			(5800, 6000)
	6.3k -> 7k 			(6300, 7000)
	6.5k -> 8k 			(6500, 8000)
	6.7k -> 9k 			(6700, 9000)
	*/
static double revive_backspin(double backspin)
{
	if (backspin < 800) {
		backspin = LINEAR_SCALE(backspin,    0,    0,  800, 500);
	} else if (backspin < 3000) {
		backspin = LINEAR_SCALE(backspin,  800,  500, 3000, 3000);
	} else if (backspin < 5000) {
		backspin = LINEAR_SCALE(backspin, 3000, 3000, 5000, 5000);
	} else if (backspin < 5800) {
		backspin = LINEAR_SCALE(backspin, 5000, 5000, 5800, 6000);
	} else if (backspin < 6300) {
		backspin = LINEAR_SCALE(backspin, 5800, 6000, 6300, 7000);
	} else if (backspin < 6500) {
		backspin = LINEAR_SCALE(backspin, 6300, 7000, 6500, 8000);
	} else {
		backspin = LINEAR_SCALE(backspin, 6500, 8000, 6700, 9000);
	}

	return backspin;
}
#endif


#if 0											// 2015/0226
static double refine_backspin(double backspin)
{
	/*
	0  -> 0				(0, 0)
	0.5K -> 0.8K		(500, 800)
	3k -> 4k			(3000, 4000)
	5k -> 6k			(5000, 6000)
	6k -> 7k			(6000, 7000)
	7k -> 8k			(7000, 8000)
	8k -> 9k			(8000, 9000)
	9k -> 9.5k			(9000, 9500)
	*/
	if (backspin < 500) {
		backspin = LINEAR_SCALE(backspin,   0,    0,  500,  800);
	} else if (backspin < 3000) {
		backspin = LINEAR_SCALE(backspin, 500,  800, 3000, 4000);
	} else if (backspin < 5000) {
		backspin = LINEAR_SCALE(backspin, 3000, 4000, 5000, 6000);
	} else if (backspin < 6000) {
		backspin = LINEAR_SCALE(backspin, 5000, 6000, 6000, 7000);
	} else if (backspin < 7000) {
		backspin = LINEAR_SCALE(backspin, 6000, 7800, 7000, 8000);
	} else if (backspin < 8000) {
		backspin = LINEAR_SCALE(backspin, 7000, 8000, 8000, 9000);
	} else {
		backspin = LINEAR_SCALE(backspin, 8000, 9000, 9000, 9500);
	}

	backspin *= 0.9;				// :)



	return backspin;
}

	/*
	0  -> 0				(0, 0)
	0.8k -> 0.5K		( 800,  500)
	4k -> 3K			(4000, 3000)
	6k -> 5k			(6000, 5000)
	7k -> 6k 			(7000, 6000)
	8k -> 7k 			(8000, 7000)
	9k -> 8k 			(9000, 8000)
	9.5k -> 9k 			(9500, 9000)
	*/
static double revive_backspin(double backspin)
{
	backspin /= 0.9;
	if (backspin < 800) {
		backspin = LINEAR_SCALE(backspin,    0,    0,  800, 500);
	} else if (backspin < 4000) {
		backspin = LINEAR_SCALE(backspin,  800,  500, 4000, 3000);
	} else if (backspin < 6000) {
		backspin = LINEAR_SCALE(backspin, 4000, 3000, 6000, 5000);
	} else if (backspin < 7000) {
		backspin = LINEAR_SCALE(backspin, 6000, 5000, 7000, 6000);
	} else if (backspin < 8000) {
		backspin = LINEAR_SCALE(backspin, 7000, 6000, 8000, 7000);
	} else if (backspin < 9000) {
		backspin = LINEAR_SCALE(backspin, 8000, 7000, 9000, 8000);
	} else {
		backspin = LINEAR_SCALE(backspin, 9000, 8000, 9500, 9000);
	}

	return backspin;
}
#endif

#if 0											// 2015/0226
static double refine_backspin(double backspin)
{
	/*
	0  -> 0		
	1K -> 1.5K
	2K -> 2.2K
	3k -> 3.0K		
	4k -> 3.5K			
	5K -> 4.6K
	6K -> 6.0K
	7K -> 7.0K
	8K -> 7.4K
	10K-> 8K
	.. *0.8
	*/
	if (backspin < 1000) {
		backspin = LINEAR_SCALE(backspin,   0,    0,  1000,  1500);
	} else if (backspin < 2000) {
		backspin = LINEAR_SCALE(backspin, 1000, 1500, 2000, 2200);
	} else if (backspin < 3000) {
		backspin = LINEAR_SCALE(backspin, 2000, 2200, 3000, 3000);
	} else if (backspin < 4000) {                               
		backspin = LINEAR_SCALE(backspin, 3000, 3000, 4000, 3500);
	} else if (backspin < 5000) {                               
		backspin = LINEAR_SCALE(backspin, 4000, 3500, 5000, 4600);
	} else if (backspin < 6000) {                               
		backspin = LINEAR_SCALE(backspin, 5000, 4600, 6000, 6000);
	} else if (backspin < 7000) {                               
		backspin = LINEAR_SCALE(backspin, 6000, 6000, 7000, 7000);
	} else if (backspin < 8000) {                               
		backspin = LINEAR_SCALE(backspin, 7000, 7000, 8000, 7400);
	} else if (backspin < 10000) {
		backspin = LINEAR_SCALE(backspin, 8000, 7400, 10000, 8000);
	} else {
		backspin *= 0.8;
	}

	return backspin;
}

static double revive_backspin(double backspin)
{
	if (backspin < 1500) {
		backspin = LINEAR_SCALE(backspin,    0,    0,   1500,  1000);
	} else if (backspin < 2200) {                                  
		backspin = LINEAR_SCALE(backspin,  1500,  1000, 2200,  2000);
	} else if (backspin < 3000) {                                  
		backspin = LINEAR_SCALE(backspin,  2200,  2000, 3000,  3000);
	} else if (backspin < 3500) {                                  
		backspin = LINEAR_SCALE(backspin,  3000,  3000, 3500,  4000);
	} else if (backspin < 4600) {                                  
		backspin = LINEAR_SCALE(backspin,  3500,  4000, 4600,  5000);
	} else if (backspin < 6000) {                                  
		backspin = LINEAR_SCALE(backspin,  4600,  5000, 6000,  6000);
	} else if (backspin < 7000) {                                  
		backspin = LINEAR_SCALE(backspin,  6000,  6000, 7000,  7000);
	} else if (backspin < 7400) {                                  
		backspin = LINEAR_SCALE(backspin,  7000,  7000, 7400,  8000);
	} else if (backspin < 8000) {                                  
		backspin = LINEAR_SCALE(backspin,  7400,  8000, 8000,  10000);
	} else {
		backspin /= 0.8;
	}

	return backspin;
}
#endif
#if 0											// 2015/0226
static double refine_backspin(double backspin)
{
	/*
	0  -> 0		
	1K -> 1.5K
	2K -> 2.2K
	3k -> 3.0K		
	4k -> 3.5K			
	5K -> 4.6K
	6K -> 6.0K
	7K -> 7.0K
	8K -> 7.4K
	10K-> 8K
	.. *0.8
	*/
	if (backspin < 1000) {
		backspin = LINEAR_SCALE(backspin,   0,    0,  1000,  1500);
	} else if (backspin < 2000) {
		backspin = LINEAR_SCALE(backspin, 1000, 1500, 2000, 2200);
	} else if (backspin < 3000) {
		backspin = LINEAR_SCALE(backspin, 2000, 2200, 3000, 3000);
	} else if (backspin < 4000) {                               
		backspin = LINEAR_SCALE(backspin, 3000, 3000, 4000, 4000);
	} else if (backspin < 5000) {                               
		backspin = LINEAR_SCALE(backspin, 4000, 4000, 5000, 5500);
	} else if (backspin < 6000) {                               
		backspin = LINEAR_SCALE(backspin, 5000, 5500, 6000, 6500);
	} else if (backspin < 7000) {                               
		backspin = LINEAR_SCALE(backspin, 6000, 6500, 7000, 7000);
	} else if (backspin < 8000) {                               
		backspin = LINEAR_SCALE(backspin, 7000, 7000, 8000, 7200);
	} else if (backspin < 10000) {
		backspin = LINEAR_SCALE(backspin, 8000, 7200, 10000, 7201);
	} else {
		backspin *= 0.72;
	}

	return backspin;
}

static double revive_backspin(double backspin)
{
	if (backspin < 1500) {
		backspin = LINEAR_SCALE(backspin,    0,    0,   1500,  1000);
	} else if (backspin < 2200) {                                  
		backspin = LINEAR_SCALE(backspin,  1500,  1000, 2200,  2000);
	} else if (backspin < 3000) {                                  
		backspin = LINEAR_SCALE(backspin,  2200,  2000, 3000,  3000);
	} else if (backspin < 4000) {                                  
		backspin = LINEAR_SCALE(backspin,  3000,  3000, 4000,  4000);
	} else if (backspin < 5500) {                                  
		backspin = LINEAR_SCALE(backspin,  4000,  4000, 5500,  5000);
	} else if (backspin < 6500) {                                  
		backspin = LINEAR_SCALE(backspin,  5500,  5000, 6500,  6000);
	} else if (backspin < 7000) {                                  
		backspin = LINEAR_SCALE(backspin,  6500,  6000, 7000,  7000);
	} else if (backspin < 7200) {                                  
		backspin = LINEAR_SCALE(backspin,  7000,  7000, 7200,  8000);
	} else if (backspin < 7201) {                                  
		backspin = LINEAR_SCALE(backspin,  7200,  8000, 7201,  10000);
	} else {
		backspin /= 0.72;
	}

	return backspin;
}
#endif

#if 0											// 2015/03/16, 2
static double refine_backspin(double backspin)
{
	// Half..
	backspin /= 2.0;
	return backspin;
}

static double revive_backspin(double backspin)
{
	backspin *= 2.0;
	return backspin;
}
#endif

#if 0
static double refine_sidespin(double sidespin, double azimuth)
{
	int sign;
	int signmatch;
	/*
	0  -> 0				(0, 0)
	0.7K -> 0.4K		(700, 450)
	1.0K -> 0.6K		(1000,  600)
	2.0K -> 1.0K		(2000, 1000)
	4.0K -> 2.0K		(4000, 2000)
	*/

	if (sidespin < 0.0) {
		sign = -1;
		sidespin *= -1;
	} else {
		sign = 1;
	}

	if (sidespin * azimuth >= 0.0) {
		signmatch = 1;
	} else {
		signmatch = 0;
	}

#define SIDESPIN_BOONGA		0.95
	if (signmatch) {
		if (sidespin < 700) {
			sidespin = LINEAR_SCALE(sidespin,   0,    0,  700,  700);
		} else if (sidespin < 1000) {
			sidespin = LINEAR_SCALE(sidespin, 700,  700, 1000,  900);
		} else if (sidespin < 2000) {
			sidespin = LINEAR_SCALE(sidespin, 1000, 900, 2000, 1500);
		} else {
			sidespin *= (1500.0/2000.0);
		}
	} else {			// Neg.
		if (sidespin < 700) {
			sidespin = LINEAR_SCALE(sidespin,   0,    0,  700,  800);
		} else if (sidespin < 1000) {
			sidespin = LINEAR_SCALE(sidespin, 700,  800,  1000,  1000);
		} else if (sidespin < 2000) {
			sidespin = LINEAR_SCALE(sidespin, 1000, 1000, 2000, 1500);
		} else {
			sidespin *= 1.0;
		}
	}
	sidespin *= sign;

#if defined(SIDESPIN_BOONGA)
	sidespin *= SIDESPIN_BOONGA;
#endif

	return sidespin;
}
#endif


