/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA shot calc.
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_spin.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2015/12/26 Spin calc.

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <math.h>
#include <string.h>

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"

#include "scamif.h"
#include "scamif_buffer.h"

#include "iana.h"
#include "iana_work.h"
#include "iana_implement.h"
#include "iana_cam.h"
#include "iana_cam_implement.h"
#include "iana_balldetect.h"
#include "iana_shot.h"
#include "iana_tool.h"
#include "iana_spin.h"
#include "iana_club.h"
#include "iana_spin_ballclub.h"
//#include "iana_club_attack.h"
#include "iana_club_mark.h"

#if _MSC_VER >= 1910									// visual studio 2017 or later
#include "iana_spin_rotation.h"
#include "spincalc.h"
#endif

#if _MSC_VER == 1400									// visual studio 2005
#include "iana_spin_rotation.h"
#endif

#if defined(__linux__)
#include "iana_spin_rotation.h"
#include "spincalc.h"
#endif

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
// #define DEBUG_SPIN

#define SPINMODEFILE		"spinmode.txt"

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

extern int g_ccam_lite;
/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
I32 iana_spin_real_clubdata(iana_t *piana);

#if _MSC_VER == 1400									// visual studio 2005
extern U32 dimpleSpin(iana_t *piana, U32 camid);
#endif

extern I32 iana_check_tsmatch(iana_t *piana);
/*!
 ********************************************************************************
 *	@brief      CAM spin
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/02/26
 *******************************************************************************/
I32 iana_spin(iana_t *piana) 
{
    I32 res;
    //	U32 camid;
    iana_shotresult_t *psr;
    iana_shotresult_t *psr0;
    iana_shotresult_t *psr1;

    iana_shotresult_t *psr2;
    iana_shotresult_t *psr20;
    iana_shotresult_t *psr21;


    //----------------------
    psr = &piana->shotresultdata;
    psr0 = &piana->shotresult[0];		// Center
    psr1 = &piana->shotresult[1];		// Side

    psr2 = &piana->shotresultdata2;
    psr20 = &piana->shotresult2[0];		// Center
    psr21 = &piana->shotresult2[1];		// Side

    //------------------------
    // 1) get mark element and tracking
    // 2) get rotation axis and magnitude
    // 3) Convert to sidespin and backspin

    //for (camid = 0; camid < 2; camid++) {
    //  before, we did the algorithm for each camera
    //}
#if _MSC_VER == 1400									// visual studio 2005
    // 1) get mark element and tracking them.
    //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "before iana_spin_mark()\n");
    iana_spin_mark(piana);

    // 2) get rotation axis and magnitude
    //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "before iana_spin_rotation()\n");
    iana_spin_rotation(piana);
    //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "after  iana_spin_rotation()\n");

    if (piana->camsensor_category == CAMSENSOR_EYEXO) {
        int camid;
        for (camid = 0; camid < NUM_CAM; camid++) {
            if (piana->processmode[camid] == CAMPROCESSMODE_BALLMARK) {
                //cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]Before dimpleSpin\n", camid);
                dimpleSpin(piana, camid);
                //cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]After dimpleSpin\n", camid);
            }
        }
    }
#endif

#if _MSC_VER >= 1910	|| defined(__linux__)								// visual studio 2017 or later    
   SpinCalc::run(piana);
#endif


    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "sas: %lf, %lf\n", psr0->spinAssurance, psr1->spinAssurance);
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-----------------------------------------\n");
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<C> axis: %10lf %10lf %10lf, b: %10lf, s: %10lf, m: %10lf\n",
        psr0->axis.x, psr0->axis.y, psr0->axis.z,
        psr0->backspin, psr0->sidespin,
        psr0->spinmag);
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-----------------------------------------\n");
    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<S> axis: %10lf %10lf %10lf, b: %10lf, s: %10lf, m: %10lf\n",
        psr1->axis.x, psr1->axis.y, psr1->axis.z,
        psr1->backspin, psr1->sidespin,
        psr1->spinmag);
    
    iana_club_estimate(piana);    

     if (psr0->spinAssurance >= SPINASSURANCE_6
        || psr0->spinAssurance >= psr1->spinAssurance) {
        memcpy(psr, psr0, sizeof(iana_shotresult_t));
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Center CAM!\n");
    } else {
        memcpy(psr, psr1, sizeof(iana_shotresult_t));
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Side CAM!\n");
    }

    if (psr20->spinAssurance >= SPINASSURANCE_6
        || psr20->spinAssurance >= psr21->spinAssurance) {
        memcpy(psr2, psr20, sizeof(iana_shotresult_t));
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "2, Center CAM!\n");
    } else {
        memcpy(psr2, psr21, sizeof(iana_shotresult_t));
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "2, Side CAM!\n");
    }

    iana_check_tsmatch(piana);

    res = 1;

    return res;
}

/*!
 ********************************************************************************
 *	@brief      CAM spin check
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/09/18
 *******************************************************************************/
I32 iana_spin_check(iana_t *piana)
{
	I32 res;
	U32 camid;
	//U32 i, j;
	I32 i, j;
	double rrm[3][3];
	double spinmagRad;

	iana_shotresult_t *psr;
	iana_cam_t	*pic;
	marksequence_t	*pmks;
	markelement_t	*pmke;
	iana_setup_t *pisetup;
	iana_cam_setup_t *picsetup;

	//--

	pisetup = &piana->isetup;
	for (camid = 0; camid < NUM_CAM; camid++) {
		pic 		= piana->pic[camid];
		pmks 		= &pic->mks;

		psr = &piana->shotresult[camid];
		spinmagRad = psr->spinmag * 2.0 * M_PI / 60.0;				// radian per sec

		picsetup = &pisetup->icsetup[camid];
		spinmagRad = spinmagRad * (1.0/picsetup->run_framerate);

		rodriguesMatrix(&psr->axis, spinmagRad, rrm);


		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---- CAMID: [%d] ", camid);
		for (i = 0; i < (I32)pmks->count; i++) {
			pmke = &pmks->mke[i];
			if (pmke->goodseq != 0) {
				for (j = pmke->birthindex; j < (I32)pmke->lastindex-1; j++) {
					cr_vector_t an, an1, an1e;

					memcpy(&an, &pmke->pos3D_quadratic[j], sizeof(cr_vector_t));
					memcpy(&an1, &pmke->pos3D_quadratic[j+1], sizeof(cr_vector_t));

					rodriguesRotate(rrm, &an, &an1e);
#ifdef DEBUG_SPIN
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                        "<%2d %2d> rot:  %10lf %10lf %10lf   org:  %10lf %10lf %10lf\n",
                        i, j, 
                        an1e.x, an1e.y, an1e.z, 
                        an1.x, an1.y, an1.z);
#endif
				}
			}
		}
	}
	res = 1;

	return res;
}

/*!
 ********************************************************************************
 *	@brief      CAM spin, using CLUB data
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2017/0428
 *******************************************************************************/
I32 iana_spin_real_clubdata(iana_t *piana) 
{
	int len = PATHBUFLEN;
	char filename[PATHBUFLEN];
	char pathstr[PATHBUFLEN];
	char drivestr[PATHBUFLEN];

	FILE *fp;

	iana_shotresult_t *psr0, *psr1;
	iana_shotresult_t *psrWinner;

	iana_shotresult_t *psr;
	iana_shotresult_t *pcsr;
	shotdata_full_t	*psdf;			// shot data full..


	int mode;					// 0: Compare mark and Ball-Club,   1: Mark mode only,   2: Ball-Club mode only
	int runmode;				// 1: Mark mode,  2: Ball-Club mode.

	//--
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++\n");

	OSAL_STR_ConvertUnicode2MultiByte(piana->szDrive, drivestr, len);
	OSAL_STR_ConvertUnicode2MultiByte(piana->szDir,  pathstr, len);
#if defined(_WIN32)				
	sprintf(filename, "%s%s\\%s", drivestr, pathstr, SPINMODEFILE);
#else
	sprintf(filename, "%s%s/%s", drivestr, pathstr, SPINMODEFILE);
#endif

	fp = cr_fopen(filename, "r");


	// mode.  1: Mark mode,  2: Ball-Club mode,  other: compare..
    // mode = -1;
	mode = 1;
	if (fp) {
		fscanf(fp, "%d", &mode);
		cr_fclose(fp);
	}

	psr = &piana->shotresultdata;
	pcsr= &piana->clubball_shotresultdata;
	psdf = &piana->shotresultfull;


	psr0 = &piana->shotresult[0];
	psr1 = &piana->shotresult[1];

	if (psr0->spinAssurance >= psr1->spinAssurance) {
		psrWinner = psr0;
	} else {
		psrWinner = psr1;
	}

	// mode.  1: Mark mode,  2: Ball-Club mode,  other: compare..
	if (mode == 1) {
		runmode = 1;
	} else if (mode == 2) {
		runmode = 2;
	} else {
#define USECLUBSPIN	0.8
		if (psr->spinAssurance < USECLUBSPIN) {
			runmode = 2;
		} else runmode = 1;
	}
	
	if (runmode == 2) {
		memcpy(psr, pcsr, sizeof(iana_shotresult_t));
	}


	//--------------- SHOT data FULLLLLLL
	memcpy(&psdf->shotguid, &piana->shotguid, sizeof(CR_GUID));

	// Basic shot data
	psdf->ballspeedx1000	= (I32)(psr->vmag * 1000);
	psdf->azimuthx1000		= (I32)(psr->azimuth * 1000);
	psdf->inclinex1000		= (I32)(psr->incline * 1000);

	psdf->sidespin			= (I32)pcsr->sidespin;
	psdf->backspin			= (I32)pcsr->backspin;
	if (runmode == 1) {					// 1: Mark mode
		psdf->spindata_kind = 0;
		psdf->sidespin		= (I32)psrWinner->sidespin;
		psdf->backspin		= (I32)psrWinner->backspin;
		psdf->rollspin		= (I32) (psr->axis.y * psr->spinmag);
	} else {							// 2: Ball-Club mode.
		psdf->spindata_kind = 1;
		psdf->sidespin		= (I32)pcsr->sidespin;
		psdf->backspin		= (I32)pcsr->backspin;
		psdf->rollspin		= (I32) 0;
	}


	// Basic club data
	if (pcsr->valid) {					// ball-club result
		psdf->clubdatakind	= 1;		//	0 : ball-marking result.  1: ball-club result	
		psdf->clubspeed_Bx1000	= (I32) (pcsr->clubspeed_B * 1000);
		psdf->clubspeed_Ax1000	= (I32) (pcsr->clubspeed_A * 1000);

		psdf->clubpathx1000		= (I32) (pcsr->clubpath * 1000);
		psdf->clubfaceanglex1000	= (I32) (pcsr->clubfaceangle * 1000);
		psdf->clubfacelengthx1000	= (I32) (pcsr->headerlenL * 1000);	// Club face length (0: discard this value)	[mm]
		psdf->clubballhitpointx1000	= (I32) (pcsr->hitoffsetL * 1000);	// Club ball hit point from club header center	[mm]
	} else {
		psdf->clubdatakind	= 0;		//	0 : ball-marking result.  1: ball-club result	
		psdf->clubspeed_Bx1000	= (I32) (psrWinner->clubspeed_B * 1000);
		psdf->clubspeed_Ax1000	= (I32) (psrWinner->clubspeed_A * 1000);

		psdf->clubpathx1000		= (I32) (psrWinner->clubpath * 1000);
		psdf->clubfaceanglex1000	= (I32) (psrWinner->clubfaceangle * 1000);
		psdf->clubfacelengthx1000	= (I32) (0);						// Club face length (0: discard this value)	[mm]
		psdf->clubballhitpointx1000	= (I32) (0);						// Club ball hit point from club toe	[mm]
	}
	psdf->rsvd4 = 0;



	// Marking Ball processing result
	psdf->mk_assurance	= (I32) (psrWinner->spinAssurance * 100);					// marking ball result, data quality assurance. 0 ~ 99
	psdf->mk_sidespin	= (I32) (psrWinner->sidespin);								// marking ball result: side spin. (+: slice, -: hook)	[rpm]
	psdf->mk_backspin	= (I32) (psrWinner->backspin);								// marking ball result: back spin. (+: backspin, -: Topspin)	[rpm]
	psdf->mk_rollspin	= (I32) (psrWinner->axis.y * psrWinner->spinmag);			// marking ball result: Roll spin. (+: clock-wise, -: Counter clock-wize)	[rpm]
	psdf->mk_spinmagnitude = (I32) (psrWinner->spinmag);							// marking ball result: spin magnitude	
	psdf->mk_axis_x_x1000000 = (I32) (psrWinner->axis.x * 1000000);				// marking ball result: spin axis vector, x value * 1000000
	psdf->mk_axis_y_x1000000 = (I32) (psrWinner->axis.y * 1000000);				// marking ball result: spin axis vector, y value * 1000000
	psdf->mk_axis_z_x1000000 = (I32) (psrWinner->axis.z * 1000000);				// marking ball result: spin axis vector, z value * 1000000
	psdf->mk_clubspeed_Bx1000 = (I32) (psrWinner->clubspeed_B * 1000);				// marking ball result: Club speed Before shot	[mm/s]
	psdf->mk_clubspeed_Ax1000 = (I32) (psrWinner->clubspeed_A * 1000);				// marking ball result: Club speed After shot	[mm/s]
	psdf->mk_clubpathx1000 = (I32) (psrWinner->clubpath * 1000);					// marking ball result: Club path. (+: right,  -: left)	[(1/1000) degree]
	psdf->mk_clubfaceanglex1000 = (I32) (psrWinner->clubfaceangle * 1000);			// marking ball result: Club face angle (+: right, -: left)	[(1/1000) degree]


	// Club-Ball processing result
	psdf->bc_assurance = (I32) (pcsr->spinAssurance * 100);							// Club - Ball result: data quality assurance. 0 ~ 99
	psdf->bc_sidespin  = (I32) (pcsr->sidespin); 									// Club - Ball result: side spin. (+: slice, -: hook)	[rpm]
	psdf->bc_backspin  = (I32) (pcsr->backspin);									// Club - Ball result: back spin (+: backspin, -: Topspin)	[rpm]
	psdf->bc_rollspin  = (I32) (0);													// Club - Ball result: Fixed to 0.	[rpm]

	psdf->bc_spinmagnitude = (I32) (pcsr->spinmag);									// Club - Ball result: spin magnitude
	psdf->bc_axis_x_x1000000 = (I32) (pcsr->axis.x * 1000000);						// Club - Ball result: spin axis vector, x value * 1000000	
	psdf->bc_axis_y_x1000000 = (I32) (pcsr->axis.y * 1000000);						// Club - Ball result: spin axis vector. Fixed to 0x00000000	
	psdf->bc_axis_z_x1000000 = (I32) (pcsr->axis.z * 1000000);						// Club - Ball result: spin axis vector, z value * 1000000	
	psdf->bc_clubspeed_Bx1000 = (I32) (pcsr->clubspeed_B * 1000);					// Club - Ball result: Club speed Before shot	[mm/s]
	psdf->bc_clubspeed_Ax1000 = (I32) (pcsr->clubspeed_A * 1000);					// Club - Ball result: Club speed After  shot	[mm/s]
	psdf->bc_clubpathx1000 = (I32) (pcsr->clubpath * 1000);							// Club - Ball result: Club path. (+: right,  -: left)	[(1/1000) degree]
	psdf->bc_clubfaceanglex1000 = (I32) (pcsr->clubfaceangle * 1000);				// Club - Ball result: Club face angle (+: right, -: left)	[(1/1000) degree]
	psdf->bc_clubfacelengthx1000 = (I32) (pcsr->headerlenL * 1000);					// Club face length (0: discard this value)	[mm]
	psdf->bc_clubballhitpointx1000 = (I32) (pcsr->hitoffsetL * 1000);				// Club ball hit point from club header center	[mm]


	psdf->goodshot = 1;

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"--\n");

	return 1;
}

#if defined (__cplusplus)
}
#endif



