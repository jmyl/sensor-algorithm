/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA shot Information store
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_readinfo.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2016/07/01 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <math.h>
#if defined(_WIN32)
#include <direct.h>
#endif
#include "cr_common.h"
#include "cr_osapi.h"
#include "scamif.h"
#include "scamif_buffer.h"

#include "iana.h"
#include "iana_cam.h"
#include "iana_tool.h"
#include "iana_readinfo.h"


#include "tinyxml2.h"
#include <cstdlib>
#include <cstring>
#include <ctime>

#if defined( _MSC_VER )
	#include <direct.h>		// _mkdir
	#include <crtdbg.h>
	#define WIN32_LEAN_AND_MEAN
	#include <windows.h>
	_CrtMemState startMemState;
	_CrtMemState endMemState;
#elif defined(MINGW32) || defined(__MINGW32__)
    #include <io.h>  // mkdir
#else
	#include <sys/stat.h>	// mkdir
#endif

#define LOCAL_DEBUG_ON 1
#define LOCAL_ERROR_TRACE_ON 1
#include "cr_dbg_on.h"

//--------
using namespace tinyxml2;




/*
#include "cr_thread.h"
#include "cr_regression.h"
#include "cr_nanoxml.h"

#include "iana_work.h"
#include "iana_implement.h"
#include "iana_tool.h"
#include "iana_checkready.h"
#include "iana_balldetect.h"
#include "iana_ready.h"
#include "iana_info.h"
#include "iana_shot.h"

#include <ipp.h>
#include <ippcc.h>
*/

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

//#if defined (__cplusplus)
//extern "C" {
//#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/


static void Get_ImagePath(char *cur_path, char *img_fileName, char *imgFilePath )
{					
	char dirPath[2048]; 							
	
	memset(imgFilePath,0,2048);
	memset(dirPath,0,2048);
						
	OSAL_FILE_GetDirPath(cur_path, dirPath);
#if defined(__linux__)									
	sprintf(imgFilePath,"%s/%s", dirPath, img_fileName);										
#else
	if(strcmp(dirPath,"") == 0)  {/* empty due to wrong cur_path*/
		sprintf(imgFilePath,"%s",  img_fileName);												
	} else {	
		sprintf(imgFilePath,"%s\\%s", dirPath, img_fileName);										
	}
#endif	

}



/*!
 ********************************************************************************
 *	@brief      read info and images
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @param[in]	fn
 *              info xml file name
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2016/0701
 *******************************************************************************/
U32 iana_readinfo_xml(HAND hiana, char *fn)
{
	U32 res;
	iana_t *piana;
	tinyxml2::XMLDocument *pdoc;
	XMLElement* ele0;
	XMLElement* ele1;
	XMLElement* ele2;
	XMLElement* ele3;
	XMLElement* ele4;
	XMLElement* ele5;
	XMLError xres;
	U32	normalbulk;	// 0: NONE, 1: NORMAL, 2: BULK
	U32 framerate;

	//--
	piana = (iana_t *)hiana;

	LOCAL_DEBUG(("%s: BEGIN with %s\n", __func__, fn));

	pdoc = new tinyxml2::XMLDocument();
	xres = pdoc->LoadFile(fn);

	if (xres != XML_SUCCESS) {
		LOCAL_TRACE_ERR(("%s: LoadFile error(%s)!!\n", __func__, fn));
		delete pdoc;
		res = 0;
		goto func_exit;
	}

	framerate = 1;				// FAKE default rate;

	{
		int i;
		
		piana->camsensor_category	= CAMSENSOR_CATEGORY;
		piana->RS_UseAttackCam		= 0;
		piana->LS_UseAttackCam		= 0;

		piana->allowedarea[IANA_AREA_TEE] 		= 1;			// TEE
		piana->allowedarea[IANA_AREA_IRON] 		= 1;			// IRON
		piana->allowedarea[IANA_AREA_PUTTER]		= 0;			// PUTTER

		for(i= 0; i < NUM_CAM; i++)
		{
			piana->processmode[i]		= CAMPROCESSMODE_NULL;		
			
			piana->RS_ic[i].icp.CamExtrinsicValid = 0;
			piana->LS_ic[i].icp.CamExtrinsicValid = 0;

			memset(&piana->RS_ic[i].icp.ProjectionPlaneCenter, 0, sizeof(cr_point_t));
			memset(&piana->LS_ic[i].icp.ProjectionPlaneCenter, 0, sizeof(cr_point_t));

			memset(&piana->info_icc[i]		, 0, sizeof(iana_camconf_t));			// Info live..
			memset(&piana->info_icc_bulk[i]	, 0, sizeof(iana_camconf_t));			// Info live..
			memset(&piana->info_startPosL[i], 0, sizeof(cr_point_t));				// Info live..		

			piana->info_startts64[i] = 0;					// Info live..
			piana->info_startindex[i] = 0;					// Info live..

			piana->info_firstindex[i] = 0;
			piana->info_lastindex[i] = 0;
					
			piana->info_firstindex_bulk[i] = 0;
			piana->info_lastindex_bulk[i] = 0;

			piana->info_runProcessingWidth[i] = piana->pic[i]->runProcessingWidth;
			piana->info_runProcessingHeight[i] = piana->pic[i]->runProcessingHeight;

		}


		memset(&piana->info_shotresult	, 0, sizeof(iana_shotresult_t));			// Info live..

		piana->info_goodshot = 0;


		{
			memset(&piana->mke2DimpleInfo[0][0], 0, sizeof(markelement2_t) * MAXCAMCOUNT * MARKSEQUENCELEN);
			memset(&piana->mke2MarkInfo[0][0], 0, sizeof(markelement2_t) * MAXCAMCOUNT * MARKSEQUENCELEN);
			memset(&piana->ballposP2Info[0][0], 0, sizeof(point_t)*MAXCAMCOUNT*MARKSEQUENCELEN);
			memset(&piana->ballrP2Info[0][0], 0, sizeof(double)*MAXCAMCOUNT*MARKSEQUENCELEN);
			memset(&piana->ballposL3DInfo[0][0], 0, sizeof(cr_point_t)*MAXCAMCOUNT*MARKSEQUENCELEN);
		}
	}

	//-- Clear previous scamif buffer.
	{
		U32 camid;

		for (camid = 0; camid < NUM_CAM; camid++) {
			scamif_imagebuf_init(piana->hscamif, camid, 1, NULL);		// NORMAL buffer
			scamif_imagebuf_init(piana->hscamif, camid, 2, NULL);		// BULK buffer..
		}
	}

	//-------
	ele0 = pdoc->FirstChildElement("SHOTINFO");
	// <version>				</version>
	// <guid>					</guid>

	{
		piana->shottime[0] = 0;
		ele1 = ele0->FirstChildElement("time");
		if (ele1) {
			char *str;
			int len;

			str = (char *)ele1->GetText();
			len = (int)strlen(str);					
			if (len >= 19) {
				//char ddd[64], ttt[64];
				if (len > 40) {
					len = 40; 
				}
				memcpy(&piana->shottime, str, len);
			}
		}
	}

	ele1 = ele0->FirstChildElement("guid");
	{
		int guidgood;
		CR_GUID guid;

		guidgood = 0;
		memset(&guid, 0, sizeof(CR_GUID));
		if (ele1) {
			char *str;
			int len;
			int idata;
			str = (char *)ele1->GetText();
			len = (int)strlen(str);					
			if (len >= 36) {
									// D6F7EDC4-67A7-483E-98B8-1382C5AE8FC8
				sscanf(str, "%8x", &idata);
				guid.Data1 = idata;

				sscanf(str+8+1, "%4x", &idata);
				guid.Data2 = (short)idata;

				sscanf(str+8+1+4+1, "%4x", &idata);
				guid.Data3 = (short)idata;

				sscanf(str+8+1+4+1+4+1, "%4x", &idata);
				guid.Data4[0] = (char) ((idata >>  8) & 0xFF);
				guid.Data4[1] = (char) ((idata >>  0) & 0xFF);

				sscanf(str+8+1+4+1+4+1+4+1, "%4x", &idata);
				guid.Data4[2] = (char) ((idata >>  8) & 0xFF);
				guid.Data4[3] = (char) ((idata >>  0) & 0xFF);

				sscanf(str+8+1+4+1+4+1+4+1+4, "%8x", &idata);
				guid.Data4[4] = (char) ((idata >> 24) & 0xFF);
				guid.Data4[5] = (char) ((idata >> 16) & 0xFF);
				guid.Data4[6] = (char) ((idata >>  8) & 0xFF);
				guid.Data4[7] = (char) ((idata >>  0) & 0xFF);
				guidgood = 1;
			} else {
				guidgood = 0;
			}
		}
		if (guidgood == 1) {
			memcpy(&piana->shotguid, &guid, sizeof(CR_GUID));
		} else {
			memset(&piana->shotguid, 0, sizeof(CR_GUID));	
		}
	}
	// <camera_category>		</camera_category>
	piana->camsensor_category	= CAMSENSOR_NULL;
	ele1 = ele0->FirstChildElement("camsensor_category");
	if (ele1 != NULL) {
		int category;
		category = -1;

		xres = ele1->QueryIntText(&category);
		if (xres == XML_SUCCESS && category >= 0) { 
			piana->camsensor_category = category; 

			iana_sensor_setup(piana, category);
		}
	}


	piana->useLeftHanded = 0;
	ele1 = ele0->FirstChildElement("UseLeftHanded");
	if (ele1 != NULL) {
		int itmp;
		xres = ele1->QueryIntText(&itmp);
		if (xres == XML_SUCCESS && itmp >= 0) { 
			piana->useLeftHanded = (itmp?1:0);
		}
	}

	piana->leftHandedType = 0;
	ele1 = ele0->FirstChildElement("LeftHandedType");
	if (ele1 != NULL) {
		int itmp;
		xres = ele1->QueryIntText(&itmp);
		if (xres == XML_SUCCESS && itmp >= 0) { 
			piana->leftHandedType = (itmp?1:0);
		}
	}

	piana->Right0Left1 = 0;
	ele1 = ele0->FirstChildElement("CurrentHanded");
	if (ele1 != NULL) {
		int itmp;
		xres = ele1->QueryIntText(&itmp);
		if (xres == XML_SUCCESS && itmp >= 0) { 
			piana->Right0Left1 = (itmp?1:0);
		}
	}
	piana->info_right0left1 = piana->Right0Left1;

	piana->RS_CamPosEstimatedLValid = 0;
	ele1 = ele0->FirstChildElement("CamPosEstimatedLValid");
	if (ele1 != NULL) {
		int itmp;
		xres = ele1->QueryIntText(&itmp);
		if (xres == XML_SUCCESS && itmp >= 0) { 
			piana->RS_CamPosEstimatedLValid = (itmp?1:0);
		}
	}

	piana->LS_CamPosEstimatedLValid = 0;
	ele1 = ele0->FirstChildElement("LS_CamPosEstimatedLValid");
	if (ele1 != NULL) {
		int itmp;
		xres = ele1->QueryIntText(&itmp);
		if (xres == XML_SUCCESS && itmp >= 0) { 
			piana->LS_CamPosEstimatedLValid = (itmp?1:0);
		}
	}

	piana->RS_UseProjectionPlane = 0;
	ele1 = ele0->FirstChildElement("UseProjectionPlane");
	if (ele1 != NULL) {
		int itmp;
		xres = ele1->QueryIntText(&itmp);
		if (xres == XML_SUCCESS && itmp >= 0) { 
			piana->RS_UseProjectionPlane = (itmp?1:0);
		}
	}

	piana->LS_UseProjectionPlane = 0;
	ele1 = ele0->FirstChildElement("LS_UseProjectionPlane");
	if (ele1 != NULL) {
		int itmp;
		xres = ele1->QueryIntText(&itmp);
		if (xres == XML_SUCCESS && itmp >= 0) { 
			piana->LS_UseProjectionPlane = (itmp?1:0);
		}
	}

	piana->RS_UseAttackCam = 0;
	ele1 = ele0->FirstChildElement("UseAttackCam");
	if (ele1 != NULL) {
		int itmp;
		xres = ele1->QueryIntText(&itmp);
		if (xres == XML_SUCCESS && itmp >= 0) { 
			piana->RS_UseAttackCam = (itmp?1:0);
		}
	}

	piana->LS_UseProjectionPlane = 0;
	ele1 = ele0->FirstChildElement("LS_UseAttackCam");
	if (ele1 != NULL) {
		int itmp;
		xres = ele1->QueryIntText(&itmp);
		if (xres == XML_SUCCESS && itmp >= 0) { 
			piana->LS_UseAttackCam = (itmp?1:0);
		}
	}


	// config
	ele1 = ele0->FirstChildElement("config");
	if (ele1) {
		char *str;
		double sx, sy, ex, ey;

		ele2 = ele1->FirstChildElement("teearea");				// teearea
		if (ele2) {
			str = (char *)ele2->GetText();
			if (str) {
				sscanf(str, "%lf %lf %lf %lf", &sx, &sy, &ex, &ey);
				piana->RS_rectLTee.sx = sx;
				piana->RS_rectLTee.sy = sy;
				piana->RS_rectLTee.ex = ex;
				piana->RS_rectLTee.ey = ey;

				piana->LS_rectLTee.sx = sx;
				piana->LS_rectLTee.sy = sy;
				piana->LS_rectLTee.ex = ex;
				piana->LS_rectLTee.ey = ey;

				/*piana->prectLTee->sx = sx;
				piana->prectLTee->sy = sy;
				piana->prectLTee->ex = ex;
				piana->prectLTee->ey = ey;
				*/
			}
		}

		ele2 = ele1->FirstChildElement("ironarea");				// ironarea
		if (ele2) {
			str = (char *)ele2->GetText();
			if (str) {
				sscanf(str, "%lf %lf %lf %lf", &sx, &sy, &ex, &ey);

				/*
				piana->prectLIron->sx = sx;
				piana->prectLIron->sy = sy;
				piana->prectLIron->ex = ex;
				piana->prectLIron->ey = ey;
				*/

				piana->RS_rectLIron.sx = sx;
				piana->RS_rectLIron.sy = sy;
				piana->RS_rectLIron.ex = ex;
				piana->RS_rectLIron.ey = ey;

				piana->LS_rectLIron.sx = sx;
				piana->LS_rectLIron.sy = sy;
				piana->LS_rectLIron.ex = ex;
				piana->LS_rectLIron.ey = ey;
			}
		}

		ele2 = ele1->FirstChildElement("putterarea");				// putterarea
		if (ele2) {
			str = (char *)ele2->GetText();
			if (str) {
				sscanf(str, "%lf %lf %lf %lf", &sx, &sy, &ex, &ey);

				/*
				piana->prectLPutter->sx = sx;
				piana->prectLPutter->sy = sy;
				piana->prectLPutter->ex = ex;
				piana->prectLPutter->ey = ey;
				*/
				piana->RS_rectLPutter.sx = sx;
				piana->RS_rectLPutter.sy = sy;
				piana->RS_rectLPutter.ex = ex;
				piana->RS_rectLPutter.ey = ey;

				piana->LS_rectLPutter.sx = sx;
				piana->LS_rectLPutter.sy = sy;
				piana->LS_rectLPutter.ex = ex;
				piana->LS_rectLPutter.ey = ey;
			}
		}
	}		// end of "config"
	
	// cameras
	ele1 = ele0->FirstChildElement("cameras");
	if (ele1) {
		ele2 = ele1->FirstChildElement("camera");
		while(ele2 != NULL) {
			int camid;
			camid = -1;
			xres = ele2->QueryIntAttribute("id", &camid);
			if ((xres == XML_SUCCESS) && (camid >= 0 && camid < NUM_CAM)) {
				//iana_cam_param_t *picp;
				iana_cam_param_t *RS_picp;
				iana_cam_param_t *LS_picp;
				double dtmp;
				double xd, yd, zd;
				char *str;

				//--
				ele3 = ele2->FirstChildElement("processmode");
				if (ele3) {
					I32 processmode;
					xres = ele3->QueryIntText(&processmode);
					if (xres == XML_SUCCESS) { 
						piana->processmode[camid] = processmode; 
					}
				}

				//picp = &piana->pic[camid]->icp;
				RS_picp = &piana->RS_ic[camid].icp;
				LS_picp = &piana->LS_ic[camid].icp;

				ele3 = ele2->FirstChildElement("ce");
				if (ele3) {
					xres = ele3->QueryDoubleText(&dtmp);
					if (xres == XML_SUCCESS) { 
						//picp->CerBallexist = dtmp; 
						RS_picp->CerBallexist = dtmp; 
						LS_picp->CerBallexist = dtmp; 
					}
				}

				ele3 = ele2->FirstChildElement("cs");
				if (ele3) {
					xres = ele3->QueryDoubleText(&dtmp);
					if (xres == XML_SUCCESS) { 
						//picp->CerBallshot = dtmp;
						RS_picp->CerBallshot = dtmp;
						LS_picp->CerBallshot = dtmp;
//#define MAXCERBALLSHOT	0.115
//#define MAXCERBALLSHOT	0.099
#define MAXCERBALLSHOT	0.15
//#define MAXCERBALLSHOT	0.1
//#define MAXCERBALLSHOT	0.08
//#define MAXCERBALLSHOT	0.20
						if (RS_picp->CerBallshot > MAXCERBALLSHOT) {
							//picp->CerBallshot = MAXCERBALLSHOT;
							RS_picp->CerBallshot = MAXCERBALLSHOT;
							LS_picp->CerBallshot = MAXCERBALLSHOT;
						}
					//	picp->CerBallshot = MAXCERBALLSHOT;
					}
				}

				ele3 = ele2->FirstChildElement("bmax");
				if (ele3) {
					xres = ele3->QueryDoubleText(&dtmp);
					if (xres == XML_SUCCESS) { 
						//picp->BallsizeMax = dtmp;
						RS_picp->BallsizeMax = dtmp;
						LS_picp->BallsizeMax = dtmp;
					}
				}

				ele3 = ele2->FirstChildElement("bmin");
				if (ele3) {
					xres = ele3->QueryDoubleText(&dtmp);
					if (xres == XML_SUCCESS) { 
						//picp->BallsizeMin = dtmp;
						RS_picp->BallsizeMin = dtmp;
						LS_picp->BallsizeMin = dtmp;
					}
				}
//#define NOREAD_CAMPOS

#if !defined(NOREAD_CAMPOS)
				ele3 = ele2->FirstChildElement("position");
				if (ele3) {
					str = (char *)ele3->GetText();
					if (str) {
						sscanf(str, "%lf %lf %lf", &xd, &yd, &zd);

						/*
						picp->CamPosL.x = xd;
						picp->CamPosL.y = yd;
						picp->CamPosL.z = zd;
						*/
						RS_picp->CamPosL.x = xd;
						RS_picp->CamPosL.y = yd;
						RS_picp->CamPosL.z = zd;

						LS_picp->CamPosL.x = xd;
						LS_picp->CamPosL.y = yd;
						LS_picp->CamPosL.z = zd;
					}
				}
				//memcpy(&picp->CamPosEstimatedL, &picp->CamPosL, sizeof(cr_point_t));			// Default..
				memcpy(&RS_picp->CamPosEstimatedL, &RS_picp->CamPosL, sizeof(cr_point_t));			// Default..
				memcpy(&LS_picp->CamPosEstimatedL, &LS_picp->CamPosL, sizeof(cr_point_t));			// Default..

#endif
#if !defined(NOREAD_CAMPOS)
				{
					//picp->CamPosEstimatedLValid = 0;
					//ele3 = ele2->FirstChildElement("ValidpositionEstimated");
					//if (ele3) {
					//	I32 CamPosEstimatedLValid;

					//	xres = ele3->QueryIntText(&CamPosEstimatedLValid);
					//	if (xres == XML_SUCCESS) { 
					//		picp->CamPosEstimatedLValid = CamPosEstimatedLValid;
					//	} else {
					//		picp->CamPosEstimatedLValid = 0;
					//	}
					//}

					if (piana->RS_CamPosEstimatedLValid || piana->LS_CamPosEstimatedLValid) 
					{
						ele3 = ele2->FirstChildElement("positionEstimated");
						if (ele3) {
							str = (char *)ele3->GetText();
							if (str) {
								sscanf(str, "%lf %lf %lf", &xd, &yd, &zd);
								/*
								picp->CamPosEstimatedL.x = xd;
								picp->CamPosEstimatedL.y = yd;
								picp->CamPosEstimatedL.z = zd;
								*/
								RS_picp->CamPosEstimatedL.x = xd;
								RS_picp->CamPosEstimatedL.y = yd;
								RS_picp->CamPosEstimatedL.z = zd;
								LS_picp->CamPosEstimatedL.x = xd;
								LS_picp->CamPosEstimatedL.y = yd;
								LS_picp->CamPosEstimatedL.z = zd;
							}
						} 
					}
				}
#endif

				if (piana->RS_UseProjectionPlane || piana->LS_UseProjectionPlane) {
					ele3 = ele2->FirstChildElement("ProjectionPlaneCenter");

					if (ele3) {
						str = (char *)ele3->GetText();
						if (str) {
							sscanf(str, "%lf %lf %lf", &xd, &yd, &zd);

							RS_picp->ProjectionPlaneCenter.x = xd;
							RS_picp->ProjectionPlaneCenter.y = yd;
							RS_picp->ProjectionPlaneCenter.z = zd;
							LS_picp->ProjectionPlaneCenter.x = xd;
							LS_picp->ProjectionPlaneCenter.y = yd;
							LS_picp->ProjectionPlaneCenter.z = zd;
						}
					} 
				}


				UNUSED(xd);UNUSED(yd);UNUSED(zd);

				//-- Intrinsic Params
				ele3 = ele2->FirstChildElement("cameraintrinsic");
				if (ele3 != NULL) {
					int intrinsicid;
					//cameraintrinsic_t *pcip;
					cameraintrinsic_t *RS_pcip;
					cameraintrinsic_t *LS_pcip;

					intrinsicid = -1;
					xres = ele3->QueryIntAttribute("id", &intrinsicid);
					if (xres == XML_SUCCESS) {
						//pcip 	= &picp->cameraintrinsic;
						//pcip->id = intrinsicid;

						RS_pcip = &RS_picp->cameraintrinsic;
						RS_pcip->id = intrinsicid;
						LS_pcip = &LS_picp->cameraintrinsic;
						LS_pcip->id = intrinsicid;


						ele4 = ele3->FirstChildElement("intrinsicparam");
						if (ele4) {
							str = (char *)ele4->GetText();
							if (str) {
								double fx, cx, fy, cy;
								sscanf(str, "%lf %lf %lf %lf", &fx, &cx, &fy, &cy);

								/*
								pcip->cameraParameter[0] = fx;
								pcip->cameraParameter[1] = cx;
								pcip->cameraParameter[2] = fy;
								pcip->cameraParameter[3] = cy;
								*/
								RS_pcip->cameraParameter[0] = fx;
								RS_pcip->cameraParameter[1] = cx;
								RS_pcip->cameraParameter[2] = fy;
								RS_pcip->cameraParameter[3] = cy;

								LS_pcip->cameraParameter[0] = fx;
								LS_pcip->cameraParameter[1] = cx;
								LS_pcip->cameraParameter[2] = fy;
								LS_pcip->cameraParameter[3] = cy;
							}
						}

						ele4 = ele3->FirstChildElement("distortparam");
						if (ele4) {
							str = (char *)ele4->GetText();
							if (str) {
								double k1, k2, p1, p2, k3;
								sscanf(str, "%lf %lf %lf %lf %lf", &k1, &k2, &p1, &p2, &k3);
								/*
								pcip->distCoeffs[0] = k1;
								pcip->distCoeffs[1] = k2;
								pcip->distCoeffs[2] = p1;
								pcip->distCoeffs[3] = p2;
								pcip->distCoeffs[4] = k3;
								*/
								RS_pcip->distCoeffs[0] = k1;
								RS_pcip->distCoeffs[1] = k2;
								RS_pcip->distCoeffs[2] = p1;
								RS_pcip->distCoeffs[3] = p2;
								RS_pcip->distCoeffs[4] = k3;

								LS_pcip->distCoeffs[0] = k1;
								LS_pcip->distCoeffs[1] = k2;
								LS_pcip->distCoeffs[2] = p1;
								LS_pcip->distCoeffs[3] = p2;
								LS_pcip->distCoeffs[4] = k3;
							}
						}
					}
				} 		 // ele3 = ele2->FirstChildElement("cameraintrinsic");

				//----- ExtrinsicParams
				ele3 = ele2->FirstChildElement("ExtrinsicParams");
				if (ele3 != NULL) {
					ele4 = ele3->FirstChildElement("ExtrinsicParamValid");
					if (ele4) {
						int paramvalid;

						xres = ele4->QueryIntText(&paramvalid);
						if (xres == XML_SUCCESS) {
							RS_picp->CamExtrinsicValid = paramvalid;
							LS_picp->CamExtrinsicValid = paramvalid;
						} else {
							RS_picp->CamExtrinsicValid = 0;
							LS_picp->CamExtrinsicValid = 0;
						}
						if (RS_picp->CamExtrinsicValid != 0) {
							U32 n;
							U32 j;
							U64 	tmp64;
							U64		*p64;


							cameraextrinsic_t *pcep;

							pcep = &RS_picp->cep;
							//--
							ele4 = ele3->FirstChildElement("RotationVector");
							if (ele4) {
								str = (char *)ele4->GetText();

								p64 = (U64 *)&pcep->rotationVector[0];
								n = 0;
								for (j = 0; j < 3; j++) {
									sscanf(str, "%llx%n", &tmp64, &n);
									str += n;
									*(p64 + j) = tmp64;
								}
							}

							ele4 = ele3->FirstChildElement("TranslationVector");
							if (ele4) {
								str = (char *)ele4->GetText();

								p64 = (U64 *)&pcep->translationVector[0];
								n = 0;
								for (j = 0; j < 3; j++) {
									sscanf(str, "%llx%n", &tmp64, &n);
									str += n;
									*(p64 + j) = tmp64;
								}
							}

							ele4 = ele3->FirstChildElement("rotationMatrix");
							if (ele4) {
								str = (char *)ele4->GetText();

								p64 = (U64 *)&pcep->r[0];
								n = 0;
								for (j = 0; j < 3*3; j++) {
									sscanf(str, "%llx%n", &tmp64, &n);
									str += n;
									*(p64 + j) = tmp64;
								}
							}

							ele4 = ele3->FirstChildElement("ExtMat");
							if (ele4) {
								str = (char *)ele4->GetText();
								p64 = (U64 *)&pcep->extMat[0][0];
								n = 0;
								for (j = 0; j < 4*4; j++) {
									sscanf(str, "%llx%n", &tmp64, &n);
									str += n;
									*(p64 + j) = tmp64;
								}
				}

							ele4 = ele3->FirstChildElement("InvExtMat");
							if (ele4) {
								str = (char *)ele4->GetText();
								p64 = (U64 *)&pcep->InvextMat[0][0];
								n = 0;
								for (j = 0; j < 4*4; j++) {
									sscanf(str, "%llx%n", &tmp64, &n);
									str += n;
									*(p64 + j) = tmp64;
								}
							}
							memcpy(&LS_picp->cep, &RS_picp->cep, sizeof(cameraextrinsic_t));
						} 	 		// if (RS_picp->CamExtrinsicValid != 0) 
					} 	// ele4 = ele3->FirstChildElement("ExtrinsicParamValid");
				} 		// ele3 = ele2->FirstChildElement("ExtrinsicParams");

#if !defined(NOREAD_CAMPOS)
				//picp->quad_UdValid = 0;
				RS_picp->quad_UdValid = 0;
				LS_picp->quad_UdValid = 0;
				ele3 = ele2->FirstChildElement("transmat");
				if (ele3) {
					U32 i;
					U32 n;
					//biquad_t *pbq;
					biquad_t *RS_pbq, *LS_pbq;
					U64 	tmp64;
					//U64		*p64;
					U64		*RS_p64, *LS_p64;

					str = (char *)ele3->GetText();
					if (str) {
						//pbq = &picp->bAp2l_raw;
						//p64 = (U64 *)&pbq->mat[0];
						RS_pbq = &RS_picp->bAp2l_raw;
						RS_p64 = (U64 *)&RS_pbq->mat[0];

						LS_pbq = &LS_picp->bAp2l_raw;
						LS_p64 = (U64 *)&LS_pbq->mat[0];
						n = 0;
						for (i = 0; i < 12; i++) {
							sscanf(str, "%llx%n", &tmp64, &n);
							str += n;
							//*(p64 + i) = tmp64;
							*(RS_p64 + i) = tmp64;
							*(LS_p64 + i) = tmp64;
						}
					}

					ele3 = ele3->NextSiblingElement("transmat");
					if (ele3) {
						str = (char *)ele3->GetText();
						if (str) {
							//pbq = &picp->bAl2p_raw;
							//p64 = (U64 *)&pbq->mat[0];

							RS_pbq = &RS_picp->bAl2p_raw;
							RS_p64 = (U64 *)&RS_pbq->mat[0];

							LS_pbq = &LS_picp->bAl2p_raw;
							LS_p64 = (U64 *)&RS_pbq->mat[0];

							n = 0;
							for (i = 0; i < 12; i++) {
								sscanf(str, "%llx%n", &tmp64, &n);
								str += n;
								//*(p64 + i) = tmp64;
								*(RS_p64 + i) = tmp64;
								*(LS_p64 + i) = tmp64;
							}
						}
					}


					//---------------------------- Undistorted...
					if (ele3) {
						ele3 = ele3->NextSiblingElement("transmat");
						if (ele3) {
							str = (char *)ele3->GetText();
							if (str) {
								//pbq = &picp->bAp2l_Ud_raw;
								//p64 = (U64 *)&pbq->mat[0];

								RS_pbq = &RS_picp->bAp2l_Ud_raw;
								RS_p64 = (U64 *)&RS_pbq->mat[0];

								LS_pbq = &LS_picp->bAp2l_Ud_raw;
								LS_p64 = (U64 *)&LS_pbq->mat[0];

								n = 0;
								for (i = 0; i < 12; i++) {
									sscanf(str, "%llx%n", &tmp64, &n);
									str += n;
									//*(p64 + i) = tmp64;
									*(RS_p64 + i) = tmp64;
									*(LS_p64 + i) = tmp64;
								}
							}
						}
					}

					if (ele3) {
						ele3 = ele3->NextSiblingElement("transmat");
						if (ele3) {
							str = (char *)ele3->GetText();
							if (str) {
								//pbq = &picp->bAl2p_Ud_raw;
								//p64 = (U64 *)&pbq->mat[0];
								RS_pbq = &RS_picp->bAl2p_Ud_raw;
								RS_p64 = (U64 *)&RS_pbq->mat[0];

								LS_pbq = &LS_picp->bAl2p_Ud_raw;
								LS_p64 = (U64 *)&LS_pbq->mat[0];

								n = 0;
								for (i = 0; i < 12; i++) {
									sscanf(str, "%llx%n", &tmp64, &n);
									str += n;
									//*(p64 + i) = tmp64;
									*(RS_p64 + i) = tmp64;
									*(LS_p64 + i) = tmp64;
								}
								//picp->quad_UdValid = 1;
								RS_picp->quad_UdValid = 1;
								LS_picp->quad_UdValid = 1;
							}
						}
					}
					//---------------------------- 
				}	// "transmat"
#else
				RS_picp->quad_UdValid = 1;
				LS_picp->quad_UdValid = 1;
#endif
				//piana->pic[camid]->icp.lpdatapaircount = 0;
				piana->RS_ic[camid].icp.lpdatapaircount = 0;
				piana->LS_ic[camid].icp.lpdatapaircount = 0;
				ele3 = ele2->FirstChildElement("pixellocalpoints");
				if (ele3 != NULL) {
					int i;
					int count;
					//lpdatapair_t *plp;
					lpdatapair_t *RS_plp;
					lpdatapair_t *LS_plp;
					
					//--
					count = -1;
					xres = ele3->QueryIntAttribute("count", &count);
					if ((xres == XML_SUCCESS) && (count > 0 && count <= LPCOUNTMAX)) {
						//plp = &piana->pic[camid]->icp.lpdatapair[0];
						RS_plp = &piana->RS_ic[camid].icp.lpdatapair[0];
						LS_plp = &piana->LS_ic[camid].icp.lpdatapair[0];
						str = (char *)ele3->GetText();
						if (str) {
							piana->RS_ic[camid].icp.lpdatapaircount = 0;
							piana->RS_ic[camid].icp.lpdatapaircount = 0;
							for (i = 0; i < count; i++) {
								I32		readres;
								U64 	tmp64;
								U64		*p64;
								U32 	n;
								double lx, ly, px, py;

								readres = sscanf(str, "%llx%n", &tmp64, &n);
								p64 = (U64 *) &lx;
								if (readres == 1) {
									*p64 = tmp64;
									str += n;
									//plp[i].Lp.x = lx;
									RS_plp[i].Lp.x = lx;
									LS_plp[i].Lp.x = lx;
								} else {
									break;
								}

								readres = sscanf(str, "%llx%n", &tmp64, &n);
								p64 = (U64 *) &ly;
								if (readres == 1) {
									*p64 = tmp64;
									str += n;
									//plp[i].Lp.y = ly;
									RS_plp[i].Lp.y = ly;
									LS_plp[i].Lp.y = ly;
								} else {
									break;
								}

								readres = sscanf(str, "%llx%n", &tmp64, &n);
								p64 = (U64 *) &px;
								if (readres == 1) {
									*p64 = tmp64;
									str += n;
									//plp[i].Pp.x = px;
									RS_plp[i].Pp.x = px;
									LS_plp[i].Pp.x = px;
								} else {
									break;
								}

								readres = sscanf(str, "%llx%n", &tmp64, &n);
								p64 = (U64 *) &py;
								if (readres == 1) {
									*p64 = tmp64;
									str += n;
									//plp[i].Pp.y = py;
									RS_plp[i].Pp.y = py;
									LS_plp[i].Pp.y = py;
								} else {
									break;
								}

								piana->RS_ic[camid].icp.lpdatapaircount++;
								piana->LS_ic[camid].icp.lpdatapaircount++;
							}
						} else {
							piana->RS_ic[camid].icp.lpdatapaircount = 0;
							piana->LS_ic[camid].icp.lpdatapaircount = 0;
						}
					}
				}
			
			} 		// if ((xres == XML_SUCCESS)...)
			ele2 = ele2->NextSiblingElement("camera");
		} 	// while(ele2 != NULL) 
	} 		// end of  "cameras"

	// camrunconfig
	ele1 = ele0->FirstChildElement("camrunconfig");
	if (ele1) {
		int camid;
		int itmp;
		int camidCheckReady;
		int camidOther;

		//---
		camidCheckReady	= -1;
		camidOther		= -1;
		ele2 = ele1->FirstChildElement("checkreadycamid");
		if (ele2) {
			xres = ele2->QueryIntText(&camid);
			if (xres == XML_SUCCESS) { 
				if (camid >= 0 && camid < NUM_CAM) {camidCheckReady = camid; }
			}
		}

		ele2 = ele1->FirstChildElement("othercamid");
		if (ele2) {
			xres = ele2->QueryIntText(&camid);
			if (xres == XML_SUCCESS) { 
				if (camid >= 0 && camid < NUM_CAM) {camidOther = camid; }
			}
		}



		piana->ts64delta = 0;
		ele2 = ele1->FirstChildElement("ts64delta");
		if (ele2) {
			char *str;
			I64 ts64;
			str = (char *)ele2->GetText();
			if (str) { 
				sscanf(str, "%llx", &ts64);
				piana->ts64delta = ts64;
			}
		}

		piana->ts64deltaAtC = 0;
		ele2 = ele1->FirstChildElement("ts64deltaAtC");
		if (ele2) {
			char *str;
			I64 ts64;
			str = (char *)ele2->GetText();
			if (str) { 
				sscanf(str, "%llx", &ts64);
				piana->ts64deltaAtC = ts64;
			}
		}

		ele2 = ele1->FirstChildElement("allowTee");
		if (ele2) {
			xres = ele2->QueryIntText(&itmp);
			if (xres == XML_SUCCESS) { 
				piana->allowedarea[IANA_AREA_TEE] = itmp;
			}
		}

		ele2 = ele1->FirstChildElement("allowIron");
		if (ele2) {
			xres = ele2->QueryIntText(&itmp);
			if (xres == XML_SUCCESS) { 
				piana->allowedarea[IANA_AREA_IRON] = itmp;
			}
		}

		ele2 = ele1->FirstChildElement("allowPutter");
		if (ele2) {
			xres = ele2->QueryIntText(&itmp);
			if (xres == XML_SUCCESS) { 
				piana->allowedarea[IANA_AREA_PUTTER] = itmp;
			}
		}


		ele2 = ele1->FirstChildElement("camera");
		while(ele2 != NULL) {
			camid = -1;
			xres = ele2->QueryIntAttribute("id", &camid);
			if ((xres == XML_SUCCESS) && (camid >= 0 && camid < NUM_CAM)) {
				ele3 = ele2->FirstChildElement("config");
				if (ele3 != NULL) {
//					int itmp;

					iana_camconf_t *piicc;		// info_icc[2];			// camconf for info processing.  
					piicc = &piana->info_icc[camid];
					if (piana->Right0Left1 == 0) {
						memcpy(piicc, &piana->RS_ic[camid].icf, sizeof(iana_camconf_t));
					} else {
						memcpy(piicc, &piana->LS_ic[camid].icf, sizeof(iana_camconf_t));
					}

					ele4 = ele3->FirstChildElement("width");
					if (ele4) {
						xres = ele4->QueryIntText(&itmp);
						if (xres == XML_SUCCESS) { piicc->width = itmp; }
					}

					ele4 = ele3->FirstChildElement("height");
					if (ele4) {
						xres = ele4->QueryIntText(&itmp);
						if (xres == XML_SUCCESS) { piicc->height = itmp; }
					}
					
					ele4 = ele3->FirstChildElement("offset_x");
					if (ele4) {
						xres = ele4->QueryIntText(&itmp);
						if (xres == XML_SUCCESS) { piicc->offset_x = itmp; }
					}

					ele4 = ele3->FirstChildElement("offset_y");
					if (ele4) {
						xres = ele4->QueryIntText(&itmp);
						if (xres == XML_SUCCESS) { piicc->offset_y = itmp; }
					}

					ele4 = ele3->FirstChildElement("framerate");
					if (ele4) {
						xres = ele4->QueryIntText(&itmp);
						if (xres == XML_SUCCESS) { piicc->framerate = itmp; framerate = itmp;}
					}

					ele4 = ele3->FirstChildElement("gain");
					if (ele4) {
						xres = ele4->QueryIntText(&itmp);
						if (xres == XML_SUCCESS) { piicc->gain = itmp; }
					}

					ele4 = ele3->FirstChildElement("exposure");
					if (ele4) {
						xres = ele4->QueryIntText(&itmp);
						if (xres == XML_SUCCESS) { piicc->exposure = itmp; }
					}

					ele4 = ele3->FirstChildElement("skip");
					if (ele4) {
						xres = ele4->QueryIntText(&itmp);
						if (xres == XML_SUCCESS) { piicc->skip = itmp; }
					}

					ele4 = ele3->FirstChildElement("multitude");
					if (ele4) {
						xres = ele4->QueryIntText(&itmp);
						if (xres == XML_SUCCESS) { piicc->multitude = itmp; }
					}

					ele4 = ele3->FirstChildElement("syncdiv");
					if (ele4) {
						xres = ele4->QueryIntText(&itmp);
						if (xres == XML_SUCCESS) { piicc->syncdiv = itmp; }
					}
					if (camid >= 0 && camid < NUM_CAM ) {
						iana_cam_property(hiana, camid, piana->hscamif, piicc);
						memcpy(&piana->RS_ic[camid].icf, piicc, sizeof(iana_camconf_t));
						memcpy(&piana->LS_ic[camid].icf, piicc, sizeof(iana_camconf_t));
					} 
				}			// "config"
				ele3 = ele2->FirstChildElement("runconfig");
				if (ele3 != NULL) {
					ele4 = ele3->FirstChildElement("runprocessingwidth");
					if (ele4) {
						xres = ele4->QueryIntText(&itmp);
						if (xres == XML_SUCCESS) { 
							piana->info_runProcessingWidth[camid] = itmp;
						}
					}

					ele4 = ele3->FirstChildElement("runprocessingheight");
					if (ele4) {
						xres = ele4->QueryIntText(&itmp);
						if (xres == XML_SUCCESS) { 
							piana->info_runProcessingHeight[camid] = itmp;
						}
					}
				}
			}
			ele2 = ele2->NextSiblingElement("camera");
		} 	// while(ele2 != NULL),   end of "camera"

		//---
		if (camidCheckReady == 0) {
			if (camidOther < 0) {
				camidOther = 1;
			}
		} else if (camidCheckReady == 1) {
			if (camidOther < 0) {
				camidOther = 0;
			}
		} else {
			camidCheckReady = 0;
			camidOther = 1;
		}

		piana->camidCheckReady = camidCheckReady;
		piana->camidOther = camidOther;

	} // end of  "camrunconfig"

	// ready
	ele1 = ele0->FirstChildElement("ready");
	if (ele1) {
		int camid;

		// camera
		ele2 = ele1->FirstChildElement("camera");
		while(ele2 != NULL) {
			camid = -1;
			xres = ele2->QueryIntAttribute("id", &camid);
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "xres: %d, camid: %d\n", xres, camid);
			if ((xres == XML_SUCCESS) && (camid >= 0 && camid < NUM_CAM)) {
				ele3 = ele2->FirstChildElement("start");
				if (ele3) {
					{
						cr_point_t	*pStartPosL;
						pStartPosL = &piana->info_startPosL[camid];
						ele4 = ele3->FirstChildElement("ballpos");
						if (ele4 != NULL) {
						char *str;
						double xd, yd, zd;

						str = (char *)ele4->GetText();
						if (str) {
							sscanf(str, "%lf %lf %lf", &xd, &yd, &zd);
							pStartPosL->x = xd;
							pStartPosL->y = yd;
							pStartPosL->z = zd;
						}
						} else {
							pStartPosL->x = -999;
							pStartPosL->y = -999;
							pStartPosL->z = -999;
						}
					} // ballpos
					{
						cr_point_t	*pStartPosP;
						pStartPosP = &piana->info_startPosP[camid];
						ele4 = ele3->FirstChildElement("ballposP");
						if (ele4 != NULL) {
							char *str;
							double xd, yd, zd;

							str = (char *)ele4->GetText();
							if (str) {
								sscanf(str, "%lf %lf %lf", &xd, &yd, &zd);
								pStartPosP->x = xd;
								pStartPosP->y = yd;
								pStartPosP->z = zd;
							}
						} else {
							pStartPosP->x = -999;
							pStartPosP->y = -999;
							pStartPosP->z = -999;
						}
					}  // ballposP
					{
						cr_point_t	*pStartPosB;
						pStartPosB = &piana->info_startPosB[camid];
						ele4 = ele3->FirstChildElement("ballposB");
						if (ele4 != NULL) {
							char *str;
							double xd, yd, zd;

							str = (char *)ele4->GetText();
							if (str) {
								sscanf(str, "%lf %lf %lf", &xd, &yd, &zd);
								pStartPosB->x = xd;
								pStartPosB->y = yd;
								pStartPosB->z = zd;
							}
						} else {
							pStartPosB->x = -999;
							pStartPosB->y = -999;
							pStartPosB->z = -999;
					}
					} // ballposB
				}		// end of "start"
				
				// images
				ele3 = ele2->FirstChildElement("images");
				normalbulk = 1;	// 0: NONE, 1: NORMAL, 2: BULK
				if (ele3 != NULL) {
					iana_camconf_t *piicc;
					U08 *imgbuf;
					U32 width, height;
					U32 isfirst;

					// Init image buffer

					if (camid >= 0 && camid < NUM_CAM ) {
						scamif_imagebuf_init(piana->hscamif, camid, normalbulk, &piana->info_icc[camid]);
					}

					piicc = &piana->info_icc[camid];
					width = piicc->width;
					height = piicc->height;
					imgbuf = (U08 *) malloc(width * height);

					ele4 = ele3->FirstChildElement("image");
					isfirst = 1;

					while(ele4 != NULL) {
						int index;
						U32 ts_h, ts_l;
						char *str;
						camimageinfo_t *pcii;
						camif_buffer_info_t  cifb;
						U64 ts64;
						int itmp;

						//--
						pcii = &cifb.cii;
						memcpy(pcii, &piana->info_icc[camid], sizeof(iana_camconf_t));			// Default value... 
						
						index = 0; ts64 = 0; ts_h = ts_l = 0; 
						ele5 = ele4->FirstChildElement("index");
						if (ele5) {
							xres = ele5->QueryIntText(&index);
							if (xres != XML_SUCCESS) { index = 0; }
						}
						if (isfirst) {
							piana->info_firstindex[camid] = index;
							isfirst = 0;
						}
						piana->info_lastindex[camid] = index;

						ele5 = ele4->FirstChildElement("ts64");
						if (ele5) {
							str = (char *)ele5->GetText();
							if (str) { 
								sscanf(str, "%llx", &ts64);
								ts_h = EXTRACTH(ts64);
								ts_l = EXTRACTL(ts64);
							}
						}

						//---------
						ele5 = ele4->FirstChildElement("offset_x");
						if (ele5) {
							xres = ele5->QueryIntText(&itmp);
							if (xres == XML_SUCCESS) { pcii->offset_x = itmp; }
						}

						ele5 = ele4->FirstChildElement("offset_y");
						if (ele5) {
							xres = ele5->QueryIntText(&itmp);
							if (xres == XML_SUCCESS) { pcii->offset_y = itmp; }
						}
						//----------
						
						ele5 = ele4->FirstChildElement("filename");
						if (ele5) {
							int len;
							str = (char *)ele5->GetText();
							len = (int)strlen(str);
							if (len > 4) {
								char *pdat;
								char c0, c1, c2, c3;
								char imgFilePath[2048];									

								Get_ImagePath( fn, str,imgFilePath);
							
								pdat = (str + len - 4);
								c0 = str[len-4+0];
								c1 = str[len-4+1];
								c2 = str[len-4+2];
								c3 = str[len-4+3];
								if (c0 == '.' 
										&& (c1 == 'j' || c1 == 'J')
										&& (c2 == 'p' || c2 == 'P')
										&& (c3 == 'g' || c3 == 'G')
								   ) {		// JPEG
									jpegLoad(piana->hjpeg, imgFilePath, (char *)imgbuf, width, height);
								} else {	// BMP
									bmpGrayLoad(imgFilePath, (char *)imgbuf, width, height);
								}
							} else {
								memset(imgbuf, 0, width * height);
							}

							// Make buffer info
							cifb.state 		= 0;		// Dummy
							cifb.ts_h 		= ts_h;
							cifb.ts_l 		= ts_l;
							cifb.frameindex = 0;		// Dummy
							//memcpy(&cifb.cii, &piana->info_icc[camid], sizeof(iana_camconf_t));


							if (camid >= 0 && camid < NUM_CAM ) {
								scamif_imagebuf_write_raw(piana->hscamif, camid, normalbulk, &cifb, imgbuf, index);
							} 
						}

						//--
						ele4 = ele4->NextSiblingElement("image");
					} 	// while(ele4 != NULL) .. end of "image",  4
					free(imgbuf);
				}		// end of "images",   3

				ele2 = ele2->NextSiblingElement("camera");
			} 	// if ((xres == XML_SUCCESS) && ...
		} 		// end of "camera",		2
	
	} // end of  "ready",	1

	// shot
	ele1 = ele0->FirstChildElement("shot");
	if (ele1) {
		int camid;
		int goodshot;

		// goodshot
		goodshot = 0;
		ele2 = ele1->FirstChildElement("goodshot");
		if (ele2) {
			xres = ele2->QueryIntText(&goodshot);
			if (xres != XML_SUCCESS) { goodshot = 0; }
		}
		piana->info_goodshot = goodshot;
		
	
		// start
		ele2 = ele1->FirstChildElement("start");
		while(ele2 != NULL) {
			camid = -1;
			xres = ele2->QueryIntAttribute("camid", &camid);
			if ((xres == XML_SUCCESS) && (camid >= 0 && camid < NUM_CAM)) {
				{
					int index;
					U32 ts_h, ts_l;
					char *str;
					U64 ts64;

					//--
					index = 0;
					ts64 = 0;
					ts_h = ts_l = 0;
					// ts64
					ele3 = ele2->FirstChildElement("ts64");
					if (ele3) {
						str = (char *)ele3->GetText();
						if (str) {
							sscanf(str, "%llx", &ts64);
						}
					}
					piana->info_startts64[camid] = ts64;
					// index
					ele3 = ele2->FirstChildElement("index");
					if (ele3) {
						xres = ele3->QueryIntText(&index);
						if (xres != XML_SUCCESS) { index = 0; }
					}
					piana->info_startindex[camid] = index;
				}

				{
					cr_point_t	*pStartPosL;
					pStartPosL = &piana->info_startPosL[camid];
					ele3 = ele2->FirstChildElement("ballpos");
					if (ele3 != NULL) {
						char *str;
						double xd, yd, zd;

						str = (char *)ele3->GetText();
						if (str) {
							sscanf(str, "%lf %lf %lf", &xd, &yd, &zd);
							pStartPosL->x = xd;
							pStartPosL->y = yd;
							pStartPosL->z = zd;
						}
					} else {
						pStartPosL->x = -999;
						pStartPosL->y = -999;
						pStartPosL->z = -999;
					}
				} // ballpos
				{
					cr_point_t	*pStartPosP;
					pStartPosP = &piana->info_startPosP[camid];
					ele3 = ele2->FirstChildElement("ballposP");
					if (ele3 != NULL) {
						char *str;
						double xd, yd, zd;

						str = (char *)ele3->GetText();
						if (str) {
							sscanf(str, "%lf %lf %lf", &xd, &yd, &zd);
							pStartPosP->x = xd;
							pStartPosP->y = yd;
							pStartPosP->z = zd;
						}
					} else {
						pStartPosP->x = -999;
						pStartPosP->y = -999;
						pStartPosP->z = -999;
					}
				}  // ballposP
				{
					cr_point_t	*pStartPosB;
					pStartPosB = &piana->info_startPosB[camid];
					ele3 = ele2->FirstChildElement("ballposB");
					if (ele3 != NULL) {
						char *str;
						double xd, yd, zd;

						str = (char *)ele3->GetText();
						if (str) {
							sscanf(str, "%lf %lf %lf", &xd, &yd, &zd);
							pStartPosB->x = xd;
							pStartPosB->y = yd;
							pStartPosB->z = zd;
						}
					} else {
						pStartPosB->x = -999;
						pStartPosB->y = -999;
						pStartPosB->z = -999;
					}
				} // ballposB
			}
			ele2 = ele2->NextSiblingElement("start");
		}			// end of "start"
	} // end of  "shot",	1


	// spin
	ele1 = ele0->FirstChildElement("spin");
	if (ele1) {
		// bulkimages
		ele2 = ele1->FirstChildElement("bulkimages");
		normalbulk = 2;	// 0: NONE, 1: NORMAL, 2: BULK
		if (ele2) {
			// camera
			ele3 = ele2->FirstChildElement("camera");
			while(ele3 != NULL) {
				int camid;
				iana_camconf_t *piicc;
				U08 *imgbuf;
				U32 width, height;
				U32 isfirst;

				//--
				camid = -1;
				xres = ele3->QueryIntAttribute("id", &camid);
				if ((xres == XML_SUCCESS) && (camid >= 0 && camid < NUM_CAM)) {
					piicc = &piana->info_icc_bulk[camid];
					//memcpy(piicc, &piana->pic[camid]->icf, sizeof(iana_camconf_t));
					if (piana->Right0Left1 == 0) {
						memcpy(piicc, &piana->RS_ic[camid].icf, sizeof(iana_camconf_t));
					} else {
						memcpy(piicc, &piana->LS_ic[camid].icf, sizeof(iana_camconf_t));
					}

					ele4 = ele3->FirstChildElement("config");					// 
					if (ele4) {
						int itmp;

						ele5 = ele4->FirstChildElement("width");
						if (ele5) {
							xres = ele5->QueryIntText(&itmp);
							if (xres == XML_SUCCESS) { piicc->width = itmp; }
						}

						ele5 = ele4->FirstChildElement("height");
						if (ele5) {
							xres = ele5->QueryIntText(&itmp);
							if (xres == XML_SUCCESS) { piicc->height = itmp; }
						}

						ele5 = ele4->FirstChildElement("offset_x");
						if (ele5) {
							xres = ele5->QueryIntText(&itmp);
							if (xres == XML_SUCCESS) { piicc->offset_x = itmp; }
						}

						ele5 = ele4->FirstChildElement("offset_y");
						if (ele5) {
							xres = ele5->QueryIntText(&itmp);
							if (xres == XML_SUCCESS) { piicc->offset_y = itmp; }
						}

						ele5 = ele4->FirstChildElement("framerate");
						if (ele5) {
							xres = ele5->QueryIntText(&itmp);
							if (xres == XML_SUCCESS) { 
								piicc->framerate = itmp; 
								if (camid != 2) {   // ATTACK CAM �� frame rate �� skip.
									framerate = itmp;
								}
							}
						}

						ele5 = ele4->FirstChildElement("gain");
						if (ele5) {
							xres = ele5->QueryIntText(&itmp);
							if (xres == XML_SUCCESS) { piicc->gain = itmp; }
						}

						ele5 = ele4->FirstChildElement("exposure");
						if (ele5) {
							xres = ele5->QueryIntText(&itmp);
							if (xres == XML_SUCCESS) { piicc->exposure = itmp; }
						}

						ele5 = ele4->FirstChildElement("skip");
						if (ele5) {
							xres = ele5->QueryIntText(&itmp);
							if (xres == XML_SUCCESS) { piicc->skip = itmp; }
						}

						ele5 = ele4->FirstChildElement("multitude");
						if (ele5) {
							xres = ele5->QueryIntText(&itmp);
							if (xres == XML_SUCCESS) { piicc->multitude = itmp; }
						}

						ele5 = ele4->FirstChildElement("syncdiv");
						if (ele5) {
							xres = ele5->QueryIntText(&itmp);
							if (xres == XML_SUCCESS) { piicc->syncdiv = itmp; }
						}
						//						iana_cam_property(hiana, camid, piicc);
						// end of "config"

						// Init image buffer
						
						
						if (camid >= 0 && camid < NUM_CAM ) {
							scamif_imagebuf_init(piana->hscamif, camid, normalbulk, piicc);		// bulk buffer..
						} 

						width = piicc->width;
						height = piicc->height;
						imgbuf = (U08 *) malloc(width * height);

						// image
						isfirst = 1;
						ele4 = ele3->FirstChildElement("image");					// 
						while(ele4 != NULL) {
							int index;
							U32 ts_h, ts_l;
							char *str;
						//camimageinfo_t *pcii;
							camif_buffer_info_t  cifb;
							U64 ts64;
//							int itmp;


							//--
							//pcii = &cifb.cii;
							//memcpy(pcii, &piana->info_icc[camid], sizeof(iana_camconf_t));			// Default value... 

							index = 0; ts64 = 0; ts_h = ts_l = 0; 

							ele5 = ele4->FirstChildElement("index");
							if (ele5) {
								xres = ele5->QueryIntText(&index);
								if (xres != XML_SUCCESS) { index = 0; }
							}

							if (isfirst) {
								piana->info_firstindex_bulk[camid] = index;
								isfirst = 0;
							}
							piana->info_lastindex_bulk[camid] = index;

							ele5 = ele4->FirstChildElement("ts64");
							if (ele5) {
								str = (char *)ele5->GetText();
								if (str) { 
									sscanf(str, "%llx", &ts64);
									ts_h = EXTRACTH(ts64);
									ts_l = EXTRACTL(ts64);
								}
							}

							//---------
							ele5 = ele4->FirstChildElement("offset_x");
							if (ele5) {
								xres = ele5->QueryIntText(&itmp);
								//if (xres == XML_SUCCESS) { pcii->offset_x = itmp; }
								if (xres == XML_SUCCESS) { piicc->offset_x = itmp; }
							}

							ele5 = ele4->FirstChildElement("offset_y");
							if (ele5) {
								xres = ele5->QueryIntText(&itmp);
								//if (xres == XML_SUCCESS) { pcii->offset_y = itmp; }
								if (xres == XML_SUCCESS) { piicc->offset_y = itmp; }
							}
							//----------

							ele5 = ele4->FirstChildElement("filename");
							if (ele5) {
								int len;
								str = (char *)ele5->GetText();

								len = (int)strlen(str);
								if (len > 4) {
									char *pdat;
									char c0, c1, c2, c3;
									char imgFilePath[2048]; 								

									Get_ImagePath( fn, str,imgFilePath);

									pdat = (str + len - 4);
									c0 = str[len-4+0];
									c1 = str[len-4+1];
									c2 = str[len-4+2];
									c3 = str[len-4+3];
									if (c0 == '.' 
											&& (c1 == 'j' || c1 == 'J')
											&& (c2 == 'p' || c2 == 'P')
											&& (c3 == 'g' || c3 == 'G')
									   ) {		// JPEG

										jpegLoad(piana->hjpeg, imgFilePath, (char *)imgbuf, width, height);
									} else {	// BMP
										bmpGrayLoad(imgFilePath, (char *)imgbuf, width, height);
									}
								} else {
									memset(imgbuf, 0, width * height);
								}

								// Make buffer info
								cifb.state 		= 0;		// Dummy
								cifb.ts_h 		= ts_h;
								cifb.ts_l 		= ts_l;
								cifb.frameindex = 0;		// Dummy
								// memcpy(&cifb.cii, piicc, sizeof(iana_camconf_t));
								memcpy(&cifb.cii, piicc, sizeof(iana_camconf_t));

								if (camid >= 0 && camid < NUM_CAM ) {
									scamif_imagebuf_write_raw(piana->hscamif, camid, normalbulk, &cifb, imgbuf, index);
								} 
							}
							//--
							ele4 = ele4->NextSiblingElement("image");
						} 	// while(ele4 != NULL) .. end of "image",  4
						free(imgbuf);
					} 	// if (ele4) .... end of config
				} 			// if ((xres == XML_SUCCESS) && (camid >= 0 && camid <= 1)) 
				ele3 = ele3->NextSiblingElement("camera");
			}	// end of "camera"
		} // end of "bulkimages"
	} // end of "spin"

	ele1 = ele0->FirstChildElement("features");
	if (ele1) { // Read feature.. 
		ele2 = ele1->FirstChildElement("camera");
		while(ele2 != NULL) {
			int camid;
			camid = -1;
			xres = ele2->QueryIntAttribute("id", &camid);
			if ((xres == XML_SUCCESS) && (camid >= 0 && camid < NUM_CAM)) {
				iana_cam_t		*pic;
				marksequence_t	*pmks;
				char *str;

				//--
				pic	= piana->pic[camid];
				pmks= &pic->mks;

				ele3 = ele2->FirstChildElement("feature");
				while(ele3 != NULL) {
					int seqnum;
					ele4 = ele3->FirstChildElement("index");
					xres = ele4->QueryIntText(&seqnum);
					if (xres == XML_SUCCESS && seqnum >= 0 && seqnum <= MARKSEQUENCELEN) { 
						markelement2_t *pmke2d;
						markelement2_t *pmke2m;
						double dtmp0, dtmp1, dtmp2;
						
						pmke2d = &piana->mke2DimpleInfo[camid][seqnum];
						pmke2m = &piana->mke2MarkInfo[camid][seqnum];
						ele4 = ele3->FirstChildElement("ballposP");
						if (ele4) {
							str = (char *)ele4->GetText();
							if (str) {
								sscanf(str, "%lf %lf", &dtmp0, &dtmp1);
								piana->ballposP2Info[camid][seqnum].x = dtmp0;
								piana->ballposP2Info[camid][seqnum].y = dtmp1;
							}
						}
						ele4 = ele3->FirstChildElement("ballRP");
						if (ele4) {
							str = (char *)ele4->GetText();
							if (str) {
								sscanf(str, "%lf", &dtmp0);
								piana->ballrP2Info[camid][seqnum] = dtmp0;
							}
						}


						ele4 = ele3->FirstChildElement("ballpos3D");
						if (ele4) {
							str = (char *)ele4->GetText();
							if (str) {
								sscanf(str, "%lf %lf %lf", &dtmp0, &dtmp1, &dtmp2);
								piana->ballposL3DInfo[camid][seqnum].x = dtmp0;
								piana->ballposL3DInfo[camid][seqnum].y = dtmp1;
								piana->ballposL3DInfo[camid][seqnum].z = dtmp2;
							}
						}

						ele4 = ele3->FirstChildElement("dimple");
						if (ele4 != NULL) {
							int vectcount;
							xres = ele4->QueryIntAttribute("count", &vectcount);
							if (xres == XML_SUCCESS && vectcount > 0 && vectcount <= MARKCOUNT2) {
								int i;

								ele5 = ele4->FirstChildElement("vect");
								for (i = 0; i < vectcount; i++) {
									int vectindex;
									if (ele5 == NULL) {
										break;
									}
									xres = ele5->QueryIntAttribute("index", &vectindex);
									if (i != vectindex) {
										// what?
									}
									if (xres == XML_SUCCESS && vectindex <= vectcount) {
										str = (char *)ele5->GetText();
										if (str) {
											U64 	*ptmp640, *ptmp641, *ptmp642;

											ptmp640 = (U64 *) &pmke2d->L3bc[i].x;
											ptmp641 = (U64 *) &pmke2d->L3bc[i].y;
											ptmp642 = (U64 *) &pmke2d->L3bc[i].z;
											sscanf(str, "%llx %llx %llx", 
													ptmp640,
													ptmp641,
													ptmp642);
										}
										pmke2d->dotcount++;
									}
									ele5 = ele5->NextSiblingElement("vect");
								}
								pmke2d->state = 1;
							}
						}	// dimple

						ele4 = ele3->FirstChildElement("othermark");
						if (ele4 != NULL) {
							int vectcount;
							xres = ele4->QueryIntAttribute("count", &vectcount);
							if (xres == XML_SUCCESS && vectcount > 0 && vectcount <= MARKCOUNT2) {
								int i;

								ele5 = ele4->FirstChildElement("vect");
								for (i = 0; i < vectcount; i++) {
									int vectindex;
									if (ele5 == NULL) {
										break;
									}
									xres = ele5->QueryIntAttribute("index", &vectindex);
									if (i != vectindex) {
										// what?
									}
//									if (xres == XML_SUCCESS && vectcount <= vectcount) { --> self-comparison always evaluates to true!!
										if (xres == XML_SUCCESS) {
										str = (char *)ele5->GetText();
										if (str) {
											U64 	*ptmp640, *ptmp641, *ptmp642;

											ptmp640 = (U64 *) &pmke2m->L3bc[i].x;
											ptmp641 = (U64 *) &pmke2m->L3bc[i].y;
											ptmp642 = (U64 *) &pmke2m->L3bc[i].z;
											sscanf(str, "%llx %llx %llx", 
													ptmp640,
													ptmp641,
													ptmp642);
										}
										pmke2m->dotcount++;
									}
									ele5 = ele5->NextSiblingElement("vect");
								}
								pmke2m->state = 1;
							}
						}	// Othermark
					}
					ele3 = ele3->NextSiblingElement("feature");
				} 	// end of "feature"
			}
			ele2 = ele2->NextSiblingElement("camera");
		}
	} // end of "features"

	// shotresult
	ele1 = ele0->FirstChildElement("shotresult");
	if (ele1) {
		iana_shotresult_t *psrt;
		double dtmp;
		char *str;

		psrt = &piana->info_shotresult;
		psrt->valid = piana->info_goodshot;

		ele2 = ele1->FirstChildElement("vmag");
		if (ele2) {
			xres = ele2->QueryDoubleText(&dtmp);
			if (xres == XML_SUCCESS) { psrt->vmag = dtmp; }
		}

		ele2 = ele1->FirstChildElement("incline");
		if (ele2) {
			xres = ele2->QueryDoubleText(&dtmp);
			if (xres == XML_SUCCESS) { psrt->incline = dtmp; }
		}

		ele2 = ele1->FirstChildElement("azimuth");
		if (ele2) {
			xres = ele2->QueryDoubleText(&dtmp);
			if (xres == XML_SUCCESS) { psrt->azimuth = dtmp; }
		}

		ele2 = ele1->FirstChildElement("sidespin");
		if (ele2) {
			xres = ele2->QueryDoubleText(&dtmp);
			if (xres == XML_SUCCESS) { psrt->sidespin = dtmp; }
		}

		ele2 = ele1->FirstChildElement("backspin");
		if (ele2) {
			xres = ele2->QueryDoubleText(&dtmp);
			if (xres == XML_SUCCESS) { psrt->backspin = dtmp; }
		}

		ele2 = ele1->FirstChildElement("spinmag");
		if (ele2) {
			xres = ele2->QueryDoubleText(&dtmp);
			if (xres == XML_SUCCESS) { psrt->spinmag = dtmp; }
		}

		ele2 = ele1->FirstChildElement("spinaxis");

		if (ele2) {
			str = (char *)ele2->GetText();
			if (str) {
				sscanf(str, "%lf %lf %lf", &psrt->axis.x, &psrt->axis.y, &psrt->axis.z);
			}
		}
	} // end of "shotresult"

	if (framerate != 1) {
		piana->framerate = framerate;
	}

	delete pdoc;
	res = 1;

	LOCAL_DEBUG(("%s: END(OK)\n", __func__));
func_exit:
	return res;
}









//#if defined (__cplusplus)
//}
//#endif



