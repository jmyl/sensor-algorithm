/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA configuration read/write
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_configure.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2016/02/22 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#if defined(_WIN32)
#include <tchar.h>
#endif
#include <math.h>
#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"

#include "scamif.h"

#include "iana.h"
// #include "iana_tool.h"
#include "iana_coordinate.h"
#include "iana_implement.h"
#include "iana_configure.h"
//#include "iana_cam.h"
//#include "iana_cam_implement.h"
//#include "iana_work.h"
/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define CONFIGURE_INI
//#define CONFIGURE_XML


#define INI_FILENAME			TEXT("scam.ini")





#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
//static I32 AreaConversionAndCheck(iana_t *piana, U32 camid, cr_rectL_t *prectL, cr_rect_t *prect, U32 right0left1);
static I32 AreaConversionAndCheck(iana_t *piana, U32 camid, cr_rectL_t *prectL, cr_rect_t *prect);

static U32 getExtendedArea( cr_point_t *CamPos, cr_rectL_t *prectLArea, cr_rectL_t *prectLAreaExt, double zvalue);
/*!
 ********************************************************************************
 *	@brief      Read Configuration file
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/02/22
 *******************************************************************************/
I32 iana_config_read(iana_t *piana)
{
	I32 res;

#if defined(CONFIGURE_INI)
	res =  iana_config_read_INI(piana);
#elif defined(CONFIGURE_XML)
#error NOT YET..
#else
	res = 1;
#endif

	return res;
}

/*!
 ********************************************************************************
 *	@brief      Write Configuration file
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/02/22
 *******************************************************************************/
I32 iana_config_write(iana_t *piana)
{
	I32 res;

#if defined(CONFIGURE_INI)
	res =  iana_config_write_INI(piana);
#elif defined(CONFIGURE_XML)
#error NOT YET..
#else
	res = 1;
#endif

	return res;
}

#if defined(CONFIGURE_INI)
/*!
 ********************************************************************************
 *	@brief      Read INI Configuration file
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/02/22
 *******************************************************************************/
I32 iana_config_read_INI(iana_t *piana)
{
	U32 i;
	U32 rl;
//#define BUFLEN		2048
#define BUFLEN		PATHBUFLEN
	//CHR szEXEPath[BUFLEN];
	CHR inifile[BUFLEN];

	//CHR szDir[BUFLEN];
	//CHR szDrive[BUFLEN];
	//CHR szFileName[BUFLEN];
	//CHR szFileExt[BUFLEN];

	CHR	buf[64];
	CHR	prefixbuf[64];
	CHR	namebuf[64];

	//-------------------------------------------------------------------------------------------
	// Get INI file.
	//-------------------------------------------------------------------------------------------

#if defined(_WIN32)						
	cr_sprintf(inifile, TEXT("%s%s\\%s"), piana->szDrive, piana->szDir, INI_FILENAME);
#else
	cr_sprintf(inifile, TEXT("%s%s/%s"), piana->szDrive, piana->szDir, INI_FILENAME);
#endif

	cr_sprintf(prefixbuf, TEXT("CONFIG"));

	//-- TEE Area
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("SHOTAREA"));

	OSAL_FILE_GetIniString(namebuf, TEXT("RUNMODE"), TEXT("1"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%d"), &piana->shotarearunmode);		// 1: Tee/Iron, both enable,  0: separated.


	//-------------------------------------------------------------------------------------------
	// CAMERA
	cr_sprintf(prefixbuf, TEXT("CAMERA"));

	//-- TEE Area
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("AREA::TEE"));

	OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("-0.05"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLTee.sx);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("-0.05"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLTee.sy);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.05"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLTee.ex);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.05"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLTee.ey);

	//-- Iron Area
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("AREA::IRON"));

	//OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("-0.25"), buf, 60, inifile);
	//cr_sscanf(buf, TEXT("%lf"), &piana->rectLIron.sx);
	//OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("-0.40"), buf, 60, inifile);
	//cr_sscanf(buf, TEXT("%lf"), &piana->rectLIron.sy);
	//OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.10"), buf, 60, inifile);
	//cr_sscanf(buf, TEXT("%lf"), &piana->rectLIron.ex);
	//OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.40"), buf, 60, inifile);
	//cr_sscanf(buf, TEXT("%lf"), &piana->rectLIron.ey);


	//OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("-0.35"), buf, 60, inifile);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("-0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLIron.sx);
	//OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("-0.40"), buf, 60, inifile);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("-0.45"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLIron.sy);
	//OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.35"), buf, 60, inifile);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLIron.ex);
	//OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.40"), buf, 60, inifile);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.45"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLIron.ey);


	//-- Putter Area
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("AREA::PUTTER"));

//	OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("-0.25"), buf, 60, inifile);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("-0.35"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLPutter.sx);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("-0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLPutter.sy);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.10"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLPutter.ex);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLPutter.ey);

	//-------------------------------------------------------------------------------------------
	// LS_CAMERA
	cr_sprintf(prefixbuf, TEXT("LS_CAMERA"));

	//-- TEE Area
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("AREA::TEE"));

	OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("-0.05"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLTee.sx);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("-0.05"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLTee.sy);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.05"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLTee.ex);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.05"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLTee.ey);

	//-- Iron Area
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("AREA::IRON"));

	//OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("-0.25"), buf, 60, inifile);
	//cr_sscanf(buf, TEXT("%lf"), &piana->rectLIron.sx);
	//OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("-0.40"), buf, 60, inifile);
	//cr_sscanf(buf, TEXT("%lf"), &piana->rectLIron.sy);
	//OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.10"), buf, 60, inifile);
	//cr_sscanf(buf, TEXT("%lf"), &piana->rectLIron.ex);
	//OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.40"), buf, 60, inifile);
	//cr_sscanf(buf, TEXT("%lf"), &piana->rectLIron.ey);


	//OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("-0.35"), buf, 60, inifile);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("-0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLIron.sx);
	//OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("-0.40"), buf, 60, inifile);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("-0.45"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLIron.sy);
	//OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.35"), buf, 60, inifile);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLIron.ex);
	//OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.40"), buf, 60, inifile);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.45"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLIron.ey);


	//-- Putter Area
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("AREA::PUTTER"));

//	OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("-0.25"), buf, 60, inifile);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("-0.35"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLPutter.sx);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("-0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLPutter.sy);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.10"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLPutter.ex);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLPutter.ey);


	//-------------------------------------------------------------------------------------------
	cr_sprintf(prefixbuf, TEXT("SENSOR"));

	//-- System
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("SYSTEM"));
	piana->camsensor_category_ini	= OSAL_FILE_GetIniInteger(namebuf, TEXT("CATEGORY"), CAMSENSOR_CBALL, inifile);		// Sensor category

	piana->useLeftHanded			= OSAL_FILE_GetIniInteger(namebuf, TEXT("UseLeftHanded"), 0, inifile);				// 
	piana->leftHandedType			= OSAL_FILE_GetIniInteger(namebuf, TEXT("LeftHandedType"), 1, inifile);				// 

	piana->oneareamode				= OSAL_FILE_GetIniInteger(namebuf, TEXT("OneAreaMode"), 0, inifile);	
////////////////////	piana->useLeftHanded			= 1;		FAKE TEST... 

	piana->RS_CamPosEstimatedLValid	= OSAL_FILE_GetIniInteger(namebuf, TEXT("CamPosEstimatedLValid"), 0, inifile);		// Camera Position Estimation Valid..
	piana->LS_CamPosEstimatedLValid	= OSAL_FILE_GetIniInteger(namebuf, TEXT("LS_CamPosEstimatedLValid"), 0, inifile);		// Camera Position Estimation Valid..

	piana->RS_UseProjectionPlane		= OSAL_FILE_GetIniInteger(namebuf, TEXT("UseProjectionPlane"), 0, inifile);		// Use Projection Plane or Not..
	piana->LS_UseProjectionPlane		= OSAL_FILE_GetIniInteger(namebuf, TEXT("LS_UseProjectionPlane"), 0, inifile);		// Use Projection Plane or Not..
	
	piana->RS_UseAttackCam = OSAL_FILE_GetIniInteger(namebuf, TEXT("UseAttackCam"), 0, inifile);		// Use attack cam or not..
	piana->LS_UseAttackCam = OSAL_FILE_GetIniInteger(namebuf, TEXT("LS_UseAttackCam"), 0, inifile);		// Use attack cam or not..

	piana->powersavingtime			= OSAL_FILE_GetIniInteger(namebuf, TEXT("POWERSAVINGTIME"), (2*60*1000), inifile);	// 
	piana->powersaving				= OSAL_FILE_GetIniInteger(namebuf, TEXT("POWERSAVING"), 1, inifile);				// 

	piana->testmode					= OSAL_FILE_GetIniInteger(namebuf, TEXT("TESTMODE"), 0, inifile);				// 20210118
#define USE_OTHERAREA
	//-------------------------------------------------------------------------------
#if defined(USE_OTHERAREA)
	//-------------------------------------------------------------------------------
	//-- Mat..
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("OTHERAREA"));				// MAT
	piana->RS_RoughBunkerMat = OSAL_FILE_GetIniInteger(namebuf, TEXT("UseOtherArea"), 0, inifile);		// UseMat. Use Mat or not..

	//-- Rough Mat
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("OTHERAREA::AREA1"));		// MAT::ROUGHAREA
	OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLRough.sx);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLRough.sy);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLRough.ex);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLRough.ey);

	//-- Bunker Mat
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("OTHERAREA::AREA2"));		// MAT::BUNKERAREA
	OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLBunker.sx);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLBunker.sy);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLBunker.ex);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLBunker.ey);


	//-- LS_Mat..
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("LS_OTHERAREA"));				// LS_MAT
	piana->LS_RoughBunkerMat = OSAL_FILE_GetIniInteger(namebuf, TEXT("UseOtherArea"), 0, inifile);		// UseMat. Use Mat or not..

	//-- LS Rough Mat
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("LS_OTHERAREA::AREA1"));
	OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLRough.sx);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLRough.sy);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLRough.ex);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLRough.ey);

	//-- LS Bunker Mat
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("LS_SPECIALAREA::AREA2"));
	OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLBunker.sx);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLBunker.sy);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLBunker.ex);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLBunker.ey);
	//-------------------------------------------------------------------------------
#else 				// defined(USE_OTHERAREA)
	//-------------------------------------------------------------------------------
	//-- Mat..
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("MAT"));
	piana->RS_RoughBunkerMat = OSAL_FILE_GetIniInteger(namebuf, TEXT("UseMat"), 0, inifile);		// Use Mat or not..

	//-- Rough Mat
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("MAT::ROUGHAREA"));
	OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLRough.sx);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLRough.sy);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLRough.ex);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLRough.ey);

	//-- Bunker Mat
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("MAT::BUNKERAREA"));
	OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLBunker.sx);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLBunker.sy);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLBunker.ex);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->RS_rectLBunker.ey);


	//-- LS_Mat..
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("LS_MAT"));
	piana->LS_RoughBunkerMat = OSAL_FILE_GetIniInteger(namebuf, TEXT("UseMat"), 0, inifile);		// Use Mat or not..

	//-- LS Rough Mat
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("LS_MAT::ROUGHAREA"));
	OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLRough.sx);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLRough.sy);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLRough.ex);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLRough.ey);

	//-- LS Bunker Mat
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("LS_MAT::BUNKERAREA"));
	OSAL_FILE_GetIniString(namebuf, TEXT("Sx"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLBunker.sx);
	OSAL_FILE_GetIniString(namebuf, TEXT("Sy"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLBunker.sy);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ex"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLBunker.ex);
	OSAL_FILE_GetIniString(namebuf, TEXT("Ey"), TEXT("0.40"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->LS_rectLBunker.ey);
	//-------------------------------------------------------------------------------
#endif			// defined(USE_OTHERAREA)
	//-------------------------------------------------------------------------------

	//-- Config
	cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("CONFIG"));
#define AIRPRESSURE_PA_DEFAULT				101325
	piana->airpressure_PA = 
		OSAL_FILE_GetIniInteger(namebuf, TEXT("AIRPRESSURE_PA"), AIRPRESSURE_PA_DEFAULT , inifile);		// Air pressure, pascal.
	piana->airpressure_PA_valid	= 
		OSAL_FILE_GetIniInteger(namebuf, TEXT("AIRPRESSURE_PA_VALID"), 0 , inifile);

#define AIRPRESSURE_PSI_DEFAULT				14.696
	OSAL_FILE_GetIniString(namebuf, TEXT("AIRPRESSURE_PSI"), TEXT("-999"), buf, 60, inifile);
	cr_sscanf(buf, TEXT("%lf"), &piana->airpressure_PSI);
	if (piana->airpressure_PSI < 0.0) {
		piana->airpressure_PSI  = AIRPRESSURE_PSI_DEFAULT;
	}
	piana->airpressure_PSI_valid = 
		OSAL_FILE_GetIniInteger(namebuf, TEXT("AIRPRESSURE_PSI_VALID"), 0 , inifile);

	piana->airpressure_altitude_m = 
		OSAL_FILE_GetIniInteger(namebuf, TEXT("AIRPRESSURE_ALTITUDE_M"), 0 , inifile);		// Altitude, m
	{
		double a_m;

		a_m = piana->airpressure_altitude_m;
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "airpressure_altitude_m: %d, %lf\n", piana->airpressure_altitude_m, a_m);
	}
	piana->airpressure_altitude_m_valid	= 
		OSAL_FILE_GetIniInteger(namebuf, TEXT("AIRPRESSURE_ALTITUDE_M_VALID"), 0 , inifile);

	piana->airpressure_altitude_feet = 
		OSAL_FILE_GetIniInteger(namebuf, TEXT("AIRPRESSURE_ALTITUDE_FEET"), 0 , inifile);		// Altitude, feet
	piana->airpressure_altitude_feet_valid	= 
		OSAL_FILE_GetIniInteger(namebuf, TEXT("AIRPRESSURE_ALTITUDE_FEET_VALID"), 0 , inifile);

	//-----------------------------------------------------------

	for (rl = 0; rl < 2; rl++) {							// rl == 0: Right handed,  1: left handed
		for (i = 0; i < NUM_CAM; i++) {
			iana_cam_param_t *picp;
			if (i == 0) {
				if (rl == 0) {		// Right
					cr_sprintf(prefixbuf, TEXT("CAMERA_CENTER"));
				} else {			// Left
					cr_sprintf(prefixbuf, TEXT("LS_CAMERA_CENTER"));
				}
			} else if (i == 1) {
				if (rl == 0) {		// Right
					cr_sprintf(prefixbuf, TEXT("CAMERA_SIDE"));
				} else {			// Left
					cr_sprintf(prefixbuf, TEXT("LS_CAMERA_SIDE"));
				}
			} else if (i == 2) {
				if (rl == 0) {		// Right
					cr_sprintf(prefixbuf, TEXT("CAMERA_ATTACK"));
				} else {			// Left
					cr_sprintf(prefixbuf, TEXT("LS_CAMERA_ATTACK"));
				}
			}

			if (rl == 0) {		// Right
				picp = &piana->RS_ic[i].icp;
			} else {			// Left
				picp = &piana->LS_ic[i].icp;
			}
			//---- Camera config
			cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("CONFIG"));
			OSAL_FILE_GetIniString(namebuf, TEXT("GAINMULT"), TEXT("1.0"), buf, 60, inifile);
			cr_sscanf(buf, TEXT("%lf"), &picp->gainmult);


			picp->m_ledbrgt = OSAL_FILE_GetIniInteger(namebuf, TEXT("LEDBRGT"), 	120, inifile);	
			picp->m_bright 	= OSAL_FILE_GetIniInteger(namebuf, TEXT("BRIGHT"), 	100, inifile);	
			picp->m_contrast= OSAL_FILE_GetIniInteger(namebuf, TEXT("CONTRAST"), 	120, inifile);	
			picp->m_gamma 	= OSAL_FILE_GetIniInteger(namebuf, TEXT("GAMMA"), 		 70, inifile);	

			//---- Ball
			cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("BALL"));

			//OSAL_FILE_GetIniString(namebuf, TEXT("CE"), TEXT("0.15"), buf, 60, inifile);
			//cr_sscanf(buf, TEXT("%lf"), &picp->CerBallexist);

			OSAL_FILE_GetIniString(namebuf, TEXT("CE"), TEXT("0.20"), buf, 60, inifile);
			cr_sscanf(buf, TEXT("%lf"), &picp->CerBallexist);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "CE<%d:%d> = %lf\n", rl, i,  picp->CerBallexist);


			OSAL_FILE_GetIniString(namebuf, TEXT("CS"), TEXT("0.15"), buf, 60, inifile);
			//OSAL_FILE_GetIniString(namebuf, TEXT("CS"), TEXT("0.20"), buf, 60, inifile);
			cr_sscanf(buf, TEXT("%lf"), &picp->CerBallshot);
			//#define MAXCERBALLSHOT	0.115
			//#define MAXCERBALLSHOT	0.099
			//#define MAXCERBALLSHOT	0.15
#define MAXCERBALLSHOT	0.20
			if (picp->CerBallshot > MAXCERBALLSHOT) {
				picp->CerBallshot = MAXCERBALLSHOT;
			}
			OSAL_FILE_GetIniString(namebuf, TEXT("BMAX"), TEXT("50"), buf, 60, inifile);
			cr_sscanf(buf, TEXT("%lf"), &picp->BallsizeMax);

			OSAL_FILE_GetIniString(namebuf, TEXT("BMIN"), TEXT("20"), buf, 60, inifile);
			cr_sscanf(buf, TEXT("%lf"), &picp->BallsizeMin);

			cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("CAMERA"));

			// Camera Position
			OSAL_FILE_GetIniString(namebuf, TEXT("PosX"), TEXT("0.0"), buf, 60, inifile);
			cr_sscanf(buf, TEXT("%lf"), &picp->CamPosL.x);

			OSAL_FILE_GetIniString(namebuf, TEXT("PosY"), TEXT("0.0"), buf, 60, inifile);
			cr_sscanf(buf, TEXT("%lf"), &picp->CamPosL.y);

			OSAL_FILE_GetIniString(namebuf, TEXT("PosZ"), TEXT("3.0"), buf, 60, inifile);
			cr_sscanf(buf, TEXT("%lf"), &picp->CamPosL.z);

			// Camera Position Estimation Valid
			//		picp->UseProjectionPlane		= OSAL_FILE_GetIniInteger(namebuf, TEXT("UseProjectionPlane"), 0, inifile);		// Use Projection Plane or Not..


			// Camera Position Estimation.
			OSAL_FILE_GetIniString(namebuf, TEXT("PosEstX"), TEXT("0.0"), buf, 60, inifile);
			cr_sscanf(buf, TEXT("%lf"), &picp->CamPosEstimatedL.x);

			OSAL_FILE_GetIniString(namebuf, TEXT("PosEstY"), TEXT("0.0"), buf, 60, inifile);
			cr_sscanf(buf, TEXT("%lf"), &picp->CamPosEstimatedL.y);

			OSAL_FILE_GetIniString(namebuf, TEXT("PosEstZ"), TEXT("3.0"), buf, 60, inifile);
			cr_sscanf(buf, TEXT("%lf"), &picp->CamPosEstimatedL.z);



			//--- Projection Plane Center.	2018/0314
			{
				float ftmp;

				//--
				OSAL_FILE_GetIniString(namebuf, TEXT("PPlaneX"), TEXT("0"), buf, 60, inifile);
				cr_sscanf(buf, TEXT("%f"), &ftmp);
				picp->ProjectionPlaneCenter.x = ftmp;

				OSAL_FILE_GetIniString(namebuf, TEXT("PPlaneY"), TEXT("0"), buf, 60, inifile);
				cr_sscanf(buf, TEXT("%f"), &ftmp);
				picp->ProjectionPlaneCenter.y = ftmp;

				OSAL_FILE_GetIniString(namebuf, TEXT("PPlaneZ"), TEXT("0"), buf, 60, inifile);
				cr_sscanf(buf, TEXT("%f"), &ftmp);
				picp->ProjectionPlaneCenter.z = ftmp;
			}

			//--- Camera Extrinsic Parameter
			{
				picp->CamExtrinsicValid = OSAL_FILE_GetIniInteger(namebuf, TEXT("ExtrinsicParamValid"), 0, inifile);
				if (picp->CamExtrinsicValid) {
					cameraextrinsic_t *pcep;
					double *protationVector;
					double *ptranslationVector;
					double *pr;
					double *pextMat;
					double *pInvextMat;

					U64    *p64;

					CHR	buf2[2048];



					//--
					pcep = &picp->cep;
					protationVector 	= &pcep->rotationVector[0];
					ptranslationVector 	= &pcep->translationVector[0];
					pr 					= &pcep->r[0][0];
					pextMat 			= &pcep->extMat[0][0];
					pInvextMat 			= &pcep->InvextMat[0][0];

					OSAL_FILE_GetIniString(namebuf, TEXT("rotationVector"), 	TEXT(""), buf2, 2000, inifile); 
					p64 = (U64 *)&protationVector[0];
					cr_sscanf(buf2, TEXT("%llx %llx %llx"), 
							p64, p64+1, p64+2);

					OSAL_FILE_GetIniString(namebuf, TEXT("translationVector"), TEXT(""), buf2, 2000, inifile); 
					p64 = (U64 *)&ptranslationVector[0];
					cr_sscanf(buf2, TEXT("%llx %llx %llx"), 
							p64, p64+1, p64+2);


					OSAL_FILE_GetIniString(namebuf, TEXT("rotationMatrix"), 	TEXT(""), buf2, 2000, inifile); 
					p64 = (U64 *)&pr[0];
					cr_sscanf(buf2, TEXT("%llx %llx %llx %llx %llx %llx %llx %llx %llx"), 
							p64, p64+1, p64+2,
							p64+3, p64+4, p64+5,
							p64+6, p64+7, p64+8);

					OSAL_FILE_GetIniString(namebuf, TEXT("extMatrix"), 		TEXT(""), buf2, 2000, inifile); 
					p64 = (U64 *)&pextMat[0];
					cr_sscanf(buf2, TEXT("%llx %llx %llx %llx  %llx %llx %llx %llx  %llx %llx %llx %llx  %llx %llx %llx %llx"),
							p64+ 0, p64+ 1, p64+ 2, p64+ 3,
							p64+ 4, p64+ 5, p64+ 6, p64+ 7,
							p64+ 8, p64+ 9, p64+10, p64+11,
							p64+12, p64+13, p64+14, p64+15);

					OSAL_FILE_GetIniString(namebuf, TEXT("InvextMatrix"), 		TEXT(""), buf2, 2000, inifile); 
					p64 = (U64 *)&pInvextMat[0];
					cr_sscanf(buf2, TEXT("%llx %llx %llx %llx  %llx %llx %llx %llx  %llx %llx %llx %llx  %llx %llx %llx %llx"),
							p64+ 0, p64+ 1, p64+ 2, p64+ 3,
							p64+ 4, p64+ 5, p64+ 6, p64+ 7,
							p64+ 8, p64+ 9, p64+10, p64+11,
							p64+12, p64+13, p64+14, p64+15);

				}
			}
		} 		// for (i = 0; i < 3; i++) 		// 
	} 			// for (rl = 0; rl < 2; rl++)   // rl == 0: Right handed,  1: left handed

	return 1;
}


/*!
 ********************************************************************************
 *	@brief      Write INI Configuration file
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/02/22
 *******************************************************************************/
I32 iana_config_write_INI(iana_t *piana)
{
	CHR inifile[BUFLEN];
	//CHR szEXEPath[BUFLEN];
	//CHR szDir[BUFLEN];
	//CHR szDrive[BUFLEN];
	//CHR szFileName[BUFLEN];
	//CHR szFileExt[BUFLEN];

	CHR	buf[64];
	CHR	prefixbuf[64];
	CHR	namebuf[64];

	//----
	//U32		RoughBunkerMat;

	U32 i;
	U32 rl;
	//-------------------------------------------------------------------------------------------
	// Get INI file.


	//GetModuleFileName( NULL, szEXEPath, BUFLEN );
	//_tsplitpath(szEXEPath, szDrive, szDir, szFileName, szFileExt);
	//cr_sprintf(inifile, TEXT("%s\\%s"), piana->szDir, INI_FILENAME);
#if defined(_WIN32)							
	cr_sprintf(inifile, TEXT("%s%s\\%s"),  piana->szDrive, piana->szDir, INI_FILENAME);
#else
	cr_sprintf(inifile, TEXT("%s%s/%s"),  piana->szDrive, piana->szDir, INI_FILENAME);
#endif

	{
		cr_sprintf(prefixbuf, TEXT("SENSOR"));

		//-- System
		cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("SYSTEM"));
		cr_sprintf(buf, TEXT("%d"), piana->powersavingtime);	
		OSAL_FILE_SetIniString(namebuf, TEXT("POWERSAVINGTIME"), buf, inifile);
		cr_sprintf(buf, TEXT("%d"), piana->powersaving);		
		OSAL_FILE_SetIniString(namebuf, TEXT("POWERSAVING"), buf, inifile);

		//--
		for (rl = 0; rl < 2; rl++) {
			for (i = 0; i < NUM_CAM; i++) {
				iana_cam_param_t *picp;

				if (i == 0) {
					if (rl == 0) {		// Right
						cr_sprintf(prefixbuf, TEXT("CAMERA_CENTER"));
					} else {			// Left
						cr_sprintf(prefixbuf, TEXT("LS_CAMERA_CENTER"));
					}
				} else if (i == 1) {
					if (rl == 0) {		// Right
						cr_sprintf(prefixbuf, TEXT("CAMERA_SIDE"));
					} else {			// Left
						cr_sprintf(prefixbuf, TEXT("LS_CAMERA_SIDE"));
					}
				} else if (i == 2) {
					if (rl == 0) {		// Right
						cr_sprintf(prefixbuf, TEXT("CAMERA_ATTACK"));
					} else {			// Left
						cr_sprintf(prefixbuf, TEXT("LS_CAMERA_ATTACK"));
					}
				}

				if (rl == 0) {		// Right
					picp = &piana->RS_ic[i].icp;
				} else {			// Left
					picp = &piana->LS_ic[i].icp;
				}
				
				//---- Camera config
				cr_sprintf(namebuf, TEXT("%s::%s"), prefixbuf, TEXT("CONFIG"));
				//OSAL_FILE_GetIniString(namebuf, TEXT("GAINMULT"), TEXT("1.0"), buf, 60, inifile);
				cr_sprintf(buf, TEXT("%lf"), picp->gainmult); 
				OSAL_FILE_SetIniString(namebuf, TEXT("GAINMULT"), buf, inifile);
			}
		}
	}
	return 1;
}

#endif

/*!
 ********************************************************************************
 *	@brief      Read Translation matrix
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/02/22
 *******************************************************************************/
I32 iana_readTransMat(iana_t *piana, U32 right0left1)
{	
	I32 res;
	U32 i;		
	U32 camid;
	FILE *fp;
	char fn[1024];
	biquad_t *pb;
	I32 readres;
	I32 udValid;
	iana_cam_param_t *picp;

	res = 0;

	for (camid = 0; camid < NUM_CAM; camid++) {
		if (right0left1 == 0) {		// Right
			sprintf(fn, "ct_%d.txt", camid);
			picp = &piana->RS_ic[camid].icp;
		} else {					// Left
			sprintf(fn, "LS_ct_%d.txt", camid);
			picp = &piana->LS_ic[camid].icp;
		}
		fp = cr_fopen(fn, "r");

		udValid = 1;

		if (fp != NULL) {
			pb = &picp->bAp2l_raw;

			for (i = 0; i < 6; i++) {
				readres = fscanf(fp, "%lf %lf\n", &pb->mat[i*2], &pb->mat[i*2+1]);
				if (readres != 2) {
					break;
				}
			}

			pb = &picp->bAl2p_raw;
			for (i = 0; i < 6; i++) {
				readres = fscanf(fp, "%lf %lf\n", &pb->mat[i*2], &pb->mat[i*2+1]);
				if (readres != 2) {
					break;
				}
			}

			pb = &picp->bAp2l_Ud_raw;

			for (i = 0; i < 6; i++) {
				readres = fscanf(fp, "%lf %lf\n", &pb->mat[i*2], &pb->mat[i*2+1]);
				if (readres != 2) {
					udValid = 0;
					break;
				}
			}

			pb = &picp->bAl2p_Ud_raw;
			for (i = 0; i < 6; i++) {
				readres = fscanf(fp, "%lf %lf\n", &pb->mat[i*2], &pb->mat[i*2+1]);
				if (readres != 2) {
					udValid = 0;
					break;
				}
			}

			fclose(fp);
			res = 1;
		} else {
			udValid = 0;
			//pb = &piana->ic[camid].icp.bAp2l_raw;
			pb = &picp->bAp2l_raw;
			for (i = 0; i < 6 * 2; i++) {
				pb->mat[i] = 0.0;
			}
			pb->mat[0*2] 	= 1.0;
			pb->mat[1*2+1] 	= 1.0;

			//pb = &piana->ic[camid].icp.bAl2p_raw;
			pb = &picp->bAl2p_raw;
			for (i = 0; i < 6 * 2; i++) {
				pb->mat[i] = 0.0;
			}
			pb->mat[0*2] 	= 1.0;
			pb->mat[1*2+1] 	= 1.0;
			res = 0;
		}

		if (udValid == 1) {
			picp->quad_UdValid = 1;
		} else {
			picp->quad_UdValid = 0;
		}
	}


	return res;
}




/*!
 ********************************************************************************
 *	@brief      Check Pixelpoint for chessboard
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/10/23
 *******************************************************************************/
I32 iana_checkChessboardPixelPoint(iana_t *piana)
{
	I32 res;

	res = iana_checkChessboardPixelPoint2(piana, 0);		// right-sided only.

	return res;
}

I32 iana_checkChessboardPixelPoint2(iana_t *piana, U32 right0left1)
{	
	I32 res;
	U32 i;
	U32 datapairgood;
	U32 camid;
	U32 count;
	point_t Lp, Pp;
	double maxddist;
	U32 checki;
	U32 checkcamid;

	double ddistmean[MAXCAMCOUNT];
	iana_cam_param_t *picp;

	//--
	datapairgood = 1;
	maxddist = 0;
	checki = 0;
	checkcamid = 0;

	for (camid = 0; camid < NUM_CAM; camid++) {

		if (right0left1 == 0) {		// Right
			picp = &piana->RS_ic[camid].icp;
		} else {					// Left
			picp = &piana->LS_ic[camid].icp;
		}

		//count = piana->ic[camid].icp.lpdatapaircount;
		count = picp->lpdatapaircount;
		if (count > 0 && count <= LPCOUNTMAX) {
			double ddistsum;
			lpdatapair_t *plp;

			//--
			//plp = &piana->ic[camid].icp.lpdatapair[0];
			plp = &picp->lpdatapair[0];
			ddistsum = 0;

			for (i = 0; i < count; i++) {
				double lx, ly, px, py;
				double dx, dy, ddist;

				//--
				lx = plp[i].Lp.x;
				ly = plp[i].Lp.y;
				px = plp[i].Pp.x;
				py = plp[i].Pp.y;

				Pp.x = px;
				Pp.y = py;

				cr_bi_quadratic(&picp->bAp2l_raw, &Pp, &Lp);
				dx = lx - Lp.x;
				dy = ly - Lp.y;
				ddist = sqrt(dx*dx+dy*dy);
				if (maxddist < ddist) {
					maxddist = ddist;
					checki = i;
					checkcamid = camid;
				}
				ddistsum += ddist;
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%4d> %10lf %10lf : %10lf %10lf vs %10lf %10lf (%10lf %10lf:%10lf)\n", 
				//		i, px, py, lx, ly, Lp.x, Lp.y, dx, dy, ddist);
			}
			ddistmean[camid] = ddistsum / count;
		} else {
			datapairgood = 0;
			break;
		}
	}
#define MAXMAXDDIST		0.02
#define MAXMEANDDIST	0.01
	if (datapairgood) {
		if (maxddist > MAXMAXDDIST) {
			datapairgood = 0;
		}
		if (ddistmean[0] > MAXMEANDDIST || ddistmean[1] > MAXMEANDDIST) {
			datapairgood = 0;
		}
	}

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d %d] maxddist: %10lf, mean: %lf %lf\n", checki, checkcamid, maxddist, ddistmean[0], ddistmean[1]);

	if (datapairgood == 0) {
		for (camid = 0; camid < NUM_CAM; camid++) {
			if (right0left1 == 0) {		// Right
				piana->RS_ic[camid].icp.lpdatapaircount = 0;
			} else {					// Left
				piana->LS_ic[camid].icp.lpdatapaircount = 0;
			}
		}
	}


	res = datapairgood;

	return res;
}

/*!
 ********************************************************************************
 *	@brief      Compensate Transmatrix
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/0913
 *******************************************************************************/
static I32 CompensateTransMat(iana_t *piana, U32 right0left1)	
{	
#if defined(HEIGHT_CALI_FLAT_DEFAULT)
	I32 res;
	U32 camid;
	iana_cam_t	*pic;
	iana_cam_param_t *picp;

	cr_point_t *pCamPosL;
	double cx, cy, cz;		// camera position
	double dc;				// Cali-flat dickness

	double xl, yl;
	double xl2, yl2;
//#define XCOUNT	20
//#define YCOUNT 	20
//#define XCOUNT	10
//#define YCOUNT 	20
//#define XCOUNT	6
//#define YCOUNT 	10

//#define XSTEP	(0.1)
//#define YSTEP	(0.1)
//#define XSTEP	(0.05)
//#define YSTEP	(0.05)

//#define XSTEP	(0.02)
//#define YSTEP	(0.02)
	
//#define XCOUNT	10
//#define YCOUNT 	15
	
#define XSTEP	(0.01)
#define YSTEP	(0.01)
//#define XCOUNT	20
//#define YCOUNT 	30
//#define XCOUNT	10
//#define YCOUNT 	20
//#define XCOUNT	20
//#define YCOUNT 	20
#define XCOUNT	40
#define YCOUNT 	60
	point_t Lp1[YCOUNT*XCOUNT];
	point_t Lp2[YCOUNT*XCOUNT];
	point_t Pp[YCOUNT*XCOUNT];

	I32 i, j;

	res = 0;

	dc = piana->height_cali_flat;

	for (camid = 0; camid < NUM_CAM; camid++) {
		if (right0left1 == 0) {		// Right
			pic = &piana->RS_ic[camid];
		} else {					// Left
			pic = &piana->LS_ic[camid];
		}

		picp 	= &pic->icp;
		pCamPosL= &picp->CamPosL;
		cx		= pCamPosL->x;
		cy		= pCamPosL->y;
		cz		= pCamPosL->z;

		for (i = 0; i < YCOUNT; i++) {
			yl = (i - YCOUNT/2) * YSTEP;
			yl2 = (cz * yl - dc * cy) / (cz - dc);
			for (j = 0; j < XCOUNT; j++) {
				point_t Lp;

				xl = (j - XCOUNT/2) * XSTEP;
				xl2 = (cz * xl - dc * cx) / (cz - dc);

				Lp1[i*XCOUNT+j].x = xl;
				Lp1[i*XCOUNT+j].y = yl;

			
				Lp2[i*XCOUNT+j].x = xl2;
				Lp2[i*XCOUNT+j].y = yl2;

				Lp.x = xl;
				Lp.y = yl;

				cr_bi_quadratic(&picp->bAl2p_raw, &Lp, &Pp[i*XCOUNT+j]);
//				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " %10lf %10lf \n", Pp[i*XCOUNT+j].x, Pp[i*XCOUNT+j].y);
//				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " %10lf %10lf \n", xl, yl);
			}
//			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
		}
#if 0
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------ \n");
		for (i = 0; i < YCOUNT; i++) {
			yl = (i - YCOUNT/2) * YSTEP;
			yl2 = (cz * yl - dc * cy) / (cz - dc);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " %10lf %10lf \n", yl, yl2);
		}

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------ \n");
		for (j = 0; j < XCOUNT; j++) {
			xl = (j - XCOUNT/2) * XSTEP;
			xl2 = (cz * xl - dc * cx) / (cz - dc);

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " %10lf %10lf \n", xl, xl2);
		}
#endif

//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------ \n");
//		for (i = 0; i < YCOUNT; i++) {
//			for (j = 0; j < XCOUNT; j++) {
//				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " %10lf %10lf \n", Lp2[i*XCOUNT+j].x, Lp2[i*XCOUNT+j].y);
//			}
//			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
//		}




		res = cr_bi_quadratic_matrix(
								&picp->bAp2l,

								&Pp[0],	// Pixel point data
								&Lp2[0],		// Local point data
								YCOUNT*XCOUNT);	// count of point

		res = cr_bi_quadratic_matrix(
								&picp->bAl2p,
								&Lp2[0],		// Local point data
								&Pp[0],	// Pixel point data
								YCOUNT*XCOUNT);	// count of point



#if 0
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------ \n");
		for (i = 0; i < YCOUNT; i++) {
			for (j = 0; j < XCOUNT; j++) {
				point_t L1, L2;
				cr_bi_quadratic(&picp->bAp2l_raw, &Pp[i*XCOUNT+j], &L1);
				cr_bi_quadratic(&picp->bAp2l,     &Pp[i*XCOUNT+j], &L2);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%2d %2d   %10lf %10lf   %10lf %10lf\n", i, j, L1.x, L1.y, L2.x, L2.y);
			}
		}
#endif

#if 0
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------ \n");
		for (i = 0; i < YCOUNT; i++) {
			for (j = 0; j < XCOUNT; j++) {
				point_t L1, L2;
				point_t P1, P2;
				double dx, dy, derr;
				L2;P2;
				cr_bi_quadratic(&picp->bAp2l_raw, &Pp[i*XCOUNT+j], &L1);
				cr_bi_quadratic(&picp->bAl2p_raw, &L1, &P1);
				dx = Pp[i*XCOUNT+j].x - P1.x;
				dy = Pp[i*XCOUNT+j].y - P1.y;

				derr = sqrt(dx*dx+dy*dy);

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%2d %2d   %10lf %10lf %10lf %10lf  %10lf %10lf %10lf %10lf %10lf\n", i, j, Pp[i*XCOUNT+j].x, Pp[i*XCOUNT+j].y, L1.x, L1.y, P1.x, P1.y, dx, dy, derr);
			}
		}
#endif

#if 0
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------ \n");
		for (i = 0; i < YCOUNT; i++) {
			for (j = 0; j < XCOUNT; j++) {
				point_t L1, L2;
				point_t P1, P2;
				double dx, dy, derr;
				L2;P2;
				cr_bi_quadratic(&picp->bAp2l, &Pp[i*XCOUNT+j], &L1);
				cr_bi_quadratic(&picp->bAl2p, &L1, &P1);
				dx = Pp[i*XCOUNT+j].x - P1.x;
				dy = Pp[i*XCOUNT+j].y - P1.y;

				derr = sqrt(dx*dx+dy*dy);

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%2d %2d   %10lf %10lf %10lf %10lf  %10lf %10lf %10lf %10lf %10lf\n", i, j, Pp[i*XCOUNT+j].x, Pp[i*XCOUNT+j].y, L1.x, L1.y, P1.x, P1.y, dx, dy, derr);
			}
		}
#endif

#if 0
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------ \n");
		//for (i = 0; i < YCOUNT; i++) 
			for (j = 0; j < XCOUNT; j++) 
		{
			//for (j = 0; j < XCOUNT; j++) 
		for (i = 0; i < YCOUNT; i++) 
			{
				point_t L1, L2;
				point_t P1, P2;
				double dx, dy, derr;
				L2;P2;
				cr_bi_quadratic(&picp->bAl2p, &Lp1[i*XCOUNT+j], &P1);
				cr_bi_quadratic(&picp->bAp2l, &P1, &L1);
				dx = Lp1[i*XCOUNT+j].x - L1.x;
				dy = Lp1[i*XCOUNT+j].y - L1.y;

				derr = sqrt(dx*dx+dy*dy);

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%2d %2d   %10lf %10lf %10lf %10lf  %10lf %10lf %10lf %10lf %10lf\n", i, j, Lp1[i*XCOUNT+j].x, Lp1[i*XCOUNT+j].y, P1.x, P1.y, L1.x, L1.y, dx, dy, derr);
			}
		}
#endif
#if 0
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------ \n");
		for (i = YCOUNT/4; i < (YCOUNT * 3)/ 4; i++) 
//		for (i = 0; i < YCOUNT; i++) 
		{
			for (j = XCOUNT/4; j < (XCOUNT*3)/4; j++) 
			//for (j = 0; j < XCOUNT; j++) 
			{
				point_t L1, L2;
				point_t P1, P2;
				L2;P2;
				cr_bi_quadratic(&picp->bAl2p_raw, &Lp1[i*XCOUNT+j], &P1);
				cr_bi_quadratic(&picp->bAp2l_raw, &P1, &L1);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%2d %2d   %10lf %10lf %10lf %10lf  %10lf %10lf\n", i, j, 
						Lp1[i*XCOUNT+j].x,
						Lp1[i*XCOUNT+j].y,
						P1.x, P1.y, L1.x, L1.y);
			}
		}
#endif


	}		// camid

	return res;
#else
	I32 res;
	U32 camid;
	iana_cam_t	*pic;
	iana_cam_param_t *picp;

	res = 1;

	for (camid = 0; camid < NUM_CAM; camid++) {
		if (right0left1 == 0) {		// Right
			pic = &piana->RS_ic[camid];
		} else {					// Left
			pic = &piana->LS_ic[camid];
		}

		picp 	= &pic->icp;
		memcpy(&picp->bAp2l, &picp->bAp2l_raw, sizeof(biquad_t));
		memcpy(&picp->bAl2p, &picp->bAl2p_raw, sizeof(biquad_t));

		memcpy(&picp->bAp2l_Ud, &picp->bAp2l_Ud_raw, sizeof(biquad_t));
		memcpy(&picp->bAl2p_Ud, &picp->bAl2p_Ud_raw, sizeof(biquad_t));

	}		// camid

	return res;
#endif
}


/*!
 ********************************************************************************
 *	@brief      Compensate Transmatrix, with ball-area weighting.
 *  @param[in]	piana
 *              IANA module handle
 *	@param[in]  pBallposL
 *				Ball Position, Local.
 *
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/0923
 *******************************************************************************/
//#define WEIGHTMULT			5
//#define WEIGHTMULT			10
//#define WEIGHTMULT			30
#define WEIGHTMULT			40
#define MINDATAPAIRCOUNT	30

#define CHESSBOARDXSTART	-0.2
#define CHESSBOARDXEND		 0.2
#define CHESSBOARDXSTEP		 0.01


#define CHESSBOARDYSTART	-0.4
#define CHESSBOARDYEND		 0.4
#define CHESSBOARDYSTEP		 0.04

//#define	XLEFT				(-0.21)
//#define	XLEFT				(-0.15)
//#define	XRIGHT				( 0.15)
#define	XLEFT				(-0.5)
#define	XRIGHT				( 0.5)

////////////////////#define YTOP				(0.25)
//#define YBOTTOM				(-0.15)
////////////////////#define YBOTTOM				(-0.5)
//#define YTOP				(0.15)
//#define YBOTTOM				(-0.15)
#define YTOP				(0.20)
#define YBOTTOM				(-0.10)
static I32 CompensateTransMat2(iana_t *piana, point_t *pBallposL, U32 right0left1)
{	

	I32 res;
	U32 camid;
	U32 i, j;

	iana_cam_t	*pic;
	iana_cam_param_t *picp;

	cr_point_t *pCamPosL;
	double cx, cy, cz;		// camera position
	double dc;				// Cali-flat dickness

	//double xl, yl;
	//double xl2, yl2;
	//double px, py;

	point_t *Lp1;
	point_t *Lp2;
	point_t *Pp1;
	point_t *Pp2;

	//---
	res = 0;
	dc = piana->height_cali_flat;

	Lp1 = (point_t *) malloc(LPCOUNTMAX * sizeof(point_t));
	Lp2 = (point_t *) malloc(LPCOUNTMAX * sizeof(point_t) * WEIGHTMULT);
	Pp1 = (point_t *) malloc(LPCOUNTMAX * sizeof(point_t));
	Pp2 = (point_t *) malloc(LPCOUNTMAX * sizeof(point_t) * WEIGHTMULT);

	for (camid = 0; camid < NUM_CAM; camid++) {
		U32	count, count2;	
		U32 storecount;
		lpdatapair_t 	*plp;
		double lx, ly, lx2, ly2;
		double px, py;

		//---
		if (right0left1 == 0) {		// Right
			pic = &piana->RS_ic[camid];
		} else {					// Left
			pic = &piana->LS_ic[camid];
		}

		picp 	= &pic->icp;
		pCamPosL= &picp->CamPosL;
		cx		= pCamPosL->x;
		cy		= pCamPosL->y;
		cz		= pCamPosL->z;

		count = picp->lpdatapaircount;
		if (count < MINDATAPAIRCOUNT) {			// Discard saved datapair... reconstruct.
			count = 0;
			for (ly = CHESSBOARDYSTART; ly < CHESSBOARDYEND; ly += CHESSBOARDYSTEP) {
				for (lx = CHESSBOARDXSTART; lx < CHESSBOARDXEND; lx += CHESSBOARDXSTEP) {
					Lp1[count].x = lx;
					Lp1[count].y = ly;
					cr_bi_quadratic(&picp->bAl2p_raw, &Lp1[count], &Pp1[count]);
					count++;
					if (count >= LPCOUNTMAX) {
						break;
					}
				}
				if (count >= LPCOUNTMAX) {
					break;
				}
			}
		} else {
			plp = &picp->lpdatapair[0];
			for (i = 0; i < count; i++) {
				Lp1[i].x = plp[i].Lp.x;
				Lp1[i].y = plp[i].Lp.y;
				Pp1[i].x = plp[i].Pp.x;
				Pp1[i].y = plp[i].Pp.y;
			}
		}

		// WEIGHT Near-Ball position point..:)
		count2 = 0;
		for (i = 0; i < count; i++) {
			lx = Lp1[i].x;
			ly = Lp1[i].y;
			px = Pp1[i].x;
			py = Pp1[i].y;

			if (
					( lx > pBallposL->x + XLEFT 		&& lx < pBallposL->x + XRIGHT)
					&& ( ly > pBallposL->y + YBOTTOM 	&& ly < pBallposL->y + YTOP)
			   ) {
				storecount = WEIGHTMULT;
			} else {
				storecount = 1;
			}

			for (j = 0; j < storecount; j++) {
				Lp2[count2].x = lx;
				Lp2[count2].y = ly;
				Pp2[count2].x = px;
				Pp2[count2].y = py;
				count2++;
				if (count2 >= WEIGHTMULT * LPCOUNTMAX) {
					break;
				}
			}
			if (count2 >= WEIGHTMULT * LPCOUNTMAX) {
				break;
			}
		}

		//---- Cali-flat compensation.
		for (i = 0; i < count2; i++) {
			lx = Lp2[i].x;
			ly = Lp2[i].y;

			lx2 = (cz * lx - dc * cx) / (cz - dc);
			ly2 = (cz * ly - dc * cy) / (cz - dc);

			Lp2[i].x = lx2;
			Lp2[i].y = ly2;
		}

		res = cr_bi_quadratic_matrix(
								&picp->bAp2l,

								&Pp2[0],	// Pixel point data
								&Lp2[0],		// Local point data
								count2);	// count of point

		res = cr_bi_quadratic_matrix(
								&picp->bAl2p,
								&Lp2[0],		// Local point data
								&Pp2[0],	// Pixel point data
								count2);	// count of point

	}		// camid

	free(Lp1);
	free(Lp2);
	free(Pp1);
	free(Pp2);

	res = 1;

	return res;
}


/*!
 ********************************************************************************
 *	@brief      Converse configurations
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/02/22
 *******************************************************************************/
I32 iana_config_conversion(iana_t *piana)	
{	
	U32 i;
	double sx, ex;				// sx < ex, 
	double sy, ey;				// sy < ey
	iana_cam_param_t *picp;
	cr_rectL_t *prectLArea;
	cr_rectL_t *prectLTee;
	cr_rectL_t *prectLIron;
	cr_rectL_t *prectLPutter;

	//-------------------------------------
//	CompensateTransMat(piana, right0left1);
	if (piana->useLeftHanded) {
		CompensateTransMat(piana, piana->Right0Left1);
	} else {
		CompensateTransMat(piana, 0);				// Righthaned Sensor Only..
	}


	prectLArea 		= piana->prectLArea;
	prectLTee 		= piana->prectLTee;
	prectLIron 		= piana->prectLIron;
	prectLPutter 	= piana->prectLPutter;
	//----
	//-- Get Full Area contatins Tee, Iron and Putter area
	// sx, Get Smallest value.
	sx = prectLTee->sx;
	if (sx > prectLIron->sx) {
		sx = prectLIron->sx;
	}
	if (sx > prectLPutter->sx) {
		sx = prectLPutter->sx;
	}

	// ex, Get Largest value.
	ex = prectLTee->ex;
	if (ex < prectLIron->ex) {
		ex = prectLIron->ex;
	}
	if (ex < prectLPutter->ex) {
		ex = prectLPutter->ex;
	}

	// sy, Get Smallest value.
	sy = prectLTee->sy;
	if (sy > prectLIron->sy) {
		sy = prectLIron->sy;
	}
	if (sy > prectLPutter->sy) {
		sy = prectLPutter->sy;
	}
	
	// ey, Get Largest value.
	ey = prectLTee->ey;
	if (ey < prectLIron->ey) {
		ey = prectLIron->ey;
	}
	if (ey < prectLPutter->ey) {
		ey = prectLPutter->ey;
	}

	prectLArea->sx = sx;
	prectLArea->ex = ex;
	prectLArea->sy = sy;
	prectLArea->ey = ey;

#define AREAL_ADDITION	(0.05)
	if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
		prectLArea->sx += (-AREAL_ADDITION);
		prectLArea->ex += ( AREAL_ADDITION);

		prectLArea->sy += (-AREAL_ADDITION);
		prectLArea->ey += ( AREAL_ADDITION);
	}
#if defined(V2000_TEST)
	for (i = 0; i < NUM_CAM; i++) {
		cr_rectL_t rectLAreaExt;
		cr_rectL_t rectLArea0, rectLArea1;


		picp = &piana->pic[i]->icp;

#define MAXTEEHEIGHT		(0.065)
		getExtendedArea(&picp->CamPosL, prectLArea, &rectLArea0, BALLDIAMETER);
		getExtendedArea(&picp->CamPosL, prectLTee, &rectLArea1, MAXTEEHEIGHT + BALLDIAMETER);

		memcpy(&rectLAreaExt, &rectLArea0, sizeof(cr_rectL_t));
		if (rectLAreaExt.sx > rectLArea1.sx) {			// get smaller..
			rectLAreaExt.sx = rectLArea1.sx;
		}
		if (rectLAreaExt.ex < rectLArea1.ex) {			// get bigger..
			rectLAreaExt.ex = rectLArea1.ex;
		}
		if (rectLAreaExt.sy > rectLArea1.sy) {			// get smaller..
			rectLAreaExt.sy = rectLArea1.sy;
		}
		if (rectLAreaExt.ey < rectLArea1.ey) {			// get bigger..
			rectLAreaExt.ey = rectLArea1.ey;
		}

		AreaConversionAndCheck(piana, i, &rectLAreaExt,	&picp->rectAreaExt);
		AreaConversionAndCheck(piana, i, prectLArea, 	&picp->rectArea_);
		AreaConversionAndCheck(piana, i, prectLTee,		&picp->rectTee);
		AreaConversionAndCheck(piana, i, prectLIron, 	&picp->rectIron);
		AreaConversionAndCheck(piana, i, prectLPutter, 	&picp->rectPutter);


#define AREAEXT_ADDVALUE	64
		if (picp->rectAreaExt.bottom < 1024 -1 - AREAEXT_ADDVALUE) {
			picp->rectAreaExt.bottom +=  AREAEXT_ADDVALUE;
		}
	}
#else


	for (i = 0; i < NUM_CAM; i++) {
		cr_rectL_t rectLAreaExt;
		cr_rectL_t rectLArea0, rectLArea1;
		double addvalue;


		picp = &piana->pic[i]->icp;

#define MAXTEEHEIGHT		(0.065)
		getExtendedArea(&picp->CamPosL, prectLArea, &rectLArea0, BALLDIAMETER);
		getExtendedArea(&picp->CamPosL, prectLTee, &rectLArea1, MAXTEEHEIGHT + BALLDIAMETER);

		memcpy(&rectLAreaExt, &rectLArea0, sizeof(cr_rectL_t));


		if (rectLAreaExt.sx > rectLArea1.sx) {			// get smaller..
			rectLAreaExt.sx = rectLArea1.sx;
		}
		if (rectLAreaExt.ex < rectLArea1.ex) {			// get bigger..
			rectLAreaExt.ex = rectLArea1.ex;
		}
		if (rectLAreaExt.sy > rectLArea1.sy) {			// get smaller..
			rectLAreaExt.sy = rectLArea1.sy;
		}
		if (rectLAreaExt.ey < rectLArea1.ey) {			// get bigger..
			rectLAreaExt.ey = rectLArea1.ey;
		}


#define AREAEXT_L_ADDVALUE 		0.03

//#define AREAEXT_L_ADDVALUE_EXT 	0.10		// too small.
//#define AREAEXT_L_ADDVALUE_EXT 	0.15
#define AREAEXT_L_ADDVALUE_EXT 	0.20

		addvalue = AREAEXT_L_ADDVALUE;
		if ((piana->camsensor_category == CAMSENSOR_P3V2) && (i == piana->camidOther)) {
			addvalue = addvalue + AREAEXT_L_ADDVALUE_EXT;     // For swing plate, expent check region for camidOrder.  2020/1026..  
		}
		rectLAreaExt.sx = rectLAreaExt.sx - addvalue;
		rectLAreaExt.ex = rectLAreaExt.ex + addvalue;
		rectLAreaExt.sy = rectLAreaExt.sy - addvalue;
		rectLAreaExt.ey = rectLAreaExt.ey + addvalue;


		AreaConversionAndCheck(piana, i, &rectLAreaExt,	&picp->rectAreaExt);
		AreaConversionAndCheck(piana, i, prectLArea, 	&picp->rectArea_);
		AreaConversionAndCheck(piana, i, prectLTee,		&picp->rectTee);
		AreaConversionAndCheck(piana, i, prectLIron, 	&picp->rectIron);
		AreaConversionAndCheck(piana, i, prectLPutter, 	&picp->rectPutter);


#define AREAEXT_ADDVALUE	64
		if (picp->rectAreaExt.bottom < 1024 -1 - AREAEXT_ADDVALUE) {
			picp->rectAreaExt.bottom +=  AREAEXT_ADDVALUE;
		}
	}
#endif
	return 1;

}



/*!
 ********************************************************************************
 *	@brief      Converse configurations, with ball-area weighting.
 *
 *  @param[in]	piana
 *              IANA module handle
 *	@param[in]  pBallposL
 *				Ball Position, Local.
 *
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/09/23
 *******************************************************************************/
I32 iana_config_conversion2(iana_t *piana, point_t *pBallposL, U32 right0left1)
{	
	I32 res;
	//-------------------------------------

	res = CompensateTransMat2(piana, pBallposL, right0left1);

	return res;

}

static I32 AreaConversionAndCheck(iana_t *piana, U32 camid, cr_rectL_t *prectL, cr_rect_t *prect)
{
/*

    Pt0 ---------- Pt1
      |            |
      |            |
      |            |
    Pt3 ---------- Pt2

*/
	I32 res;

	point_t Lp[4];
	point_t Pp[4];

	I32 itmp;

	I32		xmin, xmax;
	I32		ymin, ymax;

	I32		xl, xr;
	I32		yt, yb;
	I32 i;
	//--

	Lp[0].x = prectL->sx;
	Lp[0].y = prectL->sy;

	Lp[1].x = prectL->ex;
	Lp[1].y = prectL->sy;

	Lp[2].x = prectL->ex;
	Lp[2].y = prectL->ey;

	Lp[3].x = prectL->sx;
	Lp[3].y = prectL->ey;

//	right0left1;

	for (i = 0; i < 4; i++) {
		iana_L2P(piana, camid, &Lp[i], &Pp[i], 0);
	}

	// left < right,   top < bottom
	
	xmin = 9999; xmax = -9999;
	ymin = 9999; ymax = -9999;
	for (i = 0; i < 4; i++) {
		if (xmax < Pp[i].x) xmax = (I32)Pp[i].x;
		if (xmin > Pp[i].x) xmin = (I32)Pp[i].x;
		if (ymax < Pp[i].y) ymax = (I32)Pp[i].y;
		if (ymin > Pp[i].y) ymin = (I32)Pp[i].y;
	}
	xl =  xmin;
	xr =  xmax;
	
	yt =  ymin;
	yb =  ymax;

	if (xl < 0) {
		xl = 0; 
	}
	if (xr < 0) {
		xr = 0;
	}
	if (yt < 0) {
		yt = 0; 
	}
	if (yb < 0) {
		yb = 0;
	}

	if (xl > xr) {
		itmp = xl;
		xl = xr;
		xr = itmp;
	}

	if (yt > yb) {
		itmp = yt;
		yt = yb;
		yb = itmp;
	}

	xl = xl & (~0x0007);			
	xr = ((xr + 7)  & (~0x0007)) - 1;	

	prect->left  	= xl;
	prect->right 	= xr;
	prect->top		= yt;
	prect->bottom	= yb;

	res = 1;

	return res;
}



static U32 getExtendedArea(
		cr_point_t *pCamPos, 
		cr_rectL_t *prectLArea,
		cr_rectL_t *prectLAreaExt,
		double zvalue)
{
	U32 res;
	double sx, sy, ex, ey;
	cr_point_t p0, p1, p2, p3;
	cr_point_t pE0, pE1, pE2, pE3;
	cr_line_t  line0;
	cr_plane_t plane0;

	cr_vector_t n0;

	//--
	memcpy(prectLAreaExt, prectLArea, sizeof(cr_rectL_t));
	
	p0.x = 0; p0.y = 0; p0.z = 0;
	n0.x = 0; n0.y = 0; n0.z = 1;
	cr_plane_p0n(&p0, &n0, &plane0);		// XY Plane

	sx = prectLArea->sx;
	sy = prectLArea->sy;
	ex = prectLArea->ex;
	ey = prectLArea->ey;

	p0.x = sx; p0.y = sy; p0.z = zvalue;			// Left,  upper
	p1.x = ex; p1.y = sy; p1.z = zvalue;			// Right, upper
	p2.x = ex; p2.y = ey; p2.z = zvalue;			// Right, bottom
	p3.x = sx; p3.y = ey; p3.z = zvalue;			// Left,  upper


	//--
	cr_line_p0p1(pCamPos, &p0, &line0);				
	res = cr_point_line_plane( &line0, &plane0, &pE0);
	if (res == 0) { goto func_exit; }

	cr_line_p0p1(pCamPos, &p1, &line0);				
	res = cr_point_line_plane( &line0, &plane0, &pE1);
	if (res == 0) { goto func_exit; }

	cr_line_p0p1(pCamPos, &p2, &line0);				
	res = cr_point_line_plane( &line0, &plane0, &pE2);
	if (res == 0) { goto func_exit; }

	cr_line_p0p1(pCamPos, &p3, &line0);				
	res = cr_point_line_plane( &line0, &plane0, &pE3);
	if (res == 0) { goto func_exit; }


	if (prectLAreaExt->sx > pE0.x) { 					// smaller.
		prectLAreaExt->sx = pE0.x;
	}
	if (prectLAreaExt->sx > pE1.x) { 					// smaller.
		prectLAreaExt->sx = pE1.x;
	}
	if (prectLAreaExt->sx > pE2.x) { 					// smaller.
		prectLAreaExt->sx = pE2.x;
	}
	if (prectLAreaExt->sx > pE3.x) { 					// smaller.
		prectLAreaExt->sx = pE3.x;
	}

	if (prectLAreaExt->ex < pE0.x) { 					// Bigger.
		prectLAreaExt->ex = pE0.x;
	}
	if (prectLAreaExt->ex < pE1.x) { 					// Bigger.
		prectLAreaExt->ex = pE1.x;
	}
	if (prectLAreaExt->ex < pE2.x) { 					// Bigger.
		prectLAreaExt->ex = pE2.x;
	}
	if (prectLAreaExt->ex < pE3.x) { 					// Bigger.
		prectLAreaExt->ex = pE3.x;
	}

	if (prectLAreaExt->sy > pE0.y) { 					// smaller.
		prectLAreaExt->sy = pE0.y;
	}
	if (prectLAreaExt->sy > pE1.y) { 					// smaller.
		prectLAreaExt->sy = pE1.y;
	}
	if (prectLAreaExt->sy > pE2.y) { 					// smaller.
		prectLAreaExt->sy = pE2.y;
	}
	if (prectLAreaExt->sy > pE3.y) { 					// smaller.
		prectLAreaExt->sy = pE3.y;
	}

	if (prectLAreaExt->ey < pE0.y) { 					// Bigger.
		prectLAreaExt->ey = pE0.y;
	}
	if (prectLAreaExt->ey < pE1.y) { 					// Bigger.
		prectLAreaExt->ey = pE1.y;
	}
	if (prectLAreaExt->ey < pE2.y) { 					// Bigger.
		prectLAreaExt->ey = pE2.y;
	}
	if (prectLAreaExt->ey < pE3.y) { 					// Bigger.
		prectLAreaExt->ey = pE3.y;
	}

	res = 1;
func_exit:

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " %lf %lf ~ %lf %lf   ->  %lf %lf ~ %lf %lf\n",
		prectLArea->sx, prectLArea->sy, prectLArea->ex, prectLArea->ey,
		prectLAreaExt->sx, prectLAreaExt->sy, prectLAreaExt->ex, prectLAreaExt->ey);
	return res;
}


#if defined (__cplusplus)
}
#endif

