/*!
*******************************************************************************

                CREATZ IANA ball detect

    @section copyright_notice COPYRIGHT NOTICE
    Copyright (c) 2015 by Creatz Inc.
    All Rights Reserved. \n
    Do not duplicate without prior written consent of Creatz Inc.

*******************************************************************************
    @section file_information FILE CREATION INFORMATION
    @file   iana_balldetect.cpp
    @brief  detect ball-like objects using several circle detection algorithms
    @author Original: by yhsuk
    @date   2016/02/21 First Created

    @section checkin_information LATEST CHECK-IN INFORMATION
    $Rev$
    $Author$
    $Date$
*******************************************************************************/

//#define USE_CHT
// #define IPP_SUPPORT
 /*----------------------------------------------------------------------------
	 Description	: defines referenced header files
  -----------------------------------------------------------------------------*/
#include <string.h>

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"
#include "cr_regression.h"

#include "scamif.h"
#include "scamif_buffer.h"

#include "iana.h"
#include "iana_cam.h"
#include "iana_shot.h"
#include "iana_checkready.h"
#include "iana_balldetect.h"

#include "iana_tool.h"

#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#else
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#endif

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
*	Description	: defines Macros and definitions
-----------------------------------------------------------------------------*/
#define	WHRATE	0.4

#define MAXPOINTS	2048
#define MINPOINTS	20

#define MPROIOFFSET		3 // 2

#define LABELING_MARGIN	3 // 1

#define GUARDMEM		(32 * 1024)


#define TOOL_IMG
#define TOOL_IMG_1


// #define DEBUG_BALLDETECT

/*----------------------------------------------------------------------------
*	Description	: defines datatype
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
	Description	: static variable declaration
-----------------------------------------------------------------------------*/


//static int s_usecircleRANSAC = 0;
static int s_usecircleRANSAC = 1;
#ifdef IPP_SUPPORT
static Ipp32f Integ0[(1280 + 100) * (1024 + 100)];
static Ipp32f SumP0[1280 * 1024];
static Ipp32f SumN0[1280 * 1024];
#endif

static U08 ballimgbuf[1280 * 1024];
static U08 ballimgbuf1[1280 * 1024];

static int s_use_minr_maxr = 0;
static double s_min_r = 0;
static double s_max_r = 0;

static float s_low = 100.0f;
static float s_high = 200.0f;

/*----------------------------------------------------------------------------
    Description	: external and internal global variable
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*	Description	: declares the dll function prototype
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*	Description	: declares the external function prototype
-----------------------------------------------------------------------------*/
extern I32 calc_circle_mean_stddev(iana_ballcandidate_t *pbc, U08 *pimgbuf, U32 width, U32 height);	
	

//--
U08 cht_refine(U16 *circle_x, U16 *circle_y, U08 *circle_r, // outputs of this function
	U08 *image_original, U16 xstride, U08 min_r, U08 max_r,	// input parameters
	U16 cht_xroi_start, U16 cht_yroi_start, U16 cht_xroi_end, U16 cht_yroi_end, // input parameters
	I32 threshold_gradient, U32 threshold_vote);	// input parameters


I32 iana_getballcandidator_cht_(
	U08 *buf,
	U32 width, U32 height, 			// ROI width
	U32 width2,						// Image width for buf

	double *circle_x32,
	double *circle_y32,
	double *circle_r32,

	U32 min_r, U32 max_r,
	I32 threshold_gradient, U32 threshold_vote);

//-----------

I32 iana_getballcandidatorAdapt_Legacy(
	iana_t *piana, U32 camid, U08 *buf,
	U32 width, U32 height, 			// ROI width
	U32 width2,						// Image width for buf
	iana_ballcandidate_t bc[MAXCANDIDATE], U32 *pbcount,
	U32 halfWinSize,
	U32 multv,
	U32 nexp,
	double stdmult,
	double cermax
	, double cutmeanmult    //20190420, yhsuk.
);


U32 iana_mean_stddev_calc( iana_t *piana, U32 camid, 
		iana_ballcandidate_t *pbc, U32 candcount, U08 *pbuf, U32 width, U32 height);


static void LabellingAndSegmentation(iana_t *piana, U32 width, U32 height, U08 *pimgdest, U32 iter, imagesegment_t imgseg[MAXLABEL], int maxLabel)
{
#ifdef IPP_SUPPORT

	I32 lbbuffersize;
	U08 *lbbuffer;
	I32 segmentcount;

	I32 i, j;
	imagesegment_t 	*pis;
	U08 *pimg;
	U32 segindex;
	U32 mproioffset;
	IppiSize mproisize;
	IppiSize lbroisize;

	mproisize.width = width - 2 * MPROIOFFSET;
	mproisize.height = height - 2 * MPROIOFFSET;

	mproioffset = MPROIOFFSET * width + MPROIOFFSET;

	lbroisize.width = width - LABELING_MARGIN * 2;
	lbroisize.height = height - LABELING_MARGIN * 2;

	//-- Labeling
	if (iter == 1) {
		ippiDilate3x3_8u_C1IR(
					pimgdest + mproioffset, width,
					mproisize
		);
		ippiErode3x3_8u_C1IR(
			pimgdest + mproioffset, width,
			mproisize
		);

		if (piana->himgfunc) {
#if defined(TOOL_IMG)
			((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width, height, 10, 1, 2, 3, piana->imguserparam);
#endif
		}
	}

	ippiLabelMarkersGetBufferSize_8u_C1R(lbroisize, &lbbuffersize);	// Get buffersize
	lbbuffer = (U08 *)malloc(lbbuffersize);
    ippiLabelMarkers_8u_C1IR(
		pimgdest, width, lbroisize,
		MINLABEL,
		maxLabel,
		ippiNormInf,
		//ippiNormL1,
		&segmentcount,
		lbbuffer);
			
	free(lbbuffer);

	//-- Segm.
	pimg = pimgdest;
	for (i = 0; i < MAXLABEL; i++) {
		pis = &imgseg[i];
		memset(pis, 0, sizeof(imagesegment_t));
		pis->state = IMAGESEGMENT_NULL;
		pis->label = i;
	}

	for (i = LABELING_MARGIN; i < (I32)height - LABELING_MARGIN; i++) {
		for (j = LABELING_MARGIN; j < (I32)width - LABELING_MARGIN; j++) {
			segindex = *(pimg + j + i * width);
#ifdef DEBUG_BALLDETECT
			if (segindex != 0) {
				cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,
				"segindex of [%3d,%3d]: %d\n",
				i, j, segindex);
					}
#endif
			if (segindex >= MINLABEL && segindex <= MAXLABEL - 1) {
				pis = &imgseg[segindex];
				if (pis->state == IMAGESEGMENT_NULL) {
					pis->state = IMAGESEGMENT_FULL;
					pis->ixl = j;
					pis->iyu = i;
				}
				pis->pixelcount++;
				if ((I32)pis->ixr < j)
				{		// xr is biggest..
					pis->ixr = j;
				}
				if ((I32)pis->ixl > j)
				{		// xl is smallest..
					pis->ixl = j;
				}
				pis->iyb = i;
			}
		}
	}

#ifdef DEBUG_BALLDETECT
	for (i = 0; i < MAXLABEL; i++) {
		pis = &imgseg[i];
		if (pis->state == IMAGESEGMENT_FULL) {
		    cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,
		    "[%2d] (%3d,%3d) ~ (%3d,%3d), pixelcount: %d\n",
		    i,
		    pis->ixl, pis->iyu,
		    pis->ixr, pis->iyb,
		    pis->pixelcount);
		}
	}
#endif
   
#else


    cv::Mat src(cv::Size(width, height), CV_8UC1, pimgdest), morphed, dst;
    cv::Mat stats, cents;
    int i;

    imagesegment_t *pis;

    if (iter == 1){
        cv::morphologyEx(src, morphed, cv::MORPH_CLOSE, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)), cv::Point(-1, -1), 1, cv::BORDER_ISOLATED);
    } else{
        src.copyTo(morphed);
    }

    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)morphed.data, width, height, 10, 1, 2, 3, piana->imguserparam);
    }
    
    for (i = 0; i < MAXLABEL; i++) {
        pis = &imgseg[i];
        memset(pis, 0, sizeof(imagesegment_t));
        pis->state = IMAGESEGMENT_NULL;
        pis->label = i;
    }

    maxLabel = cv::connectedComponentsWithStats(morphed, dst, stats, cents, 8);
    
    maxLabel = maxLabel < MAXLABEL ? maxLabel : MAXLABEL;

    for (i = 1; i < maxLabel; i++) {
        pis = &imgseg[i];
        if (pis->state == IMAGESEGMENT_NULL) {
            pis->state = IMAGESEGMENT_FULL;
            pis->ixl = stats.at<int>(i, 0);
            pis->iyu = stats.at<int>(i, 1);
            pis->ixr = stats.at<int>(i, 0) + stats.at<int>(i, 2);
            pis->iyb = stats.at<int>(i, 1) + stats.at<int>(i, 3);
            pis->pixelcount = stats.at<int>(i, 4);
        }
    }

    {
        cv::Mat dstU08(cv::Size(width, height), CV_8UC1, pimgdest);
        dst.convertTo(dstU08, CV_8UC1);
    }
#endif
}

#ifdef IPP_SUPPORT
static void ThresholdAndBinarization1(iana_t *piana, U32 width, U32 height, U32 maxth, U32 minth, U08 *pbuf[5])
{	

	U08 *pimgsrc1;
	U08 *pimgdest;
	IppiSize roisize;

	U32 threshold;
	U32 threshold0;
	Ipp64f mean;				// Ipp64f == double
	Ipp64f stdev;

	roisize.width = width;
	roisize.height = height;	

#define THRESHOLD_TEST	30
	threshold = THRESHOLD_TEST;
	pimgsrc1 = pbuf[0];
	pimgdest = pbuf[1];
	ippiMean_StdDev_8u_C1R(pimgsrc1, width, roisize, &mean, &stdev);

#define THSTDMULT		(-0.5)
	threshold = (U32)(mean + THSTDMULT * stdev);

#ifdef DEBUG_BALLDETECT
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,
			"mean = %lf, stdev: %lf, threshold: %d\n", mean, stdev, threshold);
#endif
	//-----------------------
	{
		// 3-1> Histogram
		U32 cnt;
		U32 a0;
		U32 a1;
		U32 h0;
		U32 nonezero;
		U32 firstnonezero;
		U32 foundit;
#define HISTCOUNT	256
#define HISTMASK	(0xF8)              		// 1111 1000 = F8
#define HISTEXP		2
#define HISTWIDTH	(1<<HISTEXP)
		U32 hist[HISTCOUNT];
		U32 i;

		{
			Ipp32s levels[256];
			ippiHistogramEven_8u_C1R(pimgsrc1, width, roisize, (Ipp32s*)hist, levels,
				HISTCOUNT / HISTWIDTH + 1, 0, 256);
		}

		cnt = 0;
		for (i = 0; i < HISTCOUNT / HISTWIDTH; i++) {
			cnt += hist[i];
		}
#define TH_N	5
#define TH_M	4
		a0 = HISTCOUNT / HISTWIDTH;
		a1 = 0;
		nonezero = 0;
		firstnonezero = 0;
		h0 = 0;
		foundit = 0;
		for (i = HISTCOUNT / HISTWIDTH; i > 0; i--) {
			if (hist[i] != 0) {
				if (firstnonezero == 0) {
					firstnonezero = i;
				}

				if (h0 < hist[i]) {
					a0 = i;
					h0 = hist[i];
				}
				nonezero++;
				if (nonezero >= TH_N) {
					foundit = 1;
					break;
				}
			}
			else {
				h0 = 0;
				nonezero = 0;	// reset non-zero count.
			}
		}

		if (foundit == 0) {		// -_-;
			a0 = firstnonezero;
			h0 = hist[a0];
		}

		foundit = 0;
		for (i = a0; i > 0; i--) {
			if (hist[i] > (U32)(h0 * TH_M)) {
				foundit = 1;
				a1 = i;
				break;
			}
		}

		a0 *= HISTWIDTH;
		a1 *= HISTWIDTH;

#define A0_RATIO_F		(3./4.)
#define A0_RATIO_N		(3./4.)

#define A0_TH0_RATIO		0.9
		threshold0 = (U32)(a0 * A0_TH0_RATIO);
		if (foundit) {
#define THRESHOLDRRR	0.9
			threshold = (U32)((a0 + a1) / 2.);
			threshold = (U32)(threshold * THRESHOLDRRR);
		}
		else {
			threshold = (U32)(a0 * A0_RATIO_N);
		}
		{
#define SELECTRATE (0.95)
			double rate0 = SELECTRATE;
			int checkcount;
			int countsum;
			checkcount = (int)(cnt * rate0);
			countsum = 0;
			for (i = 0; i < HISTCOUNT / HISTWIDTH; i++) {
				countsum += hist[i];
				if (countsum > checkcount) {
					threshold0 = (U32)((i + 0.5) * (HISTWIDTH));
#ifdef DEBUG_BALLDETECT
					cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] countsum: %d, cnt: %d, threshold0: %lf\n", i, countsum, cnt, threshold0);
#endif
					break;
				}
			}
		}

#ifdef DEBUG_BALLDETECT
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "th: %lf -> %lf\n", threshold, threshold0);
#endif
		if (threshold < threshold0) {
			threshold = threshold0;
		}
		else {
		}
#ifdef DEBUG_BALLDETECT
		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "a0: %d, a1: %d, theshold: %d\n", a0, a1, threshold);
#endif

		if (threshold > maxth) {
			threshold = maxth;
		}

		if (threshold < minth) {
			threshold = minth;
		}
	}

#ifdef DEBUG_BALLDETECT
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "----> threshold: %d\n", threshold);
#endif

	// consider adaptive thresholding
	ippiCompareC_8u_C1R(
		pimgsrc1, width, (Ipp8u)threshold,	// source
		pimgdest, width,	// binary image. dest.
		roisize, ippCmpGreater
	);

	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width, height, 2, 1, 2, 3, piana->imguserparam);
	}
#else
static void ThresholdAndBinarization1(iana_t *piana, U32 width, U32 height, U32 maxth, U32 minth, U08 *pBufSrc, U08 *pBufDest)
{
    cv::Size imgSize(width, height);
    cv::Mat cvImgSrc(imgSize, CV_8UC1, pBufSrc), cvImgDest(imgSize, CV_8UC1, pBufDest);

    U32 threshold;
    U32 threshold0;

    {
        cv::Mat cvMean, cvStdev;

        cv::meanStdDev(cvImgSrc, cvMean, cvStdev);

#define THSTDMULT		(-0.5)
        threshold = (U32)(cvMean.at<double>(0) + THSTDMULT * cvStdev.at<double>(0));
    }

    {
#define HISTEXP		2
#define HISTCOUNT	256
#define HISTWIDTH	(1<<HISTEXP)
        U32 cnt;
        U32 a0;
        U32 a1;
        U32 h0;
        U32 nonezero;
        U32 firstnonezero;
        U32 foundit;
        I32 i;

        cv::MatND histo;
        float channelRange[] ={ 0.0, 256.0 };
        const float *channelRanges ={ channelRange };
        int histo_cnt = HISTCOUNT / HISTWIDTH;

        cv::calcHist(&cvImgSrc, 1, 0, cv::Mat(), histo, 1, &histo_cnt, &channelRanges, true, false);
        cnt = 0;
        for (i = 0; i < HISTCOUNT / HISTWIDTH; i++) {
            cnt += (int)histo.at<float>(i);
        }
#define TH_N	5
#define TH_M	4
        a0 = HISTCOUNT / HISTWIDTH;
        a1 = 0;
        nonezero = 0;
        firstnonezero = 0;
        h0 = 0;
        foundit = 0;
        for (i = HISTCOUNT / HISTWIDTH-1; i >= 0; i--) {
            if ((int)histo.at<float>(i) != 0) {
                if (firstnonezero == 0) {
                    firstnonezero = i;
                }

                if (h0 < (U32)histo.at<float>(i)) {
                    a0 = i;
                    h0 = (int)histo.at<float>(i);
                }
                nonezero++;
                if (nonezero >= TH_N) {
                    foundit = 1;
                    break;
                }
            } else {
                h0 = 0;
                nonezero = 0;	// reset non-zero count.
            }
        }

        if (foundit == 0) {		// -_-;
            a0 = firstnonezero;
            h0 = (int)histo.at<float>(a0);
        }

        foundit = 0;
        for (i = a0; i > 0; i--) {
            if ((U32)histo.at<float>(i) > (U32)(h0 * TH_M)) {
                foundit = 1;
                a1 = i;
                break;
            }
        }

        a0 *= HISTWIDTH;
        a1 *= HISTWIDTH;

#define A0_RATIO_F		(3./4.)
#define A0_RATIO_N		(3./4.)

#define A0_TH0_RATIO		0.9
        threshold0 = (U32)(a0 * A0_TH0_RATIO);
        if (foundit) {
#define THRESHOLDRRR	0.9
            threshold = (U32)((a0 + a1) / 2.);
            threshold = (U32)(threshold * THRESHOLDRRR);
        } else {
            threshold = (U32)(a0 * A0_RATIO_N);
        }

        {
#define SELECTRATE (0.95)
            double rate0 = SELECTRATE;
            int checkcount;
            int countsum;
            checkcount = (int)(cnt * rate0);
            countsum = 0;
            for (i = 0; i < HISTCOUNT / HISTWIDTH; i++) {
                countsum += (int)histo.at<float>(i);
                if (countsum > checkcount) {
                    threshold0 = (U32)((i + 0.5) * (HISTWIDTH));
#ifdef DEBUG_BALLDETECT
                    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] countsum: %d, cnt: %d, threshold0: %lf\n", i, countsum, cnt, threshold0);
#endif
                    break;
                }
            }
        }
        if (threshold < threshold0) {
            threshold = threshold0;
        }

        if (threshold > maxth) {
            threshold = maxth;
        }

        if (threshold < minth) {
            threshold = minth;
        }
            }

    cv::threshold(cvImgSrc, cvImgDest, threshold, 255., cv::THRESH_BINARY);

    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pBufDest, width, height, 2, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgDest.data, cvImgDest.cols, cvImgDest.rows, 3, 1, 2, 3, piana->imguserparam);
        threshold++; threshold--;
    }
#endif
}

#ifdef IPP_SUPPORT
static void ThresholdAndBinarization2(iana_t *piana, U32 camid, U32 width, U32 height, U32 halfWinSize, double stdmult, U08 *pbuf[5]) 
{	

	U08 *pimgsrc1;
	U08 *pimgsrc2;
	U08 *pimgdest;
	IppiSize roisize;	

	I32 i, j, ii, jj;
	U32 IntegWinsize;				// Window half size
	IppiSize roiSizeInteg;
	Ipp32f *pInteg;
	double sum, value;
	IppStatus status;


	roisize.width = width;
	roisize.height = height;


	pInteg = (Ipp32f *)malloc((width + 10) * (height + 10) * sizeof(Ipp64f));

	IntegWinsize = width;
	roiSizeInteg.width = width;
	roiSizeInteg.height = height;

	pimgsrc1 = pbuf[0];
	pimgdest = pbuf[1];
	pimgsrc2 = pbuf[2];
	status = ippiIntegral_8u32f_C1R(
			pimgsrc1,
			width,
			pInteg,
			(IntegWinsize + 1) * sizeof(Ipp32f),
			roiSizeInteg, 0.0);

	__try { 				// __try / __except().. 20180407
		for (i = 0; i < (I32)height - (I32)(halfWinSize * 2); i++) {
			I32 iihwN, iihwP;
			I32 jjhwNN, jjhwNP, jjhwPN, jjhwPP;
			ii = i + halfWinSize;

			iihwN = (ii - halfWinSize) * (IntegWinsize + 1);
			iihwP = (ii + halfWinSize) * (IntegWinsize + 1);

			if (ii >= (I32)height) {			// check 
				continue;
			}

			for (j = 0; j < (I32)width - (I32)(halfWinSize * 2); j++) {
				jj = j + halfWinSize;
				if (jj < 0 || jj >= (I32)width) {
					continue;
				}

				jjhwNN = iihwN + jj - halfWinSize;
				jjhwNP = iihwN + jj + halfWinSize;
				jjhwPN = iihwP + jj - halfWinSize;
				jjhwPP = iihwP + jj + halfWinSize;


				if (jjhwNN < 0 || jjhwNN >(I32) (height*width - 1)) {			// check 
					continue;
				}
				if (jjhwNP < 0 || jjhwNP >(I32) (height*width - 1)) {			// check 
					continue;
				}
				if (jjhwPN < 0 || jjhwPN >(I32) (height*width - 1)) {			// check 
					continue;
				}
				if (jjhwPP < 0 || jjhwPP >(I32) (height*width - 1)) {			// check 
					continue;
				}

				sum = *(pInteg + jjhwNN)
					- *(pInteg + jjhwNP)
					- *(pInteg + jjhwPN)
					+ *(pInteg + jjhwPP);

				sum = sum / (halfWinSize * halfWinSize * 4);

#define SUMMULT	0.8f // 0.7f, 1.0f, 0.5f
#define SUMSUM	0x20 // 0x50, 0x00
#define SUMMULT2 1.0f // 2.0f
				sum = sum * SUMMULT;
				*(pimgsrc2 + width * ii + jj) = (U08)sum;
				value = (*(pimgsrc1 + jj + width * ii) - sum + SUMSUM);
				//value = (*(pimgsrc1 + j + width*i));
				value *= SUMMULT2;
				if (value < 0) {
                    value = 0;
                }
				if (value > 0xFF) {
                    value = 0xFF;
                }
				*(pimgdest + width * ii + jj) = (U08)value;
			}
		}
	}
	__except (GetExceptionCode() == EXCEPTION_ACCESS_VIOLATION
		|| GetExceptionCode() == EXCEPTION_ARRAY_BOUNDS_EXCEEDED
		) {

	}


	__try { 				// __try / __except().. BTS-4-39  �߻�����. 2018/0423
		for (i = 0; i < (I32)halfWinSize; i++) {
			for (j = 0; j < (I32)width - (I32)(halfWinSize * 2); j++) {
				*(pimgdest + width * i + j) = *(pimgsrc1 + width * i + j);
			}
		}

		for (i = (I32)(height - halfWinSize); i < (I32)height; i++) { 		// BTS-4-39  �߻�����. 2018/0423
			for (j = 0; j < (I32)width - (I32)(halfWinSize * 2); j++) {
				*(pimgdest + width * i + j) = *(pimgsrc1 + width * i + j);
			}
		}


	}
	__except (GetExceptionCode() == EXCEPTION_ACCESS_VIOLATION
		|| GetExceptionCode() == EXCEPTION_ARRAY_BOUNDS_EXCEEDED
		) {

	}
	free(pInteg);
		
	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width, height, 2, 1, 2, 3, piana->imguserparam);	
	}

	{	// Erase halfwin..
		for (i = 0; i < (I32)halfWinSize; i++) {
			for (j = 0; j < (I32)width; j++) {
				*(pimgdest + width * i + j) = 0;
				*(pimgdest + width * (height - i - 1) + j) = 0;
			}
		}
		for (i = 0; i < (I32)height; i++) {
			for (j = 0; j < (I32)halfWinSize; j++) {
				*(pimgdest + width * i + j) = 0;
				*(pimgdest + width * i + (width - j - 1)) = 0;
			}
		}
	}
		
	{
		U32 threshold;
		U32 threshold_min;
		Ipp64f mean;				// Ipp64f == double
		Ipp64f stdev;

		pimgsrc1 = pimgdest;
		pimgdest = pbuf[0];
		ippiMean_StdDev_8u_C1R(pimgsrc1, width, roisize, &mean, &stdev);
		threshold = (U32)(mean + stdmult * stdev);

#define MINTHRESHOLD	60 // 35, 50, 55, 70
#define MINTHRESHOLD_EYEXO_CAM0	60 // 70, 80, 150		// 20200928
#define MINTHRESHOLD_EYEXO_CAM1	60					    // 20200928

#ifdef DEBUG_BALLDETECT
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
            "[%d]m: %lf, stdmult: %lf, stdev: %lf ----> threshold: %d\n", 
            camid, mean,  stdmult, stdev,	threshold);
#endif
		if (piana->camsensor_category == CAMSENSOR_EYEXO) {
			if (camid == 1) {
				threshold_min = MINTHRESHOLD_EYEXO_CAM1;
			} else {
				threshold_min = MINTHRESHOLD_EYEXO_CAM0;
			}
		} else {
			threshold_min = MINTHRESHOLD;
		}

		if (threshold < threshold_min) {
#ifdef DEBUG_BALLDETECT
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                       "[%d]m: %lf, stdmult: %lf, stdev: %lf, threshold: %d (-> %d)\n", 
                       camid, mean,  stdmult, stdev,	threshold, MINTHRESHOLD);
#endif
			threshold = threshold_min;
		}

		// consider adaptive threshold here
		ippiCompareC_8u_C1R(
			pimgsrc1, width, (Ipp8u)threshold,	// source
			pimgdest, width,	// binary image. dest.
			roisize, ippCmpGreater
		);

		if (piana->himgfunc) {
			((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width, height, 2, 1, 2, 3, piana->imguserparam);
		}
	}
#else
static void ThresholdAndBinarization2(iana_t *piana, U32 camid, U32 width, U32 height, U32 halfWinSize, double stdmult, U08 *pbufsrc, U08 *pbufdest, U08 *pbuf[5])
{
    I32 i, j;
    double sum, value;
    double *pImgInteg;
    cv::Mat cvImgSrc(cv::Size(width, height), CV_8UC1, pbufsrc),
        cvImgSrc2(cv::Size(width, height), CV_8UC1, pbuf[2]);
    cv::Mat cvImgInteg;
    int addrMid, addrU, addrB, addrCenter, addrUL, addrUR, addrBL, addrBR;

    cv::integral(cvImgSrc, cvImgInteg, CV_64FC1);
    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbufsrc, width, height, 2, 1, 2, 3, piana->imguserparam);
    }

    pImgInteg = (double*)cvImgInteg.data;
    for (i = 0; i < (I32)height - (I32)(halfWinSize * 2); i++) {
        addrU = i * (width+1);
        addrMid = (i + halfWinSize) * (width);
        addrB = (i + halfWinSize*2) * (width+1);

        for (j = 0; j < (I32)width - (I32)(halfWinSize * 2); j++) {
            addrUL = addrU + j;
            addrBL = addrB + j;
            addrCenter = addrMid + j + halfWinSize;
            addrUR = addrU + j + halfWinSize*2;
            addrBR = addrB + j + halfWinSize*2;

            sum = *(pImgInteg+addrUL)
                - *(pImgInteg+addrUR)
                - *(pImgInteg+addrBL)
                + *(pImgInteg+addrBR);

            sum = sum / (halfWinSize * halfWinSize * 4);

#define SUMMULT	0.8f // 0.7f, 1.0f, 0.5f
#define SUMSUM	0x20 // 0x50, 0x00
#define SUMMULT2 1.0f // 2.0f
            sum = sum * SUMMULT;
            value =  (double)*(pbufsrc+addrCenter) - sum + SUMSUM;
            
            value *= SUMMULT2;
            if (value < 0) {
                value = 0;
            }
            if (value > 0xFF) {
                value = 0xFF;
            }
            *(pbuf[2]+addrCenter) = (U08)value;
        }
    }
    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[2], width, height, 2, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgSrc2.data, width, height, 3, 1, 2, 3, piana->imguserparam);
    }

    cvImgSrc(
        cv::Range(0, halfWinSize), cv::Range(0, width - halfWinSize*2)
    ).copyTo(
        cvImgSrc2(
            cv::Range(0, halfWinSize), cv::Range(0, width - halfWinSize*2)
        )
    );

    cvImgSrc(
        cv::Range(height - halfWinSize, height), cv::Range(0, width - halfWinSize*2)
    ).copyTo(
        cvImgSrc2(
            cv::Range(height - halfWinSize, height), cv::Range(0, width - halfWinSize*2)
        )
    );

    cvImgSrc(
        cv::Range(height-halfWinSize, height), cv::Range(0, width - halfWinSize*2)
    ).copyTo(
        cvImgSrc2(
            cv::Range(height-halfWinSize, height), cv::Range(0, width - halfWinSize*2)
        )
    );

    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgSrc2.data, width, height, 2, 1, 2, 3, piana->imguserparam);
    }

    cvImgSrc2(cv::Rect(0, 0, width, halfWinSize)) = 0;
    cvImgSrc2(cv::Rect(0, height-halfWinSize, width, halfWinSize)) = 0;
    cvImgSrc2(cv::Rect(0, 0, halfWinSize, height)) = 0;
    cvImgSrc2(cv::Rect(width-halfWinSize, 0, halfWinSize, height)) = 0;

    {
        U32 threshold;
        U32 threshold_min;
        double mean, stdev;   // Ipp64f == double
        
        cv::Mat cvImgDst(cv::Size(width, height), CV_8UC1, pbufdest);
        cv::Mat cvMean, cvStdev;

        cv::meanStdDev(cvImgSrc2, cvMean, cvStdev);
        mean = cvMean.at<double>(0);
        stdev = cvStdev.at<double>(0);
        threshold = (U32)(mean + stdmult * stdev);

#define MINTHRESHOLD	60 // 35, 50, 55, 70
#define MINTHRESHOLD_EYEXO_CAM0	60 // 70, 80, 150		// 20200928
#define MINTHRESHOLD_EYEXO_CAM1	60					    // 20200928

        if (piana->camsensor_category == CAMSENSOR_EYEXO) {
            if (camid == 1) {
                threshold_min = MINTHRESHOLD_EYEXO_CAM1;
            } else {
                threshold_min = MINTHRESHOLD_EYEXO_CAM0;
            }
        } else {
            threshold_min = MINTHRESHOLD;
        }

        if (threshold < threshold_min) {
            threshold = threshold_min;
        }

        // consider adaptive threshold here
        cv::threshold(cvImgSrc2, cvImgDst, threshold, 255, cv::THRESH_BINARY);
        
        if (piana->himgfunc) {
            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbufdest, width, height, 2, 1, 2, 3, piana->imguserparam);
        }
    }
#endif
}

#ifdef IPP_SUPPORT
static void MorphologyOperation(U32 width, U32 height, U08 *pimgdest, U08 *pbuf[5])
{

	U08 *pimgsrc1;
	U08 *pimgsrc2;	
	IppiSize roisize;
	IppiSize mproisize;
	U32 mproioffset;

	mproioffset = MPROIOFFSET * width + MPROIOFFSET;;

	mproisize.width = width - 2 * MPROIOFFSET;
	mproisize.height = height - 2 * MPROIOFFSET;

	roisize.width = width;
	roisize.height = height;

	//-----------  (3) Dilate 3x3
	pimgsrc1 = pimgdest;
	pimgdest = pbuf[2];

	ippiDilate3x3_8u_C1R(
		pimgsrc1 + mproioffset, width, // source
		pimgdest + mproioffset, width, // dest
		mproisize
	);
	

	//-----------  (4) Erode 3x3
	pimgsrc1 = pimgdest;
	pimgdest = pbuf[3];

	ippiErode3x3_8u_C1R(
		pimgsrc1 + mproioffset, width, // source
		pimgdest + mproioffset, width, // dest
		mproisize
	);


	//-----------  (5) Subtract
	pimgsrc1 = pbuf[3]; // Result of Erode
	pimgsrc2 = pbuf[2]; // Result of Dilate
	pimgdest = pbuf[0];

	ippiSub_8u_C1RSfs(								// C = A - B
		pimgsrc1, width, // source, 	// B
		pimgsrc2, width, // source, 	// A
		pimgdest, width,		// C, edge result.
		roisize, 0);
#else
static void MorphologyOperation(U32 width, U32 height, U08 *pimgsrc, U08 *pimgdest, U08 *pbuf[5])
{
    cv::Mat src(cv::Size(width, height), CV_8UC1, pimgsrc), 
        dilated(cv::Size(width, height), CV_8UC1, pbuf[2]),
        eroded(cv::Size(width, height), CV_8UC1, pbuf[3]),
        dst(cv::Size(width, height), CV_8UC1, pimgdest);
    cv::Mat kernel;
    
    kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3));

    cv::morphologyEx(src, dilated, cv::MORPH_DILATE, kernel, cv::Point(-1, -1), 1, cv::BORDER_ISOLATED);
    cv::morphologyEx(src, eroded, cv::MORPH_ERODE, kernel, cv::Point(-1, -1), 1, cv::BORDER_ISOLATED);
    cv::subtract(dilated, eroded, dst);
#endif
}

static U32 CheckCircle1(iana_t *piana, U32 camid, U32 width, U32 height, U08 *pimgdest, double cermax,
						iana_ballcandidate_t bc[MAXCANDIDATE], imagesegment_t imgseg[MAXLABEL])
{
	U32 bccount;
	
	U08*	pimgd;
	U32 points;
	U32 sd;

	double xarr[MAXPOINTS + 10];
	double yarr[MAXPOINTS + 10];

	U32 i, j, k;
	imagesegment_t 	*pis;

	double icx;
	double icy;
	double icr;
	double cer;

	double crmin;
	double crmax;

	double crminratio;
	double crmaxratio;
		
        //--
	pimgd = pimgdest;				// previous result.. 
	bccount = 0;

	crmaxratio = IANA_BALLDETECT_GetMaxCircleRatio(piana->camsensor_category, camid);
	crminratio = IANA_BALLDETECT_GetMinCircleRatio(piana->camsensor_category, camid);	
	
	crmin = (piana->pic[camid]->icp.RunBallsizePMin / 2.0) * crminratio;
	crmax = (piana->pic[camid]->icp.RunBallsizePMax / 2.0) * crmaxratio;

	for (k = 0; k < MAXLABEL; k++) {
#if defined (USE_IANA_BALL)
		bc[bccount].cexist = IANA_BALL_EMPTY;
#else
		bc[bccount].cexist = 0;
#endif
		pis = &imgseg[k];
		points = 0;
		if (pis->state == IMAGESEGMENT_FULL) {
			if (piana->camsensor_category != CAMSENSOR_CONSOLE1) {
				for (i = pis->iyu; i <= pis->iyb; i++) {
					for (j = pis->ixl; j <= pis->ixr; j++) {
						sd = *(pimgd + j + i * width);
						if (sd == k) {
                            // why adding 0.5?
							xarr[points] = j + 0.5;
							yarr[points] = i + 0.5;
							points++;
							if (points >= MAXPOINTS) {
								break;
							}
						}
					}
					if (points >= MAXPOINTS) {
						break;
					}
				}
			}
			else {
				for (i = pis->iyu; i <= pis->iyb; i++) {
					for (j = pis->ixl; j <= pis->ixr; j++) {
						if (i < j + (pis->iyu + pis->iyb) / 2 - pis->ixl) {
							sd = *(pimgd + j + i * width);
							if (sd == k) {
								xarr[points] = j + 0.5;
								yarr[points] = i + 0.5;
								points++;
								if (points >= MAXPOINTS) {
									break;
								}
							}
						}
						else {
							continue;
						}
					}
					if (points >= MAXPOINTS) {
						break;
					}
				}
			}

			if (points > MINPOINTS) {
				U32 res;
				double cwidth, cheight;
				double whrate;

				cwidth = (double)(pis->ixr - pis->ixl + 1);
				cheight = (double)(pis->iyb - pis->iyu + 1);

				res = check_circle(		// 1: success, 0: fail.
						&xarr[0],
						&yarr[0],
						points,
						&icx,	// OUTPUT, x of center
						&icy,	// OUTPUT, y of center
						&icr,	// OUTPUT, radius of circle 
						&cer		// OUTPUT, normalized error
				);

#if 1
				if (s_usecircleRANSAC) {
					if (res == 0 || cer > cermax) {
						double icxR, icyR, icrR, cerR;
#ifdef DEBUG_BALLDETECT
						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "circle [%2d:%d] %d  c(%10lf, %10lf), r(%10lf), cer(%10f)\n",
									camid,
									k, 
									res,
									icx, icy, icr, cer
									);
#endif
						res = check_circle_RANSAC(
								&xarr[0],
								&yarr[0],
								points,
								crmin,
								crmax,
								&icxR,	// OUTPUT, x of center
								&icyR,	// OUTPUT, y of center
								&icrR,	// OUTPUT, radius of circle 
								&cerR	// OUTPUT, normalized error
						);

						if (res != 0) {
#ifdef DEBUG_BALLDETECT
							cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                                    "circle_RANSAC [%2d:%d] %d  C(%10lf, %10lf), r(%10lf), cer(%10f)\n",
                                    camid, k, res, icxR, icyR, icrR, cerR);
#endif
							icx = icxR;
							icy = icyR;
							icr = icrR;
							cer = cerR;
						}
						else {
							icx = 0;
							icy = 0;
							icr = 999;
							cer = 999;
						}
					}
				}
#endif

#ifdef DEBUG_BALLDETECT
				if (k == 1) {
				    cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,
					    "[%2d]:%d, C(%10lf, %10lf), r(%10lf), cer(%10f)\n",
					    k, points, icx, icy, icr, cer);
				}
#endif

				if (cheight < 1 || cwidth < 1) {
					continue;
				}
				whrate = cwidth / cheight;
				if (whrate > 1) {
					whrate = 1 / whrate;
				}
				if (res == 1
						&& (cer < cermax && (icr > crmin && icr < crmax))
						&& (icr < cwidth && icr < cheight)
						&& (icx > pis->ixl && icx < pis->ixr)
						&& (icy > pis->iyu && icy < pis->iyb)
						&& (whrate > WHRATE)
					)
						//if (res == 1 && (cer < cermax && (icr >crmin && icr < crmax))) 
				{
					if (icx + icr < width && icy + icr < height) {
						bc[bccount].P.x = icx;
						bc[bccount].P.y = icy;
						bc[bccount].P.z = 0.0;
						bc[bccount].cr = icr;
						bc[bccount].cer = cer;
#if defined (USE_IANA_BALL)
						bc[bccount].cexist = IANA_BALL_EXIST;
#else
						bc[bccount].cexist = 1;
#endif
						bccount++;
#ifdef DEBUG_BALLDETECT
						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                                "bc[%d]<%lf,%lf:%lf,%lf>\n",
                                bccount, icx, icy, icr, cer);
#endif
						if (bccount >= MAXCANDIDATE) {
							break;
						}
					}
				} 	// if (res == 1 && (cer < cermax && (icr >crmin && icr < crmax)))
			} 	// if (points > MINPOINTS) 
		} // if (pis->state == IMAGESEGMENT_FULL) 
	} 	// for (k = 0; k < MAXLABEL; k++) 
#ifdef DEBUG_BALLDETECT
	//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "bccount: %d\n", s_bccount);
#endif
	return bccount;
}


static U32 CheckCircle2(iana_t *piana, U32 camid, U32 width, U32 height, U08 *pimgdest, double cermax, 
							U08 *pbuf[5], iana_ballcandidate_t bc[MAXCANDIDATE], imagesegment_t imgseg[MAXLABEL]) 
{	
	U08 *pimgErode;
	U32 bccount;

	U08*	pimgd;
	U32 points;
	U32 sd;

	double xarr[MAXPOINTS + 10];
	double yarr[MAXPOINTS + 10];

	U32 i, j, k;
	imagesegment_t 	*pis;

	double icx;
	double icy;
	double icr;
	double cer;

	double crmin;
	double crmax;

	double crminratio;
	double crmaxratio;

	double dsum;
	double drate;
		
        //--
	pimgd = pimgdest;				// previous result.. 

	pimgErode = pbuf[3];			// Result of Erode
	bccount = 0;

	crmaxratio = IANA_BALLDETECT_GetMaxCircleRatio(piana->camsensor_category, camid);
	crminratio = IANA_BALLDETECT_GetMinCircleRatio(piana->camsensor_category, camid);	

	crmin = (piana->pic[camid]->icp.RunBallsizePMin / 2.0) * crminratio;
	crmax = (piana->pic[camid]->icp.RunBallsizePMax / 2.0) * crmaxratio;

	crmax *= 1.2;			// 20200706.. 

	for (k = 0; k < MAXLABEL; k++) {
#if defined (USE_IANA_BALL)
		bc[bccount].cexist = IANA_BALL_EMPTY;
#else
		bc[bccount].cexist = 0;
#endif

		pis = &imgseg[k];
		points = 0;
		dsum = 0.0;
		if (pis->state == IMAGESEGMENT_FULL) {
			for (i = pis->iyu; i <= pis->iyb; i++) {
				for (j = pis->ixl; j <= pis->ixr; j++) {
					double dtmp;
					sd = *(pimgd + j + i * width);
					dtmp = (double) *(pimgErode + j + i * width);
#if 0//def DEBUG_BALLDETECT
					cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                            "Label [%d] %lf + %lf = %lf %lf\n", 
                            k, dsum, dtmp, dsum+dtmp, (dsum+dtmp) / (ccc * 256.0));
#endif
					dsum = dsum + dtmp;

					if (sd == k) {
						xarr[points] = j + 0.5;
						yarr[points] = i + 0.5;
						points++;
						if (points >= MAXPOINTS) {
							break;
						}
					}
				}
				if (points >= MAXPOINTS) {
					break;
				}
			}

			drate = 0;
			if (points > MINPOINTS) {
				U32 w, h;

				h = pis->iyb - pis->iyu + 1;
				w = pis->ixr - pis->ixl + 1;

				drate = dsum / ((w*h) * 256.0);
#ifdef DEBUG_BALLDETECT
				cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                        "Label [%d] %d dsum: %lf, drate: %lf\n", 
                        k, (h*w), dsum, drate);
#endif
			}


			if (points > MINPOINTS) {
				U32 res;

				double cwidth, cheight;
				double whrate;

				cwidth = (double)(pis->ixr - pis->ixl + 1);
				cheight = (double)(pis->iyb - pis->iyu + 1);


				res = check_circle(		// 1: success, 0: fail.
						&xarr[0],
						&yarr[0],
						points,
						&icx,	// OUTPUT, x of center
						&icy,	// OUTPUT, y of center
						&icr,	// OUTPUT, radius of circle 
						&cer		// OUTPUT, normalized error
				);

				if (s_usecircleRANSAC) {
						res = 0;
						if (res == 0 || cer > cermax) { // TODO: This condition statement is always true...
							double icxR, icyR, icrR, cerR;

							res = check_circle_RANSAC(
								&xarr[0],
								&yarr[0],
								points,
								crmin,
								crmax,
								&icxR,	// OUTPUT, x of center
								&icyR,	// OUTPUT, y of center
								&icrR,	// OUTPUT, radius of circle 
								&cerR	// OUTPUT, normalized error
						);

						if (res != 0) {
#ifdef DEBUG_BALLDETECT
							cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                                    "circle_RANSAC [%2d:%d] %d  C(%10lf, %10lf), r(%10lf), cer(%10f)\n",
                                    camid, k, res, icxR, icyR, icrR, cerR);
#endif
							icx = icxR;
							icy = icyR;
							icr = icrR;
							cer = cerR;
						}
						else {
							icx = 0;
							icy = 0;
							icr = 999;
							cer = 999;
						}
					}
				}

#ifdef DEBUG_BALLDETECT
				{
					if (res /* && cer < 1.0*/)
						cr_trace(CR_MSG_AREA_GENERAL,
								CR_MSG_LEVEL_VERB,
								//CR_MSG_LEVEL_WARN, 
								"[%2d:%2d]:%d, C(%10lf, %10lf), r(%10lf), cer(%10f)\n",
								camid, k, points,
								icx, icy, icr, cer);
				}
#endif

				if (cheight < 1 || cwidth < 1) {
					continue;
				}
				whrate = cwidth / cheight;
				if (whrate > 1) {
					whrate = 1 / whrate;
				}

#define DRATE_MIN	(0.3)
				if (drate < DRATE_MIN) {
                        res = 0;
#ifdef DEBUG_BALLDETECT
						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "drate: %lf too small.\n", drate);
				}
				else {
					cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "drate: %lf\n", drate);
#endif
				}

#define CIRCLECENTER_MARGINE	3 // 10
#define RADIUS_RATE	0.5
				if (res == 1
						&& (cer < cermax && (icr > crmin && icr < crmax))
						&& (icr < cwidth && icr < cheight)
						&& (icx > pis->ixl && icx < pis->ixr)
						&& (icy > pis->iyu && icy < pis->iyb)
						&& (icx > pis->ixl + icr * RADIUS_RATE && icx < pis->ixr - icr * RADIUS_RATE)
						&& (icy > pis->iyu + icr * RADIUS_RATE && icy < pis->iyb - icr * RADIUS_RATE)

						&& (icx > icr + CIRCLECENTER_MARGINE && icx + icr + CIRCLECENTER_MARGINE < width)
						&& (icy > icr + CIRCLECENTER_MARGINE && icy + icr + CIRCLECENTER_MARGINE < height)


						&& (whrate > WHRATE)
						)
				{

#ifdef DEBUG_BALLDETECT
					
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,
								"[%2d:%2d]:%d, C(%10lf, %10lf), r(%10lf), cer(%10lf) whrate(%10lf)\n",
								camid, k, points,
								icx, icy, icr, cer, whrate);
					
#endif

#define CY_CR_MULT	0.5 // 1
					if (icx > icr 
						&& icy > icr
						&& icx + icr < width 
						&& icy + icr * CY_CR_MULT < height
						) {
						bc[bccount].P.x = icx;
						bc[bccount].P.y = icy;
						bc[bccount].P.z = 0.0;
						bc[bccount].cr = icr;
						bc[bccount].cer = cer;

#if defined (USE_IANA_BALL)
						bc[bccount].cexist = IANA_BALL_EXIST;
#else
						bc[bccount].cexist = 1;
#endif
						bccount++;
#ifdef DEBUG_BALLDETECT
						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                                    "bc[%d]<%lf,%lf:r %lf (<%lf), e %lf>\n",
                                    bccount, icx, icy, icr, crmax, cer);
#endif
						if (bccount >= MAXCANDIDATE) {
							break;
						}
					}
				} 	// if (res == 1 && (cer < cermax && (icr >crmin && icr < crmax)))
				else {
#ifdef DEBUG_BALLDETECT
					if (res == 1 && cer < cermax)
					{
						cr_trace(CR_MSG_AREA_GENERAL,
								//CR_MSG_LEVEL_VERB, 
								CR_MSG_LEVEL_WARN,
								"[%2d:%2d]:%d, C(%10lf, %10lf), r(%10lf), cer(%10f), whrate(%10lf), start~end(%d %d ~ %d %d)\n",
								camid, k, points,
								icx, icy, icr, cer, whrate,
								pis->ixl,
								pis->iyu,
								pis->ixr,
								pis->iyb
						);
					}
#endif
				}
			} 	// if (points > MINPOINTS) 
		} // if (pis->state == IMAGESEGMENT_FULL) 
	} 	// for (k = 0; k < MAXLABEL; k++) 

#if 0//def DEBUG_BALLDETECT
	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "bccount: %d\n", s_bccount);
#endif

	return bccount;
}

static U32 CheckCircle3(iana_t *piana, U32 camid, U32 width, U32 height, U08 *pimgdest, double cermax, U32 prev_bccount,
								iana_ballcandidate_t bc[MAXCANDIDATE], imagesegment_t imgseg[MAXLABEL])
{	
	U32 bccount;

	U08*	pimgd;
	U32 points;
	U32 sd;

	double xarr[MAXPOINTS + 10];
	double yarr[MAXPOINTS + 10];

	U32 i, j, k;
	imagesegment_t 	*pis;

	double icx;
	double icy;
	double icr;
	double cer;

	double crmin;
	double crmax;
	double crminratio;
	double crmaxratio;

	height = height;

	bccount = prev_bccount;

	//--
	pimgd = pimgdest;				// previous result.. 

	crmaxratio = IANA_BALLDETECT_GetMaxCircleRatio(piana->camsensor_category, camid);
	crminratio = IANA_BALLDETECT_GetMinCircleRatio(piana->camsensor_category, camid);	

	crmin = (piana->pic[camid]->icp.RunBallsizePMin / 2.0) * crminratio;
	crmax = (piana->pic[camid]->icp.RunBallsizePMax / 2.0) * crmaxratio;

	for (k = 0; k < MAXLABEL; k++) {
		pis = &imgseg[k];
		points = 0;
		if (pis->state == IMAGESEGMENT_FULL) {
			for (i = pis->iyu; i <= pis->iyb; i++) {
				for (j = pis->ixl; j <= pis->ixr; j++) {
					sd = *(pimgd + j + i * width);
					if (sd == k) {
						xarr[points] = j + 0.5;
						yarr[points] = i + 0.5;

						points++;
						if (points >= MAXPOINTS) {
							break;
						}
					}
				}
				if (points >= MAXPOINTS) {
					break;
				}
			}

			if (points > MINPOINTS) {
				U32 res;

				res = check_circle(		// 1: success, 0: fail.
					&xarr[0],
					&yarr[0],
					points,
					&icx,	// OUTPUT, x of center
					&icy,	// OUTPUT, y of center
					&icr,	// OUTPUT, radius of circle 
					&cer		// OUTPUT, normalized error
				);

			{
					I32 resR;
					double icxR, icyR, icrR, cerR;

					resR = check_circle_RANSAC(
								&xarr[0],
								&yarr[0],
								points,
								crmin,
								crmax,
								&icxR,	// OUTPUT, x of center
								&icyR,	// OUTPUT, y of center
								&icrR,	// OUTPUT, radius of circle 
								&cerR	// OUTPUT, normalized error
					);

					if (resR != 0) {
#ifdef DEBUG_BALLDETECT
						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,
                                    "[%2d]:%d, C(%10lf, %10lf), r(%10lf), cer(%10f)    vs R(%d) C(%10lf, %10lf), r(%10lf), cer(%10f)\n",
                                    k, points,
                                    icx, icy, icr, cer,
                                    // vs
                                    resR,
                                    icxR, icyR, icrR, cerR
						);
#endif
						if (s_usecircleRANSAC) {
							icx = icxR;
							icy = icyR;
							icr = icrR;
							cer = cerR;
						}
					}
					else {
						if (s_usecircleRANSAC) {
							icx = 0;
							icy = 0;
							icr = 999;
							cer = 999;
						}
					}
				}
#ifdef DEBUG_BALLDETECT
				if (k == 1) {
				    cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,
						    "[%2d]:%d, C(%10lf, %10lf), r(%10lf), cer(%10f)\n",
						    k, points,
						    icx, icy, icr, cer);
				}
#endif
				if (res == 1 && (cer < cermax && (icr > crmin && icr < crmax * 2))) {
					U32 w, h;
					U32 minx, maxx;
					U32 miny, maxy;

					//--
					w = pis->ixr - pis->ixl + 1;
					h = pis->iyb - pis->iyu + 1;
#define ALPHA	0.2
					minx = pis->ixl + (U32)(w * ALPHA);
					maxx = pis->ixr - (U32)(w * ALPHA);
					miny = pis->iyu + (U32)(h * ALPHA);
					maxy = pis->iyb - (U32)(h * ALPHA);

#ifdef DEBUG_BALLDETECT
					cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                                "bc[%d]<%lf,%lf:%lf,%lf> %d x %d (%d,%d) ~ (%d, %d)\n",
                                bccount, icx, icy, icr, cer, w, h, minx, miny, maxx, maxy);
#endif

					if (
							(icx >= minx && icx <= maxx)
							&& (icy >= miny && icy <= maxy)
							)
					{
						bc[bccount].P.x = icx;
						bc[bccount].P.y = icy;
						bc[bccount].P.z = 0.0;
						bc[bccount].cr = icr;
						bc[bccount].cer = cer;
#if defined (USE_IANA_BALL)
						bc[bccount].cexist = IANA_BALL_EXIST;
#else
						bc[bccount].cexist = 1;
#endif
						bccount++;
#ifdef DEBUG_BALLDETECT
						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                                    "bc[%d]<%lf,%lf:%lf,%lf>\n",
                                    bccount, icx, icy, icr, cer);
#endif
						if (bccount >= MAXCANDIDATE) {
							break;
						}
					}
					else {
#ifdef DEBUG_BALLDETECT
							cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                                    " Out of Box.. (%d %d) ~ (%d %d)  vs (%lf %lf)\n", 
                                    pis->ixl, pis->iyu, pis->ixr, pis->iyb, icx, icy);
#endif
					}
				} 	// if (res == 1 && (cer < cermax && (icr >crmin && icr < crmax)))
			} 	// if (points > MINPOINTS) 
		} // if (pis->state == IMAGESEGMENT_FULL) 
	} 	// for (k = 0; k < MAXLABEL; k++) 

#if 0//def DEBUG_BALLDETECT
		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "bccount: %d\n", s_bccount);
#endif
	return bccount;
}

/*!
	********************************************************************************
	*	@brief   	Get Ball candidate
	*  @param[in]	piana
	*              IANA module handle
	*  @param[in]	camid
	*              camera id. 0: Master/Center, 1: Slave/Side
	*  @param[in]	buf
	*              Image buffer
	*  @param[in]	width
	*              image width
	*  @param[in]	height
	*              image height
	*  @param[out]	bc
	*              ball candidator
	*  @param[out]	pbcount
	*              ball candidator count
	*  @param[in]	maxth
	*
	*  @param[in]	minth
	*
	*  @param[in]	multv
	*
	*  @param[in]	nexp
	*
	*
	*  @return
	*
	*	@author	    yhsuk
	*  @date       2016/02/21
	*******************************************************************************/
I32 iana_getballcandidator(		
	iana_t *piana, U32 camid, U08 *buf,
	U32 width, U32 height, 			// ROI width
	U32 width2,						// Image width for buf
	iana_ballcandidate_t bc[MAXCANDIDATE], U32 *pbcount,
	U32 maxth,
	U32 minth,
	U32 multv,
	U32 nexp,
	double cermax
)
{
	I32 res;
#ifdef IPP_SUPPORT
	IppStatus status;
#endif
	U08 *pimgsrc1;
	U08 *pimgdest;

	imagesegment_t imgseg[MAXLABEL];

	U32 bccount;

	U08 *pbufall;
	U08 *pbuf[5];
	
#ifdef DEBUG_BALLDETECT
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with wxh: %dx%d \n", width, height);
#endif

	
    {
		U32 i;
        U32 len;
		U32 len2;

        len = width * height;

		len2 = (((len + (32 - 1)) / 32) * 32);		// 32 alignment
		pbufall = (U08 *)malloc(len2 *  IANA_IMGCOUNT);
		if (pbufall == NULL) {
			res = 0;
			*pbcount = 0;
			goto func_exit;
		}

		memset(pbufall, 0, (len2 *  IANA_IMGCOUNT));
		for (i = 0; i < IANA_IMGCOUNT; i++) {
			pbuf[i] = (pbufall + len2 * i);
		}
	}

	memset(&bc[0], 0, sizeof(iana_ballcandidate_t) *MAXCANDIDATE); 
    pimgsrc1 = (U08 *)buf;
    pimgdest = pbuf[0];
    //-----------  (1) Gaussian Filtering
#ifdef IPP_SUPPORT
	{	
           
      IppiSize roisize;

      U32 mproioffset;
      IppiSize mproisize;

      U32 lbroioffset;
      IppiSize lbroisize;

      roisize.width = width;
      roisize.height = height;

      mproisize.width = width - 2 * MPROIOFFSET;
      mproisize.height = height - 2 * MPROIOFFSET;
      mproioffset = MPROIOFFSET * width + MPROIOFFSET;;

      lbroisize.width = width - LABELING_MARGIN * 2;
      lbroisize.height = height - LABELING_MARGIN * 2;
      lbroioffset = (LABELING_MARGIN)* width + (LABELING_MARGIN);

		pimgsrc1 = (U08 *)buf;
		pimgdest = pbuf[0];

		//ippiFilterGauss_8u_C1R
		status = ippiFilterLowpass_8u_C1R(
			//(Ipp8u *)pimgsrc1 + mproioffset, width,
			(Ipp8u *)pimgsrc1 + MPROIOFFSET * width2 + MPROIOFFSET, width2,
			pimgdest + mproioffset, width,
			mproisize,
			ippMskSize3x3 //ippMskSize5x5
		);

	    if (multv != 1 || nexp != 0) {
		    ippiMulC_8u_C1IRSfs((Ipp8u)multv, pimgdest + mproioffset,
			    width,
			    mproisize,
			    nexp
		    );
	    }
    }
#else
    {
        cv::Mat cvImgSrc(cv::Size(width2, height), CV_8UC1, pimgsrc1), cvImgFit, cvDst;
        if (width2 > width) {
            cvImgFit = cvImgSrc(cv::Rect(0, 0, width, height));
        } else {
            cv::copyMakeBorder(cvImgSrc, cvImgFit, 0, 0, 0, width-width2, cv::BORDER_REFLECT);
	}
        cv::boxFilter(cvImgFit, cvDst, -1, cv::Size(3,3), cv::Point(-1, -1), true, cv::BORDER_ISOLATED);

        memcpy(pimgdest, cvDst.data, sizeof(U08) * cvDst.cols * cvDst.rows);

	if (multv != 1 || nexp != 0) {
            cv::Mat mulImg;
            mulImg = cvDst * multv / pow(2.0, nexp);
            memcpy(pimgdest, mulImg.data, sizeof(U08) * mulImg.cols * mulImg.rows);
        }
    }
#endif

    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width, height, 0, 1, 2, 3, piana->imguserparam);
    }

	//-----------  (2) Threshold and binarization
#ifdef IPP_SUPPORT
    ThresholdAndBinarization1(piana, width, height, maxth, minth, pbuf);
#else
    // ThresholdAndBinarization1(piana, width, height, maxth, minth, pbuf);
    ThresholdAndBinarization1(piana, width, height, maxth, minth, pbuf[0], pbuf[1]);
#endif

	//-----------  (3) Dilate 3x3 (4) Erode 3x3 (5) Subtract
#ifdef IPP_SUPPORT
	MorphologyOperation(width, height, pbuf[1], pbuf);
#else
    MorphologyOperation(width, height, pbuf[1], pbuf[0], pbuf);
#endif

	pimgdest = pbuf[0];
	
	if (piana->himgfunc) {
#if defined(TOOL_IMG)
		((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width, height, 5, 1, 2, 3, piana->imguserparam);
#endif
	}

	//-----------  (6) Labeling and segmentation
	LabellingAndSegmentation(piana, width, height, pimgdest, 0, imgseg, MAXLABEL);	

	//-----------  (7) Check Circle
	bccount = CheckCircle1(piana, camid, width, height, pimgdest, cermax, bc, imgseg);

	*pbcount = bccount;
	res = 1;

	free(pbufall);

	iana_mean_stddev_calc(piana, camid, &bc[0], MAXCANDIDATE, buf, width2, height);
func_exit:
#ifdef DEBUG_BALLDETECT
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res %d\n", res);
#endif
	return res;
}


/*!
********************************************************************************
*   @brief   	Get Ball candidate using developed edge detection
*   @param[in]	piana
*               IANA module handle
*   @param[in]	camid
*               camera id. 0: Master/Center, 1: Slave/Side
*   @param[in]	buf
*               Image buffer
*   @param[in]	width
*               image width
*   @param[in]	height
*               image height
*   @param[in]	width2
*               image width2
*   @param[out]	bc
*               ball candidator
*   @param[out]	pbcount
*               ball candidator count
*   @param[in]	halfWinSize
*               half size of window used for adaptive thresholding
*   @param[in]	multv
*               multitude value. the number of stached images
*   @param[in]	nexp
*
*   @param[in]	stdmult
*
*   @param[in]	cermax
*               some maximum value of error
*   @param[in]	cutmeanmult
*               multiplication constant to windowed mean to cut off when binaize
*
*   @return     1: success, 0: fail.
*
*	@author	    yhsuk
*   @date       2016/02/21
*******************************************************************************/
I32 iana_getballcandidatorAdapt(
	iana_t *piana, U32 camid, U08 *buf, 
	U32 width, U32 height, 			// ROI width
	U32 width2,						// Image width for buf
	iana_ballcandidate_t bc[MAXCANDIDATE], U32 *pbcount,
	U32 halfWinSize,
	U32 multv,
	U32 nexp,
	double stdmult,
	double cermax, 
    double cutmeanmult    //20190420, yhsuk.
)
{
	I32 res;

	res = iana_getballcandidatorAdapt_Legacy(
		piana, camid, buf,
		width, height,
		width2,
		bc, pbcount,
		halfWinSize,
		multv,
		nexp,
		stdmult,
		cermax
		, cutmeanmult
	);

	iana_mean_stddev_calc(piana, camid, &bc[0], MAXCANDIDATE, buf, width2, height);
	return res;
}


I32 iana_getballcandidator_set_minr_maxr(double minr, double maxr)
{
	I32 res;

	s_min_r = minr;
	s_max_r = maxr;
	s_use_minr_maxr = 1;


	res = 1;

	return res;
}


/*!
********************************************************************************
*   @brief   	Get Ball candidate using circular Hough transformation
*   @param[in]	piana
*               IANA module handle
*   @param[in]	camid
*               camera id. 0: Master/Center, 1: Slave/Side
*   @param[in]	buf
*               Image buffer
*   @param[in]	width
*               image width
*   @param[in]	height
*               image height
*   @param[in]	width2
*               image width2
*   @param[out]	bc
*               ball candidator
*   @param[out]	pbcount
*               ball candidator count
*   @param[in]	halfWinSize
*               half size of window used for adaptive thresholding
*   @param[in]	multv
*               multitude value. the number of stached images
*   @param[in]	nexp
*
*   @param[in]	stdmult
*
*   @param[in]	cermax
*               some maximum value of error
*   @param[in]	cutmeanmult
*               multiplication constant to windowed mean to cut off when binaize
*
*   @return     1: success, 0: fail.
*
*	@author	    yhsuk
*   @date       2016/02/21
*******************************************************************************/
I32 iana_getballcandidator_cht(
	iana_t *piana, U32 camid, U08 *buf,
	U32 width, U32 height, 			// ROI width
	U32 width2,						// Image width for buf
	iana_ballcandidate_t bc[MAXCANDIDATE], U32 *pbcount,
	U32 halfWinSize,
	U32 multv,
	U32 nexp,
	double stdmult,
	double cermax, 
    double cutmeanmult    //20190420, yhsuk.
)
{
	I32 res;

	double cx, cy, cr;
	U32 min_r = 20;
	U32 max_r = 40;
	I32 threshold_gradient;
	U32 threshold_vote;

	threshold_gradient = 512; // 32, 256, 1024
	threshold_vote = 256; // 32, 64, 128, 512, 400

	if (piana->camsensor_category == CAMSENSOR_EYEXO) {

#if 0
		if (camid == 0) {
			threshold_gradient = 256;
			threshold_vote = 256;
		}
		else if (camid == 1) {
			threshold_gradient = 256;
			threshold_vote = 256;

		}
#else		
		threshold_gradient = 512;
		threshold_vote = 800;
#endif

	}

	memset(&bc[0], 0, sizeof(iana_ballcandidate_t) *MAXCANDIDATE); 

	if (s_use_minr_maxr) { 
		min_r = (U32)s_min_r;
		max_r = (U32)(s_max_r + 0.5);
		s_use_minr_maxr = 0;
	} else {
		min_r = (U32)(/* 0.8* */piana->pic[camid]->icp.RunBallsizePMin / 2.0);
		//max_r = (U32)(1.1*piana->pic[camid]->icp.RunBallsizePMax / 2.0);
		max_r = (U32)(1.5*piana->pic[camid]->icp.RunBallsizePMax / 2.0);
	}

#ifdef IPP_SUPPORT
	{
		IppiSize imgroisize;
		IppiSize imgroisize1;
		U32 roioffset;


		imgroisize1.width = width;
		imgroisize1.height = height;

        // why should we do this? mult by 1 with scalefactor 0 change nothing.
        // just wanna set imagesize to width2 -> width ?
		ippiMulC_8u_C1RSfs(
			buf, width2,
			1,
			ballimgbuf1, width,
			imgroisize1,
			0
			);								// REF / 2

#define BOARDER	4

		imgroisize.width = imgroisize1.width - BOARDER;
		imgroisize.height = imgroisize1.height - BOARDER;

		roioffset = (BOARDER / 2) + (BOARDER * width) / 2;

		ippiFilterGauss_8u_C1R(
			ballimgbuf1 + roioffset, width, // source
			ballimgbuf + roioffset, width, // dest
			imgroisize,
			ippMskSize5x5
		);

		//		gamma_correction(ballimgbuf, ballimgbuf, width, height);
	}
#else
    {
        cv::Mat cvImgSrc(cv::Size(width2, height), CV_8UC1, buf), cvImgMult, cvImgRes;
        if (width2 > width) {
            cvImgMult = cvImgSrc(cv::Rect(0, 0, width, height));
        } else {
            cv::copyMakeBorder(cvImgSrc, cvImgMult, 0, 0, 0, width-width2, cv::BORDER_REFLECT);
        }

        cv::boxFilter(cvImgMult, cvImgRes, -1, cv::Size(5, 5), cv::Point(-1, -1), true, cv::BORDER_ISOLATED);
        memcpy(ballimgbuf, cvImgRes.data, sizeof(U08) * cvImgRes.cols * cvImgRes.rows);
    }
#endif

	res = iana_getballcandidator_cht_(
		ballimgbuf,
		width, height, 			// ROI width
		width,						// Image width for buf

		&cx, &cy, &cr,
		min_r, max_r,
		threshold_gradient, threshold_vote);

	if (res) {
		if (piana->himgfunc) {
			mycircleGray((int)cx, (int)cy, (int)cr, 0xFF, ballimgbuf, width);
			((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)ballimgbuf, width, height, 0, 1, 2, 3, piana->imguserparam);
		}


		*pbcount = 1;

		bc[0].P.x = cx;
		bc[0].P.y = cy;
		bc[0].P.z = 0.0;
		bc[0].cr = cr;
		bc[0].cer = 0.05;
		bc[0].cexist = IANA_BALL_EXIST;

	}
	else {
		*pbcount = 0;
		bc[0].cexist = IANA_BALL_EMPTY;
	}

	UNUSED(cutmeanmult);
	UNUSED(cermax);
	UNUSED(stdmult);
	UNUSED(nexp);
	UNUSED(multv);
	UNUSED(halfWinSize);
	UNUSED(pbcount);
	UNUSED(bc);
	UNUSED(camid);
	UNUSED(piana);

	iana_mean_stddev_calc(piana, camid, &bc[0], MAXCANDIDATE, buf, width2, height);
	return res;
}



/*!
********************************************************************************
*   @brief   	Get Ball candidate using developed edge detection
*   @param[in]	piana
*               IANA module handle
*   @param[in]	camid
*               camera id. 0: Master/Center, 1: Slave/Side
*   @param[in]	buf
*               Image buffer
*   @param[in]	width
*               image width
*   @param[in]	height
*               image height
*   @param[in]	width2
*               image width2
*   @param[out]	bc
*               ball candidator
*   @param[out]	pbcount
*               ball candidator count
*   @param[in]	halfWinSize
*               half size of window used for adaptive thresholding
*   @param[in]	multv
*               multitude value. the number of stached images
*   @param[in]	nexp
*
*   @param[in]	stdmult
*               
*   @param[in]	cermax
*               some maximum value of error
*   @param[in]	cutmeanmult
*               multiplication constant to windowed mean to cut off when binaize
*
*   @return     1: success, 0: fail.
*
*	@author	    yhsuk
*   @date       2016/02/21
*******************************************************************************/
I32 iana_getballcandidatorAdapt_Legacy(	
	iana_t *piana, U32 camid, U08 *buf,
	U32 width, U32 height, 			// ROI width
	U32 width2,						// Image width for buf
	iana_ballcandidate_t bc[MAXCANDIDATE], U32 *pbcount,
	U32 halfWinSize,
	U32 multv,
	U32 nexp,
	double stdmult,
	double cermax, 
    double cutmeanmult    //20190420, yhsuk.
)
{
#ifdef IPP_SUPPORT
	I32 res;
	IppiSize roisize;
    IppiSize mproisize;
    IppiSize lbroisize;
    IppStatus status;

	U32 mproioffset;

	U32 lbroioffset;

	U08 *pimgsrc1;
	U08 *pimgdest;

	imagesegment_t imgseg[MAXLABEL];

	U32 bccount;

	U32 len;
	U08 *pbufall;
	U08 *pbuf[5];

	bccount = 0;

	if (piana->camsensor_category == CAMSENSOR_TCAM) {
		stdmult *= 0.5;

	}

#ifdef DEBUG_BALLDETECT
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN /*CR_MSG_LEVEL_VERB*/,
		__FUNCTION__"++ with width:%d height:%d width2: %d halfWinSize: %d multv: %d nexp: %d stdmult: %lf cermax: %lf \n", 
		width, height, width2, halfWinSize, multv, nexp, stdmult, cermax);
#endif

	if (width > FULLWIDTH || height > FULLHEIGHT) {
		res = 0;
		goto func_exit;
	}

	if (width < halfWinSize * 2 || height < halfWinSize) {			// BTS-4-39  ����. 2018/0423
		res = 0;
		goto func_exit;
	}

	memset(&bc[0], 0, sizeof(iana_ballcandidate_t) *MAXCANDIDATE); 

	roisize.width = width;
	roisize.height = height;

	mproisize.width = width - 2 * MPROIOFFSET;
	mproisize.height = height - 2 * MPROIOFFSET;
	mproioffset = MPROIOFFSET * width + MPROIOFFSET;;

	lbroisize.width = width - LABELING_MARGIN * 2;
	lbroisize.height = height - LABELING_MARGIN * 2;
	lbroioffset = (LABELING_MARGIN)* width + (LABELING_MARGIN);
	len = width * height;


	{
		U32 i;
		U32 len2;

		len2 = (((len + (32 - 1)) / 32) * 32) + GUARDMEM;		// 32 alignment
		pbufall = (U08 *)malloc(len2 *  IANA_IMGCOUNT);
		if (pbufall == NULL) {
			res = 0;
			*pbcount = 0;
			goto func_exit;
		}

		memset(pbufall, 0, (len2 *  IANA_IMGCOUNT));
		for (i = 0; i < IANA_IMGCOUNT; i++) {
			pbuf[i] = (pbufall + len2 * i);
		}
	}

	pimgdest = buf;

	{
		double cutmeanvalue;
		Ipp8u subc;
		Ipp64f mean;				// Ipp64f == double
		Ipp64f stdev;

		pimgsrc1 = buf;
		pimgdest = pbuf[0];
		ippiMean_StdDev_8u_C1R(pimgsrc1, width, roisize, &mean, &stdev);
		cutmeanvalue = mean * cutmeanmult;
		//cutmeanvalue = mean * cutmeanmult/100;

		if (cutmeanvalue > 0) {
			subc = (Ipp8u)cutmeanvalue;
		}
		else {
			subc = 10;
		}
		ippiSubC_8u_C1RSfs(
			pimgsrc1, width,
			subc,
			pimgdest, width,
			roisize, 0);

#if defined(TOOL_IMG_1)
		if (piana->himgfunc) {
			((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width2, height, 5, 1, 2, 3, piana->imguserparam);
		}
#endif
	}

    // -------------------------------------------
	__try { 				// __try / __except().. 20180407
		{	//-----------  (1) Gaussian Filtering
			pimgsrc1 = pimgdest;
			pimgdest = pbuf[1];

			{
#define USE_GAMMACORRECTION_BALLCAND
#if defined(USE_GAMMACORRECTION_BALLCAND)
				pimgsrc1 = (U08 *)buf;
				pimgdest = pbuf[2];
				gamma_correction(pimgsrc1, pimgdest, width2, height);

				if (piana->himgfunc) {
					((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgsrc1, width2, height, 0, 1, 2, 3, piana->imguserparam);
					((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width2, height, 1, 1, 2, 3, piana->imguserparam);
				}

				pimgsrc1 = pbuf[2];
				pimgdest = pbuf[1];
#endif
			}


			status = ippiFilterMedianCross_8u_C1R(
				(Ipp8u *)pimgsrc1 + MPROIOFFSET * width2 + MPROIOFFSET, width2,
				pimgdest + mproioffset, width, mproisize,
				ippMskSize5x5 /*ippMskSize3x3 ippMskSize5x5*/);

			pimgsrc1 = pbuf[1];
			pimgdest = pbuf[0];

			// consider Gaussian Blur here
            status = ippiFilterLowpass_8u_C1R(
				//(Ipp8u *)pimgsrc1 + mproioffset, width,
				//(Ipp8u *)pimgsrc1 + MPROIOFFSET  * width2 +  MPROIOFFSET, width2,
				(Ipp8u *)pimgsrc1 + mproioffset, width,
				pimgdest + mproioffset, width,
				mproisize,
				ippMskSize5x5 //ippMskSize3x3
			);
			status;
		}

		if (multv != 1 || nexp != 0) {
			ippiMulC_8u_C1IRSfs((Ipp8u)multv, pimgdest + mproioffset,
				width,
				mproisize,
				nexp
			);
		}
	}
	__except (GetExceptionCode() == EXCEPTION_ACCESS_VIOLATION
		|| GetExceptionCode() == EXCEPTION_ARRAY_BOUNDS_EXCEEDED
		) {
		// .... :)
	}

	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width, height, 1, 1, 2, 3, piana->imguserparam);
	}

#ifdef DEBUG_BALLDETECT
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" Before threshold\n");
#endif

	//-----------  (2) Threshold and binarization
	ThresholdAndBinarization2(piana, camid, width, height, halfWinSize, stdmult, pbuf);

#ifdef DEBUG_BALLDETECT
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" After threshold\n");
#endif

	//-----------  (3) Dilate 3x3 (4) Erode 3x3 (5) Subtract
	MorphologyOperation(width, height, pbuf[0], pbuf);

	pimgdest = pbuf[0];
	
	memcpy(pbuf[1], pimgdest, width*height);
	
	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimgdest, width, height, 4, 1, 2, 3, piana->imguserparam);
	}

#ifdef DEBUG_BALLDETECT
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" After #1\n");
#endif

	//-----------  (6) Labeling and segmentation
	LabellingAndSegmentation(piana, width, height, pimgdest, 0, imgseg, MAXLABEL);	

#ifdef DEBUG_BALLDETECT
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" After #2\n");
#endif

	//-----------  (7) Check Circle
	bccount = CheckCircle2(piana, camid, width,  height, pimgdest, cermax, pbuf, bc, imgseg) ;

#ifdef DEBUG_BALLDETECT
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" After #3\n");
#endif

	{
		U32 i;
		if (bccount > 0) {
			pimgdest = pbuf[1];
			for (i = 0; i < bccount; i++) {
				if (bc[i].cexist == IANA_BALL_EXIST) {
					mycircleGray((int)bc[i].P.x, (int)bc[i].P.y, (int)bc[i].cr, 0xFF, (U08 *)pimgdest, width);
				}
			}
		}
		if (piana->himgfunc) {
			((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width, height, 0, 1, 2, 3, piana->imguserparam);
		}

	}
	*pbcount = bccount;
	res = 1;

	free(pbufall);
	iana_mean_stddev_calc(piana, camid, &bc[0], MAXCANDIDATE, buf, width2, height);
func_exit:
#ifdef DEBUG_BALLDETECT
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"-- with ballcount: %d, res %d\n", bccount, res);
#endif
	return res;
#else

// (
//    iana_t *piana, U32 camid, U08 *buf,
//    U32 width, U32 height, 			// ROI width
//    U32 width2,						// Image width for buf
//    iana_ballcandidate_t bc[MAXCANDIDATE], U32 *pbcount,
//    U32 halfWinSize,
//    U32 multv,
//    U32 nexp,
//    double stdmult,
//    double cermax,
//    double cutmeanmult    //20190420, yhsuk.
	I32 res;

	U08 *pimgsrc;
	U08 *pimgdest;

	imagesegment_t imgseg[MAXLABEL];

	U32 bccount;

	U32 len;
	U08 *pbufall;
	U08 *pbuf[5];

	bccount = 0;

	if (piana->camsensor_category == CAMSENSOR_TCAM) {
		stdmult *= 0.5;
	}

	if (width > FULLWIDTH || height > FULLHEIGHT) {
		res = 0;
		goto func_exit;
	}

	if (width < halfWinSize * 2 || height < halfWinSize) {
		res = 0;
		goto func_exit;
	}

	memset(&bc[0], 0, sizeof(iana_ballcandidate_t) *MAXCANDIDATE); 
	len = width * height;
	{
		U32 i;
		U32 len2;

		len2 = (((len + (32 - 1)) / 32) * 32) + GUARDMEM;		// 32 alignment
		pbufall = (U08 *)malloc(len2 *  IANA_IMGCOUNT);
		if (pbufall == NULL) {
			res = 0;
			*pbcount = 0;
			goto func_exit;
		}

		memset(pbufall, 0, (len2 *  IANA_IMGCOUNT));
		for (i = 0; i < IANA_IMGCOUNT; i++) {
			pbuf[i] = (pbufall + len2 * i);
		}
	}

    pimgdest = pbuf[0];
    {
        cv::Mat cvImgSrc(cv::Size(width2, height), CV_8UC1, buf),
            cvImgDst(cv::Size(width2, height), CV_8UC1, pimgdest);
        cv::Mat cvMean, cvStdev;
        double cutMeanValued;
        U08 cutMeanValuei;

        cv::meanStdDev(cvImgSrc, cvMean, cvStdev);
        cutMeanValued = cvMean.at<double>(0) * cutmeanmult;
        cutMeanValuei = cutMeanValued > 0 ? (U08)cutMeanValued : 0;
        cvImgDst = cvImgSrc - cutMeanValuei;
        UNUSED(multv); UNUSED(nexp);
    }
    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, width2, height, 0, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width2, height, 1, 1, 2, 3, piana->imguserparam);

        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[0], width2, height, 2, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[1], width2, height, 3, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[2], width2, height, 4, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[3], width2, height, 5, 1, 2, 3, piana->imguserparam);
    }

    // -------------------------------------------
    pimgsrc = (U08 *)buf;
    pimgdest = pbuf[2];
    gamma_correction(pimgsrc, pimgdest, width2, height);


    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgsrc, width2, height, 0, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width2, height, 1, 1, 2, 3, piana->imguserparam);

        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[0], width2, height, 2, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[1], width2, height, 3, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[2], width2, height, 4, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[3], width2, height, 5, 1, 2, 3, piana->imguserparam);
    }
    
    pimgsrc = pbuf[2];
    pimgdest = pbuf[1];
    {
        cv::Mat cvImgSrc(cv::Size(width2, height), CV_8UC1, pimgsrc), 
            cvImgFit, 
            cvImgMed(cv::Size(width, height), CV_8UC1, pimgdest);

        if (width2 > width){
            cvImgFit = cvImgSrc(cv::Rect(0, 0, width, height));
        } else {
            cv::copyMakeBorder(cvImgSrc, cvImgFit, 0, 0, 0, width - width2, cv::BORDER_REFLECT);
        }
        cv::medianBlur(cvImgFit, cvImgMed, 5);
        // memcpy(pimgdest, cvImgMed.data, sizeof(U08) * width * height);
    }
    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgsrc, width2, height, 0, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width, height, 1, 1, 2, 3, piana->imguserparam);

        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[0], width2, height, 2, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[1], width, height, 3, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[2], width2, height, 4, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[3], width2, height, 5, 1, 2, 3, piana->imguserparam);
    }

    pimgsrc = pbuf[1];
    pimgdest = pbuf[0];
    {    
        cv::Mat cvImgMed(cv::Size(width, height), CV_8UC1, pimgsrc), 
            cvImgBlurred(cv::Size(width, height), CV_8UC1, pbuf[2]),
            cvImgMult(cv::Size(width, height), CV_8UC1, pimgdest);

        cv::boxFilter(cvImgMed, cvImgBlurred, -1, cv::Size(5, 5), cv::Point(-1, -1), true, cv::BORDER_ISOLATED);
        
        if (multv != 1 || nexp != 0) {
            cvImgMult = cvImgBlurred * multv / pow(2, nexp);
        } else {
            cvImgBlurred.copyTo(cvImgMult);
        }
        // memcpy(pimgdest, cvImgMult.data, sizeof(U08)*cvImgBlurred.cols*cvImgBlurred.rows);
    }
    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgsrc, width, height, 0, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width, height, 1, 1, 2, 3, piana->imguserparam);

        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[0], width, height, 2, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[1], width, height, 3, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[2], width, height, 4, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[3], width, height, 5, 1, 2, 3, piana->imguserparam);
    }

	//-----------  (2) Threshold and binarization
    pimgsrc = pbuf[0];
    pimgdest = pbuf[1];
    ThresholdAndBinarization2(piana, camid, width, height, halfWinSize, stdmult, pimgsrc, pimgdest, pbuf);
    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgsrc, width, height, 0, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width, height, 1, 1, 2, 3, piana->imguserparam);

        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[0], width, height, 2, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[1], width, height, 3, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[2], width, height, 4, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[3], width, height, 5, 1, 2, 3, piana->imguserparam);
    }

	//-----------  (3) Dilate 3x3 (4) Erode 3x3 (5) Subtract
    pimgsrc = pbuf[1];
    pimgdest = pbuf[0];
	MorphologyOperation(width, height, pimgsrc, pimgdest, pbuf);
    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgsrc, width, height, 0, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width, height, 1, 1, 2, 3, piana->imguserparam);

        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[0], width, height, 2, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[1], width, height, 3, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[2], width, height, 4, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[3], width, height, 5, 1, 2, 3, piana->imguserparam);
    }

	//-----------  (6) Labeling and segmentation
	LabellingAndSegmentation(piana, width, height, pimgdest, 0, imgseg, MAXLABEL);	

	//-----------  (7) Check Circle
	bccount = CheckCircle2(piana, camid, width,  height, pimgdest, cermax, pbuf, bc, imgseg);

	{
		U32 i;
		if (bccount > 0) {
			pimgdest = pbuf[1];
			for (i = 0; i < bccount; i++) {
				if (bc[i].cexist == IANA_BALL_EXIST) {
					mycircleGray((int)bc[i].P.x, (int)bc[i].P.y, (int)bc[i].cr, 0xFF, (U08 *)pimgdest, width);
				}
			}
		}
		if (piana->himgfunc) {
			((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width, height, 0, 1, 2, 3, piana->imguserparam);
		}

	}
	*pbcount = bccount;
	res = 1;

	free(pbufall);
	iana_mean_stddev_calc(piana, camid, &bc[0], MAXCANDIDATE, buf, width2, height);
func_exit:
	return res;
#endif
}

static I32 LowPassFiltering( U32 width, U32 height, U32 width2, U32 multv, U32 nexp, U08 *buf, U08 *pbuf[IANA_IMGCOUNT])
{
#ifdef IPP_SUPPORT
	U08 *pimgsrc1;
	U08 *pimgdest;
	U32 mproioffset;
	IppiSize mproisize;

	mproisize.width = width - 2 * MPROIOFFSET;
	mproisize.height = height - 2 * MPROIOFFSET;
	mproioffset = MPROIOFFSET * width + MPROIOFFSET;;

	__try {
		pimgsrc1 = (U08 *)buf;
		pimgdest = pbuf[0];

		ippiFilterMedianCross_8u_C1R(
			(Ipp8u *)pimgsrc1 + MPROIOFFSET * width2 + MPROIOFFSET, width2,
			pimgdest + mproioffset, width, mproisize,
			ippMskSize5x5);


		if (multv != 1 || nexp != 0) {
			ippiMulC_8u_C1IRSfs((Ipp8u)multv, pimgdest + mproioffset,
				width,
				mproisize,
				nexp
			);
		}

		pimgsrc1 = pbuf[0];
		pimgdest = pbuf[1];


		ippiFilterLowpass_8u_C1R(
			(Ipp8u *)pimgsrc1 + mproioffset, width,
			pimgdest + mproioffset, width,
			mproisize,
			ippMskSize5x5
		);
	}
	__except (GetExceptionCode() == EXCEPTION_ACCESS_VIOLATION
		|| GetExceptionCode() == EXCEPTION_ARRAY_BOUNDS_EXCEEDED
		|| GetExceptionCode() == EXCEPTION_INT_DIVIDE_BY_ZERO
		|| GetExceptionCode() == EXCEPTION_FLT_DIVIDE_BY_ZERO
		) {

#ifdef DEBUG_BALLDETECT
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"(%d) : __except !!!\n", __LINE__);
#endif
			return CR_ERROR;
		}
#else
    cv::Mat cvImgSrc(cv::Size(width2, height), CV_8UC1, buf), cvImgFit, cvImgFiltered1, cvImgMult, cvImgFiltered2, cvImgRes;
    if (width < width2){
        cvImgFit = cvImgSrc(cv::Rect(0, 0, width, height));
    } else {
        cv::copyMakeBorder(cvImgSrc, cvImgFit, 0, 0, 0, width - width2, cv::BORDER_REPLICATE);
    }

    cv::medianBlur(cvImgFit, cvImgFiltered1, 5);


    if (multv != 1 || nexp != 0) {
        cvImgMult = cvImgFiltered1 * multv / pow(2, nexp);
    } else {
        cvImgFiltered1.copyTo(cvImgMult);
    }
    memcpy(pbuf[0], cvImgFiltered1.data, sizeof(U08) * cvImgFiltered1.cols * cvImgFiltered1.rows);

    cv::boxFilter(cvImgMult, cvImgFiltered2, -1, cv::Size(5, 5), cv::Point(-1, -1), true, cv::BORDER_REFLECT);
    memcpy(pbuf[1], cvImgFiltered1.data, sizeof(U08) * cvImgFiltered1.cols * cvImgFiltered1.rows);
#endif
	return CR_OK;        
}

static void CannyEdgeDetection(iana_t *piana, U32 width, U32 height, U08 *pbuf[IANA_IMGCOUNT])
{
#ifdef IPP_SUPPORT
	U08 *pimgsrc1;		
	U08 *pimgdest;
	IppiSize roisize;

	IppStatus sts;
	Ipp32f low = 8.0f, high = 12.0f;
	int size, size1;
	Ipp8u *src, *dst, *buffer;
	Ipp16s *dx, *dy;

	roisize.width = width;
	roisize.height = height;	

	pimgsrc1 = src = pbuf[1];
	pimgdest = dst = pbuf[0];

	low = s_low;
	high = s_high;

	sts = ippiFilterSobelNegVertGetBufferSize_8u16s_C1R(roisize, ippMskSize3x3, &size);
	sts = ippiFilterSobelHorizGetBufferSize_8u16s_C1R(roisize, ippMskSize3x3, &size1);
	if (size < size1) size = size1;

	ippiCannyGetSize(roisize, &size1);
	if (size < size1) size = size1;
		
	buffer = (U08 *)malloc(size);
	dx = (Ipp16s *)malloc(width * height * sizeof(U16));
	dy = (Ipp16s *)malloc(width * height * sizeof(U16));
		
	sts = ippiFilterSobelNegVertBorder_8u16s_C1R(src, width, dx, width * 2, roisize,
		ippMskSize3x3, ippBorderRepl, 0, buffer);
	sts = ippiFilterSobelHorizBorder_8u16s_C1R(src, width, dy, width * 2, roisize,
		ippMskSize3x3, ippBorderRepl, 0, buffer);
	sts = ippiCanny_16s8u_C1R(dx, width * 2, dy, width * 2, dst, width, roisize, low,
		high, buffer);
	free(buffer);

	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)dst, width, height, 2, 1, 2, 3, piana->imguserparam);
	}

	free(dx);
	free(dy);
#else
    cv::Mat cvImgSrc(cv::Size(width, height), CV_8UC1, pbuf[1]), cvGradX, cvGradY, cvImgDst;

    cv::Sobel(cvImgSrc, cvGradX, CV_16SC1, 1, 0, 3, 1.0, 0.0, cv::BORDER_REPLICATE);
    cv::Sobel(cvImgSrc, cvGradY, CV_16SC1, 0, 1, 3, 1.0, 0.0, cv::BORDER_REPLICATE);

    cv::Canny(cvGradX, cvGradY, cvImgDst, s_low, s_high);

    memcpy(pbuf[0], cvImgDst.data, sizeof(U08) * cvImgDst.cols * cvImgDst.rows);

    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pbuf[0], width, height, 2, 1, 2, 3, piana->imguserparam);
    }
#endif
}


/*!
********************************************************************************
*   @brief   	Get Ball candidate using canny edge detection
*   @param[in]	piana
*               IANA module handle
*   @param[in]	camid
*               camera id. 0: Master/Center, 1: Slave/Side
*   @param[in]	buf
*               Image buffer
*   @param[in]	width
*               image width
*   @param[in]	height
*               image height
*   @param[in]	width2
*               image width2
*   @param[out]	bc
*               ball candidator
*   @param[out]	pbcount
*               ball candidator count
*   @param[in]	maxth
*               maximum threshold when binarize during the algo. (should be deleted)
*   @param[in]	minth
*               minimum threshold when binarize during the algo. (should be deleted)
*   @param[in]	multv
*               multitude value. the number of stached images
*   @param[in]	nexp
*
*   @param[in]	cermax
*               some maximum value of error
*
*   @return
*
*	@author	    yhsuk
*   @date       2016/02/21
*******************************************************************************/
I32 iana_getballcandidatorCanny( // Used only once at iana_ready. All the others are deactivated.
	iana_t *piana, U32 camid, U08 *buf,		
	U32 width, U32 height, 			// ROI width
	U32 width2,						// Image width for buf
	iana_ballcandidate_t bc[MAXCANDIDATE], U32 *pbcount,
	U32 maxth,
	U32 minth,
	U32 multv,
	U32 nexp,
	double cermax
)
{
	I32 res;
	U32 iter;

//	IppStatus status;

//	U08 *pimgsrc1;
//	U08 *pimgsrc2;
	U08 *pimgdest;

	imagesegment_t imgseg[MAXLABEL];

	U32 bccount;

	U32 len;
	U08 *pbufall;
	U08 *pbuf[IANA_IMGCOUNT];

	//-----------------
#ifdef DEBUG_BALLDETECT
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with wxh: %dx%d \n", width, height);
#endif

	memset(&bc[0], 0, sizeof(iana_ballcandidate_t) *MAXCANDIDATE); 
	bccount = 0;
	for (iter = 0; iter < MAXCANDIDATE; iter++) {
#if defined (USE_IANA_BALL)
		bc[iter].cexist = IANA_BALL_EMPTY;
#else
		bc[iter].cexist = 0;
#endif
	}

	//-----------------
#define WIDTHTOOSMALL	16
#define HEIGHTTOOSMALL	16
	if ((width > FULLWIDTH || height > FULLHEIGHT || width2 > FULLWIDTH)
		|| (width < WIDTHTOOSMALL || height < HEIGHTTOOSMALL || width2 < WIDTHTOOSMALL)
		)
	{

#ifdef DEBUG_BALLDETECT
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__" width, height fail..\n");
#endif
		res = 0;
		*pbcount = 0;
		goto func_exit;
	}

	if (piana->himgfunc) {
		//((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf, width, height, 0, 1, 2, 3, piana->imguserparam);
		((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, width2, height, 0, 1, 2, 3, piana->imguserparam);
	}
	len = width * height;
	{
		U32 i;
		U32 len2;

		len2 = (((len + (32 - 1)) / 32) * 32) + GUARDMEM;		// 32 alignment
		pbufall = (U08 *)malloc(len2 *  IANA_IMGCOUNT);
		if (pbufall == NULL) {
			res = 0;
			*pbcount = 0;
			goto func_exit;
		}

		memset(pbufall, 0, (len2 *  IANA_IMGCOUNT));
		for (i = 0; i < IANA_IMGCOUNT; i++) {
			pbuf[i] = (pbufall + len2 * i);
		}
	}

	//-----------  (1) Low pass Filtering
	if(LowPassFiltering(width, height, width2, multv, nexp, buf, pbuf) != CR_OK)
	{
		if (pbufall) {
			free(pbufall);
			pbufall = NULL;
		}
		res = 0;
		*pbcount = 0;
		goto func_exit;	
	}

	pimgdest = pbuf[1];	
	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, width, height, 1, 1, 2, 3, piana->imguserparam);
	}

	CannyEdgeDetection(piana, width, height, pbuf);
	pimgdest = pbuf[0];

	UNUSED(minth);
	UNUSED(maxth);

	for (iter = 0; iter < 2; iter++) {

		//-----------  (6) Labeling and segmentation
        LabellingAndSegmentation(piana, width, height, pimgdest, iter, imgseg, 2 * MAXLABEL);

		//-----------  (7) Check Circle
		bccount = CheckCircle3(piana, camid, width, height, pimgdest, cermax, bccount, bc, imgseg);

		if (bccount > 0) {
			break;
		}
	} // for (iter = 0; iter < 2; iter++) 

	*pbcount = bccount;
	res = 1;

	free(pbufall);
	iana_mean_stddev_calc(piana, camid, &bc[0], MAXCANDIDATE, buf, width2, height);
func_exit:
#ifdef DEBUG_BALLDETECT
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res %d\n", res);
#endif
	return res;
}


U32 iana_mean_stddev_calc(iana_t *piana, U32 camid, iana_ballcandidate_t *pbc, U32 candcount, U08 *pbuf, U32 width, U32 height)
{
    U32 res;
    U32 i;
	
    //--
    for (i = 0; i < candcount; i++) { 
	    if ((pbc+i)->cexist) {
		    calc_circle_mean_stddev(pbc+i, pbuf, width, height);

#ifdef DEBUG_BALLDETECT
		    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                "[%d:%d] mean: %lf stddev: %lf   meanH: %lf stddevH: %lf\n", 
				camid, i,
				(pbc+i)->mean,
				(pbc+i)->stddev,
				(pbc+i)->meanH,
				(pbc+i)->stddevH);
#endif
        }
    }
    res = 1;

    UNUSED(piana);UNUSED(camid);
    return res;
}

double IANA_BALLDETECT_GetMaxCircleRatio(I32 camsensor_category, U32 camid)
{
	double crmaxratio;
	
	if (camsensor_category == CAMSENSOR_EYEXO) {
		crmaxratio = CRMAXRATIO_EYEXO;
	} else if (camsensor_category == CAMSENSOR_P3V2) {
			if (camid == 0) {
				crmaxratio = CRMAXRATIO_P3V2_0; 		// camid 0
			} else {
				crmaxratio = CRMAXRATIO_P3V2_1; 		// camid 1
			}
	} else {
		crmaxratio = CRMAXRATIO;
	}

	return crmaxratio;
}

double IANA_BALLDETECT_GetMaxCircleRatio2(I32 camsensor_category, U32 camid)
{
	double crmaxratio;

	if (camsensor_category == CAMSENSOR_EYEXO) {
		if (camid == 0) {
			crmaxratio = CRMAXRATIO_EYEXO0;
		} else {
			crmaxratio = CRMAXRATIO_EYEXO;
		}
	} else if ( camsensor_category == CAMSENSOR_P3V2) {
		crmaxratio = CRMAXRATIO_P3V2;	

		if (camid == 0) {
			crmaxratio = CRMAXRATIO_P3V2_0; 		// camid 0
		}
		else {
			crmaxratio = CRMAXRATIO_P3V2_1; 		// camid 1
		}
	} else {
		crmaxratio = CRMAXRATIO_Z;
	}

	return crmaxratio;	
}


double IANA_BALLDETECT_GetMinCircleRatio(I32 camsensor_category, U32 camid)
{
	double crminratio;
	
	if (camsensor_category == CAMSENSOR_EYEXO) {
		crminratio = CRMINRATIO_EYEXO;
	} else if (camsensor_category == CAMSENSOR_P3V2) {
			crminratio = CRMINRATIO;
	} else {
		crminratio = CRMINRATIO;
	}

	UNUSED(camid);

	return crminratio;	
}

#if defined (__cplusplus)
}
#endif
