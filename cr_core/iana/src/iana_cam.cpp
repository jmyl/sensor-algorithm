/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA cam
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_cam.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2015/12/20 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <string.h>

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"

#include "scamif.h"
//#include "scamif_main.h"
#include "scamif_buffer.h"
#include "iana.h"
#include "iana_work.h"
#include "iana_implement.h"
#include "iana_cam.h"
#include "iana_cam_implement.h"
#include "iana_tool.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define RUNGAINFILE	"runexposure.txt"
#define EXPVFILE	"expv.txt"



//#define XBDCHECKRETRY	10
//#define XBDCHECKRETRY	20

//#define XBDCHECKRETRY	5
//#define XBDCHECKSLEEP	50
#define XBDCHECKRETRY	100
#define XBDCHECKSLEEP	50
//#define XBDCHECKRETRY	10
//#define XBDCHECKSLEEP	1000

//#define POWERSAVING_FRAMERATE	1
//#define POWERSAVING_FRAMERATE	2
//#define POWERSAVING_FRAMERATE	5
#define POWERSAVING_FRAMERATE	15

//#define	POWERSAVING_EXPOSURE	30
//#define	POWERSAVING_EXPOSURE	200
#define	POWERSAVING_EXPOSURE	50

#define	POWERSAVING_GAIN	20000

//#define MEAN_REFERENCE_EYEXO	200.0
#define MEAN_REFERENCE_EYEXO	195.0
#define MEAN_MINRATIO_EYEXO		(0.2)
#define MEAN_MAXRATIO_EYEXO		(4.0)
//#define MEAN_GAINRATIO_BONUS_EYEXO	(2.0)
#define MEAN_GAINRATIO_BONUS_EYEXO	(2.5)

#define MEAN_REFERENCE_Z3		130.0
#define MEAN_MINRATIO_Z3		(0.2)
#define MEAN_MAXRATIO_Z3		(4.0)
#define MEAN_GAINRATIO_BONUS_Z3	(2.0)

#define EXPOSURE_RUN_CAM0_MAX	120
#define EXPOSURE_RUN_CAM0_MIN	60

#define EXPOSURE_RUN_CAM1_MAX	60
#define EXPOSURE_RUN_CAM1_MIN	30

//#define P3V2_BUNKER_GAINRATIO	(0.7)
#define P3V2_BUNKER_GAINRATIO	(0.5)

#define SCAMIF_RETRY	4

#define EXPOSURE_POST_DEFAULT		0
#define EXPOSURE_EXPIATE_DEFAULT	34


#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

static iana_camconf_t icconf_null[MAXCAMCOUNT] = 		// Low...		// 0, 1: Center/SideCAM
{
	{ // center
		64, //1280, //320, 	//  width;
		64, // 1024, // 240, 	// U32 height;
		0,	// 512,		// U32 offset_x;
		0, // 512,		// U32 offset_y;
		30, // 1, // 30,	// U32 framerate;
		(10 * 256), // U32 gain;
		1, // 0, //10,  // 50,		// U32 exposure;
		1, 		// U32 skip;
		1, 		// U32 multitude;
		1 		// U32 syncdiv
		, 110 , 100, 100, 100	// 	ledbrgt, lut_bright, lut_contrast, lut_gamma
		, 0		// U32 exposure_post
		, 34	// U32 exposure_expiate
	},
	{	// Side
		64, // 1280, //320, 	//  width;
		64, // 1024, // 240, 	// U32 height;
		0, // 512,		// U32 offset_x;
		0, // 512,		// U32 offset_y;
		30, // 1, // 30,	// U32 framerate;
		(10 * 256), // U32 gain;
		1, // 0, // 10, // 50,		// U32 exposure;
		1, 		// U32 skip;
		1, 		// U32 multitude;
		2 // 1 		// U32 syncdiv
		, 110 , 100, 100, 100	// 	ledbrgt, lut_bright, lut_contrast, lut_gamma
		, 0		// U32 exposure_post
		, 34	// U32 exposure_expiate
	},	
};



static iana_camconf_t icconf_base[MAXCAMCOUNT] =					// 0, 1: Center/SideCAM
{
	//{
		{ // center
			320, // 1280, 	//  width;
			240, // 1024, 	// U32 height;
			0,		// U32 offset_x;
			0,		// U32 offset_y;
			1,	// U32 framerate;
			(10 * 256), // U32 gain;
			20,		// U32 exposure;
			1, 		// U32 skip;
			1, 		// U32 multitude;
			1 		// U32 syncdiv
		, 110 , 100, 100, 100	// 	ledbrgt, lut_bright, lut_contrast, lut_gamma
			, 0		// U32 exposure_post
			, 34	// U32 exposure_expiate
		},
		{	// Side
			320, // 1280, 	//  width;
			240, // 1024, 	// U32 height;
			0,		// U32 offset_x;
			0,		// U32 offset_y;
			1,	// U32 framerate;
			(10 * 256), // U32 gain;
			20,		// U32 exposure;
			1, 		// U32 skip;
			1, 		// U32 multitude;
			1 		// U32 syncdiv
		, 110 , 100, 100, 100	// 	ledbrgt, lut_bright, lut_contrast, lut_gamma
			, 0		// U32 exposure_post
			, 34	// U32 exposure_expiate
		},		
};


#define CHECKREADY_FPS	20
static iana_camconf_t icconf_fullrange_CheckReady_0[MAXCAMCOUNT] =				// 0, 1: Center/SideCAM,    2: AttCam,   When camidCheckReady == 1
{
	{ // center
		1280, 	//  width;
		1024, 	// U32 height;
		0,		// U32 offset_x;
		0,		// U32 offset_y;
		CHECKREADY_FPS, // 30,  	// U32 framerate;
		2000,  		// FULLRANGE_BASEGAIN, // (10 * 256), // U32 gain;
		70,			// FULLRANGE_EXPOSURE,		// 35,  // 40, 		// 30, // 20,		// U32 exposure;
		1, 		// U32 skip;
		1,			// 2, // 1, //4, 	// 1, 		// U32 multitude;
		1 		// U32 syncdiv
		, 110 , 100, 100, 100	// 	ledbrgt, lut_bright, lut_contrast, lut_gamma
		, 0		// U32 exposure_post
		, 34	// U32 exposure_expiate
	},
	{	// Side
		1280, // 32, 	//  width;
		1024, // 32, 	// U32 height;
		0,		// U32 offset_x;
		0,		// U32 offset_y;
		CHECKREADY_FPS, // 30, // 30,		// U32 framerate;
		2000,		//FULLRANGE_BASEGAIN, // (15*256)// U32 gain;
		70,			// FULLRANGE_EXPOSURE, // 35,   // 40, // 30,  /*20, */		// U32 exposure;
		1, 		// U32 skip;
		1, // 4, //4, // 1,		// U32 multitude;
		1 		// U32 syncdiv
		, 110 , 100, 100, 100	// 	ledbrgt, lut_bright, lut_contrast, lut_gamma
		, 0		// U32 exposure_post
		, 34	// U32 exposure_expiate
	},
};




static iana_camconf_t icconf_fullrange_CheckReady_1[MAXCAMCOUNT] =				// 0, 1: Center/SideCAM,    2: AttCam,   When camidCheckReady == 1
{
	{ // center
		1280,	// 32	//  width;
		1024, 	// 32  // U32 height;
		0,		// U32 offset_x;
		0,		// U32 offset_y;
		CHECKREADY_FPS, // 30,  	// U32 framerate;
		2000,  		// FULLRANGE_BASEGAIN, // (10 * 256), // U32 gain;
		70,			// FULLRANGE_EXPOSURE,		// 35,  // 40, 		// 30, // 20,		// U32 exposure;
		1, 		// U32 skip;
		1, // 1, //4, 	// 1, 		// U32 multitude;
		1 		// U32 syncdiv
		, 110 , 100, 100, 100	// 	ledbrgt, lut_bright, lut_contrast, lut_gamma
		, 0		// U32 exposure_post
		, 34	// U32 exposure_expiate
	},
	{	// Side
		1280, 	//  width;
		1024, 	// U32 height;
		0,		// U32 offset_x;
		0,		// U32 offset_y;
		CHECKREADY_FPS, // 30, // 30,		// U32 framerate;
		2000,		//FULLRANGE_BASEGAIN, // (15*256)// U32 gain;
		70,			// FULLRANGE_EXPOSURE, // 35,   // 40, // 30,  /*20, */		// U32 exposure;
		1, 		// U32 skip;
		1,						// 2, // 4, //4, // 1,		// U32 multitude;
		1 		// U32 syncdiv
		, 110 , 100, 100, 100	// 	ledbrgt, lut_bright, lut_contrast, lut_gamma
		, 0		// U32 exposure_post
		, 34	// U32 exposure_expiate
	},
};



static iana_camconf_t icconf_run[MAXCAMCOUNT] =		// 0, 1: Center/SideCAM,    2: AttCam
{
	{ // center
		480, 			// CAM0_RUN_WIDTH,		//480, 	//  width;
		240,			// CAM0_RUN_HEIGHT,		// 240, 	// U32 height;
		0,		// U32 offset_x;			// must be changed...
		0,		// U32 offset_y;			// must be changed...
		2500,			// CAM0_RUNFRAMERATE,		// 2000, //1800,	// 2000,	// U32 framerate;
		3000,			// CAM0_RUN_BASEGAIN, // (15*256)// U32 gain;
		30,				// CAM0_RUN_EXPOSURE, // 30, // 35, // 30, //20,		// U32 exposure;
		4,				// CAM0_RUN_SKIP, 	//4, 		// U32 skip;
		4,				// CAM0_MULTITUDE,  // 4,   // 6,  // 4, //1, //4, 		// U32 multitude;
		1				// CAM0_SYNCDIV 		// U32 syncdiv
		, 110 , 100, 100, 100	// 	ledbrgt, lut_bright, lut_contrast, lut_gamma
		, 0		// U32 exposure_post
		, 34	// U32 exposure_expiate
	},

	{ // Side
		480, 		// CAM1_RUN_WIDTH,					//  width;
		240, 		// CAM1_RUN_HEIGHT,				// U32 height;
		0,		// U32 offset_x;			// must be changed...
		0,		// U32 offset_y;			// must be changed...
		2500,			// CAM0_RUNFRAMERATE,				// U32 framerate;
		3000,			// CAM1_RUN_BASEGAIN, // (15*256)// U32 gain;
		30,				// CAM1_RUN_EXPOSURE,				// U32 exposure;
		4,				// CAM1_RUN_SKIP, 					// U32 skip;
		4,				// CAM1_MULTITUDE,					// U32 multitude;
		1				// CAM1_SYNCDIV 					// U32 syncdiv
		, 110 , 100, 100, 100	// 	ledbrgt, lut_bright, lut_contrast, lut_gamma
		, 0		// U32 exposure_post
		, 34	// U32 exposure_expiate
	},
};

static iana_camconf_t icconf_run_putter[MAXCAMCOUNT] = 
{
	{ // center
		480,				// RUN_WIDTH_PUTTER, 	// 480, 	//  width;
		240,				// RUN_HEIGHT_PUTTER, 	// 240, 	// U32 height;
		0,					// U32 offset_x;	// must be changed...
		0,					// U32 offset_y;	// must be changed...
		100,				// RUNFRAMERATE_PUTTER,		// 100, //50,	// U32 framerate;
		2000,				// RUN_BASEGAIN_PUTTER, // (15*256)// U32 gain;
		30,					// RUN_EXPOSURE_PUTTER, // 40, 	// U32 exposure;
		1, 		// U32 skip;
		1, 		// U32 multitude;
		1 		// U32 syncdiv
		, 110 , 100, 100, 100	// 	ledbrgt, lut_bright, lut_contrast, lut_gamma
		, 0		// U32 exposure_post
		, 34	// U32 exposure_expiate
	},
	{ // side
		480,				//RUN_WIDTH_PUTTER, 	// 480, 	//  width;
		240,				// RUN_HEIGHT_PUTTER, 	// 240, 	// U32 height;
		0,					// U32 offset_x;	// must be changed...
		0,					// U32 offset_y;	// must be changed...
		100,				// RUNFRAMERATE_PUTTER,		// 100, //50,	// U32 framerate;
		2000,				// RUN_BASEGAIN_PUTTER, // (15*256)// U32 gain;
		30,					// RUN_EXPOSURE_PUTTER, // 40, 	// U32 exposure;
		1, 		// U32 skip;
		1, 		// U32 multitude;
		1 		// U32 syncdiv
		, 110 , 100, 100, 100	// 	ledbrgt, lut_bright, lut_contrast, lut_gamma
		, 0		// U32 exposure_post
		, 34	// U32 exposure_expiate
	},
};


static 	cameraintrinsic_t s_cip[CAMINTRINSIC_LAST+1] = {
	{
		CAMINTRINSIC_NULL,				// 0
		{1280.0, (1280.0/2), 1024.0, (1024.0/2)},	// fx, cx, fy, cy
		{0.0, 0.0, 0.0, 0.0, 0.0}					// k1, k2, p1, p2, k3
	},
	{
		CAMINTRINSIC_12mm0,				// 1
		{2.6289979124075621e+03, 6.5210869132871505e+02, 2.6318099724901112e+03, 4.4455699970677682e+02},	// fx, cx, fy, cy
		{-5.0052388751897225e-01, 8.9882018088234816e-01, 3.2939111920267574e-03, 1.1720886942373910e-03, -7.8860977423566352e+00} // k1, k2, p1, p2, k3
	},
	{
		CAMINTRINSIC_8mm0,				// 2
		{1.6653781507724193e+03, 6.7030021085116027e+02, 1.6655792718574105e+03, 5.2877306954661628e+02},	// fx, cx, fy, cy
		{-5.1668095809779835e-01, 2.8861141353274045e-01, -6.6297960753441826e-03, -5.4271240451613540e-03, -2.0533735504559578e-01} // k1, k2, p1, p2, k3
	},
	{
		CAMINTRINSIC_VARIFOCAL_3_8mm0,	// 3
		{7.3946632907260448e+02, 6.6435458616736287e+02, 7.4031452628009708e+02, 4.5585831786856937e+02},	// fx, cx, fy, cy
		{-3.8124366333573129e-01, 2.0672296756780303e-01, -2.8257787679831007e-04, -3.0702930991555439e-05, -6.3100409676798572e-02} // k1, k2, p1, p2, k3
	},

#if 0
	{
		CAMINTRINSIC_4mm0,				// 4
		{8.2979587427866352e+02, 6.8195072354966260e+02 , 8.2956909318739770e+02,  4.5798680288370247e+02},	// fx, cx, fy, cy
		{-4.3987580639608492e-01,  2.2195000948711086e-01, -1.1088961090581376e-03, -7.3015045879953227e-04, -6.0546911408520955e-02} // k1, k2, p1, p2, k3
	}
#endif

#if 1		// After 20181130
	{
		CAMINTRINSIC_4mm0,				// 4
		{8.5119937777323969e+02, 6.3950000000000000e+02 , 8.4927440891515187e+02,  5.1150000000000000e+02},	// fx, cx, fy, cy
		{-4.7783157305964813e-01,  2.6391523020399305e-01, -3.9709620629073119e-03, -1.7022405571969158e-03, -7.3587835554841427e-02} // k1, k2, p1, p2, k3
	}
#endif

	,
	{
		CAMINTRINSIC_12mm1,				// 5, 20190320
		//{2.6289979124075621e+03, 6.5210869132871505e+02, 2.6318099724901112e+03, 4.4455699970677682e+02},	// fx, cx, fy, cy
		//{-5.0052388751897225e-01, 8.9882018088234816e-01, 3.2939111920267574e-03, 1.1720886942373910e-03, -7.8860977423566352e+00} // k1, k2, p1, p2, k3
		{2.5483658847517399e+03, 5.2887949644245407e+02, 2.5493908721671896e+03, 5.1455434466861038e+02},	// fx, cx, fy, cy
		{-3.3666153995855519e-01, -1.6900219191163850e+00, 2.2578757004498352e-03, 1.4672579353715394e-03, 9.6583431546876231e+00} // k1, k2, p1, p2, k3
		// ->   XYZ = 0.327, -1.084, 2.942

	},
	{
		CAMINTRINSIC_8mm1,				// 6, 20190320
		//{1.6653781507724193e+03, 6.7030021085116027e+02, 1.6655792718574105e+03, 5.2877306954661628e+02},	// fx, cx, fy, cy
		//{-5.1668095809779835e-01, 2.8861141353274045e-01, -6.6297960753441826e-03, -5.4271240451613540e-03, -2.0533735504559578e-01} // k1, k2, p1, p2, k3

		//{1.8723627978441816e+03, 5.0056671412264291e+02, 1.8867494689007940e+03, 5.0447236091261368e+02},	// fx, cx, fy, cy
		//{-5.5892824521471851e-01, 8.7588148996521675e-02, 1.3895666634296297e-03, 2.6108560473384560e-02, 2.1746467841861328e-01} // k1, k2, p1, p2, k3
		//// ->   XYZ = -0.299, -1.154, 3.189
  
		//{1.9270970205138128e+03, 4.9802591362281748e+02, 1.9367766091833284e+03, 4.9261759340653157e+02},	// fx, cx, fy, cy
		//{-5.5773199666010365e-01, -3.9920189486519059e-01, 4.5652731723450999e-03, 2.6725170723270165e-02, 2.1811971948579041e+00} // k1, k2, p1, p2, k3

		//{1.6494617012748402e+03, 6.4893736815342095e+02, 1.6524722415913768e+03, 5.6405529847191485e+02},	// fx, cx, fy, cy
		//{-4.8606616625889087e-01, 1.7824540221631863e-01,  -9.1608164951271828e-03, -3.7980313905479879e-03, -2.3175935631989631e-04} // k1, k2, p1, p2, k3
		//// TRY3. 2.7...


		//{1.6982896923859685e+03, 5.8130205364203687e+02, 1.7061462926008626e+03, 5.1712089964052484e+02},	// fx, cx, fy, cy
		//{-4.4677781497419911e-01, -3.2159324238754683e-01,  -3.5677847112619747e-03,  6.8570711169009448e-03, 1.3131856270599780e+00} // k1, k2, p1, p2, k3
		//// TRY4. -0.284, -1.036,  2.887

		{1.7504191336594010e+03, 5.6346296440662695e+02, 1.7573994237148404e+03, 5.0469275762766267e+02},	// fx, cx, fy, cy
		{-4.7668681735136786e-01, -2.3464440473979639e-01,  -1.9568458929960599e-03,  9.8812449524960030e-03, 1.1651353204080261e+00} // k1, k2, p1, p2, k3
	}

	,{
		CAMINTRINSIC_25mm5MP,				// 7, 20191002
		{5.3502165406913509e+03,  7.3723410655455575e+02, 5.3458494240991404e+03, 5.8662813801318657e+02},	// fx, cx, fy, cy
		{1.6510130828976560e-01, -1.0988169264076193e+00,  4.4330416407570156e-03,  2.2000557444162343e-02, 3.3738487125471988e+02} // k1, k2, p1, p2, k3
	}

	,{
		CAMINTRINSIC_6mm5MP,				// 8, 20200318
		{1.2196154332945459e+03,    5.3470867754233484e+02, 1.2158845313676336e+03,5.3103167815549978e+02  },	// fx, cx, fy, cy
		{-5.4360044972147326e-01,  2.9488350623001830e-01,3.5857548382736268e-03, -7.4560300136911365e-04, -9.2260693957934986e-02} // k1, k2, p1, p2, k3
	}

	,{
		CAMINTRINSIC_12mm2,					// 9, 20201006, by hschoi
		{2.64365350e+03 ,5.33516737e+02,2.61214368e+03, 5.43191068e+02},	// fx, cx, fy, cy
		{-5.00709282e-01, 3.41118190e-02, 7.23143819e-04, 5.63558600e-03, -9.52956090e-01} // k1, k2, p1, p2, k3
	}
};



//I32 iana_cam_runparam_init(HAND hiana);
/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
I32	iana_cam_cconf_refine(iana_t *piana, U32 camid, U32 camconf);

/*!
 ********************************************************************************
 *	@brief      CAM start
 *
 *  @param[in]	h
 *              IANA module handle
 *  @return		Terminate result. 
 *
 *	@author	    yhsuk
 *  @date       2015/12/20
 *******************************************************************************/
I32 iana_cam_start(HAND hiana)
{
	I32 res;
	U32 runstate;
	iana_t *piana;
	U32 i, j;

	//--
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with hiana: %p\n",  hiana);
	
	
	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}
	piana = (iana_t *)hiana;

	//----
	// 1) set camera parameter
	// 2) scamif start
	// 1) Set camera configuration
	//icctype = piana->icctype;

	runstate = IANA_RUN_EMPTY;
	iana_cam_reconfig(piana, 0);
	// 2) scamif start
	i = 0;
	if (piana->opmode != IANA_OPMODE_FILE) {
		for (i = 0; i < XBDCHECKRETRY; i++) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" xbdchecktry: %d\n", i);
			if (piana->needxbdcheck == 0) {
				break;
			}

			cr_sleep(XBDCHECKSLEEP);
		}
		if (piana->needxbdcheck != 0) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" xbdchecktry  FAIL!!!! !!!\n");
			res = 0;
			goto func_exit;
		}

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" xbdchecktry  GOOD!!!  %d\n", i);

		if (i != 0) {
			//cr_sleep(500);
			//cr_sleep(500);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" xbdchecktry  After Sleep\n");
		}


	}


	res = scamif_check_host_cam_subnet(piana->hscamif);
	if (res == 0 ) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); 
		goto func_exit;
	}


	res = scamif_start(piana->hscamif);
	if (res == 0 /*res < 0*/) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); 
		res = scamif_start(piana->hscamif);
		if (res == 0 /*res < 0*/) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res);
			goto func_exit;
		}
	}



//#define TIMER_ALLMASTER  see iana.h
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" -=-=-=-=-=-= Before iana_cam_update() call.. :P\n", __LINE__); 
	if (piana->camsensor_category == CAMSENSOR_Z3 ||piana->camsensor_category == CAMSENSOR_EYEXO) {
		res = iana_cam_update_all(piana, piana->hscamif);
	} else {
		iana_cam_update(piana, piana->hscamif, 0);				// XXXXXXXXXXYYYYYYYYYZZZZZZZ
		iana_cam_update(piana, piana->hscamif, 1);				// XXXXXXXXXXYYYYYYYYYZZZZZZZ
	}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__" -=-=-=-=-=-= After  iana_cam_update() call.. :P\n", __LINE__); 

	scamif_camtimer_stop(piana->hscamif);

	res = scamif_camtimer_reset3(piana->hscamif);


	if (res == 0 ) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); 
		goto func_exit;
	}

	for(i = 0; i < NUM_CAM; i++) {
		for(j = 0; j<2; j++) {
			res = scamif_imagebuf_init(piana->hscamif, i, NORMALBULK_NORMAL+j, NULL);
			if (res == 0 ) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"line=%d(i=%d, j=%d) res: %d\n", __LINE__, i, j, res); 
				goto func_exit;
			}
		}
		
	}

	if (piana->camsensor_category == CAMSENSOR_Z3 || piana->camsensor_category == CAMSENSOR_EYEXO) {
		res = iana_cam_update_all(piana, piana->hscamif);
	} else {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__"-=-=-=-=-=-= Before iana_cam_update() call.. :P\n", __LINE__); 
		res = iana_cam_update(piana, piana->hscamif, 0);

		if (res == 0) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); 
			goto func_exit;
		}
		res = iana_cam_update(piana, piana->hscamif, 1);				
	}

	if (res == 0 ) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); 
		goto func_exit;
	}

	if (piana->camsensor_category == CAMSENSOR_Z3 || piana->camsensor_category == CAMSENSOR_EYEXO) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__" swspec, barcode update.n", res);
		scamif_security_update(piana->hscamif);
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res %d\n", res);
	return res;
}


/*!
 ********************************************************************************
 *	@brief      CAM re-start
 *
 *  @param[in]	h
 *              IANA module handle
 *  @return		1: Success, 0: Fail 
 *
 *	@author	    yhsuk
 *  @date       2016/02/19
 *******************************************************************************/
I32 iana_cam_restart(HAND hiana)
{
	I32 res;
	iana_t *piana;
	I32 i,j;

	//--
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with hiana: %p\n",  hiana);
	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}
	piana = (iana_t *)hiana;


	if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
		res = scamif_cam_start2(piana->hscamif, 0, NULL, NULL, NULL, 1);	// GEV_STREAMING_CHANNEL_MULTI	=	1,
		IANA_CAM_UpdateLedLut(piana, 1);
	} else {
		res = scamif_cam_start(piana->hscamif, 0, NULL, NULL, NULL);
	}
	if (res == 0 || res < 0) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); goto func_exit;}

	if (res) {


		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"before start 2\n");
		res = iana_cam_start(hiana);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"after start 2\n");
	}
//	if (res == 0 /*res < 0*/) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); goto func_exit;}


	for(i = 0; i<2; i++) {
		for(j = 0; j<2; j++) {
			scamif_imagebuf_init(piana->hscamif, i, NORMALBULK_NORMAL+j, NULL);
			//	if (res == 0 /*res < 0*/) {cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); goto func_exit;}
		}
	}


	if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
		scamif_security_update(piana->hscamif);
	}
func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res %d\n", res);
	return res;
}



/*!
 ********************************************************************************
 *	@brief      CAM STOP
 *
 *  @param[in]	h
 *              IANA module handle
 *  @return		1: Success, 0: Fail  
 *
 *	@author	    yhsuk
 *  @date       2015/12/20
 *******************************************************************************/
I32 iana_cam_stop(HAND hiana)
{
	I32 res;
	iana_t *piana;

	//--
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with hiana: %p\n",  hiana);
	
	if (hiana != NULL) {
		piana = (iana_t *)hiana;
		res = scamif_cam_stop(piana->hscamif);
	} else {
		res = 0;
	}

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res %d\n", res);
	return res;
}

/*!
 ********************************************************************************
 *	@brief      set image configure
 *
 *  @param[in]	h
 *              IANA module handle
 *  *param[in]  runstate
 *  @return		configuration type index
 *
 *	@author	    yhsuk
 *  @date       2015/12/20
 *******************************************************************************/
I32 iana_cam_cconf(HAND hiana, U32 camconf)
//, U32 sx0, U32 sy0, U32 sx1, U32 sy1)
{
	I32 res;
	iana_t *piana;
	iana_camconf_t *picf0;
	iana_camconf_t *picf1;

	//--
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with hiana: %p\n",  hiana);
	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}
	piana = (iana_t *)hiana;

	picf0 = &piana->pic[0]->icf;
	picf1 = &piana->pic[1]->icf;

//#define PUTTER_NROT
#if defined(PUTTER_NROT)
	if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
#define AUTOINIT		1			// 1: AutoInit in CAM,  0: NO.
		if (camconf == IANA_CAMCONF_RUN_PUTTER) {
			int i;
			for(i=0; i < NUM_CAM; i++) 
			{
				iana_cam_fmcinit_config(piana, i, 1, 0,  AUTOINIT);	// ZROT: 0...
			}
		} else {
			iana_setup_t *pisetup;
			pisetup = &piana->isetup;
			
			for(i=0; i < NUM_CAM; i++) 
			{
				iana_cam_fmcinit_config(piana, i, 1, pisetup->icsetup[0].zrtmode,  AUTOINIT);	// fmcinit: Do FMC Init or Not.    zrot: 1: ZROT mode,  0: NROT mode				// auto init
			}
		}
	}
#endif
	switch (camconf) {
		case IANA_CAMCONF_NULL:
		default:
			{
				memcpy(picf0, &icconf_null[0], sizeof(iana_camconf_t));
				memcpy(picf1, &icconf_null[1], sizeof(iana_camconf_t));
			}
			break;
		case IANA_CAMCONF_BASE:
			{
				memcpy(picf0, &icconf_base[0], sizeof(iana_camconf_t));
				memcpy(picf1, &icconf_base[1], sizeof(iana_camconf_t));
			}
			break;

		case IANA_CAMCONF_FULLRANGE:
			{
				if (piana->camidCheckReady == 0) {
					memcpy(picf0, &icconf_fullrange_CheckReady_0[0], sizeof(iana_camconf_t));
					memcpy(picf1, &icconf_fullrange_CheckReady_0[1], sizeof(iana_camconf_t));
				} else if (piana->camidCheckReady == 1) {
					memcpy(picf0, &icconf_fullrange_CheckReady_1[0], sizeof(iana_camconf_t));
					memcpy(picf1, &icconf_fullrange_CheckReady_1[1], sizeof(iana_camconf_t));
				} else {

				}
			}
			break;

		case IANA_CAMCONF_POWERSAVING:
			{
				//memcpy(picf0, &icconf_fullrange[0], sizeof(iana_camconf_t));
				//memcpy(picf1, &icconf_fullrange[1], sizeof(iana_camconf_t));

				if (piana->camidCheckReady == 0) {
					memcpy(picf0, &icconf_fullrange_CheckReady_0[0], sizeof(iana_camconf_t));
					memcpy(picf1, &icconf_fullrange_CheckReady_0[1], sizeof(iana_camconf_t));
				} else if (piana->camidCheckReady == 1) {
					memcpy(picf0, &icconf_fullrange_CheckReady_1[0], sizeof(iana_camconf_t));
					memcpy(picf1, &icconf_fullrange_CheckReady_1[1], sizeof(iana_camconf_t));
				} else {

				}

#if defined(POWERSAVING_FRAMERATE)
				picf0->framerate = POWERSAVING_FRAMERATE;
				picf1->framerate = POWERSAVING_FRAMERATE;
#endif

#if defined(POWERSAVING_EXPOSURE)
				picf0->exposure = POWERSAVING_EXPOSURE;
				picf1->exposure = POWERSAVING_EXPOSURE;
#endif

#if defined(POWERSAVING_GAIN)
				picf0->exposure = POWERSAVING_GAIN;
				picf1->exposure = POWERSAVING_GAIN;
#endif

			}
			break;
		case IANA_CAMCONF_RUN:			
			{
				memcpy(picf0, &icconf_run[0], sizeof(iana_camconf_t));
				memcpy(picf1, &icconf_run[1], sizeof(iana_camconf_t));

				// Change offset..
				picf0->offset_x = piana->pic[0]->offset_x;
				picf0->offset_y = piana->pic[0]->offset_y;
				picf1->offset_x = piana->pic[1]->offset_x;
				picf1->offset_y = piana->pic[1]->offset_y;


				{ 
					FILE *fp;
					fp = cr_fopen(RUNGAINFILE, "r");
					if (fp) {
						int mode;
						int exposure;
						mode = 0;
						fscanf(fp, "%d", &mode);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
						if (mode) {
							fscanf(fp, "%d", &exposure);
							picf0->exposure = (U32) exposure;
						}

						cr_fclose(fp);
					} 				
				}
			}

			piana->pic[0]->rungain0 = piana->pic[0]->rungain = picf0->gain;
			piana->pic[1]->rungain0 = piana->pic[1]->rungain = picf1->gain;
			break;
		case IANA_CAMCONF_RUN_PUTTER:			
			{
				memcpy(picf0, &icconf_run_putter[0], sizeof(iana_camconf_t));
				memcpy(picf1, &icconf_run_putter[1], sizeof(iana_camconf_t));

				// Change offset..
				picf0->offset_x = piana->pic[0]->offset_x;
				picf0->offset_y = piana->pic[0]->offset_y;
				picf1->offset_x = piana->pic[1]->offset_x;
				picf1->offset_y = piana->pic[1]->offset_y;

				piana->pic[0]->rungain0 = piana->pic[0]->rungain = picf0->gain;
				piana->pic[1]->rungain0 = piana->pic[1]->rungain = picf1->gain;
			}
			break;
			/*
		default:
			{
				res = 0;
				goto func_exit;
			}
			*/
	}

// -- Refine ..  20200806
	iana_cam_cconf_refine(piana, 0, camconf);		// cam0
	iana_cam_cconf_refine(piana, 1, camconf);		// cam1


	// Camera property..
	picf0->gain = (U32) (picf0->gain * piana->pic[0]->icp.gainmult);
	picf1->gain = (U32) (picf1->gain * piana->pic[1]->icp.gainmult);


	piana->pic[2]->icp.gainmult = 1;

	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": <%d> width: %d, height: %d\n", 0, picf0->width, picf0->height);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": <%d> width: %d, height: %d\n", 1, picf1->width, picf1->height);

	}

	res = iana_cam_property(hiana, 0, piana->hscamif, picf0);

	if (res == 1) {
		res = iana_cam_property_BULK(hiana, 0, piana->hscamif, picf0->width, picf0->height, picf0->offset_x, picf0->offset_y);
	}

	if (res == 1) {
		res = iana_cam_property(hiana, 1,piana->hscamif,  picf1);
		if (res == 1) {
			res = iana_cam_property_BULK(hiana, 1, piana->hscamif, picf1->width, picf1->height, picf1->offset_x, picf1->offset_y);
		}
	}


	if (res != 0) {
		piana->currentcamconf = camconf;
	} else {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"currentcamconf: %d -> %d, camconf: %d, res: %d\n",
			piana->currentcamconf, IANA_CAMCONF_CHANGEME, camconf, res);

		piana->currentcamconf = IANA_CAMCONF_CHANGEME;
	}
func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res %d\n", res);
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Refine Image config.
 *
 *  @param[in]	h
 *              IANA module handle
 *  @param[in]  camid
 *              camera id
 *  @param[in]  runstate
 *  @return		1: Success, 0: Fail.
 *
 *	@author	    yhsuk
 *  @date       2020/08/06
 *******************************************************************************/
I32	iana_cam_cconf_refine(iana_t *piana, U32 camid, U32 camconf)
{
	I32 res;
	iana_camconf_t *picf;

	//---
	if (camid >= NUM_CAM) {
		res = 0;
		goto func_exit;
	}
	picf = &piana->pic[camid]->icf;


#if 0    // Remove.. 20201002..   see updateGain().
	if (piana->camsensor_category == CAMSENSOR_EYEXO || piana->camsensor_category == CAMSENSOR_Z3 ) {
		if (camconf == IANA_CAMCONF_RUN) {
			double gainratio;
			double mean, stddev;
			double meanH, stddevH;

			double mean_reference;
			double mean_minratio;
			double mean_maxratio;
			double mean_gainratio_bonus;

			//--
			//		iana_sensor_setup(piana, CAMSENSOR_EYEXO);

			if (piana->camsensor_category == CAMSENSOR_EYEXO) {
				mean_reference = MEAN_REFERENCE_EYEXO;
				mean_minratio = MEAN_MINRATIO_EYEXO;
				mean_maxratio = MEAN_MAXRATIO_EYEXO;
				mean_gainratio_bonus = MEAN_GAINRATIO_BONUS_EYEXO;
			} else if (piana->camsensor_category == CAMSENSOR_Z3 ) {
				mean_reference = MEAN_REFERENCE_Z3;
				mean_minratio = MEAN_MINRATIO_Z3;
				mean_maxratio = MEAN_MAXRATIO_Z3;
				mean_gainratio_bonus = MEAN_GAINRATIO_BONUS_Z3;
			} else {
				mean_reference = -999;
				mean_minratio = -999;
				mean_maxratio = -999;
				mean_gainratio_bonus = -999;
			}

			if (mean_reference > 0) {
				mean = piana->pic[camid]->icp.mean;
				stddev = piana->pic[camid]->icp.stddev;
				meanH = piana->pic[camid]->icp.meanH;
				stddevH = piana->pic[camid]->icp.stddevH;

				if (mean < mean_reference / mean_maxratio) {
					gainratio = 1.0;		// NO-touch.. :P
				} else {
					gainratio = mean_reference / mean;

					gainratio = (gainratio - 1.0) * mean_gainratio_bonus + 1.0;

					if (gainratio < mean_minratio) {
						gainratio = mean_minratio;
					}
					if (gainratio > mean_maxratio) {
						gainratio = mean_maxratio;
					}

				}

				{
					FILE *fp;
#define GRAINRATIOFILE	"gainratio.txt"
					fp = cr_fopen(GRAINRATIOFILE, "r");
					if (fp) {
						int mode;
						mode = 0;
						fscanf(fp, "%d", &mode);
						if (mode) {
							gainratio = 1.0;
						}
						cr_fclose(fp);
					}
				}
/*
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%d] mean: %lf stddev: %lf meanH: %lf stddevH: %lf         gain: %d -> %d with ratio: %lf\n", 
						camid,
						mean, stddev,
						meanH, stddevH,
						picf->gain, (U32) (picf->gain * gainratio), gainratio);
*/
				picf->gain = (U32) (picf->gain * gainratio);
			}
			res = 1;
		} else {	// NOT YET.. :P
			res = 1;
		}
	} else {
		res = 1;
	}
#endif   // Remove.. 20201002..   see updateGain().

	if (piana->camsensor_category == CAMSENSOR_CBALL 
	//		|| piana->camsensor_category == CAMSENSOR_Z3 
			) {
		if (camconf == IANA_CAMCONF_RUN) {
			I32 mode;			
			I32 v0, v1;
			I32 vv;			
 
			FILE *fp;
			fp = cr_fopen(EXPVFILE, "r");
			if (fp) {

				//--						
				mode = 0;
				fscanf(fp, "%d", &mode);
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
				fscanf(fp, "%d", &v0);
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
				fscanf(fp, "%d", &v1);
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

				cr_fclose(fp); 

				if (mode) {
					if (camid == 0) {
						vv = v0;
						if (vv < EXPOSURE_RUN_CAM0_MIN) {
							vv = EXPOSURE_RUN_CAM0_MIN;
						}
						if (vv > EXPOSURE_RUN_CAM0_MAX) {
							vv = EXPOSURE_RUN_CAM0_MAX;
						}
					} else {
						vv = v1;
						if (vv < EXPOSURE_RUN_CAM1_MIN) {
							vv = EXPOSURE_RUN_CAM1_MIN;
						}
						if (vv > EXPOSURE_RUN_CAM1_MAX) {
							vv = EXPOSURE_RUN_CAM1_MAX;
						}
					}

					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%d] exposure: %d -> %d\n", 
							camid, picf->exposure, vv);
					picf->exposure = vv;
				}

			}
							

			res = 1;
		} else {	// NOT YET.. :P
			res = 1;
		}
	} else {
		res = 1;
	}
#if defined(ROUGHBUNKER_COMPENSATION)
	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		if (camconf == IANA_CAMCONF_RUN) {
			iana_cam_t	*pic;

			//--
			pic = piana->pic[camid];
			if (iana_isInBunker(piana, &pic->icp.b3d)) {
				picf->gain = (U32)(picf->gain * P3V2_BUNKER_GAINRATIO);
			}
		}
	}
#endif


	if (piana->camsensor_category == CAMSENSOR_EYEXO 
		|| piana->camsensor_category == CAMSENSOR_Z3
		|| piana->camsensor_category == CAMSENSOR_CBALL
		) {
		if (camconf == IANA_CAMCONF_RUN || camconf == IANA_CAMCONF_RUN) {

		}
	}


func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      camera property 
 *
 *  @param[in]	hiana
 *              IANA module handle
 *  @param[in]  camid
 *              camera id. pscamif->mastercamid: Master, pscamif->mastercamid: Slave   //  0: Master, 1: Slave
 *  @param[in]  pccc
 *              cam config cam.
 *  @return		1: Success, 0: Fail.
 *
 *	@author	    yhsuk
 *  @date       2016/02/18
 *******************************************************************************/
I32 iana_cam_property(HAND hiana, U32 camid, HAND hscamif, iana_camconf_t *pccc)
{
	I32 res;
	iana_t *piana;

	//--
	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}
	piana = (iana_t *)hiana;

	res = scamif_image_property2(hscamif, camid,
			pccc->width, pccc->height,
			pccc->offset_x, pccc->offset_y,
			pccc->framerate,
			pccc->gain,
			pccc->exposure,
			pccc->skip,
			pccc->multitude,
			pccc->syncdiv,
			pccc->exposure_post,
			pccc->exposure_expiate			
			);


func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      camera property for LUT 
 *
 *  @param[in]	hiana
 *              IANA module handle
 *  @param[in]  camid
 *              camera id. pscamif->mastercamid: Master, pscamif->mastercamid: Slave   //  0: Master, 1: Slave
 *  @param[in]  pccc
 *              cam config cam.
 *  @return		1: Success, 0: Fail.
 *
 *	@author	    yhsuk
 *  @date       2019/0307
 *******************************************************************************/
I32 iana_cam_property_LUT(HAND hiana, U32 camid, HAND hscamif, iana_camconf_t *pccc)
{
	I32 res;
	iana_t *piana;

	//--
	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}
	piana = (iana_t *)hiana;

	res = scamif_image_property_LUT(hscamif, camid,
		pccc->lut_bright,
		pccc->lut_contrast,
		pccc->lut_gamma);

func_exit:
	return res;
}
/*!
 ********************************************************************************
 *	@brief      camera property for LEDbright 
 *
 *  @param[in]	hiana
 *              IANA module handle
 *  @param[in]  camid
 *              camera id. pscamif->mastercamid: Master, pscamif->mastercamid: Slave   //  0: Master, 1: Slave
 *  @param[in]  pccc
 *              cam config cam.
 *  @return		1: Success, 0: Fail.
 *
 *	@author	    yhsuk
 *  @date       2019/0307
 *******************************************************************************/
I32 iana_cam_property_LEDbright(HAND hiana, U32 camid, HAND hscamif, iana_camconf_t *pccc)
{
	I32 res;
	iana_t *piana;

	//--
	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}
	piana = (iana_t *)hiana;

	res = scamif_image_property_LEDbright(hscamif, camid,
		pccc->ledbrgt);

func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      camera property for BULK...
 *
 *  @param[in]	hiana
 *              IANA module handle
 *  @param[in]  camid
 *              camera id. pscamif->mastercamid: Master, pscamif->mastercamid: Slave   //  0: Master, 1: Slave
 *  @param[in]  pccc
 *              cam config cam.
 *  @return		1: Success, 0: Fail.
 *
 *	@author	    yhsuk
 *  @date       2016/02/18
 *******************************************************************************/
I32 iana_cam_property_BULK(HAND hiana, U32 camid, HAND hscamif, U32 width, U32 height, U32 offset_x, U32 offset_y)
{
	I32 res;
	iana_t *piana;

	//--
	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}
	piana = (iana_t *)hiana;

	res = scamif_image_property_BULK(hscamif, camid,
			width, height, offset_x, offset_y);

func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      update camera configure
 *
 *  @param[in]	h
 *              IANA module handle
 *  @return		1: Success, 0: Fail.
 *
 *	@author	    yhsuk
 *  @date       2016/02/18
 *******************************************************************************/
I32 iana_cam_update(HAND hiana, HAND hscamif, U32 camid)
{
	I32 res;
	iana_t *piana;
	U32 i;

	//--
	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}
	piana = (iana_t *)hiana;


	//scamif_cam_pause(piana->hscamif, 0);
	//scamif_cam_pause(piana->hscamif, 1);
	res = 0;

	for (i = 0; i < SCAMIF_RETRY; i++) {
		if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
			res = scamif_image_update2(hscamif, camid, 1);
		} else {
			res = scamif_image_update(hscamif, camid, 1);
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" 0:%d : %d\n",i, res);
		}
		if (res == 1) 
			break;
	}


	//scamif_cam_resume(piana->hscamif, 1);
	//scamif_cam_resume(piana->hscamif, 0);

func_exit:
	return res;
}



/*!
 ********************************************************************************
 *	@brief      update both camera configure
 *
 *  @param[in]	h
 *              IANA module handle
 *  @return		1: Success, 0: Fail.
 *
 *	@author	    yhsuk
 *  @date       2019/0412 
 *******************************************************************************/
I32 iana_cam_update_all(HAND hiana, HAND hscamif)
{
	I32 res;
	iana_t *piana;
	U32 i;

	//--

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with hiana: %p\n",  hiana);

	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}
	piana = (iana_t *)hiana;


	//scamif_cam_pause(piana->hscamif, 0);
	//scamif_cam_pause(piana->hscamif, 1);
	res = 0;

	if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
		for (i = 0; i < SCAMIF_RETRY; i++) {
			res = scamif_image_update_all(hscamif, 1);
			if (res == 1) {
				break;
			}
		}
	} else {
		for (i = 0; i < SCAMIF_RETRY; i++) {
			res = scamif_image_update(hscamif, 0, 1);
			if (res == 1) {
				break;
			}
		}

		for (i = 0; i < SCAMIF_RETRY; i++) {
			res = scamif_image_update(hscamif, 1, 1);
			if (res == 1) {
				break;
			}
		}
	}
func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"--\n");

	return res;
}


//ATTACKCAM
/*!
 ********************************************************************************
 *	@brief      Config fmc init and zrot mode
 *
 *  @param[in]	h
 *              IANA module handle
 *  @param[in]	camid
 *              camera id. pscamif->mastercamid: Master, pscamif->mastercamid: Slave   //  0: Master, 1: Slave
 *  @param[in]	fmcinit
 *              1: Do FMC Init   0: Not
 *  @param[in]	zrot
 *              1: ZROT mode,  0: NROT mode
 *  @return		1: Success, 0: Fail.
 *
 *	@author	    yhsuk
 *  @date       2016/11/02
 *******************************************************************************/
I32 iana_cam_fmcinit_config(HAND hiana, U32 camid, U32 fmcinit, U32 zrot, U32 autoinit)				// fmcinit: Do FMC Init or Not.    zrot: 1: ZROT mode,  0: NROT mode
{
	I32 res;
	iana_t *piana;

	//--
	if (hiana == NULL) {
		res = 0;
	} else {
		piana = (iana_t *)hiana;
		if (camid < NUM_CAM ) {
			if (piana->hscamif) {
				res = scamif_cam_fmcinit_config(piana->hscamif, camid, fmcinit, zrot, autoinit);
			} else {
				res = 0;
			}
		}  else {
			res = 0;
		}
	}
	return res;
}





/*!
 ********************************************************************************
 *	@brief      get All camera interinsic parameters
 *
 *  @param[in]	h
 *              IANA module handle
 *  @param[in]	paramid
 *              camera parameter id
 *  @param[out]	pcip
 *              camera intrinsic parameter
 *  @return		1: Good.  0: bad param id
 *
 *	@author	    yhsuk
 *  @date       2017/08/04
 *******************************************************************************/
I32 iana_cam_getCamInterinsicParamAll(HAND hiana) 
{
	I32 res;
	U32 camid;
	U32 rl;
	iana_t *piana;
	iana_cam_param_t *picp;
	iana_setup_t *pisetup;
	iana_cam_setup_t *picsetup;

	iana_cam_t	*pic;



	//----
	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}
	piana = (iana_t *)hiana;
	pisetup = &piana->isetup;

	for (rl = 0; rl < 2; rl++) {
		for (camid = 0; camid < NUM_CAM; camid++) {
			I32 CamparamRes;
			U32 camintrinsicparamid;
			if (rl == 0) {
				pic = &piana->RS_ic[camid];
			} else if (rl == 1) {
				pic = &piana->LS_ic[camid];
			} else {
				continue;		// what?
			}

			picp = &pic->icp;

			picsetup = &pisetup->icsetup[camid];
			camintrinsicparamid = picsetup->camintrinsicid;

			CamparamRes = iana_cam_getCamInterinsicParam(piana, camintrinsicparamid, &picp->cameraintrinsic);
			if (CamparamRes) {
				picp->bCamparamValid = 1;
			}
		}
	}
	res = 1;

func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      get camera interinsic parameters
 *
 *  @param[in]	h
 *              IANA module handle
 *  @param[in]	paramid
 *              camera parameter id
 *  @param[out]	pcip
 *              camera intrinsic parameter
 *  @return		1: Good.  0: bad param id
 *
 *	@author	    yhsuk
 *  @date       2017/08/04
 *******************************************************************************/
I32 iana_cam_getCamInterinsicParam(HAND hiana, U32 paramid, cameraintrinsic_t *pcip)	
{
	I32 res;
	//--
	UNUSED(hiana);
	if (paramid <= CAMINTRINSIC_LAST) {
		memcpy(pcip, &s_cip[paramid], sizeof(cameraintrinsic_t));
		res = 1;
	} else {
		res = 0;
	}
	return res;
}


/*!
 ********************************************************************************
 *	@brief      update LUT value to sensor CAM
 *
 *  @param[in]	hiana
 *              IANA module handle
 *  @param[in]	forceupdate
 *              if 1, force to update the value
 *  @return		1: Good.  0: bad 
 *
 *	@author	    yhsuk
 *  @date       2017/08/10
 *******************************************************************************/
I32 IANA_CAM_UpdateLedLut(HAND hiana, int forceupdate)
{
	I32 ledbrgt[MAXCAMCOUNT];
	I32 bright[MAXCAMCOUNT], contrast[MAXCAMCOUNT], gamma[MAXCAMCOUNT];
	U32 camid_;
	FILE *fp;
	U32 mode;
	U32 updateme;
	iana_t *piana;

	piana = (iana_t *)hiana;

	for (camid_ = 0; camid_ < NUM_CAM; camid_++) {
		//ledbrgt = 130;			// 11.7V,  2A per line.
		//bright = 100; contrast = 100; gamma = 100;
		ledbrgt[camid_]	= piana->pic[camid_]->icp.m_ledbrgt;
		bright[camid_]	= piana->pic[camid_]->icp.m_bright;
		contrast[camid_]= piana->pic[camid_]->icp.m_contrast;
		gamma[camid_]	= piana->pic[camid_]->icp.m_gamma;
	}

	updateme = 0;
	if (forceupdate) {
		updateme = 1;
	}

	fp = cr_fopen("lutled.txt", "r");
	if (fp) {
		int ret;
		int itmp;
		fscanf(fp, "%d", &mode);
		fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
		if (mode == 1) {
			//--
			// bright
			ret = fscanf(fp, "%d", &itmp);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
			if (ret == 1 && itmp >= 0) {
				bright[0] =bright[1] = (U32)itmp;	
			}
			// contrast
			ret = fscanf(fp, "%d", &itmp);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
			if (ret == 1 && itmp >= 0) {
				contrast[0] = contrast[1] = (U32)itmp;	
			}
			// gamma
			ret = fscanf(fp, "%d", &itmp);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
			if (ret == 1 && itmp >= 0) {
				gamma[0] = gamma[1] = (U32)itmp;	
			}
			// LED Bright
			ret = fscanf(fp, "%d", &itmp);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
			if (ret == 1 && itmp >= 0) {
				ledbrgt[0] = ledbrgt[1] = (U32)itmp;	
			}
		}
		cr_fclose(fp);
	}

	if (updateme == 0) {
		for (camid_ = 0; camid_ < NUM_CAM; camid_++) {
			if (ledbrgt[camid_]	!= piana->pic[camid_]->icp.m_ledbrgt)	{updateme = 1; break;}
			if (bright[camid_]	!= piana->pic[camid_]->icp.m_bright)	{updateme = 1; break;}
			if (contrast[camid_]!= piana->pic[camid_]->icp.m_contrast)	{updateme = 1; break;}
			if (gamma[camid_]	!= piana->pic[camid_]->icp.m_gamma)		{updateme = 1; break;}
		}
	}

	if (updateme) {
		for (camid_ = 0; camid_ < NUM_CAM; camid_++) {
			scamif_led_brightness(piana->hscamif, camid_, ledbrgt[camid_]);
			scamif_image_LUT(piana->hscamif, camid_,bright[camid_], contrast[camid_], gamma[camid_]);

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] ledbrgt: %d bright: %d contrast: %d gamma: %d\n",
				camid_,
				ledbrgt[camid_],
				bright[camid_], contrast[camid_], gamma[camid_]
			);
		}
		for (camid_ = 0; camid_ < NUM_CAM; camid_++) {
			//ledbrgt = 130;			// 11.7V,  2A per line.
			//bright = 100; contrast = 100; gamma = 100;
			piana->pic[camid_]->icp.m_ledbrgt	= ledbrgt[camid_];
			piana->pic[camid_]->icp.m_bright	= bright[camid_];
			piana->pic[camid_]->icp.m_contrast	= contrast[camid_];
			piana->pic[camid_]->icp.m_gamma		= gamma[camid_];
		}
	}
	return 1;
}


/*!
 ********************************************************************************
 *	@brief      update gain value to sensor CAM
 *
 *  @param[in]	hiana
 *              IANA module handle
 *  @return		1: Good.  0: bad 
 *
 *	@author	    yhsuk
 *  @date       2017/08/10
 *******************************************************************************/
I32 IANA_CAM_UpdateGain(HAND hiana)	
{
	I32 res;
	double targetmean[MAXCAMCOUNT];
	iana_t *piana;

	piana = (iana_t *)hiana;

	//--
	if (piana->processarea == IANA_AREA_PUTTER) {
		res = 1;
		goto func_exit;
	}

#define TARGETMEAN_CAM0	200.0
#define TARGETMEAN_CAM1	140.0

#define TARGETMEAN_CAM0_P3V2	220
#define TARGETMEAN_CAM1_P3V2	170

	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		targetmean[0] = TARGETMEAN_CAM0_P3V2;
		targetmean[1] = TARGETMEAN_CAM1_P3V2;
	}
	else {
		targetmean[0] = TARGETMEAN_CAM0;
		targetmean[1] = TARGETMEAN_CAM1;
	}

#define UPDATEGAIN_MAXCOUNT	2
	if (piana->updategainCount < UPDATEGAIN_MAXCOUNT) {
		U32 currenttick;
		U32 timediff;
		U32 camid;
		U32 needupdate[MAXCAMCOUNT];
		I32 currentgain, newgain[MAXCAMCOUNT];
#define UPDATEGAIN_TIMEDIFF	100
		currenttick = cr_gettickcount();
		timediff = currenttick - piana->updategainTick;
		if (timediff > UPDATEGAIN_TIMEDIFF) {
			for (camid = 0; camid < NUM_CAM; camid++) {
				iana_camconf_t *picf;
				double currentmean;
				I32 gaindiff;
				double gainratio;

				needupdate[camid] = 0;
				//---
				picf = &piana->pic[camid]->icf;
				currentgain = picf->gain;

				currentmean = piana->pic[camid]->icp.mean;
				gainratio = currentmean  / targetmean[camid];
#define UPDATEGAIN_MIN	(0.5)	
#define UPDATEGAIN_MAX	(2.0)

				if (gainratio > UPDATEGAIN_MIN &&  gainratio < UPDATEGAIN_MAX) {
					newgain[camid] = (U32)(currentgain / gainratio);
					if (newgain[camid] > currentgain) {
						gaindiff = (I32)(newgain[camid] - currentgain);
					} else {
						gaindiff = (I32)(currentgain - newgain[camid]);
					}
					needupdate[camid] = 1;
				}
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] needupdate[]: %d <%d:%d> currentmean: %lf targetmean: %lf currentgain: %d newgain: %d, gainratio: %lf\n",
					camid, needupdate[camid], piana->updategainCount, currenttick,
					currentmean, targetmean[camid], currentgain, newgain[camid], gainratio);
			}

			if (needupdate[0] && needupdate[1]) {
				for (camid = 0; camid < NUM_CAM; camid++) {
					iana_camconf_t *picf;

					//--
					picf = &piana->pic[camid]->icf;
					scamif_image_property_gain(piana->hscamif, camid, newgain[camid]);	
					scamif_image_update_gain(piana->hscamif, camid);
					picf->gain = newgain[camid];

					piana->pic[camid]->rungain = picf->gain;
				}
				piana->updategainCount++;
				piana->updategainTick = currenttick;
			}
		}
	}

	res = 1;

func_exit:
	return 1;
}

 


/*!
 ********************************************************************************
 *	@brief      setup sensor parameter
 *
 *  @param[in]	hiana
 *              IANA module handle
 *  @param[out]	camsensorcategory
 *              Camera sensor category
 *  @return		1: Good.  0: bad 
 *
 *	@author	    yhsuk
 *  @date       2017/08/10
 *******************************************************************************/
I32 iana_sensor_setup(HAND hiana, U32 camsensorcategory)
{
	I32 res;
	iana_t *piana;
	iana_setup_t *pisetup;
	iana_cam_setup_t *picsetup;


	//--
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with hiana: %p, camsensorcaetgory: %d\n", hiana, camsensorcategory);
	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}
	piana = (iana_t *)hiana;
	if (camsensorcategory > CAMSENSOR_LAST) {
		res = 0;
		goto func_exit;
	}

	{	// Set default value for exposure post, expiate.   2019-11/29
		U32 i;

		pisetup = &piana->isetup;
		for (i = 0; i < NUM_CAM; i++) {
			picsetup = &pisetup->icsetup[i];			// 0: Center, 1: Side
			picsetup->exposure_post = EXPOSURE_POST_DEFAULT;
			picsetup->exposure_expiate = EXPOSURE_EXPIATE_DEFAULT;
		}
	}
	switch(camsensorcategory) {
		//-----------------------------------------------------------------------
		case CAMSENSOR_COMMON:
		case CAMSENSOR_NULL:
		//-----------------------------------------------------------------------
		{
			pisetup->camsensorcategory = camsensorcategory;

			//---- Center Camera
			picsetup = &pisetup->icsetup[0];

			picsetup->masterslave			= 0;									// 0: MASTER, 1: SLAVE
			picsetup->camintrinsicid		= CAMINTRINSIC_12mm0;
			picsetup->zrtmode 				= 1;									//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLMARK;			

			picsetup->run_width 			= 480;			
			picsetup->run_height 			= 240;			

			picsetup->run_processing_width 	= picsetup->run_width;
			picsetup->run_processing_height = picsetup->run_height;			

			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 40);
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2);

			picsetup->run_framerate 		= 2500;
			picsetup->run_basegain 			= 15*256;

			picsetup->run_exposure 			= 30;
			picsetup->run_skip 				= 2;
			picsetup->run_multitude 		= 4;
			picsetup->run_syncdiv 			= 1;

			picsetup->fullrange_exposure	= 40;
			picsetup->fullrange_basegain 	= 15*256;

			picsetup->run_width_putter 		= 480;
			picsetup->run_height_putter 	= 240;

			picsetup->offsetoffset_x_putter = -420;
			picsetup->offsetoffset_y_putter = -120;

			picsetup->run_framerate_putter 	= 100;
			picsetup->run_basegain_putter 	= 15*256;
			picsetup->run_exposure_putter 	= 40;
			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			picsetup->bulk_width 			= 96;
			picsetup->bulk_height 			= 96;

			picsetup->bulk_preshot_count 	= 6;
			picsetup->mult_for_bulk 		= 8;

			picsetup->bulk_preshot_additional_time = (-((SCAM_TSSCALE / picsetup->run_framerate) * picsetup->run_syncdiv * 10));
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);


			//---- Side Camera
			picsetup = &pisetup->icsetup[1];

			picsetup->masterslave			= 1;									// 0: MASTER, 1: SLAVE
			picsetup->camintrinsicid		= CAMINTRINSIC_12mm0;
			picsetup->zrtmode 				= 1;								//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLMARK;			

			picsetup->run_width 			= 480;								// == cam0.runwidth
			picsetup->run_height 			= 240;								// == cam0.run_height

			picsetup->run_processing_width 	= picsetup->run_width;
			picsetup->run_processing_height = picsetup->run_height;			

			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 40);		// == cam0.o_x
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2);		// == cam0.o_y

			picsetup->run_framerate 		= 2500;								// == cam0.run_framerate
			picsetup->run_basegain 			= 15*256;							// == cam0.run_basegain

			picsetup->run_exposure 			= 30;								// == cam0.run_exposure
			picsetup->run_skip 				= 2;								// == cam0.run_skip
			picsetup->run_multitude 		= 4;								// == cam0.run_multitude
			picsetup->run_syncdiv 			= 1;

			picsetup->fullrange_exposure	= 40;
			picsetup->fullrange_basegain 	= 15*256;

			picsetup->run_width_putter 		= 480;
			picsetup->run_height_putter 	= 240;

			picsetup->offsetoffset_x_putter = -420;
			picsetup->offsetoffset_y_putter = -120;

			picsetup->run_framerate_putter 	= 100;
			picsetup->run_basegain_putter 	= 15*256;
			picsetup->run_exposure_putter 	= 40;
			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			picsetup->bulk_width 			= 96;
			picsetup->bulk_height 			= 96;

			picsetup->bulk_preshot_count 	= 6;
			picsetup->mult_for_bulk 		= 8;

			picsetup->bulk_preshot_additional_time = 0;
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);


			//--
			res = 1;
			break;
		}

		//-----------------------------------------------------------------------
		case CAMSENSOR_VISION2:
		case CAMSENSOR_ZCAM:
		//-----------------------------------------------------------------------
		{
			pisetup->camsensorcategory = camsensorcategory;

			//---- Center Camera
			picsetup = &pisetup->icsetup[0];

			picsetup->masterslave			= 0;									// 0: MASTER, 1: SLAVE
			picsetup->camintrinsicid		= CAMINTRINSIC_12mm0;
			picsetup->zrtmode 				= 1;									//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLMARK;			

			picsetup->run_width 			= 480;			
			picsetup->run_height 			= 240;			

			picsetup->run_processing_width 	= picsetup->run_width;
			picsetup->run_processing_height = picsetup->run_height;			

			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 40);
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2);

			picsetup->run_framerate 		= 2500;
			picsetup->run_basegain 			= 15*256;

			picsetup->run_exposure 			= 30;
			picsetup->run_skip 				= 2;
			picsetup->run_multitude 		= 4;
			picsetup->run_syncdiv 			= 1;

			picsetup->fullrange_exposure	= 40;
			picsetup->fullrange_basegain 	= 15*256;

			picsetup->run_width_putter 		= 480;
			picsetup->run_height_putter 	= 240;

			picsetup->offsetoffset_x_putter = -420;
			picsetup->offsetoffset_y_putter = -120;

			picsetup->run_framerate_putter 	= 100;
			picsetup->run_basegain_putter 	= 15*256;
			picsetup->run_exposure_putter 	= 40;
			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			picsetup->bulk_width 			= 96;
			picsetup->bulk_height 			= 96;

			picsetup->bulk_preshot_count 	= 6;
			picsetup->mult_for_bulk 		= 8;

			picsetup->bulk_preshot_additional_time = (-((SCAM_TSSCALE / picsetup->run_framerate) * picsetup->run_syncdiv * 10));
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);


			//---- Side Camera
			picsetup = &pisetup->icsetup[1];

			picsetup->masterslave			= 1;									// 0: MASTER, 1: SLAVE
			picsetup->camintrinsicid		= CAMINTRINSIC_12mm0;
			picsetup->zrtmode 				= 1;								//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLMARK;			

			picsetup->run_width 			= 480;								// == cam0.runwidth
			picsetup->run_height 			= 240;								// == cam0.run_height

			picsetup->run_processing_width 	= picsetup->run_width;
			picsetup->run_processing_height = picsetup->run_height;			

			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 40);		// == cam0.o_x
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2);		// == cam0.o_y

			picsetup->run_framerate 		= 2500;								// == cam0.run_framerate
			picsetup->run_basegain 			= 15*256;							// == cam0.run_basegain

			picsetup->run_exposure 			= 30;								// == cam0.run_exposure
			picsetup->run_skip 				= 2;								// == cam0.run_skip
			picsetup->run_multitude 		= 4;								// == cam0.run_multitude
			picsetup->run_syncdiv 			= 1;

			picsetup->fullrange_exposure	= 40;
			picsetup->fullrange_basegain 	= 15*256;

			picsetup->run_width_putter 		= 480;
			picsetup->run_height_putter 	= 240;

			picsetup->offsetoffset_x_putter = -420;
			picsetup->offsetoffset_y_putter = -120;

			picsetup->run_framerate_putter 	= 100;
			picsetup->run_basegain_putter 	= 15*256;
			picsetup->run_exposure_putter 	= 40;
			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			picsetup->bulk_width 			= 96;
			picsetup->bulk_height 			= 96;

			picsetup->bulk_preshot_count 	= 6;
			picsetup->mult_for_bulk 		= 8;

			picsetup->bulk_preshot_additional_time = 0;
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);

			//--
			res = 1;
			break;
		}

		case CAMSENSOR_VISION2CB:
		case CAMSENSOR_CBALL:
		{
			pisetup->camsensorcategory = camsensorcategory;

			//---- Center Camera
			picsetup = &pisetup->icsetup[0];

			picsetup->masterslave			= 1;						// 0: MASTER, 1: SLAVE
			picsetup->camintrinsicid		= CAMINTRINSIC_8mm0;
			picsetup->zrtmode 				= 0;									//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLCLUB;			
#if 1
			//picsetup->run_width 			= 720;			
			//picsetup->run_height 			= 310;			

			//picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 300);  // for 720x310
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2);

			picsetup->run_width 			= 640;			
			picsetup->run_height 			= 310;			

			//picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 200);  // for 640x310
			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 240);  // for 640x310
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2);
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 +50);
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 +30);
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 +20);
			
			picsetup->run_processing_width 	= 480;
			picsetup->run_processing_height = 240;			

#endif
#if 0
			picsetup->run_width 			= 400;								// == cam0.runwidth
			picsetup->run_height 			= 200;								// == cam0.run_height

			picsetup->run_processing_width 	= picsetup->run_width;
			picsetup->run_processing_height = picsetup->run_height;			

			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 40);		// == cam0.o_x
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2);		// == cam0.o_y

#endif

			picsetup->run_framerate 		= 2100;				// real framerate is 700
			picsetup->run_basegain 			= 2000;
			picsetup->run_exposure 			= 70;

			picsetup->run_basegain 			= 2500;				// 20171021
			picsetup->run_exposure 			= 90;				// 20171021

			picsetup->run_basegain 			= 3000;				// 20180408
			picsetup->run_exposure 			= 80;				// 20180408




			picsetup->run_skip 				= 1;
			picsetup->run_multitude 		= 4;
			picsetup->run_syncdiv 			= 3;

			picsetup->fullrange_exposure	= 70;
			picsetup->fullrange_basegain 	= 2000;

			picsetup->run_width_putter 		= 480;
			picsetup->run_height_putter 	= 240;

//			picsetup->offsetoffset_x_putter = -380;
//			picsetup->offsetoffset_y_putter = -120;

			picsetup->offsetoffset_x_putter = (-(I32)picsetup->run_width_putter + 100);  // for 640x310
			picsetup->offsetoffset_y_putter = (-(I32)picsetup->run_height_putter/2);



			//picsetup->run_framerate_putter 	= 100;
			//picsetup->run_basegain_putter 	= 15*256;
			//picsetup->run_exposure_putter 	= 30;

			//picsetup->run_framerate_putter 	= 100;
			//picsetup->run_framerate_putter 	= 300;
			picsetup->run_framerate_putter 	= 200;
//#define PUTTER_EXPOSURE_CBALL	50
//#define PUTTER_EXPOSURE_CBALL	100
#define PUTTER_EXPOSURE_CBALL	200
//			picsetup->run_basegain_putter 	= 800;
			picsetup->run_basegain_putter 	= 1600;

			picsetup->run_exposure_putter 	= PUTTER_EXPOSURE_CBALL;

			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			// 720 * 310 * 40(frame) = 8.52 MByte.   Transfer in 80 msec : 852 Mbps.. -_-;
			picsetup->bulk_width 			= picsetup->run_width;
			picsetup->bulk_height 			= picsetup->run_height;

			picsetup->bulk_preshot_count 	= 8;
			picsetup->mult_for_bulk 		= 1;


			//------------------------------------------------------
#if 0
			picsetup->bulk_width 			= 96;
			picsetup->bulk_height 			= 96;

			picsetup->bulk_preshot_count 	= 8;
			picsetup->mult_for_bulk 		= 8;
#endif
			//------------------------------------------------------
			//picsetup->bulk_preshot_additional_time = (-((SCAM_TSSCALE / picsetup->run_framerate) * picsetup->run_syncdiv * 10));
			picsetup->bulk_preshot_additional_time = (-((SCAM_TSSCALE / picsetup->run_framerate) * picsetup->run_syncdiv * 5));
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);

			//---- Side Camera
			picsetup = &pisetup->icsetup[1];

			picsetup->masterslave			= 0;									// 0: MASTER, 1: SLAVE
			picsetup->camintrinsicid		= CAMINTRINSIC_12mm0;
			picsetup->zrtmode 				= 1;								//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLMARK;			

			picsetup->run_width 			= 400;								// == cam0.runwidth
			picsetup->run_height 			= 200;								// == cam0.run_height

			picsetup->run_processing_width 	= picsetup->run_width;
			picsetup->run_processing_height = picsetup->run_height;			

			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 40);		// == cam0.o_x
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2);		// == cam0.o_y

			picsetup->run_framerate 		= 2100;								// == cam0.run_framerate
			picsetup->run_basegain 			= 7000;							// == cam0.run_basegain

			picsetup->run_exposure 			= 30;								// == cam0.run_exposure

			picsetup->run_exposure 			= 40;								// TEST.. 20180408


			picsetup->run_skip 				= 1;								// == cam0.run_skip
			picsetup->run_multitude 		= 4;								// == cam0.run_multitude
			picsetup->run_syncdiv 			= 1;

			picsetup->fullrange_exposure	= 70;
			picsetup->fullrange_basegain 	= 2000;

			picsetup->run_width_putter 	= 480;
			picsetup->run_height_putter 	= 240;

//			picsetup->offsetoffset_x_putter = -380;
//			picsetup->offsetoffset_y_putter = -120;

			picsetup->offsetoffset_x_putter = (-(I32)picsetup->run_width_putter + 100);  // for 640x310
			picsetup->offsetoffset_y_putter = (-(I32)picsetup->run_height_putter/2);

			//picsetup->run_framerate_putter 	= 100;
			//picsetup->run_basegain_putter 	= 15*256;
			//picsetup->run_exposure_putter 	= 30;
			///picsetup->run_framerate_putter 	= 100;
			//picsetup->run_framerate_putter 	= 300;
			picsetup->run_framerate_putter 	= 200;


			//picsetup->run_basegain_putter 	= 1000;
			//picsetup->run_exposure_putter 	= 100;

			/////picsetup->run_basegain_putter 	= 2000;
			picsetup->run_basegain_putter 	= 3000;
			picsetup->run_exposure_putter 	= PUTTER_EXPOSURE_CBALL;


			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			picsetup->bulk_width 			= 96;
			picsetup->bulk_height 			= 96;

			picsetup->bulk_preshot_count 	= 8;
			picsetup->mult_for_bulk 		= 8;

			picsetup->bulk_preshot_additional_time = 0;
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);


			//--
			res = 1;
			break;
		}

		case CAMSENSOR_CBALL2:
		{
			pisetup->camsensorcategory = camsensorcategory;

			//---- Center Camera
			picsetup = &pisetup->icsetup[0];

			picsetup->masterslave			= 1;						// 0: MASTER, 1: SLAVE
			picsetup->camintrinsicid		= CAMINTRINSIC_8mm0;
			picsetup->zrtmode 				= 0;									//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLCLUB;			

			picsetup->run_width 			= 640;			
			picsetup->run_height 			= 480;

			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 200);  // for 640x480
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 50);
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 100);
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2);
			
			//picsetup->run_processing_width 	= 480;
			//picsetup->run_processing_height = 240;			

			picsetup->run_processing_width 	= 480;
			//picsetup->run_processing_height = 320;			
			picsetup->run_processing_height = 240;			

			picsetup->run_framerate 		= 2100;				// real framerate is 2100 / 4 = 525
			picsetup->run_basegain 			= 2000;
			picsetup->run_exposure 			= 120;

			picsetup->run_skip 				= 1;
			picsetup->run_multitude 		= 4;
			picsetup->run_syncdiv 			= 4;				// :)

			picsetup->fullrange_exposure	= 70;
			picsetup->fullrange_basegain 	= 2000;

			picsetup->run_width_putter 		= 480;
			picsetup->run_height_putter 	= 240;

			picsetup->offsetoffset_x_putter = -420;
			picsetup->offsetoffset_y_putter = -120;

			picsetup->run_framerate_putter 	= 100;
			picsetup->run_basegain_putter 	= 800;
			picsetup->run_exposure_putter 	= 100;
			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			picsetup->bulk_width 			= picsetup->run_width;
			picsetup->bulk_height 			= picsetup->run_height;

			picsetup->bulk_preshot_count 	= 8;
			picsetup->mult_for_bulk 		= 1;


			picsetup->bulk_preshot_additional_time = (-((SCAM_TSSCALE / picsetup->run_framerate) * picsetup->run_syncdiv * 5));
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);

			//---- Side Camera
			picsetup = &pisetup->icsetup[1];

			picsetup->masterslave			= 0;									// 0: MASTER, 1: SLAVE
			picsetup->camintrinsicid		= CAMINTRINSIC_12mm0;
			picsetup->zrtmode 				= 1;								//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLMARK;			

			picsetup->run_width 			= 400;								// == cam0.runwidth
			picsetup->run_height 			= 200;								// == cam0.run_height

			picsetup->run_processing_width 	= picsetup->run_width;
			picsetup->run_processing_height = picsetup->run_height;			

			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 40);		// == cam0.o_x
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2);		// == cam0.o_y

			picsetup->run_framerate 		= 2100;								// == cam0.run_framerate
			picsetup->run_basegain 			= 7000;							// == cam0.run_basegain

			picsetup->run_exposure 			= 30;								// == cam0.run_exposure
			picsetup->run_skip 				= 1;								// == cam0.run_skip
			picsetup->run_multitude 		= 4;								// == cam0.run_multitude
			picsetup->run_syncdiv 			= 1;

			picsetup->fullrange_exposure	= 70;
			picsetup->fullrange_basegain 	= 2000;

			picsetup->run_width_putter 		= 480;
			picsetup->run_height_putter 	= 240;

			picsetup->offsetoffset_x_putter = -420;
			picsetup->offsetoffset_y_putter = -120;

			picsetup->run_framerate_putter 	= 100;
			picsetup->run_basegain_putter 	= 1000;
			picsetup->run_exposure_putter 	= 100;
			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			picsetup->bulk_width 			= 96;
			picsetup->bulk_height 			= 96;

			picsetup->bulk_preshot_count 	= 8;
			picsetup->mult_for_bulk 		= 8;

			picsetup->bulk_preshot_additional_time = 0;
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);


			//--
			res = 1;
			break;
		}
#if 0
		case CAMSENSOR_CONSOLE1:
		{
			pisetup->camsensorcategory = camsensorcategory;

			//---- Center Camera
			picsetup = &pisetup->icsetup[0];

			picsetup->masterslave			= 1;						// 0: MASTER, 1: SLAVE
			picsetup->camintrinsicid		= CAMINTRINSIC_4mm0;
			picsetup->zrtmode 				= 0;									//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLCLUB;			

			picsetup->run_width 			= 720;			
			picsetup->run_height 			= 310;			

			picsetup->run_processing_width 	= 480;
			picsetup->run_processing_height = 240;			

			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 300);  // for 720x310
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2);

			picsetup->run_framerate 		= 2100;				// real framerate is 700
			picsetup->run_basegain 			= 2000;

			picsetup->run_exposure 			= 70;
			picsetup->run_skip 				= 1;
			picsetup->run_multitude 		= 4;
			picsetup->run_syncdiv 			= 3;

			picsetup->fullrange_exposure	= 70;
			picsetup->fullrange_basegain 	= 2000;

			picsetup->run_width_putter 		= 480;
			picsetup->run_height_putter 	= 240;

			picsetup->offsetoffset_x_putter = -420;
			picsetup->offsetoffset_y_putter = -120;

			picsetup->run_framerate_putter 	= 100;
			picsetup->run_basegain_putter 	= 15*256;
			picsetup->run_exposure_putter 	= 30;
			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			// 720 * 310 * 40(frame) = 8.52 MByte.   Transfer in 80 msec : 852 Mbps.. -_-;
			picsetup->bulk_width 			= picsetup->run_width;
			picsetup->bulk_height 			= picsetup->run_height;

			picsetup->bulk_preshot_count 	= 8;
			picsetup->mult_for_bulk 		= 1;

			picsetup->bulk_preshot_additional_time = (-((SCAM_TSSCALE / picsetup->run_framerate) * picsetup->run_syncdiv * 10));
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);


			//---- Side Camera
			picsetup = &pisetup->icsetup[1];

			picsetup->masterslave			= 0;									// 0: MASTER, 1: SLAVE
			picsetup->camintrinsicid		= CAMINTRINSIC_4mm0;
			picsetup->zrtmode 				= 1;								//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLMARK;			

			picsetup->run_width 			= 400;								// == cam0.runwidth
			picsetup->run_height 			= 200;								// == cam0.run_height

			picsetup->run_processing_width 	= picsetup->run_width;
			picsetup->run_processing_height = picsetup->run_height;			

			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 40);		// == cam0.o_x
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2);		// == cam0.o_y

			picsetup->run_framerate 		= 2100;								// == cam0.run_framerate
			picsetup->run_basegain 			= 7000;							// == cam0.run_basegain

			picsetup->run_exposure 			= 30;								// == cam0.run_exposure
			picsetup->run_skip 				= 1;								// == cam0.run_skip
			picsetup->run_multitude 		= 4;								// == cam0.run_multitude
			picsetup->run_syncdiv 			= 1;

			picsetup->fullrange_exposure	= 70;
			picsetup->fullrange_basegain 	= 2000;

			picsetup->run_width_putter 		= 480;
			picsetup->run_height_putter 	= 240;

			picsetup->offsetoffset_x_putter = -420;
			picsetup->offsetoffset_y_putter = -120;

			picsetup->run_framerate_putter 	= 100;
			picsetup->run_basegain_putter 	= 15*256;
			picsetup->run_exposure_putter 	= 30;
			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			picsetup->bulk_width 			= 96;
			picsetup->bulk_height 			= 96;

			picsetup->bulk_preshot_count 	= 8;
			picsetup->mult_for_bulk 		= 8;

			picsetup->bulk_preshot_additional_time = 0;
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);


			//--
			res = 1;
			break;
		}
#endif
#if 1
//#define CONSOLE1_TEST1
		case CAMSENSOR_CONSOLE1:
		{
			pisetup->camsensorcategory = camsensorcategory;

			//---- Center Camera
			picsetup = &pisetup->icsetup[0];

			picsetup->masterslave			= 1;						// 0: MASTER, 1: SLAVE
			picsetup->camintrinsicid		= CAMINTRINSIC_4mm0;
			//picsetup->zrtmode 			= 1;									//1: zrot, 0: NROT
			picsetup->zrtmode 			= 0;									//1: zrot, 0: NROT
			//picsetup->processmode 			= CAMPROCESSMODE_BALLMARK;			
			picsetup->processmode 			= CAMPROCESSMODE_BALLCLUB;			

//			picsetup->run_width 			= 480;			
//			picsetup->run_height 			= 240;			

			picsetup->run_width 			= 560;
			picsetup->run_height 			= 220;
#if defined(CONSOLE1_TEST1)
			picsetup->run_width 			= 640;
			picsetup->run_height 			= 310;
#endif

			picsetup->run_processing_width 	= picsetup->run_width;
			picsetup->run_processing_height = picsetup->run_height;			


			//picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 40);
//			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 100);
//			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 20 );
//
//
			//picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 100);
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 20 );

			//picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 160);
			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 220);
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 10 );
#if defined(V2000_TEST)
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 10 );
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 20 );
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 40 );
		//	picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 - 20 );		// 2018/1121, testing..
#endif


			//picsetup->run_framerate 		= 2600;				
			picsetup->run_framerate 		= 2670;				

#if defined(CONSOLE1_TEST1)
			picsetup->run_framerate 		= 2500;
#endif
			//picsetup->run_framerate 		= 2500;				
			//picsetup->run_framerate 		= 2100;				
			//picsetup->run_framerate 		= 2000;				
			//picsetup->run_basegain 			= 8000;
			//picsetup->run_exposure 			= 50;
			//

			//picsetup->run_basegain 			= 7000;
			//picsetup->run_exposure 			= 40;


//			picsetup->run_basegain 			= 6000;
//			picsetup->run_exposure 			= 40;


			//picsetup->run_basegain 			= 7000;
			//picsetup->run_exposure 			= 70;

			//picsetup->run_basegain 			= 8000;
			//picsetup->run_exposure 			= 60;
			picsetup->run_basegain 			= 8500;
			picsetup->run_exposure 			= 50;

			picsetup->run_skip 				= 1;
			//picsetup->run_multitude 		= 4;
			////////////////////////////picsetup->run_multitude 		= 6;
			picsetup->run_multitude 		= 4;
			//picsetup->run_syncdiv 			= 1;
			picsetup->run_syncdiv 			= 2;
#if defined(CONSOLE1_TEST1)
			picsetup->run_syncdiv 			= 3;
#endif

			picsetup->fullrange_exposure	= 70;
			picsetup->fullrange_basegain 	= 2000;

			picsetup->run_width_putter 		= 480;
			picsetup->run_height_putter 	= 240;

			picsetup->offsetoffset_x_putter = -420;
			picsetup->offsetoffset_y_putter = -120;

			picsetup->run_framerate_putter 	= 100;
			picsetup->run_basegain_putter 	= 15*256;
			picsetup->run_exposure_putter 	= 30;
			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			picsetup->bulk_width 			= picsetup->run_width;
			picsetup->bulk_height 			= picsetup->run_height;
			picsetup->mult_for_bulk 		= 1;
//			picsetup->bulk_width 			= 96;
//			picsetup->bulk_height 			= 96;
//			picsetup->mult_for_bulk 		= 8;

			picsetup->bulk_preshot_count 	= 8;

			picsetup->bulk_preshot_additional_time = (-((SCAM_TSSCALE / picsetup->run_framerate) * picsetup->run_syncdiv * 10));
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);


			//---- Side Camera
			picsetup = &pisetup->icsetup[1];

			picsetup->masterslave			= 0;									// 0: MASTER, 1: SLAVE
			picsetup->camintrinsicid		= CAMINTRINSIC_4mm0;
			picsetup->zrtmode 				= 1;								//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLMARK;			

			//picsetup->run_width 			= 480;								// == cam0.runwidth
			//picsetup->run_height 			= 240;								// == cam0.run_height

			picsetup->run_width 			= 400;								// == cam0.runwidth
			picsetup->run_height 			= 210;								// == cam0.run_height
#if defined(CONSOLE1_TEST1)
			picsetup->run_width 			= 480;								// for 2100 fps.
			picsetup->run_height 			= 220;								// 
#endif

			picsetup->run_processing_width 	= picsetup->run_width;
			picsetup->run_processing_height = picsetup->run_height;			

//			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 100);
//			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 20 );

			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 50);
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 10 );
#if defined(V2000_TEST)
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 20 );
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 40 );
#endif

			//picsetup->run_framerate 		= 2500;								// == cam0.run_framerate
			//picsetup->run_framerate 		= 2100;								// == cam0.run_framerate
			
			//picsetup->run_framerate 		= 2000;								// == cam0.run_framerate
			//picsetup->run_framerate 		= 2600;								// == cam0.run_framerate
			picsetup->run_framerate 		= 2670;								// == cam0.run_framerate
#if defined(V2000_TEST)
			//////////////////// picsetup->run_framerate 		= 2100;								// == cam0.run_framerate
#endif

			//picsetup->run_basegain 			= 8000;							// == cam0.run_basegain
			//
			//picsetup->run_basegain 			= 7000;							// == cam0.run_basegain
			//picsetup->run_exposure 			= 40;								// == cam0.run_exposure

			//////////////////////////////////////////////////
			//////////////////////////////////////////////////
			//////////////////////////////////////////////////
			//////////////////////////////////////////////////

			
			picsetup->run_basegain 			= 7000;							// == cam0.run_basegain
			picsetup->run_exposure 			= 30;								// == cam0.run_exposure
#if defined(V2000_TEST)
			picsetup->run_basegain 			= 2000;							// 20181121.TEST.
			//picsetup->run_exposure 			= 20;								// == cam0.run_exposure
			picsetup->run_exposure 			= 30;								// == cam0.run_exposure
#else
//#define TEST1

#if defined(TEST1)
			//picsetup->run_basegain 			= 4000;							// == cam0.run_basegain
			picsetup->run_basegain 			= 2000;							// == cam0.run_basegain
			picsetup->run_exposure 			= 60;								// == cam0.run_exposure
#endif
#endif




			//picsetup->run_exposure 			= 25;								// == cam0.run_exposure
			picsetup->run_skip 				= 1;								// == cam0.run_skip
			//picsetup->run_multitude 		= 4;								// == cam0.run_multitude
			/////////////picsetup->run_multitude 		= 6;								// == cam0.run_multitude
			picsetup->run_multitude 		= 4;								// == cam0.run_multitude
			picsetup->run_syncdiv 			= 1;

			picsetup->fullrange_exposure	= 70;
			picsetup->fullrange_basegain 	= 2000;

			picsetup->run_width_putter 		= 480;
			picsetup->run_height_putter 	= 240;

			picsetup->offsetoffset_x_putter = -420;
			picsetup->offsetoffset_y_putter = -120;

			picsetup->run_framerate_putter 	= 100;
			picsetup->run_basegain_putter 	= 15*256;
			picsetup->run_exposure_putter 	= 30;
			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			picsetup->bulk_width 			= picsetup->run_width;
			picsetup->bulk_height 			= picsetup->run_height;
			picsetup->mult_for_bulk 		= 1;

//			picsetup->bulk_width 			= 96;
//			picsetup->bulk_height 			= 96;
			//picsetup->mult_for_bulk 		= 8;

			picsetup->bulk_preshot_count 	= 8;

			picsetup->bulk_preshot_additional_time = 0;
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);


			//--
			res = 1;
			break;
		}
#endif

		case CAMSENSOR_TCAM:
		{
			pisetup->camsensorcategory = camsensorcategory;

			//---- Center Camera
			picsetup = &pisetup->icsetup[0];

			picsetup->masterslave			= 1;						// 0: MASTER, 1: SLAVE
			picsetup->camintrinsicid		= CAMINTRINSIC_8mm0;
			picsetup->zrtmode 			= 0;									//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLCLUB;			

			picsetup->run_width 			= 640;
			picsetup->run_height 			= 310;

			picsetup->run_processing_width 	= 480;
			picsetup->run_processing_height = 240;			


			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 240);
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 20 );

			//---- RUN..
			picsetup->run_framerate 		= 2100;				
			//picsetup->run_basegain 			= 2000;
			//picsetup->run_exposure 			= 70;

			//picsetup->run_basegain 			= 1000;
			//picsetup->run_exposure 			= 150;

			//picsetup->run_basegain 			= 2000;
			picsetup->run_basegain 			= 1000;
			picsetup->run_exposure 			= 130;

			//picsetup->run_basegain 			= 3000;				// 20180408
			//picsetup->run_exposure 			= 80;				// 20180408

			picsetup->run_skip 				= 1;
			picsetup->run_multitude 		= 4;
			picsetup->run_syncdiv 			= 3;

			//---- Full Range..
//			picsetup->fullrange_exposure	= 70;
//			picsetup->fullrange_basegain 	= 2000;
			/////picsetup->fullrange_exposure	= 30;
			
			//picsetup->fullrange_exposure	= 70;
			picsetup->fullrange_exposure	= 40;
			//picsetup->fullrange_basegain 	= 2000;
			picsetup->fullrange_basegain 	= 1000;

			picsetup->run_width_putter 		= 480;
			picsetup->run_height_putter 	= 240;

			picsetup->offsetoffset_x_putter = -420;
			picsetup->offsetoffset_y_putter = -120;

			picsetup->offsetoffset_x_putter = (-(I32)picsetup->run_width_putter + 100);  // for 640x310
			picsetup->offsetoffset_y_putter = (-(I32)picsetup->run_height_putter/2);


			picsetup->run_framerate_putter 	= 200;
			picsetup->run_basegain_putter 	= 1600;
			picsetup->run_exposure_putter 	= PUTTER_EXPOSURE_CBALL;

			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			picsetup->bulk_width 			= picsetup->run_width;
			picsetup->bulk_height 			= picsetup->run_height;

			picsetup->bulk_preshot_count 	= 8;
			picsetup->mult_for_bulk 		= 1;

			picsetup->bulk_preshot_additional_time = (-((SCAM_TSSCALE / picsetup->run_framerate) * picsetup->run_syncdiv * 5));
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);


			//---- Side Camera
			picsetup = &pisetup->icsetup[1];

//#define TTT_TEST
			picsetup->masterslave			= 0;									// 0: MASTER, 1: SLAVE
			picsetup->camintrinsicid		= CAMINTRINSIC_4mm0;
			picsetup->zrtmode 				= 1;								//1: zrot, 0: NROT
#if defined(TTT_TEST)
			picsetup->zrtmode 				= 0;								//1: zrot, 0: NROT
#endif
			picsetup->processmode 			= CAMPROCESSMODE_BALLMARK;			

			//picsetup->run_width 			= 480;								
			//picsetup->run_height 			= 240;								

			picsetup->run_width 			= 400;								
			//picsetup->run_height 			= 210;								
			picsetup->run_height 			= 230;								

#if defined(TTT_TEST)
			picsetup->run_width 			= 320;								
			picsetup->run_height 			= 150;								
#endif
			picsetup->run_processing_width 	= picsetup->run_width;
			picsetup->run_processing_height = picsetup->run_height;			


			//picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 50);
			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 80);
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 10 );
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 100 );
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 80 );
#if defined(TTT_TEST)
			//picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 30);
			///picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 100);
			///picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 70 );

			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 50);
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 60 );
#endif

			//picsetup->run_framerate 		= 2670;								
			picsetup->run_framerate 		= 2100;								
#if defined(TTT_TEST)
			picsetup->run_framerate 		= 2000;								
#endif

			picsetup->run_basegain 			= 7000;							
			picsetup->run_exposure 			= 30;								


#define TEST11
#if defined(TEST11)
			//picsetup->run_basegain 			= 4000;						
			picsetup->run_basegain 			= 2000;						
			//picsetup->run_exposure 			= 60;					
			picsetup->run_exposure 			= 20;					
			//picsetup->run_exposure 			= 30;					

			picsetup->run_basegain 			= 2000;						
			//picsetup->run_exposure 			= 60;					
			picsetup->run_exposure 			= 20;					


#endif

#if defined(TTT_TEST)
			//picsetup->run_exposure 			= 60;					
			//picsetup->run_exposure 			= 80;					
			picsetup->run_exposure 			= 80;					
#endif

			//picsetup->run_skip 				= 1;				
			picsetup->run_skip 				= 2;				
			picsetup->run_multitude 		= 4;			
			picsetup->run_syncdiv 			= 1;

			//picsetup->fullrange_exposure	= 70;
			//picsetup->fullrange_basegain 	= 2000;

			////picsetup->fullrange_exposure	= 30;
			picsetup->fullrange_exposure	= 20;
			picsetup->fullrange_basegain 	= 1000;

			picsetup->run_width_putter 		= 480;
			picsetup->run_height_putter 	= 240;

			picsetup->offsetoffset_x_putter = -420;
			picsetup->offsetoffset_y_putter = -120;

			picsetup->run_framerate_putter 	= 100;
			picsetup->run_basegain_putter 	= 15*256;
			picsetup->run_exposure_putter 	= 30;
			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			picsetup->bulk_width 			= picsetup->run_width;
			picsetup->bulk_height 			= picsetup->run_height;
			picsetup->mult_for_bulk 		= 1;

//			picsetup->bulk_width 			= 96;
//			picsetup->bulk_height 			= 96;
			//picsetup->mult_for_bulk 		= 8;

			picsetup->bulk_preshot_count 	= 8;

			picsetup->bulk_preshot_additional_time = 0;
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);


			//--
			res = 1;
			break;
		}
//#define FPS2000_TEST
		case CAMSENSOR_Z3:
		{
			pisetup->camsensorcategory = camsensorcategory;

			//---- Center Camera
			picsetup = &pisetup->icsetup[0];

			picsetup->masterslave			= 1;									// 0: MASTER, 1: SLAVE
			//picsetup->camintrinsicid		= CAMINTRINSIC_8mm0;
			picsetup->camintrinsicid		= CAMINTRINSIC_8mm1;					// 20190320

			picsetup->zrtmode 				= 0;									//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLCLUB;			
			//picsetup->run_width 			= 720;			
			//picsetup->run_height 			= 310;			

			//picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 300);  // for 720x310
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2);

			picsetup->run_width 			= 640;			
			picsetup->run_height 			= 310;			

			//picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 200);  // for 640x310
			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 240);  // for 640x310
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2);
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 +50);
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 +30);
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 +20);
			
			picsetup->run_processing_width 	= 480;
			picsetup->run_processing_height = 240;			

			picsetup->run_framerate 		= 2220;				// real framerate is 740
			picsetup->run_basegain 			= 2000;
			//picsetup->run_exposure 			= 70;
			picsetup->run_exposure 			= 100;

			picsetup->run_skip 				= 1;

			picsetup->run_multitude 		= 4;
			picsetup->run_syncdiv 			= 3;

			picsetup->fullrange_exposure	= 70;
			picsetup->fullrange_basegain 	= 2000;

			picsetup->run_width_putter 		= 480;
			picsetup->run_height_putter 	= 240;


			picsetup->offsetoffset_x_putter = (-(I32)picsetup->run_width_putter + 100);  // for 640x310
			picsetup->offsetoffset_y_putter = (-(I32)picsetup->run_height_putter/2);


			picsetup->run_framerate_putter 	= 200;
//#define PUTTER_EXPOSURE_CBALL	50
//#define PUTTER_EXPOSURE_CBALL	100
#define PUTTER_EXPOSURE_Z3	200
///#define PUTTER_EXPOSURE_Z3	100
//			picsetup->run_basegain_putter 	= 800;
			//////////picsetup->run_basegain_putter 	= 1600;
			picsetup->run_basegain_putter 	= 800;
			//picsetup->run_basegain_putter 	= 2000;
			picsetup->run_exposure_putter 	= PUTTER_EXPOSURE_Z3;

			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			// 720 * 310 * 40(frame) = 8.52 MByte.   Transfer in 80 msec : 852 Mbps.. -_-;
			picsetup->bulk_width 			= picsetup->run_width;
			picsetup->bulk_height 			= picsetup->run_height;

			picsetup->bulk_preshot_count 	= 8;
			picsetup->mult_for_bulk 		= 1;

			//------------------------------------------------------
			//picsetup->bulk_preshot_additional_time = (-((SCAM_TSSCALE / picsetup->run_framerate) * picsetup->run_syncdiv * 10));
			picsetup->bulk_preshot_additional_time = (-((SCAM_TSSCALE / picsetup->run_framerate) * picsetup->run_syncdiv * 5));
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);

			//---- Side Camera
			picsetup = &pisetup->icsetup[1];

			picsetup->masterslave			= 0;									// 0: MASTER, 1: SLAVE
			//picsetup->camintrinsicid		= CAMINTRINSIC_12mm0;


			picsetup->camintrinsicid		= CAMINTRINSIC_12mm1;					// 20190320
			picsetup->zrtmode 				= 1;								//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLMARK;			

//			picsetup->run_width 			= 400;								// == cam0.runwidth
//			picsetup->run_height 			= 200;								// == cam0.run_height

//#if defined(Z3_NEOCLUB)
			picsetup->run_width 			= 512;								// == cam0.runwidth
			picsetup->run_height 			= 200;								// == cam0.run_height
//#endif

			picsetup->run_processing_width 	= picsetup->run_width;
			picsetup->run_processing_height = picsetup->run_height;			

			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 40);		// == cam0.o_x
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2);		// == cam0.o_y

//#if defined(Z3_NEOCLUB)
			//picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 80);		// == cam0.o_x
			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + 80+32);		// == cam0.o_x
//#endif
			picsetup->run_framerate 		= 2220;								// == cam0.run_framerate

			//picsetup->run_basegain 			= 7000;							// == cam0.run_basegain
			//picsetup->run_basegain 			= 2000;
			picsetup->run_basegain 			= 4000;
//			picsetup->run_exposure 			= 30;								// == cam0.run_exposure
			picsetup->run_exposure 			= 40;								// == cam0.run_exposure


			picsetup->run_skip 				= 2;
			picsetup->run_multitude 		= 3;								// == cam0.run_multitude

			//picsetup->run_skip 				= 1;								// == cam0.run_skip
			//picsetup->run_multitude 		= 4;								// == cam0.run_multitude
			picsetup->run_syncdiv 			= 1;

			picsetup->fullrange_exposure	= 70;
			picsetup->fullrange_basegain 	= 2000;

			picsetup->run_width_putter 		= 480;
			picsetup->run_height_putter 	= 240;

			picsetup->offsetoffset_x_putter = (-(I32)picsetup->run_width_putter + 100);  // for 640x310
			picsetup->offsetoffset_y_putter = (-(I32)picsetup->run_height_putter/2);

			picsetup->run_framerate_putter 	= 200;


			/////////////picsetup->run_basegain_putter 	= 3000;
			picsetup->run_basegain_putter 	= 800;
			picsetup->run_exposure_putter 	= PUTTER_EXPOSURE_Z3;

			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

#define MULT4BULK_Z3

#if defined(MULT4BULK_Z3)
			picsetup->bulk_width 			= 96;
			picsetup->bulk_height 			= 96;
//#if defined(Z3_NEOCLUB)
			picsetup->bulk_height = 128;
			picsetup->bulk_width = 160;
//#endif
			

			picsetup->mult_for_bulk 		= 8;


#else
			picsetup->bulk_width 			= picsetup->run_width;
			picsetup->bulk_height 			= picsetup->run_height;

			picsetup->mult_for_bulk 		= 1;
#endif
			picsetup->bulk_preshot_count 	= 8;

			picsetup->bulk_preshot_additional_time = 0;
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);


			//--
			res = 1;
			break;
		}

//#define USE_12mm2
		case CAMSENSOR_EYEXO:
		{
			pisetup->camsensorcategory = camsensorcategory;

			//------------------------------------------------------
			//---- Center Camera
			picsetup = &pisetup->icsetup[0];
			picsetup->masterslave			= 1;									// 0: MASTER, 1: SLAVE
#if defined(USE_12mm2)
			picsetup->camintrinsicid		= CAMINTRINSIC_12mm2;					
#else
			picsetup->camintrinsicid		= CAMINTRINSIC_12mm1;					
#endif
			picsetup->zrtmode 				= 0;									//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLCLUB;			

//#define EYEXO_SIZETEST_0		// 20200604
//#define EYEXO_SIZETEST_1		// 20200604
//#define EYEXO_SIZETEST_2		
#define EYEXO_SIZETEST_3
//#define EYEXO_SIZETEST_NEWFPS_1			// 20200605

			picsetup->run_width 			= 512;					// 20191204
			picsetup->run_height 			= 240;			

			picsetup->run_framerate 		= 2220;				// real framerate is 740
			picsetup->run_basegain 			= 3500;

#if defined(EYEXO_SIZETEST_NEWFPS_1)
			picsetup->run_framerate 		= 2000;				// real framerate is 2000/3 = 666.6666666

			picsetup->run_width 			= 560;
			picsetup->run_height 			= 260;			
#endif

			//picsetup->run_basegain 			= 3000;
			//picsetup->run_basegain 			= 2000;				// 20200312

			picsetup->run_exposure 		= 40;
			picsetup->run_basegain 			= 3000;
			picsetup->run_basegain 			= 2600;					// 20200717.. 

			picsetup->run_skip 				= 1;
			picsetup->run_multitude 		= 4;
			picsetup->run_syncdiv 			= 2;


			//picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width/2 + (-32));  
			picsetup->offsetoffset_x = (-(I32)picsetup->run_width / 2 + (-64));
			//picsetup->offsetoffset_x = (-(I32)picsetup->run_width / 2 + (-96));
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + 0);

			picsetup->run_processing_width 	= 480;
			picsetup->run_processing_height = 240;			


			picsetup->fullrange_exposure	= 70;
			picsetup->fullrange_basegain 	= 2000;

			//picsetup->fullrange_exposure	= 40;				//  20200806, test
			//picsetup->fullrange_exposure	= 60;				//  20200806, test
			picsetup->fullrange_exposure	= 50;				//  20200806, test
			picsetup->fullrange_basegain 	= 2000;



			
			//picsetup->run_width_putter 	= 480;
			//picsetup->run_height_putter 	= 240;

			picsetup->run_width_putter 		= 640;
			picsetup->run_height_putter 	= 310;

			picsetup->offsetoffset_x_putter = (-(I32)picsetup->run_width_putter + 100);  // for 640x310
			picsetup->offsetoffset_y_putter = (-(I32)picsetup->run_height_putter/2);
			picsetup->run_framerate_putter 	= 200;

#define PUTTER_EXPOSURE_EYEXO	200
			picsetup->run_basegain_putter 	= 800;
			picsetup->run_exposure_putter 	= PUTTER_EXPOSURE_EYEXO;

			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			//--
			picsetup->bulk_width 			= picsetup->run_width;
			picsetup->bulk_height 			= picsetup->run_height;

			picsetup->bulk_preshot_count 	= 8;
			picsetup->mult_for_bulk 		= 1;
			
			picsetup->bulk_preshot_additional_time = (-((SCAM_TSSCALE / picsetup->run_framerate) * picsetup->run_syncdiv * 5));
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);
///AAAA

			//- TESTTEST.. 20200811
			picsetup->run_exposure 			= 30;					// FN: 30. SN: 50  ??? TODO: fix it.. -_-;
			//picsetup->run_basegain 			= 4000;
			picsetup->run_basegain 			= 3000;
			picsetup->exposure_post 		= 0;


			//- TESTTEST.. 20200916
			picsetup->run_exposure 			= 50;
			picsetup->run_basegain 			= 2000;
			picsetup->exposure_post 		= 0;




			//------------------------------------------------------
			//---- Side Camera
			picsetup 						= &pisetup->icsetup[1];
			picsetup->masterslave			= 0;									// 0: MASTER, 1: SLAVE
			picsetup->camintrinsicid		= CAMINTRINSIC_25mm5MP;					
			picsetup->zrtmode 				= 1;									//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLMARK;			

			/*
			   480x220      Current
			   496x215
			   512x208      32pixel, 16mm (6+6,   3mm+3mm)
			   528x202
		   */ 
			picsetup->run_width 			= 512;			// 480 -> 512  (+32)
			picsetup->run_height 			= 208;			

#if defined(EYEXO_SIZETEST_0)				// 20200604
			picsetup->run_width 			= 464;			// -> -48
			picsetup->run_height 			= 228;			// -> +10
#endif
#if defined(EYEXO_SIZETEST_1)				// 20200604
			picsetup->run_width 			= 448;			// -> -64
			picsetup->run_height 			= 236;			// -> +28
#endif
#if defined(EYEXO_SIZETEST_2)				
			picsetup->run_width 			= 512;
			picsetup->run_height 			= 208;
#endif

#if defined(EYEXO_SIZETEST_3)				
			picsetup->run_width 			= 480;
			picsetup->run_height 			= 222;
#endif

			picsetup->run_framerate 		= 2220;
			picsetup->run_exposure 			= 30;	
			
			picsetup->run_basegain 			= 1600;

//			picsetup->run_basegain 			= 1200;			// 20200716.. 
			picsetup->run_basegain 			= 1400;			// 20200717.. 

			//picsetup->run_basegain 			= 2000;	


#if defined(EYEXO_SIZETEST_NEWFPS_1)
			picsetup->run_framerate 		= 2000;

			picsetup->run_width 			= 528;
			picsetup->run_height 			= 228;
#endif

			picsetup->run_skip 				= 1;
			picsetup->run_multitude 		= 6;				// 20191204
			picsetup->run_syncdiv 			= 1;


#define EYEXO_CLUB_ASSIGN		(80+32+32+16)			// 	160


			//picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + (80+32+32+16));
			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + EYEXO_CLUB_ASSIGN);
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2);		// == cam0.o_y


#if defined(EYEXO_SIZETEST_0)
#undef	EYEXO_CLUB_ASSIGN
#define EYEXO_CLUB_ASSIGN		(80+32+32)			// 133
#endif
#if defined(EYEXO_SIZETEST_1)				
#undef	EYEXO_CLUB_ASSIGN
#define EYEXO_CLUB_ASSIGN		(80+32+16)			// 128
#endif
#if defined(EYEXO_SIZETEST_2)			
#undef	EYEXO_CLUB_ASSIGN
#define EYEXO_CLUB_ASSIGN		(80+32+32+16+32)	// 192
#endif

#if defined(EYEXO_SIZETEST_3)			
#undef	EYEXO_CLUB_ASSIGN
#define EYEXO_CLUB_ASSIGN		(80+32+32+16+16)	// 176
#endif

#if defined(EYEXO_SIZETEST_NEWFPS_1)
#undef	EYEXO_CLUB_ASSIGN
#define EYEXO_CLUB_ASSIGN		(80+32+32+16+32)	// 192
#endif

			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width + EYEXO_CLUB_ASSIGN);		// == cam0.o_x
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2);					// == cam0.o_y

			picsetup->run_processing_width 	= picsetup->run_width;
			picsetup->run_processing_height = picsetup->run_height;			

			//picsetup->exposure_post 		= 	10;				// EXPOSURE_POST_DEFAULT;
			picsetup->exposure_post 		= 	20;				// EXPOSURE_POST_DEFAULT;
			//picsetup->exposure_post 		= 	15;
			picsetup->exposure_expiate 		= 	EXPOSURE_EXPIATE_DEFAULT;


//			picsetup->exposure_post 		= 	50;			// TEST.. 20200811



			picsetup->fullrange_exposure	= 70;
			picsetup->fullrange_basegain 	= 2000;


			//picsetup->fullrange_exposure	= 40;				//  20200806, test
			//picsetup->fullrange_exposure	= 60;				//  20200806, test
			picsetup->fullrange_exposure	= 50;				//  20200806, test
			picsetup->fullrange_basegain 	= 2000;


			//picsetup->run_width_putter 	= 480;
			//picsetup->run_height_putter 	= 240;

			picsetup->run_width_putter 		= 640;
			picsetup->run_height_putter 	= 310;

			picsetup->offsetoffset_x_putter = (-(I32)picsetup->run_width_putter + 100);  // for 640x310
			picsetup->offsetoffset_y_putter = (-(I32)picsetup->run_height_putter/2);
			picsetup->run_framerate_putter 	= 200;

#define PUTTER_EXPOSURE_EYEXO	200
			picsetup->run_basegain_putter 	= 800;
			picsetup->run_exposure_putter 	= PUTTER_EXPOSURE_EYEXO;

			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;



			//- TESTTEST.. 20200811
			picsetup->run_exposure 			= 30;	
			picsetup->run_basegain 			= 1600;
			//picsetup->exposure_post 		= 0;
			picsetup->exposure_post 		= 20;			// 20200916

			//--
			picsetup->bulk_width 			= 240;
			picsetup->bulk_height 			= picsetup->run_height;

#if defined(EYEXO_SIZETEST_NEWFPS_1)
			picsetup->bulk_width 			= 256;
#endif


			picsetup->mult_for_bulk 		= 4;

			picsetup->bulk_preshot_count 	= 8;
			
			picsetup->bulk_preshot_additional_time = 0;
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);

			//--
			res = 1;
			break;
		}

		case CAMSENSOR_P3V2:
		{
			pisetup->camsensorcategory = camsensorcategory;

			//---- Center Camera
			picsetup = &pisetup->icsetup[0];

			picsetup->masterslave			= 1;						// 0: MASTER, 1: SLAVE
			//picsetup->camintrinsicid		= CAMINTRINSIC_8mm0;
			picsetup->camintrinsicid		= CAMINTRINSIC_8mm1;
			picsetup->zrtmode 				= 0;									//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLCLUB;			
			//picsetup->run_width 			= 640;			
			//picsetup->run_height 			= 310;			

			picsetup->run_width 			= 512;			
			picsetup->run_height 			= 260;			


			//picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width/2 + (-32));  
			//picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width/2 + (-32 - 32));  
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 +20);

			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width/2 + (-32 - 32 + 16));  
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 +20 + 20 );

			
			//picsetup->run_processing_width 	= 480;
			//picsetup->run_processing_height = 240;			
			picsetup->run_processing_width 	= picsetup->run_width;
			picsetup->run_processing_height = picsetup->run_height;			

			picsetup->run_framerate 		= 1200;				
//			picsetup->run_framerate 		= 1000;			
			//picsetup->run_framerate 		= 100;			
			//picsetup->run_basegain 			= 2000;

			//--
			//picsetup->run_exposure 			= 100;
			//picsetup->run_basegain 			= 4000;
			picsetup->run_exposure 			= 100;
			picsetup->run_basegain 			= 3000;
			//--

			picsetup->run_skip 				= 1;
			picsetup->run_multitude 		= 4;
			picsetup->run_syncdiv 			= 1;

			//picsetup->fullrange_exposure	= 100;
			//picsetup->fullrange_basegain 	= 4000;

			picsetup->fullrange_exposure	= 100;
			picsetup->fullrange_basegain 	= 2000;

			picsetup->run_width_putter 		= 480;
			picsetup->run_height_putter 	= 240;

			picsetup->offsetoffset_x_putter = (-(I32)picsetup->run_width_putter + 100);  // for 640x310
			picsetup->offsetoffset_y_putter = (-(I32)picsetup->run_height_putter/2);

			picsetup->run_framerate_putter 	= 200;
#define PUTTER_EXPOSURE_CBALL	200
			picsetup->run_basegain_putter 	= 1600;

			picsetup->run_exposure_putter 	= PUTTER_EXPOSURE_CBALL;

			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			picsetup->bulk_width 			= picsetup->run_width;
			picsetup->bulk_height 			= picsetup->run_height;

			picsetup->bulk_preshot_count 	= 8;
			picsetup->mult_for_bulk 		= 1;


			picsetup->bulk_preshot_additional_time = (-((SCAM_TSSCALE / picsetup->run_framerate) * picsetup->run_syncdiv * 5));
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);

#if defined(ROUGHBUNKER_COMPENSATION)
			//----
			//picsetup->fullrange_exposure	= 100;						// 20200924.. for XXX BUNKER.. XXX
			picsetup->fullrange_exposure	= 70;						// 20200924.. for XXX BUNKER.. XXX
			picsetup->fullrange_basegain 	= 1000;

			//picsetup->run_exposure 			= 100;
			//picsetup->run_basegain 			= 2000;
#endif
			//---- Side Camera
			picsetup = &pisetup->icsetup[1];

			picsetup->masterslave			= 0;									// 0: MASTER, 1: SLAVE
			picsetup->camintrinsicid		= CAMINTRINSIC_6mm5MP;
			picsetup->zrtmode 				= 0;								//1: zrot, 0: NROT
			picsetup->processmode 			= CAMPROCESSMODE_BALLCLUB;			

			picsetup->run_width 			= 512;
			picsetup->run_height 			= 260;

			picsetup->run_processing_width 	= picsetup->run_width;
			picsetup->run_processing_height = picsetup->run_height;			

			//picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width/2 + (-32-32) + (-64));  
			//picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + (32));

			//picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width/2 + (-32-32) + (-64));  
			picsetup->offsetoffset_x 		= (-(I32)picsetup->run_width/2 + (-32-32) + (-32));  
			picsetup->offsetoffset_y 		= (-(I32)picsetup->run_height/2 + (32));


			picsetup->run_framerate 		= 1200;
			//picsetup->run_framerate 		= 1000;
			//picsetup->run_framerate 		= 100;
			//picsetup->run_basegain 			= 2000;

			//--
			//picsetup->run_exposure 			= 100;
			//picsetup->run_basegain 			= 4000;
			picsetup->run_exposure 			= 100;
			picsetup->run_basegain 			= 3000;
			//--

			picsetup->run_skip 				= 1;
			picsetup->run_multitude 		= 4;
			picsetup->run_syncdiv 			= 1;

			picsetup->fullrange_exposure	= 100;
			picsetup->fullrange_basegain 	= 2000;

			picsetup->run_width_putter 		= 480;
			picsetup->run_height_putter 	= 240;

			picsetup->offsetoffset_x_putter = (-(I32)picsetup->run_width_putter + 100);  // for 640x310
			picsetup->offsetoffset_y_putter = (-(I32)picsetup->run_height_putter/2);

			picsetup->run_framerate_putter 	= 200;
			picsetup->run_basegain_putter 	= 1600;
			picsetup->run_exposure_putter 	= PUTTER_EXPOSURE_CBALL;

			picsetup->run_skip_putter 		= 1;
			picsetup->run_multitude_putter 	= 1;
			picsetup->run_syncdiv_putter 	= 1;

			picsetup->bulk_width 			= picsetup->run_width;
			picsetup->bulk_height 			= picsetup->run_height;

			picsetup->bulk_preshot_count 	= 8;
			picsetup->mult_for_bulk 		= 8;

			picsetup->bulk_preshot_count 	= 8;
			picsetup->mult_for_bulk 		= 1;


			picsetup->bulk_preshot_additional_time = (-((SCAM_TSSCALE / picsetup->run_framerate) * picsetup->run_syncdiv * 5));
			picsetup->bulk_time_preshot 	= ((U64)((1.0/picsetup->run_framerate) * TSSCALE_D) * picsetup->bulk_preshot_count);
#if defined(ROUGHBUNKER_COMPENSATION)
			//----
			//picsetup->fullrange_exposure	= 100;						// 20200924.. for XXX BUNKER.. XXX
			picsetup->fullrange_exposure	= 70;						// 20200924.. for XXX BUNKER.. XXX
			//picsetup->fullrange_exposure	= 50;						// 20200924.. for XXX BUNKER.. XXX
			picsetup->fullrange_basegain 	= 1000;

			//picsetup->run_exposure 			= 100;
			//picsetup->run_basegain 			= 2000;

#endif
			//--
			res = 1;
			break;
		}

		default:
		{	// what?
			res = 0;
			break;
		}

	}

	if (res == 1) {
		U32 camid;

		//--
		for (camid = 0; camid < NUM_CAM; camid++) { 
			picsetup = &pisetup->icsetup[camid];

			//
			icconf_null[camid].exposure_post 		= picsetup->exposure_post;
			icconf_null[camid].exposure_expiate 	= picsetup->exposure_expiate;
			//
			icconf_base[camid].exposure_post 		= picsetup->exposure_post;
			icconf_base[camid].exposure_expiate 	= picsetup->exposure_expiate;

			//icconf_fullrange[camid].gain 		= picsetup->fullrange_basegain;
			//icconf_fullrange[camid].exposure 	= picsetup->fullrange_exposure;


			icconf_fullrange_CheckReady_0[camid].gain 				= picsetup->fullrange_basegain;
			icconf_fullrange_CheckReady_0[camid].exposure  			= picsetup->fullrange_exposure;
			icconf_fullrange_CheckReady_0[camid].exposure_post 		= picsetup->exposure_post;
			icconf_fullrange_CheckReady_0[camid].exposure_expiate 	= picsetup->exposure_expiate;

			icconf_fullrange_CheckReady_1[camid].gain 				= picsetup->fullrange_basegain;
			icconf_fullrange_CheckReady_1[camid].exposure 			= picsetup->fullrange_exposure;
			icconf_fullrange_CheckReady_1[camid].exposure_post 		= picsetup->exposure_post;
			icconf_fullrange_CheckReady_1[camid].exposure_expiate 	= picsetup->exposure_expiate;


			//---
			icconf_run[camid].width		= picsetup->run_width;
			icconf_run[camid].height	= picsetup->run_height;
			icconf_run[camid].framerate = picsetup->run_framerate;
			icconf_run[camid].gain		= picsetup->run_basegain;
			icconf_run[camid].exposure	= picsetup->run_exposure;
			icconf_run[camid].skip		= picsetup->run_skip;
			icconf_run[camid].multitude = picsetup->run_multitude;
			icconf_run[camid].syncdiv	= picsetup->run_syncdiv;

			icconf_run[camid].exposure_post 	= picsetup->exposure_post;
			icconf_run[camid].exposure_expiate	= picsetup->exposure_expiate;

			//---
			icconf_run_putter[camid].width		= picsetup->run_width_putter;
			icconf_run_putter[camid].height		= picsetup->run_height_putter;
			icconf_run_putter[camid].framerate	= picsetup->run_framerate_putter;
			icconf_run_putter[camid].gain		= picsetup->run_basegain_putter;
			icconf_run_putter[camid].exposure	= picsetup->run_exposure_putter;

			icconf_run_putter[camid].exposure_post 		= picsetup->exposure_post;
			icconf_run_putter[camid].exposure_expiate	= picsetup->exposure_expiate;
		}
	}

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res %d\n", res);
	return res;
}


#if defined (__cplusplus)
}
#endif

