/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2015/12/20 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
//#include <direct.h>
#if defined(_WIN32)
#include <direct.h>
#include <tchar.h>
#endif
#include <math.h>
#include <string.h>

#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#else
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#endif

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"
#include "cr_nanoxml.h"
#include "scamif.h"
#include "scamif_buffer.h"

#include "iana_adapt.h"
#include "iana.h"
#include "iana_implement.h"
#include "iana_core.h"
#include "iana_cam.h"
#include "iana_work.h"
#include "iana_checkready.h"
#include "iana_ready.h"
#include "iana_shot.h"
#include "iana_run.h"
#include "iana_tool.h"
#include "iana_coordinate.h"
#include "iana_configure.h"
#include "iana_xbrd.h"
#include "iana_spin.h"
#include "iana_spin_ballclub.h"
//#include "iana_club_attack.h"
#include "iana_security.h"
#include "iana_simresult.h"

#if _MSC_VER >= 1910									// visual studio 2017 or later
#include "spincalc.h"
#endif

#include "iana_club_mark.h"
#if defined(_WIN32)
#include <shobjidl.h>
#endif

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

#define DEFAULT_GVSP_PORT_MASTER 15566		// Master Image 수신하는 HOST 측 Port
#define DEFAULT_GVSP_PORT_SLAVE  15567		// Slave Image 수신하는 HOST 측 Port

#define DEFAULT_GVSP_PORT_ATC0	 15568		// AttackCamera 0 Image 수신하는 HOST 측 Port
#define DEFAULT_GVSP_PORT_ATC1   15569		// AttackCamera 1 Image 수신하는 HOST 측 Port



//#define DEFAULT_GVSP_CAMPORT 3956			// Device Camera 의 Port
//////////#define DEFAULT_GVCP_CAMPORT 3956			// Device Camera 의 Port
#define DEFAULT_GVCP_MSGPORT 54377			// MSG 수신하는 HOST 측 port


//----
#define DEFAULT_GVSP_PORT_LS_MASTER		(DEFAULT_GVSP_PORT_MASTER+10)	// 15576		// Left sided, Master Image 수신하는 HOST 측 Port
#define DEFAULT_GVSP_PORT_LS_SLAVE		(DEFAULT_GVSP_PORT_SLAVE+10)	// 15577		// Left sided, Slave Image 수신하는 HOST 측 Port

#define DEFAULT_GVSP_PORT_LS_ATC0		(DEFAULT_GVSP_PORT_ATC0+10)		// 15578		// Left sided, AttackCamera 0 Image 수신하는 HOST 측 Port
#define DEFAULT_GVSP_PORT_LS_ATC1		(DEFAULT_GVSP_PORT_ATC1+10)		// 15579		// Left sided, AttackCamera 1 Image 수신하는 HOST 측 Port

#define DEFAULT_GVCP_LS_MSGPORT			(DEFAULT_GVCP_MSGPORT+10)		// 54387		// Left sided, MSG 수신하는 HOST 측 port

#define SHOTDATAFILE2		"shotmode2.txt"
#define SAVEBALLCLUBFILE	"D:\\ballclub.txt"
#define SAVEIMAGEDATA		"saveimagedata.txt"
#define IMAGE_TXT			_T("image.txt")
#define CLUBPATHTXT		_T("clubpath.txt")
#define IMAGEINFO_XML		_T("imageinfo.xml")
#define SHOTINFO_JSON		_T("shotinfo.json")
#define BALLIMPACTFILE		"ballimpact.jpg"

#define VLIMITFILE	"vlimit.txt"

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

static int s_shot_experiment = 0;

//static int s_opmode_FILE = 0;
static int s_opmode_FILE = 1;


/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/
extern int g_ccam_lite;

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/
extern void IANA_INFO_WriteShotResultToXml(iana_shotresult_t *psr, double spinmag2d, double spinaxis2d);
extern I32 set_check_circle_RANSAC_id(
		U32 ransac_id
		);

extern U32 IANA_SHOT_RefineBallPos_Bulk(iana_t *piana, U32 camid);


/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
int iana_scamif_fnCB(void *pData, 
	I32 nSize, U32 ts_h, U32 ts_l, I32 width, I32 height, I32 offset_x, I32 offset_y,
	U32 normalbulk, U32 skip, U32 multitude,
	U32 camid,	PARAM_T userparam);

static I32 imgbuftimediff(iana_t *piana, I64 *pt64diff);

static U32 checkshotmode2(iana_t *piana);


U32 iana_shot_experiment(iana_t *piana);
static void VerifyCamProcessingTime(iana_t *piana);

void iana_changerightleft(iana_t *piana);
void iana_changerightleft_LITE(iana_t *piana);


//I32 check_tsmatch(iana_t *piana);
I32 iana_check_tsmatch(iana_t *piana);

I32 iana_do_goodshot(iana_t *piana);
I32 iana_do_callback(iana_t *piana, U32 status);

static void Init_BallMarkImageData(iana_t *piana)
{
	int i;
	U32 camid;
	iana_cam_t	*pic;
	ballmarkimg_t *pbmim;

	for (camid = 0; camid < MAXCAMCOUNT; camid++) {
		pic   = &piana->RS_ic[camid];
		memset(&pic->bmimg[0], 0, sizeof(ballmarkimg_t) * MARKSEQUENCELEN);

		for (i = 0; i < MARKSEQUENCELEN; i++) {
			pbmim = &pic->bmimg[i];

			pbmim->valid	= 0;
			pbmim->pimg 	= (U08 *) malloc(WIDTHI * HEIGHTI);
			pbmim->pimgRaw 	= (U08 *) malloc(WIDTHI * HEIGHTI);
			pbmim->pimgBW 	= (U08 *) malloc(WIDTHI * HEIGHTI);
			pbmim->width	= WIDTHI;
			pbmim->height	= HEIGHTI;
		}

		pic   = &piana->LS_ic[camid];
		memset(&pic->bmimg[0], 0, sizeof(ballmarkimg_t) * MARKSEQUENCELEN);

		for (i = 0; i < MARKSEQUENCELEN; i++) {
			pbmim = &pic->bmimg[i];

			pbmim->valid	= 0;
			pbmim->pimg 	= (U08 *) malloc(WIDTHI * HEIGHTI);
			pbmim->pimgRaw 	= (U08 *) malloc(WIDTHI * HEIGHTI);
			pbmim->pimgBW 	= (U08 *) malloc(WIDTHI * HEIGHTI);
			pbmim->width	= WIDTHI;
			pbmim->height	= HEIGHTI;
		}
	}
}


static void Deinit_BallMarkImageData(iana_t *piana)
{
	int i;
	U32 camid;
	iana_cam_t	*pic;
	ballmarkimg_t *pbmim;

	for (camid = 0; camid < MAXCAMCOUNT; camid++) {
		pic   = &piana->RS_ic[camid];
		for (i = 0; i < MARKSEQUENCELEN; i++) {
			pbmim = &pic->bmimg[i];

			free(pbmim->pimg);
			free(pbmim->pimgRaw);
			free(pbmim->pimgBW);
		}
		memset(&pic->bmimg[0], 0, sizeof(ballmarkimg_t) * MARKSEQUENCELEN);

		pic   = &piana->LS_ic[camid];
		for (i = 0; i < MARKSEQUENCELEN; i++) {
			pbmim = &pic->bmimg[i];

			free(pbmim->pimg);
			free(pbmim->pimgRaw);
			free(pbmim->pimgBW);
		}
		memset(&pic->bmimg[0], 0, sizeof(ballmarkimg_t) * MARKSEQUENCELEN);
	}
}

//#define GHOSTSHOT
#if defined(GHOSTSHOT)
static void Check_GhostShot(iana_t *piana)
{
	static int ghostshotinterval_prev = 0;
	static int fakeballspeed = 10;
	FILE *fp;

	//-----
	int ghostshotmode	= 0;
	int ghostshotinterval = 0;
	int ballspeedX10	= 0;
	int clubspeed_AX10	= 0;
	int clubspeed_BX10	= 0;
	int clubpathX10		= 0;
	int clubfaceangleX10= 0;
	int sidespin 		= 0;
	int backspin 		= 0;
	int azimuthX10 		= 0;
	int inclineX10 		= 0;
	//--------------

	fp = cr_fopen("shot.txt", "r");
	if (fp) {
		fscanf(fp, "%d", &ghostshotmode);
		fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

		fscanf(fp, "%d", &ghostshotinterval);
		fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

		if (ghostshotmode == 1 && ghostshotinterval != ghostshotinterval_prev) {
			U32 status;
			IANA_shotdata_t sd, *psd;
			status = IANA_SENSOR_STATE_GOODSHOT;
			psd = &sd;

			fscanf(fp, "%d", &ballspeedX10);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

			fscanf(fp, "%d", &clubspeed_AX10);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

			fscanf(fp, "%d", &clubspeed_BX10);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

			fscanf(fp, "%d", &clubpathX10);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

			fscanf(fp, "%d", &clubfaceangleX10);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

			fscanf(fp, "%d", &sidespin);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

			fscanf(fp, "%d", &backspin);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

			fscanf(fp, "%d", &azimuthX10);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

			fscanf(fp, "%d", &inclineX10);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

			/*
			fscanf(fp, "%d", &sidespin);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
			*/
			psd->ballspeedx1000		= ballspeedX10*100;
			psd->clubspeed_Bx1000 	= clubspeed_BX10*100;
			psd->clubspeed_Ax1000 	= clubspeed_AX10*100;
			psd->clubpathx1000 		= clubpathX10*100;
			psd->clubfaceanglex1000 = clubfaceangleX10*100;
			psd->sidespin 			= sidespin;
			psd->backspin 			= backspin;
			psd->azimuthx1000 		= azimuthX10*100;
			psd->inclinex1000 		= inclineX10*100;

			if (piana->hcbfunc != NULL) {
				((IANA_CALLBACKFUNC*)(piana->hcbfunc))(
				(HAND) piana,
				status,
				psd,
				piana->userparam);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "After  Shot CallBack \n");
			}
		}
		ghostshotinterval_prev = ghostshotinterval;
		cr_fclose(fp);
	}
}	
#endif

/*!
 ********************************************************************************
 *	@brief      IANA core 
 *
 *  @return		trivial..
 *
 *	@author	    yhsuk
 *  @date       2015/12/20
 *******************************************************************************/

I32 iana_core(iana_t *piana)
{
	I32 res;
	U32 allowTee, allowIron, allowPutter;
	I32 needsetgamma;
	static I32 gamma = -1;



	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, "%s: ++ with hiana: %p\n", __func__, piana);

//#define GHOSTSHOT
#if defined(GHOSTSHOT)
	Check_GhostShot(piana);			
#endif

#define	READGOODCOUNT_Z3 3
	if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
		piana->readygoodcountGOOD = READGOODCOUNT_Z3;
	} else {
		// piana->readygoodcountGOOD = READGOODCOUNT;
	}

	if (piana->needrunstateempty == 1) {
		piana->runstate = IANA_RUN_EMPTY;
		piana->needrunstateempty = 0;
 	}

	if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
		if (piana->oneareamode) {
			if (piana->allowedarea[0] || piana->allowedarea[1])	{
				piana->allowedarea[IANA_AREA_TEE] = 1;
				piana->allowedarea[IANA_AREA_IRON] = 1;
			}
		}
	}

	needsetgamma = 0;

	if (piana->NeedChangeRight0Left1) {
 		iana_changerightleft(piana);
		if (piana->useLeftHanded) {
			iana_run_restart(piana);
		}
		needsetgamma = 1;
	}

#define LUTLED_TEST

#if defined(LUTLED_TEST)
	IANA_CAM_UpdateLedLut(piana, 0);
#endif
	if (piana->opmode == IANA_OPMODE_DEFAULT ||piana->opmode == IANA_OPMODE_CAM) {
		//---- Check Camera sanity
//		U64 ts640, ts641;
		
		I64 ts64diff=0;
		U32 currenttick=0;
		
		//---

		UNUSED(ts64diff);
		UNUSED(currenttick);

		VerifyCamProcessingTime(piana);

		//---------
		if (piana->activated == 0) {
			piana->runstate = IANA_RUN_EMPTY;
 		}
		if (piana->activated == 1) {
			if (piana->runstate == IANA_RUN_EMPTY) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d to Prepared.\n", __LINE__);

				piana->runstate = IANA_RUN_PREPARED;
				piana->runstate_prepared_tick = cr_gettickcount();
				// [[[ 2016/1215
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": fmc_init #0\n");
//#define TIMER_ALLMASTER  see iana.h

				scamif_camtimer_stop(piana->hscamif);
 
				scamif_camtimer_reset3(piana->hscamif);

				scamif_fmc_init(piana->hscamif);
#define SLEEP_AFTER_FMC_INIT	500
				if (piana->camsensor_category != CAMSENSOR_Z3&&piana->camsensor_category != CAMSENSOR_EYEXO) {
					cr_sleep(SLEEP_AFTER_FMC_INIT);
				}
				// ]]] 2016/1215

				needsetgamma = 1;
				IANA_CAM_UpdateLedLut(piana, 1);
			}
			if (piana->opmode != IANA_OPMODE_FILE) {
				if (piana->runstate == IANA_RUN_READY) {			// XXX  2020/1002 TEST.
					IANA_CAM_UpdateGain(piana);
				}
			}
		}

#if !defined(_DEBUG)
		if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
			U32 currenttick0;
			currenttick0 = cr_gettickcount();
#define HEARTBEAT_TICK			(5*1000)
#define HEARTBEAT_CHECKTICK		(   500)
			if (currenttick0 > piana->heartbeat_tick + HEARTBEAT_CHECKTICK
					|| currenttick0 < piana->heartbeat_tick) {
				piana->heartbeat_tick = currenttick0;
				scamif_cam_heartbeat(piana->hscamif, 0, HEARTBEAT_TICK);
			}
		}
#endif
	} 		// if (piana->opmode == IANA_OPMODE_DEFAULT ||piana->opmode == IANA_OPMODE_CAM) 
	else if (piana->opmode == IANA_OPMODE_FILE) {
		piana->restartcount = 0;
		piana->restartAtCcount = 0;
	}


	// Update allowed area and area range
	//
	allowTee 	= piana->allowedarea[IANA_AREA_TEE];
	allowIron 	= piana->allowedarea[IANA_AREA_IRON];
	allowPutter	= piana->allowedarea[IANA_AREA_PUTTER];

#if defined(USE_SHOTAREARUNMODE)
	if (piana->shotarearunmode == 1) {
		if (allowTee || allowIron) {
			allowTee = 1;
			allowIron = 1;
		}
	}
#endif

	if (piana->shotarea == IANA_AREA_PUTTER &&  allowPutter) {
		piana->processarea = piana->shotarea;
	}


//#define CHECKCHECKCOUNT		0
//#define CHECKCHECKCOUNT		2
#define CHECKCHECKCOUNT		5
//#define CHECKCHECKCOUNT		10


	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__" runstate: %d, tsdiffbadcount: %d, imagesuccessbadcount: %d restartcount: %d\n",
	//	piana->runstate, piana->tsdiffbadcount, piana->imagesuccessbadcount, piana->restartcount);

#if 0
	{
		U32 cpuid[4];
		U32 rrr;

		rrr = iana_xboard_cpusid_read(piana, cpuid);
	
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "res: %d    %08x %08x %08x %08x\n", rrr, 
			cpuid[0], 
			cpuid[1], 
			cpuid[2], 
			cpuid[3]);

	}
#endif

	if (
		piana->tsdiffbadcount > CHECKCHECKCOUNT || 
		
		piana->imagesuccessbadcount > CHECKCHECKCOUNT || piana->restartcount > CHECKCHECKCOUNT ) {			// BAD case..
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d to Prepared.\n", __LINE__);
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "tsdiffbadcount: %d, imagesuccessbadcount: %d, restartcount: %d\n",
		//	piana->tsdiffbadcount, piana->imagesuccessbadcount, piana->restartcount);

		piana->runstate = IANA_RUN_PREPARED;
		piana->runstate_prepared_tick = cr_gettickcount();
	} else {
		switch(piana->runstate) {
			case IANA_RUN_NULL:
				// do nothing
				break;

			case IANA_RUN_EMPTY:
				// do nothing
				break;
			case IANA_RUN_CAMCHECK:
				// do nothing
				break;
			case IANA_RUN_POWERSAVING:
				{
					U32 currenttick;
					U32 checkchange;
					I32 checkresult;

					currenttick = cr_gettickcount();
					if (currenttick < piana->powersavingtick) {
						piana->powersavingtick = currenttick;
					}

#define POWERSAVINGCHECK	(1000 * 1)
					if (currenttick > piana->powersavingtick + POWERSAVINGCHECK) {
						checkchange = 1;
					} else {
						checkchange = 0;
					}

					checkresult = iana_checkpowersaving(piana, checkchange);
					if (checkresult > 0) {
						U32 camid_;
						U32 ledbrgt;

						for (camid_ = 0; camid_ < NUM_CAM; camid_++) {
							ledbrgt = piana->pic[camid_]->icp.m_ledbrgt;
							scamif_led_brightness(piana->hscamif, camid_, ledbrgt);
						}

						piana->runstate = IANA_RUN_EMPTY;
					} else if (checkresult < 0) {
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"--\n");
						piana->needrestart = 1;				
					}

#define POWERSAVINGCHECK_TOOBIG	(1000 * 60 * 30)							
					if (currenttick > piana->powersavingtick + POWERSAVINGCHECK_TOOBIG) {
						piana->needrestart = 1;					// too big time..
					}
				}
				break;
			case IANA_RUN_PREPARED:
				{
					U32 i;
					iana_cam_param_t *picp;

					U32 currenttick;
 
					//--
					iana_teepos_update(piana);


					if (piana->camsensor_category == CAMSENSOR_P3V2) {			// Apply teedot mode for P3V2. 20200904
						piana->teedotmode = 1;
					}	else {
						piana->teedotmode = 0;
					}

					currenttick = cr_gettickcount();

//#define				RUNSTATE_PREPARED_TICK_MAX	(30*1000)
#define				RUNSTATE_PREPARED_TICK_MAX	(2*60*1000)
//#define				RUNSTATE_PREPARED_TICK_MAX	(5*1000)
					if (currenttick > piana->runstate_prepared_tick +RUNSTATE_PREPARED_TICK_MAX || currenttick < piana->runstate_prepared_tick) {
						piana->currentcamconf = IANA_CAMCONF_CHANGEME;
						piana->runstate_prepared_tick = currenttick;
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "RUNSTATE_PREPARED_CHECK\n");
					}

					for (i = 0; i < NUM_CAM; i++) 
					{
						picp = &piana->pic[i]->icp;

						picp->mPxx_ = 0;
						picp->bPxx_ = 0;
						picp->mPyy_ = 0;
						picp->bPyy_ = 0;

						memset(&picp->maPx[0], 0, sizeof(double)*3);
						memset(&picp->maPy[0], 0, sizeof(double)*3);
						memset(&picp->maPr[0], 0, sizeof(double)*3);

						memset(&picp->maLx[0], 0, sizeof(double)*3);
						memset(&picp->maLy[0], 0, sizeof(double)*3);
						memset(&picp->maLr[0], 0, sizeof(double)*3);

						picp->mxy_ = 0;
						picp->bxy_ = 0;
					}


					if (piana->opmode == IANA_OPMODE_FILE && s_opmode_FILE) {
						U32 camid;
						iana_cam_t	*pic;
						cr_point_t	*pstartPosL;
						point_t	Lp;


						for (camid = 0; camid < NUM_CAM; camid++) {
							pic   =piana->pic[camid];


							//----
							pstartPosL = &piana->info_startPosL[camid];

							Lp.x = pstartPosL->x;
							Lp.y = pstartPosL->y;

							pic->icp.L.x = Lp.x;
							pic->icp.L.y = Lp.y;

							pic->runProcessingWidth = piana->info_runProcessingWidth[camid];
							pic->runProcessingHeight = piana->info_runProcessingHeight[camid];

							pic->icp.P.x	= piana->info_startPosP[camid].x;
							pic->icp.P.y	= piana->info_startPosP[camid].y;
							pic->icp.P.z	= piana->info_startPosP[camid].z;
							pic->icp.cr		= 0;

							pic->icp.b3d.x	= piana->info_startPosB[camid].x;
							pic->icp.b3d.y	= piana->info_startPosB[camid].y;
							pic->icp.b3d.z	= piana->info_startPosB[camid].z;

							{
								double lr_, pr_;
								iana_ballradius(piana, camid, 
										&pic->icp.b3d,
										&pr_, &lr_);

								pic->icp.cr = pr_;
								cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] pr: %lf, lr: %lf\n", 
										camid, pr_, lr_);

							}
						}					

						for (camid = 0; camid < NUM_CAM; camid++) { 
							piana->pic[camid]->offset_x = piana->info_icc[camid].offset_x;
							piana->pic[camid]->offset_y = piana->info_icc[camid].offset_y;
						}

						piana->runstate = IANA_RUN_READY;
#if defined(DO_CONFIG_CONVERSION)
						iana_config_conversion2(piana, &Lp);
#endif
						break;
					}
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "before iana_checkready\n\n\n");
					iana_checkready(piana);
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "after  iana_checkready\n\n\n");

					if (piana->runstate == IANA_RUN_READY) {
						//IANA_Prepare4Ready(piana);			// To ready state..
#define TOREADY_SLEEP_PUTTER	200
#if defined(TOREADY_SLEEP_PUTTER)
						if (piana->processarea == IANA_AREA_PUTTER) {
							cr_sleep(TOREADY_SLEEP_PUTTER);
						}
#endif
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "before IANA_Prepare4Ready\n");
						IANA_Prepare4Ready(piana);			// To ready state..
						piana->ironAreaBallExistCount = 0;			// 20201130
						piana->ironAreaCheckCount = 0;				// 20201201
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "after  IANA_Prepare4Ready\n");
					}
				}
				break;
			case IANA_RUN_READY:
				{				 
					if (piana->camsensor_category == CAMSENSOR_EYEXO) {
						{
							int len = PATHBUFLEN;
							CHR wransacconf[PATHBUFLEN];
							char ransacconf[PATHBUFLEN];
							FILE *fp;
							int mode;
							int ransac_id;
							int ransac_seed;
							//--
							mode = 0;ransac_id = 0; ransac_seed =0;
#if defined(_WIN32)							
							cr_sprintf(wransacconf, _T("%s%s\\rconf.txt"), piana->szDrive, piana->szDir);
#else
							cr_sprintf(wransacconf, _T("%s%s/rconf.txt"), piana->szDrive, piana->szDir);
#endif

							OSAL_STR_ConvertUnicode2MultiByte(wransacconf,  ransacconf, len);
							fp = cr_fopen(ransacconf, "r");
							if (fp) {
								fscanf(fp, "%d", &mode);
								fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
								fscanf(fp, "%d", &ransac_id);
								fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
								fscanf(fp, "%d", &ransac_seed);
								fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
								cr_fclose(fp);
							}

							if (mode != 0) {
								piana->ransac_id	= ransac_id;		// 0: deterministic.  1: rand() with timer seed setting. 2: rand() with seed setting.
								piana->ransac_seed	= ransac_seed;
							}
						}
						{
							U32 ransac_kind;
							U32 rand_seed;
							if (piana->ransac_id == 0) {
								ransac_kind = 0;
							} else {
								ransac_kind = 6;
							}
							rand_seed = 0;
							if (piana->ransac_id == 1) {
								rand_seed = cr_gettickcount();
								piana->ransac_seed = rand_seed;
							} else if (piana->ransac_id == 2) {
								rand_seed = piana->ransac_seed;
							}


							//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "RANSAC     ransac_id: %d, ransac_kind: %d, ransac_seed: %d\n", 
							//		piana->ransac_id,
							//		ransac_kind,
							//		piana->ransac_seed);

                            set_check_circle_RANSAC_id(ransac_kind);
							srand(rand_seed);
						}
					}
					if (checkshotmode2(piana) == 1) {
						piana->runstate = IANA_RUN_EMPTY; 
						break;
					}


					if (piana->opmode == IANA_OPMODE_CAM) {
						if (piana->powersaving) {
							U32 currenttick;

							currenttick = cr_gettickcount();

							if (currenttick < piana->readystarttick) {
								piana->readystarttick = currenttick;
								piana->updategainCount = 0;
								piana->updategainTick = currenttick;
							}

							if (currenttick > piana->readystarttick + piana->powersavingtime) {
								piana->runstate = IANA_RUN_POWERSAVING;

								piana->powersavingtick = currenttick;

								cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "POWERSAVING!  %d %d\n", currenttick, piana->readystarttick);
								break;
							}
						}
					}


					// 1) Get Latest Image (Side-cam only)
					// 2) Detect Ready Ball and check Shot
#if 1
					if (piana->processarea == IANA_AREA_PUTTER) {
						res = iana_ready(piana);
					} else {
						I32 iter;
						if (piana->readymercycount == 0) {
							piana->readymercycount = 1;
						}
#define CALCREADY	4
						res = 0;
						for (iter = 0; iter < CALCREADY; iter++) {
							res = iana_ready(piana);
//							cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" after iana_ready(), res: %d, runstate: %d, iter: %d\n", 
//								res, piana->runstate, iter);					
							if (res == 0) {
								break;
							}
							if (piana->runstate != IANA_RUN_READY) {
								break;
							}
						}
					}
#endif
					if (piana->runstate == IANA_RUN_TRANSITORY) {

					}
				}
				break;
			case IANA_RUN_TRANSITORY:
				{
					U32 i;
 
					//I32 saveimagedata = 1;

					piana->shotresultflag = SHOTRESULT_NOSHOT;
					piana->shotresultcheckme = SHOTCHECKME_NO;

					memset(&piana->shotresultdata, 0, sizeof(iana_shotresult_t));
					memset(&piana->shotresult[0]    , 0, sizeof(iana_shotresult_t) * MAXCAMCOUNT);

					memset(&piana->shotresultdata2, 0, sizeof(iana_shotresult_t));
					memset(&piana->shotresult2[0]    , 0, sizeof(iana_shotresult_t) * MAXCAMCOUNT);


					memset(&piana->clubball_shotresultdata, 0, sizeof(iana_shotresult_t));
					memset(&piana->clubball_shotresult    , 0, sizeof(iana_shotresult_t) * MAXCAMCOUNT);

					piana->shotresultdata.valid = 0;
					piana->shotresultdata.readflag = 0;

					memset(&piana->shotresultfull, 0, sizeof(shotdata_full_t));
					if (piana->opmode == IANA_OPMODE_CAM) {
						iana_makeguid((char *) &piana->shotguid);		// Make GUID
					}
					memcpy(&piana->shotresultfull.shotguid, &piana->shotguid, sizeof(CR_GUID));

					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "before iana_shot()\n");
					if (s_shot_experiment) {
						iana_shot_experiment(piana);
					} else {
						//scamif_lock(piana->hscamif, SCAMIF_LOCK_WRITE);
						iana_shot(piana);
						//scamif_lock(piana->hscamif, SCAMIF_LOCK_NULL);
					}

					scamif_lock(piana->hscamif, SCAMIF_LOCK_NORMAL_WRITE);					// 20191112

					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "after iana_shot()\n");

					piana->shotresultdata.valid = 1;
					if (piana->opmode == IANA_OPMODE_FILE) {
						iana_simresult(piana);
					}

					if (piana->shotresultflag == SHOTRESULT_GOODSHOT) {
						iana_do_goodshot(piana);
					} else {
//#define READYMERCY

#if defined(READYMERCY)
						U32 currenttick;
//#define READYMERCYTIME	(200)
#define READYMERCYTIME		(500)
//#define READYMERCYTIME		(1000)
#define READY_NOMERCYTIME	(1000)

						currenttick = cr_gettickcount();

						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ZZZ currenttick: %d, (readystarttick; %d, diff: %d), (goodreadytick: %d,   diff: %d) \n",
								currenttick, 
								piana->readystarttick, currenttick - piana->readystarttick,
								piana->goodreadytick, currenttick - piana->goodreadytick);
						if (currenttick > piana->readystarttick + READY_NOMERCYTIME) {
							if (currenttick > piana->goodreadytick + READYMERCYTIME || currenttick < piana->goodreadytick) {
								cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ZZZ -- 1\n");
								piana->runstate = IANA_RUN_TRIALSHOT;
							} else {
								cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ZZZ -- 2\n");
								piana->runstate = IANA_RUN_READY;
							}
						} else {
							cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ZZZ -- 3\n");
							piana->runstate = IANA_RUN_TRIALSHOT;
						}
#else
						piana->runstate = IANA_RUN_TRIALSHOT;
#endif
						if (piana->opmode == IANA_OPMODE_FILE) {
							if (piana->activated == 1) {
								U32 status;
								IANA_shotdata_t sd;

								//--
								status = IANA_SENSOR_STATE_SWING;
								memset(&sd, 0, sizeof(IANA_shotdata_t));
								cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Before Shot CallBack, TRIALSHOT\n");

								((IANA_CALLBACKFUNC*)(piana->hcbfunc))(
									(HAND) piana,
									status,
									&sd,
									piana->userparam);
								cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "After  Shot CallBack, TRIALSHOT \n");
							} else {
								cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Activated: %d, discard callback.\n", 
										piana->activated);

							}
						}
					}
#if defined(INFO_ALLCONDITION)
#else
					if (piana->opmode == IANA_OPMODE_DEFAULT ||piana->opmode == IANA_OPMODE_CAM) 
#endif
					{
						U32 balldetected;
						if (piana->pic[0]->bcseqcount != 0 || piana->pic[1]->bcseqcount != 0) {
							balldetected = 1;
						} else {
							balldetected = 0;
						}
						if (balldetected) {
							if (piana->runstate == IANA_RUN_TRIALSHOT || piana->runstate == IANA_RUN_GOODSHOT) {
								// start info
								iana_info_start((HAND)piana, piana->shotresultflag,	piana->shotresultcheckme);
								iana_info_camrunconfig(piana);
								iana_info_ready(piana);
								iana_info_shot(piana);
								if (piana->shotresultflag) {
									iana_info_spin(piana);
								}
								if (piana->camsensor_category == CAMSENSOR_EYEXO) {
									iana_info_feature(piana);
								}
								iana_info_shotresult(piana);
								iana_info_end(piana);
							}
						} else {
							cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BALL IS NOT DETECTED AT ALL.  WWWWWW \n");
						}
					}

					if (piana->opmode == IANA_OPMODE_FILE) {
						if (piana->camsensor_category == CAMSENSOR_EYEXO) {
							if (piana->runstate == IANA_RUN_TRIALSHOT || piana->runstate == IANA_RUN_GOODSHOT) {
								iana_info_feature2(piana);				// save feature to seperate file.
							}
						}
					}

					//piana->runstate = IANA_RUN_EMPTY;
					//-- Update and clear veteran information
					if (piana->runstate != IANA_RUN_READY) {
						for (i = 0; i < NUM_CAM; i++) {
							memcpy(&piana->pic[i]->vtrPrev[0], &piana->pic[i]->vtr[0], sizeof(iana_veteran_t) * 3);
							memset(&piana->pic[i]->vtr[0], 0, sizeof(iana_veteran_t) * 3);
						}
					}
					scamif_lock(piana->hscamif, SCAMIF_LOCK_NULL);					// 20191112
				}
				break;
			case IANA_RUN_GOODSHOT:
				{
					// Wait for state change.. :)
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "GOODSHOT.\n");
					if (piana->opmode == IANA_OPMODE_FILE) {
						piana->runstate = IANA_RUN_EMPTY;
					} else {
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d to Prepared.\n", __LINE__);
						piana->runstate = IANA_RUN_PREPARED;
						piana->runstate_prepared_tick = cr_gettickcount();
						// [[[ 2016/1215
//#define TIMER_ALLMASTER  see iana.h

						scamif_camtimer_stop(piana->hscamif);

						res = scamif_camtimer_reset3(piana->hscamif);

						if (res > 0) {
							scamif_fmc_init(piana->hscamif);
							if (piana->camsensor_category != CAMSENSOR_Z3&&piana->camsensor_category != CAMSENSOR_EYEXO) {
								cr_sleep(SLEEP_AFTER_FMC_INIT);
							}

						} else {
							piana->needrestart = 1;
						}

					}
					cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d readygoodcount ZERO.\n", __LINE__);

					piana->readygoodcount = 0;
				}
				break;
			case IANA_RUN_TRIALSHOT:
				{
					// Wait for state change.. :)
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Trail shot.\n");
					if (piana->opmode == IANA_OPMODE_FILE) {
						piana->runstate = IANA_RUN_EMPTY;
					} else {
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d to Prepared.\n", __LINE__);
						piana->runstate = IANA_RUN_PREPARED;
						piana->runstate_prepared_tick = cr_gettickcount();
						scamif_camtimer_stop(piana->hscamif);

						res = scamif_camtimer_reset3(piana->hscamif);
						if (res > 0) {
							scamif_fmc_init(piana->hscamif);
							if (piana->camsensor_category != CAMSENSOR_Z3&&piana->camsensor_category != CAMSENSOR_EYEXO) {
								cr_sleep(SLEEP_AFTER_FMC_INIT);
							}

							piana->currentcamconf = IANA_CAMCONF_CHANGEME;
							cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d, before _LITE()\n", __LINE__);

							iana_run_start_LITE(piana);
#define TRIALSHOT_SLEEP		20
#if defined(TRIALSHOT_SLEEP)
							cr_sleep(TRIALSHOT_SLEEP);
#endif
						} else {
							piana->needrestart = 1;
						}
					}

					piana->readygoodcount = 0;
					cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d readygoodcount ZERO.\n", __LINE__);

				}
				break;
			default:
				{
					piana->runstate = IANA_RUN_NULL;
				}
		}
	}


	//----
	if (piana->opmode == IANA_OPMODE_DEFAULT ||piana->opmode == IANA_OPMODE_CAM) 
	{
		if (piana->needrestart == 0 && piana->needrestart2 == 0) {
			iana_cam_reconfig(piana, 1);		// configure camera..
		}
	}

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with runstate: %d", piana->runstate);
	return 0;
}


void setcurrentdirstr(HAND h, CHR *pcurrentdir, U32 lenstr)
{
	iana_t *piana;

	//--
	piana = (iana_t *) h;

	if (piana == NULL) {
		goto func_exit;
	}
	cr_sprintf(&g_datapath.szDataDir[0], TEXT("%s"), pcurrentdir);
	OSAL_STR_ConvertUnicode2MultiByte(g_datapath.szDataDir, g_datapath.sDataDir, PATHBUFLEN);

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" currentdir: %s\n", g_datapath.sDataDir);

#if defined(_WIN32)
	cr_sprintf(piana->szImageDirRoot, _T("%s%s"), g_datapath.szDataDir, _T("SCAMIMG\\"));
#else
	cr_sprintf(piana->szImageDirRoot, _T("%s/%s"), g_datapath.szDataDir, _T("SCAMIMG/"));
#endif
	cr_sprintf(piana->szImageDir, _T("%s%s"), piana->szImageDirRoot, _T("CURRENT"));

#if defined(_WIN32)
	cr_sprintf(g_datapath.szDataScDataDir, _T("%s\\%s\\"), g_datapath.szDataDir, _T("SCDATA"));
#else
	cr_sprintf(g_datapath.szDataScDataDir, _T("%s/%s/"), g_datapath.szDataDir, _T("SCDATA"));
#endif
	OSAL_STR_ConvertUnicode2MultiByte(g_datapath.szDataScDataDir,  g_datapath.sDataScDataDir, PATHBUFLEN);

	cr_wmkdir(g_datapath.szDataDir);
	cr_wmkdir(g_datapath.szDataScDataDir);

	cr_wmkdir(piana->szImageDirRoot);
	cr_wmkdir(piana->szImageDir);

	g_datapath.mode = 2;

	UNUSED(lenstr);
func_exit:
	return;
}

/*!
 ********************************************************************************
 *	@brief      IANA core thread main function
 *
 *  @return		trivial..
 *
 *	@author	    yhsuk
 *  @date       2015/11/30
 *******************************************************************************/
void *iana_core_mainfunc(void *startcontext)
{
	iana_t *piana;	
	cr_thread_t *pt;
	
	volatile U32	loop;
	U32				u0;
	U32		lcount;
	I32		res;

	iana_setup_t *pisetup;
	iana_cam_setup_t *picsetup;

	//--------------------------------------- 
	// Check handle of Sensor
	piana = (iana_t *)startcontext;
	if (piana == NULL) {
		// TODO... what?
		goto func_exit;
	}
 
	pisetup = &piana->isetup;

	//---------------------------------------
	// make scamif 
	piana->hscamif = scamif_create2((HAND)iana_scamif_fnCB, CAMWORKMODE_SENSING, 0, (PARAM_T)piana);	
	piana->Right0Left1 = 0;				
	piana->RS_hscamif = piana->hscamif;			// Default is right-handed.

	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		//scamif_camtimer_virtualslavetimer_set(piana->hscamif, 1);			// use virtual slave timer mode.
		scamif_camtimer_virtualslavetimer_set(piana->hscamif, 0);			// NO... :P
	} else {
		scamif_camtimer_virtualslavetimer_set(piana->hscamif, 0);			// NO...
	}


	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" currentdir: %s\n", g_datapath.sDataDir);
	g_datapath.mode = 0;


	{
		U08 *ip[2];			// camera IP
		U32 port[2];			// Port for Receiving Image


		U32 masterslave[2];			// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE

		// Center CAM
		picsetup = &pisetup->icsetup[0];

		ip[0] 		=  (U08 *)"172.16.1.201";

		if (piana->camsensor_category == CAMSENSOR_Z3) {
			ip[0] 		=  (U08 *)"172.16.1.222";
		} else if (piana->camsensor_category == CAMSENSOR_EYEXO) {
			ip[0] 		=  (U08 *)"172.16.1.232";
		} else {
			ip[0] 		=  (U08 *)"172.16.1.201";
		}
	
		//ip[0] 		=  (U08 *)"172.16.1.202";
		port[0] 			= DEFAULT_GVSP_PORT_MASTER;
//		masterslave[0] 	= SCAMIF_MASTERSLAVE_CAM0;	// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE
		masterslave[0] 	= picsetup->masterslave;	// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE

		// Side CAM
		picsetup = &pisetup->icsetup[1];
		ip[1] 		= (U08 *) "172.16.1.202";

		if (piana->camsensor_category == CAMSENSOR_Z3) {
			ip[1] 		=  (U08 *)"172.16.1.222";
		} else if (piana->camsensor_category == CAMSENSOR_EYEXO) {
			ip[1] 		=  (U08 *)"172.16.1.232";
		} else {
			ip[1] 		= (U08 *) "172.16.1.202";
		}
		//ip[1] 		= (U08 *) "172.16.1.203";
		port[1] 			= DEFAULT_GVSP_PORT_SLAVE;
		//masterslave[1] 	= SCAMIF_MASTERSLAVE_CAM1;	// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE
		masterslave[1] 	= picsetup->masterslave;	// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE

		if (piana->opmode == IANA_OPMODE_DEFAULT || piana->opmode == IANA_OPMODE_CAM) {
			if (piana->camsensor_category == CAMSENSOR_Z3)
			{
				ip[0] 		=  (U08 *)"172.16.1.222";
				ip[1] 		=  (U08 *)"172.16.1.222";
				res = scamif_cam_start2(piana->hscamif,
							NUM_CAM,				// camera count
							ip,				// camera IP
							port,			// Port for Receiving ImageF
							masterslave		// Master: 0, Slave: 1
							, 1				// GEV_STREAMING_CHANNEL_MULTI	=	1,
							);	
				IANA_CAM_UpdateLedLut(piana, 1);
			} else if (piana->camsensor_category == CAMSENSOR_EYEXO) 
			{
				ip[0] 		=  (U08 *)"172.16.1.232";
				ip[1] 		=  (U08 *)"172.16.1.232";
				res = scamif_cam_start2(piana->hscamif,
							NUM_CAM,				// camera count
							ip,				// camera IP
							port,			// Port for Receiving ImageF
							masterslave		// Master: 0, Slave: 1
							, 1				// GEV_STREAMING_CHANNEL_MULTI	=	1,
							);	
				IANA_CAM_UpdateLedLut(piana, 1);

			} else {
				res = scamif_cam_start(piana->hscamif,
						2,				// camera count
						//CAMWORKMODE_SENSING, // Work mode. 
						ip,				// camera IP
						port,			// Port for Receiving Image
						masterslave		// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE
						);
			}
		} else {
			res = scamif_cam_start_dummy(piana->hscamif,
					2,				// camera count
//					CAMWORKMODE_SENSING, // Work mode. 
					masterslave		// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE
					);				
		}

		piana->currentcamconf = IANA_CAMCONF_CHANGEME;
	}



	piana->hscamifAtC = NULL;


	//---------------------------------------
	// Left ? make scamif 
	if (piana->useLeftHanded) {
		piana->LS_hscamif = scamif_create2((HAND)iana_scamif_fnCB, CAMWORKMODE_SENSING, 1, (PARAM_T)piana);	
		if (piana->camsensor_category == CAMSENSOR_P3V2) {
			scamif_camtimer_virtualslavetimer_set(piana->LS_hscamif, 1);			// use virtual slave timer mode.
		} else {
			scamif_camtimer_virtualslavetimer_set(piana->LS_hscamif, 0);			// NO...
		}
			{
			U08 *ip[MAXCAMCOUNT];			// camera IP
			U32 port[MAXCAMCOUNT];		// Port for Receiving Image


			U32 masterslave[MAXCAMCOUNT];		// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE

			// Center CAM
			picsetup 		= &pisetup->icsetup[0];
			ip[0] 			=  (U08 *)"172.16.1.211";				
			port[0] 		= DEFAULT_GVSP_PORT_LS_MASTER;
			masterslave[0] 	= picsetup->masterslave;	// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE

			// Side CAM
			picsetup 		= &pisetup->icsetup[1];
			ip[1] 			= (U08 *) "172.16.1.212";
			port[1] 		= DEFAULT_GVSP_PORT_LS_SLAVE;
			masterslave[1] 	= picsetup->masterslave;	// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE

			if (piana->opmode == IANA_OPMODE_DEFAULT || piana->opmode == IANA_OPMODE_CAM) {
				if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
					ip[0] 		=  (U08 *)"172.16.1.224";
					ip[1] 		=  (U08 *)"172.16.1.224";

					res = scamif_cam_start2(piana->LS_hscamif,
							NUM_CAM,				// camera count
							ip,				// camera IP
							port,			// Port for Receiving ImageF
							masterslave		// Master: 0, Slave: 1
							, 1				// GEV_STREAMING_CHANNEL_MULTI	=	1,
							);	
				} else {
					res = scamif_cam_start(piana->LS_hscamif,
							2,				// camera count
							ip,				// camera IP
							port,			// Port for Receiving Image
							masterslave		// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE
							);				
				}
			} else {
				res = scamif_cam_start_dummy(piana->LS_hscamif,
						2,				// camera count
						masterslave		// Master: SCAMIF_MASTER, Slave: SCAMIF_SLAVE
						);				
			}
			piana->currentcamconf = IANA_CAMCONF_CHANGEME;
		}

		piana->LS_hscamifAtC = NULL;

	} else {
		piana->LS_hscamif = NULL;
		piana->LS_hscamifAtC = NULL;
	}


	//--------------------------------------- 
	// Change state..
	pt = (cr_thread_t *) piana->hth_core;
	pt->ustate = CR_THREAD_STATE_INITED;

	loop = 1;
	cr_thread_setpriority(pt->hthread, THREADPRIORITY_IANA_CORE);

	pt->ustate = CR_THREAD_STATE_RUN;
	//---------------------------------------


	piana->needrestart = 0;
	piana->needrestart2 = 0;

	//piana->needresetcam = 1;				// Reset Camera 
	piana->needresetcam = 0;				// Reset Camera 
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" needresetsensor\n");
	//piana->needresetsensor = 1;			// Reset Sensor
	piana->needresetsensor = 0;			// Reset Sensor
	//piana->needxbdcheck = 1;
	iana_xboard_needbdcheck(piana);


//	piana->resetcamtick;
//	piana->resetsensortick;

	piana->tsdiffbadcount = 0;
	piana->imagesuccessbadcount = 0;
	piana->restartcount = 0;
	piana->tsdiffchecktick = 0;


	piana->heartbeat_tick = 0;

	piana->tsdiffAtCbadcount = 0;
	piana->imagesuccessAtCbadcount = 0;
	piana->restartAtCcount = 0;

	piana->needrunstateempty = 0;


	piana->runstate_prepared_tick = 0;
	//piana->needxbdcheck = 1;
	//iana_xboard_needbdcheck(piana);

	
//-----------

//#if defined(ZROTMODE)
//#define ZROTNROT		1			// 1: zrot, 0: NROT
//#elif defined(NROTMODE)
//#define ZROTNROT		0			// 1: zrot, 0: NROT
//#else
//#error wht?
//#endif
//

#define AUTOINIT		1			// 1: AutoInit in CAM,  0: NO.
	if (piana->opmode != IANA_OPMODE_FILE) 
	{
		I32 ret;
		int camid;

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"before start 1\n");
		for(camid=0; camid < NUM_CAM; camid++) 
		{
			iana_cam_fmcinit_config(piana, camid, 1, pisetup->icsetup[0].zrtmode,  AUTOINIT);	// fmcinit: Do FMC Init or Not.    zrot: 1: ZROT mode,  0: NROT mode				// auto init
		}

		ret = iana_cam_start(piana);			// Camera Start..
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"after start 1 ret: %d\n", ret);
		if (ret == 0) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d ret: %d.   NEEDRESTART.\n", __LINE__, ret); 
			piana->needrestart = 1;

			cr_log(piana->hcl, 3, "needrestart %d ret: %d.   NEEDRESTART.\n", __LINE__, ret);
		}

	}

	Init_BallMarkImageData(piana);

	piana->ransac_id	= 0;		// 0: deterministic.  1: rand() with timer seed setting. 2: rand() with seed setting.
	piana->ransac_seed	= 0;

    set_check_circle_RANSAC_id(0);	// 0: deterministic

//-------------
	lcount = 0;

	memset(&piana->shotresultfull, 0, sizeof(shotdata_full_t));
#if _MSC_VER >= 1910									// visual studio 2017 or later
	SpinCalc::init();
#endif

	while(loop) {
		U32 check_msec;
#define CHECK_PERIOD	1				// 1000 Hz
//#define CHECK_PERIOD	10				// 100 Hz
//#define CHECK_PERIOD	100				// 10 Hz
#define LCOUNT	4
//		cr_trace(CR_MSG_AREA_GENERAL,CR_MSG_LEVEL_WARN /* CR_MSG_LEVEL_VERB*/, __FUNCTION__" loop. before sleep\n");
#define LCOUNTDECIMATION 4
		
		if (++lcount >= LCOUNTDECIMATION) {
			lcount = 0;
			check_msec = CHECK_PERIOD;
		} else {
			check_msec = 0;
		}
		
		u0 = cr_event_wait (pt->hevent, check_msec);

//		cr_trace(CR_MSG_AREA_GENERAL,CR_MSG_LEVEL_WARN /* CR_MSG_LEVEL_VERB*/, __FUNCTION__" loop.\n");
		
		if (pt->ustate == CR_THREAD_STATE_NEEDSTOP) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" I would be killed...\n");
			break;
		}

		if (piana->needrestart) {
			I32 ires;
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d to Prepared.\n", __LINE__);
			piana->runstate = IANA_RUN_PREPARED;
			piana->runstate_prepared_tick = cr_gettickcount();
			if (piana->needresetcam == 0) {
	#define 	RESTARTBADCOUNT	5
///////////////////	#define 	RESTARTBADCOUNT	10
//	#define 	RESTARTBADCOUNT	20
//	#define 	RESTARTBADCOUNT	1
				if (piana->restartcount > RESTARTBADCOUNT) 
				{
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" RESET CAM..\n");
					iana_xboard_needbdcheck(piana);
					cr_sleep(500);
					piana->needresetcam = 1;			// Reset Camera 
					piana->needrestart = 1;
					piana->restartcount = 0;

				} else {
					piana->restartcount++;
					if (piana->xboard_category == XBOARD_CATEGORY_XMINIHUB) {
						iana_xboard_needbdcheck(piana);
						piana->needresetcam = 1;
					}

					//{
					//	I64 ts64diff;
					//	imgbuftimediff(piana, &ts64diff);
					//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, ".. time diff: %10lf (%20I64d) #1 \n", ts64diff/TSSCALE_D, ts64diff);
					//}
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" call iana_run_restart() -- restartcount: %d\n", piana->restartcount);
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d to Prepared.\n", __LINE__);
					piana->runstate = IANA_RUN_PREPARED;
					piana->runstate_prepared_tick = cr_gettickcount();
					ires = iana_run_restart(piana);
					if (ires > 0) {
						scamif_camtimer_stop(piana->hscamif);

						ires = scamif_camtimer_reset3(piana->hscamif);

						if (ires > 0) {
							// 
						} else {
							piana->needrestart = 1;
						}

						if (piana->opmode == IANA_OPMODE_DEFAULT ||piana->opmode == IANA_OPMODE_CAM) 
						{
							cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" before                    iana_cam_reconfig\n");
							res = iana_cam_reconfig(piana, 1);		// configure camera..
							cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" after                     iana_cam_reconfig\n");

							cr_sleep(100);
						}

						//{
						//	I64 ts64diff;
						//	imgbuftimediff(piana, &ts64diff);
						//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, ".. time diff: %10lf (%20I64d) #2 \n", ts64diff/TSSCALE_D, ts64diff);
						//}

						
						//cr_sleep(50);

						//{
						//	I64 ts64diff;
						//	imgbuftimediff(piana, &ts64diff);
						//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, ".. time diff: %10lf (%20I64d) #3 \n", ts64diff/TSSCALE_D, ts64diff);
						//}
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" after iana_run_restart()\n");
					}
					if (ires == 0 || ires < 0) {
						piana->needrestart = 1;

						cr_log(piana->hcl, 3, "needrestart %s %d", "after iana_run_restart()", __LINE__);
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d to Prepared.\n", __LINE__);
						piana->runstate = IANA_RUN_PREPARED;
						piana->runstate_prepared_tick = cr_gettickcount();
					}
				}
			}	//	if (piana->needresetcam == 0) 

		}	// if (piana->needrestart)

		if (piana->needrestart == 0 && piana->needrestart2) {
			I32 ires;
			
			piana->restartcount++;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d to Prepared.\n", __LINE__);
			piana->runstate = IANA_RUN_PREPARED;
			piana->runstate_prepared_tick = cr_gettickcount();

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" call iana_run_restart2()\n");
			
			ires = iana_run_restart2(piana);
			if (ires > 0) {

				scamif_camtimer_stop(piana->hscamif);

				scamif_camtimer_reset3(piana->hscamif);
				scamif_fmc_init(piana->hscamif);
				if (piana->camsensor_category != CAMSENSOR_Z3&&piana->camsensor_category != CAMSENSOR_EYEXO) {
					cr_sleep(SLEEP_AFTER_FMC_INIT);
				}					
		
				if (piana->opmode == IANA_OPMODE_DEFAULT ||piana->opmode == IANA_OPMODE_CAM) 
				{
					iana_cam_reconfig(piana, 1);		// configure camera..
				}

				piana->needrestart  = 0;
				piana->needrestart2 = 0;
				cr_sleep(100);
			}
		}

		if (piana->opmode == IANA_OPMODE_DEFAULT ||piana->opmode == IANA_OPMODE_CAM) {
			I32 res0;
			I64 ts64diff;
			res0 = imgbuftimediff(piana, &ts64diff);
			if (res0 != 0) {
				piana->ts64delta = ts64diff;			
			}
		}

		switch(piana->state) {
			case IANA_STATE_CREATED:
				{
					// 
					piana->state = IANA_RUN_EMPTY;
					break;
				}

			case IANA_STATE_INITED:
				{
					// 
					piana->state = IANA_RUN_EMPTY;
					break;
				}

			case IANA_STATE_STARTED:
				{
					iana_core(piana);
					break;
				}

			case IANA_STATE_STOPPED:
				{
					piana->state = IANA_RUN_EMPTY;
					// 
					break;
				}
			case IANA_STATE_DELETED:
				{
					piana->state = IANA_RUN_EMPTY;
					loop = 0;
					// 
					break;
				}

			default:
				{
					piana->state = IANA_RUN_EMPTY;
					// WHAT?
					break;
				}
		}
	}


	{
		HAND RS_h;
		HAND LS_h;

		HAND RS_AtC_h;
		HAND LS_AtC_h;
		//--
		RS_h = piana->RS_hscamif;
		LS_h = piana->LS_hscamif;

		RS_AtC_h = piana->RS_hscamifAtC;
		LS_AtC_h = piana->LS_hscamifAtC;

		piana->hscamif = NULL;
		piana->RS_hscamif = NULL;
		piana->LS_hscamif = NULL;

		if (RS_h != NULL) {
			scamif_delete(RS_h);
		}
		if (LS_h != NULL) {
			scamif_delete(LS_h);
		}

		piana->hscamifAtC = NULL;
		piana->RS_hscamifAtC = NULL;
		piana->LS_hscamifAtC = NULL;


		if (RS_AtC_h != NULL) {
			scamif_delete(RS_AtC_h);
		}
		if (LS_AtC_h != NULL) {
			scamif_delete(LS_AtC_h);
		}
	}

	Deinit_BallMarkImageData(piana);	

	pt->ustate = CR_THREAD_STATE_STOPPING;

	//--------------------------------------- 

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"--\n");
	// I MUST die... -_-;
	return 0;
}

/*!
 ********************************************************************************
 *	@brief   	Function callback.   
 *	@param[in]  
 *				
 *	@param[in]  
 *				
 *	@param[out] 
 *				
 *
 *  @return		
 *
 *	@author	    yhsuk
 *  @date       2016/02/21
 *******************************************************************************/

int iana_scamif_fnCB(
	void *pData, 
	I32 nSize,
	U32 ts_h, U32 ts_l,
	I32 width, I32 height,
	I32 offset_x, I32 offset_y,
	U32 normalbulk, U32 skip, U32 multitude,		// 
	U32 camid,						// !!!
	PARAM_T userparam)
{

	//--
	UNUSED(pData);
	UNUSED(nSize);
	UNUSED(ts_h);
	UNUSED(ts_l);
	UNUSED(width);
	UNUSED(height);
	UNUSED(offset_x);
	UNUSED(offset_y);
	UNUSED(normalbulk);
	UNUSED(skip);
	UNUSED(multitude);
	UNUSED(camid);


//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "cb, %d\n", camid);
	//---
#if 0
	if (userparam != 0) {
		iana_t *piana;
		cr_thread_t *pt;

		piana = (iana_t *)userparam;

		if (piana->hth_core != NULL) {
			pt = (cr_thread_t *) piana->hth_core;
			cr_event_set(pt->hevent);				// send event to core..
		}
	}
#else
	UNUSED(userparam);
#endif
	return 1;
}

//
///*!
// ********************************************************************************
// *	@brief      IANA Command 
// *
// *
// *	@author	    yhsuk
// *  @date       2015/11/30
// *******************************************************************************/
static I32 imgbuftimediff(iana_t *piana, I64 *pt64diff)					// tsCheckReady - tsOther
{
	I32 res;

	U32 camidCheckReady, camidOther;

	camif_buffer_info_t *pbCheckReady, *pbOther;
	U32 rindexCheckReady, rindexOther;
	//-------------------

	pbCheckReady = NULL;
	pbOther = NULL;

#define FAKE_TDIFF	(1000 * (1000 * 1000 * 1000LL))
	*pt64diff = FAKE_TDIFF;

	camidCheckReady = piana->camidCheckReady;
	camidOther		= piana->camidOther;


	rindexCheckReady = 0;
	rindexOther = 0;
	res = scamif_imagebuf_peek_latest(piana->hscamif, camidCheckReady, NORMALBULK_NORMAL, &pbCheckReady, NULL, &rindexCheckReady);	
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<0> res: %d\n", res);
	if (res) {
		res = scamif_imagebuf_peek_latest(piana->hscamif, camidOther, NORMALBULK_NORMAL, &pbOther, NULL, &rindexOther);	
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<1> res: %d\n", res);
	} else {
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%d> peek latest res: %d\n", 0, res);
//		res = scamif_imagebuf_peek_latest(piana->hscamif, 1, 1, &pb1, NULL, &rindex1);	
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%d> peek latest res: %d\n", 1, res);
//		res = 0;
	}
//#define TIMERSYNC_DISPLAY
	if (res) {
		U64 ts64CheckReady, ts64Other;
		I64 ts64diff;

		ts64CheckReady = MAKEU64(pbCheckReady->ts_h, pbCheckReady->ts_l);			// tsMaster
		ts64Other = MAKEU64(pbOther->ts_h, pbOther->ts_l);			// tsSlave

		ts64diff = (I64) (ts64CheckReady - ts64Other);				// tsMaster - tsSlave
		*pt64diff = ts64diff;
#if defined(TIMERSYNC_DISPLAY)
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "rindex(%d, %d)t0: %lf, t1: %lf, diff: %lf\n",rindexCheckReady,rindexOther,  ts64CheckReady/TSSCALE_D , ts64Other/TSSCALE_D, ts64diff/TSSCALE_D);
#endif
	} else {
#if defined(TIMERSYNC_DISPLAY)
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%d> peek latest res: %d  (ts64diff: %lld)\n", 1, res, *pt64diff);
#endif
	}

	res = 1;
	return res;
}


static U32 checkshotmode2(iana_t *piana)
{
	U32 res;
#if defined(SHOTDATAFILE2)
	static U32 s_shotmode = 0;
	U32 shotmode;
	FILE *fp;
	double vmag, azimuth, incline;
	double sidespin, backspin, rollspin;
	double spinmag;
	cr_vector_t axis;
	iana_shotresult_t *psr;

	//--
	res = 0;
	fp = cr_fopen(SHOTDATAFILE2, "r");
	if (fp) {
		fscanf(fp, "%d", &shotmode);
		fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
		if (shotmode != 0 && shotmode != s_shotmode)
		{
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "=============== FAKE shot with shotmode %d\n", shotmode);
			fscanf(fp, "%lf", &vmag);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

			fscanf(fp, "%lf", &azimuth);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

			fscanf(fp, "%lf", &incline);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

			fscanf(fp, "%lf", &sidespin);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

			fscanf(fp, "%lf", &backspin);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

			fscanf(fp, "%lf", &rollspin);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

			axis.x = backspin;
			axis.y = rollspin;
			axis.z = -sidespin;
			spinmag = cr_vector_normalization(&axis, &axis);

			//--
			psr = &piana->shotresultdata;

			psr->vmag = vmag;
			psr->incline = incline;
			psr->azimuth = azimuth;

			psr->backspin = backspin;
			psr->sidespin = sidespin;

			psr->spinmag = spinmag;
			memcpy(&psr->axis, &axis, sizeof(cr_vector_t));

			piana->shotresultdata.valid = 1;
			piana->shotresultflag = SHOTRESULT_GOODSHOT;
			piana->runstate = IANA_RUN_GOODSHOT;

			if (piana->hcbfunc) {
				U32 status;
				IANA_shotdata_t sd;
				status = IANA_SENSOR_STATE_GOODSHOT;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "1.. vmag: %lf\n", psr->vmag);
				sd.ballspeedx1000 	= (int) (psr->vmag * 1000);
				sd.clubspeed_Bx1000 	= 0;
				sd.clubspeed_Ax1000 	= 0;
				sd.clubpathx1000 		= 0;
				sd.clubfaceanglex1000	= 0;
				sd.sidespin 			= (int) (psr->sidespin);
				sd.backspin 			= (int) (psr->backspin);
				sd.azimuthx1000 		= (int) (psr->azimuth * 1000);
				sd.inclinex1000 		= (int) (psr->incline * 1000);

				sd.axisx				= psr->axis.x;
				sd.axisy				= psr->axis.y;
				sd.axisz				= psr->axis.z;
				sd.spinmag				= psr->spinmag;

				if (piana->activated == 1) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Before Shot CallBack , vmag: %lf\n",sd.ballspeedx1000/1000.0 );
					((IANA_CALLBACKFUNC*)(piana->hcbfunc))(
					(HAND) piana,
					status,
					&sd,
					piana->userparam);
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "After  Shot CallBack \n");
				} else {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Activated: %d, discard callback.\n", 
							piana->activated);

				}
			}
			piana->shotresultdata.valid = 0;
			piana->shotresultflag = SHOTRESULT_NOSHOT;

			//piana->runstate = IANA_RUN_EMPTY;
			res = 1;
			//--
		}
		s_shotmode = shotmode;
		cr_fclose(fp);
	}
	return res;
	
#else
	res = 0;
	return res;
#endif
}


U32 iana_shot_experiment(iana_t *piana)
{
	U32 camid;
	iana_cam_param_t *picp, *picp0, *picp1;
	cr_point_t	CamPosLOrg[MAXCAMCOUNT];
	iana_shotresult_t sr0;
	iana_shotresult_t *psr;
	//cr_point_t	CamPosLtest;
	double dx, dy, dz;
	double dx0, dy0, dz0;
	double dx1, dy1, dz1;
	U32 index;
	FILE *fp;

	//----
	for (camid = 0; camid < NUM_CAM; camid++) {
		picp = &piana->pic[camid]->icp;
		memcpy(&CamPosLOrg[camid], &picp->CamPosL, sizeof(cr_point_t));
	}

	picp0 = &piana->pic[0]->icp;
	picp1 = &piana->pic[1]->icp;


	fp = cr_fopen("D:\\shot.csv", "w");
	if (fp) {
#define POSWIDTH	0.3	
#define POSDELTA	0.1	
		fprintf(fp, "Index, CAMID, CAM0 x ORG, CAM0 y ORG, CAM0 z ORG, CAM1 x ORG, CAM1 y ORG, CAM1 z ORG, CAM0 x, CAM0 y, CAM0 z, CAM1 x, CAM1 y, CAM1 z, dx0, dy0, dz0, dx1, dy1, dz1, vmagORG, inclineORG, azimuthORG, backspinORG, sidespinORG, vmag, incline, azimuth, backspin, sidespin \n");
		iana_shot(piana);

		dx0 = dy0 = dz0 = 0;
		dx1 = dy1 = dz1 = 0;
		index = 0;

		psr = &piana->shotresultdata;
		memcpy(&sr0, psr, sizeof(iana_shotresult_t));
		psr = &piana->shotresultdata;
		fprintf(fp, "%d, %d,   %lf, %lf, %lf,    %lf, %lf, %lf,    %lf, %lf, %lf,    %lf, %lf, %lf,    %lf, %lf, %lf,  %lf, %lf, %lf,     %lf, %lf, %lf, %lf, %lf,      %lf, %lf, %lf, %lf, %lf,    %lf, %lf, %lf, %lf, %lf\n", 
				index++, 0,
				CamPosLOrg[0].x, CamPosLOrg[0].y, CamPosLOrg[0].z, 
				CamPosLOrg[1].x, CamPosLOrg[1].y, CamPosLOrg[1].z, 
				CamPosLOrg[0].x, CamPosLOrg[0].y, CamPosLOrg[0].z, 
				CamPosLOrg[1].x, CamPosLOrg[1].y, CamPosLOrg[1].z, 
				dx0, dy0, dz0, dx1, dy1, dz1,
				sr0.vmag, sr0.incline, sr0.azimuth,    sr0.backspin, sr0.sidespin,
				psr->vmag, psr->incline, psr->azimuth,    psr->backspin, psr->sidespin,
				0.0, 0.0, 0.0, 0.0, 0.0
			   );


		{

			picp0->CamPosL.x = picp0->CamPosEstimatedL.x;
			picp0->CamPosL.y = picp0->CamPosEstimatedL.y;
			picp0->CamPosL.z = picp0->CamPosEstimatedL.z;
				
			picp1->CamPosL.x = picp1->CamPosEstimatedL.x;
			picp1->CamPosL.y = picp1->CamPosEstimatedL.y;
			picp1->CamPosL.z = picp1->CamPosEstimatedL.z;
				
			dx0 = picp0->CamPosL.x - CamPosLOrg[0].x;
			dy0 = picp0->CamPosL.y - CamPosLOrg[0].y;
			dz0 = picp0->CamPosL.z - CamPosLOrg[0].z;

			dx1 = picp1->CamPosL.x - CamPosLOrg[1].x;
			dy1 = picp1->CamPosL.y - CamPosLOrg[1].y;
			dz1 = picp1->CamPosL.z - CamPosLOrg[1].z;

			piana->shotresultflag = SHOTRESULT_NOSHOT;
			piana->shotresultcheckme = SHOTCHECKME_NO;

			memset(&piana->shotresultdata, 0, sizeof(iana_shotresult_t));
			memset(&piana->shotresult    , 0, sizeof(iana_shotresult_t) * MAXCAMCOUNT);

			memset(&piana->clubball_shotresultdata, 0, sizeof(iana_shotresult_t));
			memset(&piana->clubball_shotresult    , 0, sizeof(iana_shotresult_t) * MAXCAMCOUNT);

			piana->shotresultdata.valid = 0;
			piana->shotresultdata.readflag = 0;

			memset(&piana->shotresultfull, 0, sizeof(shotdata_full_t));

			iana_shot(piana);

			psr = &piana->shotresultdata;

			fprintf(fp, "%d, %d,   %lf, %lf, %lf,    %lf, %lf, %lf,    %lf, %lf, %lf,    %lf, %lf, %lf,    %lf, %lf, %lf,  %lf, %lf, %lf,     %lf, %lf, %lf, %lf, %lf,      %lf, %lf, %lf, %lf, %lf,    %lf, %lf, %lf, %lf, %lf\n", 
					index++, camid, 
					CamPosLOrg[0].x, CamPosLOrg[0].y, CamPosLOrg[0].z, 
					CamPosLOrg[1].x, CamPosLOrg[1].y, CamPosLOrg[1].z, 
					picp0->CamPosL.x, picp0->CamPosL.y, picp0->CamPosL.z,
					picp1->CamPosL.x, picp1->CamPosL.y, picp1->CamPosL.z,
					dx0, dy0, dz0, dx1, dy1, dz1,
					sr0.vmag, sr0.incline, sr0.azimuth,    sr0.backspin, sr0.sidespin,
					psr->vmag, psr->incline, psr->azimuth,    psr->backspin, psr->sidespin,
					psr->vmag-sr0.vmag, psr->incline-sr0.incline, psr->azimuth-sr0.azimuth,    psr->backspin-sr0.backspin, psr->sidespin-sr0.sidespin);
			fflush(fp);
		}

		for (camid = 0; camid < NUM_CAM; camid++) {
			memcpy(&piana->pic[0]->icp.CamPosL, &CamPosLOrg[0], sizeof(cr_point_t));
			memcpy(&piana->pic[1]->icp.CamPosL, &CamPosLOrg[1], sizeof(cr_point_t));

			picp = &piana->pic[camid]->icp;
			for (dx = -POSWIDTH; dx <= POSWIDTH + 0.01; dx += POSDELTA) {
				for (dy = -POSWIDTH; dy <= POSWIDTH + 0.01; dy += POSDELTA) {
					for (dz = -POSWIDTH; dz <= POSWIDTH + 0.01; dz += POSDELTA) {

						picp->CamPosL.x = CamPosLOrg[camid].x + dx;
						picp->CamPosL.y = CamPosLOrg[camid].y + dy;
						picp->CamPosL.z = CamPosLOrg[camid].z + dz;

						piana->shotresultflag = SHOTRESULT_NOSHOT;

						memset(&piana->shotresultdata, 0, sizeof(iana_shotresult_t));
						memset(&piana->shotresult    , 0, sizeof(iana_shotresult_t) * MAXCAMCOUNT);

						memset(&piana->clubball_shotresultdata, 0, sizeof(iana_shotresult_t));
						memset(&piana->clubball_shotresult    , 0, sizeof(iana_shotresult_t) * MAXCAMCOUNT);

						piana->shotresultdata.valid = 0;
						piana->shotresultdata.readflag = 0;

						memset(&piana->shotresultfull, 0, sizeof(shotdata_full_t));

						iana_shot(piana);

						psr = &piana->shotresultdata;

						if (camid == 0) {
							dx0 = dx; dy0 = dy;	dz0 = dz;
							dx1 = dy1 = dz1 = 0;
						} else {
							dx0 = dy0 = dz0 = 0;
							dx1 = dx; dy1 = dy;	dz1 = dz;
						}
						fprintf(fp, "%d, %d,   %lf, %lf, %lf,    %lf, %lf, %lf,    %lf, %lf, %lf,    %lf, %lf, %lf,    %lf, %lf, %lf,  %lf, %lf, %lf,     %lf, %lf, %lf, %lf, %lf,      %lf, %lf, %lf, %lf, %lf,    %lf, %lf, %lf, %lf, %lf\n", 
								index++, camid, 
								CamPosLOrg[0].x, CamPosLOrg[0].y, CamPosLOrg[0].z, 
								CamPosLOrg[1].x, CamPosLOrg[1].y, CamPosLOrg[1].z, 
								picp0->CamPosL.x, picp0->CamPosL.y, picp0->CamPosL.z,
								picp1->CamPosL.x, picp1->CamPosL.y, picp1->CamPosL.z,
								dx0, dy0, dz0, dx1, dy1, dz1,
								sr0.vmag, sr0.incline, sr0.azimuth,    sr0.backspin, sr0.sidespin,
								psr->vmag, psr->incline, psr->azimuth,    psr->backspin, psr->sidespin,
								psr->vmag-sr0.vmag, psr->incline-sr0.incline, psr->azimuth-sr0.azimuth,    psr->backspin-sr0.backspin, psr->sidespin-sr0.sidespin);
						fflush(fp);
					} // dz
				} //dy
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "index; %d\n", index);
				fflush(fp);
			} // dx
		} 	// for (camid = 0; camid < 2; camid++) 

		{

			picp0->CamPosL.x = picp0->CamPosEstimatedL.x;
			picp0->CamPosL.y = picp0->CamPosEstimatedL.y;
			picp0->CamPosL.z = picp0->CamPosEstimatedL.z;
				
			picp1->CamPosL.x = picp1->CamPosEstimatedL.x;
			picp1->CamPosL.y = picp1->CamPosEstimatedL.y;
			picp1->CamPosL.z = picp1->CamPosEstimatedL.z;
				
			dx0 = picp0->CamPosL.x - CamPosLOrg[0].x;
			dy0 = picp0->CamPosL.y - CamPosLOrg[0].y;
			dz0 = picp0->CamPosL.z - CamPosLOrg[0].z;

			dx1 = picp1->CamPosL.x - CamPosLOrg[1].x;
			dy1 = picp1->CamPosL.y - CamPosLOrg[1].y;
			dz1 = picp1->CamPosL.z - CamPosLOrg[1].z;

			piana->shotresultflag = SHOTRESULT_NOSHOT;

			memset(&piana->shotresultdata, 0, sizeof(iana_shotresult_t));
			memset(&piana->shotresult    , 0, sizeof(iana_shotresult_t) * MAXCAMCOUNT);

			memset(&piana->clubball_shotresultdata, 0, sizeof(iana_shotresult_t));
			memset(&piana->clubball_shotresult    , 0, sizeof(iana_shotresult_t) * MAXCAMCOUNT);

			piana->shotresultdata.valid = 0;
			piana->shotresultdata.readflag = 0;

			memset(&piana->shotresultfull, 0, sizeof(shotdata_full_t));

			iana_shot(piana);

			psr = &piana->shotresultdata;

			fprintf(fp, "%d, %d,   %lf, %lf, %lf,    %lf, %lf, %lf,    %lf, %lf, %lf,    %lf, %lf, %lf,    %lf, %lf, %lf,  %lf, %lf, %lf,     %lf, %lf, %lf, %lf, %lf,      %lf, %lf, %lf, %lf, %lf,    %lf, %lf, %lf, %lf, %lf\n", 
					index++, camid, 
					CamPosLOrg[0].x, CamPosLOrg[0].y, CamPosLOrg[0].z, 
					CamPosLOrg[1].x, CamPosLOrg[1].y, CamPosLOrg[1].z, 
					picp0->CamPosL.x, picp0->CamPosL.y, picp0->CamPosL.z,
					picp1->CamPosL.x, picp1->CamPosL.y, picp1->CamPosL.z,
					dx0, dy0, dz0, dx1, dy1, dz1,
					sr0.vmag, sr0.incline, sr0.azimuth,    sr0.backspin, sr0.sidespin,
					psr->vmag, psr->incline, psr->azimuth,    psr->backspin, psr->sidespin,
					psr->vmag-sr0.vmag, psr->incline-sr0.incline, psr->azimuth-sr0.azimuth,    psr->backspin-sr0.backspin, psr->sidespin-sr0.sidespin);
			fflush(fp);
		}
		cr_fclose(fp);
	}

	return 1;
}

/*!
 ********************************************************************************
 *	@brief      IANA core check time
 *
 *  @return		trivial..
 *
 *	@author	    yhsuk
 *  @date       2017/11/21
 *******************************************************************************/
#if 1
static void VerifyCamProcessingTime(iana_t *piana)
{
	I64 ts64diff;
	U32 currenttick;

	currenttick = cr_gettickcount();

	//#define TSDIFFCHECKTICK	1000
#define TSDIFFCHECKTICK	100
//////#define TSDIFFCHECKTICK	500
	if (currenttick > piana->tsdiffchecktick + TSDIFFCHECKTICK || currenttick < piana->tsdiffchecktick) {
		I32 res0, res1, resAt0;
		//	static int iii = 0;
		U64 ts640, ts641;
		U64 ts64At0;

		double t0, t1, tAt0;

		I64 tsdiff0, tsdiff1;
		double td0, td1;
		double td01;

		camif_buffer_info_t *pb0, *pb1;
		camif_buffer_info_t *pbAt0;


		//---
		piana->tsdiffchecktick = currenttick;
		imgbuftimediff(piana, &ts64diff);
//#define RECLEAR_TIMEDIFF	(0.002)
#define RECLEAR_TIMEDIFF	(0.020)
//#define RECLEAR_TIMEDIFF	(0.0005)
#define TDTOOBIGCOUNT	5
#if defined(RECLEAR_TIMEDIFF)
		if (piana->camsensor_category == CAMSENSOR_P3V2) {		// 20200319
			double td;
			static int tdtoobigcount = 0;

			td = ts64diff/TSSCALE_D;
			if (td < - RECLEAR_TIMEDIFF || td > RECLEAR_TIMEDIFF) {
				tdtoobigcount++;
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "tdtoobigcount: %d, td: %lf\n", tdtoobigcount, td);
				if (tdtoobigcount > TDTOOBIGCOUNT) {
					scamif_camtimer_reset3(piana->hscamif);		
					tdtoobigcount = 0;
				}
			} else {
				tdtoobigcount = 0;
			}
		}
#endif
#define TS64DIFFBAD		(2.0)
#define BADCOUNTBAD		20
		//	if (!(++iii % 10)) 
		ts640 = 777;
		ts641 = 888;


		resAt0 = 0;
		{
			res0 = scamif_imagebuf_peek_latest(piana->hscamif, 0, NORMALBULK_NORMAL, &pb0, NULL, NULL);		// camid 0;
			res1 = scamif_imagebuf_peek_latest(piana->hscamif, 1, NORMALBULK_NORMAL, &pb1, NULL, NULL);		// camid 1;	

			UNUSED(ts64At0);UNUSED(td0);UNUSED(td1);UNUSED(tsdiff1);UNUSED(pbAt0);UNUSED(tAt0);UNUSED(resAt0);

			if (res0 && res1) {
				ts640 = MAKEU64(pb0->ts_h, pb0->ts_l);
				ts641 = MAKEU64(pb1->ts_h, pb1->ts_l);
				tsdiff0 = (I64) (ts640 - ts641);

				t0 = ts640 / TSSCALE_D;
				t1 = ts641 / TSSCALE_D;

				td01 = (I64) (ts640 - ts641) /TSSCALE_D ;


				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, ".. time diff: t0: %10lf t1: %10lf    t0 - t1: %20.18lf\n", 
				//		t0, t1, td01);

				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, ".. time diff: t0: %lld t1: %lld    t0 - t1: %lld\n", 
				//		ts640, ts641, tsdiff0);

			} else {
				tsdiff0 = 0;			// Discard it..
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ts64.. res0: %d, res1: %d, resAt0: %d\n", res0, res1, resAt0);
			}
			ts64diff = tsdiff0;
		}
		if (
				(ts64diff/TSSCALE_D > TS64DIFFBAD) 
				|| (ts64diff/TSSCALE_D < -TS64DIFFBAD)
		   ) {
			piana->tsdiffbadcount++;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, ".. %lld %lld,time diff: %10lf (%20I64d) (tsdiffbadcount: %d, restartcount: %d )\n", 
				ts640,
				ts641,
			
				ts64diff/TSSCALE_D, ts64diff,
					piana->tsdiffbadcount, piana->restartcount);
		} else {
			U32 goodtick;
			U32 ticksuccess;
			int ci;

			goodtick = 1;
			for (ci = 0; ci < 2; ci++) {
				ticksuccess = scamif_imagebuf_timetick(piana->hscamif, ci);
//#define TICKBADTICK2		1000
//#define TICKBADTICK2		2000
#define TICKBADTICK2		5000
//#define TICKBADTICK2		10000
				//#define TICKBADTICK2		500
				if (currenttick > ticksuccess + TICKBADTICK2) {
					//scamif_imagebuf_timetickupdate(piana->hscamif, ci);
					piana->imagesuccessbadcount++;
#if !defined(_DEBUG)
					piana->imagesuccessbadcount += BADCOUNTBAD;
#endif
					goodtick = 0;
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"%d [%d] ct:%d >  %d (%d+%d)\n", __LINE__,
					//		ci, currenttick, ticksuccess +TICKBADTICK2, ticksuccess, TICKBADTICK2);
				} else {
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"%d [%d] ct:%d <  %d (%d+%d)\n",__LINE__,
					//	ci, currenttick, ticksuccess +TICKBADTICK2, ticksuccess, TICKBADTICK2);
				}
			}

			if (goodtick == 1) {
				piana->tsdiffbadcount = 0;			// good..
				piana->imagesuccessbadcount = 0;

				piana->restartcount = 0;
				piana->needrestart = 0;
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" Clear tsdiffbadcount, imagesuccessbadcount, restartcount, needrestart... \n");
			}

		}

		if (piana->imagesuccessbadcount > BADCOUNTBAD || piana->tsdiffbadcount > BADCOUNTBAD) {
			piana->needrestart = 1;
			//piana->needxbdcheck = 1;
			//iana_xboard_needbdcheck(piana);


			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "NEEDRESTART!!!  imagesuccessbadcount %d, tsdiffbadcount: %d, \n", piana->imagesuccessbadcount, piana->tsdiffbadcount);
			cr_log(piana->hcl, 3, "needrestart %s %d", "Needrestart", __LINE__);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d to Prepared.\n", __LINE__);
			piana->runstate = IANA_RUN_PREPARED;
			piana->runstate_prepared_tick = cr_gettickcount();

			piana->tsdiffbadcount = 0;
			piana->imagesuccessbadcount = 0;
		}

	}
}
#endif

static void ResetAttributeWithRightHanded(iana_t *piana)
{
	U32 camid;

	piana->hscamif    = piana->RS_hscamif;
	piana->hscamifAtC = piana->RS_hscamifAtC;
	piana->hxboard    = piana->RS_hxboard;

	for (camid = 0; camid < NUM_CAM; camid++) {
		piana->pic[camid] = &piana->RS_ic[camid];
	}

	piana->prectLArea	= &piana->RS_rectLArea;
	piana->prectLTee	= &piana->RS_rectLTee;
	piana->prectLIron	= &piana->RS_rectLIron;
	piana->prectLPutter	= &piana->RS_rectLPutter;

	piana->pRoughBunkerMat	= &piana->RS_RoughBunkerMat;
	piana->prectLRough		= &piana->RS_rectLRough;
	piana->prectLBunker		= &piana->RS_rectLBunker;

	piana->pUseAttackCam	= &piana->RS_UseAttackCam;

	piana->pCamPosEstimatedLValid	= &piana->RS_CamPosEstimatedLValid;
	piana->pUseProjectionPlane		= &piana->RS_UseProjectionPlane;
}

static void ResetAttributeWithLeftHanded(iana_t *piana)
{
	U32 camid;

	piana->hscamif    = piana->LS_hscamif;
	piana->hscamifAtC = piana->LS_hscamifAtC;
	piana->hxboard 	  = piana->LS_hxboard;

	for (camid = 0; camid < NUM_CAM; camid++) {
		piana->pic[camid] = &piana->LS_ic[camid];
	}

	piana->prectLArea	= &piana->LS_rectLArea;
	piana->prectLTee	= &piana->LS_rectLTee;
	piana->prectLIron	= &piana->LS_rectLIron;
	piana->prectLPutter	= &piana->LS_rectLPutter;

	piana->pRoughBunkerMat	= &piana->LS_RoughBunkerMat;
	piana->prectLRough		= &piana->LS_rectLRough;
	piana->prectLBunker		= &piana->LS_rectLBunker;

	piana->pUseAttackCam	= &piana->LS_UseAttackCam;

	piana->pCamPosEstimatedLValid	= &piana->LS_CamPosEstimatedLValid;
	piana->pUseProjectionPlane		= &piana->LS_UseProjectionPlane;
}

/*!
 ********************************************************************************
 *	@brief      IANA Change right / left 
 *
 *  @param[in]	piana
 *   			handle.
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2018/0824
 *******************************************************************************/
void iana_changerightleft(iana_t *piana)		
{

	//---------------------------------------------------------------
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "current rightleft: %d,   required rightleft: %d\n", piana->Right0Left1, piana->Right0Left1Required);

	if (piana->useLeftHanded != 0) {
		// Previous..
		piana->runstate = IANA_RUN_EMPTY;
		//piana->runstate = IANA_RUN_POWERSAVING;
		scamif_lock(piana->hscamif,    SCAMIF_LOCK_WRITE_DATAMEMONLY);
		piana->currentcamconf = IANA_CAMCONF_CHANGEME;

		iana_cam_reconfig(piana, 1);		// configure camera..
		
		//---------------------------------------------------------------
		// Change..
		if (piana->Right0Left1Required == 0) {	
			ResetAttributeWithRightHanded(piana);
		} else {
			ResetAttributeWithLeftHanded(piana);
		}

		piana->Right0Left1 = piana->Right0Left1Required;
		piana->runstate = IANA_RUN_EMPTY;
		scamif_lock(piana->hscamif,    SCAMIF_LOCK_NULL);
		piana->currentcamconf = IANA_CAMCONF_CHANGEME;

		iana_cam_reconfig(piana, 1);		// configure camera..

		//--
		piana->NeedChangeRight0Left1 = 0;
	} else {

		piana->Right0Left1 = piana->Right0Left1Required;
		piana->runstate = IANA_RUN_EMPTY;
		piana->currentcamconf = IANA_CAMCONF_CHANGEME;

		iana_cam_reconfig(piana, 1);		// configure camera..
		piana->NeedChangeRight0Left1 = 0;
	}

	return;
}



void iana_changerightleft_LITE(iana_t *piana)	
{

	//---------------------------------------------------------------
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "current rightleft: %d,   required rightleft: %d\n", piana->Right0Left1, piana->Right0Left1Required);


	if (piana->useLeftHanded == 0) 
	{
		piana->NeedChangeRight0Left1 = 0;
		if (piana->opmode != IANA_OPMODE_FILE) {
			piana->Right0Left1 = piana->Right0Left1Required;			/// !!!!!
		}
		goto func_exit;
	}
	
	//---------------------------------------------------------------
	// Change..
	if (piana->Right0Left1Required == 0) {
		ResetAttributeWithRightHanded(piana);
	} else {
		ResetAttributeWithLeftHanded(piana);
	}

	piana->Right0Left1 = piana->Right0Left1Required;

	//--
	piana->NeedChangeRight0Left1 = 0;

func_exit:
	return;
}


I32 iana_check_tsmatch(iana_t *piana)
{
	U32 camid;
	U32 i, j;
	U64 ts640, ts641;
	double tsd0, tsd1;
	double tsddiff;
	camif_buffer_info_t *pb;

	I32 res;
	double periodd;
	double tsshotd;
	I32 minindex;
	double mintsddiff;

	U32 goodindex[MAXCAMCOUNT];
	
	I32 marksequencelen;
	double tsd[MAXCAMCOUNT][MARKSEQUENCELEN];

	//--
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}
	
	periodd = 1.0 / (double)piana->framerate;

	for (i = 0; i < (U32)marksequencelen; i++) {
		piana->bulkpairindex[i] = BULKPAIRINDEX_NONE;
		tsd[0][i] = -99999999;
		tsd[1][i] = -99999999;
	}

	goodindex[0] = goodindex[1] = 0;
	for (camid = 0; camid < NUM_CAM; camid++) {
		double tsdmin;
		tsshotd = (double )(piana->pic[camid]->ts64shot / TSSCALE_D);
		//
		minindex = -1;
		mintsddiff = 999999;
		tsdmin = 9999999;
		for (i = 0; i < (U32)marksequencelen; i++) {
			double diffabs;
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, i, &pb, NULL);
			if (res == 0) {
				continue;
			}
			ts640 = MAKEU64(pb->ts_h, pb->ts_l);			// tsMaster
			tsd0 = (double) (ts640 / TSSCALE_D);

			tsd[camid][i] = tsd0;
			diffabs = tsshotd - tsd0;
			if (diffabs < 0) {
				diffabs = -diffabs;
			}

			if (mintsddiff > diffabs) {
				minindex = i;
				mintsddiff = diffabs;
				tsdmin = tsd0;
			}
		}

		if (minindex >= 0) {
			piana->bulkshotindex[camid] = minindex;
			goodindex[camid] = 1;
		} else {
			piana->bulkshotindex[camid] = piana->pic[camid]->rindexshotbulk;
			goodindex[camid] = 0;		
		}
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] tsshotd : %lf, ts_index: %lf, diff: %lf, index: %d\n",
		//		camid, tsshotd*1000, tsdmin*1000, (tsdmin - tsshotd)*1000, minindex);
	}


	if (goodindex[0] != 0 && goodindex[1] != 0) {
		for (i = 0; i < (U32)marksequencelen; i++) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, 0, NORMALBULK_BULK, i, &pb, NULL);
			if (res == 0) {
				continue;
			}
			ts640 = MAKEU64(pb->ts_h, pb->ts_l);			// tsMaster
			tsd0 = (double) (ts640 / TSSCALE_D);

			minindex = -1;
			mintsddiff = 999999;
			for (j = 0; j < (U32)marksequencelen; j++) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, 1, NORMALBULK_BULK, j, &pb, NULL);
				if (res == 0) {
					continue;
				}
				ts641 = MAKEU64(pb->ts_h, pb->ts_l);			// tsMaster
				tsd1 = (double) (ts641 / TSSCALE_D);
				tsddiff = tsd0 - tsd1;

				if (tsddiff > 0) {
					if (mintsddiff > tsddiff) {
						mintsddiff = tsddiff;
						minindex = j;
					}
				}
			}

			if (mintsddiff > periodd) {
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%d, %d mintsddiff: %lf (ms)\n",
				//		i, minindex,
				//		mintsddiff * 1000);
				minindex = -1;				// fail.
			}
			if (minindex >= 0) {
				piana->bulkpairindex[i] = minindex;
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "cam0 frame %d matches cam1 %d (%lf vs %lf, diff: %lf)\n", 
				//		i, minindex, tsd0, (tsd0-mintsddiff) , mintsddiff * 1000);
			} else {
				piana->bulkpairindex[i] = BULKPAIRINDEX_NONE;
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "cam0 frame %d DONOT have cam1 match..\n");
			}
		}
	}
	for (i = 0; i < (U32)marksequencelen; i++) {
		if (piana->bulkpairindex[i] != BULKPAIRINDEX_NONE) {
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK TS32 %6d == %6d\n", 
			//		i, 
			//		piana->bulkpairindex[i]);
		}
	}


	{
		U32 foundit;
		piana->tsdiff = 0;
		foundit = 0;
		j = 0;
		for (i = 0; i < (U32)marksequencelen; i++) {
			j = piana->bulkpairindex[i];
			if (j != BULKPAIRINDEX_NONE) {
				foundit = 1;
				break;
			}
		}
		if (foundit) {
			piana->tsdiff = tsd[0][i] - tsd[1][j];

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "tsdiff: %lf (%lf)\n", 
					piana->tsdiff, piana->tsdiff * 1000);
		}
	}

	return 1;
}

static I32 NeedSaveImage(iana_t *piana)
{
	I32 saveimagedata;
	
	//------------------------------------
	if (piana->opmode != IANA_OPMODE_FILE) {
		saveimagedata = 1;
	} else {
		saveimagedata = 0;
	}
	
	{
		FILE *fp;
		I32 mode;
		CHR pathT[PATHBUFLEN];
		char path[PATHBUFLEN];

#if defined(_WIN32)
		cr_sprintf(pathT, _T("%s%s%s"), piana->szDrive, piana->szDir, SAVEIMAGEDATA);
#else
		cr_sprintf(pathT, _T("%s%s/%s"), piana->szDrive, piana->szDir, SAVEIMAGEDATA);
#endif
		OSAL_STR_ConvertUnicode2MultiByte(pathT,  path, PATHBUFLEN);

		fp = cr_fopen(path, "r");

		mode = saveimagedata;
		if (fp) {
			fscanf(fp, "%d", &mode);
			cr_fclose(fp);
		}
		saveimagedata = mode;
	}

	return saveimagedata;
}


#if defined(IMAGE_TXT)
static void Update_Image_Txt_File(iana_t *piana, int value)
{
	FILE *fp;

	int len = PATHBUFLEN;
	CHR szDest[PATHBUFLEN];
	char fn[PATHBUFLEN];

#if defined(_WIN32)
	cr_sprintf(szDest, _T("%s\\%s"), piana->szImageDir, IMAGE_TXT);
#else
	cr_sprintf(szDest, _T("%s/%s"), piana->szImageDir, IMAGE_TXT);
#endif
	OSAL_STR_ConvertUnicode2MultiByte(szDest,  fn, len);

	fp = cr_fopen(fn, "w");
	if (fp) {
		if(value == 0) {
			fprintf(fp, "0");
		} else {
			fprintf(fp, "1\n");
		}
		cr_fclose(fp);
	}
}
#endif

#if defined(CLUBPATHTXT)
static void Update_Clubpath_Txt_File(iana_t *piana)
{
	CHR clubpathT[PATHBUFLEN];
	char  clubpath[PATHBUFLEN];
	FILE *fp;
	int drawpathmode;
#if defined(_WIN32)
	cr_sprintf(clubpathT, _T("%s%s%s"), piana->szDrive, piana->szDir, CLUBPATHTXT);
#else
	cr_sprintf(clubpathT, _T("%s%s/%s"), piana->szDrive, piana->szDir, CLUBPATHTXT);
#endif
	OSAL_STR_ConvertUnicode2MultiByte(clubpathT,  clubpath, PATHBUFLEN);

	fp = cr_fopen(clubpath, "r");
	if (fp) {
		fscanf(fp, "%d", &drawpathmode);
		cr_fclose(fp);

		piana->drawpathmode = drawpathmode;
	}
}
#endif

//#define GOODSHOT_SLEEP_TIME	2000
#if defined(GOODSHOT_SLEEP_TIME)
#define GOODSHOT_SLEEP_TXT	"goodshottime.txt"
static void Update_GoodShotTime_Txt_File(void)
{
	FILE *fp;
	U32 mode;
	U32 sleeptime;

	//							sleeptime = GOODSHOT_SLEEP_TIME;
	sleeptime = 0;
	fp = cr_fopen(GOODSHOT_SLEEP_TXT, "r");
	if (fp) {
		mode = 0;
		fscanf(fp, "%d", &mode);
		fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

		if (mode == 1) {
			fscanf(fp, "%d", &sleeptime);
		}
		cr_fclose(fp);
	}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Goodshot sleep.. %d\n", sleeptime);
	cr_sleep(sleeptime);				// XXX
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Goodshot sleep.. AFTER\n");
}

#endif


//#define SAVE_BULK_DATA
#if defined(SAVE_BULK_DATA)
static void SaveBulkImage(iana_t *piana)
{
	U32 i, j;
	I32 res;
	U32 firstbi;
	char filepath[1024];
	U08 *buf;
	camif_buffer_info_t *pb;
	camimageinfo_t     *pcii;
	I32 marksequencelen;

	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}

	//--
	firstbi = piana->pic[0]->firstbulkindex;
	saveimageprepare(&filepath[0]);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[BULK DATA] %s\n", filepath);
	res = 0;
	buf = NULL;
	pb = NULL;

	for (i = 0; i < marksequencelen; i++) {
			for (j = 0; j < 10; j++) {
				res =  scamif_imagebuf_randomaccess(piana->hscamif, 0, NORMALBULK_BULK, i + firstbi, &pb, &buf);
				if (res == 1) {
					break;
				}
				cr_sleep(1);
			}
			if (res == 0) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail..Break\n");
				break;
			}
			pcii = &pb->cii;
			saveimage(&filepath[0], (char *)buf, pcii->width, pcii->height, 0, i);
		}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Done\n");
	}
#endif


static void Write_ShotInfo_Json_File(char *fn, iana_t *piana, double spinmag2d, double spinaxis2d, U32 impactimageExist, camif_buffer_info_t *pb)
{
	FILE *fp;
	iana_shotresult_t *psr;
	U32 camidImpact;
	camimageinfo_t     *pcii;

	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		camidImpact = 1;		
		psr = &piana->shotresultdata2;
	} else if (piana->camsensor_category == CAMSENSOR_P3V2) {
		camidImpact = 0;					
		psr = &piana->shotresultdata;
	} else {
		camidImpact = 1;	
		psr = &piana->shotresultdata;
		
	}	

	if (impactimageExist == 1) {	
		pcii = &pb->cii;
	} else {	//		impactimageExist = 0;
		pcii = NULL;
	}	

	fp = cr_fopen(fn, "w");
	if (fp) {
		fprintf(fp, "{\n");	
		{
			fprintf(fp, "\t\"DATA\": {\n"); 
			{
				fprintf(fp, "\t\t\"ballspeed\": \"%10.4f\",\n", (float)psr->vmag); 
				fprintf(fp, "\t\t\"incline\": \"%10.4f\",\n", (float)psr->incline); 
				fprintf(fp, "\t\t\"azimuth\": \"%10.4f\",\n", (float)psr->azimuth); 
				fprintf(fp, "\t\t\"backspin\": \"%10.4f\",\n", (float)psr->backspin); 
				fprintf(fp, "\t\t\"sidespin\": \"%10.4f\",\n", (float)psr->sidespin); 
				fprintf(fp, "\t\t\"spinmag2d\": \"%10.4f\",\n", (float)spinmag2d); 
				fprintf(fp, "\t\t\"spinaxis2d\": \"%10.4f\",\n", (float)spinaxis2d); 

				// Club data
				fprintf(fp, "\t\t\"clubspeed\": \"%10.4f\",\n", (float)psr->clubspeed_B); 
				fprintf(fp, "\t\t\"Assurance_clubspeed\": \"%d\",\n", psr->Assurance_clubspeed_B); 

				fprintf(fp, "\t\t\"clubpath\": \"%10.4f\",\n", (float)psr->clubpath); 
				fprintf(fp, "\t\t\"Assurance_clubpath\": \"%d\",\n", psr->Assurance_clubpath); 

				fprintf(fp, "\t\t\"clubfaceangle\": \"%10.4f\",\n", (float)psr->clubfaceangle); 
				fprintf(fp, "\t\t\"Assurance_clubfaceangle\": \"%d\",\n", psr->Assurance_clubfaceangle); 

				fprintf(fp, "\t\t\"clubattackangle\": \"%10.4f\",\n", (float)psr->clubattackangle); 
				fprintf(fp, "\t\t\"Assurance_clubattackangle\": \"%d\",\n", psr->Assurance_clubattackangle); 

				fprintf(fp, "\t\t\"clubloftangle\": \"%10.4f\",\n", (float)psr->clubloftangle); 
				fprintf(fp, "\t\t\"Assurance_clubloftangle\": \"%d\",\n", psr->Assurance_clubloftangle); 

				fprintf(fp, "\t\t\"clublieangle\": \"%10.4f\",\n", (float)psr->clublieangle); 
				fprintf(fp, "\t\t\"Assurance_clublieangle\": \"%d\",\n", psr->Assurance_clublieangle); 

				fprintf(fp, "\t\t\"clubfaceimpactLateral\": \"%10.4f\",\n", (float)psr->clubfaceimpactLateral); 
				fprintf(fp, "\t\t\"Assurance_clubfaceimpactLateral\": \"%d\",\n", psr->Assurance_clubfaceimpactLateral); 

				fprintf(fp, "\t\t\"clubfaceimpactVertical\": \"%10.4f\",\n", (float)psr->clubfaceimpactVertical); 
				fprintf(fp, "\t\t\"Assurance_clubfaceimpactVertical\": \"%d\"\n", psr->Assurance_clubfaceimpactVertical); 
			} 	
			fprintf(fp, "\t}\n,");

			fprintf(fp, "\t\"BALLIMPACT\": {\n"); 
			{
				iana_cam_t	*pic;
				iana_cam_param_t 	*picp;
								
				U32 radius;
				I32 offset_x, offset_y;
				I32 xpos, ypos;

				//--
				pic 	= piana->pic[camidImpact];
				picp 	= &pic->icp;		

								
				if (impactimageExist) {
					U64 ts64shot, ts64;
					double td;
					double xposd, yposd, radiusd;
					double ma[3];
					I32 xx, yy;

					offset_x= pcii->offset_x;
					offset_y= pcii->offset_y;

					ts64shot 	= pic->ts64shot;
					ts64 		= MAKEU64(pb->ts_h, pb->ts_l);	
					td 			= ((I64)(ts64 - ts64shot)) / TSSCALE_D;

					memcpy(&ma[0], &picp->maPx[0], sizeof(double)*3);
					xposd = ma[2]*td*td + ma[1]*td + ma[0];
					memcpy(&ma[0], &picp->maPy[0], sizeof(double)*3);
					yposd = ma[2]*td*td + ma[1]*td + ma[0];
					memcpy(&ma[0], &picp->maPr[0], sizeof(double)*3);
					radiusd   = ma[2]*td*td + ma[1]*td + ma[0];

					xpos = (I32) (xposd - offset_x);
					if (xpos < 0) xpos = 0;
					ypos = (I32) (yposd - offset_y);
					if (ypos < 0) ypos = 0;
					radius = (U32) (radiusd);

					// Transpose and H-flip.
					xx = pcii->height - ypos - 1;
					yy = xpos;

					xpos = xx;
					ypos = yy;
				} else {
					xpos = 0;
					ypos = 0;
					radius = 0;
				}

				fprintf(fp, "\t\t\"valid\": \"%d\",\n", impactimageExist);
				fprintf(fp, "\t\t\"name\": \"%s\",\n", BALLIMPACTFILE);
				fprintf(fp, "\t\t\"xpos\": \"%d\",\n", xpos);
				fprintf(fp, "\t\t\"ypos\": \"%d\",\n", ypos);
				fprintf(fp, "\t\t\"radius\": \"%d\"\n", radius);
			}
			fprintf(fp, "\t}\n");
		}
		fprintf(fp, "}\n");

		cr_fclose(fp);
	}
}


static void SaveClubImageFiles(iana_t *piana, U32 impactimageExist, U08 	*buf, camimageinfo_t     *pcii)
{
	int len = PATHBUFLEN;
	char dirpath0[PATHBUFLEN];
	char dirpath[PATHBUFLEN];
	char pathstr[PATHBUFLEN];
	char drivestr[PATHBUFLEN];
	char filename[PATHBUFLEN];
	int width;
	int height;
	
	iana_cam_t	*pic;
	
	//--
	pic 		= piana->pic[0];
	width = piana->clubball_shotresultdata.width;
	height = piana->clubball_shotresultdata.height;
	
	OSAL_STR_ConvertUnicode2MultiByte(piana->szDrive,  drivestr, len);
	OSAL_STR_ConvertUnicode2MultiByte(piana->szDir,  pathstr, len);

#if defined(_WIN32)
	sprintf(dirpath0, "%s%s%s", drivestr, pathstr, "SCAMIMG\\");
	sprintf(dirpath, "%s%s", dirpath0, "CURRENT\\");
#else
	sprintf(dirpath0, "%s%s/%s", drivestr, pathstr, "SCAMIMG/");
	sprintf(dirpath, "%s%s", dirpath0, "CURRENT/");
#endif

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "drivestr: %s\n", drivestr);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "pathstr:	%s\n", pathstr);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "dirpath0: %s\n", dirpath0);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "dirpath:	%s\n", dirpath);
	
	cr_sprintf(piana->szImageDir, _T("%s%s"), piana->szImageDirRoot, _T("CURRENT"));
	OSAL_STR_ConvertUnicode2MultiByte(piana->szImageDir,  pathstr, len);
	
	if (g_datapath.mode == 1 || g_datapath.mode == 2 ) {
		OSAL_STR_ConvertUnicode2MultiByte(piana->szImageDirRoot,	 dirpath0,	len);	// SCAMIMG
		OSAL_STR_ConvertUnicode2MultiByte(piana->szImageDir,		 dirpath,	len);	// CURRENT
	}
	
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "g_datapath.mode: %d\n", g_datapath.mode);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " --> dirpath:  %s\n", dirpath);
	if (piana->clubball_shotresultdata.valid) {
		sprintf(filename, "%s.jpg", CLUBFILENAME);
	
	
		iana_spin_clubimage_overlap(piana, 0);			// 2: Topview..
		saveimageJpegBGR(piana->hjpeg, 
							CLUBFILEQVALUE,
							dirpath,
							filename,
							(char *)pic->pimgClubOverlap,
							width, 
							height);
	}
#if defined(_WIN32)
#pragma warning(disable:4456)
#endif
	if (impactimageExist) {
#ifdef IPP_SUPPORT
		U32 width, height;
		IppiSize roisize;
		U32 widthT, heightT;
		IppiSize roisizeT;
		U08 *pimg, *pimgT;
		U08 *pimgMV, *pimgMH;
	
		//--
	
		width = pcii->width;
		height = pcii->height;
		roisize.width = width;
		roisize.height = height;
		pimg = buf;
	
		widthT = height;
		heightT = width;
		roisizeT.width = widthT;
		roisizeT.height = heightT;
	
		pimg	= pic->pimg[0];
		pimgT	= pic->pimg[1];
		pimgMV	= pic->pimg[2];
		pimgMH	= pic->pimg[3];
	
	
		if (piana->camsensor_category == CAMSENSOR_P3V2) {
			memcpy(pimg, buf, width*height);
		}
		else {
			gamma_correction(buf, pimg, width, height);
		}


		ippiTranspose_8u_C1R(
							pimg, width,
							pimgT, widthT,
							roisize);
	
		ippiMirror_8u_C1R(
							pimgT, widthT,
							pimgMV, widthT,
							roisizeT, ippAxsVertical);
	
		ippiMirror_8u_C1R(
							pimgT, widthT,
							pimgMH, widthT,
							roisizeT, ippAxsHorizontal);
	
		if (piana->himgfunc) {
			((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimg, 		width,	height,  0, 1, 2, 3, piana->imguserparam);
			((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimgT,		widthT, heightT, 1, 1, 2, 3, piana->imguserparam);
			((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimgMV,		widthT, heightT, 2, 1, 2, 3, piana->imguserparam);
			((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimgMH,		widthT, heightT, 3, 1, 2, 3, piana->imguserparam);
		}
	
		saveimageJpeg2(
					piana->hjpeg, 
					CLUBFILEQVALUE,
					dirpath,
					BALLIMPACTFILE,
					(char *)pimgMH,
					widthT, heightT);
#else

        U32 width, height;
        U32 widthT, heightT;

        U08 *pimg, *pimgT;
        U08 *pimgMV, *pimgMH;

        //--
        cv::Mat cvImgSrc(cv::Size(pcii->width, pcii->height), CV_8UC1, pic->pimg[0]),
            cvImgTrans, cvImgMirrorVert, cvImgMirrorHoriz;

        width = pcii->width;
        height = pcii->height;

        widthT = height;
        heightT = width;

        pimg	= pic->pimg[0];
        pimgT	= pic->pimg[1];
        pimgMV	= pic->pimg[2];
        pimgMH	= pic->pimg[3];

		if (piana->camsensor_category == CAMSENSOR_P3V2) {
			memcpy(pimg, buf, width*height);
		}
		else {
			gamma_correction(buf, pimg, width, height);
		}

		
        cv::transpose(cvImgSrc, cvImgTrans);

        cv::flip(cvImgTrans, cvImgMirrorVert, 1);

        cv::flip(cvImgTrans, cvImgMirrorHoriz, 0);

        memcpy(pimgT, cvImgTrans.data, sizeof(U08) * cvImgTrans.cols * cvImgTrans.rows);
        memcpy(pimgMV, cvImgMirrorVert.data, sizeof(U08) * cvImgMirrorVert.cols * cvImgMirrorVert.rows);
        memcpy(pimgMH, cvImgMirrorHoriz.data, sizeof(U08) * cvImgMirrorHoriz.cols * cvImgMirrorHoriz.rows);

        if (piana->himgfunc) {
            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimg, width, height, 0, 1, 2, 3, piana->imguserparam);
            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgT, widthT, heightT, 1, 1, 2, 3, piana->imguserparam);
            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgMV, widthT, heightT, 2, 1, 2, 3, piana->imguserparam);
            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgMH, widthT, heightT, 3, 1, 2, 3, piana->imguserparam);
        }

        saveimageJpeg2(
            piana->hjpeg,
            CLUBFILEQVALUE,
            dirpath,
            BALLIMPACTFILE,
            (char *)pimgMH,
            widthT, heightT);
#endif		
		}
#if defined(_WIN32)
#pragma warning(default:4456)
#endif
}

static U32 NeedCallback(iana_t *piana)
{
	FILE *fp;
	U32 docallbackflag = 1;

	fp = fopen(VLIMITFILE, "r");
	if (fp) {
		int mode;
	
		//--
		mode = 0;
		fscanf(fp, "%d", &mode);
		fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
		if (mode) {
			double vmin_tee, vmax_tee;
			double vmin_iron, vmax_iron;
			double vmin_putter, vmax_putter;
			double vmin, vmax;
			double vmag;
	
			//---
			fscanf(fp, "%lf %lf", &vmin_tee, &vmax_tee);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
	
			fscanf(fp, "%lf %lf", &vmin_iron, &vmax_iron);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
	
			fscanf(fp, "%lf %lf", &vmin_putter, &vmax_putter);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
	
			if (piana->processarea == IANA_AREA_TEE) {
				vmin = vmin_tee; 
				vmax = vmax_tee;
			} else if (piana->processarea == IANA_AREA_IRON) {
				vmin = vmin_iron;
				vmax = vmax_iron;
			} else if (piana->processarea == IANA_AREA_PUTTER) {
				vmin = vmin_putter;
				vmax = vmax_putter;
			} else {
				vmin = -999;   // -_-
				vmax = 999;    // 
			}
	
			vmag = piana->shotresultdata.vmag;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "VMAGLIMIT.  vmin: %lf vmax: %lf, processarea: %d\n", vmin, vmax, piana->processarea);
			if (vmag < vmin || vmag > vmax) {
				docallbackflag = 0;
			}
		}
		fclose(fp);
	}
	return docallbackflag;
}



I32 iana_do_goodshot(iana_t *piana)
{
	I32 res;
	I32 saveimagedata;
	U32 i;
	U32 docallbackflag;

	saveimagedata = NeedSaveImage(piana);
	//--
	piana->runstate = IANA_RUN_GOODSHOT;

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "saveresult #1\n");
	// Make directory...
	cr_wmkdir(piana->szImageDirRoot);
	cr_wmkdir(piana->szImageDir);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "saveresult #2\n");

#if defined(IMAGE_TXT)
	//__debugbreak();
	if (saveimagedata){
		Update_Image_Txt_File(piana, 0);
	}
#endif

	docallbackflag = 1;
	if (piana->camsensor_category == CAMSENSOR_P3V2 && piana->opmode != IANA_OPMODE_FILE) {
		docallbackflag = NeedCallback(piana);
	}
	
	if (docallbackflag) {
		iana_do_callback(piana, IANA_SENSOR_STATE_GOODSHOT);
	}


	if (saveimagedata) {
#if defined(_WIN32)	// TODO: depricated after verifying new code	
		I32 tryiter;
		I32 dogood;
		
#define TRYITER	10
		for (tryiter = 0; tryiter < TRYITER; tryiter++) {
			dogood = 0;
			__try {
				int success;
				SHFILEOPSTRUCTW op = {0};
				CHR szDest[PATHBUFLEN];

				memset(szDest, 0, sizeof(szDest));
				op.wFunc = FO_DELETE;
				cr_sprintf(szDest, _T("%s\\*.jpg"), piana->szImageDir);
				//cr_sprintf(szDest, _T("%s\\a.jpg\0\0"), piana->szImageDir);
				op.pFrom = szDest;
				op.fFlags = FOF_FILESONLY | FOF_SILENT | FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_NORECURSION;
				//							SHFileOperationW(&op);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "saveresult #3\n");

				success = SHFileOperation(&op);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "success: %d\n", success);
				dogood = 1;
			} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
					|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
					|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
					|| GetExceptionCode()== EXCEPTION_FLT_DIVIDE_BY_ZERO
					){
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"(%d) : __except !!!\n", __LINE__);
			}
			if (dogood) {
				break;
			}
		}
#else
		CHR szDest[PATHBUFLEN];
		memset(szDest, 0, sizeof(szDest));
#if defined(_WIN32)		
		cr_sprintf(szDest, _T("%s\\*.jpg"), piana->szImageDir);
#else
		cr_sprintf(szDest, _T("%s/*.jpg"), piana->szImageDir);	
#endif
		OSAL_FILE_DeleteFiles(szDest, CR_TRUE);
#endif
	}

	if (piana->opmode == IANA_OPMODE_FILE) {
		piana->needrestart = 0;
		piana->needrestart2 = 0;
	}
	//#define SAVE_BULK_DATA
#if defined(SAVE_BULK_DATA)
	SaveBulkImage(piana);
#endif

	if (g_ccam_lite) {
		extern U32 IANA_SHOT_RefineBallPos_Bulk(iana_t *piana, U32 camid);
		res = IANA_SHOT_RefineBallPos_Bulk(piana, 0 /*camid*/);
		res = iana_spin_clubpath(piana, NORMALBULK_BULK);
	}


	if (saveimagedata) {
		U32 camidImpact;
		U08 	*buf;
		camif_buffer_info_t *pb;
		camimageinfo_t     *pcii;
		U32 impactimageExist;

		//-
		if (piana->camsensor_category == CAMSENSOR_P3V2) {
			iana_check_tsmatch(piana);
		}

#if defined(CLUBPATHTXT)
		Update_Clubpath_Txt_File(piana);
#endif
		//#define DRAWPATH_BUNGABUNGA
#if defined(DRAWPATH_BUNGABUNGA)
		piana->drawpathmode = 3;
#endif

		{
			int len = PATHBUFLEN;
			CHR fnT[PATHBUFLEN];
			char fn[PATHBUFLEN];
//			FILE *fp;
			double spinmag2d;
			double spinaxis2d;
			iana_shotresult_t *psr;

			U32 indexshot;

			
			//--

			if (piana->camsensor_category == CAMSENSOR_P3V2) {
				camidImpact = 0;
			} else {
				camidImpact = 1;
			}
			indexshot = piana->bulkshotindex[1];
			pb = NULL; buf = NULL;
			res = scamif_imagebuf_randomaccess(piana->hscamif, camidImpact, NORMALBULK_BULK, indexshot, &pb, &buf);
			if (res) {
				impactimageExist = 1;
				pcii = &pb->cii;
			} else {
				impactimageExist = 0;
				pcii = NULL;
			}

			if (piana->camsensor_category == CAMSENSOR_EYEXO) {
				psr = &piana->shotresultdata2;
			} else {
				psr = &piana->shotresultdata;
			}
			spinmag2d 	= psr->backspin * psr->backspin + psr->sidespin * psr->sidespin;
			spinmag2d 	= sqrt(spinmag2d);
			spinaxis2d 	= atan2(psr->sidespin, psr->backspin);	
			spinaxis2d 	= spinaxis2d *180.0 / 3.141592;
#if defined(_WIN32)					
			cr_sprintf(fnT, _T("%s\\%s"), piana->szImageDir, IMAGEINFO_XML);
#else
			cr_sprintf(fnT, _T("%s/%s"), piana->szImageDir, IMAGEINFO_XML);
#endif
			OSAL_STR_ConvertUnicode2MultiByte(fnT,  fn, len);
			cr_nanoxml_create2(fn, 0);
			nxmlie(0, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			nxmlie(1, "<IMAGEINFO>");

			{
				nxmlie(1, "<shotresult>");

				IANA_INFO_WriteShotResultToXml(psr, spinmag2d, spinaxis2d);
				
				nxmlie(-1, "</shotresult>");
			}
#if defined(_WIN32)					
			cr_sprintf(fnT, _T("%s\\%s"), piana->szImageDir, SHOTINFO_JSON);
#else
			cr_sprintf(fnT, _T("%s/%s"), piana->szImageDir, SHOTINFO_JSON);
#endif
			OSAL_STR_ConvertUnicode2MultiByte(fnT,  fn, len);

			Write_ShotInfo_Json_File(fn, piana, spinmag2d, spinaxis2d, impactimageExist, pb);
		}
		//if (piana->camsensor_category == CAMSENSOR_CATEGORY) 
		if (piana->processarea != IANA_AREA_PUTTER)
		{
			SaveClubImageFiles(piana, impactimageExist, buf, pcii);

			iana_spin_mark_spin_check(piana);
		}

	}

	if (saveimagedata) {
#if defined(_WIN32)		
		__try {
#endif		
			if (piana->processarea != IANA_AREA_PUTTER) {
				iana_clubimage_fit(piana);		// save and frame..
				iana_spin_ballclub_check(piana);
			} else {
				iana_putter_check(piana);
			}
#if defined(_WIN32)					
		} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
				|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
				|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
				|| GetExceptionCode()== EXCEPTION_FLT_DIVIDE_BY_ZERO
				){
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"(%d) : __except !!!\n", __LINE__);
		}
#endif
		nxmlie(-1, "</IMAGEINFO>");
		cr_nanoxml_delete(NULL, 0);
	}


	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV GOODSHOT.\n");
	if (piana->opmode != IANA_OPMODE_FILE) {
		iana_run_needrestart(piana);
		piana->currentcamconf = IANA_CAMCONF_CHANGEME;
	}

	if (saveimagedata) {
		CHR szSrc[PATHBUFLEN];
		CHR szDest[PATHBUFLEN];
#if 1 && defined(_WIN32)
		SHFILEOPSTRUCT s = { 0 };
#endif

		//-- Delete '9' Directory
		cr_sprintf(szSrc, _T("%s%d"), piana->szImageDirRoot, 9);
#if defined(_WIN32)	// TODO: depricated after verifying new code	
		__try { 


			HRESULT hr;

			hr = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED | COINIT_DISABLE_OLE1DDE);
			if (SUCCEEDED(hr)) {
				IFileOperation *pfo;
				hr = CoCreateInstance(CLSID_FileOperation, NULL, CLSCTX_ALL, IID_PPV_ARGS(&pfo));
				if (SUCCEEDED(hr)) {
					DWORD flags = FOF_SILENT | FOF_NOCONFIRMATION | FOF_NOERRORUI;

					hr = pfo->SetOperationFlags(flags);
					if (SUCCEEDED(hr)) {
						IShellItem *psiFrom = NULL;
						hr = SHCreateItemFromParsingName(szSrc, NULL, IID_PPV_ARGS(&psiFrom));

						if (SUCCEEDED(hr)) {
							hr = pfo->DeleteItem(psiFrom, NULL);
						}
						if (psiFrom != NULL) {
							psiFrom->Release();
						}
						hr = pfo->PerformOperations();
					}
					pfo->Release();
				}
			}
			CoUninitialize();

		} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION)
		{
			//
		}
#else
		OSAL_FILE_DeleteFiles(szSrc, CR_FALSE);
#endif

#if defined(_WIN32)	// TODO: depricated after verifying new code	
		__try { 
			for (i = 9; i >= 1; i--) {
				//							cr_wmkdir(piana->szImageDirRoot);
				cr_sprintf(szSrc,  _T("%s%d"), piana->szImageDirRoot, i-1);
				cr_sprintf(szDest, _T("%s%d"), piana->szImageDirRoot, i);				
				//MoveFile(szSrc, szDest);
				MoveFileEx(szSrc, szDest, MOVEFILE_COPY_ALLOWED | MOVEFILE_REPLACE_EXISTING);

			}
		} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION)
		{
			//
		}
#else
		for (i = 9; i >= 1; i--) {
			cr_sprintf(szSrc,  _T("%s%d"), piana->szImageDirRoot, i-1);
			cr_sprintf(szDest, _T("%s%d"), piana->szImageDirRoot, i);						
			OSAL_FILE_Move(szDest,szSrc );
		}	
#endif		

		cr_sprintf(szDest, _T("%s%d"), piana->szImageDirRoot, 0);

#if defined(_WIN32)	// TODO: depricated after verifying new code	
		//-- Copy Current Directory to '0' Directory
		s.hwnd = NULL;
		s.wFunc = FO_COPY;
		s.pTo =	 szDest;
		s.pFrom = piana->szImageDir;
		s.fFlags = FOF_SILENT;
		{
			I32 tryiter;
			I32 dogood;

			for (tryiter = 0; tryiter < TRYITER; tryiter++) {
				dogood = 0;	
				__try {

					SHFileOperation( &s );

					cr_wmkdir(piana->szImageDir);	// TODO:  is this code line really needed??
					dogood = 1;
				} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
						|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
						|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
						|| GetExceptionCode()== EXCEPTION_FLT_DIVIDE_BY_ZERO
						){
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"(%d) : __except !!!\n", __LINE__);
				}
				if (dogood) {
					break;
				}
			}
		}
#else
		OSAL_FILE_Copy(szDest, piana->szImageDir);
#endif

	}

		
#if defined(IMAGE_TXT)
	if (saveimagedata) {
		Update_Image_Txt_File(piana, 1);
	}
#endif
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Image save done.\n");


#if defined(GOODSHOT_SLEEP_TIME)
	Update_GoodShotTime_Txt_File();
#endif

	res = 1;
	return res;
}


I32 iana_do_callback(iana_t *piana, U32 status)
{
	I32 res;

	if (piana->activated == 0) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Activated: %d, discard callback.\n", 
				piana->activated);
		res = 0;
		goto func_exit;
	}

	if (piana->cbfunc1_id == 0) {
		if (piana->hcbfunc) {
			//		U32 status;
			IANA_shotdata_t sd;
			iana_shotresult_t *psr;
			iana_shotresult_t *psr2;


			//-------------------------------
			psr  = NULL;
			psr2 = NULL;
			if (piana->camsensor_category == CAMSENSOR_EYEXO) {
				psr  = &piana->shotresultdata;
				psr2 = &piana->shotresultdata2;
			} else {
				psr  = &piana->shotresultdata;
			}
			//		status = IANA_SENSOR_STATE_GOODSHOT;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "1.. vmag: %lf\n", psr->vmag);
			sd.ballspeedx1000 	= (int) (psr->vmag * 1000);
			//sd.clubspeed_Bx1000 	= 0;
			//sd.clubspeed_Ax1000 	= 0;
			//sd.clubpathx1000 		= 0;
			//sd.clubfaceanglex1000	= 0;
			sd.clubspeed_Bx1000 	= (int) (psr->clubspeed_B * 1000);
			sd.clubspeed_Ax1000 	= (int) (psr->clubspeed_A * 1000);
			sd.clubpathx1000 		= (int) (psr->clubpath * 1000);
			sd.clubfaceanglex1000	= (int) (psr->clubfaceangle * 1000);


			sd.sidespin 			= (int) (psr->sidespin);
			sd.backspin 			= (int) (psr->backspin);
			sd.azimuthx1000 		= (int) (psr->azimuth * 1000);
			sd.inclinex1000 		= (int) (psr->incline * 1000);

			sd.axisx				= psr->axis.x;
			sd.axisy				= psr->axis.y;
			sd.axisz				= psr->axis.z;
			sd.spinmag				= psr->spinmag;
			if (piana->camsensor_category == CAMSENSOR_EYEXO) {
				sd.sidespin 			= (int) (psr2->sidespin);
				sd.backspin 			= (int) (psr2->backspin);

				sd.axisx				= psr2->axis.x;
				sd.axisy				= psr2->axis.y;
				sd.axisz				= psr2->axis.z;
				sd.spinmag				= psr2->spinmag;
			}

			{
//#define SHOTTXT		"zshot.txt"
#if defined(SHOTTXT)
				{
					FILE *fp;
					U32 mode;
					fp = cr_fopen(SHOTTXT, "r");
					if (fp) {
						mode = 0;
						fscanf(fp, "%d", &mode);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

						if (mode == 1) {
							double ballspeed;
							double clubspeed_A;
							double clubspeed_B;
							double clubpath;
							double clubfaceangle;
							double sidespin;
							double backspin;
							double azimuth;
							double incline;

							//--
							fscanf(fp, "%lf", &ballspeed);
							fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

							fscanf(fp, "%lf", &clubspeed_A);
							fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

							fscanf(fp, "%lf", &clubspeed_B);
							fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

							fscanf(fp, "%lf", &clubpath);
							fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

							fscanf(fp, "%lf", &clubfaceangle);
							fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

							fscanf(fp, "%lf", &sidespin);
							fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

							fscanf(fp, "%lf", &backspin);
							fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

							fscanf(fp, "%lf", &azimuth);
							fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

							fscanf(fp, "%lf", &incline);
							fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

							//-----------
							sd.ballspeedx1000 		= (I32) (ballspeed * 1000);
							sd.clubspeed_Bx1000 	= (I32) (clubspeed_B * 1000);
							sd.clubspeed_Ax1000 	= (I32) (clubspeed_A * 1000);
							sd.clubpathx1000 		= (I32) (clubpath * 1000);
							sd.clubfaceanglex1000 	= (I32) (clubfaceangle * 1000);
							sd.sidespin 			= (I32) (sidespin);
							sd.backspin 			= (I32) (backspin);
							sd.azimuthx1000 		= (I32) (azimuth * 1000);
							sd.inclinex1000 		= (I32) (incline * 1000);
						}
						cr_fclose(fp);
					}
				}
#endif

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Before shot callback \n");

				((IANA_CALLBACKFUNC*)(piana->hcbfunc))(
				(HAND) piana,
				status,
				&sd,
				piana->userparam);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "After  Shot CallBack \n");
			}
		} 		// if (piana->hcbfunc) 
	} 	// if (piana->cbfunc1_id == 0) 

	if (piana->cbfunc1_id == 1) {
		if (piana->hcbfunc1) {
			IANA_shotdataEX1_t	sdEX1, *psdEX1;
			iana_shotresult_t *psr;

			//--
			psdEX1 = &sdEX1;
			memset(psdEX1, 0, sizeof(IANA_shotdataEX1_t));
			if (piana->camsensor_category == CAMSENSOR_EYEXO) {
				psr = &piana->shotresultdata2;
			} else {
				psr  = &piana->shotresultdata;
			}
			memcpy((char *)&psdEX1->shotguid, (char *) &piana->shotguid, sizeof(CR_GUID));

			psdEX1->category = piana->camsensor_category;
			psdEX1->rightlefthanded = piana->Right0Left1;

			{
				cr_point_t *pb3d;
				iana_cam_t	*pic;

				pic = piana->pic[0];			// 
				pb3d = &pic->icp.b3d;			// ball 3d position
				psdEX1->xposx1000 = (int)(pb3d->x * 1000);	// Shot ball x position [mm]
				psdEX1->yposx1000 = (int)(pb3d->y * 1000);	// Shot ball y position [mm]
				psdEX1->zposx1000 = (int)(pb3d->z * 1000);	// Shot ball z position [mm]
			}

			//--
			psdEX1->ballspeedx1000 	= (int) (psr->vmag * 1000);				// ball speed. [m/s] * 1000 = [mm/s]
			psdEX1->inclineX1000	= (int) (psr->incline * 1000);			// ball incline. [degree] * 1000
			psdEX1->azimuthX1000	= (int) (psr->azimuth * 1000);			// ball azimuth. [degree] * 1000

			{
				int spincalc_method;			// Spin calc. method.  0: Using Ball/Club path. 1: Ball Marking based. 2: Dimple based.

				if (piana->camsensor_category == CAMSENSOR_EYEXO) {
					spincalc_method = 2;		// 2: Dimple based.
				} else if (piana->camsensor_category == CAMSENSOR_P3V2) {
					spincalc_method = 0;		// 0: Using Ball/Club path
				} else {
					spincalc_method = 1;		// 1: Ball Marking based
				}
				psdEX1->spincalc_method = spincalc_method;
			}

			psdEX1->assurance_spin = 90;		// Spin calcuration assurance. 0-100

			psdEX1->backspinX1000 = (int) (psr->backspin*1000);				// backspin. [rpm] * 1000
			psdEX1->sidespinX1000 = (int) (psr->sidespin*1000);				// sidespin. [rpm] * 1000
			psdEX1->rollspinX1000 = (int) (psr->rollspin*1000);				// rollspin. [rpm] * 1000

			{
				int clubcalc_method;			// club calc. method.  0: marking-less method, 1: another marking-less method, 2: bar-marking, 3: bar-marking and dot-marking
				if (piana->camsensor_category == CAMSENSOR_EYEXO) {
					clubcalc_method = 2;		// 3: bar-marking and dot-marking
				} else if (piana->camsensor_category == CAMSENSOR_P3V2) {
					clubcalc_method = 0;		// 1: another marking-less method
				} else {
					clubcalc_method = 1;		// 0: marking-less method,
				}
				psdEX1->clubcalc_method = clubcalc_method;
			}

			psdEX1->assurance_clubspeed 			= psr->Assurance_clubspeed_B;		
			psdEX1->assurance_clubpath 				= psr->Assurance_clubpath;
			psdEX1->assurance_faceangle 			= psr->Assurance_clubfaceangle;
			psdEX1->assurance_attackangle 			= psr->Assurance_clubattackangle;
			psdEX1->assurance_loftangle 			= psr->Assurance_clubloftangle;
			psdEX1->assurance_lieangle 				= psr->Assurance_clublieangle;
			psdEX1->assurance_faceimpactLateral		= psr->Assurance_clubfaceimpactLateral;
			psdEX1->assurance_faceimpactVertical 	= psr->Assurance_clubfaceimpactVertical;


			psdEX1->clubspeedX1000				= (int) (psr->clubspeed_B * 1000);			//  club speed. [m/s] * 1000
			psdEX1->clubpathX1000				= (int) (psr->clubpath * 1000);				//  club path angle. [degree] * 1000
			psdEX1->faceangleX1000				= (int) (psr->clubfaceangle * 1000);		//  face angle. [degree] * 1000
			psdEX1->attackangleX1000			= (int) (psr->clubattackangle * 1000);		//  attack angle. [degree] * 1000
			psdEX1->loftangleX1000				= (int) (psr->clubloftangle * 1000);		//  loftangle. [degree] * 1000
			psdEX1->lieangleX1000				= (int) (psr->clublieangle * 1000);			//  lie angle. [degree] * 1000
			psdEX1->faceimpactLateralX1000		= (int) (psr->clubfaceimpactLateral * 1000);	//  horizontal deviation of impact point from club center. [mm]
			psdEX1->faceimpactVerticalX1000	= (int) (psr->clubfaceimpactVertical * 1000);	//  vertical deviation of impact point from club center. [mm]

			{
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Before shot callback1");
				((IANA_CALLBACKFUNC1*)(piana->hcbfunc1))(
					(HAND) piana,
					status,
					(HAND) psdEX1,
					1,
					piana->userparam
				);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "After  Shot CallBack1\n");
			}
		} 		// if (piana->hcbfunc) 
	} 	// if (piana->cbfunc1_id == 1) 




	res = 1;

func_exit:
	return res;
}




#if defined (__cplusplus)
}
#endif


