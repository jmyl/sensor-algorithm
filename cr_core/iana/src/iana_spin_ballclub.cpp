/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA shot using ball club.
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2017 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_spin_ballclub.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2017/04/05 Spin mark

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <math.h>
#include <string.h>

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"
#include "cr_nanoxml.h"

#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#else
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#endif

#include "scamif.h"
#include "scamif_buffer.h"

#include "iana.h"
#include "iana_work.h"
#include "iana_implement.h"
#include "iana_cam.h"
#include "iana_cam_implement.h"
#include "iana_balldetect.h"
#include "iana_shot.h"
#include "iana_tool.h"
#include "iana_coordinate.h"
#include "iana_spin.h"
#include "iana_spin_ballclub.h"
//#include "iana_club_attack.h"

#include "cr_regression.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define CLUB3_TEST

#define USE_GAMMACORRECTION_CLUBCAM

//#define USE_II

//#define USE_TT

#define HEADXOFFSET	20
#define HEADYOFFSET	40
//#define HEADIMGWIDTH	100
//#define HEADIMGHEIGHT	100
#define HEADIMGWIDTH	150
//#define HEADIMGWIDTH	200
#define HEADIMGHEIGHT	150
#define HEADXSEARCH	(HEADXOFFSET * 4)

//#define PRESHOTCOUNT	12	
#define PRESHOTCOUNT	8
#define POSTSHOTCOUNT	16
//#define POSTSHOTCOUNT	8

//#define DISCARDCOUNT 4
//////////#define DISCARDCOUNT 3
#define DISCARDCOUNT 1

#define  CLUBPATHDECIMATIONFILE "clubdecimation.txt"

#if defined(_WIN32)
#pragma warning(disable:4456)
#endif
#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

typedef struct _regressionx {
	//I32 i0;				// Input: from index
	//I32 i1;				// Input: to index
	double td0;				// Input: from index
	double td1;				// Input: to index
	I32 frame0;
	I32 frame1;
	I32 usequadratic;	// Input: 1: Use quadratic regression.  0: linear regressiong
	//I32 recalcshotindex;// Input: 1: recalc shotindexcombined.  0: Use shotindexcombined..
	I32 recalcshotframe;// Input: 1: recalc shotindexcombined.  0: Use shotindexcombined..
	double ma[3];
	double maiitt[3];
	//double shotindexcombined;
	double shotframecombined;
} regressionx_t;



/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

#if defined(USE_GAMMACORRECTION_CLUBCAM)
static int s_use_gammacorrection_clubcam = 1;
#else
static int s_use_gammacorrection_clubcam = 0;
#endif

#if defined(_DEBUG)
static int s_clubHposEst2 = 0;
#else
static int s_clubHposEst2 = 1;
#endif

static int s_hlenminmax = 1;

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/
extern int g_ccam_lite;



/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
I32 iana_spin_clubpath_image(iana_t *piana, U32 camid, 
		U08 *buf, 
		clubpath_context_t *pcpc,
		U08 *diffimg,
		U08 *diffimgFilter,
		U08 *diffimgBinary
		);	// Deprecated

I32 iana_spin_clubpath_cam(iana_t *piana, U32 camid, U32 normalbulk);	// Deprecated
I32 iana_spin_clubpath_cam2(iana_t *piana, U32 camid);	// Deprecated

static I32 ClubPath_UpdateContext(iana_t *piana, U32 camid, clubpath_context_t 	*pcpc);
static I32 ClubPath_CalcPosition(iana_t *piana, U32 camid, clubpath_context_t *pcpc);
static I32 ClubPath_CalcPosition_Hough(iana_t *piana, U32 camid, clubpath_context_t *pcpc);
static I32 ClubPath_CalcPosition_Shape(iana_t *piana, U32 camid, clubpath_context_t *pcpc);
static I32 ClubPath_CalcPosition_RefineShape(iana_t *piana, U32 camid, clubpath_context_t *pcpc);

static I32 ClubPath_FilterPreshot(iana_t *piana, U32 camid, clubpath_context_t *pcpc);
static I32 ClubPath_RefinePosition(iana_t *piana, U32 camid, clubpath_context_t *pcpc);
static I32 ClubPath_RegressPosition(iana_t *piana, U32 camid, clubpath_context_t *pcpc, regressionx_t *prgx);

static I32 ClubPath_CalcSpin(iana_t *piana, U32 camid, clubpath_context_t *pcpc);
static I32 ClubPath_CalcSpin_SideSpin(iana_t *piana, U32 camid, clubpath_context_t *pcpc);
static I32 ClubPath_CalcSpin_BackSpin(iana_t *piana, U32 camid, clubpath_context_t *pcpc);

static I32 ClubPath_CalcRoi(iana_t *piana, U32 camid, clubpath_context_t *pcpc);
static I32 ClubPath_CalcClubData(iana_t *piana, U32 camid, clubpath_context_t *pcpc);

I32 iana_spin_clubpath_filtering(iana_t *piana, U32 camid, clubpath_context_t *pcpc);
double geareffect(double dx, double vb);	// Deprecated


static I32 ClubPath_GetClubHposEst(iana_t *piana, U32 camid, clubpath_context_t *pcpc, U32 index, point_t *pPp, point_t *pLp);
static I32 ClubPath_GetClubHposEst2(iana_t *piana, U32 camid, clubpath_context_t *pcpc, U32 index, point_t *pPp, point_t *pLp);

static I32 DrawClubPath( iana_t *piana, U32 camid, U08 *pimg, U32 isGray, U32 decimation); 		// isGray 1: Gray,   0: BGR
static I32 SaveBallClubData(iana_t *piana, U32 camid);


I32 iana_spin_clubpath_updaterefimage(iana_t *piana, U32 camid, U32 normalbulk, U32 rindex, U08 *refbuf);	// Deprecated



//-------------------------------------------------------------------------------------------------------------------------------------
static I32 GetClubPath3(iana_t *piana, U32 camid, U32 normalbulk);
static I32	makeRef(iana_t *piana, U32 camid, iana_cam_t *pic, U32 width, U32 height, U32 normalbulk, U32 multitude, I32 r0, I32 r1);
static I32 ClubPath_ProcessImage3(iana_t *piana, U32 camid, 
		U08 *buf, 
		clubpath_context_t *pcpc,
		U08 *diffimg,
		U08 *diffimgFilter,
		U08 *diffimgBinary
		);

//-------------------------------------------------------------------------------------------------------------------------------------

/*!
 ********************************************************************************
 *	@brief      CAM Get Club Path
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2017/0405
 *******************************************************************************/
I32 iana_spin_clubpath(iana_t *piana, U32 normalbulk)
{
	I32 res;
	U32 camid;
	iana_cam_t	*pic;
	U32 right0left1;

	res = 0;

	right0left1 = piana->Right0Left1;

	if (piana->opmode == IANA_OPMODE_FILE) {
		piana->Right0Left1 = piana->info_right0left1;
	}
	for (camid = 0; camid < NUM_CAM; camid++) {
		pic = piana->pic[camid];
		if (piana->processmode[camid] == CAMPROCESSMODE_BALLCLUB) {
			res = GetClubPath3(piana, camid, normalbulk);
			if (res) {
				memcpy(&piana->clubball_shotresultdata, &piana->clubball_shotresult[camid], sizeof(iana_shotresult_t));
			}
		}
	}
	if (piana->opmode == IANA_OPMODE_FILE) {
		piana->Right0Left1 = right0left1;
	}

	return res;
}



/*!
 ********************************************************************************
 *	@brief      CAM Get Club Path, 3rd.
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2019/0906
 *******************************************************************************/
static I32 GetClubPath3(iana_t *piana, U32 camid, U32 normalbulk)	
{
#ifdef IPP_SUPPORT
	I32 res;

	iana_cam_t	*pic;
	iana_cam_param_t *picp;
	camimageinfo_t     *pcii;
	camif_buffer_info_t *pb;
	
	U32	width;
	U32	height;
	U32	offset_x;
	U32	offset_y;
	U08	*buf;
	U32	rindex;
	U32 multitude;

	U08 *bufimg;
	U08 *diffimg;
	U08 *diffimgFilter;
	U08 *diffimgBinary;

	U08 *procimg;
	I32 j;
	I32 index;
	U32 m;
	U32 frame;

	clubpath_context_t 	*pcpc;
//	U32 updateRefImage;

	I32 r0, r1;
	I32 refr0, refr1, refr2;

	U08 *bufgamma;
	
	U64 ts64, ts64prev;
	U64 ts64Mt;
	U32 indexprev;
	U32 mprev;
	U32 frameprev;
	
	U64 periodm;

	//-
	IppiSize ssize;
	IppiRect sroi, droi;
	int sstep, dstep;
	IppStatus status;
    int 	bufsize;
    double	xfact, yfact;

	Ipp8u 	*resizebuf;

	U32 refimagestate;

	I32 marksequencelen;

	//--------------------
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++\n");

	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}

	if (normalbulk == NORMALBULK_NORMAL) {
		// 
	} else {
		normalbulk = NORMALBULK_BULK;
	}

	res		= 1;

	pic 	= piana->pic[camid];
	picp	= &pic->icp;

	bufimg 	= NULL;

	buf 	= NULL;
	pb 		= NULL;
	resizebuf = NULL;

    //#if defined(USE_GAMMACORRECTION_CLUBCAM)
	bufgamma = NULL;
//#endif
	pcpc 	= &pic->cpc;
	//--
	memset(pcpc, 0, sizeof(clubpath_context_t));
#if 0
	pic->rowcolsumcount = 0;
	memset(pic->rowsum, 0, sizeof(U32) * MARKSEQUENCELEN * FULLHEIGHT);
	memset(pic->rowsumfrom, 0, sizeof(U32) * MARKSEQUENCELEN);
	memset(pic->rowsumto, 0, sizeof(U32) * MARKSEQUENCELEN);

	memset(pic->colsum, 0, sizeof(U32) * MARKSEQUENCELEN * FULLWIDTH);
	memset(pic->colsumfrom, 0, sizeof(U32) * MARKSEQUENCELEN);
	memset(pic->colsumto, 0, sizeof(U32) * MARKSEQUENCELEN);

	memset(pic->rowcolsumvalid, 0, sizeof(U32) * MARKSEQUENCELEN);

	//memset(pcpc->rowsumsum, 0, sizeof(U32) * FULLHEIGHT);
	//memset(pcpc->colsumsum, 0, sizeof(U32) * FULLWIDTH);
#endif

	res = scamif_imagebuf_peek_latest(piana->hscamif, camid, normalbulk, &pb, NULL, NULL);
	if (res == 0) {
		res = 0;
		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" club fail %d res: %d\n", __LINE__, res);
		goto func_exit;
	}

	//---
	pcii 		= &pb->cii;
	multitude   = pcii->multitude;
	width 		= pcii->width;
	height 		= pcii->height;
	offset_x 	= pcii->offset_x;
	offset_y 	= pcii->offset_y;

	if (multitude == 0) multitude = 1;
	{
		U32 skip, syncdiv;
		U64 period;

		syncdiv = pcii->syncdiv;	if (syncdiv == 0) {	syncdiv = 1;}
		skip = pcii->skip;			if (skip == 0) {skip = 1;}
		period = TSSCALE / (U64) pcii->framerate;			// nsec
		periodm = period * skip * syncdiv;
	}

	//---
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" start..\n");
	if (normalbulk == NORMALBULK_NORMAL) { 		// Re-calc shotindex
		U32	rindex;
		U32	shotindex0, shotindex;
		U32 rindex0, rindex1;
		camif_buffergroup_t *pbgx;
		U32 count;

		shotindex = pic->rindexshot;

		pbgx = NULL;
		scamif_imagebuf_groupinfo2(piana->hscamif, camid, normalbulk, &pb, &pbgx);
		count = pbgx->count;				// buffer slot count

		shotindex0 = pic->rindexshot;
		rindex0 = shotindex0 - 5 + count;
		rindex1 = shotindex0 + 5 + count;

		shotindex = shotindex0;
		for (rindex = rindex0; rindex < rindex1; rindex++) {
			U32 detected;

			detected = 0;

			// 1) get image buffer
			if (piana->opmode == IANA_OPMODE_FILE) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
				if (res == 0) {
					break;
				}
			} else {
				//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bulk, img read\n", i);
				for (j = 0; j < 20; j++) {
					res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
					if (res == 1) {
						break;
					}
					cr_sleep(1);
				}
				if (res == 0) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", rindex, j);
					break;
				}
			}
			pcii 		= &pb->cii;
			width 		= pcii->width;
			height 		= pcii->height;
			offset_x 	= pcii->offset_x;
			offset_y 	= pcii->offset_y;
			multitude   = pcii->multitude;
			if (multitude == 0) multitude = 1;


			ts64 = MAKEU64(pb->ts_h, pb->ts_l);
			if (ts64 > pic->ts64shot) {
				shotindex = rindex;
				break;
			}
		}

		res = scamif_imagebuf_trimindex2(piana->hscamif, camid, normalbulk, shotindex, &shotindex0);
		pcpc->shotindex = pic->rindexshot = shotindex0;
	}

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" after ts64..\n");


	if (normalbulk == NORMALBULK_NORMAL) {
		camif_buffer_info_t *pb;
		camif_buffergroup_t *pbg;
		U32 count;
		pbg = NULL;
		scamif_imagebuf_groupinfo2(piana->hscamif, camid, normalbulk, &pb, &pbg);
		count = pbg->count;				// buffer slot count

		r0 = pic->rindexshot - PRESHOTCOUNT / multitude;
		//r0 = pic->rindexshot - PRESHOTCOUNT/2;
//		if (r0 < 0) { r0 = 0; }
		r1 = pic->rindexshot + POSTSHOTCOUNT / multitude;
//		if (r1 >= MARKSEQUENCELEN) { r1 = MARKSEQUENCELEN - 1; }

		r0 += count;
		r1 += count;
		pcpc->shotindex 	=  pic->rindexshot;

		refr0 = r0 - 10;
		refr1 = pic->rindexshot + count;
		refr2 = r1 + 10;
	} else {
		r0 = pic->rindexshotbulk - PRESHOTCOUNT;
		if (r0 < 0) { r0 = 0; }
		r1 = pic->rindexshotbulk + POSTSHOTCOUNT;
		if (r1 >= marksequencelen) { r1 = marksequencelen - 1; }

		pcpc->shotindex 	=  pic->rindexshotbulk;

		refr0 = r0 - 10;
		refr1 = pic->rindexshotbulk;
		refr2 = r1 + 10;
	}

//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" after r0 r1 (%d ~ %d)..\n", r0, r1);

	pcpc->shotframe 	=  0;
	pcpc->startindex 	=  r0;
	pcpc->endindex 		=  r1;

	pcpc->ts64shot = pic->ts64shot;

	pcpc->diffimg 	  = NULL;
	pcpc->diffimgFilter = NULL;
	pcpc->diffimgBinary = NULL;

	pcpc->prefimgNoBall= (U08 *)malloc(FULLWIDTH * FULLHEIGHT);
//	memcpy(pcpc->prefimgNoBall, pic->prefimg, FULLWIDTH * FULLHEIGHT);
	//--
	diffimg			= NULL;
	diffimgFilter	= NULL;
	diffimgBinary	= NULL;

	memset(&pcpc->linecount[0], 0, sizeof(I32) * MARKSEQUENCELEN);

	pcpc->count = 0;
	pcpc->shotindex = pic->rindexshotbulk + pic->firstbulkindex;
	pcpc->shotindexcombined = (double)pcpc->shotindex;
	pcpc->aftershot = 0;

	pcpc->preshot_mtxa_valid = 0;
	pcpc->preshot_mtya_valid = 0;

	pcpc->postshot_mtxa_valid = 0;
	pcpc->postshot_mtya_valid = 0;

	pcpc->preshot_myxa[2] = 0; pcpc->preshot_myxa[1] = 0; pcpc->preshot_myxa[0] = 0;
	pcpc->postshot_myxa[2] = 0; pcpc->postshot_myxa[1] = 0; pcpc->postshot_myxa[0] = 0;
	//--------
	{
		double balldia;
		double dx, dy;
		point_t Pp1, Lp1; 
		point_t Pp2, Lp2; 

		Lp1.x = pic->icp.L.x;
		Lp1.y = pic->icp.L.y;
		Pp1.x = pic->icp.P.x;
		Pp1.y = pic->icp.P.y;

		Lp2.x = Lp1.x;
		Lp2.y = Lp1.y + BALLDIAMETER;

		iana_L2P(piana, camid, &Lp2, &Pp2, 0);
		dx = Pp1.x - Pp2.x;
		dy = Pp1.y - Pp2.y;
		balldia = sqrt(dx*dx+dy*dy);
#define HLENMIN	2		
#define HLENMAX	3		
		pcpc->hlenmin = (balldia * HLENMIN);
		pcpc->hlenmax =  (balldia * HLENMAX);
		pcpc->hlenEstimated = (pcpc->hlenmin + pcpc->hlenmax)/2;
		pcpc->hlenEstimatedL = ((BALLDIAMETER * (HLENMIN + HLENMAX)) / 2); 
		pcpc->hitoffsetL = 0;
	}

//	multitude = 1;
	memset(pcpc->hlen, 0, sizeof(I32) * MARKSEQUENCELEN);

	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pic->prefimg, width, height, 0, 1, 2, 3, piana->imguserparam);
	}

	makeRef(piana, camid, pic, width, height, normalbulk, multitude, refr0, refr1);
	//makeRef(piana, camid, pic, width, height, normalbulk, r0, r1);
	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pic->prefimg, width, height, 2, 1, 2, 3, piana->imguserparam);
	}

    xfact 		= 1.0;
    yfact 		= 1.0 * multitude;
    ssize.width 	= width;
    ssize.height 	= height;

    sroi.x 		= 0;
    sroi.y 		= 0;
    sroi.width 	= width;
    sroi.height	= height/multitude;

    droi.x 		= 0;
    droi.y 		= 0;
    droi.width 	= (int)(sroi.width * xfact + 0.1);
    droi.height	= (int)(sroi.height * yfact + 0.1);

    {
    #if 1
        sstep 		= sroi.width;
        dstep		= droi.width;
    #endif

        ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_CUBIC, &bufsize);
        resizebuf = (U08 *)malloc(bufsize);

	// refimgNoBall...  Fake..
	status = ippiResizeSqrPixel_8u_C1R(
			pic->prefimg,
			ssize,
			sstep,
			sroi,
			//
			(Ipp8u*) pcpc->prefimgNoBall,
			dstep,
			droi,
			//
			xfact,
			yfact,
			0,		// xshift,
			0,		// yshift,
			IPPI_INTER_CUBIC,
			resizebuf
			);
    }
	
	if (s_use_gammacorrection_clubcam) {
		gamma_correction(pcpc->prefimgNoBall, pcpc->prefimgNoBall, width, height);
	}
	//-------------------------------------------------------
	// Get shot frame..
	m = 0;
	frame = 0;
	pcpc->shotframe = 0;
	ts64prev = 0;
	indexprev 	= 0;
	mprev 		= 0;
	frameprev 	= 0;

	for (index = r0; index <= r1; index++) {
		U32 detected;

		detected = 0;
		if (normalbulk == NORMALBULK_NORMAL) {
			rindex = index;
		} else {
			rindex = pic->firstbulkindex + index;
		}
		// 1) get image buffer
		if (piana->opmode == IANA_OPMODE_FILE) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
			if (res == 0) {
				break;
			}
		} else {
			//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bulk, img read\n", i);
			for (j = 0; j < 20; j++) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
				if (res == 1) {
					break;
				}
				cr_sleep(1);
			}
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read [%d:%d]\n", i, j);
			if (res == 0) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", index, j);
				break;
				//continue;
			}
		}

		pcii 		= &pb->cii;
		width 		= pcii->width;
		height 		= pcii->height;
		offset_x 	= pcii->offset_x;
		offset_y 	= pcii->offset_y;
		multitude   = pcii->multitude;
		if (multitude == 0) multitude = 1;


		ts64 = MAKEU64(pb->ts_h, pb->ts_l);

		//------
#if 1
		for (m = 0; m < multitude; m++) {
			ts64Mt = ts64 - (periodm * (multitude - m -1));

			if (frame != 0 && ts64Mt > pcpc->ts64shot) {
				if (pcpc->ts64shot > (ts64Mt + ts64prev)/2) {
					pcpc->shotindex = index;
					pcpc->shotm 	= m;
					pcpc->shotframe = frame;
				} else {
					pcpc->shotindex = indexprev;
					pcpc->shotm 	= mprev;
					pcpc->shotframe = frameprev;
				}
				detected = 1;
				break;
			}
			ts64prev  	= ts64Mt;
			indexprev 	= index;
			mprev 		= m;
			frameprev 	= frame;

			frame++;
		}
#endif
		if (detected) {
			break;
		}

	}

	m = 0;
	frame = 0;
	refimagestate = 0;			// NO YET..
	for (index = r0; index <= r1; index++) 
	{
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "--- index <%d> \n", index);
		if (normalbulk == NORMALBULK_NORMAL) {
			rindex = index;
		} else {
			rindex = pic->firstbulkindex + index;
		}

		// 1) get image buffer
		if (piana->opmode == IANA_OPMODE_FILE) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
			if (res == 0) {
				break;
			}
		} else {
			//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bulk, img read\n", i);
			for (j = 0; j < 20; j++) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
				if (res == 1) {
					break;
				}
				cr_sleep(1);
			}
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read [%d:%d]\n", i, j);
			if (res == 0) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", index, j);
				break;
				//continue;
			}
		}
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " read data, rindex: %d, normalbulk: %d\n", rindex, normalbulk);

		pcii 		= &pb->cii;
		width 		= pcii->width;
		height 		= pcii->height;
		offset_x 	= pcii->offset_x;
		offset_y 	= pcii->offset_y;
		multitude   = pcii->multitude;
		if (multitude == 0) multitude = 1;

		if (bufimg 	      			== NULL) 	{bufimg 			= (U08 *)malloc(width*height);}
		if (pcpc->diffimg 	  		== NULL) 	{pcpc->diffimg 		= (U08 *)malloc(width*height);}
		if (pcpc->diffimgFilter 	== NULL) 	{pcpc->diffimgFilter= (U08 *)malloc(width*height);}
		if (pcpc->diffimgBinary 	== NULL) 	{pcpc->diffimgBinary= (U08 *)malloc(width*height);}
//#if defined(USE_GAMMACORRECTION_CLUBCAM)
		if (bufgamma 				== NULL) 	{bufgamma 			= (U08 *)malloc(width*height);}
//#endif


		diffimg			= pcpc->diffimg;
		diffimgFilter	= pcpc->diffimgFilter;
		diffimgBinary	= pcpc->diffimgBinary;

		pcpc->width = width;
		pcpc->height = height;
		pcpc->offset_x = offset_x;
		pcpc->offset_y = offset_y;
		pcpc->index = index;
		//pcpc->m     = m;
		//pcpc->frame = frame;

		pcpc->gain = pcii->gain;
		pcpc->exposure = pcii->exposure;

		pcpc->multitude = multitude;

		ts64 = MAKEU64(pb->ts_h, pb->ts_l);


		UNUSED(ts64Mt);	
		{
			U32 skip, syncdiv;
			U64 period;

			syncdiv = pcii->syncdiv;	if (syncdiv == 0) {	syncdiv = 1;}
			skip = pcii->skip;			if (skip == 0) {skip = 1;}
			period = TSSCALE / (U64) pcii->framerate;			// nsec
			periodm = period * skip * syncdiv;
		}

		for (m = 0; m < multitude; m++) {
			pcpc->m     = m;
			pcpc->frame = frame;

			ts64Mt = ts64 - (periodm * (multitude - m -1));
			pcpc->ts64[frame] = ts64Mt;
			pcpc->tsd[frame] = (ts64Mt / TSSCALE_D) - (pcpc->ts64shot / TSSCALE_D);

			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" m: %d\n", m);

			if (multitude <= 1) {
				procimg = buf;
			} else {
				procimg = pic->pimgFull;
				status = ippiResizeSqrPixel_8u_C1R(
						//(Ipp8u *)buf + m * pcii->width * (pcii->height / multitude),
						(Ipp8u *)buf + pcii->width * ((m*pcii->height) / multitude),
						ssize,
						sstep,
						sroi,
						//
						(Ipp8u*) procimg,
						dstep,
						droi,
						//
						xfact,
						yfact,
						0,		// xshift,
						0,		// yshift,
						IPPI_INTER_CUBIC,
						resizebuf
						);
				if (piana->himgfunc) {
					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)procimg, droi.width, droi.height, 1, 1, 2, 3, piana->imguserparam);
				}
			}

			//----- Update Context
			ClubPath_UpdateContext(piana, camid, pcpc);
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " (%10lf %10lf)\n", pcpc->x_, pcpc->y_);

			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " xxx 1\n");
			//----- 1st image processing
			if (s_use_gammacorrection_clubcam) {
				gamma_correction(procimg, bufgamma, width, height);
				ClubPath_ProcessImage3(piana, camid, 
						bufgamma, 
						pcpc,
						diffimg,
						diffimgFilter,
						diffimgBinary
						);
			} else {
				ClubPath_ProcessImage3(piana, camid, 
						procimg, 
						pcpc,
						diffimg,
						diffimgFilter,
						diffimgBinary
						);
			}

			//---- Calc club header position
			if (piana->camsensor_category == CAMSENSOR_EYEXO) {
				// what..
			} else {
				ClubPath_CalcPosition(piana, camid, pcpc);
			}

			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "After clubpath\n");

			{
				int itmp;
				int xx, yy;

				memset(bufimg, 0, width*height);
				for (j = 0; j < (int)pcpc->roiheight; j++) {
					if (s_use_gammacorrection_clubcam) {
					memcpy(bufimg + pcpc->startx + (j+pcpc->starty) * width, (char *)(bufgamma + pcpc->startx + (j+pcpc->starty) * width), pcpc->roiwidth);
					} else {
					memcpy(bufimg + pcpc->startx + (j+pcpc->starty) * width, (char *)(buf + pcpc->startx + (j+pcpc->starty) * width), pcpc->roiwidth);
					}
				}

				xx = (int)pcpc->hsx[frame];					// '+' Shaped..
				yy = (int)pcpc->hsy[frame];
#define CROSSHALF	10

				for (j = -CROSSHALF; j < CROSSHALF; j++) {
					if (xx > CROSSHALF && xx < (I32)(width - CROSSHALF) && yy > CROSSHALF && yy < (I32) (height - CROSSHALF)) 
					{

						itmp = (int) *(bufimg + (xx+0) + (j+yy) * width);
						itmp = 0xFF - itmp;
						*(bufimg + (xx+0) + (j+yy) * width) = (U08) (itmp & 0xFF);

						itmp = (int) *(bufimg + (xx+j) + (0+yy) * width);
						itmp = 0xFF - itmp;
						*(bufimg + (xx+j) + (0+yy) * width) = (U08) (itmp & 0xFF);
					}
				}

				xx = (int)pcpc->hx[frame][0];			// 'X' HOUGH..
				yy = (int)pcpc->hy[frame][0];
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "=========================> [%d] %lf %lf\n", pcpc->datacount, pcpc->x_, pcpc->y_);
				for (j = -CROSSHALF; j < CROSSHALF; j++) {
					if (xx > CROSSHALF && xx < (I32)(width - CROSSHALF) && yy > CROSSHALF && yy < (I32) (height - CROSSHALF)) 
					{
						itmp = (int)*(bufimg + (xx+j) + (j+yy) * width);
						itmp = 0xFF - itmp;
						//itmp = 0xFF;
						*(bufimg + (xx+j) + (j+yy) * width) = (U08) (itmp & 0xFF);

						itmp = (int)*(bufimg + (xx+j) + (-j+yy) * width);
						itmp = 0xFF - itmp;
						//itmp = 0xFF;
						*(bufimg + (xx+j) + (-j+yy) * width) = (U08) (itmp & 0xFF);
					}
				}

				if (piana->himgfunc) {
					//				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)bufimg, width, height, 0, 1, 2, 3, piana->imguserparam);
					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)bufimg, width, height, 2, 1, 2, 3, piana->imguserparam);
				}
			}
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Shape..\n");

			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)(diffimgBinary), width, height, 4, 1, 2, 3, piana->imguserparam);
			}
			j++;		// fake7Aa
			frame++;

		} 		// for (m = 0; m < multitude; m++) 
	} 			// for (index = r0; index <= r1; index++) 

	free(bufimg);
	free(diffimg);
	free(diffimgFilter);
	free(diffimgBinary);
	free(pcpc->prefimgNoBall);
	free(bufgamma);
	free(resizebuf);

	pcpc->diffimg 	  = NULL;
	pcpc->diffimgFilter = NULL;
	pcpc->diffimgBinary = NULL;
	pcpc->prefimgNoBall = NULL;

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" Before Refine\n");

	res = ClubPath_RefinePosition(piana, camid, pcpc);
	if (res ) {
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" Before ClubPath_CalcClubData\n");

		//iana_spin_clubimage_overlap(piana, camid);
		//	res = iana_spin_clubpath_header(piana, camid, pcpc);
		res = ClubPath_CalcClubData(piana, camid, pcpc);
	}
	if (res) {
		res = ClubPath_CalcSpin(piana, camid, pcpc);
	}

	if (res) {
		iana_shotresult_t *psr;
		iana_shotresult_t *pcsr;;

		//-----
		psr  = &piana->shotresultdata;
		pcsr = &piana->clubball_shotresult[camid];

		memcpy(pcsr, psr, sizeof(iana_shotresult_t));

		//

		pcsr->readflag 		= 0;
		pcsr->spinAssurance = 1;

		pcsr->sidespin 		= pcpc->sidespin;
		pcsr->backspin 		= pcpc->backspin;

		pcsr->axis.x 		= pcpc->backspin;
		pcsr->axis.y 		= 0;			// rollspin;
		pcsr->axis.z 		= -pcpc->sidespin;

		pcsr->spinmag 		= cr_vector_normalization(&pcsr->axis, &pcsr->axis);

		if (piana->camsensor_category != CAMSENSOR_CONSOLE1) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-------\n");

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "clubspdB: %lf -> %lf\n", pcsr->clubspeed_B, pcpc->clubspeedB);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "clubspdA: %lf -> %lf\n", pcsr->clubspeed_A, pcpc->clubspeedA);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "clubface: %lf -> %lf\n", pcsr->clubfaceangle, pcpc->clubfaceangle);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "clubpath: %lf -> %lf\n", pcsr->clubpath, pcpc->clubpath);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-------\n");

			pcsr->clubspeed_B 	= pcpc->clubspeedB;
			pcsr->clubspeed_A 	= pcpc->clubspeedA;
			pcsr->clubfaceangle	= pcpc->clubfaceangle;
			pcsr->clubpath    	= pcpc->clubpath;
		} else {
			pcpc->clubspeedB 	= pcsr->clubspeed_B;
			pcpc->clubspeedA 	= pcsr->clubspeed_A;
			pcpc->clubfaceangle	= pcsr->clubfaceangle;
			pcpc->clubpath    	= pcsr->clubpath;

		}
		pcsr->headerlenL	= pcpc->hlenEstimatedL;
		pcsr->hitoffsetL	= pcpc->hitoffsetL;
		pcsr->geareffect	= pcpc->geareffect;

		pcsr->width			= pcpc->width;
		pcsr->height		= pcpc->height;

		pcsr->valid			= 1;

		//------------------
		psr->clubfaceangle	= pcpc->clubfaceangle;
		psr->clubpath		= pcpc->clubpath;
		psr->clubspeed_A	= pcpc->clubspeedA;
		psr->clubspeed_B	= pcpc->clubspeedB;


		psr->clubAssurance  = 0.95;

		//--
		psr->clubattackangle = 0;
		psr->clubloftangle = 0;
		psr->clublieangle = 0;
		psr->clubfaceimpactLateral = 0;
		psr->clubfaceimpactVertical = 0;

		psr->Assurance_clubspeed_B 			= 90;
		psr->Assurance_clubspeed_A 			= 90;
		psr->Assurance_clubpath 			= 90;
		psr->Assurance_clubfaceangle 		= 85;
		psr->Assurance_clubattackangle		= 0;
		psr->Assurance_clubloftangle 		= 0;
		psr->Assurance_clublieangle 		= 0;
		psr->Assurance_clubfaceimpactLateral = 0;
		psr->Assurance_clubfaceimpactVertical = 0;
		//--
	} else {
		// Estimated clubspeed.. 
		iana_shotresult_t *psr;
		psr  = &piana->shotresultdata;
		psr->clubspeed_B = fabs(psr->clubspeed_B);
		psr->clubspeed_A = fabs(psr->clubspeed_A);
		if (psr->clubspeed_B == 0 || psr->clubspeed_A == 0) {
			psr->clubspeed_B = 2;
			psr->clubspeed_A = 1;
		}
#define CLUBSPEED_A_ADDVALUE		(1000.0)
		psr->clubspeed_A = psr->clubspeed_A + CLUBSPEED_A_ADDVALUE;

		psr->clubAssurance  = 0.50;


		//--
		psr->clubattackangle = 0;
		psr->clubloftangle = 0;
		psr->clublieangle = 0;
		psr->clubfaceimpactLateral = 0;
		psr->clubfaceimpactVertical = 0;

		psr->Assurance_clubspeed_B 			= 80;
		psr->Assurance_clubspeed_A 			= 80;
		psr->Assurance_clubpath 			= 80;
		psr->Assurance_clubfaceangle 		= 85;
		psr->Assurance_clubattackangle		= 0;
		psr->Assurance_clubloftangle 		= 0;
		psr->Assurance_clublieangle 		= 0;
		psr->Assurance_clubfaceimpactLateral = 0;
		psr->Assurance_clubfaceimpactVertical = 0;
		//--

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" CLUB_ESTIMATION   path: %lf  face: %lf  speedB: %lf  speedA: %lf\n",
				psr->clubpath,
				psr->clubfaceangle,
				psr->clubspeed_B,
				psr->clubspeed_A);
	}
	res = 1;
func_exit:
	return res;
#else
	I32 res;

	iana_cam_t	*pic;
	iana_cam_param_t *picp;
	camimageinfo_t     *pcii;
	camif_buffer_info_t *pb;
	
	U32	width;
	U32	height;
	U32	offset_x;
	U32	offset_y;
	U08	*buf;
	U32	rindex;
	U32 multitude;

	U08 *bufimg;
	U08 *diffimg;
	U08 *diffimgFilter;
	U08 *diffimgBinary;

	U08 *procimg;
	I32 j;
	I32 index;
	U32 m;
	U32 frame;

	clubpath_context_t 	*pcpc;
//	U32 updateRefImage;

	I32 r0, r1;
	I32 refr0, refr1, refr2;

	U08 *bufgamma;
	
	U64 ts64, ts64prev;
	U64 ts64Mt;
	U32 indexprev;
	U32 mprev;
	U32 frameprev;
	
	U64 periodm;

	//-
    cv::Size ssize;
    cv::Rect sroi, droi;
	double	xfact, yfact; 

	U32 refimagestate;

	I32 marksequencelen;

	//--------------------
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}

	if (normalbulk == NORMALBULK_NORMAL) {
		// 
	} else {
		normalbulk = NORMALBULK_BULK;
	}

	res		= 1;

	pic 	= piana->pic[camid];
	picp	= &pic->icp;

	bufimg 	= NULL;

	buf 	= NULL;
	pb 		= NULL;

    bufgamma = NULL;
	pcpc 	= &pic->cpc;
	
    //--
	memset(pcpc, 0, sizeof(clubpath_context_t));

	res = scamif_imagebuf_peek_latest(piana->hscamif, camid, normalbulk, &pb, NULL, NULL);
	if (res == 0) {
		res = 0;
		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" club fail %d res: %d\n", __LINE__, res);
		goto func_exit;
	}

	//---
	pcii 		= &pb->cii;
	multitude   = pcii->multitude;
	width 		= pcii->width;
	height 		= pcii->height;
	offset_x 	= pcii->offset_x;
	offset_y 	= pcii->offset_y;

	if (multitude == 0) multitude = 1;
	{
		U32 skip, syncdiv;
		U64 period;

		syncdiv = pcii->syncdiv;	if (syncdiv == 0) {	syncdiv = 1;}
		skip = pcii->skip;			if (skip == 0) {skip = 1;}
		period = TSSCALE / (U64) pcii->framerate;			// nsec
		periodm = period * skip * syncdiv;
	}

	//---
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" start..\n");
	if (normalbulk == NORMALBULK_NORMAL) { 		// Re-calc shotindex
		U32	rindex;
		U32	shotindex0, shotindex;
		U32 rindex0, rindex1;
		camif_buffergroup_t *pbgx;
		U32 count;

		shotindex = pic->rindexshot;

		pbgx = NULL;
		scamif_imagebuf_groupinfo2(piana->hscamif, camid, normalbulk, &pb, &pbgx);
		count = pbgx->count;				// buffer slot count

		shotindex0 = pic->rindexshot;
		rindex0 = shotindex0 - 5 + count;
		rindex1 = shotindex0 + 5 + count;

		shotindex = shotindex0;
		for (rindex = rindex0; rindex < rindex1; rindex++) {
			U32 detected;

			detected = 0;

			// 1) get image buffer
			if (piana->opmode == IANA_OPMODE_FILE) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
				if (res == 0) {
					break;
				}
			} else {
				//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bulk, img read\n", i);
				for (j = 0; j < 20; j++) {
					res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
					if (res == 1) {
						break;
					}
					cr_sleep(1);
				}
				if (res == 0) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", rindex, j);
					break;
				}
			}
			pcii 		= &pb->cii;
			width 		= pcii->width;
			height 		= pcii->height;
			offset_x 	= pcii->offset_x;
			offset_y 	= pcii->offset_y;
			multitude   = pcii->multitude;
			if (multitude == 0) multitude = 1;


			ts64 = MAKEU64(pb->ts_h, pb->ts_l);
			if (ts64 > pic->ts64shot) {
				shotindex = rindex;
				break;
			}
		}

		res = scamif_imagebuf_trimindex2(piana->hscamif, camid, normalbulk, shotindex, &shotindex0);
		pcpc->shotindex = pic->rindexshot = shotindex0;
	}

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" after ts64..\n");


	if (normalbulk == NORMALBULK_NORMAL) {
		camif_buffer_info_t *pb;
		camif_buffergroup_t *pbg;
		U32 count;
		pbg = NULL;
		scamif_imagebuf_groupinfo2(piana->hscamif, camid, normalbulk, &pb, &pbg);
		count = pbg->count;				// buffer slot count

		r0 = pic->rindexshot - PRESHOTCOUNT / multitude;
		//r0 = pic->rindexshot - PRESHOTCOUNT/2;
//		if (r0 < 0) { r0 = 0; }
		r1 = pic->rindexshot + POSTSHOTCOUNT / multitude;
//		if (r1 >= MARKSEQUENCELEN) { r1 = MARKSEQUENCELEN - 1; }

		r0 += count;
		r1 += count;
		pcpc->shotindex 	=  pic->rindexshot;

		refr0 = r0 - 10;
		refr1 = pic->rindexshot + count;
		refr2 = r1 + 10;
	} else {
		r0 = pic->rindexshotbulk - PRESHOTCOUNT;
		if (r0 < 0) { r0 = 0; }
		r1 = pic->rindexshotbulk + POSTSHOTCOUNT;
		if (r1 >= marksequencelen) { r1 = marksequencelen - 1; }

		pcpc->shotindex 	=  pic->rindexshotbulk;

		refr0 = r0 - 10;
		refr1 = pic->rindexshotbulk;
		refr2 = r1 + 10;
	}

//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" after r0 r1 (%d ~ %d)..\n", r0, r1);

	pcpc->shotframe 	=  0;
	pcpc->startindex 	=  r0;
	pcpc->endindex 		=  r1;

	pcpc->ts64shot = pic->ts64shot;

	pcpc->diffimg 	  = NULL;
	pcpc->diffimgFilter = NULL;
	pcpc->diffimgBinary = NULL;

	pcpc->prefimgNoBall= (U08 *)malloc(FULLWIDTH * FULLHEIGHT);
//	memcpy(pcpc->prefimgNoBall, pic->prefimg, FULLWIDTH * FULLHEIGHT);
	//--
	diffimg			= NULL;
	diffimgFilter	= NULL;
	diffimgBinary	= NULL;

	memset(&pcpc->linecount[0], 0, sizeof(I32) * MARKSEQUENCELEN);

	pcpc->count = 0;
	pcpc->shotindex = pic->rindexshotbulk + pic->firstbulkindex;
	pcpc->shotindexcombined = (double)pcpc->shotindex;
	pcpc->aftershot = 0;

	pcpc->preshot_mtxa_valid = 0;
	pcpc->preshot_mtya_valid = 0;

	pcpc->postshot_mtxa_valid = 0;
	pcpc->postshot_mtya_valid = 0;

	pcpc->preshot_myxa[2] = 0; pcpc->preshot_myxa[1] = 0; pcpc->preshot_myxa[0] = 0;
	pcpc->postshot_myxa[2] = 0; pcpc->postshot_myxa[1] = 0; pcpc->postshot_myxa[0] = 0;
	//--------
	{
		double balldia;
		double dx, dy;
		point_t Pp1, Lp1; 
		point_t Pp2, Lp2; 

		Lp1.x = pic->icp.L.x;
		Lp1.y = pic->icp.L.y;
		Pp1.x = pic->icp.P.x;
		Pp1.y = pic->icp.P.y;

		Lp2.x = Lp1.x;
		Lp2.y = Lp1.y + BALLDIAMETER;

		iana_L2P(piana, camid, &Lp2, &Pp2, 0);
		dx = Pp1.x - Pp2.x;
		dy = Pp1.y - Pp2.y;
		balldia = sqrt(dx*dx+dy*dy);
#define HLENMIN	2		
#define HLENMAX	3		
		pcpc->hlenmin = (balldia * HLENMIN);
		pcpc->hlenmax =  (balldia * HLENMAX);
		pcpc->hlenEstimated = (pcpc->hlenmin + pcpc->hlenmax)/2;
		pcpc->hlenEstimatedL = ((BALLDIAMETER * (HLENMIN + HLENMAX)) / 2); 
		pcpc->hitoffsetL = 0;
	}

//	multitude = 1;
	memset(pcpc->hlen, 0, sizeof(I32) * MARKSEQUENCELEN);

	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pic->prefimg, width, height, 0, 1, 2, 3, piana->imguserparam);
	}

	makeRef(piana, camid, pic, width, height, normalbulk, multitude, refr0, refr1);
	//makeRef(piana, camid, pic, width, height, normalbulk, r0, r1);
	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pic->prefimg, width, height, 2, 1, 2, 3, piana->imguserparam);
	}

    xfact 		= 1.0;
    yfact 		= 1.0 * multitude;
    ssize.width 	= width;
    ssize.height 	= height;

    sroi.x 		= 0;
    sroi.y 		= 0;
    sroi.width 	= width;
    sroi.height	= height/multitude;

    droi.x 		= 0;
    droi.y 		= 0;
    droi.width 	= (int)(sroi.width * xfact + 0.1);
    droi.height	= (int)(sroi.height * yfact + 0.1);

    {
        cv::Mat cvImgRef(sroi.size(), CV_8UC1, pic->prefimg), cvImgNoBall;
        cv::resize(cvImgRef, cvImgNoBall, droi.size(), xfact, yfact,cv::INTER_CUBIC);
        memcpy(pcpc->prefimgNoBall, cvImgNoBall.data, sizeof(U08) * cvImgNoBall.cols * cvImgNoBall.rows);
    }
        
	
	if (s_use_gammacorrection_clubcam) {
		gamma_correction(pcpc->prefimgNoBall, pcpc->prefimgNoBall, width, height);
	}
	//-------------------------------------------------------
	// Get shot frame..
	m = 0;
	frame = 0;
	pcpc->shotframe = 0;
	ts64prev = 0;
	indexprev 	= 0;
	mprev 		= 0;
	frameprev 	= 0;

	for (index = r0; index <= r1; index++) {
		U32 detected;

		detected = 0;
		if (normalbulk == NORMALBULK_NORMAL) {
			rindex = index;
		} else {
			rindex = pic->firstbulkindex + index;
		}
		// 1) get image buffer
		if (piana->opmode == IANA_OPMODE_FILE) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
			if (res == 0) {
				break;
			}
		} else {
			//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bulk, img read\n", i);
			for (j = 0; j < 20; j++) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
				if (res == 1) {
					break;
				}
				cr_sleep(1);
			}
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read [%d:%d]\n", i, j);
			if (res == 0) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", index, j);
				break;
				//continue;
			}
		}

		pcii 		= &pb->cii;
		width 		= pcii->width;
		height 		= pcii->height;
		offset_x 	= pcii->offset_x;
		offset_y 	= pcii->offset_y;
		multitude   = pcii->multitude;
		if (multitude == 0) multitude = 1;


		ts64 = MAKEU64(pb->ts_h, pb->ts_l);

		//------
#if 1
		for (m = 0; m < multitude; m++) {
			ts64Mt = ts64 - (periodm * (multitude - m -1));

			if (frame != 0 && ts64Mt > pcpc->ts64shot) {
				if (pcpc->ts64shot > (ts64Mt + ts64prev)/2) {
					pcpc->shotindex = index;
					pcpc->shotm 	= m;
					pcpc->shotframe = frame;
				} else {
					pcpc->shotindex = indexprev;
					pcpc->shotm 	= mprev;
					pcpc->shotframe = frameprev;
				}
				detected = 1;
				break;
			}
			ts64prev  	= ts64Mt;
			indexprev 	= index;
			mprev 		= m;
			frameprev 	= frame;

			frame++;
		}
#endif
		if (detected) {
			break;
		}

	}

	m = 0;
	frame = 0;
	refimagestate = 0;			// NO YET..
	for (index = r0; index <= r1; index++) 
	{
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "--- index <%d> \n", index);
		if (normalbulk == NORMALBULK_NORMAL) {
			rindex = index;
		} else {
			rindex = pic->firstbulkindex + index;
		}

		// 1) get image buffer
		if (piana->opmode == IANA_OPMODE_FILE) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
			if (res == 0) {
				break;
			}
		} else {
			//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bulk, img read\n", i);
			for (j = 0; j < 20; j++) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
				if (res == 1) {
					break;
				}
				cr_sleep(1);
			}
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read [%d:%d]\n", i, j);
			if (res == 0) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", index, j);
				break;
				//continue;
			}
		}
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " read data, rindex: %d, normalbulk: %d\n", rindex, normalbulk);

		pcii 		= &pb->cii;
		width 		= pcii->width;
		height 		= pcii->height;
		offset_x 	= pcii->offset_x;
		offset_y 	= pcii->offset_y;
		multitude   = pcii->multitude;
		if (multitude == 0) multitude = 1;

		if (bufimg 	      			== NULL) 	{bufimg 			= (U08 *)malloc(width*height);}
		if (pcpc->diffimg 	  		== NULL) 	{pcpc->diffimg 		= (U08 *)malloc(width*height);}
		if (pcpc->diffimgFilter 	== NULL) 	{pcpc->diffimgFilter= (U08 *)malloc(width*height);}
		if (pcpc->diffimgBinary 	== NULL) 	{pcpc->diffimgBinary= (U08 *)malloc(width*height);}
//#if defined(USE_GAMMACORRECTION_CLUBCAM)
		if (bufgamma 				== NULL) 	{bufgamma 			= (U08 *)malloc(width*height);}
//#endif


		diffimg			= pcpc->diffimg;
		diffimgFilter	= pcpc->diffimgFilter;
		diffimgBinary	= pcpc->diffimgBinary;

		pcpc->width = width;
		pcpc->height = height;
		pcpc->offset_x = offset_x;
		pcpc->offset_y = offset_y;
		pcpc->index = index;
		//pcpc->m     = m;
		//pcpc->frame = frame;

		pcpc->gain = pcii->gain;
		pcpc->exposure = pcii->exposure;

		pcpc->multitude = multitude;

		ts64 = MAKEU64(pb->ts_h, pb->ts_l);


		UNUSED(ts64Mt);	
		{
			U32 skip, syncdiv;
			U64 period;

			syncdiv = pcii->syncdiv;	if (syncdiv == 0) {	syncdiv = 1;}
			skip = pcii->skip;			if (skip == 0) {skip = 1;}
			period = TSSCALE / (U64) pcii->framerate;			// nsec
			periodm = period * skip * syncdiv;
		}

		for (m = 0; m < multitude; m++) {
			pcpc->m     = m;
			pcpc->frame = frame;

			ts64Mt = ts64 - (periodm * (multitude - m -1));
			pcpc->ts64[frame] = ts64Mt;
			pcpc->tsd[frame] = (ts64Mt / TSSCALE_D) - (pcpc->ts64shot / TSSCALE_D);

			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" m: %d\n", m);

			if (multitude <= 1) {
				procimg = buf;
			} else {
                cv::Mat cvImgBuf(cv::Size(width, height), CV_8UC1, buf), cvImgSlice, cvImgResized;

                cvImgSlice = cvImgBuf(cv::Rect(0, (m*height) / multitude, width, height / multitude));
                cv::resize(cvImgSlice, cvImgResized, droi.size(), xfact, yfact, cv::INTER_CUBIC);

                procimg = pic->pimgFull;
                memcpy(procimg, cvImgResized.data, sizeof(U08) *cvImgResized.cols * cvImgResized.rows);

				if (piana->himgfunc) {
					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)procimg, droi.width, droi.height, 1, 1, 2, 3, piana->imguserparam);
				}
			}

			//----- Update Context
			ClubPath_UpdateContext(piana, camid, pcpc);

            //----- 1st image processing
			if (s_use_gammacorrection_clubcam) {
				gamma_correction(procimg, bufgamma, width, height);
				ClubPath_ProcessImage3(piana, camid, 
						bufgamma, 
						pcpc,
						diffimg,
						diffimgFilter,
						diffimgBinary
						);
			} else {
				ClubPath_ProcessImage3(piana, camid, 
						procimg, 
						pcpc,
						diffimg,
						diffimgFilter,
						diffimgBinary
						);
			}

			//---- Calc club header position
			if (piana->camsensor_category == CAMSENSOR_EYEXO) {
				// what..
			} else {
				ClubPath_CalcPosition(piana, camid, pcpc);
			}

			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "After clubpath\n");

			{
				int itmp;
				int xx, yy;

				memset(bufimg, 0, width*height);
				for (j = 0; j < (int)pcpc->roiheight; j++) {
					if (s_use_gammacorrection_clubcam) {
					memcpy(bufimg + pcpc->startx + (j+pcpc->starty) * width, (char *)(bufgamma + pcpc->startx + (j+pcpc->starty) * width), pcpc->roiwidth);
					} else {
					memcpy(bufimg + pcpc->startx + (j+pcpc->starty) * width, (char *)(buf + pcpc->startx + (j+pcpc->starty) * width), pcpc->roiwidth);
					}
				}

				xx = (int)pcpc->hsx[frame];					// '+' Shaped..
				yy = (int)pcpc->hsy[frame];
#define CROSSHALF	10

				for (j = -CROSSHALF; j < CROSSHALF; j++) {
					if (xx > CROSSHALF && xx < (I32)(width - CROSSHALF) && yy > CROSSHALF && yy < (I32) (height - CROSSHALF)) 
					{

						itmp = (int) *(bufimg + (xx+0) + (j+yy) * width);
						itmp = 0xFF - itmp;
						*(bufimg + (xx+0) + (j+yy) * width) = (U08) (itmp & 0xFF);

						itmp = (int) *(bufimg + (xx+j) + (0+yy) * width);
						itmp = 0xFF - itmp;
						*(bufimg + (xx+j) + (0+yy) * width) = (U08) (itmp & 0xFF);
					}
				}

				xx = (int)pcpc->hx[frame][0];			// 'X' HOUGH..
				yy = (int)pcpc->hy[frame][0];
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "=========================> [%d] %lf %lf\n", pcpc->datacount, pcpc->x_, pcpc->y_);
				for (j = -CROSSHALF; j < CROSSHALF; j++) {
					if (xx > CROSSHALF && xx < (I32)(width - CROSSHALF) && yy > CROSSHALF && yy < (I32) (height - CROSSHALF)) 
					{
						itmp = (int)*(bufimg + (xx+j) + (j+yy) * width);
						itmp = 0xFF - itmp;
						//itmp = 0xFF;
						*(bufimg + (xx+j) + (j+yy) * width) = (U08) (itmp & 0xFF);

						itmp = (int)*(bufimg + (xx+j) + (-j+yy) * width);
						itmp = 0xFF - itmp;
						//itmp = 0xFF;
						*(bufimg + (xx+j) + (-j+yy) * width) = (U08) (itmp & 0xFF);
					}
				}

				if (piana->himgfunc) {
					//				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)bufimg, width, height, 0, 1, 2, 3, piana->imguserparam);
					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)bufimg, width, height, 2, 1, 2, 3, piana->imguserparam);
				}
			}
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Shape..\n");

			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)(diffimgBinary), width, height, 4, 1, 2, 3, piana->imguserparam);
			}
			j++;		// fake7Aa
			frame++;

		} 		// for (m = 0; m < multitude; m++) 
	} 			// for (index = r0; index <= r1; index++) 

	free(bufimg);
	free(diffimg);
	free(diffimgFilter);
	free(diffimgBinary);
	free(pcpc->prefimgNoBall);
	free(bufgamma);

	pcpc->diffimg 	  = NULL;
	pcpc->diffimgFilter = NULL;
	pcpc->diffimgBinary = NULL;
	pcpc->prefimgNoBall = NULL;


	res = ClubPath_RefinePosition(piana, camid, pcpc);
	if (res ) {
		res = ClubPath_CalcClubData(piana, camid, pcpc);
	}
	if (res) {
		res = ClubPath_CalcSpin(piana, camid, pcpc);
	}

	if (res) {
		iana_shotresult_t *psr;
		iana_shotresult_t *pcsr;;

		//-----
		psr  = &piana->shotresultdata;
		pcsr = &piana->clubball_shotresult[camid];

		memcpy(pcsr, psr, sizeof(iana_shotresult_t));

		//

		pcsr->readflag 		= 0;
		pcsr->spinAssurance = 1;

		pcsr->sidespin 		= pcpc->sidespin;
		pcsr->backspin 		= pcpc->backspin;

		pcsr->axis.x 		= pcpc->backspin;
		pcsr->axis.y 		= 0;			// rollspin;
		pcsr->axis.z 		= -pcpc->sidespin;

		pcsr->spinmag 		= cr_vector_normalization(&pcsr->axis, &pcsr->axis);

		if (piana->camsensor_category != CAMSENSOR_CONSOLE1) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-------\n");

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "clubspdB: %lf -> %lf\n", pcsr->clubspeed_B, pcpc->clubspeedB);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "clubspdA: %lf -> %lf\n", pcsr->clubspeed_A, pcpc->clubspeedA);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "clubface: %lf -> %lf\n", pcsr->clubfaceangle, pcpc->clubfaceangle);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "clubpath: %lf -> %lf\n", pcsr->clubpath, pcpc->clubpath);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-------\n");

			pcsr->clubspeed_B 	= pcpc->clubspeedB;
			pcsr->clubspeed_A 	= pcpc->clubspeedA;
			pcsr->clubfaceangle	= pcpc->clubfaceangle;
			pcsr->clubpath    	= pcpc->clubpath;
		} else {
			pcpc->clubspeedB 	= pcsr->clubspeed_B;
			pcpc->clubspeedA 	= pcsr->clubspeed_A;
			pcpc->clubfaceangle	= pcsr->clubfaceangle;
			pcpc->clubpath    	= pcsr->clubpath;

		}
		pcsr->headerlenL	= pcpc->hlenEstimatedL;
		pcsr->hitoffsetL	= pcpc->hitoffsetL;
		pcsr->geareffect	= pcpc->geareffect;

		pcsr->width			= pcpc->width;
		pcsr->height		= pcpc->height;

		pcsr->valid			= 1;

		//------------------
		psr->clubfaceangle	= pcpc->clubfaceangle;
		psr->clubpath		= pcpc->clubpath;
		psr->clubspeed_A	= pcpc->clubspeedA;
		psr->clubspeed_B	= pcpc->clubspeedB;


		psr->clubAssurance  = 0.95;

		//--
		psr->clubattackangle = 0;
		psr->clubloftangle = 0;
		psr->clublieangle = 0;
		psr->clubfaceimpactLateral = 0;
		psr->clubfaceimpactVertical = 0;

		psr->Assurance_clubspeed_B 			= 90;
		psr->Assurance_clubspeed_A 			= 90;
		psr->Assurance_clubpath 			= 90;
		psr->Assurance_clubfaceangle 		= 85;
		psr->Assurance_clubattackangle		= 0;
		psr->Assurance_clubloftangle 		= 0;
		psr->Assurance_clublieangle 		= 0;
		psr->Assurance_clubfaceimpactLateral = 0;
		psr->Assurance_clubfaceimpactVertical = 0;
		//--
	} else {
		// Estimated clubspeed.. 
		iana_shotresult_t *psr;
		psr  = &piana->shotresultdata;
		psr->clubspeed_B = fabs(psr->clubspeed_B);
		psr->clubspeed_A = fabs(psr->clubspeed_A);
		if (psr->clubspeed_B == 0 || psr->clubspeed_A == 0) {
			psr->clubspeed_B = 2;
			psr->clubspeed_A = 1;
		}
#define CLUBSPEED_A_ADDVALUE		(1000.0)
		psr->clubspeed_A = psr->clubspeed_A + CLUBSPEED_A_ADDVALUE;

		psr->clubAssurance  = 0.50;


		//--
		psr->clubattackangle = 0;
		psr->clubloftangle = 0;
		psr->clublieangle = 0;
		psr->clubfaceimpactLateral = 0;
		psr->clubfaceimpactVertical = 0;

		psr->Assurance_clubspeed_B 			= 80;
		psr->Assurance_clubspeed_A 			= 80;
		psr->Assurance_clubpath 			= 80;
		psr->Assurance_clubfaceangle 		= 85;
		psr->Assurance_clubattackangle		= 0;
		psr->Assurance_clubloftangle 		= 0;
		psr->Assurance_clublieangle 		= 0;
		psr->Assurance_clubfaceimpactLateral = 0;
		psr->Assurance_clubfaceimpactVertical = 0;
		//--

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" CLUB_ESTIMATION   path: %lf  face: %lf  speedB: %lf  speedA: %lf\n",
				psr->clubpath,
				psr->clubfaceangle,
				psr->clubspeed_B,
				psr->clubspeed_A);
	}
	res = 1;
func_exit:
	return res;
#endif
}


 static I32	makeRef(iana_t *piana, U32 camid, iana_cam_t *pic, U32 width, U32 height, U32 normalbulk, U32 multitude, I32 r0, I32 r1)
{				// Remove the ready ball mark from the reference Image
#ifdef IPP_SUPPORT
	I32 res;

	float 		*pRefImgf;
	float 		*pimgf;

	IppiSize 	roisizef;
	IppiSize 	roisizef2;
	float 		alphavalue;
	U32 count;

	U08	*buf;
	I32	rindex;
	camif_buffer_info_t *pb;
	I32 j;

	//----------
	roisizef.width = width;
	roisizef.height = height;


	
	pRefImgf 	= (float *) malloc(width * height*sizeof(float));
	pimgf 		= (float *) malloc(width * height*sizeof(float));

	memset(pRefImgf, 0, width * height * sizeof(float));

	res = 0;
	buf = NULL;
	count = 0;
//#define MKREF_ADD	10
#if defined(MKREF_ADD)
	r0 -= MKREF_ADD;
	if (r0 < 0) r0 = 0;
	r1 += MKREF_ADD;
#endif
	
	for (rindex = r0; rindex <= r1; rindex++) {
		// 1) get image buffer
		if (piana->opmode == IANA_OPMODE_FILE) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
			if (res == 0) {
				continue;
			}
		} else {
			//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bulk, img read\n", i);
			for (j = 0; j < 20; j++) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
				if (res == 1) {
					break;
				}
				cr_sleep(1);
			}
			if (res == 0) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "read Fail.. [%d:%d]\n", rindex, j);
				continue;
			}
		}

		ippiConvert_8u32f_C1R(
				buf, width,
				pimgf, width * sizeof(float),
				roisizef);

		ippiAdd_32f_C1IR(
				pimgf, width*sizeof(float), 
				pRefImgf, 
				width*sizeof(float), 
				roisizef);

		count++;

		if (piana->himgfunc) {
			((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf, width, height, 0, 1, 2, 3, piana->imguserparam);
		}
	}


	alphavalue = 1.0f / (float) count;
	if (multitude > 1) {
		U32 i;
		roisizef2.width = width;
		roisizef2.height = height/multitude;
		for (i = 1; i < multitude; i++) {
			ippiAdd_32f_C1IR(
					pRefImgf + width *((height * i) /multitude),
					width*sizeof(float), 
					pRefImgf, 
					width*sizeof(float), 
					roisizef2);
		}
		alphavalue = alphavalue / multitude;
	}





	ippiMulC_32f_C1IR(alphavalue, pRefImgf, width*sizeof(float), roisizef);
	ippiConvert_32f8u_C1R (pRefImgf, width * sizeof(float), 
			pic->prefimg, width,
			roisizef, ippRndNear);

	free(pRefImgf);
	free(pimgf);

	return 1;
#else
    I32 res;

    cv::Mat cvRefImg, cvRefImgF, cvImgF;

    cv::Size roisizef, roisizefSlice;
    
    float 		alphavalue;
    U32 count;

    U08	*buf;
    I32	rindex;
    camif_buffer_info_t *pb;
    I32 j;

    //----------
    roisizef.width = width;
    roisizef.height = height;

    res = 0;
    buf = NULL;
    count = 0;

    cvRefImgF = cv::Mat(roisizef, CV_32FC1);
    cvRefImgF = 0;
    for (rindex = r0; rindex <= r1; rindex++) {
        cv::Mat cvImgBuf;
        // 1) get image buffer
        if (piana->opmode == IANA_OPMODE_FILE) {
            res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
            if (res == 0) {
                continue;
            }
        } else {
            for (j = 0; j < 20; j++) {
                res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
                if (res == 1) {
                    break;
                }
                cr_sleep(1);
            }
            if (res == 0) {
                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "read Fail.. [%d:%d]\n", rindex, j);
                continue;
            }
        }

        cvImgBuf = cv::Mat(roisizef, CV_8UC1, buf);
        cvImgBuf.convertTo(cvImgF, CV_32FC1);

        cvRefImgF += cvImgF;

        count++;

        if (piana->himgfunc) {
            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, width, height, 0, 1, 2, 3, piana->imguserparam);
        }
    }

    alphavalue = 1.0f / (float)count;
    if (multitude > 1) {
        U32 i;
        cv::Mat cvRefAbove, cvRefBelow;

        roisizefSlice.width = width;
        roisizefSlice.height = height/multitude;
    
        cvRefAbove = cvRefImgF(cv::Rect(0, 0, roisizefSlice.width, roisizefSlice.height));
        cvRefBelow = cvRefImgF(cv::Rect(0, roisizefSlice.height, roisizefSlice.width, height - roisizefSlice.height));

        for (i = 1; i < multitude; i++) {
            cv::Mat cvImgSlice;

            cvImgSlice = cvRefImgF(
                cv::Rect(0, (height * i) /multitude, 
                    roisizefSlice.width, roisizefSlice.height)
            );

            cvRefAbove += cvImgSlice;
        }
        alphavalue = alphavalue / multitude;
        
        cv::vconcat(std::vector<cv::Mat>({ cvRefAbove, cvRefBelow }), cvRefImgF);
    }

    cvRefImgF *= alphavalue;
    cvRefImgF.convertTo(cvRefImg, CV_8UC1);

    memcpy(pic->prefimg, cvRefImg.data, sizeof(U08) * cvRefImg.cols * cvRefImg.rows);

    return 1;
#endif
}


/*!
 ********************************************************************************
 *	@brief      Club image 3
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2019/0906
 *******************************************************************************/
static I32 ClubPath_ProcessImage3(iana_t *piana, U32 camid,
		U08 *buf, 
		clubpath_context_t *pcpc,
		U08 *diffimg,
		U08 *diffimgFilter,
		U08 *diffimgBinary
		)
{
#ifdef IPP_SUPPORT
	U32	width;
	U32	height;
	U32	offset_x;
	U32	offset_y;
	U32 gain;
	U32 exposure;

	IppiSize roisize;
	IppiSize mproisize;
	U32 mproioffset;

	iana_cam_t	*pic;

	Ipp8u threshold;
	Ipp64f mean;				// Ipp64f == double
	Ipp64f stdev;

	U32 frame;
	//-----
	pic 	= piana->pic[camid];

	width 		= pcpc->width;
	height 		= pcpc->height;
	offset_x 	= pcpc->offset_x;
	offset_y 	= pcpc->offset_y;

	gain 		= pcpc->gain;
	exposure 	= pcpc->exposure;

	roisize.width = pcpc->roiwidth;
	roisize.height = pcpc->roiheight;

	frame = pcpc->frame;
	ippiAbsDiff_8u_C1R(								// C = |A - B|
			pcpc->prefimgNoBall 	+ pcpc->imgoffset, width,				// B
			buf 			+ pcpc->imgoffset, width,					// A
			diffimg 		+ pcpc->imgoffset, width, 					// C, result.
			roisize);

	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pcpc->prefimgNoBall, width, height, 0, 1, 2, 3, piana->imguserparam);
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf, width, height, 2, 1, 2, 3, piana->imguserparam);
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)diffimg, width, height, 4, 1, 2, 3, piana->imguserparam);
	}

	{
		double ypos;
		double mm;

		U32 mulc;
		U32 scale;
		//-----

		ypos = pic->icp.L.y;

		//mm = 10;
		if (pcpc->aftershot) {
			//mm = 5;
			mm = 10;
		} else {
			mm = 10;
		}
		//mm = 20;

		getMulcScale((float)mm, &mulc, &scale);

		ippiMulC_8u_C1IRSfs(
				(Ipp8u)mulc, 
				diffimg 		+ pcpc->imgoffset, width, 					// C, result.
				roisize, 
				scale 		// nexp
				);
	}

	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)diffimg, width, height, 0, 1, 2, 3, piana->imguserparam);
	}

#define BOARDER	4
//#define BOARDER	10
	mproisize.width 	= pcpc->roiwidth - BOARDER;
	mproisize.height 	= pcpc->roiheight - BOARDER;

	memset(diffimgBinary, 0, width*height);
	mproioffset = pcpc->imgoffset + (BOARDER/2) + (BOARDER * width)/2;

	ippiFilterGauss_8u_C1R(
		diffimg			+ mproioffset, width, // source
		diffimgFilter	+ mproioffset, width, // dest
		mproisize,
		ippMskSize5x5
	);	


	ippiDilate3x3_8u_C1R(
			diffimgFilter			+ mproioffset, width, // source
			diffimgBinary	+ mproioffset, width, // dest
			mproisize
			);	
	ippiErode3x3_8u_C1IR(
			diffimgFilter + mproioffset, width, 
			mproisize
			);	

	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)diffimgFilter, width, height, 0, 1, 2, 3, piana->imguserparam);
	}

#if 1
	{
		double ts64deltad;

		ts64deltad = pcpc->tsd[frame];

	//	if (ts64deltad > 0) 
		{
			double pr_;
			double lr_;
			I32 xb, yb;
			I32 x0, y0, x1, y1;
			point_t L2; 	// for radius calc
			point_t P2; 	// for radius calc, pixel

			point_t bcL; 
			point_t bcP; 

			iana_cam_param_t *picp;
			//----
			picp = &piana->pic[camid]->icp;

			if (ts64deltad > 0) {
				//bcL.x	= picp->mx_ * ts64deltad + picp->bx_;
				//bcL.y	= picp->my_ * ts64deltad + picp->by_;
				//lr_ 	= picp->mr_ * ts64deltad + picp->br_;

				bcL.x	= picp->maLx[2] * ts64deltad*ts64deltad + picp->maLx[1] * ts64deltad + picp->maLx[0];
				bcL.y	= picp->maLy[2] * ts64deltad*ts64deltad + picp->maLy[1] * ts64deltad + picp->maLy[0];
				lr_		= picp->maLr[2] * ts64deltad*ts64deltad + picp->maLr[1] * ts64deltad + picp->maLr[0];
			} else {
				bcL.x = pic->icp.L.x;
				bcL.y = pic->icp.L.y;
				//lr_   = picp->br_ * 1.2;
				lr_   = picp->maLr[0] * 1.2;
			}
			iana_L2P(piana, camid, &bcL, &bcP, 0);

			//L2.x = bcL.x + BALLRADIUS*2;
			//L2.x = bcL.x + BALLRADIUS*1;
			L2.x = bcL.x + lr_;
			L2.y = bcL.y;
			iana_L2P(piana, camid, &L2, &P2, 0);

			pr_ = DDIST(bcP.x, bcP.y, P2.x, P2.y);

			pr_ = pr_ * 0.7;

			xb = (I32)(bcP.x  - offset_x);
			yb = (I32)(bcP.y  - offset_y);

			if (xb > 0 && xb < (int)width-2 && yb > 0 && yb < (int)height-1) {
				I32 i, j;
				x0 = xb - (I32)pr_;	if (x0 < 0) x0 = 0; if (x0 > (int)width-1) x0 = width-1;
				x1 = xb + (I32)pr_; if (x1 < 0) x1 = 0; if (x1 > (int)width-1) x1 = width-1;
				y0 = yb - (I32)pr_;	if (y0 < 0) y0 = 0; if (y0 > (int)height-1) y0 = height-1;
				//y1 = yb + (I32)(pr_ * 2);	if (y1 < 0) y1 = 0; if (y1 > (int)height-1) y1 = height-1;
				y1 = yb + (I32)pr_;	if (y1 < 0) y1 = 0; if (y1 > (int)height-1) y1 = height-1;
				for (i = y0; i < (int)y1; i++) {
					for (j = x0; j < (int)x1; j++) {
						*(diffimgFilter + j + i * width) = 0;
					}
				}

				if (piana->himgfunc) {
					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)diffimgFilter, width, height, 0, 1, 2, 3, piana->imguserparam);
				}
			}

		}
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "xb yb = %d %d\n", xb, yb);
	}
#endif

	ippiMean_StdDev_8u_C1R(
			diffimgFilter + mproioffset, width,
			mproisize, 
			&mean, 
			&stdev);

#define STDMULT	2.0
#define THRESHOLD_MAX	100
#define THRESHOLD_MIN	20
	threshold = (Ipp8u) (mean + stdev * STDMULT);
	if (threshold > THRESHOLD_MAX) {
		threshold = THRESHOLD_MAX;
	}
	if (threshold < THRESHOLD_MIN) {
		threshold = THRESHOLD_MIN;
	}

#define THRESHOLD_BALLCLUB	50
	memset(diffimgBinary, 0, width*height);

	ippiCompareC_8u_C1R(
		diffimgFilter + mproioffset, width, (Ipp8u)threshold,	// source
		diffimgBinary + mproioffset, width,	// binary image. dest.
		mproisize, ippCmpGreater
	);

	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)diffimgBinary, width, height, 0, 1, 2, 3, piana->imguserparam);
	}

	ippiErode3x3_8u_C1IR(
			diffimgBinary + mproioffset, width, 
			mproisize
			);	

	ippiDilate3x3_8u_C1IR(
			diffimgBinary + mproioffset, width, 
			mproisize
			);	

	if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)diffimg, width, height, 0, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)diffimgFilter, width, height, 2, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)diffimgBinary, width, height, 4, 1, 2, 3, piana->imguserparam);
	}

	return 1;
#else
    U32	width;
    U32	height;
    U32	offset_x, offset_y;
    U32	roiOffset_x, roiOffset_y;

    cv::Rect roiRect;
    cv::Size roisize;

    iana_cam_t	*pic;

    double threshold;

    U32 frame;

    cv::Mat cvImgNoBall, cvImgBuf, cvImgDiff;
    cv::Mat cvImgDiffCrop, cvImgMorph, cvImgBin, cvImgMorph2;
    //-----
    pic 	= piana->pic[camid];

    width 		= pcpc->width;
    height 		= pcpc->height;
    offset_x 	= pcpc->offset_x;
    offset_y 	= pcpc->offset_y;
    roiOffset_x = pcpc->imgoffset % width;
    roiOffset_y = pcpc->imgoffset / width;

    roiRect = cv::Rect(roiOffset_x, roiOffset_y, pcpc->roiwidth, pcpc->roiheight);
    roisize = roiRect.size();

    frame = pcpc->frame;

    cvImgNoBall = cv::Mat(cv::Size(width, height), CV_8UC1, pcpc->prefimgNoBall);
    cvImgBuf = (cv::Mat(cv::Size(width, height), CV_8UC1, buf));
    cv::absdiff(cvImgBuf(roiRect), cvImgNoBall(roiRect), cvImgDiff);

    // memcpy(diffimg, cvImgDiff.data, sizeof(U08) * width * height);
    memset(diffimgFilter, 0, sizeof(U08) * width * height);

    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pcpc->prefimgNoBall, width, height, 0, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgNoBall.data, cvImgNoBall.cols, cvImgNoBall.rows, 1, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, width, height, 2, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgBuf.data, cvImgBuf.cols, cvImgBuf.rows, 3, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgDiff.data, cvImgDiff.cols, cvImgDiff.rows, 4, 1, 2, 3, piana->imguserparam);
    }

    {
        double ypos;
        double mm;

        U32 mulc;
        U32 scale;
        //-----

        ypos = pic->icp.L.y;

        if (pcpc->aftershot) {
            mm = 10;
        } else {
            mm = 10;
        }

        getMulcScale((float)mm, &mulc, &scale);

        cvImgDiff = cvImgDiff * (U08)mulc / pow(2.0, scale);
    }

    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgDiff.data, cvImgDiff.cols, cvImgDiff.rows, 0, 1, 2, 3, piana->imguserparam);
    }

    cvImgDiff.copyTo(cvImgNoBall(roiRect));
    memcpy(diffimg, cvImgNoBall.data, width*height);

    // cvImgDiffCrop = cvImgDiff(cv::Rect(roiOffset_x, roiOffset_y, roisize.width, roisize.height));

    {
        cv::Mat cvImgFilter;

        cv::GaussianBlur(cvImgDiff, cvImgFilter, cv::Size(5, 5), 1.0, 1.0, cv::BORDER_ISOLATED);
        cv::morphologyEx(cvImgFilter, cvImgMorph, 
            cv::MORPH_CLOSE, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3,3)), 
            cv::Point(-1, -1), // anchor
            1, // iterations
            cv::BORDER_ISOLATED);

        if (piana->himgfunc) {
            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgMorph.data, cvImgMorph.cols, cvImgMorph.rows, 0, 1, 2, 3, piana->imguserparam);
        }
        cvImgMorph.copyTo(cvImgNoBall(roiRect));
    }

    {
        double ts64deltad;

        ts64deltad = pcpc->tsd[frame];

        {
            double pr_;
            double lr_;
            I32 xb, yb;
            I32 x0, y0, x1, y1;
            point_t L2; 	// for radius calc
            point_t P2; 	// for radius calc, pixel

            point_t bcL;
            point_t bcP;

            iana_cam_param_t *picp;
            //----
            picp = &piana->pic[camid]->icp;

            if (ts64deltad > 0) {
                bcL.x	= picp->maLx[2] * ts64deltad*ts64deltad + picp->maLx[1] * ts64deltad + picp->maLx[0];
                bcL.y	= picp->maLy[2] * ts64deltad*ts64deltad + picp->maLy[1] * ts64deltad + picp->maLy[0];
                lr_		= picp->maLr[2] * ts64deltad*ts64deltad + picp->maLr[1] * ts64deltad + picp->maLr[0];
            } else {
                bcL.x = pic->icp.L.x;
                bcL.y = pic->icp.L.y;
                lr_   = picp->maLr[0] * 1.2;
            }

            iana_L2P(piana, camid, &bcL, &bcP, 0);

            L2.x = bcL.x + lr_;
            L2.y = bcL.y;
            iana_L2P(piana, camid, &L2, &P2, 0);

            pr_ = DDIST(bcP.x, bcP.y, P2.x, P2.y);

            pr_ = pr_ * 0.7;

            xb = (I32)(bcP.x  - offset_x);
            yb = (I32)(bcP.y  - offset_y);

            if (xb > 0 && 
                xb < (int)width-2 && 
                yb > 0 && 
                yb < (int)height-1) 
            {
                cv::Rect coverArea;
                cv::Mat cover;
                x0 = xb - (I32)pr_;	if (x0 < 0) x0 = 0; if (x0 >(int)width-1) x0 = width-1;
                x1 = xb + (I32)pr_; if (x1 < 0) x1 = 0; if (x1 >(int)width-1) x1 = width-1;
                y0 = yb - (I32)pr_;	if (y0 < 0) y0 = 0; if (y0 >(int)height-1) y0 = height-1;
                y1 = yb + (I32)pr_;	if (y1 < 0) y1 = 0; if (y1 >(int)height-1) y1 = height-1;

                coverArea = cv::Rect(x0, y0, x1-x0+1, y1-y0+1);
                cover = cv::Mat(coverArea.size(), CV_8UC1);
                // cover = cv::Mat(cv::Size(x1 - x0 + 1, y1 - y0 + 1), CV_8UC1);
                cover = 0;
                //coverArea.x = x0 - roiOffset_x;
                //coverArea.y = y0 - roiOffset_y;
                //coverArea.width = cover.cols;
                //coverArea.height = cover.rows;
                cover.copyTo(cvImgNoBall(coverArea));

                if (piana->himgfunc) {
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgNoBall.data, cvImgNoBall.cols, cvImgNoBall.rows, 0, 1, 2, 3, piana->imguserparam);
                }
            }
        }
    }

    memcpy(diffimgFilter, cvImgNoBall.data, sizeof(U08) * cvImgNoBall.cols * cvImgNoBall.rows);

    {
        cv::Mat cvMean, cvStdev;
        cv::meanStdDev(cvImgNoBall(roiRect), cvMean, cvStdev);

#define STDMULT	2.0
#define THRESHOLD_MAX	100
#define THRESHOLD_MIN	20
        threshold = cvMean.at<double>(0) + cvStdev.at<double>(0)* STDMULT;
        if (threshold > THRESHOLD_MAX) {
            threshold = THRESHOLD_MAX;
        }
        if (threshold < THRESHOLD_MIN) {
            threshold = THRESHOLD_MIN;
        }
    }

#define THRESHOLD_BALLCLUB	50

    cv::threshold(cvImgNoBall(roiRect), cvImgBin, threshold, 255.0, cv::THRESH_BINARY);

    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgBin.data, cvImgBin.cols, cvImgBin.rows, 0, 1, 2, 3, piana->imguserparam);
    }

    cv::morphologyEx(cvImgBin, cvImgMorph2, 
        cv::MORPH_OPEN, cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)),
        cv::Point(-1, -1), // anchor
        1, // iterations
        cv::BORDER_ISOLATED);

    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgMorph2.data, cvImgMorph2.cols, cvImgMorph2.rows, 0, 1, 2, 3, piana->imguserparam);
    }

    cvImgMorph2.copyTo(cvImgNoBall(roiRect));
    memcpy(diffimgBinary, cvImgNoBall.data, width*height);


    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)diffimg, width, height, 0, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)diffimgFilter, width, height, 2, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)diffimgBinary, width, height, 4, 1, 2, 3, piana->imguserparam);
    }
    return 1;
#endif
}

/*!
 ********************************************************************************
 *	@brief      Club
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2017/0405
 *******************************************************************************/
static I32 ClubPath_UpdateContext(iana_t *piana, U32 camid,	
		clubpath_context_t 	*pcpc)
{
	ClubPath_CalcRoi(piana, camid, pcpc);

	return 1;
}

/*!
 ********************************************************************************
 *	@brief      Club
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2017/0405
 *******************************************************************************/
static I32 ClubPath_CalcPosition(	
		iana_t *piana, 
		U32 camid, 
		clubpath_context_t *pcpc)
{
	//I32 index;
	I32 frame;

//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " yyy 1\n");
	ClubPath_CalcPosition_Hough(piana, camid, pcpc);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " yyy 2\n");
	ClubPath_CalcPosition_Shape(piana, camid, pcpc);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " yyy 3\n");
	ClubPath_CalcPosition_RefineShape(piana, camid, pcpc);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " yyy 4\n");

	//index = pcpc->indexm;
	frame = pcpc->frame;

	/*
	if (index >= (I32)pcpc->shotindex && pcpc->preshotgood == 0) {
		pcpc->aftershot = 1;
	} else {
		ClubPath_FilterPreshot(piana, camid, pcpc);
	}
	*/

	if (pcpc->aftershot == 0) {
		ClubPath_FilterPreshot(piana, camid, pcpc);
	}

#if defined(CLUB3_TEST)
	if (frame >= (I32)pcpc->shotframe +1 && pcpc->preshotgood == 1) {
		pcpc->aftershot = 1;
	}
#else
	if (frame >= (I32)pcpc->shotframe && pcpc->preshotgood == 1) {
		pcpc->aftershot = 1;
	}
#endif

//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " yyy 4\n");



	return 1;
}

/*!
 ********************************************************************************
 *	@brief      Club
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2017/0405
 *******************************************************************************/
static I32 ClubPath_FilterPreshot(	
		iana_t *piana, 
		U32 camid, 
		clubpath_context_t *pcpc)
{
	I32 res;
	//I32 index;
	I32 frame;
	regressionx_t rgx;

	double xx[MARKSEQUENCELEN], yy[MARKSEQUENCELEN];
	I32 count;
	U32 right0left1;

	//------------
	right0left1 = piana->Right0Left1;
	//---

	frame = pcpc->frame;

	rgx.frame0 = 0;
	rgx.frame1 = pcpc->frame;
	rgx.usequadratic = 0;
	
	rgx.recalcshotframe = 1;
	res = ClubPath_RegressPosition(piana, camid, pcpc, &rgx);
	pcpc->maiitt[2] = rgx.maiitt[2];
	pcpc->maiitt[1] = rgx.maiitt[1];
	pcpc->maiitt[0] = rgx.maiitt[0];
	if (res) {
		I32 j;
		double xj_;
		double m_, b_;
		double myxa[3];
		double r2;


		m_ = rgx.ma[1];
		b_ = rgx.ma[0];
	
		pcpc->preshot_m_ = m_;
		pcpc->preshot_b_ = b_;
		pcpc->shotframecombined = rgx.shotframecombined;

		//for (j = 0; j <= index; j++) {
		//	xj_ = m_ * j + b_;
		//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%2d  %4d\n", j, (int) xj_);
		//}
		UNUSED(xj_);
		count = 0;
		for (j = 0; j <= frame; j++) {
			if (pcpc->hsexist[j] == IANA_CLUB_EXIST) {
				xx[count] = (int)pcpc->hsx[j];					// '+' Shaped..
				yy[count] = (int)pcpc->hsy[j];
				count++;
			}
		}
#define DISCARDCOUNT1 0
		if (count > 3 + DISCARDCOUNT) 
		{
			res = caw_quadraticregression(xx+DISCARDCOUNT, yy+DISCARDCOUNT, count-DISCARDCOUNT-DISCARDCOUNT1, &myxa[2], &myxa[1], &myxa[0], &r2);
			res = 0;	// XXX
			if (right0left1 == 0) {			// RIGHT HANDED
				if (res == 0 || myxa[2] < 0.0) {
					myxa[2] = 0.0;
					cr_regression(xx+DISCARDCOUNT, yy+DISCARDCOUNT, count-DISCARDCOUNT-DISCARDCOUNT1, &myxa[1], &myxa[0]);
				}
			} else { 						// LEFT HANDED
				if (res == 0 || myxa[2] > 0.0) {
					myxa[2] = 0.0;
					cr_regression(xx+DISCARDCOUNT, yy+DISCARDCOUNT, count-DISCARDCOUNT-DISCARDCOUNT1, &myxa[1], &myxa[0]);
				}
			}

			pcpc->preshot_myxa[2] = myxa[2];
			pcpc->preshot_myxa[1] = myxa[1];
			pcpc->preshot_myxa[0] = myxa[0];
			res = 1;
		} else {
			pcpc->preshot_myxa[2] = 0;
			pcpc->preshot_myxa[1] = 0;
			pcpc->preshot_myxa[0] = 0;
			res = 0;
		}
	}

	pcpc->preshotgood = res;
	return res;
}

////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////// HERE I MISSED SOME FUNCTIONS TO WORK ///////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
/*!
 ********************************************************************************
 *	@brief      Club
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2017/0405
 *******************************************************************************/
static I32 ClubPath_CalcPosition_Hough(	
		iana_t *piana, 
		U32 camid, 
		clubpath_context_t *pcpc)
{
#ifdef IPP_SUPPORT
	//U32 index;
	U32 frame;
	U32	offset_x;
	U32	offset_y;

	U32	width;
	U32	height;
	U32 startx, starty;
	IppiSize roisize;
	U32 imgoffset;

	U32	widthT;
	U32	heightT;
	U32 startxT, startyT;
	IppiSize roisizeT;
	U32 imgoffsetT;

	U32	widthT2;
	U32	heightT2;
	U32 startxT2, startyT2;
	IppiSize roisizeT2;
	U32 imgoffsetT2;

	U08 *pimg;
	U08 *pimgT;
	U08 *pimgT2;
	
	U08 *pimgProc;

	double	xfact;
	double	yfact;

	U32 right0left1;

	//------------
	right0left1 = piana->Right0Left1;

	pimgProc = pcpc->diffimgBinary;
	//pimgProc = pcpc->diffimgFilter;
	offset_x 	= pcpc->offset_x;
	offset_y 	= pcpc->offset_y;
	//index = pcpc->indexm;
	frame = pcpc->frame;

	width 		= pcpc->width;
	height 		= pcpc->height;
	roisize.width 	= pcpc->roiwidth;
	roisize.height 	= pcpc->roiheight;
	startx		= pcpc->startx;
	starty		= pcpc->starty;
	imgoffset	= startx + starty * width;

	widthT 		= height;
	heightT 	= width;
	roisizeT.width = pcpc->roiheight;
	roisizeT.height = pcpc->roiwidth;
	startxT		= starty;
	startyT		= startx;
	imgoffsetT	= startxT + startyT * widthT;

	pimg = (U08 *) malloc(width * height);
	pimgT = (U08 *) malloc(width * height);
	pimgT2 = (U08 *) malloc(width * height);

#define XFACT		(1.0/4.0)
#define YFACT		(1.0/4.0)
	xfact = XFACT;
	yfact = YFACT;

	widthT2 		= (U32) (widthT * xfact + 0.1);
	heightT2 		= (U32) (heightT * yfact + 0.1);
	roisizeT2.width = (U32) (roisizeT.width * xfact + 0.1);
	roisizeT2.height = (U32) (roisizeT.height * yfact + 0.1);
	startxT2		= (U32) (startxT * xfact + 0.1);
	startyT2		= (U32) (startyT * yfact + 0.1);
	imgoffsetT2		= startxT2 + startyT2 * widthT2;
	
	{
		IppPointPolar delta;
		int maxLineCount;
		int BufSize;
		Ipp8u* pBuffer;
		IppPointPolar Line[MAXLINECOUNT]; 
		int LineCount;
		IppPointPolar dstRoi[2]; 
		IppStatus ippstatus;
		//U32 i;
		I32 i;



		/*
#define FROM_RHO		1
#define FROM_DEGREE		1

#define TO_RHO			200
#define TO_DEGREE		180

#define DELTA_RHO		2.0
#define DELTA_DEGREE	2.0
*/

#define FROM_RHO		1
#define FROM_DEGREE		20

#define TO_RHO			200
#define TO_DEGREE		120

#define DELTA_RHO		2.0
#define DELTA_DEGREE	2.0


		dstRoi[0].rho 	= (float) FROM_RHO;
		dstRoi[0].theta = (Ipp32f) (float) DEGREE2RADIAN(FROM_DEGREE);

		dstRoi[1].rho 	= (float) TO_RHO;
		dstRoi[1].theta = (Ipp32f) (float) DEGREE2RADIAN(TO_DEGREE);

		delta.rho 	= (float) DELTA_RHO;
		delta.theta = (float) DEGREE2RADIAN(DELTA_DEGREE);
		maxLineCount = MAXLINECOUNT;

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "### h  #1\n");
		ippstatus = ippiHoughLineGetSize_8u_C1R(
				roisizeT, 
				delta,
				maxLineCount, 
				&BufSize);

		//pBuffer = ippsMalloc_8u(BufSize);
		pBuffer = (U08 *)malloc(BufSize);
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "### h  #2\n");

		ippstatus = ippiTranspose_8u_C1R(
				pimgProc + imgoffset, width,
				pimgT + imgoffsetT, widthT,
				roisize);
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "### h  #2_1\n");

		{	
			//3)  Resize 4x4
			IppiSize ssize;
			IppiRect sroi, droi;
			int sstep, dstep;
			IppStatus status;
			U08		*resizebuf;
			int	bufsize;
			//			double rr;

			//------
#if 1
			ssize.width		= widthT;
			ssize.height	= heightT;
			//sroi.x 			= pcpc->starty;
			//sroi.y 			= pcpc->startx;
			sroi.x 			= 0;
			sroi.y 			= 0;
			sroi.width 		= roisizeT.width;
			sroi.height 	= roisizeT.height;

			//droi.x 			= pcpc->starty/4;
			//droi.y 			= pcpc->startx/4;
			droi.x 			= 0;
			droi.y 			= 0;
			droi.width 		= roisizeT2.width;
			droi.height 	= roisizeT2.height;

			sstep 			= widthT;
			dstep 			= widthT2;
#endif

			ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_CUBIC, &bufsize);
			//resizebuf = ippsMalloc_8u(bufsize);
			resizebuf = (U08 *)malloc(bufsize);

			status = ippiResizeSqrPixel_8u_C1R(
					//pimgMirror + pcpc->imgoffset,
					pimgT + imgoffsetT,
					ssize,
					sstep,
					sroi,
					//
					//(Ipp8u*) pimgT2 + (pcpc->starty/4 + (pcpc->startx * dstep)/4),
					(Ipp8u*) pimgT2 + imgoffsetT2,
					dstep,
					droi,
					//
					xfact,
					yfact,
					0,			// shiftx
					0,			// shift
					IPPI_INTER_CUBIC,
					(Ipp8u*) resizebuf
					);

			free(resizebuf);
//#define HOUGHTHRESHOLD	20
#define HOUGHTHRESHOLD	15
			ippstatus = ippiHoughLine_Region_8u32f_C1R(
					//(Ipp8u*) pimgT2 + (pcpc->starty/4 + (pcpc->startx * dstep)/4), dstep,
					(Ipp8u*) pimgT2 + imgoffsetT2, dstep,
					roisizeT2, 
					Line, 
					dstRoi,
					MAXLINECOUNT,
					&LineCount, 
					delta, 
					HOUGHTHRESHOLD,						// threshold
					pBuffer);
		}
		//ippsFree(pBuffer);
		free(pBuffer);


		/*
		if (LineCount > 0) {
			for (i = 0; i < (U32)LineCount; i++) {
				//Line[i].theta = (Ipp32f) (M_PI -  Line[i].theta);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
						"[%d]rho: %f, theta: %f (%f)\n",
						i,
						Line[i].rho, Line[i].theta, 
						(float)(Line[i].theta * 180.0f / M_PI));
			}
		} else {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "LineCount: 0..\n");
		}
		*/

		pcpc->linecount[frame] = LineCount;
		//for (i = 0; i < (U32) LineCount; i++) 
		for (i = 0; i < (I32) LineCount; i++) 
		{
			double m_, b_;
			double theta, rho;

			//----
			// cos A (X~-X~0) + sin A (Y~-Y~0) = r
			// X~ = Y, Y~ = X,
			// -> cos A (Y - Y0) + sin A(X - X0) = r
			// -> sin A (X - X0) = - cos A  (Y-Y0) + r
			// -> X = - (cos A / sin A) * (Y-Y0) + r / sin A + X0
			// -> X =   m_ * Y - m_ * Y0 + r / sin A + X0
			// ->   =   m_ * Y + b_
			// where m_ = - (cos A / sin A),
			//       b_ = -m_ * Y0 + (r / sin A) / (xfact or yfact) + X0
			//       
			//         (Y0 = startxT2 /xfact, X0 = startyT2 / yfact)

			theta 	= Line[i].theta;
			rho 	= Line[i].rho;
			m_ 		= - cos(theta)/sin (theta);
			b_ 		= - m_ * (startxT2 / xfact) + (rho / sin(theta)) / xfact + (startyT2 / yfact);

			pcpc->theta_[frame][i] = theta;					// theta[i] = theta_[i] - M_PI;
			pcpc->m_[frame][i] = m_;								// x = m_ * y + b_;
			pcpc->b_[frame][i] = b_;								
		}

//#define M_MIN		(1.0)
//#define M_MIN		(2.0)
#define M_MIN		(5.0)
//#define X0_WIDTH	10
//#define X1_WIDTH	 5
//#define Y0_WIDTH	10
//#define Y1_WIDTH	 5
//#define X0_WIDTH	30

#define X0_WIDTH	40
#define X1_WIDTH	10

//#define Y0_WIDTH	30
#define Y0_WIDTH	40
#define Y1_WIDTH	10
#if 1
		// Count hough projection value of Y.
		memset(&pcpc->houghpvalue0[frame][0], 0, sizeof(U32) * FULLHEIGHT);
		memset(&pcpc->houghpvalue1[frame][0], 0, sizeof(U32) * FULLHEIGHT);
		if (pcpc->linecount[frame] > 0) {
			int x, y;
			int xx, xx0, xx1;
			int yy, yy0, yy1;
			double m_, b_;
			int xxPrev, yyPrev;
			
			m_ = pcpc->m_[frame][0];
			b_ = pcpc->b_[frame][0];
			xxPrev = -1;
			yyPrev = -1;
			if (m_ > -M_MIN && m_ < M_MIN) {
				for (y = pcpc->starty; y <= (int)pcpc->endy; y++) {
					x = (int) (m_ * y + b_);

					xx0 = x - X0_WIDTH;
					xx1 = x + X1_WIDTH;
					if (xx0 < pcpc->startx)	{ xx0 = pcpc->startx; }
					if (xx0 > pcpc->endx)	{ xx0 = pcpc->endx; }
					if (xx1 < pcpc->startx)	{ xx1 = pcpc->startx; }
					if (xx1 > pcpc->endx)	{ xx1 = pcpc->endx; }

					for (xx = xx0; xx <= xx1; xx++) {
						yy = (int) (-m_ * (xx - x) + y);
						if (yy >= pcpc->starty &&  yy <= (int)pcpc->endy) {
							if (*(pimgProc + xx + yy * width)) {
							//	if (yy != yyPrev) 
								{
									if (xx <= x) {		// xx0 ~ x
										pcpc->houghpvalue0[frame][y]++;
									} else {			// x   ~ xx1
										pcpc->houghpvalue1[frame][y]++;
									}
								}
								yyPrev = yy;
							}
						}
					}
				}
			} else {
				for (x = pcpc->startx; x <= (int)pcpc->endx; x++) {
					y = (int) ((1.0/m_) * (x - b_));

					yy0 = y - Y0_WIDTH;
					yy1 = y + Y1_WIDTH;
					if (yy0 < pcpc->starty)	{ yy0 = pcpc->starty; }
					if (yy0 > pcpc->endy)	{ yy0 = pcpc->endy; }
					if (yy1 < pcpc->starty)	{ yy1 = pcpc->starty; }
					if (yy1 > pcpc->endy)	{ yy1 = pcpc->endy; }

					for (yy = yy0; yy <= yy1; yy++) {
						xx = (int) ((-1.0/m_) * (yy - y) + x);
						if (xx >= pcpc->startx &&  xx <= (int)pcpc->endx) {
							if (*(pimgProc + xx + yy * width)) {
							//	if (xx != xxPrev) 
								{
									if (xx <= x) {		// xx0 ~ x
										pcpc->houghpvalue0[frame][y]++;
									} else {			// x   ~ xx1
										pcpc->houghpvalue1[frame][y]++;
									}
									//if (yy <= y) {		// yy0 ~ y
									//	pcpc->houghpvalue0[frame][x]++;
									//} else {			// y   ~ yy1
									//	pcpc->houghpvalue1[frame][x]++;
									//}
								}
								xxPrev = xx;
							}
						}
					}
				} 	// for (x = pcpc->startx; x <= (int)pcpc->endx; x++) 
			} 		// if (m_ > -M_MIN && m_ < M_MIN) 
		}

		// Count projection value of X, Y
		memset(&pcpc->houghXpvalue[frame][0], 0, sizeof(U32) * FULLWIDTH);
		memset(&pcpc->houghYpvalue[frame][0], 0, sizeof(U32) * FULLHEIGHT);
		if (pcpc->linecount[frame] > 0) {
			int x, y;
			int xx, xx0, xx1;
			int yy, yy0, yy1;
			double m_, b_;
			
			m_ = pcpc->m_[frame][0];
			b_ = pcpc->b_[frame][0];


			if (m_ > -0.01 && m_ < 0.01) {
				m_ = 0.01;
			}
			for (x = pcpc->startx; x <= (int)pcpc->endx; x++) {
				y = (int) ((x - b_) / m_);
				yy0 = y - Y0_WIDTH;
				yy1 = y + Y1_WIDTH;
				if (yy0 < pcpc->starty)	{ yy0 = pcpc->starty; }
				if (yy0 > pcpc->endy)	{ yy0 = pcpc->endy; }
				if (yy1 < pcpc->starty)	{ yy1 = pcpc->starty; }
				if (yy1 > pcpc->endy)	{ yy1 = pcpc->endy; }

				for (yy = yy0; yy <= yy1; yy++) {
					if (*(pimgProc + x + yy * width)) {
						pcpc->houghXpvalue[frame][x]++;
					}
				}
			}


			for (y = pcpc->starty; y <= (int)pcpc->endy; y++) {
				x = (int) (m_ * y + b_);

				xx0 = x - X0_WIDTH;
				xx1 = x + X1_WIDTH;
				if (xx0 < pcpc->startx)	{ xx0 = pcpc->startx; }
				if (xx0 > pcpc->endx)	{ xx0 = pcpc->endx; }
				if (xx1 < pcpc->startx)	{ xx1 = pcpc->startx; }
				if (xx1 > pcpc->endx)	{ xx1 = pcpc->endx; }

				for (xx = xx0; xx <= xx1; xx++) {
					if (*(pimgProc + xx + y * width)) {
						pcpc->houghYpvalue[frame][y]++;
					}
				}
			}
		}
#endif

		if (piana->himgfunc) {
			int x, y;
			int x0;
			double m_, b_;
			int xx0, xx1;
			int yy0, yy1;
//			double theta, rho;

			//--
			memcpy(pimg, pimgProc, width*height);

			//for (i = 0; i < (U32) pcpc->linecount[frame]; i++) 
			for (i = 0; i < (I32) pcpc->linecount[frame]; i++) 
			{
				if (i > 0) break;
//				theta 	= pcpc->theta_[i];
//				rho 	= Line[i].rho;
				m_ 		= pcpc->m_[frame][i];
				b_ 		= pcpc->b_[frame][i];

				//for (y = 0; y < (int)height; y++) 
				for (y = pcpc->starty; y <= (int)pcpc->endy; y++) 
				{
					x = (int) (m_ * y + b_);
					if (x >= pcpc->startx && x <= (int) pcpc->endx) {
						*(pimg + x + y * width) = (U08)(0xFF - i * 0x20);
					}

					if (m_ > -M_MIN && m_ < M_MIN) {
						xx0 = x - X0_WIDTH;
						if (xx0 >= pcpc->startx && xx0 <= (int) pcpc->endx) {
							*(pimg + xx0 + y * width) = (U08)(0xFF - i * 0x20);
						}

						xx1 = x + X1_WIDTH;
						if (xx1 >= pcpc->startx && xx1 <= (int) pcpc->endx) {
							*(pimg + xx1 + y * width) = (U08)(0xFF - i * 0x20);
						}
					} else {
						yy0 = y - Y0_WIDTH;
						if (yy0 >= pcpc->starty && yy0 <= (int) pcpc->endy) {
							*(pimg + x + yy0 * width) = (U08)(0xFF - i * 0x20);
						}

						yy1 = y + Y1_WIDTH;
						if (yy1 >= pcpc->starty && yy1 <= (int) pcpc->endy) {
							*(pimg + x + yy1 * width) = (U08)(0xFF - i * 0x20);
						}
					}


					if (i == 0) {
						int xx;
						int yy;
						//-- THICK line
						x0 = x - 1;
						if (x0 >= pcpc->startx && x0 <= (int) pcpc->endx) {
							*(pimg + x0 + y * width) = (U08)(0xFF - i * 0x20);
						}
						x0 += 2;
						if (x0 >= pcpc->startx && x0 <= (int) pcpc->endx) {
							*(pimg + x0 + y * width) = (U08)(0xFF - i * 0x20);
						}

						//---
						if (y == (int)((pcpc->starty + pcpc->endy)/2)) {
							if (m_ > -M_MIN && m_ < M_MIN) {
								//for (xx = 0; xx < (int)width; xx++) 
								for (xx = pcpc->startx; xx <= (int)pcpc->endx; xx++) 
								{
									yy = (int) (-m_ * (xx - x) + y);
									//if (yy >= 0 && yy < (int) height) 
									if (yy >= pcpc->starty && yy <= (int) pcpc->endy) 
									{
										*(pimg + xx + yy * width) = (U08)(0xFF);
									}
								}
							} else {
								//for (xx = 0; xx < (int)width; xx++) 
								for (yy = pcpc->starty; yy <= (int)pcpc->endy; yy+= 5)
								{
									xx = (int) ((-1.0/m_) * (yy - y) + x);
									if (xx >= pcpc->startx && xx <= (int) pcpc->endx) 
									{
										*(pimg + xx + yy * width) = (U08)(0xFF);
									}
								}
							}
							//----
						}
						for (yy = 0; yy < (int)height; yy++) {
							/*
							xx = pcpc->houghpvalue0[frame][yy];
							*(pimg + xx + yy * width) = (U08)(0xFF);
							*/

							/*
							xx = pcpc->houghpvalue1[frame][yy];
							*(pimg + xx + yy * width) = (U08)(0xFF);
							*/

							xx = pcpc->houghpvalue0[frame][yy] + pcpc->houghpvalue1[frame][yy];
							*(pimg + xx + yy * width) = (U08)(0xFF);
						}

						for (yy = 0; yy < (int)height; yy++) {
							xx = width - pcpc->houghYpvalue[frame][yy] - 1;
							if (xx >= 0 && xx < (int)width) {
								*(pimg + xx + yy * width) = (U08)(0xFF);
							}
						}

						for (xx = 0; xx < (int)width; xx++) {
							yy = pcpc->houghXpvalue[frame][xx];
							*(pimg + xx + yy * width) = (U08)(0xFF);
						}

					} 	// if (i == 0) 
				} 		// for (y = pcpc->starty; y <= (int)pcpc->endy; y++) 
			} 			// for (i = 0; i < (U32) pcpc->linecount[frame]; i++) 
			{
				for (x = 0; x < (int) width; x += 10) {
					for (y = 0; y < (int) height; y++) {
						xx0 = *(pimg + x + y * width);
						x0 = 0xFF - xx0;
						x0 = (xx0 + x0) / 2;
						*(pimg + x + y * width) = (U08) x0;
					}
				}
				for (x = 0; x < (int) width; x++) {
					for (y = 0; y < (int) height; y += 10){
						xx0 = *(pimg + x + y * width);
						x0 = 0xFF - xx0;
						x0 = (xx0 + x0) / 2;
						*(pimg + x + y * width) = (U08) x0;
					}
				}

				for (x = 1; x < (int) width; x += 50) {
					for (y = 0; y < (int) height; y++) {
						xx0 = *(pimg + x + y * width);
						x0 = 0xFF - xx0;
						//x0 = (xx0 + x0) / 2;
						*(pimg + x + y * width) = (U08) x0;
					}
				}
				for (x = 0; x < (int) width; x++) {
					for (y = 1; y < (int) height; y += 50){
						xx0 = *(pimg + x + y * width);
						x0 = 0xFF - xx0;
						//x0 = (xx0 + x0) / 2;
						*(pimg + x + y * width) = (U08) x0;
					}
				}
			}
            if (piana->himgfunc) {
			((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimg, width, height, 1, 1, 2, 3, piana->imguserparam);
		}
	}
	}

	free(pimg);
	free(pimgT);
	free(pimgT2);

#define HPV0		3
//#define HPV1		10
#define HPV1		15
#define LEN1MIN		10
#define LEN1MAX		70
#define LEN1MERCY	5
#define HPVCOUNTMIN	10
#define HPVSHAFTCOUNTMIN	20
	if (pcpc->linecount[frame] > 0) {
		int i;
		int y;
		int hpv;
		int hpvsum;
		int hpvcount;
		int len0, len1;
		int hpvshaft;
		int checkstate;
		int sx, sy;
		int len1max;
		int len1maxsy;
		int hpv0, hpv1;
		
		
		for (i = 0; i < pcpc->linecount[frame]; i++) {
			//get Club Last point

			checkstate = 0;
			len0 = 0;
			len1 = 0;

			hpvsum 	 = 0;
			hpvcount = 0;
			hpvshaft  = -1;
			if (right0left1 == 0) {		// Right handed
				for (y = pcpc->endy; y >= pcpc->starty; y--) {
					hpv = pcpc->houghpvalue0[frame][y] + pcpc->houghpvalue1[frame][y];
					if (hpv > HPV0 && hpv < HPV1) {
						hpvsum+= hpv;
						hpvcount++;
					}
				}
			} else {				// Left handed
				for (y = pcpc->endy; y <= pcpc->starty; y--) {
					hpv = pcpc->houghpvalue0[frame][y] + pcpc->houghpvalue1[frame][y];
					if (hpv > HPV0 && hpv < HPV1) {
						hpvsum+= hpv;
						hpvcount++;
					}
				}
			}
			if (hpvcount > HPVSHAFTCOUNTMIN) {
				hpvshaft = (int)((double)hpvsum / hpvcount);
			}
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "hpvshaft: %d, hpvcount: %d\n", hpvshaft, hpvcount);

			sy = -1;
			len1max = -1;

			if (hpvshaft > 0) {
				hpv0 = hpvshaft;
			} else {
				hpv0 = HPV0;
			}
			hpv1 = HPV1;
			hpvsum = 0;
			if (right0left1 == 0) {		// Right handed
				for (y = pcpc->starty; y <= pcpc->endy; y++) {
					hpv = pcpc->houghpvalue0[frame][y] + pcpc->houghpvalue1[frame][y];
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "--checkstate: %d, y: %d, len0: %d, len1: %d, hpv: %d \n", checkstate, y, len0, len1, hpv);
					switch (checkstate) {
						case 0:
							if (hpv > hpv0) {
								checkstate = 1;
								len0 = 0;
								len1 = 1;
								sy = y;
								hpvsum+= hpv;
							} else {
								len0++;
							}
							break;
						case 1:
							if (hpv > hpv0) {
								len1++;
								hpvsum+= hpv;
								if (len1max < len1) {
									len1max = len1;
									len1maxsy = sy;
								}
							} else {
								len0++;
								if (len0 > HPVCOUNTMIN) {
									checkstate = 2;
									break;
								}
							}
							if (len1 > LEN1MAX) {
								checkstate = 2;
							}
							break;
						default:
							checkstate = 0;
							break;
					}

					if (checkstate == 2) {
						break;
					}
				}
			} else {				// LEFT
				for (y = pcpc->endy; y >= pcpc->starty; y--) {
					hpv = pcpc->houghpvalue0[frame][y] + pcpc->houghpvalue1[frame][y];
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "--checkstate: %d, y: %d, len0: %d, len1: %d, hpv: %d \n", checkstate, y, len0, len1, hpv);
					switch (checkstate) {
						case 0:
							if (hpv > hpv0) {
								checkstate = 1;
								len0 = 0;
								len1 = 1;
								sy = y;
								hpvsum+= hpv;
							} else {
								len0++;
							}
							break;
						case 1:
							if (hpv > hpv0) {
								len1++;
								hpvsum+= hpv;
								if (len1max < len1) {
									len1max = len1;
									len1maxsy = sy;
								}
							} else {
								len0++;
								if (len0 > HPVCOUNTMIN) {
									checkstate = 2;
									break;
								}
							}
							if (len1 > LEN1MAX) {
								checkstate = 2;
							}
							break;
						default:
							checkstate = 0;
							break;
					}

					if (checkstate == 2) {
						break;
					}
				}
			}

		//	if (i == 0) 
			{
				double m_, b_;
				I32 hpexist = 0;

				if (checkstate != 2) {
					sx = -1;
					sy = -1;
					hpexist = IANA_CLUB_EMPTY;
				} else {
					hpexist = IANA_CLUB_EXIST;
				}

				if (len1 < 1) len1 = 1;
				m_ = pcpc->m_[frame][i];
				b_ = pcpc->b_[frame][i];
				sx = (int) (m_ * sy + b_);
//				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]sx, sy: %d, %d\n", i, sx, sy);
//				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "    len: %d endy  : %d, hpv: %d\n", len1, (sy+len1), (int) (hpvsum/len1));
//				hpv = pcpc->houghpvalue0[frame][y] + pcpc->houghpvalue1[frame][y];
				pcpc->hx[frame][i] = sx;
				pcpc->hy[frame][i] = sy;
				//pcpc->hpv[frame][i] = (int)(hpvsum/len1);
				//pcpc->hlen[frame][i] = (int)len1;
				pcpc->hpexist[frame][i] = hpexist;
				if (pcpc->aftershot == 0) {
					iana_cam_t		*pic;
					pic 		= piana->pic[camid];

					if (sx < pic->icp.P.x - pcpc->offset_x - 10) {
						pcpc->hpexist[frame][i] = IANA_CLUB_EMPTY;
					}
				}
			}
		}
	}
	UNUSED(piana);
	UNUSED(camid);
	return 1;
    
#else
	U32 frame;
	U32	offset_x;
	U32	offset_y;

	U32	width;
	U32	height;
    cv::Size imgSize;

	U32 startx, starty;
	U32 imgoffset;
    cv::Rect roiRect;
    cv::Size roisize;

	U32	widthT;
	U32	heightT;

	U32 startxT, startyT;
	U32 imgoffsetT;
    cv::Rect roiRectT;
    cv::Size roisizeT;

	U32	widthT2;
	U32	heightT2;

	U32 startxT2, startyT2;
	U32 imgoffsetT2;
    cv::Rect roiRectT2;
    cv::Size roisizeT2;

	U08 *pimg;
	U08 *pimgT;
	U08 *pimgT2;
	
	U08 *pimgProc;

	double	xfact;
	double	yfact;

	U32 right0left1;

	//------------
	right0left1 = piana->Right0Left1;

	pimgProc = pcpc->diffimgBinary;
	offset_x 	= pcpc->offset_x;
	offset_y 	= pcpc->offset_y;
	frame = pcpc->frame;

	width 		= pcpc->width;
	height 		= pcpc->height;
    imgSize = cv::Size(width, height);

	startx		= pcpc->startx;
	starty		= pcpc->starty;
	imgoffset	= startx + starty * width;
    roiRect = cv::Rect(startx, starty, pcpc->roiwidth, pcpc->roiheight);
    roisize = roiRect.size();

	widthT 		= height;
	heightT 	= width;

	startxT		= starty;
	startyT		= startx;
	imgoffsetT	= startxT + startyT * widthT;
    roiRectT = cv::Rect(startxT, startyT, pcpc->roiheight, pcpc->roiwidth);
    roisizeT = roiRectT.size();

	pimg = (U08 *) malloc(width * height);
	pimgT = (U08 *) malloc(width * height);
	pimgT2 = (U08 *) malloc(width * height);

#define XFACT		(1.0/4.0)
#define YFACT		(1.0/4.0)
	xfact = XFACT;
	yfact = YFACT;

	widthT2 		= (U32) (widthT * xfact + 0.1);
	heightT2 		= (U32) (heightT * yfact + 0.1);
	
    startxT2		= (U32) (startxT * xfact + 0.1);
	startyT2		= (U32) (startyT * yfact + 0.1);
	imgoffsetT2		= startxT2 + startyT2 * widthT2;
    roisizeT2.width = (U32)(roisizeT.width * xfact + 0.1);
    roisizeT2.height = (U32)(roisizeT.height * yfact + 0.1);
    roiRectT2 = cv::Rect(startxT2, startyT2, roisizeT2.width, roisizeT2.height);
	
	{
        I32 i, LineCount;

        cv::Mat cvImgProc(imgSize, CV_8UC1, pimgProc), cvImgROI, cvImgTrans, cvImgTResized;
        std::vector<cv::Vec2f> lines; 
        
        double deltaRho, deltaTheta;
        double minTheta, maxTheta;


#define DELTA_RHO		2.0
#define DELTA_DEGREE	2.0
        deltaRho    = (double) DELTA_RHO;
        deltaTheta  = (double) DEGREE2RADIAN(DELTA_DEGREE);
		
        minTheta = 20.0 * M_PI / 180.0;
        maxTheta = 120.0 * M_PI / 180.0;
        cvImgROI = cvImgProc(roiRect);
        cv::transpose(cvImgROI, cvImgTrans);
        cv::resize(cvImgTrans, cvImgTResized, roisizeT2, xfact, yfact, cv::INTER_CUBIC); // pimgT2
        
#define HOUGHTHRESHOLD	15
        cv::HoughLines(cvImgTResized, lines, deltaRho, deltaTheta, HOUGHTHRESHOLD, 0.0, 0.0, minTheta, maxTheta);
        //////////////////// We have to see more about differences between ipp hough and opencv hough.
        ////////////////////////////////////////////////////////////////////////////////////////////////////

        LineCount = (I32)(lines.size() < MAXLINECOUNT ? lines.size() : MAXLINECOUNT);

        // maxLineCount = MAXLINECOUNT;
        // MAXLINECOUNT = 4, LineCount, maxLineCount
        
		pcpc->linecount[frame] = LineCount;
		for (i = 0; i < (I32) LineCount; i++) {
			double m_, b_;
			float theta, rho;

			//----
			// cos A (X~-X~0) + sin A (Y~-Y~0) = r
			// X~ = Y, Y~ = X,
			// -> cos A (Y - Y0) + sin A(X - X0) = r
			// -> sin A (X - X0) = - cos A  (Y-Y0) + r
			// -> X = - (cos A / sin A) * (Y-Y0) + r / sin A + X0
			// -> X =   m_ * Y - m_ * Y0 + r / sin A + X0
			// ->   =   m_ * Y + b_
			// where m_ = - (cos A / sin A),
			//       b_ = -m_ * Y0 + (r / sin A) / (xfact or yfact) + X0
			//       
			//         (Y0 = startxT2 /xfact, X0 = startyT2 / yfact)

			theta 	= lines[i][1];
			rho 	= lines[i][0];
			m_ 		= - cos(theta)/sin (theta);
			b_ 		= - m_ * (startxT2 / xfact) + (rho / sin(theta)) / xfact + (startyT2 / yfact);

			pcpc->theta_[frame][i] = theta;					// theta[i] = theta_[i] - M_PI;
			pcpc->m_[frame][i] = m_;								// x = m_ * y + b_;
			pcpc->b_[frame][i] = b_;								
		}

#define M_MIN		(5.0)

#define X0_WIDTH	40
#define X1_WIDTH	10

#define Y0_WIDTH	40
#define Y1_WIDTH	10

        // Count hough projection value of Y.
		memset(&pcpc->houghpvalue0[frame][0], 0, sizeof(U32) * FULLHEIGHT);
		memset(&pcpc->houghpvalue1[frame][0], 0, sizeof(U32) * FULLHEIGHT);
		if (pcpc->linecount[frame] > 0) {
			int x, y;
			int xx, xx0, xx1;
			int yy, yy0, yy1;
			double m_, b_;
			int xxPrev, yyPrev;
			
			m_ = pcpc->m_[frame][0];
			b_ = pcpc->b_[frame][0];
			xxPrev = -1;
			yyPrev = -1;
			if (m_ > -M_MIN && m_ < M_MIN) {
				for (y = pcpc->starty; y <= (int)pcpc->endy; y++) {
					x = (int) (m_ * y + b_);

					xx0 = x - X0_WIDTH;
					xx1 = x + X1_WIDTH;
					if (xx0 < pcpc->startx)	{ xx0 = pcpc->startx; }
					if (xx0 > pcpc->endx)	{ xx0 = pcpc->endx; }
					if (xx1 < pcpc->startx)	{ xx1 = pcpc->startx; }
					if (xx1 > pcpc->endx)	{ xx1 = pcpc->endx; }

					for (xx = xx0; xx <= xx1; xx++) {
						yy = (int) (-m_ * (xx - x) + y);
						if (yy >= pcpc->starty &&  yy <= (int)pcpc->endy) {
							if (*(pimgProc + xx + yy * width)) {
								if (xx <= x) {		// xx0 ~ x
									pcpc->houghpvalue0[frame][y]++;
								} else {			// x   ~ xx1
									pcpc->houghpvalue1[frame][y]++;
								}
								yyPrev = yy;
							}
						}
					}
				}
			} else {
				for (x = pcpc->startx; x <= (int)pcpc->endx; x++) {
					y = (int) ((1.0/m_) * (x - b_));

					yy0 = y - Y0_WIDTH;
					yy1 = y + Y1_WIDTH;
					if (yy0 < pcpc->starty)	{ yy0 = pcpc->starty; }
					if (yy0 > pcpc->endy)	{ yy0 = pcpc->endy; }
					if (yy1 < pcpc->starty)	{ yy1 = pcpc->starty; }
					if (yy1 > pcpc->endy)	{ yy1 = pcpc->endy; }

					for (yy = yy0; yy <= yy1; yy++) {
						xx = (int) ((-1.0/m_) * (yy - y) + x);
						if (xx >= pcpc->startx &&  xx <= (int)pcpc->endx) {
							if (*(pimgProc + xx + yy * width)) {
								if (xx <= x) {		// xx0 ~ x
									pcpc->houghpvalue0[frame][y]++;
								} else {			// x   ~ xx1
									pcpc->houghpvalue1[frame][y]++;
								}
								xxPrev = xx;
							}
						}
					}
				} 	// for (x = pcpc->startx; x <= (int)pcpc->endx; x++) 
			} 		// if (m_ > -M_MIN && m_ < M_MIN) 
		}

		// Count projection value of X, Y
		memset(&pcpc->houghXpvalue[frame][0], 0, sizeof(U32) * FULLWIDTH);
		memset(&pcpc->houghYpvalue[frame][0], 0, sizeof(U32) * FULLHEIGHT);
		if (pcpc->linecount[frame] > 0) {
			int x, y;
			int xx, xx0, xx1;
			int yy, yy0, yy1;
			double m_, b_;
			
			m_ = pcpc->m_[frame][0];
			b_ = pcpc->b_[frame][0];


			if (m_ > -0.01 && m_ < 0.01) {
				m_ = 0.01;
			}
			for (x = pcpc->startx; x <= (int)pcpc->endx; x++) {
				y = (int) ((x - b_) / m_);
				yy0 = y - Y0_WIDTH;
				yy1 = y + Y1_WIDTH;
				if (yy0 < pcpc->starty)	{ yy0 = pcpc->starty; }
				if (yy0 > pcpc->endy)	{ yy0 = pcpc->endy; }
				if (yy1 < pcpc->starty)	{ yy1 = pcpc->starty; }
				if (yy1 > pcpc->endy)	{ yy1 = pcpc->endy; }

				for (yy = yy0; yy <= yy1; yy++) {
					if (*(pimgProc + x + yy * width)) {
						pcpc->houghXpvalue[frame][x]++;
					}
				}
			}


			for (y = pcpc->starty; y <= (int)pcpc->endy; y++) {
				x = (int) (m_ * y + b_);

				xx0 = x - X0_WIDTH;
				xx1 = x + X1_WIDTH;
				if (xx0 < pcpc->startx)	{ xx0 = pcpc->startx; }
				if (xx0 > pcpc->endx)	{ xx0 = pcpc->endx; }
				if (xx1 < pcpc->startx)	{ xx1 = pcpc->startx; }
				if (xx1 > pcpc->endx)	{ xx1 = pcpc->endx; }

				for (xx = xx0; xx <= xx1; xx++) {
					if (*(pimgProc + xx + y * width)) {
						pcpc->houghYpvalue[frame][y]++;
					}
				}
			}
		}

		if (piana->himgfunc) {
			int x, y;
			int x0;
			double m_, b_;
			int xx0, xx1;
			int yy0, yy1;

			//--
			memcpy(pimg, pimgProc, width*height);

			for (i = 0; i < (I32) pcpc->linecount[frame]; i++) {
				if (i > 0) break;
				m_ 		= pcpc->m_[frame][i];
				b_ 		= pcpc->b_[frame][i];

				for (y = pcpc->starty; y <= (int)pcpc->endy; y++) {
					x = (int) (m_ * y + b_);
					if (x >= pcpc->startx && x <= (int) pcpc->endx) {
						*(pimg + x + y * width) = (U08)(0xFF - i * 0x20);
					}

					if (m_ > -M_MIN && m_ < M_MIN) {
						xx0 = x - X0_WIDTH;
						if (xx0 >= pcpc->startx && xx0 <= (int) pcpc->endx) {
							*(pimg + xx0 + y * width) = (U08)(0xFF - i * 0x20);
						}

						xx1 = x + X1_WIDTH;
						if (xx1 >= pcpc->startx && xx1 <= (int) pcpc->endx) {
							*(pimg + xx1 + y * width) = (U08)(0xFF - i * 0x20);
						}
					} else {
						yy0 = y - Y0_WIDTH;
						if (yy0 >= pcpc->starty && yy0 <= (int) pcpc->endy) {
							*(pimg + x + yy0 * width) = (U08)(0xFF - i * 0x20);
						}

						yy1 = y + Y1_WIDTH;
						if (yy1 >= pcpc->starty && yy1 <= (int) pcpc->endy) {
							*(pimg + x + yy1 * width) = (U08)(0xFF - i * 0x20);
						}
					}


					if (i == 0) {
						int xx;
						int yy;
					
                        //-- THICK line
						x0 = x - 1;
						if (x0 >= pcpc->startx && x0 <= (int) pcpc->endx) {
							*(pimg + x0 + y * width) = (U08)(0xFF - i * 0x20);
						}
						x0 += 2;
						if (x0 >= pcpc->startx && x0 <= (int) pcpc->endx) {
							*(pimg + x0 + y * width) = (U08)(0xFF - i * 0x20);
						}

						//---
						if (y == (int)((pcpc->starty + pcpc->endy)/2)) {
							if (m_ > -M_MIN && m_ < M_MIN) {
								for (xx = pcpc->startx; xx <= (int)pcpc->endx; xx++) {
									yy = (int) (-m_ * (xx - x) + y);

									if (yy >= pcpc->starty && yy <= (int) pcpc->endy) {
										*(pimg + xx + yy * width) = (U08)(0xFF);
									}
								}
							} else {
								for (yy = pcpc->starty; yy <= (int)pcpc->endy; yy+= 5) {
									xx = (int) ((-1.0/m_) * (yy - y) + x);
									if (xx >= pcpc->startx && xx <= (int) pcpc->endx) {
										*(pimg + xx + yy * width) = (U08)(0xFF);
									}
								}
							}
						}

						for (yy = 0; yy < (int)height; yy++) {
							xx = pcpc->houghpvalue0[frame][yy] + pcpc->houghpvalue1[frame][yy];
							*(pimg + xx + yy * width) = (U08)(0xFF);
						}

						for (yy = 0; yy < (int)height; yy++) {
							xx = width - pcpc->houghYpvalue[frame][yy] - 1;
							if (xx >= 0 && xx < (int)width) {
								*(pimg + xx + yy * width) = (U08)(0xFF);
							}
						}

						for (xx = 0; xx < (int)width; xx++) {
							yy = pcpc->houghXpvalue[frame][xx];
							*(pimg + xx + yy * width) = (U08)(0xFF);
						}
					} 	// if (i == 0) 
				} 		// for (y = pcpc->starty; y <= (int)pcpc->endy; y++) 
			} 			// for (i = 0; i < (U32) pcpc->linecount[frame]; i++) 

			{
				for (x = 0; x < (int) width; x += 10) {
					for (y = 0; y < (int) height; y++) {
						xx0 = *(pimg + x + y * width);
						x0 = 0xFF - xx0;
						x0 = (xx0 + x0) / 2;
						*(pimg + x + y * width) = (U08) x0;
					}
				}
				for (x = 0; x < (int) width; x++) {
					for (y = 0; y < (int) height; y += 10){
						xx0 = *(pimg + x + y * width);
						x0 = 0xFF - xx0;
						x0 = (xx0 + x0) / 2;
						*(pimg + x + y * width) = (U08) x0;
					}
				}

				for (x = 1; x < (int) width; x += 50) {
					for (y = 0; y < (int) height; y++) {
						xx0 = *(pimg + x + y * width);
						x0 = 0xFF - xx0;
						//x0 = (xx0 + x0) / 2;
						*(pimg + x + y * width) = (U08) x0;
					}
				}
				for (x = 0; x < (int) width; x++) {
					for (y = 1; y < (int) height; y += 50){
						xx0 = *(pimg + x + y * width);
						x0 = 0xFF - xx0;
						//x0 = (xx0 + x0) / 2;
						*(pimg + x + y * width) = (U08) x0;
					}
				}
			}
            if (piana->himgfunc) {
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimg, width, height, 1, 1, 2, 3, piana->imguserparam);
            }
		}
	}

	free(pimg);
	free(pimgT);
	free(pimgT2);

#define HPV0		3
#define HPV1		15
#define LEN1MIN		10
#define LEN1MAX		70
#define LEN1MERCY	5
#define HPVCOUNTMIN	10
#define HPVSHAFTCOUNTMIN	20
	if (pcpc->linecount[frame] > 0) {
		int i;
		int y;
		int hpv;
		int hpvsum;
		int hpvcount;
		int len0, len1;
		int hpvshaft;
		int checkstate;
		int sx, sy;
		int len1max;
		int len1maxsy;
		int hpv0, hpv1;
		
		
		for (i = 0; i < pcpc->linecount[frame]; i++) {
			checkstate = 0;
			len0 = 0;
			len1 = 0;

			hpvsum 	 = 0;
			hpvcount = 0;
			hpvshaft  = -1;
			if (right0left1 == 0) {		// Right handed
				for (y = pcpc->endy; y >= pcpc->starty; y--) {
					hpv = pcpc->houghpvalue0[frame][y] + pcpc->houghpvalue1[frame][y];
					if (hpv > HPV0 && hpv < HPV1) {
						hpvsum+= hpv;
						hpvcount++;
					}
				}
			} else {				// Left handed
				for (y = pcpc->endy; y <= pcpc->starty; y--) {
					hpv = pcpc->houghpvalue0[frame][y] + pcpc->houghpvalue1[frame][y];
					if (hpv > HPV0 && hpv < HPV1) {
						hpvsum+= hpv;
						hpvcount++;
					}
				}
			}
			if (hpvcount > HPVSHAFTCOUNTMIN) {
				hpvshaft = (int)((double)hpvsum / hpvcount);
			}

			sy = -1;
			len1max = -1;

			if (hpvshaft > 0) {
				hpv0 = hpvshaft;
			} else {
				hpv0 = HPV0;
			}
			hpv1 = HPV1;
			hpvsum = 0;
			if (right0left1 == 0) {		// Right handed
				for (y = pcpc->starty; y <= pcpc->endy; y++) {
					hpv = pcpc->houghpvalue0[frame][y] + pcpc->houghpvalue1[frame][y];
					switch (checkstate) {
						case 0:
							if (hpv > hpv0) {
								checkstate = 1;
								len0 = 0;
								len1 = 1;
								sy = y;
								hpvsum+= hpv;
							} else {
								len0++;
							}
							break;
						case 1:
							if (hpv > hpv0) {
								len1++;
								hpvsum+= hpv;
								if (len1max < len1) {
									len1max = len1;
									len1maxsy = sy;
								}
							} else {
								len0++;
								if (len0 > HPVCOUNTMIN) {
									checkstate = 2;
									break;
								}
							}
							if (len1 > LEN1MAX) {
								checkstate = 2;
							}
							break;
						default:
							checkstate = 0;
							break;
					}

					if (checkstate == 2) {
						break;
					}
				}
			} else {				// LEFT
				for (y = pcpc->endy; y >= pcpc->starty; y--) {
					hpv = pcpc->houghpvalue0[frame][y] + pcpc->houghpvalue1[frame][y];
					switch (checkstate) {
						case 0:
							if (hpv > hpv0) {
								checkstate = 1;
								len0 = 0;
								len1 = 1;
								sy = y;
								hpvsum+= hpv;
							} else {
								len0++;
							}
							break;
						case 1:
							if (hpv > hpv0) {
								len1++;
								hpvsum+= hpv;
								if (len1max < len1) {
									len1max = len1;
									len1maxsy = sy;
								}
							} else {
								len0++;
								if (len0 > HPVCOUNTMIN) {
									checkstate = 2;
									break;
								}
							}
							if (len1 > LEN1MAX) {
								checkstate = 2;
							}
							break;
						default:
							checkstate = 0;
							break;
					}

					if (checkstate == 2) {
						break;
					}
				}
			}

			{
				double m_, b_;
				I32 hpexist = 0;

				if (checkstate != 2) {
					sx = -1;
					sy = -1;
					hpexist = IANA_CLUB_EMPTY;
				} else {
					hpexist = IANA_CLUB_EXIST;
				}

				if (len1 < 1) len1 = 1;
				m_ = pcpc->m_[frame][i];
				b_ = pcpc->b_[frame][i];
				sx = (int) (m_ * sy + b_);
			
                pcpc->hx[frame][i] = sx;
				pcpc->hy[frame][i] = sy;
				pcpc->hpexist[frame][i] = hpexist;
				
                if (pcpc->aftershot == 0) {
					iana_cam_t		*pic;
					pic 		= piana->pic[camid];

					if (sx < pic->icp.P.x - pcpc->offset_x - 10) {
						pcpc->hpexist[frame][i] = IANA_CLUB_EMPTY;
					}
				}
			}
		}
	}
	UNUSED(piana);
	UNUSED(camid);
	return 1;
#endif

}
/*!
 ********************************************************************************
 *	@brief      Club
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2017/0405
 *******************************************************************************/
static I32 ClubPath_CalcPosition_Shape(	
		iana_t *piana, 
		U32 camid, 
		clubpath_context_t *pcpc)
{
#ifdef IPP_SUPPORT
	int linenum;
	U08 *pimg0;
	U32 frame;
	I32 sx, sy, ex, ey;
	I32 hx, hy;
	I32 linecount;
	U32 width, height;
	IppiSize roisize;
	U32 imgoffset;
	I32 buffersize;

	imagesegment_t imgseg[MAXLABEL];
	imagesegment_t *pis;

	U32 right0left1;
	//------------
//	__debugbreak();
	right0left1 = piana->Right0Left1;

	//-----
	// 1) Set Image Processing Rectangle  around current hx, hy
	// 2) Labelling
	// 3) Get Header position with image shape 


	frame = pcpc->frame;
	linecount = pcpc->linecount[frame];


	width = pcpc->width;
	height = pcpc->height;

	pimg0 = (U08 *)malloc(width*height);

	for (linenum = 0; linenum < linecount; linenum++) {
		if (pcpc->hpexist[frame][linenum] != IANA_CLUB_EXIST) {
			continue;
		}
		pcpc->count++;
		// 1) Set Image Processing Rectangle  around current hx, hy
		{
			U32 roiwidth, roiheight;
			hx = pcpc->hx[frame][linenum];
			hy = pcpc->hy[frame][linenum];

			sx = hx - HEADIMGWIDTH/2;
			ex = hx + HEADIMGWIDTH/2;

			//sy = hy - (HEADIMGWIDTH*2)/3;
			//ey = hy + (HEADIMGWIDTH/3);

			sy = hy - HEADIMGHEIGHT/2;
			ey = hy + HEADIMGHEIGHT/2;


			if (sx < 0) sx = 0; if (sx >= (I32)width) sx = width-1;
			if (ex < 0) ex = 0; if (ex >= (I32)width) ex = width-1;

			if (sy < 0) sy = 0; if (sy >= (I32)height) sy = height-1;
			if (ey < 0) ey = 0; if (ey >= (I32)height) ey = height-1;

			roiwidth = ex - sx + 1;
			roiheight = ey - sy + 1;

			roisize.width 	= roiwidth;
			roisize.height 	= roiheight;

			imgoffset = sx + sy * width;
		} 		// Set Image Processing Rectangle  around current hx, hy


		// 2) Labelling
		{
			int i, j;
			IppStatus ippstatus;
			U08 *pBuffer;
			I32 segmentcount;
			I32 segframe;
			ippiCopy_8u_C1R(
					pcpc->diffimgBinary + imgoffset, width,
					pimg0 + imgoffset, width,
					roisize);

			ippstatus = ippiLabelMarkersGetBufferSize_8u_C1R(roisize, &buffersize);	// Get buffersize
			pBuffer = (U08 *)malloc(buffersize);
			ippstatus = ippiLabelMarkers_8u_C1IR(
					pimg0 + imgoffset, width, 
					roisize, 
					MINLABEL, 
					MAXLABEL, 
					ippiNormInf,
					&segmentcount,
					pBuffer);
			free(pBuffer);


			memset(&imgseg[0], 0, MAXLABEL * sizeof(imagesegment_t));
			for (i = 0; i < MAXLABEL; i++) {
				pis = &imgseg[i];
				pis->state = IMAGESEGMENT_NULL;
				pis->pixelcount = 0;
				pis->label = i;
			}

			for (i = sy + 1; i < ey; i++) {
				for (j = sx + 1; j < ex; j++) {
					segframe = *(pimg0 + j + i * width);

					if (segframe >= MINLABEL && segframe <= MAXLABEL - 1) {
						pis = &imgseg[segframe];
						if (pis->state == IMAGESEGMENT_NULL) {
							pis->state = IMAGESEGMENT_FULL;
							pis->ixl = j;
							pis->iyu = i;
						}
						pis->pixelcount++;
						if (pis->ixr < (U32)j) {		// xr is biggest..
							pis->ixr = j;
						}
						if (pis->ixl > (U32)j) {		// xl is smallest..
							pis->ixl = j;
						}
						pis->iyb = i;
					}
				}
			}
		} 						// Labelling


		// 3) Get Header position with image shape 
		{
			int i, j;
			int k;
			double mindist;
			int mindistk;
			int mindistx, mindisty;
			int pixelcountMin;
			int segmentheightMin;
			int segmentheightMax;
			int segmentwidthMin;

//#define PIXELCOUNTMIN		200
#define PIXELCOUNTMIN		150
//#define SEGMENTHEIGHTMIN	30
#define SEGMENTHEIGHTMIN	20
#define SEGMENTHEIGHTMAX	200
//#define SEGMENTWIDTHMIN		15
#define SEGMENTWIDTHMIN		10
			pixelcountMin = PIXELCOUNTMIN;
			segmentheightMin = SEGMENTHEIGHTMIN;
			segmentheightMax = SEGMENTHEIGHTMAX;
			segmentwidthMin = SEGMENTWIDTHMIN;
			
			mindist = 9999;
			mindistk = -1;
			mindistx = 9999;
			mindisty = 9999;
			for (k = MINLABEL; k <  MAXLABEL - 1; k++) {
				U32 segmentheight;
				U32 segmentwidth;
				I32 xx, yy;
				I32 foundit;

				pis = &imgseg[k];

				if (pis->state == IMAGESEGMENT_FULL) {
					segmentheight = pis->iyb - pis->iyu + 1;
					segmentwidth  = pis->ixr - pis->ixl + 1;
					/*
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------- seg[%d] <%3d %3d> ~ <%3d %3d> pixelcount: %d, segmentwidth: %d, segmentheight: %d\n",
							k, 
							pis->ixl, pis->iyu,
							pis->ixr, pis->iyb,
							pis->pixelcount, segmentwidth, segmentheight
							);
					*/
					if (pis->pixelcount > (U32)pixelcountMin && segmentheight > (U32)segmentheightMin && segmentwidth > (U32)segmentwidthMin) {
						double dx, dy, distance;
						int firstx, lastx;
						int calcwidth;

						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "====\n");
						firstx = pis->ixr;
						lastx = pis->ixl;
						//for (i = pis->iyu; i <= (int)pis->iyb; i++) 
//#define CALCCOUNT	5
//#define CALCCOUNT	10
//#define CALCCOUNT	20
//#define CALCCOUNT0	10
//#define CALCCOUNT	30
#define CALCCOUNT0	10
//#define CALCCOUNT	40
#define CALCCOUNT	20
//#define CALCCOUNT	60
//#define CALCCOUNT0	5
//#define CALCCOUNT	10

//#define CALCWIDTH	5
#define CALCWIDTH	2
						calcwidth=0;
						for (i = CALCCOUNT0; i < CALCCOUNT; i++) 
						{
							yy = i + pis->iyu;
							if (yy >= (I32)pis->iyb) break;
							for (j = pis->ixl; j <= (int)pis->ixr; j++) {
								if (k == *(pimg0 + j + yy * width)) {
									if (j < firstx) {
										firstx = j;
									}
									if (j > lastx) {
										lastx = j;
									}
									calcwidth++;
									if (calcwidth > CALCWIDTH) {
										break;
									}
								}
							}
						}

						xx = (firstx + lastx)/2;
						yy = pis->iyu;
						foundit = 0;

						for (i = pis->iyu; i < (I32)pis->iyb; i++) 
						{
							for (j = pis->ixl; j <= (int)pis->ixr; j++) {
								if (k == *(pimg0 + j + i * width)) {
#define CALCY_XADD	5
									if (j <= xx + CALCY_XADD) {			// good.
										xx = j;
										yy = i;
										foundit = 1;
									}
									break;
								}
							}
							if (foundit) {
								break;
							}
						}

//#define HXOFFSET	20
#define HXOFFSET	40
#define HYWEIGHT	1.5
						dx = (pcpc->hx[frame][0]-HXOFFSET) - xx;
						dy = (pcpc->hy[frame][0] - yy) * HYWEIGHT;

						distance = sqrt(dx*dx+dy*dy);

						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " [%d] xx yy: %d %d, dist: %lf\n", k, xx, yy, distance);
						if (mindist > distance) {
							mindist = distance;
							mindistk = k;
							mindistx = xx;
							mindisty = yy;
						}
					}
				} 			// if (pis->state == IMAGESEGMENT_FULL) 
			} 		// for (k = MINLABEL; k <  MAXLABEL - 1; k++) 
			if (mindistk >= 0) {
				pcpc->hsx[frame] = mindistx;
				pcpc->hsy[frame] = mindisty;
				pcpc->hsexist[frame] = IANA_CLUB_EXIST;
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " =====>(%d)xx yy: %d %d, dist: %lf\n", mindistk, mindistx, mindisty, mindist);
			}
		} 			//  Get Header position with image shape 
		if (pcpc->hsexist[frame] == IANA_CLUB_EMPTY) {
			pcpc->hsx[frame] = pcpc->hx[frame][linenum];
			pcpc->hsy[frame] = pcpc->hy[frame][linenum];
			pcpc->hsexist[frame] = IANA_CLUB_EXIST;
		}

//#define HSX_MARGIN	20
#define HSX_MARGIN	40
#define HSX_CHECKCOUNT	4
		// cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " <%d> hsx: %d vs %d\n", frame, pcpc->hsx[frame], width);
		if ( pcpc->hsx[frame] + HSX_MARGIN > (I32)width && pcpc->count < HSX_CHECKCOUNT) {
			U32 i;
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " --> EMPTY\n");
			for (i = 0; i <= frame; i++) {
				pcpc->hsexist[i] = IANA_CLUB_EMPTY;
			}
			pcpc->count = 0;
		}
		if (linenum>=0) break;
	} 				// for (i = 0; i < linecount; i++) 


	if (pcpc->hsexist[frame] == IANA_CLUB_EXIST) {
		I32 x, y;
		x = pcpc->hsx[frame];
		y = pcpc->hsy[frame];
#define POSITIONCHECK_X		40
//#define POSITIONCHECK_X		80
#define POSITIONCHECK_Y		40
		//if (x > (I32)width - POSITIONCHECK_X  && y > (I32)height/2 + POSITIONCHECK_Y) 
		if (x > (I32)width - POSITIONCHECK_X)
		{
			pcpc->hsexist[frame] = IANA_CLUB_EMPTY;
		}
	}

	free(pimg0);
	camid;piana;
	return 1;
#else
    int linenum;
    U08 *pimg0;
    U32 frame;
    I32 sx, sy, ex, ey;
    I32 hx, hy;
    I32 linecount;

    U32 width, height;
    cv::Size imgSize, roisize;
    cv::Rect imgROI;

    imagesegment_t imgseg[MAXLABEL];
    imagesegment_t *pis;

    //------------
    // 1) Set Image Processing Rectangle  around current hx, hy
    // 2) Labelling
    // 3) Get Header position with image shape 

    frame = pcpc->frame;
    linecount = pcpc->linecount[frame];


    width = pcpc->width;
    height = pcpc->height;
    imgSize = cv::Size(width, height);

    pimg0 = (U08 *)malloc(width*height);

    for (linenum = 0; linenum < linecount; linenum++) {
        if (pcpc->hpexist[frame][linenum] != IANA_CLUB_EXIST) {
            continue;
        }
        pcpc->count++;
        // 1) Set Image Processing Rectangle  around current hx, hy
        {
            U32 roiwidth, roiheight;
            hx = pcpc->hx[frame][linenum];
            hy = pcpc->hy[frame][linenum];

            sx = hx - HEADIMGWIDTH/2;
            ex = hx + HEADIMGWIDTH/2;

            sy = hy - HEADIMGHEIGHT/2;
            ey = hy + HEADIMGHEIGHT/2;


            if (sx < 0) {
				sx = 0; 
            }
			if (sx >= (I32)width) {
				sx = width-1;
			}
            if (ex < 0) {
				ex = 0; 
            }
			if (ex >= (I32)width) {
				ex = width-1;
			}
            if (sy < 0) {
				sy = 0; 
            }
			if (sy >= (I32)height) {
				sy = height-1;
			}
            if (ey < 0) {
				ey = 0; 
            }
			if (ey >= (I32)height) {
				ey = height-1;
			}

            roiwidth = ex - sx + 1;
            roiheight = ey - sy + 1;

            roisize.width 	= roiwidth;
            roisize.height 	= roiheight;

            imgROI.x = sx;
            imgROI.y = sy;
            imgROI.width = roiwidth;
            imgROI.height = roiheight;
        } 		// Set Image Processing Rectangle  around current hx, hy


        // 2) Labelling
        {
            cv::Mat cvImg(imgSize, CV_8UC1, pcpc->diffimgBinary), cvROIImg, cvImgLabel, cvImgLabel08;
            cv::Mat cvStats, cvCents;
            int labelCnt;
            int i;

            cvROIImg = cvImg(imgROI);
            labelCnt = cv::connectedComponentsWithStats(cvROIImg, cvImgLabel, cvStats, cvCents, 8, CV_32S);

            labelCnt = labelCnt < MAXLABEL ? labelCnt : MAXLABEL;
            
            cvImgLabel.convertTo(cvImgLabel08, CV_8UC1);
            cvImgLabel08.copyTo(cvImg(imgROI));

            memcpy(pimg0, cvImg.data, sizeof(U08) * cvImg.cols * cvImg.rows);
            
            memset(&imgseg[0], 0, MAXLABEL * sizeof(imagesegment_t));
            for (i = 0; i < MAXLABEL; i++) {
                pis = &imgseg[i];
                pis->state = IMAGESEGMENT_NULL;
                pis->pixelcount = 0;
                pis->label = i;
            }

            for(i = 1; i < labelCnt; i++){
                pis = &imgseg[i];
                pis -> state = IMAGESEGMENT_FULL;
                pis -> ixl = cvStats.at<int>(i, 0) + sx;
                pis -> iyu = cvStats.at<int>(i, 1) + sy;
                pis -> ixr = cvStats.at<int>(i, 0) + cvStats.at<int>(i, 2) + ex;
                pis -> iyb = cvStats.at<int>(i, 1) + cvStats.at<int>(i, 3) + ey;
                pis -> pixelcount = cvStats.at<int>(i, 4);
            }
        } 						// Labelling

        // 3) Get Header position with image shape 
        {
            int i, j;
            int k;
            double mindist;
            int mindistk;
            int mindistx, mindisty;
            int pixelcountMin;
            int segmentheightMin;
            int segmentheightMax;
            int segmentwidthMin;

#define PIXELCOUNTMIN		150
#define SEGMENTHEIGHTMIN	20
#define SEGMENTHEIGHTMAX	200
#define SEGMENTWIDTHMIN		10
            pixelcountMin = PIXELCOUNTMIN;
            segmentheightMin = SEGMENTHEIGHTMIN;
            segmentheightMax = SEGMENTHEIGHTMAX;
            segmentwidthMin = SEGMENTWIDTHMIN;

            mindist = 9999;
            mindistk = -1;
            mindistx = 9999;
            mindisty = 9999;
            for (k = MINLABEL; k < MAXLABEL - 1; k++) {
                U32 segmentheight;
                U32 segmentwidth;
                I32 xx, yy;
                I32 foundit;

                pis = &imgseg[k];

                if (pis->state == IMAGESEGMENT_FULL) {
                    segmentheight = pis->iyb - pis->iyu + 1;
                    segmentwidth  = pis->ixr - pis->ixl + 1;
                    /*
                    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------- seg[%d] <%3d %3d> ~ <%3d %3d> pixelcount: %d, segmentwidth: %d, segmentheight: %d\n",
                            k,
                            pis->ixl, pis->iyu,
                            pis->ixr, pis->iyb,
                            pis->pixelcount, segmentwidth, segmentheight
                            );
                    */
                    if (pis->pixelcount > (U32)pixelcountMin && segmentheight > (U32)segmentheightMin && segmentwidth > (U32)segmentwidthMin) {
                        double dx, dy, distance;
                        int firstx, lastx;
                        int calcwidth;

                        firstx = pis->ixr;
                        lastx = pis->ixl;
                        //for (i = pis->iyu; i <= (int)pis->iyb; i++) 
#define CALCCOUNT0	10
#define CALCCOUNT	20
#define CALCWIDTH	2
                        calcwidth = 0;
                        for (i = CALCCOUNT0; i < CALCCOUNT; i++) {
                            yy = i + pis->iyu;
                            if (yy >= (I32)pis->iyb) break;
                            for (j = pis->ixl; j <= (int)pis->ixr; j++) {
                                if (k == *(pimg0 + j + yy * width)) {
                                    if (j < firstx) {
                                        firstx = j;
                                    }
                                    if (j > lastx) {
                                        lastx = j;
                                    }
                                    calcwidth++;
                                    if (calcwidth > CALCWIDTH) {
                                        break;
                                    }
                                }
                            }
                        }

                        xx = (firstx + lastx)/2;
                        yy = pis->iyu;
                        foundit = 0;

                        for (i = pis->iyu; i < (I32)pis->iyb; i++) {
                            for (j = pis->ixl; j <= (int)pis->ixr; j++) {
                                if (k == *(pimg0 + j + i * width)) {
#define CALCY_XADD	5
                                    if (j <= xx + CALCY_XADD) {			// good.
                                        xx = j;
                                        yy = i;
                                        foundit = 1;
                                    }
                                    break;
                                }
                            }
                            if (foundit) {
                                break;
                            }
                        }

#define HXOFFSET	40
#define HYWEIGHT	1.5
                        dx = (pcpc->hx[frame][0]-HXOFFSET) - xx;
                        dy = (pcpc->hy[frame][0] - yy) * HYWEIGHT;

                        distance = sqrt(dx*dx+dy*dy);

                        //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " [%d] xx yy: %d %d, dist: %lf\n", k, xx, yy, distance);
                        if (mindist > distance) {
                            mindist = distance;
                            mindistk = k;
                            mindistx = xx;
                            mindisty = yy;
                        }
                    }
                } 			// if (pis->state == IMAGESEGMENT_FULL) 
            } 		// for (k = MINLABEL; k <  MAXLABEL - 1; k++) 
            if (mindistk >= 0) {
                pcpc->hsx[frame] = mindistx;
                pcpc->hsy[frame] = mindisty;
                pcpc->hsexist[frame] = IANA_CLUB_EXIST;
                //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " =====>(%d)xx yy: %d %d, dist: %lf\n", mindistk, mindistx, mindisty, mindist);
            }
        } 			//  Get Header position with image shape 
        if (pcpc->hsexist[frame] == IANA_CLUB_EMPTY) {
            pcpc->hsx[frame] = pcpc->hx[frame][linenum];
            pcpc->hsy[frame] = pcpc->hy[frame][linenum];
            pcpc->hsexist[frame] = IANA_CLUB_EXIST;
        }

#define HSX_MARGIN	40
#define HSX_CHECKCOUNT	4
            // cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " <%d> hsx: %d vs %d\n", frame, pcpc->hsx[frame], width);
        if (pcpc->hsx[frame] + HSX_MARGIN > (I32)width && pcpc->count < HSX_CHECKCOUNT) {
            U32 i;
            //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " --> EMPTY\n");
            for (i = 0; i <= frame; i++) {
                pcpc->hsexist[i] = IANA_CLUB_EMPTY;
            }
            pcpc->count = 0;
        }
        if (linenum>=0) break;
    } 				// for (i = 0; i < linecount; i++) 

    if (pcpc->hsexist[frame] == IANA_CLUB_EXIST) {
        I32 x, y;
        x = pcpc->hsx[frame];
        y = pcpc->hsy[frame];

#define POSITIONCHECK_X		40
#define POSITIONCHECK_Y		40
        if (x > (I32)width - POSITIONCHECK_X) {
            pcpc->hsexist[frame] = IANA_CLUB_EMPTY;
        }
    }

    free(pimg0);
    UNUSED(camid); UNUSED(piana);
    return 1;
#endif
}

/*!
 ********************************************************************************
 *	@brief      Club
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2017/0528
 *******************************************************************************/
static I32 ClubPath_CalcPosition_RefineShape(
		iana_t *piana, 
		U32 camid, 
		clubpath_context_t *pcpc)
{
	I32 res;
	I32 frame;
	I32 shotframe;
	double x_, y_;
	double hx, hy;
	double bx, by;
	double dx, dy, dist;
	double ballradius;
	U32	offset_x;
	U32	offset_y;
	marksequence_t	*pmks;
	iana_cam_t		*pic;
		/*
	U32 right0left1;
	//------------
	right0left1 = piana->right0left1;
	*/


	//----
	pic 		= piana->pic[camid];
	pmks 		= &pic->mks;

	frame 		= pcpc->frame;
	shotframe 	= pcpc->shotframe;
	offset_x = pcpc->offset_x;
	offset_y = pcpc->offset_y;

	ballradius = (int) (pic->icp.RunBallsizePMax / 2.0);


	if (pcpc->aftershot == 0 && pcpc->preshotgood) {
		double prederr;
		double q_, m_, b_;
		double tsd;
		UNUSED(frame);
		q_ = 0;
		m_ = pcpc->preshot_m_;
		b_ = pcpc->preshot_b_;

		hx = pcpc->hsx[frame];
		//x_ = hx = pcpc->hsx[frame];

		tsd = pcpc->tsd[frame];
#if defined(USE_II)
		x_ = q_ * frame*frame + m_ * frame + b_;
#else
		x_ = q_ * tsd*tsd + m_ * tsd + b_;
#endif
		y_ = pcpc->preshot_myxa[2] * x_*x_ + pcpc->preshot_myxa[1] * x_ + pcpc->preshot_myxa[0];
		hx = pcpc->hsx[frame];
		hy = pcpc->hsy[frame];

		prederr =  y_ - hy;
//#define PREDERR_Y_MIN	10
//#define PREDERR_Y_MIN	20
#define PREDERR_Y_MIN	30
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "y_: %lf hy: %lf, err: %lf\n", y_, hy, prederr);


		if (prederr < -PREDERR_Y_MIN || prederr > PREDERR_Y_MIN) {
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "hy: %lf => %lf\n", hy, y_);
			pcpc->hsy[frame] = (I32)y_;
		}

		prederr =  x_ - hx;
//#define PREDERR_X_MIN	10
#define PREDERR_X_MIN	20
		if (prederr < -PREDERR_X_MIN || prederr > PREDERR_X_MIN) {
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "hx: %lf => %lf\n", hx, x_);
			pcpc->hsx[frame] = (I32)x_;
		}
	}


	UNUSED(dx);UNUSED(dy);UNUSED(bx);UNUSED(by);UNUSED(dist);

	res = 1;
	return res;
}

/*!
 ********************************************************************************
 *	@brief      Club
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: changed, 0: NOT changed.
 *
 *	@author	    yhsuk
 *  @date       2017/0430
 *******************************************************************************/
static I32 ClubPath_RefinePosition(iana_t *piana, U32 camid, clubpath_context_t *pcpc)
{
	I32 res;
	//I32 i0, i1;
	I32 frame0, frame1;
	I32 i;
	I32 validcount;
	I32 shotframe;
	regressionx_t rgx;
	iana_cam_t		*pic;

	U32 right0left1;

	I32 marksequencelen;


	//------------
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}
	
	right0left1 = piana->Right0Left1;
	//------
	//#define SHOTVALIDCOUNT	4
	//#define SHOTVALIDCOUNT0	3
	//#define DISCARDCOUNT 3
	//#define DISCARDCOUNT 4
//#define SHOTVALIDCOUNT	(4+DISCARDCOUNT)
//#define SHOTVALIDCOUNT0	(3+DISCARDCOUNT)


//#define SHOTVALIDCOUNT	4
//#define SHOTVALIDCOUNT0	3

#define SHOTVALIDCOUNT	3
#define SHOTVALIDCOUNT0	3


	//#define SHOTPOSTSHOTPRE	4
#define SHOTPOSTSHOTPRE	6
	//#define SHOTPOSTSHOTPRE	10

#if defined(CLUB3_TEST)
//#define SHOTPRESHOTPOST	2
#define SHOTPRESHOTPOST	4
#else
#define SHOTPRESHOTPOST	6
#endif
//#define SHOTPRESHOTPOST	4
////////#define SHOTPRESHOTPOST	2
	shotframe = pcpc->shotframe;
	pic 		= piana->pic[camid];

	// Get valid start frame (after shot or with minimum valid count)

	frame0 = 0;
	validcount = 0;
	for (i = marksequencelen-1; i >= 0; i--) {
		if (pcpc->hsexist[i] == IANA_CLUB_EXIST) {
			validcount++;
			frame0 = i;

			if (i <= (I32) pcpc->shotframe - SHOTPOSTSHOTPRE) {
				if (validcount >= SHOTVALIDCOUNT+3) {
					//		i0 = i;
					break;
				}
			}
		}
	}

	if (validcount < SHOTVALIDCOUNT+2) {
		res = 0;
		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" club fail %d res: %d\n", __LINE__, res);
		goto func_exit;
	}
	frame1 = marksequencelen-1;

	//--
	rgx.frame0 = frame0;
	rgx.frame1 = frame1;
	rgx.usequadratic = 1;
	rgx.recalcshotframe = 0;
	rgx.shotframecombined = pcpc->shotframecombined;
	res = ClubPath_RegressPosition(piana, camid, pcpc, &rgx);
	pcpc->maiitt[2] = rgx.maiitt[2];
	pcpc->maiitt[1] = rgx.maiitt[1];
	pcpc->maiitt[0] = rgx.maiitt[0];


	if (res) {
		pcpc->postshot_ma[2] = rgx.ma[2];
		pcpc->postshot_ma[1] = rgx.ma[1];
		pcpc->postshot_ma[0] = rgx.ma[0];

		// Re-estimate X-point 

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n\n\n\n");
		for (i = 0; i < marksequencelen; i++) {
			double x_, y_;
			double xi;
			double erri;
			double q_, m_, b_;

			double tsd;
			
			if (i <= shotframe) {
				q_ = 0;
				m_ = pcpc->preshot_m_;
				b_ = pcpc->preshot_b_;
			} else {
				q_ = pcpc->postshot_ma[2];
				m_ = pcpc->postshot_ma[1];
				b_ = pcpc->postshot_ma[0];
			}
			tsd = pcpc->tsd[i];
			if (pcpc->hsexist[i] == IANA_CLUB_EXIST) {
#if defined(USE_II)
				x_ = (q_ * i*i + m_ * i + b_);
#else
				x_ = (q_ * tsd*tsd + m_ * tsd + b_);
#endif
				y_ = (pcpc->hsy[i]);

				xi = (double)(pcpc->hsx[i]);
				if (i != shotframe) {
					erri = xi - x_; 
					if (erri < 0) {erri = -erri;}

					//#define ERRIMIN	10
#define ERRIMIN	20
					//#define ERRIMIN	15
					if (erri > ERRIMIN) {
						pcpc->hsexist[i] = IANA_CLUB_EXILE;
					}
				}
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%2d  %10d %10d  %s\n", i, (I32) x_, (I32) y_,
				//		pcpc->hsexist[i] == IANA_CLUB_EXIST?"":"<-- EXILE.." );
			}
			if (i == shotframe) {
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
			}
		}
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n\n\n\n");


		{
			I32 iter;
			I32 count;
			double xx[MARKSEQUENCELEN], yy[MARKSEQUENCELEN];
			point_t Pp, Lp;
			double ma[3];
			double r2;
			//I32 ishotframecombined;

#define REFINE_R2_0	0.7
			iter = 0;
			//ishotframecombined = (I32) pcpc->shotframecombined;
			while(1) {
				count = 0;
				for (i = 0; i < marksequencelen; i++) 
				//for (i = 0; i <= ishotframecombined + SHOTPRESHOTPOST; i++) 
					{
					if (pcpc->hsexist[i] == IANA_CLUB_EXIST) {
						Pp.x = (double)pcpc->hsx[i] + (double)pcpc->offset_x;
						Pp.y = (double)pcpc->hsy[i] + (double)pcpc->offset_y;

						iana_P2L(piana, camid, &Pp, &Lp, 0);
						//xx[count] = Lp.x;
						//yy[count] = Lp.y;
						yy[count] = Lp.x;
						xx[count] = Lp.y;
						count++;
					}
				}

				r2 = 0;
				if (count > 3) {
					res = caw_quadraticregression(xx, yy, count, &ma[2], &ma[1], &ma[0], &r2);
					if (res > 0) {
						memcpy(&pcpc->preshot_mtxa[0], &ma[0], sizeof(double) * 3);
						pcpc->preshot_mtxa_valid = 1;
					}
				} else {
					res = 0;
					cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" club fail %d res: %d\n", __LINE__, res);
					goto func_exit;
				}
#define CHECKITER_MIN 2
				if (iter < CHECKITER_MIN || r2 < REFINE_R2_0) {
					double y_, ey_;
					double maxey;
					I32 maxey_i;
					maxey = 0.0;
					maxey_i = 0;
					for (i = 0; i < marksequencelen; i++) {
						if (pcpc->hsexist[i] == IANA_CLUB_EXIST) {
							Pp.x = (double)pcpc->hsx[i] + (double)pcpc->offset_x;
							Pp.y = (double)pcpc->hsy[i] + (double)pcpc->offset_y;
							iana_P2L(piana, camid, &Pp, &Lp, 0);

						//	y_ = ma[2] * Lp.x * Lp.x + ma[1] * Lp.x + ma[0];
							y_ = ma[2] * Lp.y * Lp.y + ma[1] * Lp.y + ma[0];

//							ey_ = y_ - Lp.y;
							ey_ = y_ - Lp.x;
							if (ey_ < 0) ey_ = -ey_;

							if (maxey < ey_) {
								maxey = ey_;
								maxey_i = i;
							}
							/*
							if (iter == 0) {
								break;
							}
							*/
						}
					}

					pcpc->hsexist[maxey_i] = IANA_CLUB_EXILE;
					cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" at %d,  [%d] -> EXILE\n", __LINE__, maxey_i);
				} else {
					break;
				}
				iter++;
			}
		}
		
		{
			I32 count;
			I32 ishotframecombined;
			double tt[MARKSEQUENCELEN], xx[MARKSEQUENCELEN], yy[MARKSEQUENCELEN];
			point_t Pp, Lp;
			U64 ts64shot;
			double ts64shotd;
			double ma[3];
			double r2;

			ishotframecombined = (I32) pcpc->shotframecombined;

			ts64shot = pcpc->ts64[ishotframecombined];
			ts64shotd = (double) (ts64shot / TSSCALE_D);

			//-- 1) Pre-shot..
			count = 0;
			for (i = 0; i <= ishotframecombined + SHOTPRESHOTPOST; i++) {
				if (pcpc->hsexist[i] == IANA_CLUB_EXIST) {
					Pp.x = (double)pcpc->hsx[i] + (double)pcpc->offset_x;
					Pp.y = (double)pcpc->hsy[i] + (double)pcpc->offset_y;

					iana_P2L(piana, camid, &Pp, &Lp, 0);
					xx[count] = Lp.x;
					yy[count] = Lp.y;
					tt[count] = (pcpc->ts64[i]  / TSSCALE_D) - ts64shotd;
					count++;
				}
			}

			pcpc->preshot_mtxa_valid = 0;
			pcpc->preshot_mtya_valid = 0;
			if (count > 3) {
				I32 preshot;

//#define PPPREPRE	1
///#define PPPREPRE	4
#define PPPREPRE	5
//#define PPPREPRE	7
				if (count > PPPREPRE) {
					preshot = count - PPPREPRE;
					count =  PPPREPRE;
				} else {
					preshot = 0;
				}
				res = caw_quadraticregression(tt+preshot, xx+preshot, count, &ma[2], &ma[1], &ma[0], &r2);
//#define REFINE_R2	0.8
#define REFINE_R2	0.7
				//if (r2 < REFINE_R2) {
				//	res = 0;
				//	goto func_exit;
				//}
				if (res > 0) {
					memcpy(&pcpc->preshot_mtxa[0], &ma[0], sizeof(double) * 3);
					pcpc->preshot_mtxa_valid = 1;
				}

				//res = caw_quadraticregression(tt, yy, count, &ma[2], &ma[1], &ma[0], &r2);
				res = cr_regression(tt+preshot, yy+preshot, count, &ma[1], &ma[0]);
				ma[2] = 0;
				if (res > 0) {
					memcpy(&pcpc->preshot_mtya[0], &ma[0], sizeof(double) * 3);
					pcpc->preshot_mtya_valid = 1;
				}
			} else {
				res = 0;
				cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" club fail %d res: %d\n", __LINE__, res);
				goto func_exit;
			}


			//-- 2) Post-shot..
			count = 0;
			//for (i = ishotframecombined+1; i < MARKSEQUENCELEN; i++) 
			for (i = ishotframecombined-1; i < marksequencelen; i++) 
			//for (i = ishotframecombined-3; i < MARKSEQUENCELEN; i++) 
			
			{
				if (pcpc->hsexist[i] == IANA_CLUB_EXIST) {
					Pp.x = (double)pcpc->hsx[i] + (double)pcpc->offset_x;
					Pp.y = (double)pcpc->hsy[i] + (double)pcpc->offset_y;

					iana_P2L(piana, camid, &Pp, &Lp, 0);
					xx[count] = Lp.x;
					yy[count] = Lp.y;
					tt[count] = (pcpc->ts64[i]  / TSSCALE_D) - ts64shotd;
					count++;
				}
			}

			pcpc->postshot_mtxa_valid = 0;
			pcpc->postshot_mtya_valid = 0;
			if (count > 3) {
				res = caw_quadraticregression(tt, xx, count, &ma[2], &ma[1], &ma[0], &r2);
				//if (r2 < REFINE_R2) {
				//	res = 0;
				//	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" club fail %d res: %d\n", __LINE__, res);
				//	goto func_exit;
				//}
				if (res > 0) {
					memcpy(&pcpc->postshot_mtxa[0], &ma[0], sizeof(double) * 3);
					pcpc->postshot_mtxa_valid = 1;
				}
				else {
					res = 0;
					cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" club fail %d res: %d\n", __LINE__, res);
					goto func_exit;
				}

				//res = caw_quadraticregression(tt, yy, count, &ma[2], &ma[1], &ma[0], &r2);
				res = cr_regression(tt, yy, count, &ma[1], &ma[0]);
				ma[2] = 0;
				if (res > 0) {
					memcpy(&pcpc->postshot_mtya[0], &ma[0], sizeof(double) * 3);
					pcpc->postshot_mtya_valid = 1;
				}
			} else {
				res = 0;
				cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" club fail %d res: %d\n", __LINE__, res);
				goto func_exit;
			}

			{
				double x_, y_;
				double mxa[3];
				double mya[3];
				double td;
				for (i = 0; i < marksequencelen; i++) {
					if (i <= ishotframecombined) {
						memcpy(&mxa[0], &pcpc->preshot_mtxa[0], sizeof(double) * 3);
						memcpy(&mya[0], &pcpc->preshot_mtya[0], sizeof(double) * 3);
					} else {
						memcpy(&mxa[0], &pcpc->postshot_mtxa[0], sizeof(double) * 3);
						memcpy(&mya[0], &pcpc->postshot_mtya[0], sizeof(double) * 3);
					}
					if (pcpc->hsexist[i] == IANA_CLUB_EXIST) {
						td = (pcpc->ts64[i]  / TSSCALE_D) - ts64shotd;
						x_ = mxa[2] * td*td + mxa[1] * td + mxa[0];
						y_ = mya[2] * td*td + mya[1] * td + mya[0];

						Pp.x = (int)pcpc->hsx[i] + pcpc->offset_x;
						Pp.y = (int)pcpc->hsy[i] + pcpc->offset_y;
						iana_P2L(piana, camid, &Pp, &Lp, 0);

						/*
						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d] (%10lf) hxL,hyL %10lf %10lf vs %10lf %10lf\n",
								i, td, 
								Lp.x, Lp.y,
								x_, y_);
								*/

					}
					//if (i == ishotframecombined) {
					//	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
					//}
				}
			}
		}


		//---------------------------------------------------------------
		{			// Re-calc 				preshot_myxa and postshot_myxa
			U32 frame;
			U32 icount;
			double x_;
			I32 beforeshotcount;
			I32 aftershotcount;
			double xx[MARKSEQUENCELEN], yy[MARKSEQUENCELEN];

			//--
			beforeshotcount = 0;
			aftershotcount = 0;
			icount = 0;
			for (frame = 0; frame < (U32) marksequencelen; frame++) {
				double q_, m_, b_;
				double tsd;
				if (frame < pcpc->shotframe) {		// Before shot
					q_ = 0;
					m_ = pcpc->preshot_m_;
					b_ = pcpc->preshot_b_;


				} else {
					q_ = pcpc->postshot_ma[2];
					m_ = pcpc->postshot_ma[1];
					b_ = pcpc->postshot_ma[0];
				}
				tsd = pcpc->tsd[frame];
#if defined(USE_II)
				x_ = (I32) (q_*frame*frame + m_*frame + b_);
#else
				x_ = (I32) (q_*tsd*tsd + m_*tsd + b_);
#endif
				if (pcpc->hsexist[frame] == IANA_CLUB_EXIST) {
					if (frame < pcpc->shotframe) {		// Before shot
						beforeshotcount++;
					} else {
						aftershotcount++;
					}
					xx[icount] = x_;
					yy[icount] = pcpc->hsy[frame];
					icount++;
				}
			}

			if (icount >= 3) {
				double r2;
				//double q_, m_, b_;
				double mya[3];
				//I32 pickme;
				I32 regressioncount;

				//--
				regressioncount = beforeshotcount + SHOTPRESHOTPOST;
				if (regressioncount > (I32)icount) {
					regressioncount = (I32)icount;
				}
				caw_quadraticregression(xx, yy, regressioncount, &mya[2], &mya[1], &mya[0], &r2);

				if (right0left1 == 0) {			// RIGHT Handed
#define MAXA2	(0.001)
					if (mya[2] >MAXA2) {
						res = 0;
						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" club fail %d res: %d\n", __LINE__, res);
						goto func_exit;
					}
					if (mya[2] < 0) {
						res = 0;
						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" club fail %d res: %d\n", __LINE__, res);
						goto func_exit;
						//mya[2] = 0;
						//cr_regression(xx, yy, regressioncount, &mya[1], &mya[0]);
					}


				} else {
					if (mya[2] < -MAXA2) {
						res = 0;
						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" club fail %d res: %d\n", __LINE__, res);
						goto func_exit;
					}

					if (mya[2] > 0) {
						res = 0;
						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" club fail %d res: %d\n", __LINE__, res);
						goto func_exit;

						//mya[2] = 0;
						//cr_regression(xx, yy, regressioncount, &mya[1], &mya[0]);
					}
				}
				/*
				if (r2 < REFINE_R2) {
					res = 0;
					cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" club fail %d res: %d\n", __LINE__, res);
					goto func_exit;
				}
				*/

				pcpc->preshot_myxa[2] = mya[2];
				pcpc->preshot_myxa[1] = mya[1];
				pcpc->preshot_myxa[0] = mya[0];

				//-----
#if 1
				regressioncount = icount - beforeshotcount + 2;
				if (regressioncount < 3) {
					regressioncount = 3;
				}
				if (regressioncount > (I32)icount) {
					regressioncount = icount;
				}
				frame0 = icount - regressioncount;
				caw_quadraticregression(xx+frame0, yy+frame0, regressioncount, &mya[2], &mya[1], &mya[0], &r2);
				if (right0left1 == 0) {			// RIGHT Handed
					if (mya[2] < 0) {
						mya[2] = 0;
						cr_regression(xx+frame0, yy+frame0, regressioncount, &mya[1], &mya[0]);
					}
				} else {						// LEFT Handed
					if (mya[2] > 0) {
						mya[2] = 0;
						cr_regression(xx+frame0, yy+frame0, regressioncount, &mya[1], &mya[0]);
					}
				}
				/*
				if (r2 < REFINE_R2) {
					res = 0;
					cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d res: %d\n", __LINE__, res);
					goto func_exit;
				}
				*/
#endif
#if 0
				regressioncount = icount;
				frame0 = 0;
				caw_quadraticregression(xx, yy, icount, &mya[2], &mya[1], &mya[0], &r2);
				if (mya[2] < 0) {
					mya[2] = 0;
					cr_regression(xx, yy, icount, &mya[1], &mya[0]);
				}
#endif
				pcpc->postshot_myxa[2] = mya[2];
				pcpc->postshot_myxa[1] = mya[1];
				pcpc->postshot_myxa[0] = mya[0];
			} 		// if (icount >= 3) 
		}
		//---------------------------------------------------------------
		res = 1;


		{
			I32 i, j;
			I32 y;
			I32 x_, y_;
			I32 y0, y1;
			I32 hpv, hpvsum;
			I32 hpvmean, hpvcount, hpvthreshold;
			I32 hlen;
			I32 hpvfilted[FULLHEIGHT];
			point_t Pc, Lc;
			double ydist;
			iana_cam_t	*pic;

//			double q_, m_, b_;
//			double mya[3];

			I32 frame;

			double yy[MARKSEQUENCELEN], hh[MARKSEQUENCELEN];
			I32 hcount;


			//-------
			pcpc->myha[2] = pcpc->myha[1] = pcpc->myha[0] = 0;
			pic = piana->pic[camid];
			memset(&hpvfilted[0], 0, sizeof(I32) * FULLHEIGHT);
			//#define WINDOWY		4
#define WINDOWY		10
			//#define WINDOWY		0
#define WINDOWYh	(WINDOWY/2)

//			q_ = 0;
//			m_ = pcpc->preshot_m_;
//			b_ = pcpc->preshot_b_;


//			memcpy(&mya[0], &pcpc->preshot_myxa[0], sizeof(double)*3);
			hcount = 0;
			for (frame = 0; frame < marksequencelen; frame++) {
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "frame: %d\n", frame);
//				if (frame > (I32)pcpc->shotframe) {		
//					q_ = pcpc->postshot_ma[2];
//					m_ = pcpc->postshot_ma[1];
//					b_ = pcpc->postshot_ma[0];
//					memcpy(&mya[0], &pcpc->postshot_myxa[0], sizeof(double)*3);
//				}

				for (i = WINDOWYh; i < pcpc->endy-WINDOWYh; i++) {
					hpvsum = 0;
					for (j = -WINDOWYh; j <= WINDOWYh; j++) {
						hpvsum += (pcpc->houghpvalue0[frame][i+j] +  pcpc->houghpvalue1[frame][i+j]);
					}
					hpvfilted[i] = hpvsum / (2*WINDOWYh+1);
				}
 
				if (s_clubHposEst2) {
					ClubPath_GetClubHposEst2(piana, camid, pcpc, frame, &Pc, &Lc);
				} else {
					ClubPath_GetClubHposEst(piana, camid, pcpc, frame, &Pc, &Lc);
				}
				x_ = (I32) (Pc.x - pcpc->offset_x);
				y_ = (I32) (Pc.y - pcpc->offset_y);


//				x_ = (I32) (q_*frame*frame + m_*frame + b_);
//				y_ = (I32) (mya[2]*x_*x_ + mya[1]*x_ + mya[0]);

//				Pc.x = x_ + pcpc->offset_x;
//				Pc.y = y_ + pcpc->offset_y;
//				iana_P2L(piana, camid, &Pc, &Lc);

				ydist = Lc.y - pic->icp.L.y;

#define HEADERCALC_YOFFSET0	50
#define HEADERCALC_YOFFSET1	100

				hpvsum = 0;
				hpvcount = 0;
				for (y = pcpc->endy - HEADERCALC_YOFFSET1; y < pcpc->endy; y++) {
					hpv = hpvfilted[y];
					if (hpv > 0) {
						hpvsum += hpv;
						hpvcount++;
					}
				}
#define HPVCOUNTMIN	10
#define HEADERCALC_THRESHOLD_RATE	1.5
				//#define HEADERCALC_THRESHOLD_RATE	1.3
#define HEADERCALC_THRESHOLD_MIN	7
				if (hpvcount > HPVCOUNTMIN) {
					hpvmean = hpvsum / hpvcount;
				} else {
					hpvmean = (I32) (HEADERCALC_THRESHOLD_MIN / HEADERCALC_THRESHOLD_RATE);
				}

				hpvthreshold = (I32) (hpvmean * HEADERCALC_THRESHOLD_RATE);
				if (hpvthreshold < HEADERCALC_THRESHOLD_MIN) {
					hpvthreshold = HEADERCALC_THRESHOLD_MIN;
				}


				if (right0left1 == 0) {			// RIGHT Handed
					y0 = -999;
					y1 = -999;
					for (y = 0; y < pcpc->endy; y++) {
						hpv = hpvfilted[y];
						if (hpv > hpvthreshold) {
							y0 = y;
							break;
						}
					}

					if (y0 > 0) {		// CANNOT found it;;
						for (y = y0 + 10; y < pcpc->endy; y++) {
							//hpv = pcpc->houghpvalue0[frame][y] +  pcpc->houghpvalue1[frame][y];
							hpv = hpvfilted[y];
							if (hpv < hpvthreshold) {
								y1 = y;
								break;
							}
						}
					}

					if (y0 > 0 && y1 > 0 && y1 > y0) {
						hlen = y1 - y0;
						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d]threshold: %3d,  y0: %3d y1: %3d, hlen: %3d ydist: %10lf\n", frame, hpvthreshold, y0, y1, hlen, ydist);
						pcpc->hlen[frame] = hlen;
						yy[hcount] = ydist;
						hh[hcount] = hlen;
						hcount++;
					}
				} else {						// LEFT Handed
					y0 = 9999;
					y1 = 9999;
					for (y = pcpc->endy-1; y > 0 ; y--) {
						hpv = hpvfilted[y];
						if (hpv > hpvthreshold) {
							y0 = y;
							break;
						}
					}

					if (y0 < 9999) {		// CANNOT found it;;
						for (y = y0 - 10; y > 0 ; y--) 
						{
							//hpv = pcpc->houghpvalue0[frame][y] +  pcpc->houghpvalue1[frame][y];
							hpv = hpvfilted[y];
							if (hpv < hpvthreshold) {
								y1 = y;
								break;
							}
						}
					}

					if (y0 < 9999  && y1 < 9999 && y1 < y0) {
						//hlen = y1 - y0;
						hlen = y0 - y1;
						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d]threshold: %3d,  y0: %3d y1: %3d, hlen: %3d ydist: %10lf\n", frame, hpvthreshold, y0, y1, hlen, ydist);
						pcpc->hlen[frame] = hlen;
						yy[hcount] = ydist;
						hh[hcount] = hlen;
						hcount++;
					}
				}
			}
			if (hcount > 3) {
				I32 res;
				double ma[3];
				double r2;
				point_t Pc, Lc;
				double x_, y_;
				double hlen_;

				res = caw_quadraticregression(yy, hh, hcount, &ma[2], &ma[1], &ma[0], &r2);
				if (res > 0) {
					pcpc->myha[2] = ma[2];
					pcpc->myha[1] = ma[1];
					pcpc->myha[0] = ma[0];
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "hcount: %d ma[2,1,0]: %10lf %10lf %10lf\n", hcount, ma[2], ma[1], ma[0]);

					for (frame = 0; frame < marksequencelen; frame++) {
						if (s_clubHposEst2) {
							ClubPath_GetClubHposEst2(piana, camid, pcpc, frame, &Pc, &Lc);
						} else {
							ClubPath_GetClubHposEst(piana, camid, pcpc, frame, &Pc, &Lc);
						}

						x_ = Pc.x - pcpc->offset_x;
						y_ = Pc.y - pcpc->offset_y;
						ydist = Lc.y - pic->icp.L.y;

						hlen_ = ma[2]*ydist*ydist + ma[1]*ydist + ma[0];

						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d] y0, ydist, hlen_ : %10lf %10lf %10lf\n", frame, 
						//		Lc.y, ydist, hlen_);
					}
					hlen_ = ma[2]*0*0 + ma[1]*0 + ma[0];			// ma[0].... 
					
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "hlen at BALL y0, hlen_ : %10lf      %10lf\n", pic->icp.L.y, hlen_);
					if (s_hlenminmax) {
						if (hlen_ < pcpc->hlenmin) {
							hlen_ = pcpc->hlenmin;
						}
						if (hlen_ > pcpc->hlenmax) {
							hlen_ = pcpc->hlenmax;
						}
					}
					pcpc->hlenEstimated = hlen_;
					{
						double dx, dy;
						point_t Pp1, Lp1; 
						point_t Pp2, Lp2; 

						Lp1.x = pic->icp.L.x;
						Lp1.y = pic->icp.L.y;
						Pp1.x = pic->icp.P.x;
						Pp1.y = pic->icp.P.y;

						Pp2.x = Pp1.x + pcpc->hlenEstimated;
						Pp2.y = Pp1.y;
						iana_P2L(piana, camid, &Pp2, &Lp2, 0);

						dx = Lp1.x - Lp2.x;
						dy = Lp1.y - Lp2.y;
						pcpc->hlenEstimatedL = sqrt(dx*dx+dy*dy);
						/*
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "hlen: %10lf   hlenL: %10lf  %10lf[mm]   (%10lf ~ %10lf)\n", 
								pcpc->hlenEstimated, 
								pcpc->hlenEstimatedL,
								pcpc->hlenEstimatedL*1000,
								pcpc->hlenmin, pcpc->hlenmax);
								*/
					}

					{
						double x0_, y0_;
						//point_t Pp0, Lp0;
						double x_, y_;
						point_t Pp, Lp;

						x0_ = pic->icp.P.x - pcpc->offset_x;
						y0_ = pic->icp.P.y - pcpc->offset_y;				// Ball position

						x_ = x0_;
						y_ = pcpc->preshot_myxa[2] * x_*x_ + pcpc->preshot_myxa[1] * x_ + pcpc->preshot_myxa[0];

						Pp.x = x_ + pcpc->offset_x;
						Pp.y = y_ + pcpc->offset_y;
						Pp.y = Pp.y + pcpc->hlenEstimated / 2;
						iana_P2L(piana, camid, &Pp, &Lp, 0);			// Hit point
						pcpc->hitoffsetL = pic->icp.L.x - Lp.x;		

						/*
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "hit point: %10lf %10lf (Ballpos: %10lf %10lf)  deltaLx: %10lf %10lf[mm]\n", 
								Lp.x, Lp.y,
								pic->icp.L.x, pic->icp.L.y,
								pcpc->hitoffset,
								pcpc->hitoffset * 1000
								);
						*/
					}

				}
			}
		}
	} else {
		res = 0;
	}
	UNUSED(camid);
	UNUSED(piana);
func_exit:
	return res;
}


static I32 ClubPath_GetClubHposEst(iana_t *piana, U32 camid, clubpath_context_t *pcpc, U32 frame, point_t *pPp, point_t *pLp)
{
	I32 res;
	double x_, y_;
	double mya[3];
	double q_, m_, b_;
	double tsd;

	//---
	if (frame <= pcpc->shotframe) {		
		q_ = 0;
		m_ = pcpc->preshot_m_;
		b_ = pcpc->preshot_b_;
		memcpy(&mya[0], &pcpc->preshot_myxa[0], sizeof(double)*3);
	}  else {
		q_ = pcpc->postshot_ma[2];
		m_ = pcpc->postshot_ma[1];
		b_ = pcpc->postshot_ma[0];
		memcpy(&mya[0], &pcpc->postshot_myxa[0], sizeof(double)*3);
	}

	tsd = pcpc->tsd[frame];
#if defined(USE_II)
	x_ = (I32) (q_*frame*frame + m_*frame + b_);
#else
	{
		double itq_, itm_, itb_;
		itq_ = pcpc->maiitt[2];
		itm_ = pcpc->maiitt[1];
		itb_ = pcpc->maiitt[0];
		tsd = itq_ * frame*frame + itm_ * frame + itb_;
	}
	x_ = (I32) (q_*tsd*tsd + m_*tsd + b_);
#endif
	y_ = (I32) (mya[2]*x_*x_ + mya[1]*x_ + mya[0]);

	pPp->x = x_ + pcpc->offset_x;
	pPp->y = y_ + pcpc->offset_y;
	iana_P2L(piana, camid, pPp, pLp, 0);

	res = 1;

	return res;
}


static I32 ClubPath_GetClubHposEst2(iana_t *piana, U32 camid, clubpath_context_t *pcpc, U32 frame, point_t *pPp, point_t *pLp)	
{
	I32 res;
	double x_a, y_a;
	double x_b, y_b;
	double x_, y_;
	double mya[3];
	double q_, m_, b_;
	double tsd;
	//------------

	//--- B
	q_ = 0;
	m_ = pcpc->preshot_m_;
	b_ = pcpc->preshot_b_;
	memcpy(&mya[0], &pcpc->preshot_myxa[0], sizeof(double)*3);

	tsd = pcpc->tsd[frame];
#if defined(USE_II)
	x_b = (I32) (q_*frame*frame + m_*frame + b_);
#else
	{
		double itq_, itm_, itb_;
		itq_ = pcpc->maiitt[2];
		itm_ = pcpc->maiitt[1];
		itb_ = pcpc->maiitt[0];
		tsd = itq_ * frame*frame + itm_ * frame + itb_;
	}
	x_b = (I32) (q_*tsd*tsd + m_*tsd + b_);
#endif
	y_b = (I32) (mya[2]*x_b*x_b + mya[1]*x_b + mya[0]);

	//--- A
	q_ = pcpc->postshot_ma[2];
	m_ = pcpc->postshot_ma[1];
	b_ = pcpc->postshot_ma[0];
	memcpy(&mya[0], &pcpc->postshot_myxa[0], sizeof(double)*3);

	tsd = pcpc->tsd[frame];
#if defined(USE_II)
	x_a = (I32) (q_*frame*frame + m_*frame + b_);
#else
	{
		double itq_, itm_, itb_;
		itq_ = pcpc->maiitt[2];
		itm_ = pcpc->maiitt[1];
		itb_ = pcpc->maiitt[0];
		tsd = itq_ * frame*frame + itm_ * frame + itb_;
	}
	x_a = (I32) (q_*tsd*tsd + m_*tsd + b_);
#endif
	y_a = (I32) (mya[2]*x_a*x_a + mya[1]*x_a + mya[0]);

#define FRAME0	2
#define FRAME1	2
	if ((I32)frame <= (I32)pcpc->shotframe -FRAME0 ) {		
		x_ = x_b;
		y_ = y_b;
	} else if ((I32)frame <= (I32)pcpc->shotframe +FRAME1 ) {		
		double p, q;

		q = (frame  - (pcpc->shotframe -FRAME0)) / ((double)(FRAME0+FRAME1));
		p = 1.0 - q;

		x_ = x_b * p + x_a * q;
		y_ = y_b * p + y_a * q;
	} else {
		x_ = x_a;
		y_ = y_a;
	}


	pPp->x = x_ + pcpc->offset_x;
	pPp->y = y_ + pcpc->offset_y;
	iana_P2L(piana, camid, pPp, pLp, 0);

	res = 1;

	return res;
}



/*!
 ********************************************************************************
 *	@brief      Club
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2017/0516
 *******************************************************************************/
static I32 ClubPath_RegressPosition(iana_t *piana, U32 camid, clubpath_context_t *pcpc, regressionx_t *prgx)	
{
	I32 res;

	//I32 frame;
	I32 i, j;
	//I32 i0, i1;
	I32 frame0, frame1;
	I32 validcount;
	//I32 xb, yb;
	iana_cam_t	*pic;
	I32 shotframe;
	double shotframedecimal;
	double shotframecombined;
	double shotx;
	I32 goodcount[MARKSEQUENCELEN];

	I32 gcmax;
	I32 gcmi;
	double gcmm_, gcmb_;
	double gcm_shotframecombined;

	double xj, xj_;
	double errj;
	double m_, b_;

	//------
	// 1) check valid count
	// 2) Get line passes ball x poition and club x postion vs frame value.
	//    And count points touch for each line. (Under proper error value)
	// 3) Get Maximum point count line and set MaxLine
	// 4) Check if each point is contained to MaxLine. and  Re-estimate using regression;
	// 5) Re-estimate X-point 
//#define SHOTVALIDCOUNT	4
//#define SHOTVALIDCOUNT	6
//#define SHOTVALIDCOUNT0	3
//#define SHOTVALIDCOUNT0	5
	
	pic 	= piana->pic[camid];
	shotframe = pcpc->shotframe;
	shotx = (double) (pic->icp.P.x - pcpc->offset_x);

	frame0 = prgx->frame0;
	frame1 = prgx->frame1;
	// 1) Check valid count
	validcount = 0;
	for (i = frame0; i <= frame1; i++) {
		if (pcpc->hsexist[i] == IANA_CLUB_EXIST) {
			validcount++;
		}
	}

	if (validcount < SHOTVALIDCOUNT) {
		res = 0;
		//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" fail %d res: %d\n", __LINE__, res);
		goto func_exit;
	}

	memset(goodcount, 0, sizeof(I32)*MARKSEQUENCELEN);
	// 2) Get line passes ball x poition and club x postion vs frame value.
	//    And count points touch for each line. (Under proper error value)
	gcmax = 0;		// Good count max
	gcmi  = 0;
	gcmm_ = 0.0;	// m_ for Good count max
	gcmb_ = 0.0;	// b_ for Good count max
	gcm_shotframecombined = 0.0;

	for (i = frame0; i <= frame1; i++) 
	{
		if (pcpc->hsexist[i] == IANA_CLUB_EXIST)
		{
			// 1) line
//#define SHOTINDEXDECIMAL		(0.9)
//#define SHOTINDEXDECIMALSTEP	(0.1)
#define SHOTINDEXDECIMAL		(1.8)
#define SHOTINDEXDECIMALSTEP	(0.2)

			for (shotframedecimal = -SHOTINDEXDECIMAL; shotframedecimal < SHOTINDEXDECIMAL; shotframedecimal += SHOTINDEXDECIMALSTEP) {
				double deltai;

				if (prgx->recalcshotframe == 0) {
					shotframecombined = prgx->shotframecombined;
				} else {
					shotframecombined = shotframe + shotframedecimal;
				}

				deltai = i - shotframecombined;
				if (deltai > -SHOTINDEXDECIMALSTEP && deltai < SHOTINDEXDECIMALSTEP) {			// Too small deltai
					continue;
				}


				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "========= shotindexcombined: %lf,  deltai: %lf\n", shotframecombined, deltai);

				m_ = ((double)(pcpc->hsx[i] - shotx)) / deltai;					// x = m_ * i + b_,  m_ = (x1 - x0) / (i1 - i0), b_ = x1 - m_*i1
				b_ = (double)(shotx - m_ * shotframecombined);

				goodcount[i] = 0;
				for (j = frame0; j <= frame1; j++) 
				{
					if (pcpc->hsexist[j] == IANA_CLUB_EXIST) {
						xj = (double)(pcpc->hsx[j]);
						{
							xj_ = m_ * j + b_;
						}
						errj = xj - xj_;
						if (errj < 0) {errj = -errj;}
#define ERRRJMIN	10
						if (errj < ERRRJMIN) {
							goodcount[i]++;
						}

						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d %10lf j:%d] xj: %10lf xj: %10lf, errj: %10lf\n",
						//		i, shotframecombined, j, 
						//		xj, xj_, errj);
					}
				}
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "--[%d] goodcount: %d\n", i, goodcount[i]);

				// 3) Get Maximum point count line and set MaxLine
				if (gcmax < goodcount[i]) {
					gcmax = goodcount[i];
					gcmm_ = m_;
					gcmb_ = b_;
					gcmi  = i;
					gcm_shotframecombined = shotframecombined;
				}
				if (prgx->recalcshotframe == 0) {
					break;
				}
			}
		}
	}

//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "== shotframe: %d, shotx: %d, gm i: %d %lf, count: %d, m_: %lf, b: %lf\n", 
//			shotframe, (int) shotx, gcmi, gcm_shotframecombined, gcmax, gcmm_, gcmb_);
	
	// 4) Check if each point is contained to MaxLine. and  Re-estimate using regression;
	if (gcmax >= SHOTVALIDCOUNT0) {
//#define SHOTPOINTCOUNT	5
#define SHOTPOINTCOUNT	0
//#define SHOTPOINTCOUNT	5

//#define SHOTPOINTCOUNT	3
		I32 count;
		double ii[MARKSEQUENCELEN + SHOTPOINTCOUNT];
		double tt[MARKSEQUENCELEN + SHOTPOINTCOUNT];
		double xx[MARKSEQUENCELEN + SHOTPOINTCOUNT];

		count = 0;
		for (j = frame0; j <= frame1; j++) 
		{
			if (pcpc->hsexist[j] == IANA_CLUB_EXIST) {
				xj = (double)(pcpc->hsx[j]);
				xj_ = gcmm_ * j + gcmb_;

				errj = xj - xj_;
				if (errj < 0) {errj = -errj;}
				//#define ERRRJMIN	10
				if (errj < ERRRJMIN) {
//#if defined(USE_II)
					ii[count] = (double)j;
//#else
					tt[count] = pcpc->tsd[j];;
//#endif
					xx[count] = (double)xj;
					count++;
				}
			}
		}
		if (count > 2 + DISCARDCOUNT) {
			double ma[3];
			double r2;
			//--- SHOT POINT weight...

			caw_quadraticregression(ii, tt, count, &ma[2], &ma[1], &ma[0], &r2);
			prgx->maiitt[2] = ma[2];
			prgx->maiitt[1] = ma[1];
			prgx->maiitt[0] = ma[0];
			
			
			
			for (j = 0; j < SHOTPOINTCOUNT; j++) {
				//ii[count+j] = (double)shotframe;
#if defined(USE_II)
				ii[count+j] = gcm_shotframecombined;
#else
				tt[count+j] = 0;				// shot time.. :P
#endif
				xx[count+j] = (double)shotx;
			}

			if (prgx->usequadratic) {
#if defined(USE_II)
				caw_quadraticregression(ii+DISCARDCOUNT, xx+DISCARDCOUNT, count+SHOTPOINTCOUNT-DISCARDCOUNT, &ma[2], &ma[1], &ma[0], &r2);
#else
				caw_quadraticregression(tt+DISCARDCOUNT, xx+DISCARDCOUNT, count+SHOTPOINTCOUNT-DISCARDCOUNT, &ma[2], &ma[1], &ma[0], &r2);
#endif
			} else {
				ma[2] = 0;
#if defined(USE_II)
				cr_regression(ii+DISCARDCOUNT, xx+DISCARDCOUNT, count+SHOTPOINTCOUNT-DISCARDCOUNT, &ma[1], &ma[0]);
#else
				cr_regression(tt+DISCARDCOUNT, xx+DISCARDCOUNT, count+SHOTPOINTCOUNT-DISCARDCOUNT, &ma[1], &ma[0]);
#endif
			}
			prgx->ma[2] = ma[2];
			prgx->ma[1] = ma[1];
			prgx->ma[0] = ma[0];
			if (prgx->recalcshotframe == 1) {
				prgx->shotframecombined = gcm_shotframecombined;
			}
			res = 1;
		} else {
			prgx->ma[2] = 0;
			prgx->ma[1] = 0;
			prgx->ma[0] = 0;

			prgx->maiitt[2] = 0;
			prgx->maiitt[1] = 0;
			prgx->maiitt[0] = 0;
			res = 0;
		}
	} else {
		res = 0;
	}

	UNUSED(camid);
	UNUSED(piana);
func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Club
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: changed, 0: NOT changed.
 *
 *	@author	    yhsuk
 *  @date       2017/0419
 *******************************************************************************/
static I32 ClubPath_CalcRoi(	
		iana_t *piana, 
		U32 camid, 
		clubpath_context_t *pcpc
		)
{
	U32 width, height;
	I32 shotx;
	I32 startx, starty;
	I32 endx, endy;
	I32 roiwidth, roiheight;
	I32 imgoffset;
	I32 shotframe;
	I32 frame;
	iana_cam_t	*pic;
	U64 ts64;
	double tsd;

	//--
	width 	= pcpc->width;
	height 	= pcpc->height;

	pic 	= piana->pic[camid];

	shotx = (I32) pic->icp.P.x - pcpc->offset_x;
	shotframe = pcpc->shotframe;
	frame = pcpc->frame;

	ts64 = pcpc->ts64[frame];
	tsd = pcpc->tsd[frame];
//	tsd  = (ts64 / TSSCALE_D) - (pcpc->ts64shot / TSSCALE_D);

#if defined(CLUB3_TEST)
	if (frame < (I32)pcpc->shotframe+1) 
#else
	if (frame < (I32)pcpc->shotframe) 
#endif
//	if (tsd < 0.0)
	{		// Before shot
		//startx = shotx - width/4;
		startx = shotx - width/8;
		if (startx < 0) {
			startx = 0;
		}
		roiwidth = (width * 7) / 8;
		endx = startx + roiwidth - 1;
	} else {		// After shot
		double m_, b_;

		startx = 0;

		m_ = pcpc->preshot_m_;
		b_ = pcpc->preshot_b_;

		//x_ = m_ * frame + b_;			// (shotframe, shotx)
//#define AFTERSHOTRATE	0.8
#define AFTERSHOTRATE	0.5
		//endx = (int) ((m_ * AFTERSHOTRATE) * (frame - shotframe) + shotx + width / 32);
#if defined(USE_II)
		endx = (int) ((m_ * AFTERSHOTRATE) * (frame - shotframe) + shotx + width / 16);
#else
		endx = (int) ((m_ * AFTERSHOTRATE) * tsd + shotx + width / 16);
#endif
			
		//endx = shotx + width / 16;
	}
	if (endx >= (I32)width) endx = width-1;
#define MINENDX	32
	if (endx < MINENDX) endx = MINENDX;
	roiwidth = endx - startx + 1;

	starty = 0;
	roiheight =  height;


//#define FULLIMAGE
#if defined(FULLIMAGE)
	startx = 0;
	starty = 0;

	roiwidth  =  pcpc->width;
	roiheight =  pcpc->height;

#endif
	imgoffset =  startx + starty * width;

	endx = startx + roiwidth - 1;
	endy = starty + roiheight - 1;

	pcpc->startx 	= startx;
	pcpc->starty 	= starty;
	pcpc->roiwidth  = roiwidth;
	pcpc->roiheight = roiheight;
	pcpc->imgoffset = imgoffset;

	pcpc->endx = endx;
	pcpc->endy = endy;

	UNUSED(piana); UNUSED(camid);
	return 1;
}


/*!
 ********************************************************************************
 *	@brief      Club
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: changed, 0: NOT changed.
 *
 *	@author	    yhsuk
 *  @date       2017/0426
 *******************************************************************************/
static I32 ClubPath_CalcClubData(	
		iana_t *piana, 
		U32 camid, 
		clubpath_context_t *pcpc
		)
{
	I32 res;
	iana_shotresult_t *psr;
	iana_cam_t	*pic;
	//---
	
	pic 	= piana->pic[camid];
	psr = &piana->shotresultdata;
//	offset_x = pcpc->offset_x;
//	offset_y = pcpc->offset_y;

//#define RAND_FIX_TEST_0		1234
#if defined(RAND_FIX_TEST_0)
	srand(RAND_FIX_TEST_0);
#endif

//	q_ = 0;
//	m_ = pcpc->preshot_m_;
//	b_ = pcpc->preshot_b_;
//	count = 0;
//	countbs = 0;
//	countas = 0;
//	aftershot = 0;
	//-------
	{
		point_t Pc;
		point_t Lc0, Lc1;
		double dx, dy, dtmp;
		double tsd;
		double clubspeedB, clubspeedA;
		U32 f0, f1;

		//---
		// Before shot speed
//#define CLUBSPEED_BEFORESHOT	2
//#define CLUBSPEED_AFTERSHOT		2
#define CLUBSPEED_BEFORESHOT	1
#define CLUBSPEED_AFTERSHOT		1
		f0 = pcpc->shotframe - CLUBSPEED_BEFORESHOT;
		f1 = pcpc->shotframe;

		if (f0 > 0 && f1 > 0) {
			f0--;
			f1--;
		}

		tsd = pcpc->tsd[f1] - pcpc->tsd[f0];
#define DTMIN	((1.0/1000.0) * 0.01)
		if (tsd >DTMIN) {
			ClubPath_GetClubHposEst2(piana, camid, pcpc, 
					f0,
					&Pc, &Lc0);
			ClubPath_GetClubHposEst2(piana, camid, pcpc, 
					f1,
					&Pc, &Lc1);

			dx = Lc1.x - Lc0.x;
			dx = 0;
			dy = Lc1.y - Lc0.y;

			dtmp = sqrt(dx*dx+dy*dy)/tsd;
			clubspeedB = dtmp;

		} else {
			res = 0;
			//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" fail %d res: %d\n", __LINE__, res);
			goto func_exit;
		}


		{	// Check sanity of estimated position.
			double bx, by;
			double dx, dy;
			double distance;
			bx = pic->icp.L.x;
			by = pic->icp.L.y;

			//--
			dx = Lc0.x -bx;
			dy = Lc0.y -by;

			distance = sqrt(dx*dx + dy*dy);
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "distance btw ball and club 0 : %lf\n", distance);
#define HEADER_CLULB_MINDISTANCE	0.3
			if (distance > HEADER_CLULB_MINDISTANCE) {
				res = 0;
				//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" fail %d res: %d\n", __LINE__, res);
				goto func_exit;
			}
			//--
			dx = Lc1.x -bx;
			dy = Lc1.y -by;

			distance = sqrt(dx*dx + dy*dy);

			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "distance btw ball and club 1 : %lf\n", distance);
			if (distance > HEADER_CLULB_MINDISTANCE) {
				res = 0;
				//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" fail %d res: %d\n", __LINE__, res);
				goto func_exit;
			}
		}

		//---
#if 1
		{
			U32 frame0, frame1;

			//frame0 = pcpc->shotframe - 2;
			//frame1 = pcpc->shotframe - 1;
			if (pcpc->shotframe > 1) {
				frame0 = pcpc->shotframe - 1;
				frame1 = pcpc->shotframe - 0;


				frame0--;
				UNUSED(frame1);
			} else {
				frame0 = 0;
				frame1 = 1;
			}


			ClubPath_GetClubHposEst(piana, camid, pcpc, frame0, &Pc, &Lc0);
			ClubPath_GetClubHposEst(piana, camid, pcpc, frame1, &Pc, &Lc1);

			tsd = pcpc->tsd[frame1] - pcpc->tsd[frame0];

			dx = Lc1.x - Lc0.x;
			dx = 0;
			dy = Lc1.y - Lc0.y;

			dtmp = sqrt(dx*dx+dy*dy)/tsd;

#define DTMIN	((1.0/1000.0) * 0.01)
			if (tsd >DTMIN) {
				clubspeedB = dtmp;
			} else {
				clubspeedB = 0;
			}

			{		// Club speed XXX.. 2019/0116
				double vmag;
				double smashfactor;
				double maxsmashfactor;		// 20200812

				vmag = psr->vmag;

				if (vmag < 30.0) {
					maxsmashfactor = 1.3;
				} else if (vmag < 40.0) {
					maxsmashfactor = LINEAR_SCALE(vmag, 30.0, 1.30, 40.0, 1.38);
				} else if (vmag < 50.0) {
					maxsmashfactor = LINEAR_SCALE(vmag, 40.0, 1.38, 50.0, 1.45);
				} else if (vmag < 60.0) {
					maxsmashfactor = LINEAR_SCALE(vmag, 50.0, 1.45, 60.0, 1.52);
				} else {
					maxsmashfactor = 1.52;
				}

					
				if (clubspeedB < 30) {
					clubspeedB = LINEAR_SCALE(clubspeedB, 20.0, 20.0, 30.0, 33.0);
				} else if (clubspeedB < 35) {
					clubspeedB = LINEAR_SCALE(clubspeedB, 30.0, 33.0, 35.0, 39.0);
				} else if (clubspeedB < 40) {
					clubspeedB = LINEAR_SCALE(clubspeedB, 35.0, 39.0, 40.0, 42.0);
				} else if (clubspeedB < 45) {
					clubspeedB = LINEAR_SCALE(clubspeedB, 40.0, 42.0, 45.0, 45.0);
				} else if (clubspeedB < 50) {
					clubspeedB = LINEAR_SCALE(clubspeedB, 45.0, 45.0, 50.0, 49.0);
				} else if (clubspeedB < 55) {
					clubspeedB = LINEAR_SCALE(clubspeedB, 50.0, 49.0, 55.0, 53.0);
				} else {
					clubspeedB = clubspeedB - 5.0;
				}

				// 2019/0405
				if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {			// Match to GCQ
					FILE *fp;
					int mode;

					fp = cr_fopen("gcqmode.txt", "r");
					mode = 1;
					if (fp) {
						fscanf(fp, "%d", &mode);
						cr_fclose(fp);
					}

					if (mode == 1) {
						if (clubspeedB < 20) {
							clubspeedB = LINEAR_SCALE(clubspeedB,  0.0,  0.0, 20.0, 23.0);
						} else if (clubspeedB < 30) {
							clubspeedB = LINEAR_SCALE(clubspeedB, 20.0, 23.0, 30.0, 30.0);
						} else if (clubspeedB < 40) {
							clubspeedB = LINEAR_SCALE(clubspeedB, 30.0, 30.0, 40.0, 38.0);
						} else if (clubspeedB < 50) {
							clubspeedB = LINEAR_SCALE(clubspeedB, 40.0, 38.0, 50.0, 48.0);
						} else {
							clubspeedB = clubspeedB - 2.0;
						}
					}
				}
				smashfactor = vmag / clubspeedB;
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "smashfactor: %lf (csB: %lf)\n", smashfactor, clubspeedB);
#define SMASHFACTOR_LARGE	1.51
#define SMASHFACTOR_SMALL	0.5
				if (smashfactor > maxsmashfactor) {
					smashfactor = (maxsmashfactor) + ((rand() / (double) RAND_MAX)  - 0.5) * 0.02;
					clubspeedB = vmag / smashfactor;
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "====> smashfactor: %lf (csB: %lf)\n", smashfactor, clubspeedB);
				}
			}

			/*
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "=========> vvv: %10lf   (%10lf %10lf) ~ (%10lf %10lf) %10lf %10lf  %10lf %10lf\n",
				dtmp, Lc0.x, Lc0.y, Lc1.x, Lc1.y, dx, dy,
				Lc1.x - pic->icp.L.x,
				Lc1.y - pic->icp.L.y);
				*/

			//--
			frame0 = pcpc->shotframe +5;
			frame1 = pcpc->shotframe +7;

			ClubPath_GetClubHposEst(piana, camid, pcpc, frame0, &Pc, &Lc0);
			ClubPath_GetClubHposEst(piana, camid, pcpc, frame1, &Pc, &Lc1);

			tsd = pcpc->tsd[frame1] - pcpc->tsd[frame0];

			dx = Lc1.x - Lc0.x;
			dx = 0;
			dy = Lc1.y - Lc0.y;

			dtmp = sqrt(dx*dx+dy*dy)/tsd;
			if (tsd >DTMIN) {
				clubspeedA = dtmp;
			} else {
				clubspeedA = 0;
			}

			if (clubspeedA > clubspeedB) {
				res = 0;
				//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" fail %d res: %d\n", __LINE__, res);
				goto func_exit;
			}
			/*
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "##############> vvv: %10lf   (%10lf %10lf) ~ (%10lf %10lf) %10lf %10lf  %10lf %10lf\n",
				dtmp, Lc0.x, Lc0.y, Lc1.x, Lc1.y, dx, dy,
				Lc1.x - pic->icp.L.x,
				Lc1.y - pic->icp.L.y);
				*/
		}
#endif
#if 0
		// After shot speed
		f0 = pcpc->shotframe + 1;
		f1 = pcpc->shotframe + 1 + CLUBSPEED_AFTERSHOT;

		f0++;
		f1++;
		tsd = pcpc->tsd[f1] - pcpc->tsd[f0];
#define DTMIN	((1.0/1000.0) * 0.01)
		if (tsd >DTMIN) {
			ClubPath_GetClubHposEst2(piana, camid, pcpc, 
					f0,
					&Pc, &Lc0);
			ClubPath_GetClubHposEst2(piana, camid, pcpc, 
					f1,
					&Pc, &Lc1);

			dx = Lc1.x - Lc0.x;
			dy = Lc1.y - Lc0.y;

			dtmp = sqrt(dx*dx+dy*dy)/tsd;
			clubspeedA = dtmp;
		} else {
			clubspeedA = 0;
		}
#endif

#define CLUBAB_MAX	0.95
		if (clubspeedA > clubspeedB * CLUBAB_MAX) {
			clubspeedA = clubspeedB * CLUBAB_MAX;
		}
		pcpc->clubspeedB = clubspeedB;
		pcpc->clubspeedA = clubspeedA;
	}

	{
		double x0, x1;
		double y0, y1;
		double theta;
		double degree;
		double myxa[3];
		point_t Pc0, Pc1;
		point_t Lc0, Lc1;
		double q_, m_, b_;

		double dx, dy, dtmp;

		//----
		memcpy(&myxa[0], &pcpc->preshot_myxa[0], sizeof(double)*3);
		q_ = myxa[2];
		m_ = myxa[1];
		b_ = myxa[0];
//#define BALLPOS_OFFSET1	(10)
//#define BALLPOS_OFFSET2	(-10)
#define BALLPOS_OFFSET1	(5)
//#define BALLPOS_OFFSET2	(-20)
#define BALLPOS_OFFSET2	(-5)
		x0 = (pic->icp.P.x - pcpc->offset_x)+BALLPOS_OFFSET1;
		y0 = q_ * x0 * x0 + m_ * x0 + b_;

		x1 = (pic->icp.P.x - pcpc->offset_x)+BALLPOS_OFFSET2;
		y1 = q_ * x1 * x1 + m_ * x1 + b_;

		Pc0.x = x0 + pcpc->offset_x;
		Pc0.y = y0 + pcpc->offset_y;

		Pc1.x = x1 + pcpc->offset_x;
		Pc1.y = y1 + pcpc->offset_y;

		iana_P2L(piana, camid, &Pc0, &Lc0, 0);
		iana_P2L(piana, camid, &Pc1, &Lc1, 0);

		dx = Lc1.x - Lc0.x;
		dy = Lc1.y - Lc0.y;

		dtmp = dx / dy;
		theta =  atan(dtmp);
		degree = RADIAN2DEGREE(theta);


#define VMAG_USECLUBRESULT0	10.0
#define CLUBPATH_SPREAD0	10.0

#define VMAG_USECLUBRESULT1	20.0
#define CLUBPATH_SPREAD1	15.0

#define VMAG_USECLUBRESULT2	30.0
#define CLUBPATH_SPREAD2	40.0


		{
			double vmag;
			double maxspread;

			vmag = psr->vmag;

			if (vmag < VMAG_USECLUBRESULT0) {
				maxspread = CLUBPATH_SPREAD0;
			} else if (vmag < VMAG_USECLUBRESULT1) {
				maxspread = LINEAR_SCALE(vmag, VMAG_USECLUBRESULT0, CLUBPATH_SPREAD0, VMAG_USECLUBRESULT1, CLUBPATH_SPREAD1);
			} else if (vmag < VMAG_USECLUBRESULT2) {
				maxspread = LINEAR_SCALE(vmag, VMAG_USECLUBRESULT1, CLUBPATH_SPREAD1, VMAG_USECLUBRESULT2, CLUBPATH_SPREAD2);
			} else {
				maxspread = CLUBPATH_SPREAD2;
			}


			if (degree < psr->clubpath - maxspread) {
				degree = psr->clubpath - maxspread;
			}

			if (degree > psr->clubpath + maxspread) {
				degree = psr->clubpath + maxspread;
			}
		}
		pcpc->clubpath = degree;
	}

	pcpc->clubfaceangle = (10.0 * psr->azimuth - pcpc->clubpath) / 9.0;

	res = 1;

func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      Club
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: changed, 0: NOT changed.
 *
 *	@author	    yhsuk
 *  @date       2017/0426
 *******************************************************************************/
I32 iana_spin_clubimage_overlap(iana_t *piana, U32 camid)		
{
#ifdef IPP_SUPPORT
	I32 res;

	iana_cam_t	*pic;
	iana_cam_param_t *picp;
	camimageinfo_t     *pcii;
	camif_buffer_info_t *pb;
	
	U32	width;
	U32	height;
	U32	offset_x;
	U32	offset_y;
	U08	*buf;
	U32	rindex;

	U08 *bufimg;

	I32 r0, r1;

	clubpath_context_t 	*pcpc;

	I32 x_, y_;
	int icount;
	int discardcount;

	I32 index;
	I32 frame;
	I32 j;

//	U08 *IREF_2;
	U08 *MAX_2;
	U08 *MsR_2;
	U08 *I_2;
	U08 *II_2;
	U08 *IR_2;
	U08 *IR;

	int startx, starty;
	int roiwidth, roiheight;
	int imgoffset;
	IppiSize roisize;


	point_t Pc, Lc;
	IppiSize fullroisize;
	int clubpathimg_decimation;

	//I32 beforeshot;
	I32 beforeshotcount;
	I32 aftershot;
	I32 aftershotcount;


	I32 clubx[MARKSEQUENCELEN];
	I32 cluby[MARKSEQUENCELEN];

	I32 clubCx[MARKSEQUENCELEN];
	I32 clubCy[MARKSEQUENCELEN];

	I32 clubxyvalid[MARKSEQUENCELEN];
	I32 clubxycount;
	I32 drawclub[MARKSEQUENCELEN];
	I32 drawclub2[MARKSEQUENCELEN];

	double myha[3];
	U08 *pimgrefgamma;

	I32 marksequencelen;

	//--------------------
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}
	
	width = 0;
	height = 0;
	offset_x = 0;
	offset_y = 0;

	pimgrefgamma = NULL;

	memset(&drawclub[0], 0, sizeof(I32) * MARKSEQUENCELEN);
	j = 0;
#define CLUBPATHIMG_DECIMATION	3
	clubpathimg_decimation =  CLUBPATHIMG_DECIMATION;
	{
		FILE *fp;
		int mode;
		int decimation;

		int len = PATHBUFLEN;
		char dirpath[PATHBUFLEN];
		char pathstr[PATHBUFLEN];
		char drivestr[PATHBUFLEN];

							 
		OSAL_STR_ConvertUnicode2MultiByte(piana->szDrive, drivestr, len);
		OSAL_STR_ConvertUnicode2MultiByte(piana->szDir,  pathstr, len);

#if defined(_WIN32)	
		sprintf(dirpath, "%s%s\\%s", drivestr, pathstr, CLUBPATHDECIMATIONFILE);
#else
		sprintf(dirpath, "%s%s/%s", drivestr, pathstr, CLUBPATHDECIMATIONFILE);
#endif

		fp = cr_fopen(dirpath, "r");

		if (fp) {

			fscanf(fp, "%d", &mode);
			fscanf(fp, "%d", &decimation);

			cr_fclose(fp);

			if (mode == 1) {
				clubpathimg_decimation =  decimation;
			}
		}

	}

	fullroisize.height = 0; fullroisize.width = 0;

	res 	= 1;
	pic 	= piana->pic[camid];
	picp	= &pic->icp;

	bufimg 	= NULL;
	buf 	= NULL;
	pb 		= NULL;

//	IREF_2  = NULL;
	MAX_2 = NULL;
	MsR_2 = NULL;
	I_2 = NULL;
	II_2 = NULL;
	IR_2 = NULL;

	//IR = pic->pimgClubOverlap;
	IR = NULL;

	pcpc 	= &pic->cpc;
	r0 = pic->rindexshotbulk - PRESHOTCOUNT;
	if (r0 < 0) { r0 = 0; }
	r1 = pic->rindexshotbulk + POSTSHOTCOUNT;
	if (r1 >= marksequencelen) { r1 = marksequencelen - 1; }

	icount = 0;
	discardcount = 0;
	beforeshotcount = 0;
	aftershotcount = 0;
	aftershot = 0;

	//--
	for (j = 0; j < marksequencelen; j++) {
		clubx[j] = 0;
		cluby[j] = 0;
		clubCx[j] = 0;
		clubCy[j] = 0;
		clubxyvalid[j] = 0;
	}
	clubxycount = 0;


	myha[2] = pcpc->myha[2];
	myha[1] = pcpc->myha[1];
	myha[0] = pcpc->myha[0];

	//--

// IIII	IR_2 = Ref/2;
// IIII	MAX_2 = memset(MAX_2, 0x80, LEN);
//	if (icount >= 3) 
	{
//		double q_, m_, b_;
//		double mya[3];
		I32 pickme;


//		q_ = 0;
//		m_ = pcpc->preshot_m_;
//		b_ = pcpc->preshot_b_;

//		memcpy(&mya[0], &pcpc->preshot_myxa[0], sizeof(double)*3);
		frame = 0;
		for (index = 0; index < marksequencelen; index++) {
			rindex = pic->firstbulkindex + index;
//			if (index > (I32)pcpc->shotindex) {		
//				q_ = pcpc->postshot_ma[2];
//				m_ = pcpc->postshot_ma[1];
//				b_ = pcpc->postshot_ma[0];
//				memcpy(&mya[0], &pcpc->postshot_myxa[0], sizeof(double)*3);
//			}

			// 1) get image buffer
			if (piana->opmode == IANA_OPMODE_FILE) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, rindex, &pb, &buf);
				if (res == 0) {
					break;
				}
			} else {
				for (j = 0; j < 20; j++) {
					res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, rindex, &pb, &buf);
					if (res == 1) {
						break;
					}
					cr_sleep(1);
				}
				if (res == 0) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", index, j);
					break;
				}
			}

			pcii 		= &pb->cii;
			width 		= pcii->width;
			height 		= pcii->height;
			offset_x 	= pcii->offset_x;
			offset_y 	= pcii->offset_y;

			if (bufimg 	      		== NULL) 	{				// 
				bufimg 		= (U08 *)malloc(width*height);
				MAX_2 		= (U08 *)malloc(width*height);
				MsR_2 		= (U08 *)malloc(width*height);
				I_2 		= (U08 *)malloc(width*height);
				II_2 		= (U08 *)malloc(width*height);
				IR_2 		= (U08 *)malloc(width*height);
				IR 			= (U08 *)malloc(width*height);
				pimgrefgamma 	= (U08 *)malloc(width*height);
				memset(bufimg, 0, (width*height));
				memset(MAX_2, 0, (width*height));
				memset(MsR_2, 0, (width*height));
				memset(I_2, 0, (width*height));
				memset(II_2, 0, (width*height));
				memset(IR_2, 0, (width*height));

				fullroisize.width = width;
				fullroisize.height = height;

				if (s_use_gammacorrection_clubcam) {
				gamma_correction(pic->prefimg, pimgrefgamma, width, height);
				ippiMulC_8u_C1RSfs(
						pimgrefgamma, width,
						1, 
						IR_2	/*IREF_2*/, 		width,
						fullroisize,
						1);								// REF / 2
				} else {
				ippiMulC_8u_C1RSfs(
						pic->prefimg,	width,
						1, 
						IR_2	/*IREF_2*/, 		width,
						fullroisize,
						1);								// REF / 2
				}

				memset(MAX_2, 0x80, width*height);		// MAX_2

				//IIII		MsR_2 = (MAX_2 - REF_2);
				ippiSub_8u_C1RSfs(			// C = A - B
						IR_2 	+ 0, width, 	// B
						MAX_2 	+ 0, width, 	// A
						MsR_2 	+ 0, width,	// C, 
						fullroisize, 0);	
			}


//#define DISPLAY_PRESHOT	3
//#define DISPLAY_POSTSHOT	3
///#define DISPLAY_PRESHOT		2
///#define DISPLAY_POSTSHOT	2
#if defined(DISPLAY_PRESHOT) && defined(DISPLAY_POSTSHOT)
			if (!(frame %clubpathimg_decimation	) 
					|| (
						(frame > (I32)pcpc->shotframe - DISPLAY_PRESHOT)
						&& (frame < (I32)pcpc->shotframe + DISPLAY_POSTSHOT)	
						) 
			   ) {
				pickme = 1;
			} else {
				pickme = 0;
			}
#else
			if (!(frame %clubpathimg_decimation	) ) 
			{
				pickme = 1;
			} else {
				pickme = 0;
			}
#endif

//#define CROSS_SHOW_RANGE	0.5
#define CROSS_SHOW_RANGE	0.7
//#define CROSS_SHOW_RANGE	2.0
//#define CROSS_SHOW_RANGE	1.0

			if (s_clubHposEst2) {
				ClubPath_GetClubHposEst2(piana, camid, pcpc, frame, &Pc, &Lc);
			} else {
				ClubPath_GetClubHposEst(piana, camid, pcpc, frame, &Pc, &Lc);
			}
			x_ = (I32) (Pc.x - pcpc->offset_x);
			y_ = (I32) (Pc.y - pcpc->offset_y);

			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" frame x_ y_ : %4d %4d %4d\n", frame, x_, y_);


			if (x_ > 0 && x_ < (I32)width && y_ > 0 && y_ < (I32)height) {
				if (Lc.y > pic->icp.L.y - CROSS_SHOW_RANGE && Lc.y < pic->icp.L.y + CROSS_SHOW_RANGE)
				{
					double ydist;
					double hlen_;

					ydist = Lc.y - pic->icp.L.y;
					//hlen_ = myha[2]*ydist*ydist + myha[1]*ydist + myha[0];
					//hlen_ = myha[0];
					hlen_ = pcpc->hlenEstimated;

					clubx[clubxycount] = x_;
					cluby[clubxycount] = y_;


					clubCx[clubxycount] = x_;
					clubCy[clubxycount] = (I32)(y_ + hlen_);


					clubxyvalid[clubxycount] = 1;

					drawclub2[clubxycount] = pickme;
					clubxycount++;
				}
			}

			if (pickme) 
			{	
				drawclub[frame] = 1;
				if (s_use_gammacorrection_clubcam) {
				memcpy(bufimg, buf, width * height);
				gamma_correction(bufimg, bufimg, width, height);
				} else {
				memcpy(bufimg, buf, width * height);
				}
				
				//---------------------------------------------------------------------------
				//IIII		I_2 = I/2;
				//IIII      II_2 = I_2 + MsR_2
				//IIII		IR_2 = IR_2 + II_2;
				//IIII		IR_2 = IR_2 - MAX_2
				{
//#define FULLIMAGE1

#define HEADIMGWIDTH1	150
#define HEADIMGHEIGHT1	100
#if defined(FULLIMAGE1)
					startx = 0;
					roiwidth  =  width;
					starty = 0;
					roiheight  =  height;
					imgoffset = 0;
					roisize.width = roiwidth;
					roisize.height = roiheight;
#else
					startx = (I32) (x_ - HEADIMGWIDTH1/2);
					if (startx < 0)  startx = 0;
					if (startx > (I32) (width - HEADIMGWIDTH1-10)) {
						startx = (I32) (width - HEADIMGWIDTH1-10);
					}
					roiwidth  =  HEADIMGWIDTH1;

					starty = (int) (y_ - HEADIMGHEIGHT1/2);
					if (starty < 0)  starty = 0;
					if (starty > (I32) (height - HEADIMGHEIGHT1-10)) {
						starty = (I32) (height - HEADIMGHEIGHT1-10);
					}

					//roiheight  =  HEADIMGHEIGHT1;
					roiheight  =  height - starty;
					imgoffset = startx + starty * width;
					roisize.width = roiwidth;
					roisize.height = roiheight;
#endif
				}


				// IIII, I_2 = I/2
				ippiMulC_8u_C1RSfs(			
						bufimg + imgoffset,	width,
						1, 
						I_2 + imgoffset, 	width,
						roisize,
						1);								// REF / 2


				//IIII		II_2 = I_2 + MsR_2;
				ippiAdd_8u_C1RSfs(			// C = A + B
						MsR_2 	+ imgoffset, width, 	// B
						I_2 	+ imgoffset, width, 	// A
						II_2 	+ imgoffset, width,	// C
						roisize, 0);	

				//IIII		IR_2 = IR_2 + II_2;
				ippiAdd_8u_C1IRSfs (					// A = A + B
						II_2	+ imgoffset, width,		// B
						IR_2 	+ imgoffset, width,		// A
						roisize, 0);

				//IIII		IR_2 = IR_2 - MAX_2
				ippiSub_8u_C1RSfs(			// C = A - B
						MAX_2 	+ imgoffset, width, 	// B
						IR_2 	+ imgoffset, width, 	// A
						IR_2 	+ imgoffset, width,	// C, edge result.
						roisize, 0);	


				if (piana->himgfunc) {
					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)bufimg, width, height, 0, 1, 2, 3, piana->imguserparam);
				}
			} else {
				drawclub[frame] = 0;
			}
			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)IR_2, width, height, 2, 1, 2, 3, piana->imguserparam);
			}
			j++;		// fake.
			frame++;
		}
	}

	// IIII IR = IR_2 * 2;
	ippiMulC_8u_C1RSfs(
			IR_2, width,
			2, 
			IR, 		width,
			fullroisize,
			0);			

	{ 	// Ready Club overlap image and draw Ready Ball
		I32 i;
		U08 *pimg;

		pimg = pic->pimgClubOverlap;
		for (i = 0; i < (I32) (width * height); i++) {
			U08 c;
			c = *(IR + i);
			*(pimg + i*3 + 0) = c;			// R
			*(pimg + i*3 + 1) = c;			// G
			*(pimg + i*3 + 2) = c;			// B
		}

		// Draw Ready Ball 

		{
			int r;
			int x, y;
			r = (int) (pic->icp.RunBallsizePMax / 2.0);
			x = (int) (pic->icp.P.x - offset_x);
			y = (int) (pic->icp.P.y - offset_y);

			mycircleBGRAdd4(x, y, r-1, MKBGR(0x40,0x40,0xFF), pimg, width);
			mycircleBGR(x, y, r-1, MKBGR(0xFF,0xFF,0xFF), pimg, width);
		}

		{
			marksequence_t	*pmks;
			int r, x, y;

			//--
			pimg = pic->pimgClubOverlap;
			for (frame = 0; frame < marksequencelen; frame++) {
				pmks 		= &pic->mks;

				x = (int) pmks->ballposP2[frame].x;
				y = (int) pmks->ballposP2[frame].y;
				r = (int) (pic->icp.RunBallsizePMax / 2.0);

				if (x > pic->icp.P.x - r * 2) {
					continue;
				}

				x = x - offset_x;
				y = y - offset_y;

				if (
						(x < r*3 || x > (int)width -r*3)
						|| 	(y < r*3 || y > (int)height -r*3)
				   ) {
					continue;
				}
				mycircleBGRAdd4(x, y, r-1, MKBGR(0xFF,0x40,0x40), pimg, width);
				mycircleBGR(x, y, r-1, MKBGR(0xFF,0xFF,0xFF), pimg, width);
			}
		}
	}

	{
		I32 x, y;
		point_t Pc, Lc;
		y = height/2;
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Img %d x %d\n", width, height);
		for (x = 0; x < (I32)width; x+=10) {
			Pc.x = x;
			Pc.y = y;
			iana_P2L(piana, camid, &Pc, &Lc, 0);
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%10lf %10lf : %10lf %10lf\n", Pc.x, Pc.y, Lc.x, Lc.y);
		}
	}

	if (
		piana->camsensor_category == CAMSENSOR_CBALL
		|| piana->camsensor_category == CAMSENSOR_CBALL2
		|| piana->camsensor_category == CAMSENSOR_Z3 
        || piana->camsensor_category == CAMSENSOR_EYEXO
		||	piana->camsensor_category == CAMSENSOR_P3V2
		//||	piana->camsensor_category == CAMSENSOR_CONSOLE1
		) {
			if (piana->drawpathmode & 0x01) {
				DrawClubPath(piana, camid, pic->pimgClubOverlap, 0, clubpathimg_decimation);
			}
	}

	{
		IppiSize roisizeM;
		roisizeM.width = width;
		roisizeM.height = height;
		ippiMirror_8u_C3IR(
				pic->pimgClubOverlap, 
				roisizeM.width*3,
				roisizeM, 
				ippAxsBoth);
	}
	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pic->pimgClubOverlap, width, height, 0, 4, 2, 3, piana->imguserparam);
	}

	free(bufimg);
	free(MAX_2);
	free(MsR_2);
	free(I_2);
	free(II_2);
	free(IR_2);
	free(IR);
	free(pimgrefgamma);

	
	return res;
#else
	I32 res;

	iana_cam_t	*pic;
	iana_cam_param_t *picp;
	camimageinfo_t     *pcii;
	camif_buffer_info_t *pb;
	
	U32	width;
	U32	height;
	U32	offset_x;
	U32	offset_y;
    cv::Size bufSize;
    cv::Rect bufROI;
	U08	*buf, *bufimg, *pimgrefgamma;
	U32	rindex;

	I32 r0, r1;

	clubpath_context_t 	*pcpc;

	I32 x_, y_;
	int icount;
	int discardcount;

	I32 index;
	I32 frame;
	I32 j;

	point_t Pc, Lc;
	int clubpathimg_decimation;

	I32 beforeshotcount;
	I32 aftershot;
	I32 aftershotcount;

	I32 clubx[MARKSEQUENCELEN];
	I32 cluby[MARKSEQUENCELEN];

	I32 clubCx[MARKSEQUENCELEN];
	I32 clubCy[MARKSEQUENCELEN];

	I32 clubxyvalid[MARKSEQUENCELEN];
	I32 clubxycount;
	I32 drawclub[MARKSEQUENCELEN];
	I32 drawclub2[MARKSEQUENCELEN];

	double myha[3];

	I32 marksequencelen;

	//--------------------
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}
	
	width = 0;
	height = 0;
	offset_x = 0;
	offset_y = 0;

    bufimg = (U08*)malloc(sizeof(U08) *width * height);
	pimgrefgamma = NULL;

	memset(&drawclub[0], 0, sizeof(I32) * MARKSEQUENCELEN);
	j = 0;
#define CLUBPATHIMG_DECIMATION	3
	clubpathimg_decimation =  CLUBPATHIMG_DECIMATION;
	{
		FILE *fp;
		int mode;
		int decimation;

		int len = PATHBUFLEN;
		char dirpath[PATHBUFLEN];
		char pathstr[PATHBUFLEN];
		char drivestr[PATHBUFLEN];

							 
		OSAL_STR_ConvertUnicode2MultiByte(piana->szDrive, drivestr, len);
		OSAL_STR_ConvertUnicode2MultiByte(piana->szDir,  pathstr, len);

#if defined(_WIN32)	
		sprintf(dirpath, "%s%s\\%s", drivestr, pathstr, CLUBPATHDECIMATIONFILE);		
#else
		sprintf(dirpath, "%s%s/%s", drivestr, pathstr, CLUBPATHDECIMATIONFILE);
#endif
		fp = cr_fopen(dirpath, "r");

		if (fp) {
			fscanf(fp, "%d", &mode);
			fscanf(fp, "%d", &decimation);

			cr_fclose(fp);

			if (mode == 1) {
				clubpathimg_decimation =  decimation;
			}
		}
	}

	res 	= 1;
	pic 	= piana->pic[camid];
	picp	= &pic->icp;

	buf 	= NULL;
	pb 		= NULL;

	pcpc 	= &pic->cpc;
	r0 = pic->rindexshotbulk - PRESHOTCOUNT;
	if (r0 < 0) { 
        r0 = 0; 
    }
	
    r1 = pic->rindexshotbulk + POSTSHOTCOUNT;
	if (r1 >= marksequencelen) { 
        r1 = marksequencelen - 1; 
    }

	icount = 0;
	discardcount = 0;
	beforeshotcount = 0;
	aftershotcount = 0;
	aftershot = 0;

	//--
	for (j = 0; j < marksequencelen; j++) {
		clubx[j] = 0;
		cluby[j] = 0;
		clubCx[j] = 0;
		clubCy[j] = 0;
		clubxyvalid[j] = 0;
	}
	clubxycount = 0;


	myha[2] = pcpc->myha[2];
	myha[1] = pcpc->myha[1];
	myha[0] = pcpc->myha[0];

	//--
	{
		I32 pickme;
        cv::Mat cvImgOverlap;
		frame = 0;
		for (index = 0; index < marksequencelen; index++) {
            cv::Mat cvImgHalf, cvImgDiff, cvImg128;
			rindex = pic->firstbulkindex + index;

			// 1) get image buffer
			if (piana->opmode == IANA_OPMODE_FILE) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, rindex, &pb, &buf);
				if (res == 0) {
					break;
				}
			} else {
				for (j = 0; j < 20; j++) {
					res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, rindex, &pb, &buf);
					if (res == 1) {
						break;
					}
					cr_sleep(1);
				}
				if (res == 0) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", index, j);
					break;
				}
			}

			pcii 		= &pb->cii;
			width 		= pcii->width;
			height 		= pcii->height;
			offset_x 	= pcii->offset_x;
			offset_y 	= pcii->offset_y;
            bufSize = cv::Size(width, height);
            bufROI = cv::Rect(offset_x, offset_y, width, height);

			if (bufimg 	      		== NULL) 	{
                cv::Mat cvRefImg(bufSize, CV_8UC1, pic->prefimg);

				if (s_use_gammacorrection_clubcam) {
                    cv::Mat cvImgGamma;

                    pimgrefgamma = (U08*)malloc(sizeof(U08) * width * height);
				    gamma_correction(pic->prefimg, pimgrefgamma, width, height);
                    cvImgGamma = cv::Mat(bufSize, CV_8UC1, pimgrefgamma);

                    cvImgHalf = cvImgGamma / 2;
				} else {
                    cvImgHalf = cvRefImg / 2;
				}

                cvImg128 = cv::Mat(bufSize, CV_8UC1);
                cvImg128 = 0x80;
                cv::subtract(cvImg128, cvImgHalf, cvImgDiff);
			}


#if defined(DISPLAY_PRESHOT) && defined(DISPLAY_POSTSHOT)
			if (!(frame %clubpathimg_decimation	) || 
                (   (frame > (I32)pcpc->shotframe - DISPLAY_PRESHOT) && 
                (frame < (I32)pcpc->shotframe + DISPLAY_POSTSHOT)   )
			   ) {
				pickme = 1;
			} else {
				pickme = 0;
			}
#else
			if (!(frame %clubpathimg_decimation	) ) {
				pickme = 1;
			} else {
				pickme = 0;
			}
#endif

#define CROSS_SHOW_RANGE	0.7
			if (s_clubHposEst2) {
				ClubPath_GetClubHposEst2(piana, camid, pcpc, frame, &Pc, &Lc);
			} else {
				ClubPath_GetClubHposEst(piana, camid, pcpc, frame, &Pc, &Lc);
			}
			x_ = (I32) (Pc.x - pcpc->offset_x);
			y_ = (I32) (Pc.y - pcpc->offset_y);

			if (x_ > 0 && x_ < (I32)width && y_ > 0 && y_ < (I32)height) {
				if (Lc.y > pic->icp.L.y - CROSS_SHOW_RANGE && Lc.y < pic->icp.L.y + CROSS_SHOW_RANGE) {
					double ydist;
double hlen_;

ydist = Lc.y - pic->icp.L.y;
hlen_ = pcpc->hlenEstimated;

clubx[clubxycount] = x_;
cluby[clubxycount] = y_;

clubCx[clubxycount] = x_;
clubCy[clubxycount] = (I32)(y_ + hlen_);

clubxyvalid[clubxycount] = 1;

drawclub2[clubxycount] = pickme;
clubxycount++;
                }
            }

            if (pickme) {
                int startx, starty;
                int roiwidth, roiheight;
                int imgoffset;
                cv::Size roisize;
                cv::Rect roiRect;

                cv::Mat cvImgInit, cvImgMult, cvImgAdd, cvImgAdd2;

                drawclub[frame] = 1;
                if (s_use_gammacorrection_clubcam) {
                    // memcpy(bufimg, buf, width * height);
                    gamma_correction(buf, bufimg, width, height);
                } else {
                    memcpy(bufimg, buf, width * height);
                }

                //---------------------------------------------------------------------------
                //IIII		I_2 = I/2;
                //IIII      II_2 = I_2 + MsR_2
                //IIII		IR_2 = IR_2 + II_2;
                //IIII		IR_2 = IR_2 - MAX_2
                {
#define HEADIMGWIDTH1	150
#define HEADIMGHEIGHT1	100
                    startx = (I32)(x_ - HEADIMGWIDTH1/2);
                    if (startx < 0) {
                        startx = 0;
                    }
                    if (startx >(I32) (width - HEADIMGWIDTH1-10)) {
                        startx = (I32)(width - HEADIMGWIDTH1-10);
                    }
                    roiwidth  =  HEADIMGWIDTH1;

                    starty = (int)(y_ - HEADIMGHEIGHT1/2);
                    if (starty < 0) {
                        starty = 0;
                    }
                    if (starty >(I32) (height - HEADIMGHEIGHT1-10)) {
                        starty = (I32)(height - HEADIMGHEIGHT1-10);
}
                    //roiheight  =  HEADIMGHEIGHT1;
                    roiheight  =  height - starty;

                    roiRect = cv::Rect(startx, starty, roiwidth, roiheight);
                    roisize = roiRect.size();

                    imgoffset = startx + starty * width;
                }

                // cvImgHalf = IR_2
                // cvImg128 = MAX_2
                // cvImgDiff = MsR_2
                // cvImgMult = I_2
                // cvImgAdd = II_2
                /////////////////////////
                // IIII, I_2 = I/2
                cvImgInit = cv::Mat(bufSize, CV_8UC1, bufimg);
                cvImgMult = cvImgInit(roiRect) / 2;
                cv::add(cvImgDiff(roiRect), cvImgMult, cvImgAdd);
                cv::add(cvImgAdd, cvImgHalf, cvImgAdd2);
                cvImgAdd2 = cvImgAdd2 - cvImg128;

                cvImgAdd2.copyTo(cvImgInit(roiRect));
                memcpy(bufimg, cvImgInit.data, sizeof(U08) * cvImgInit.cols * cvImgInit.rows);
                if (piana->himgfunc) {
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)bufimg, width, height, 0, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(cvImgAdd2.data), cvImgAdd2.cols, cvImgAdd2.rows, 2, 1, 2, 3, piana->imguserparam);
                    width++; width--;
                }
            } else {
                drawclub[frame] = 0;
            }
            if (piana->himgfunc) {
                // ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)IR_2, width, height, 2, 1, 2, 3, piana->imguserparam);


            }
            j++;		// fake.
            frame++;
        }

        {
            cv::Mat cvImgOverlapColor;
            cv::cvtColor(cvImgOverlap, cvImgOverlapColor, cv::COLOR_GRAY2RGB);
            memcpy(pic->pimgClubOverlap, cvImgOverlapColor.data, sizeof(U08) * width * height * 3);
        }
	}

	{ 	// Ready Club overlap image and draw Ready Ball
		//I32 i;
		U08 *pimg;

		pimg = pic->pimgClubOverlap;

		// Draw Ready Ball 
		{
			int r;
			int x, y;
			r = (int) (pic->icp.RunBallsizePMax / 2.0);
			x = (int) (pic->icp.P.x - offset_x);
			y = (int) (pic->icp.P.y - offset_y);

			mycircleBGRAdd4(x, y, r-1, MKBGR(0x40,0x40,0xFF), pimg, width);
			mycircleBGR(x, y, r-1, MKBGR(0xFF,0xFF,0xFF), pimg, width);
		}

		{
			marksequence_t	*pmks;
			int r, x, y;

			//--
			pimg = pic->pimgClubOverlap;
			for (frame = 0; frame < marksequencelen; frame++) {
				pmks 		= &pic->mks;

				x = (int) pmks->ballposP2[frame].x;
				y = (int) pmks->ballposP2[frame].y;
				r = (int) (pic->icp.RunBallsizePMax / 2.0);

				if (x > pic->icp.P.x - r * 2) {
					continue;
				}

				x = x - offset_x;
				y = y - offset_y;

				if (
						(x < r*3 || x > (int)width -r*3)
						|| 	(y < r*3 || y > (int)height -r*3)
				   ) {
					continue;
				}
				mycircleBGRAdd4(x, y, r-1, MKBGR(0xFF,0x40,0x40), pimg, width);
				mycircleBGR(x, y, r-1, MKBGR(0xFF,0xFF,0xFF), pimg, width);
			}
		}
	}

	{
		I32 x, y;
		point_t Pc, Lc;
		y = height/2;
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Img %d x %d\n", width, height);
		for (x = 0; x < (I32)width; x+=10) {
			Pc.x = x;
			Pc.y = y;
			iana_P2L(piana, camid, &Pc, &Lc, 0);
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%10lf %10lf : %10lf %10lf\n", Pc.x, Pc.y, Lc.x, Lc.y);
		}
	}

	if (
		piana->camsensor_category == CAMSENSOR_CBALL
		|| piana->camsensor_category == CAMSENSOR_CBALL2
		|| piana->camsensor_category == CAMSENSOR_Z3 
        || piana->camsensor_category == CAMSENSOR_EYEXO
		|| piana->camsensor_category == CAMSENSOR_P3V2
		) {
			if (piana->drawpathmode & 0x01) {
				DrawClubPath(piana, camid, pic->pimgClubOverlap, 0, clubpathimg_decimation);
			}
}

	{
		cv::Size roisizeM;
        cv::Mat cvFlipped;
		roisizeM.width = width;
		roisizeM.height = height;

        cv::flip(cv::Mat(roisizeM, CV_8UC3, pic->pimgClubOverlap), 
            cvFlipped,
            -1);
		memcpy(pic->pimgClubOverlap, cvFlipped.data, sizeof(U08) * width * height * 3);
	}

	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pic->pimgClubOverlap, width, height, 0, 4, 2, 3, piana->imguserparam);
	}
	
    free(bufimg);
	return res;
#endif
}

static I32 DrawClubPath(	
		iana_t *piana, 
		U32 camid, 
		U08 *pimg, 
		U32 isGray, 		// isGray 1: Gray,   0: BGR
		U32 decimation
)	
{
	I32 res;

	//U32 index;
	U32 frame;
	iana_cam_t	*pic;

	U32	width;
	U32	height;

	clubpath_context_t 	*pcpc;

	I32 clubx[MARKSEQUENCELEN];
	I32 cluby[MARKSEQUENCELEN];

	I32 clubCx[MARKSEQUENCELEN];
	I32 clubCy[MARKSEQUENCELEN];

	I32 clubxyvalid[MARKSEQUENCELEN];
	I32 clubxycount;
	I32 drawclub[MARKSEQUENCELEN];
	I32 drawclub2[MARKSEQUENCELEN];

	point_t Pc, Lc;
	iana_shotresult_t *psr;

	I32 marksequencelen;

	//--------------------
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}
	
	
	psr = &piana->shotresultdata;
#if defined(_DEBUG)
#define CLUBASSURANCE_DRAWPATH_MIN	0.2
#else
#define CLUBASSURANCE_DRAWPATH_MIN	0.8
#endif
	if (psr->clubAssurance < CLUBASSURANCE_DRAWPATH_MIN) {
		res = 1;
		goto func_exit;
	}

	memset(&drawclub[0], 0, sizeof(I32) * MARKSEQUENCELEN);

	pic 	= piana->pic[camid];
	pcpc 	= &pic->cpc;

	//--
	for (frame = 0; frame < MARKSEQUENCELEN; frame++) {
		clubx[frame] = 0;
		cluby[frame] = 0;
		clubCx[frame] = 0;
		clubCy[frame] = 0;
		clubxyvalid[frame] = 0;
	}
	clubxycount = 0;

	width 		= pcpc->width;
	height 		= pcpc->height;

	if (decimation < 1) {
		decimation = 1;
	}
	for (frame = 0; frame < (U32)marksequencelen; frame++) {
		I32 x_, y_;
		U32 pickme;

		//#define CROSS_SHOW_RANGE	0.5
#define CROSS_SHOW_RANGE	0.7
		//#define CROSS_SHOW_RANGE	2.0
		//#define CROSS_SHOW_RANGE	1.0

		if (s_clubHposEst2) {
			ClubPath_GetClubHposEst2(piana, camid, pcpc, frame, &Pc, &Lc);
		} else {
			ClubPath_GetClubHposEst(piana, camid, pcpc, frame, &Pc, &Lc);
		}
		x_ = (I32) (Pc.x - pcpc->offset_x);
		y_ = (I32) (Pc.y - pcpc->offset_y);

		if (!(frame %decimation)) 
		{
			pickme = 1;
		} else {
			pickme = 0;
		}

		if (x_ > 0 && x_ < (I32)width && y_ > 0 && y_ < (I32)height) {
			if (Lc.y > pic->icp.L.y - CROSS_SHOW_RANGE && Lc.y < pic->icp.L.y + CROSS_SHOW_RANGE)
			{
				double ydist;
				double hlen_;

				ydist = Lc.y - pic->icp.L.y;
				hlen_ = pcpc->hlenEstimated;
				clubx[clubxycount] = x_;
				cluby[clubxycount] = y_;

				clubCx[clubxycount] = x_;
				clubCy[clubxycount] = (I32)(y_ + hlen_);

				clubxyvalid[clubxycount] = 1;

				drawclub2[clubxycount] = pickme;
				clubxycount++;
			}
		}
#define BEFORE_SHOT_AFTER_FRAME	3
//#define BEFORE_SHOT_AFTER_FRAME	5
#if defined(BEFORE_SHOT_AFTER_FRAME)
		if (frame > pcpc->shotframecombined + BEFORE_SHOT_AFTER_FRAME) {
			break;
		}
#endif
	}

	// Draw Club path
	{
		I32 i;
		I32 x1, y1;
		I32 x2, y2;
		I32 x3, y3;
		I32 clubxPrev, clubyPrev;
		I32 clubCxPrev, clubCyPrev;
		//		I32 radius;
		I32 halfw;
		I32 sx, sy, ex, ey;

		U32 bgrvalue;


		clubxPrev = 0; clubyPrev = 0;
		clubCxPrev = 0; clubCyPrev = 0;
		
		for (i = 0; i < clubxycount; i++) {
			point_t Pc, Lc;
			double ydist;
			U32 drawme;
			x1 = clubx[i];
			y1 = cluby[i];

			Pc.x = x1 + pcpc->offset_x;
			Pc.y = y1 + pcpc->offset_y;

			iana_P2L(piana, camid, &Pc, &Lc, 0);
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d] %10lf %10lf , %10lf %10lf\n", i, Pc.x, Pc.y, Lc.x, Lc.y);

#define CLUBYLAST	0.8
#define CLUBYDISTMAX	0.6
//#define CLUBYDISTFROM	0.1
//#define CLUBYDISTFROM	0.2
#define CLUBYDISTFROM	0.3
#define CLUBYDISTTO		0.5
			drawme = 1;
			ydist = Lc.y - pic->icp.L.y;
			if (ydist < -CLUBYDISTFROM || ydist > CLUBYDISTTO) 
			{
				drawme = 0;
			}
			if (drawclub2[i]) {
				halfw = 4;
			} else {
				halfw = 2;
			}
			x1 = clubx[i];
			y1 = cluby[i];

//#define LINEVWIDTH	10
#define LINEVWIDTH0	2
//#define LINEVWIDTH1	4
#define LINEVWIDTH1	2
			if (x1 < 0) {x1 = 0;}  if (x1 >= (I32)width-LINEVWIDTH0) {x1 = width-1-LINEVWIDTH0;}
			if (y1 < 0) {y1 = 0;}  if (y1 >= (I32)height-LINEVWIDTH0) {y1 = height-1-LINEVWIDTH0;}

			if (drawme) {
				if (i != 0) {
					if (isGray) {
//#define LINEVWIDTH	10
#define LINEVWIDTH0	2
//#define LINEVWIDTH1	4
#define LINEVWIDTH1	2
						mylineGrayV(x1, y1, clubxPrev, clubyPrev, 0x80, pimg, width, LINEVWIDTH0);
					} else {
						bgrvalue = MKBGR(0,150,255);
						mylineBGRV(x1, y1, clubxPrev, clubyPrev, bgrvalue, pimg, width, LINEVWIDTH0);
					}
				}
			}

			x2 = clubCx[i];
			y2 = clubCy[i];

			if (x2 < 0) {x2 = 0;}  if (x2 >= (I32)width-LINEVWIDTH0) {x2 = width-1-LINEVWIDTH0;}
			if (y2 < 0) {y2 = 0;}  if (y2 >= (I32)height-LINEVWIDTH0) {y2 = height-1-LINEVWIDTH0;}

			if (drawme) {
				if (i != 0) {
					if (isGray) {
						//mylineGray(x2, y2, clubCxPrev, clubCyPrev, 0x80, pimg, width);
						mylineGrayV(x2, y2, clubCxPrev, clubCyPrev, 0x80, pimg, width, LINEVWIDTH0);
					} else {
						mylineBGRV(x2, y2, clubCxPrev, clubCyPrev, MKBGR(0xFF,0x00,0x00), pimg, width, LINEVWIDTH0);
						bgrvalue = MKBGR(0,150,255);
						mylineBGRV(x2, y2, clubCxPrev, clubCyPrev, bgrvalue, pimg, width, LINEVWIDTH0);
					}
				}
			}
			x3 = (x1 + x2)/2;
			y3 = (y1 + y2)/2;

			sx = x3-halfw; sy = y3-halfw;
			ex = x3+halfw; ey = y3+halfw;

			if (sx < 0) {sx = 0;} if (sx >= (I32)width-LINEVWIDTH1) sx = width-1-LINEVWIDTH1;
			if (sy < 0) {sy = 0;} if (sy >= (I32)height-LINEVWIDTH1) sy = height-1-LINEVWIDTH1;
			if (ex < 0) {ex = 0;} if (ex >= (I32)width-LINEVWIDTH1) ex = width-1-LINEVWIDTH1;
			if (ey < 0) {ey = 0;} if (ey >= (I32)height-LINEVWIDTH1) ey = height-1-LINEVWIDTH1;

#if defined(_DEBUG)
					// CLUB HEADER CENTER
			if (drawme) {
				if (isGray) {
					myrectGray(sx, sy, ex, ey, 0xFF, pimg, width);
					if (i != 0) {
						mylineGrayV(x3, y3, (clubxPrev+clubCxPrev)/2, (clubyPrev+clubCyPrev)/2, 0xFF, pimg, width, LINEVWIDTH1);
					}
				} else {
					U32 bgrvalue;

					if (i != 0) {
						mylineBGRV(x3, y3, (clubxPrev+clubCxPrev)/2, (clubyPrev+clubCyPrev)/2, MKBGR(0xFF,0x20,0xFF), pimg, width, LINEVWIDTH1);
					}

					bgrvalue = MKBGR(0xFF,0xFF,0x00);
					if (Lc.y > CLUBYLAST) {
						bgrvalue = MKBGR(0x00,0x00,0xFF);
					}
					ydist = Lc.y - pic->icp.L.y;
					if (ydist < -CLUBYDISTMAX || ydist > CLUBYDISTMAX) {
						bgrvalue = MKBGR(0x00,0x00,0xFF);
					}
					
					myrectBGR(sx, sy, ex, ey, bgrvalue, pimg, width);

				}
			}
#endif
			clubxPrev = x1;
			clubyPrev = y1;

			if (clubxPrev < 0) {clubxPrev = 0;} if (clubxPrev >= (I32)width) {clubxPrev = width-1;}
			if (clubyPrev < 0) {clubyPrev = 0;} if (clubyPrev >= (I32)height) {clubyPrev = height-1;}

			clubCxPrev = x2;
			clubCyPrev = y2;
			if (clubCxPrev < 0) {clubCxPrev = 0;} if (clubCxPrev >= (I32)width) {clubCxPrev = width-1;}
			if (clubCyPrev < 0) {clubCyPrev = 0;} if (clubCyPrev >= (I32)height) {clubCyPrev = height-1;}

		}
	}


	res = 1;
func_exit:
	return res;
}



//-------------------------------------
I32 iana_spin_ballclub_check(iana_t *piana) {	
	I32 res;
	U32 camid;

	for (camid = 0; camid < NUM_CAM; camid++) {
		if (piana->processmode[camid] == CAMPROCESSMODE_BALLCLUB) {
			SaveBallClubData(piana, camid);
		}
	}

	res = 1;

	return res;
}


static I32 SaveBallClubData(iana_t *piana, U32 camid)	
{
// #if 1
#ifdef IPP_SUPPORT
	I32 res;
	U32 i, j;
	camif_buffer_info_t *pb;
	iana_cam_t	*pic;
	camimageinfo_t     *pcii;
	clubpath_context_t 	*pcpc;

	U32	width;
	U32	height;
	U32	offset_x;
	U32	offset_y;

	U08 *buf;
	U08 *pimg3, *pimgG, *pimg3R, *pimg3RR;
	int len = PATHBUFLEN;
	char imgpath[PATHBUFLEN];
	char fn[1024];
	U32 bulkindexFrom, bulkindexTo;

	iana_setup_t *pisetup;
//	iana_cam_setup_t *picsetup;
	U32 imagedata;
	U64 ts64;


	//--
	imagedata = 1;
	nxmlie(1,  "<camera id=\"%d\">", camid);

	pic 	= piana->pic[camid];
	pcpc 	= &pic->cpc;
	pimg3 = NULL;
	pimg3R = NULL;
	pimg3RR = NULL;

	pisetup = &piana->isetup;

	bulkindexFrom = pcpc->bulkindexFrom;
	bulkindexTo = pcpc->bulkindexTo;

	OSAL_STR_ConvertUnicode2MultiByte(piana->szImageDir, imgpath, len);
	//for (i = 0; i < MARKSEQUENCELENATTCAM; i++) 
	buf = NULL;
	pb = NULL;
	pimgG = NULL;
	for (i = bulkindexFrom; i <= bulkindexTo; i++) 
	{
		if (piana->opmode == IANA_OPMODE_FILE) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, i, &pb, &buf);
			if (res == 0) {
				break;
			}
		} else {
			res = 0;
			for (j = 0; j < 20; j++) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, i, &pb, &buf);
				if (res == 1) {
					break;
				}
				cr_sleep(1);
			}

			if (j != 0) {
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read .... nogood. (%d, %d, %d)\n", camid, i, j);
			}
			if (res == 0) {
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail. No more. [%d, %d:%d]Break\n",camid, i, j);
				break;
			}
		}

		pcii 		= &pb->cii;
		width 		= pcii->width;
		height 		= pcii->height;
		offset_x 	= pcii->offset_x;
		offset_y 	= pcii->offset_y;
		ts64 = MAKEU64(pb->ts_h, pb->ts_l);

		if (pimg3 == NULL) {
			pimg3 = (U08 *) malloc(width*height*3 + 128);
			pimgG = (U08 *) malloc(width*height + 128);
	//		pimg3R= (U08 *) malloc(FULLWIDTH*FULLHEIGHT*3 + 128);
		}

		if (piana->camsensor_category == CAMSENSOR_P3V2) {
			memcpy(pimgG, buf, width*height);
		}
		else {
			gamma_correction(buf, pimgG, width, height);
		}
		/*
		if (piana->himgfunc != NULL) {
			((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf, width, height, 0, 1, 2, 3, piana->imguserparam);
			((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimgG, width, height, 2, 1, 2, 3, piana->imguserparam);
		}
		*/

		for (j = 0; j < (width * height); j++) {
			U08 c;
			c = *(pimgG + j);
			*(pimg3 + j*3 + 0) = c;			// R
			*(pimg3 + j*3 + 1) = c;			// G
			*(pimg3 + j*3 + 2) = c;			// B
		}
		if (piana->himgfunc != NULL) {
			((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimg3, width, height, 0, 4, 2, 3, piana->imguserparam);
		}
		if (
			piana->camsensor_category == CAMSENSOR_CBALL
			|| piana->camsensor_category == CAMSENSOR_CBALL2
			|| piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO
			||	piana->camsensor_category == CAMSENSOR_P3V2
//					||	piana->camsensor_category == CAMSENSOR_CONSOLE1
			
			) {
			double vmag;
			iana_shotresult_t *psr;
			//--------------------
			psr = &piana->shotresultdata;
			vmag = psr->vmag;
#if defined(_DEBUG)
#define DRAW_CLUBPATH_MINVMAG	0
#else
#define DRAW_CLUBPATH_MINVMAG	10
#endif
			if (vmag > DRAW_CLUBPATH_MINVMAG) {
				if (piana->drawpathmode & 0x01) {
					DrawClubPath(
						piana, 
						camid, 
						pimg3, 
						0, 		// isGray 1: Gray,   0: BGR
						1);
				}
			}
		}

		{
			//resize to 720, 310;
			Ipp8u 	*resizebuf;
			IppiSize ssize;
			IppiRect sroi, droi;
			IppStatus ippstatus;

			int 	bufsize;
			double xfact, yfact;
			int sstep, dstep;

			//--
			sroi.x 		= 0;
			sroi.y 		= 0;
			sroi.width 	= pcpc->ex - pcpc->sx + 1;
			sroi.height	= height;
			ssize.width = sroi.width;
			ssize.height = height;

			droi.x 		= 0;
			droi.y 		= 0;
			//droi.width 	= CAM0_RUN_WIDTH;
			//droi.height	= CAM0_RUN_HEIGHT;
			droi.width 	= pisetup->icsetup[0].run_width;
			droi.height	= pisetup->icsetup[0].run_height;

			xfact = (double) droi.width / (double)sroi.width;
			yfact = (double) droi.height / (double)sroi.height;


			sstep = width * 3;
			dstep = droi.width * 3;

			ippiResizeGetBufSize(sroi, droi, 3, /*IPPI_INTER_LINEAR*/ IPPI_INTER_CUBIC, &bufsize);
			resizebuf = (U08 *)malloc(bufsize);
			if (pimg3R == NULL) {
				pimg3R= (U08 *) malloc(droi.width*droi.height*3 + 128);
				pimg3RR= (U08 *) malloc(droi.width*droi.height*3 + 128);
			}
			ippstatus = ippiResizeSqrPixel_8u_C3R(
					pimg3 + (pcpc->sx * 3),
					ssize,
					sstep, // width*3,
					sroi,
					//
					pimg3R,
					dstep,  // destwidth*3,
					droi,
					//
					xfact,
					yfact,
					0,			// shiftx
					0,			// shifty
					IPPI_INTER_CUBIC /*IPPI_INTER_LINEAR*/ /*IPPI_INTER_CUBIC*/,
					(Ipp8u*) resizebuf
					);
			if (piana->himgfunc != NULL) {
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimg3R, width, height, 2, 4, 2, 3, piana->imguserparam);
			}
			free(resizebuf);

			{
				IppiSize roisizeM;
				roisizeM.width = droi.width;
				roisizeM.height = droi.height;
				ippstatus = ippiMirror_8u_C3IR(
						pimg3R, 
						roisizeM.width*3,
						roisizeM, 
						ippAxsBoth);
			}
		}
		if (imagedata == 1) {

			nxmlie(1,  "<imagedata>");
			{
				U64 ts64shot;
				I32 cx, cy;
				double balldirection;
				double clubdirection;
				iana_cam_param_t *picp;
				iana_shotresult_t *pcsr;;

				picp	= &pic->icp;
				pcsr = &piana->clubball_shotresult[camid];

				ts64shot  = pic->ts64shot;
				balldirection = atan(picp->mxy_);
				balldirection = RADIAN2DEGREE(balldirection);
				clubdirection = pcsr->clubpath;

				cx = (I32)(pic->icp.P.x + 0.5);
				cy = (I32)(pic->icp.P.y + 0.5);

				cx = cx - offset_x;
				cy = cy - offset_y;

				cx = width - cx;
				cy = height - cy;
				nxmlie(0,  "<processmode>%d</processmode>", piana->processmode[camid]);
				nxmlie(0,  "<width>%d</width>", width);
				nxmlie(0,  "<height>%d</height>", height);
				nxmlie(0,  "<shotts64>%llu</shotts64>", ts64shot);
				nxmlie(0,  "<ballposition>%d %d</ballposition>", cx, cy);
				nxmlie(0,  "<balldirection>%10.6lf</balldirection>", balldirection);
				nxmlie(0,  "<clubdirection>%10.6lf</clubdirection>", clubdirection);
			}
			nxmlie(-1,  "</imagedata>");
			imagedata = 0;
			nxmlie(1,  "<images>");
		}

		if (camid == 0) {
			sprintf(fn, "topview%02d.jpg", (i - bulkindexFrom));
		} else {
			sprintf(fn, "sideview%02d.jpg", (i - bulkindexFrom));
		}

		nxmlie(1,  "<image>");
		{
			ts64 = MAKEU64(pb->ts_h, pb->ts_l);
			nxmlie(0,  "<ts64>%llu</ts64>", ts64);
			nxmlie(0,  "<filename>%s</filename>", fn);
		}
		nxmlie(-1,  "</image>");

		saveimageJpegBGR(piana->hjpeg, 
				80,
				imgpath,
				fn,
				(char *)pimg3R,
				pisetup->icsetup[0].run_width,			// CAM0_RUN_WIDTH, 
				pisetup->icsetup[0].run_height			// CAM0_RUN_HEIGHT
				);

#define SLEEP_CLUBBALL	1
#if defined(SLEEP_CLUBBALL)
		cr_sleep(SLEEP_CLUBBALL);
#endif
	}
	free(pimg3);
	free(pimg3R);
	free(pimg3RR);
	free(pimgG);
	res = 1;

	nxmlie(-1,  "</images>");
	nxmlie(-1, "</camera>");
	return res;

#else

    I32 res;
	U32 i, j;
	camif_buffer_info_t *pb;
	iana_cam_t	*pic;
	camimageinfo_t     *pcii;
	clubpath_context_t 	*pcpc;

	U32	width, height;
	U32	offset_x, offset_y;

	U08 *buf;
	
    int len = PATHBUFLEN;
	char imgpath[PATHBUFLEN];
	char fn[1024];
	U32 bulkindexFrom, bulkindexTo;

	iana_setup_t *pisetup;
	U32 imagedata;
	U64 ts64;

	//-- 
	imagedata = 1;
	nxmlie(1,  "<camera id=\"%d\">", camid);

	pic 	= piana->pic[camid];
	pcpc 	= &pic->cpc;

	pisetup = &piana->isetup;

	bulkindexFrom = pcpc->bulkindexFrom;
	bulkindexTo = pcpc->bulkindexTo;

	OSAL_STR_ConvertUnicode2MultiByte(piana->szImageDir, imgpath, len);

	buf = NULL;
	pb = NULL;
	
    for (i = bulkindexFrom; i <= bulkindexTo; i++) {
        cv::Size bufSize, roiSize;
        cv::Mat imgBuf, imgBufColor, imgGamma, imgResizedM;
            // ,3R, 3RR?
            ;
        if (piana->opmode == IANA_OPMODE_FILE) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, i, &pb, &buf);
			if (res == 0) {
				break;
			}
		} else {
			res = 0;
			for (j = 0; j < 20; j++) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, i, &pb, &buf);
				if (res == 1) {
					break;
				}
				cr_sleep(1);
			}

			if (j != 0) {
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read .... nogood. (%d, %d, %d)\n", camid, i, j);
			}
			if (res == 0) {
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail. No more. [%d, %d:%d]Break\n",camid, i, j);
				break;
			}
		}

		pcii 		= &pb->cii;
		width 		= pcii->width;
		height 		= pcii->height;
		offset_x 	= pcii->offset_x;
		offset_y 	= pcii->offset_y;
		ts64 = MAKEU64(pb->ts_h, pb->ts_l);

        bufSize = cv::Size(width, height);
        imgBuf = cv::Mat(bufSize, CV_8UC1, buf);
        cv::cvtColor(imgBuf, imgBufColor, cv::COLOR_GRAY2RGB);

        imgGamma = cv::Mat(bufSize, CV_8UC1);

		// TODO: latest code merge - opencv 
		gamma_correction(buf, imgGamma.data, width, height);

		if (piana->himgfunc != NULL) {
			((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf, width, height, 0, 1, 2, 3, piana->imguserparam);
			((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)(imgGamma.data), width, height, 2, 1, 2, 3, piana->imguserparam);
		}

		if (piana->himgfunc != NULL) {
			((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)(imgBufColor.data), width, height, 0, 4, 2, 3, piana->imguserparam);
		}

        if (piana->camsensor_category == CAMSENSOR_CBALL
			|| piana->camsensor_category == CAMSENSOR_CBALL2
			|| piana->camsensor_category == CAMSENSOR_Z3
            || piana->camsensor_category == CAMSENSOR_EYEXO
			|| piana->camsensor_category == CAMSENSOR_P3V2) 
        {
			double vmag;
			iana_shotresult_t *psr;
			//--------------------
			psr = &piana->shotresultdata;
			vmag = psr->vmag;
#if defined(_DEBUG)
#define DRAW_CLUBPATH_MINVMAG	0
#else
#define DRAW_CLUBPATH_MINVMAG	10
#endif
			if (vmag > DRAW_CLUBPATH_MINVMAG) {
				if (piana->drawpathmode & 0x01) {
					DrawClubPath(
						piana, 
						camid, 
                        imgBufColor.data,
						0, 		// isGray 1: Gray,   0: BGR
						1);
				}
			}
		}

		{
			//resize to 720, 310;
			cv::Rect sROIResize;
            cv::Size sSizeResize, dSizeResize;
            cv::Mat imgBufResized;

            double xfact, yfact;
        
            sROIResize = cv::Rect(
                pcpc->sx, 0, 
                pcpc->ex - pcpc->sx, height);
            sSizeResize = sROIResize.size();
            dSizeResize = cv::Size(pisetup->icsetup[0].run_width, pisetup->icsetup[0].run_height);

            xfact = (double)sSizeResize.width / (double)dSizeResize.width;
            yfact = (double)sSizeResize.height / (double)dSizeResize.height;

            cv::resize(imgBufColor(sROIResize), imgBufResized, dSizeResize, xfact, yfact, cv::INTER_CUBIC);
            
			if (piana->himgfunc != NULL) {
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)(imgBufResized.data), imgBufResized.cols, imgBufResized.rows, 2, 4, 2, 3, piana->imguserparam);
			}

            cv::flip(imgBufResized, imgResizedM, -1);
		}

		if (imagedata == 1) {

			nxmlie(1,  "<imagedata>");
			{
				U64 ts64shot;
				I32 cx, cy;
				double balldirection;
				double clubdirection;
				iana_cam_param_t *picp;
				iana_shotresult_t *pcsr;;

				picp	= &pic->icp;
				pcsr = &piana->clubball_shotresult[camid];

				ts64shot  = pic->ts64shot;
				balldirection = atan(picp->mxy_);
				balldirection = RADIAN2DEGREE(balldirection);
				clubdirection = pcsr->clubpath;

				cx = (I32)(pic->icp.P.x + 0.5);
				cy = (I32)(pic->icp.P.y + 0.5);

				cx = cx - offset_x;
				cy = cy - offset_y;

				cx = width - cx;
				cy = height - cy;
				nxmlie(0,  "<processmode>%d</processmode>", piana->processmode[camid]);
				nxmlie(0,  "<width>%d</width>", width);
				nxmlie(0,  "<height>%d</height>", height);
				nxmlie(0,  "<shotts64>%llu</shotts64>", ts64shot);
				nxmlie(0,  "<ballposition>%d %d</ballposition>", cx, cy);
				nxmlie(0,  "<balldirection>%10.6lf</balldirection>", balldirection);
				nxmlie(0,  "<clubdirection>%10.6lf</clubdirection>", clubdirection);
			}
			nxmlie(-1,  "</imagedata>");
			imagedata = 0;
			nxmlie(1,  "<images>");
		}

		if (camid == 0) {
			sprintf(fn, "topview%02d.jpg", (i - bulkindexFrom));
		} else {
			sprintf(fn, "sideview%02d.jpg", (i - bulkindexFrom));
		}

		nxmlie(1,  "<image>");
		{
			ts64 = MAKEU64(pb->ts_h, pb->ts_l);
			nxmlie(0,  "<ts64>%llu</ts64>", ts64);
			nxmlie(0,  "<filename>%s</filename>", fn);
		}
		nxmlie(-1,  "</image>");

		saveimageJpegBGR(piana->hjpeg, 
				80,
				imgpath,
				fn,
				(char *)(imgResizedM.data),
				pisetup->icsetup[0].run_width,			// CAM0_RUN_WIDTH, 
				pisetup->icsetup[0].run_height			// CAM0_RUN_HEIGHT
				);

		cr_sleep(1);
	}
	res = 1;

	nxmlie(-1,  "</images>");
	nxmlie(-1, "</camera>");
	return res;
#endif
}



/*!
 ********************************************************************************
 *	@brief      Club
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: changed, 0: NOT changed.
 *
 *	@author	    yhsuk
 *  @date       2017/0518
 *******************************************************************************/
static I32 ClubPath_CalcSpin(iana_t *piana, U32 camid, clubpath_context_t *pcpc)	
{
	I32 res;

	res = ClubPath_CalcSpin_SideSpin(piana, camid, pcpc);
	if (res) {
		res = ClubPath_CalcSpin_BackSpin(piana, camid, pcpc);
	}

	return res;
}

/*!
 ********************************************************************************
 *	@brief      Club
 *	            SIDESPIN, using EUCAM logic
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: changed, 0: NOT changed.
 *
 *	@author	    yhsuk
 *  @date       2017/0518
 *******************************************************************************/
static I32 ClubPath_CalcSpin_SideSpin(iana_t *piana, U32 camid, clubpath_context_t *pcpc)	
{
	I32 res;
	double vmag;
	double incline;
	double azimuth;

	double clubspeedB;

	double clubpath;
	double deltaDegree;
	double deltaDegreeMax;

	double sidespin;

	U32 isteeshot;

	iana_shotresult_t *psr;
	iana_cam_t	*pic;

	//-----------
//#define RAND_FIX_TEST_1		1234
#if defined(RAND_FIX_TEST_1)
	srand(RAND_FIX_TEST_1);
#endif	
	
	pic = piana->pic[camid];
	psr = &piana->shotresultdata;
	{
		vmag			= psr->vmag;
		incline 		= psr->incline;
		azimuth 		= psr->azimuth;

		clubspeedB 		= pcpc->clubspeedB;
		clubspeedB 		= pcpc->clubspeedB;
		clubpath 		= pcpc->clubpath;

		deltaDegree = azimuth - clubpath;

		isteeshot = 0;
		if (piana->processarea == IANA_AREA_NULL) {
			double x, y;
			double dist;

			x = pic->icp.L.x;
			y = pic->icp.L.y;
#define ISTEEDISTANCE	0.05
			dist = sqrt((x-0)*(x-0) + (y-0)*(y-0));
			if (dist < ISTEEDISTANCE) {
				isteeshot = 1;
			}
		} else {
			if (piana->processarea == IANA_AREA_TEE) {
				isteeshot = 1;
			}
		}
	}

	//--
//#define CLUBPATHDEVIATIONMAX1  8.0
#define CLUBPATHDEVIATIONMAX1  10.0
//#define CLUBPATHDEVIATIONMAX2  4.0
//#define CLUBPATHDEVIATIONMAX3  6.0
#define CLUBPATHDEVIATIONMAX3  8.0
//#define CLUBPATHDEVIATIONMAX4  3.0
	{
		if (isteeshot) {
			deltaDegreeMax = CLUBPATHDEVIATIONMAX1;
		} else {
			deltaDegreeMax = CLUBPATHDEVIATIONMAX3;
		}

		//---- 
		if (deltaDegree < - deltaDegreeMax) {
			deltaDegree = - deltaDegreeMax;
			deltaDegree += ((rand() % 1234) / 1234.0 - 0.5);
		}
		if (deltaDegree > deltaDegreeMax) {
			deltaDegree = deltaDegreeMax;
			deltaDegree += ((rand() % 1234) / 1234.0 - 0.5);
		}
	}
	
	{
		double dmult;
		double effv;
		double ddg;
		double sidespincoeff;
#define 	SSC1		1.5
#define 	SSC2		2.5
#define 	SSC3		3.0
#define 	SSC4		3.5
#define 	SSC5		3.2
#define 	SSC10		3.1
#define 	SSC20		3.0
		if (deltaDegree > 0) {
			ddg = deltaDegree;
		} else {
			ddg = -deltaDegree;
		}

		if (ddg < 1.0) {
			sidespincoeff = SSC1;
		} else if (ddg < 2.0) {
			sidespincoeff = LINEAR_SCALE(ddg, 1.0, SSC1, 2.0, SSC2);
		} else if (ddg < 3.0) {
			sidespincoeff = LINEAR_SCALE(ddg, 2.0, SSC2, 3.0, SSC3);
		} else if (ddg < 4.0) {
			sidespincoeff = LINEAR_SCALE(ddg, 3.0, SSC3, 4.0, SSC4);
		} else if (ddg < 5.0) {
			sidespincoeff = LINEAR_SCALE(ddg, 4.0, SSC4, 5.0, SSC5);
		} else if (ddg < 10.0) {
			sidespincoeff = LINEAR_SCALE(ddg, 5.0, SSC5,10.0, SSC10);
		} else if (ddg < 20.0) {
			sidespincoeff = LINEAR_SCALE(ddg,10.0,SSC10,20.0, SSC20);
		} else {
			sidespincoeff = SSC20;
		}

		if (isteeshot == 0) {			// For Iron shot... 
			sidespincoeff *= 0.9;
		} else {
			sidespincoeff *= 1.0;
		}

		if (vmag < 30.0) {
			dmult = 1.5;
		} else if (vmag < 40) {
			dmult = LINEAR_SCALE(vmag, 30.0, 1.5, 40.0, 2.0);
		} else if (vmag < 50) {
			dmult = LINEAR_SCALE(vmag, 40.0, 2.0, 50.0, 2.0);
		} else if (vmag < 55) {
			dmult = LINEAR_SCALE(vmag, 50.0, 2.0, 55.0, 1.4);
		} else if (vmag < 60) {
			dmult = LINEAR_SCALE(vmag, 55.0, 1.4, 60.0, 1.4);
		} else {
			dmult = 1.4;
		}
		sidespincoeff *= dmult;


		if (vmag < 5.0) {
			effv = vmag;
		} else if (vmag < 10.0) {
			effv = LINEAR_SCALE(vmag, 5.0,   5.0, 10.0, 6.0);
		} else if (vmag < 20) {
			effv = LINEAR_SCALE(vmag, 10.0,  6.0, 20.0, 15.0);
		} else if (vmag < 30) {
			effv = LINEAR_SCALE(vmag, 20.0, 15.0, 30.0, 20.0);
		} else if (vmag < 40) {
			effv = LINEAR_SCALE(vmag, 30.0, 20.0, 40.0, 30.0);
		} else if (vmag < 50) {
			effv = LINEAR_SCALE(vmag, 40.0, 30.0, 50.0, 40.0);
		} else if (vmag < 55) {
			effv = LINEAR_SCALE(vmag, 50.0, 40.0, 55.0, 50.0);
		} else if (vmag < 70) {
			effv = LINEAR_SCALE(vmag, 55.0, 50.0, 70.0, 70.0);
		} else {
			effv = 70.0;
		}

		sidespin = deltaDegree * effv * sidespincoeff;
	}
	pcpc->sidespin = sidespin;

	//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "## SIDESPIN azimuth: %lf clubpath: %lf vmag: %lf sidespin: %lf\n",
	//azimuth, clubpath, vmag, sidespin);

	res = 1;
	return res;
}



/*!
 ********************************************************************************
 *	@brief      Club
 *	            BACKSPIN, using EUCAM logic
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: changed, 0: NOT changed.
 *
 *	@author	    yhsuk
 *  @date       2017/0518
 *******************************************************************************/
static I32 ClubPath_CalcSpin_BackSpin(iana_t *piana, U32 camid, clubpath_context_t *pcpc)	
{
	I32 res;
	double vmag;
	double incline;
	double azimuth;

	double clubspeedB;

	double clubpath;
	double deltaDegree;

	double backspin;

	U32 isteeshot;

	iana_shotresult_t *psr;
	iana_cam_t	*pic;

	//-----------
//#define RAND_FIX_TEST_2		1234
#if defined(RAND_FIX_TEST_2)
	srand(RAND_FIX_TEST_2);
#endif

	pic = piana->pic[camid];
	psr = &piana->shotresultdata;
	{
		vmag			= psr->vmag;
		incline 		= psr->incline;
		azimuth 		= psr->azimuth;

		clubspeedB 		= pcpc->clubspeedB;
		clubspeedB 		= pcpc->clubspeedB;
		clubpath 		= pcpc->clubpath;

		deltaDegree = azimuth - clubpath;

		isteeshot = 0;
		if (piana->processarea == IANA_AREA_NULL) {
			double x, y;
			double dist;

			x = pic->icp.L.x;
			y = pic->icp.L.y;
#define ISTEEDISTANCE	0.05
			dist = sqrt((x-0)*(x-0) + (y-0)*(y-0));
			if (dist < ISTEEDISTANCE) {
				isteeshot = 1;
			}
		} else {
			if (piana->processarea == IANA_AREA_TEE) {
				isteeshot = 1;
			}
		}
	}

	{
#define BACKSPIN_MAX_TEE		7000
#define BACKSPIN_MIN_TEE		100.0

#define BACKSPIN_MAX_IRON		11570.0
#define BACKSPIN_MIN_IRON		100.0
		double backspincoeff;
		double max_backspin;
		double min_backspin;
		if (isteeshot == 1) {			
			if (incline < 8) {
				backspincoeff = 5;
			} else if (incline < 10) {
				backspincoeff = LINEAR_SCALE(incline, 8.0, 5.0, 10.0, 4.8);
			} else if (incline < 15) {
				backspincoeff = LINEAR_SCALE(incline, 10.0,4.8, 15.0, 4.5);
			} else if (incline < 20.0) {
				backspincoeff = LINEAR_SCALE(incline, 15.0, 4.5, 20.0, 4.2);
			} else if (incline < 30.0) {
				backspincoeff = LINEAR_SCALE(incline, 20.0, 4.2, 30.0, 4.0);
			} else if (incline < 40.0) {
				backspincoeff = LINEAR_SCALE(incline, 30.0, 4.0, 40.0, 3.5);
			} else if (incline < 50.0) {
				backspincoeff = LINEAR_SCALE(incline, 40.0, 3.5, 50.0, 2.5);
			} else {
				backspincoeff = 2.5;
			} 
		} else {
			if (incline < 15) {
				backspincoeff = 5;
			} else if (incline < 20.0) {
				backspincoeff = LINEAR_SCALE(incline, 15.0, 5.0, 20.0, 5.0);
			} else if (incline < 30.0) {
				backspincoeff = LINEAR_SCALE(incline, 20.0, 5.0, 30.0, 6.0);
			} else if (incline < 40.0) {
				backspincoeff = LINEAR_SCALE(incline, 30.0, 6.0, 40.0, 6.0);
			} else if (incline < 50.0) {
				backspincoeff = LINEAR_SCALE(incline, 40.0, 6.0, 50.0, 6.5);
			} else if (incline < 60.0) {
				backspincoeff = LINEAR_SCALE(incline, 50.0, 6.5, 60.0, 7.0);
			} else {
				backspincoeff = 7.0;
			} 
		}

		{
			double effectvmag;

			if (vmag < 50.0) {
				effectvmag = vmag;
			} else if (vmag < 60.0) {
				effectvmag = LINEAR_SCALE(vmag, 50.0, 50.0, 60.0, 52.0);
			} else if (vmag < 70.0) {
				effectvmag = LINEAR_SCALE(vmag, 60.0, 52.0, 70.0, 53.1);
			} else {
				effectvmag = 53.1;
			}
			backspin = backspincoeff * effectvmag * incline;
		}


		if (isteeshot == 1) {			
			double backspin2;
			//backspin = backspin * 0.9;
			//backspin = backspin * 0.85;

			// 0: 0
			// ~ 2800: 3000
			// ~ 4000: 3800
			// ~ 5000: 4200
			// ~ 6000: 6000
			// ~ 8000: 8000
			// ~     

			if (backspin < 500) 
			{
				backspin2 = backspin;
			} else if (backspin < 800) {
				backspin2 = LINEAR_SCALE(backspin, 500.0,   500.0,  800.0, 1500.0);
			} else if (backspin < 1000) {
				backspin2 = LINEAR_SCALE(backspin, 800.0,  1500.0, 1000.0, 2000.0);
			} else if (backspin < 2000) {
				backspin2 = LINEAR_SCALE(backspin, 1000.0, 2000.0, 2000.0, 3000.0);
			} else if (backspin < 4000) {
				backspin2 = LINEAR_SCALE(backspin, 2000.0, 3000.0, 4000.0, 3800.0);
			} else if (backspin < 5000) {
				backspin2 = LINEAR_SCALE(backspin, 4000.0, 3800.0, 5000.0, 4200.0);
			} else if (backspin < 6000) {
				backspin2 = LINEAR_SCALE(backspin, 5000.0, 4200.0, 6000.0, 6000.0);
			} else {
				backspin2 = backspin;
			}

			backspin = backspin2;

			max_backspin = BACKSPIN_MAX_TEE;
			min_backspin = BACKSPIN_MIN_TEE;
		} else {

			double backspin2;

			if (backspin < 500) 
			{
				backspin2 = backspin;
			} else if (backspin < 800) {
				backspin2 = LINEAR_SCALE(backspin,  500.0,  500.0,  800.0, 1800.0);
			} else if (backspin < 1000) {
				backspin2 = LINEAR_SCALE(backspin,  800.0, 1800.0, 1000.0, 2200.0);
			} else if (backspin < 2000) {
				backspin2 = LINEAR_SCALE(backspin, 1000.0, 2200.0, 2000.0, 3500.0);
			} else if (backspin < 4000) {
				backspin2 = LINEAR_SCALE(backspin, 2000.0, 3500.0, 4000.0, 4000.0);
			} else if (backspin < 5000) {
				backspin2 = LINEAR_SCALE(backspin, 4000.0, 4000.0, 5000.0, 4400.0);
			} else if (backspin < 6000) {
				backspin2 = LINEAR_SCALE(backspin, 5000.0, 4400.0, 6000.0, 6000.0);
			} else {
				backspin2 = backspin;
			}

			backspin = backspin2;

			max_backspin = BACKSPIN_MAX_IRON;
			min_backspin = BACKSPIN_MIN_IRON;
		}

		if (backspin > max_backspin) {
			backspin = max_backspin;
			backspin += ((rand() / (double) RAND_MAX)  - 0.5) * 1000;
		}

		if (backspin < min_backspin) {
			backspin = min_backspin;
			backspin += ((rand() / (double) RAND_MAX)  - 0.5) * 50;
		}
	}
	pcpc->backspin = backspin;

	//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "## BACKSIN incline: %lf vmag: %lf backspin: %lf\n",
	//		incline, vmag, backspin);
	res = 1;
	return res;
}



//////// TODO: INDEX -> FRAME!!!
/*!
 ********************************************************************************
 *	@brief      CAM club image fit
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2017/07/07
 *******************************************************************************/
I32 iana_clubimage_fit(iana_t * piana)	
{
	I32 res;

	U32 camidBC;

	clubpath_context_t 	*pcpc;

	I32 bulkshotindexBC;
	
	iana_cam_t	*picBC;
	U64 ts64shot; double ts64shotd;
	U64 ts64shotBC; double ts64shotdBC;
	double tsdiff;

	I32 index;

	camif_buffer_info_t *pb;
	U32 j;

	I32 marksequencelen;
	//----
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}	
	
	res = 0;

	if (piana->camsensor_category == CAMSENSOR_VISION2CB
		|| piana->camsensor_category == CAMSENSOR_CBALL
		|| piana->camsensor_category == CAMSENSOR_CBALL2
		|| piana->camsensor_category == CAMSENSOR_CONSOLE1
		|| piana->camsensor_category == CAMSENSOR_TCAM
		|| piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO
		|| piana->camsensor_category == CAMSENSOR_P3V2

		) {
		camidBC = 0;
		picBC  = piana->pic[camidBC];

		ts64shot  = picBC->ts64shot;
		ts64shotd = ts64shot/TSSCALE_D;
		bulkshotindexBC = 10;		// FAKE value.

		ts64shotdBC = 0;

		pcpc = &picBC->cpc;


		//--
		if (piana->camsensor_category == CAMSENSOR_P3V2) {
			piana->pic[0]->cpc.bulkshotindex = 0;
			piana->pic[0]->cpc.bulkindexFrom = 0;
			piana->pic[0]->cpc.bulkindexTo = marksequencelen-1;

			piana->pic[1]->cpc.bulkshotindex = 0;
			piana->pic[1]->cpc.bulkindexFrom = 0;
			piana->pic[1]->cpc.bulkindexTo = marksequencelen-1;
		}


		pcpc->bulkshotindex = 0;
		pcpc->bulkindexFrom = 0;
		pcpc->bulkindexTo = marksequencelen-1;


		//---------------------------------------------------
		pb = NULL;
		for (index = 0; index < marksequencelen; index++) {
			if (piana->opmode == IANA_OPMODE_FILE) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camidBC, NORMALBULK_BULK, index, &pb, NULL);
				if (res == 0) {
					break;
				}
			} else {
				for (j = 0; j < 20; j++) {
					res = scamif_imagebuf_randomaccess(piana->hscamif, camidBC, NORMALBULK_BULK, index, &pb, NULL);
					if (res == 1) {
						break;
					}
					cr_sleep(1);
				}
				if (res == 0) {
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", index, j);
					break;
					//continue;
				}
			}

			ts64shotBC = MAKEU64(pb->ts_h, pb->ts_l);
			ts64shotdBC = ts64shotBC/TSSCALE_D;

			tsdiff = ts64shotdBC - ts64shotd;
	#define TSDIFFMIN	(0.001 * 0.1)
			if (tsdiff > 0 || tsdiff > -TSDIFFMIN) {
				bulkshotindexBC = index;

				break;
			}
		}
		pcpc = &piana->pic[camidBC]->cpc;
		pcpc->bulkshotindex = bulkshotindexBC;
		pcpc->bulkindexFrom = 0;
		pcpc->bulkindexTo   = marksequencelen;

		pcpc->sx = 0;
		pcpc->ex = pcpc->width-1;
		//---------------------------------------------------

	}

	res = 1;

	return res;
}


#if defined (__cplusplus)
}
#endif


