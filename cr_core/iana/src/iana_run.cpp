/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA RUN
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_run.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2016/02/30 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <math.h>

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"

#include "scamif.h"
//#include "scamif_main.h"

#include "iana.h"
#include "iana_run.h"
#include "iana_implement.h"
#include "iana_cam.h"
// #include "iana_tool.h"
#include "iana_coordinate.h"
#include "iana_cam_implement.h"
#include "iana_work.h"
#include "iana_readinfo.h"
#include "iana_configure.h"
#include "iana_xbrd.h"

#include "iana_extrinsic.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
void iana_changerightleft_LITE(iana_t *piana);
I32 iana_recalc_campos(iana_t *piana);



/*----------------------------------------------------------------------------
 *	Description	: local functions
 -----------------------------------------------------------------------------*/
static void converttransmat(iana_t *piana)
{
	U32 camid;
	U32 i;
	U32 count;
	point_t Lp[1024];
	point_t LpP[1024];
	point_t PpD[1024];
	point_t PpUnd[1024];

	point_t LpPRec[1024];
	point_t LpCRec[1024];




	biquad_t bAp2l, bAl2p;
	biquad_t bAp2lUnd, bAl2pUnd;
	iana_cam_param_t *picp;

	double x, y;

	//---
	for(camid = 0; camid < NUM_CAM; camid++) {
		picp = &piana->pic[camid]->icp;

		// 1) get L and P points 
		count = 0;
		for (y = -0.5; y < 0.5 + 0.01; y+= 0.05) {
			for (x = -0.4; x < 0.4 + 0.01; x+= 0.05) {
				Lp[count].x = x; Lp[count].y = y;
				count++;
			}
		}
		for (i = 0; i < count; i++) {
			cr_bi_quadratic(&picp->bAl2p_raw, &Lp[i], &PpD[i]);
		}

		// 2) Undistortion.. 
		for (i = 0; i < count; i++) {
			cr_undistortPoint(&PpD[i], &PpUnd[i], picp->cameraintrinsic.cameraParameter, picp->cameraintrinsic.distCoeffs);			// to Undisorted....
		}

		// 3) get points on the Projection plane
		{
#define	HEIGHT_CALI_FLATE		0.007
			for (i = 0;  i < count; i++) {
				cr_point_t crLpcp, crLppp;
				cr_line_t  ctocaliplateline;			// line passes camera point to point on the cali plate

				//---
				crLpcp.x = Lp[i].x;
				crLpcp.y = Lp[i].y;
				crLpcp.z = HEIGHT_CALI_FLATE;
				if (*piana->pCamPosEstimatedLValid) {
					cr_line_p0p1(&picp->CamPosEstimatedL, &crLpcp, &ctocaliplateline);
				} else {
					cr_line_p0p1(&picp->CamPosL, &crLpcp, &ctocaliplateline);
				}

				cr_point_line_plane(&ctocaliplateline, &picp->ProjectionPlane, &crLppp);
				LpP[i].x = crLppp.x;
				LpP[i].y = crLppp.y;
			}
		}

		// 4) Get quad matrix for Project Plane.
		{
			//-- Distorted
			cr_bi_quadratic_matrix(
					&bAp2l,
					&PpD[0],				// Pixel point, distorted
					&LpP[0],				// Local point data, on the Projected Plane
					count);
			cr_bi_quadratic_matrix(
					&bAl2p,
					&LpP[0],				// Local point data, on the Projected Plane
					&PpD[0],				// Pixel point, distorted
					count);
			
			//-- UnDistorted
			cr_bi_quadratic_matrix(
					&bAp2lUnd,
					&PpUnd[0],				// Pixel point, Undistorted
					&LpP[0],				// Local point data, on the Projected Plane
					count);
			cr_bi_quadratic_matrix(
					&bAl2pUnd,
					&LpP[0],				// Local point data, on the Projected Plane
					&PpUnd[0],				// Pixel point, Undistorted
					count);
		}

		{
			U64	*p64;


			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] Transmatrix ------------ \n", camid);

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Ap2l, distorted\n");
			p64 = (U64 *)&bAp2l.mat[0];
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
					"0x%08llx 0x%08llx 0x%08llx 0x%08llx\n", *(p64+ 0), *(p64+ 1), *(p64+ 2), *(p64+ 3));
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
					"0x%08llx 0x%08llx 0x%08llx 0x%08llx\n", *(p64+ 4), *(p64+ 5), *(p64+ 6), *(p64+ 7));
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
					"0x%08llx 0x%08llx 0x%08llx 0x%08llx\n", *(p64+ 8), *(p64+ 9), *(p64+10), *(p64+11));

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n\nAl2p, distorted\n");
			p64 = (U64 *)&bAl2p.mat[0];
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
					"0x%08llx 0x%08llx 0x%08llx 0x%08llx\n", *(p64+ 0), *(p64+ 1), *(p64+ 2), *(p64+ 3));
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
					"0x%08llx 0x%08llx 0x%08llx 0x%08llx\n", *(p64+ 4), *(p64+ 5), *(p64+ 6), *(p64+ 7));
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
					"0x%08llx 0x%08llx 0x%08llx 0x%08llx\n", *(p64+ 8), *(p64+ 9), *(p64+10), *(p64+11));

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n\nAp2l, Undistorted\n");
			p64 = (U64 *)&bAp2lUnd.mat[0];
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
					"0x%08llx 0x%08llx 0x%08llx 0x%08llx\n", *(p64+ 0), *(p64+ 1), *(p64+ 2), *(p64+ 3));
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
					"0x%08llx 0x%08llx 0x%08llx 0x%08llx\n", *(p64+ 4), *(p64+ 5), *(p64+ 6), *(p64+ 7));
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
					"0x%08llx 0x%08llx 0x%08llx 0x%08llx\n", *(p64+ 8), *(p64+ 9), *(p64+10), *(p64+11));

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n\nAl2p, Undistorted\n");
			p64 = (U64 *)&bAl2pUnd.mat[0];
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
					"0x%08llx 0x%08llx 0x%08llx 0x%08llx\n", *(p64+ 0), *(p64+ 1), *(p64+ 2), *(p64+ 3));
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
					"0x%08llx 0x%08llx 0x%08llx 0x%08llx\n", *(p64+ 4), *(p64+ 5), *(p64+ 6), *(p64+ 7));
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
					"0x%08llx 0x%08llx 0x%08llx 0x%08llx\n", *(p64+ 8), *(p64+ 9), *(p64+10), *(p64+11));
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n\n");

		}

		{
			// 1) UndP -> LpP
			// 2) LpP  -> LpC

			cr_vector_t n;
			cr_point_t fixpedpoint;
			cr_plane_t  ctocaliplateplane;			// cali plate

			n.x = n.y = 0; n.z = 1;
			fixpedpoint.x = fixpedpoint.y = 0;
			fixpedpoint.z = HEIGHT_CALI_FLATE;
			cr_plane_p0n(&fixpedpoint, &n, &ctocaliplateplane);			// xy plane.. :)


			for (i = 0; i < count; i++) { 			// LpP -> Lpc
				double z;
				cr_point_t crLpPp;
				cr_point_t crLpCp;
				cr_line_t  ctopline;			// line passes camera point to point on the Projection plane

				cr_bi_quadratic(&bAp2lUnd, &PpUnd[i], &LpPRec[i]); // UndP -> LpP

				/*res = */cr_plane_xy2z(&picp->ProjectionPlane, LpPRec[i].x, LpPRec[i].y, &z);

				crLpPp.x = LpPRec[i].x;
				crLpPp.y = LpPRec[i].y;
				crLpPp.z = z;

				cr_line_p0p1(&picp->CamPosEstimatedL, &crLpPp, &ctopline);

				cr_point_line_plane(&ctopline, &ctocaliplateplane, &crLpCp);
				LpCRec[i].x = crLpCp.x;
				LpCRec[i].y = crLpCp.y;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] %10lf %10lf   ->   %10lf %10lf   (%10lf %10lf) -> %10lf %10lf   (%10lf %10lf)\n",
						i,
						PpUnd[i].x, PpUnd[i].y, 
						LpPRec[i].x, LpPRec[i].y, 
						LpP[i].x, LpP[i].y, 
						LpCRec[i].x, LpCRec[i].y, Lp[i].x, Lp[i].y);
			}
		}
	}
}


/*!
 ********************************************************************************
 *	@brief      Run start
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/02/23
 *******************************************************************************/

I32 iana_run_start(iana_t *piana)
{
	I32 res;
	int convert_tansmat = 0;
	int recalc_campos = 1;	

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with hiana: %p\n",  piana);
	
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" before wait piana->hmutex\n");
	cr_mutex_wait(piana->hmutex, INFINITE);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" after  wait piana->hmutex\n");

	if (piana->opmode == IANA_OPMODE_DEFAULT ||piana->opmode == IANA_OPMODE_CAM) {

		res = iana_runparam_init(piana);
		iana_setReadyCam(piana);

		if (res < 0) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); 
			cr_mutex_release(piana->hmutex);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d After release piana->hmutex\n", __LINE__);
			goto func_exit;
		}

	} else {
#if defined(HEIGHT_CALI_FLAT_DEFAULT)
		piana->height_cali_flat = ggg_HEIGHT_CALI_FLAT_DEFAULT;
#else
		piana->height_cali_flat = 0;
#endif
		res = iana_runparam_init(piana);
		iana_setReadyCam(piana);


		{
			U32 camid;
			for (camid = 0; camid < NUM_CAM; camid++) {
				piana->info_runProcessingWidth[camid]	= piana->pic[camid]->runProcessingWidth;
				piana->info_runProcessingHeight[camid]	= piana->pic[camid]->runProcessingHeight;
			}
		}

		res = iana_readinfo_xml(piana, piana->readinfofile);

		if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
			piana->trajmode = 3;				// scapegoat.. :P
		} else {
			piana->trajmode = 1;
		}


		iana_cam_getCamInterinsicParamAll(piana);
		if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"Category: CAMSENSOR_CONSOLE1, camidCheckReady: %d\n", piana->camidCheckReady);
//#define CAMEXTRINSIC_VALID_ZERO
#if defined(CAMEXTRINSIC_VALID_ZERO)
			piana->pic[0]->icp.CamExtrinsicValid = 0;
			piana->pic[1]->icp.CamExtrinsicValid = 0;
#endif
			if (piana->pic[0]->icp.CamExtrinsicValid == 0 ||piana->pic[1]->icp.CamExtrinsicValid == 0) {			// 
				iana_calc_extrinsic(piana);
			}

		}

		if (recalc_campos) {
			iana_recalc_campos(piana);
		}

		iana_changerightleft_LITE(piana);

		iana_getProjectPlane(piana);	
		
		if (convert_tansmat) {
			converttransmat(piana);
		}

		{
			U32 camidReady;
			U32 camidOther;
			cr_point_t	*pStartPosLReady;
			cr_point_t	*pStartPosLOther;
	

			double dx, dy;
			double dist;

			camidReady = piana->camidCheckReady;
			camidOther = piana->camidOther;

			pStartPosLReady = &piana->info_startPosL[camidReady];
			pStartPosLOther = &piana->info_startPosL[camidOther];

			//-- Check position diff.
			UNUSED(dx);UNUSED(dy);UNUSED(dist);

			{
				U32 camid;
				U32 pointBGood;
				cr_point_t  *pP[MAXCAMCOUNT], *pL[MAXCAMCOUNT];
				cr_point_t  *pB[MAXCAMCOUNT];

				pointBGood = 1;
				for (camid = 0; camid < NUM_CAM; camid++) {
					pP[camid] = &piana->info_startPosP[camid];
					pL[camid] = &piana->info_startPosL[camid];
					pB[camid] = &piana->info_startPosB[camid];
					if (pP[camid]->x < -100 || pP[camid]->y < -100 || pP[camid]->z < -100) {
						point_t P0;
						point_t L0;

						L0.x = pL[camid]->x;
						L0.y = pL[camid]->y;
						iana_L2P(piana, camid, &L0, &P0, 0);
						pP[camid]->x = P0.x;
						pP[camid]->y = P0.y;
					}


					if (pB[camid]->x < -100 || pB[camid]->y < -100 || pB[camid]->y < -100) {
						pointBGood = 0;
					}
				}
				if (pointBGood == 0) {


				}
			}
		}

		res = iana_checkChessboardPixelPoint(piana);

#if defined(HEIGHT_CALI_FLAT_DEFAULT)
		if (piana->height_cali_flat < -990) {
			piana->height_cali_flat = ggg_HEIGHT_CALI_FLAT_DEFAULT;
		}
#endif
		if (res < 0) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); 
			cr_mutex_release(piana->hmutex);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d After release piana->hmutex\n", __LINE__);
			goto func_exit;
		}

		res = iana_config_conversion(piana);

		if (res < 0) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); 
			cr_mutex_release(piana->hmutex);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d After release piana->hmutex\n", __LINE__);
			goto func_exit;
		}
	}
	
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d to Prepared.\n", __LINE__);
	piana->state 		= IANA_STATE_STARTED;
	piana->runstate 	= IANA_RUN_PREPARED;
	piana->runstate_prepared_tick = cr_gettickcount();
	piana->activated	= 1;
	piana->readygoodcount = 0;

	cr_mutex_release(piana->hmutex);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d After release piana->hmutex\n", __LINE__);
func_exit:
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": #9  exit (%d)\n", res);	
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res %d\n", res);

	return res;
}



/*!
 ********************************************************************************
 *	@brief      Run start, LITE
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2017/11/13
 *******************************************************************************/
I32 iana_run_start_LITE(iana_t *piana)
{
	I32 res;

	res = 0;
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++\n");
	cr_mutex_wait(piana->hmutex, INFINITE);

	if (piana->opmode == IANA_OPMODE_DEFAULT ||piana->opmode == IANA_OPMODE_CAM) {
		
		res = iana_runparam_init(piana);

		if (res < 0) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); 
			cr_mutex_release(piana->hmutex);
			goto func_exit;
		}
	}

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d to Prepared.\n", __LINE__);
	
	piana->state 		= IANA_STATE_STARTED;
	piana->runstate 	= IANA_RUN_PREPARED;
	piana->runstate_prepared_tick = cr_gettickcount();
	piana->activated	= 1;
	piana->readygoodcount = 0;
	cr_mutex_release(piana->hmutex);

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"--\n");

	return res;
}




/*!
 ********************************************************************************
 *	@brief      Run stop
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/02/23
 *******************************************************************************/
I32 iana_run_stop(iana_t *piana)
{
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with hiana: %p\n",  piana);
	
	if (piana->opmode == IANA_OPMODE_DEFAULT ||piana->opmode == IANA_OPMODE_CAM) {
		iana_cam_stop(piana);
	}
	piana->state 		= IANA_STATE_STOPPED;
	piana->runstate 	= IANA_RUN_EMPTY;
	piana->activated	= 0;
	
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res %d\n", 1);
	
	return 1;
}

/*!
 ********************************************************************************
 *	@brief      Run Pause
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/02/23
 *******************************************************************************/
I32 iana_run_pause(iana_t *piana)
{
	piana->state 		= IANA_STATE_STOPPED;
	piana->runstate 	= IANA_RUN_EMPTY;
	piana->activated	= 0;
	return 1;
}

/*!
 ********************************************************************************
 *	@brief      Run Restart
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/02/23
 *******************************************************************************/
I32 iana_run_restart(iana_t *piana)
{
	I32 res;

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with hiana: %p\n",  piana);
	
	cr_mutex_wait(piana->hmutex, INFINITE);

	piana->needrestart = 0;
	piana->needrestart2 = 0;

	piana->currentcamconf = IANA_CAMCONF_CHANGEME;

	res = 0;
	if (piana->opmode == IANA_OPMODE_DEFAULT ||piana->opmode == IANA_OPMODE_CAM) {

		res = iana_cam_restart(piana);

		if (res == 0 || res < 0) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,"%d res: %d\n", __LINE__, res); 
			cr_mutex_release(piana->hmutex);
			goto func_exit;
		}

		res = iana_runparam_init(piana);
		iana_setReadyCam(piana);
	} else {
		res = 1;
	}


	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d to Prepared.\n", __LINE__);
	piana->state 		= IANA_STATE_STARTED;
	piana->runstate 	= IANA_RUN_PREPARED;
	piana->runstate_prepared_tick = cr_gettickcount();
	piana->readygoodcount = 0;

	cr_mutex_release(piana->hmutex);

func_exit:

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res %d\n", res);
	
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Run Restart2
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/04/24
 *******************************************************************************/
I32 iana_run_restart2(iana_t *piana)
{

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with hiana: %p\n",  piana);
	
	cr_mutex_wait(piana->hmutex, INFINITE);

	piana->needrestart = 0;
	piana->needrestart2 = 0;

	piana->currentcamconf = IANA_CAMCONF_CHANGEME;
	if (piana->opmode == IANA_OPMODE_DEFAULT ||piana->opmode == IANA_OPMODE_CAM) {
		//scamif_cam_resume(piana->hscamif, 1);
		//scamif_cam_resume(piana->hscamif, 0);

		iana_runparam_init(piana);
		iana_setReadyCam(piana);
	}

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d to Prepared.\n", __LINE__);
	piana->state 		= IANA_STATE_STARTED;
	piana->runstate 	= IANA_RUN_PREPARED;
	piana->runstate_prepared_tick = cr_gettickcount();
	piana->readygoodcount = 0;

	cr_mutex_release(piana->hmutex);

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res %d\n", 1);
	return 1;

}



/*!
 ********************************************************************************
 *	@brief      Need Restart
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/02/23
 *******************************************************************************/
I32 iana_run_needrestart(iana_t *piana)
{
	piana->needrestart = 1;
	//piana->needxbdcheck = 1;
	//iana_xboard_needbdcheck(piana);

	//cr_log(piana->hcl, 3, "needrestart _____run_needrestart %d", __LINE__);

	return 1;
}



/*!
 ********************************************************************************
 *	@brief      Need Restart2
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/02/23
 *******************************************************************************/
I32 iana_run_needrestart2 (iana_t *piana)
{
	UNUSED(piana);		
	piana->needrestart2 = 1;					// FAKE..
	//piana->needxbdcheck = 1;
	//iana_xboard_needbdcheck(piana);

	//cr_log(piana->hcl, 3, "needrestart2 _____run_needrestart2 %d", __LINE__);

	return 1;
}


void iana_setReadyCam(iana_t *piana)
{

#if 1
#define CHECKREADY_SIDE
//#define CHECKREADY_CENTER
#endif
//#define CHECKREADY_CENTER

	piana->camidCheckReady	= 0;		// Default
	piana->camidOther		= 1;

#if defined(CHECKREADY_SIDE)
	piana->camidCheckReady	= 1;
	piana->camidOther		= 0;
#endif
#if defined(CHECKREADY_CENTER)
	piana->camidCheckReady	= 0;
	piana->camidOther		= 1;
#endif
	
	if (piana->camsensor_category == CAMSENSOR_TCAM) {
		piana->camidCheckReady	= 0;		// TCAM, CENTERCAM ..
		piana->camidOther		= 1;

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"Category: CAMSENSOR_TCAM, camidCheckReady: %d\n", piana->camidCheckReady);
	}


	if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
		//piana->camidCheckReady	= 0;		// CONSOLE1, CENTERCAM ..
		//piana->camidOther		= 1;
		piana->camidCheckReady	= 1;		// CONSOLE1, CENTERCAM ..
		piana->camidOther		= 0;

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"Category: CAMSENSOR_CONSOLE1, camidCheckReady: %d\n", piana->camidCheckReady);
	}

	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		piana->camidCheckReady	= 0;
		piana->camidOther		= 1;
	}

	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		piana->camidCheckReady	= 0;
		piana->camidOther		= 1;
	}



//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"           camidCheckReady: %d\n", piana->camidCheckReady);


}


I32 iana_recalc_campos(iana_t *piana)
{
	I32 res;
//	U32 camid;
	double cam2cam;
	iana_cam_param_t *picp0, *picp1;
	cr_point_t *pcampos0, *pcampos1;
	double z;
	cr_point_t cp;
	cr_vector_t v0, v1;

	//--
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		cam2cam = 0.575;
	} else {
		res = 0;
		goto func_exit;
	}
	picp0 = &piana->pic[0]->icp;
	picp1 = &piana->pic[1]->icp;

	pcampos0 = &picp0->CamPosL;
	pcampos1 = &picp1->CamPosL;

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" campos0: (%lf %lf %lf) campos1: (%lf %lf %lf)\n",
			pcampos0->x, pcampos0->y, pcampos0->z, pcampos1->x, pcampos1->y, pcampos1->z);
	// 1) recalc cam height
	z = (pcampos0->z + pcampos1->z) / 2.0;

	// 2) recalc x, y.
	cp.x = (pcampos0->x + pcampos1->x) / 2.0;
	cp.y = (pcampos0->y + pcampos1->y) / 2.0;
	cp.z = z;
	
	pcampos0->z = z;
	pcampos1->z = z;

	cr_vector_make2(&cp, pcampos0, &v0);
//	v0.z = 0;
	cr_vector_normalization(&v0, &v0);
	cr_vector_scalar(&v0, cam2cam/2.0, &v0);
	cr_vector_add(&v0, &cp, pcampos0);

	cr_vector_make2(&cp, pcampos1, &v1);
//	v1.z = 0;
	cr_vector_normalization(&v1, &v1);
	cr_vector_scalar(&v1, cam2cam/2.0, &v1);
	cr_vector_add(&v1, &cp, pcampos1);

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" campos0: (%lf %lf %lf) campos1: (%lf %lf %lf)\n",
			pcampos0->x, pcampos0->y, pcampos0->z, pcampos1->x, pcampos1->y, pcampos1->z);
	res = 1;


func_exit:
	return res;
}




#if defined (__cplusplus)
}
#endif



