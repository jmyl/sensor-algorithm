/*!
 *******************************************************************************
                                                                                
                   Creatz Camera Sensor 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2012 by Creatz Inc.
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc. \n

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_extrinsic.cpp
	 @brief  Extrinsic parameters... 
	 @author YongHo Suk                                 
	 @date   2018/11/23 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/



/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#if defined(_WIN32) 
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <winsock2.h>
#include <windows.h>
#endif
//#define CALC_EXTRINSIC

#if defined(CALC_EXTRINSIC)

#include <cv.h>

#include <highgui.h>
#include <cxcore.h>
#endif

//#include <math.h>
#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"

#include "iana.h"
#include "iana_work.h"
#include "iana_implement.h"
#include "iana_cam.h"
#include "iana_cam_implement.h"

// #include "iana_tool.h"


#if defined(CALC_EXTRINSIC)
#pragma comment(lib, "opencv_highgui220.lib")
#pragma comment(lib, "opencv_core220.lib")
#pragma comment(lib, "opencv_video220.lib")
#pragma comment(lib, "opencv_ml220.lib")
#pragma comment(lib, "opencv_legacy220.lib")
#pragma comment(lib, "opencv_imgproc220.lib")
#pragma comment(lib, "opencv_calib3d220.lib")
#endif

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines datatype
 -----------------------------------------------------------------------------*/
#if defined(CALC_EXTRINSIC)
	typedef	struct _opencv_param {
	CvMat *image_points;
	CvMat *intrinsic_matrix;
	CvMat *distortion_coeffs;
	CvMat *intrinsic_matrix_Updated;
	CvMat *distortion_coeffs_Updated;
	CvMat *rotation_vector;
	CvMat *translation_vector;

	CvMat *r;
	CvMat *ir;
	CvMat *extparam;
	CvMat *Invextparam;

	CvMat *camposEst_vector;
} opencv_param_t;
#endif
/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/
I32 iana_P2L_EP(iana_t *piana, U32 camid, double zvalue, point_t *pPp, point_t *pLp0);


cr_point_t camPos;
/*!
 ********************************************************************************
 *	@brief      Calc Extrinsic parameter
 *
 *
 *  @return		0: Fail, 1: Success
 *
 *	@author	    yhsuk
 *  @date       2012/12/07
 *******************************************************************************/
I32 iana_calc_extrinsic(iana_t *piana)
{
#if defined(CALC_EXTRINSIC)

	U32 res;
	U32 i, j;
	U32 camid;

	U32 pointcount;
//	caw_t 		*pcaw;	
//	camparam_t	*pcp;

//	cameraintrinsic_t *pcip;
	double *pcameraParameter;			// // fx, cx, fy, cy
	double *pdistCoeffs;				// k1, k2, p1, p2, k3	


	cameraextrinsic_t *pcep;

	opencv_param_t ocp;
	opencv_param_t *pocp;

	CvMat *object_points;

	point_t *pPp;
	point_t *pLp;


	iana_cam_t *pic;
	iana_cam_param_t *picp;


	//-----------------------------------------
	//static int zzzz = 1;
	static int zzzz = 0;

	if (zzzz == 1) {
		res = 1;
		goto func_exit;
	}
#define POINTCOUNT	(5000)
	pPp = (point_t *) malloc(sizeof(point_t) * POINTCOUNT);
	pLp = (point_t *) malloc(sizeof(point_t) * POINTCOUNT);


	pocp = &ocp;

	pocp->intrinsic_matrix 	= cvCreateMat(3, 3, CV_64FC1);
	pocp->distortion_coeffs = cvCreateMat(5, 1, CV_64FC1);
	pocp->rotation_vector 	= cvCreateMat(3, 1, CV_64FC1);
	pocp->translation_vector= cvCreateMat(3, 1, CV_64FC1);
	pocp->camposEst_vector 	= cvCreateMat(3, 1, CV_64FC1);
	pocp->r 				= cvCreateMat(3, 3, CV_64FC1);		// Rodrigues
	pocp->ir 				= cvCreateMat(3, 3, CV_64FC1);		// Inv Rodrigues
	pocp->extparam 			= cvCreateMat(4, 4, CV_64FC1);		// Extrinsic parameter
	pocp->Invextparam 		= cvCreateMat(4, 4, CV_64FC1);		// Inv Extrinsic parameter

	for (camid = 0; camid < NUM_CAM; camid++) {
		double lx, ly;
		int ix, iy;

		//---
		pic  = piana->pic[camid];				// Attack Cam..
		picp = &pic->icp;

		pcep = &picp->cep;

		picp->CamExtrinsicValid = 1;							// :D

		pcameraParameter 	= &picp->cameraintrinsic.cameraParameter[0];
		pdistCoeffs			= &picp->cameraintrinsic.distCoeffs[0];


		CV_MAT_ELEM(*pocp->intrinsic_matrix, double, 0,0) = (double)pcameraParameter[0];  // fx
		CV_MAT_ELEM(*pocp->intrinsic_matrix, double, 0,1) = 0;
		CV_MAT_ELEM(*pocp->intrinsic_matrix, double, 0,2) = (double)pcameraParameter[1];  // cx
		CV_MAT_ELEM(*pocp->intrinsic_matrix, double, 1,0) =  0;

		CV_MAT_ELEM(*pocp->intrinsic_matrix, double, 1,1) = (double)pcameraParameter[2];  // fy
		CV_MAT_ELEM(*pocp->intrinsic_matrix, double, 1,2) = (double)pcameraParameter[3];  // cy

		CV_MAT_ELEM(*pocp->distortion_coeffs,double, 0,0) = (double)pdistCoeffs[0];  // k1
		CV_MAT_ELEM(*pocp->distortion_coeffs,double, 1,0) = (double)pdistCoeffs[1];  // k2
		CV_MAT_ELEM(*pocp->distortion_coeffs,double, 2,0) = (double)pdistCoeffs[2];  // p1
		CV_MAT_ELEM(*pocp->distortion_coeffs,double, 3,0) = (double)pdistCoeffs[3];  // p2
		CV_MAT_ELEM(*pocp->distortion_coeffs,double, 4,0) = (double)pdistCoeffs[4];  // k3

		// 1) Make Pixel/Local pair
		pointcount = 0;
		//for (iy = -100; iy < 801; iy+= 10)					// 41
		for (iy = 0; iy < 401; iy+= 10)
		{
			//for (ix = -200; ix < 201; ix += 10) 			// 91
			for (ix = -400; ix < 401; ix += 10) 			// 91
			{
				lx = (double)ix / 1000.;
				ly = (double)iy / 1000.;
				(pLp+pointcount)->x = lx;
				(pLp+pointcount)->y = ly;

				iana_L2P(piana, camid, pLp+pointcount, pPp+pointcount, 0);
				pointcount++;
			//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] lx ly: %lf %lf\n", camid, lx, ly);

				if (pointcount >=  POINTCOUNT) {
					break;
				}
			}
			if (pointcount >=  POINTCOUNT) {
				break;
			}
		}

		pocp->image_points 	= cvCreateMat(pointcount, 2, CV_64FC1);
		object_points 	= cvCreateMat(pointcount, 3, CV_64FC1);
		for (i = 0; i < pointcount; i++) {
			CV_MAT_ELEM(*pocp->image_points, double, i, 0) = (double)(pPp+i)->x;
			CV_MAT_ELEM(*pocp->image_points, double, i, 1) = (double)(pPp+i)->y;

			CV_MAT_ELEM(*object_points, double, i, 0) = (double)(pLp+i)->x;
			CV_MAT_ELEM(*object_points, double, i, 1) = (double)(pLp+i)->y;

			CV_MAT_ELEM(*object_points,	double, i, 2) = 0;
		}

		{
			double x, y, z;
			// 2) Get Extrinsic params
			{	//
				cvFindExtrinsicCameraParams2(
						object_points,
						pocp->image_points,
						pocp->intrinsic_matrix,
						pocp->distortion_coeffs,
						pocp->rotation_vector,
						pocp->translation_vector);


				cvRodrigues2(pocp->rotation_vector, pocp->r, NULL);
				cvInvert(pocp->r, pocp->ir);
				cvGEMM(pocp->ir, pocp->translation_vector, -1, NULL, 0, pocp->camposEst_vector, 0);


				x = CV_MAT_ELEM(*pocp->camposEst_vector, double, 0, 0);
				y = CV_MAT_ELEM(*pocp->camposEst_vector, double, 1, 0);
				z = CV_MAT_ELEM(*pocp->camposEst_vector, double, 2, 0);

				camPos.x = x;
				camPos.y = y;
				camPos.z = z;


				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] CAMPOS    %lf %lf %lf   vs %lf %lf %lf\n", camid, 
						x, y, z, 
						picp->CamPosL.x,
						picp->CamPosL.y,
						picp->CamPosL.z);
			}

			//-- Extrinsic param..
			{	//
				for (i = 0; i < 3; i++) {
					for (j = 0; j < 3; j++) {
						CV_MAT_ELEM(*pocp->extparam, double, i, j) = CV_MAT_ELEM(*pocp->r, double, i, j);
					}
					CV_MAT_ELEM(*pocp->extparam, double, i, 3) = CV_MAT_ELEM(*pocp->translation_vector, double, i, 0);
				}

				CV_MAT_ELEM(*pocp->extparam, double, 3, 0) = 0;
				CV_MAT_ELEM(*pocp->extparam, double, 3, 1) = 0;
				CV_MAT_ELEM(*pocp->extparam, double, 3, 2) = 0;
				CV_MAT_ELEM(*pocp->extparam, double, 3, 3) = 1;

				cvInvert(pocp->extparam, pocp->Invextparam, CV_LU);


				//
				for (i = 0; i < 3; i++) {
					pcep->rotationVector[i] 	= CV_MAT_ELEM(*pocp->rotation_vector, 	double, i, 0);
					pcep->translationVector[i] 	= CV_MAT_ELEM(*pocp->translation_vector, 	double, i, 0);
				}

				for (i = 0; i < 3; i++) {
					for (j = 0; j < 3; j++) {
						pcep->r[i][j] 			= CV_MAT_ELEM(*pocp->r, 	double, i, j);
					}
				}

				for (i = 0; i < 4; i++) {
					for (j = 0; j < 4; j++) {
						pcep->extMat[i][j] 		= CV_MAT_ELEM(*pocp->extparam, double, i, j);
						pcep->InvextMat[i][j] 	= CV_MAT_ELEM(*pocp->Invextparam, double, i, j);

					}
				}
			}
		}
#if 1
		// 3) Get Local Points using extrinsic params
		{
			double dx, dy, dz;
			double zvalue;
			double err0;
			point_t Lp0;
			double cx, cy, cz;

			cx = picp->CamPosL.x;
			cy = picp->CamPosL.y;
			cz = picp->CamPosL.z;
			for (i = 0; i < pointcount; i++) {
				dx = (cx - (pLp+i)->x);
				dy = (cy - (pLp+i)->y);
				dz = (cz - 0);
				zvalue = sqrt(dx*dx + dy*dy + dz* dz);
				//zvalue = zvalue_ext();

				iana_P2L_EP(piana, camid, zvalue, pPp+i, &Lp0);
				dx = (pLp+i)->x - Lp0.x; dy = (pLp+i)->y - Lp0.y; dz = 0;
				err0 = sqrt(dx*dx + dy*dy + dz*dz);

				   //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] P %10.6lf %10.6lf    \tL%10.6lf %10.6lf   with Z: %10.6lf  \tLp0 : %10.6lf %10.6lf   %10.6lf\n", 
				   //camid, 
				   //(pPp+i)->x, (pPp+i)->y,
				   //(pLp+i)->x, (pLp+i)->y, zvalue,
				   //Lp0.x, Lp0.y, err0);

			}
		}
#endif

		// 4) Compare and calc error.

		// 5) Thats all... -_-;

		cvReleaseMat(&pocp->image_points);
		cvReleaseMat(&object_points);
	}


	cvReleaseMat(&pocp->intrinsic_matrix);
	cvReleaseMat(&pocp->distortion_coeffs);
	cvReleaseMat(&pocp->rotation_vector);
	cvReleaseMat(&pocp->translation_vector);
	cvReleaseMat(&pocp->camposEst_vector);
	cvReleaseMat(&pocp->r);
	cvReleaseMat(&pocp->ir);
	cvReleaseMat(&pocp->extparam);
	cvReleaseMat(&pocp->Invextparam);

	free(pPp);
	free(pLp);
	res = 1;

func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"--\n");

	return res;
#else


	UNUSED(piana);
	return 0;
#endif
}

#if 0
void iana_P2L_EP(iana_t *piana, U32 camid, double zvalue, point_t *pPp, 
		point_t *pLp0)
{
	cr_plane_t xyplane;

	cr_vector_t nv;
	cr_point_t origin;

	cr_point_t crwPp;
	cr_point_t camPos0;
	cr_point_t crLp;
	cr_point_t Lp3d;

	iana_cam_t *pic;
	iana_cam_param_t *picp;


	//--
	if (zvalue < 0.0) {
#define DEFAULT_ZVALUE_P2L 0.5
		zvalue = DEFAULT_ZVALUE_P2L;
	}
	iana_P2L3D_EP(piana, camid, zvalue, pPp, &Lp3d);


	//---------
	// 1) make xyplane:  Z = 0 plane
	nv.x = 0; nv.y = 0; nv.z = 1;			
	origin.x = 0; origin.y = 0; origin.z = 0; 
	cr_plane_p0n(&origin, &nv, &xyplane);	

	crwPp.x = Lp3d.x;
	crwPp.y = Lp3d.y;
	crwPp.z = Lp3d.z;


	pic  = piana->pic[camid];				
	picp = &pic->icp;

	picp->CamPosL.x;

	camPos0.x = picp->CamPosL.x;
	camPos0.y = picp->CamPosL.y;
	camPos0.z = picp->CamPosL.z;

	iana_Ptr2TargetPlanePtr(&crwPp, &crLp, &camPos0, &xyplane);
	/*
	   cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "        wLp: \t\t\t\t\t\t%lf %lf %lf, %lf\n", 
	   crLp.x, crLp.y, crLp.z);
	   */

	pLp0->x = crLp.x;
	pLp0->y = crLp.y;
}
#endif

#if defined (__cplusplus)
}
#endif

