/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA cam
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_work.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2016/02/20 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

#if defined(_WIN32)
//#include <windows.h>
#include <tchar.h>
//#include <atlstr.h> 
#endif
#include <math.h>
#include <string.h>

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"

#include "scamif.h"
#include "scamif_buffer.h"

#include "iana.h"
#include "iana_implement.h"
#include "iana_cam.h"
#include "iana_cam_implement.h"
#include "iana_work.h"
// #include "iana_tool.h"
#include "iana_coordinate.h"
#include "iana_xbrd.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/



#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/

/*!
 ********************************************************************************
 *	@brief      Reconfigure camera mode
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/02/20
 *******************************************************************************/
I32 iana_cam_reconfig(iana_t *piana, U32 update)
{
	I32 res;
	U32 camconf;
	U32 i;

	switch (piana->runstate) {
		case IANA_RUN_NULL: 		camconf = IANA_CAMCONF_NULL; 		break;
		case IANA_RUN_EMPTY: 		camconf = IANA_CAMCONF_NULL; 		break;
		case IANA_RUN_CAMCHECK: 	camconf = IANA_CAMCONF_NULL; 		break;
		case IANA_RUN_PREPARED: 	camconf = IANA_CAMCONF_FULLRANGE; 	break; 
		case IANA_RUN_READY: 		camconf = IANA_CAMCONF_RUN; 		break;
		case IANA_RUN_TRANSITORY: 	camconf = IANA_CAMCONF_RUN; 		break;
		case IANA_RUN_GOODSHOT: 	camconf = IANA_CAMCONF_NULL; 		break;
		case IANA_RUN_TRIALSHOT: 	camconf = IANA_CAMCONF_RUN; 		break;
		case IANA_RUN_POWERSAVING: 	camconf = IANA_CAMCONF_POWERSAVING;	break;

		default: 					camconf = IANA_CAMCONF_NULL; 		break;
	}

	if (camconf == IANA_CAMCONF_RUN && piana->processarea == IANA_AREA_PUTTER) {
		camconf = IANA_CAMCONF_RUN_PUTTER;
	}



	if (piana->currentcamconf != camconf) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__":  runstate: %d, currentcamconf: %d, camconfig: %d\n",
			piana->runstate, piana->currentcamconf, camconf);

//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": before iana_cam_cconf.   currentcamconf: %d, camconfig: %d\n",
//				piana->currentcamconf,camconf);
		res = iana_cam_cconf(piana, camconf);
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": after iana_cam_cconf, res: %d\n", res);
		if (res > 0 && update) {
			//#define TIMER_ALLMASTER  see iana.h
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__"-=-=-=-=-=-= Before iana_cam_update() call.. :P\n", __LINE__); 

#define RECONFIG_TRY	5
			if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
					res = iana_cam_update_all(piana, piana->hscamif);
			} else {
				for (i = 0; i < RECONFIG_TRY; i++) {

					res = iana_cam_update(piana, piana->hscamif, 0);
					if (res > 0) {
						res = iana_cam_update(piana, piana->hscamif, 1);
					}
					if (res > 0) {
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__":--- GOOD iana_cam_update res: %d\n", res);
						break;
					}
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__":--- #3 iana_cam_update res: %d\n", res);
					cr_sleep(20);
				}
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__":--- #4 iana_cam_update res: %d\n", res);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,__FUNCTION__"-=-=-=-=-=-= After  iana_cam_update() call.. :P\n", __LINE__); 
			}
		}
	} else {
		res = 1;
	}

	if (res == 0 || res < 0) {
		piana->needrestart = 1;
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" needrestart, res; %d", res);
//		cr_log(piana->hcl, 3, "needrestart %s %d", "after _____cam_update", __LINE__);
	}

	return res;
}

/*!
 ********************************************************************************
 *	@brief      Set area
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/02/20
 *******************************************************************************/
I32 iana_setarea(iana_t *piana, U32 area)
{
	I32 res;

	memset(&piana->allowedarea[0], 0, sizeof(U32) * 3);
	piana->shotarea 	= IANA_AREA_NULL;
	//piana->processarea 	= IANA_AREA_NULL;			// 2021/0127
	res = 0;

	if (area >= IANA_AREA_TEE && area <= IANA_AREA_PUTTER) {
		piana->shotarea 	= area;
		piana->allowedarea[area] = 1;
		res = 1;
	}
	return res;
}

/*!
 ********************************************************************************
 *	@brief      Set area allow
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @param[in]	allowTee
 *              allow Tee area
 *  @param[in]	allowIron
 *              allow Iron area
 *  @param[in]	allowPutter
 *              allow Putter area
 *
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/02/20
 *******************************************************************************/
I32 iana_setareaallow(iana_t *piana, U32 allowTee, U32 allowIron, U32 allowPutter)
{
//	I32 res;
//	U32 activated;


	piana->allowedarea[IANA_AREA_TEE] 		= allowTee;
	piana->allowedarea[IANA_AREA_IRON] 		= allowIron;
	piana->allowedarea[IANA_AREA_PUTTER]	= allowPutter;

#if defined(USE_SHOTAREARUNMODE)
	if (piana->shotarearunmode == 1) {
		if (allowTee || allowIron) {
			allowTee = 1;
			allowIron = 1;
		}
	}
#endif
	//activated = piana->activated;
	//piana->activated = 0;
	//cr_sleep(50);
	//piana->activated = activated;
	piana->needrunstateempty = 1;

	return 1;
}

/*!
 ********************************************************************************
 *	@brief      CAM get proper ts64to
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	ts64to
 *            	time stamp for search
 *  @param[out]	pts64Rto
 *            	time stamp for ready cam
 *  @param[out]	prindexR
 *            	read index for ready cam
 *  @param[out]	pts64Oto
 *            	time stamp for the other cam
 *  @param[out]	prindexO
 *            	read index for the other cam
 * 
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/02/24
 *******************************************************************************/
I32 iana_shot_getts(iana_t *piana, U64 ts64to, 
		U64 *pts64Rfrom, U64 *pts64Rto, U32 *prindexR, 
		U64 *pts64Ofrom, U64 *pts64Oto, U32 *prindexO)
{
	I32 res;
//	U64 ts64shot;
//	U64 ts64to;
	U64 ts64Rfrom,  ts64Rto;
	U64 ts64Ofrom,  ts64Oto;
	U32 camidR, camidO;			// Camid for Ready camera and the Other camera
	U32 camRgood, camOgood;
	U32 rindexR, rindexO;
	U32 i;
	//---

	camidR = piana->camidCheckReady;
	camidO = 1 - camidR;

//	ts64shot = piana->ic[camidR].ts64shot;

	rindexR = 0;
	rindexO = 0;

	ts64Rfrom = 0;
	ts64Ofrom = 0;
	ts64Rto = 0;
	ts64Oto = 0;

#define RETRYCOUNT	10
	camRgood = 0;
	camOgood = 0;

	for (i = 0; i < RETRYCOUNT; i++) {
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---> Rg: %d, Og: %d\n", camRgood, camOgood);
		if (camRgood == 0) {
			res = scamif_imagebuf_peek_latest(piana->hscamif, camidR, NORMALBULK_NORMAL, NULL, NULL, &rindexR);	
			if (res == 0) {
				cr_sleep(5);
				continue;
			}
			res = scamif_imagebuf_timestamprange(piana->hscamif, camidR, 1, rindexR, &ts64Rfrom, &ts64Rto);
			if (ts64Rto >= ts64to) {
				camRgood = 1;
		//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]  ====> RG\n");
			}
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]iR: %d, tRf: %lf, tRt: %lf\n", i, rindexR, (ts64Rfrom/ TSSCALE_D), (ts64Rto/TSSCALE_D));
		}
		if (camOgood == 0) {
			res = scamif_imagebuf_peek_latest(piana->hscamif, camidO, NORMALBULK_NORMAL, NULL, NULL, &rindexO);	
			if (res == 0) {
				cr_sleep(5);
				continue;
			}
			res = scamif_imagebuf_timestamprange(piana->hscamif, camidO, 1, rindexO, &ts64Ofrom, &ts64Oto);
			if (ts64Oto >= ts64to) {
				camOgood = 1;
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]  ====> OG\n");
			}
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]iO: %d, tOf: %lld, tOt: %lld\n", i, rindexO, ts64Ofrom, ts64Oto);
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]iO: %d, tOf: %lf, tOt: %lf\n", i, rindexO, (ts64Ofrom/ TSSCALE_D), (ts64Oto/TSSCALE_D));
		}
		if (camRgood && camOgood) {
			break;
		}
		//cr_sleep(10);
		cr_sleep(5);
	}

	if (camRgood && camOgood) {
		*pts64Rfrom = ts64Rfrom;
		*pts64Rto = ts64Rto;
		*prindexR = rindexR;

		*pts64Ofrom = ts64Ofrom;
		*pts64Oto = ts64Oto;
		*prindexO = rindexO;

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "R: %lf, %d,  O: %lf, %d\n",
				ts64Rto / TSSCALE_D, rindexR, ts64Oto / TSSCALE_D, rindexO);
		res = 1;
	} else {
		res = 0;
	}
	return res;
}

/*!
 ********************************************************************************
 *	@brief      Make GUID
 *
 *  @param[out]	guid
 *              guid. char [16]
 *
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/02/20
 *******************************************************************************/
I32 iana_makeguid(char guid[])
{
	OSAL_SYS_GenerateGUID(guid);
	return 1;
}


/*!
 ********************************************************************************
 *	@brief      Translate ts64 from ReadyCheck to Other
 *
 *  @param[in]	ts64R
 *            	ts64 of ReadyCheck Cam
 *
 *  @return		ts64 of Other Cam
 *
 *	@author	    yhsuk
 *  @date       2016/0707
 *******************************************************************************/
inline U64 iana_ts64_r2o(iana_t *piana, U64 ts64r)
{
	U64 ts64o;

	if (piana == NULL) {
		ts64o = 0;
	} else {
		ts64o = (U64)((I64)ts64r - piana->ts64delta);		// ReadyCheck to Other cam
	}

	return ts64o;
}


/*!
 ********************************************************************************
 *	@brief      Translate ts64 from Other to ReadyCheck
 *
 *  @param[in]	ts64o
 *            	ts64 of Other cam
 *
 *  @return		ts64 of ReadyCheck Cam
 *
 *	@author	    yhsuk
 *  @date       2016/0707
 *******************************************************************************/
inline U64 iana_ts64_o2r(iana_t *piana, U64 ts64o)
{
	U64 ts64r;

	if (piana == NULL) {
		ts64r = 0;
	} else {
		ts64r = (U64)((I64)ts64o + piana->ts64delta);		// Other to  ReadyCheck cam
	}

	return ts64r;
}


/*!
 ********************************************************************************
 *	@brief      Translate my ts64 to ReadyCheck
 *
 *  @param[in]	ts64
 *            	ts64 of My cam
 *  @param[in]	camid
 *            	Camera id of me
 *  @return		ts64 of ReadyCheck Cam
 *
 *	@author	    yhsuk
 *  @date       2016/0707
 *******************************************************************************/
inline U64 iana_ts64_2r(iana_t *piana, U32 camid, U64 ts64)
{
	U64 ts64r;
	scamif_t * pscamif;

	//------------
	pscamif = (scamif_t *) piana->hscamif;


	if (piana == NULL) {
		ts64r = 0;
	} 
	else if (camid == (U32)piana->camidCheckReady)		// I am CheckReady Cam... -_-;
	{		
		ts64r = ts64;
	} else {						// I am Other cam
		ts64r = (U64)((I64)ts64 + piana->ts64delta);		// Other cam to ReadyCheck cam
	}

	return ts64r;
}

/*!
 ********************************************************************************
 *	@brief      Translate my ts64 to Other Cam
 *
 *  @param[in]	ts64
 *            	ts64 of My cam
 *  @param[in]	camid
 *            	Camera id of me
 *  @return		ts64 of Slave
 *
 *	@author	    yhsuk
 *  @date       2016/0707
 *******************************************************************************/
inline U64 iana_ts64_2o(iana_t *piana, U32 camid, U64 ts64)
{
	U64 ts64o;
	scamif_t * pscamif;

	//------------
	pscamif = (scamif_t *) piana->hscamif;

	if (piana == NULL) {
		ts64o = 0;
	} 
	else if (camid == (U32)piana->camidOther)				// I am Other Cam... -_-;
	{		
		ts64o = ts64;
	} else {						// I am Master!!!  :P
		ts64o = (U64)((I64)ts64 - piana->ts64delta);		//  ReadyCheck cam to Other cam
	}

	return ts64o;
}

/*!
 ********************************************************************************
 *	@brief      Translate my ts64 to other cam
 *
 *  @param[in]	ts64
 *            	ts64 of My cam
 *  @param[in]	camidfrom
 *            	Camera id of me
 *  @param[in]	camidto
 *            	Camera id of target
 *  @return		ts64to
 *              ts64 of target cam
 *
 *	@author	    yhsuk
 *  @date       2016/0707
 *******************************************************************************/
U64 iana_ts64_from2to(iana_t *piana, U32 camidfrom, U32 camidto, U64 ts64from)
{
	U64 ts64to;
	scamif_t * pscamif;

	//------------
	pscamif = (scamif_t *) piana->hscamif;

	if (piana == NULL) {
		ts64to = 0;
	} else if (camidfrom == camidto) {
		ts64to = ts64from;			// I am I.
	} else {						// Change.
		//if (ts64from == 0) 
		if (ts64from == piana->camidCheckReady) 
		{		// CheckReady cam to Other cam
			ts64to = (U64)((I64)ts64from - piana->ts64delta);		// ReadyCheck cam to Other cam
		} else 
		{		//  Other cam to CheckReady cam
			ts64to = (U64)((I64)ts64from + piana->ts64delta);		//  Other cam to CheckReady cam
		}
	}

	return ts64to;
}



/*!
 ********************************************************************************
 *	@brief      Get ball position - 2D/3D 
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @param[in]	p0
 *              Tee area ball position
 *  @param[in]	p1
 *              Iron area ball position
 *  @param[in]	p2
 *              Putter area ball position
 *  @param[in]	threeDimension
 *              3D(CR_TRUE) or 2D(CR_FALSE)
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2020/07/30
 *******************************************************************************/
U32 iana_getballposition(iana_t *piana, HAND p0, HAND p1, HAND p2, CR_BOOL threeDimension)
{
	U32 res;
	U32 area;

	iana_veteran_t	*pvtr;
	iana_ballposition_t	*pbp[3];
	iana_cam_t		*pic;

	//--
	if (piana != NULL && p0 != NULL && p1 != NULL && p2 != NULL) {

		pbp[0] = (iana_ballposition_t *) p0;
		pbp[1] = (iana_ballposition_t *) p1;
		pbp[2] = (iana_ballposition_t *) p2;

		for (area = IANA_AREA_TEE; area <= IANA_AREA_PUTTER; area++) {
			memset(pbp[area], 0, sizeof(iana_ballposition_t));
		}
		if (piana->processarea <= IANA_AREA_PUTTER) {
			pbp[piana->processarea]->shotresult = 1;
		}

		pic = piana->pic[piana->camidCheckReady];
		for (area = IANA_AREA_TEE; area <= IANA_AREA_PUTTER; area++) {
			U32 ballexist;
			pvtr = &pic->vtr[area];
			if ( pvtr->state == IANA_VETERAN_EXIST ||pvtr->state == IANA_VETERAN_SHOT) {
				ballexist = 1;
			} else {
				ballexist = 0;
			}
			if (ballexist) {
				pbp[area]->ballexist = 1;
				if(threeDimension)
				{
					pbp[area]->x = (int) (pic->icp.b3d.x * 1000);
					pbp[area]->y = (int) (pic->icp.b3d.y * 1000);
					pbp[area]->z = (int) (pic->icp.b3d.z * 1000);
				}
				else
				{
					pbp[area]->x = (int) (pvtr->L.x * 1000);
					pbp[area]->y = (int) (pvtr->L.y * 1000);
					pbp[area]->z = 0;
				}
			} else {
				pbp[area]->ballexist = 0;
				pbp[area]->x = 0;
				pbp[area]->y = 0;
				pbp[area]->z = 0;
			}
		}
		res = 1;
	} else {
		res = 0;
	}
	return res;
}



/*!
 ********************************************************************************
 *	@brief      Get shot result, extened.
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @param[in]	clearresult
 *              1:clear result after read,  0: let-it-be.
 *  @param[in]	p1
 *              CR2_shotdata_ballEx_t
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2016/07/18
 *******************************************************************************/

U32 iana_shotdata_ballEx(iana_t *piana, U32 clearresult, HAND p1)
{
	U32 res;

	iana_shotresult_t *psr;
	iana_shotdata_ballEx_t *psrbEx;

	//--
	if (piana != NULL && p1 != NULL) {
		psr = &piana->shotresultdata; 
		psrbEx = (iana_shotdata_ballEx_t *) p1;
		if (psr->valid == 0 || psr->readflag == 1) {		// in-valid result or Already read...
			memset(psrbEx, 0, sizeof(iana_shotdata_ballEx_t));
		} else {
			psrbEx->valid		= 1;
			psrbEx->reserved	= 0;
			psrbEx->incline		= psr->incline;
			psrbEx->azimuth		= psr->azimuth;
			psrbEx->vmag		= psr->vmag;
			psrbEx->shotAssurance	= 1;
			//psrbEx->shotAssurance	= 1;
			psrbEx->spinmag		= psr->spinmag;
			psrbEx->spinaxis[0]	= psr->axis.x;
			psrbEx->spinaxis[1]	= psr->axis.y;
			psrbEx->spinaxis[2]	= psr->axis.z;
			//psrbEx->spinAssurance	= 1;
			psrbEx->spinAssurance	= psr->spinAssurance;
		}

		if (clearresult) {
			psr->readflag = 1;
		}
		res = 1;
	} else {
		res = 0;
	}
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Get shot data, Extended.
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @param[in]	p0
 *              iana_shotdataEX_t
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2020/04/09
 *******************************************************************************/

U32 iana_shotdataEx(iana_t *piana, HAND p0)
{
	U32 res;

	iana_shotresult_t *psr;
	iana_shotdataEX_t *psdEx;

	//--
	if (piana != NULL && p0 != NULL) {
		psr = &piana->shotresultdata; 
		psdEx = (iana_shotdataEX_t *) p0;
		if (psr->valid == 0 || psr->readflag == 1) {		// in-valid result or Already read...
			memset(psdEx, 0, sizeof(iana_shotdataEX_t));
		} else {
			psdEx->ballspeedx1000		= (I32) (psr->vmag * 1000);
			psdEx->sidespin 			= (I32) (psr->sidespin);
			psdEx->backspin 			= (I32) (psr->backspin);
			psdEx->azimuthx1000 		= (I32) (psr->azimuth * 1000);

			psdEx->inclinex1000 		= (I32) (psr->incline* 1000);
			psdEx->clubspeed_Bx1000 	= (I32) (psr->clubspeed_B * 1000);
			psdEx->clubspeed_Ax1000 	= (I32) (psr->clubspeed_A * 1000);
			psdEx->clubpathx1000 		= (I32) (psr->clubpath * 1000);
			psdEx->clubfaceanglex1000 	= (I32) (psr->clubfaceangle * 1000);
			psdEx->hitheightx1000		= (I32) (0);
		}
		res = 1;
	} else {
		res = 0;
	}
	return res;
}


// ATTACKCAM
U32 iana_setRunBallsizeMinMax(iana_t *piana, U32 camid, cr_rectL_t *prectRunL, double sizeratio)
{
	point_t Pp, Pp2;
	point_t Lp, Lp2;
	double sx, sy, ex, ey;
	double sx0, sy0, ex0, ey0;
	double dx, dy, dd;
	double minsize, maxsize;
	double dtmp;

	double balldiameter;
	double theta;
	iana_cam_t	*pic;
	cr_point_t *pcampos;
	//double dx, dy, dz, dl;
	double dz, dl;

	//----
	pic   =piana->pic[camid];
	pcampos = &pic->icp.CamPosL;


	sx = prectRunL->sx; ex = prectRunL->ex;
	sy = prectRunL->sy; ey = prectRunL->ey;

	if (sx > ex) { dtmp = ex; ex = sx; sx = dtmp; }					// EX is bigger. :P
	if (sy > ey) { dtmp = ey; ey = sy; sy = dtmp; }					// EX is bigger. :P

	Pp.x = 0; Pp.y = 0;
	iana_P2L(piana, camid, &Pp, &Lp, 0);
	sx0 = Lp.x; ex0 = Lp.x;
	sy0 = Lp.y; ey0 = Lp.y;

	Pp.x = FULLWIDTH-1; Pp.y = 0;
	iana_P2L(piana, camid, &Pp, &Lp, 0);
	if (sx0 > Lp.x) {
		sx0 = Lp.x;
	}
	if (ex0 < Lp.x) {
		ex0 = Lp.x;
	}
	if (sy0 > Lp.y) {
		sy0 = Lp.y; 
	}
	if (ey0 < Lp.y) {
		ey0 = Lp.y;
	}

	Pp.x = FULLWIDTH-1; Pp.y = FULLHEIGHT-1;
	iana_P2L(piana, camid, &Pp, &Lp, 0);
	if (sx0 > Lp.x) {
		sx0 = Lp.x; 
	}
	if (ex0 < Lp.x) {
		ex0 = Lp.x;
	}
	if (sy0 > Lp.y) {
		sy0 = Lp.y; 
	}
	if (ey0 < Lp.y) {
		ey0 = Lp.y;
	}

	Pp.x = 0; Pp.y = FULLHEIGHT-1;
	iana_P2L(piana, camid, &Pp, &Lp, 0);
	if (sx0 > Lp.x) {
		sx0 = Lp.x; 
	}
	if (ex0 < Lp.x) {
		ex0 = Lp.x;
	}
	if (sy0 > Lp.y) {
		sy0 = Lp.y; 
	}
	if (ey0 < Lp.y) {
		ey0 = Lp.y;
	}

	if (sx < sx0) {
		sx = sx0; 
	}
	if (ex > ex0) {
		ex = ex0;
	}
	if (sy < sy0) {
		sy = sy0; 
	}
	if (ey > ey0) {
		ey = ey0;
	}

	//-- left-top
	{
		dx = fabs(pcampos->x - sx);
		dy = fabs(pcampos->y - sy);
		dz = fabs(pcampos->z);

		dl = sqrt(dx*dx + dy*dy + dz*dz);
		if (dz < dl) {
			theta = acos(dz / dl);
		} else {
			theta = 0;
		}
	}
	balldiameter = BALLDIAMETER / cos(theta) ;
	//Lp.x = sx; Lp.y = sy; Lp2.x = sx +				0; Lp2.y = sy + BALLDIAMETER ;
	Lp.x = sx; Lp.y = sy; Lp2.x = sx+balldiameter; Lp2.y = sy;
	iana_L2P(piana, camid, &Lp, &Pp, 0);
	iana_L2P(piana, camid, &Lp2, &Pp2, 0);
	dx = Pp.x - Pp2.x; dy = Pp.y - Pp2.y;
	dd = sqrt(dx*dx + dy*dy);
	minsize = dd; maxsize = dd;
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%d] Lp: %lf %lf Lp2: %lf %lf  Pp: %lf %lf Pp2: %lf %lf dx dy: %lf %lf dd: %d\n", camid, 
	//		Lp.x, Lp.y,
	//		Lp2.x, Lp2.y,
	//		Pp.x, Pp.y,
	//		Pp2.x, Pp2.y,
	//		dx, dy, dd);

	//-- left-bottom
	{
		dx = fabs(pcampos->x - sx);
		dy = fabs(pcampos->y - ey);
		dz = fabs(pcampos->z);

		dl = sqrt(dx*dx + dy*dy + dz*dz);
		if (dz < dl) {
			theta = acos(dz / dl);
		} else {
			theta = 0;
		}
	}
	balldiameter = BALLDIAMETER / cos(theta) ;
//	Lp.x = sx; Lp.y = ey; Lp2.x = sx + BALLDIAMETER; Lp2.y = ey;
	//Lp.x = sx; Lp.y = ey; Lp2.x = sx ; Lp2.y = ey+ BALLDIAMETER;
	Lp.x = sx; Lp.y = ey; Lp2.x = sx+balldiameter; Lp2.y = ey;
	iana_L2P(piana, camid, &Lp, &Pp,0);
	iana_L2P(piana, camid, &Lp2, &Pp2,0);
	dx = Pp.x - Pp2.x; dy = Pp.y - Pp2.y;
	dd = sqrt(dx*dx + dy*dy);
	if (minsize > dd) minsize = dd;
	if (maxsize < dd) maxsize = dd;
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%d] Lp: %lf %lf Lp2: %lf %lf  Pp: %lf %lf Pp2: %lf %lf dx dy: %lf %lf dd: %d\n", camid, 
	//		Lp.x, Lp.y,
	//		Lp2.x, Lp2.y,
	//		Pp.x, Pp.y,
	//		Pp2.x, Pp2.y,
	//		dx, dy, dd);


	//-- right-top
	{
		dx = fabs(pcampos->x - ex);
		dy = fabs(pcampos->y - sy);
		dz = fabs(pcampos->z);

		dl = sqrt(dx*dx + dy*dy + dz*dz);
		if (dz < dl) {
			theta = acos(dz / dl);
		} else {
			theta = 0;
		}
	}
	balldiameter = BALLDIAMETER / cos(theta) ;
//	Lp.x = ex; Lp.y = sy; Lp2.x = ex + BALLDIAMETER; Lp2.y = sy;
	//Lp.x = ex; Lp.y = sy; Lp2.x = ex;  Lp2.y = sy+ BALLDIAMETER;
	Lp.x = ex; Lp.y = sy; Lp2.x = ex-balldiameter;  Lp2.y = sy;
	iana_L2P(piana, camid, &Lp, &Pp,0);
	iana_L2P(piana, camid, &Lp2, &Pp2,0);
	dx = Pp.x - Pp2.x; dy = Pp.y - Pp2.y;
	dd = sqrt(dx*dx + dy*dy);
	if (minsize > dd) minsize = dd;
	if (maxsize < dd) maxsize = dd;
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%d] Lp: %lf %lf Lp2: %lf %lf  Pp: %lf %lf Pp2: %lf %lf dx dy: %lf %lf dd: %d\n", camid, 
	//		Lp.x, Lp.y,
	//		Lp2.x, Lp2.y,
	//		Pp.x, Pp.y,
	//		Pp2.x, Pp2.y,
	//		dx, dy, dd);


	//-- right-bottom
	{
		dx = fabs(pcampos->x - ex);
		dy = fabs(pcampos->y - ey);
		dz = fabs(pcampos->z);

		dl = sqrt(dx*dx + dy*dy + dz*dz);
		if (dz < dl) {
			theta = acos(dz / dl);
		} else {
			theta = 0;
		}
	}
	balldiameter = BALLDIAMETER / cos(theta) ;
//	Lp.x = ex; Lp.y = ey; Lp2.x = ex + BALLDIAMETER; Lp2.y = ey;
	//Lp.x = ex; Lp.y = ey; Lp2.x = ex; Lp2.y = ey + BALLDIAMETER;
	//Lp.x = ex; Lp.y = ey; Lp2.x = ex; Lp2.y = ey - BALLDIAMETER;
	Lp.x = ex; Lp.y = ey; Lp2.x = ex-balldiameter; Lp2.y = ey;
	iana_L2P(piana, camid, &Lp, &Pp,0);
	iana_L2P(piana, camid, &Lp2, &Pp2,0);
	dx = Pp.x - Pp2.x; dy = Pp.y - Pp2.y;
	dd = sqrt(dx*dx + dy*dy);
	if (minsize > dd) minsize = dd;
	if (maxsize < dd) maxsize = dd;
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%d] Lp: %lf %lf Lp2: %lf %lf  Pp: %lf %lf Pp2: %lf %lf dx dy: %lf %lf dd: %d\n", camid, 
	//		Lp.x, Lp.y,
	//		Lp2.x, Lp2.y,
	//		Pp.x, Pp.y,
	//		Pp2.x, Pp2.y,
	//		dx, dy, dd);



	piana->pic[camid]->icp.RunBallsizePMin = minsize * sizeratio;
	piana->pic[camid]->icp.RunBallsizePMax = maxsize * sizeratio;

	return 1;
}

// ATTACKCAM
U32 iana_setReadyRunBallsizeMinMax(iana_t *piana, U32 camid, double sizeratio)
{
	U32 res;
	cr_rectL_t rectRunL;
	U32 allowTee;
	U32 allowIron;
	U32 allowPutter;
	double minx, maxx;
	double miny, maxy;
	double sx, sy, ex, ey;


	allowTee 	= piana->allowedarea[IANA_AREA_TEE];
	allowIron 	= piana->allowedarea[IANA_AREA_IRON];
	allowPutter	= piana->allowedarea[IANA_AREA_PUTTER];
	if (allowTee == 0 && allowIron == 0 && allowPutter == 0) {
		minx = -2.0; maxx =  2.0;
		miny = -2.0; maxy =  2.0;
	} else {
		minx = 999; maxx = -999;
		miny = 999; maxy = -999;
		if (allowTee) {
			sx = piana->prectLTee->sx; ex = piana->prectLTee->ex;
			sy = piana->prectLTee->sy; ey = piana->prectLTee->ey;
			if (minx > sx) { minx = sx; } if (maxx < ex) { maxx = ex; }
			if (miny > sy) { miny = sy; } if (maxy < ey) { maxy = ey; }
		}
		if (allowIron) {
			sx = piana->prectLIron->sx; ex = piana->prectLIron->ex;
			sy = piana->prectLIron->sy; ey = piana->prectLIron->ey;
			if (minx > sx) { minx = sx; } if (maxx < ex) { maxx = ex; }
			if (miny > sy) { miny = sy; } if (maxy < ey) { maxy = ey; }
		}
		if (allowPutter) {
			sx = piana->prectLPutter->sx; ex = piana->prectLPutter->ex;
			sy = piana->prectLPutter->sy; ey = piana->prectLPutter->ey;
			if (minx > sx) { minx = sx; } if (maxx < ex) { maxx = ex; }
			if (miny > sy) { miny = sy; } if (maxy < ey) { maxy = ey; }
		}
	}

	rectRunL.sx = minx; rectRunL.ex = maxx; rectRunL.sy = miny; rectRunL.ey = maxy;
	res = iana_setRunBallsizeMinMax(piana, camid, &rectRunL, sizeratio);

	return res;
}

// ATTACKCAM
U32 iana_setRunRunBallsizeMinMax(iana_t *piana, U32 camid, cr_rect_t *prectRunP, double sizeratio)
{
	U32 res;
	double sx, sy, ex, ey;
	double dtmp;
	point_t Pp, Pp2;
	point_t Lp, Lp2;
	cr_rectL_t rectRunL;

	//----
	sx = (double)prectRunP->left; ex = (double)prectRunP->right;
	sy = (double)prectRunP->top; ey = (double)prectRunP->bottom;
	
	if (sx > ex) { dtmp = sx; sx = ex; ex = dtmp; }
	if (sy > ey) { dtmp = sy; sy = ey; ey = dtmp; }

	Pp.x = sx; Pp.y = sy;
	Pp2.x = ex; Pp2.y = ey;

	iana_P2L(piana, camid, &Pp, &Lp,0);
	sx = Lp.x; sy = Lp.y;

	iana_P2L(piana, camid, &Pp2, &Lp2,0);
	ex = Lp2.x; ey = Lp2.y;

	if (sx > ex) { dtmp = sx; sx = ex; ex = dtmp; }
	if (sy > ey) { dtmp = sy; sy = ey; ey = dtmp; }
	rectRunL.sx = sx; rectRunL.ex = ex;
	rectRunL.sy = sy; rectRunL.ey = ey;
	
	res = iana_setRunBallsizeMinMax(piana, camid, &rectRunL, sizeratio);


	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " [%d] rec (%8ld %8ld ~ %8ld %8ld : %10lf %10lf ~ %10lf %10lf) min: %10lf max: %10lf\n", camid,
	//	prectRunP->left, prectRunP->top, prectRunP->right, prectRunP->bottom,
	//	rectRunL.sx, rectRunL.sy, rectRunL.ex, rectRunL.ey,
	//	piana->ic[camid].icp.RunBallsizePMin,
	//	piana->ic[camid].icp.RunBallsizePMax);


	return res;
}


/*!
 ********************************************************************************
 *	@brief      Get angle between shot planes
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @param[in]	pangle
 *   			0=< angle <= 90. (degree)
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2018/04/27
 *******************************************************************************/

U32 iana_shotplane_angle(iana_t *piana, double *pangle)
{
	U32 res;
	double angle;

	iana_cam_t	*pic;
	iana_cam_param_t *picp;

	cr_plane_t *pplane0, *pplane1;		// Shot Plane

	//---
	pic 	= piana->pic[0];
	picp 	= &pic->icp;
	pplane0 = &picp->ShotPlane;

	pic 	= piana->pic[1];
	picp 	= &pic->icp;
	pplane1 = &picp->ShotPlane;

	res = cr_angle_plane_plane(pplane0, pplane1, &angle);

	if (res) {
		*pangle = angle;
	}

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Plane angle, res: %d, angle: %lf\n", res, angle);

	return res;
}



/*!
 ********************************************************************************
 *	@brief      set right / left sided
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @param[in]	right0left1
 *   			0: right,  1: left
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2018/08/24
 *******************************************************************************/

U32 iana_setrightleft(iana_t *piana, U32 right0left1)
{
	U32 res;

	//---
	if (right0left1 == 1) {
		piana->Right0Left1Required = 1;
	} else {
		piana->Right0Left1Required = 0;
	}
	piana->NeedChangeRight0Left1 = 1;

	res = 1;
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Tee position update
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		1: Success, 0: Fail
 *
 *	@author	    yhsuk
 *  @date       2020/09/03
 *******************************************************************************/

U32 iana_teepos_update(iana_t *piana)
{
	U32 res;
	U32 camid;

	cr_plane_t xyplane;
	point_t teeCenterL;

	iana_cam_t		*pic;
	//---

	{ // make xyplane:  Z = 0 plane
		cr_point_t origin;
		cr_vector_t nv;

		origin.x = 0; origin.y = 0; origin.z = 0; 
		nv.x = 0; nv.y = 0; nv.z = 1; 
		cr_plane_p0n(&origin, &nv, &xyplane);
	}

	{ // get center point of Tee area
		cr_rectL_t *pteeRectL;

		pteeRectL = piana->prectLTee;

		teeCenterL.x = (piana->prectLTee->sx + pteeRectL->ex)/2.0;
		teeCenterL.y = (piana->prectLTee->sy + pteeRectL->ey)/2.0;
	}
	for (camid = 0; camid < NUM_CAM; camid++) {
		cr_point_t *pcampos;
		point_t *pteeCenterL;
		point_t *pteeCenterP;
		cr_point_t *pteeTop3d;
		cr_point_t teeTopProjection;
		cr_line_t cam2teeTop;
		point_t *pteeTopProjectionL;
		point_t *pteeTopProjectionP;

		//--
		pic = piana->pic[camid];

		pteeCenterL = &pic->teeCenterL;			// Store tee Center L position and set P position
		memcpy(pteeCenterL, &teeCenterL, sizeof(point_t));
		pteeCenterP = &pic->teeCenterP;
		iana_L2P(piana, camid, pteeCenterL, pteeCenterP, 0);

		pteeTop3d = &pic->teeTop3d;				// set tee top 3d position
		pteeTop3d->x = pteeCenterL->x;
		pteeTop3d->y = pteeCenterL->y;
#define TEEHEIGHT	0.08
		pteeTop3d->z = TEEHEIGHT;

		pcampos = &pic->icp.CamPosL;
		cr_line_p0p1(pteeTop3d, pcampos, &cam2teeTop);		// camera to tee top pos
 
		cr_point_line_plane(&cam2teeTop, &xyplane, &teeTopProjection);	// get tee top projection point

		pteeTopProjectionL = &pic->teeTopProjectionL;	// Store tee top projection L point
		pteeTopProjectionL->x = teeTopProjection.x;
		pteeTopProjectionL->y = teeTopProjection.y;

		pteeTopProjectionP = &pic->teeTopProjectionP;
		iana_L2P(piana, camid, pteeTopProjectionL, pteeTopProjectionP, 0);
	}
	res = 1;
	return res;
}



#if defined (__cplusplus)
}
#endif

// Not used
#if 0

U32 iana_NetworkThrottlingIndex_Set(U32 value)				// 1: success, 0: fail
{
	U32 res;
	// TODO: Add your control notification handler code here
	HKEY hKey;
	LONG lResult;

	lResult = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Multimedia\\SystemProfile"), 0, KEY_ALL_ACCESS|KEY_WOW64_64KEY, &hKey);

	//value = 0xFFFFFFFF;
	//NetworkThrottlingIndex
	if(lResult == ERROR_SUCCESS)
	{
		lResult = RegSetValueEx(hKey, _T("NetworkThrottlingIndex"), 0, REG_DWORD, (LPBYTE) &value, sizeof(DWORD));
		RegCloseKey(hKey);
	}

	if (lResult == ERROR_SUCCESS) {
		res = 1;
	} else {
		res = 0;
	}

	return res;
}

#endif


