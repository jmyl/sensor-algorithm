/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA RGB LED
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2020 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_rgbled.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2020/03/31 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#if defined(_WIN32)
#include <tchar.h>
#include <direct.h>
#endif
#include "cr_common.h"

#include "iana.h"
#if !defined(XU_HW)
#include "xminh.h"
#endif
#include "iana_xbrd.h"
#include "iana_rgbled.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
U32 iana_xminh_rgbled_rgb(iana_t *piana, U32 right0left1, U32 bcam1, U32 bcam2, U16 rgbcode1[], U16 rgbcode2[]);

/*WORD*/
U16 RGB565_ColorCode(				// RGB888 to RGB565 code. by ywchoi.
	U08 /*BYTE*/	red,
	U08 /*BYTE*/	green,
	U08 /*BYTE*/	blue
)
{
	U16 value;
	U16 ur, ug, ub;

#define RED_MAX	0x1f
#define GREEN_MAX	0x3f
#define BLUE_MAX	0x3f

#if 0
	ur = (U16) ((float)RED_MAX   * (float) (red+1) / (float)0xFF);
	if (ur > RED_MAX) ur = RED_MAX;
	ug = (U16) ((float)GREEN_MAX * (float) (green+1) / (float)0xFF);
	if (ug > GREEN_MAX) ug = GREEN_MAX;
	ub = (U16) ((float)BLUE_MAX  * (float) (blue+1) / (float)0xFF);
	if (ub > BLUE_MAX) ub = BLUE_MAX;
#endif

	ur = (U16) ((RED_MAX * (red+1)) / 0xFF);
	if (ur > RED_MAX) ur = RED_MAX;
	ug = (U16) ((GREEN_MAX * (green+1)) / 0xFF);
	if (ug > GREEN_MAX) ug = GREEN_MAX;
	ub = (U16) ((RED_MAX * (blue+1)) / 0xFF);
	if (ub > BLUE_MAX) ub = BLUE_MAX;

/*
	value = (0x1f & red);
	value <<= 6;
	value |= (U16) (0x3f & green);
	value <<= 5;
	value |= (U16)(0x1f & blue);
*/

	value = (0x1f & ur);
	value <<= 6;
	value |= (U16) (0x3f & ug);
	value <<= 5;
	value |= (U16)(0x1f & ub);

	return value;
}

/*!
 ********************************************************************************
 *	@brief      Create iana module
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @param[in]	camcode
 *              camera code (0x00: NULL, 0x01: cam1, 0x02: cam2, 0x03: cam1 and cam2
 *  @param[in]	r_
 *              R code. (8bit only)
 *  @param[in]	g_
 *              G code. (8bit only)
 *  @param[in]	b_
 *              B code. (8bit only)
 *
 *  @return		good result
 *
 *	@author	    yhsuk
 *  @date       2020/0331
 *******************************************************************************/
U32 iana_rgbled_rgb0(iana_t *piana, U32 right0left1, U32 camcode, U08 r_, U08 g_, U08 b_) 
{
	U32 res;
	U32 i;
	iana_cam_t	*pic0, *pic1;
	U32 cam1, cam2;

	//--
	pic0 = piana->pic[0];
	pic1 = piana->pic[1];
	cam1 = camcode & 0x01;
	cam2 = camcode & 0x02;

	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		if (piana->xboard_category == XBOARD_CATEGORY_XMINIHUB) {
			HAND hxbrd;
			U16 rgbcode1[12], rgbcode2[12];

			//--
			if (right0left1 == 0) {
				hxbrd = piana->RS_hxboard;
			} else {
				hxbrd = piana->LS_hxboard;
			}
			if (hxbrd != NULL) {
				iana_xboard_sensor_rgbled_xminh_inactive(piana, hxbrd, camcode);
				for (i = 0; i < 12; i++) {
					if (cam1) {pic0->r[i] = r_; pic0->g[i] = g_; pic0->b[i] = b_;}
					if (cam2) {pic1->r[i] = r_; pic1->g[i] = g_; pic1->b[i] = b_;}
					rgbcode1[i] = RGB565_ColorCode(pic0->r[i], pic0->g[i], pic0->b[i]);
					rgbcode2[i] = RGB565_ColorCode(pic1->r[i], pic1->g[i], pic1->b[i]);
				}

				iana_xminh_rgbled_rgb(piana, right0left1, cam1, cam2, rgbcode1, rgbcode2);
			}
		}
	}

	res = 1;				// GOOD..
	return res;
}



U32 iana_rgbled_rgb1(iana_t *piana, U32 right0left1, U32 camcode, U32 ledid, U08 r_, U08 g_, U08 b_)
{
	U32 res;
	U32 i;
	iana_cam_t	*pic0, *pic1;
	U32 cam1, cam2;

	//----
	pic0 = piana->pic[0];
	pic1 = piana->pic[1];
	cam1 = camcode & 0x01;
	cam2 = camcode & 0x02;
	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		if (piana->xboard_category == XBOARD_CATEGORY_XMINIHUB) {
			HAND hxbrd;
			U16 rgbcode1[12], rgbcode2[12];
			//--
			if (right0left1 == 0) {
				hxbrd = piana->RS_hxboard;
			} else {
				hxbrd = piana->LS_hxboard;
			}
			if (hxbrd != NULL) {
				iana_xboard_sensor_rgbled_xminh_inactive(piana, hxbrd, camcode);
				for (i = 0; i < 12; i++) {
					if (cam1) {
						if (ledid == i) {
							pic0->r[i] = r_; pic0->g[i] = g_; pic0->b[i] = b_;
						}
						rgbcode1[i] = RGB565_ColorCode(pic0->r[i], pic0->g[i], pic0->b[i]);
					}
					if (cam2) {
						if (ledid == i) {
							pic1->r[i] = r_; pic1->g[i] = g_; pic1->b[i] = b_;
						}
						rgbcode2[i] = RGB565_ColorCode(pic1->r[i], pic1->g[i], pic1->b[i]);
					}
				}

				iana_xminh_rgbled_rgb(piana, right0left1, cam1, cam2, rgbcode1, rgbcode2);
			}
		}
	}

	res = 1;				// GOOD..
	return res;
}

U32 iana_rgbled_rgb2(iana_t *piana, U32 right0left1, U32 camcode, U08 r_[12], U08 g_[12], U08 b_[12])
{
	U32 res;
	iana_cam_t	*pic0, *pic1;
	U32 i;
	U32 cam1, cam2;

	//--
	pic0 = piana->pic[0];
	pic1 = piana->pic[1];
	cam1 = camcode & 0x01;
	cam2 = camcode & 0x02;
	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		if (piana->xboard_category == XBOARD_CATEGORY_XMINIHUB) {
			HAND hxbrd;
			U16 rgbcode1[12], rgbcode2[12];
			//--
			if (right0left1 == 0) {
				hxbrd = piana->RS_hxboard;
			} else {
				hxbrd = piana->LS_hxboard;
			}
			if (hxbrd != NULL) {
				iana_xboard_sensor_rgbled_xminh_inactive(piana, hxbrd, camcode);
				for (i = 0; i < 12; i++) {
					if (cam1) {
						pic0->r[i] = r_[i]; pic0->g[i] = g_[i]; pic0->b[i] = b_[i];
						rgbcode1[i] = RGB565_ColorCode(pic0->r[i], pic0->g[i], pic0->b[i]);
					}
					if (cam2) {
						pic1->r[i] = r_[i]; pic1->g[i] = g_[i]; pic1->b[i] = b_[i];
						rgbcode2[i] = RGB565_ColorCode(pic1->r[i], pic1->g[i], pic1->b[i]);
					}
				}

				iana_xminh_rgbled_rgb(piana, right0left1, cam1, cam2, rgbcode1, rgbcode2);
			}
		}
	}

	res = 1;				// GOOD..
	return res;
}


U32 iana_rgbled_rgb3(iana_t *piana, U32 right0left1, U32 camcode, U32 tablecode, U32 itercount, U32 direction)
{
	U32 res;
	HAND hxbrd;

	//--
	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		if (piana->xboard_category == XBOARD_CATEGORY_XMINIHUB) {
			if (right0left1 == 0) {
				hxbrd = piana->RS_hxboard;
			} else {
				hxbrd = piana->LS_hxboard;
			}
			if (hxbrd != NULL) {
				iana_xboard_sensor_rgbled_xminh(piana, hxbrd, camcode, tablecode, itercount, direction);
			}
		}
	}

	res = 1;				// GOOD..
	return res;
}


U32 iana_rgbled_rgb4(iana_t *piana, U32 right0left1, U32 camcode, U32 rotationtype, U32 gradianttype, U32 animationspeed, U32 animationmode)
{
	U32 res;
	iana_xboard_t *	pxbrd;
#if !defined(XU_HW)
	//--
	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		if (piana->xboard_category == XBOARD_CATEGORY_XMINIHUB) {
			RGB_ANIMATION_CONTROL rac;

			if (right0left1 == 0) {
				pxbrd = (iana_xboard_t *)piana->RS_hxboard;
			} else {
				pxbrd = (iana_xboard_t *)piana->LS_hxboard;
			}
			if (pxbrd != NULL) {
				memset(&rac, 0, sizeof(RGB_ANIMATION_CONTROL));
				rac.RotationType	= rotationtype		& 0x03;
				rac.GradientType	= gradianttype		& 0x03;
				rac.AnimationSpeed	= animationspeed	& 0x0F;
				rac.reserved		= (0 & 0x7F);
				rac.AnimationMode	= animationmode		& 0x01;
				if (camcode & 0x01) {
					XMINH_RgbAnimationControl(pxbrd->hXbrd, XMINH_SLAVE_ID_CAM1, &rac);
				}
				if (camcode & 0x02) {
					XMINH_RgbAnimationControl(pxbrd->hXbrd, XMINH_SLAVE_ID_CAM2, &rac);
				}
			}
		}
	}
#endif
	res = 1;				// GOOD..
	return res;
}


U32 iana_xminh_rgbled_rgb(iana_t *piana, U32 right0left1, U32 bcam1, U32 bcam2, 
		U16 rgbcode1[], U16 rgbcode2[])
{
	U32 res;
	iana_xboard_t *	pxbrd;
#if !defined(XU_HW)
	//----
	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		if (piana->xboard_category == XBOARD_CATEGORY_XMINIHUB) {
			if (right0left1 == 0) {
				pxbrd = (iana_xboard_t *)piana->RS_hxboard;
			} else {
				pxbrd = (iana_xboard_t *)piana->LS_hxboard;
			}
			if (pxbrd != NULL) {
				cr_mutex_wait(pxbrd->hmutex, INFINITE);

				if (bcam1) {
					XMINH_RGBLED_Set(pxbrd->hXbrd, XMINH_SLAVE_ID_CAM1, rgbcode1);		// CAM1 (centercam)
				}

				if (bcam2) {
					XMINH_RGBLED_Set(pxbrd->hXbrd, XMINH_SLAVE_ID_CAM2, rgbcode2);		// CAM2 (centercam)
				}
				cr_mutex_release(pxbrd->hmutex);
			}
		}
	}
#endif
	res = 1;				// GOOD..
	return res;
}


#if defined (__cplusplus)
}
#endif


