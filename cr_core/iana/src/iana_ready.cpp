/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA check ready
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_checkready.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2016/02/21 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <math.h>

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"
#include "cr_nanoxml.h"

#include "scamif.h"
//#include "scamif_main.h"
#include "scamif_buffer.h"

#include "iana.h"
#include "iana_work.h"
#include "iana_implement.h"
#include "iana_tool.h"
#include "iana_coordinate.h"
#include "iana_cam.h"
#include "iana_shot.h"
//#include "iana_cam_implement.h"
#include "iana_checkready.h"
#include "iana_balldetect.h"
#include "iana_ready.h"
#include "iana_work.h"

#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#else
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#endif


/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

#define READY_MAXTH	40
#define READY_MINTH	20
#define READY_MULTV	3
#define READY_EXPN	1

#define HALFWINSIZE_CHECKREADY	16
//#define HALFWINSIZE_CHECKREADY	4
//#define HALFWINSIZE_CHECKREADY	32

#define THSTDMULT_		1.5
#if defined(V2000_TEST)
//----------------------------
#undef  READY_MULTV
#undef READY_EXPN
#undef THSTDMULT_
#undef HALFWINSIZE_CHECKREADY

//#define READY_MULTV		2
#define READY_MULTV		1
#define READY_EXPN		0
//#define THSTDMULT_		0.3
#define THSTDMULT_		1.0
#define HALFWINSIZE_CHECKREADY	4
//----------------------------
#else
////////////////////////#error shit7

#undef  READY_MULTV
#undef READY_EXPN
#undef THSTDMULT_
#undef HALFWINSIZE_CHECKREADY

//#define READY_MULTV		2
#define READY_MULTV		1
#define READY_EXPN		0
//#define THSTDMULT_		0.3
#define THSTDMULT_		1.0
#define HALFWINSIZE_CHECKREADY	4
//   TEST0407
#undef THSTDMULT_
//#define THSTDMULT_		2.0
#define THSTDMULT_		0.7

#endif


#if defined(_WIN32)
#pragma warning(disable:4456)
#endif
#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
I32 checkreadyposition(iana_t *piana, U32 mercy);

/*!
 ********************************************************************************
 *	@brief      CAM ball ready
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		Terminate result. 
 *
 *	@author	    yhsuk
 *  @date       2016/02/23
 *******************************************************************************/
I32 iana_ready(iana_t *piana)	
{
#ifdef IPP_SUPPORT // #else /* IPP_SUPPORT */ 검색해서 해당 내용 확인.
	I32 res;
	camif_buffer_info_t *pb;
	iana_cam_t	*pic;
	camimageinfo_t	*pcii;
	U32 multitude;
	U64 period;
	U64 periodm;
	U32 skip, syncdiv;
  U64 ts64;
	U64 ts64Ready;
	U64 ts64Mt;				// ts64 for CheckReady, Multiple
	double ts64d;
	I32 m;

	I32 balldisappeared;

	U08 *buf;
	U08 *procimg;
	U32 rindex;
	U32 camidReady;
	U32 camidOther;
	U32 camid;

	IppiSize ssize;
	IppiRect sroi, droi;
	int sstep, dstep;
	IppStatus status;
	Ipp8u 	*resizebuf;
	int 	bufsize;

    cr_rect_t rectAreaM;

	iana_ballcandidate_t bc[MAXCANDIDATE];
	U32 bcount;

	double	xfact, yfact; 
	cr_rect_t rectArea;
	cr_rectL_t rectLArea;
	cr_rect_t rectReadyArea;
	cr_rectL_t rectLReadyArea;
	scamif_t *pscamif;

	double cutmean_ready;
	U32 ironAreaCheck;
	U32 ironAreaCheckActive;
	U32 ironAreaBallExist;
	//--------------

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with hiana: %p\n",  piana);
	
#define CUTMEAN_READY	0.5	
#define CUTMEAN_READY_EYEXO	0.2			// for EYEXO

	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		cutmean_ready = CUTMEAN_READY_EYEXO;
	} else {
		cutmean_ready = CUTMEAN_READY;
	}
//	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---------------------\n");

	pscamif = (scamif_t *) piana->hscamif;


	ironAreaCheckActive = 0;

//#define IRONAREACHECK_ENABLE					// NOT YET.  20201207

#if defined(IRONAREACHECK_ENABLE)
	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		piana->ironAreaCheckCount++;
//#define IRONAREACHECKCOUNT	10
#define IRONAREACHECKCOUNT	5
		if (piana->ironAreaCheckCount % IRONAREACHECKCOUNT == 0) {
			ironAreaCheckActive = 1;
		}
	}
#endif
	//{
	//	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---------------------\n");
	//	for (camid = 0; camid < 3; camid++) {
	//		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "camid[%d]  L(%10lf %10lf)  P(%10lf %10lf)\n", camid,
	//			piana->ic[camid].icp.L.x,
	//			piana->ic[camid].icp.L.y,
	//			piana->ic[camid].icp.P.x,
	//			piana->ic[camid].icp.P.y);
	//	}
	//	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---------------------\n");
	//}

	ironAreaCheck = 0;
	ironAreaBallExist = 0;

	// 1) get image
	camidReady = piana->camidCheckReady;
	camidOther = piana->camidOther;
	pic   =piana->pic[camidReady];

	buf = NULL;
	pcii = NULL;
	pb = NULL;
	rindex = 0;
	//__debugbreak();
	if (piana->opmode == IANA_OPMODE_FILE) {
		rindex = piana->info_startindex[camidReady];			// rindex
		res = scamif_imagebuf_randomaccess(piana->hscamif, camidReady, NORMALBULK_NORMAL, rindex, &pb, &buf);
		pcii = &pb->cii;

		if (piana->shotarea		== IANA_AREA_PUTTER) {
			piana->processarea	 = IANA_AREA_PUTTER;
		}

		if (res != 0 && buf != NULL) {
			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)(
					(char *)buf, pcii->width, pcii->height, 0, 1, 2, 3, piana->imguserparam);
			}
		}
	} else {

		res = scamif_imagebuf_read(piana->hscamif, camidReady, NORMALBULK_NORMAL, &pb, &buf, &rindex);	
		
		if (res != 0) {
			pcii = &pb->cii;
		}

	}
	if (res == 0) {	// No image data yet.
		goto func_exit;
	}
	piana->lastrindex[camidReady] = rindex;

	//period = (1000_000_000LL) / (U64) framerate;			// nsec
	period = TSSCALE / (U64) pcii->framerate;			// nsec
	//period = (1000000) /  pcii->pciiframerate;			// usec
	multitude 	= pcii->multitude;
	if (multitude == 0) {
		multitude = 1;
	}
	skip 	= pcii->skip;
	if (skip == 0) {
		skip = 1;
	}
	syncdiv = pcii->syncdiv;
	//if (syncdiv == 0 || camidReady == 0) {				// master: always 1
	//	syncdiv = 1;
	//}

	if (syncdiv == 0 || camidReady == (U32)pscamif->mastercamid) {				// master: always 1
		syncdiv = 1;
	}

	//periodm = period * multitude * skip * syncdiv;
	periodm = period * skip * syncdiv;

	ts64 = MAKEU64(pb->ts_h,pb->ts_l);
	//ts64d = ((ts64 / 1000000) / 1000.0);
	ts64d = ts64 / TSSCALE_D;

	ts64Ready = ts64;

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%d: %d> ts64: %016llx (%lf)\n", camidReady, rindex, ts64, ts64d);

	
	//--
	piana->runindexcount++;
	//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "runindexcount: %d\n", piana->runindexcount);
//	if (piana->runindexcount < RUNINDEX_MERCY) {
//		res = 1;
//		goto func_exit;
//	}
//#define XYFACT		0.5
#define XYFACT		1
/////////////#define XYFACT		0.5

#define XYFACT_CONSOLE1		0.5
//#define XYFACT_CONSOLE1		0.4
//#define XYFACT_CONSOLE1		1
	if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
		xfact 		= XYFACT_CONSOLE1;
		yfact 		= (XYFACT_CONSOLE1 * multitude);
	} else {
		xfact 		= XYFACT;
		yfact 		= (XYFACT * multitude);
	}

	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
//#define XYFACT_25MMTEST		0.70710678					// SQRT(2)/2
#define XYFACT_25MMTEST		0.5
		xfact 		= XYFACT_25MMTEST;
		yfact 		= (XYFACT_25MMTEST * multitude);
	}

	//
	//xfact 		= 1.0;
	//yfact 		= 1.0;

	procimg = pic->pimgFull;

	{
//		double	xshift, yshift; 
		point_t Lp;
		point_t Pp;
		

		//--
		Lp.x = pic->icp.L.x;				// Center..
		Lp.y = pic->icp.L.y;

		iana_L2P(piana, camidReady, &Lp, &Pp, 0);
		//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "RD P(%lf, %lf)-L(%lf, %lf)\n",
		//		Pp.x, Pp.y, Lp.x, Lp.y);

//#define ADDX	32		
//#define ADDY	32		
#define ADDX	48
#define ADDY	48
//#define ADDX	64
//#define ADDY	64
		rectArea.left 	= (long) (Pp.x - ADDX+1);
		rectArea.right 	= (long) (Pp.x + ADDX);
		rectArea.top 	= (long) (Pp.y - ADDY+1);
		rectArea.bottom = (long) (Pp.y + ADDY);


		rectLArea.sx 	= (double) (Pp.x - ADDX+1);
		rectLArea.ex 	= (double) (Pp.x + ADDX	 );
		rectLArea.sy 	= (double) (Pp.y - ADDY+1);
		rectLArea.ey 	= (double) (Pp.y + ADDY  );

		{
			long l0;
			l0 = rectArea.top;
			l0 = ((l0 + 3) / 4) * 4;
			rectArea.top = l0;
			l0 = rectArea.bottom;
			l0 = ((l0 + 3) / 4) * 4;
			rectArea.bottom = l0;
		}

		/*
		rectArea.left 	= (long) (Pp.x - 5*ADDX+1);
		rectArea.right 	= (long) (Pp.x + ADDX);
		rectArea.top 	= (long) (Pp.y - ADDY+1);
		rectArea.bottom = (long) (Pp.y + ADDY);
		*/
		/*
		rectArea.left 	= 0;
		rectArea.right 	= 480-1;
		rectArea.top 	= 0;
		rectArea.bottom = 240-1;
		*/
	}

	{
		memcpy(&rectReadyArea, &pic->icp.rectAreaExt, sizeof(cr_rect_t));

		if (rectReadyArea.left < (long)pic->offset_x) {
			rectReadyArea.left = pic->offset_x;
		}

		if (rectReadyArea.right > (long) (pic->offset_x + pcii->width - 1)) {
			rectReadyArea.right = pic->offset_x + pcii->width - 1;
		}

		if (rectReadyArea.top < (long)pic->offset_y) {
			rectReadyArea.top = pic->offset_y;
		}

		if (rectReadyArea.bottom > (long) (pic->offset_y + pcii->height - 1)) {
			rectReadyArea.bottom = pic->offset_y + pcii->height - 1;
		}

		rectLReadyArea.sx = rectReadyArea.left;
		rectLReadyArea.ex = rectReadyArea.right;
		rectLReadyArea.sy = rectReadyArea.top;
		rectLReadyArea.ey = rectReadyArea.bottom;
	}

	resizebuf = NULL;
	balldisappeared = 0;

	//-------------------------------------------- For each Multitude
//	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "rectArea T~B: %4d %4d offset_y: %4d\n", 
//			rectArea.top, rectArea.bottom, pic->offset_y);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d #1\n", __LINE__);
	for (m = 0; m < (I32) multitude; m++) {
		double mindist;
		double mindistLx, mindistLy;
		int mindistIndex;
		int doitmore = 0;

		//--
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d #1 m: %d\n", __LINE__, m);
		//ts64Mt = ts64 + (periodm * (multitude - m -1));
		ts64Mt = ts64 - (periodm * (multitude - m -1));
		/*
		rectAreaM.left 	= (long) (rectArea.left 	- pic->offset_x);
		rectAreaM.right = (long) (rectArea.right 	- pic->offset_x);
		*/
		ironAreaCheck = 0;
		if (piana->camsensor_category == CAMSENSOR_P3V2)
		{
			if (ironAreaCheckActive) {
				if (m == 0) {
					if ( (
								//(piana->shotarea == IANA_AREA_TEE) || 
								iana_isInTee(piana, &pic->icp.b3d))
							&& piana->allowedarea[IANA_AREA_IRON]) {			// Ready: in TEE area. but Iron area is allowed.
						ironAreaCheck = 1;
					}
				}
			}
		}

		if (ironAreaCheck) {
			rectAreaM.left = (long)(rectLReadyArea.sx - pic->offset_x);
			rectAreaM.right = (long)(rectLReadyArea.ex - pic->offset_x);
			rectAreaM.top = (long)((rectReadyArea.top - pic->offset_y) / multitude + ((pcii->height *m) / multitude)); //  + m * 2;
			rectAreaM.bottom = (long)((rectReadyArea.bottom - pic->offset_y) / multitude + ((pcii->height *m) / multitude)); //  + m * 2;
		} else {
			rectAreaM.left = (long)(rectLArea.sx - pic->offset_x);
			rectAreaM.right = (long)(rectLArea.ex - pic->offset_x);
			rectAreaM.top = (long)((rectArea.top - pic->offset_y) / multitude + ((pcii->height *m) / multitude)); //  + m * 2;
			rectAreaM.bottom = (long)((rectArea.bottom - pic->offset_y) / multitude + ((pcii->height *m) / multitude)); //  + m * 2;
		}

		ssize.width 	= pcii->width;
		ssize.height 	= pcii->height;

		//		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "W: %d, H: %d\n", ssize.width, ssize.height);
		sroi.x 		= 0;
		sroi.y 		= 0;
		sroi.width 	= rectAreaM.right - rectAreaM.left + 1;
		sroi.height	= rectAreaM.bottom   - rectAreaM.top  + 1;

		if (
			( pcii->width < (U32)sroi.width  || pcii->height < (U32)sroi.height)
			|| (pcii->width > FULLWIDTH || pcii->height > FULLHEIGHT)
			|| (sroi.width  > FULLWIDTH || sroi.height > FULLHEIGHT)
			)
		{
			if (resizebuf != NULL) {
				free(resizebuf);
			}
			resizebuf = NULL;
			res = 0;
			piana->runstate = IANA_RUN_TRANSITORY;
			goto func_exit;
		
		}

		droi.x 		= 0;
		droi.y 		= 0;
		droi.width 	= (int) (sroi.width * xfact + 0.001);
		droi.height	= (int) (sroi.height * yfact + 0.001);

		sstep 		= pcii->width;
		dstep		= droi.width;


		/*
		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%d,%d>, (%4d: %4d ~ %4d,  %10lf ~ %10lf)\n", 
				camidReady,m, rectAreaM.top, rectAreaM.bottom, rectAreaM.bottom - rectAreaM.top,
				rectLArea.sy, rectLArea.ey);

		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "droi (%d , %d)\n", 
				droi.width, droi.height);
		*/

		if (resizebuf == NULL) {
			ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_CUBIC, &bufsize);
			resizebuf = (U08 *)malloc(bufsize);
		}

		__try {
			status = ippiResizeSqrPixel_8u_C1R(
				//(Ipp8u *)buf + rectAreaM.left + rectAreaM.top * ssize.width,
				(Ipp8u *)buf + rectAreaM.left + rectAreaM.top * ssize.width,
				ssize,
				sstep,
				sroi,
				//
				(Ipp8u*) procimg,
				dstep,
				droi,
				//
				xfact,
				yfact,
				0,		// xshift,
				0,		// yshift,
				IPPI_INTER_CUBIC,
				resizebuf
				);
		} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
				|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
				|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
				|| GetExceptionCode()== EXCEPTION_FLT_DIVIDE_BY_ZERO
				){
			if (resizebuf != NULL) {
				free(resizebuf);
			}
			resizebuf = NULL;
			res = 0;
			piana->runstate = IANA_RUN_TRANSITORY;
			goto func_exit;
		}

		   if (piana->himgfunc) {
		   ((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)(
		   (char *)procimg, 
		   droi.width, droi.height, 3 /*m*/, 1, 2, 3, piana->imguserparam);
		   }
		bc;
		bcount;
		mindist;
		mindistIndex;

		// 3) Get Ball positions, if any.
		mindist = 1.0e10;
		mindistIndex = -1;
		mindistLx = pic->icp.L.x;
		mindistLy = pic->icp.L.y;
		{
			double cermax;
			double crmin;
			double crmax;
			double crminratio;
			double crmaxratio;
			U32 i;

			iana_setReadyRunBallsizeMinMax(piana, camidReady, xfact);

			crmaxratio = IANA_BALLDETECT_GetMaxCircleRatio(piana->camsensor_category, camidReady);
			crminratio = IANA_BALLDETECT_GetMinCircleRatio(piana->camsensor_category, camidReady);	

			crmin = (piana->pic[camidReady]->icp.RunBallsizePMin / 2.0) * crminratio;
			crmax = (piana->pic[camidReady]->icp.RunBallsizePMax / 2.0) * crmaxratio;

			//cermax = piana->ic[camidReady].icp.CerBallexist;
			cermax = pic->icp.CerBallexist;


			memset(bc, 0, sizeof(bc));
			bcount = 0;

#define READY_MULTV_Z	3
#define READY_EXPN_Z	1
#define THSTDMULT_Z	1.0
#define READY_MULTV_PUTTER_Z	1
#define READY_EXPN_PUTTER_Z		0
#define THSTMULT_COUNT	5
#define THSTMULT_COUNT_PUTTER	5

			if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {

				U32 thstmult_count;
				double thstmult_IRON[THSTMULT_COUNT] = {2.0, 1.3, 0.9, 0.4, 0.2};
				double thstmult_TEE[THSTMULT_COUNT] = {0.9, 0.4, 0.2, 1.3, 2.0};
				double thstmult_PUTTER[THSTMULT_COUNT_PUTTER] = {1.5, 1.0, 0.5, 0.3, 0.15}; 

				if (piana->camsensor_category == CAMSENSOR_EYEXO) {
					//double thstmult_IRON[THSTMULT_COUNT] = {1.5, 1.3, 0.9, 0.4, 0.2};
					//double thstmult_TEE[THSTMULT_COUNT] = {0.9, 0.4, 0.2, 1.3, 2.0};
					thstmult_IRON[0] = 1.5;
					thstmult_IRON[1] = 1.3;
					thstmult_IRON[2] = 0.9;
					thstmult_IRON[3] = 0.4;
					thstmult_IRON[4] = 0.2;
					thstmult_TEE[0] = 0.9;
					thstmult_TEE[1] = 0.4;
					thstmult_TEE[2] = 0.2;
					thstmult_TEE[3] = 1.3;
					thstmult_TEE[4] = 2.0;
				}

				if (piana->processarea == IANA_AREA_PUTTER) {
					thstmult_count = THSTMULT_COUNT_PUTTER;
				} else {
					thstmult_count = THSTMULT_COUNT;
				}
				for (i = 0; i < thstmult_count; i++) {
					double thstm;
					int ready_multv_z;
					int ready_expn_z;
					if (piana->processarea == IANA_AREA_PUTTER) {
						thstm = thstmult_PUTTER[i];
						ready_multv_z = READY_MULTV_PUTTER_Z;
						ready_expn_z  = READY_EXPN_PUTTER_Z;
					} else if (piana->processarea == IANA_AREA_IRON) {
						thstm = thstmult_IRON[i];
						ready_multv_z = READY_MULTV_Z;
						ready_expn_z  = READY_EXPN_Z;
					} else {			// TEE
						thstm = thstmult_TEE[i];
						ready_multv_z = READY_MULTV_Z;
						ready_expn_z  = READY_EXPN_Z;
					}

					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d #1-1 Before Adapt() m: %d\n", __LINE__, m);

					iana_getballcandidatorAdapt
						(piana, camidReady, procimg, droi.width, droi.height, droi.width, &bc[0], &bcount,
						 HALFWINSIZE_CHECKREADY,
						 ready_multv_z, ready_expn_z,
						 thstm, 			// thstmult[i],
						 cermax
						 ,cutmean_ready	// 20190420, yhsuk
						 );

					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d #1-2 After Adapt() bcount:%d with %lfm: %d\n", __LINE__,
					//		bcount, thstm,
					//		m);
//					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%d bc: %d] thstm: %lf\n", camidReady, i, bcount, thstm);
					if (bcount > 0) {
						break;
					}
				}
			} else {
//				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d #1-1 before Adapt() m: %d\n", __LINE__, m);

				iana_getballcandidatorAdapt
					(piana, camidReady, procimg, droi.width, droi.height, droi.width, &bc[0], &bcount,
					 HALFWINSIZE_CHECKREADY,
					 READY_MULTV, READY_EXPN,
					 THSTDMULT_,
					 cermax
					 ,cutmean_ready	// 20190420, yhsuk
					 );

//				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d #1-2 After Adapt() m: %d\n", __LINE__, m);
			}
#define USECANNY
#if defined(USECANNY)
			if (piana->camsensor_category != CAMSENSOR_CONSOLE1 && piana->camsensor_category != CAMSENSOR_Z3&&piana->camsensor_category != CAMSENSOR_EYEXO)   
			{
				if (bcount == 0) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "before CANNY.\n");
					iana_getballcandidatorCanny
						(piana, camidReady, procimg, 
						 droi.width, droi.height, 
						 droi.width, 
						 &bc[0], &bcount,
						 100,			// fake
						 10,			// fake
						 READY_MULTV, READY_EXPN,
						 cermax);

					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "After CANNY.\n");
				}
			}
#endif

			// refine ball candidate
			if (bcount > 0) {
				double mindistmean, mindiststddev, mindistmeanH, mindiststddevH;

				mindistmean		= 0;
				mindiststddev	= 0;
				mindistmeanH	= 0;
				mindiststddevH	= 0;
				for (i = 0; i < bcount; i++) {
#if defined (USE_IANA_BALL)
					if (bc[i].cexist == IANA_BALL_EXIST) 
#else
						if (bc[i].cexist)
#endif
						{
							if (bc[i].cer > cermax || bc[i].cr < crmin || bc[i].cr > crmax) {	// Filtering..
#if defined (USE_IANA_BALL)
								bc[i].cexist = IANA_BALL_EMPTY;
#else
								bc[i].cexist = 0;
#endif
								continue;
							} else {
								point_t Pp, Lp;
								double dist;

								//-- Converse to Pixel point.
								/*
								   cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "x: %lf -> %lf -> %lf -> %lf\n",
								   bc[i].cx,  
								   (bc[i].cx / xfact),
								   (bc[i].cx / xfact) + rectArea.left,
								   (bc[i].cx / xfact) + rectArea.left + pic->offset_x);
								   */
								if (ironAreaCheck) {
									bc[i].P.x = (bc[i].P.x / xfact) + (rectReadyArea.left - pic->offset_x) + pic->offset_x;
								} else {
									bc[i].P.x = (bc[i].P.x / xfact) + (rectArea.left - pic->offset_x) + pic->offset_x;
								}
#define MULTITUDE_FRACTION

								if (ironAreaCheck) {
#if defined(MULTITUDE_FRACTION)
									bc[i].P.y = (((bc[i].P.y+0.5) * multitude )/ yfact) + (rectReadyArea.top - pic->offset_y)  + pic->offset_y;
#else
									bc[i].P.y = (bc[i].P.y / (yfact/multitude)) + (rectReadyArea.top - pic->offset_y)  + pic->offset_y;
#endif
								} else {
#if defined(MULTITUDE_FRACTION)
								bc[i].P.y = (((bc[i].P.y+0.5) * multitude )/ yfact) + (rectArea.top - pic->offset_y)  + pic->offset_y;
#else
								bc[i].P.y = (bc[i].P.y / (yfact/multitude)) + (rectArea.top - pic->offset_y)  + pic->offset_y;
#endif
								}
								bc[i].cr /= xfact;

								//-- Converse to Local point.
								Pp.x = bc[i].P.x;
								Pp.y = bc[i].P.y;
								iana_P2L(piana, camidReady, &Pp, &Lp, 0);
								bc[i].L.x = Lp.x;
								bc[i].L.y = Lp.y;
								dist = DDIST(pic->icp.L.x, pic->icp.L.y, bc[i].L.x, bc[i].L.y);
								if (dist < mindist) {
									mindist = dist;
									mindistIndex = i;
									mindistLx = bc[i].L.x;
									mindistLy = bc[i].L.y;

									mindistmean = bc[i].mean;
									mindiststddev = bc[i].stddev;
									mindistmeanH = bc[i].meanH;
									mindiststddevH = bc[i].stddevH;
								}

								if (ironAreaCheck) {
									double dist2ReadyBall;
									double dx, dy;
									//double dist2Tee;

									dx = pic->icp.L.x - Lp.x;
									dy = pic->icp.L.y - Lp.y;
									dist2ReadyBall = sqrt(dx*dx + dy*dy);
#define DIST2TEE_MAX	0.05
									if (dist2ReadyBall > DIST2TEE_MAX || !iana_isInTeeL(piana, Lp.x, Lp.y)) {
										ironAreaBallExist = iana_isInIronL(piana, Lp.x, Lp.y);
									}
									cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" ironAreaBallExist: %d  (%lf %lf)\n", ironAreaBallExist, Lp.x, Lp.y);
								}
							}
						} 		// if (bc[i].cexist) 

					if (ironAreaBallExist) {
						break;
					}
				} 			// for (i = 0; i < bcount; i++) 

				if (mindistIndex >= 0) {
					pic->icp.mean 	= mindistmean;
					pic->icp.stddev = mindiststddev;
					pic->icp.meanH 	= mindistmeanH;
					pic->icp.stddevH = mindiststddevH;
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] mindistmean: %lf\n", camidReady, mindistmean);
				}
			} 				// if (bcount > 0) 
		} 					// 3) Get Ball positions, if any.

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d #2 m: %d\n", __LINE__, m);
		// Check ball moving or disappear..
		//#define MAXMINDIST	0.01
		//#define MAXMINDIST	0.05
#define MAXMINDIST		0.02
//#define MAXMINDIST_TEE	0.02
#if defined(V2000_TEST)
#define MAXMINDIST_TEE	0.03
#define MAXMINDIST_F	0.005
#else
#define MAXMINDIST_TEE	0.01
#define MAXMINDIST_F	0.005
#endif
		//#define MAXMINDIST	0.05
		//#define MAXMINDIST	0.1
		//#define RUNINDEX_MERCY	5
#define RUNINDEX_MERCY	10
		//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ts64Mt: %016llx,  rindex: %d\n", ts64Mt, rindex);

		pic->rindexshot = rindex;
		pic->mshot 		= m;
		pic->ts64shot = ts64Mt;
		pic->ts64shot0 = ts64Mt;

		if (ironAreaBallExist) {
			break;
		}

		{				// Check ball disappear..
			U32 isInTeeflag;
			U32 goodball;
			double maxmindist;

			//----
			balldisappeared = 0;

			isInTeeflag = iana_isInTee(piana, &pic->icp.b3d);

			if (piana->opmode == IANA_OPMODE_FILE) {
				maxmindist = MAXMINDIST_F;
			} else {
				if (isInTeeflag) {
					maxmindist = MAXMINDIST_TEE;
				} else {
					maxmindist = MAXMINDIST;
				}
			}

			if (mindistIndex < 0 || mindist > maxmindist) {
				U32 unindexcount_mercy;

				if (piana->opmode == IANA_OPMODE_FILE) {
					unindexcount_mercy = 0;
				} else {
					unindexcount_mercy = RUNINDEX_MERCY;
				}

				if (piana->runindexcount >= unindexcount_mercy) { // Ball disappear!~!
					balldisappeared = 1;
					doitmore = 1;
					cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ts64: %016llx (%lf), pic->ts64shot (Mt): %016llx(%lf), pic->rindexshot: %d  pic->m: %d\n", 
							ts64, 
							(double)ts64/TSSCALE_D,
							pic->ts64shot,
							(double)pic->ts64shot/TSSCALE_D,
							pic->rindexshot, pic->mshot
							);
				}
				goodball = 0;
			} else {
				if (piana->readymercycount != 0) {
					//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "readymercycount: %d to ZERO\n", piana->readymercycount);
					//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mindistIndex: %d L: %10lf %10lf cr: %10lf\n", 
					//		mindistIndex,
					//		bc[mindistIndex].L.x,
					//		bc[mindistIndex].L.y,
					//		bc[mindistIndex].cr
					//		);
					//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mindist: %lf     (vs %lf)\n", mindist, maxmindist);
				}
				piana->readymercycount = 0;
				goodball = 1;
				piana->goodreadytick = cr_gettickcount();
			}


			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d #3 m: %d\n", __LINE__, m);
			{
				U32 w, h;
				char *pref;
				char *pimg;
				double dbr;
				I32 ioffset;

				double dsum;
				int value;
				int icount;
				//U32 i, j;
				I32 i, j;

				//---
				icount;
				//dbr = pic->icp.cr;
				dbr = pic->icp.cr/xfact;
				pref = (char *)&pic->BallImg[0];
				pimg = (char *)procimg;

				//w = (int) ((dbr+0.5) * 2);
				//h = (int) ((dbr+0.5) * 2);
#define REFWIDTHMULT 	2
#define REFHEIGHTMULT 	2
				//w = w * REFWIDTHMULT;
				//h = h * REFHEIGHTMULT;

				w = pic->biw;
				h = pic->bih;

				w = (w + 3) & 0xFFFFFFFC;			// 4byte align
				h = (h + 3) & 0xFFFFFFFC;			// 4byte align
#ifdef IPP_SUPPORT
				ioffset = (ADDY-(I32)dbr) * droi.width + (ADDX-(I32)dbr);
#else
                ioffset = (ADDY-(I32)dbr) * widthResize + (ADDX-(I32)dbr);
#endif
				if (ioffset < 0) ioffset = 0;




				// Check reference..			TODO:  check dsum of reference image and screening... 
				//				dsum = 0;
				//				for (i = 0; i < h; i++) {
				//					for (j = 0; j < w; j++) {
				//						value= (int)((U08)*(pref + i*w+j));
				//						dsum = dsum + value * value;
				//					}
				//				}
				//				dsum = sqrt(dsum / (h * w));
				//				cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ref dsum: %lf\n", dsum);

				if (piana->opmode != IANA_OPMODE_FILE) {
					I32 ox, oy;
					I32 iw, ih;
					//ox = (I32)((w - dbr) / 2);
					//oy = (I32)((h - dbr) / 2);
					//iw = (I32) dbr;
					//ih = (I32) dbr;
					ox = (I32)(w/2 - dbr);
					oy = (I32)(h/2 - dbr);
					iw = (I32) dbr*2;
					ih = (I32) dbr*2;
					if (ox < 0) { ox = (I32)0; }
					if (oy < 0) { oy = (I32)0; }
					if (balldisappeared) {
						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] w, h: (%d, %d),  dbr: %lf, ioffset; %d\n", camidReady, w, h, dbr, ioffset);

						// Check reference..
						dsum = 0;
//						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "calc dsum1\n");

						for (i = 0; i < ih; i++) {
							for (j = 0; j < iw; j++) {
								value= (int)((U08)*(pref + (i+oy)*w+(j+ox)));
								dsum = dsum + value * value;
							}
						}
						dsum = sqrt(dsum / (ih * iw));

						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "  dsum = %lf\n", dsum);
						{
							FILE *fp;
							U32 mode;
							U32 revivevalue;
							double revivevalue_ref;
							iana_cam_t	*pic;
							U32 isIron;

							//-----
							pic 	= piana->pic[camidReady];
							
#define DSUM_REVIVE	40
#define DSUM_VALID_REF	40
#define DSUM_VALID_REF_TEE	50
#define DSUM_REVIVE_TEE		20

#define DSUM_REVIVE_Z3			20			// for Z3			
#define DSUM_VALID_REF_Z3		30		// for Z3
#define DSUM_VALID_REF_TEE_Z3	50
#define DSUM_REVIVE_TEE_Z3		15

#define DSUM_REVIVE_EYEXO			25	
#define DSUM_VALID_REF_EYEXO		30
#define DSUM_VALID_REF_TEE_EYEXO	50
#define DSUM_REVIVE_TEE_EYEXO		25


#define DSUM_REVIVE_P3V2			30
#define DSUM_VALID_REF_P3V2			30
#define DSUM_VALID_REF_TEE_P3V2		50
#define DSUM_REVIVE_TEE_P3V2		20





#define BALLPOS_HEIGHT_TEE	0.03
							if (pic->icp.b3d.z < BALLPOS_HEIGHT_TEE) {
								isIron = 1;
							} else {
								isIron = 0;		// DRIVER...... 
							}


							if (piana->camsensor_category == CAMSENSOR_Z3) {
								if (isIron == 0) {
									revivevalue_ref = DSUM_VALID_REF_TEE_Z3;
									revivevalue = DSUM_REVIVE_TEE_Z3;
								} else {
									revivevalue_ref = DSUM_VALID_REF_Z3;
									revivevalue = DSUM_REVIVE_Z3;
								}
							} else if (piana->camsensor_category == CAMSENSOR_EYEXO) {
								if (isIron == 0) {
									revivevalue_ref = DSUM_VALID_REF_TEE_EYEXO;
									revivevalue = DSUM_REVIVE_TEE_EYEXO;
								} else {
									revivevalue_ref = DSUM_VALID_REF_EYEXO;
									revivevalue = DSUM_REVIVE_EYEXO;
								}
							} else if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
								if (isIron == 0) {
									revivevalue_ref = DSUM_VALID_REF_TEE;
									revivevalue = 20;
								} else {
									revivevalue_ref = DSUM_VALID_REF;
									revivevalue = 25;
								}
							} else if (piana->camsensor_category == CAMSENSOR_P3V2) {
								if (isIron == 0) {
									revivevalue_ref = DSUM_VALID_REF_TEE_P3V2;
									revivevalue = DSUM_REVIVE_TEE_P3V2;
								} else {
									revivevalue_ref = DSUM_VALID_REF_P3V2;
									revivevalue = DSUM_REVIVE_P3V2;
								}
							} else {
								if (isIron == 0) {
									revivevalue_ref = DSUM_VALID_REF_TEE;
									revivevalue = DSUM_REVIVE_TEE;
								} else {
									revivevalue_ref = DSUM_VALID_REF;
									revivevalue = DSUM_REVIVE;
								}
							}

							mode;fp;
#if 1						// 20191217.. REVIVE
							if (dsum > revivevalue_ref /*DSUM_VALID_REF*/) {
								double dsum1;
								//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "calc dsum\n");
								// compare image... 
								dsum1 = 0.0;
								icount = 0;
								for (i = 0; i < /*(U32)*/ ih; i++) {
									for (j = 0; j < /*(U32)*/ iw; j++) {
#ifdef IPP_SUPPORT
										value= (int)((U08)*(pref + (i+oy)*w+(j+ox))) - (int)((U08)*(pimg + (i+oy) * droi.width + (j+ox)));
#else
                                        value= (int)((U08)*(pref + (i+oy)*w+(j+ox))) - (int)((U08)*(pimg + (i+oy) * widthResize + (j+ox)));
#endif
										if (value > 0) {
											dsum1 = dsum1 + value * value;
										}
									}
								}
								dsum1 = sqrt(dsum1 / (ih * iw));

								//#define DSUM_REVIVE	40
								cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "dsum(%lf, %lf, %d)   =   %lf\n", dsum, (double)revivevalue, piana->readymercycount, dsum1);

#ifdef IPP_SUPPORT
								mycircleGray((int)(ox+dbr), (int)(ox+dbr), (int)dbr, 0xFF, (U08 *)pimg, droi.width);
#else
                                mycircleGray((int)(ox+dbr), (int)(ox+dbr), (int)dbr, 0xFF, (U08 *)pimg, widthResize);
#endif
								
								if (dsum1 < revivevalue) {
									balldisappeared = 0;			// REVIVE!!
									cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " REVIVE!!!\n");
									piana->readymercycount = 0;
								} else {
									piana->readymercycount++;
#define READYMERCYCOUNT	2
//#define READYMERCYCOUNT	1
									if (piana->readymercycount > READYMERCYCOUNT) {
										balldisappeared = 1;
										cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---- to trans!!\n");
									} else {
										balldisappeared = 0;
									}
								}
							} else {
								cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "dsum: %lf, revivevalue_ref: %lf\n", dsum, revivevalue_ref);
							}
#endif

						}
						if (piana->himgfunc) {
							//I32 ox, oy;
							//ox = (I32)((w - dbr) / 2);
							//oy = (I32)((h - dbr) / 2);
							//((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)(pimg +(int)(ox + oy * droi.width), droi.width, droi.height, 3 /*4*/, 1, 2, 3, piana->imguserparam);
							//((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)(pimg, droi.width, droi.height, 3 /*4*/, 1, 2, 3, piana->imguserparam);
#ifdef IPP_SUPPORT
							((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)(pimg, droi.width, droi.height, 0 /*4*/, 1, 2, 3, piana->imguserparam);
#else
                            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)(pimg, widthResize, heightResize, 0 /*4*/, 1, 2, 3, piana->imguserparam);
#endif
						}
						if (piana->himgfunc) {
							//((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)(pref, w, h, 3 /*5*/, 1, 2, 3, piana->imguserparam);
							((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)(pref, w, h, 1 /*5*/, 1, 2, 3, piana->imguserparam);
						}
					} else {
						if (goodball) {

							I32 biw, bih;
							biw = droi.width;
							bih = droi.height;

							if (biw > BALLIMAGE_REF_SIZE) {
								biw = BALLIMAGE_REF_SIZE;
							}
							if (bih > BALLIMAGE_REF_SIZE) {
								bih = BALLIMAGE_REF_SIZE;
							}

							memcpy(pref, pimg, biw * bih);
							pic->biw = biw;
							pic->bih = bih;


#define BALLPOSITION_TEE_UPDATERATE		0.2
#define BALLPOSITION_IRON_UPDATERATE	0.02

							if (piana->camsensor_category == CAMSENSOR_EYEXO) {
								// what?
							} else {
								m++;				// 20181128
							}
						} // if (goodball) 
					}
				} else {
					balldisappeared = 1;
				}		
			}
		}

		if (piana->testmode == 1) {
			if (balldisappeared) {
				balldisappeared = 0;
			}
		}
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d #4 m: %d\n", __LINE__, m);
//#define RECALC_SHOTTS
		if (balldisappeared) { // Ball disappear!~!
			U32 i;
			U32 camidO;
			U32 rindexO;
			U64 ts64Ofit;
			U64 ts64Ofrom;
			U64 ts64Oto;

			//--
//			camidO = 1 - camidReady;		// the other camid
			camidO = piana->camidOther;		// the other camid
			ts64Ofit = 0;
			rindexO = 0;

			if (piana->opmode == IANA_OPMODE_FILE) {
				rindexO  = piana->info_startindex[camidO];	// rindex
				ts64Ofit = piana->info_startts64[camidO];	// 
			} else {
#if defined(RECALC_SHOTTS)
				for (i = 0; i < 5; i++) 
				{
					U64 ts64MtO;

					ts64MtO = iana_ts64_from2to(piana, camidReady, camidO, ts64Mt);
					res = scamif_imagebuf_timestamprangeEx(piana->hscamif, camidO, 1, ts64MtO,
							&rindexO, &ts64Ofit, &ts64Ofrom, &ts64Oto);
					if (res == 1) {
						break;
					}
					cr_sleep(5);
				}
				if (res == 0) {
					ts64Ofit = 0;
					rindexO  = 0;
				} 
#else
				ts64Ofrom;
				ts64Oto;

				ts64Ofit = ts64Mt;

				{
					U32  currentindex;
					U32  rindex;
					U64 ts64_;
					camif_buffer_info_t *pb;


					res = scamif_imagebuf_peek_latest(
							piana->hscamif, camidO, NORMALBULK_NORMAL, 
							&pb, 			// camif_buffer_info_t **ppb
							NULL, 		// U08 **pbuf
							&currentindex);	// Get Latest image buffer and info. Preserve read-index

					if (res == 0 || pb == NULL) {			// check no data.. cause of crash.. 20171030
						continue;
					}
					ts64_ = MAKEU64(pb->ts_h, pb->ts_l);
#define CAMTSMARGIN   ((U64) ((0.0002 * TSSCALE_D)))
					rindex = currentindex;
					if (ts64_ - CAMTSMARGIN < ts64Mt) {
						rindex = currentindex;
					} else {
//#define SEARCHRANGE	32
#define SEARCHRANGE	32
//						for (i = 1; i < 10; i++) 
						for (i = 1; i < SEARCHRANGE; i++) 
						{
							res = scamif_imagebuf_trimindex2(piana->hscamif, camidO, 1, (I32)(currentindex-i), &rindex);
							res = scamif_imagebuf_randomaccess(piana->hscamif, camidO, NORMALBULK_NORMAL, rindex, &pb, NULL);

							ts64_ = MAKEU64(pb->ts_h, pb->ts_l);

							cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%2d:%3d> %llx (%lf) vs %llx (%lf)\n", 
								i, rindex,
								ts64Mt, (double)ts64Mt/TSSCALE_D,
								ts64_, (double)ts64_/TSSCALE_D	);
							if (ts64_ - CAMTSMARGIN < ts64Mt) {
								break;
							}
						}
					}
					rindexO = rindex;
				}
#endif
			}

			cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " camidO[%d]ts64shot : %llx -> %llx, rindex: %d -> %d\n", 
				camidO, piana->pic[camidO]->ts64shot, ts64Ofit, piana->pic[camidO]->rindexshot,rindexO);
			
			piana->pic[camidO]->ts64shot   = ts64Ofit;
			piana->pic[camidO]->rindexshot = rindexO;

			piana->runstate = IANA_RUN_TRANSITORY;
			cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Ball Disappeared!![%d], ts: %016llx (%lf), rindex: %d\n",
					m, 
					ts64Mt,
					ts64Mt/TSSCALE_D, rindex);
			cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "                     O  ts: %016llx (%lf), rindex: %d  (%lf)\n",
					ts64Ofit,
					ts64Ofit/TSSCALE_D, 
					rindexO, (piana->ts64delta / TSSCALE_D));
//			OutputDebugStringA("Ball Disappeared..\n") ;

			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)procimg, droi.width, droi.height, 0, 1, 2, 3, piana->imguserparam);
			}

			break;
		}

		if (piana->camsensor_category == CAMSENSOR_EYEXO) {
			//
		} else {
			if (doitmore == 0) {
				m++;
				//break;
			}
		}
	} 		// for (m = 0; m < multitude; m++) 
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d #5 m: %d\n", __LINE__, m);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" m: %d\n", m);
	free(resizebuf);
	resizebuf = NULL;
	if (ironAreaCheckActive) {
		U32 ironAreaBallExistCountClear;

		ironAreaBallExistCountClear = 1;
		if (ironAreaCheck) {
			if (ironAreaBallExist) {
				if (((piana->shotarea == IANA_AREA_TEE) || iana_isInTee(piana, &pic->icp.b3d))
					&& piana->allowedarea[IANA_AREA_IRON]) {			// Ready: in TEE area. but Iron area is allowed.
					piana->ironAreaBallExistCount++;					// 20201130++;
					ironAreaBallExistCountClear = 0;
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" ironAreaBallExistCount: %d\n", piana->ironAreaBallExistCount);
//#define IRONAREABALLEXISTCOUNT_MAX	5
#define IRONAREABALLEXISTCOUNT_MAX	3
					if (piana->ironAreaBallExistCount >= IRONAREABALLEXISTCOUNT_MAX) {
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" NO-Ready... ironAreaBallExistCount\n");
						piana->ironAreaBallExistCount = 0;
						res = 0;
						piana->runstate = IANA_RUN_TRANSITORY;												// NO GOOD.. 
						goto func_exit;
					}
				}
			}
		}
		if (ironAreaBallExistCountClear) {
			piana->ironAreaBallExistCount = 0;
		}
	}


	//if (balldisappeared || piana->opmode == IANA_OPMODE_FILE) 
	if (balldisappeared || piana->opmode == IANA_OPMODE_FILE)
	{ // Ball disappeared.. or FILE mode
		int ii;
		for (ii = 0; ii < (NUM_CAM - 1); ii++)				// Other cam and attack cam. .:P
		{
			I32 i;
			U32 camid;
			U32 rindex;
			U32 startindex;
			U32 updatedone;
			point_t LpO, PpO;
			HAND hscamifOA;

			//----

			if (ii == 0) {		// The other cam.
				camid = camidOther;		// the other camid
				hscamifOA = piana->hscamif;
				LpO.x = piana->pic[camidOther]->icp.L.x;
				LpO.y = piana->pic[camidOther]->icp.L.y;
			} else {	// ii == 1
				// TODO: process for remaing CAMs if needed, but now you shall not be here!!
				camid = 0;	// just to prevent compile warning
			}
			iana_L2P(piana, camid, &LpO, &PpO, 0);
			pic   =piana->pic[camid];


			if (piana->opmode == IANA_OPMODE_FILE) {				// File mode
				startindex = piana->info_startindex[camid];
			} else {												// Realtime mode
				if (ii == 0) {		// Other cam.
					res = scamif_imagebuf_peek_latest(
							piana->hscamif, camid, NORMALBULK_NORMAL, 
							NULL, 		// camif_buffer_info_t **ppb
							NULL, 		// U08 **pbuf
							&startindex);	// Get Latest image buffer and info. Preserve read-index
					if (res == 0) {
						continue;
					}
				} else {
					// TODO: process for remaing CAMs if needed, but now you shall not be here!!
					startindex = 0; // just to prevent compile warning
				}
			}
			//--

#define PRESHOTINDEX	5
			updatedone = 0;
			for (i = -PRESHOTINDEX; i < 0; i++) {
				double bsmin, bsmax;
				point_t Lp0;
				point_t Pp0;

				//------------------- Get Data buffer from imagebuf
				if (ii == 0) {		// Other cam.
					res = scamif_imagebuf_trimindexdelta(piana->hscamif, camid, 1,  startindex, i, &rindex);
				} else {
					// TODO: process for remaing CAMs if needed, but now you shall not be here!!
					rindex = 0; // just to prevent compile warning
				}
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "CHECK BALLPOS[CAMID %d] rindex, i: %d, rindex: %d (from %d)\n", camid, i, rindex, startindex);
				if (res) {
					if (ii == 0) {		// Other cam.
						res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_NORMAL, rindex, &pb, &buf);
					} else {
						// TODO: process for remaing CAMs if needed, but now you shall not be here!!
					}
				}
				if (res == 0) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "rindex recalc fail. camid: %d\n", camid);
					continue;
				}

				pcii = &pb->cii;
				multitude 	= pcii->multitude;
				if (multitude == 0) {
					multitude = 1;
				}

				xfact 	= XYFACT;
				yfact 	= (XYFACT * multitude);

				//------------------- 
				procimg = pic->pimgFull;

				Lp0.x = pic->icp.L.x;				//
				Lp0.y = pic->icp.L.y;

				iana_L2P(piana, camid, &Lp0, &Pp0, 0);

				rectArea.left 	= (long) (Pp0.x - ADDX+1);
				rectArea.right 	= (long) (Pp0.x + ADDX);
				rectArea.top 	= (long) (Pp0.y - ADDY+1);
				rectArea.bottom = (long) (Pp0.y + ADDY);

				//-
				m = 0;
				rectAreaM.left 	= (long) (rectArea.left 	- pic->offset_x);
				rectAreaM.right = (long) (rectArea.right 	- pic->offset_x);
				//rectAreaM.top 	= (long) ((rectArea.top 	- pic->offset_y) / multitude + (pcii->height / multitude) * m);
				//rectAreaM.bottom = (long)((rectArea.bottom	- pic->offset_y) / multitude + (pcii->height / multitude) * m);
				rectAreaM.top 	= (long) ((rectArea.top 	- pic->offset_y) / multitude + ((m*pcii->height) / multitude));
				rectAreaM.bottom = (long)((rectArea.bottom	- pic->offset_y) / multitude + ((m*pcii->height) / multitude));

				ssize.width 	= pcii->width;
				ssize.height 	= pcii->height;

				//		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "W: %d, H: %d\n", ssize.width, ssize.height);
				sroi.x 		= 0;
				sroi.y 		= 0;
				sroi.width 	= rectAreaM.right - rectAreaM.left + 1;
				sroi.height	= rectAreaM.bottom   - rectAreaM.top  + 1;

				droi.x 		= 0;
				droi.y 		= 0;
				droi.width 	= (int) (sroi.width * xfact + 0.001);
				droi.height	= (int) (sroi.height * yfact + 0.001);

				sstep 		= pcii->width;
				dstep		= droi.width;

				if (resizebuf == NULL) {
					ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_CUBIC, &bufsize);
					//resizebuf = ippsMalloc_8u(bufsize);
					resizebuf = (U08 *)malloc(bufsize);
					//resizebuf = ippsMalloc_8u(bufsize * 2);
				}
				status = ippiResizeSqrPixel_8u_C1R(
						//(Ipp8u *)buf + rectAreaM.left + rectAreaM.top * ssize.width,
						(Ipp8u *)buf + rectAreaM.left + rectAreaM.top * ssize.width,
						ssize,
						sstep,
						sroi,
						//
						(Ipp8u*) procimg,
						dstep,
						droi,
						//
						xfact,
						yfact,
						0,		// xshift,
						0,		// yshift,
						IPPI_INTER_CUBIC,
						resizebuf
						);

				if (piana->himgfunc) {
					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)(
						(char *)procimg, 
						droi.width, droi.height, 0, 1, 2, 3, piana->imguserparam);
				}

				iana_setReadyRunBallsizeMinMax(piana, camid, xfact);
				bsmin = piana->pic[camid]->icp.RunBallsizePMin;
				bsmax = piana->pic[camid]->icp.RunBallsizePMax;


				//				piana->ic[camid].icp.RunBallsizePMin *= 0.8;
				/////////////////piana->pic[camid]->icp.RunBallsizePMax *= 1.2;

#if defined(V2000_TEST)
				iana_getballcandidatorAdapt
					(piana, camid, procimg, droi.width, droi.height, droi.width, &bc[0], &bcount,
					 HALFWINSIZE_CHECKREADY,
					 READY_MULTV, READY_EXPN,
					 THSTDMULT_,
					 0.5   // 0.3  /*1.0*/ /*cermax*/
					);

#else
				iana_getballcandidatorAdapt
					(piana, camid, procimg, droi.width, droi.height, droi.width, &bc[0], &bcount,
					 4, 	// HALFWINSIZE_CHECKREADY,
					 1, 0,	// READY_MULTV, READY_EXPN,
					 1, 	// THSTDMULT_,
					 0.5   // 0.3  /*1.0*/ /*cermax*/
					 ,cutmean_ready	// 20190420, yhsuk
					);
#endif
				//piana->ic[camid].icp.RunBallsizePMin = bsmin;
				//piana->ic[camid].icp.RunBallsizePMax = bsmax;

				if (bcount > 0) {
					I32 i;
					point_t Lpi;
					point_t Ppi;
					double mindist;
					I32    minindex;
					point_t minLp;
					point_t minPp;
					double dx, dy, dd;
					//				cr_rectL_t rectRunL;
					double ballradiusMax;
					double ballradiusMin;

					//---
					mindist = 999;
					minindex = -1;
					minLp.x = Lp0.x;
					minLp.y = Lp0.y;
					minPp.x = Pp0.x;
					minPp.y = Pp0.y;
					//rectRunL.sx = pic->icp.L.x - BALLDIAMETER;
					//rectRunL.sy = pic->icp.L.y - BALLDIAMETER;
					//rectRunL.ex = pic->icp.L.x + BALLDIAMETER;
					//rectRunL.ey = pic->icp.L.y + BALLDIAMETER;
					//iana_setRunBallsizeMinMax(piana, camid, &rectRunL, 1.2);
					//ballradiusMax = (int) (pic->icp.RunBallsizePMax / 2.0);
					//ballradiusMax *= 1.2;
					//ballradiusMin = (int) (pic->icp.RunBallsizePMin / 2.0);
					//ballradiusMin *= 0.8;

					ballradiusMax = (bsmax / 2.0) * 1.2;
					ballradiusMin = (bsmin / 2.0) * 0.7;

					for (i = 0; i < (I32)bcount; i++) {
						Ppi.x = (bc[i].P.x / xfact) + (rectArea.left - pic->offset_x) + pic->offset_x;
#if defined(MULTITUDE_FRACTION)
						Ppi.y = ( (bc[i].P.y+0.5) * multitude) / yfact + (rectArea.top - pic->offset_y)  + pic->offset_y;
#else
						Ppi.y = (bc[i].P.y / (yfact/multitude)) + (rectArea.top - pic->offset_y)  + pic->offset_y;
#endif
						bc[i].cer;

						//iana_P2L(piana, camidReady, &Ppi, &Lpi);
						if (bc[i].cr > ballradiusMax || bc[i].cr  < ballradiusMin) {
							break;
						}
						iana_P2L(piana, camid, &Ppi, &Lpi, 0);
						dx = Lpi.x - Lp0.x;
						dy = Lpi.y - Lp0.y;
						dd = sqrt(dx*dx + dy*dy);
						if (mindist > dd) {
							mindist = dd;
							minindex = i;
							minLp.x = Lpi.x;
							minLp.y = Lpi.y;
							minPp.x = Ppi.x;
							minPp.y = Ppi.y;
						}
						//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] cer: %lf size: %lf  dd: %10lf (%10lf %10lf) vs (%10lf %10lf)\n", 
						//	i, bc[i].cer, bc[i].cr, 
						//	dd, Lp0.x, Lp0.y, Lpi.x, Lpi.y);
					}
#define MINDDDIST	0.10
					if (minindex >= 0) {

						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "~[%d]  %d dd:%10lf C1(%10lf %10lf) -> C0(%10lf %10lf)  P(%10lf %10lf)\n", i,
								minindex, mindist, 
								pic->icp.L.x, pic->icp.L.y, 
								minLp.x, minLp.y, minPp.x, minPp.y);
						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "~       C0-C1(%10lf %10lf)\n", minLp.x-pic->icp.L.x, minLp.y-pic->icp.L.y);


						if (mindist < MINDDDIST) {
							//iana_L2P(piana, camidReady, &minLp, &minPp);
//							iana_L2P(piana, camid, &minLp, &minPp);
							cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]  %d dd:%10lf C1(%10lf %10lf) -> C0(%10lf %10lf)  P(%10lf %10lf)\n", i,
									minindex, mindist, 
									pic->icp.L.x, pic->icp.L.y, 
									minLp.x, minLp.y, minPp.x, minPp.y);
							cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "       C0-C1(%10lf %10lf)\n", minLp.x-pic->icp.L.x, minLp.y-pic->icp.L.y);

							//LpO, PpO;

							LpO.x = minLp.x;				//
							LpO.y = minLp.y;
							PpO.x = minPp.x;				//
							PpO.y = minPp.y;
							updatedone = 1;
							break;
						}
					}
				} 	// if (bcount > 0) 
				if (updatedone) {
					break;
				}
			} 			// for (i = -PRESHOTINDEX; i < 0; i++) 

			pic->icp.L.x = LpO.x;				//
			pic->icp.L.y = LpO.y;
			pic->icp.P.x = PpO.x;				//
			pic->icp.P.y = PpO.y;
		} 	// for (ii = 0; ii < 2; ii++)				// Other cam and attack cam. .:P

		for (camid = 0; camid < NUM_CAM; camid++) {
			cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "camid[%d]  L(%10lf %10lf)  P(%10lf %10lf)\n", camid,
				piana->pic[camid]->icp.L.x,
				piana->pic[camid]->icp.L.y,
				piana->pic[camid]->icp.P.x,
				piana->pic[camid]->icp.P.y);
		}
	} 	// if (piana->runstate == IANA_RUN_READY || piana->opmode == IANA_OPMODE_FILE)  // Update 
	else
	{ // BALL GOOD..
		static int dodecimation = 0;

		// Check Othercam..
		//camidOther = piana->camidOther;
		pic   =piana->pic[camidOther];
#define DODECIMATIONCOUNT	10	
		if ((++dodecimation % DODECIMATIONCOUNT) == 0) {
			//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__ " start of  decimation\n");
			dodecimation = 0;
			if (piana->opmode == IANA_OPMODE_FILE) {
				rindex = piana->info_startindex[camidOther];			// rindex
				res = scamif_imagebuf_randomaccess(piana->hscamif, camidOther, NORMALBULK_NORMAL, rindex, &pb, &buf);
				pcii = &pb->cii;

				if (piana->shotarea		== IANA_AREA_PUTTER) {
					piana->processarea	 = IANA_AREA_PUTTER;
				}

				if (res != 0 && buf != NULL) {
					if (piana->himgfunc) {
						((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)(
							(char *)buf, pcii->width, pcii->height, 0, 1, 2, 3, piana->imguserparam);
					}
				}
			} else {
				res = scamif_imagebuf_peek_latest(piana->hscamif, camidOther, NORMALBULK_NORMAL, NULL, NULL, NULL);
				if (res) {		// If there is any available frame.
					res = scamif_imagebuf_read_latest(piana->hscamif, camidOther, NORMALBULK_NORMAL, &pb, &buf, &rindex);	
					pcii = &pb->cii;
				}
			}
			if (res == 0) {	// No image data yet.
				goto func_exit;
			}

			multitude 	= pcii->multitude;
			if (multitude == 0) {
				multitude = 1;
			}


			if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
				xfact 		= XYFACT_CONSOLE1;
				yfact 		= (XYFACT_CONSOLE1 * multitude);
			} else {
				xfact 		= XYFACT;
				yfact 		= (XYFACT * multitude);
			}


			procimg = pic->pimgFull;
			{
				point_t Lp;
				point_t Pp;

				//--
				Lp.x = pic->icp.L.x;				// Center..
				Lp.y = pic->icp.L.y;

				iana_L2P(piana, camidOther, &Lp, &Pp, 0);

				rectArea.left 	= (long) (Pp.x - ADDX+1);
				rectArea.right 	= (long) (Pp.x + ADDX);
				rectArea.top 	= (long) (Pp.y - ADDY+1);
				rectArea.bottom = (long) (Pp.y + ADDY);


				rectLArea.sx 	= (double) (Pp.x - ADDX+1);
				rectLArea.ex 	= (double) (Pp.x + ADDX	 );
				rectLArea.sy 	= (double) (Pp.y - ADDY+1);
				rectLArea.ey 	= (double) (Pp.y + ADDY  );

				{
					long l0;
					l0 = rectArea.top;
					l0 = ((l0 + 3) / 4) * 4;
					rectArea.top = l0;
					l0 = rectArea.bottom;
					l0 = ((l0 + 3) / 4) * 4;
					rectArea.bottom = l0;
				}
			}

			m = 0;		// :P
			{
				double mindist;
				double mindistLx, mindistLy;
				int mindistIndex;
				double mindistmean, mindiststddev, mindistmeanH, mindiststddevH;
				//ts64Mt = ts64 + (periodm * (multitude - m -1));
				ts64Mt = ts64 - (periodm * (multitude - m -1));
				/*
				   rectAreaM.left 	= (long) (rectArea.left 	- pic->offset_x);
				   rectAreaM.right = (long) (rectArea.right 	- pic->offset_x);
				   */
				rectAreaM.left 	= (long) (rectLArea.sx 	- pic->offset_x);
				rectAreaM.right = (long) (rectLArea.ex 	- pic->offset_x);
				rectAreaM.top 	= (long) ((rectArea.top 	- pic->offset_y) / multitude + ((pcii->height *m)/ multitude)); //  + m * 2;
				rectAreaM.bottom = (long)((rectArea.bottom	- pic->offset_y) / multitude + ((pcii->height *m)/ multitude)); //  + m * 2;
				ssize.width 	= pcii->width;
				ssize.height 	= pcii->height;

				sroi.x 		= 0;
				sroi.y 		= 0;
				sroi.width 	= rectAreaM.right - rectAreaM.left + 1;
				sroi.height	= rectAreaM.bottom   - rectAreaM.top  + 1;

				mindistmean		= 0;
				mindiststddev	= 0;
				mindistmeanH	= 0;
				mindiststddevH	= 0;

				if (
						( pcii->width < (U32)sroi.width  || pcii->height < (U32)sroi.height)
						|| (pcii->width > FULLWIDTH || pcii->height > FULLHEIGHT)
						|| (sroi.width  > FULLWIDTH || sroi.height > FULLHEIGHT)
				   )
				{
					if (resizebuf != NULL) {
						free(resizebuf);
					}
					resizebuf = NULL;
					res = 0;
					piana->runstate = IANA_RUN_TRANSITORY;
					goto func_exit;

				}

				droi.x 		= 0;
				droi.y 		= 0;
				droi.width 	= (int) (sroi.width * xfact + 0.001);
				droi.height	= (int) (sroi.height * yfact + 0.001);

				sstep 		= pcii->width;
				dstep		= droi.width;

				if (resizebuf == NULL) {
					ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_CUBIC, &bufsize);
					resizebuf = (U08 *)malloc(bufsize);
				}
				__try {
					status = ippiResizeSqrPixel_8u_C1R(
							//(Ipp8u *)buf + rectAreaM.left + rectAreaM.top * ssize.width,
							(Ipp8u *)buf + rectAreaM.left + rectAreaM.top * ssize.width,
							ssize,
							sstep,
							sroi,
							//
							(Ipp8u*) procimg,
							dstep,
							droi,
							//
							xfact,
							yfact,
							0,		// xshift,
							0,		// yshift,
							IPPI_INTER_CUBIC,
							resizebuf
							);
				} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
						|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
						|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
						|| GetExceptionCode()== EXCEPTION_FLT_DIVIDE_BY_ZERO
						){
					if (resizebuf != NULL) {
						free(resizebuf);
					}
					resizebuf = NULL;
					res = 0;
					piana->runstate = IANA_RUN_TRANSITORY;
					goto func_exit;
				}

                //{
                //    cv::Mat src(cv::Size(pcii->width, pcii->height), CV_8UC1, buf), cropped, dst;
                //    U32 l, r, t, b;
                //    l = rectAreaM.left;
                //    r = rectAreaM.right;
                //    t = rectAreaM.top;
                //    b = rectAreaM.bottom;
                //    if (l < 0 ||
                //        t < 0 ||
                //        r+1 > pcii->width ||
                //        b+1 > pcii->height) {

                //        res = 0;
                //        piana->runstate = IANA_RUN_TRANSITORY;
                //        goto func_exit;
                //    }
                //    cropped = src(cv::Rect(l, t, r-l+1, b-t+1));
                //    cv::resize(cropped, dst, cv::Size(droi.width, droi.height), xfact, yfact, cv::INTER_CUBIC);
                //    memcpy(procimg, dst.data, sizeof(U08)*droi.width*droi.height);
                //}


				mindist = 1.0e10;
				mindistIndex = -1;
				mindistLx = pic->icp.L.x;
				mindistLy = pic->icp.L.y;
				{
					double cermax;
					double crmin;
					double crmax;
					double crminratio;
					double crmaxratio;
					U32 i;

					iana_setReadyRunBallsizeMinMax(piana, camidOther, xfact);

					crmaxratio = IANA_BALLDETECT_GetMaxCircleRatio(piana->camsensor_category, camidOther);
					crminratio = IANA_BALLDETECT_GetMinCircleRatio(piana->camsensor_category, camidOther);						

					crmin = (piana->pic[camidOther]->icp.RunBallsizePMin / 2.0) * crminratio;
					crmax = (piana->pic[camidOther]->icp.RunBallsizePMax / 2.0) * crmaxratio;

					cermax = pic->icp.CerBallexist;

					memset(bc, 0, sizeof(bc));
					bcount = 0;
#define CERMAX_MULT_CAMOTHER	2
					iana_getballcandidatorAdapt
						(piana, camidOther, procimg, droi.width, droi.height, droi.width, &bc[0], &bcount,
						 HALFWINSIZE_CHECKREADY,
						 READY_MULTV, READY_EXPN,
						 THSTDMULT_,
						 cermax * CERMAX_MULT_CAMOTHER
						 ,cutmean_ready	// 20190420, yhsuk
						 );

					// refine ball candidate
					if (bcount > 0) {
						for (i = 0; i < bcount; i++) {
#if defined (USE_IANA_BALL)
							if (bc[i].cexist == IANA_BALL_EXIST) 
#else
								if (bc[i].cexist)
#endif
								{
									//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%d]: (%10lf %10lf : r %10lf (%10lf %10lf), e %10lf (%10lf))\n", 
									//		camidOther, i, 
									//		bc[i].P.x, bc[i].P.y, bc[i].cr, crmin, crmax, bc[i].cer, crmax);

									if (bc[i].cer > cermax * CERMAX_MULT_CAMOTHER|| bc[i].cr < crmin || bc[i].cr > crmax) {	// Filtering..
#if defined (USE_IANA_BALL)
										bc[i].cexist = IANA_BALL_EMPTY;
#else
										bc[i].cexist = 0;
#endif
										continue;
									} else {
										point_t Pp, Lp;
										double dist;

										//-- Converse to Pixel point.
										/*
										   cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "x: %lf -> %lf -> %lf -> %lf\n",
										   bc[i].cx,  
										   (bc[i].cx / xfact),
										   (bc[i].cx / xfact) + rectArea.left,
										   (bc[i].cx / xfact) + rectArea.left + pic->offset_x);
										   */
										bc[i].P.x = (bc[i].P.x / xfact) + (rectArea.left - pic->offset_x) + pic->offset_x;
#if defined(MULTITUDE_FRACTION)
										bc[i].P.y = (bc[i].P.y+0.5) * multitude / yfact + (rectArea.top - pic->offset_y)  + pic->offset_y;
#else
										bc[i].P.y = (bc[i].P.y / (yfact/multitude)) + (rectArea.top - pic->offset_y)  + pic->offset_y;
#endif
										bc[i].cr /= xfact;

										//-- Converse to Local point.
										Pp.x = bc[i].P.x;
										Pp.y = bc[i].P.y;
										iana_P2L(piana, camidOther, &Pp, &Lp, 0);
										bc[i].L.x = Lp.x;
										bc[i].L.y = Lp.y;
										dist = DDIST(pic->icp.L.x, pic->icp.L.y, bc[i].L.x, bc[i].L.y);
										if (dist < mindist) {
											mindist = dist;
											mindistIndex = i;
											mindistLx = bc[i].L.x;
											mindistLy = bc[i].L.y;

											mindistmean = bc[i].mean;
											mindiststddev = bc[i].stddev;
											mindistmeanH = bc[i].meanH;
											mindiststddevH = bc[i].stddevH;
										}
									}
								} 		// if (bc[i].cexist) 
						} 			// for (i = 0; i < bcount; i++) 
					} 				// if (bcount > 0) 
				} 					// 3) Get Ball positions, if any.


				if (mindistIndex >= 0) {
					point_t Lp;
					point_t Pp;
					double lx, ly;
					double updaterate;

					updaterate =  BALLPOSITION_TEE_UPDATERATE;
					lx = mindistLx * updaterate + pic->icp.L.x * (1.0 - updaterate);
					ly = mindistLy * updaterate + pic->icp.L.y * (1.0 - updaterate);
					//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BallPos Other [%d:%d] (%10lf %10lf : %10lf %10lf) -> (%10lf %10lf)\n", 
					//		camidOther, mindistIndex,
					//		pic->icp.P.x, pic->icp.P.y, pic->icp.L.x, pic->icp.L.y, lx, ly);
					pic->icp.L.x = lx;
					pic->icp.L.y = ly;
					Lp.x = lx;
					Lp.y = ly;
					iana_L2P(piana, camidOther, &Lp, &Pp, 0);

					pic->icp.P.x = Pp.x;
					pic->icp.P.y = Pp.y;

					pic->icp.mean 	= mindistmean;
					pic->icp.stddev = mindiststddev;
					pic->icp.meanH 	= mindistmeanH;
					pic->icp.stddevH = mindiststddevH;
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] mindistmean: %lf\n", camidOther, mindistmean);
					// TODO: Update B3d.. :P
				}

//#define MERCY_PIXEL_DISTANCE	10
#define MERCY_PIXEL_DISTANCE	20
//#define MERCY_PIXEL_DISTANCE	30			// 20180926..

			} 	// m = 0;		// :P
			//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__ " END of decimation\n");
		} 	// if ((++dodecimation % DODECIMATIONCOUNT) == 0) 
//		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__ "Before checkreadyposition\n");
		checkreadyposition(piana, MERCY_PIXEL_DISTANCE);				// TODO: Check with time..
//		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__ "After checkreadyposition\n");
	} 		// BALL GOOD..

	free(resizebuf);

	res = 1;
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d #6 \n", __LINE__);
func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res %d\n", res);
	return res;

#else /* IPP_SUPPORT */ 

    I32 res;
    camif_buffer_info_t *pb;
    iana_cam_t	*pic;
    camimageinfo_t	*pcii;
    U32 multitude;
    U64 period;
    U64 periodm;
    U32 skip, syncdiv;
    U64 ts64;
    U64 ts64Ready;
    U64 ts64Mt;				// ts64 for CheckReady, Multiple
    double ts64d;
    I32 m;

    I32 balldisappeared;

    U08 *buf;
    U08 *procimg;
    U32 rindex;
    U32 camidReady;
    U32 camidOther;
    U32 camid;

    iana_ballcandidate_t bc[MAXCANDIDATE];
    U32 bcount;

    double	xfact, yfact;
    cr_rect_t rectArea;
    cr_rectL_t rectLArea;

    scamif_t *pscamif;

    double cutmean_ready;
    //--------------

    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with hiana: %p\n", piana);

#define CUTMEAN_READY	0.5	
#define CUTMEAN_READY_EYEXO	0.2
    if (piana->camsensor_category == CAMSENSOR_EYEXO) {
        cutmean_ready = CUTMEAN_READY_EYEXO;
    } else {
        cutmean_ready = CUTMEAN_READY;
    }

    pscamif = (scamif_t *)piana->hscamif;

    // 1) get image
    camidReady = piana->camidCheckReady;
    camidOther = piana->camidOther;
    pic   =piana->pic[camidReady];

    buf = NULL;
    pcii = NULL;
    pb = NULL;
    rindex = 0;
    
    if (piana->opmode == IANA_OPMODE_FILE) {
        rindex = piana->info_startindex[camidReady];			// rindex
        res = scamif_imagebuf_randomaccess(piana->hscamif, camidReady, NORMALBULK_NORMAL, rindex, &pb, &buf);
        pcii = &pb->cii;

        if (piana->shotarea		== IANA_AREA_PUTTER) {
            piana->processarea	 = IANA_AREA_PUTTER;
        }

        if (res != 0 && buf != NULL) {
            if (piana->himgfunc) {
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, pcii->width, pcii->height, 0, 1, 2, 3, piana->imguserparam);
            }
        }
    } else {
        res = scamif_imagebuf_read(piana->hscamif, camidReady, NORMALBULK_NORMAL, &pb, &buf, &rindex);
        if (res != 0) {
            pcii = &pb->cii;
        }
    }

    if (res == 0) {	// No image data yet.
        goto func_exit;
    }
    piana->lastrindex[camidReady] = rindex; // [jmyl] lastrindex는 사용하는 부분 없는 듯...

    period = TSSCALE / (U64)pcii->framerate;			// nsec
    multitude 	= pcii->multitude;
    if (multitude == 0) {
        multitude = 1;
    }
    skip 	= pcii->skip;
    if (skip == 0) {
        skip = 1;
    }
    syncdiv = pcii->syncdiv;

    if (syncdiv == 0 || camidReady == (U32)pscamif->mastercamid) {				// master: always 1
        syncdiv = 1;
    }

    periodm = period * skip * syncdiv;
    ts64 = MAKEU64(pb->ts_h, pb->ts_l);
    ts64d = ts64 / TSSCALE_D;

    ts64Ready = ts64;

    //--
    piana->runindexcount++;

#define XYFACT		1
#define XYFACT_CONSOLE1		0.5
    if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
        xfact 		= XYFACT_CONSOLE1;
        yfact 		= (XYFACT_CONSOLE1 * multitude);
    } else {
        xfact 		= XYFACT;
        yfact 		= (XYFACT * multitude);
    }

    if (piana->camsensor_category == CAMSENSOR_EYEXO) {
#define XYFACT_25MMTEST		0.5
        xfact 		= XYFACT_25MMTEST;
        yfact 		= (XYFACT_25MMTEST * multitude);
    }

    procimg = pic->pimgFull;

    {
        point_t Lp;
        point_t Pp;

        //--
        Lp.x = pic->icp.L.x;				// Center..
        Lp.y = pic->icp.L.y;

        iana_L2P(piana, camidReady, &Lp, &Pp, 0);

#define ADDX	48
#define ADDY	48
        rectArea.left 	= (long)(Pp.x - ADDX+1);
        rectArea.right 	= (long)(Pp.x + ADDX);
        rectArea.top 	= (long)(Pp.y - ADDY+1);
        rectArea.bottom = (long)(Pp.y + ADDY);


        rectLArea.sx 	= (double)(Pp.x - ADDX+1);
        rectLArea.ex 	= (double)(Pp.x + ADDX);
        rectLArea.sy 	= (double)(Pp.y - ADDY+1);
        rectLArea.ey 	= (double)(Pp.y + ADDY);

        {
            long l0;
            l0 = rectArea.top;
            l0 = ((l0 + 3) / 4) * 4;
            rectArea.top = l0;
            l0 = rectArea.bottom;
            l0 = ((l0 + 3) / 4) * 4;
            rectArea.bottom = l0;
        }
    }
    balldisappeared = 0;

    for (m = 0; m < (I32)multitude; m++) {
        double mindist;
        double mindistLx, mindistLy;
        int mindistIndex;
        int doitmore = 0;
        int width, height, widthROI, heightROI, widthResize, heightResize;
        int l, r, t, b; // left, right, top, bottom
        ts64Mt = ts64 - (periodm * (multitude - m -1));

        l = (int)(rectLArea.sx 	- pic->offset_x);
        r = (int)(rectLArea.ex 	- pic->offset_x);
        t = (int)((rectArea.top 	- pic->offset_y) / multitude + ((pcii->height *m)/ multitude)); //  + m * 2;
        b = (int)((rectArea.bottom	- pic->offset_y) / multitude + ((pcii->height *m)/ multitude)); //  + m * 2;

        width 	= (int)pcii->width;
        height 	= (int)pcii->height;

        l = l < 0 ? 0 : l;
        t = t < 0 ? 0 : t;
        r = width < r+1 ? width - 1 : r;
        b = height < b+1 ? height - 1 : b;

        widthROI = r - l + 1;
        heightROI = b - t + 1;
        
        if( widthROI <= 0 || heightROI <= 0)
            continue;

        /* widthROI = MAX(widthROI, 0); */
        /* heightROI = MAX(heightROI, 0); */

        widthResize = (int)(widthROI * xfact + 0.001);
        heightResize = (int)(heightROI * yfact + 0.001);

        if ((width > FULLWIDTH || height > FULLHEIGHT)) {
            res = 0;
            piana->runstate = IANA_RUN_TRANSITORY;
            goto func_exit;
        } // here we need to avoid crash

        cv::Mat cvImgSrc(cv::Size(width, height), CV_8UC1, buf), cvImgCrop, cvImgResized;

        cvImgCrop = cvImgSrc(cv::Rect(l, t, widthROI, heightROI));

        cv::resize(cvImgCrop, cvImgResized, cv::Size(widthResize, heightResize), xfact, yfact, cv::INTER_CUBIC);

        if (piana->himgfunc) {
            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)procimg, widthResize, heightResize, 3 /*m*/, 1, 2, 3, piana->imguserparam);
        }
        UNUSED(bc);
        UNUSED(bcount);
        UNUSED(mindist);
        UNUSED(mindistIndex);

        // 3) Get Ball positions, if any.
        mindist = 1.0e10;
        mindistIndex = -1;
        mindistLx = pic->icp.L.x;
        mindistLy = pic->icp.L.y;
        {
            double cermax;
            double crmin;
            double crmax;
            double crminratio;
            double crmaxratio;
            U32 i;

            iana_setReadyRunBallsizeMinMax(piana, camidReady, xfact);

            crmaxratio = IANA_BALLDETECT_GetMaxCircleRatio(piana->camsensor_category, camidReady);
            crminratio = IANA_BALLDETECT_GetMinCircleRatio(piana->camsensor_category, camidReady);

            crmin = (piana->pic[camidReady]->icp.RunBallsizePMin / 2.0) * crminratio;
            crmax = (piana->pic[camidReady]->icp.RunBallsizePMax / 2.0) * crmaxratio;

            cermax = pic->icp.CerBallexist;

            memset(bc, 0, sizeof(bc));
            bcount = 0;

#define READY_MULTV_Z	3
#define READY_EXPN_Z	1
#define THSTDMULT_Z	1.0
#define READY_MULTV_PUTTER_Z	1
#define READY_EXPN_PUTTER_Z		0
#define THSTMULT_COUNT	5
#define THSTMULT_COUNT_PUTTER	5
            if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {

                U32 thstmult_count;
                double thstmult_IRON[THSTMULT_COUNT] ={ 2.0, 1.3, 0.9, 0.4, 0.2 };
                double thstmult_TEE[THSTMULT_COUNT] ={ 0.9, 0.4, 0.2, 1.3, 2.0 };
                double thstmult_PUTTER[THSTMULT_COUNT_PUTTER] ={ 1.5, 1.0, 0.5, 0.3, 0.15 };

                if (piana->camsensor_category == CAMSENSOR_EYEXO) {
                    //double thstmult_IRON[THSTMULT_COUNT] = {1.5, 1.3, 0.9, 0.4, 0.2};
                    //double thstmult_TEE[THSTMULT_COUNT] = {0.9, 0.4, 0.2, 1.3, 2.0};
                    thstmult_IRON[0] = 1.5;
                    thstmult_IRON[1] = 1.3;
                    thstmult_IRON[2] = 0.9;
                    thstmult_IRON[3] = 0.4;
                    thstmult_IRON[4] = 0.2;
                    thstmult_TEE[0] = 0.9;
                    thstmult_TEE[1] = 0.4;
                    thstmult_TEE[2] = 0.2;
                    thstmult_TEE[3] = 1.3;
                    thstmult_TEE[4] = 2.0;
                }

                if (piana->processarea == IANA_AREA_PUTTER) {
                    thstmult_count = THSTMULT_COUNT_PUTTER;
                } else {
                    thstmult_count = THSTMULT_COUNT;
                }
                for (i = 0; i < thstmult_count; i++) {
                    double thstm;
                    int ready_multv_z;
                    int ready_expn_z;
                    if (piana->processarea == IANA_AREA_PUTTER) {
                        thstm = thstmult_PUTTER[i];
                        ready_multv_z = READY_MULTV_PUTTER_Z;
                        ready_expn_z  = READY_EXPN_PUTTER_Z;
                    } else if (piana->processarea == IANA_AREA_IRON) {
                        thstm = thstmult_IRON[i];
                        ready_multv_z = READY_MULTV_Z;
                        ready_expn_z  = READY_EXPN_Z;
                    } else {			// TEE
                        thstm = thstmult_TEE[i];
                        ready_multv_z = READY_MULTV_Z;
                        ready_expn_z  = READY_EXPN_Z;
                    }

                    iana_getballcandidatorAdapt
                    (piana, camidReady, procimg, widthResize, heightResize, widthResize, &bc[0], &bcount,
                        HALFWINSIZE_CHECKREADY,
                        ready_multv_z, ready_expn_z,
                        thstm, 			// thstmult[i],
                        cermax
                        , cutmean_ready	// 20190420, yhsuk
                    );

                    if (bcount > 0) {
                        break;
                    }
                }
            } else {
                iana_getballcandidatorAdapt
                (piana, camidReady, procimg, widthResize, heightResize, widthResize, &bc[0], &bcount,
                    HALFWINSIZE_CHECKREADY,
                    READY_MULTV, READY_EXPN,
                    THSTDMULT_,
                    cermax
                    , cutmean_ready	// 20190420, yhsuk
                );
            }

#define USECANNY
#if defined(USECANNY)
            if (piana->camsensor_category != CAMSENSOR_CONSOLE1 && piana->camsensor_category != CAMSENSOR_Z3&&piana->camsensor_category != CAMSENSOR_EYEXO) {
                if (bcount == 0) {

                    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "before CANNY.\n");

                    iana_getballcandidatorCanny(
                        piana, camidReady, procimg,
                        widthResize, heightResize,
                        widthResize,
                        &bc[0], &bcount,
                        100,			// fake
                        10,			// fake
                        READY_MULTV, READY_EXPN,
                        cermax);
                    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "After CANNY.\n");
                }
            }
#endif

            if (bcount > 0) {
                double mindistmean, mindiststddev, mindistmeanH, mindiststddevH;

                mindistmean		= 0;
                mindiststddev	= 0;
                mindistmeanH	= 0;
                mindiststddevH	= 0;
                for (i = 0; i < bcount; i++) {
                    if (bc[i].cexist == IANA_BALL_EXIST) {
                        if (bc[i].cer > cermax || bc[i].cr < crmin || bc[i].cr > crmax) {	// Filtering..
                            bc[i].cexist = IANA_BALL_EMPTY;
                            continue;
                        } else {
                            point_t Pp, Lp;
                            double dist;

                            //-- Converse to Pixel point.
                            bc[i].P.x = (bc[i].P.x / xfact) + (rectArea.left - pic->offset_x) + pic->offset_x;

#define MULTITUDE_FRACTION
#if defined(MULTITUDE_FRACTION)
                            bc[i].P.y = (((bc[i].P.y+0.5) * multitude)/ yfact) + (rectArea.top - pic->offset_y)  + pic->offset_y;
#else
                            bc[i].P.y = (bc[i].P.y / (yfact/multitude)) + (rectArea.top - pic->offset_y)  + pic->offset_y;
#endif
                            bc[i].cr /= xfact;

                            //-- Converse to Local point.
                            Pp.x = bc[i].P.x;
                            Pp.y = bc[i].P.y;
                            iana_P2L(piana, camidReady, &Pp, &Lp, 0);
                            bc[i].L.x = Lp.x;
                            bc[i].L.y = Lp.y;
                            dist = DDIST(pic->icp.L.x, pic->icp.L.y, bc[i].L.x, bc[i].L.y);
                            if (dist < mindist) {
                                mindist = dist;
                                mindistIndex = i;
                                mindistLx = bc[i].L.x;
                                mindistLy = bc[i].L.y;

                                mindistmean = bc[i].mean;
                                mindiststddev = bc[i].stddev;
                                mindistmeanH = bc[i].meanH;
                                mindiststddevH = bc[i].stddevH;
                            }
                        }
                    } 		// if (bc[i].cexist) 
                } 			// for (i = 0; i < bcount; i++) 

                if (mindistIndex >= 0) {
                    pic->icp.mean 	= mindistmean;
                    pic->icp.stddev = mindiststddev;
                    pic->icp.meanH 	= mindistmeanH;
                    pic->icp.stddevH = mindiststddevH;
                    //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] mindistmean: %lf\n", camidReady, mindistmean);
                }
            } 				// if (bcount > 0) 
        } 					// 3) Get Ball positions, if any.

        // Check ball moving or disappear..

#define MAXMINDIST		0.02
#define MAXMINDIST_TEE	0.01
#define MAXMINDIST_F	0.005
#define RUNINDEX_MERCY	10
        pic->rindexshot = rindex;
        pic->mshot 		= m;
        pic->ts64shot = ts64Mt;
        pic->ts64shot0 = ts64Mt;

        {				// Check ball disappear..
            U32 isInTeeflag;
            U32 goodball;
            double maxmindist;

            //----
            balldisappeared = 0;

            isInTeeflag = iana_isInTee(piana, &pic->icp.b3d);

            if (piana->opmode == IANA_OPMODE_FILE) {
                maxmindist = MAXMINDIST_F;
            } else {
                if (isInTeeflag) {
                    maxmindist = MAXMINDIST_TEE;
                } else {
                    maxmindist = MAXMINDIST;
                }
            }

            if (mindistIndex < 0 || mindist > maxmindist) {
                U32 unindexcount_mercy;

                if (piana->opmode == IANA_OPMODE_FILE) {
                    unindexcount_mercy = 0;
                } else {
                    unindexcount_mercy = RUNINDEX_MERCY;
                }

                if (piana->runindexcount >= unindexcount_mercy) { // Ball disappear!~!
                    balldisappeared = 1;
                    doitmore = 1;
                    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ts64: %016llx (%lf), pic->ts64shot (Mt): %016llx(%lf), pic->rindexshot: %d  pic->m: %d\n",
                        ts64,
                        (double)ts64/TSSCALE_D,
                        pic->ts64shot,
                        (double)pic->ts64shot/TSSCALE_D,
                        pic->rindexshot, pic->mshot
                    );
                }
                goodball = 0;
            } else {
                if (piana->readymercycount != 0) {
                    // error/warning messages?
                }
                piana->readymercycount = 0;
                goodball = 1;
                piana->goodreadytick = cr_gettickcount();
            }

            //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d #3 m: %d\n", __LINE__, m);
            {
                U32 w, h;
                char *pref;
                char *pimg;
                double dbr;
                I32 ioffset;

                double dsum;
                int value;
                int icount;
                I32 i, j;

                //---
                UNUSED(icount);
                dbr = pic->icp.cr/xfact;
                pref = (char *)&pic->BallImg[0];
                pimg = (char *)procimg;

#define REFWIDTHMULT 	2
#define REFHEIGHTMULT 	2
                w = pic->biw;
                h = pic->bih;

                w = (w + 3) & 0xFFFFFFFC;			// 4byte align
                h = (h + 3) & 0xFFFFFFFC;			// 4byte align
                ioffset = (ADDY-(I32)dbr) * widthResize + (ADDX-(I32)dbr);
                if (ioffset < 0) ioffset = 0;

                // Check reference..			TODO:  check dsum of reference image and screening... 
                //				dsum = 0;
                //				for (i = 0; i < h; i++) {
                //					for (j = 0; j < w; j++) {
                //						value= (int)((U08)*(pref + i*w+j));
                //						dsum = dsum + value * value;
                //					}
                //				}
                //				dsum = sqrt(dsum / (h * w));
                //				cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ref dsum: %lf\n", dsum);

                if (piana->opmode != IANA_OPMODE_FILE) {
                    I32 ox, oy;
                    I32 iw, ih;
                    ox = (I32)(w/2 - dbr);
                    oy = (I32)(h/2 - dbr);
                    iw = (I32)dbr*2;
                    ih = (I32)dbr*2;
                    if (ox < 0) { ox = (I32)0; }
                    if (oy < 0) { oy = (I32)0; }
                    if (balldisappeared) {
                        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] w, h: (%d, %d),  dbr: %lf, ioffset; %d\n", camidReady, w, h, dbr, ioffset);

                        // Check reference..
                        dsum = 0;
                        //						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "calc dsum1\n");

                        for (i = 0; i < ih; i++) {
                            for (j = 0; j < iw; j++) {
                                value= (int)((U08)*(pref + (i+oy)*w+(j+ox)));
                                dsum = dsum + value * value;
                            }
                        }
                        dsum = sqrt(dsum / (ih * iw));

                        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "  dsum = %lf\n", dsum);
                        {
                            FILE *fp;
                            U32 mode;
                            U32 revivevalue;
                            double revivevalue_ref;
                            iana_cam_t	*pic;
                            U32 isIron;

                            //-----
                            pic 	= piana->pic[camidReady];

#define DSUM_REVIVE	40
#define DSUM_VALID_REF	40
#define DSUM_VALID_REF_TEE	50
#define DSUM_REVIVE_TEE		20

#define DSUM_REVIVE_Z3			20			// for Z3			
#define DSUM_VALID_REF_Z3		30		// for Z3
#define DSUM_VALID_REF_TEE_Z3	50
#define DSUM_REVIVE_TEE_Z3		15

#define DSUM_REVIVE_EYEXO			25	
#define DSUM_VALID_REF_EYEXO		30
#define DSUM_VALID_REF_TEE_EYEXO	50
#define DSUM_REVIVE_TEE_EYEXO		25


#define BALLPOS_HEIGHT_TEE	0.03
                            if (pic->icp.b3d.z < BALLPOS_HEIGHT_TEE) {
                                isIron = 1;
                            } else {
                                isIron = 0;		// DRIVER...... 
                            }

                            if (piana->camsensor_category == CAMSENSOR_Z3) {
                                if (isIron == 0) {
                                    revivevalue_ref = DSUM_VALID_REF_TEE_Z3;
                                    revivevalue = DSUM_REVIVE_TEE_Z3;
                                } else {
                                    revivevalue_ref = DSUM_VALID_REF_Z3;
                                    revivevalue = DSUM_REVIVE_Z3;
                                }
                            } else if (piana->camsensor_category == CAMSENSOR_EYEXO) {
                                if (isIron == 0) {
                                    revivevalue_ref = DSUM_VALID_REF_TEE_EYEXO;
                                    revivevalue = DSUM_REVIVE_TEE_EYEXO;
                                } else {
                                    revivevalue_ref = DSUM_VALID_REF_EYEXO;
                                    revivevalue = DSUM_REVIVE_EYEXO;
                                }
                            } else if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
                                if (isIron == 0) {
                                    revivevalue_ref = DSUM_VALID_REF_TEE;
                                    revivevalue = 20;
                                } else {
                                    revivevalue_ref = DSUM_VALID_REF;
                                    revivevalue = 25;
                                }
                            } else {
                                if (isIron == 0) {
                                    revivevalue_ref = DSUM_VALID_REF_TEE;
                                    revivevalue = DSUM_REVIVE_TEE;
                                } else {
                                    revivevalue_ref = DSUM_VALID_REF;
                                    revivevalue = DSUM_REVIVE;
                                }
                            }

                            UNUSED(mode); UNUSED(fp);
#if 1						// 20191217.. REVIVE
                            if (dsum > revivevalue_ref /*DSUM_VALID_REF*/) {
                                double dsum1;
                                dsum1 = 0.0;
                                icount = 0;
                                for (i = 0; i < /*(U32)*/ ih; i++) {
                                    for (j = 0; j < /*(U32)*/ iw; j++) {
                                        value= (int)((U08)*(pref + (i+oy)*w+(j+ox))) - (int)((U08)*(pimg + (i+oy) * widthResize + (j+ox)));
                                        if (value > 0) {
                                            dsum1 = dsum1 + value * value;
                                        }
                                    }
                                }
                                dsum1 = sqrt(dsum1 / (ih * iw));

                                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "dsum(%lf, %lf, %d)   =   %lf\n", dsum, (double)revivevalue, piana->readymercycount, dsum1);

                                mycircleGray((int)(ox+dbr), (int)(ox+dbr), (int)dbr, 0xFF, (U08 *)pimg, widthResize);

                                if (dsum1 < revivevalue) {
                                    balldisappeared = 0;			// REVIVE!!
                                    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " REVIVE!!!\n");
                                    piana->readymercycount = 0;
                                } else {
                                    piana->readymercycount++;
#define READYMERCYCOUNT	2
                                    if (piana->readymercycount > READYMERCYCOUNT) {
                                        balldisappeared = 1;
                                        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---- to trans!!\n");
                                    } else {
                                        balldisappeared = 0;
                                    }
                                }
                            } else {
                                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "dsum: %lf, revivevalue_ref: %lf\n", dsum, revivevalue_ref);
                            }
#endif

                        }
                        if (piana->himgfunc) {
                            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)(pimg, widthResize, heightResize, 0 /*4*/, 1, 2, 3, piana->imguserparam);
                        }
                        if (piana->himgfunc) {
                            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)(pref, w, h, 1 /*5*/, 1, 2, 3, piana->imguserparam);
                        }
                    } else {
                        if (goodball) {

                            I32 biw, bih;
                            biw = widthResize;
                            bih = heightResize;

                            if (biw > BALLIMAGE_REF_SIZE) {
                                biw = BALLIMAGE_REF_SIZE;
                            }
                            if (bih > BALLIMAGE_REF_SIZE) {
                                bih = BALLIMAGE_REF_SIZE;
                            }

                            memcpy(pref, pimg, biw * bih);
                            pic->biw = biw;
                            pic->bih = bih;


#define BALLPOSITION_TEE_UPDATERATE		0.2
#define BALLPOSITION_IRON_UPDATERATE	0.02

                            if (piana->camsensor_category == CAMSENSOR_EYEXO) {
                                // what?
                            } else {
                                m++;				// 20181128
                            }
                        } // if (goodball) 
                    }
                } else {
                    balldisappeared = 1;
                }
            }
        }

        //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d #4 m: %d\n", __LINE__, m);
//#define RECALC_SHOTTS
        if (balldisappeared) { // Ball disappear!~!
            U32 i;
            U32 camidO;
            U32 rindexO;
            U64 ts64Ofit;
            U64 ts64Ofrom;
            U64 ts64Oto;

            //--
//			camidO = 1 - camidReady;		// the other camid
            camidO = piana->camidOther;		// the other camid
            ts64Ofit = 0;
            rindexO = 0;

            if (piana->opmode == IANA_OPMODE_FILE) {
                rindexO  = piana->info_startindex[camidO];	// rindex
                ts64Ofit = piana->info_startts64[camidO];	// 
            } else {
#if defined(RECALC_SHOTTS)
                for (i = 0; i < 5; i++) {
                    U64 ts64MtO;

                    ts64MtO = iana_ts64_from2to(piana, camidReady, camidO, ts64Mt);
                    res = scamif_imagebuf_timestamprangeEx(piana->hscamif, camidO, 1, ts64MtO,
                        &rindexO, &ts64Ofit, &ts64Ofrom, &ts64Oto);
                    if (res == 1) {
                        break;
                    }
                    cr_sleep(5);
                }
                if (res == 0) {
                    ts64Ofit = 0;
                    rindexO  = 0;
                }
#else
                UNUSED(ts64Ofrom);
                UNUSED(ts64Oto);

                ts64Ofit = ts64Mt;

                {
                    U32  currentindex;
                    U32  rindex;
                    U64 ts64_;
                    camif_buffer_info_t *pb;


                    res = scamif_imagebuf_peek_latest(
                        piana->hscamif, camidO, NORMALBULK_NORMAL,
                        &pb, 			// camif_buffer_info_t **ppb
                        NULL, 		// U08 **pbuf
                        &currentindex);	// Get Latest image buffer and info. Preserve read-index

                    if (res == 0 || pb == NULL) {			// check no data.. cause of crash.. 20171030
                        continue;
                    }
                    ts64_ = MAKEU64(pb->ts_h, pb->ts_l);
#define CAMTSMARGIN   ((U64) ((0.0002 * TSSCALE_D)))
                    rindex = currentindex;
                    if (ts64_ - CAMTSMARGIN < ts64Mt) {
                        rindex = currentindex;
                    } else {
                        //#define SEARCHRANGE	32
#define SEARCHRANGE	32
//						for (i = 1; i < 10; i++) 
                        for (i = 1; i < SEARCHRANGE; i++) {
                            res = scamif_imagebuf_trimindex2(piana->hscamif, camidO, 1, (I32)(currentindex-i), &rindex);
                            res = scamif_imagebuf_randomaccess(piana->hscamif, camidO, NORMALBULK_NORMAL, rindex, &pb, NULL);

                            ts64_ = MAKEU64(pb->ts_h, pb->ts_l);

                            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%2d:%3d> %llx (%lf) vs %llx (%lf)\n",
                                i, rindex,
                                ts64Mt, (double)ts64Mt/TSSCALE_D,
                                ts64_, (double)ts64_/TSSCALE_D);
                            if (ts64_ - CAMTSMARGIN < ts64Mt) {
                                break;
                            }
                        }
                    }
                    rindexO = rindex;
                }
#endif
            }

            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " camidO[%d]ts64shot : %llx -> %llx, rindex: %d -> %d\n",
                camidO, piana->pic[camidO]->ts64shot, ts64Ofit, piana->pic[camidO]->rindexshot, rindexO);

            piana->pic[camidO]->ts64shot   = ts64Ofit;
            piana->pic[camidO]->rindexshot = rindexO;

            piana->runstate = IANA_RUN_TRANSITORY;
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Ball Disappeared!![%d], ts: %016llx (%lf), rindex: %d\n",
                m,
                ts64Mt,
                ts64Mt/TSSCALE_D, rindex);
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "                     O  ts: %016llx (%lf), rindex: %d  (%lf)\n",
                ts64Ofit,
                ts64Ofit/TSSCALE_D,
                rindexO, (piana->ts64delta / TSSCALE_D));
//            OutputDebugStringA("Ball Disappeared..\n");

            if (piana->himgfunc) {
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)procimg, widthResize, heightResize, 0, 1, 2, 3, piana->imguserparam);
            }

            break;
        }

        if (piana->camsensor_category == CAMSENSOR_EYEXO) {
            //
        } else {
            if (doitmore == 0) {
                m++;
                //break;
            }
        }
    } 		// for (m = 0; m < multitude; m++) 
    //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d #5 m: %d\n", __LINE__, m);
    //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" m: %d\n", m);

    if (balldisappeared || piana->opmode == IANA_OPMODE_FILE) { // Ball disappeared.. or FILE mode
        int ii;
        for (ii = 0; ii < (NUM_CAM - 1); ii++)				// Other cam and attack cam. .:P
        {
            I32 i;
            U32 camid;
            U32 rindex;
            U32 startindex;
            U32 updatedone;
            point_t LpO, PpO;
            HAND hscamifOA;

            //----

            if (ii == 0) {		// The other cam.
                camid = camidOther;		// the other camid
                hscamifOA = piana->hscamif;
                LpO.x = piana->pic[camidOther]->icp.L.x;
                LpO.y = piana->pic[camidOther]->icp.L.y;
            } else {	// ii == 1
                // TODO: process for remaing CAMs if needed, but now you shall not be here!!
                camid = 0;	// just to prevent compile warning
            }
            iana_L2P(piana, camid, &LpO, &PpO, 0);
            pic   =piana->pic[camid];


            if (piana->opmode == IANA_OPMODE_FILE) {				// File mode
                startindex = piana->info_startindex[camid];
            } else {												// Realtime mode
                if (ii == 0) {		// Other cam.
                    res = scamif_imagebuf_peek_latest(
                        piana->hscamif, camid, NORMALBULK_NORMAL,
                        NULL, 		// camif_buffer_info_t **ppb
                        NULL, 		// U08 **pbuf
                        &startindex);	// Get Latest image buffer and info. Preserve read-index
                    if (res == 0) {
                        continue;
                    }
                } else {
                    // TODO: process for remaing CAMs if needed, but now you shall not be here!!
                    startindex = 0; // just to prevent compile warning
                }
            }
            //--

#define PRESHOTINDEX	5
            updatedone = 0;
            for (i = -PRESHOTINDEX; i < 0; i++) {
                double bsmin, bsmax;
                point_t Lp0;
                point_t Pp0;

                int width, height, widthROI, heightROI, widthResize, heightResize;
                int l, r, t, b; // left, right, top, bottom

                //------------------- Get Data buffer from imagebuf
                if (ii == 0) {		// Other cam.
                    res = scamif_imagebuf_trimindexdelta(piana->hscamif, camid, 1, startindex, i, &rindex);
                } else {
                    // TODO: process for remaing CAMs if needed, but now you shall not be here!!
                    rindex = 0; // just to prevent compile warning
                }

                if (res) {
                    if (ii == 0) {		// Other cam.
                        res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_NORMAL, rindex, &pb, &buf);
                    } else {
                        // TODO: process for remaing CAMs if needed, but now you shall not be here!!
                    }
                }

                if (res == 0) {
                    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "rindex recalc fail. camid: %d\n", camid);
                    continue;
                }

                pcii = &pb->cii;
                multitude 	= pcii->multitude;
                if (multitude == 0) {
                    multitude = 1;
                }

                xfact 	= XYFACT;
                yfact 	= (XYFACT * multitude);

                //------------------- 
                procimg = pic->pimgFull;

                Lp0.x = pic->icp.L.x;				//
                Lp0.y = pic->icp.L.y;

                iana_L2P(piana, camid, &Lp0, &Pp0, 0);

                rectArea.left 	= (long)(Pp0.x - ADDX+1);
                rectArea.right 	= (long)(Pp0.x + ADDX);
                rectArea.top 	= (long)(Pp0.y - ADDY+1);
                rectArea.bottom = (long)(Pp0.y + ADDY);

                //-
                m = 0;
                l = (int)(rectArea.left 	- pic->offset_x);
                r = (int)(rectArea.right 	- pic->offset_x);
                t = (int)((rectArea.top 	- pic->offset_y) / multitude + ((m*pcii->height) / multitude));
                b = (int)((rectArea.bottom	- pic->offset_y) / multitude + ((m*pcii->height) / multitude));

                width 	= pcii->width;
                height 	= pcii->height;

                l = l < 0 ? 0 : l;
                t = t < 0 ? 0 : t;
                r = width < r+1 ? width - 1 : r;
                b = height < b+1 ? height - 1 : b;

                //		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "W: %d, H: %d\n", ssize.width, ssize.height);
                widthROI 	= r - l + 1;
                heightROI	= b - t + 1;

                if( widthROI <= 0 || heightROI <= 0)
                    continue;

                widthResize 	= (int)(widthROI * xfact + 0.001);
                heightResize	= (int)(heightROI * yfact + 0.001);

                if ((width > FULLWIDTH || height > FULLHEIGHT)) {
                    res = 0;
                    piana->runstate = IANA_RUN_TRANSITORY;
                    goto func_exit;
                }

                {
                    cv::Mat cvImgSrc(cv::Size(width, height), CV_8UC1, buf), cvImgCropped, cvImgResized;

                    cvImgCropped = cvImgSrc(cv::Rect(l, t, widthROI, heightROI));

                    cv::resize(cvImgCropped, cvImgResized, cv::Size(widthResize, heightResize), xfact, yfact, cv::INTER_CUBIC);

                    memcpy(procimg, cvImgResized.data, sizeof(U08) * cvImgResized.cols * cvImgResized.rows);
                }

                if (piana->himgfunc) {
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)procimg, widthResize, heightResize, 0, 1, 2, 3, piana->imguserparam);
                }

                iana_setReadyRunBallsizeMinMax(piana, camid, xfact);
                bsmin = piana->pic[camid]->icp.RunBallsizePMin;
                bsmax = piana->pic[camid]->icp.RunBallsizePMax;

                iana_getballcandidatorAdapt(
                    piana, camid, procimg,
                    widthResize, heightResize, widthResize,
                    &bc[0], &bcount,
                    4, 	// HALFWINSIZE_CHECKREADY,
                    1, 0,	// READY_MULTV, READY_EXPN,
                    1, 	// THSTDMULT_,
                    0.5,   // 0.3  /*1.0*/ /*cermax*/
                    cutmean_ready	// 20190420, yhsuk
                );

                if (bcount > 0) {
                    I32 i;
                    point_t Lpi;
                    point_t Ppi;
                    double mindist;
                    I32    minindex;
                    point_t minLp;
                    point_t minPp;
                    double dx, dy, dd;
                    //				cr_rectL_t rectRunL;
                    double ballradiusMax;
                    double ballradiusMin;

                    //---
                    mindist = 999;
                    minindex = -1;
                    minLp.x = Lp0.x;
                    minLp.y = Lp0.y;
                    minPp.x = Pp0.x;
                    minPp.y = Pp0.y;

                    ballradiusMax = (bsmax / 2.0) * 1.2;
                    ballradiusMin = (bsmin / 2.0) * 0.7;

                    for (i = 0; i < (I32)bcount; i++) {
                        Ppi.x = (bc[i].P.x / xfact) + (rectArea.left - pic->offset_x) + pic->offset_x;
#if defined(MULTITUDE_FRACTION)
                        Ppi.y = ((bc[i].P.y+0.5) * multitude) / yfact + (rectArea.top - pic->offset_y)  + pic->offset_y;
#else
                        Ppi.y = (bc[i].P.y / (yfact/multitude)) + (rectArea.top - pic->offset_y)  + pic->offset_y;
#endif


                        //iana_P2L(piana, camidReady, &Ppi, &Lpi);
                        if (bc[i].cr > ballradiusMax || bc[i].cr  < ballradiusMin) {
                            break;
                        }
                        iana_P2L(piana, camid, &Ppi, &Lpi, 0);
                        dx = Lpi.x - Lp0.x;
                        dy = Lpi.y - Lp0.y;
                        dd = sqrt(dx*dx + dy*dy);
                        if (mindist > dd) {
                            mindist = dd;
                            minindex = i;
                            minLp.x = Lpi.x;
                            minLp.y = Lpi.y;
                            minPp.x = Ppi.x;
                            minPp.y = Ppi.y;
                        }
                    }

#define MINDDDIST	0.10
                    if (minindex >= 0) {

                        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "~[%d]  %d dd:%10lf C1(%10lf %10lf) -> C0(%10lf %10lf)  P(%10lf %10lf)\n", i,
                            minindex, mindist,
                            pic->icp.L.x, pic->icp.L.y,
                            minLp.x, minLp.y, minPp.x, minPp.y);
                        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "~       C0-C1(%10lf %10lf)\n", minLp.x-pic->icp.L.x, minLp.y-pic->icp.L.y);


                        if (mindist < MINDDDIST) {
                            //iana_L2P(piana, camidReady, &minLp, &minPp);
//							iana_L2P(piana, camid, &minLp, &minPp);
                            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]  %d dd:%10lf C1(%10lf %10lf) -> C0(%10lf %10lf)  P(%10lf %10lf)\n", i,
                                minindex, mindist,
                                pic->icp.L.x, pic->icp.L.y,
                                minLp.x, minLp.y, minPp.x, minPp.y);
                            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "       C0-C1(%10lf %10lf)\n", minLp.x-pic->icp.L.x, minLp.y-pic->icp.L.y);

                            LpO.x = minLp.x;				//
                            LpO.y = minLp.y;
                            PpO.x = minPp.x;				//
                            PpO.y = minPp.y;
                            updatedone = 1;
                            break;
                        }
                    }
                } 	// if (bcount > 0) 
                if (updatedone) {
                    break;
                }
            } 			// for (i = -PRESHOTINDEX; i < 0; i++) 

            pic->icp.L.x = LpO.x;				//
            pic->icp.L.y = LpO.y;
            pic->icp.P.x = PpO.x;				//
            pic->icp.P.y = PpO.y;
        } 	// for (ii = 0; ii < 2; ii++)				// Other cam and attack cam. .:P

        for (camid = 0; camid < NUM_CAM; camid++) {
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "camid[%d]  L(%10lf %10lf)  P(%10lf %10lf)\n", camid,
                piana->pic[camid]->icp.L.x,
                piana->pic[camid]->icp.L.y,
                piana->pic[camid]->icp.P.x,
                piana->pic[camid]->icp.P.y);
        }
    } 	// if (piana->runstate == IANA_RUN_READY || piana->opmode == IANA_OPMODE_FILE)  // Update 
    else { // BALL GOOD..
        static int dodecimation = 0;

        // Check Othercam..
        //camidOther = piana->camidOther;
        pic   =piana->pic[camidOther];
#define DODECIMATIONCOUNT	10	
        if ((++dodecimation % DODECIMATIONCOUNT) == 0) {
            //cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__ " start of  decimation\n");
            dodecimation = 0;
            if (piana->opmode == IANA_OPMODE_FILE) {
                rindex = piana->info_startindex[camidOther];			// rindex
                res = scamif_imagebuf_randomaccess(piana->hscamif, camidOther, NORMALBULK_NORMAL, rindex, &pb, &buf);
                pcii = &pb->cii;

                if (piana->shotarea		== IANA_AREA_PUTTER) {
                    piana->processarea	 = IANA_AREA_PUTTER;
                }

                if (res != 0 && buf != NULL) {
                    if (piana->himgfunc) {
                        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)(
                            (char *)buf, pcii->width, pcii->height, 0, 1, 2, 3, piana->imguserparam);
                    }
                }
            } else {
                res = scamif_imagebuf_peek_latest(piana->hscamif, camidOther, NORMALBULK_NORMAL, NULL, NULL, NULL);
                if (res) {		// If there is any available frame.
                    res = scamif_imagebuf_read_latest(piana->hscamif, camidOther, NORMALBULK_NORMAL, &pb, &buf, &rindex);
                    pcii = &pb->cii;
                }
            }
            if (res == 0) {	// No image data yet.
                goto func_exit;
            }

            multitude 	= pcii->multitude;
            if (multitude == 0) {
                multitude = 1;
            }


            if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
                xfact 		= XYFACT_CONSOLE1;
                yfact 		= (XYFACT_CONSOLE1 * multitude);
            } else {
                xfact 		= XYFACT;
                yfact 		= (XYFACT * multitude);
            }
            
            procimg = pic->pimgFull;
            
            {
                point_t Lp;
                point_t Pp;

                //--
                Lp.x = pic->icp.L.x;				// Center..
                Lp.y = pic->icp.L.y;

                iana_L2P(piana, camidOther, &Lp, &Pp, 0);

                rectArea.left 	= (long)(Pp.x - ADDX+1);
                rectArea.right 	= (long)(Pp.x + ADDX);
                rectArea.top 	= (long)(Pp.y - ADDY+1);
                rectArea.bottom = (long)(Pp.y + ADDY);


                rectLArea.sx 	= (double)(Pp.x - ADDX+1);
                rectLArea.ex 	= (double)(Pp.x + ADDX);
                rectLArea.sy 	= (double)(Pp.y - ADDY+1);
                rectLArea.ey 	= (double)(Pp.y + ADDY);

                {
                    long l0;
                    l0 = rectArea.top;
                    l0 = ((l0 + 3) / 4) * 4;
                    rectArea.top = l0;
                    l0 = rectArea.bottom;
                    l0 = ((l0 + 3) / 4) * 4;
                    rectArea.bottom = l0;
                }
            }

            m = 0;		// :P
            {
                double mindist;
                double mindistLx, mindistLy;
                int mindistIndex;
                double mindistmean, mindiststddev, mindistmeanH, mindiststddevH;

                int width, height, widthROI, heightROI, widthResize, heightResize;
                int l, r, t, b; // left, right, top, bottom
                ts64Mt = ts64 - (periodm * (multitude - m -1));

                l = (int)(rectLArea.sx 	- pic->offset_x);
                r = (int)(rectLArea.ex 	- pic->offset_x);
                t = (int)((rectArea.top 	- pic->offset_y) / multitude + ((pcii->height *m)/ multitude)); //  + m * 2;
                b = (int)((rectArea.bottom	- pic->offset_y) / multitude + ((pcii->height *m)/ multitude)); //  + m * 2;

                width 	= pcii->width;
                height 	= pcii->height;

                l = l < 0 ? 0 : l;
                t = t < 0 ? 0 : t;
                r = width < r+1 ? width - 1 : r;
                b = height < b+1 ? height - 1 : b;

                widthROI 	= r - l + 1;
                heightROI	= b - t  + 1;

                if (widthROI <= 0 || heightROI <= 0) {
                    res = 0;
                    piana->runstate = IANA_RUN_TRANSITORY;
                    goto func_exit;
                }

                mindistmean		= 0;
                mindiststddev	= 0;
                mindistmeanH	= 0;
                mindiststddevH	= 0;

                if ((width > FULLWIDTH || height > FULLHEIGHT)) {
                    res = 0;
                    piana->runstate = IANA_RUN_TRANSITORY;
                    goto func_exit;
                }

                widthResize 	= (int)(widthROI * xfact + 0.001);
                heightResize	= (int)(heightROI * yfact + 0.001);

                {
                    cv::Mat cvImgSrc(cv::Size(width, height), CV_8UC1, buf), cvImgCropped, cvImgResized;

                    cvImgCropped = cvImgSrc(cv::Rect(l, t, widthROI, heightROI));

                    cv::resize(cvImgCropped, cvImgResized, cv::Size(widthResize, heightResize), xfact, yfact, cv::INTER_CUBIC);

                    memcpy(procimg, cvImgCropped.data, sizeof(U08) * cvImgCropped.cols * cvImgCropped.rows);
                }

                mindist = 1.0e10;
                mindistIndex = -1;
                mindistLx = pic->icp.L.x;
                mindistLy = pic->icp.L.y;

                {
                    double cermax;
                    double crmin;
                    double crmax;
                    double crminratio;
                    double crmaxratio;
                    U32 i;

                    iana_setReadyRunBallsizeMinMax(piana, camidOther, xfact);

                    crmaxratio = IANA_BALLDETECT_GetMaxCircleRatio(piana->camsensor_category, camidOther);
                    crminratio = IANA_BALLDETECT_GetMinCircleRatio(piana->camsensor_category, camidOther);

                    crmin = (piana->pic[camidOther]->icp.RunBallsizePMin / 2.0) * crminratio;
                    crmax = (piana->pic[camidOther]->icp.RunBallsizePMax / 2.0) * crmaxratio;

                    cermax = pic->icp.CerBallexist;

                    memset(bc, 0, sizeof(bc));
                    bcount = 0;
#define CERMAX_MULT_CAMOTHER	2
                    iana_getballcandidatorAdapt
                    (piana, camidOther, procimg, widthResize, heightResize, widthResize, &bc[0], &bcount,
                        HALFWINSIZE_CHECKREADY,
                        READY_MULTV, READY_EXPN,
                        THSTDMULT_,
                        cermax * CERMAX_MULT_CAMOTHER
                        , cutmean_ready	// 20190420, yhsuk
                    );

                    if (bcount > 0) {
                        for (i = 0; i < bcount; i++) {
                            if (bc[i].cexist == IANA_BALL_EXIST) {
                                if (bc[i].cer > cermax * CERMAX_MULT_CAMOTHER|| bc[i].cr < crmin || bc[i].cr > crmax) {	// Filtering..
                                    bc[i].cexist = IANA_BALL_EMPTY;
                                    continue;
                                } else {
                                    point_t Pp, Lp;
                                    double dist;

                                    //-- Converse to Pixel point.
                                    bc[i].P.x = (bc[i].P.x / xfact) + (rectArea.left - pic->offset_x) + pic->offset_x;
#if defined(MULTITUDE_FRACTION)
                                    bc[i].P.y = (bc[i].P.y+0.5) * multitude / yfact + (rectArea.top - pic->offset_y)  + pic->offset_y;
#else
                                    bc[i].P.y = (bc[i].P.y / (yfact/multitude)) + (rectArea.top - pic->offset_y)  + pic->offset_y;
#endif
                                    bc[i].cr /= xfact;

                                    //-- Converse to Local point.
                                    Pp.x = bc[i].P.x;
                                    Pp.y = bc[i].P.y;
                                    iana_P2L(piana, camidOther, &Pp, &Lp, 0);
                                    bc[i].L.x = Lp.x;
                                    bc[i].L.y = Lp.y;
                                    dist = DDIST(pic->icp.L.x, pic->icp.L.y, bc[i].L.x, bc[i].L.y);
                                    if (dist < mindist) {
                                        mindist = dist;
                                        mindistIndex = i;
                                        mindistLx = bc[i].L.x;
                                        mindistLy = bc[i].L.y;

                                        mindistmean = bc[i].mean;
                                        mindiststddev = bc[i].stddev;
                                        mindistmeanH = bc[i].meanH;
                                        mindiststddevH = bc[i].stddevH;
                                    }
                                }
                            } 		// if (bc[i].cexist) 
                        } 			// for (i = 0; i < bcount; i++) 
                    } 				// if (bcount > 0) 
                } 					// 3) Get Ball positions, if any.


                if (mindistIndex >= 0) {
                    point_t Lp;
                    point_t Pp;
                    double lx, ly;
                    double updaterate;

                    updaterate =  BALLPOSITION_TEE_UPDATERATE;
                    lx = mindistLx * updaterate + pic->icp.L.x * (1.0 - updaterate);
                    ly = mindistLy * updaterate + pic->icp.L.y * (1.0 - updaterate);
                    //cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BallPos Other [%d:%d] (%10lf %10lf : %10lf %10lf) -> (%10lf %10lf)\n", 
                    //		camidOther, mindistIndex,
                    //		pic->icp.P.x, pic->icp.P.y, pic->icp.L.x, pic->icp.L.y, lx, ly);
                    pic->icp.L.x = lx;
                    pic->icp.L.y = ly;
                    Lp.x = lx;
                    Lp.y = ly;
                    iana_L2P(piana, camidOther, &Lp, &Pp, 0);

                    pic->icp.P.x = Pp.x;
                    pic->icp.P.y = Pp.y;

                    pic->icp.mean 	= mindistmean;
                    pic->icp.stddev = mindiststddev;
                    pic->icp.meanH 	= mindistmeanH;
                    pic->icp.stddevH = mindiststddevH;
                    //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] mindistmean: %lf\n", camidOther, mindistmean);
                    // TODO: Update B3d.. :P
                }
            } 	// m = 0;		// :P
        } 	// if ((++dodecimation % DODECIMATIONCOUNT) == 0) 

#define MERCY_PIXEL_DISTANCE	20
        checkreadyposition(piana, MERCY_PIXEL_DISTANCE);				// TODO: Check with time..
    } 		// BALL GOOD..

	res = 1;
func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res %d\n", res);
	return res;

#endif
}



I32 checkreadyposition(iana_t *piana, U32 mercy)
{
	U32 camidReady;
	U32 camidO;
	iana_cam_t	*pic;
	point_t Lp;
	point_t Pp;

	U32 ox, oy;
	double dx, dy;

	double distance;

	//----
	camidReady = piana->camidCheckReady;
	pic   =piana->pic[camidReady];

	Lp.x = pic->icp.L.x;
	Lp.y = pic->icp.L.y;

	Pp.x = pic->icp.P.x;
	Pp.y = pic->icp.P.y;

	iana_readyarea(piana, camidReady, Lp.x, Lp.y, &ox, &oy);

	dx = (double)pic->offset_x - (double)ox;
	dy = (double)pic->offset_y - (double)oy;

	distance = sqrt(dx*dx + dy*dy);

//#define MIN_ALLOW_DISTANCE	10
#define MIN_ALLOW_DISTANCE	20
	if (distance > MIN_ALLOW_DISTANCE) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%d) ox oy %d %d  vs %d %d   dist: %lf\n", camidReady, 
				ox, oy, pic->offset_x, pic->offset_y, distance);
	}
	if (distance > (double)mercy) {
		if (piana->checkreadypositioncount > 0) {

			piana->checkreadypositioncount++;
#define CHECKREADYPOSITIONCOUNT	5
			if (piana->checkreadypositioncount > CHECKREADYPOSITIONCOUNT) {
				double distance1;

				pic->offset_x = ox;
				pic->offset_y = oy;
				camidO = 1 - camidReady;
				pic    = piana->pic[camidO];

				iana_L2P(piana, camidO, &Lp, &Pp, 0);
				iana_readyarea(piana, camidO, Lp.x, Lp.y, &ox, &oy);

				dx = (double)pic->offset_x - (double)ox;
				dy = (double)pic->offset_y - (double)oy;

				distance1 = sqrt(dx*dx + dy*dy);

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%d) ox oy %d %d  vs %d %d   dist: R %lf  O %lf\n", camidO, 
						ox, oy, pic->offset_x, pic->offset_y, distance, distance1);

				pic->offset_x = ox;
				pic->offset_y = oy;

				piana->currentcamconf = IANA_CAMCONF_CHANGEME;
				iana_cam_reconfig(piana, 1);		// configure camera..
				piana->checkreadypositioncount = 0;
			}
		} else {
			piana->checkreadypositioncount = 1;
		}
	} else {
		piana->checkreadypositioncount = 0;
	}
	return 1;
}






#if defined (__cplusplus)
}
#endif

