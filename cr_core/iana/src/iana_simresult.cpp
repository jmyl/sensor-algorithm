/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA shot Information store
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2020 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_simresult.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2020/08/21 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <math.h>
#include <string.h>

#if defined(_WIN32)
#include <direct.h>
#endif
#include "cr_common.h"
//#include "cr_thread.h"
//#include "cr_osapi.h"
//#include "cr_regression.h"
#include "cr_nanoxml.h"

//#include "scamif.h"
//#include "scamif_buffer.h"

#include "iana.h"
//#include "iana_work.h"
//#include "iana_implement.h"
//#include "iana_tool.h"
//#include "iana_cam.h"
//#include "iana_checkready.h"
//#include "iana_balldetect.h"
//#include "iana_ready.h"
//#include "iana_info.h"
//#include "iana_shot.h"

#include "iana_simresult.h"
#include "iana_adapt.h"

#include "iana_traj.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define IANA_TRAJ_TIMEDELTA		(0.02)			// use this... 

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/

I32 gettraj(double vmag, double azimuth, double incline, double backspin, double sidespin, 
				double *pcarrydist, double *pofflinedist, double *pmaxheight, double *pdescentangle);
/*!
 ********************************************************************************
 *	@brief      Write simulation result
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuks
 *  @date       2020/0821
 *******************************************************************************/
I32 iana_simresult(iana_t *piana)
{
	I32 res;
	iana_shotresult_t *psr;
	iana_shotresult_t *psr2;

	double vmag;
	double azimuth;
	double incline;
	double backspin;
	double sidespin;
	double spinmag;
	double spinaxis;

	double carrydist, offlinedist, maxheight, descentangle;

	double clubspeed;
	double clubspeedAtImpact;
	double smashfactor;
	double attackangle;
	double clubpath;

	double lieangle;
	double dynamicloft;
	double faceaxis;
	double faceimpactHorizontal;
	double faceimpactVertical;

	double faceangle;
	double faceangleToPath;
	double closurerate;

	int clubspeed_flag;
	int clubspeedAtImpact_flag;
	int smashfactor_flag;
	int attackangle_flag;
	int clubpath_flag;

	int lieangle_flag;
	int dynamicloft_flag;
	int faceaxis_flag;
	int faceimpactHorizontal_flag;
	int faceimpactVertical_flag;

	int faceangle_flag;
	int faceangleToPath_flag;
	int closurerate_flag;

	//--------------
	if (strlen(piana->simresultfile) == 0) {
		res = 0;
		goto func_exit;
	}

	{
		psr  = &piana->shotresultdata;
		psr2 = &piana->shotresultdata2;

		//--

		// Ball data
		vmag = 0;
		azimuth = 0;
		incline = 0;
		backspin = 0;
		sidespin = 0;
		spinmag = 0;
		spinaxis = 0;

		// Traj data
		carrydist = 0;
		offlinedist = 0;
		maxheight = 0;
		descentangle = 0;

		// Club data
		clubspeed = 0;
		clubspeedAtImpact = 0;
		smashfactor = 0;
		attackangle = 0;
		clubpath = 0;

		lieangle = 0;
		dynamicloft = 0;
		faceaxis = 0;
		faceimpactHorizontal = 0;
		faceimpactVertical = 0;

		faceangle = 0;
		faceangleToPath = 0;
		closurerate = 0;

		clubspeed_flag = 0;
		clubspeedAtImpact_flag = 0;
		smashfactor_flag = 0;
		attackangle_flag = 0;
		clubpath_flag = 0;

		lieangle_flag = 0;
		dynamicloft_flag = 0;
		faceaxis_flag = 0;
		faceimpactHorizontal_flag = 0;
		faceimpactVertical_flag = 0;

		faceangle_flag = 0;
		faceangleToPath_flag = 0;
		closurerate_flag = 0;

		if (piana->shotresultflag) {	// GOOD..

			//-- Ball data
			vmag = psr->vmag;
			azimuth = psr->azimuth;
			incline = psr->incline;


			if (piana->camsensor_category == CAMSENSOR_EYEXO) {
				backspin = psr2->backspin;
				sidespin = psr2->sidespin;
			} else {
				backspin = psr->backspin;
				sidespin = psr->sidespin;
			}
			spinmag = sqrt(backspin*backspin + sidespin*sidespin);
			spinaxis = atan2(sidespin, backspin);
			spinaxis = spinaxis * 180.0 / 3.141592;			// radian to degree.

			//-- Trajectory data
			gettraj( vmag, azimuth, incline, backspin, sidespin, 
					&carrydist, &offlinedist, &maxheight, &descentangle);
#define GGG 49.999
			//-- Club data
			if (psr->Assurance_clubspeed_B > GGG) {
				clubspeed = psr->clubspeed_B; clubspeed_flag = 1;
				clubspeedAtImpact = clubspeed; clubspeedAtImpact_flag = 0;
				smashfactor = vmag / (clubspeed + 0.00000001); smashfactor_flag = 1;
			}
			if (psr->Assurance_clubattackangle > GGG) {
				attackangle = psr->clubattackangle; attackangle_flag = 1;
			}
			if (psr->Assurance_clubpath > GGG) {
				clubpath = psr->clubpath; clubpath_flag = 1;
			}


			if (psr->Assurance_clublieangle > GGG) {
				lieangle = psr->clublieangle; lieangle_flag = 1;
			}
			if (psr->Assurance_clubloftangle > GGG) {
				dynamicloft = psr->clubloftangle; dynamicloft_flag = 1;
			}
			{
				faceaxis = 0; faceaxis_flag = 0;
			}
			if (psr->Assurance_clubfaceimpactLateral > GGG) {
				faceimpactHorizontal = psr->clubfaceimpactLateral; faceimpactHorizontal_flag = 1;
			}
			if (psr->Assurance_clubfaceimpactVertical > GGG) {
				faceimpactVertical = psr->clubfaceimpactVertical; faceimpactVertical_flag = 1;
			}
			
			if (psr->Assurance_clubfaceangle > GGG) {
				faceangle = psr->clubfaceangle; faceangle_flag = 1;
				faceangleToPath = faceangle - clubpath;faceangleToPath_flag = 1;
			}

			{
				closurerate = 0; closurerate_flag = 0;
			}
		} else {
			// 0..
		}
	}

	//--------------
	cr_nanoxml_create2(piana->simresultfile, 0);

	nxmlie(0, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	nxmlie(1, "<simulation_result>");
	{
		//------------
		nxmlie(1, "<simul_info>");
		{
			CR_Time_t st;
			OSAL_SYS_GetLocalTime( &st);

			nxmlie(0, "<build_time>%s %s</build_time>", __DATE__, __TIME__);
			nxmlie(0, "<build_version>%d.%d.%d</build_version>", CAMSENSOR_MAJOR, CAMSENSOR_MINOR, CAMSENSOR_BUILD);
			nxmlie(0, "<run_time> %4d-%02d-%02d %02d:%02d:%02d </run_time>",
					(U32) st.wYear,
					(U32) st.wMonth,
					(U32) st.wDay,
					(U32) st.wHour,
					(U32) st.wMinute,
					(U32) st.wSecond);
		}
		nxmlie(-1, "</simul_info>");

		//------------
		nxmlie(1, "<shot_info>");
		{
			{	// Sensor Name
#define MAXSTR	1024
				char sensorname[MAXSTR];
				if (piana->camsensor_category == CAMSENSOR_CBALL) {
					sprintf(&sensorname[0], "ZCAM2X");
				} else if (piana->camsensor_category == CAMSENSOR_Z3) {
					sprintf(&sensorname[0], "ZCAM3");
				} else if (piana->camsensor_category == CAMSENSOR_EYEXO) {
					sprintf(&sensorname[0], "EYEXO");
				} else if (piana->camsensor_category == CAMSENSOR_P3V2) {
					sprintf(&sensorname[0], "P3V2");
				} else {
					sprintf(&sensorname[0], "OTHERSENSOR");
				}
				nxmlie(0, "<sensor_name>%s</sensor_name>", sensorname);
			}
#if defined(_WIN32)				
			{
				char dir[MAXSTR];
				char drive[MAXSTR];
				char wdir[MAXSTR];
				
				_splitpath_s(piana->readinfofile,
						drive, sizeof(drive),             // need drive
						dir, sizeof(dir),    // directory
						NULL, 0,             // 
						NULL, 0);      
				sprintf(wdir, "%s%s", drive, dir);
				nxmlie(0, "<datapath> %s </datapath>", wdir);
			}
#else
			nxmlie(0, "<datapath> %s </datapath>", piana->readinfofile);
#endif


			{	// shot guid
				char guidstr[64];
				CR_GUID *pguid;

				//--
				pguid = (CR_GUID *) &piana->shotguid;
				sprintf(guidstr,
						"%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X",
						(U32) pguid->Data1,  
						(U32) pguid->Data2,  
						(U32) pguid->Data3,  
						(U32) pguid->Data4[0],   
						(U32) pguid->Data4[1],   
						(U32) pguid->Data4[2],   
						(U32) pguid->Data4[3],   
						(U32) pguid->Data4[4],   
						(U32) pguid->Data4[5],   
						(U32) pguid->Data4[6],   
						(U32) pguid->Data4[7]
				   );
				nxmlie(0, "<guid>%s</guid>", guidstr);
			}
			nxmlie(0, "<time>%s</time>", piana->shottime);		// shot time

			nxmlie(0, "<goodshot>%d</goodshot>", piana->shotresultflag);
		}
		nxmlie(-1, "</shot_info>");

		nxmlie(1, "<balldata>");
		{
			nxmlie(0, "<vmag>%f</vmag>", vmag);
			nxmlie(0, "<azimuth>%f</azimuth>", azimuth);
			nxmlie(0, "<incline>%f</incline>", incline);
			nxmlie(0, "<backspin>%f</backspin>", backspin);
			nxmlie(0, "<sidespin>%f</sidespin>", sidespin);
			nxmlie(0, "<spinmag>%f</spinmag>", spinmag);
			nxmlie(0, "<spinaxis>%f</spinaxis>", spinaxis);
		}
		nxmlie(-1, "</balldata>");

		nxmlie(1, "<flightdata>");
		{
			nxmlie(0, "<carrydist>%f</carrydist>", carrydist);
			nxmlie(0, "<totaldist>%f</totaldist>", carrydist);
			nxmlie(0, "<offlinedist>%f</offlinedist>", offlinedist);
			nxmlie(0, "<maxheight>%f</maxheight>", maxheight);
			nxmlie(0, "<descentangle>%f</descentangle>", descentangle);
		}
		nxmlie(-1, "</flightdata>");

		nxmlie(1, "<clubdata>");
		{
			nxmlie(0, "<clubspeed valid=\"%d\">%f</clubspeed>", clubspeed_flag, clubspeed);
			nxmlie(0, "<clubspeedAtImpact valid=\"%d\">%f</clubspeedAtImpact>", clubspeedAtImpact_flag, clubspeedAtImpact);
			nxmlie(0, "<smashfactor valid=\"%d\">%f</smashfactor>", smashfactor_flag, smashfactor);
			nxmlie(0, "<attackangle valid=\"%d\">%f</attackangle>", attackangle_flag, attackangle);
			nxmlie(0, "<clubpath valid=\"%d\">%f</clubpath>", clubpath_flag, clubpath);

			nxmlie(0, "<lieangle valid=\"%d\">%f</lieangle>", lieangle_flag, lieangle);
			nxmlie(0, "<dynamicloft valid=\"%d\">%f</dynamicloft>", dynamicloft_flag, dynamicloft);
			nxmlie(0, "<faceaxis valid=\"%d\">%f</faceaxis>", faceaxis_flag, faceaxis);
			nxmlie(0, "<faceimpactHorizontal valid=\"%d\">%f</faceimpactHorizontal>", faceimpactHorizontal_flag, faceimpactHorizontal);
			nxmlie(0, "<faceimpactVertical valid=\"%d\">%f</faceimpactVertical>", faceimpactVertical_flag, faceimpactVertical);

			nxmlie(0, "<faceangle valid=\"%d\">%f</faceangle>", faceangle_flag, faceangle);
			nxmlie(0, "<faceangleToPath valid=\"%d\">%f</faceangleToPath>", faceangleToPath_flag, faceangleToPath);
			nxmlie(0, "<closurerate valid=\"%d\">%f</closurerate>", closurerate_flag, closurerate);

		}
		nxmlie(-1, "</clubdata>");
	}
	nxmlie(-1, "</simulation_result>");
	cr_nanoxml_delete(NULL, 0);

	res = 1;
func_exit:
	return res;
}


I32 gettraj(
		double vmag, double azimuth, double incline, double backspin, double sidespin, 
		double *pcarrydist, double *pofflinedist, double *pmaxheight, double *pdescentangle)
{
	I32 res;
	iana_traj_data_t   td, *ptd;
	iana_traj_result_t *ptr, tr;
	HAND htraj;

	U32 i;



	double x, y, z;
	double x0, y0, z0;
	double maxsideAbs, maxside;
	double maxh;
	double dangle;
	double carrydist;
	//--
	ptd = &td;
	ptr = &tr;
	htraj = iana_traj_scapegoat_create();


	ptd->vmag 		= (double) vmag;
	ptd->azimuth	= (double) azimuth;
	ptd->incline	= (double) incline;
	ptd->hitheight	= (double) 0;
	ptd->backspin	= (double) backspin;
	ptd->sidespin	= (double) sidespin;

	ptd->windmag	= (double) 0;
	ptd->windangle	= (double) 0;

	iana_traj_scapegoat_airpressure_altitude(0);


	iana_traj_scapegoat(htraj,	
			IANA_TRAJ_TIMEDELTA, //(0.01),			// IANA_TRAJ_TIMEDELTA, 
			ptd,
			ptr);
	iana_traj_scapegoat_delete(htraj);


	x0 = 0; 
	y0 = 0; 
	z0 = 0; 

	maxsideAbs = 0;
	maxside = 0;
	maxh = 0;
	dangle = 0;
	carrydist = 0;
	for (i = 0; i < ptr->count; i++) {
		x = ptr->x[i];
		y = ptr->y[i];
		z = ptr->z[i];

		if (i != 0) {
			if (maxsideAbs < fabs(x)) {
				maxsideAbs = fabs(x);
				maxside = x;
			}

			if (maxh < z) {
				maxh = z;
			}

			if (z < 0) {
				double x1, y1;
				double dx, dy, dz;
				double dxy;
				x1 = (x+x0)/2;
				y1 = (y+y0)/2;
				carrydist = sqrt(x1*x1 + y1*y1);

				dx = x - x0;
				dy = y - y0;
				dz = z - z0;

				dxy = sqrt(dx*dx+dy*dy);

				dangle = -atan2(dz, dxy);
				dangle = dangle * 180 / 3.141592;
				break;
			}
		}
		x0 = x; y0 = y; z0 = z;
	}


	*pcarrydist = carrydist;
	*pofflinedist = maxside;
	*pmaxheight = maxh;
	*pdescentangle = dangle;

	res = 1;
	return res;
}
		
		



#if defined (__cplusplus)
}
#endif
