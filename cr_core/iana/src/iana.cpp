/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2015/12/20 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#if defined(_WIN32)
#include <tchar.h>
#include <direct.h>
#endif
#include "cr_common.h"

#include "cr_thread.h"
#include "cr_osapi.h"

#include "scamif.h"
//#include "scamif_main.h"

#include "iana.h"
#include "iana_configure.h"
#include "iana_adapt.h"
#include "iana_implement.h"
#include "iana_core.h"

// #include "iana_tool.h"
#include "iana_coordinate.h"
#include "iana_xbrd.h"

#include "cr_bugtrap.h"
#include "cr_cam_jpeg.h"
#include "iana_extrinsic.h"
#include "iana_run.h"
#include "iana_traj.h"

#include "iana_club_mark.h"

#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#endif

#include "LanCheck2.h"
/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

//int g_ccam_lite = 1;
int g_ccam_lite = 0;
#if defined(HEIGHT_CALI_FLAT_DEFAULT)
double ggg_HEIGHT_CALI_FLAT_DEFAULT = HEIGHT_CALI_FLAT_DEFAULT;
#else
double ggg_HEIGHT_CALI_FLAT_DEFAULT = 0;
#endif

datapath_t g_datapath;


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
void *iana_manager_mainfunc(void *startcontext);
U32 iana_NetworkThrottlingIndex_Set(U32 value);				// 1: success, 0: fail

void CheckNetworkConfigure();



/*!
 ********************************************************************************
 *	@brief      Create iana module
 *
 *  @return		iana module handle
 *
 *	@author	    yhsuk
 *  @date       2015/12/20
 *******************************************************************************/

HAND IANA_create(U32 opmode, PARAM_T p0, PARAM_T p1, PARAM_T p2, PARAM_T p3)
{
	iana_t *piana;
	iana_setup_t *pisetup;
	U32 camid;	
	//--------------  Init IPP!!!
#ifdef IPP_SUPPORT
	ippStaticInit();
#endif
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": #1\n");
	
	piana = (iana_t *) malloc(sizeof(iana_t));
	if(piana == NULL)
	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s[%d] IANA malloc failed!!\n", __func__, __LINE__);
		return NULL;
	}


	if (g_datapath.mode == 0 || g_datapath.mode == 1) 
	{
		U32 mode;
		FILE *fp;

		fp = cr_fopen(DATAPATHTXT, "r");

		mode = 0;
		if (fp) {
			fscanf(fp, "%d", &mode);
			cr_fclose(fp);
		}
		if (mode == 1) {
#if defined(_WIN32)			
			cr_sprintf(&g_datapath.szDataDir[0], TEXT("%s\\"), DATAPATH);
#else
			cr_sprintf(&g_datapath.szDataDir[0], TEXT("%s/"), DATAPATH);
#endif
		} else {
#if defined(_WIN32)
			GetModuleFileName( NULL, g_datapath.szEXEPath, PATHBUFLEN );
			_tsplitpath(g_datapath.szEXEPath, g_datapath.szDrive, g_datapath.szDir, g_datapath.szFileName, g_datapath.szFileExt);
			cr_sprintf(&g_datapath.szDataDir[0], TEXT("%s%s"), g_datapath.szDrive, g_datapath.szDir);
#elif defined(__linux__)				
			strcpy(g_datapath.szDrive,"");
			OSAL_FILE_GetCurrentExecDir(PATHBUFLEN, g_datapath.szDir);
			memcpy(g_datapath.szDataDir, g_datapath.szDir, PATHBUFLEN );
#else
#error "unknown platform.."
#endif
		}

		OSAL_STR_ConvertUnicode2MultiByte(g_datapath.szDataDir,  g_datapath.sDataDir, PATHBUFLEN);

		g_datapath.mode = mode;
	}


	if (piana == NULL) {
		goto func_exit;
	}

	{		// Check Network Configuration.    20180601
		CheckNetworkConfigure();
	}


	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": #2\n");

	//-------------
	memset(piana, 0, sizeof(iana_t));

	piana->hcl = cr_log_create(0);


	//-- PATH
	{
		if (g_datapath.mode == 0) {
#if defined(_WIN32)			
			GetModuleFileName( NULL, piana->szEXEPath, PATHBUFLEN );
			_tsplitpath(piana->szEXEPath, piana->szDrive, piana->szDir, piana->szFileName, piana->szFileExt);
			cr_sprintf(piana->szImageDirRoot, _T("%s%s%s"), piana->szDrive, piana->szDir, _T("SCAMIMG\\"));
#elif defined(__linux__)	
			strcpy(piana->szDrive, "");
			OSAL_FILE_GetCurrentExecDir(PATHBUFLEN, piana->szDir);
			cr_sprintf(piana->szImageDirRoot, "%s/%s", piana->szDir, "SCAMIMG/");
#else
#error "unknown platform.."
#endif			
		} else {
#if defined(_WIN32)					
			cr_sprintf(piana->szImageDirRoot, _T("%s%s"), g_datapath.szDataDir, _T("SCAMIMG\\"));
#else
			cr_sprintf(piana->szImageDirRoot, _T("%s/%s"), g_datapath.szDataDir, _T("SCAMIMG/"));
#endif
		}
		cr_sprintf(piana->szImageDir, _T("%s%s"), piana->szImageDirRoot, _T("CURRENT"));
	}


	if (g_datapath.mode == 0) {
		cr_mkdir("SCDATA");
	} else {
#if defined(_WIN32)						
		cr_sprintf(g_datapath.szDataScDataDir, _T("%s\\%s\\"), g_datapath.szDataDir, _T("SCDATA"));
#else
		cr_sprintf(g_datapath.szDataScDataDir, _T("%s/%s/"), g_datapath.szDataDir, _T("SCDATA"));
#endif
		OSAL_STR_ConvertUnicode2MultiByte(g_datapath.szDataScDataDir,  g_datapath.sDataScDataDir, PATHBUFLEN);

		cr_mkdir(g_datapath.sDataDir);
		cr_mkdir(g_datapath.sDataScDataDir);
	}

//	if (piana->camsensor_category == CAMSENSOR_Z3 || piana->camsensor_category == CAMSENSOR_EYEXO)
//	{
////#define BUGTRAP_DEFAULTPORTNUM		9999
//#define BUGTRAP_DEFAULTPORTNUM		9987
//
//#define BUGTRAP_SERVER_IP			"13.112.208.112"
//		piana->hcbgt = iana_bugtrap_create(BUGTRAP_SERVER_IP, BUGTRAP_DEFAULTPORTNUM, CAMSENSOR_MAJOR, CAMSENSOR_MINOR, CAMSENSOR_BUILD, NULL);
//		iana_bugtrap_installSehFilter();
//
//	}

	piana->hmutex = cr_mutex_create();

	piana->state = SCAMIF_STATE_CREATED;

	piana->hcbfunc = NULL;
	piana->userparam = 0;

	piana->himgfunc = NULL;
	piana->imguserparam = 0;

	piana->hscamif = NULL;
	piana->hscamifAtC = NULL;

	piana->RS_hscamif = NULL;
	piana->RS_hscamifAtC = NULL;

	piana->LS_hscamif = NULL;
	piana->LS_hscamifAtC = NULL;

	piana->Right0Left1 = 0;				// 0: Right-Handed, 1: Left-Handed
	piana->useLeftHanded = 0;			// 0: Two Camera Sensor Set,   1: One Camera Sensor Set
	piana->Right0Left1Required = 0;
	piana->NeedChangeRight0Left1 = 0;
	
	piana->hxboard = NULL;
	piana->RS_hxboard = NULL;
	piana->LS_hxboard = NULL;

	piana->hjpeg = (HAND) cr_cam_jpeg_create();
	piana->opmode = opmode;
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": #3\n");

	piana->clubcalc_method_required = CR2CLUBCALC_DEFAULT;

	iana_setReadyCam(piana);

	piana->camsensor_category	= CAMSENSOR_CATEGORY;
	piana->camsensor_category_ini	= CAMSENSOR_NULL;


	piana->RS_CamPosEstimatedLValid = 0;
	piana->RS_UseProjectionPlane = 0;
	piana->LS_CamPosEstimatedLValid = 0;
	piana->LS_UseProjectionPlane = 0;
	piana->RS_UseAttackCam = 0;
	piana->LS_UseAttackCam = 0;

	piana->pCamPosEstimatedLValid	= &piana->RS_CamPosEstimatedLValid;
	piana->pUseProjectionPlane		= &piana->RS_UseProjectionPlane;
	piana->pUseAttackCam				= &piana->RS_UseAttackCam;

	piana->prectLArea	= &piana->RS_rectLArea;
	piana->prectLTee	= &piana->RS_rectLTee;
	piana->prectLIron	= &piana->RS_rectLIron;
	piana->prectLPutter	= &piana->RS_rectLPutter;
	piana->pRoughBunkerMat	= &piana->RS_RoughBunkerMat;
	piana->prectLRough	= &piana->RS_rectLRough;
	piana->prectLBunker	= &piana->RS_rectLBunker;

	{
		iana_cam_param_t *picp;
		for (camid = 0; camid < MAXCAMCOUNT; camid++) {
			piana->pic[camid] = &piana->RS_ic[camid];
			
			picp = &piana->RS_ic[camid].icp;
			picp->bCamparamValid = 0;
			picp->quad_UdValid = 0;
			picp->CamExtrinsicValid = 0;

			picp = &piana->LS_ic[camid].icp;
			picp->bCamparamValid = 0;
			picp->quad_UdValid = 0;
			picp->CamExtrinsicValid = 0;

		}
	}

	iana_sensor_setup(piana, piana->camsensor_category);				// setup iana_setup_t

	pisetup = &piana->isetup;
	
	iana_runparam_init(piana);

		if (piana->camsensor_category == CAMSENSOR_Z3 || piana->camsensor_category == CAMSENSOR_EYEXO)
		{
	//#define BUGTRAP_DEFAULTPORTNUM		9999
	#define BUGTRAP_DEFAULTPORTNUM		9987
	
	#define BUGTRAP_SERVER_IP			"13.112.208.112"
			piana->hcbgt = iana_bugtrap_create(BUGTRAP_SERVER_IP, BUGTRAP_DEFAULTPORTNUM, CAMSENSOR_MAJOR, CAMSENSOR_MINOR, CAMSENSOR_BUILD, NULL);
			iana_bugtrap_installSehFilter();
	
		}

	iana_setReadyCam(piana);
	iana_allocbuffer(piana);

	memset(&piana->iif, 0, sizeof(iana_info_t));

	piana->iif.fileformat = 1;					// 0: Bitmat, 1: jpeg
	piana->iif.qvalue = 90;						// Default Qvalue: 90.

	piana->infomode = IANA_INFO_NO;
	sprintf(piana->readinfofile, "info.xml");

	for (camid = 0; camid < MAXCAMCOUNT; camid++) {
		piana->processmode[camid]		= pisetup->icsetup[camid].processmode;
	}

//#define READGOODCOUNT	5
#define READGOODCOUNT	4
//#define READGOODCOUNT	8
//#define READGOODCOUNT	10
	piana->readygoodcountGOOD = READGOODCOUNT;


//	piana->drawpathmode = 0x3;			// 0x01: Shot club path,   0x02: Putter path
	piana->drawpathmode = 0x00;			// 0x01: Shot club path,   0x02: Putter path

//------
	memset(&piana->allowedarea[0], 0, sizeof(U32) * 3);		// NOT allowed... at all.
	piana->processarea 	= IANA_AREA_NULL;
	piana->shotarea 	= IANA_AREA_NULL;

	memset(&piana->shotresult, 0, sizeof(iana_shotresult_t) * MAXCAMCOUNT);

//	memset(&piana->clubball_shotresult, 0, sizeof(iana_clubball_shotresult_t) * 2);
	memset(&piana->clubball_shotresult, 0, sizeof(iana_shotresult_t) * MAXCAMCOUNT);



	piana->windspeedx1000		= 0;
	piana->winddirectionx1000	= 0;


	{		// airepressure...

		if (piana->airpressure_PA_valid) {
			iana_traj_scapegoat_airpressure_Pa((double)piana->airpressure_PA);
		} else if (piana->airpressure_PSI_valid) {
			iana_traj_scapegoat_airpressure_psi((double)piana->airpressure_PSI);
		} else if (piana->airpressure_altitude_m_valid) {
			iana_traj_scapegoat_airpressure_altitude((double)piana->airpressure_altitude_m);
		} else if (piana->airpressure_altitude_feet_valid) {
			// 1 feet = 0.3048 m
#define FEET_TO_M(x)  (((double)(x)) * 0.3048)
			iana_traj_scapegoat_airpressure_altitude(FEET_TO_M(piana->airpressure_altitude_feet));
		}

	}

	piana->hcm = (HAND) iana_club_mark_create((HAND) piana); // 20200106.. 

//-----
#if 0	// BAD Place..
	//---- 
	{		// XBOARD 
		piana->hxboard = iana_xboard_create(piana);
	}
#endif
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": #4\n");

	//------ Make manager module thread.
	{
		cr_thread_t *pt;
		U32 i;
		
		pt = (cr_thread_t *) malloc(sizeof(cr_thread_t));
		if (pt == NULL) {
			// TODO...WHAT?
			goto func_exit;
		}
		memset(pt, 0, sizeof(cr_thread_t));

		piana->hth = (HAND) pt;					// Preserve Thread handle.

		pt->ustate = CR_THREAD_STATE_NULL;
		pt->hevent = cr_event_create();

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": #5\n");

		pt->hthread = (cr_thread_t *) cr_thread_create(iana_manager_mainfunc, (HAND)piana);

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": #6\n");

		if (pt->hthread == NULL) {
			goto func_exit;
		}
#define IANA_WAIT	100
		for (i = 0; i < IANA_WAIT; i++) {
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": #7-%d\n", i);
			if (
					pt->ustate == CR_THREAD_STATE_RUN 
					|| pt->ustate == CR_THREAD_STATE_STOPPING) 
			{
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_INFO, "IANA thread create done!\n");
				break;
			}
			cr_sleep(10);

		}

		if ( pt->ustate == CR_THREAD_STATE_RUN || pt->ustate == CR_THREAD_STATE_STOPPING)  {
			//good.
		} else {
			// bad
		}
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": #8\n");
	}

	//------ Make core module thread.
	{
		cr_thread_t *pt;
		U32 i;
		
		pt = (cr_thread_t *) malloc(sizeof(cr_thread_t));
		if (pt == NULL) {
			// TODO...WHAT?
			goto func_exit;
		}
		memset(pt, 0, sizeof(cr_thread_t));

		piana->hth_core = (HAND) pt;					// Preserve Thread handle.

		pt->ustate = CR_THREAD_STATE_NULL;
		pt->hevent = cr_event_create();

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": #9\n");

		pt->hthread = (cr_thread_t *) cr_thread_create(iana_core_mainfunc, (HAND)piana);

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": #10\n");
	
		cr_thread_setpriority(pt->hthread, NORMAL_USER_PRIORITY);			// THREAD PRIORITY.. -_-; 20160301
		//cr_thread_setpriority(pt->hthread, 2);			// THREAD PRIORITY.. -_-; 20190602

		if (pt->hthread == NULL) {
			goto func_exit;
		}
		for (i = 0; i < IANA_WAIT; i++) {
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": #11-%d\n", i);

			if (
					pt->ustate == CR_THREAD_STATE_RUN 
					|| pt->ustate == CR_THREAD_STATE_STOPPING) 
			{
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_INFO, "IANA thread create done!\n");
				break;
			}
			cr_sleep(10);
		}

		if ( pt->ustate == CR_THREAD_STATE_RUN || pt->ustate == CR_THREAD_STATE_STOPPING)  {
			//good.
		} else {
			// bad
		}
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": #12\n");

	}

	if (piana->opmode == IANA_OPMODE_FILE) {
		piana->xboard_category = XBOARD_CATEGORY_NULL;
	} else {
		if (
				piana->camsensor_category == CAMSENSOR_Z3 
				|| piana->camsensor_category == CAMSENSOR_EYEXO
		) {
			piana->xboard_category = XBOARD_CATEGORY_NULL;	
		} else if (piana->camsensor_category == CAMSENSOR_P3V2) {
			piana->xboard_category = XBOARD_CATEGORY_XMINIHUB;
			//piana->xboard_category = XBOARD_CATEGORY_NULL;


/////////////////////			piana->useLeftHanded = 0;			// NO USE LEFTHANDED.. :P
		} else {
			piana->xboard_category = XBOARD_CATEGORY_XMINI;		// default.. :)
		}
	}


	if (piana->opmode != IANA_OPMODE_FILE) {
			//piana->hxboard = iana_xboard_create2(piana, 0);
			piana->hxboard = iana_xboard_create3(piana, 0, piana->xboard_category);
			piana->RS_hxboard = piana->hxboard;

			if (piana->useLeftHanded) {
				//piana->hxboard = iana_xboard_create2(piana, 1); // xboard check in _create2();
				piana->LS_hxboard = iana_xboard_create3(piana, 1, piana->xboard_category);
			} else {
				piana->LS_hxboard = NULL;
			}
	} else {
	}
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": #13\n");
	
	piana->state = SCAMIF_STATE_INITED;
func_exit:
	UNUSED(opmode); 	UNUSED(p0); 	UNUSED(p1); 	UNUSED(p2); 	UNUSED(p3);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__": #14\n");

	return (HAND) piana;
}


/*!
 ********************************************************************************
 *	@brief      Delete IANA module
 *
 *  @param[in]	h
 *              IANA module handle
 *  @return		Terminate result. 
 *
 *	@author	    yhsuk
 *  @date       2012/12/20
 *******************************************************************************/
I32 IANA_delete(HAND h) 
{
	I32 res;
	iana_t *piana;
	cr_thread_t *ptmanager;
	cr_thread_t *ptcore;
	
	//---
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++ (h: %p)\n",  h);
	res = CR_OK;
	if (h) {


		piana = (iana_t *) h;
		ptmanager	= (cr_thread_t *) piana->hth;
		ptcore		= (cr_thread_t *) piana->hth_core;


		
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d \n", __LINE__);
		if (ptmanager != NULL) {
			ptmanager->ustate = CR_THREAD_STATE_NEEDSTOP;		// You shall die...
			cr_event_set(ptmanager->hevent);
		}

//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d \n", __LINE__);
		if (ptcore != NULL) {
			ptcore->ustate = CR_THREAD_STATE_NEEDSTOP;		// You shall die...
			cr_event_set(ptcore->hevent);
		}

//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d \n", __LINE__);
		if (ptmanager != NULL) {
			cr_thread_join(ptmanager->hthread);					// Wait for termincation..
			cr_event_delete(ptmanager->hevent);
			free(ptmanager);
			piana->hth = NULL;
		}
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d \n", __LINE__);
		if (ptcore != NULL) {
			cr_thread_join(ptcore->hthread);					// Wait for termincation..
			cr_event_delete(ptcore->hevent);
			free(ptcore);
			piana->hth_core = NULL;
		}

//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d \n", __LINE__);

		if (piana->opmode != IANA_OPMODE_FILE) {
			iana_config_write(piana);
		}
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d \n", __LINE__);

		cr_cam_jpeg_delete(piana->hjpeg);
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d \n", __LINE__);
		cr_mutex_delete(piana->hmutex);
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d \n", __LINE__);
		iana_freebuffer(piana);
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d \n", __LINE__);

		if (piana->opmode != IANA_OPMODE_FILE) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d \n", __LINE__);
				// XBOARD 
			if (piana->RS_hxboard) {
				iana_xboard_delete(piana->RS_hxboard);
				piana->RS_hxboard = NULL;
			}
			if (piana->LS_hxboard) {				iana_xboard_delete(piana->LS_hxboard);
				piana->LS_hxboard = NULL;
			}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d \n", __LINE__);
		}

//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d \n", __LINE__);
		cr_log_delete(piana->hcl);
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d \n", __LINE__);

		iana_club_mark_delete(piana->hcm);		// 20200106.. 
		piana->hcm = NULL;
		
		free(piana);
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d \n", __LINE__);
	} else {
		// what!
	}

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d \n", __LINE__);
	res = CR_OK;
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"--\n");


	return res;
}


/*!
 ********************************************************************************
 *	@brief      IANA thead manager main function
 *
 *  @return		trivial..
 *
 *	@author	    yhsuk
 *  @date       2015/11/30
 *******************************************************************************/
void *iana_manager_mainfunc(void *startcontext)
{
	iana_t *piana;	
	cr_thread_t *pt;
	
	volatile U32	loop;
	U32				u0;

	I32 t0, t1;
	CLanCheck2	lanCheck;
	U32 tCheckLan, tcurrent;
	U32 tCheckUsage;

	//--------------------------------------- 
	// Check handle of Sensor
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++\n");

	piana = (iana_t *)startcontext;
	if (piana == NULL) {
		// TODO... what?
		goto func_exit;
	}


	//--------------------------------------- 
	// Change state..
	pt = (cr_thread_t *) piana->hth;
	pt->ustate = CR_THREAD_STATE_INITED;

	loop = 1;

	cr_thread_setpriority(pt->hthread, THREADPRIORITY_IANA_MANAGER);

	pt->ustate = CR_THREAD_STATE_RUN;
	//---------------------------------------


	t0 = cr_gettickcount();
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"camsensor_category: %d\n", piana->camsensor_category);

	if (piana->opmode != IANA_OPMODE_FILE) {
		switch(piana->camsensor_category) {
			case CAMSENSOR_CBALL:				// ZCAM 2.x
				{
					lanCheck.setCheckIP(0, "172.16.1.201");
					lanCheck.setCheckIP(1, "172.16.1.202");
					lanCheck.setCheckIP(2, "172.16.1.50");
				}
				break;
			case CAMSENSOR_Z3:					// ZCAM3
				{
					lanCheck.setCheckIP(0, "172.16.1.222");
				}
				break;
			case CAMSENSOR_EYEXO:				// EYEXO
				{
					lanCheck.setCheckIP(0, "172.16.1.232");
				}
				break;
			case CAMSENSOR_P3V2:				// P3V3
				{
					lanCheck.setCheckIP(0, "172.16.1.201");
					lanCheck.setCheckIP(1, "172.16.1.202");
					lanCheck.setCheckIP(2, "172.16.1.50");
				}
				break;
			default:
				{
					// what?
				}
		}
		lanCheck.start();
	}

	tCheckLan = cr_gettickcount();
	tCheckUsage = cr_gettickcount();
	while(loop) {
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__" loop.\n");
#define CHECK_PERIOD	100				// 100 Hz
		u0 = cr_event_wait (pt->hevent, CHECK_PERIOD);
		if (pt->ustate == CR_THREAD_STATE_NEEDSTOP) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" I would be killed...\n");
			break;
		}

#define CRASH_TEST
#if defined(CRASH_TEST)
	{
		FILE *fp;
		U32 crashmode;

		fp = cr_fopen("crashme.txt", "r");
		if (fp != NULL) {
			fscanf(fp, "%d", &crashmode);
			if (crashmode == 1) {
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "crash me!!\n");
				cr_sleep(1000);
				*(char *) NULL = 0;			// :)
			}
			cr_fclose(fp);
		}

	}
#endif

		t1 = cr_gettickcount();
#define DEBUGOUT_TICK		1000
#define DEBUGCONFIGFILE		"debugoutconfig.txt"
#if defined(DEBUGCONFIGFILE)
		if (t1 < t0 || t1 > t0 + DEBUGOUT_TICK) {
			FILE *fp;
			int mode;
			int area;
			int level;

			mode = -1;
			fp = cr_fopen(DEBUGCONFIGFILE, "r");

			if (fp) {
				fscanf(fp, "%d", &mode);

				if (mode == 0) {
					cr_trace_disable();
				} else if (mode == 1) {
					cr_trace_enable();
				} else if (mode == 2) {
					fscanf(fp, "%x", &area);
					fscanf(fp, "%d", &level);

					cr_trace_config(area, level);
					cr_trace_enable();
				}
				cr_fclose(fp);
			}
			t0 = t1;
#endif
		}

		if (piana->opmode != IANA_OPMODE_FILE) {
			static U32 s_prevgood = 0;
			tcurrent = cr_gettickcount();
//#define CHECKLAN_TIME	(1000)
#define CHECKLAN_TIME	(5*1000)
			if (tcurrent > tCheckLan + CHECKLAN_TIME || tcurrent < tCheckLan) {
				U32 i;
				int nRet;
				int nSpeed = -1;
				char *strIp;
				int allgood = 1;
				for (i = 0; i < MAX_CHECK_IP; i++) {
					if (lanCheck.isrun(i)) {
						lanCheck.getLanInfo(i,&nRet, &nSpeed);
						if (nRet == 0 
								|| s_prevgood == 0
							) {
							strIp = lanCheck.getCheckIP(i);
							//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%2d], IP: %s Connection: %d, speed: %d\n", i, strIp, nRet, nSpeed);
							cr_log(piana->hcl, 3, " [%2d], IP: %s Connection: %d, speed: %d Mbps", i, strIp, nRet, (nSpeed/1000000));
						}
						if (nRet == 0) {
							allgood = 0;
						}
					}
				}
				if (allgood == 0) {
					s_prevgood = 0;
				} else {
					s_prevgood = 1;
				}
				tCheckLan = tcurrent;
			}

#define CHECKLAN_USAGE	(5 * 1000)
			if (tcurrent > tCheckUsage + CHECKLAN_USAGE || tcurrent < tCheckUsage) {
				I32 usage;
				static I32 s_usage5 = 999;

				//---
				usage = OSAL_SYS_GetCpuUsage(); //GetUsage();
				tCheckUsage = tcurrent;
				if (s_usage5 != usage/5) {
					cr_log(piana->hcl, 3, "CPU Usage: %3d%%", usage);
					s_usage5 = usage/5;
				}
			}
		}
	}

	pt->ustate = CR_THREAD_STATE_STOPPING;
	//--------------------------------------- 

	if (piana->opmode != IANA_OPMODE_FILE) {
		lanCheck.stop();
	}
func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"--\n");
	return 0;
}


/*!
 ********************************************************************************
 *	@brief      Set IANA image callback function
 *
 *  @return		1: success, 0: fail
 *
 *	@author	    yhsuk
 *  @date       2016/02/19
 *******************************************************************************/
I32 iana_image_callback_set(HAND hiana, HAND hfunc, PARAM_T userparam)
{
	I32 res;
	iana_t *piana;	

	//--------------------------------------- 
	// Check handle of Sensor
	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}
	piana = (iana_t *)hiana;


	piana->himgfunc = hfunc;
	piana->imguserparam = userparam;

	res = 1;

func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      iana run parameter init
 *
 *  @param[in]	h
 *              IANA module handle
 *
 *  @return		success
 *
 *	@author	    yhsuk
 *  @date       2016/02/22
 *******************************************************************************/

I32 iana_runparam_init(iana_t *piana)
{
	I32 res;
	U32 camid;
	iana_cam_param_t *picp;
	iana_setup_t *pisetup;
	iana_cam_setup_t *picsetup;


	//-----
	// 1) Read Trans matrix
	// 2) Read configuration (INI ? XML?)
	// 3) Converse parameters
#if defined(HEIGHT_CALI_FLAT_DEFAULT)
	piana->height_cali_flat = ggg_HEIGHT_CALI_FLAT_DEFAULT;
#else
	piana->height_cali_flat = 0;
#endif

	pisetup = &piana->isetup;

	res = iana_config_read(piana);

	if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"Category: CAMSENSOR_CONSOLE1, camidCheckReady: %d\n", piana->camidCheckReady);
//#define CAMEXTRINSIC_VALID_ZERO
#if defined(CAMEXTRINSIC_VALID_ZERO)
		int i;
		for(i=0; i < MAXCAMCOUNT; i++) 
		{
			piana->pic[i]->icp.CamExtrinsicValid = 0;		
		}
#endif
		if (piana->pic[0]->icp.CamExtrinsicValid == 0 ||piana->pic[1]->icp.CamExtrinsicValid == 0) {			// 
			iana_calc_extrinsic(piana);
		}

	}

	if (piana->camsensor_category != CAMSENSOR_Z3 && piana->camsensor_category != CAMSENSOR_EYEXO) {
		piana->oneareamode = 0;
	}


	if (piana->camsensor_category == CAMSENSOR_Z3 || piana->camsensor_category == CAMSENSOR_EYEXO) {
		piana->trajmode = 3;				// scapegoat.. :P
	} else {
		piana->trajmode = 1;
	}

	res = iana_getProjectPlane(piana);

	if (piana->opmode != IANA_OPMODE_FILE) {
		if (piana->camsensor_category_ini != CAMSENSOR_NULL) {
			piana->camsensor_category = piana->camsensor_category_ini;
		}

		iana_sensor_setup(piana, piana->camsensor_category);
	}

	iana_cam_getCamInterinsicParamAll(piana);

	res = iana_readTransMat(piana, 0);			// right.
	res = iana_readTransMat(piana, 1);			// left.

	for (camid = 0; camid < MAXCAMCOUNT; camid++) {
		piana->pic[camid]->icp.lpdatapaircount = 0;
	}
	

	{
		for (camid = 0; camid < MAXCAMCOUNT; camid++) {
			picsetup = &pisetup->icsetup[camid];
			
			piana->pic[camid]->runProcessingWidth		= picsetup->run_processing_width;
			piana->pic[camid]->runProcessingHeight	= picsetup->run_processing_height;
		}
	}

	piana->framerate		= pisetup->icsetup[0].run_framerate;
	piana->framerate_putter		= pisetup->icsetup[0].run_framerate_putter;


#if defined(HEIGHT_CALI_FLAT_DEFAULT)
	if (piana->height_cali_flat < -990) {
		piana->height_cali_flat = ggg_HEIGHT_CALI_FLAT_DEFAULT;
	}
#endif

	res = iana_config_conversion(piana);

	for (camid = 0; camid <MAXCAMCOUNT; camid++) {
		memset(&piana->pic[camid]->vtr[0], 0, sizeof(iana_veteran_t) * 3);
		memset(&piana->pic[camid]->vtrPrev[0], 0, sizeof(iana_veteran_t) * 3);

		picp = &piana->pic[camid]->icp;

		memset(&picp->maPx[0], 0, sizeof(double)*3);
		memset(&picp->maPy[0], 0, sizeof(double)*3);
		memset(&picp->maPr[0], 0, sizeof(double)*3);

		memset(&picp->maLx[0], 0, sizeof(double)*3);
		memset(&picp->maLy[0], 0, sizeof(double)*3);
		memset(&picp->maLr[0], 0, sizeof(double)*3);

		picp->mPxx_ = 0;
		picp->bPxx_ = 0;
		picp->mPyy_ = 0;
		picp->bPyy_ = 0;



#define MAXRADpTICK	0.63							// for 2000 fps, 12000 RPM
//#define MAXRADpTICK	0.79							// for 1600 fps, 12000 RPM
#define RADpTICKMARGIN	(1.2)
		picp->maxangleOnetick = (MAXRADpTICK*RADpTICKMARGIN) * (360 / (2*M_PI));
	}


//---------
#if 0
	{
		extern void testLP2(iana_t *piana);
		testLP2(piana);
	}
#endif /* 0 */



	res = 1;
	return 1;
}
		

/*!
 ********************************************************************************
 *	@brief      iana allocation buffers for image processing
 *
 *  @param[in]	h
 *              IANA module handle
 *
 *  @return		success
 *
 *	@author	    yhsuk
 *  @date       2016/02/22
 *******************************************************************************/
I32 iana_allocbuffer(iana_t *piana)
{
	I32 res;
	U32 i, j;
	iana_cam_t	*pic;

	for (i = 0; i < MAXCAMCOUNT; i++) {
		//pic = piana->pic[i];
		//--
		pic = &piana->RS_ic[i];
		for (j = 0; j < IANA_IMGCOUNT; j++) {
			pic->pimg[j] = (U08 *) malloc(FULLWIDTH * FULLHEIGHT);
		}
		pic->prefimg = (U08 *) malloc(FULLWIDTH * FULLHEIGHT);
		pic->pimgFull = (U08 *) malloc(FULLWIDTH * FULLHEIGHT);
		//pic->pimgClubOverlap = (U08 *) malloc(FULLWIDTH * FULLHEIGHT);
		pic->pimgClubOverlap = (U08 *) malloc(FULLWIDTH * FULLHEIGHT*3);

		//--
		pic = &piana->LS_ic[i];
		for (j = 0; j < IANA_IMGCOUNT; j++) {
			pic->pimg[j] = (U08 *) malloc(FULLWIDTH * FULLHEIGHT);
		}
		pic->prefimg = (U08 *) malloc(FULLWIDTH * FULLHEIGHT);
		pic->pimgFull = (U08 *) malloc(FULLWIDTH * FULLHEIGHT);
		//pic->pimgClubOverlap = (U08 *) malloc(FULLWIDTH * FULLHEIGHT);
		pic->pimgClubOverlap = (U08 *) malloc(FULLWIDTH * FULLHEIGHT*3);
	}


	res = 1;
	return res;
}
		

I32 iana_freebuffer(iana_t *piana)
{
	I32 res;
	U32 i, j;
	iana_cam_t	*pic;

	for (i = 0; i < MAXCAMCOUNT; i++) 
	{
		//---
		pic = &piana->RS_ic[i];
		for (j = 0; j < IANA_IMGCOUNT; j++) {
			if (pic->pimg[j] != NULL) {
				free(pic->pimg[j]);	pic->pimg[j] = NULL;
			}
		}
		if (pic->prefimg != NULL) {
			free(pic->prefimg); 		pic->prefimg = NULL;
		}
		if (pic->pimgFull != NULL) {
			free(pic->pimgFull); 		pic->pimgFull = NULL;
		}
		if (pic->pimgClubOverlap != NULL) {
			free(pic->pimgClubOverlap);	pic->pimgClubOverlap = NULL;
		}

		//---
		pic = &piana->LS_ic[i];
		for (j = 0; j < IANA_IMGCOUNT; j++) {
			if (pic->pimg[j] != NULL) {
				free(pic->pimg[j]);	pic->pimg[j] = NULL;
			}
		}
		if (pic->prefimg != NULL) {
			free(pic->prefimg); 		pic->prefimg = NULL;
		}
		if (pic->pimgFull != NULL) {
			free(pic->pimgFull); 		pic->pimgFull = NULL;
		}
		if (pic->pimgClubOverlap != NULL) {
			free(pic->pimgClubOverlap);	pic->pimgClubOverlap = NULL;
		}
	}

	res = 1;
	return res;
}


#if defined(_WIN32)
void CheckNetworkConfigure(void)
{
	HKEY hKey;
	LONG lResult;
	DWORD value;
	DWORD datasize;
//	CString str;
	int goodvalue;
	
	//--
	datasize = sizeof(DWORD);
	value = 0;

//	lResult = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Multimedia\\SystemProfile"), 0, KEY_ALL_ACCESS|KEY_WOW64_64KEY, &hKey);
	lResult = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Multimedia\\SystemProfile"), 0, KEY_READ|KEY_WOW64_64KEY, &hKey);

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" lResult #1  : %d\n", lResult);



	//NetworkThrottlingIndex
	if(lResult == ERROR_SUCCESS) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" RegOpenKeyEx Success.\n");
		lResult = RegQueryValueEx(hKey, _T("NetworkThrottlingIndex"), 0, NULL, (LPBYTE) &value, &datasize);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" RegQueryValueEx result: %d, value: 0x%08x\n", lResult, value);
		RegCloseKey(hKey);
	} else {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" RegOpenKeyEx Fail.\n");
	}
#define GOODVALUE 0xFFFFFFFF
	goodvalue = 0;
	if(lResult == ERROR_SUCCESS) {
		if (value == GOODVALUE) {
			goodvalue = 1;		// GOOD..
		} else {
			// BAD.
		}
	}


	if (goodvalue == 0) {
#define NETWORKCONFIGURE		_T("networkconfig.exe")
		ShellExecute(NULL, _T("open"), NETWORKCONFIGURE, _T("1"), NULL, SW_HIDE) ;
	}
}


#elif defined(__linux__)	
// TODO:
void CheckNetworkConfigure(void)
{
}
#endif







#if defined (__cplusplus)
}
#endif


