/*!
 *******************************************************************************
                                                                                
                CREATZ IANA 
                                                                                
  	@section copyright_notice COPYRIGHT NOTICE
  	Copyright (c) 2015 ~ 2016 by Creatz Inc. 
  	All Rights Reserved. \n
  	Do not duplicate without prior written consent of Creatz Inc.

*******************************************************************************
  	@section file_information FILE CREATION INFORMATION
	@file   iana_exile.cpp
	@brief  IANA exile ball candidates
	@author Original: by yhsuk
    @date   2017/07/10 First Created

	@section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
*******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <math.h>
#include <string.h>

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"

#include "scamif.h"
#include "scamif_buffer.h"

#include "iana.h"
#include "iana_work.h"
#include "iana_implement.h"
#include "iana_cam.h"
#include "iana_cam_implement.h"
#include "iana_shot.h"
#include "iana_balldetect.h"
#include "iana_exile.h"

#include "iana_extrinsic.h"
/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

#define RSMAXCOUNT	1024
// #define DEBUG_EXILE
/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/
static int s_do_toonear = 1;
static int s_do_maxy = 1;

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/

static I32 ExileBallCandidate_RANSAC(iana_t *piana, U32 camid, double t0, double t1);
static I32 ExileBallCandidate_RANSAC_ID(iana_t *piana, U32 camid, double t0, double t1, U32 id);
static I32 CalcOverapTime(iana_t *piana, double *pt0, double *pt1);

/*!
 ********************************************************************************
 *	@brief      CAM Exile ball candidates
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/07/10
 *******************************************************************************/
I32 iana_exile(iana_t *piana)
{
	I32 res;
	double t0, t1;

	CalcOverapTime(piana, &t0, &t1);		// Calc overlap time

	IANA_ExileBallCandidate(piana, 0, t0, t1);
	IANA_ExileBallCandidate(piana, 1, t0, t1);

#ifdef DEBUG_EXILE
	{
		U32 camid;
		U32 i, j;
		iana_ballcandidate_t *pbc;
		iana_cam_t	*pic;
		U64 ts64, ts64shot;
		double ts64shotd;
		double ts64d;

		for (camid = 0; camid < NUM_CAM; camid++) {
			pic = piana->pic[camid];
			ts64shot = pic->ts64shot;
			ts64shotd = (double) (ts64shot / TSSCALE_D);

			for (i = 0; i < BALLSEQUENCELEN; i++) { 
				for (j = 0; j < BALLCCOUNT; j++) {
					pbc = &pic->bc[i][j];
					if (pbc->cexist == IANA_BALL_EXIST) {
						double x, y;
						double tsd;

						ts64 = pbc->ts64;
						ts64d = (double) (ts64 / TSSCALE_D);
						tsd = ts64d - ts64shotd;
						x = pbc->L.x;
						y = pbc->L.y;
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] %2d %2d   %10.6lf(%10.6lf) : %10.6lf %10.6lf \n", camid, i, j, ts64d, tsd, x, y);
					}
				}
			} 
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
		}
	}
#endif
	res = 1;
	return res;
}

/*!
 ********************************************************************************
 *	@brief      CAM Exile ball candidates for camid, with RANSAC. :P
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/10/26
 *******************************************************************************/
I32 IANA_ExileBallCandidate(iana_t *piana, U32 camid, double t0, double t1)
{
	I32 res;
	U64 ts64shot;
	U64 ts64;

	double ts64shotd;
	double ts64d;

	iana_cam_t	*pic;
	iana_cam_param_t *picp;
	U32 bcseqcount;

	ballgroup_t *pbg;

	double sx, sy;				// Ball start point
	U32 i, j;

	iana_ballcandidate_t *pbc;


	I32 ballcount;
	I32 multiballcount;
	I32 ballcountmax;
	
    if (camid >= NUM_CAM) 
	{
		res = 0;
		goto func_exit;
	}

//#define RAND_FIX_TEST		1234
#if defined(RAND_FIX_TEST)
	srand(RAND_FIX_TEST);
#endif

	pic = piana->pic[camid];
	picp = &pic->icp;

	ts64shot = pic->ts64shot;
	ts64shotd = (double) (ts64shot / TSSCALE_D);
	bcseqcount = pic->bcseqcount;
	if (bcseqcount > BALLSEQUENCELEN) {
		res = 0;
		goto func_exit;
	}

    // delete: save s_do_toonear to file

	//-- Basic filtering
    //TODO:traveling index is little bit weird
	if (s_do_toonear) {
		for (i = 0; i < BALLSEQUENCELEN /*bcseqcount*/; i++) {
			for (j = 0; j < BALLCCOUNT; j++) {
				pbc = &pic->bc[i][j];
				if (pbc->cexist == IANA_BALL_EXIST) {
					U32 ii, jj, startjj;
					for (ii = i; ii < BALLSEQUENCELEN; ii++) {
						if (ii == i) {
							startjj = j+1;
						} else {
							startjj = 0;
						}
						for (jj = startjj; jj < BALLCCOUNT; jj++) {
							iana_ballcandidate_t *pbc0;
							pbc0 = &pic->bc[ii][jj];
							if (pbc0->cexist == IANA_BALL_EXIST) {
								double dx, dy;
								double dlen;
								dx = pbc->L.x - pbc0->L.x;
								dy = pbc->L.y - pbc0->L.y;
								dlen = sqrt(dx*dx+dy*dy);

#define CHECK_TOONEAR	0.0005
								if (dlen < CHECK_TOONEAR) {
									pbc->cexist = IANA_BALL_EXILE;
									pbc0->cexist = IANA_BALL_EXILE;
									cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%d] [%d:%d]EXILE_TOONEAR, %lf\n", i, j, ii, jj, dlen);
									continue;
								}
							}
						}
					}
				}
			}
		}
	}

    sx = piana->pic[camid]->icp.L.x;
    sy = piana->pic[camid]->icp.L.y;

#ifdef DEBUG_EXLIE
	{
		double sx0, sy0;
		double dx, dy, dist;

		sx0 = piana->pic[piana->camidCheckReady]->icp.L.x;
		sy0 = piana->pic[piana->camidCheckReady]->icp.L.y;

		dx = sx0-sx;
		dy = sy0-sy;
		dist = sqrt(dx*dx + dy*dy);

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " sx, sy (%lf %lf) vs sx0, sy0 (%lf %lf)  dist: %lf\n", sx, sy, sx0, sy0, dist);
	}
#endif

	for (i = 0; i < BALLSEQUENCELEN /*bcseqcount*/; i++) {
		for (j = 0; j < BALLCCOUNT; j++) {
			pbc = &pic->bc[i][j];
			if (pbc->cexist == IANA_BALL_EXIST) {
				ts64 = pbc->ts64;
				ts64d = (double) (ts64 / TSSCALE_D);

#define CHECK_TS64D		(0.005)
#define CHECK_TOOSMALLY	(-0.010)
#define CHECK_TOOFAR	(0.5)
				if (ts64d < ts64shotd + CHECK_TS64D) {
					double dx, dy;
					double dlen;

					if (pbc->L.y < sy + CHECK_TOOSMALLY) { 		// 1) EXILE_TOOSMALLY
						pbc->cexist = IANA_BALL_EXILE;
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%d] EXILE_TOOSMALLY\n", i, j);
						continue;
					}

					dx = pbc->L.x - sx;
					dy = pbc->L.y - sy;
					dlen = sqrt(dx*dx+dy*dy);

					if (dlen > CHECK_TOOFAR) { 					// 2) EXILE_TOOFAR;
						pbc->cexist = IANA_BALL_EXILE;
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%d] EXILE_TOOFAR\n", i, j);
						continue;
					}
				}
			}
		}
	}

	ExileBallCandidate_RANSAC(piana, camid, t0, t1);

#if defined(_WIN32)
	__try {
#endif	
		pbg = &picp->bgp;
		memset(pbg, 0, sizeof(ballgroup_t));
		ballcount = 0;
		pbg->ballcount = 0;
		for (i = 0; i < BALLIDMAX; i++) {
			pbg->bid[i].groupid = -1;
		}

		multiballcount = 0;
		ballcountmax = BALLIDMAX;

		{
			double maxLy;
			U32 ballcount_;
	
			maxLy = -999999;
			ballcount_ = 0;
			for (i = 0; i < BALLSEQUENCELEN /*bcseqcount*/; i++) {
				for (j = 0; j < BALLCCOUNT; j++) {
					pbc = &pic->bc[i][j];
					if (pbc->cexist == IANA_BALL_EXIST) {
						ts64d = (double) (pbc->ts64 / TSSCALE_D);
						if (ts64d < ts64shotd) {
							ballcount_++;

#define MAXLY_COUNT	6
							if (ballcount_ > MAXLY_COUNT) {
								if (maxLy < pbc->L.y) {
									maxLy = pbc->L.y;
								} else {
									cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "MAXLY exile.. with %d, ly: %lf vs maxLy: %lf\n", 
											ballcount_, pbc->L.y, maxLy);
									pbc->cexist = IANA_BALL_EXILE;
								}
							} else {
								if (maxLy < pbc->L.y) {
									maxLy = pbc->L.y;
								}
							}
						} // if (ts64d < ts64shotd + CHECK_TS64D) 
					} 	// if (pbc->cexist == IANA_BALL_EXIST) 
				}		// for (j = 0; j < BALLCCOUNT; j++) 
			} 			// for (i = 0; i < BALLSEQUENCELEN /*bcseqcount*/; i++) 
		}

		for (i = 0; i < BALLIDMAX; i++) {
			pbg->bid[i].cexist = IANA_BALL_EMPTY;
		}

		for (i = 0; i < BALLSEQUENCELEN /*bcseqcount*/; i++) { // 3) EXILE_ONLYONE
			U32 existed;

			existed = 0;
			if (s_do_maxy == 0) {
				for (j = 0; j < BALLCCOUNT; j++) {
					ballid_t *pbid;
					pbc = &pic->bc[i][j];
					pbid = &pbg->bid[ballcount];
					if (pbc->cexist == IANA_BALL_EXIST) {
						if (existed == 0) {
							existed = 1;
							pbid->cexist = IANA_BALL_EXIST;
							pbid->seq = i;
							pbid->id =  j;
							ballcount++;
							if (ballcount >= ballcountmax /*BALLIDMAX*/) {
								break;
							}
						} else {
							pbc->cexist = IANA_BALL_EXILE;
							pbid->cexist = IANA_BALL_EMPTY;
						}
					} else {
						pbid->cexist = IANA_BALL_EMPTY;
					}
				}
			} else {
				double maxy;
				I32 maxj;
				ballid_t *pbid;
				
				//--
				pbid = &pbg->bid[ballcount];

				maxy = -999;
				maxj = -1;
				for (j = 0; j < BALLCCOUNT; j++) {
					pbc = &pic->bc[i][j];
					if (pbc->cexist == IANA_BALL_EXIST) {
						if (pbc->L.y > maxy) {
							maxy = pbc->L.y;
							maxj = j;
						}
					}
				}
				if (maxj >= 0) {
					int multiballcheck = 0;

					pbid->cexist = IANA_BALL_EXIST;
					pbid->seq = i;
					pbid->id =  maxj;
				//	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
				//		// what?
				//	} else 
					{
						for (j = 0; j < BALLCCOUNT; j++) {
							pbc = &pic->bc[i][j];
							if (j != (U32)maxj) {
								if (pbc->cexist == IANA_BALL_EXIST) {
									pbc->cexist = IANA_BALL_EXILE;
									multiballcheck = 1;
								}
							}
						}
					}
					if (multiballcheck) {
						multiballcount++;
					}

					ballcount++;
				} else {
					pbid->cexist = IANA_BALL_EMPTY;
				}
			}

			if (ballcount >= ballcountmax /*BALLIDMAX*/) {
				break;
			}
		}

		if (piana->processarea != IANA_AREA_PUTTER) {
//#define CHECKMULTIBALLCOUNT	3
#define CHECKMULTIBALLCOUNT	1
			if (multiballcount >= CHECKMULTIBALLCOUNT) {
				U32 ballcount0;
				ballcount0 = 0;
				for (i = 0; i < BALLSEQUENCELEN; i++) { 
					for (j = 0; j < BALLCCOUNT; j++) {
						pbc = &pic->bc[i][j];
						if (pbc->cexist == IANA_BALL_EXIST) {
#define MULTIBALL_COUNTMAX	6
							if(ballcount0 >= MULTIBALL_COUNTMAX) {
								pbc->cexist = IANA_BALL_EXILE;
							} else {
								ballcount0++;
							}
						}
					}
				}
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] ===========> hballcount: %d, ballcount0: %d, multiballcount: %d\n", 
					camid,
					ballcount, ballcount0, multiballcount);
				for (i = (U32)ballcount0; i < (U32)ballcount; i++) {
					ballid_t *pbid;
					pbid = &pbg->bid[i];
					pbid->cexist = IANA_BALL_EXILE;
				}
				ballcount = ballcount0;
			}
		}

		pbg->ballcount = ballcount;
		pbg->elementcount = 0;

		//=============================================

		/*
		// 4) EXILE_SUMTD
		// 5) EXILE_DIFFV



		// 8) EXILE_AREAOUT
		*/
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%d] ballcount: %d\n", camid, __LINE__, ballcount);
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "EXILE #5\n");
		res = 1;
#if defined(_WIN32)		
	} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
					|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
					|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
					)
	{
		res = 0;
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" WHAT Exception: %d\n", GetExceptionCode());
	} 
#endif

	ballcount = 0;
	for (i = 0; i < BALLSEQUENCELEN; i++) { 
		for (j = 0; j < BALLCCOUNT; j++) {
			pbc = &pic->bc[i][j];
			if (pbc->cexist == IANA_BALL_EXIST) {

#ifdef DEBUG_EXLIE
				double x, y;
				double tsd;

				ts64 = pbc->ts64;
				ts64d = (double) (ts64 / TSSCALE_D);
				tsd = ts64d - ts64shotd;
				x = pbc->L.x;
				y = pbc->L.y;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] %2d %2d   %10.6lf(%10.6lf) : %10.6lf %10.6lf \n", camid, i, j, ts64d, tsd, x, y);
#endif
				ballcount++;
			}
		}
	} 

//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%d] ballcount: %d\n", camid, __LINE__, ballcount);

func_exit:
//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"-- %d\n", res);
	return res;
}


/*!
 ********************************************************************************
 *	@brief      CAM Exile with RANSAC algorithm
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2020/08/29
 *******************************************************************************/
static I32 ExileBallCandidate_RANSAC(iana_t *piana, U32 camid, double t0, double t1)	
{
	I32 res;

	//--
	ExileBallCandidate_RANSAC_ID(piana, camid, t0, t1, IANA_EXILE_RANSAC_THETA);
	ExileBallCandidate_RANSAC_ID(piana, camid, t0, t1, IANA_EXILE_RANSAC_TY);
	ExileBallCandidate_RANSAC_ID(piana, camid, t0, t1, IANA_EXILE_RANSAC_TX);
	ExileBallCandidate_RANSAC_ID(piana, camid, t0, t1, IANA_EXILE_RANSAC_YX);

	res = 1;

	return res;
}

/*!
 ********************************************************************************
 *	@brief      CAM Exile with RANSAC algorithm with id
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2020/08/29
 *******************************************************************************/
static I32 ExileBallCandidate_RANSAC_ID(iana_t *piana, U32 camid, double t0, double t1, U32 id)
{
	I32 res;
	I32 i, j, k;
	char *pwhy;
	double checkvalue0;			// min fabs(dx)
	double checkvalue1;			// max in group
	//I32 checkcount;				// valid max in group count
	double checkcount;				// valid max in group count
	double x0[RSMAXCOUNT], y0[RSMAXCOUNT];
	double ts[RSMAXCOUNT];
	I32 index0[RSMAXCOUNT];
	I32 index1[RSMAXCOUNT];
	//I32 maxingroup;
	double maxingroup;
	double m0, b0;

	iana_cam_t	*pic;
	iana_cam_param_t *picp;
	iana_ballcandidate_t *pbc;

	U64 ts64shot;
	U64 ts64;

	double ts64shotd;
	double ts64d;

	double sx, sy;

	I32 count;
	I32 count1;		// count for group selection

#define OVERLAPPED_WEIGHT	1.5 // 2.0, 10
	//-----
	if (camid >= NUM_CAM) {
		res = 0;
		goto func_exit;
	}

	if (id == IANA_EXILE_RANSAC_YX) {
		pwhy = "IANA_EXILE_RANSAC_YX";
		checkvalue0 = 0.001;
		checkvalue1 = 0.005; // 0.002
		checkvalue1 = BALLRADIUS / 4;
		checkcount  = 4;
	} else if (id == IANA_EXILE_RANSAC_TX) {
		pwhy = "IANA_EXILE_RANSAC_TX";
		checkvalue0 = 0.001;
		checkvalue1 = 0.005;
		checkvalue1 = BALLRADIUS / 4;
		checkcount  = 4;
	} else if (id == IANA_EXILE_RANSAC_TY) {
		pwhy = "IANA_EXILE_RANSAC_TY";
		checkvalue0 = 0.001;
		checkvalue1 = 0.005;
		checkvalue1 = BALLRADIUS / 4;
		checkcount  = 4;
	} else if (id == IANA_EXILE_RANSAC_THETA) {
		pwhy = "IANA_EXILE_RANSAC_THETA";
		checkvalue0 = 0.001;
		//checkvalue1 = 3;				// Too loose..
		//checkvalue1 = 2;				//   not yet.
		//checkvalue1 = 1;				// good.. 

		//checkvalue1 = 5;
		if (piana->processarea == IANA_AREA_PUTTER) {
			checkvalue1 = 10;
		} else {
			checkvalue1 = 3;
		}


		checkcount  = 4;
	} else {
		res = 0;
		goto func_exit;

	}

	pic = piana->pic[camid];
	picp = &pic->icp;

	ts64shot = pic->ts64shot;
	ts64shotd = (double) (ts64shot / TSSCALE_D);

	sx = picp->L.x;
	sy = picp->L.y;
	
	// Prepare dataset
	count = 0;
	for (i = 0; i < BALLSEQUENCELEN /*bcseqcount*/; i++) {
		for (j = 0; j < BALLCCOUNT; j++) {
			double x, y;
			double tsd;
			//--
			pbc = &pic->bc[i][j];
			if (pbc->cexist == IANA_BALL_EXIST) {
				ts64 = pbc->ts64;
				ts64d = (double) (ts64 / TSSCALE_D);
				tsd = ts64d - ts64shotd;
				x = pbc->L.x;
				y = pbc->L.y;
				ts[count] = ts64d;
				if (id == IANA_EXILE_RANSAC_YX) {
					x0[count] = y;			// data 0
					y0[count] = x;			// data 1
				} else if (id == IANA_EXILE_RANSAC_TX) {
					x0[count] = tsd;			// data 0
					y0[count] = x;			// data 1
				} else if (id == IANA_EXILE_RANSAC_TY) {
					x0[count] = tsd;			// data 0
					y0[count] = y;			// data 1
				} else if (id == IANA_EXILE_RANSAC_THETA) {
					x0[count] = x;			// data 0
					y0[count] = y;			// data 1

				} else {// WHAT?
					res = 0;
					goto func_exit;
				}

				index0[count] = i;			// index 0
				index1[count] = j;			// index 1
				count++;
				if (count >= RSMAXCOUNT) {
					break;
				}
			}
		}	// for (j;;)
		if (count >= RSMAXCOUNT) {
			break;
		}
	}		// for (i;;)

#define COUNT_RANSAC_ID_MIN	3
	if (count < COUNT_RANSAC_ID_MIN) {
		res = 0;
		goto func_exit;
	}
	maxingroup = 0.0;
	m0 = 1.0;
	b0 = 0;

#define RSITER	64
	for (k = 0; k < RSITER; k++) {
		I32 i0, i1;
		double dx, dy;
		double m, b;
		double delta;
		//I32 ingroup;
		double ingroup;

		// select candidate
		i0 = rand() % count;
		i1 = rand() % count;
		if (i0 == i1) continue;

		// make model

		count1 = count;
		if (id == IANA_EXILE_RANSAC_THETA) {
			dx = x0[i0] - sx;
			dy = y0[i0] - sy;

			m = RADIAN2DEGREE(atan2(dx, dy));
			b = 0;
#define COUNT1_THETA	10
			if (count1 > COUNT1_THETA) {
				count1 = COUNT1_THETA;
			}
		} else {
			dx = x0[i0] - x0[i1];
			dy = y0[i0] - y0[i1];
			if (fabs(dx) < checkvalue0) {
				continue;
			}
			m = dy / dx;
			b = y0[i0] - m * x0[i0];
			if (id == IANA_EXILE_RANSAC_TY) {
				if (m < 0) {			// TY must positive..    20201110
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " [%d %d] %s   m = %lf < 0. fail.\n", camid, k,  pwhy, m);
					continue;			// FAIL..
				}
			}
		}

		// check in-group count;

		ingroup = 0.0;
		for (i = 0; i < count1; i++) 
		{
			if (id == IANA_EXILE_RANSAC_THETA) {
				double theta_;
				dx = x0[i] - sx;
				dy = y0[i] - sy;

				theta_ = RADIAN2DEGREE(atan2(dx, dy));

				delta = m - theta_;
				if (fabs(delta) < checkvalue1) {
					if (ts[i] > t0 && ts[i] < t1) {
						ingroup = ingroup + OVERLAPPED_WEIGHT;
					} else {
						ingroup++;
					}
				}
			} else {
				delta = (y0[i] - (m*x0[i] + b));
				if (fabs(delta) < checkvalue1) {
					if (ts[i] > t0 && ts[i] < t1) {
						ingroup = ingroup + OVERLAPPED_WEIGHT;
					} else {
						ingroup++;
					}
				}
			}
		}

		if (maxingroup < ingroup) {
			maxingroup = ingroup;
			m0 = m;
			b0 = b;
		}
		/*
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] %s m: %lf b: %lf ingoup: %d maxingroup: %d\n",
				camid, pwhy, 
				m, b,
				ingroup, maxingroup);
		*/
	}

	if (maxingroup >= checkcount) {	// good.
		I32 countgood, countexile;
		I32 c;
		double delta;

		//---
		countgood = 0;
		countexile = 0;
		i = 0; j = 0;
		for (c = 0; c < count; c++) {
			if (id == IANA_EXILE_RANSAC_THETA) {
				double theta_;
				double dx, dy;
				dx = x0[c] - sx;
				dy = y0[c] - sy;

				theta_ = RADIAN2DEGREE(atan2(dx, dy));

				delta = m0 - theta_;
				i = index0[c];
				j = index1[c];
				pbc = &pic->bc[i][j];

				if (fabs(delta) > checkvalue1) {
					pbc->cexist = IANA_BALL_EXILE;
					countexile++;

					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] %s EXILE    %2d, %2d  (%10.6lf , %10.6lf),  delta: %lf\n", 
							camid, pwhy, i, j, 
							pbc->L.x,
							pbc->L.y,
							delta);
				} else {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] %s NO EXILE %2d, %2d  (%10.6lf , %10.6lf),  delta: %lf\n", 
							camid, pwhy, i, j, 
							pbc->L.x,
							pbc->L.y,
							delta);
					countgood++;
				}
			} else {
				delta = (y0[c] - (m0*x0[c] + b0));
				if (fabs(delta) > checkvalue1) {				// OUTLIER..
					i = index0[c];
					j = index1[c];
					pbc = &pic->bc[i][j];
					pbc->cexist = IANA_BALL_EXILE;
					countexile++;

					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] %s EXILE %2d, %2d  (%10.6lf , %10.6lf),  delta: %lf\n", 
							camid, pwhy, i, j, 
							pbc->L.x,
							pbc->L.y,
							delta);
				} else {
					countgood++;
				}
			}
		}

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n[%d] %s countgood: %d, countexile: %d with m: %lf b: %lf\n",
				camid, pwhy, countgood, countexile, m0, b0);
		res = 1;
	} else {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] %s FAIL. with m: %lf b: %lf\n", camid, pwhy, m0, b0);
		res = 0;
	}


	for (i = 0; i < BALLSEQUENCELEN /*bcseqcount*/; i++) {
		for (j = 0; j < BALLCCOUNT; j++) {
			double x, y;
			double tsd;
			//--
			pbc = &pic->bc[i][j];
			if (pbc->cexist == IANA_BALL_EXIST) {
				ts64 = pbc->ts64;
				ts64d = (double) (ts64 / TSSCALE_D);
				tsd = ts64d - ts64shotd;
				x = pbc->L.x;
				y = pbc->L.y;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] %s %2d %2d   %10.6lf : %10.6lf %10.6lf \n", camid, pwhy, i, j, tsd, x, y);
			}
		}	// for (j;;)
		if (count >= RSMAXCOUNT) {
			break;
		}
	}		// for (i;;)

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");


func_exit:
	return res;
}


static I32 CalcOverapTime(iana_t *piana, double *pt0, double *pt1)
{
	I32 res;
	double tfrom[MAXCAMCOUNT], tto[MAXCAMCOUNT];
	double t0, t1;
	iana_cam_t	*pic;
	iana_ballcandidate_t *pbc;

	U32 camid;
	U32 i, j;

	//--
#define INIT_TIME_T0T1	999
	for (camid = 0; camid < NUM_CAM; camid++) {
		pic = piana->pic[camid];

		tfrom[camid] = INIT_TIME_T0T1;
		tto[camid]   = -INIT_TIME_T0T1;
		for (i = 0; i < BALLSEQUENCELEN; i++) { 
			for (j = 0; j < BALLCCOUNT; j++) {
				pbc = &pic->bc[i][j];
				if (pbc->cexist == IANA_BALL_EXIST) {
					double ts64d;
					ts64d = (double) (pbc->ts64 / TSSCALE_D);
					if (tfrom[camid] > ts64d) {
						tfrom[camid] = ts64d;
					}
					if (tto[camid] < ts64d) {
						tto[camid] = ts64d;
					}
				}
			}
		} 
	}

	t0 = tfrom[0];
	if (t0 < tfrom[1]) {
		t0 = tfrom[1];
	}

	t1 = tto[0];
	if (t1 > tto[1]) {
		t1 = tto[1];
	}

	if (piana->camsensor_category == CAMSENSOR_P3V2)
	{
		double tsshotd;

		tsshotd = piana->pic[0]->ts64shot / TSSCALE_D;

#define MAXT1DELTA	(0.001 * 8)
		if (t1 > tsshotd + MAXT1DELTA) {
			t1 = tsshotd + MAXT1DELTA;
		}
	}

	if (t0 > t1) {		// FAIL.. -_-;
		t0 = -INIT_TIME_T0T1;
		t1 = INIT_TIME_T0T1;
	}
	*pt0 = t0;
	*pt1 = t1;

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
        "tf ~ tt: %lf %lf,   %lf %lf\n",
        tfrom[0], tto[0],
        tfrom[1], tto[1]);

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "t0 ~ t1: %lf %lf\n", t0, t1);
	res = 1;

	return res;
}

#if defined (__cplusplus)
}
#endif


