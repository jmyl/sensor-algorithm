/********************************************************************************
                                                                                
                   Creatz Golf sensor 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 by Creatz Inc.
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc. \n

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_traj.c
	 @brief  Golf Trajectory.
	 @author YongHo Suk                                 
	 @date   2011/10/05 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/

#include "cr_common.h"

#include "iana_traj.h"

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#if defined( _MSC_VER ) && (_MSC_VER <= 1200)    // 1200 = MSVC 6.0.
#define __FUNCTION__
#endif


/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*

             ^               
             |Z               
             |              .
             |          .       .
             |       .            .
             |     .               .
             |   .                  .
             | .                     .
             |------------------------.----->		Y
            /                          .
           /
		  /
		 V  X

*/


/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/
#if defined (__cplusplus)
extern "C" {
#endif
typedef struct _crtraj_ {
	void *hiana_traj;
	int category;				// 1: Legacy, 2: intermediate, 3: scapegoat
	double winddir;				// [degree]
	double windmag;				// [m/s]
} crtraj_t;
/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/

/*!
 ********************************************************************************
 *	@brief      create trajectory object
 *  @return		handle of trajectory object
 *
 *	@author	    yhsuk
 *  @date       2020/0611
 *******************************************************************************/
void *crtraj_create(void)
{
	ptraj = (crtraj_t *) malloc (sizeof(crtraj_t));
	if (ptraj == NULL) {
		goto func_exit;
	}

	memset(ptraj, 0, sizeof(crtraj_t));

	ptraj->category = 1;		// 1: Legacy, 2: intermediate, 3: scapegoat
	ptraj->winddir = 0.0;		// [degree]
	ptraj->windmag = 0.0;		// [m/s]

func_exit:
	return ptraj;
}



/*!
 ********************************************************************************
 *	@brief      delete trajectory object
 *	@param[in]  htraj  handle of trajectory object
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2020/0611
 *******************************************************************************/
void crtraj_delete(HAND htraj)
{
	if (htraj) {
		free(htraj);
	}
}


/*!
 ********************************************************************************
 *	@brief      set wind.
 *	@param[in]  htraj  handle of trajectory object
 *	@param[in]  winddir		[degree]
 *	@param[in]  windmag		[m/s]
 *	@return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2020/0611
 *******************************************************************************/
int crtraj_wind(HAND htraj,
		double winddir,
		double windmag)
{
	int res;


	res = 1;
	return res;
}



/*!
 ********************************************************************************
 *	@brief      set trajectory category
 *	@param[in]  htraj  handle of trajectory object
 *	@param[in]  category  1: Legacy, 2: intermediate, 3: scapegoat
 *	@return		0: fail, 1: Good.
 *
 *	@author	    yhsuk
 *  @date       2020/0611
 *******************************************************************************/
int crtraj_category(void *htraj, int category)
{
	int res;

	if (category < 1 || category > 3) {
		res = 0;
		goto func_exit;
	}

	res = 1;
func_exit:
	return res;
}



/*!
 ********************************************************************************
 *	@brief      set airpressure in psi
 *	@param[in]  htraj  handle of trajectory object
 *	@param[in]  airpressure	in psi
 *	@return		-1: fail. 0: set to default, 1; good.
 *
 *	@author	    yhsuk
 *  @date       2020/0611
 *******************************************************************************/
int crtraj_airpressure_psi(
		void *htraj,
		double airpressure_psi)
{
	int res;

	if (ptraj->category != 3) {
		res = -1; // fail
	} else {
		res = XXX;
	}

	return res;
}

/*!
 ********************************************************************************
 *	@brief      set airpressure in Pa
 *	@param[in]  htraj  handle of trajectory object
 *	@param[in]  airpressure	in Pa
 *	@return		-1: fail. 0: set to default, 1; good.
 *
 *	@author	    yhsuk
 *  @date       2020/0611
 *******************************************************************************/
int crtraj_airpressure_Pa(double airpressure_Pa)
{
	I32 res;

	if (ptraj->category != 3) {
		res = -1; // fail
	} else {
		res = XXX;
	}
	return res;
}


/*!
 ********************************************************************************
 *	@brief      set airpressure in altitude
 *	@param[in]  htraj  handle of trajectory object
 *	@param[in]  altitude in m
 *	@return		-1: fail. 0: set to default, 1; good.
 *
 *	@author	    yhsuk
 *  @date       2020/0611
 *******************************************************************************/
int crtraj_airpressure_altitude(double altitude_m)
{
	int res;

	if (ptraj->category != 3) {
		res = -1; // fail
	} else {
		res = XXX;
	}
	return res;
}


/*!
 ********************************************************************************
 *	@brief      get trajectory
 *	@param[in]  htraj		handle of trajectory object
 *	@param[in]  timedelta	time resolution for trajectory (sec)
 *	@param[in]  vmag		[m/s]
 *	@param[in]  incline		[degree]
 *	@param[in]  azimuth		[degree]
 *	@param[in]  backspin	[rpm]
 *	@param[in]  sidespin	[rpm]
 *	@param[out] ptr			result.
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2009/11/08
 *******************************************************************************/
void crtraj_traj(
		void 	*htraj,
		double  timedelta,
		double  vmag,
		double  incline,
		double  azimuth,
		double  backspin,
		double  sidespin,

		crtraj_result_t *ptr)
{




}


#if defined (__cplusplus)
}
#endif

