/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA shot calc.
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_shot.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2015/12/20 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <math.h>
#include <string.h>

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"
#include "cr_nanoxml.h"

#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#else
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#endif

#include "scamif.h"
//#include "scamif_main.h"
#include "scamif_buffer.h"

#include "iana.h"
#include "iana_work.h"
#include "iana_implement.h"
#include "iana_cam.h"
#include "iana_cam_implement.h"
#include "iana_balldetect.h"
#include "iana_shot.h"
#include "iana_exile.h"
#include "iana_tool.h"
#include "iana_coordinate.h"
#include "iana_spin.h"
#include "iana_spin_ballclub.h"

#include "iana_club.h"
#include "iana_club_mark.h"

#include "iana_shot_recalc.h"

#include "cr_regression.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#if defined(_WIN32)
#pragma warning(disable:4456)
#endif
#if defined (__cplusplus)
extern "C" {
#endif

#define P3V2SHOTFILEIRON		"P3shotmodifyIRON.txt"
#define P3V2SHOTFILEDRIVER		"P3shotmodifyDRIVER.txt"

#define SHOTDATAFILE		"shotmode.txt"

//#define CHECKREADYFILE	"checkreadyball.txt"

//#define READYIMAGE_PRESHOT	8

/////////#define READYIMAGE_PRESHOT	2
//#define READYIMAGE_PRESHOT	4
/////////////////////////////#define READYIMAGE_PRESHOT	6
//#define READYIMAGE_PRESHOT	3					// 20171111-1
//#define READYIMAGE_PRESHOT	1					// 20171111-2
//#define READYIMAGE_PRESHOT	2
//#define READYIMAGE_PRESHOT	4
#define READYIMAGE_PRESHOT	3

#define TS64DELTA_UPDATE
/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/
static int s_pbtype = 1;			// 0: P2Raw, 1: P2
static int s_useL = 1;

static int s_preshot = READYIMAGE_PRESHOT;

//static int s_i0 = -6;
static int s_i0 = -8;
//static int s_refine_with_CHT = 0;
static int s_refine_with_CHT = 1;
//static int s_rmt_expn_1 = 1;
static int s_rmt_expn_1 = 0;

static int s_use_pp_pp = 1;
static int s_putter_out_gamma = 1;


/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/
extern U32 getplane(cr_point_t *array, U32 arraylen, cr_plane_t *pplane);
extern I32 IANA_CLUB_ModifyShot4P3V2(double *pvmag, double *pincline,	U32 isIron);	// See iana_club.cpp
extern I32 calc_circle_mean_stddev(iana_ballcandidate_t *pbc, U08 *pimgbuf, U32 width, U32 height);

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/
extern U32 IANA_SHOT_RefineBallPos_Bulk(iana_t *piana, U32 camid);
// extern U32 refineShot(iana_t *piana); // not used
extern I32 getp3d(iana_t *piana, cr_point_t point[2], cr_point_t *p3d);
// extern U32 updateVmag(iana_t *piana, U32 camid); // not used
extern U32 apparentBallRadius(iana_t *piana, U32 camid);
extern U32 bulkFirstindex(iana_t *piana, U32 camid, U32 *pFirstindex); 
extern I32 iana_clubpath_cam_param_lite(iana_t *piana, U32 camid, U32 normalbulk);
extern I32 check_isIron(iana_t *piana, U32 camid);
extern double quadregression_errcalc(double xx[], double yy[], U32 count, double ma[3]);

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
I32 iana_shot_calc(iana_t *piana);
U32 iana_shot_select_winner(iana_t *piana, U32 camid);
static I32 GetShotCandidate(iana_t *piana, U32 camid);
static I32 RegressShotData(iana_t *piana, U32 camid);
I32 iana_shot_plane(iana_t *piana, U32 camid);
I32 iana_shot_3Dtrajline(iana_t *piana);
I32 iana_shot_allpointsonline(iana_t *piana, U32 camid);
static I32 GetPointOnLine(iana_t *piana, U32 camid, cr_point_t *pongline, cr_point_t *ponline);

I32 iana_requestBulk(iana_t *piana);
I32 iana_requestBulk_TimeStamp(iana_t *piana, U64 *pts64C, U64 *pts64S);
#if defined(CHECKREADYFILE)
static I32 CheckReadyBall(iana_t *piana);
#endif
static I32 GetShotPlane0(iana_t *piana, U32 camid);
static I32 GetShotPlane3(iana_t *piana, U32 camid);

I32 display_shotcalc(iana_t *piana, U32 camid);

I32 iana_shot_refine(iana_t *piana);

static I32 CalcBallRadiusUsing3DPositions(iana_t *piana);
static U32 CheckShotResult(iana_t *piana);
static void Calc_Incline_Azimuth(iana_t *piana);
static I32 CalcVmag(iana_t *piana);
static void OptionalFileOperation4GoodShot(iana_t *piana);
static void UpdateShotResultCheckFlag(iana_t *piana);

/*----------------------------------------------------------------------------
 *	Description	: local functions
 -----------------------------------------------------------------------------*/

static void ResetAssuranceClubData(iana_shotresult_t *psr)
{
	psr->Assurance_clubspeed_B = 0;
	psr->Assurance_clubspeed_A = 0;
	psr->Assurance_clubpath = 0;
	psr->Assurance_clubfaceangle = 0;
	psr->Assurance_clubattackangle = 0;
	psr->Assurance_clubloftangle = 0;
	psr->Assurance_clublieangle = 0;
	psr->Assurance_clubfaceimpactLateral = 0;
	psr->Assurance_clubfaceimpactVertical = 0;
}


static int vmagcompare(const void *arg1, const void *arg2)
{
	int res;
	double v1, v2;

	v1 = *(double *) arg1;
	v2 = *(double *) arg2;
	if (v1 < v2) {
		res = 1;
	} else if (v1 == v2) {
		res = 0;
	} else {
		res = -1;
	}

	return res;
}

/*!
 ********************************************************************************
 *	@brief      CAM shot
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/02/24
 *******************************************************************************/
I32 iana_shot(iana_t *piana)
{
	I32 res;

	//--------------------------------------------------
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, "%s[%d]: ++ with hiana: %p\n",  __func__, __LINE__, piana);

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "before iana_shot_calc\n");

	//--- 2) Get basic shot data (Vmag, incline, azimuth)
	piana->ballpaircount = 0;
	memset(&piana->ballpair[0], 0, sizeof(U32) * MARKSEQUENCELEN);
	res = iana_shot_calc(piana);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "res of iana_shot_calc(): %d\n", res);
	{
		iana_shotresult_t *psr;
		
		psr = &piana->shotresultdata;
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "vmag    : %10.4lf\n", psr->vmag);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "incline : %10.4lf\n", psr->incline);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "azimuth : %10.4lf\n", psr->azimuth);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "backspin: %10.4lf\n", (double)psr->backspin);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "sidespin: %10.4lf\n", (double)psr->sidespin);
	}
#define DISPLAY_SHOT_CALC
#if defined(DISPLAY_SHOT_CALC)
	if (piana->opmode == IANA_OPMODE_FILE) {
		display_shotcalc(piana, 0);
		display_shotcalc(piana, 1);
	}
#endif


//#define FAKERESULT 1

#if defined(FAKERESULT)
	res = FAKERESULT;
#endif

	if (res == 0) {
		piana->shotresultflag = SHOTRESULT_NOSHOT;			// NO SHOT

		goto func_exit;
	}

#if defined(CHECKREADYFILE)
	{
		FILE *fp;
		U32 checkmode;

		checkmode = 1;
		fp = cr_fopen(CHECKREADYFILE, "r");
		if (fp) {
			fscanf(fp, "%d", &checkmode);
			cr_fclose(fp);
		}


		if (checkmode) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "call CheckReadyBall");
			res = CheckReadyBall(piana);
		}
	}
#endif

	if (res == 0) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Result of readyball: %d. Make No-shot.. -_-;\n", res);
		piana->shotresultflag = SHOTRESULT_NOSHOT;			// Make No shot.

		goto func_exit;
	}
	
//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "processarea: %d\n", piana->processarea);
	if (piana->processarea == IANA_AREA_PUTTER) {
		iana_shotresult_t *psr, *psr2;
		psr  = &piana->shotresultdata;
		psr2 = &piana->shotresultdata2;

		//--
		if (piana->camsensor_category == CAMSENSOR_EYEXO) {
			memcpy(psr2, psr, sizeof(iana_shotresult_t));				// 20200603
		}

		if (piana->opmode != IANA_OPMODE_FILE) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Putter, SCAMIF_STOP\n");
			scamif_stop(piana->hscamif);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Putter, SCAMIF_STOP done\n");

		} else {
		}
		piana->shotresultflag = SHOTRESULT_GOODSHOT;		
		goto func_exit;
	}

	if (piana->opmode == IANA_OPMODE_FILE) {
	} else {
		scamif_stop(piana->hscamif);
	}
// In case of P3V2, lock later
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "scamif_lock() here EXCEPT P3V2..\n");
	if (piana->camsensor_category != CAMSENSOR_P3V2) {				// 20200903
		scamif_lock(piana->hscamif, SCAMIF_LOCK_NORMAL_WRITE);					
	}

//#define SLEEP_AFTER_BULK_REQUEST	100
#if defined(SLEEP_AFTER_BULK_REQUEST)
	cr_sleep(SLEEP_AFTER_BULK_REQUEST);
#endif

	if (piana->camsensor_category != CAMSENSOR_P3V2) 
	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "before iana_spin()\n");
		res = iana_spin(piana);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "after iana_spin(), res: %d\n", res);
		iana_spin_check(piana);
	}

	if (piana->camsensor_category == CAMSENSOR_Z3 || piana->camsensor_category == CAMSENSOR_EYEXO) {
		iana_shot_recalc(piana);
	}

	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "iana_requestBulk() here for P3V2..\n");
		iana_requestBulk(piana);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "after iana_requestBulk()\n");

		res = iana_club_calc(piana);
		res = iana_club_spin_estimate(
				piana,
				0,					//camid,
				0.0				// angleoffset
				);

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "scamif_lock() here for P3V2..\n");
		scamif_lock(piana->hscamif, SCAMIF_LOCK_NORMAL_WRITE);					// 20200903
        IANA_SHOT_RefineBallPos_Bulk(piana, 0);
        IANA_SHOT_RefineBallPos_Bulk(piana, 1);

		iana_shot_recalc(piana);
		{
			U32 isIron;
			double vmag, incline, azimuth;
			iana_shotresult_t *psr, *psr2;

			//---------- // 20200612

			isIron = check_isIron(piana, 0 /*camid*/);
			psr	= &piana->shotresultdata;
			psr2= &piana->shotresultdata2;

			vmag = psr->vmag;
			incline = psr->incline;
			azimuth = psr->azimuth;

			IANA_CLUB_ModifyShot4P3V2(
					&vmag,
					&incline,
					isIron		// FIX to IRON. :P
					);

			//-------  20200915.. 


			if (piana->camsensor_category == CAMSENSOR_P3V2) {	// TODO: PH3 duplicated code . fixme after P3 stable code merge
				double vmagadd, azimuthadd, inclineadd;
				double vmagmult, azimuthmult, inclinemult;

				//--

				// SG's suggestion. 20200915 
//#define VMAG_MULT_IRON_SG		1.05
//#define INCLINE_MULT_IRON_SG	1.015

//#define VMAG_MULT_DRIVER_SG		1.0
//#define INCLINE_MULT_DRIVER_SG	1.025

#define VMAG_MULT_IRON_SG		1.0
#define INCLINE_MULT_IRON_SG	1.025

#define VMAG_MULT_DRIVER_SG		1.05
#define INCLINE_MULT_DRIVER_SG	1.015

				vmagadd = 0;
				azimuthadd = 0;
				inclineadd = 0;
				vmagmult = 1.0;
				azimuthmult = 1.0;
				inclinemult = 1.0;
				if (isIron) {
					vmagmult = VMAG_MULT_IRON_SG;
					inclinemult = INCLINE_MULT_IRON_SG;
				}
				else {
					vmagmult = VMAG_MULT_DRIVER_SG;
					inclinemult = INCLINE_MULT_DRIVER_SG;
				}

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "P3SHOT isIron: %d vmag: %lf -> %lf (%lf, %lf) incline: %lf -> %lf (%lf, %lf) azimuth: %lf -> %lf (%lf, %lf)\n",
					isIron,
					vmag, vmagadd, vmagmult, (vmag + vmagadd) * vmagmult,
					incline, inclineadd, inclinemult, (incline + inclineadd) * inclinemult,
					azimuth, azimuthadd, azimuthmult, (azimuth + azimuthadd) * azimuthmult);

				vmag = (vmag + vmagadd) * vmagmult;
				incline = (incline + inclineadd) * inclinemult;
				azimuth = (azimuth + azimuthadd) * azimuthmult;
			}

			//-------  20200911.. 

#if defined(P3V2SHOTFILEIRON) && defined(P3V2SHOTFILEDRIVER)
			{

				U32 shotmode;
				FILE *fp;
				double vmagadd, azimuthadd, inclineadd;
				double vmagmult, azimuthmult, inclinemult;

				//--

				vmagadd = 0;
				azimuthadd = 0;
				inclineadd = 0;
				vmagmult = 1.0;
				azimuthmult = 1.0;
				inclinemult = 1.0;
				if (isIron) {
					fp = cr_fopen(P3V2SHOTFILEIRON, "r");
				} else {
					fp = cr_fopen(P3V2SHOTFILEDRIVER, "r");
				}
				if (fp) {
					fscanf(fp, "%d", &shotmode);
					fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
					if (shotmode == 1) {
						fscanf(fp, "%lf", &vmagadd);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
						fscanf(fp, "%lf", &vmagmult);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
						fscanf(fp, "%lf", &inclineadd);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
						fscanf(fp, "%lf", &inclinemult);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
						fscanf(fp, "%lf", &azimuthadd);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
						fscanf(fp, "%lf", &azimuthmult);
					}
					cr_fclose(fp);
				}

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "P3SHOT isIron: %d vmag: %lf -> %lf (%lf, %lf) incline: %lf -> %lf (%lf, %lf) azimuth: %lf -> %lf (%lf, %lf)\n", 
						isIron, 
						vmag, vmagadd, vmagmult, (vmag + vmagadd) * vmagmult,
						incline, inclineadd, inclinemult, (incline + inclineadd) * inclinemult,
						azimuth, azimuthadd, azimuthmult, (azimuth + azimuthadd) * azimuthmult);

				vmag = (vmag + vmagadd) * vmagmult;
				incline = (incline + inclineadd) * inclinemult;
				azimuth = (azimuth + azimuthadd) * azimuthmult;
			}
#endif
// TODO: duplicated////////////////////////////////////////////////////////////////////
			psr2->vmag 		= psr->vmag 	= vmag;
			psr2->incline 	= psr->incline 	= incline;
			psr2->azimuth 	= psr->azimuth 	= azimuth;
		}
	} else {
		iana_clubpath_cam_param_lite(piana, 0, NORMALBULK_NORMAL);
		iana_clubpath_cam_param_lite(piana, 1, NORMALBULK_NORMAL);

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"() clubcalc_method_required: %d\n", piana->clubcalc_method_required);
#define CLUBMODE_TEST

#if defined(CLUBMODE_TEST)
		{
			FILE *fp;
			U32 mode;
			U32 clubmode;
#if defined(_WIN32)
			fp = cr_fopen("d://clubmode.txt", "r");
#else
#define CLUBMODE_TXT_PATH CONFIG_DIR"/clubmode.txt"

			fp = cr_fopen(CLUBMODE_TXT_PATH, "r");
#endif
			if (fp) {
				fscanf(fp, "%d", &mode);
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
				fscanf(fp, "%d", &clubmode);
				cr_fclose(fp);
				if (mode) {
					piana->clubcalc_method_required = clubmode;
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"() clubcalc_method_required: ----->    %d\n", piana->clubcalc_method_required);
				}
			} 
		}
#endif

		if (piana->camsensor_category == CAMSENSOR_Z3 || 
			piana->camsensor_category == CAMSENSOR_CBALL) {
			iana_shotresult_t *psr, *psr2;
			U32 clubcalc_mode;

       //0: CR2CLUBCALC_DEFAULT
       //1: CR2CLUBCALC_NOCALC
       //2: CR2CLUBCALC_CLUB_NOMARK
       //3: CR2CLUBCALC_CLUB_MARK1
       //4: CR2CLUBCALC_CLUB_MARK2

			psr  = &piana->shotresultdata;
			psr2 = &piana->shotresultdata2;

			if (piana->clubcalc_method_required == CR2CLUBCALC_CLUB_MARK1) {  // CR2CLUBCALC_CLUB_MARK1
				clubcalc_mode = CLUBCALC_CLUB_MARK_TOPBAR;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"() Z3, required: %d, mode: CLUBCALC_CLUB_MARK_TOPBAR\n", 
						piana->clubcalc_method_required);
			} else {
				clubcalc_mode = CR2CLUBCALC_CLUB_NOMARK;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"() Z3, required: %d, mode: CR2CLUBCALC_CLUB_NOMARK\n", 
						piana->clubcalc_method_required);
			}
			
			if (clubcalc_mode == CLUBCALC_CLUB_MARK_TOPBAR) {
				res = iana_club_mark(piana, CLUBCALC_CLUB_MARK_TOPBAR);
			} else {
				res = iana_club_estimate(piana);
				res = iana_spin_clubpath(piana, NORMALBULK_NORMAL);					
			}
		} else if (piana->camsensor_category == CAMSENSOR_EYEXO) {
			iana_shotresult_t *psr, *psr2;
			U32 clubcalc_mode;

			//-----
			if (piana->clubcalc_method_required == CR2CLUBCALC_CLUB_MARK1) {
				clubcalc_mode = CLUBCALC_CLUB_MARK_BARDOT;
			} else if (piana->clubcalc_method_required == CR2CLUBCALC_CLUB_MARK2) {
				clubcalc_mode = CLUBCALC_CLUB_MARK_TOPBAR;
			} else if (piana->clubcalc_method_required == CR2CLUBCALC_DEFAULT) {
				clubcalc_mode = CLUBCALC_CLUB_MARK_BARDOT;
			} else {
				clubcalc_mode = CLUBCALC_NOCALC;
			}
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"() clubcalc_method_required: %d clubcalc_mode: %d\n", piana->clubcalc_method_required, clubcalc_mode);

			if (clubcalc_mode == CLUBCALC_CLUB_MARK_BARDOT 
				|| clubcalc_mode == CLUBCALC_CLUB_MARK_TOPBAR) 
			{ 
				res = iana_club_mark(piana, clubcalc_mode);

				psr  = &piana->shotresultdata;
				psr2 = &piana->shotresultdata2;
				if (psr->Assurance_clubspeed_B < 10) {
					iana_shotresult_t *psr0;

					//--

					iana_club_estimate(piana);
					psr0 = &piana->shotresult[0];

					psr->clubspeed_A = psr0->clubspeed_A;
					psr->clubspeed_B = psr0->clubspeed_B;
					psr->clubpath = 0;

					//psr->Assurance_clubspeed_B = 30;
					//psr->Assurance_clubspeed_A = 30;
					psr->Assurance_clubspeed_B = 0;
					psr->Assurance_clubspeed_A = 0;
					psr->Assurance_clubpath = 0;


					psr2->clubspeed_A = psr0->clubspeed_A;
					psr2->clubspeed_B = psr0->clubspeed_B;
					psr2->clubpath = 0;

					//psr2->Assurance_clubspeed_B = 30;
					//psr2->Assurance_clubspeed_A = 30;
					psr2->Assurance_clubspeed_B = 0;
					psr2->Assurance_clubspeed_A = 0;
					psr2->Assurance_clubpath = 0;

				}
				iana_spin_real_clubdata(piana);
			} else {
				psr  = &piana->shotresultdata;
				psr2 = &piana->shotresultdata2;

				ResetAssuranceClubData(psr);
				ResetAssuranceClubData(psr2);				
			}

		} else {
			res = iana_club_estimate(piana);
			res = iana_spin_clubpath(piana, NORMALBULK_NORMAL);
		}

		if (piana->clubcalc_method_required == CR2CLUBCALC_NOCALC) {
			iana_shotresult_t *psr, *psr2;

			psr  = &piana->shotresultdata;
			psr2 = &piana->shotresultdata2;

			ResetAssuranceClubData(psr);
			ResetAssuranceClubData(psr2);
		}


		{
			iana_shotresult_t *psr;

			psr  = &piana->shotresultdata;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"()\n"
				"Assurance_clubspeed_B = %d\n"
				"Assurance_clubspeed_A = %d\n"
				"Assurance_clubpath = %d\n"
				"Assurance_clubfaceangle = %d\n"
				"Assurance_clubattackangle = %d\n"
				"Assurance_clubloftangle = %d\n"
				"Assurance_clublieangle = %d\n"
				"Assurance_clubfaceimpactLateral = %d\n"
				"Assurance_clubfaceimpactVertical = %d\n"
				,
				psr->Assurance_clubspeed_B,
				psr->Assurance_clubspeed_A,
				psr->Assurance_clubpath,
				psr->Assurance_clubfaceangle,
				psr->Assurance_clubattackangle,
				psr->Assurance_clubloftangle,
				psr->Assurance_clublieangle,
				psr->Assurance_clubfaceimpactLateral,
				psr->Assurance_clubfaceimpactVertical);
		}

	}

		// Check shot result.
#if 0
	if (res) {
		res = CheckShotResult(piana);
	}
#endif

	if (res == 0) {
		piana->shotresultflag = SHOTRESULT_NOSHOT;			// NO SHOT
		goto func_exit;
	} else 	{
		//--
		piana->shotresultflag = SHOTRESULT_GOODSHOT;			// GOOD SHOT!!!!

		OptionalFileOperation4GoodShot(piana);
	}

	UpdateShotResultCheckFlag(piana);

func_exit:
//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res %d\n", res);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"-- with res %d\n", res);
	return res;
}


/*!
 ********************************************************************************
 *	@brief      CAM shot refine.
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2020/03/04
 *******************************************************************************/
I32 iana_shot_refine(iana_t *piana)
{
	I32 res;
	U32 camid;
	double	tsdiff;		// timestamp difference in sec..  ts_of_cam0 - ts_of_cam1 20200304   
	iana_cam_t	*pic;
	marksequence_t	*pmks;
	//point_t Pp;

	U32 marksequencelen;

	double tt[MARKSEQUENCELEN], xx[MARKSEQUENCELEN], yy[MARKSEQUENCELEN];
	double xma[MAXCAMCOUNT][3];
	double yma[MAXCAMCOUNT][3];

	U32 mavalid[MAXCAMCOUNT];

	I32 indexshot;
	double ts64dshot;
	//---
	tsdiff = piana->tsdiff;


	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}


	indexshot 	= piana->bulkshotindex[1];		// shot time of CAM1
	pic 		= piana->pic[1];	
	pmks 		= &pic->mks;

	ts64dshot 	= pmks->ts64[indexshot] / TSSCALE_D;
	for (camid = 0; camid < NUM_CAM; camid++) {
		U32 i;

		U32 count;
		double xr2, yr2;
		double xer2, yer2;

		//---
		//indexshot 	= piana->bulkshotindex[camid];
		pic 		= piana->pic[camid];	
		pmks 		= &pic->mks;

		//ts64dshot 	= pmks->ts64[indexshot] / TSSCALE_D;

		count = 0;
		for (i = 0; i < marksequencelen; i++) {
			double td;
			double x_, y_;

			if (s_pbtype == 0)			// 0: P2Raw, 1: P2
			{
				x_ = pmks->ballposP2Raw[i].x;
				y_ = pmks->ballposP2Raw[i].y;
			} else {
				x_ = pmks->ballposP2[i].x;
				y_ = pmks->ballposP2[i].y;
			}
			if (pmks->ballPos2valid[i] == 1) {
				if (s_useL) {
					point_t Pp, Lp;

					Pp.x = x_;
					Pp.y = y_;
					iana_P2L(piana, camid, &Pp, &Lp, 0);
					xx[count] = Lp.x;
					yy[count] = Lp.y;
				} else {
					xx[count] = x_;
					yy[count] = y_;
				}


				if (x_ > 0 && y_ > 0) {
					td = pmks->ts64[i] / TSSCALE_D - ts64dshot;
					if (camid == 0) {
						td = td - tsdiff; // shift ts of cam0  to cam1						
					}
					if (td > 0) {
						tt[count] = td;
						count++;
					}
				}
			}

#define XY_QUADREG_ERR		50
//#define XY_QUADREG_COUNT	4
#define XY_QUADREG_COUNT	99999
#define XY_REG_COUNT		3

		} 	// for (i = 0; i < marksequencelen; i++) 

		mavalid[camid] = 0;
#define CCCC_	5
//#define CCCC_	10
		if (count > CCCC_) {
			count = CCCC_;
		}
		if (count >= XY_QUADREG_COUNT) {
			caw_quadraticregression(tt, yy, count, &yma[camid][2], &yma[camid][1], &yma[camid][0], &yr2);
			caw_quadraticregression(tt, xx, count, &xma[camid][2], &xma[camid][1], &xma[camid][0], &xr2);

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] QUAD regression xma: %lf %lf %lf, %lf   yma: %lf %lf %lf, %lf\n",
					camid,
					xma[camid][2], xma[camid][1], xma[camid][0], xr2,
					yma[camid][2], yma[camid][1], yma[camid][0], yr2);

			yer2 = quadregression_errcalc(tt, yy, count, &yma[camid][0]);
			xer2 = quadregression_errcalc(tt, xx, count, &xma[camid][0]);

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mean square Err: %lf %lf \n", xer2, yer2);

			if (xer2 > XY_QUADREG_ERR || yer2 > XY_QUADREG_ERR) {
				mavalid[camid] = 0;
			} else {
				mavalid[camid] = 1;
			}
		}
		if (count >= XY_REG_COUNT && mavalid[camid] == 0) {
			xma[camid][2] = 0;
			yma[camid][2] = 0;

			cr_regression2(tt, yy, count, &yma[camid][1], &yma[camid][0], &yr2);
			cr_regression2(tt, xx, count, &xma[camid][1], &xma[camid][0], &xr2);

			yer2 = quadregression_errcalc(tt, yy, count, &yma[camid][0]);
			xer2 = quadregression_errcalc(tt, xx, count, &xma[camid][0]);


			if (xer2 > XY_QUADREG_ERR || yer2 > XY_QUADREG_ERR) {
				mavalid[camid] = 0;
			} else {
				mavalid[camid] = 1;
			}
		}
	} 	// for (camid = 0; camid < 2; camid++) 

	if (mavalid[0] == 0 || mavalid[1] ==0) {
		res = 0;
		goto func_exit;
	}
#define TDRANGE	0.010
#define TDINC	0.001
	{
		point_t Pp, Lp;
		cr_point_t position0[MAXCAMCOUNT];
		cr_point_t p3d0;
		cr_point_t p3d0_;
		double td, td2;
		double td_;

		U32 firstcalc;

		//--
		firstcalc = 1;

		p3d0_.x = p3d0_.y = p3d0_.z = 0;
		td_ = 0;
		for (td = 0; td < TDRANGE; td += 0.001) {
			td2 = td*td;

			for (camid = 0; camid < NUM_CAM; camid++) {
				Pp.x = td2 * xma[camid][2] + td * xma[camid][1] + xma[camid][0];
				Pp.y = td2 * yma[camid][2] + td * yma[camid][1] + yma[camid][0];
				if (s_useL) {
					Lp.x = Pp.x;
					Lp.y = Pp.y;
				} else {
					iana_P2L(piana, camid, &Pp, &Lp, 0);
				}

				position0[camid].x = Lp.x; position0[camid].y = Lp.y; position0[camid].z = 0;
			}
			getp3d(piana, position0, &p3d0);

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%10lf  %10lf %10lf %10lf\n",
					td, 
					p3d0.x,
					p3d0.y,
					p3d0.z);

			if (firstcalc) {
				firstcalc = 0;
			} else {
				double dx, dy, dz;
				double dxy, dlen;
				double azimuth, incline;
				double vmag;

				dx = p3d0.x -p3d0_.x;
				dy = p3d0.y -p3d0_.y;
				dz = p3d0.z -p3d0_.z;

				dxy = sqrt(dx*dx + dy*dy);
				dlen = sqrt(dxy*dxy + dz*dz);

				azimuth = RADIAN2DEGREE(atan2(dx, dy));
				incline = RADIAN2DEGREE(atan2(dz, dxy));

				vmag = dlen / (td-td_);

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%10lf  vmag: %10lf, azimuth: %10lf, incline: %10lf\n", td, vmag, azimuth, incline);
			}
			memcpy(&p3d0_, &p3d0, sizeof(cr_point_t));
			td_ = td;
		}
	}
	res = 1;

func_exit:
	return res;
}

#if defined(TS64DELTA_UPDATE)
static void Update_TS64Delta(iana_t *piana)
{
	I64 ts64DeltaOld;
	I64 ts64DeltaNew;
	U32 camidReady;
	U32 camidOther;

	//-----
	camidReady = piana->camidCheckReady;
	camidOther = piana->camidOther;

	ts64DeltaOld = piana->ts64delta;

	piana->ts64delta = piana->pic[camidReady]->ts64shot - piana->pic[camidOther]->ts64shot;
	ts64DeltaNew = piana->ts64delta;

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ts64delta : %016llx (%10lf) -> %016llx (%10lf)\n",
			ts64DeltaOld, ts64DeltaOld/TSSCALE_D, ts64DeltaNew, ts64DeltaNew/TSSCALE_D);
}
#endif

/*!
 ********************************************************************************
 *	@brief      CAM shot calc
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/02/25
 *******************************************************************************/
I32 iana_shot_calc(iana_t *piana)
{
	I32 res;
	U32 camid;
	iana_cam_t	*pic;
	I32 count0, count1, count2;
	iana_ballcandidate_t *pbs;
	iana_shotresult_t *psr;
	iana_shotresult_t *psr2;
	//	iana_cam_param_t *picp;
	

	I32 iter;

	//----

	res = 0;
	count0 = 0;
	count1 = 0;

	piana->pic[0]->bcseqcount = 0;
	piana->pic[1]->bcseqcount = 0;
	for (iter = 0; iter < 2; iter++) {
		res = 0;
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, "%s[%d]: ++ with hiana: %p\n", __func__, __LINE__, piana);

		psr = &piana->shotresultdata;
		memset(psr, 0, sizeof(iana_shotresult_t));
		memset(&piana->shotresult[0]    , 0, sizeof(iana_shotresult_t) * MAXCAMCOUNT);

		psr2 = &piana->shotresultdata2;
		memset(psr2, 0, sizeof(iana_shotresult_t));
		memset(&piana->shotresult2[0]    , 0, sizeof(iana_shotresult_t) * MAXCAMCOUNT);

		memset(&piana->clubball_shotresultdata  , 0, sizeof(iana_shotresult_t));
		memset(&piana->clubball_shotresult		, 0, sizeof(iana_shotresult_t) * MAXCAMCOUNT);



		psr->valid = 0;

		for (camid = 0; camid < NUM_CAM; camid++) 
		{
			pic = piana->pic[camid];
			memset(&pic->mks, 0, sizeof(marksequence_t));
		}

		{
			U32 camidReady, camidOther;
			U32 camidReadyBackup, camidOtherBackup;

			camidReadyBackup = piana->camidCheckReady;
			camidOtherBackup = piana->camidOther;

			camidReady = 0;				// Center..
			camidOther = 1;				// Side

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "--- get shot candidate...\n");
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---- camidReady = %d.. -----------\n", camidReadyBackup);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "piana->pic[%d]->ts64shot: %llu %lf\n",
					camidReadyBackup,
					piana->pic[camidReadyBackup]->ts64shot,
					piana->pic[camidReadyBackup]->ts64shot / TSSCALE_D
					);

			piana->camidCheckReady	= camidReady;
			piana->camidOther		= camidOther;
			GetShotCandidate(piana, camidReady);			
			if (piana->pic[camidReady]->bcseqcount == 0) {
				piana->camidCheckReady	= camidReadyBackup;	
				piana->camidOther		= camidOtherBackup;	
				res = 0;
				goto func_exit;
			}
			iana_shot_select_winner(piana, camidReady);

			//if (piana->camsensor_category == CAMSENSOR_EYEXO || piana->camsensor_category == CAMSENSOR_P3V2)
			{          // 20200107
				double t0, t1;					// recalc ts64.. :P
//				iana_exile_cam_overlaptime(piana, &t0, &t1);		// Calc overlap time
				t0 = -0.010;
				t1 = 0.010;
				IANA_ExileBallCandidate(piana, camidReady, t0, t1);
				RegressShotData(piana, camidReady);			// Change ts64shot..
				piana->pic[camidOther]->ts64shot = piana->pic[camidReady]->ts64shot;	// Update ts64shot..

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "--> piana->pic[%d]->ts64shot: %llu %lf\n",
						camidReadyBackup,
						piana->pic[camidReadyBackup]->ts64shot,
						piana->pic[camidReadyBackup]->ts64shot / TSSCALE_D
						);
			}
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---- camidOther = %d.. -----------\n", camidOther);
			GetShotCandidate(piana, camidOther);
			iana_shot_select_winner(piana, camidOther);

			piana->camidCheckReady	= camidReadyBackup;	
			piana->camidOther		= camidOtherBackup;	
		}


		for (camid = 0; camid < NUM_CAM; camid++) {
			iana_cam_param_t *picp;
			pic = piana->pic[camid];
			picp 	= &pic->icp;

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] mH: %lf mH_min: %lf mH_max: %lf\n",
					camid, 
					picp->meanRH_last,
					picp->meanRH_min,
					picp->meanRH_max);
		}

		iana_exile(piana);				// Filterling bad ball candidates

		{
			U32		camidReady;
			U32		camidOther;
			U32		countReady;
			U32		countOther;
			I32 	regres;

			camidReady = piana->camidCheckReady;
			camidOther = piana->camidOther;


			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "camidReady %d\n", camidReady);
			regres = RegressShotData(piana, camidReady);
			if (regres < 0) {
				if (iter == 0) {
					// do it again.
					regres = RegressShotData(piana, camidReady);
					if (regres < 0) {
						// do it again.
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "with regres %d, i_shot_reg()\n", regres);
						continue;
					}
				}
			}
			if (regres >= 0) {
				countReady = (U32)regres;
			} else {
				countReady = 0;
			}
			if (countReady == 0) { 
				res = 0;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "EEE %d\n", camidReady );
				break;
				//goto func_exit; 
			}

			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "camidOther %d\n", camidOther);
			countOther = RegressShotData(piana, camidOther);

			if (camidReady == 0) {
				count0 = countReady;
				count1 = countOther;
			} else {
				count0 = countOther;
				count1 = countReady;
			}
		}

		if (count1 == 0) {
			res = 0;
			break;
		}

		count2 = 0;

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "count0: %d, count1: %d count2: %d\n", count0, count1, count2);
#define COUNTTOTAL	(3+2)
		if (count0 + count1 < COUNTTOTAL) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "total count too small. %d\n", count0+count1);
			res = 0;
			break;
			//goto func_exit;
		} else {
			res = 1;
		}


#if defined(TS64DELTA_UPDATE)
		Update_TS64Delta(piana);
#endif


		if (res == 0) { 
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "EEE 1\n");
			break;
			//goto func_exit; 
		}
		
		iana_shot_plane(piana, 0);
		iana_shot_plane(piana, 1);

		{
			U32 res;
			double angle;

			res = iana_shotplane_angle(piana, &angle);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"{%d} Plane angle, res: %d, angle: %lf\n", __LINE__, res, angle);

		}

		iana_shot_3Dtrajline(piana);

		iana_shot_allpointsonline(piana, 0);
		iana_shot_allpointsonline(piana, 1);

		// Recalc Ball Radius using 3D-information.  (20200721)
		CalcBallRadiusUsing3DPositions(piana);

		// Calc incline and azimuth
		Calc_Incline_Azimuth(piana);

		// Calc Vmag
		res = CalcVmag(piana);
		if (res == 0) { 
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "EEE 1\n");
			break;
			//goto func_exit; 
		}

		psr->shotAssurance = 1.0;   // :)

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "vmag: %10.4lf, incline: %10.4lf, azimuth: %10.4lf\n",
		//		psr->vmag, psr->incline, psr->azimuth);
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "--------------------------\n\n\n\n");
		res = 1;
		//-------------- Check shot parameter and filtering

#define VMAG_MIN0	2
#define VMAG_MIN1	1
#define VMAG_MAX	100

#define VMAG_MIN0_STRICT	4
#define VMAG_MIN1_STRICT	3
#define VMAG_MAX_STRICT		85

#define VMAG_MIN0_PUTTER	1
#define VMAG_MIN1_PUTTER	0.2
#define VMAG_MAX_PUTTER	30

		if (piana->processarea == IANA_AREA_PUTTER) {		// REESTIMATE result
			U32 i;
			U32 bfirst;
			double azimuth[MAXCAMCOUNT], vmag[MAXCAMCOUNT];
			U32 count[MAXCAMCOUNT];
			double xx[BALLSEQUENCELEN + 1], yy[BALLSEQUENCELEN + 1], vv[BALLSEQUENCELEN + 1];
			double tt[BALLSEQUENCELEN + 1], ttdelta[BALLSEQUENCELEN + 1];
			double matx[3], maty[3], mayv[3], mayx[3];

			for (camid = 0; camid < NUM_CAM; camid++) {
				cr_point_t BallP0, BallP, BallPprev;
				U32 cnt;
				double tsd0, tsd, tsdprev;

				pic = piana->pic[camid];

				tsd0 = 0;
				tsd = 0;
				tsdprev = 0;
				bfirst = 1;
				cnt = 0;
				vmag[camid] = -999;
				azimuth[camid] = -999;
				memset(&BallPprev, 0, sizeof(cr_point_t));
				for (i = 0; i < BALLSEQUENCELEN; i++) {
					//pbs = &piana->ic[camid].bs[i];
					pbs = &pic->bs[i];
					if (pbs->cexist == IANA_BALL_EXIST) {
						double dx, dy;
						tsd = (double)((pbs->ts64) / TSSCALE_D);
						memcpy(&BallP, &pic->PointOnLine[i], sizeof(cr_point_t));
						if (bfirst) {
							tsd0 = tsd;
							memcpy(&BallP0, &BallP, sizeof(cr_point_t));

							tsdprev = tsd;
							memcpy(&BallPprev, &BallP, sizeof(cr_point_t));
							bfirst = 0;
							continue;
						}

						xx[cnt] = BallP.x;
						yy[cnt] = BallP.y;
						tt[cnt] = tsd - tsd0;
						ttdelta[cnt] = tsd - tsdprev;

						dx = BallP.x - BallPprev.x;
						dy = BallP.y - BallPprev.y;

						vv[cnt] = sqrt(dx*dx + dy*dy);
						vv[cnt] = vv[cnt] / ttdelta[cnt];

						tsdprev = tsd;
						memcpy(&BallPprev, &BallP, sizeof(cr_point_t));
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d %2d] %10lf\n", camid, i, vv[cnt]);
						cnt++;
					}
				}
				if (cnt > 3) {
					double theta;
					double r2;
					double y;
					caw_quadraticregression(tt, xx, cnt, &matx[2], &matx[1], &matx[0], &r2);
					caw_quadraticregression(tt, yy, cnt, &maty[2], &maty[1], &maty[0], &r2);
					caw_quadraticregression(yy, vv, cnt, &mayv[2], &mayv[1], &mayv[0], &r2);
					caw_quadraticregression(yy, xx, cnt, &mayx[2], &mayx[1], &mayx[0], &r2);

					/*
					   vmag[camid] = mayv[2] * VMAGCALC_YVALUE * VMAGCALC_YVALUE + mayv[1] * VMAGCALC_YVALUE + mayv[0];
					   */
					y = (yy[cnt-1] + yy[0]) / 2;
					vmag[camid] = mayv[2] * y * y + mayv[1] * y + mayv[0];

					theta =  atan((2 * mayx[2] * y + mayx[1]));

					azimuth[camid] = RADIAN2DEGREE(theta);
				} else {
					cnt = 0;
				}
				count[camid] = cnt;

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] count: %d, vmag: %10lf azimuth: %10lf\n", camid, cnt, vmag[camid], azimuth[camid]);
			} // for (camid = 0; camid < 2; camid++) 

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ORG  vmag: %10lf azimuth: %10lf\n", psr->vmag, psr->azimuth);

			if (count[0] == 0 && count[1] == 0) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "NO!!\n");
			} else {
				if (count[0] > count[1]) {
					camid = 0;
				} else {
					camid = 1;
				}

				psr->vmag = vmag[camid];
				psr->azimuth = azimuth[camid];
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "====>vmag: %10lf azimuth: %10lf\n\n", psr->vmag, psr->azimuth);
			}
			psr->incline = 0.0;					// Putter.. incline is ZERO. :P

			if (psr->vmag > VMAG_MAX_PUTTER) {
				res = 0;
			} else if (psr->vmag > VMAG_MIN0_PUTTER) {
				if (psr->azimuth < -60 || psr->azimuth > 60) {
					res = 0;
				}
			} else if (psr->vmag > VMAG_MIN1_PUTTER) {
				if (psr->azimuth < -30 || psr->azimuth > 30) {
					res = 0;
				}
			} else {
				res = 0;
			}
		} else {
			double vmag_min0, vmag_min1;
			double vmag_max;
			U32 readytime;
			U32 currenttick;

#define READYTIME_CHECK	(3 * 1000)
			currenttick = cr_gettickcount();
			readytime = currenttick - piana->readystarttick;
			if (currenttick > piana->readystarttick + READYTIME_CHECK) {
				vmag_min0 = VMAG_MIN0;
				vmag_min1 = VMAG_MIN1;
				vmag_max  = VMAG_MAX;
			} else {
				vmag_min0 = VMAG_MIN0_STRICT;
				vmag_min1 = VMAG_MIN0_STRICT;
				vmag_max  = VMAG_MAX_STRICT;
			}

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "readytime: %dm, vmag_min0: %lf vmag_min1: %lf vmag_max: %lf\n", readytime, vmag_min0, vmag_min1, vmag_max);

			if (psr->vmag > vmag_max) {
				res = 0;
			} else if (psr->vmag > vmag_min1) {
				// check incline and azimuth
				if (psr->incline < -20 /*-10*/ || psr->incline > 88) {
					res = 0;
				}
				if (psr->azimuth < -60 || psr->azimuth > 60) {
					res = 0;
				}
			} else {
				res = 0;
			}

			if (piana->camsensor_category == CAMSENSOR_P3V2) {	// 20200612
				IANA_CLUB_ModifyShot4P3V2(
						&psr->vmag,
						&psr->incline,
						1		// FIX to IRON. :P
						);
			}
		}
		break;
	} 		// for (iter = 0; iter < 2; iter++) 

	
	if (res) {
		U32 docheckresult;

		docheckresult = 1;
		if (piana->camsensor_category == CAMSENSOR_P3V2) {
			if (piana->processarea == IANA_AREA_PUTTER) {
				docheckresult = 0;
			}
		}

		if (docheckresult) {
			res = CheckShotResult(piana);			// check result once more.. 20200819
		}
	}
	
func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res %d\n", res);

	return res;
}



I32 display_shotcalc(iana_t *piana, U32 camid)
{
#ifdef IPP_SUPPORT
	I32 res;
	I32 i;
	U32 m;
	I32 rindex;
	U32 rindexshot;

	iana_cam_t	*pic;
	iana_cam_param_t *picp;
	camif_buffer_info_t  *pb;
	camimageinfo_t	*pcii;
	scamif_t * pscamif;

	I64 ts64;
	double ts64d;
	I64 ts64m;
	I64 ts64delta;
	double ts64deltad;

	I64 ts64shot;
	double ts64dshot;
	U08 *procimg;

	U32 multitude;
	U32 skip, syncdiv;
	U64 period;
	U64 periodm;
	U32 offset_x;
	U32 offset_y;
	U32 width, height;

	double	xfact, yfact; 
	IppiSize ssize;
	IppiRect sroi, droi;
	int sstep, dstep;
	IppStatus status;
	Ipp8u 	*resizebuf;
	int 	bufsize;
	U08 *buf;
	//-------------

	pscamif = (scamif_t *) piana->hscamif;
	
	pic = piana->pic[camid];
	picp = &pic->icp;

	rindexshot = pic->rindexshot;
	ts64shot = pic->ts64shot;
	ts64dshot = ts64shot / TSSCALE_D;
	procimg = pic->pimgFull;
	offset_x = pic->offset_x;
	offset_y = pic->offset_y;
//#define XYXYFACT	0.5	
#define XYXYFACT	1.0
	resizebuf = NULL;
	for (i = -2; i < 6; i++) {
		rindex = rindexshot + i;
		res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_NORMAL, rindex, &pb, &buf);
		if (res == 0) {
			continue;
		}

		pcii 		= &pb->cii;
		period = TSSCALE / (U64) pcii->framerate;			// nsec
		multitude 	= pcii->multitude;
		if (multitude == 0) {
			multitude = 1;
		}
		skip 	= pcii->skip;
		if (skip == 0) {
			skip = 1;
		}
		syncdiv = pcii->syncdiv;

		if (syncdiv == 0 || camid == (U32)pscamif->mastercamid) {				// master: always 1
			syncdiv = 1;
		}
		ts64 = MAKEU64(pb->ts_h,pb->ts_l);
		ts64d = ts64 / TSSCALE_D;

		periodm = period * skip * syncdiv;

		xfact 		= XYXYFACT;
		yfact 		= (XYXYFACT * multitude);

		width 		= pcii->width;
		height 		= pcii->height;

		ssize.width 	= width;
		ssize.height 	= height;

		sroi.x 		= 0;
		sroi.y 		= 0;
		sroi.width 	= width;
		sroi.height	= height/multitude;

		droi.x 		= 0;
		droi.y 		= 0;
		droi.width 	= (int) (sroi.width * xfact + 0.1);
		droi.height	= (int) (sroi.height * yfact + 0.1);

		sstep 		= sroi.width;
		dstep		= droi.width;

		if (resizebuf == NULL) {
			ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_CUBIC, &bufsize);
			resizebuf = (U08 *)malloc(bufsize);
		}


		for (m = 0; m < multitude; m++) {
			status = ippiResizeSqrPixel_8u_C1R(
					(Ipp8u *)buf + pcii->width * ((m*pcii->height) / multitude),
					ssize,
					sstep,
					sroi,
					//
					(Ipp8u*) procimg,
					dstep,
					droi,
					//
					xfact,
					yfact,
					0,		// xshift,
					0,		// yshift,
					IPPI_INTER_CUBIC, 		// IPPI_INTER_NN,   // IPPI_INTER_CUBIC,
					resizebuf
					);



			ts64m = ts64 - (periodm * (multitude - m -1));

			ts64delta = ts64m - pic->ts64shot;
			ts64deltad = ts64delta / TSSCALE_D;

			if (ts64deltad < 0) {
				continue;
			}

			if (piana->opmode == IANA_OPMODE_FILE) {
				I32 x, y, r;
				double ma[3];
				double td;

				//--
				td = ts64deltad;
				//x = (I32) (picp->mPx_ * ts64deltad + picp->bPx_   - offset_x);
				//y = (I32) (picp->mPy_ * ts64deltad + picp->bPy_   - offset_y);
				//r = (I32) (picp->mPr_ * ts64deltad + picp->bPr_);

				memcpy(&ma[0], &picp->maPx[0], sizeof(double)*3);
				x = (I32) (ma[2]*td*td + ma[1]*td + ma[0] - offset_x);
				memcpy(&ma[0], &picp->maPy[0], sizeof(double)*3);
				y = (I32) (ma[2]*td*td + ma[1]*td + ma[0] - offset_y);
				memcpy(&ma[0], &picp->maPr[0], sizeof(double)*3);
				r = (I32) (ma[2]*td*td + ma[1]*td + ma[0]);

				mycircleGray(x, y, r, 0xFF, procimg, width);
			}

			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf + pcii->width * ((m*pcii->height) / multitude), sroi.width, sroi.height, 0, 1, 2, 3, piana->imguserparam);
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)procimg, droi.width, droi.height, 1, 1, 2, 3, piana->imguserparam);
			}
		}
	}

	res = 1;

	if (resizebuf != NULL) {
		free(resizebuf);
		resizebuf = NULL;
	}
	return res;
#else

    I32 res;
    I32 i;
    U32 m;
    I32 rindex;
    U32 rindexshot;

    iana_cam_t	*pic;
    iana_cam_param_t *picp;
    camif_buffer_info_t  *pb;
    camimageinfo_t	*pcii;
    scamif_t * pscamif;

    I64 ts64;
    double ts64d;
    I64 ts64m;
    I64 ts64delta;
    double ts64deltad;

    I64 ts64shot;
    double ts64dshot;
    U08 *procimg;

    U32 multitude;
    U32 skip, syncdiv;
    U64 period;
    U64 periodm;
    U32 offset_x;
    U32 offset_y;
    U32 width, height;
    U32 widthCrop, heightCrop, widthResize, heightResize;

    double	xfact, yfact;
    U08 *buf;
    //-------------

    pscamif = (scamif_t *)piana->hscamif;

    pic = piana->pic[camid];
    picp = &pic->icp;

    rindexshot = pic->rindexshot;
    ts64shot = pic->ts64shot;
    ts64dshot = ts64shot / TSSCALE_D;
    procimg = pic->pimgFull;
    offset_x = pic->offset_x;
    offset_y = pic->offset_y;

#define XYXYFACT	1.0
    for (i = -2; i < 6; i++) {
        rindex = rindexshot + i;
        res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_NORMAL, rindex, &pb, &buf);
        if (res == 0) {
            continue;
        }

        pcii 		= &pb->cii;
        period = TSSCALE / (U64)pcii->framerate;			// nsec
        multitude 	= pcii->multitude;
        if (multitude == 0) {
            multitude = 1;
        }
        
        skip 	= pcii->skip;
        if (skip == 0) {
            skip = 1;
        }
        
        syncdiv = pcii->syncdiv;
        if (syncdiv == 0 || camid == (U32)pscamif->mastercamid) {				// master: always 1
            syncdiv = 1;
        }
        
        ts64 = MAKEU64(pb->ts_h, pb->ts_l);
        ts64d = ts64 / TSSCALE_D;

        periodm = period * skip * syncdiv;

        xfact 		= XYXYFACT;
        yfact 		= (XYXYFACT * multitude);

        width 		= pcii->width;
        height 		= pcii->height;

        widthCrop 	= width;
        heightCrop	= height/multitude;

        widthResize 	= (int)(widthCrop * xfact + 0.1);
        heightResize	= (int)(heightCrop * yfact + 0.1);

        for (m = 0; m < multitude; m++) {
            cv::Mat cvImgSrc(cv::Size(width, height), CV_8UC1, buf), cvImgCropped, cvImgResized;
            
            cvImgCropped = cvImgSrc(cv::Rect(0, m*height/multitude, widthCrop, heightCrop));
            cv::resize(cvImgCropped, cvImgResized, cv::Size(widthResize, heightResize), xfact, yfact, cv::INTER_CUBIC);

            memcpy(procimg, cvImgResized.data, sizeof(U08) * cvImgResized.cols * cvImgResized.rows);

            ts64m = ts64 - (periodm * (multitude - m -1));

            ts64delta = ts64m - pic->ts64shot;
            ts64deltad = ts64delta / TSSCALE_D;

            if (ts64deltad < 0) {
                continue;
            }

            if (piana->opmode == IANA_OPMODE_FILE) {
                I32 x, y, r;
                double ma[3];
                double td;

                //--
                td = ts64deltad;

                memcpy(&ma[0], &picp->maPx[0], sizeof(double)*3);
                x = (I32)(ma[2]*td*td + ma[1]*td + ma[0] - offset_x);
                memcpy(&ma[0], &picp->maPy[0], sizeof(double)*3);
                y = (I32)(ma[2]*td*td + ma[1]*td + ma[0] - offset_y);
                memcpy(&ma[0], &picp->maPr[0], sizeof(double)*3);
                r = (I32)(ma[2]*td*td + ma[1]*td + ma[0]);

                mycircleGray(x, y, r, 0xFF, procimg, width);
            }

            if (piana->himgfunc) {
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf + pcii->width * ((m*pcii->height) / multitude), widthCrop, heightCrop, 0, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)procimg, widthResize, heightResize, 1, 1, 2, 3, piana->imguserparam);
            }
        }
    }

    res = 1;

    return res;
#endif
}


static I32 CheckReadyBallPosition(iana_cam_t	*pic, camimageinfo_t	*pcii, U08 *buf, U32 multitude)
{	//-----------------------  check ready ball position
	I32 res0;
	iana_ballcandidate_t bc;
	I32 cxx, cyy, crr;
	I32 i0, j0;
	I32 x0, y0;
	I32 w0, h0;
	iana_cam_param_t *picp;

	picp = &pic->icp;

	//--
	cxx = (I32)(pic->icp.P.x - pic->offset_x);
	crr = (I32)(pic->icp.cr);
	//				crr = 20;
	w0 = crr*2;
	h0 = crr*2;

	cyy = (I32)((pic->icp.P.y - pic->offset_y + (multitude-1)* pcii->height) / multitude);

	y0 = cyy;
	y0 = y0 - crr/multitude;

	x0 = cxx;
	x0 = x0 - crr;

	for (i0 = 0; i0 < h0; i0++) {
		for (j0 = 0; j0 < w0; j0++) {
			*(pic->pimgFull + (i0 * w0) +j0) = *(buf + (y0 + i0/multitude) * pcii->width + (j0 + x0));
		}
	}

	memset(&bc, 0, sizeof(iana_ballcandidate_t));

	bc.P.x = crr;
	bc.P.y = crr;
	bc.cr = crr;

	res0 =  calc_circle_mean_stddev(&bc, pic->pimgFull, w0, h0);

	if (res0) {
		double meanH;

		meanH = bc.meanH;
		picp->meanRH_last = meanH;
		if (picp->meanRH_min > meanH) {
			picp->meanRH_min = meanH;
		}
		if (picp->meanRH_max < meanH) {
			picp->meanRH_max = meanH;
		}
		/*
		   cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] res0: %d. m: %lf s: %lf  mH: %lf, sH: %lf\n",
		   camid, res0,
		   bc.mean, bc.stddev,
		   bc.meanH, bc.stddevH);
		   */
		/*
		   cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] mH: %lf mH_min: %lf mH_max: %lf\n",
		   camid, 
		   picp->meanRH_last,
		   picp->meanRH_min,
		   picp->meanRH_max);
		   */
	}

	return res0;
}




/*!
 ********************************************************************************
 *	@brief      CAM shot candidate
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/02/25
 *******************************************************************************/
static I32 GetShotCandidate(iana_t *piana, U32 camid)
{
#ifdef IPP_SUPPORT
	U32 res;

	camif_buffer_info_t *pb;
	iana_cam_t	*pic;
	camimageinfo_t	*pcii;
	U32 multitude;
	U32 skip, syncdiv;
	U64 period;
	U64 periodm;
	U32 offset_x;
	U32 offset_y;
	U32 width, height;

	U64 ts64;
	double ts64d;
	U64 ts64m;

	U64 ts64shot;
	double ts64dshot;

	U32 rindexF;
	double ts64dF;
	U08 *procimg;
	U32 rindexshot;

	iana_ballcandidate_t bc[MAXCANDIDATE];

	double	xfact, yfact; 
	U32 shot_halfwinsize;

	double cermax;
	double crmin;
	double crmax;


	IppiSize ssize;
	IppiRect sroi, droi;
	int sstep, dstep;
	IppStatus status;
	Ipp8u 	*resizebuf;
	int 	bufsize;
	U08 *buf;
	U08 *bufOrg;

	//U32 i, j;
	I32 i, j;
	U32 m;

	I32 i0, i1, i2;

	U32 seqcount;
	U32 noballcount;
	scamif_t * pscamif;

	iana_cam_param_t *picp;

	cr_point_t shotstartL;

	U08		*pRefImg;

	U16 	*sumbuf16;
	U16 	*curbuf16;
	U08 	*diffbuf;
	U32		sumbufwidth;
	U32		sumbufheight;
	//U32		sumbufinited;
	U32		sumbufcount;
	IppiSize buf16roisize;

	
	U32 	bDiff = 1;
	U32		bRefImgMade;
	//-----------------------------------------------------
	//-- Candidate.
	if (camid >= NUM_CAM) {
		res = 0;
		goto func_exit;
	}

	pscamif = (scamif_t *) piana->hscamif;

	pic = piana->pic[camid];
	picp = &pic->icp;

	pb = NULL;
	buf = NULL;

	memcpy(&shotstartL, &picp->L, sizeof(cr_point_t));

	rindexshot = pic->rindexshot;
	ts64shot = pic->ts64shot;
	ts64dshot = ts64shot / TSSCALE_D;
	procimg = pic->pimgFull;
	offset_x = pic->offset_x;
	offset_y = pic->offset_y;
		

	memset(&pic->bc[0], 0, sizeof(iana_ballcandidate_t) * BALLSEQUENCELEN * BALLCCOUNT);
	memset(&bc[0], 		0, sizeof(iana_ballcandidate_t) * MAXCANDIDATE);

	pic->bcseqcount = 0;
	ts64shot = 0;

	picp->meanRH_min = 999;
	picp->meanRH_max = 0;

	picp->meanRH_last = 0;


	if (piana->opmode == IANA_OPMODE_FILE) {
		if (camid < NUM_CAM ) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_NORMAL, rindexshot, &pb, NULL);
		} else {
			res = 0;
		}
	} else {
		if (camid < NUM_CAM ) {
			res = scamif_imagebuf_peek_latest(piana->hscamif, camid, NORMALBULK_NORMAL, &pb, NULL, NULL);
		} else {
			res = 0;
		}
	}

	if (res == 0) {
		goto func_exit;
	}

	pcii 		= &pb->cii;
	period = TSSCALE / (U64) pcii->framerate;			// nsec
	multitude 	= pcii->multitude;
	if (multitude == 0) {
		multitude = 1;
	}
	skip 	= pcii->skip;
	if (skip == 0) {
		skip = 1;
	}
	syncdiv = pcii->syncdiv;

	//if (syncdiv == 0 || camid == 0) {				// master: always 1
	//	syncdiv = 1;
	//}

	if (syncdiv == 0 || camid == (U32)pscamif->mastercamid) {				// master: always 1
		syncdiv = 1;
	}


	periodm = period * skip * syncdiv;

#define SHOT_HALFWINSIZE	8
#define XYFACT				(0.5)
//#define XYFACT_EYEXO_CAM0				1   // better, but not enough...
#define XYFACT_EYEXO_CAM0				0.5
#define XYFACT_EYEXO_CAM1				0.5
//#define XYFACT_CONSOLE1		(0.3)
//#define XYFACT_CONSOLE1		(0.4)
#define XYFACT_CONSOLE1		0.5




	if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
		xfact 		= XYFACT_CONSOLE1;
		yfact 		= (XYFACT_CONSOLE1 * multitude);
		//shot_halfwinsize = (U32)(0.8 * SHOT_HALFWINSIZE * XYFACT_CONSOLE1 / XYFACT);
		shot_halfwinsize = (U32)(SHOT_HALFWINSIZE * XYFACT_CONSOLE1 / XYFACT);
		//shot_halfwinsize = (U32)(1.0 * SHOT_HALFWINSIZE * XYFACT_CONSOLE1 / XYFACT);
	} else if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		if (camid == 0) {
			xfact 		= XYFACT_EYEXO_CAM0;
			yfact 		= (xfact * multitude);
			shot_halfwinsize = SHOT_HALFWINSIZE;
		} else {
			xfact 		= XYFACT_EYEXO_CAM1;
			yfact 		= (xfact * multitude);
			shot_halfwinsize = SHOT_HALFWINSIZE;
		}
	} else {
		xfact 		= XYFACT;
		yfact 		= (XYFACT * multitude);
		shot_halfwinsize = SHOT_HALFWINSIZE;
	}

	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		if (camid == 0) {
			shot_halfwinsize = SHOT_HALFWINSIZE;
		} else {
			shot_halfwinsize = SHOT_HALFWINSIZE;
		}
	}
	////shot_halfwinsize = (U32)(SHOT_HALFWINSIZE*1.5);
	width 		= pcii->width;
	height 		= pcii->height;

	ssize.width 	= width;
	ssize.height 	= height;

	sroi.x 		= 0;
	sroi.y 		= 0;
	sroi.width 	= width;
	sroi.height	= height/multitude;

	droi.x 		= 0;
	droi.y 		= 0;
	droi.width 	= (int) (sroi.width * xfact + 0.1);
	droi.height	= (int) (sroi.height * yfact + 0.1);

	sstep 		= sroi.width;
	dstep		= droi.width;

	ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_CUBIC, &bufsize);
	//resizebuf = ippsMalloc_8u(bufsize);
	resizebuf = (U08 *)malloc(bufsize);


	//--
	sumbufwidth 	= width;
	sumbufheight 	= height;
	sumbufcount 	= 0;
	buf16roisize.width = width;
	buf16roisize.height = height;
	bRefImgMade		= 0;
	sumbuf16 		= NULL;
	curbuf16		= NULL;
	diffbuf			= NULL;

	if (bDiff) {
		pRefImg = (U08*) pic->prefimg;
		memset(pRefImg, 0, FULLWIDTH * FULLHEIGHT);
		//sumbuf16 		= (Ipp16u *)ippsMalloc_8u(width * height * sizeof(Ipp16u));	
		//curbuf16		= (Ipp16u *)ippsMalloc_8u(width * height * sizeof(Ipp16u));	
		//diffbuf			= (U08 *)	ippsMalloc_8u(width * height * sizeof(U08));	
#define GUARDMEM		(32 * 1024)
		sumbuf16 		= (Ipp16u *)malloc(width * height * sizeof(Ipp16u) + GUARDMEM);	
		curbuf16		= (Ipp16u *)malloc(width * height * sizeof(Ipp16u) + GUARDMEM);	
		diffbuf			= (U08 *)	malloc(width * height * sizeof(U08) + GUARDMEM);	
		memset(sumbuf16, 0, width * height * sizeof(Ipp16u));
		memset(curbuf16, 0, width * height * sizeof(Ipp16u));
		memset(diffbuf, 0, width * height * sizeof(U08));
	}

	//crmin  = pic->icp.BallsizeMin * xfact / 2.0;
	//crmax  = pic->icp.BallsizeMax * xfact / 2.0;

	{
		cr_rect_t rectRunP;
		double crminratio, crmaxratio;

#define RECTOFFSET	70
		rectRunP.left = offset_x - RECTOFFSET;
		rectRunP.top = offset_y - RECTOFFSET;

		if (rectRunP.left < 0)  rectRunP.left = 0;
		if (rectRunP.top < 0)  rectRunP.top = 0;

		//rectRunP.right = offset_x + sroi.width;
		//rectRunP.bottom = offset_y + sroi.height;
		rectRunP.right = offset_x + width + RECTOFFSET;
		rectRunP.bottom = offset_y + height + RECTOFFSET;


//		iana_setReadyRunBallsizeMinMax(piana, camid, xfact);
		iana_setRunRunBallsizeMinMax(piana, camid, &rectRunP, xfact);

		crminratio = CRMINRATIO_;
		crmaxratio = IANA_BALLDETECT_GetMaxCircleRatio2(piana->camsensor_category, camid);
		
		crmin = (piana->pic[camid]->icp.RunBallsizePMin / 2.0) * crminratio;
		crmax = (piana->pic[camid]->icp.RunBallsizePMax / 2.0) * crmaxratio;

		if (piana->camsensor_category == CAMSENSOR_P3V2) {
#define CRMAX_ALLOWED_CR_RATIO_P3V2   2.0
			if (crmax > piana->pic[camid]->icp.cr * xfact * CRMAX_ALLOWED_CR_RATIO_P3V2) {
				crmax = piana->pic[camid]->icp.cr * xfact * CRMAX_ALLOWED_CR_RATIO_P3V2;
			}
		}

#define MINCRMIN	5.0
		if (crmin < MINCRMIN) {
			crmin = MINCRMIN;
		}
	}
	cermax = pic->icp.CerBallshot;


	seqcount = 0;
	noballcount = 0;
	ts64dF = -1;
	rindexF = 0;

	//calcstate = 0;			// 0: BEFORE_SHOT,  1: SHOT DETECTED
	//shotballcount = 0;
//#define REFIMGCOUNT	4
#define REFIMGCOUNT	1

//#define READCOUNT_CANDIDATE2	10
//#define READCOUNT_CANDIDATE2	8
//#define READCOUNT_CANDIDATE2	10
#define READCOUNT_CANDIDATE2	16
//#define READCOUNT_CANDIDATE2	14
//#define READCOUNT_CANDIDATE2	20
//#define BEFORE_CHECK	(-5)
//#define BEFORE_CHECK	(-4)
//#define BEFORE_CHECK	(-6)
//#define BEFORE_CHECK	(-8)
/////////////#define BEFORE_CHECK	(-10)
//#define BEFORE_CHECK	(-16)

//#define BEFORE_CHECK	(-4)
#define BEFORE_CHECK	(-2)
	
	if (piana->processarea != IANA_AREA_PUTTER) {
		i0 = BEFORE_CHECK;
		i1 = (U32)(READCOUNT_CANDIDATE+s_preshot);
		i2 = (U32)(READCOUNT_CANDIDATE2+s_preshot);
	} else {
		i0 = BEFORE_CHECK;
		//#define PUTTERPROCESSINGCOUNT		20
		/////#define PUTTERPROCESSINGCOUNT		24
#define PUTTERPROCESSINGCOUNT		INFOIMAGE_PUTTER_COUNT
		i1 = PUTTERPROCESSINGCOUNT;
		i2 = i1;
	}

#define TCAM_I_MULT	(0.6)
	if (piana->camsensor_category == CAMSENSOR_TCAM) {
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "_TCAM, i0: %d, i1: %d, i2: %d\n", i0, i1, i2);
		i0 = (I32) (i0 *TCAM_I_MULT);
		i1 = (I32) (i1 *TCAM_I_MULT);
		i2 = (I32) (i2 *TCAM_I_MULT);
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-----> i0: %d, i1: %d, i2: %d\n", i0, i1, i2);

	}

//#define CONSOLE1_I_MULT_CENTER	0.4
//#define CONSOLE1_I_MULT_SIDE	0.4

#define CONSOLE1_I_MULT_CENTER	0.4
#define CONSOLE1_I_MULT_SIDE	0.4
	if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
		double i_mult;
		
		if (camid == 0) {
			i_mult = CONSOLE1_I_MULT_CENTER;
		} else {
			i_mult = CONSOLE1_I_MULT_SIDE;
		}
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "_TCAM, i0: %d, i1: %d, i2: %d\n", i0, i1, i2);
//		i0 = (I32) -1;
		//i0 = (I32) -4;

		i0 = (I32) -6;

		i0 = (I32) s_i0;
		i1 = (I32) 4;
		i2 = (I32) 5;

		//i1 = (I32) 6;
		//i2 = (I32) 7;

		//i1 = (I32) 10;
		//i2 = (I32) 12;

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-----> i0: %d, i1: %d, i2: %d\n", i0, i1, i2);

		s_preshot = 0;
	}


    for (i = i0; i < (I32)i2; i++) {
		U32 rindex;				// Current processing index;

//		rindex = rindexshot + i - 2;
//		rindex = rindexshot + i - READYIMAGE_PRESHOT;

		rindex = rindexshot + i - s_preshot;
		if (piana->opmode != IANA_OPMODE_FILE) {
			int j;
			U64 ts64_;
			double ts64d_;
			res = 0;
			pb = NULL;
			ts64_ = 0;
			ts64d_ = 0;

#define IMAGE_GET_TRY	10
			for (j = 0; j < IMAGE_GET_TRY; j++) {
				if (camid < NUM_CAM ) {
					res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_NORMAL, rindex, &pb, &buf);
				} else {
					res = 0;
				}
				if (res == 0 || pb == NULL || buf == NULL) {
					cr_sleep(5);
					continue;
				}
				ts64_ = MAKEU64(pb->ts_h,pb->ts_l);
				ts64d_ = ts64_ / TSSCALE_D;

				if (ts64dF < 0.0) {
					ts64dF = ts64d_;
					rindexF = rindex;
				}

				if (res && (ts64d_ >= ts64dF)) {
					break;
				} else {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" SLEEP\n");
					cr_sleep(5);
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" SLEEP done\n");
					continue;
				}
			}
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "R,  res: %d, rindex: %d,  pb: %p, ts64:  %llx(%llu), ts64d:  %lf j: %d\n", res, rindex, pb, ts64, ts64, ts64d, j);
		}
		//if (piana->processarea == IANA_AREA_PUTTER) {
		//	cr_sleep(20);				// FAKE... XXX
		//}

		if (camid < NUM_CAM ) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_NORMAL, rindex, &pb, &buf);
		} else {
			res = 0;
		}

		if (res == 0 || buf == NULL) {
			continue;
		}

		ts64 = MAKEU64(pb->ts_h,pb->ts_l);
		ts64d = ts64 / TSSCALE_D;

		bufOrg = buf;		// backup original buf.

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d], i: %d, rindex: %d, res: %d\n", camid, i, rindex, res);
		if (piana->himgfunc) {
			((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf, pcii->width, pcii->height, 3, 1, 2, 3, piana->imguserparam);
		}
#define CHECK_READYBALLPOS_MAXTIME	((0.001)*100)
		if (ts64d > ts64dshot && ts64d < ts64dshot + CHECK_READYBALLPOS_MAXTIME) {
			CheckReadyBallPosition(pic, pcii, buf, multitude);
		}

		//if (piana->camsensor_category == CAMSENSOR_EYEXO || piana->camsensor_category == CAMSENSOR_P3V2)
		{
			double ts64d_checktime;
//#define TS64D_CHECKTIME	(0.005)
#define TS64D_CHECKTIME_0	(0.015)
#define TS64D_CHECKTIME_1	(0.003)
			if (camid == 0) {
				ts64d_checktime = TS64D_CHECKTIME_0;
			} else {
				ts64d_checktime = TS64D_CHECKTIME_1;
			}
			if (ts64d < ts64dshot - ts64d_checktime) {		// 20210105.. :)
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,  "ts64d: %lf  ( - ts64dshot %lf) = %lf\n", 
						ts64d, ts64dshot,
						ts64d - ts64dshot);
				continue;					
			}
		}

		if (res) {
			double cermax;

			//---
			if (bDiff) { 
				if (bRefImgMade == 0) {

					if (sumbufcount < REFIMGCOUNT) {
						ippiConvert_8u16u_C1R(buf, width,  curbuf16, width*2, buf16roisize);
						ippiAdd_16u_C1IRSfs (curbuf16, width*2, sumbuf16, width*2, buf16roisize, 0);
						sumbufcount++;
						continue;
					}

					ippiDivC_16u_C1IRSfs((Ipp16u)sumbufcount, sumbuf16, width*2, buf16roisize, 0);
					ippiConvert_16u8u_C1R(sumbuf16, width*2,  pic->prefimg, width, buf16roisize);
					if (piana->himgfunc) {
						((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pic->prefimg, sumbufwidth, sumbufheight, 0, 1, 2, 3, piana->imguserparam);
					}
					free(curbuf16);
					free(sumbuf16);
					bRefImgMade = 1;
				}

#define BUFREF_SUB

//#define BUFREF_ABS
				if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
#if 1
					if (camid == 1) {
						gamma_correction(buf, diffbuf, width, height);
					} else {
						memcpy(diffbuf, buf, width*height);
					}
#endif
					if (piana->himgfunc) {
						((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)diffbuf, width, height, 0, 1, 2, 3, piana->imguserparam);
					}

#if 1
#if defined(BUFREF_SUB)
					ippiSub_8u_C1RSfs(			// C = A - B
							pic->prefimg, 	width, 	// B
							buf, 			width, 	// A
							diffbuf, 		width,	// C, 
							buf16roisize, 0);	
#elif defined(BUFREF_ABS)
					ippiAbsDiff_8u_C1R(			// C = A - B
							pic->prefimg, 	width, 	// B
							buf, 			width, 	// A
							diffbuf, 		width,	// C, 
							buf16roisize);
#else
					memcpy(diffbuf, buf, width*height);
#endif

#endif



#if 1
					if (piana->shotarea != IANA_AREA_TEE) {
						I32 sx, ex;
						I32 ii;

						sx = (I32) (picp->P.x - offset_x - crmax*5);
						ex = (I32) (picp->P.x - offset_x + crmax*5);
						for (ii = 0; ii < (I32)height; ii++) {
							memcpy( (diffbuf + sx + ii * width), (buf + sx + ii * width), (ex-sx));
						}
					}
#endif

					if (piana->himgfunc) {
						((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)diffbuf, width, height, 1, 1, 2, 3, piana->imguserparam);
					}



				} else {
#if defined(BUFREF_SUB)
					ippiSub_8u_C1RSfs(			// C = A - B
							pic->prefimg, 	width, 	// B
							buf, 			width, 	// A
							diffbuf, 		width,	// C, 
							buf16roisize, 0);	
#elif defined(BUFREF_ABS)
					ippiAbsDiff_8u_C1R(			// C = A - B
							pic->prefimg, 	width, 	// B
							buf, 			width, 	// A
							diffbuf, 		width,	// C, 
							buf16roisize);
#else
					memcpy(diffbuf, buf, width*height);
#endif
				}
					buf = diffbuf;
			}

			//ts64 = MAKEU64(pb->ts_h,pb->ts_l);
			//ts64d = ts64 / TSSCALE_D;
			cermax = piana->pic[camid]->icp.CerBallshot;

			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "rindex: %d, ts64: %llx (%lf)\n", rindex, ts64, ts64d);
			for (m = 0; m < multitude; m++) 
			//for (m = 0; m < multitude; m +=2)
			{
				U32 bcount;

				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "i m [%d:%d]\n", i, m);
				if (resizebuf == NULL) {
					ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_CUBIC, &bufsize);
					//resizebuf = ippsMalloc_8u(bufsize);
					resizebuf = (U08 *)malloc(bufsize);
				}
				status = ippiResizeSqrPixel_8u_C1R(
						//(Ipp8u *)buf + m * pcii->width * (pcii->height / multitude),
						(Ipp8u *)buf + pcii->width * ((m*pcii->height) / multitude),
						ssize,
						sstep,
						sroi,
						//
						(Ipp8u*) procimg,
						dstep,
						droi,
						//
						xfact,
						yfact,
						0,		// xshift,
						0,		// yshift,
						IPPI_INTER_CUBIC, 		// IPPI_INTER_NN,   // IPPI_INTER_CUBIC,
						resizebuf
						);


				if (piana->himgfunc) {
 					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf + pcii->width * ((m*pcii->height) / multitude), sroi.width, sroi.height, 0, 1, 2, 3, piana->imguserparam);
 					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)procimg, droi.width, droi.height, 1, 1, 2, 3, piana->imguserparam);

				}



				ts64m = ts64 - (periodm * (multitude - m -1));
				{
					double ts64md;

					ts64md = ts64m / TSSCALE_D;
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,  "ts64md: %lf   (shot: %lf)  DELTA: %lf\n", ts64md, ts64dshot
							,ts64md - ts64dshot
							);
				}

#define RUNPROCESSINGAREA


//---------------------------------------------------------------
				{
					I32	widthRun, heightRun;

					I32 width, height;
					I32 startx, starty;
					U32 bcountperseq;
					U32 do_cht;

					double cutmean_shot;


					//--
					bcountperseq = 0;

					width 	= (int)(sroi.width * xfact + 0.1);
					height	= (int)(sroi.height * yfact + 0.1);

#define TRY_CATEGORY7
					if (piana->processarea != IANA_AREA_PUTTER) {
						iana_cam_setup_t *picsetup;
						iana_setup_t *pisetup;

						offset_x;
						picp->P;
						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "offset_x: %d, p.x: %d, (px-ox: %d) vs rPW: %d\n",
						//		(int)offset_x, (int)picp->P.x, 
						//		(int)(picp->P.x - offset_x), 
						//		(int)(piana->ic[camid].runProcessingWidth));


						widthRun  = (int)(piana->pic[camid]->runProcessingWidth * xfact + 0.1);
						heightRun = (int)(piana->pic[camid]->runProcessingHeight * (yfact / multitude) + 0.1);

						if (heightRun > height) {
							heightRun = height;
						}
						if (widthRun > width) {
							widthRun = width;
						}
						startx = 0;

						//starty = (height - heightRun)/2;
						pisetup = &piana->isetup;
						picsetup = &pisetup->icsetup[camid];
#if defined(TRY_CATEGORY7_)
						starty = (I32)(-picsetup->offsetoffset_y * (yfact / multitude)) - heightRun/2;
						if (starty < 0) {
							starty = 0;
						}
#else
						starty = (height - heightRun)/2;
						if (starty < 0) {
							starty = 0;
						}
#endif

//#define WIDTHRUN_MARGIN	16
#if defined(WIDTHRUN_MARGIN)
						if (camid == 0 || camid == 1) {
							I32 ballposx;

							ballposx = (I32)((picp->P.x - offset_x) * xfact);
							widthRun = (ballposx - startx +1) - WIDTHRUN_MARGIN;
							widthRun = ((widthRun + 7) / 8) * 8;
						}
#endif

					} else {
						widthRun = width;
						heightRun = height;
						startx = 0;
						starty = 0;
					}


					bcount = 0;

					if (bcount == 0) {			// Canny .. fail. use adapt.
#define CANDADPT_RETRY_EYEXO	2
#define CANDADPT_RETRY			1
#define CANDADPT_RETRY_A		10
						U32 iter;
						U32 candadpt_retry;
						double thstdmult[CANDADPT_RETRY_A] = {
							1.5 ,3.0, 5.0
						};

						if (piana->camsensor_category == CAMSENSOR_EYEXO) {
							if (camid == 0) {
								thstdmult[0] = 5.0;
								//thstdmult[1] = 2.0;
								thstdmult[1] = 1.0;
								//thstdmult[2] = 10.0;
								thstdmult[2] = 4.0;
								candadpt_retry = 3;
							} else {
								//////thstdmult[0] = 2.0;
								thstdmult[0] = 1.0;
								thstdmult[1] = 5.0;
								//thstdmult[2] = 10.0;
								thstdmult[2] = 2;
								candadpt_retry = CANDADPT_RETRY_EYEXO;
								//candadpt_retry = 3;
							}
						} else {
							//candadpt_retry = CANDADPT_RETRY;
							candadpt_retry = 3;
						}
						for (iter = 0; iter < candadpt_retry; iter++) {
							int rmt;
							int expn;

							cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] iter: %d / %d\n", camid, iter, candadpt_retry);
							if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
								if (camid == 0) {
									rmt = 1;
								} else {
									//rmt = 4;
									rmt = 1;
								}
								expn = 0;
							} else if (piana->camsensor_category == CAMSENSOR_P3V2) {
								if (camid == 0) {
									rmt = 3;
									expn = 1;
								} else {
									rmt = 1;
									expn = 1;
								}
							} else {
								rmt = 3;
								expn = 1;
							}
#define CUTMEAN_SHOT_EYEXO	0.0005
#define CUTMEAN_SHOT	0.5
							if (piana->camsensor_category == CAMSENSOR_EYEXO) {
								if (camid == 0) {
									rmt = 1;
									expn = 0;
								} else {
									if (s_rmt_expn_1) {
										rmt = 1;
										expn = 0;
									} else {
										//rmt = 3;
										//expn = 1;
										//rmt = 2;
										//expn = 0;

										rmt = 1;			// 20200904.. 
										expn = 0;

										//rmt = 5;
										//expn = 2;

									}

								}
								cutmean_shot = CUTMEAN_SHOT_EYEXO;
							} else {
								cutmean_shot = CUTMEAN_SHOT;
							}

							//res = iana_getballcandidator_cht
							res = iana_getballcandidatorAdapt
								(
									piana, 
									camid, 
									(Ipp8u *)procimg + startx + starty * width,
									widthRun, heightRun, width, 
									&bc[0], 
									&bcount,
									shot_halfwinsize, 		// SHOT_HALFWINSIZE, 
									rmt , expn,// 3, 2, // 1, 0, // 4, 0,  // 1, 0,				// READY_MULTV, READY_EXPN,
									thstdmult[iter],
									cermax
									, cutmean_shot					// 20190420, yhsuk
									);
									thstdmult;
							if (bcount > 0) {
								cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Retry GOOD. bcount: %d\n", bcount);
								break;
							}
						}
						//if (bcount == 0) {
						//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Retry FAIL.\n");
						//}
					}

					if (piana->camsensor_category == CAMSENSOR_EYEXO) {
						do_cht = 1;
					} else {
						do_cht = 0;
						//do_cht = 1;
						//s_refine_with_CHT = 0;
					}


					if (bcount == 0 && do_cht) {
						double tsmddelta;

						tsmddelta = ts64m / TSSCALE_D - pic->ts64shot / TSSCALE_D;
//#define TSMDDELTA_RANGE	(0.005)
//#define TSMDDELTA_RANGE	(0.010)
#define TSMDDELTA_RANGE	(0.07)
//#define ADDITIONAL_CHT_SEQCOUNT	8
#define ADDITIONAL_CHT_SEQCOUNT	10
						if (tsmddelta > 0 && tsmddelta <TSMDDELTA_RANGE
								&& (seqcount < ADDITIONAL_CHT_SEQCOUNT)				// 20210105
						   )
						{
							cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "before additional _CHT (seqcount: %d), widthRun, heightRun: %d, %d,  width, height: %d, %d\n",
								seqcount,
								widthRun, heightRun, width, height);
							if (piana->opmode == IANA_OPMODE_FILE) {
								if (piana->himgfunc) {
									((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)(Ipp8u *)procimg + startx + starty * width, width, height, 5, 1, 2, 3, piana->imguserparam);
								}
							}

							res = iana_getballcandidator_cht
								(
									piana, 
									camid, 
									(Ipp8u *)procimg + startx + starty * width,
									widthRun, heightRun, width, 
									&bc[0], 
									&bcount,
									8, 					// FAKE value
									1, 0,				// FAKE value
									5.0, 				// FAKE value
									0.05, 				// FAKE value
									0.05 				// FAKE value
									);
							cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "After additional _CHT\n");
							//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-----> i0: %d, i1: %d, i2: %d\n", i0, i1, i2);
						}
					}


					if (bcount > 0) {
						if (s_refine_with_CHT && do_cht) 
						{	// Refine with CHT algorithm.
							iana_ballcandidate_t bc_cht[MAXCANDIDATE];
							iana_ballcandidate_t bc_refined[MAXCANDIDATE];
							U32 bcount_cht;
							U32 bcount_refine;

							I32 sx, sy, ex, ey;
							I32 widthR, heightR;

							I32 mincrindex;
							double mincr;

							//--
							memset(&bc_refined[0], 0, sizeof(iana_ballcandidate_t) *MAXCANDIDATE);
							memset(&bc_cht[0], 0, sizeof(iana_ballcandidate_t) *MAXCANDIDATE);
							for (j = 0; j < (I32)MAXCANDIDATE; j++) {
								bc_cht[j].cexist 		= IANA_BALL_EMPTY;
								bc_refined[j].cexist 	= IANA_BALL_EMPTY;
							}

							bcount_refine = 0;
							mincrindex = 0;
							mincr = 999;
							for (j = 0; j < (I32)MAXCANDIDATE; j++) {
								if (bc[j].cexist == IANA_BALL_EXIST) {
									sx = (I32) (bc[j].P.x - bc[j].cr - shot_halfwinsize * 2);
									ex = (I32) (bc[j].P.x + bc[j].cr + shot_halfwinsize * 2);

									if (sx < 0) sx = 0;

									sy = (I32) (bc[j].P.y - bc[j].cr - shot_halfwinsize * 2);
									ey = (I32) (bc[j].P.y + bc[j].cr + shot_halfwinsize * 2);

									if (sy < 0) sy = 0;

									widthR = ex - sx + 1;
									widthR= ((widthR + 7) / 8) * 8;

									heightR = ey - sy + 1;

									cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" before _CHT.. %d\n", __LINE__);
									res = iana_getballcandidator_cht
										(
										 piana, 
										 camid, 
										 (Ipp8u *)procimg + sx + sy * width,
										 widthR, heightR, width, 
										 &bc_cht[0], 
										 &bcount_cht,
										 shot_halfwinsize, 		// FAKE..
										 1 , 0,					// FAKE..
										 10,					// FAKE..
										 0.1					// FAKE
										 , 10					// FAKE
										);
									cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" after _CHT.. %d\n", __LINE__);

									if (bc_cht[0].cexist == IANA_BALL_EXIST) {
										I32 ii, jj;
										I32 sxx, syy, exx, eyy;
										I32 wxx, hyy;
										double meanb;
										double meanb_min;
										I32 sumb, countpixel;
										U08 *pball = (U08 *)procimg + sx + sy * width;

										//--- Add checking for pixel mean value of ball candidate.   20201126, yhsuk.
										// bc_cht[j] -> bc_cht[0]    20201228.
										//sxx = (I32)(bc_cht[j].P.x - bc_cht[j].cr * 0.7071);     // 0.7071 ~= 1/SQRT(2)
										sxx = (I32)(bc_cht[0].P.x - bc_cht[0].cr * 0.7071); 
										if (sxx < 0) sxx = 0;
										//syy = (I32)(bc_cht[j].P.y - bc_cht[j].cr * 0.7071);
										syy = (I32)(bc_cht[0].P.y - bc_cht[0].cr * 0.7071);
										if (syy < 0) syy = 0;

										//exx = (I32)(bc_cht[j].P.x + bc_cht[j].cr * 0.7071); 
										//eyy = (I32)(bc_cht[j].P.y + bc_cht[j].cr * 0.7071);
										exx = (I32)(bc_cht[0].P.x + bc_cht[0].cr * 0.7071); 
										eyy = (I32)(bc_cht[0].P.y + bc_cht[0].cr * 0.7071);

										if (exx >= widthR) {
											exx = widthR;
										}

										if (eyy >= heightR) {
											eyy = heightR;
										}

										sumb = 0; countpixel = 0;
										wxx = exx - sxx + 1;
										hyy = eyy - syy + 1;

										if (wxx < 5 || hyy < 5) {
											bc_cht[0].cexist = IANA_BALL_EMPTY;			// NOT good.
											cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" wxx: %d,  hyy: %d.  discard.\n", wxx, hyy);
										} else {
											cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" ------- before xx: \n");
											for (ii = syy; ii < eyy; ii++) {
												for (jj = sxx; jj < exx; jj++) {
													sumb += *(pball + ii * width + jj);
													countpixel++;
												}
											}
											cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" xx: %d ~ %d, yy: %d ~ %d, sumb: %d, countpixel: %d\n",
													sxx, exx, syy, eyy, sumb, countpixel);
											if (countpixel > (5*5)) {
												meanb = (double)(sumb) / countpixel;
											} else {
												meanb = 0;
											}
#define MEANB_MIN0	50
#define MEANB_MIN1	100
											if (seqcount < 5) {
												meanb_min = MEANB_MIN0;
											} else {
												meanb_min = MEANB_MIN1;
											}
											//										cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" sumb: %d countpixel: %d  meanb: %lf\n", sumb, countpixel, meanb);

											if (meanb > meanb_min) {	// Check mean value.
												cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d %d] sumb: %d countpixel: %d  meanb: %lf (> %lf)\n", camid, seqcount, sumb, countpixel, meanb, meanb_min);
												memcpy(&bc_refined[bcount_refine], &bc_cht[0], sizeof(iana_ballcandidate_t));
												bc_refined[bcount_refine].P.x = (bc_refined[bcount_refine].P.x + sx - startx);
												bc_refined[bcount_refine].P.y = (bc_refined[bcount_refine].P.y + sy - starty);
												///////////	bc_refined[j].cexist = IANA_BALL_EXILE;

												if (mincr > bc_refined[bcount_refine].cr) {
													mincrindex = bcount_refine;
													mincr = bc_refined[bcount_refine].cr;
												}
												bcount_refine++;
											}
										}
									}
								}
							}
							if (bcount_refine > 0) {			// refine success!!
								memcpy(&bc[0], &bc_refined[0], sizeof(iana_ballcandidate_t)*bcount_refine);
								bcount = bcount_refine;
							} 
						}

                        for (j = 0; j < (I32)MAXCANDIDATE; j++) {
#if defined (USE_IANA_BALL)
							if (bc[j].cexist == IANA_BALL_EXIST) 
#else
								if (bc[j].cexist)
#endif
								{
                                if (bc[j].cer > cermax || bc[j].cr < crmin || bc[j].cr > crmax) {	// Filtering..
#if defined (USE_IANA_BALL)
										bc[j].cexist = IANA_BALL_EMPTY;
#else
										bc[j].cexist = 0;
#endif
										continue;
									} else {
										point_t Pp, Lp, Lp2;

#define MULTITUDE_FRACTION
										//-- Converse to Pixel point.
										bc[j].P.x = ((bc[j].P.x + startx) / xfact) + offset_x;
#if defined(MULTITUDE_FRACTION)
										bc[j].P.y = ((bc[j].P.y + starty + 0.5) * multitude) / yfact + offset_y;
#else
										bc[j].P.y = ((bc[j].P.y + starty) / (yfact/multitude)) + offset_y;
#endif
										bc[j].P.z = 0.0;
										bc[j].cr /= xfact;

										//-- Converse to Local point.
										Pp.x = bc[j].P.x;
										Pp.y = bc[j].P.y;
										iana_P2L(piana, camid, &Pp, &Lp, 0);
										bc[j].L.x = Lp.x;
										bc[j].L.y = Lp.y;
										bc[j].L.z = 0.0;

										 /*
										cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "c[%d:%d:%d] rindex: %d, bc[%d], P<%10lf %10lf> L<%10lf %10lf> cer: %lf, cr: %lf\n", 
												camid, i, m, rindex, j, 
												bc[j].P.x, bc[j].P.y, 
												bc[j].L.x, bc[j].L.y,
												bc[j].cer,
												bc[j].cr);
										   */
//#define SHOTCHECK_YMARGIN	0.03
#define SHOTCHECK_YMARGIN	0.015
										if (piana->camsensor_category == CAMSENSOR_P3V2) {
											double dx, dy;
											dx = bc[j].L.x - pic->icp.L.x;
											dy = bc[j].L.y - pic->icp.L.y;
											if (dy < SHOTCHECK_YMARGIN) {
												if (dy < -SHOTCHECK_YMARGIN) {
													bc[j].cexist = IANA_BALL_EMPTY;
													continue;
												}
											}
										} else if (piana->camsensor_category == CAMSENSOR_EYEXO) {
											double dx, dy;
											double dlen;
											dx = bc[j].L.x - pic->icp.L.x;
											dy = bc[j].L.y - pic->icp.L.y;
											dlen = sqrt(dx*dx + dy*dy);

#define SHOTCHECK_YMARGIN_EYEXO_CAM0		0.015
#define SHOTCHECK_YMARGIN_EYEXO_CAM1		0.005
#define SHOTCHECK_YMARGIN_EYEXO_CAM1_1		0.001
#define SHOTCHECK_DLENMARGIN_EYEXO_CAM1_1	0.005
											if (camid == 0) {
												if (dy < SHOTCHECK_YMARGIN_EYEXO_CAM0) {
													bc[j].cexist = IANA_BALL_EMPTY;
													continue;
												}
											} else {		// camid == 1
												if (dy < SHOTCHECK_YMARGIN_EYEXO_CAM1) {
													if (!(
															dy > SHOTCHECK_YMARGIN_EYEXO_CAM1_1
															&& dlen >SHOTCHECK_DLENMARGIN_EYEXO_CAM1_1
														)
													   ) {
														bc[j].cexist = IANA_BALL_EMPTY;
														continue;
													}
												}
											}
										} else {
											if (bc[j].L.y < pic->icp.L.y + SHOTCHECK_YMARGIN) {
												bc[j].cexist = IANA_BALL_EMPTY;
												continue;
											}
										}

										//-- get radius
										Pp.x = bc[j].P.x + bc[j].cr;
										Pp.y = bc[j].P.y;
										iana_P2L(piana, camid, &Pp, &Lp2, 0);
										bc[j].lr = DDIST(Lp.x, Lp.y, Lp2.x, Lp2.y);

										//-- Time index
										bc[j].tsindex = rindex;
										bc[j].ts64 = ts64m;
										bc[j].m = m;
										memcpy(&pic->bc[seqcount][bcountperseq], &bc[j], sizeof(iana_ballcandidate_t));
										//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "c[%d] bc[%d], tsindex: %d, ts: %lf, m: %d\n",
										//		camid, j, 
										//		bc[j].tsindex,
										//		bc[j].ts64 / TSSCALE_D,
										//		bc[j].m);

										bcountperseq++;
										if (bcountperseq >= BALLCCOUNT) {
											break;
										}
									}
								} 		// if (bc[i].cexist) 
						} 			// for (i = 0; i < bcount; i++) 
						if (bcountperseq) {
							seqcount++;
							if (seqcount == 1) {
								if (pic->icp.cr < 1) {
									pic->icp.cr = pic->bc[0][0].cr;
								}
							}
//#define MAXSEQCOUNT	5
//#define MAXSEQCOUNT	8
//#define MAXSEQCOUNT	10
#define MAXSEQCOUNT	16
#define CHECKSEQCOUNT		5
//#define CHECKSEQCOUNT		3
//#define CHECKNOBALLCOUNT	10
//#define CHECKNOBALLCOUNT	4
#define CHECKNOBALLCOUNT	3
#define CHECKSEQCOUNT2		8
//#define CHECKNOBALLCOUNT2	5
#define CHECKNOBALLCOUNT2	3
						}
						noballcount = 0;
					} else { 				// if (bcount > 0) 
						noballcount++;

					}
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "seqcount: %d, noballcount: %d\n", seqcount, noballcount);



#define TOOL_IMG_2
#if defined(TOOL_IMG_2)
					if (piana->opmode == IANA_OPMODE_FILE) {
						I32 cxx, cyy, crr;
						if (bcount > 0) {
							for (j = 0; j < (I32)MAXCANDIDATE; j++)
							{
								if (bc[j].cexist == IANA_BALL_EXIST) {
									cxx = (I32)((bc[j].P.x - offset_x) * xfact - startx);
									cyy = (I32)((bc[j].P.y - offset_y) * (yfact/multitude) - starty);
									crr = (I32)(bc[j].cr * xfact);
									mycircleGray(cxx, cyy, crr, 0xFF, (U08 *)procimg, width);
									cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "c[%d:%2d:%d] rindex: %3d, tmd: %10.6lf (%10.6lf) bc[%d], P<%10lf %10lf> L<%10lf %10lf> cer: %lf, cr: %lf\n", 
											camid, i, m, rindex, 
											(ts64m / TSSCALE_D),
											(ts64m / TSSCALE_D - ts64dshot),
											j, 
											bc[j].P.x, bc[j].P.y, 
											bc[j].L.x, bc[j].L.y,
											bc[j].cer,
											bc[j].cr);
								}
							}
						}

						if (piana->himgfunc) {
							((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)procimg, width, height, 3 /*4*/, 1, 2, 3, piana->imguserparam);
						}
					}
#endif
					// IMGCALLBACK for shot candidator.. with mult-data.
					if (seqcount >= MAXSEQCOUNT) {
						break;
					}
				}

				// DISPLAY_DISPLAY
				if (seqcount >= CHECKSEQCOUNT) {
					if (noballcount > CHECKNOBALLCOUNT) {
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "SEQ limit #0  seqcount: %d, noballcount: %d\n", seqcount, noballcount );
						break;
					}
				}

			} 					// for (m = 0; m < multitude; m++) 
		}

		if (seqcount >= CHECKSEQCOUNT) {
			if (noballcount > CHECKNOBALLCOUNT) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "SEQ limit #0  seqcount: %d, noballcount: %d\n", seqcount, noballcount );
				break;
			}
		}


		if (seqcount >= CHECKSEQCOUNT2) {
			if (noballcount > CHECKNOBALLCOUNT2) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "SEQ limit #0-1  seqcount: %d, noballcount: %d\n", seqcount, noballcount );
				break;
			}
		}

		//if (seqcount >= BALLSEQUENCELEN) 
        if (seqcount >= MAXSEQCOUNT) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "SEQ limit #1  %d\n", seqcount );
			break;
		}
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------\nCAM[%d] %d seqcount: %d\n", camid, i, seqcount);

		if (i >= i1-1) {
//#define MINSEQCOUNT	4
#define MINSEQCOUNT	5
			if (seqcount > MINSEQCOUNT) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "SEQ limit #2  %d\n", seqcount );
				break;
			}
#if 0			// comment out. .20190504
			if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {			// Match to GCQ. 20190406
				if (seqcount == 0) {
					break;
				}
			}
#endif
		}
	} 							// for (i = 0; i < READCOUNT; i++) 
	pic->bcseqcount = seqcount;
	
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------\nCAM[%d] seqcount: %d\n", camid, seqcount);

#if 0
	for (i = 0; i < (I32)seqcount; i++) {
		for (j = 0; j < BALLCCOUNT; j++) {
			iana_ballcandidate_t *pbc;
			pbc = &pic->bc[i][j];
#if defined (USE_IANA_BALL)
			if (pbc->cexist == IANA_BALL_EXIST) 
#else
			if (pbc->cexist)
#endif
			{
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "bc[%d,%d] <idx:%d, m: %d, ts: %lf>\n",
						i, j, pbc->tsindex, pbc->m, pbc->ts64/TSSCALE_D);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " p<%lf,%lf:%lf,%lf>, l<%lf, %lf>\n", 
						pbc->P.x, pbc->P.y, pbc->cr, pbc->cer, 
						pbc->L.x, pbc->L.y);
			}
		}
	}
#endif
	//ippsFree(resizebuf);
	free(resizebuf);
	if (bDiff) {
		//ippsFree(diffbuf);
		free(diffbuf);
	}

	//ippsFree(prevImg);
	//ippsFree(dImg);

	res = 1;
func_exit:
	return res;
#else
// TODO: latest code merge - opencv
    U32 res;

    camif_buffer_info_t *pb;
    iana_cam_t	*pic;
    camimageinfo_t	*pcii;
    U32 multitude;
    U32 skip, syncdiv;
    U64 period;
    U64 periodm;
    U32 offset_x;
    U32 offset_y;
    U32 width, height;

    U64 ts64;
    double ts64d;
    U64 ts64m;

    U64 ts64shot;
    double ts64dshot;

    U32 rindexF;
    double ts64dF;
    U08 *procimg;
    U32 rindexshot;

    iana_ballcandidate_t bc[MAXCANDIDATE];

    double	xfact, yfact;
    U32 shot_halfwinsize;

    double cermax;
    double crmin;
    double crmax;


    cv::Size ssize;
    cv::Rect sroi, droi;
    int sstep, dstep;
    U08 *buf;
    U08 *bufOrg;

    I32 i, j;
    U32 m;

    I32 i0, i1, i2;

    U32 seqcount;
    U32 noballcount;
    scamif_t * pscamif;

    iana_cam_param_t *picp;

    cr_point_t shotstartL;

    U08		*pRefImg;

    U16 	*sumbuf16;
    U16 	*curbuf16;
    U08 	*diffbuf;
    U32		sumbufwidth;
    U32		sumbufheight;

    U32		sumbufcount;
    cv::Size buf16roisize;

    U32		bRefImgMade;

    //-- Candidate.
    if (camid >= NUM_CAM) {
        res = 0;
        goto func_exit;
    }

    pscamif = (scamif_t *)piana->hscamif;

    pic = piana->pic[camid];
    picp = &pic->icp;

    pb = NULL;
    buf = NULL;

    memcpy(&shotstartL, &picp->L, sizeof(cr_point_t));

    rindexshot = pic->rindexshot;
    ts64shot = pic->ts64shot;
    ts64dshot = ts64shot / TSSCALE_D;
    procimg = pic->pimgFull;
    offset_x = pic->offset_x;
    offset_y = pic->offset_y;


    memset(&pic->bc[0], 0, sizeof(iana_ballcandidate_t) * BALLSEQUENCELEN * BALLCCOUNT);
    memset(&bc[0], 0, sizeof(iana_ballcandidate_t) * MAXCANDIDATE);

    pic->bcseqcount = 0;
    ts64shot = 0;

    picp->meanRH_min = 999;
    picp->meanRH_max = 0;

    picp->meanRH_last = 0;


    if (piana->opmode == IANA_OPMODE_FILE) {
        if (camid < NUM_CAM) {
            res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_NORMAL, rindexshot, &pb, NULL);
        } else {
            res = 0;
        }
    } else {
        if (camid < NUM_CAM) {
            res = scamif_imagebuf_peek_latest(piana->hscamif, camid, NORMALBULK_NORMAL, &pb, NULL, NULL);
        } else {
            res = 0;
        }
    }

    if (res == 0) {
        goto func_exit;
    }

    pcii 		= &pb->cii;
    period = TSSCALE / (U64)pcii->framerate;			// nsec
    multitude 	= pcii->multitude;
    if (multitude == 0) {
        multitude = 1;
    }
    skip 	= pcii->skip;
    if (skip == 0) {
        skip = 1;
    }
    syncdiv = pcii->syncdiv;

    if (syncdiv == 0 || camid == (U32)pscamif->mastercamid) {				// master: always 1
        syncdiv = 1;
    }


    periodm = period * skip * syncdiv;

#define SHOT_HALFWINSIZE	8
#define XYFACT				(0.5)
#define XYFACT_EYEXO_CAM0				0.5
#define XYFACT_EYEXO_CAM1				0.5
#define XYFACT_CONSOLE1		0.5

    if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
        xfact 		= XYFACT_CONSOLE1;
        yfact 		= (XYFACT_CONSOLE1 * multitude);
        shot_halfwinsize = (U32)(SHOT_HALFWINSIZE * XYFACT_CONSOLE1 / XYFACT);
    } else if (piana->camsensor_category == CAMSENSOR_EYEXO) {
        if (camid == 0) {
            xfact 		= XYFACT_EYEXO_CAM0;
            yfact 		= (xfact * multitude);
            shot_halfwinsize = SHOT_HALFWINSIZE;
        } else {
            xfact 		= XYFACT_EYEXO_CAM1;
            yfact 		= (xfact * multitude);
            shot_halfwinsize = SHOT_HALFWINSIZE;
        }
    } else {
        xfact 		= XYFACT;
        yfact 		= (XYFACT * multitude);
        shot_halfwinsize = SHOT_HALFWINSIZE;
    }

    if (piana->camsensor_category == CAMSENSOR_EYEXO) {
        if (camid == 0) {
            shot_halfwinsize = SHOT_HALFWINSIZE;
        } else {
            shot_halfwinsize = SHOT_HALFWINSIZE;
        }
    }

    width 		= pcii->width;
    height 		= pcii->height;

    ssize.width 	= width;
    ssize.height 	= height;
    
    sroi.x 		= 0;
    sroi.y 		= 0;
    sroi.width 	= width;
    sroi.height	= height/multitude;

    droi.x 		= 0;
    droi.y 		= 0;
    droi.width 	= (int)(sroi.width * xfact + 0.1);
    droi.height	= (int)(sroi.height * yfact + 0.1);

    sstep 		= sroi.width;
    dstep		= droi.width;

    //--
    sumbufwidth 	= width;
    sumbufheight 	= height;
    sumbufcount 	= 0;
    buf16roisize.width = width;
    buf16roisize.height = height;
    bRefImgMade		= 0;
    sumbuf16 		= NULL;
    curbuf16		= NULL;
    diffbuf			= NULL;

    pRefImg = (U08*)pic->prefimg;
    memset(pRefImg, 0, FULLWIDTH * FULLHEIGHT);

#define GUARDMEM		(32 * 1024)
    sumbuf16 		= (U16 *)malloc(width * height * sizeof(U16) + GUARDMEM);
    curbuf16		= (U16 *)malloc(width * height * sizeof(U16) + GUARDMEM);
    diffbuf			= (U08 *)malloc(width * height * sizeof(U08) + GUARDMEM);
    memset(sumbuf16, 0, width * height * sizeof(U16));
    memset(curbuf16, 0, width * height * sizeof(U16));
    memset(diffbuf, 0, width * height * sizeof(U08));

    {
        cr_rect_t rectRunP;
        double crminratio, crmaxratio;

#define RECTOFFSET	70
        rectRunP.left = offset_x - RECTOFFSET;
        rectRunP.top = offset_y - RECTOFFSET;

        if (rectRunP.left < 0)  rectRunP.left = 0;
        if (rectRunP.top < 0)  rectRunP.top = 0;

        rectRunP.right = offset_x + width + RECTOFFSET;
        rectRunP.bottom = offset_y + height + RECTOFFSET;

        iana_setRunRunBallsizeMinMax(piana, camid, &rectRunP, xfact);

        crminratio = CRMINRATIO_;
		crmaxratio = IANA_BALLDETECT_GetMaxCircleRatio2(piana->camsensor_category, camid);	

        crmin = (piana->pic[camid]->icp.RunBallsizePMin / 2.0) * crminratio;
        crmax = (piana->pic[camid]->icp.RunBallsizePMax / 2.0) * crmaxratio;
#define MINCRMIN	5.0
        if (crmin < MINCRMIN) {
            crmin = MINCRMIN;
        }
    }
    cermax = pic->icp.CerBallshot;


    seqcount = 0;
    noballcount = 0;
    ts64dF = -1;
    rindexF = 0;

#define REFIMGCOUNT	1

#define READCOUNT_CANDIDATE2	16
#define BEFORE_CHECK	(-2)

    if (piana->processarea != IANA_AREA_PUTTER) {
        i0 = BEFORE_CHECK;
        i1 = (U32)(READCOUNT_CANDIDATE+s_preshot);
        i2 = (U32)(READCOUNT_CANDIDATE2+s_preshot);
    } else {
        i0 = BEFORE_CHECK;

#define PUTTERPROCESSINGCOUNT		INFOIMAGE_PUTTER_COUNT
        i1 = PUTTERPROCESSINGCOUNT;
        i2 = i1;
    }

#define TCAM_I_MULT	(0.6)
    if (piana->camsensor_category == CAMSENSOR_TCAM) {
        i0 = (I32)(i0 *TCAM_I_MULT);
        i1 = (I32)(i1 *TCAM_I_MULT);
        i2 = (I32)(i2 *TCAM_I_MULT);
    }

#define CONSOLE1_I_MULT_CENTER	0.4
#define CONSOLE1_I_MULT_SIDE	0.4
    if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
        double i_mult;

        if (camid == 0) {
            i_mult = CONSOLE1_I_MULT_CENTER;
        } else {
            i_mult = CONSOLE1_I_MULT_SIDE;
        }

        i0 = (I32)-6;

        i0 = (I32)s_i0;
        i1 = (I32)4;
        i2 = (I32)5;

        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-----> i0: %d, i1: %d, i2: %d\n", i0, i1, i2);

        s_preshot = 0;
    }


    for (i = i0; i < (I32)i2; i++) {
        U32 rindex;				// Current processing index;

        rindex = rindexshot + i - s_preshot;
        if (piana->opmode != IANA_OPMODE_FILE) {
            int j;
            U64 ts64;
            double ts64d;
            res = 0;
            pb = NULL;
            ts64 = 0;
            ts64d = 0;

#define IMAGE_GET_TRY	10
            for (j = 0; j < IMAGE_GET_TRY; j++) {
                if (camid < NUM_CAM) {
                    res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_NORMAL, rindex, &pb, &buf);
                } else {
                    res = 0;
                }
                if (res == 0 || pb == NULL || buf == NULL) {
                    cr_sleep(5);
                    continue;
                }
                ts64 = MAKEU64(pb->ts_h, pb->ts_l);
                ts64d = ts64 / TSSCALE_D;

                if (ts64dF < 0.0) {
                    ts64dF = ts64d;
                    rindexF = rindex;
                }

                if (res && (ts64d >= ts64dF)) {
                    break;
                } else {
                    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" SLEEP\n");
                    cr_sleep(5);
                    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" SLEEP done\n");
                    continue;
                }
            }
        }

        if (camid < NUM_CAM) {
            res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_NORMAL, rindex, &pb, &buf);
        } else {
            res = 0;
        }

        if (res == 0 || buf == NULL) {
            continue;
        }
        bufOrg = buf;		// backup original buf.

        if (piana->himgfunc) {
            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, pcii->width, pcii->height, 3, 1, 2, 3, piana->imguserparam);
        }

        CheckReadyBallPosition(pic, pcii, buf, multitude);

        if (res) {
            double cermax;

            //---
            if (bRefImgMade == 0) {
                cv::Mat cvImgSrc(ssize, CV_8UC1, buf), 
                    cvImg16(ssize, CV_16UC1, curbuf16),
                    cvImgDiv;

                if (sumbufcount < REFIMGCOUNT) {
                    cvImgSrc.convertTo(cvImg16, CV_16UC1);
                    memcpy(sumbuf16, cvImg16.data, sizeof(U16) * cvImg16.cols * cvImg16.rows);
                    sumbufcount++;
                    continue;
                }
                
                cvImg16 /= sumbufcount;
                cvImg16.convertTo(cvImgDiv, CV_8UC1);
                memcpy(pic->prefimg, cvImgDiv.data, sizeof(U08) * cvImgDiv.cols * cvImgDiv.rows);
                if (piana->himgfunc) {
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pic->prefimg, sumbufwidth, sumbufheight, 0, 1, 2, 3, piana->imguserparam);
                }
                free(curbuf16);
                free(sumbuf16);
                bRefImgMade = 1;
            }

#define BUFREF_SUB
            if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
                if (camid == 1) {
                    gamma_correction(buf, diffbuf, width, height);
                } else {
                    memcpy(diffbuf, buf, width*height);
                }
                if (piana->himgfunc) {
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)diffbuf, width, height, 0, 1, 2, 3, piana->imguserparam);
                }

                {
                    cv::Mat cvImgRef1(ssize, CV_8UC1, pic->prefimg), cvImgRef2(ssize, CV_8UC1, buf), cvImgSub;

                    cv::subtract(cvImgRef2, cvImgRef1, cvImgSub);
                    memcpy(diffbuf, cvImgSub.data, sizeof(U08) * cvImgSub.cols * cvImgSub.rows);
                }

                if (piana->shotarea != IANA_AREA_TEE) {
                    I32 sx, ex;
                    I32 ii;

                    sx = (I32)(picp->P.x - offset_x - crmax*5);
                    ex = (I32)(picp->P.x - offset_x + crmax*5);
                    for (ii = 0; ii < (I32)height; ii++) {
                        memcpy((diffbuf + sx + ii * width), (buf + sx + ii * width), (ex-sx));
                    }
                }

                if (piana->himgfunc) {
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)diffbuf, width, height, 1, 1, 2, 3, piana->imguserparam);
                }

            } else {
                cv::Mat cvImgRef1(ssize, CV_8UC1, pic->prefimg), cvImgRef2(ssize, CV_8UC1, buf), cvImgSub;

                cv::subtract(cvImgRef2, cvImgRef1, cvImgSub);
                memcpy(diffbuf, cvImgSub.data, sizeof(U08) * cvImgSub.cols * cvImgSub.rows);
            }

            buf = diffbuf;

            ts64 = MAKEU64(pb->ts_h, pb->ts_l);
            ts64d = ts64 / TSSCALE_D;
            cermax = piana->pic[camid]->icp.CerBallshot;

            for (m = 0; m < multitude; m++) {
                U32 bcount;

                cv::Mat cvImgBuf(ssize, CV_8UC1, buf), cvImgCrop, cvImgResize;

                cvImgCrop = cvImgBuf(cv::Rect(0, (m*pcii->height) / multitude, width, pcii->height / multitude));
                cv::resize(cvImgCrop, cvImgResize, droi.size(), xfact, yfact, cv::INTER_CUBIC);
                memcpy(procimg, cvImgResize.data, sizeof(U08) * cvImgResize.cols * cvImgResize.rows);

                if (piana->himgfunc) {
                    //((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf + pcii->width * ((m*pcii->height) / multitude), sroi.width, sroi.height, 0, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgCrop.data, sroi.width, sroi.height, 0, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)procimg, droi.width, droi.height, 1, 1, 2, 3, piana->imguserparam);

                }

                ts64m = ts64 - (periodm * (multitude - m -1));

#define RUNPROCESSINGAREA
//---------------------------------------------------------------
                {
                    I32	widthRun, heightRun;

                    I32 width, height;
                    I32 startx, starty;
                    U32 bcountperseq;

                    double cutmean_shot;


                    //--
                    bcountperseq = 0;

                    width 	= (int)(sroi.width * xfact + 0.1);
                    height	= (int)(sroi.height * yfact + 0.1);

#define TRY_CATEGORY7
                    if (piana->processarea != IANA_AREA_PUTTER) {
                        iana_cam_setup_t *picsetup;
                        iana_setup_t *pisetup;

                        UNUSED(offset_x);

                        widthRun  = (int)(piana->pic[camid]->runProcessingWidth * xfact + 0.1);
                        heightRun = (int)(piana->pic[camid]->runProcessingHeight * (yfact / multitude) + 0.1);

                        if (heightRun > height) {
                            heightRun = height;
                        }
                        if (widthRun > width) {
                            widthRun = width;
                        }
                        startx = 0;

                        pisetup = &piana->isetup;
                        picsetup = &pisetup->icsetup[camid];

                        starty = (height - heightRun)/2;
                        if (starty < 0) {
                            starty = 0;
                        }

                    } else {
                        widthRun = width;
                        heightRun = height;
                        startx = 0;
                        starty = 0;
                    }

                    bcount = 0;

                    if (bcount == 0) {			// Canny .. fail. use adapt.
#define CANDADPT_RETRY_EYEXO	2
#define CANDADPT_RETRY			1
#define CANDADPT_RETRY_A		10
                        U32 iter;
                        U32 candadpt_retry;
                        double thstdmult[CANDADPT_RETRY_A] ={
                            1.5 ,3.0, 5.0
                        };

                        if (piana->camsensor_category == CAMSENSOR_EYEXO) {
                            if (camid == 0) {
                                thstdmult[0] = 5.0;
                                thstdmult[1] = 1.0;
                                thstdmult[2] = 10.0;
                            } else {
                                thstdmult[0] = 1.0;
                                thstdmult[1] = 5.0;
                                thstdmult[2] = 10.0;
                            }
                            candadpt_retry = CANDADPT_RETRY_EYEXO;
                        } else {
                            candadpt_retry = CANDADPT_RETRY;
                        }
                        for (iter = 0; iter < candadpt_retry; iter++) {
                            int rmt;
                            int expn;

                            if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
                                if (camid == 0) {
                                    rmt = 1;
                                } else {
                                    rmt = 1;
                                }
                                expn = 0;
                            } else if (piana->camsensor_category == CAMSENSOR_P3V2) {
                                if (camid == 0) {
                                    rmt = 3;
                                    expn = 1;
                                } else {
                                    rmt = 1;
                                    expn = 1;
                                }
                            } else {
                                rmt = 3;
                                expn = 1;
                            }
#define CUTMEAN_SHOT_EYEXO	0.0005
#define CUTMEAN_SHOT	0.5
                            if (piana->camsensor_category == CAMSENSOR_EYEXO) {
                                if (camid == 0) {
                                    rmt = 1;
                                    expn = 0;
                                } else {
                                    if (s_rmt_expn_1) {
                                        rmt = 1;
                                        expn = 0;
                                    } else {
                                        rmt = 1;			// 20200904.. 
                                        expn = 0;
                                    }

                                }
                                cutmean_shot = CUTMEAN_SHOT_EYEXO;
                            } else {
                                cutmean_shot = CUTMEAN_SHOT;
                            }

                            res = iana_getballcandidatorAdapt (
                                piana,
                                camid,
                                (U08 *)procimg + startx + starty * width,
                                widthRun, heightRun, width,
                                &bc[0],
                                &bcount,
                                shot_halfwinsize, 		// SHOT_HALFWINSIZE, 
                                rmt, expn,// 3, 2, // 1, 0, // 4, 0,  // 1, 0,				// READY_MULTV, READY_EXPN,
                                thstdmult[iter],
                                cermax, 
                                cutmean_shot					// 20190420, yhsuk
                            );
                            UNUSED(thstdmult);
                            if (bcount > 0) {
                                break;
                            }
                        }
                    }


                    if (bcount > 0) {
                        if (s_refine_with_CHT) {	// Refine with CHT algorithm.
                            iana_ballcandidate_t bc_cht[MAXCANDIDATE];
                            iana_ballcandidate_t bc_refined[MAXCANDIDATE];
                            U32 bcount_cht;
                            U32 bcount_refine;

                            I32 sx, sy, ex, ey;
                            I32 widthR, heightR;

                            I32 mincrindex;
                            double mincr;

                            //--
                            memset(&bc_refined[0], 0, sizeof(iana_ballcandidate_t) *MAXCANDIDATE);
                            memset(&bc_cht[0], 0, sizeof(iana_ballcandidate_t) *MAXCANDIDATE);
                            for (j = 0; j < (I32)MAXCANDIDATE; j++) {
                                bc_cht[j].cexist 		= IANA_BALL_EMPTY;
                                bc_refined[j].cexist 	= IANA_BALL_EMPTY;
                            }

                            bcount_refine = 0;
                            mincrindex = 0;
                            mincr = 999;
                            for (j = 0; j < (I32)MAXCANDIDATE; j++) {
                                if (bc[j].cexist == IANA_BALL_EXIST) {
                                    sx = (I32)(bc[j].P.x - bc[j].cr - shot_halfwinsize * 2);
                                    ex = (I32)(bc[j].P.x + bc[j].cr + shot_halfwinsize * 2);

                                    if (sx < 0) sx = 0;

                                    sy = (I32)(bc[j].P.y - bc[j].cr - shot_halfwinsize * 2);
                                    ey = (I32)(bc[j].P.y + bc[j].cr + shot_halfwinsize * 2);

                                    if (sy < 0) sy = 0;

                                    widthR = ex - sx + 1;
                                    widthR= ((widthR + 7) / 8) * 8;

                                    heightR = ey - sy + 1;

                                    res = iana_getballcandidator_cht(
                                        piana,
                                        camid,
                                        (U08 *)procimg + sx + sy * width,
                                        widthR, heightR, width,
                                        &bc_cht[0],
                                        &bcount_cht,
                                        shot_halfwinsize, 		// FAKE..
                                        1, 0,					// FAKE..
                                        10,					// FAKE..
                                        0.1					// FAKE
                                        , 10					// FAKE
                                    );

                                    if (bc_cht[0].cexist == IANA_BALL_EXIST) {
                                        memcpy(&bc_refined[j], &bc_cht[0], sizeof(iana_ballcandidate_t));
                                        bc_refined[j].P.x = (bc_refined[j].P.x + sx - startx);
                                        bc_refined[j].P.y = (bc_refined[j].P.y + sy - starty);

                                        if (mincr > bc_refined[j].cr) {
                                            mincrindex = j;
                                            mincr = bc_refined[j].cr;
                                        }
                                        bcount_refine++;
                                    }
                                }
                            }
                            if (bcount_refine > 0) {			// refine success!!
                                bcount = bcount_refine;
                                memcpy(&bc[0], &bc_refined[0], sizeof(iana_ballcandidate_t) *MAXCANDIDATE);
                            }
                        }

                        for (j = 0; j < (I32)MAXCANDIDATE; j++) {
                            if (bc[j].cexist == IANA_BALL_EXIST) {
                                if (bc[j].cer > cermax || bc[j].cr < crmin || bc[j].cr > crmax) {	// Filtering..
                                    bc[j].cexist = IANA_BALL_EMPTY;
                                    continue;
                                } else {
                                    point_t Pp, Lp, Lp2;

#define MULTITUDE_FRACTION
                                    //-- Converse to Pixel point.
                                    bc[j].P.x = ((bc[j].P.x + startx) / xfact) + offset_x;
                                    bc[j].P.y = ((bc[j].P.y + starty + 0.5) * multitude) / yfact + offset_y;
                                    bc[j].P.z = 0.0;
                                    bc[j].cr /= xfact;

                                    //-- Converse to Local point.
                                    Pp.x = bc[j].P.x;
                                    Pp.y = bc[j].P.y;
                                    iana_P2L(piana, camid, &Pp, &Lp, 0);
                                    bc[j].L.x = Lp.x;
                                    bc[j].L.y = Lp.y;
                                    bc[j].L.z = 0.0;

#define SHOTCHECK_YMARGIN	0.015
                                    if (piana->camsensor_category == CAMSENSOR_P3V2) {
                                        double dx, dy;
                                        dx = bc[j].L.x - pic->icp.L.x;
                                        dy = bc[j].L.y - pic->icp.L.y;
                                        if (dy < SHOTCHECK_YMARGIN) {
                                            if (dy < -SHOTCHECK_YMARGIN) {
                                                bc[j].cexist = IANA_BALL_EMPTY;
                                                continue;
                                            }
                                        }
                                    } else if (piana->camsensor_category == CAMSENSOR_EYEXO) {
                                        double dx, dy;
                                        double dlen;
                                        dx = bc[j].L.x - pic->icp.L.x;
                                        dy = bc[j].L.y - pic->icp.L.y;
                                        dlen = sqrt(dx*dx + dy*dy);

#define SHOTCHECK_YMARGIN_EYEXO_CAM0		0.015
#define SHOTCHECK_YMARGIN_EYEXO_CAM1		0.005
#define SHOTCHECK_YMARGIN_EYEXO_CAM1_1		0.001
#define SHOTCHECK_DLENMARGIN_EYEXO_CAM1_1	0.005
                                        if (camid == 0) {
                                            if (dy < SHOTCHECK_YMARGIN_EYEXO_CAM0) {
                                                bc[j].cexist = IANA_BALL_EMPTY;
                                                continue;
                                            }
                                        } else {		// camid == 1
                                            if (dy < SHOTCHECK_YMARGIN_EYEXO_CAM1) {
                                                if (!(
                                                    dy > SHOTCHECK_YMARGIN_EYEXO_CAM1_1
                                                    && dlen >SHOTCHECK_DLENMARGIN_EYEXO_CAM1_1
                                                    )
                                                    ) {
                                                    bc[j].cexist = IANA_BALL_EMPTY;
                                                    continue;
                                                }
                                            }
                                        }
                                    } else {
                                        if (bc[j].L.y < pic->icp.L.y + SHOTCHECK_YMARGIN) {
                                            bc[j].cexist = IANA_BALL_EMPTY;
                                            continue;
                                        }
                                    }

                                    //-- get radius
                                    Pp.x = bc[j].P.x + bc[j].cr;
                                    Pp.y = bc[j].P.y;
                                    iana_P2L(piana, camid, &Pp, &Lp2, 0);
                                    bc[j].lr = DDIST(Lp.x, Lp.y, Lp2.x, Lp2.y);

                                    //-- Time index
                                    bc[j].tsindex = rindex;
                                    bc[j].ts64 = ts64m;
                                    bc[j].m = m;
                                    memcpy(&pic->bc[seqcount][bcountperseq], &bc[j], sizeof(iana_ballcandidate_t));
                                 
                                    bcountperseq++;
                                    if (bcountperseq >= BALLCCOUNT) {
                                        break;
                                    }
                                }
                            } 		// if (bc[i].cexist) 
                        } 			// for (i = 0; i < bcount; i++) 
                        if (bcountperseq) {
                            seqcount++;
                            if (seqcount == 1) {
                                if (pic->icp.cr < 1) {
                                    pic->icp.cr = pic->bc[0][0].cr;
                                }
                            }
#define MAXSEQCOUNT	16
#define CHECKSEQCOUNT		5
#define CHECKNOBALLCOUNT	3
#define CHECKSEQCOUNT2		8
#define CHECKNOBALLCOUNT2	3
                        }
                        noballcount = 0;
                    } else { 				// if (bcount > 0) 
                        noballcount++;

                    }

#define TOOL_IMG_2
#if defined(TOOL_IMG_2)
                    if (piana->opmode == IANA_OPMODE_FILE) {
                        I32 cxx, cyy, crr;
                        if (bcount > 0) {
                            for (j = 0; j < (I32)MAXCANDIDATE; j++) {
                                if (bc[j].cexist == IANA_BALL_EXIST) {
                                    cxx = (I32)((bc[j].P.x - offset_x) * xfact - startx);
                                    cyy = (I32)((bc[j].P.y - offset_y) * (yfact/multitude) - starty);
                                    crr = (I32)(bc[j].cr * xfact);
                    
                                    mycircleGray(cxx, cyy, crr, 0xFF, (U08 *)procimg, width);
                                    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "c[%d:%2d:%d] rindex: %3d, tmd: %10.6lf (%10.6lf) bc[%d], P<%10lf %10lf> L<%10lf %10lf> cer: %lf, cr: %lf\n",
                                        camid, i, m, rindex,
                                        (ts64m / TSSCALE_D),
                                        (ts64m / TSSCALE_D - ts64dshot),
                                        j,
                                        bc[j].P.x, bc[j].P.y,
                                        bc[j].L.x, bc[j].L.y,
                                        bc[j].cer,
                                        bc[j].cr);
                                }
                            }
                        }

                        if (piana->himgfunc) {
                            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)procimg, width, height, 3 /*4*/, 1, 2, 3, piana->imguserparam);
                        }
                    }
#endif
                    // IMGCALLBACK for shot candidator.. with mult-data.
                    if (seqcount >= MAXSEQCOUNT) {
                        break;
                    }
                }

                // DISPLAY_DISPLAY
                if (seqcount >= CHECKSEQCOUNT) {
                    if (noballcount > CHECKNOBALLCOUNT) {
                        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "SEQ limit #0  seqcount: %d, noballcount: %d\n", seqcount, noballcount);
                        break;
                    }
                }

            } 					// for (m = 0; m < multitude; m++) 
        }

        if (seqcount >= CHECKSEQCOUNT) {
            if (noballcount > CHECKNOBALLCOUNT) {
                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "SEQ limit #0  seqcount: %d, noballcount: %d\n", seqcount, noballcount);
                break;
            }
        }


        if (seqcount >= CHECKSEQCOUNT2) {
            if (noballcount > CHECKNOBALLCOUNT2) {
                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "SEQ limit #0-1  seqcount: %d, noballcount: %d\n", seqcount, noballcount);
                break;
            }
        }

        //if (seqcount >= BALLSEQUENCELEN) 
        if (seqcount >= MAXSEQCOUNT) {
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "SEQ limit #1  %d\n", seqcount);
            break;
        }
        //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------\nCAM[%d] %d seqcount: %d\n", camid, i, seqcount);

        if (i >= i1-1) {
            //#define MINSEQCOUNT	4
#define MINSEQCOUNT	5
            if (seqcount > MINSEQCOUNT) {
                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "SEQ limit #2  %d\n", seqcount);
                break;
            }
        }
    } 							// for (i = 0; i < READCOUNT; i++) 
    pic->bcseqcount = seqcount;

    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "------\nCAM[%d] seqcount: %d\n", camid, seqcount);
    if (diffbuf != NULL) {
        free(diffbuf);
    }
    res = 1;
func_exit:
    return res;
#endif
}



/*!
 ********************************************************************************
 *	@brief      CAM shot regression
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		ball count used in regression
 *
 *	@author	    yhsuk
 *  @date       2016/02/25
 *******************************************************************************/
static I32 RegressShotData(iana_t *piana, U32 camid) 
{
	I32 res;
	iana_cam_t	*pic;
	iana_cam_param_t *picp;
	U32 count;
	U64 ts64;
//	U64 ts64delta;
	I64 ts64delta;
	U32 bcseqcount;
	double Lxx[BALLSEQUENCELEN + 1], Lyy[BALLSEQUENCELEN + 1], tt[BALLSEQUENCELEN + 1+128], Lrr[BALLSEQUENCELEN + 1+128];
	double Pxx[BALLSEQUENCELEN + 1], Pyy[BALLSEQUENCELEN + 1], Prr[BALLSEQUENCELEN + 1+128];
	iana_ballcandidate_t *pbc;
	iana_ballcandidate_t *pbs;
	U32 i, j;
	point_t firstPL;
	double dlen;
	U32 isFirst;

	U32	camidReady;
	U32	camidOther;
	U32 ballcount_min;
	

	//--
	camidReady = piana->camidCheckReady;
	camidOther = piana->camidOther;

	pic = piana->pic[camid];
	picp = &pic->icp;

	bcseqcount = pic->bcseqcount;


	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "bcseqcount: %d\n", bcseqcount);
	memset(&pic->bs[0], 0, sizeof(iana_ballcandidate_t) * BALLSEQUENCELEN);
	if (camid == camidReady) 
	//--------- SHOT time re-estimation.
	{
		U64 ts64s[BALLSEQUENCELEN+1];
		U32 count;
		count = 0;
		pbc = NULL;

		for (i = 0; i < bcseqcount; i++) {
			pbs = &pic->bs[i];
			ts64 = 0;
			for (j = 0; j < BALLCCOUNT; j++) {
				pbc = &pic->bc[i][j];
				if (pbc->cexist == IANA_BALL_EXIST) {
					ts64 = pbc->ts64;
					break;
				}
			}


//#define REESTIMATION_YMARGIN	0.005
//#define REESTIMATION_YMARGIN	0.05
//#define REESTIMATION_YMARGIN	0.03
#define REESTIMATION_YMARGIN	0.01
#define REESTIMATION_TMARGIN	((U64) (TSSCALE_D * 0.0005))
			if (ts64 != 0) {
				if (pbc->L.y > pic->icp.L.y + REESTIMATION_YMARGIN 
					//&& ts64 > pic->ts64shot + REESTIMATION_TMARGIN
					) 
				{
					ts64delta = ts64 - pic->ts64shot;
					ts64s[count] = ts64;

					Lyy[count] = pbc->L.y;
					tt[count] = ts64delta / TSSCALE_D;

					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<C%d i:%2d CT:%d> ts64: %016llx (%10lf), yy: %lf\n", camid, i, count, 
					//		ts64, ts64/TSSCALE_D, yy[count]);
					count++;
				} else {
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<C%d i:%2d CT:%d> ts64: %016llx (%10lf), yy: %lf vs (%lf  %lf)\n", camid, i, count, 
					//		ts64, ts64/TSSCALE_D, pbc->L.y,pic->icp.L.y, pic->icp.L.y + REESTIMATION_YMARGIN
					//		);

				}
			}




			if (count >= BALLSEQUENCELEN) {
				break;
			}
		}




#define BALLCCOUNT_MIN1		3
		if (count >= BALLCCOUNT_MIN1) {
			double my_, by_, r2;
			//double bally_;
			U64 ts64shotnew;
			double ts64deltadnew;

			//--
			cr_regression2(tt, Lyy, count, &my_,  &by_, &r2);
			//cr_regression2(tt+2, yy+2, count-2, &my_,  &by_, &r2);
#define BADR2	0.6
			if (r2 < BADR2) {
				res = -1;
				goto func_exit;
			}
			ts64shotnew = 0;
			ts64deltadnew = 0;
#define MINVALUE_MY		(1e-10)
			if (my_ < -MINVALUE_MY || my_ > MINVALUE_MY) {
#define TSDELTA_MIN		(0.001)
				ts64deltadnew = (pic->icp.L.y - by_ ) / my_;
				ts64shotnew = (U64)((I64) (ts64deltadnew * TSSCALE_D) + pic->ts64shot);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%d> ts64deltadnew: %lf, ts64shotnew: %016llx (%lf)\n",
						camid,
						ts64deltadnew, ts64shotnew, ts64shotnew/TSSCALE_D);

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%d> NEW ts64shot !!!!!  %lld(%016llx %lf) -> %lld(%016llx %lf)   L.y: %10lf => %10lf\n", 
						camid,
						pic->ts64shot,pic->ts64shot, pic->ts64shot/TSSCALE_D,
						ts64shotnew,ts64shotnew, ts64shotnew/TSSCALE_D
						,
				pic->icp.L.y, by_
						);
				pic->ts64shot = ts64shotnew;
			}


			{
				FILE *fp;
				int mode;
				//mode = 0;
				mode = 1;
				fp = cr_fopen("wcwc.txt", "r");
				if (fp) {
					fscanf(fp, "%d", &mode);
					cr_fclose(fp);
				}

#define CHECKDELTA_MAX	(0.001)
				if ( mode == 1 
						&& (ts64deltadnew > CHECKDELTA_MAX ||  ts64deltadnew < -CHECKDELTA_MAX)
				   ){
					I32  iii;
					I32  rindexshot0;
					U32  rindex;
					U64 ts64_;
					camif_buffer_info_t *pb;

					iana_cam_t	*picOther;
					//--	For camidReady.
#define INDEXSEARCH	10
#define CAMTSMARGIN   ((U64) ((0.0002 * TSSCALE_D)))
					rindexshot0 = pic->rindexshot;
					for (iii = -INDEXSEARCH; iii < INDEXSEARCH; iii++) {
						res = scamif_imagebuf_trimindex2(piana->hscamif, camidReady, 1, (I32)(rindexshot0+iii), &rindex);
						res = scamif_imagebuf_randomaccess(piana->hscamif, camidReady, NORMALBULK_NORMAL, rindex, &pb, NULL);

						if (res == 0) {
							continue;
						}
						ts64_ = MAKEU64(pb->ts_h, pb->ts_l);
						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%d:%d> ri+ii: %d, rindex: %d    ts64: %lld, ts64shot: %lld \n",
						//		camidReady, iii, rindexshot0+iii, rindex, ts64_, pic->ts64shot);

						if (ts64_ + CAMTSMARGIN > pic->ts64shot) {
							rindexshot0 = rindex;
							break;
						}
					}
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "camidReady<%d> rindexshot: %d -> %d\n", camidReady, pic->rindexshot,  rindexshot0);
					pic->rindexshot = rindexshot0;

					//----
					//
					picOther = piana->pic[camidOther];

					rindexshot0 = picOther->rindexshot;
					for (iii = -INDEXSEARCH; iii < INDEXSEARCH; iii++) {
						res = scamif_imagebuf_trimindex2(piana->hscamif, camidOther, 1, (I32)(rindexshot0+iii), &rindex);
						res = scamif_imagebuf_randomaccess(piana->hscamif, camidOther, NORMALBULK_NORMAL, rindex, &pb, NULL);

						if (res == 0) {
							continue;
						}
						ts64_ = MAKEU64(pb->ts_h, pb->ts_l);
						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%d:%d> ri+ii: %d, rindex: %d    ts64: %lld, ts64shot: %lld \n",
						//		camidOther, iii, rindexshot0+iii, rindex, ts64_, picOther->ts64shot);

						if (ts64_ + CAMTSMARGIN > picOther->ts64shot) {
							rindexshot0 = rindex;
							break;
						}
					}

					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "camidOther<%d> rindexshot: %d -> %d\n", camidOther, picOther->rindexshot,  rindexshot0);
					picOther->rindexshot = rindexshot0;

					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "DO IT again..\n");
					res = -1;
					goto func_exit;
				}
			}
		}
	} else {
		U64 ts64shot;
		double ts64shotd;
		ts64shot = piana->pic[camidReady]->ts64shot;
		ts64shotd = ts64shot/TSSCALE_D;

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "camidOther, <%d> NEW ts64shot !!!!!  %lld (%lf) -> %lld (%lf)\n",
				camid,
				pic->ts64shot, pic->ts64shot/TSSCALE_D,
				ts64shot, ts64shotd
				);
		pic->ts64shot = ts64shot;
		pic->ts64shot0 = piana->pic[camidReady]->ts64shot0;
	}
	//--------------------------------



	isFirst = 1;
	firstPL.x = -999;
	firstPL.y = -999;

	count = 0;
	for (i = 0; i < bcseqcount; i++) {
		pbs = &pic->bs[i];
#if defined (USE_IANA_BALL)
		pbs->cexist = IANA_BALL_EMPTY;
#else
		pbs->cexist = 0;
#endif
		for (j = 0; j < BALLCCOUNT; j++) {
			pbc = &pic->bc[i][j];
#if defined (USE_IANA_BALL)
			if (pbc->cexist == IANA_BALL_EXIST) 
#else
			if (pbc->cexist)
#endif
			{
				double reestimation_ymargin;
				if (isFirst) {
					double dx, dy;

					isFirst = 0;
					dx = firstPL.x - pbc->L.x;
					dy = firstPL.y - pbc->L.y;

					dlen = sqrt(dx*dx +dy*dy);
#define MINDEN_FIRST	0.005
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "dlen: %lf\n", dlen);
					if (dlen > MINDEN_FIRST) {
						firstPL.x = pbc->L.x;
						firstPL.y = pbc->L.y;
						//break;
					}
				}
				ts64 = pbc->ts64;

#define REESTIMATION_YMARGIN2				0.01
#define REESTIMATION_YMARGIN2_EYEXO_CAM0	0.01
//#define REESTIMATION_YMARGIN2_EYEXO_CAM1	0.005
#define REESTIMATION_YMARGIN2_EYEXO_CAM1	0.001
#define REESTIMATION_YMARGIN2_P3V2_CAM0		0.01
#define REESTIMATION_YMARGIN2_P3V2_CAM1		(-0.01)




				if (piana->camsensor_category == CAMSENSOR_EYEXO) {
					if (camid == 0) {
						reestimation_ymargin = REESTIMATION_YMARGIN2_EYEXO_CAM0;
					} else {		// camid == 1
						reestimation_ymargin = REESTIMATION_YMARGIN2_EYEXO_CAM1;
					}
				} else if (piana->camsensor_category == CAMSENSOR_P3V2) {
					if (camid == 0) {
						reestimation_ymargin = REESTIMATION_YMARGIN2_P3V2_CAM0;
					} else {		// camid == 1
						reestimation_ymargin = REESTIMATION_YMARGIN2_P3V2_CAM1;
					}
				} else {
					reestimation_ymargin = REESTIMATION_YMARGIN2;
				}
				if (pbc->L.y < pic->icp.L.y + reestimation_ymargin) {
					break;
				}
#define MINMERCYCOUNT	4
				if (pic->bcseqcount > MINMERCYCOUNT) {
					if (count == 0) {
						double mindlen;
	//#define MINDLEN	(0.001)
	//#define MINDLEN	(0.005)
	//#define MINDLEN	(0.050)
	//#define MINDLEN	(0.030)
//	#define MINDLEN	(0.010)
//#define MINDLEN	(0.005)
#define MINDLEN	(0.003)
#define MINDLEN_EYEXO	(0.001)

						if (piana->camsensor_category == CAMSENSOR_EYEXO) {
							mindlen = MINDLEN_EYEXO;
						}
						else {
							mindlen = MINDLEN;
						}
						dlen = sqrt((firstPL.x - pbc->L.x) * (firstPL.x - pbc->L.x) + (firstPL.y - pbc->L.y) * (firstPL.y - pbc->L.y));
						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]dlen: %lf\n", i, dlen);
						if (dlen < mindlen) {
							break;
						}
					}
				}

				memcpy(pbs, pbc, sizeof(iana_ballcandidate_t));		// backup selected ball candidate 
				ts64delta = pbc->ts64 - pic->ts64shot;

				Lxx[count] = pbc->L.x;
				Pxx[count] = pbc->P.x;
				Lyy[count] = pbc->L.y;
				Pyy[count] = pbc->P.y;
				tt[count] = ts64delta / TSSCALE_D;
				Lrr[count] = pbc->lr;

				Prr[count] = pbc->cr;

				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "xxyyrr[%d:%d]   %10lf  %10lf  %10lf  %10lf\n",
				//		camid, count+1, tt[count], xx[count], yy[count], rr[count]);

				count++;
				break;
			} 
#if defined (USE_IANA_BALL)
			else if (pbc->cexist == IANA_BALL_EXILE) 
			{
				firstPL.x = pbc->L.x;
				firstPL.y = pbc->L.y;
			}
#endif
		}
		if (count >= BALLSEQUENCELEN) {
			break;
		}
	}

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "count: %d\n", count);
	picp->ballcount = count;

//#define BALLCCOUNT_MIN	3
#define BALLCCOUNT_MIN_CAMIDREADY	3
#define BALLCCOUNT_MIN_CAMIDOTHER	2

	if (camid == camidReady) {
		ballcount_min = BALLCCOUNT_MIN_CAMIDREADY;
	} else {
		ballcount_min = BALLCCOUNT_MIN_CAMIDOTHER;
	}
	
	
	if (count >= ballcount_min) {
		double maPx[3];
		double maPy[3];
		double maPr[3];

		double maLx[3];
		double maLy[3];
		double maLr[3];

//		double mx_, bx_;
//		double my_, by_;
//		double mr_, br_;
		double mxy_, bxy_;
//		double vy;
		double r2;
//		double mPx_, bPx_;
//		double mPy_, bPy_;
//		double mPr_, bPr_;

		double mPxx_, bPxx_;
		double mPyy_, bPyy_;

		U32 countR;

		U32 countr_max;
		U32 ballcount_min_1;


		//---
//#define COUNTR_MAX	5
#define COUNTR_MAX	10
#define BALLCCOUNT_MIN_1	5

#define COUNTR_MAX_EYEXO		16
#define BALLCCOUNT_MIN_1_EYEXO	16				// fake.. :P

		if (piana->camsensor_category == CAMSENSOR_EYEXO) {
			countr_max = COUNTR_MAX_EYEXO;
			ballcount_min_1 = BALLCCOUNT_MIN_1_EYEXO;
		} else {
			countr_max = COUNTR_MAX;
			ballcount_min_1 = BALLCCOUNT_MIN_1;
		}


		countR = count;
		if (countR > countr_max) {
			countR = countr_max;
		}
		///if (countR >= BALLCCOUNT_MIN_1) 
		if (countR >= ballcount_min_1)
		{
			caw_quadraticregression(tt, Lxx, count, &maLx[2], &maLx[1], &maLx[0], &r2);
			caw_quadraticregression(tt, Lyy, count, &maLy[2], &maLy[1], &maLy[0], &r2);
			caw_quadraticregression(tt, Lrr, count, &maLr[2], &maLr[1], &maLr[0], &r2);

			caw_quadraticregression(tt, Pxx, count, &maPx[2], &maPx[1], &maPx[0], &r2);
			caw_quadraticregression(tt, Pyy, count, &maPy[2], &maPy[1], &maPy[0], &r2);
			caw_quadraticregression(tt, Prr, count, &maPr[2], &maPr[1], &maPr[0], &r2);
		} else {
			maLx[2] = 0;
			maLy[2] = 0;
			maLr[2] = 0;

			maPx[2] = 0;
			maPy[2] = 0;
			maPr[2] = 0;

			cr_regression2(tt, Lxx, count, &maLx[1], &maLx[0], &r2);
			cr_regression2(tt, Lyy, count, &maLy[1], &maLy[0], &r2);
			cr_regression2(tt, Lrr, count, &maLr[1], &maLr[0], &r2);

			cr_regression2(tt, Pxx, count, &maPx[1], &maPx[0], &r2);
			cr_regression2(tt, Pyy, count, &maPy[1], &maPy[0], &r2);
			cr_regression2(tt, Prr, count, &maPr[1], &maPr[0], &r2);
		}


		cr_regression2(tt, Pxx, count, &mPxx_,  &bPxx_, &r2);
		cr_regression2(tt, Pyy, count, &mPyy_,  &bPyy_, &r2);

		cr_regression2(Lyy, Lxx, count, &mxy_, &bxy_, &r2);


		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---------------------cam: %d\n", camid);
		for (i = 0; i < count; i++) {
			double lxx, lyy, lrr;
			lxx = tt[i]*tt[i] * maLx[2] + tt[i] * maLx[1] + maLx[0];
			lyy = tt[i]*tt[i] * maLy[2] + tt[i] * maLy[1] + maLy[0];
			lrr = tt[i]*tt[i] * maLr[2] + tt[i] * maLr[1] + maLr[0];
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "(%2d) %10.6lf %10.6lf %10.6lf   %10.6lf %10.6lf   %10.6lf %10.6lf\n", 
					i,
					tt[i], 
					Lxx[i], lxx,
					Lyy[i], lyy,
					Lrr[i], lrr);
		}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---------------------%d\n");



#if 0
		cr_regression2(tt, Lxx, count, &mx_,  &bx_, &r2);
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mx0_: %10lf, bx0_: %10lf,  r2: %10lf\n",  mx_, bx_, r2);
		cr_regression2(tt, Lyy, count, &my_,  &by_, &r2);
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mx0_: %10lf, bx0_: %10lf,  r2: %10lf\n",  mx_, bx_, r2);

		cr_regression2(tt, Pxx, count, &mPx_,  &bPx_, &r2);
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mx0_: %10lf, bx0_: %10lf,  r2: %10lf\n",  mx_, bx_, r2);
		cr_regression2(tt, Pyy, count, &mPy_,  &bPy_, &r2);
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mx0_: %10lf, bx0_: %10lf,  r2: %10lf\n",  mx_, bx_, r2);


		cr_regression2(tt, Lrr, countR /*count*/, &mr_,  &br_, &r2);
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mx0_: %10lf, bx0_: %10lf,  r2: %10lf\n",  mx_, bx_, r2);

		cr_regression2(tt, Rrr, countR /* count*/, &mPr_,  &bPr_, &r2);

		cr_regression2(Lyy, Lxx, count, &mxy_, &bxy_, &r2);
#endif
		
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mx0_: %10lf, bx0_: %10lf,  r2: %10lf\n",  mx_, bx_, r2);

		memcpy(&picp->maPx[0], &maPx[0], sizeof(double)*3);
		memcpy(&picp->maPy[0], &maPy[0], sizeof(double)*3);
		memcpy(&picp->maPr[0], &maPr[0], sizeof(double)*3);

		memcpy(&picp->maLx[0], &maLx[0], sizeof(double)*3);
		memcpy(&picp->maLy[0], &maLy[0], sizeof(double)*3);
		memcpy(&picp->maLr[0], &maLr[0], sizeof(double)*3);

		picp->mPxx_ = mPxx_;
		picp->bPxx_ = bPxx_;
		picp->mPyy_ = mPyy_;
		picp->bPyy_ = bPyy_;

		picp->mxy_= mxy_;
		picp->bxy_= bxy_;

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mx0_: %lf, bx0_: %lf\n",  mx_, bx_);
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "my0_: %lf, by0_: %lf\n",  my_, by_);
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mr0_: %lf, br0_: %lf\n",  mr_, br_);
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mxy0_: %lf, bxy0_: %lf\n",mxy_,bxy_);

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "vy: %lf\n", vy);
		//if (vy < 2.0) {
		//	for (i = 0; i < count; i++) {
		//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%d:%3d> t%10lf x%10lf y%10lf r%10lf\n", camid, i,
		//				xx[i], yy[i], tt[i], rr[i]);
		//	}
		//}
		res = count;
	} else {
		res = 0;
	}

func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      CAM shot plane
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/02/26
 *******************************************************************************/
static U32 s_shot_plane_mode = 3; // 0, 1, 2
I32 iana_shot_plane(iana_t *piana, U32 camid)
{
	I32 res;
	if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
		if (s_shot_plane_mode == 3) {
			res = GetShotPlane3(piana, camid);
		} else {
			res = 0;
		}
	} else {
		res = GetShotPlane0(piana, camid);
	}

#ifdef DEBUG_SHOT
	{
		cr_point_t ppp;
		iana_ballcandidate_t *pbs;
		iana_cam_t	*pic;
		iana_cam_param_t *picp;
		cr_point_t *pCamPosL;
		cr_plane_t *pShotPlane;		// Shot Plane
		double value;
		U32 i;

		pic 	= piana->pic[camid];
		picp 	= &pic->icp;
		pCamPosL= &picp->CamPosL;
		pShotPlane = &picp->ShotPlane;

		value =  cr_check_P_on_Plane(pCamPosL, pShotPlane);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:CAMPOS] %10lf %10lf %10lf: %10lf\n", 
				camid, pCamPosL->x, pCamPosL->y, pCamPosL->z);
		for (i = 0; i < pic->bcseqcount; i++) {
			//---
			pbs = &pic->bs[i];
			
            if (pbs->cexist == IANA_BALL_EXIST) {
				double value;
				ppp.x = pbs->L.x;
				ppp.y = pbs->L.y;
				ppp.z = 0;

				value =  cr_check_P_on_Plane(&ppp, pShotPlane);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%d] cr_check_P_on_Plane()  %10lf %10lf : %10lf\n", 
						camid, i,
						ppp.x, ppp.y, value);
			}
		}
	}
#endif

	return res;
}

/*!
 ********************************************************************************
 *	@brief      get the best shot plane passing all ball positions and camera position. local line and shot plane are updated(calculated) in here
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @param[in]	camid
 *              camera index - 0:master/center, 1:slave/side
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/02/24
 *******************************************************************************/
static I32 GetShotPlane0(iana_t *piana, U32 camid)	
{
	I32 res;

	U32 i;
	U32 foundit;
	iana_cam_t	*pic;
	iana_cam_param_t *picp;

	cr_vector_t m;
	cr_point_t p0;			// Point at L.y = 0.0

	cr_point_t *pCamPosL;

	cr_vector_t v0;			// 
	cr_vector_t n;

	cr_line_t *pLocLine;
	cr_plane_t *pShotPlane;		// Shot Plane
	
	//---
	pic 	= piana->pic[camid];
	picp 	= &pic->icp;
	pCamPosL= &picp->CamPosL;
	pShotPlane = &picp->ShotPlane;

	//--- Get Local Line 
	m.v[0] = picp->mxy_;
	m.v[1] = 1.0;
	m.v[2] = 0.0;

	foundit = 0;
	i = 0;

#if DEBUG_SHOT
	for (i = 0; i < pic->bcseqcount; i++) {
		iana_ballcandidate_t *pbs;
		pbs = &pic->bs[i];
		if (pbs->cexist == IANA_BALL_EXIST) {
			p0.x = pbs->L.x;
			p0.y = pbs->L.y;
			p0.z = 0.0;
			foundit = 1;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "foundit!  %lf %lf\n", p0.x, p0.y);
			break;
		}
	}
#endif

	if (foundit == 0) {
		p0.x = picp->bxy_;
		p0.y = 0.0;
		p0.z = 0.0;
	}

	{
		double mx, my;
		U32 mcount;

		mx = 0; my = 0; mcount = 0;
		for (i = 0; i < pic->bcseqcount; i++) {
			iana_ballcandidate_t *pbs;
			pbs = &pic->bs[i];
			if (pbs->cexist == IANA_BALL_EXIST) {
				mx = mx + pbs->L.x;
				my = my + pbs->L.y;
				mcount++;
			}
		}
		if (mcount > 1) {
			p0.x = mx / mcount;
			p0.y = my / mcount;
			p0.z = 0;
		}
	}

    // get local line here
	pLocLine = &picp->LocLine;
	cr_line_p0m(&p0, &m, pLocLine);

#ifdef DEBUG_SHOT
	for (i = 0; i < pic->bcseqcount; i++) {
		iana_ballcandidate_t *pbs;
		pbs = &pic->bs[i];
		if (pbs->cexist == IANA_BALL_EXIST) {
			cr_point_t p0;
			double value;

			//--
			p0.x = pbs->L.x;
			p0.y = pbs->L.y;
			p0.z = 0.0;

			value = cr_check_P_on_Line(&p0, pLocLine);

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" <%d> %lf %lf %lf   on LocLine: %lf\n", i, p0.x, p0.y, p0.z, value);
		}
	}
#endif

	//--- Make Shot plane
	//  Sight vector between cam and Point at ly = 0.0    v0 = startP - camP
	v0.x = p0.x - pCamPosL->x;
	v0.y = p0.y - pCamPosL->y;
	v0.z = p0.z - pCamPosL->z;

	// n = v0 x m_of_line
	cr_vector_cross(&v0, &pLocLine->m, &n);

	// make plane with cam position and n
	cr_plane_p0n(pCamPosL, &n, pShotPlane);

	res = 1;
	return res;
}

/*!
 ********************************************************************************
 *	@brief      get the best shot plane passing all ball positions and camera position. local line and shot plane are updated(calculated) in here
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @param[in]	camid
 *              camera index - 0:master/center, 1:slave/side
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/02/24
 *******************************************************************************/
static I32 GetShotPlane3(iana_t *piana, U32 camid)
{
	I32 res;
	U32 i;
	U32 count;
	iana_cam_t	*pic;
	iana_cam_param_t *picp;

	cr_point_t *pCamPosL;

	cr_vector_t m;
	cr_point_t p0;			// Point at L.y = 0.0
	point_t    Pp;

#define MAXPLANE3	128
	double xx[MAXPLANE3], yy[MAXPLANE3];
	double mx_, bx_;
	double r2;
	cr_line_t LocLine0;		// Without Start point
	cr_line_t LocLine1;		// With Start point
	cr_line_t *pLocLine;
	cr_vector_t v0;
	cr_vector_t n;
	cr_vector_t n0;
	cr_plane_t *pShotPlane;		// Shot Plane

	double nvalue;

	//---------

	//-- Camposition
	pic 	= piana->pic[camid];
	picp 	= &pic->icp;
	pCamPosL= &picp->CamPosL;
	pLocLine = &picp->LocLine;
	pShotPlane = &picp->ShotPlane;

	count = 0;

	//
#define STARTPOINT_MULT_3	4
	for (i = 0; i < STARTPOINT_MULT_3; i++) {
		xx[count] = picp->L.x; yy[count] = picp->L.y; count++;
	}
	
	for (i = 0; i < pic->bcseqcount; i++) {
		iana_ballcandidate_t *pbs;
		pbs = &pic->bs[i];

#if defined (USE_IANA_BALL)
		if (pbs->cexist == IANA_BALL_EXIST) 
#else
		if (pbs->cexist)
#endif
		{
			point_t Lp;
			Pp.x = pbs->P.x;
			Pp.y = pbs->P.y;

#define ZVALUE2		(0.5) // 0.1, 10
			iana_P2L_EP(piana, camid, ZVALUE2, &Pp, &Lp); 

			xx[count] = Lp.x;
			yy[count] = Lp.y;

			count++;
		}

		if (count >= MAXPLANE3) {
			break;
		}
	}

#define MINCOUNT3	2 // 3
	if (count < MINCOUNT3 + STARTPOINT_MULT_3) {
		res = 0;
		goto func_exit;
	}

	//------
	cr_regression2(yy+STARTPOINT_MULT_3,xx+STARTPOINT_MULT_3, count-STARTPOINT_MULT_3, &mx_,  &bx_, &r2);
	m.v[0] = mx_;
	m.v[1] = 1.0;
	m.v[2] = 0.0;

	p0.x = xx[STARTPOINT_MULT_3];
	p0.y = yy[STARTPOINT_MULT_3];
	p0.z = 0;

	cr_line_p0m(&p0, &m, &LocLine0);		// Without Start Point

	//------
	cr_regression2(yy,xx, count, &mx_,  &bx_, &r2);
	m.v[0] = mx_;
	m.v[1] = 1.0;
	m.v[2] = 0.0;

	p0.x = xx[0];
	p0.y = yy[0];
	p0.z = 0;

	//--
	cr_line_p0m(&p0, &m, &LocLine1);		// With Start Point

	{
		double dist;
		dist = cr_check_P_on_Line(&picp->L,  &LocLine0);

#define DISTANCE_TOO_LARGE	0.05
		if (dist < DISTANCE_TOO_LARGE) {		// good. Use	WITH-START-POINT
			memcpy(pLocLine, &LocLine1, sizeof(cr_line_t));
		} else {				//       Use	WITHOUT-START-POINT
			memcpy(pLocLine, &LocLine0, sizeof(cr_line_t));
		}
	}


	//--- Make Shot plane
	//  Sight vector between cam and Point at ly = 0.0    v0 = startP - camP

	v0.x = pLocLine->p0.x - pCamPosL->x;
	v0.y = pLocLine->p0.y - pCamPosL->y;
	v0.z = pLocLine->p0.z - pCamPosL->z;

	// n = v0 x m_of_line
	cr_vector_cross(&v0, &pLocLine->m, &n);
	nvalue = cr_vector_normalization(&n, &n0);
#define TOOSMALLVALUE	(1e-10)
	if (nvalue > TOOSMALLVALUE) {
		memcpy(&n, &n0, sizeof(cr_vector_t));
	}

	// make plane with cam position and n
	cr_plane_p0n(pCamPosL, &n, pShotPlane);


	res = 1;

func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      CAM shot 3D line
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/02/26
 *******************************************************************************/
I32 iana_shot_3Dtrajline(iana_t *piana)
{
	I32 res;
	iana_cam_t	*pic0;
	iana_cam_param_t *picp0;

	iana_cam_t	*pic1;
	iana_cam_param_t *picp1;

	cr_plane_t  *ppl0;	// Plane of Center CAM
	cr_plane_t  *ppl1;	// Plane of Side   CAM

	cr_line_t *pShotLine;

	//---------------------------------------------
	pic0 	= piana->pic[0];
	picp0 	= &pic0->icp;
	pic1 	= piana->pic[1];
	picp1 	= &pic1->icp;

	ppl0 = &picp0->ShotPlane;
	ppl1 = &picp1->ShotPlane;

	pShotLine = &piana->ShotLine;

	cr_line_plane_plane(
			ppl0,
			ppl1,
			pShotLine);

	{
		double v0, v1;

		v0 = cr_check_Line_on_Plane(pShotLine, ppl0);
		v1 = cr_check_Line_on_Plane(pShotLine, ppl1);

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"shotplane0 on shotline: %lf vs shotplane1 on shotline: %lf\n", v0, v1);
	}

	res = 1;
	return res;
}

/*!
 ********************************************************************************
 *	@brief      CAM shot get all points on the 3D line
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/02/25
 *******************************************************************************/
I32 iana_shot_allpointsonline(iana_t *piana, U32 camid)	
{
	I32 res;
	U32 i;

	iana_cam_t	*pic;
	iana_ballcandidate_t *pbs;

	//-----
	pic = piana->pic[camid];

#ifdef DEBUG_SHOT
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]-----pbs   BallPos2L\n", camid);
	for (i = 0; i < BALLSEQUENCELEN; i++) {
        ballmarkinfo_t		*pbmi;
		pbs = &pic->bs[i];
		pbmi = &pic->bmi[i];

		//if (pbs->cexist == IANA_BALL_EXIST && pbmi->valid) 
		//if (pbs->cexist == IANA_BALL_EXIST)
		{
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                "<%2d>  %10lf %10lf \n", 
				i, pbs->L.x, pbs->L.y);
		}
	}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---------\n");
#endif

	memset(&pic->PointOnLine[0], 0, sizeof(cr_point_t) * BALLSEQUENCELEN);
	for (i = 0; i < BALLSEQUENCELEN; i++) {
		pbs = &pic->bs[i];
#if defined (USE_IANA_BALL)
		if (pbs->cexist == IANA_BALL_EXIST) 
#else
		if (pbs->cexist)
#endif
		{
			GetPointOnLine(piana, camid, &pbs->L, &pic->PointOnLine[i]);
		}
	}

	res = 1;
	return res;
}

/*!
 ********************************************************************************
 *	@brief      CAM shot get point on the 3D line
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @param[in]	camid
 *              camera id
 *  @param[in]	pongline
 *              point on the ground line
 *  @param[out]	ponline
 *              point on the shot line
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/02/26
 *******************************************************************************/
static I32 GetPointOnLine(iana_t *piana, U32 camid, cr_point_t *pongline, cr_point_t *ponline)
{
	I32 res;
	double v0, v1;

	iana_cam_t	*pic;
	cr_line_t   cambline;	// camera to ball line
	cr_point_t  pOnTraj;
	
	double dx, dy, dz, dvalue;
	//----
	pic = piana->pic[camid];

	cr_line_p0p1(pongline, &pic->icp.CamPosL, &cambline);	// make camera to ball line


	//res =  cr_point_line_line(&cambline, &piana->ShotLine, ponline, &pOnTraj);					// use  cam-local line
	res =  cr_point_line_line(&cambline, &piana->ShotLine, &pOnTraj, ponline);					// use traj line

	dx = ponline->x - pOnTraj.x;
	dy = ponline->y - pOnTraj.y;
	dz = ponline->z - pOnTraj.z;
	dvalue = sqrt(dx*dx + dy*dy + dz*dz);

	v0 = cr_check_P_on_Line(ponline, &piana->ShotLine);
	v1 = cr_check_P_on_Line(&pOnTraj, &piana->ShotLine);

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "pol   %10lf %10lf %10lf (%10lf)vs %10lf %10lf %10lf (%10lf),   e: %10lf %10lf %10lf   %10lf\n",
	//  ponline->x, ponline->y, ponline->z, v0,
	//  pOnTraj.x, pOnTraj.y, pOnTraj.z, v1,
	//  dx, dy, dz, dvalue );


	return res;
}
	
/*!
 ********************************************************************************
 *	@brief      CAM bulk request
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/11/04
 *******************************************************************************/
I32 iana_requestBulk(iana_t *piana)		
{
	I32 res;
	//U64 ts64C;
	//U64 ts64S;

	U32 i;
	U64 ts64[MAXCAMCOUNT];


	U64 ts64shot;
	I32 axx1024;
	I32 bxx1024;

	I32 ayx1024;
	I32 byx1024;


	iana_cam_t	*pic;
	iana_cam_param_t *picp;
	iana_setup_t *pisetup;
	iana_cam_setup_t *picsetup;

	I32 marksequencelen;

	U32 camidlist[NUM_CAM];
	U32 camid;

	U32 offset_x;
	U32 offset_y;
	U32 bulkmult;

	U32 	rindex;
	//----
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}
	pisetup = &piana->isetup;

	//--- 1) get timestamp.


	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK CENTER\n\n\n\n");

	iana_requestBulk_TimeStamp(piana, &ts64[0], &ts64[1]);

	scamif_stop(piana->hscamif);
	cr_sleep(10);					// why?   TODO: Remove this sleep. :P
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "B 00\n");


	{
		I32 processmode0, processmode1;
		processmode0 = piana->processmode[0];
		processmode1 = piana->processmode[1];

		if ((processmode0 == CAMPROCESSMODE_BALLCLUB) && (processmode1 == CAMPROCESSMODE_BALLMARK)) {
			camidlist[0] = 1;
			camidlist[1] = 0;
		} else  {
			camidlist[0] = 0;
			camidlist[1] = 1;
		}
	}

	for (i = 0; i < NUM_CAM; i++) {
		camid = camidlist[i];
		picsetup = &pisetup->icsetup[camid];

		pic = piana->pic[camid];

		if (piana->opmode == IANA_OPMODE_FILE) {
			// DO NOTHING..
		} else {
			U32 readframe;

			offset_x = pic->offset_x;
			offset_y = pic->offset_y;


			readframe = marksequencelen;

			bulkmult = picsetup->mult_for_bulk;
			scamif_image_property_BULK(piana->hscamif, camid, 
					picsetup->bulk_width,
					picsetup->bulk_height,
					offset_x, offset_y);

			scamif_imagebuf_init(piana->hscamif, camid, 2, NULL);

			ts64shot = piana->pic[camid]->ts64shot;
			picp = &piana->pic[camid]->icp;

			if (bulkmult == 1 || bulkmult == 0) {
				axx1024 = 0;
				bxx1024 = 0;

				ayx1024 = 0;
				byx1024 = 0;
			} else {
				axx1024 = (I32) (picp->mPxx_ * 1024);
				bxx1024 = (I32) (picp->bPxx_ * 1024);

				ayx1024 = (I32) (picp->mPyy_ * 1024);
				byx1024 = (I32) (picp->bPyy_ * 1024);
			}


			cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%d] Before BULK request, tsfrom: %lld(%llx), tsshot: %lld(%llx) zzz\n", camid, 
					ts64[camid], ts64[camid],
					ts64shot, ts64shot );


			scamif_cam_BULK64(piana->hscamif, 
					camid,   	// BULK transfer.. Center
					ts64[camid],
					readframe,
					0,			// wait time
					0, 			// continue after bulk
					bulkmult,
					1
					// --
					, ts64shot,
					axx1024, 
					bxx1024, 
					ayx1024, 
					byx1024 
					);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "B %d\n", i);
		}

		pic->firstbulkindex = (U32) -1;
		res = bulkFirstindex(piana, camid, &rindex); 
		if (res == 1) {
			pic->firstbulkindex = rindex;
		}
	}
	return 1;
}

/*!
 ********************************************************************************
 *	@brief      CAM bulk request timestamp ts64
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/11/04
 *******************************************************************************/
I32 iana_requestBulk_TimeStamp(iana_t *piana, U64 *pts64C, U64 *pts64S )	
{
	U64 ts64shot;			// for checkready cam
	U64 ts64from;

	U64 ts64shotO;			// for other cam
	U64 ts64fromO;
	
	U32 camidR;					// Camid for Ready camera
	U32 camidO;					// Camid for the Other camera

	U64 preshot;
	iana_setup_t *pisetup;

	camidR = piana->camidCheckReady;
	camidO = 1 - camidR;

	pisetup = &piana->isetup;

	preshot = pisetup->icsetup[0].bulk_time_preshot;

	ts64shot = piana->pic[camidR]->ts64shot;
	ts64from = ts64shot - preshot;

	ts64shotO = piana->pic[camidO]->ts64shot;
	ts64fromO = ts64shotO - preshot;


	if (camidR == 0) {			// Center (master) camd is Ready cam 
		*pts64C = ts64from;
		*pts64S = ts64fromO;
	} else { 					// Side (master) camd is Ready cam 
		*pts64C = ts64fromO;
		*pts64S = ts64from;
	}


	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" CENTER_0: %016llx (%lf),  SIDE_1: %016llx (%lf)\n", 
		*pts64C, *pts64C/ TSSCALE_D,
		*pts64S, *pts64S/ TSSCALE_D);


	return 1;
}

/*!
 ********************************************************************************
 *	@brief      CAM shot putter check..
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/02/24
 *******************************************************************************/
I32 iana_putter_check(iana_t *piana)
{
	I32 res;
	U32 camid;

	U32 rindexshot;
	U32 rindex;
	U32 i, j;
	iana_cam_t	*pic;
	camif_buffer_info_t *pb;
	U32 imagecount;
	camimageinfo_t     *pcii;

	U32	width;
	U32	height;
	U32	offset_x;
	U32	offset_y;

	U08 *buf;
	int len = PATHBUFLEN;
	char imgpath[PATHBUFLEN];
	char fn[1024];
	
	char *pimg3R;
	U32 imagedata;
	U64 ts64;

	I32 cx, cy;
	//--

	pimg3R = NULL;
	imagedata = 1;

	OSAL_STR_ConvertUnicode2MultiByte(piana->szImageDir, imgpath, len);

	for (camid = 0; camid < NUM_CAM; camid++) {
		pic = piana->pic[camid];
		rindexshot = pic->rindexshot;
		imagedata = 1;
		cx = 0; cy = 0;
		nxmlie(1, "<camera id=\"%d\">", camid);
		{
			if (piana->processarea == IANA_AREA_PUTTER) {
				imagecount = INFOIMAGE_PUTTER_COUNT;
			} else {
				imagecount = INFOIMAGE_COUNT;
			}

#define PRESHOTIMAGE_PUTTER_COUNT	10
			for (i = 0; i < imagecount ; i++) 
			{
				//							rindex = (rindexshot + i - READYIMAGE_PRESHOT);
				rindex = (rindexshot + i - PRESHOTIMAGE_PUTTER_COUNT);

				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_NORMAL, rindex, &pb, &buf);

				if (res) {

					pcii 		= &pb->cii;
					width 		= pcii->width;
					height 		= pcii->height;
					offset_x 	= pcii->offset_x;
					offset_y 	= pcii->offset_y;

					if (imagedata == 1) {
						nxmlie(1,  "<imagedata>");
						{
							U64 ts64shot;
//							I32 cx, cy;
							double balldirection;
							double clubdirection;
							iana_cam_param_t *picp;
							iana_shotresult_t *pcsr;;

							picp	= &pic->icp;
							pcsr = &piana->clubball_shotresult[camid];

							ts64shot  = pic->ts64shot;
							balldirection = atan(picp->mxy_);
							balldirection = RADIAN2DEGREE(balldirection);
							clubdirection = pcsr->clubpath;

							cx = (I32)(pic->icp.P.x);
							cy = (I32)(pic->icp.P.y);

							cx = cx - offset_x;
							cy = cy - offset_y;

							cx = width - cx;
							cy = height - cy;
							nxmlie(0,  "<processmode>%d</processmode>", piana->processmode[camid]);
							nxmlie(0,  "<width>%d</width>", width);
							nxmlie(0,  "<height>%d</height>", height);
							nxmlie(0,  "<shotts64>%llu</shotts64>", ts64shot);
							nxmlie(0,  "<ballposition>%d %d</ballposition>", cx, cy);
							nxmlie(0,  "<balldirection>%10.6lf</balldirection>", balldirection);
							//		nxmlie(0,  "<clubdirection>%10.6lf</clubdirection>", clubdirection);
							nxmlie(0,  "<clubdirection>0.0</clubdirection>");						// NO Club directin YET..
						}
						nxmlie(-1,  "</imagedata>");
						imagedata = 0;
						nxmlie(1,  "<images>");
					}

					if (pimg3R == NULL) {
						pimg3R= (char *) malloc(width*height*3 + 128);
					}


					if (s_use_pp_pp) {
#ifdef IPP_SUPPORT
						U08 *bufP;
						point_t Ppp[4],PppP[4], Lpp[4], LppP[4];
						U32 i;
						double xL, yT, xR, yB;
						double srcQuad[4][2], dstQuad[4][2];
						IppiSize roisize;
						IppiRect roi;

						//----
						bufP= (U08 *) malloc(width*height + 128);
						Ppp[0].x = offset_x; 			Ppp[0].y = offset_y;
						Ppp[1].x = offset_x+width-1; 	Ppp[1].y = offset_y;

						Ppp[2].x = offset_x+width-1; 	Ppp[2].y = offset_y+height-1;
						Ppp[3].x = offset_x; 			Ppp[3].y = offset_y+height-1;

						for (i = 0; i < 4; i++) {
							iana_P2L(piana, camid, &Ppp[i], &Lpp[i], 0);
							srcQuad[i][0] = Ppp[i].x-offset_x;
							srcQuad[i][1] = Ppp[i].y-offset_y;
						}
						yT = Lpp[0].y; if (yT > Lpp[3].y) { yT = Lpp[3].y; }		// Smaller
						yB = Lpp[1].y; if (yB < Lpp[2].y) { yB = Lpp[2].y; }		// Bigger

						xL = Lpp[2].x; if (xL < Lpp[3].x) { xL = Lpp[3].x; }		// Bigger
						xR = Lpp[0].x; if (xR > Lpp[1].x) { xR = Lpp[1].x; }		// Smaller


//#define PP_MARGIN_L	0.1
//#define PP_MARGIN_L	0.03
#if defined(PP_MARGIN_L)

						xL -= PP_MARGIN_L;
						xR += PP_MARGIN_L;
						yT += PP_MARGIN_L;
						yB -= PP_MARGIN_L;
#endif

						LppP[0].x = LppP[1].x = xR;
						LppP[2].x = LppP[3].x = xL;

						LppP[0].y = LppP[3].y = yT;
						LppP[1].y = LppP[2].y = yB;


						for (i = 0; i < 4; i++) {
							iana_L2P(piana, camid, &LppP[i], &PppP[i], 0);
							dstQuad[i][0] = PppP[i].x-offset_x;
							dstQuad[i][1] = PppP[i].y-offset_y;
						}

						roisize.width  		= width;
						roisize.height 		= height;

						roi.x 		= 0;
						roi.y 		= 0;
						roi.width 	= width;
						roi.height	= height;

						ippiWarpPerspectiveQuad_8u_C1R(
								buf,  roisize, width, roi, dstQuad,
								bufP,          width, roi, srcQuad,
								IPPI_INTER_CUBIC);


						if (s_putter_out_gamma) {
							gamma_correction(bufP, bufP, width, height);
						}

						for (j = 0; j < (I32) (width * height); j++) {
							U08 c;
							c = *(bufP + j);
							*(pimg3R + j*3 + 0) = c;			// R
							*(pimg3R + j*3 + 1) = c;			// G
							*(pimg3R + j*3 + 2) = c;			// B
						} 
#else
                        U08 *bufP;
                        point_t Ppp[4], PppP[4], Lpp[4], LppP[4];
                        U32 i;
                        double xL, yT, xR, yB;
                        cv::Point2f srcQuad[4], dstQuad[4];
                        cv::Size ssize(width, height);

                        cv::Mat perspMat;
                        cv::Mat cvImgBuf(ssize, CV_8UC1, buf), cvImgTransf;

                        //----
                        bufP= (U08 *)malloc(width*height + 128);
                        Ppp[0].x = offset_x; 			
                        Ppp[0].y = offset_y;

                        Ppp[1].x = offset_x+width-1; 	
                        Ppp[1].y = offset_y;

                        Ppp[2].x = offset_x+width-1; 	
                        Ppp[2].y = offset_y+height-1;

                        Ppp[3].x = offset_x; 			
                        Ppp[3].y = offset_y+height-1;

                        for (i = 0; i < 4; i++) {
                            iana_P2L(piana, camid, &Ppp[i], &Lpp[i], 0);
                            srcQuad[i] = cv::Point2f((float)Ppp[i].x-offset_x, (float)Ppp[i].y-offset_y);
                        }

                        yT = Lpp[0].y; if (yT > Lpp[3].y) { yT = Lpp[3].y; }		// Smaller
                        yB = Lpp[1].y; if (yB < Lpp[2].y) { yB = Lpp[2].y; }		// Bigger

                        xL = Lpp[2].x; if (xL < Lpp[3].x) { xL = Lpp[3].x; }		// Bigger
                        xR = Lpp[0].x; if (xR > Lpp[1].x) { xR = Lpp[1].x; }		// Smaller

                        LppP[0].x = LppP[1].x = xR;
                        LppP[2].x = LppP[3].x = xL;

                        LppP[0].y = LppP[3].y = yT;
                        LppP[1].y = LppP[2].y = yB;


                        for (i = 0; i < 4; i++) {
                            iana_L2P(piana, camid, &LppP[i], &PppP[i], 0);
                            dstQuad[i] = cv::Point2f((float)PppP[i].x-offset_x, (float)PppP[i].y-offset_y);
                        }

                        perspMat = cv::getPerspectiveTransform(srcQuad, dstQuad);
                        cv::warpPerspective(cvImgBuf, cvImgTransf, perspMat, ssize, cv::INTER_CUBIC, cv::BORDER_REPLICATE);
                        memcpy(bufP, cvImgTransf.data, sizeof(U08) * cvImgTransf.cols * cvImgTransf.rows);

                        if (s_putter_out_gamma) {
                            gamma_correction(bufP, bufP, width, height);
                        }

                        for (j = 0; j < (width * height); j++) {
                            U08 c;
                            c = *(bufP + j);
                            *(pimg3R + j*3 + 0) = c;			// R
                            *(pimg3R + j*3 + 1) = c;			// G
                            *(pimg3R + j*3 + 2) = c;			// B
                        }
#endif
					} else {
						U08 *pbufdest;
						pbufdest = NULL;
						if (s_putter_out_gamma) {
							pbufdest = (U08 *)malloc(width*height);
							gamma_correction(buf, pbufdest, width, height);
						} else {
							pbufdest = buf; 
						}
						for (j = 0; j <  (width * height); j++) {
							U08 c;
							c = *(pbufdest + j);
							*(pimg3R + j*3 + 0) = c;			// R
							*(pimg3R + j*3 + 1) = c;			// G
							*(pimg3R + j*3 + 2) = c;			// B
						}
						if (s_putter_out_gamma) {
							free(pbufdest);
						}
					}

					{
#ifdef IPP_SUPPORT
                        IppiSize roisizeM;

                        roisizeM.width = width;
                        roisizeM.height = height;
                        ippiMirror_8u_C3IR(
                            (Ipp8u *)pimg3R,
                            roisizeM.width*3,
                            roisizeM,
                            ippAxsBoth
                        );
#else
                        cv::Mat cvImgSrc(cv::Size(width, height), CV_8UC3, pimg3R), cvImgFlipped;

                        cv::flip(cvImgSrc, cvImgFlipped, -1);
                        memcpy(pimg3R, cvImgFlipped.data, sizeof(U08) * 3 * cvImgFlipped.cols * cvImgFlipped.rows);
#endif
					}

					{	// Draw scale..
						//double x0, y0;
						double Lx0, Ly0;
						double Px, Py;
						I32 xn, yn;
						I32 xn_1, yn_1;
						point_t Pp, Lp;
						I32 n;
						U32 bgrvalue;
						
#define MAXXXYY	10
						double xx[MAXXXYY], yy[MAXXXYY];
						Lx0 = pic->icp.L.x;
						Ly0 = pic->icp.L.y;

						for (n = 0; n < MAXXXYY; n++) {
							Lp.x = Lx0;
							Lp.y = Ly0 + n * (0.01 * 10);				// per 10cm
							iana_L2P(piana, camid, &Lp, &Pp, 0);

							Px = Pp.x - offset_x;
							Py = Pp.y - offset_y;

							Px = width - Px;
							Py = height - Py;

							xx[n] = Px;
							yy[n] = Py;
							if (s_use_pp_pp) {
								//yy[n] = height/2;
								yy[n] = yy[0];
							}
						}

						for (n = 1; n < MAXXXYY; n++) {
							xn_1 = (I32) (xx[n-1] + 0.5);
							yn_1 = (I32) (yy[n-1] + 0.5);
							xn   = (I32) (xx[n] + 0.5);
							yn   = (I32) (yy[n] + 0.5);

#define BARLENGTH	10	
#define XMARGIN	(5)
#define YMARGIN	(BARLENGTH + 5)
							if(
									(xn_1 > XMARGIN && xn_1 < (I32)width - XMARGIN)
									&& (xn > XMARGIN && xn <(I32) width - XMARGIN)
									&& (yn_1 > YMARGIN && yn_1 <(I32) height - YMARGIN)
									&& (yn > YMARGIN && yn <(I32) height - YMARGIN)
							  )
							{
								bgrvalue = MKBGR(0,150,255);
								//mylineBGR(xn_1, yn_1-1, xn, yn-1, bgrvalue, (U08 *)pimg3R, width);
								mylineBGR(xn_1, yn_1+0, xn, yn+0, bgrvalue, (U08 *)pimg3R, width);
								mylineBGR(xn_1, yn_1+1, xn, yn+1, bgrvalue, (U08 *)pimg3R, width);

								if (n == 1) {
									//mylineBGR(xn_1-1, yn_1-BARLENGTH, xn_1-1, yn_1+BARLENGTH, MKBGR(0, 0, 0xFF), (U08 *)pimg3R, width);
									mylineBGR(xn_1+0, yn_1-BARLENGTH, xn_1+0, yn_1+BARLENGTH, MKBGR(0, 0, 0xFF), (U08 *)pimg3R, width);
									mylineBGR(xn_1+1, yn_1-BARLENGTH, xn_1+1, yn_1+BARLENGTH, MKBGR(0, 0, 0xFF), (U08 *)pimg3R, width);
								}

								//mylineBGR(xn-1, yn-BARLENGTH, xn-1, yn+BARLENGTH, MKBGR(0, 0, 0xFF), (U08 *)pimg3R, width);
								mylineBGR(xn+0, yn-BARLENGTH, xn+0, yn+BARLENGTH, MKBGR(0, 0, 0xFF), (U08 *)pimg3R, width);
								mylineBGR(xn+1, yn-BARLENGTH, xn+1, yn+BARLENGTH, MKBGR(0, 0, 0xFF), (U08 *)pimg3R, width);
							}
						}

						if (piana->himgfunc) {
							((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimg3R, width, height, 3, 4, 2, 3, piana->imguserparam);
						}
					}


					if (camid == 0) {
						sprintf(fn, "topview%02d.jpg", i);
					} else {
						sprintf(fn, "m%d_%04d.jpg", camid, i);
					}
					nxmlie(1,  "<image>");
					{
						ts64 = MAKEU64(pb->ts_h, pb->ts_l);
						nxmlie(0,  "<ts64>%llu</ts64>", ts64);
						nxmlie(0,  "<filename>%s</filename>", fn);
					}
					nxmlie(-1,  "</image>");


					saveimageJpegBGR(piana->hjpeg, 
							80,
							imgpath,
							fn,
							(char *)pimg3R,
							width,			// CAM0_RUN_WIDTH, 
							height			// CAM0_RUN_HEIGHT
							);
				}
			} 		// for (i = 0; i < imagecount /*INFOIMAGECOUNT*/; i++) 
		}
		nxmlie(-1,  "</images>");
		nxmlie(-1, "</camera>");
	}

	if (pimg3R != NULL) {
		free(pimg3R);
	}

	res = 1;
	return res;
}


#if defined(CHECKREADYFILE)
static I32 CheckReadyBall1(iana_t *piana)
{
	I32 res;
	I32 res0;
//	U32	camidReady;
	camif_buffer_info_t *pb;
	U08 *buf;
	iana_cam_t	*pic;
	U32 width;
	U32 height;
	IppiSize roisize;
	U08 *pimgdest;

	U32 checkindex;
	I32 checkindex_aftershot;

	U32	camidCheckready;

	//--
	res = 1;
	camidCheckready = piana->camidCheckReady;
	if (piana->camsensor_category == CAMSENSOR_P3V2 && piana->processarea == IANA_AREA_PUTTER) {
		camidCheckready = piana->camidOther;
	}

	//-- Check distance between Ball Ready positin and Shotline 
	{
		double dist1, dist2;
		double toofardist;
		// check 

		pic = piana->pic[camidCheckready];
		dist1 = cr_distance_P_Line(&pic->icp.b3d, &piana->ShotLine);
		dist2 = cr_check_P_on_Line(&pic->icp.b3d, &piana->ShotLine);

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%d] dist1: %lf  check dist2: %lf\n", camidCheckready, dist1, dist2);
#define READYPOS2SHOTLINE_TOO_FAR			(0.15)					// ball diameter.. 
#define READYPOS2SHOTLINE_TOO_FAR__PUTTER	(0.042)					// ball diameter.. 
		
#define CHECKINDEX_AFTERSHOT 5
#define CHECKINDEX_AFTERSHOT_PUTTER 10

		if (piana->processarea == IANA_AREA_PUTTER) {
			toofardist = READYPOS2SHOTLINE_TOO_FAR__PUTTER;
			checkindex_aftershot = CHECKINDEX_AFTERSHOT_PUTTER;
		} else {
			toofardist = READYPOS2SHOTLINE_TOO_FAR;
			checkindex_aftershot = CHECKINDEX_AFTERSHOT;
		}

		if (dist1 > toofardist) {
			res = 0;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,  __FUNCTION__" Distance between Readypos and shotline TOO LARGE. %lf  >  %lf\n", dist1, toofardist);
			goto func_exit;
		}
	}

	checkindex = piana->pic[camidCheckready]->rindexshot + checkindex_aftershot;

	res0 = scamif_imagebuf_randomaccess(piana->hscamif, camidCheckready, 1, checkindex, &pb, &buf); 

	if (res0) {
		I32 x0, y0;
		I32 y;
		I32 cr0;
		I32 sx, ex;
		I32 sums, sumb;
		I32 multitude;
		I32 m;
		I32 i, j;
		double means, meanb;

		width = pb->cii.width;
		height = pb->cii.height;

		roisize.width  		= width;
		roisize.height 		= height;

		pic = piana->pic[camidCheckready];
		pimgdest = pic->pimg[0];

		x0 = (I32) (pic->icp.P.x - pic->offset_x);
		y0 = (I32) (pic->icp.P.y - pic->offset_y);				// Ball position
		cr0 = (I32) (pic->icp.cr);

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" cr0: %d\n", cr0);
		//cr0 = cr0 * 2;
		//cr0 = cr0 * 4;

#define CR_MIN	15
#define CR_MAX	40
		if (cr0 < CR_MIN) {
			cr0 = CR_MIN;
		}
		if (cr0 > CR_MAX) {
			cr0 = CR_MAX;
		}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" -----> cr0: %d\n", cr0);

		sx = x0 - cr0;
		ex = x0 + cr0;

		// TODO: latest code merge - opencv
		if (sx >= 0 && ex <= (I32)width && sx < ex) {
			roisize.width  		= ex - sx;
			//
			//memset(pimgdest, 0xFF, DEF_WIDTH * DEF_HEIGHT);
			ippiAbsDiff_8u_C1R(						// C = |A - B|
					buf + sx, width, 					// source,		// B
					pic->prefimg + sx, width, 			// source,		// A
					(Ipp8u *)pimgdest + sx, 	width, 		// C, result.
					roisize);


			multitude 	= pb->cii.multitude;
			if (multitude < 1) {
				multitude = 1;
			}

			sums = 0;
			sumb = 0;
			for (m = 0; m < multitude; m++) {
				for (i = -cr0; i < cr0; i += multitude) {
					y = (m * height + y0 + i) / multitude;
					for (j = sx; j <= ex; j++) {
						sums = sums + (I32)*(pimgdest + y * width + j);
						sumb = sumb + (I32)*(buf      + y * width + j);
					}
				}
			}

			means = (double)sums / (4.0 * cr0 * cr0);
			meanb = (double)sumb / (4.0 * cr0 * cr0);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,  __FUNCTION__" sums: %d, means: %lf,  sumb: %d, meanb: %lf\n", sums, means, sumb, meanb);
//#define CHECK_READYBALL_MEANS		50
#define CHECK_READYBALL_MEANS		40
//#define CHECK_READYBALL_MEANS_GGG	70
#define CHECK_READYBALL_MEANS_GGG	60

#define CHECK_READYBALL_MEANB_MIN	40
#define CHECK_READYBALL_MEANB_CHECK	70
			if (meanb > CHECK_READYBALL_MEANB_MIN) {
				if (means > CHECK_READYBALL_MEANS_GGG) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,  __FUNCTION__" GOOD... \n");
				} else {
					if (means < CHECK_READYBALL_MEANS) {
						res = 0;
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,  __FUNCTION__" DISCARD IT  #1 check_readyball..\n");
					} else if (meanb > CHECK_READYBALL_MEANB_CHECK) {
						res = 0;
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,  __FUNCTION__" DISCARD IT  #2 check_readyball..\n");
					}
				}
			}

			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf, width, height, 0, 1, 2, 3, piana->imguserparam);
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pic->prefimg, width, height, 1, 1, 2, 3, piana->imguserparam);
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimgdest, width, height, 2, 1, 2, 3, piana->imguserparam);
			}
		}
	}

func_exit:
	return res;
}

static I32 CheckReadyBall3(iana_t *piana)
{
	I32 res;
	I32 res0;
//	U32	camidReady;
	camif_buffer_info_t *pb;
	camimageinfo_t	*pcii;
	
	U08 *buf;
	iana_cam_t	*pic;
	U32 width;
	U32 height;
	IppiSize roisize;
	U08 *pimgdest;

	U32 checkindex;
	I32 checkindex_aftershot;

	U32	camidCheckready;

	//--
	res = 1;
	camidCheckready = piana->camidCheckReady;
//	if (piana->camsensor_category == CAMSENSOR_P3V2 && piana->processarea == IANA_AREA_PUTTER) {
//		camidCheckready = piana->camidOther;
//	}

	if (piana->processarea == IANA_AREA_PUTTER) {
		checkindex_aftershot = CHECKINDEX_AFTERSHOT_PUTTER;
	} else {
		checkindex_aftershot = CHECKINDEX_AFTERSHOT;
	}

	checkindex = piana->pic[camidCheckready]->rindexshot + checkindex_aftershot;

	res0 = scamif_imagebuf_randomaccess(piana->hscamif, camidCheckready, 1, checkindex, &pb, &buf); 

	if (res0) {
		I32 x0, y0;
		I32 cr0;
		I32 sx, ex;
		I32 sy, ey;
//		I32 sums, sumb;
		I32 multitude;
//		I32 m;
//		I32 i, j;
//		double means, meanb;
		U32 w0, h0;

		//----
		pcii 		= &pb->cii;
		multitude 	= pcii->multitude;
		if (multitude == 0) {
			multitude = 1;
		}

		width = pb->cii.width;
		height = pb->cii.height;

		roisize.width  		= width;
		roisize.height 		= height;

		pic = piana->pic[camidCheckready];
		pimgdest = pic->pimg[0];

		x0 = (I32) (pic->icp.P.x - pic->offset_x);
		y0 = (I32) (pic->icp.P.y - pic->offset_y);				// Ball position
		cr0 = (I32) (pic->icp.cr);

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" cr0: %d\n", cr0);
		//cr0 = cr0 * 2;
		//cr0 = cr0 * 4;

#define CR_MIN	15
#define CR_MAX	40
		if (cr0 < CR_MIN) {
			cr0 = CR_MIN;
		}
		if (cr0 > CR_MAX) {
			cr0 = CR_MAX;
		}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" -----> cr0: %d\n", cr0);
#define CR0_MULT	(2)
		sx = x0 - cr0* CR0_MULT;
		ex = x0 + cr0* CR0_MULT;

		sy = (y0 - cr0* CR0_MULT);
		ey = (y0 + cr0* CR0_MULT);

		w0 = cr0*8; h0 = cr0* CR0_MULT*2;

		if ((sx >= 0 && ex <= (I32)width) && (sy >= 0 && ey <= (I32)height)) {
#ifdef IPP_SUPPORT			
			U32 bcount;
			iana_ballcandidate_t bc[MAXCANDIDATE];
			double	xfact, yfact; 
			IppiSize ssize;
			IppiRect sroi, droi;
			int sstep, dstep;
			IppStatus status;
			Ipp8u 	*resizebuf;
			int 	bufsize;

			//-----=
			xfact 		= 1;
			yfact 		= multitude;

			width 		= pcii->width;
			height 		= pcii->height;


			ssize.width 	= w0;
			ssize.height 	= h0/multitude;

			sroi.x 		= 0;
			sroi.y 		= 0;
			sroi.width 	= w0;
			sroi.height	= h0/multitude;

			droi.x 		= 0;
			droi.y 		= 0;
			droi.width 	= w0;
			droi.height	= h0;

			sstep 		= width;
			dstep		= w0;

			ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_CUBIC, &bufsize);
			resizebuf = (U08 *)malloc(bufsize);

			status = ippiResizeSqrPixel_8u_C1R(
					(Ipp8u *)buf + sx + (sy/multitude) * width,
					ssize,
					sstep,
					sroi,
					//
					(Ipp8u*) pimgdest,
					dstep,
					droi,
					//
					xfact,
					yfact,
					0,		// xshift,
					0,		// yshift,
					IPPI_INTER_CUBIC, 		// IPPI_INTER_NN,   // IPPI_INTER_CUBIC,
					resizebuf
					);

			free(resizebuf);
#else

			// TODO: latest code merge - opencv 

#endif

			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, width, height, 0, 1, 2, 3, piana->imguserparam);
				((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf + sx + (sy / multitude) * width, width, height-100, 1, 1, 2, 3, piana->imguserparam);
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimgdest, w0, h0, 2, 1, 2, 3, piana->imguserparam);
			}
			
			iana_setReadyRunBallsizeMinMax(piana, camidCheckready, 1.2);


			res0 = iana_getballcandidatorAdapt(
					piana, 
					camidCheckready, 
					(Ipp8u *)pimgdest,
					w0, h0, w0,
					&bc[0], 
					&bcount,
					SHOT_HALFWINSIZE, 
					1, 1, 
					5,
					piana->pic[camidCheckready]->icp.CerBallshot
					, 0.1
					);

/*
			res0 = iana_getballcandidator_cht
				(
				 piana, 
				 camidCheckready, 
				 (Ipp8u *)pimgdest,
				 w0, h0, w0,
				 &bc[0], 
				 &bcount,
				 8, 					// FAKE value
				 1, 0,				// FAKE value
				 5.0, 				// FAKE value
				 0.05, 				// FAKE value
				 0.05 				// FAKE value
				);
*/

			if (res0 && bcount > 0) {
				if (bc[0].cexist == IANA_BALL_EXIST) {
					res = 0;
				}
			}
		}
	}

	return res;
}


/*!
 ********************************************************************************
 *	@brief      Check ready position.. compare with REFIMG
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good. Ball is NOT there.   0: Bad.. Ball is still there.. 
 *
 *	@author	    yhsuk
 *  @date       2018/1001
 *******************************************************************************/
static I32 CheckReadyBall(iana_t *piana)
{
	I32 res;


	if (piana->camsensor_category == CAMSENSOR_P3V2 && piana->processarea == IANA_AREA_PUTTER) {
		res = CheckReadyBall3(piana);
	} else {
		res = CheckReadyBall1(piana);
	}

	return res;
}


#endif

/*!
 ********************************************************************************
 *	@brief      Calc Ball Radius using 3D positions
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: Good.  0; BAD
 *
 *	@author	    yhsuk
 *  @date       2020/0721
 *******************************************************************************/
static I32 CalcBallRadiusUsing3DPositions(iana_t *piana)	
{
	I32 res;
	U32 i;
	U32 camid;

	iana_cam_t	*pic;
	iana_cam_param_t *picp;
	iana_ballcandidate_t *pbs;
	cr_point_t *pp;
	cr_point_t *pCamPosL;
	
	//----

	// 1) get campos0, 	campos1
	
	for (camid = 0; camid < NUM_CAM; camid++) {
		double tt[MARKSEQUENCELEN];
		double Lrr[MARKSEQUENCELEN];
		double Prr[MARKSEQUENCELEN];
		double r2;
		double maPrr[3];
		double maLrr[3];
		U32 count;

		//--
		pic = piana->pic[camid];
		picp 	= &pic->icp;
		pCamPosL= &picp->CamPosL;
		count = 0;
		for (i = 0; i < BALLSEQUENCELEN; i++) {
			I64 ts64delta;
			I64 ts64;
			double td;


			//--
			pbs = &pic->bs[i];
			pp = &pic->PointOnLine[i];

			ts64 = pbs->ts64;
			ts64delta = ts64 - pic->ts64shot;
			td = ts64delta / TSSCALE_D;
			if (pbs->cexist == IANA_BALL_EXIST) {
				cr_point_t ballL;
				point_t Ld, Pd;
				double l0;	// cam to ball, ground position
				double l1;	// cam to ball, 3d position
				double lrCalc;
				double prCalc;

				//---

				ballL.x = pbs->L.x;
				ballL.y = pbs->L.y;
				ballL.z = 0;
				l0 = cr_vector_distance(&ballL, pCamPosL);
				l1 = cr_vector_distance(pp, pCamPosL);
				lrCalc = ((l0 / l1) * 0.042)/2;

				Ld.x = pbs->L.x + lrCalc;
				Ld.y = pbs->L.y;
				iana_L2P(piana, camid, &Ld, &Pd, 0);
				{
					double dx, dy;
					dx = Pd.x - pbs->P.x;
					dy = Pd.y - pbs->P.y;
					prCalc = sqrt(dx*dx + dy * dy);
				}

				tt[count] = td;
				Prr[count] = prCalc;
				Lrr[count] = lrCalc;
				count++;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%2d:%10lf] P (%lf %lf : %lf)  L (%lf %lf : %lf) 3D (%lf %lf %lf), l0: %lf l1: %lf lrCalc: %lf  prCalc: %lf\n",
						camid, i, td,
						pbs->P.x, 
						pbs->P.y, 
						pbs->cr,
						pbs->L.x, 
						pbs->L.y, 
						pbs->lr,
						pp->x,
						pp->y,
						pp->z,

						l0, l1, lrCalc, 
						prCalc
						
						);
			}
		} 			// for (i = 0; i < BALLSEQUENCELEN; i++) 

		memset(&maPrr[0], 0, sizeof(double)*3);
		memset(&maLrr[0], 0, sizeof(double)*3);
		if (count >= 2) {
			cr_regression2(tt, Prr, count, &maPrr[1], &maPrr[0], &r2);
			cr_regression2(tt, Lrr, count, &maLrr[1], &maLrr[0], &r2);
		}
		memcpy(&pic->maPrr[0], &maPrr[0], sizeof(double)*3);
		memcpy(&pic->maLrr[0], &maLrr[0], sizeof(double)*3);
	}

	res = 1;

	return res;
}

static void Calc_Incline_Azimuth(iana_t *piana)
{	// Calc incline and azimuth
	double mx, my, mz;
	double azimuth;
	double incline;
	iana_shotresult_t *psr;

	psr = &piana->shotresultdata;

	mx = piana->ShotLine.m.x;
	my = piana->ShotLine.m.y;
	mz = piana->ShotLine.m.z;

	azimuth = RADIAN2DEGREE(atan2(mx, my));
	incline = RADIAN2DEGREE(atan2(mz, my)); 		// Theta = tan-1( h / d)

	if (azimuth < -90) {
		if (incline < -90) {
			azimuth = azimuth + 180;
			incline = incline + 180;
		}
		if (incline > 90) {
			azimuth = azimuth + 180;
			incline = incline - 180;
		}
	}
	if (azimuth > +90) {
		if (incline < -90) {
			azimuth = azimuth - 180;
			incline = incline + 180;
		}
		if (incline > 90) {
			azimuth = azimuth - 180;
			incline = incline - 180;
		}
	}

	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		// NO touch.. :P
	}
	if (piana->camsensor_category == CAMSENSOR_Z3) {			// Match to GCQ. 20190516... 
		FILE *fp;
		int mode;
		double incline1;
		fp = cr_fopen("incmode.txt", "r");
		mode = 1;				// 20190516
		if (fp) {
			fscanf(fp, "%d", &mode);
			cr_fclose(fp);
		}
		if (incline < 10) {
			//incline1 = LINEAR_SCALE(incline,  0.0,  0.0, 10.0, 12.0);		// GCQ  20190406
			incline1 = LINEAR_SCALE(incline,  0.0,  0.1, 10.0, 11.5);		// 20190516
		} else if (incline < 20) {
			//incline1 = LINEAR_SCALE(incline, 10.0, 12, 20.0, 22.5);		// GCQ  20190406
			incline1 = LINEAR_SCALE(incline, 10.0, 11.5, 20.0, 22.0);		// 20190516
		} else if (incline < 30) {
			//incline1 = LINEAR_SCALE(incline, 20.0, 22.5, 30.0, 33.0);		// GCQ	20190406
			incline1 = LINEAR_SCALE(incline, 20.0, 22.0, 30.0, 32.5);		// 20190516
		} else if (incline < 50) {
			//incline1 = LINEAR_SCALE(incline, 30.0, 33.0, 50.0, 55.0);		// GCQ  20190406
			incline1 = LINEAR_SCALE(incline, 30.0, 32.5, 50.0, 54.0);		// 20190516
		} else if (incline < 60) {
			//incline1 = LINEAR_SCALE(incline, 50.0, 55.0, 60.0, 63.0);		// GCQ	20190406
			incline1 = LINEAR_SCALE(incline, 50.0, 54.0, 60.0, 62.0);		// 20190516
		} else {
			//incline1 = incline + 3.0;										// GCQ 	20190406
			incline1 = incline + 2.0;										// 20190516
		}
		if (mode == 0) {
			// ORG incline
		} else if (mode == 1) {
			incline = incline1;
		} else if (mode == 2) {
			incline = (incline + incline1) / 2.0;
		}
	}

	psr->incline = incline;
	psr->azimuth = azimuth;

}

static I32 CalcVmag(iana_t *piana)
{
	U32 camid;
	I32 res = 1;
	cr_point_t Lprev, Lcurrent;
	U64 ts64, ts64prev;
	double vmagc[BALLSEQUENCELEN*2];
	U32 vmagcount;
	U32 countc[MAXCAMCOUNT];
	iana_ballcandidate_t *pbs;
	U32 i;
	iana_shotresult_t *psr;

	psr = &piana->shotresultdata;
	
	memset(&Lprev, 0, sizeof(cr_point_t));
	vmagcount = 0;
	for (camid = 0; camid < NUM_CAM; camid++) {
		iana_cam_t	*pic;
		double vmag;
		U32 count;

		vmag = 0.0;
		count = 0;
		ts64prev = 0;
		pic = piana->pic[camid];
		for (i = 0; i < BALLSEQUENCELEN; i++) {
			//pbs = &piana->ic[camid].bs[i];
			pbs = &pic->bs[i];
#if defined (USE_IANA_BALL)
			if (pbs->cexist == IANA_BALL_EXIST) 
#else
			if (pbs->cexist) 
#endif
			{
				memcpy(&Lcurrent, &pic->PointOnLine[i], sizeof(cr_point_t));
				ts64 = pbs->ts64;
				if (count > 0) {
					double vmag0;
					double dist;
					double tdelta;
					dist = DDIST3(Lprev.x,Lprev.y,Lprev.z, Lcurrent.x,Lcurrent.y,Lcurrent.z);

					tdelta = (ts64 - ts64prev)/TSSCALE_D;

					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%10lf %10lf %10lf> dist: %lf, tdelta: %lf\n", 
					//	Lcurrent.x,Lcurrent.y,Lcurrent.z,
						//	dist, tdelta);
#define TDELTA_MIN		(0.001 * 0.4)
#define TDELTA_MAX		(0.001 * 50)
					if (tdelta > TDELTA_MIN && tdelta < TDELTA_MAX) {
						vmag0 = dist / tdelta;
						vmagc[vmagcount++] = vmag0;
						vmag += vmag0;
						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "v[%d:%d:%d] = %lf (%lf)\n", camid, i, count, vmag0, vmag / count); 
					} else {
						continue;
						// discard it..
					}
				}
				count++;
				memcpy(&Lprev, &Lcurrent, sizeof(cr_point_t));
				ts64prev = ts64;
			}
		}
		countc[camid] = count;
	/*
	   if (count >= 2) {
		   vmagc[camid] = vmag / (count -1); 
	   } else {
		   vmagc[camid] = 0.0;
	   }
			   */
	}

	{	
		// Median/Filtering/Mean/
		int    mi;		// median index;
		double vmd;		// median value;
		double vmax, vmin;
		double vm;

		vm = -1;
		//for (i = 0; i < vmagcount; i++) {
		//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "vmagc[%d]: %lf\n", i, vmagc[i]);
		//}
		qsort( (void *)vmagc, vmagcount, sizeof(double), vmagcompare);
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---->\n");
		//for (i = 0; i < vmagcount; i++) {
		//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "vmagc[%d]: %lf\n", i, vmagc[i]);
		//}

		if (vmagcount > 2) {
			U32 count;
			double vmagsum;
			int vmag_start;

			//--
			mi = vmagcount/2;
			vmd = vmagc[mi];

			vmax = vmd * 1.1;
			vmin = vmd * 0.9;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "vmd(%d): %lf,  %lf ~ %lf\n", mi, vmd, vmax, vmin);

			vm = -1;

			vmagsum = 0.0;
			count = 0;
#define VMAGC_START	4
			if (vmagcount < VMAGC_START*2) {
				vmag_start = 1;
			} else {
				vmag_start = VMAGC_START;
			}
#if 1
			for (i = vmag_start; i < vmagcount - 1; i++)
			{
				double vtmp;

				vtmp = vmagc[i];
				if (vtmp > vmin && vtmp < vmax) {
					//if (i < vmagcount-1) // discard last one.. :P
					{				
						vmagsum += vtmp;
						count++;
#define VMAG_MAXCOUNT 10
						if (count >= VMAG_MAXCOUNT) {
							break;
						}
					}
				}
			}
			if (count >= 1) {
				vm = vmagsum / count;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "vmag average: %lf with %d\n", vm, count);
			} else {
				vm = -999;
			}
#endif
			if (vm < 0) {
				res = 0;
			}
		} else {
			res = 0;
		}

		if (res == 0) { 
			return res;
					//goto func_exit; 
		}
		psr->vmag = vm;
	}
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "vmag[0]: %lf, vmag[1]: %lf\n", vmagc[0], vmagc[1]);
	//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "vmag[0]: %lf, vmag[1]: %lf\n", vmagc[0], vmagc[1]);

	return res;
}

static void OptionalFileOperation4GoodShot(iana_t *piana)
{
#if defined(SHOTDATAFILE)
	{
		U32 shotmode;
		FILE *fp;
		double vmag, azimuth, incline;
		double sidespin, backspin, rollspin;
		double spinmag;
		cr_vector_t axis;
		iana_shotresult_t *psr;

		psr = &piana->shotresultdata;
		
		fp = cr_fopen(SHOTDATAFILE, "r");
		if (fp) {
			fscanf(fp, "%d", &shotmode);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
			if (shotmode == 1) {
				fscanf(fp, "%lf", &vmag);
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

				fscanf(fp, "%lf", &azimuth);
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

				fscanf(fp, "%lf", &incline);
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

				fscanf(fp, "%lf", &sidespin);
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

				fscanf(fp, "%lf", &backspin);
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

				fscanf(fp, "%lf", &rollspin);
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

				axis.x = backspin;
				axis.y = rollspin;
				axis.z = -sidespin;
				spinmag = cr_vector_normalization(&axis, &axis);

				//--
				psr->vmag = vmag;
				psr->incline = incline;
				psr->azimuth = azimuth;

				psr->backspin = backspin;
				psr->sidespin = sidespin;

				psr->spinmag = spinmag;
				memcpy(&psr->axis, &axis, sizeof(cr_vector_t));
			}
			cr_fclose(fp);
		}
	}
#endif

//#define VMULTFILE		"vmult.txt"
//#define VLOGFILE		"vlog.txt"
#if defined(VMULTFILE) 
	{
		FILE *fp, *fpv;
		double vmag, vmag2;
		double vmult;
		iana_shotresult_t *psr;

		//--
		psr = &piana->shotresultdata;
		vmult = 1.0;
		vmag = psr->vmag;
		fp = cr_fopen(VMULTFILE, "r");
		if (fp) {
			int mode;
			mode = 0;
			fscanf(fp, "%d", &mode);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
			if (mode) {
				fscanf(fp, "%lf", &vmult);
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
			}
			cr_fclose(fp);
		}

		vmag2 = vmag*vmult;
		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "kkk vmult: %lf, vmag: %10lf -> %10lf\n", vmult, vmag, vmag2);
		fpv = cr_fopen(VLOGFILE, "a");
		if (fpv) {
			fprintf(fpv, "vmag: %10lf    -> %10lf\n", vmag, vmag2);
			cr_fclose(fpv);
		}

		psr->vmag = vmag2;
	}
#endif


		//zzzz
//#define SAVE_BULK_DATA

#if defined(SAVE_BULK_DATA)
	{
		U32 i, j;
		I32 res;
		U32 firstbi;
		char filepath[1024];
		U08 *buf;
		camif_buffer_info_t *pb;
		camimageinfo_t     *pcii;
		I32 marksequencelen;

		//--
		firstbi = piana->pic[0]->firstbulkindex;
		saveimageprepare(&filepath[0]);
		res = 0;
		buf = NULL;
		pb = NULL;

		if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
			marksequencelen = MARKSEQUENCELEN_EYEXO;
		} else {
			marksequencelen = MARKSEQUENCELEN;
		}


		for (i = 0; i < marksequencelen; i++) {
			for (j = 0; j < 10; j++) {
				res =  scamif_imagebuf_randomaccess(piana->hscamif, 0, NORMALBULK_BULK, i + firstbi, &pb, &buf);
				if (res == 1) {
					break;
				}
				cr_sleep(1);
			}
			if (res == 0) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail..Break\n");
				break;
			}
			pcii = &pb->cii;
			saveimage(&filepath[0], (char *)buf, pcii->width, pcii->height, 0, i);
		}
	}
#endif
}

static void UpdateShotResultCheckFlag(iana_t *piana)
{	
	iana_shotresult_t *psr;

	//--
	psr = &piana->shotresultdata;
	if (piana->shotresultflag) {

#define CHECK_VMAG		80
#define CHECK_INCLINE	80
#define CHECK_AZIMUTH	50
#define CHECK_BACKSPIN	9000
#define CHECK_SIDESPIN	3000
#define CHECK_TOPSPIN	(-3000)

		if (psr->vmag > CHECK_VMAG) {
			piana->shotresultcheckme = SHOTCHECKME_YES;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " Checkme. vmag: %lf\n", psr->vmag);
		}

		if (psr->incline > CHECK_INCLINE) {
			piana->shotresultcheckme = SHOTCHECKME_YES;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " Checkme. incline: %lf\n", psr->incline);
		}

		if (psr->azimuth > CHECK_AZIMUTH || psr->azimuth < -CHECK_AZIMUTH ) {
			piana->shotresultcheckme = SHOTCHECKME_YES;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " Checkme. azimuth: %lf\n", psr->azimuth);
		}

		if (psr->backspin > CHECK_BACKSPIN || psr->backspin < CHECK_TOPSPIN) {
			piana->shotresultcheckme = SHOTCHECKME_YES;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " Checkme. backspin: %lf\n", psr->backspin);
		}

		if (psr->sidespin > CHECK_SIDESPIN || psr->sidespin < -CHECK_SIDESPIN) {
			piana->shotresultcheckme = SHOTCHECKME_YES;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " Checkme. sidespin: %lf\n", psr->sidespin);
		}
	}
	
#define CHECKME_NO
#if defined(CHECKME_NO)
//#error shit
	piana->shotresultcheckme = SHOTCHECKME_NO;
#endif
}

// Check Shot Result.. 20200818.
static U32 CheckShotResult(iana_t *piana)
{
	U32 res;
	iana_shotresult_t *psr;
	double vmag;
	double incline;
	double azimuth;

	double incline_discard_min;
	double incline_min;
	double incline_max;

	double azimuth_max;
	//---
	psr = &piana->shotresultdata;
	vmag = psr->vmag;
	incline = psr->incline;
	azimuth = psr->azimuth;


	//                        incline discard_min    min   discard_max
	//  vmag: ~ 10                    -10            -7    70
	//        ~ 30                    -10            -7    80
	//        ~ 40                    -10            -7    70
	//        ~ 50                    -10            -7    60
	//        ~ 60                    -10            -7    50
	//        ~                       -10            -7    40

	incline_discard_min = -10;
	incline_min 		= -7;
	if (vmag < 10) {
		incline_max 		= 70;
		//azimuth_max = 30;
		azimuth_max = 45;
	} else if (vmag < 30) {
		incline_max 		= LINEAR_SCALE(vmag,  10.0, 70.0, 30.0, 80.0);
		azimuth_max			= 45;
	} else if (vmag < 40) {
		incline_max 		= LINEAR_SCALE(vmag,  30.0, 80.0, 40.0, 70.0);
		azimuth_max			= 45;
	} else if (vmag < 50) {
		incline_max 		= LINEAR_SCALE(vmag,  40.0, 70.0, 50.0, 60.0);
		azimuth_max			= 45;
	} else if (vmag < 60) {
		incline_max 		= LINEAR_SCALE(vmag,  50.0, 60.0, 60.0, 50.0);
		azimuth_max			= 45;
	} else {
		incline_max 		= 40.0;
		azimuth_max			= 45;
	}


	res = 1;
	if (incline < incline_discard_min) {
		res = 0;
	} 
	if (incline > incline_max) {
		res = 0;
	} 
	if (azimuth < -azimuth_max || azimuth > azimuth_max) {
		res = 0;
	}

	if (res != 0) {
		iana_cam_t	*pic;
		iana_cam_param_t *picp;

		U32 camidReady;
		U32 check_meanRH;

		//--
		camidReady = piana->camidCheckReady;
		pic = piana->pic[camidReady];

		if (iana_isInBunker(piana, &pic->icp.b3d)) {
			check_meanRH = 0;
		} else {
			check_meanRH = 1;
		}
//#define CHECKVMAG	(15.0)
#define CHECKVMAG	(15.0)
		if (check_meanRH && vmag < CHECKVMAG) {
			U32 camid;

			double meanRH_last[MAXCAMCOUNT];
			double meanRH_min[MAXCAMCOUNT];
			double meanRH_max[MAXCAMCOUNT];
			U32 res0[MAXCAMCOUNT];

			for (camid = 0; camid < NUM_CAM; camid++) {
				pic = piana->pic[camid];
				picp 	= &pic->icp;
				meanRH_last[camid] 	= picp->meanRH_last;
				meanRH_min[camid] 	= picp->meanRH_min;
				meanRH_max[camid] 	= picp->meanRH_max;
#define MH_L	100
#define MH_LM	(1.0/1.5)
				res0[camid] = 1;
				if (meanRH_last[camid] > MH_L) {
					//res = 0;
					double ratio;

					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] mH: %lf\n", camid, meanRH_last[camid]);

					ratio = meanRH_last[camid] / meanRH_max[camid];
					if (ratio > MH_LM) {
						res0[camid] = 0;
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] ratio: %lf mH_max: %lf\n", camid, ratio, meanRH_max[camid]);
					}
				} else {
				}
			}
			if (res0[0] == 0 && res0[1] == 0) {
				res = 0;
			}
			if (res == 0) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "bad meanRH..\n");
				for (camid = 0; camid < NUM_CAM; camid++) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] mH: %lf mH_min: %lf mH_max: %lf\n",
							camid, 
							meanRH_last[camid],
							meanRH_min[camid],
							meanRH_max[camid]);
				}
			}
		}
	}
	return res;
}



typedef struct _crdiffratio {
	double crdiff;
	double crratio;
} crdiffratio_t;


int crdiffratioCompare(const void *r1, const void *r2) 
{
	crdiffratio_t *pcrd1 = (crdiffratio_t *)r1;
	crdiffratio_t *pcrd2 = (crdiffratio_t *)r2;

	//--
	if (pcrd1->crdiff > pcrd2->crdiff) { return  1;}
	if (pcrd1->crdiff < pcrd2->crdiff) { return -1;}
	return 0;
}

U32 iana_shot_select_winner(iana_t *piana, U32 camid)		// 2021-0117
{
	U32 res;
	U32 i, j;
	iana_ballcandidate_t *pbc;
	iana_cam_t	*pic;
	double cr;
	double crdiff;
	double crratio;

	double crratio_cr;

	crdiffratio_t cdr[MAXCANDIDATE*BALLCCOUNT];
	U32 cdrcount;
	//-----
	pic = piana->pic[camid];
	cr = pic->icp.cr;


	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" cr of Ready Ball: %lf\n", cr);
	cdrcount = 0;
	for (i = 0; i < (I32)MAXCANDIDATE; i++) {
		for (j = 0; j < BALLCCOUNT; j++) {
			pbc = &pic->bc[i][j];
			if (pbc->cexist == IANA_BALL_EXIST) {
				crdiff = fabs(cr - pbc->cr);
				crratio = pbc->cr / cr;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"[%d (%d %d)] cr: %lf diff: %lf ratio: %lf\n", 
						camid, i, j, pbc->cr, crdiff, crratio);
				cdr[cdrcount].crdiff = crdiff;
				cdr[cdrcount].crratio = crratio;
				cdrcount++;
			}
		}
	}
#define CRRATIO_MIN0	0.90
#define CRRATIO_MAX0	1.15
#define CRRATIO_MIN1	0.85
#define CRRATIO_MAX1	1.20
#define CRRATIO_RECALC_MIN	4
	if (cdrcount > 0) {
		double sum_crratio;
		I32 count_crratio;
		U32 selectindex;

		//--
		sum_crratio = 0;
		count_crratio = 0;
		qsort( (void *) &cdr[0], cdrcount, sizeof(crdiffratio_t), crdiffratioCompare);
#define SELECTINDEX	4
		selectindex = cdrcount / SELECTINDEX;
		if (selectindex > 1) {
			for (i = 0; i < selectindex; i++) {
				sum_crratio += cdr[i].crratio;
				count_crratio++;
			} 
			crratio_cr = cr * (sum_crratio / count_crratio); 
		} else {
			crratio_cr = cr * cdr[selectindex].crratio;
		}

#if 0
		for (i = 0; i < cdrcount; i++) {
			if (cdr[i].crratio > CRRATIO_MIN0 && cdr[i].crratio < CRRATIO_MAX0) {
				sum_crratio += cdr[i].crratio;
				count_crratio++;
			} 
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"[%d (%d)] crdiff: %lf crratio: %lf\n",
					camid, i, 
					cdr[i].crdiff,
					cdr[i].crratio);
		}
		if (count_crratio >= CRRATIO_RECALC_MIN) {
			crratio_cr = cr * (sum_crratio / count_crratio); 
		} else {
			crratio_cr = cr;
		}
#endif
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"cr: %lf -> crratio_cr: %lf\n", cr, crratio_cr);

		for (i = 0; i < (I32)MAXCANDIDATE; i++) {
			for (j = 0; j < BALLCCOUNT; j++) {
				double crRatio;
				pbc = &pic->bc[i][j];
				if (pbc->cexist == IANA_BALL_EXIST) {
					crRatio = pbc->cr/crratio_cr;
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"[%d (%d %d)] cr: %lf cr_of_Ball: %lf crRatio: %lf\n", 
							camid, i, j, crratio_cr, pbc->cr, crRatio);
					if (crRatio < CRRATIO_MIN1 || crRatio > CRRATIO_MAX1) {
						pbc->cexist = IANA_BALL_EXILE;
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" EXILE [%d (%d %d)] RATIO\n", camid, i, j);
					}
				}
			}
		}
	}


	res = 1;

	return res;
}



#if defined (__cplusplus)
}
#endif
