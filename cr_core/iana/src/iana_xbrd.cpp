/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2016/12/21 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <string.h>

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"

#include "iana.h"
#include "iana_xbrd.h"
#if !defined(XU_HW)
#include "xbrd.h"
#include "xmbd.h"
#include "xminh.h"
#endif 
/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#if !defined(XU_HW)
#define			USE_XBRD
//#define			USE_XMBD
#define USE_XMINH
#endif

//#define TIMEKEEPALIVE_NORMAL				(1000 *1000)
//#define HUBRESET_NOMAL					(1000 * 1000)

#define TIMEKEEPALIVE_NORMAL				(5 * 1000)
#define HUBRESET_NOMAL					(300 * 1000)


//#define TIMEKEEPALIVE_NORMAL				(1000 * 1000)
//#define HUBRESET_NOMAL					(1000 * 1000)
//
//#define HUBRESETDURATION				(4*1000)
//#define HUBRESETDURATION				(2*1000)
//#define HUBRESETDURATION				(1000)
//#define HUBRESETDURATION				(500)
//#define HUBRESETDURATION				(200)
//#define HUBRESETDURATION				(300)
#define HUBRESETDURATION				(2*1000)
//#define HUBRESETDURATION				(50)
//#define HUBRESETDURATION				(2)
//#define HUBRESETDURATION				(0)

#if defined(_DEBUG)
#undef TIMEKEEPALIVE_NORMAL
#define TIMEKEEPALIVE_NORMAL				(1000 *1000)
#endif

#define IANA_XBOARD_WAIT	100

#define XBOARD_RUN
#define XBOARD_SENSORRESET_TICK 300

//#define TIMEOUT_CAM1	(1*1000)
//#define TIMEOUT_CAM2	(1*1000)
//#define TIMEOUT_XMINH	(1*1000)

////////#define TIMEOUT_CAM1	(5*1000)
//////////#define TIMEOUT_CAM2	(5*1000)
/////////#define TIMEOUT_XMINH	(5*1000)

//#define TIMEOUT_CAM1	(50*1000)
//#define TIMEOUT_CAM2	(50*1000)
//#define TIMEOUT_XMINH	(50*1000)

//#define TIMEOUT_CAM1	(10*1000)
//#define TIMEOUT_CAM2	(10*1000)
//#define TIMEOUT_XMINH	(10*1000)

#define TIMEOUT_CAM1	(30*1000)
#define TIMEOUT_CAM2	(30*1000)
#define TIMEOUT_XMINH	(30*1000)

#if defined(_DEBUG)
			/*
#undef TIMEOUT_CAM1
#undef TIMEOUT_CAM2
#undef TIMEOUT_XMINH

#define TIMEOUT_CAM1	(50*1000)
#define TIMEOUT_CAM2	(50*1000)
#define TIMEOUT_XMINH	(50*1000)
*/
#endif

//#define P3V2_IRLED_DEFAULT	110
#define P3V2_IRLED_DEFAULT	100

#define INIT_TRY_ITER	5

#define CHECKCOUNT		5
#define CHECK_PERIOD0	200
#define CHECK_PERIOD1	200			
#define CHECK_PERIOD_XMINIHUB	200		
#define CHECK_PERIOD_XMINIHUB_RGB	10


//#define CHECK_XMHSTATUS
#define XMHSTATUS_CAM1		0x0001
#define XMHSTATUS_CAM2		0x0002
#define XMHSTATUS_XMINH		0x0001

#define XBRD_CONNECTION_BAD_MAX	20

//#define RESETCAMSLEEP	200
#define RESETCAMSLEEP	3000

//#define DOOMSDAYITER	500
#define DOOMSDAYITER	200
//#define DOOMSDAYITER	200000

#define GOODGOOD	5
//#define GOODGOOD	20

#define TIMEKEEPALIVE_AFTERSTOP			(5 *1000)
//#define TIMEKEEPALIVE_AFTERSTOP			(1 *1000)
#define HUBRESET_AFTERSTOP				(10 * 1000)

//#define TIMEKEEPALIVE_AFTERSTOP			(60 *1000)
//#define HUBRESET_AFTERSTOP				(70 * 1000)

//#define TIMEKEEPALIVE_AFTERSTOP			(200)
//#define HUBRESET_AFTERSTOP				(300)

#define P3V2_IRLED_DEFAULT_CAM1	130				// 20201112, TEST.
#define P3V2_IRLED_DEFAULT_CAM2	110

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/
	
	
#if defined(USE_XBRD)
static XBRD_REQUEST 	s_XbrdRequest;
#endif
	
#if defined(USE_XMBD)
static XMBD_REQUEST 	s_XbrdRequest;
//#define XBOARD_TIMEOUT	100
#define XBOARD_TIMEOUT	2000
#endif

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/
extern U16 RGB565_ColorCode(				// RGB888 to RGB565 code. by ywchoi.
	U08 /*BYTE*/	red,
	U08 /*BYTE*/	green,
	U08 /*BYTE*/	blue
);

extern U32 iana_xminh_rgbled_rgb(iana_t *piana, U32 right0left1, U32 bcam1, U32 bcam2, U16 rgbcode1[], U16 rgbcode2[]);
/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/


void *iana_xboard_mainfunc(void *startcontext);

U32 iana_xboard_cameraonoff(iana_t *piana, U32 delaytick);
U32 iana_xboard_sensorreset(iana_t *piana, U32 delaytick);

U32 iana_xboard_sensorreset_xminh(
		iana_t *piana, 
		U32 delaytick);
U32 iana_xboard_cameraonoff_xminh(
		iana_t *piana, 
		U32 delaytick);
#if defined(USE_XMINH)	
I32 iana_xboard_sensor_init_xminh(				// 20200330
		iana_t *piana, 
		iana_xboard_t *pxbrd, 

		XMINH_CAMMODE cammode_cam1, 	// CAM1 (sidecam): Master
		XMINH_CAMMODE cammode_cam2, 	// CAM2 (centercam): Slave
		U32 bright_cam1,				// IRLED, CAM1, Sidecam
		U32 bright_cam2, 				// IRLED, CAM2, Centercam 
		U32 timeout_cam1,
		U32 timeout_cam2,
		U32 timeout_xminh);
#endif

U32 iana_xboard_rgbcode_read(HAND hiana, HAND hxbrd, U32 tablecode);
U32 iana_xboard_rgbcode_work(HAND hiana, HAND hxbrd);


/*----------------------------------------------------------------------------
 *	Description	: local functions
 -----------------------------------------------------------------------------*/

static U32 turn_onoff_camera(iana_xboard_t *pxbrd, U32 delaytick) 
{
	U32 res;

	if (pxbrd == NULL) {
		res = 0;
	} else {
		//XBRD_REQUEST		gstXbrdRequest;
#if defined(USE_XBRD)
		XBRD_STATUS			gstXbrdStatus;
#endif
#if defined(USE_XMBD)
		XMBD_STATUS			gstXbrdStatus;
#endif
		U32 i;

#if defined(USE_XBRD) || defined(USE_XMBD)
		s_XbrdRequest.dwCamStatus1 = 2;				// OFF
		s_XbrdRequest.dwCamStatus2 = 2;				// OFF
		s_XbrdRequest.dwCamLightStatus = 2;			// OFF
		s_XbrdRequest.dwIndicatorStatus = 1;			// ON
		//----
		// s_XbrdRequest.dwHubResetDuration = hubresetduration;	// [msec]
		s_XbrdRequest.dwCamStatus3 = 3;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED1 = 3;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED2 = 3;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED3 = 3;		// 1 for ON, 2 for OFF, other for no change
#endif
		for (i = 0; i < 2; i++) {
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #1  (%d)\n", i);
#if defined(USE_XBRD)
			if (XBRD_RET_OK == XBRD_Connect (pxbrd->hXbrd)) 
			{
				XBRD_RET	iRet;

				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #2  (%d)\n", i);
				iRet = XBRD_Control (pxbrd->hXbrd, &s_XbrdRequest, &gstXbrdStatus);

				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #3  (%d)\n", i);
				XBRD_Disconnect (pxbrd->hXbrd);
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #4  (%d)\n", i);
				break;
			}
#endif
#if defined(USE_XMBD)
			//if (XMBD_ERROR_NO_ERROR == XMBD_Connect (pxbrd->hXbrd)) 
			{
				XMBD_ERROR	iRet;

				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #2  (%d)\n", i);
				iRet = XMBD_Control (pxbrd->hXbrd, &s_XbrdRequest, &gstXbrdStatus, XBOARD_TIMEOUT);

				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #3  (%d)\n", i);
				//XMBD_Disconnect (pxbrd->hXbrd);
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #4  (%d)\n", i);
				//break;
			}
#endif
		}
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" Sleep(%d)\n", delaytick);
		cr_sleep(delaytick);
#if defined(USE_XBRD) || defined(USE_XMBD)
		s_XbrdRequest.dwCamStatus1 = 1;				// ON
		s_XbrdRequest.dwCamStatus2 = 1;				// ON
		s_XbrdRequest.dwCamLightStatus = 1;			// ON
		s_XbrdRequest.dwIndicatorStatus = 1;			// ON
		//----
		// s_XbrdRequest.dwHubResetDuration = hubresetduration;	// [msec]
		s_XbrdRequest.dwCamStatus3 = 3;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED1 = 3;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED2 = 3;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED3 = 3;		// 1 for ON, 2 for OFF, other for no change
#endif
		for (i = 0; i < 2; i++) {
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #5  (%d)\n", i);
#if defined(USE_XBRD)
			if (XBRD_RET_OK == XBRD_Connect (pxbrd->hXbrd)) {
				XBRD_RET	iRet;
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #6  (%d)\n", i);

				iRet = XBRD_Control (pxbrd->hXbrd, &s_XbrdRequest, &gstXbrdStatus);
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #7  (%d)\n", i);

				XBRD_Disconnect (pxbrd->hXbrd);
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #8  (%d)\n", i);

				break;
			}
#endif
#if defined(USE_XMBD)
			//if (XMBD_ERROR_NO_ERROR == XMBD_Connect (pxbrd->hXbrd)) 
			{
				XMBD_ERROR	iRet;
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #6  (%d)\n", i);

				iRet = XMBD_Control (pxbrd->hXbrd, &s_XbrdRequest, &gstXbrdStatus, XBOARD_TIMEOUT);
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #7  (%d)\n", i);

				//XMBD_Disconnect (pxbrd->hXbrd);
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #8  (%d)\n", i);

				//break;
			}
#endif
		}
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #9\n");
		res = 1;
	}

	return res;
}


static U32 turn_onoff_camera_xminh(
		iana_t *piana, 
		iana_xboard_t *pxbrd, 
		U32 delaytick)
{
#if defined(USE_XMINH)	

	XMINH_RET	iRet;
	U32 i;

	//--

	iRet = XMINH_Disconnect(pxbrd->hXbrd);
	iRet = XMINH_Connect(pxbrd->hXbrd);

#if 0
	iRet = XMINH_ResetCamera(						// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
			pxbrd->hXbrd,							// [IN] XMINH_Create 함수에 의해 생성된 핸들
			XMINH_SLAVE_ID_CAM1						// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
			);

	iRet = XMINH_ResetCamera(						// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
			pxbrd->hXbrd,							// [IN] XMINH_Create 함수에 의해 생성된 핸들
			XMINH_SLAVE_ID_CAM2						// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
			);

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" After reset cam..\n");
	cr_sleep(delaytick);

//	cr_sleep(1000*5);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" After reset cam SLEEP\n");
	// IRLED Traing 에 2초, 2초, 2초.... 최대 3회 try. 알아서 쉬세요.

#endif
	delaytick;
	iRet = (XMINH_RET)0;
	for (i = 0; i < INIT_TRY_ITER; i++) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" try iana_xboard_sensor_init_xminh()\n");
		iRet = (XMINH_RET) iana_xboard_sensor_init_xminh(
				piana,
				pxbrd,
				XMINH_CAMMODE_MASTER,			// CAM1 (Sidecam): Master
				XMINH_CAMMODE_SLAVE,			// CAM2 (Centercam): Slave
				P3V2_IRLED_DEFAULT,							// IRLED, CAM1, SideCam 
				P3V2_IRLED_DEFAULT,							// IRLED, CAM2, CenterCam
				TIMEOUT_CAM1,
				TIMEOUT_CAM2,
				TIMEOUT_XMINH);
		// exception code 11 이 날라올 수 있다. (Slave 가 응답 안하네..) -> SLEEP 더줘야 할 지도 모름.
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"     iana_xboard_sensor_init_xminh() res: %d\n", iRet);
		if (iRet == XMINH_RET_OK) {
			break;
		}
	}

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"     iRet: %d\n", iRet);
	return (I32)iRet;

#else
	return 0;
#endif
}



static U32 reset_sensor(iana_xboard_t *pxbrd, U32 delaytick)
{
	U32 res;

	cr_mutex_wait(pxbrd->hmutex, INFINITE);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++ %d\n", delaytick);
	if (pxbrd == NULL) {
		res = 0;
	} else {
		//XBRD_REQUEST		gstXbrdRequest;
#if defined(USE_XBRD)
		XBRD_STATUS			gstXbrdStatus;
#endif
#if defined(USE_XMBD)
		XMBD_STATUS			gstXbrdStatus;
#endif
		U32 i;

		//gstXbrdRequest.dwTimeKeepAlive = 10;
		//gstXbrdRequest.dwTimeHubReset =  60 * 1000;		// one minute

#if defined(USE_XBRD) || defined(USE_XMBD)
		s_XbrdRequest.dwTimeKeepAlive = 20;
		s_XbrdRequest.dwTimeHubReset =  10000*1000;

		s_XbrdRequest.dwCamStatus1 = 2;
		s_XbrdRequest.dwCamStatus2 = 2;
		s_XbrdRequest.dwCamLightStatus = 2;
		s_XbrdRequest.dwIndicatorStatus = 2;
		//----
		// s_XbrdRequest.dwHubResetDuration = hubresetduration;	// [msec]
		s_XbrdRequest.dwCamStatus3 = 3;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED1 = 3;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED2 = 3;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED3 = 3;		// 1 for ON, 2 for OFF, other for no change
#endif
		//for (i = 0; i < 2; i++) 
		for (i = 0; i < 1; i++) 
		{
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #1  (%d)\n", i);
#if defined(USE_XBRD)
			if (XBRD_RET_OK == XBRD_Connect (pxbrd->hXbrd)) {
				XBRD_RET	iRet;
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #2  (%d)\n", i);
				iRet = XBRD_Control (pxbrd->hXbrd, &s_XbrdRequest, &gstXbrdStatus);
				XBRD_Disconnect (pxbrd->hXbrd);
				break;
			}
#endif
#if defined(USE_XMBD)
			//if (XMBD_ERROR_NO_ERROR == XMBD_Connect (pxbrd->hXbrd)) 
			{
				XMBD_ERROR	iRet;
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #2  (%d)\n", i);
				iRet = XMBD_Control (pxbrd->hXbrd, &s_XbrdRequest, &gstXbrdStatus, XBOARD_TIMEOUT);
				//XMBD_Disconnect (pxbrd->hXbrd);
				// break;
			}
#endif
		}
		cr_sleep(delaytick);
		res = 1;
	}
	cr_mutex_release(pxbrd->hmutex);

	return res;
}


/*!
 ********************************************************************************
 *	@brief      Create iana module
 *
 *  @return		iana module handle
 *
 *	@author	    yhsuk
 *  @date       2015/12/20
 *******************************************************************************/
	
HAND iana_xboard_create(HAND hiana)
{
	HAND res;

	res = iana_xboard_create2(hiana, 0);			// Right.. 

	return res;
}


HAND iana_xboard_create2(HAND hiana, U32 right0left1)
{
	HAND res;
	U32 xboard_category;

	//--
	//if (hiana) {
	//	iana_t * piana;
	//	piana = (iana_t *)hiana;
	//	xboard_category = piana->xboard_category;
	//} else {
	//	xboard_category = XBOARD_CATEGORY_XMINI;
	//}

	xboard_category = XBOARD_CATEGORY_XMINI;
	res = iana_xboard_create3(hiana, right0left1, xboard_category);

	return res;
}


HAND iana_xboard_create3(HAND hiana, U32 right0left1, U32 xboard_category)
{
	iana_xboard_t *pxbrd;

	//---
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++\n");

	//xmbd_test();

	pxbrd = (iana_xboard_t *) malloc(sizeof(iana_xboard_t));
	if (pxbrd == NULL) {
		goto func_exit;
	}

	memset(pxbrd, 0, sizeof(iana_xboard_t));

	pxbrd->hmutex = cr_mutex_create();	
	pxbrd->category = xboard_category;			// 20200330

	if (xboard_category == XBOARD_CATEGORY_XMINIHUB) {
	} else /*if (xboard_category == XBOARD_CATEGORY_XMINI) */
	{	// Default..
#if defined(USE_XBRD)
		memset(&s_XbrdRequest, 0, sizeof(XBRD_REQUEST));
#endif
#if defined(USE_XMBD)
		memset(&s_XbrdRequest, 0, sizeof(XMBD_REQUEST));
#endif

#if defined(USE_XBRD) || defined(USE_XMBD)
		s_XbrdRequest.dwTimeKeepAlive 	=  TIMEKEEPALIVE_NORMAL;			// 
		s_XbrdRequest.dwTimeHubReset 	= HUBRESET_NOMAL;				// 
		s_XbrdRequest.dwCamStatus1 		= 1;
		s_XbrdRequest.dwCamStatus2 		= 1;
		s_XbrdRequest.dwCamLightStatus 	= 1;
		s_XbrdRequest.dwIndicatorStatus = 1;
		//--
		s_XbrdRequest.dwHubResetDuration = HUBRESETDURATION;	// [msec]
		s_XbrdRequest.dwCamStatus3 		= 1;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED1 		= 1;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED2 		= 1;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED3 		= 1;		// 1 for ON, 2 for OFF, other for no change
#endif		
	}

	//-------------
	pxbrd->right0left1 = right0left1;
	
	//------ Make manager module thread.
	{
		cr_thread_t *pt;		
		
		pt = (cr_thread_t *) malloc(sizeof(cr_thread_t));
		if (pt == NULL) {
			// TODO...WHAT?
			goto func_exit;
		}
		memset(pt, 0, sizeof(cr_thread_t));
		pxbrd->hth = (HAND) pt;					// Preserve Thread handle.
		pxbrd->hiana = (HAND) hiana;				// iana handle.

//__debugbreak();
		pt->ustate = CR_THREAD_STATE_NULL;
		pt->hevent = cr_event_create();
		pt->hthread = (cr_thread_t *) cr_thread_create(iana_xboard_mainfunc, (HAND)pxbrd);

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "hthread: %p \n", pt->hthread);
		if (pt->hthread == NULL) {
			goto func_exit;
		}

#if 1
		if (hiana) {
			iana_t * piana;
			piana = (iana_t *)hiana;
		//	if (piana->camsensor_category != CAMSENSOR_Z3&&piana->camsensor_category != CAMSENSOR_EYEXO) 
			if (
					xboard_category == XBOARD_CATEGORY_XMINI
					|| xboard_category == XBOARD_CATEGORY_XMINIHUB 
				)
			{
				U32 i;
				for (i = 0; i < IANA_XBOARD_WAIT; i++) {
					if (
							pt->ustate == CR_THREAD_STATE_RUN 
							|| pt->ustate == CR_THREAD_STATE_STOPPING) 
					{
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_INFO, "IANA xbrd thread create done!\n");
						break;
					}
					cr_sleep(10);
				}
			}
		}

		if ( pt->ustate == CR_THREAD_STATE_RUN || pt->ustate == CR_THREAD_STATE_STOPPING)  {
			//good.
		} else {
			// bad
		}
#endif
	}

	
func_exit:
	return (HAND) pxbrd;
}


/*!
 ********************************************************************************
 *	@brief      Delete IANA xbrd module
 *
 *  @param[in]	h
 *              IANA module handle
 *  @return		Terminate result. 
 *
 *	@author	    yhsuk
 *  @date       2012/12/20
 *******************************************************************************/
I32 iana_xboard_delete(HAND h) 
{
	I32 res;
	iana_xboard_t *pxbrd;
	cr_thread_t *pth;
	
	//---
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++ (h: %p)\n",  h);
	res = CR_OK;
	if (h) {
		pxbrd = (iana_xboard_t *) h;
		pth	= (cr_thread_t *) pxbrd->hth;
		
		if (pth != NULL) {
			pth->ustate = CR_THREAD_STATE_NEEDSTOP;		// You shall die...
			cr_event_set(pth->hevent);
			cr_thread_join(pth->hthread);					// Wait for termincation..
			cr_event_delete(pth->hevent);
			free(pth);
			pxbrd->hth = NULL;
		}

		cr_mutex_delete (pxbrd->hmutex);
		free(pxbrd);
	} else {
		// what!
	}

	res = CR_OK;
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"--\n");

	return res;
}





/*!
 ********************************************************************************
 *	@brief      IANA thead manager main function
 *
 *  @return		trivial..
 *
 *	@author	    yhsuk
 *  @date       2015/11/30
 *******************************************************************************/

void *iana_xboard_mainfunc(void *startcontext)
{
	//XBRD_RET	iRet;
#if defined(USE_XMINH)
	XMINH_RET	iRet;
#endif
	iana_xboard_t *pxbrd;
	iana_xbrd_rgb_t *pxrgb;
	iana_t *piana;	
	cr_thread_t *pt;
	
	volatile U32	loop;
	U32				u0;
	U32 	waittime;
	U32		goodcount;

	U32 category;
	U32 xbrd_connected;
	U32 xbrd_connection_bad;

	I32 xmhstatus;
	//--------------------------------------- 
	// Check handle of Sensor
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++\n");
	pxbrd = (iana_xboard_t *)startcontext;
	if (pxbrd == NULL) {
		// TODO... what?
		goto func_exit;
	}

	piana = (iana_t *)pxbrd->hiana;

	category = pxbrd->category;
	xbrd_connected = 0;
	xbrd_connection_bad = 0;

	if (category == XBOARD_CATEGORY_XMINI) {
#if defined(USE_XBRD)
		if (pxbrd->right0left1 == 0) {			// Right
			pxbrd->hXbrd = XBRD_Create ((unsigned char *)XBRD_RS_IP, XBRD_PORT);
		} else {								// Left
			pxbrd->hXbrd = XBRD_Create ((unsigned char *)XBRD_LS_IP, XBRD_PORT);
		}
#endif
#if defined(USE_XMBD)
		//pxbrd->hXbrd = XMBD_Create ((PCHAR)XBRD_IP, XBRD_PORT, XBOARD_TIMEOUT);
		if (pxbrd->right0left1 == 0) {			// Right
			pxbrd->hXbrd = XMBD_Create ((PCHAR)XBRD_IP, XBRD_RS_IP, XBOARD_TIMEOUT);
		} else {								// Left
			pxbrd->hXbrd = XMBD_Create ((PCHAR)XBRD_IP, XBRD_LS_IP, XBOARD_TIMEOUT);
		}
#endif
	} else if (category == XBOARD_CATEGORY_XMINIHUB) {
#if defined(USE_XMINH)	
		XMINH_CFG	sCfg;

		if (pxbrd->right0left1 == 0) {			// Right
			//wcscpy((char *)&sCfg.szIpAddress[0], XBRD_RS_IP);  
			wcscpy(&sCfg.szIpAddress[0], XMINH_RS_IP);  
			sCfg.wIpPort = XBRD_PORT;
			pxbrd->hXbrd = XMINH_Create(&sCfg);
		} else {								// Left
			//wcscpy((char *)&sCfg.szIpAddress[0], XBRD_LS_IP);  
			wcscpy(&sCfg.szIpAddress[0], XMINH_LS_IP);  
			sCfg.wIpPort = XBRD_PORT;
			pxbrd->hXbrd = XMINH_Create(&sCfg);
		}

#endif
	} else {
		// XXX
	}

	goodcount = 0;
	if (piana) {
		if (pxbrd->right0left1 == 0) {	// right
			piana->RS_hxboard = (HAND) pxbrd;
		} else {						// left
			piana->LS_hxboard = (HAND) pxbrd;
		}
	}

	if (category == XBOARD_CATEGORY_XMINI) {
#if defined(XBOARD_RUN)
//#define XBOARD_START_RESET
#if defined(XBOARD_START_RESET)
		iana_xboard_cameraonoff(piana, XBOARD_SENSORRESET_TICK);
#endif
#endif
	}

	if (piana) {
		piana->needresetcam = 0;			// CLEAR Reset Camera 
		piana->needresetsensor = 0;			// CLEAR Reset Sensor
		piana->resetcamtick = cr_gettickcount();;
		piana->resetsensortick = cr_gettickcount();;
	}

	//--------------------------------------- 
	// Change state..
	pt = (cr_thread_t *) pxbrd->hth;
	pt->ustate = CR_THREAD_STATE_INITED;

	loop = 1;

	pt->ustate = CR_THREAD_STATE_RUN;
	//---------------------------------------

	if (category == XBOARD_CATEGORY_XMINI) {
#if defined(_DEBUG)
#if 0
		s_XbrdRequest.dwTimeKeepAlive 	= (1000*1000);				// just TEN msec..
		s_XbrdRequest.dwTimeHubReset 	= (1000*1000);
		s_XbrdRequest.dwCamStatus1 		= 1;
		s_XbrdRequest.dwCamStatus2 		= 1;
		s_XbrdRequest.dwCamLightStatus 	= 1;
		s_XbrdRequest.dwIndicatorStatus = 1;
		//---
		s_XbrdRequest.dwHubResetDuration = HUBRESETDURATION;	// [msec]
		s_XbrdRequest.dwCamStatus3 		= 1;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED1 		= 1;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED2 		= 1;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED3 		= 1;		// 1 for ON, 2 for OFF, other for no change

		if (XBRD_RET_OK == XBRD_Connect (pxbrd->hXbrd)) {
			XBRD_STATUS			gstXbrdStatus;

			iRet = XBRD_Control (pxbrd->hXbrd, &s_XbrdRequest, &gstXbrdStatus);
			XBRD_Disconnect (pxbrd->hXbrd);
		}
#endif
#endif
	} else if (category == XBOARD_CATEGORY_XMINIHUB) {
#if defined(USE_XMINH)		
		cr_mutex_wait(pxbrd->hmutex, INFINITE);
		iRet = XMINH_Connect(pxbrd->hXbrd);
		if (XMINH_RET_OK == iRet) {
			xbrd_connected = 1;
			xbrd_connection_bad = 0;
		} else {
			xbrd_connected = 0;
		}
		cr_mutex_release(pxbrd->hmutex);
#endif
	}
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" Before Loop\n");

	if (category == XBOARD_CATEGORY_XMINIHUB) {
#if defined(USE_XMINH)			
		cr_mutex_wait(pxbrd->hmutex, INFINITE);
		if (xbrd_connected) {
			U32 i;

			iRet = XMINH_RET_ILLEGAL_RESPONSE;
			for (i = 0; i < INIT_TRY_ITER; i++) {
				iRet = (XMINH_RET) iana_xboard_sensor_init_xminh(
						piana,
						pxbrd,
						XMINH_CAMMODE_MASTER,			// CAM1 (Sidecam): Master
						XMINH_CAMMODE_SLAVE,			// CAM2 (Centercam): Slave
						P3V2_IRLED_DEFAULT_CAM1,							// IRLED, CAM1, SideCam 
						P3V2_IRLED_DEFAULT_CAM2,							// IRLED, CAM2, CenterCam
						TIMEOUT_CAM1,
						TIMEOUT_CAM2,
						TIMEOUT_XMINH);
				if (iRet == XMINH_RET_OK) {
					break;
				}
			}

			if (iRet != XMINH_RET_OK) {
				// ...
			}
		}
		cr_mutex_release(pxbrd->hmutex);
#endif		
	}

	pxrgb = &pxbrd->xrgb;
	pxrgb->active = 0;
	pxrgb->direction = 0;	 
	pxrgb->itercount = 0;	 
	pxrgb->current_iter = 0;
	pxrgb->linecount = 0;	 
	pxrgb->current_line = 0;

	pxrgb->runstamp = 0;		// run stamp. compare to runstamp_required
	pxrgb->runstamp_required = 0;	

	waittime = 50;
	while(loop) {
		if (pt->ustate == CR_THREAD_STATE_NEEDSTOP) {
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" I would be killed...\n");
			break;
		}

		if (piana != NULL && piana->needxbdcheck) {
			waittime = CHECK_PERIOD0;
		} else {
			waittime = CHECK_PERIOD1;
		}

		if (category == XBOARD_CATEGORY_XMINIHUB) {
			U32 res0;
			res0 = iana_xboard_rgbcode_work((HAND)piana, (HAND)pxbrd);
			if (res0 == 1) {
				waittime = CHECK_PERIOD_XMINIHUB_RGB;
			} else {
				waittime = CHECK_PERIOD_XMINIHUB;
			}
		}

		u0 = cr_event_wait (pt->hevent, waittime);
		//cr_sleep(10);
		if (pt->ustate == CR_THREAD_STATE_NEEDSTOP) {
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" I would be killed...\n");
			break;
		}

//#define FAKE_NONEEDXBOARD
#if defined(FAKE_NONEEDXBOARD)
		if (piana!= NULL) {
			if (loop < 0x10000) {

				piana->needxbdcheck = 0;
				cr_sleep(100);
				continue;
			}
		}
#endif

		if (category == XBOARD_CATEGORY_XMINIHUB) {
#if defined(USE_XMINH)				
			cr_mutex_wait(pxbrd->hmutex, INFINITE);
			if (xbrd_connected == 0) {
				//XMINH_RET					iRet;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" XMINH reconnect try.. at %d\n", __LINE__);
				XBRD_Disconnect (pxbrd->hXbrd);
				iRet = XMINH_Connect(pxbrd->hXbrd);
				if (XMINH_RET_OK == iRet) {
					U32 i;
					for (i = 0; i < INIT_TRY_ITER; i++) {
						iRet = (XMINH_RET) iana_xboard_sensor_init_xminh(
								piana,
								pxbrd,
								XMINH_CAMMODE_MASTER,			// CAM1 (Sidecam): Master
								XMINH_CAMMODE_SLAVE,			// CAM2 (Centercam): Slave
								P3V2_IRLED_DEFAULT_CAM1, // 110,							// IRLED, CAM1, SideCam 
								P3V2_IRLED_DEFAULT_CAM2, // 110,							// IRLED, CAM2, CenterCam
								TIMEOUT_CAM1,
								TIMEOUT_CAM2,
								TIMEOUT_XMINH);
						if (iRet == XMINH_RET_OK) {
							xbrd_connected = 1;
							xbrd_connection_bad = 0;
							break;
						}
					}
				} else {
					xbrd_connected = 0;
				}
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" XMINH reconnect RESULT: %d.. at %d\n", xbrd_connected, __LINE__);
			}
			cr_mutex_release(pxbrd->hmutex);

			if (xbrd_connected == 0) {
				continue;
			}
 
			cr_mutex_wait(pxbrd->hmutex, INFINITE);
			{
				U32 bad_detected;

				//--
				bad_detected = 0;
				xmhstatus = -999;
				iRet = 	XMINH_KickWatchdog(pxbrd->hXbrd, (PDWORD)&xmhstatus, XMINH_SLAVE_ID_CAM1);	// For XMINI-HUB
				if (iRet >= XMINH_RET_INVALID_HANDLE
#if defined(CHECK_XMHSTATUS)
					|| xmhstatus != XMHSTATUS_CAM1
#endif
					) {
					bad_detected = 1;
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" kickdog of CAM1: %d, status: %08x\n", iRet, xmhstatus);
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" connection bad detected at %d\n", __LINE__);
				} else {
					if (xbrd_connection_bad > 0) {
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" kickdog of CAM1: %d, status: %08x, xbrd_connection_bad: %d\n", iRet, xmhstatus, xbrd_connection_bad);
					}
				}

				xmhstatus = -999;
				iRet = 	XMINH_KickWatchdog(pxbrd->hXbrd, (PDWORD)&xmhstatus, XMINH_SLAVE_ID_CAM2);	// For XMINI-HUB
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" kickdog of CAM2: %d, status: %08x\n", iRet, xmhstatus);
				if (iRet >= XMINH_RET_INVALID_HANDLE 
#if defined(CHECK_XMHSTATUS)
					|| xmhstatus != XMHSTATUS_CAM2
#endif
					) {
					bad_detected = 1;
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" kickdog of CAM2: %d, status: %08x\n", iRet, xmhstatus);
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" connection bad detected at %d\n", __LINE__);
				} else {
					if (xbrd_connection_bad > 0) {
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" kickdog of CAM2: %d, status: %08x, xbrd_connection_bad: %d\n", iRet, xmhstatus, xbrd_connection_bad);
					}
				}

				xmhstatus = -999;
				iRet = 	XMINH_KickWatchdog(pxbrd->hXbrd, (PDWORD)&xmhstatus, XMINH_SLAVE_ID_XMINH);	// For XMINI-HUB
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" kickdog of XMINH: %d, status: %08x\n", iRet, xmhstatus);
				if (iRet >= XMINH_RET_INVALID_HANDLE 
#if defined(CHECK_XMHSTATUS)					
					|| xmhstatus != XMHSTATUS_XMINH
#endif					
					) {
					bad_detected = 1;
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" kickdog of XMINH: %d, status: %08x\n", iRet, xmhstatus);
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" connection bad detected at %d\n", __LINE__);
				} else {
					if (xbrd_connection_bad > 0) {
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" kickdog of XMINH: %d, status: %08x, xbrd_connection_bad: %d\n", iRet, xmhstatus, xbrd_connection_bad);
					}
				}


				if (bad_detected) {
					xbrd_connection_bad++;
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" xbrd_connection_bad: %d\n", xbrd_connection_bad);
				} else {
					xbrd_connection_bad = 0;
				}
			}
			cr_mutex_release(pxbrd->hmutex);

			if (xbrd_connection_bad > XBRD_CONNECTION_BAD_MAX) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" xbrd_connection_bad: %d at %d. SHOULD re-connect.\n", xbrd_connection_bad, __LINE__);
				xbrd_connected = 0;
				cr_mutex_wait(pxbrd->hmutex, INFINITE);
				XMINH_Disconnect(pxbrd->hXbrd);
				cr_mutex_release(pxbrd->hmutex);
				continue;
			}
#endif			
		}


		if (piana == NULL) {
			continue;
		}

		if (
				category == XBOARD_CATEGORY_NULL 
				//|| category == XBOARD_CATEGORY_XMINIHUB
		)
		{// fake checking.
			if (piana != NULL) {
				piana->needxbdcheck = 0;
				if (piana->needresetsensor) {				
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "XB #-1-1\n");  
					iana_xboard_sensorreset(piana, XBOARD_SENSORRESET_TICK);
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "XB #-1-2\n");
					piana->resetsensortick = cr_gettickcount();;
					piana->resetcamtick = cr_gettickcount();;
					piana->needresetsensor = 0;
					piana->needresetcam = 0;
				}
			}
			continue;
		}



#if defined(XBOARD_RUN)
		if (category == XBOARD_CATEGORY_XMINI) {
			if (piana) {
				if (piana->needresetsensor) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "XB #0-1\n");
					iana_xboard_sensorreset(piana, XBOARD_SENSORRESET_TICK);
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "XB #0-2\n");
					piana->resetsensortick = cr_gettickcount();;
					piana->resetcamtick = cr_gettickcount();;
					piana->needresetsensor = 0;
					piana->needresetcam = 0;
				}

				if (piana->needresetcam) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "XB #0-3\n");
					iana_xboard_cameraonoff(piana, XBOARD_SENSORRESET_TICK);
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "XB #0-4\n");
					cr_sleep(RESETCAMSLEEP);
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "XB #0-5\n");
					piana->resetcamtick = cr_gettickcount();;
					piana->needresetcam = 0;
				}
			}
		} else if (category == XBOARD_CATEGORY_XMINIHUB) {
			if (piana) {	
				if (piana->needresetsensor) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "XB #0-1\n");
					cr_mutex_wait(pxbrd->hmutex, INFINITE);
					iana_xboard_sensorreset_xminh(piana, XBOARD_SENSORRESET_TICK);
					cr_mutex_release(pxbrd->hmutex);
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "XB #0-2\n");
					piana->resetsensortick = cr_gettickcount();;
					piana->resetcamtick = cr_gettickcount();;
					piana->needresetsensor = 0;
					piana->needresetcam = 0;
				}

				if (piana->needresetcam) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "XB #0-3\n");
					cr_mutex_wait(pxbrd->hmutex, INFINITE);
					iana_xboard_cameraonoff_xminh(piana, XBOARD_SENSORRESET_TICK);
					cr_mutex_release(pxbrd->hmutex);
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "XB #0-4\n");
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "XB #0-5\n");
//					cr_sleep(RESETCAMSLEEP);
					piana->resetcamtick = cr_gettickcount();;
					piana->needresetcam = 0;
				}
			}

			if (piana) {
				piana->needxbdcheck = 0;			// TODO CHECK GOOD COUNT.. :P
			}

		}

		if (category == XBOARD_CATEGORY_XMINI) 
		{
#if defined(USE_XBRD)
			XBRD_STATUS			gstXbrdStatus;
#endif
#if defined(USE_XMBD)
			XMBD_STATUS			gstXbrdStatus;
#endif

			static int timekeepalive = TIMEKEEPALIVE_NORMAL;
			static int timehubreset = TIMEKEEPALIVE_NORMAL;
			static int hubresetduration = HUBRESETDURATION;
#if defined(USE_XBRD) || defined(USE_XMBD)
			s_XbrdRequest.dwTimeKeepAlive = timekeepalive; // TIMEKEEPALIVE_NORMAL;				// just one minute
			s_XbrdRequest.dwTimeHubReset =  timehubreset; // HUBRESET_NOMAL;				// just one minute
			s_XbrdRequest.dwCamStatus1 = 1;
			s_XbrdRequest.dwCamStatus2 = 1;
			s_XbrdRequest.dwCamLightStatus = 1;
			s_XbrdRequest.dwIndicatorStatus = 1;
			//--
			s_XbrdRequest.dwHubResetDuration = hubresetduration;	// [msec]
			s_XbrdRequest.dwCamStatus3 = 1;		// 1 for ON, 2 for OFF, other for no change
			s_XbrdRequest.dwCamIRLED1 = 1;		// 1 for ON, 2 for OFF, other for no change
			s_XbrdRequest.dwCamIRLED2 = 1;		// 1 for ON, 2 for OFF, other for no change
			s_XbrdRequest.dwCamIRLED3 = 1;		// 1 for ON, 2 for OFF, other for no change
#endif			
			{
				static int iii = 0;
				static int jjj = 0;
				int 	doomsdayiter;
				int		mode;

				doomsdayiter = DOOMSDAYITER;
				mode = 0;
				{
					FILE *fp;
					fp = cr_fopen("doomsday.txt", "r");
					if (fp) {
						fscanf(fp, "%d", &mode);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

						if (mode == 1) {
							fscanf(fp, "%d", &doomsdayiter);
						}
						fclose(fp);
					}
				}

				if (mode == 1) {
					if (!(++iii % doomsdayiter)) 
					//if (!(iii++ % doomsdayiter)) 
					{
						jjj++;
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n\n\n\n\n\n\n\n[%d]~~~~~~ XBoard Timeout RESET.. \n", jjj);
#if defined(USE_XBRD)
						s_XbrdRequest.dwTimeKeepAlive = 10; // TIMEKEEPALIVE_NORMAL;				// just one minute
						s_XbrdRequest.dwTimeHubReset =  timehubreset; // HUBRESET_NOMAL;				// just one minute
#endif
#if defined(USE_XMBD)
						s_XbrdRequest.dwTimeKeepAlive = 10; // TIMEKEEPALIVE_NORMAL;				// just one minute
						s_XbrdRequest.dwTimeHubReset =  timehubreset; // HUBRESET_NOMAL;				// just one minute
#endif
					}
				}
			}
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "XB #1\n");
#if defined(USE_XBRD)
			if (XBRD_RET_OK == XBRD_Connect (pxbrd->hXbrd)) 
#endif
#if defined(USE_XMBD)
			//if (XMBD_ERROR_NO_ERROR == XBRD_Connect (pxbrd->hXbrd)) 
#endif
			{
#if defined(USE_XBRD)
				XBRD_RET	ibRet;
				ibRet = XBRD_Control (pxbrd->hXbrd, &s_XbrdRequest, &gstXbrdStatus);
#endif
#if defined(USE_XMBD)

				XMBD_ERROR	ibRet;
				ibRet = XMBD_Control (pxbrd->hXbrd, &s_XbrdRequest, &gstXbrdStatus, XBOARD_TIMEOUT);
#endif

			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" XB #3\n");
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" XBRD_Control, ibRet: %d\n", ibRet);


#if defined(USE_XBRD)
				/////////cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "######################### XBoard CTRL. ret: %d %s\n", ibRet, ibRet == XBRD_RET_OK?"OK":"FAIL" );
				XBRD_Disconnect (pxbrd->hXbrd);
#endif
#if defined(USE_XMBD)
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "######################### XBoard CTRL. ret: %d %s\n", ibRet, ibRet == XMBD_ERROR_NO_ERROR?"OK":"FAIL" );
				//XBRD_Disconnect (pxbrd->hXbrd);
#endif

				goodcount++;
				if (piana == NULL) {
					piana->needxbdcheck = 0;
				} else {
					if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
						piana->needxbdcheck = 0;
					} else {
#if defined(USE_XBRD)
						if (ibRet == XBRD_RET_OK)
#endif
#if defined(USE_XMBD)
							if (ibRet == XMBD_ERROR_NO_ERROR)
#endif
							{
								if (goodcount > GOODGOOD) {
									if (piana != NULL) {
										piana->needxbdcheck = 0;
									}
								}
							} 
#if defined(USE_XMBD) || defined(USE_XBRD)
							else 
#endif
							{
								goodcount = 0;
							}
					}
				}
			} 
		}
#else
		if (piana) {
			piana->needresetsensor = 0;
			piana->needresetcam = 0;
		}
#endif

#if 0
		//
#define CHECKCOUNT		5
#define CHECK_PERIOD0	200
#define CHECK_PERIOD1	200			
		if (piana != NULL && piana->needxbdcheck) {
			waittime = CHECK_PERIOD0;
			u0 = cr_event_wait (pt->hevent, waittime);
		} else {
			waittime = CHECK_PERIOD1;
			u0 = cr_event_wait (pt->hevent, waittime);
		}
		if (pt->ustate == CR_THREAD_STATE_NEEDSTOP) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d \n", __LINE__);
			break;
		}
#endif

#if 0
		//
#define CHECKCOUNT		5
#define CHECK_PERIOD0	200
#define CHECK_PERIOD1	200			
		if (piana != NULL && piana->needxbdcheck) {
			waittime = CHECK_PERIOD0;
			u0 = cr_event_wait (pt->hevent, waittime);
		} else {
			waittime = CHECK_PERIOD1;
			u0 = cr_event_wait (pt->hevent, waittime);
		}
		if (pt->ustate == CR_THREAD_STATE_NEEDSTOP) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d \n", __LINE__);
			break;
		}
#endif
	}			// MAIN LOOP

	pt->ustate = CR_THREAD_STATE_STOPPING;
	//--------------------------------------- 
	if (category == XBOARD_CATEGORY_XMINI) {
#if defined(XBOARD_RUN)								
		//XBRD_REQUEST		gstXbrdRequest;
#if defined(USE_XBRD)
		XBRD_STATUS			gstXbrdStatus;
#endif
#if defined(USE_XMBD)
		XMBD_STATUS			gstXbrdStatus;
#endif
		U32 i;

#if defined(USE_XMBD) || defined(USE_XBRD)
		//s_XbrdRequest.dwTimeKeepAlive = (5*1000);				// just TEN msec..
		//s_XbrdRequest.dwTimeHubReset =  (12*1000);
		s_XbrdRequest.dwTimeKeepAlive 	= TIMEKEEPALIVE_AFTERSTOP;				// just TEN msec..
		s_XbrdRequest.dwTimeHubReset 	= HUBRESET_AFTERSTOP;
		s_XbrdRequest.dwCamStatus1 		= 1;
		s_XbrdRequest.dwCamStatus2 		= 1;
		s_XbrdRequest.dwCamLightStatus 	= 1;
		s_XbrdRequest.dwIndicatorStatus = 1;
		//---
		s_XbrdRequest.dwHubResetDuration = HUBRESETDURATION;	// [msec]
		s_XbrdRequest.dwCamStatus3 		= 1;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED1 		= 1;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED2 		= 1;		// 1 for ON, 2 for OFF, other for no change
		s_XbrdRequest.dwCamIRLED3 		= 1;		// 1 for ON, 2 for OFF, other for no change
#endif

		//		for (i = 0; i < 10; i++) 
		for (i = 0; i < 2; i++) 
		{
#if defined(USE_XBRD)
			if (XBRD_RET_OK == XBRD_Connect (pxbrd->hXbrd)) {
				XBRD_RET	ibRet;
				ibRet = XBRD_Control (pxbrd->hXbrd, &s_XbrdRequest, &gstXbrdStatus);
				XBRD_Disconnect (pxbrd->hXbrd);
				break;
			}
#endif
#if defined(USE_XMBD)
			//if (XMBD_ERROR_NO_ERROR == XMBD_Connect (pxbrd->hXbrd)) 
			{
				XMBD_ERROR	ibRet;
				ibRet = XMBD_Control (pxbrd->hXbrd, &s_XbrdRequest, &gstXbrdStatus, XBOARD_TIMEOUT);
				//	XMBD_Disconnect (pxbrd->hXbrd);
				//break;
			}
		//cr_sleep(100);
#endif
		}

#endif
#if defined(USE_XBRD)
		XBRD_Delete(pxbrd->hXbrd);
#endif
#if defined(USE_XMBD)
		XMBD_Delete(pxbrd->hXbrd);
#endif
	} else if (category == XBOARD_CATEGORY_XMINIHUB) {
#if defined(USE_XMINH)				
		cr_mutex_wait(pxbrd->hmutex, INFINITE);
#define RESET_AFTER
#if defined(RESET_AFTER)
		iRet = XMINH_ResetCamera(						// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
				pxbrd->hXbrd,							// [IN] XMINH_Create 함수에 의해 생성된 핸들
				XMINH_SLAVE_ID_CAM1						// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
				);

		iRet = XMINH_ResetCamera(						// [OUT] 성공 실패 및 그 원인 XMINH_RET 참조
				pxbrd->hXbrd,							// [IN] XMINH_Create 함수에 의해 생성된 핸들
				XMINH_SLAVE_ID_CAM2						// [IN] 슬레이브 식별변호, XMINH_SLAVE_ID 참조
				);

		iRet = XMINH_ResetXminh(pxbrd->hXbrd);
#endif

		XMINH_Disconnect(pxbrd->hXbrd);
		XMINH_Delete(pxbrd->hXbrd);
		cr_mutex_release(pxbrd->hmutex);
#endif		
	}

func_exit:
	return 0;
}


U32 iana_xboard_sensor_irled_xminh(iana_t *piana, HAND hxbrd, U32 cam1cam2, U32 ledbright)
{
#if defined(USE_XMINH)			
	XMINH_RET	iRet;
	iana_xboard_t *pxbrd;
	//--

	pxbrd = (iana_xboard_t *)hxbrd;


	if (cam1cam2 == 1) {
		iRet = XMINH_IRLED_Set(pxbrd->hXbrd, XMINH_SLAVE_ID_CAM1, (WORD)ledbright);
	} else if (cam1cam2 == 2) {
		iRet = XMINH_IRLED_Set(pxbrd->hXbrd, XMINH_SLAVE_ID_CAM2, (WORD)ledbright);
	} else {
		iRet = XMINH_RET_ILLEGAL_RESPONSE;
	}

	piana;

	return iRet;
#else
	return 0;
#endif
}


U32 iana_xboard_sensor_rgbled_xminh(HAND hiana, HAND hxbrd, U32 cam1cam2, U32 tablecode, U32 itercount, U32 direction)
{
#if defined(USE_XMINH)			
	XMINH_RET	iRet;
	iana_t * piana;
	iana_xboard_t *pxbrd;
	iana_xbrd_rgb_t *pxrgb;
	//--

	pxbrd = NULL;
	pxrgb = NULL;

	if (hiana == NULL) {
		pxbrd = (iana_xboard_t *)hxbrd;
	} else {
		piana = (iana_t *)hiana;	
		if (hxbrd == NULL) {
			pxbrd = (iana_xboard_t *)piana->hxboard;			// Current handed.. :P
		} else {
			pxbrd = (iana_xboard_t *)hxbrd;
		}
	}

	if (pxbrd == NULL) {
		iRet = XMINH_RET_INVALID_PARAMETER;
		goto func_exit;
	}

	pxrgb = &pxbrd->xrgb;

	if (tablecode >= 0 && tablecode < XBRD_RGB_TABLECODE_COUNT) {
		cr_mutex_wait(pxbrd->hmutex, INFINITE);
		pxrgb->tablecode = tablecode;
		pxrgb->itercount = itercount;
		pxrgb->direction = direction;
		pxrgb->active = 0;				// NOT YET..

		pxrgb->runstamp_required = pxrgb->runstamp + 1;
		cr_mutex_release(pxbrd->hmutex);
		iRet = XMINH_RET_OK;
	} else {
		iRet = XMINH_RET_INVALID_PARAMETER;
	}
	cam1cam2;

func_exit:
	return iRet;
#else
	return 0;
#endif
}


U32 iana_xboard_sensor_rgbled_xminh_inactive(HAND hiana, HAND hxbrd, U32 cam1cam2)
{
#if defined(USE_XMINH)			
	XMINH_RET	iRet;
	iana_t * piana;
	iana_xboard_t *pxbrd;
	iana_xbrd_rgb_t *pxrgb;
	//--

	pxbrd = NULL;
	pxrgb = NULL;

	if (hiana == NULL) {
		pxbrd = (iana_xboard_t *)hxbrd;
	} else {
		piana = (iana_t *)hiana;	
		if (hxbrd == NULL) {
			pxbrd = (iana_xboard_t *)piana->hxboard;			// Current handed sensor
		} else {
			pxbrd = (iana_xboard_t *)hxbrd;
		}
	}

	if (pxbrd == NULL) {
		iRet = XMINH_RET_INVALID_PARAMETER;
		goto func_exit;
	}

	pxrgb = &pxbrd->xrgb;

	cr_mutex_wait(pxbrd->hmutex, INFINITE);
	pxrgb->active = 0;				// NOT YET..
	cr_mutex_release(pxbrd->hmutex);
	iRet = XMINH_RET_OK;
	cam1cam2;
	
func_exit:
	return iRet;
#else
	return 0;
#endif
}







/*!
 ********************************************************************************
 *	@brief      IANA camera power reset 
 *
 *  @return		trivial..
 *
 *	@author	    yhsuk
 *  @date       2017/01/05
 *******************************************************************************/
U32 iana_xboard_cameraonoff(iana_t *piana, U32 delaytick)
{
	U32 res;
	iana_xboard_t *pxbrd;

	//--------------------------------------- 
	// Check handle of Sensor
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++ (%08x)\n", piana);
	if (piana == NULL) {
		res = 0;
		goto func_exit;
	}

	pxbrd = (iana_xboard_t *)piana->hxboard;
	if (pxbrd == NULL) {
		res = 0;
		goto func_exit;
	}
	res = turn_onoff_camera(pxbrd, delaytick); 

func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      IANA sensor reset 
 *
 *  @return		trivial..
 *
 *	@author	    yhsuk
 *  @date       2017/01/05
 *******************************************************************************/
U32 iana_xboard_sensorreset(iana_t *piana, U32 delaytick)
{
	U32 res;
	iana_xboard_t *pxbrd;

	//--------------------------------------- 
	// Check handle of Sensor
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++ (%08x)\n",  piana);
	if (piana == NULL) {
		res = 0;
		goto func_exit;
	}

	pxbrd = (iana_xboard_t *)piana->hxboard;
	if (pxbrd == NULL) {
		res = 0;
		goto func_exit;
	}
	res = reset_sensor(pxbrd, delaytick);

	iana_xboard_needbdcheck(piana);

func_exit:
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"-- (%08x)\n", (U32) res);
	return res;
}




#if defined(USE_XMINH)			

/////////////// XMINIHUB //////////////////////////////

I32 iana_xboard_sensor_init_xminh(				// 20200330
		iana_t *piana, 
		iana_xboard_t *pxbrd, 

		XMINH_CAMMODE cammode_cam1, 	// CAM1 (Sidecam): master
		XMINH_CAMMODE cammode_cam2, 	// CAM2 (Centercam): slave
		U32 bright_cam1, 				// IRLED, CAM1, SideCam 
		U32 bright_cam2,				// IRLED, CAM2, CenterCam
		U32 timeout_cam1,
		U32 timeout_cam2,
		U32 timeout_xminh)
{
	XMINH_RET	iRet;
	IRLED_RESCAN_STATUS			eIrledRescanStatus;
	//--
	
//#define SWAP_CAMMODE
#if defined(SWAP_CAMMODE)
	{
		XMINH_CAMMODE cammode_tmp;

		cammode_tmp = cammode_cam1;
		cammode_cam1 = cammode_cam2;
		cammode_cam2 = cammode_tmp;
	}

#endif
	// cam1, slave/master
	iRet = XMINH_CAMMODE_Set( pxbrd->hXbrd,			// [IN] XMINH_Create ?⑥닔???섑빐 ?앹꽦???몃뱾
							XMINH_SLAVE_ID_CAM1,
							cammode_cam1);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d res: %d\n", __LINE__, iRet);
	if (iRet != XMINH_RET_OK) { goto func_exit; }

	// cam1, Turn on.
	iRet = XMINH_ZCAM_PowerControl(pxbrd->hXbrd, XMINH_SLAVE_ID_CAM1, ZCAM_POWER_STATE_ON);	
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d res: %d\n", __LINE__, iRet);

	// cam2, slave/master
	iRet = XMINH_CAMMODE_Set( pxbrd->hXbrd,			// [IN] XMINH_Create ?⑥닔???섑빐 ?앹꽦???몃뱾
							XMINH_SLAVE_ID_CAM2,
							cammode_cam2);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d res: %d\n", __LINE__, iRet);
	if (iRet != XMINH_RET_OK) { goto func_exit; }
	// cam2, Turn on.
	iRet = XMINH_ZCAM_PowerControl(pxbrd->hXbrd, XMINH_SLAVE_ID_CAM2, ZCAM_POWER_STATE_ON);	
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d res: %d\n", __LINE__, iRet);
	

//	cr_sleep(1000);
	iRet = XMINH_SetXminhMode(
		pxbrd->hXbrd,				// [IN] XMINH_Create 함수에 의해 생성된 핸들
		XMINH_SYNC_MODE_NORMAL		// [IN] 카메라 싱크 모드, XMINH_SYNC_MODE 참조
	);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d res: %d\n", __LINE__, iRet);
	
	
	
	
	// cam1, check Rescan and set IRLED brightness
	iRet = XMINH_IRLED_IsRescanComplete(pxbrd->hXbrd, XMINH_SLAVE_ID_CAM1, &eIrledRescanStatus);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d res: %d\n", __LINE__, iRet);
	if (iRet != XMINH_RET_OK) { goto func_exit; }
	iRet = XMINH_IRLED_Set(pxbrd->hXbrd, XMINH_SLAVE_ID_CAM1, (WORD)bright_cam1);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d res: %d\n", __LINE__, iRet);

	// cam2, check Rescan and set IRLED brightness
	iRet = XMINH_IRLED_IsRescanComplete(pxbrd->hXbrd, XMINH_SLAVE_ID_CAM2, &eIrledRescanStatus);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d res: %d\n", __LINE__, iRet);
	if (iRet != XMINH_RET_OK) { goto func_exit; }

	iRet = XMINH_IRLED_Set(pxbrd->hXbrd, XMINH_SLAVE_ID_CAM2, (WORD)bright_cam2);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d res: %d\n", __LINE__, iRet);
	if (iRet != XMINH_RET_OK) { goto func_exit; }
#if defined(RESET_AFTER)
	// cam1, set Watchdog timer
	iRet = XMINH_SetWatchdogTimeoutValue(pxbrd->hXbrd,
							XMINH_SLAVE_ID_CAM1,
							(WORD)timeout_cam1);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d res: %d\n", __LINE__, iRet);
	iRet = XMINH_EnableWatchdog( pxbrd->hXbrd, XMINH_SLAVE_ID_CAM1);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d res: %d\n", __LINE__, iRet);
	if (iRet != XMINH_RET_OK) { goto func_exit; }

	// cam2, set Watchdog timer
	iRet = XMINH_SetWatchdogTimeoutValue(pxbrd->hXbrd,
							XMINH_SLAVE_ID_CAM2,
							(WORD)timeout_cam2);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d res: %d\n", __LINE__, iRet);
	iRet = XMINH_EnableWatchdog( pxbrd->hXbrd, XMINH_SLAVE_ID_CAM2);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d res: %d\n", __LINE__, iRet);
	if (iRet != XMINH_RET_OK) { goto func_exit; }

	// XMINH, set Watchdog timer
	iRet = XMINH_SetWatchdogTimeoutValue(pxbrd->hXbrd,
							XMINH_SLAVE_ID_XMINH,
							(WORD)timeout_xminh);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d res: %d\n", __LINE__, iRet);
	iRet = XMINH_EnableWatchdog( pxbrd->hXbrd, XMINH_SLAVE_ID_XMINH);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d res: %d\n", __LINE__, iRet);
	if (iRet != XMINH_RET_OK) { goto func_exit; }
#endif

	XMINH_EnableIWDG(pxbrd->hXbrd, XMINH_SLAVE_ID_CAM1);
	XMINH_EnableIWDG(pxbrd->hXbrd, XMINH_SLAVE_ID_CAM2);
	XMINH_EnableIWDG(pxbrd->hXbrd, XMINH_SLAVE_ID_XMINH);

	piana;
func_exit:
	return (I32)iRet;
}

#endif

//iana_xboard_sensorreset_xminh(piana, XBOARD_SENSORRESET_TICK);

U32 iana_xboard_sensorreset_xminh(
		iana_t *piana, 
		U32 delaytick)
{
	return iana_xboard_sensorreset(piana, delaytick);
}


U32 iana_xboard_cameraonoff_xminh(
		iana_t *piana, 
		U32 delaytick)
{
	U32 res;
	iana_xboard_t *pxbrd;

	//--------------------------------------- 
	// Check handle of Sensor
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++ (%08x)\n",  piana);
	if (piana == NULL) {
		res = 0;
		goto func_exit;
	}

	pxbrd = (iana_xboard_t *)piana->hxboard;
	if (pxbrd == NULL) {
		res = 0;
		goto func_exit;
	}
	res = turn_onoff_camera_xminh(piana, pxbrd, delaytick);

func_exit:

	return res;
}




I32 iana_xboard_needbdcheck(HAND hiana)
{
	I32 res;
	iana_t * piana;
	cr_thread_t *pt;
	iana_xboard_t *pxbrd;


	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++ (%08x)\n",  hiana);

	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}

	piana = (iana_t *)hiana;	
	piana->needxbdcheck = 1;

	if (piana->hxboard != NULL) {
		pxbrd = (iana_xboard_t *) piana->hxboard;
		pt = (cr_thread_t *) pxbrd->hth;
		if (pt->hevent) {
			cr_event_set(pt->hevent);
		}
	}

	res = 1;

func_exit:
	return res;

}

/*!
 ********************************************************************************
 *	@brief      IANA XMINI CPU ID
 *
 *  @param[in]	h
 *              IANA module handle
 *  @param[out]	pbuf
 *              CPUID, 16byte (U32 * 4)
 *  @return		1: GOOD, 0: BAD
 *
 *	@author	    yhsuk
 *  @date       2019/01/17
 *******************************************************************************/
U32 iana_xboard_cpusid_read(HAND hiana, U32 *pbuf)
{
	U32 res;
	iana_t * piana;
	iana_xboard_t *pxbrd;

	//--------------------------------------- 
	// Check handle of Sensor

	memset(pbuf, 0, sizeof(U32) * 4);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++ (%08x)\n",  hiana);
	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}

	piana = (iana_t *)hiana;	
	pxbrd = (iana_xboard_t *)piana->hxboard;


	if (piana->Right0Left1 == 0) {
		pxbrd = (iana_xboard_t *)piana->RS_hxboard;
	} else {
		pxbrd = (iana_xboard_t *)piana->LS_hxboard;
	}
	if (pxbrd == NULL) {
		res = 0;
		goto func_exit;
	}
#if defined(USE_XBRD)
	XBRD_Connect(pxbrd->hXbrd);
	res = XBRD_GetCPUID ( pxbrd->hXbrd, pbuf);
	XBRD_Disconnect (pxbrd->hXbrd);

	if (res == XBRD_RET_OK) {
		res = 1;
	} else {
		res = 0;
	}
#else
	res = 1;
#endif

func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      IANA XBoard Flash erase... 
 *
 *  @param[in]	h
 *              IANA module handle
 *  @param[in]	address
 *              Address of flash memory..   16bit ranage
 *  @param[in]	length
 *              Byte length					16bit. Max: 1024
 *  @return		1: GOOD, 0: BAD
 *
 *	@author	    yhsuk
 *  @date       2019/01/17
 *******************************************************************************/
U32 iana_xboard_flash_erase(HAND hiana, U32 addr, U32 length)
{
	U32 res;
#if defined(USE_XBRD)	
	iana_t * piana;
	iana_xboard_t *pxbrd;

	//--------------------------------------- 
	// Check handle of Sensor
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++ (%08x)\n", hiana);

	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}

	if (length > XBRD_TRANSFER_BIG_BUFFER_NETSIZE) {
		res = 0;
		goto func_exit;
	}

	piana = (iana_t *)hiana;	
	pxbrd = (iana_xboard_t *)piana->hxboard;

	if (piana->Right0Left1 == 0) {
		pxbrd = (iana_xboard_t *)piana->RS_hxboard;
	} else {
		pxbrd = (iana_xboard_t *)piana->LS_hxboard;
	}
	if (pxbrd == NULL) {
		res = 0;
		goto func_exit;
	}

	XBRD_Connect(pxbrd->hXbrd);
	res = XBRD_ErazeMemory (pxbrd->hXbrd, addr, length);
	XBRD_Disconnect (pxbrd->hXbrd);
	if (res == XBRD_RET_OK) {
		res = 1;
	} else {
		res = 0;
	}
func_exit:
#else
		res = 1;
#endif

	return res;
}

/*!
 ********************************************************************************
 *	@brief      IANA XBoard Flash read... 
 *
 *  @param[in]	h
 *              IANA module handle
 *  @param[out]	pbuf
 *              Read buffer memory  
 *  @param[in]	address
 *              Address of flash memory..   16bit ranage
 *  @param[in]	length
 *              Byte length					16bit.  Max: 1024
 *  @return		1: GOOD, 0: BAD
 *
 *	@author	    yhsuk
 *  @date       2019/01/17
 *******************************************************************************/
U32 iana_xboard_flash_read(HAND hiana, U08 *pbuf, U32 addr, U32 length)
{
	U32 res;
#if defined(USE_XBRD)	
	iana_t * piana;
	iana_xboard_t *pxbrd;

	//--------------------------------------- 
	// Check handle of Sensor
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++ (%08x)\n",  hiana);

	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}

	if (length > XBRD_TRANSFER_BIG_BUFFER_NETSIZE) {
		res = 0;
		goto func_exit;
	}

	piana = (iana_t *)hiana;	
	pxbrd = (iana_xboard_t *)piana->hxboard;

	if (piana->Right0Left1 == 0) {
		pxbrd = (iana_xboard_t *)piana->RS_hxboard;
	} else {
		pxbrd = (iana_xboard_t *)piana->LS_hxboard;
	}
	if (pxbrd == NULL) {
		res = 0;
		goto func_exit;
	}

	XBRD_Connect(pxbrd->hXbrd);
	res = XBRD_ReadMemory (pxbrd->hXbrd, pbuf, addr, length);
	XBRD_Disconnect (pxbrd->hXbrd);
	if (res == XBRD_RET_OK) {
		res = 1;
	} else {
		res = 0;
	}
func_exit:
#else
		res = 1;
#endif

	return res;
}

/*!
 ********************************************************************************
 *	@brief      IANA XBoard Flash write
 *
 *  @param[in]	h
 *              IANA module handle
 *  @param[out]	pbuf
 *              Write buffer memory  
 *  @param[out]	pwritebackbuf
 *              write back buffer memory. NULL: discard it.
 *  @param[in]	address
 *              Address of flash memory..   16bit ranage
 *  @param[in]	length
 *              Byte length					16bit.
 *  @return		1: GOOD, 0: BAD
 *
 *	@author	    yhsuk
 *  @date       2019/01/17
 *******************************************************************************/
U32 iana_xboard_flash_write(HAND hiana, U08 *pbuf, U08 *pwritebackbuf, U32 addr, U32 length)
{
	U32 res;
#if defined(USE_XBRD)	
	iana_t * piana;
	iana_xboard_t *pxbrd;

	//--------------------------------------- 
	// Check handle of Sensor
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++ (%08x)\n", hiana);

	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}

	if (length > XBRD_TRANSFER_BIG_BUFFER_NETSIZE) {
		res = 0;
		goto func_exit;
	}

	piana = (iana_t *)hiana;	
	pxbrd = (iana_xboard_t *)piana->hxboard;

	if (piana->Right0Left1 == 0) {
		pxbrd = (iana_xboard_t *)piana->RS_hxboard;
	} else {
		pxbrd = (iana_xboard_t *)piana->LS_hxboard;
	}
	if (pxbrd == NULL) {
		res = 0;
		goto func_exit;
	}

	XBRD_Connect(pxbrd->hXbrd);
	res = XBRD_WriteMemory (pxbrd->hXbrd, pbuf, pwritebackbuf, addr, length);
	XBRD_Disconnect (pxbrd->hXbrd);
	if (res == XBRD_RET_OK) {
		res = 1;
	} else {
		res = 0;
	}
func_exit:
#else
		res = 1;
#endif

	return res;
}


/*!
 ********************************************************************************
 *	@brief      IANA XBoard Flash write with erase.
 *
 *  @param[in]	h
 *              IANA module handle
 *  @param[out]	pbuf
 *              Write buffer memory  
 *  @param[out]	pwritebackbuf
 *              write back buffer memory. NULL: discard it.
 *  @param[in]	address
 *              Address of flash memory..   16bit ranage
 *  @param[in]	length
 *              Byte length					16bit.
 *  @return		1: GOOD, 0: BAD
 *
 *	@author	    yhsuk
 *  @date       2019/01/17
 *******************************************************************************/
U32 iana_xboard_flash_write_witherase(HAND hiana, U08 *pbuf, U08 *pwritebackbuf, U32 addr, U32 length)
{
	U32 res;
#if defined(USE_XBRD)	
	iana_t * piana;
	iana_xboard_t *pxbrd;

	//--------------------------------------- 
	// Check handle of Sensor
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++ (%08x)\n", hiana);

	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}

	if (length > XBRD_TRANSFER_BIG_BUFFER_NETSIZE) {
		res = 0;
		goto func_exit;
	}

	piana = (iana_t *)hiana;	
	pxbrd = (iana_xboard_t *)piana->hxboard;

	if (piana->Right0Left1 == 0) {
		pxbrd = (iana_xboard_t *)piana->RS_hxboard;
	} else {
		pxbrd = (iana_xboard_t *)piana->LS_hxboard;
	}
	if (pxbrd == NULL) {
		res = 0;
		goto func_exit;
	}

	XBRD_Connect(pxbrd->hXbrd);
	res = XBRD_ErazeMemory (pxbrd->hXbrd, addr, length);
	res = XBRD_WriteMemory (pxbrd->hXbrd, pbuf, pwritebackbuf, addr, length);
	XBRD_Disconnect (pxbrd->hXbrd);
	if (res == XBRD_RET_OK) {
		res = 1;
	} else {
		res = 0;
	}
func_exit:
#else
		res = 1;
#endif

	return res;
}


/////////// for test application ////////////////////////
U32 iana_xboard_sensorreset_(iana_t *piana, iana_xboard_t *pxbrd, U32 delaytick)
{
	U32 res;
	
	if(piana != NULL)
	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"piana is not used\n");
	}

	res = reset_sensor(pxbrd, delaytick);
	
	iana_xboard_needbdcheck(piana);
	
	return res;
}

U32 iana_xboard_cameraonoff_xminh_(
		iana_t *piana, 
		iana_xboard_t *pxbrd, 
		U32 delaytick)
{
	return turn_onoff_camera_xminh(piana, pxbrd, delaytick);
}


U32 iana_xboard_rgbcode_read(HAND hiana, HAND hxbrd, U32 tablecode)
{
	U32 res;
	iana_t * piana;
	iana_xboard_t *pxbrd;
	iana_xbrd_rgb_t *pxrgb;

	FILE *fp;
	char filename[1024];

	U32 code;
	U32 count;
	U32 i, j;
	//--
	if (hiana == NULL) {
		res = 0;
		goto func_exit;
	}

	piana = (iana_t *)hiana;	
	//pxbrd = (iana_xboard_t *)piana->hxboard;
	pxbrd = (iana_xboard_t *)hxbrd;
	pxrgb = &pxbrd->xrgb;

	if (tablecode >= 0 && tablecode < XBRD_RGB_TABLECODE_COUNT) {
		sprintf(filename, "CRRGBCODE\\rgbtable%d.txt", tablecode);

		fp = fopen(filename, "r");
		if (fp) {
			fscanf(fp, "%d", &count);
			if (count > 0 && count <= XBRD_RGB_LINE_COUNT) {
				for (i = 0; i < count; i++) {
					for (j = 0; j < 12; j++) {
						fscanf(fp, "%x", &code);
						pxrgb->r[i][j] = (U08) code;
					}
					fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
					for (j = 0; j < 12; j++) {
						fscanf(fp, "%x", &code);
						pxrgb->g[i][j] = (U08) code;
					}
					fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
					for (j = 0; j < 12; j++) {
						fscanf(fp, "%x", &code);
						pxrgb->b[i][j] = (U08) code;
					}
					fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
					//
					fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
				}
				pxrgb->linecount = count;
				pxrgb->current_line = 0;
				res = 1;		// GOOD.
			} else {
				res = 0;
			}
			fclose(fp);
		} else {
			res = 0;
		}

	} else {
		res = 0;
	}

func_exit:
	return res;
}


U32 iana_xboard_rgbcode_work(HAND hiana, HAND hxbrd)
{
	U32 res;
	iana_xboard_t *pxbrd;
	iana_xbrd_rgb_t *pxrgb;
	iana_t *piana;	
	U32 iter;
	U32 itercount;

	//----
	itercount = 0;

	
	piana = (iana_t *)hiana;	
	if (hiana == NULL) {
		pxbrd = (iana_xboard_t *)hxbrd;
	} else {
		if (hxbrd == NULL) {
			pxbrd = (iana_xboard_t *)piana->hxboard;			// Current handed.. :P
		} else {
			pxbrd = (iana_xboard_t *)hxbrd;
		}
	}

	if (pxbrd == NULL) {
		res = 0;
		goto func_exit;
	}
	
	pxrgb = &pxbrd->xrgb;
	
	if (pxrgb->runstamp != pxrgb->runstamp_required) {
		//--
		cr_mutex_wait(pxbrd->hmutex, INFINITE);
		res = iana_xboard_rgbcode_read(piana, pxbrd, pxrgb->tablecode);
		if (res) {	// good result
			if(pxrgb->itercount > 0 && pxrgb->linecount > 0) {
				pxrgb->current_iter = 0;
				if (pxrgb->direction == 0) {
					pxrgb->current_line = 0;
				} else {
					pxrgb->current_line = pxrgb->linecount - 1;
				}
				pxrgb->active = 1;			

			} else {
				pxrgb->active = 0;			// STOP.. :P
			}
		}
		pxrgb->runstamp = pxrgb->runstamp_required;
		cr_mutex_release(pxbrd->hmutex);
	}
	if (pxrgb->active) {
//#define RGBITER 13
#define RGBITER 27
		for (iter = 0; iter < RGBITER; iter++) {
			//			waittime = CHECK_PERIOD_XMINIHUB_RGB;
			U16 rgbcode1[12], rgbcode2[12];
			U32 cam1, cam2;
			U32 i;
			I32 cline;		// current line

			//--
			cr_sleep(10);
			cam1 = 1;
			cam2 = 1;

			cline = (I32)pxrgb->current_line;
			for (i = 0; i < 12; i++) {
				rgbcode1[i] = RGB565_ColorCode(pxrgb->r[cline][i], pxrgb->g[cline][i], pxrgb->b[cline][i]);
				rgbcode2[i] = RGB565_ColorCode(pxrgb->r[cline][i], pxrgb->g[cline][i], pxrgb->b[cline][i]);
			}
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d\n", __LINE__);
			iana_xminh_rgbled_rgb(piana, pxbrd->right0left1, cam1, cam2, rgbcode1, rgbcode2);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d\n", __LINE__);

			if (pxrgb->direction == 0) {		// 0: Normal
				cline++;
				if (cline >= (I32)pxrgb->linecount) {
					cline = 0;

					pxrgb->current_iter++;
				}
			} else if (pxrgb->direction == 1) {		// 1: Reverse
				cline--;
				if (cline < 0) {
					cline = pxrgb->linecount - 1;
					pxrgb->current_iter++;
				}
			} else {
				pxrgb->active = 0;   // what?
			}

			pxrgb->current_line = cline;

			itercount++;
			if (pxrgb->current_iter >= pxrgb->itercount) {
				pxrgb->active = 0;
			}
			if (pxrgb->active == 0) {
				break;
			}
		}
	}

	if (pxrgb->active == 0) {
		res = 0;
	} else {
		res = 1;
	}

func_exit:
	return res;
}



#if defined (__cplusplus)
}
#endif


