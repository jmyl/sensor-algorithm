/*!
*******************************************************************************
                                                                                
            CREATZ IANA club data using mark
                                                                                
  	@section copyright_notice COPYRIGHT NOTICE
  	Copyright (c) 2015 ~ 2019 by Creatz Inc. 
  	All Rights Reserved. \n
  	Do not duplicate without prior written consent of Creatz Inc.

*******************************************************************************
  	@section file_information FILE CREATION INFORMATION
	@file   iana_club_mark.cpp
	@brief  IANA
	@author Original: by yhsuk
	@date   2019/12/10 club data using mark

	@section checkin_information LATEST CHECK-IN INFORMATION
    $Rev$ 
    $Author$
    $Date$
*******************************************************************************/

/*
   For Left-handed

	double clubspeed_B;				: 
	double clubspeed_A;				:
	double clubpath;				:
	double clubfaceangle;			:

	double clubattackangle;			:

	double clubloftangle;			:
	double clublieangle;			:	..??  

	double clubfaceimpactLateral;	:	..??
	double clubfaceimpactVertical;	:
*/


/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <math.h>
#include <string.h>

#if defined(_WIN32)
#include <tchar.h>
#include <direct.h>
#endif
#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"
#include "cr_nanoxml.h"

#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#else
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#endif

#include "scamif.h"
#include "scamif_buffer.h"

#include "iana.h"
#include "iana_work.h"
#include "iana_implement.h"
#include "iana_cam.h"
#include "iana_cam_implement.h"
#include "iana_tool.h"
#include "iana_coordinate.h"
#include "cr_geometric.h"

#include "cr_regression.h"

#include "iana_club_mark.h"
#include "cr_ellipsecalc.h"
#include "iana_kalman.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define MINBARTHICK_	0.3 // 0.4
#define MAXBARTHICK_	1.0 // 0.6, 0.7

#define MINBARLEN_	0.5 // 1.0, 1.2
#define MAXBARLEN_	4 // 2.5, 3.3, 3.5, 

#define MINMBARTHICK_	0.02 // 0.05, 0.1, 0.3
#define MAXMBARTHICK_	1.5 // 1.0, 1.3

#define MINMBARLEN_		2.0 // 1.5, 1.0, 1.8, 3.0
#define MAXMBARLEN_		6.0

#define FEAUTRE_TYPE

#define USE_UPPERBAR_3D

#define SPEED_BAR2D_MULT	(1/1.03)	// (1/1.04)	

//#define TOPBAR_CLUBSPEED_MULT	(1.03) // 1.05
#define TOPBAR_CLUBSPEED_MULT_IRON	(1.055)
#define TOPBAR_CLUBSPEED_MULT_TEE	(1.03) // 1.025
#define BALLPOS_HEIGHT_TEE	0.03

#define CLUBFACEIMPACT_LATERAL_IRON_MAX	50
#define CLUBFACEIMPACT_LATERAL_TEE_MAX	60
#define CLUBFACEIMPACT_VERTICAL_IRON_MAX	30
#define CLUBFACEIMPACT_VERTICAL_TEE_MAX	35

#define CALC_BARDOT_DOT_2D_DT	0.001
#define CALC_BARDOT_DOT_2D_T0	(-0.005) // (-0.003)
#define CALC_BARDOT_DOT_2D_T1	(0.000)

#define CALC_BARDOT_DOT_3D_DT	0.001
#define CALC_BARDOT_DOT_3D_T0	(-0.003)
#define CALC_BARDOT_DOT_3D_T1	(0.000)

#define CALC_BARDOT_BAR_2D_DT	0.001
#define CALC_BARDOT_BAR_2D_T0	(-0.005)
#define CALC_BARDOT_BAR_2D_T1	(0.000) // (-0.003)

#define CALC_BARDOT_BAR_3D_DT	0.001
#define CALC_BARDOT_BAR_3D_T0	(-0.003)
#define CALC_BARDOT_BAR_3D_T1	(-0.001) // 0.000
	
#define BARDOT_EXTDATA_IMPACT	(-0.0003) // 0.0
#define BARDOT_EXTDATA_IMPACT0	(-0.005)
#define BARDOT_EXTDATA_IMPACT_D	(0.0005)

//#define BARLP_DY	(0.005)					// 2.8
//#define BARLP_DY	(0.010)
//#define BARLP_DY	(0.007)
//#define BARLP_DY	(0.002)					//  
#define BARLP_DY	0.0
//#define BARLP_DY	(0.001)					//  6.695685 

//#define BARDXDYLENGTH	0.1
#define BARDXDYLENGTH	0.2
//#define BARDXDYLENGTH	0.0

//#define CENTER_ZOFFSET	(-0.005)				// bellow 5mm
#define CENTER_ZOFFSET	(0.000)				

#define TOPBAR_EXTDATA_IMPACT	(-0.0005)
#define TOPBAR_EXTDATA_IMPACT0	(-0.005)
#define TOPBAR_EXTDATA_IMPACT_D	(0.0005)

#define DOTREGIONUPPER	0.02 // 0.012
#define DOTREGIONLOWER	0.025 // 0.012, 0.04

#define BARREGIONUPPER	0.02
#define BARREGIONLOWER	0.025

#define LABELING_MARGIN	3

#define XY_QUADREG_COUNT	4 // 3
#define XY_REG_COUNT		3
#define XY_QUADREG_R2		0.96 // 0.98
#define XY_QUADREG_ERR		0.005 // 0.001 ~ 0.005
#define XY_QUADREG_COUNT_TEST	3

#define MAXMA2	5000

#define REG_TIME	(-0.002)

#define GRAYLEVELS 256

#define BARDOT_NEO

#define LOWERDOT_CLUBSPEED_MULT	(1.025) // 1.03, 1.05
#define LOWERDOT_CLUBSPEED_MULT0	1.0

#define LOWERDOT_2D_CLUBSPEED_MULT	1.03

// #define DEBUG_CLUB_MARK

#if defined(_WIN32)
#pragma warning(disable:4456)
#endif
#if defined (__cplusplus)
extern "C" {
#endif


/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/
static double MINBARTHICK = MINBARTHICK_;
static double MAXBARTHICK = MAXBARTHICK_;

static double MINBARLEN = MINBARLEN_;
static double MAXBARLEN = MAXBARLEN_;

// 0: y vs t, x vs t    (x = a2 t^2 + a1 t + a0, ...) 
// 1: t vs y, x vs y    (x = a2 y^2 + a1 t + a0, ...)
// 2: t vs y, x vs t    (x = a2 t^2 + a1 t + a0, ...) 

static U32 s_usebufEdge = 0; // 1
static U32 s_featuretype = 0;			// 0: center, angle compensation
										// 1: end of club header

static U32 s_barmethod = 1;				// 0: Legacy, 1: free of bar position
static U32 s_use_vcmax = 1; // 0

static double s_dotregionupper	= DOTREGIONUPPER;
static double s_dotregionlower = DOTREGIONLOWER;

static double s_barregionupper = BARREGIONUPPER;
static double s_barregionlower = BARREGIONLOWER;
/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/
extern I32 iana_clubpath_cam_param_lite(iana_t *piana, U32 camid, U32 normalbulk);

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
static I32 RecognizeClubmark(iana_t *piana, U32 camid, clubmark_t *pcm);
static I32 GetClubmarkThreshold(
		iana_t *piana, U32 camid,
		clubmark_t *pcm,
		U08 *buf, 
		camimageinfo_t     	*pcii,
		point_t *pPp,
		double Pr
		);
static I32 EstimateBallPosition(iana_t *piana, U32 camid, U64 ts64, U32 index, U32 indexshot, point_t *pPp, double *pPr);
static I32 BinarizeClubmarkImage(
		iana_t *piana, U32 camid, 
		clubmark_t *pcm,
		U08 *buf,
		camif_buffer_info_t *pb,
		double xfact,
		double yfact,
		U32 multitude,

		U32 *pwidth2,
		U32 *pheight2,
		I32 *psx2,
		I32 *pwb2,
		U08 *bufOut,
		U08 *bufOutEdge
		, point_t *pPp,
		double Pr
		);

static I32 LabelingClubmark(
		iana_t *piana, U32 camid, 
		clubmark_t *pcm,
		U32 width,
		U32 height,
		I32 startx,
		I32 wb,
		U08 *bufLabel,
		U08 *bufBin,
		clubmark_seg_t	clm[]
		//imagesegment_t imgseg[],
		//rectObj_t rectObj[]
		);

static I32 ComputeData(iana_t *piana, clubmark_t *pcm);
static I32 ComputeData_Dot(iana_t *piana, clubmark_t *pcm, cr_vector_t *pclub2dvect, U32 *pvalid2d, cr_vector_t *pclub3dvect, U32 *pvalid3d);
static I32 ComputeData_Dot_2D(iana_t *piana, U32 camid, clubmark_t *pcm, cr_vector_t *pclubpath);
static I32 ComputeData_Dot_3D(iana_t *piana, clubmark_t *pcm, cr_vector_t *pclubpath);
static I32 ComputeData_Dot_P3dE(iana_t *piana, clubmark_t *pcm, double td, cr_point_t *p3d);
static I32 ComputeData_Bar(iana_t *piana, clubmark_t *pcm, cr_vector_t *pclub2dvect, U32 *pvalid2d, cr_vector_t *pclub3dvect, U32 *pvalid3d);
static I32 ComputeData_Bar_2D(iana_t *piana, U32 camid, clubmark_t *pcm, cr_vector_t *pclubpath);
static I32 ComputeData_Bar_3D(iana_t *piana, clubmark_t *pcm, cr_vector_t *pclubpath);
static I32 ComputeData_Bar_P3dE(iana_t *piana, clubmark_t *pcm, double td, cr_point_t *p3d);


static I32 ComputeData_Upperline_3Dot(iana_t *piana, clubmark_t *pcm);
static I32 ComputeData_Upperline_3Dot_Face(iana_t *piana, U32 camid, clubmark_t *pcm);


static I32 ComputeData_Extdata(
		iana_t *piana, clubmark_t *pcm,
		double *pclubfaceangle,
		double *pclubloftangle,
		double *pclublieangle,
		double *pclubfaceimpactLateral,
		double *pclubfaceimpactVertical);

static I32 ComputeData_Extdata2(
		iana_t *piana, clubmark_t *pcm,
		double *pclubfaceangle,
		double *pclubloftangle,
		double *pclublieangle,
		double *pclubfaceimpactLateral,
		double *pclubfaceimpactVertical);

static I32 ComputeData_Extdata_Index(
		iana_t *piana, clubmark_t *pcm,
		U32 camid,					// 0, 1...
		U32 index,					// index
		double *pclubfaceangle,
		double *pclubloftangle,
		double *pclublieangle,
		double *pclubfaceimpactLateral,
		double *pclubfaceimpactVertical);

static I32 ComputeData_Topbar_Extdata(
		iana_t *piana, clubmark_t *pcm,
		double *pclubfaceangle,
		double *pclubfaceimpactLateral);

static I32 ComputeData_Topbar_Extdata2(
		iana_t *piana, clubmark_t *pcm,
		double *pclubfaceangle,
		double *pclubfaceimpactLateral);

static I32 ComputeData_Topbar_Extdata_Index(
		iana_t *piana, clubmark_t *pcm,
		U32 camid,					// 0, 1...
		U32 index,					// index
		double *pclubfaceangle,
		double *pclubfaceimpactLateral);


static I32 GetClubmarkTimestamp(iana_t *piana, clubmark_t *pcm);

static I32 OpenClubmarkInfoXml(iana_t *piana);
static I32 CloseClubmarkInfoXml(iana_t *piana);
static I32 StoreClubmarkInfoXml(iana_t *piana);

static I32 GetFaceData(iana_t *piana, U32 camid, U32 onedotmode);



//--------------------------------------------------------------------------
static double calcpolyarea(double x[], double y[], U32 count);
//static U32 rectobjcalc( double x[], double y[], U32 count, rectObj_t rectO[]);
static U32 findrect(
		point_t p0[],
		point_t p1[],
		point_t p2[],
		point_t p3[],
		rectObj_t *pro);

static U32 isinside(
		point_t	  *pinP,			// 
		rectObj_t *pro);
static U32 rectpixelcount(				// pixel count in the rectangular
		U32 width,
		U08 *bufLabel,
		U08 *bufBin,
		clubmark_seg_t	*pclm
		);

static I32 SelectClubmark(
		iana_t *piana, U32 camid, 
		clubmark_t *pcm,
		U32 width,
		U32 height,
		double bx, 
		double by,
		double br,
		double xfact, double yfact,
		double l1cm, 
		U08 *bufBin,
		clubmark_seg_t	clm[]);

static I32 GetBarDotCount(
		iana_t *piana, 
		U32 camid, 
		clubmark_t *pcm,
		clubmark_seg_t	clm[],
		//clubmark_cam_t *pcmc,
		U32 index);
static I32 ExileClubmarkData(iana_t *piana, U32 camid, 
		clubmark_t *pcm,
		U32 width, U32 height, U32 allowbar, U32 allowdot);
static I32 RegressionClubmark(iana_t *piana, U32 camid, 
		clubmark_t *pcm
		);
static I32 RegressionUpperbar(iana_t *piana, U32 camid, clubmark_t *pcm);
//static I32 RegressionBar(iana_t *piana, U32 camid, clubmark_t *pcm);
static I32 RegressionMbar(iana_t *piana, U32 camid, clubmark_t *pcm);

static I32 RegressionLowerDot_3D(iana_t *piana, clubmark_t *pcm);		// 20200608
static I32 RegressionUpperbar_3D(iana_t *piana, clubmark_t *pcm);

static I32	DisplayClubmarkBarDot(iana_t *piana, U32 camid, 
		clubmark_t *pcm,
		U32 index0, U32 index1, U32 displaydot, U32 displaydotEst, U32 displaybar, U32 displaybarEst);

I32 getp3d(iana_t *piana, cr_point_t point[2], cr_point_t *p3d);
double quadregression_errcalc(double xx[], double yy[], U32 count, double ma[3]);

static I32 ClassifyClubmarkClass(
		clubmark_t *pcm,
		clubmark_seg_t	*pclm,
		U08 *pimg, 
		I32 segindex,
		I32 width, I32 height);

static I32 GetClubmarkRegion(iana_t *piana, U32 camid, clubmark_t *pcm);

static double calcRectArea(point_t vertex[4]);
static double calcTriArea(point_t vertex[3]);
static double calcDist(point_t *pvertex0, point_t *pvertex1);

//rename it. this does not calculate general club data.
static I32 calcclubdata(cr_vector_t *pclubvect, double *pclubspeed, double *pclubpathangle, double *pattackangle);
static I32 removeoutlier(double xx[], double yy[], U32 count, double xx0[], double yy0[], U32 *pnewcount, double maxdistance);
static I32 getmaxdistance(double xx[], double yy[], U32 count, U32 *pmaxdistindex, double *pmaxdistance);


// TODO: need refactoring these new functions!!!
I32 club_mark_revive_dot(iana_t *piana, U32 camid, clubmark_t *pcm);

I32 clubspeed_dot_3d_mult(iana_t *piana, clubmark_t *pcm, double *pmultvalue);
I32 clubspeed_dot_2d_mult(iana_t *piana, clubmark_t *pcm, double *pmultvalue);


/*----------------------------------------------------------------------------
 *	Description	: local functions 
 -----------------------------------------------------------------------------*/
static I32 RegressionDot(iana_t *piana, U32 camid, clubmark_t *pcm, CR_BOOL lowerDot) 
{
	I32 res;
	I32 index;
	I32 indexshot, index0, index1;	
	U32 count;

	double xfact, yfact;
	U32 offset_x, offset_y;
	clubmark_cam_t *pcmc;
	clubmark_feature_t	*pcft;

	I32 marksequencelen;

	//-----------------------------------

	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}
	
	pcmc = &pcm->cmc[camid];

	indexshot = pcmc->indexshot;
	index0 = pcmc->index0;
	index1 = pcmc->index1;
	xfact = pcmc->xfact;
	yfact = pcmc->yfact;

	//------------------------------
	{ 	// Regression DOT..		
		double tt[MARKSEQUENCELEN], xx[MARKSEQUENCELEN], yy[MARKSEQUENCELEN];

		// Check Dot position
		pcmc->dotLp.mavalid = 0;
		for (index = 0; index < marksequencelen; index++) {
			pcmc->dotLp.valid[index] = 0;
		}
		count = 0;

		if(lowerDot == CR_FALSE) 	{
			U32 i;
			rectObj_t 		*pro;
			clubmark_dot_t *pcmd;
			
			for (index = index0; index <= indexshot; index++) {
				point_t Pb;
				double Pr;
				double x0, y0;

				//--
				pcft = &pcmc->cft[index];
				pcmd = &pcft->cmd;
				
				offset_x = pcft->offset_x; offset_y = pcft->offset_y;
				if (pcmd->count) {
					for (i = 0; i < pcmd->count; i++) {
						point_t Pp, *pLp;
						if (pcmd->clm[i].state == CLUBMARK_DOT) {
							memcpy(&Pb, &pcft->Pp, sizeof(point_t));
							Pr = pcft->Pr;

							pro = &pcmd->clm[i].rectObj;

							x0 = pro->p[0].x + pro->p[1].x + pro->p[2].x + pro->p[3].x;
							y0 = pro->p[0].y + pro->p[1].y + pro->p[2].y + pro->p[3].y;

							Pp.x = x0/(4.0 * xfact) + offset_x; 
	                        Pp.y = y0/(4.0 * yfact) + offset_y;

							pcmc->dotLp.valid[index] = 1;
							pLp = &pcmc->dotLp.Lp[index];
							iana_P2L(piana, camid, &Pp, pLp, 0);

							tt[count] = pcft->tsdiffd;
							xx[count] = pLp->x;
							yy[count] = pLp->y;

							count++;
							break;
						}
					}
				}
			}
		}
		else {	/* lowerDot == CR_TRUE */
			for (index = index0; index <= indexshot; index++) 
			{
				clubmark_face_t *pcfc;

				//--
				//pcmd = &pcmc->cmd[index];
				pcft = &pcmc->cft[index];
				pcfc = &pcmc->face[index];
				
				//offset_x = pcmd->offset_x; offset_y = pcmd->offset_y;
				offset_x = pcft->offset_x; offset_y = pcft->offset_y;

				if (pcfc->lowerdot.valid)
				{
					tt[count] = pcft->tsdiffd;
					xx[count] = pcfc->lowerdot.cdotL.x;
					yy[count] = pcfc->lowerdot.cdotL.y;

#define LOWERDOT_MAXT		(-0.0003) // (-0.001), (-0.0005)
					if (tt[count] < LOWERDOT_MAXT) {
						count++;
					}
				}
			}
		}		


		{
			U32 mavalid;
			double xma[3], yma[3];
			double xr2, yr2;

			//--
			xma[2] = xma[1] = xma[0] = 0;
			yma[2] = yma[1] = yma[0] = 0;

			xr2 = 0; yr2 = 0;

			mavalid = 0;
			if (count >= XY_QUADREG_COUNT_TEST) {
				if(lowerDot == CR_FALSE) 	{
					caw_quadraticregression(tt, xx, count, &xma[2], &xma[1], &xma[0], &xr2);
					caw_quadraticregression(tt, yy, count, &yma[2], &yma[1], &yma[0], &yr2);
				} 
				else {
					double xx0[MARKSEQUENCELEN], yy0[MARKSEQUENCELEN];
					U32 newcount;
					double maxdistance;

					//---

#define MAXDISTANCE_LOWERDOT	(0.004)
					maxdistance = MAXDISTANCE_LOWERDOT;
					removeoutlier(tt, xx, count, xx0, yy0, &newcount, maxdistance);
					caw_quadraticregression(xx0, yy0, newcount, &xma[2], &xma[1], &xma[0], &xr2);

					maxdistance = MAXDISTANCE_LOWERDOT;
					removeoutlier(tt, yy, count, xx0, yy0, &newcount, maxdistance);
					caw_quadraticregression(xx0, yy0, newcount, &yma[2], &yma[1], &yma[0], &yr2);
				}

#ifdef DEBUG_CLUB_MARK
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                    "[%d] QUAD regression xma: %lf %lf %lf, %lf   yma: %lf %lf %lf, %lf\n",
					camid, 
					xma[2], xma[1], xma[0], xr2,
					yma[2], yma[1], yma[0], yr2);
#endif
				xr2 = quadregression_errcalc(tt, xx, count, &xma[0]);
				yr2 = quadregression_errcalc(tt, yy, count, &yma[0]);
                //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #1 mean square Err: %lf %lf \n", xr2, yr2);

				if (xr2 > XY_QUADREG_ERR || yr2 > XY_QUADREG_ERR) {
					mavalid = 0;
				} else {
					mavalid = 1;
				}

				if ((xma[2] < -MAXMA2 || xma[2] > MAXMA2) || 
                    (yma[2] < -MAXMA2 || yma[2] > MAXMA2)) {
					mavalid = 0;
				}
			} 

			if (mavalid == 0) {
				if (count >= XY_REG_COUNT) {
					xma[2] = 0;
					cr_regression2(tt, xx, count, &xma[1], &xma[0], &xr2);
					yma[2] = 0;
					cr_regression2(tt, yy, count, &yma[1], &yma[0], &yr2);

					xr2 = quadregression_errcalc(tt, xx, count, &xma[0]);
					yr2 = quadregression_errcalc(tt, yy, count, &yma[0]);
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #2 mean square Err: %lf %lf \n", xr2, yr2);

					if (xr2 > XY_QUADREG_ERR || yr2 > XY_QUADREG_ERR) {
						mavalid = 0;
					} else {
						mavalid = 1;
					}
				} else {
					mavalid = 0;
				}
			} 
			pcmc->dotLp.mavalid = mavalid;
			memcpy(&pcmc->dotLp.xma[0], &xma[0], sizeof(double)*3);
			memcpy(&pcmc->dotLp.yma[0], &yma[0], sizeof(double)*3);

#ifdef DEBUG_CLUB_MARK
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                "[%d] regression valid: %d   xma: %lf %lf %lf, %lf   yma: %lf %lf %lf, %lf\n",
                camid, mavalid,
                xma[2], xma[1], xma[0], xr2,
                yma[2], yma[1], yma[0], yr2);
#endif

			for (index = 0; index < marksequencelen; index++) {
				double td;
				U32 valid;
				double x0, y0;
				double x0_, y0_;
				double ex_, ey_;
				double xma[3], yma[3];

				//--
				pcft = &pcmc->cft[index];
				td = pcft->tsdiffd;

				valid = pcmc->dotLp.valid[index];
				if (valid) {
					x0 = pcmc->dotLp.Lp[index].x;
					y0 = pcmc->dotLp.Lp[index].y;
				} else {
					x0 = -1;
					y0 = -1;
				}

				memcpy(&xma[0], &pcmc->dotLp.xma[0], sizeof(double)*3);
				x0_ = xma[2]*td*td + xma[1]*td + xma[0];
				memcpy(&yma[0], &pcmc->dotLp.yma[0], sizeof(double)*3);
				y0_ = yma[2]*td*td + yma[1]*td + yma[0];
				if (valid) {
					ex_ = x0_ - x0;
					ey_ = y0_ - y0;
				} else {
					ex_ = 0;
					ey_ = 0;
				}

#ifdef DEBUG_CLUB_MARK
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                    "[%d] regression result: %2d %10lf  x_, y_: %10lf  %10lf   (%d:  x, y: %10lf %10lf,  %10lf %10lf)\n",  
                    camid, index, td,
                    x0_, y0_,
                    valid,
                    x0, y0, ex_, ey_);
#endif
			}
		}
	}
	UNUSED(piana);
	UNUSED(camid);

	res = 1;
	return res;
}

/********************************************************************************/
HAND iana_club_mark_create(HAND hiana)
{
	HAND h;

	// 20200106.. 
	h = (HAND) malloc(sizeof(clubmark_t));

	UNUSED(hiana);

	return h;
}


I32 iana_club_mark_clear(HAND hcm)
{
	I32 res;

	if (hcm) {
		memset(hcm, 0, sizeof(clubmark_t));
	}
	res = 1;
	return res;
}

I32 iana_club_mark_delete(HAND hcm)
{
	I32 res;

	if (hcm) { // 20200106.. 
		free(hcm);
	}

	res = 1;
	return res;
}
/********************************************************************************/


/*!
 ********************************************************************************
 *	@brief      club data using mark
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2019/1210
 *******************************************************************************/
I32 iana_club_mark(iana_t *piana, U32 clubcalc_method)
{
	I32 res;
	I32 indexshot, index0, index1;
	I32 itmp;
	clubmark_t *pcm;


	if (clubcalc_method != CLUBCALC_CLUB_MARK_BARDOT && clubcalc_method != CLUBCALC_CLUB_MARK_TOPBAR)
	{
		res = 0;
		goto func_exit;
	}

#if defined(FEAUTRE_TYPE)
	{
		FILE *fp;
		U32 mode;
		U32 featuretype;

		int len = PATHBUFLEN;
		CHR wfilename[PATHBUFLEN];
		char filename[PATHBUFLEN];

#if defined(_WIN32)						
		cr_sprintf(wfilename, _T("%s%s\\marktype.txt"), piana->szDrive, piana->szDir);
#else
		cr_sprintf(wfilename, _T("%s%s/marktype.txt"), piana->szDrive, piana->szDir);
#endif
		OSAL_STR_ConvertUnicode2MultiByte(wfilename, filename, len);

		fp = cr_fopen(filename, "r");
		if (fp != NULL) {
			fscanf(fp, "%d", &mode);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
			if (mode != 0) {
				I32 readres;
				readres = fscanf(fp, "%d", &featuretype);
				if (readres == 1) {
					s_featuretype = featuretype;
				}
			}
			cr_fclose(fp);
		}
	}
#endif

    // get image
	iana_clubpath_cam_param_lite(piana, 0, NORMALBULK_NORMAL);	
	iana_clubpath_cam_param_lite(piana, 1, NORMALBULK_NORMAL);	

	if (clubcalc_method == CLUBCALC_CLUB_MARK_BARDOT) {
		MINBARTHICK = MINBARTHICK_;
		MAXBARTHICK = MAXBARTHICK_;

		MINBARLEN = MINBARLEN_;
		MAXBARLEN = MAXBARLEN_;
	} else if (clubcalc_method == CLUBCALC_CLUB_MARK_TOPBAR) {
		MINBARTHICK = MINMBARTHICK_;
		MAXBARTHICK = MAXMBARTHICK_;

		MINBARLEN = MINMBARLEN_;
		MAXBARLEN = MAXMBARLEN_;
	}

	if (piana->hcm == NULL) { 
		piana->hcm = iana_club_mark_create((HAND) piana);
		if (piana->hcm == NULL) {		// WHAT?
			res = 0;
			goto func_exit;
		}
	}

	if (piana->opmode == IANA_OPMODE_FILE) {
		OpenClubmarkInfoXml(piana);
	}

	iana_club_mark_clear(piana->hcm);
	pcm = (clubmark_t *)piana->hcm;

	pcm->clubcalc_method = clubcalc_method;

	
	// 0) get timestamp and time difference
	{
		U32 camid;
#define CLUBMARK0_BEFORESHOT_COUNT	8 // 1
#define CLUBMARK0_AFTERSHOT_COUNT	0 // 2
		//--

		for (camid = 0; camid < NUM_CAM; camid++) {
			indexshot = piana->bulkshotindex[camid];
			itmp = indexshot - CLUBMARK0_BEFORESHOT_COUNT;
			if (itmp < 0) {
				itmp = 0;
			}
			index0 = (U32)itmp;
			index1 = (U32) indexshot + CLUBMARK0_AFTERSHOT_COUNT;

			pcm->cmc[camid].indexshot = indexshot;
			pcm->cmc[camid].index0 = index0;
			pcm->cmc[camid].index1 = index1;
			//pcm->cmc[camid].normalbulk = NORMALBULK_BULK;
			//--
		}
		GetClubmarkTimestamp(piana, pcm);
	}

	// 1) get marking of cam0
	if (clubcalc_method == CLUBCALC_CLUB_MARK_BARDOT) {
		pcm->cmc[0].normalbulk = NORMALBULK_BULK;
		pcm->cmc[1].normalbulk = NORMALBULK_BULK;

        //image processing
		RecognizeClubmark(piana, 0, pcm);
		RecognizeClubmark(piana, 1, pcm);

        //regression first
		RegressionLowerDot_3D(piana, pcm);		// 20200608
		res = RegressionUpperbar_3D(piana, pcm);

		if (res == 0) { // if regression failed
			club_mark_revive_dot(piana, 0, pcm);
			club_mark_revive_dot(piana, 1, pcm);

			res = ComputeData_Upperline_3Dot(piana, pcm);

			if (res != 0) {
				RegressionUpperbar(piana, 0, pcm);
				RegressionUpperbar(piana, 1, pcm);
				RegressionUpperbar_3D(piana, pcm);
			}
		}

		ComputeData(piana, pcm);

	} else if (clubcalc_method == CLUBCALC_CLUB_MARK_TOPBAR) {
		pcm->cmc[0].normalbulk = NORMALBULK_BULK;
		pcm->cmc[1].normalbulk = NORMALBULK_BULK;

        //image processing
		RecognizeClubmark(piana, 0, pcm);
		RecognizeClubmark(piana, 1, pcm);

		RegressionLowerDot_3D(piana, pcm);
		RegressionUpperbar_3D(piana, pcm);

		ComputeData(piana, pcm);
	}

	if (piana->opmode == IANA_OPMODE_FILE) {
		StoreClubmarkInfoXml(piana);
		CloseClubmarkInfoXml(piana);
	}
	res = 1;
func_exit:
	return res;
}

static U32 VerifyDot_3D(cr_vector_t *pClubvect_dot_3d, cr_vector_t clubvect_dot_2d[2])
{
	double norm_dot_3d;
	double norm_dot_2d[2];
	double normdiff_max;
	double dtmp;
	U32 valid_dot_3d = 1;
	// check clubvect_dot_3d with clubvect_dot_2d[0] and [1]
	// 1) check norm difference
	// 2) check angle on the XY-plane

	norm_dot_3d = cr_vector_norm(pClubvect_dot_3d);
	norm_dot_2d[0] = cr_vector_norm(&clubvect_dot_2d[0]);
	norm_dot_2d[1] = cr_vector_norm(&clubvect_dot_2d[1]);

	normdiff_max = norm_dot_3d - norm_dot_2d[0]; 
	normdiff_max = fabs(normdiff_max);
			
	dtmp = norm_dot_2d[0] - norm_dot_2d[1]; 
	dtmp = fabs(dtmp);
			
	if (normdiff_max < dtmp) { 
		normdiff_max = dtmp;
	}
			
	dtmp = norm_dot_2d[1] - norm_dot_3d; 
	dtmp = fabs(dtmp);
			
	if (normdiff_max < dtmp) { 
		normdiff_max = dtmp;
	}
			
	if (normdiff_max > 999) {
		valid_dot_3d = 0;
	} else {
		cr_vector_t cv_dot_3d_2d;
		double angle_dot_3d_2d0;
		double angle_dot_2d0_2d1;
		double angle_dot_2d1_3d;
		double angle_max;

		cv_dot_3d_2d.x = pClubvect_dot_3d->x; 
		cv_dot_3d_2d.y = pClubvect_dot_3d->y; 
		cv_dot_3d_2d.z = 0;

		angle_dot_3d_2d0 	= cr_vector_angle(&cv_dot_3d_2d, &clubvect_dot_2d[0]);
		angle_dot_2d0_2d1 	= cr_vector_angle(&clubvect_dot_2d[0], &clubvect_dot_2d[1]);
		angle_dot_2d1_3d 	= cr_vector_angle(&clubvect_dot_2d[1], &cv_dot_3d_2d); 

		angle_max 			= angle_dot_3d_2d0;
		if (angle_max < angle_dot_2d0_2d1) { 
			angle_max = angle_dot_2d0_2d1; 
		}
		if (angle_max < angle_dot_2d1_3d) { 
			angle_max = angle_dot_2d1_3d; 
		}

#define ANGLEDIFF_DOT_MAX	3.0
		if (angle_max > DEGREE2RADIAN(ANGLEDIFF_DOT_MAX)) {
			valid_dot_3d = 0;
		}
	}

	return valid_dot_3d;
}



static U32 VerifyBar_3D(cr_vector_t *pClubvect_bar_3d, cr_vector_t clubvect_bar_2d[2], clubmark_t *pcm)
{
	double norm_bar_3d;
	double norm_bar_2d[2];
	double normdiff_max;
	double dtmp;
	U32 valid_bar_3d = 1;	
	// check clubvect_bar_3d with clubvect_bar_2d[0] and [1]
	// 1) check norm difference
	// 2) check angle on the XY-plane

	norm_bar_3d = cr_vector_norm(pClubvect_bar_3d);
	norm_bar_2d[0] = cr_vector_norm(&clubvect_bar_2d[0]);
	norm_bar_2d[1] = cr_vector_norm(&clubvect_bar_2d[1]);

	normdiff_max = norm_bar_3d - norm_bar_2d[0]; 
	normdiff_max = fabs(normdiff_max);
			
	dtmp = norm_bar_2d[0] - norm_bar_2d[1]; 
	dtmp = fabs(dtmp);
			
	if (normdiff_max < dtmp) { 
		normdiff_max = dtmp;
	}
			
	dtmp = norm_bar_2d[1] - norm_bar_3d; 
	dtmp = fabs(dtmp);
			
	if (normdiff_max < dtmp) { 
		normdiff_max = dtmp;
	}

	if (normdiff_max > 999) {
		valid_bar_3d = 0;
	} else {
		cr_vector_t cv_bar_3d_2d;
		double angle_max;
		double anglediff_bar_max;

		cv_bar_3d_2d.x = pClubvect_bar_3d->x; cv_bar_3d_2d.y = pClubvect_bar_3d->y; cv_bar_3d_2d.z = 0;

		angle_max = cr_vector_angle(&cv_bar_3d_2d, &clubvect_bar_2d[0]);
		dtmp = cr_vector_angle(&clubvect_bar_2d[0], &clubvect_bar_2d[1]); 
				
		if (angle_max < dtmp) { 
			angle_max = dtmp;
		}
		dtmp = cr_vector_angle(&clubvect_bar_2d[1], &cv_bar_3d_2d); 
				
		if (angle_max < dtmp) { 
			angle_max = dtmp;
		}

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BAR, angle_max: %lf\n", RADIAN2DEGREE(angle_max));
#define ANGLEDIFF_BAR_MAX			4.0 // 1.5, 3.0
#define ANGLEDIFF_BAR_MAX_TOPBAR	5.0
		if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_TOPBAR) {
			anglediff_bar_max = ANGLEDIFF_BAR_MAX_TOPBAR;
		} else {
			anglediff_bar_max = ANGLEDIFF_BAR_MAX;
		}
		if (angle_max > DEGREE2RADIAN(anglediff_bar_max)) {
			valid_bar_3d = 0;
		}
	}

	return valid_bar_3d;		
}

static I32 CalcExtraData(iana_t *piana, clubmark_t *pcm, 
						U32 valid_dot_3d, U32 valid_bar_3d, 
						U32 valid_dot_2d[2], U32 valid_bar_2d[2], 
						cr_vector_t clubvect_dot_2d[2], cr_vector_t clubvect_bar_2d[2],
						cr_vector_t *pClubvect_dot_3d, cr_vector_t *pClubvect_bar_3d)
{   // Calculate some extra results, like lie or loft, are computed more below using 3d informations. Find '_extdata' and 'calcclubdata'
        // will be re-calculate several times
	I32 res = 1;        
	double clubspeed;
	double clubpathangle;
	double clubattackangle;

	double clubfaceangle;
	double clubloftangle;
	double clublieangle;
	double clubfaceimpactLateral;
	double clubfaceimpactVertical;

	cr_vector_t clubvect;

	U32		Assurance_clubspeed_B;				// [0 ~ 100]
	U32		Assurance_clubspeed_A;				// [0 ~ 100]
	U32		Assurance_clubpath;					// [0 ~ 100]
	U32		Assurance_clubfaceangle;			// [0 ~ 100]
	U32		Assurance_clubattackangle;			// [0 ~ 100]
	U32		Assurance_clubloftangle;			// [0 ~ 100]
	U32		Assurance_clublieangle;				// [0 ~ 100]
	U32		Assurance_clubfaceimpactLateral;	// [0 ~ 100]
	U32		Assurance_clubfaceimpactVertical;	// [0 ~ 100]

	U32		clubfaceangleEXT_valid;
	//---
	Assurance_clubspeed_B 			= 0;
	Assurance_clubspeed_A 			= 0;
	Assurance_clubpath				= 0;
	Assurance_clubfaceangle			= 0;
	Assurance_clubattackangle		= 0;
	Assurance_clubloftangle			= 0;
	Assurance_clublieangle			= 0;
	Assurance_clubfaceimpactLateral = 0;
	Assurance_clubfaceimpactVertical= 0;

	clubspeed 	= 0;
	clubpathangle 	= 0;
	clubattackangle 	= 0;


	clubfaceangle = 0;
	clubloftangle = 0;
	clublieangle = 0;
	clubfaceimpactLateral = 0;
	clubfaceimpactVertical = 0;

	clubfaceangleEXT_valid = 0;
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "valid_dot_3d: %d valid_dot_2d[0]: %d valid_dot_2d[1]: %d \n", valid_dot_3d, valid_dot_2d[0], valid_dot_2d[1]);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "valid_bar_3d: %d valid_bar_2d[0]: %d valid_bar_2d[1]: %d \n", valid_bar_3d, valid_bar_2d[0], valid_bar_2d[1]);

	if (valid_dot_3d) {
		U32 recalc_extdata;
		U32 mres;
		double clubspeed_mult;

		Assurance_clubspeed_B 	= 95;
		Assurance_clubspeed_A 	= 80;
		Assurance_clubpath		= 90;
		Assurance_clubattackangle = 90;

		memcpy(&clubvect, pClubvect_dot_3d, sizeof(cr_vector_t));
		calcclubdata(&clubvect, &clubspeed, &clubpathangle, &clubattackangle);
		
		mres = clubspeed_dot_3d_mult(piana, pcm, &clubspeed_mult);
		if (mres == 0) {
			clubspeed_mult = LOWERDOT_CLUBSPEED_MULT;
		}
		
		clubspeed = clubspeed * clubspeed_mult;


#define MAXATTACKANGLE	15.0
		if (clubattackangle > MAXATTACKANGLE || clubattackangle <-MAXATTACKANGLE) {
			Assurance_clubattackangle = 0;
		}

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "line %d  attackangle: %lf\n", __LINE__, clubattackangle);

		recalc_extdata = 0;

		if (valid_bar_3d) {
			//--
			s_barmethod = 1;
			ComputeData_Extdata(
					piana, pcm,
					&clubfaceangle,
					&clubloftangle,
					&clublieangle,
					&clubfaceimpactLateral,
					&clubfaceimpactVertical);
			clubfaceangleEXT_valid = 1;
			Assurance_clubfaceangle = 90;					//measured..
			Assurance_clubloftangle = 90;
			if (clubloftangle < 0.0) {
				Assurance_clubloftangle = 0;
			}
			Assurance_clublieangle = 90;
			Assurance_clubfaceimpactLateral = 90;
			Assurance_clubfaceimpactVertical = 90;

		} else {
			if (valid_bar_2d[0] && valid_bar_2d[1]) {
				Assurance_clubfaceangle = 80;					//calc'ed.
				s_barmethod = 1;
				ComputeData_Extdata(				// 20200525.. Revive.. :P
						piana, pcm,
						&clubfaceangle,
						&clubloftangle,
						&clublieangle,
						&clubfaceimpactLateral,
						&clubfaceimpactVertical);
				clubfaceangleEXT_valid = 1;				
				Assurance_clubfaceimpactLateral = 80;
				Assurance_clubfaceimpactVertical = 80;

				if (clubloftangle < 0.0) {
					Assurance_clubloftangle = 0;
				}
			} else {
				Assurance_clubfaceangle = 0;

				Assurance_clubloftangle = 0;
				Assurance_clublieangle = 0;
				Assurance_clubfaceimpactLateral = 0;
				Assurance_clubfaceimpactVertical = 0;

				clubfaceangleEXT_valid = 0;
				recalc_extdata = 1;
			}
		}

		if (recalc_extdata) {
			double clubfaceangle2;
			double clubloftangle2;
			double clublieangle2;
			double clubfaceimpactLateral2;
			double clubfaceimpactVertical2;

			//--
			clubfaceangle2 = 0;
			clubloftangle2 = 0;
			clublieangle2 = 0;
			clubfaceimpactLateral2 = 0;
			clubfaceimpactVertical2 = 0;


			res = ComputeData_Extdata2(piana, pcm, 
					&clubfaceangle2,
					&clubloftangle2,
					&clublieangle2,
					&clubfaceimpactLateral2,
					&clubfaceimpactVertical2);	
			if (res != 0) {
				Assurance_clubloftangle = 90;
				if (clubloftangle2 < 0.0) {
					Assurance_clubloftangle = 0;
				}
				clubfaceangleEXT_valid = 1;
				Assurance_clubfaceangle = 90;
				Assurance_clublieangle = 90;
				Assurance_clubfaceimpactLateral = 90;
				Assurance_clubfaceimpactVertical = 90;

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "extdata recalc success. \n");
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "extdata clubfaceangle          : %lf -> %lf\n", clubfaceangle, clubfaceangle2);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "extdata clubloftangle          : %lf -> %lf\n", clubloftangle, clubloftangle2);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "extdata clublieangle           : %lf -> %lf\n", clublieangle, clublieangle2);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "extdata clubfaceimpactLateral  : %lf -> %lf\n", clubfaceimpactLateral, clubfaceimpactLateral2);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "extdata clubfaceimpactVertical : %lf -> %lf\n", clubfaceimpactVertical, clubfaceimpactVertical2);

				clubfaceangle 			= clubfaceangle2;
				clubloftangle 			= clubloftangle2;
				clublieangle 			= clublieangle2;
				clubfaceimpactLateral 	= clubfaceimpactLateral2;
				clubfaceimpactVertical	= clubfaceimpactVertical2;

			} else {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "extdata recalc fail..\n");
			}
		}
	} else { 			// if (valid_dot_3d)
		if (valid_bar_3d) {
			U32 recalc_extdata;

			//--
			recalc_extdata = 0;

			Assurance_clubspeed_B 	= 95;
			Assurance_clubspeed_A 	= 80;
			Assurance_clubpath		= 90;
			Assurance_clubattackangle = 80;

			memcpy(&clubvect, pClubvect_bar_3d, sizeof(cr_vector_t));
			calcclubdata(&clubvect, &clubspeed, &clubpathangle, &clubattackangle);
			if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_BARDOT) {
				//clubattackangle = clubattackangle /2.0;			// -_-;
			}
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "line %d  attackangle: %lf\n", __LINE__, clubattackangle);

			Assurance_clubfaceangle = 80;					//calc'ed.
			if (valid_dot_2d[0] && valid_dot_2d[1]) {
				s_barmethod = 1;
				res = ComputeData_Extdata(				// 20200525.. Revive.. :P
						piana, pcm,
						&clubfaceangle,
						&clubloftangle,
						&clublieangle,
						&clubfaceimpactLateral,
						&clubfaceimpactVertical);
				if (res) {
					Assurance_clubfaceimpactLateral = 80;
					Assurance_clubfaceimpactVertical = 80;
					clubfaceangleEXT_valid = 1;
					recalc_extdata = 0;
				} else {
					Assurance_clubfaceimpactLateral = 0;
					Assurance_clubfaceimpactVertical = 0;
					recalc_extdata = 1;
				}
				if (clubloftangle < 0.0) {
					Assurance_clubloftangle = 0;
				}
			} else {
				if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_TOPBAR) {
					Assurance_clubloftangle = 0;
					Assurance_clublieangle = 0;
					Assurance_clubfaceimpactVertical = 0;
					clubloftangle = 0;
					clublieangle = 0;
					clubfaceimpactVertical = 0;

					res = ComputeData_Topbar_Extdata(		// without clubloftangle, clublieangle, clubfaceimpactVertical.
							piana, pcm,
							&clubfaceangle,
							&clubfaceimpactLateral);

					if (res) {
						Assurance_clubfaceangle = 90;					//measured..
						Assurance_clubfaceimpactLateral = 90;
						clubfaceangleEXT_valid = 1;
						recalc_extdata = 0;
					} else {
						Assurance_clubfaceangle = 90;					//measured..
						Assurance_clubfaceimpactLateral = 90;
						recalc_extdata = 1;
					}
				} else {
					Assurance_clubfaceimpactLateral = 0;
					Assurance_clubfaceimpactVertical = 0;
					recalc_extdata = 1;
				}
			}

			if (recalc_extdata) {
				double clubfaceangle2;
				double clubloftangle2;
				double clublieangle2;
				double clubfaceimpactLateral2;
				double clubfaceimpactVertical2;

				//---
				clubfaceangle2 = 0;
				clubloftangle2 = 0;
				clublieangle2 = 0;
				clubfaceimpactLateral2 = 0;
				clubfaceimpactVertical2 = 0;

				if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_BARDOT) {
					res = ComputeData_Extdata2(piana, pcm, 
							&clubfaceangle2,
							&clubloftangle2,
							&clublieangle2,
							&clubfaceimpactLateral2,
							&clubfaceimpactVertical2);	
					clubfaceangleEXT_valid = 1;
					Assurance_clubfaceangle = 90;
					Assurance_clubloftangle = 90;
					if (clubloftangle < 0.0) {
						Assurance_clubloftangle = 0;
					}
					Assurance_clublieangle = 90;
					Assurance_clubfaceimpactLateral = 90;
					Assurance_clubfaceimpactVertical = 90;
				} else if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_TOPBAR) {
					res = ComputeData_Topbar_Extdata2(
							piana, pcm, 
							&clubfaceangle2,
							&clubfaceimpactLateral2);
					Assurance_clubfaceangle = 90;
					Assurance_clubfaceimpactLateral = 90;

					Assurance_clubloftangle = 0;
					Assurance_clublieangle = 0;
					Assurance_clubfaceimpactVertical = 0;
					clubloftangle2 = 0;
					clublieangle2 = 0;
					clubfaceimpactVertical2 = 0;
					clubfaceangleEXT_valid = 1;
				} else {
					res = 0;
				}

				if (res != 0) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "extdata recalc success. \n");
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "extdata clubfaceangle          : %lf -> %lf\n", clubfaceangle, clubfaceangle2);
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "extdata clubloftangle          : %lf -> %lf\n", clubloftangle, clubloftangle2);
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "extdata clublieangle           : %lf -> %lf\n", clublieangle, clublieangle2);
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "extdata clubfaceimpactLateral  : %lf -> %lf\n", clubfaceimpactLateral, clubfaceimpactLateral2);
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "extdata clubfaceimpactVertical : %lf -> %lf\n", clubfaceimpactVertical, clubfaceimpactVertical2);


					clubfaceangle 			= clubfaceangle2;
					clubloftangle 			= clubloftangle2;
					clublieangle 			= clublieangle2;
					clubfaceimpactLateral 	= clubfaceimpactLateral2;
					clubfaceimpactVertical	= clubfaceimpactVertical2;

				} else {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "extdata recalc fail..\n");
				}
			}
		} else {					// if (valid_bar_3d) 		// direction and speed: bar
		
			Assurance_clubspeed_B 	= 95;
			Assurance_clubspeed_A 	= 80;
			Assurance_clubpath		= 90;
			Assurance_clubattackangle = 0;
			Assurance_clubfaceangle = 80;					//calc'ed.


			if (valid_dot_2d[0]) {			// DOT, cam0
				memcpy(&clubvect, &clubvect_dot_2d[0], sizeof(cr_vector_t));
				calcclubdata(&clubvect, &clubspeed, &clubpathangle, &clubattackangle);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "DOT, cam0, line %d  attackangle: %lf\n", __LINE__, clubattackangle);
			} else if (valid_dot_2d[1]) {	// DOT, cam1
				memcpy(&clubvect, &clubvect_dot_2d[1], sizeof(cr_vector_t));
				calcclubdata(&clubvect, &clubspeed, &clubpathangle, &clubattackangle);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "DOT, cam1, line %d  attackangle: %lf\n", __LINE__, clubattackangle);
			} else if (valid_bar_2d[0]) {	// BAR, cam0

				memcpy(&clubvect, &clubvect_bar_2d[0], sizeof(cr_vector_t));
				calcclubdata(&clubvect, &clubspeed, &clubpathangle, &clubattackangle);
#if defined(SPEED_BAR2D_MULT)
				clubspeed = clubspeed * SPEED_BAR2D_MULT;
#endif
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BAR, cam0, line %d  attackangle: %lf\n", __LINE__, clubattackangle);
			} else if (valid_bar_2d[1]) {	// BAR, cam1
				memcpy(&clubvect, &clubvect_bar_2d[1], sizeof(cr_vector_t));
				calcclubdata(&clubvect, &clubspeed, &clubpathangle, &clubattackangle);
#if defined(SPEED_BAR2D_MULT)
				clubspeed = clubspeed * SPEED_BAR2D_MULT;
#endif
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "line %d  attackangle: %lf\n", __LINE__, clubattackangle);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "line %d  attackangle: %lf\n", __LINE__, clubattackangle);
			} else {
				Assurance_clubspeed_B 	= 0;
				Assurance_clubspeed_A 	= 0;
				Assurance_clubpath		= 0;
				Assurance_clubfaceangle = 0;
				Assurance_clubattackangle = 0;
			}

			{
				U32 mres;
				double clubspeed_mult;
				mres = clubspeed_dot_2d_mult(piana, pcm, &clubspeed_mult);
				if (mres == 0) {
					clubspeed_mult = LOWERDOT_2D_CLUBSPEED_MULT;
				}
				clubspeed = clubspeed * clubspeed_mult;
			}
		}
	} 	 // if (valid_dot_3d), else ... 


	if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_TOPBAR) {
		iana_cam_t	*pic;
		cr_point_t *pb3d;				// ball 3d position

		pic = piana->pic[0];			// 
		pb3d = &pic->icp.b3d;			// ball 3d position

		if (pic->icp.b3d.z < BALLPOS_HEIGHT_TEE) {
			clubspeed = clubspeed * TOPBAR_CLUBSPEED_MULT_IRON;			// IRON
		} else {
			clubspeed = clubspeed * TOPBAR_CLUBSPEED_MULT_TEE;			// DRIVER
		}

	}

	//---------------------------------- CHECK Impact point
	{
		U32 isTee;
		I32 impactLateralMax;
		I32 impactVerticalMax;
		iana_cam_t	*pic;

		//--
		pic = piana->pic[0];			// 

		if (pic->icp.b3d.z >= BALLPOS_HEIGHT_TEE) {
			isTee = 1;
			impactLateralMax = CLUBFACEIMPACT_LATERAL_TEE_MAX;
			impactVerticalMax = CLUBFACEIMPACT_VERTICAL_TEE_MAX;
		} else {
			isTee = 0;
			impactLateralMax = CLUBFACEIMPACT_LATERAL_IRON_MAX;
			impactVerticalMax = CLUBFACEIMPACT_VERTICAL_IRON_MAX;
		}

		if (Assurance_clubfaceimpactLateral > 50) {
			if (clubfaceimpactLateral < -impactLateralMax) {
				clubfaceimpactLateral = -impactLateralMax;
			}
			if (clubfaceimpactLateral >  impactLateralMax) {
				clubfaceimpactLateral =  impactLateralMax;
			}
		}
		if (Assurance_clubfaceimpactVertical > 50) {
			if (clubfaceimpactVertical < -impactVerticalMax) {
				clubfaceimpactVertical = -impactVerticalMax;
			}
			if (clubfaceimpactVertical >  impactVerticalMax) {
				clubfaceimpactVertical =  impactVerticalMax;
			}
		}

		if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_TOPBAR) {
//#define TOPBAR_IMPACT_VERTICAL_FAKE
#if defined(TOPBAR_IMPACT_VERTICAL_FAKE)
			if (Assurance_clubfaceimpactLateral > 50) {
				Assurance_clubfaceimpactVertical = 70;
				clubfaceimpactVertical	= 0;
			}
#endif
		}
	}

	//---------------------------------- CHECK smashfactor... 	20200521
	{
		if (Assurance_clubspeed_B > 80) {
			iana_shotresult_t *psr;
			double vmag;
			double smashfactor;

			//--
			psr  = &piana->shotresultdata;
			vmag = psr->vmag;

#define MIN_CLUBSPEED 1.0
			if (clubspeed >= MIN_CLUBSPEED) {
				double smashfactorN;
				double clubspeedN;
				smashfactor = vmag / clubspeed;

				if (smashfactor < 1.49) {
					smashfactorN = smashfactor;
				} else if (smashfactor < 1.51) {
					smashfactorN = LINEAR_SCALE(smashfactor, 1.49, 1.49, 1.51, 1.51);
				} else if (smashfactor < 2.00) {
					smashfactorN = LINEAR_SCALE(smashfactor, 1.51, 1.51, 2.00, 1.53);
				} else {
					smashfactorN = 1.53;
				}
				clubspeedN = vmag / smashfactorN;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "SMASHFACTORN   %lf -> %lf,  clubspeed: %lf -> %lf\n", 
					smashfactor,
					smashfactorN,
					clubspeed, 
					clubspeedN);

				clubspeed = clubspeedN;
			}
		}
	}

	cr_vector_normalization(&clubvect, &clubvect);
	cr_vector_scalar(&clubvect, clubspeed, &clubvect);


	{ // save restults
		iana_shotresult_t *psr;
		iana_shotresult_t *psr2;

		psr  = &piana->shotresultdata;
		psr2 = &piana->shotresultdata2;

		psr2->clubspeed_B 	=  psr->clubspeed_B 		= clubspeed;
		psr2->clubspeed_A 	=  psr->clubspeed_A;
		psr2->clubpath 		=  psr->clubpath			= clubpathangle;
		if (clubfaceangleEXT_valid) {
			psr->clubfaceangle = clubfaceangle;
		} else {
			Assurance_clubfaceangle = 0;					// NOT valid.. :P
		}

		psr2->clubfaceangle =  psr->clubfaceangle;

		psr2->clubattackangle = psr->clubattackangle 	= clubattackangle;

		psr2->clubloftangle 		= psr->clubloftangle 			= clubloftangle;
		psr2->clublieangle 			= psr->clublieangle 			= clublieangle;
		psr2->clubfaceimpactLateral = psr->clubfaceimpactLateral 	= clubfaceimpactLateral;
		psr2->clubfaceimpactVertical= psr->clubfaceimpactVertical 	= clubfaceimpactVertical;


		psr2->Assurance_clubspeed_B 	= psr->Assurance_clubspeed_B 		= Assurance_clubspeed_B;
		psr2->Assurance_clubspeed_A 	= psr->Assurance_clubspeed_A		= Assurance_clubspeed_A;
		psr2->Assurance_clubpath 		= psr->Assurance_clubpath			= Assurance_clubpath;
		psr2->Assurance_clubfaceangle 	= psr->Assurance_clubfaceangle		= Assurance_clubfaceangle;
		psr2->Assurance_clubattackangle = psr->Assurance_clubattackangle 	= Assurance_clubattackangle;

		psr2->Assurance_clubloftangle 			= psr->Assurance_clubloftangle = Assurance_clubloftangle;
		psr2->Assurance_clublieangle 			= psr->Assurance_clublieangle  = Assurance_clublieangle;
		psr2->Assurance_clubfaceimpactLateral 	= psr->Assurance_clubfaceimpactLateral = Assurance_clubfaceimpactLateral;
		psr2->Assurance_clubfaceimpactVertical	= psr->Assurance_clubfaceimpactVertical = Assurance_clubfaceimpactVertical;
	}

	return res;
}


static I32 VerifyResult(iana_t *piana)
{	
	iana_shotresult_t *psr;
	iana_shotresult_t *psr2;
	double smashfactor;

	double vmag;
	double clubspeed;
	double vmagMin;
	//--

	psr  = &piana->shotresultdata;
	psr2 = &piana->shotresultdata2;

	vmag = psr->vmag;
	clubspeed = psr->clubspeed_B;
	smashfactor = vmag / clubspeed;

/****************************************
		1.42	30
		1.43	35
		1.44	35
		1.45	35
		1.46	40
		1.47	45
		1.48	47
		1.49	47
		1.5		50
		1.51	55
****************************************/

	if (smashfactor < 1.42) {
		vmagMin = 1.0;
	}
	else if (smashfactor < 1.45) {
		vmagMin = LINEAR_SCALE(smashfactor, 1.42, 30.0, 1.45, 35.0);
	}
	else if (smashfactor < 1.46) {
		vmagMin = LINEAR_SCALE(smashfactor, 1.45, 35, 1.46, 40.0);
	}
	else if (smashfactor < 1.49) {
		vmagMin = LINEAR_SCALE(smashfactor, 1.46, 40.0, 1.49, 47.0);
	}
	else if (smashfactor < 1.50) {
		vmagMin = LINEAR_SCALE(smashfactor, 1.49, 47.0, 1.50, 55.0);
	}
	else {
		vmagMin = 55.0;
	}


	if (vmag < vmagMin) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "fail. sf: %lf,  vmag: %lf, vmagMin; %lf\n", smashfactor, vmag, vmagMin);
		psr2->Assurance_clubspeed_B = psr->Assurance_clubspeed_B = 0;
		psr2->Assurance_clubspeed_A = psr->Assurance_clubspeed_A = 0;
		psr2->Assurance_clubpath 	= psr->Assurance_clubpath	= 0;
		psr2->Assurance_clubfaceangle 	= psr->Assurance_clubfaceangle	= 0;
		psr2->Assurance_clubattackangle	= psr->Assurance_clubattackangle= 0;
		psr2->Assurance_clubloftangle 	= psr->Assurance_clubloftangle	= 0;
		psr2->Assurance_clublieangle 	= psr->Assurance_clublieangle	= 0;
		psr2->Assurance_clubfaceimpactLateral 	= psr->Assurance_clubfaceimpactLateral	= 0;
		psr2->Assurance_clubfaceimpactVertical 	= psr->Assurance_clubfaceimpactVertical	= 0;

		return CR_ERROR;
	}

	return CR_OK;
}

/*!
 ********************************************************************************
 *	@brief      computes club data like clubpath, attackangle, or additionally, loft, lie, etc with refined mark position information, the bardot.
 *              TODO? : split computing and saving(= updating result to shotresult structure)?
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @param[in]	pcm
 *              pointer to the club mark structure
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2020/04/30
 *******************************************************************************/
static I32 ComputeData(iana_t *piana, clubmark_t *pcm)
{
	I32 res;
	U32 valid_dot_2d[MAXCAMCOUNT];
	U32 valid_dot_3d;
	
	cr_vector_t clubvect_dot_2d[MAXCAMCOUNT];
	cr_vector_t clubvect_dot_3d;

	U32 valid_bar_2d[MAXCAMCOUNT];
	U32 valid_bar_3d;
	
	cr_vector_t clubvect_bar_2d[MAXCAMCOUNT];
	cr_vector_t clubvect_bar_3d;

		
	//--------------------------------------------------
	if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_TOPBAR) {
		valid_dot_2d[0] = valid_dot_2d[1] = valid_dot_3d = 0;
		memset(&clubvect_dot_2d[0], 0, sizeof(cr_vector_t)*MAXCAMCOUNT);
		memset(&clubvect_dot_3d, 0, sizeof(cr_vector_t));
	} else {
		res = ComputeData_Dot(piana, pcm, &clubvect_dot_2d[0], &valid_dot_2d[0], &clubvect_dot_3d, &valid_dot_3d);
	}

	
	if (valid_dot_3d) {
		valid_dot_3d = VerifyDot_3D(&clubvect_dot_3d, clubvect_dot_2d);
	}	

	res = ComputeData_Bar(piana, pcm, &clubvect_bar_2d[0], &valid_bar_2d[0], &clubvect_bar_3d, &valid_bar_3d);

	
	if (valid_bar_3d) {
		valid_bar_3d = VerifyBar_3D(&clubvect_bar_3d, clubvect_bar_2d, pcm);
	}
	

	res = CalcExtraData(piana, pcm, valid_dot_3d, valid_bar_3d, 
					valid_dot_2d, valid_bar_2d, 
					clubvect_dot_2d, clubvect_bar_2d,
					&clubvect_dot_3d, &clubvect_bar_3d);

	res = 1;

	VerifyResult(piana);			// Check Result value... 

	return res;
}

/*!
 ********************************************************************************
 *	@brief      club data using mark
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2019/1210
 *******************************************************************************/
static I32 ComputeData_Dot(iana_t *piana, clubmark_t *pcm, cr_vector_t *pclub2dvect, U32 *pvalid2d, cr_vector_t *pclub3dvect, U32 *pvalid3d)
{
	I32 res;
	U32 camid;
	U32 mavalid[MAXCAMCOUNT];
	
	clubmark_cam_t *pcmc;
	clubmark_face_t *pcfc;

	U32 index;
	//--
	for (camid = 0; camid < NUM_CAM; camid++) {
		pcmc = &pcm->cmc[camid];
		for (index = 0; index < MARKSEQUENCELEN; index++) {
			pcfc = &pcmc->face[index];
			memset(&pcfc->lowerdot.cdotL3d, 0, sizeof(cr_point_t));
			memset(&pcfc->lowerdot.cdotL3dE,0, sizeof(cr_point_t));
		}
	}

	for (camid = 0; camid < NUM_CAM; camid++) {
		mavalid[camid] = pcm->cmc[camid].dotLp.mavalid;
		pvalid2d[camid] = mavalid[camid];
		if (mavalid[camid]) {
			ComputeData_Dot_2D(piana, camid, pcm, pclub2dvect+camid);
			{
				double clubspeed;
				double clubpathangle;
				double attackangle;

				calcclubdata(pclub2dvect+camid, &clubspeed, &clubpathangle, &attackangle);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "EYESENSOR Dot 2d[%d] club speed: %lf,   path: %lf,   attck: %6.2lf\n", 
						camid, 
						clubspeed, clubpathangle, attackangle);
			}
		}
	}
	res = 1;

	*pvalid3d = 0;
	if (mavalid[0] == 0 && mavalid[1] == 0) {
		res = 0;
	} else if (mavalid[0] && mavalid[1] ) {
		ComputeData_Dot_3D(piana, pcm, pclub3dvect);
		{
			double clubspeed;
			double clubpathangle;
			double attackangle;

			calcclubdata(pclub3dvect, &clubspeed, &clubpathangle, &attackangle);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "EYESENSOR Dot 3dclub speed: %lf,   path: %lf,   attck: %6.2lf\n\n", 
					clubspeed, clubpathangle, attackangle);
		}

		*pvalid3d = 1;
		res = 1;
	}

	return res;
}


static I32 ComputeData_Dot_2D(iana_t *piana, U32 camid, clubmark_t *pcm, cr_vector_t *pclubpath)
{
	I32 res;
	clubmark_cam_t *pcmc;
	clubmark_Lp_t *pdotLp;

	double xma[3];
	double yma[3];
	
	double t0;
	double x0, y0;
	double t1;
	double x1, y1;

	U32 count;
	double vcsum;
	double vc;
	double vcmax;
	cr_vector_t vcvect;
	//--
	pcmc = &pcm->cmc[camid];
	pdotLp = &pcmc->dotLp;

    memcpy(&xma[0], &pdotLp->xma[0], sizeof(double) * 3);
    memcpy(&yma[0], &pdotLp->yma[0], sizeof(double) * 3);

	t0 = CALC_BARDOT_DOT_2D_T0 - CALC_BARDOT_DOT_2D_DT;
	x0 = xma[2] * t0*t0 + xma[1] * t0 + xma[0];
	y0 = yma[2] * t0*t0 + yma[1] * t0 + yma[0];

	count = 0;
	vcsum = 0.0;
	vcmax = 0.0;
	for (t1 = CALC_BARDOT_DOT_2D_T0; 
		t1 <= (CALC_BARDOT_DOT_2D_T1 + CALC_BARDOT_DOT_2D_DT / 10.0); 
		t1 += CALC_BARDOT_DOT_2D_DT) 
	{
		double dx, dy, dlen;
		x1 = xma[2] * t1*t1 + xma[1] * t1 + xma[0];
		y1 = yma[2] * t1*t1 + yma[1] * t1 + yma[0];

		dx = x1 - x0;
		dy = y1 - y0;
		dlen = sqrt(dx*dx + dy*dy);
		vc = dlen / (t1 - t0);
		if (vcmax < vc) { vcmax = vc; }
		vcsum = vcsum + vc;

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%d:%2d] (%lf ~ %lf) speed: %lf\n", camid, count, t0, t1, vc);

		x0 = x1;
		y0 = y1;
		t0 = t1;
		count++;
		
	}

	vc = vcsum / count;				// mean of speed

#define MAXVCRATIO	(1.05) // 1.2, 1.1
	if (s_use_vcmax) {
		if (vcmax > vc * MAXVCRATIO) {
			vc = vc * MAXVCRATIO;
		} else {
			vc = vcmax;
		}
	}

	t0 = CALC_BARDOT_DOT_2D_T1 - CALC_BARDOT_DOT_2D_DT/2.0;
	x0 = xma[2] * t0*t0 + xma[1] * t0 + xma[0];
	y0 = yma[2] * t0*t0 + yma[1] * t0 + yma[0];

	t1 = CALC_BARDOT_DOT_2D_T1 + CALC_BARDOT_DOT_2D_DT/2.0;
	x1 = xma[2] * t1*t1 + xma[1] * t1 + xma[0];
	y1 = yma[2] * t1*t1 + yma[1] * t1 + yma[0];

	vcvect.x = (x1-x0);
	vcvect.y = (y1-y0);
	vcvect.z = 0;

	cr_vector_normalization(&vcvect, &vcvect);
	cr_vector_scalar(&vcvect, vc, pclubpath);
	res = 1;

	UNUSED(piana);
	return res;
}

static I32 ComputeData_Dot_3D(iana_t *piana, clubmark_t *pcm, cr_vector_t *pclubpath)
{
	I32 res;
	
	double t0;
	double t1;

	U32 count;
	double vcsum;
	double vc;
	double vcmax;
	cr_vector_t vcvect;

	cr_point_t point3d0, point3d1;
	//--


	t0 = CALC_BARDOT_DOT_3D_T0 - CALC_BARDOT_DOT_3D_DT;
	ComputeData_Dot_P3dE(piana, pcm, t0, &point3d0);

	count = 0;
	vcsum = 0.0;
	vcmax = 0.0;
	for (t1 = CALC_BARDOT_DOT_3D_T0; 
		t1 <= (CALC_BARDOT_DOT_3D_T1 + CALC_BARDOT_DOT_3D_DT / 10.0); 
		t1 += CALC_BARDOT_DOT_3D_DT) 
	{
		double dx, dy, dz, dlen;

		ComputeData_Dot_P3dE(piana, pcm, t1, &point3d1);

		dx = point3d1.x - point3d0.x;
		dy = point3d1.y - point3d0.y;
		dz = point3d1.z - point3d0.z;
		dlen = sqrt(dx*dx + dy*dy + dz*dz);
		vc = dlen / (t1 - t0);
		if (vcmax < vc) { 
            vcmax = vc; 
        }
		vcsum = vcsum + vc;

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [:%2d] (%lf ~ %lf) speed: %lf\n", count, t0, t1, vc);

		memcpy(&point3d0, &point3d1, sizeof(cr_point_t));
		t0 = t1;
		count++;
	}

	vc = vcsum / count;				// mean of speed
	if (s_use_vcmax) {
		if (vcmax > vc * MAXVCRATIO) {
			vc = vc * MAXVCRATIO;
		} else {
			vc = vcmax;
		}
	}

	t0 = CALC_BARDOT_DOT_2D_T1 - CALC_BARDOT_DOT_2D_DT/2.0;
	ComputeData_Dot_P3dE(piana, pcm, t0, &point3d0);

	t1 = CALC_BARDOT_DOT_2D_T1 + CALC_BARDOT_DOT_2D_DT/2.0;
	ComputeData_Dot_P3dE(piana, pcm, t1, &point3d1);

	cr_vector_sub(&point3d1, &point3d0, &vcvect); 
	cr_vector_normalization(&vcvect, &vcvect);
	cr_vector_scalar(&vcvect, vc, pclubpath);
	res = 1;

	UNUSED(piana);
	return res;
}


static I32 ComputeData_Dot_P3dE(iana_t *piana, clubmark_t *pcm, double td, cr_point_t *p3d)
{
	I32 res;
	clubmark_cam_t *pcmc;
	clubmark_Lp_t *pdotLp;

	double xma[MAXCAMCOUNT][3];
	double yma[MAXCAMCOUNT][3];

	cr_point_t point2d[MAXCAMCOUNT];
	double t0;
	U32 camid;

	//--

	for (camid = 0; camid < NUM_CAM; camid++) {
		pcmc = &pcm->cmc[camid];
		pdotLp = &pcmc->dotLp;
		memcpy(&xma[camid][0], &pdotLp->xma[0], sizeof(double) * 3);
		memcpy(&yma[camid][0], &pdotLp->yma[0], sizeof(double) * 3);
	}

	t0 = td;
	for (camid = 0; camid < NUM_CAM; camid++) {
		point2d[camid].x = xma[camid][2] * t0*t0 + xma[camid][1] * t0 + xma[camid][0];
		point2d[camid].y = yma[camid][2] * t0*t0 + yma[camid][1] * t0 + yma[camid][0];
		point2d[camid].z = 0;
	}
	getp3d(piana, &point2d[0], p3d);

	//--
	res = 1;
	return res;
}

static I32 ComputeData_Bar(iana_t *piana, clubmark_t *pcm, cr_vector_t *pclub2dvect, U32 *pvalid2d, cr_vector_t *pclub3dvect, U32 *pvalid3d)
{
	I32 res;
	U32 camid;
	U32 mavalid[MAXCAMCOUNT];

	clubmark_cam_t *pcmc;
	clubmark_face_t *pcfc;
	//clubmark_feature_t	*pcft;

	U32 index;

	//--

	for (camid = 0; camid < NUM_CAM; camid++) {
		pcmc = &pcm->cmc[camid];
		for (index = 0; index < MARKSEQUENCELEN; index++) {
			pcfc = &pcmc->face[index];
			memset(&pcfc->upperline.clineLp03d, 0, sizeof(cr_point_t));
			memset(&pcfc->upperline.clineLp03dE,0, sizeof(cr_point_t));
		}
	}
	
	for (camid = 0; camid < NUM_CAM; camid++) {
		mavalid[camid] = pcm->cmc[camid].barLp[0].mavalid;
		pvalid2d[camid] = mavalid[camid];
		if (mavalid[camid]) {

			ComputeData_Bar_2D(piana, camid, pcm, pclub2dvect+camid);

			{
				double clubspeed;
				double clubpathangle;
				double attackangle;

				calcclubdata(pclub2dvect+camid, &clubspeed, &clubpathangle, &attackangle);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "EYESENSOR BAR 2d[%d] club speed: %lf,   path: %lf,   attck: %6.2lf\n", 
						camid, 
						clubspeed, clubpathangle, attackangle);
			}

		}
	}
	res = 1;

	*pvalid3d = 0;
	if (mavalid[0] == 0 && mavalid[1] == 0) {
		res = 0;
	} else if (mavalid[0] && mavalid[1] ) {
		ComputeData_Bar_3D(piana, pcm, pclub3dvect);
		*pvalid3d = 1;
		res = 1;
	}

	return res;
}





//upperline_3dot
static I32 ComputeData_Upperline_3Dot(iana_t *piana, clubmark_t *pcm)
{
	I32 res;
	U32 camid;
	U32 mavalid[MAXCAMCOUNT];

	//--

	for (camid = 0; camid < NUM_CAM; camid++) {
		mavalid[camid] = pcm->cmc[camid].barLp[0].mavalid;
	}
	if (mavalid[0] == 0 && mavalid[1] == 0) {
		for (camid = 0; camid < NUM_CAM; camid++) {
			ComputeData_Upperline_3Dot_Face(piana, camid, pcm);
		}
		res = 1;
	} else {
		res = 0;
	}

	UNUSED(piana);
	return res;
}

static I32 ComputeData_Upperline_3Dot_Face(iana_t *piana, U32 camid, clubmark_t *pcm)
{
	I32 res;

	clubmark_cam_t *pcmc;
	clubmark_Lp_t *pdotLp;


	double xmad[3];
	double ymad[3];
	U32 offset_x, offset_y;
	double xfact, yfact;

	U32 index0, index1;
	U32 index;
	U32 i;
	//------
	res = 0;

	pcmc = &pcm->cmc[camid];
	if (pcmc->dotLp.mavalid == 0) {
		res = 0;
		goto func_exit;
	}

	xfact = pcmc->xfact;
	yfact = pcmc->yfact;

	index0 = pcmc->index0;
	index1 = pcmc->index1;

	// reconstruct upperline with 3dot.
	pdotLp = &pcmc->dotLp;

	memcpy(&xmad[0], &pdotLp->xma[0], sizeof(double) * 3);
	memcpy(&ymad[0], &pdotLp->yma[0], sizeof(double) * 3);

	for (index = index0; index <= index1; index++) {
		clubmark_dot_t *pcmd;
		clubmark_bar_t *pcmb;
		clubmark_region_t *pcrd;
		clubmark_region_t *pcrb;
		clubmark_face_t *pcfc;
		clubmark_feature_t	*pcft;

		double xd, yd;
		double td;

		double xx[MAXCANDIDATE], yy[MAXCANDIDATE];
		double sumx, sumy;
		U32 count;

		//--
		pcft = &pcmc->cft[index];
		pcfc = &pcmc->face[index];
		pcmd = &pcft->cmd;
		pcmb = &pcft->cmb;
		pcrd = &pcft->crd;
		pcrb = &pcft->crb;

		offset_x = pcft->offset_x; 
		offset_y = pcft->offset_y;

		// 1) get Dot point
		td = pcft->tsdiffd;

		xd = xmad[2]*td*td + xmad[1]*td + xmad[0];
		yd = ymad[2]*td*td + ymad[1]*td + ymad[0];

		count = 0;
		sumx = 0.0; sumy = 0.0;
		for (i = 0; i < pcmd->count; i++) {
			clubmark_state_t state;
			clubmark_exilewhy_t exilewhy;

			//--
			state = pcmd->clm[i].state;
			exilewhy = pcmd->clm[i].exilewhy;

			if ( state == CLUBMARK_DOT 
				|| (
					state == CLUBMARK_EXILE 
					&& (exilewhy == CLUBMARK_EXILEWHY_SMALLER_LOWERMAXY1 || exilewhy == CLUBMARK_EXILEWHY_SMALLER_LOWERMAXY2)
				   )
			   ) 
			{
				imagesegment_t 	*pis;
				point_t Pp, Lp;
				double xdi, ydi;

				//--
				pis = &pcmd->clm[i].imgseg;
				Pp.x = pis->mx;
				Pp.y = pis->my;
				Pp.x = Pp.x / xfact + offset_x;
				Pp.y = Pp.y / yfact + offset_y;
				iana_P2L(piana, camid, &Pp, &Lp, 0);

				xdi = Lp.x;
				ydi = Lp.y;
#define MINDIST_XD	(0.04)
#define MAXDIST_XD	(0.08)
#define MAXDIST_YD	(0.05)
				if ((xdi > xd + MINDIST_XD && xdi < xd + MAXDIST_XD) && (ydi > yd - MAXDIST_YD && ydi < yd + MAXDIST_YD)) {
					xx[count] = xdi;
					yy[count] = ydi;
					sumx += xdi;
					sumy += ydi;
					count++;
				}
			}
		}

#define	REVIVE_MINCOUNT 2
		if (count >= REVIVE_MINCOUNT) {
			double ma[2];
			double r2;
			cr_vector_t	m_;
			cr_point_t p0_;
			cr_line_t line0_;
			cr_point_t dp_;

			//--
			cr_regression2(yy, xx, count, &ma[1], &ma[0], &r2);


			m_.x = ma[1] * 1.0;
			m_.y = 1.0;
			m_.z = 0;
#if 0								// why??... -_-;
			if (ma[1] > 0) {
				m_.x = ma[1] * 1.0;
				m_.y = 1.0;
				m_.z = 0;
			} else {
				m_.x = -ma[1] * 1.0;
				m_.y = -1.0;
				m_.z = 0;
			}
#endif
			cr_vector_normalization(&m_, &m_);

			p0_.x = sumx / count;
			p0_.y = sumy / count;
			p0_.z = 0;

			cr_line_p0m(&p0_, &m_, &line0_);

			dp_.x = xd;
			dp_.y = yd;
			dp_.z = 0;

			cr_footPerpendicular_Line(&dp_, &line0_, &p0_);		// get perpendicular point.

			cr_line_p0m(&p0_, &m_, &pcfc->upperline.clineL);



			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "3dot_face, [%d %10lf] m: %lf %lf p0_: %lf %lf %lf\n", 
					camid, td,
					m_.x, m_.y,
					p0_.x, 
					p0_.y, 
					p0_.z);
#if 0

#if 1
			m_.x = -ma[1] * 1.0;
			m_.y = -1.0;
			m_.z = 0;
#endif

			p0_.x = sumx / count;
			p0_.y = sumy / count;
			p0_.z = 0;

			cr_line_p0m(&p0_, &m_, &line0_);

			dp_.x = xd;
			dp_.y = yd;
			dp_.z = 0;

			cr_footPerpendicular_Line(&dp_, &line0_, &p0_);		// get perpendicular point.

			cr_line_p0m(&p0_, &m_, &pcfc->upperline.clineL);



			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-, p0_: %lf %lf %lf\n", 
					p0_.x, 
					p0_.y, 
					p0_.z);

#endif
			pcfc->upperline.valid = 1;
		} else {
			pcfc->upperline.valid = 0;
		}
		res = 1;
	}

func_exit:
	return res;
}
// upperline_3dot fin.




// bar calculation
static I32 ComputeData_Bar_2D(iana_t *piana, U32 camid, clubmark_t *pcm, cr_vector_t *pclubpath)
{
	I32 res;
	clubmark_cam_t *pcmc;
	clubmark_Lp_t *pbarLp;

	double xma[3];
	double yma[3];
	
	double t0;
	double x0, y0;
	double t1;
	double x1, y1;

	U32 count;
	double vcsum;
	double vc;
	double vcmax;
	cr_vector_t vcvect;
	//--
	pcmc = &pcm->cmc[camid];
	pbarLp = &pcmc->barLp[0];

	memcpy(&xma[0], &pbarLp->xma[0], sizeof(double) * 3);
	memcpy(&yma[0], &pbarLp->yma[0], sizeof(double) * 3);

	t0 = CALC_BARDOT_BAR_2D_T0 - CALC_BARDOT_BAR_2D_DT;
	x0 = xma[2] * t0*t0 + xma[1] * t0 + xma[0];
	y0 = yma[2] * t0*t0 + yma[1] * t0 + yma[0];

	count = 0;
	vcsum = 0.0;
	vcmax = 0.0;
	for (t1 = CALC_BARDOT_BAR_2D_T0; 
		t1 <= (CALC_BARDOT_BAR_2D_T1 + CALC_BARDOT_BAR_2D_DT / 10.0); 
		t1 += CALC_BARDOT_BAR_2D_DT) 
	{
		double dx, dy, dlen;
		x1 = xma[2] * t1*t1 + xma[1] * t1 + xma[0];
		y1 = yma[2] * t1*t1 + yma[1] * t1 + yma[0];

		dx = x1 - x0;
		dy = y1 - y0;
		dlen = sqrt(dx*dx + dy*dy);
		vc = dlen / (t1 - t0);
		if (vcmax < vc) { vcmax = vc; }
		vcsum = vcsum + vc;
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [%d:%2d] (%lf ~ %lf) speed: %lf\n", camid, count, t0, t1, vc);

		x0 = x1;
		y0 = y1;
		t0 = t1;
		count++;
	}

	vc = vcsum / count;				// mean of speed
	if (s_use_vcmax) {
		if (vcmax > vc * MAXVCRATIO) {
			vc = vc * MAXVCRATIO;
		} else {
			vc = vcmax;
		}
	}


	t0 = CALC_BARDOT_BAR_2D_T1 - CALC_BARDOT_BAR_2D_DT/2.0;
	x0 = xma[2] * t0*t0 + xma[1] * t0 + xma[0];
	y0 = yma[2] * t0*t0 + yma[1] * t0 + yma[0];

	t1 = CALC_BARDOT_BAR_2D_T1 + CALC_BARDOT_BAR_2D_DT/2.0;
	x1 = xma[2] * t1*t1 + xma[1] * t1 + xma[0];
	y1 = yma[2] * t1*t1 + yma[1] * t1 + yma[0];

	vcvect.x = (x1-x0);
	vcvect.y = (y1-y0);
	vcvect.z = 0;

	cr_vector_normalization(&vcvect, &vcvect);
	cr_vector_scalar(&vcvect, vc, pclubpath);
	res = 1;

	UNUSED(piana);
	return res;
}

static I32 ComputeData_Bar_3D(iana_t *piana, clubmark_t *pcm, cr_vector_t *pclubpath)
{
	I32 res;

	double t0;
	double t1;

	U32 count;
	double vcsum;
	double vc;
	double vcmax;
	cr_vector_t vcvect;

//	cr_point_t point2d[2];
	cr_point_t point3d0, point3d1;
	//--


	t0 = CALC_BARDOT_BAR_3D_T0 - CALC_BARDOT_BAR_3D_DT;
	ComputeData_Bar_P3dE(piana, pcm, t0, &point3d0);

	count = 0;
	vcsum = 0.0;
	vcmax = 0.0;
	for (t1 = CALC_BARDOT_BAR_3D_T0; 
		t1 <= (CALC_BARDOT_BAR_3D_T1 + CALC_BARDOT_BAR_3D_DT / 10.0); 
		t1 += CALC_BARDOT_BAR_3D_DT) 
	{
		double dx, dy, dz, dlen;

		ComputeData_Bar_P3dE(piana, pcm, t1, &point3d1);

		dx = point3d1.x - point3d0.x;
		dy = point3d1.y - point3d0.y;
		dz = point3d1.z - point3d0.z;
		dlen = sqrt(dx*dx + dy*dy + dz*dz);
		vc = dlen / (t1 - t0);
		vcsum = vcsum + vc;
		if (vcmax < vc) { vcmax = vc; }
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" [:%2d] (%lf ~ %lf) speed: %lf\n", count, t0, t1, vc);

		memcpy(&point3d0, &point3d1, sizeof(cr_point_t));
		t0 = t1;
		count++;
	}

	vc = vcsum / count;				// mean of speed
	if (s_use_vcmax) {
		if (vcmax > vc * MAXVCRATIO) {
			vc = vc * MAXVCRATIO;
		} else {
			vc = vcmax;
		}
	}

	t0 = CALC_BARDOT_BAR_2D_T1 - CALC_BARDOT_BAR_2D_DT/2.0;
	ComputeData_Bar_P3dE(piana, pcm, t0, &point3d0);

	t1 = CALC_BARDOT_BAR_2D_T1 + CALC_BARDOT_BAR_2D_DT/2.0;
	ComputeData_Bar_P3dE(piana, pcm, t1, &point3d1);

	cr_vector_sub(&point3d1, &point3d0, &vcvect); 
	cr_vector_normalization(&vcvect, &vcvect);
	cr_vector_scalar(&vcvect, vc, pclubpath);
	res = 1;

	UNUSED(piana);
	return res;
}

static I32 ComputeData_Bar_P3dE(iana_t *piana, clubmark_t *pcm, double td, cr_point_t *p3d)
{
	I32 res;
	clubmark_cam_t *pcmc;
	clubmark_Lp_t *pbarLp;

	double xma[MAXCAMCOUNT][3];
	double yma[MAXCAMCOUNT][3];
	
	cr_point_t point2d[2];
	double t0;
	U32 camid;

	//--

	for (camid = 0; camid < NUM_CAM; camid++) {
		pcmc = &pcm->cmc[camid];
		pbarLp = &pcmc->barLp[0];
		memcpy(&xma[camid][0], &pbarLp->xma[0], sizeof(double) * 3);
		memcpy(&yma[camid][0], &pbarLp->yma[0], sizeof(double) * 3);
	}

	t0 = td;
	for (camid = 0; camid < NUM_CAM; camid++) {
		point2d[camid].x = xma[camid][2] * t0*t0 + xma[camid][1] * t0 + xma[camid][0];
		point2d[camid].y = yma[camid][2] * t0*t0 + yma[camid][1] * t0 + yma[camid][0];
		point2d[camid].z = 0;
	}
	getp3d(piana, &point2d[0], p3d);

	res = 1;
	return res;
}
// bar calculation fin.

static void ComputeData_Extdata_Points(U32 camIdx, clubmark_t *pcm, double t0, cr_point_t *pDotLp2d, cr_point_t *pBarLp2d0, cr_point_t *pBarLp2d1)
{
	clubmark_cam_t *pcmc;
	clubmark_Lp_t *pdotLp;
	double xma[3], yma[3], thma[3];
	double dx, dy;
	clubmark_Lp_t *pbarLp;
	double barlineangle;

	pcmc = &pcm->cmc[camIdx];
	pdotLp = &pcmc->dotLp;
	memcpy(&xma[0], &pdotLp->xma[0], sizeof(double) * 3);
	memcpy(&yma[0], &pdotLp->yma[0], sizeof(double) * 3);

	pDotLp2d->x = xma[2] * t0*t0 + xma[1] * t0 + xma[0];
	pDotLp2d->y = yma[2] * t0*t0 + yma[1] * t0 + yma[0];
	pDotLp2d->z = 0.0;

	pbarLp = &pcmc->barLp[0];
	memcpy(&xma[0], &pbarLp->xma[0], sizeof(double) * 3);
	memcpy(&yma[0], &pbarLp->yma[0], sizeof(double) * 3);
	memcpy(&thma[0], &pbarLp->thma[0], sizeof(double)*3);

	pBarLp2d0->x = xma[2] * t0*t0 + xma[1] * t0 + xma[0];
	pBarLp2d0->y = yma[2] * t0*t0 + yma[1] * t0 + yma[0]; /* + BARLP_DY; */
	pBarLp2d0->z = 0.0;

	barlineangle = thma[2] * t0*t0 + thma[1] * t0 + thma[0];

	dx = sin(barlineangle) * BARDXDYLENGTH;
	dy = cos(barlineangle) * BARDXDYLENGTH;
	pBarLp2d1->x = pBarLp2d0->x + dx;
	pBarLp2d1->y = pBarLp2d0->y + dy;
	pBarLp2d1->z = 0.0;
}


static void ComputeData_Extdata_3DLines(iana_t *piana, cr_point_t *pDotLp3d,
										cr_point_t dotLp2d[2], cr_point_t barLp2d0[2], cr_point_t barLp2d1[2],
										cr_line_t *pHline3d, cr_line_t *pVline3d, 
										cr_vector_t *pUh, cr_vector_t *pUv)
{
	cr_point_t barLp3d0;				// bar fixed point 3d at impact.
	cr_point_t barLp3d1;				// bar another point 3d at impact.

	// get 3d points
	getp3d(piana, dotLp2d, pDotLp3d);	
	getp3d(piana, barLp2d0, &barLp3d0);
	getp3d(piana, barLp2d1, &barLp3d1);
	
	
	cr_line_p0p1(&barLp3d0, pDotLp3d, pHline3d);  // Horizontal line 3d of club face.	faceangle, lie, faceimpactLateral 
	cr_vector_normalization(&pHline3d->m, pUh);	  // directional unit vector of horizontal line, X-axis positive.
	if (pUh->x < 0) {
		cr_vector_scalar(pUh, -1, pUh);
	}
	
	cr_line_p0p1(&barLp3d0, &barLp3d1, pVline3d); // Vertical line 3d of club face. loftangle, faceimpactVertical
	cr_vector_normalization(&pVline3d->m, pUv);	  // directional unit vector of vertical line, Z-axis positive.
	if (pUv->z < 0) {
		cr_vector_scalar(pUv, -1, pUv);
	}
	
	if (s_barmethod == 1) { 	// Use foot of perpendicular. change.. :P
		cr_point_t fpl;
		cr_line_t hline3d1; 	// Horizontal line 3d of club face
		cr_vector_t uh1;		// directional unit vector of horizontal line, X-axis positive.
	
		//--
		cr_footPerpendicular_Line(pDotLp3d, pVline3d, &fpl);
	
		cr_line_p0p1(&fpl, pDotLp3d, &hline3d1);  
		cr_vector_normalization(&hline3d1.m, &uh1);   // directional unit vector of horizontal line, X-axis positive.
		if (uh1.x < 0) {
			cr_vector_scalar(&uh1, -1, &uh1);
		}
	
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n\nhline, fixed point: %lf %lf %lf, %lf %lf %lf	 vs ftl recalced: %lf %lf %lf, %lf %lf %lf	(e: %lf %lf %lf, %lf %lf %lf)\n\n",
				pHline3d->p0.x, pHline3d->p0.y, pHline3d->p0.z, pUh->x, pUh->y, pUh->z, 
				hline3d1.p0.x, hline3d1.p0.y, hline3d1.p0.z, uh1.x, uh1.y, uh1.z, 
	
				hline3d1.p0.x - pHline3d->p0.x,
				hline3d1.p0.y - pHline3d->p0.y,
				hline3d1.p0.z - pHline3d->p0.z,
				uh1.x - pUh->x, 
				uh1.y - pUh->y, 
				uh1.z - pUh->z);
	
		memcpy(pHline3d, &hline3d1, sizeof(cr_line_t));
	}

}

static void ComputeData_Extdata_ClubAngles(cr_line_t *pHline3d, cr_line_t *pVline3d, double *pClubfaceangle, double *pClubloftangle, double *pClublieangle)
{
	double dx, dy, dz, dxy;
	double clubfaceangle;
	double clubloftangle;
	double clublieangle;

	// Horizontal line 3d of club face.   faceangle, lie,
	dx = pHline3d->m.x;
	dy = pHline3d->m.y;
	dz = pHline3d->m.z;

	dxy = sqrt(dx*dx + dy*dy);

	clubfaceangle = -atan2(dy, dx);
	clubfaceangle = RADIAN2DEGREE(clubfaceangle);
	if (clubfaceangle < -90) {
		clubfaceangle = clubfaceangle + 180;
	}
	if (clubfaceangle > +90) {
		clubfaceangle = clubfaceangle - 180;
	}

	clublieangle = -atan2(dz, dxy);
	clublieangle = RADIAN2DEGREE(clublieangle);
	if (clublieangle < -90) {
		clublieangle = clublieangle + 180;
	}
	if (clublieangle > +90) {
		clublieangle = clublieangle - 180;
	}

	// Vertical line 3d of club face.   loftangle
	dx = pVline3d->m.x;
	dy = pVline3d->m.y;
	dz = pVline3d->m.z;

	dxy = sqrt(dx*dx + dy*dy);

	clubloftangle = -atan2(dxy, dz);
	clubloftangle = RADIAN2DEGREE(clubloftangle);
	if (clubloftangle < -90) {
		clubloftangle = clubloftangle + 180;
	}
	if (clubloftangle > +90) {
		clubloftangle = clubloftangle - 180;
	}			

	*pClubfaceangle = clubfaceangle;
	*pClublieangle = clublieangle;
	*pClubloftangle = clubloftangle;	
}



static I32 ComputeData_Extdata(
		iana_t *piana, clubmark_t *pcm,
		double *pclubfaceangle,
		double *pclubloftangle,
		double *pclublieangle,
		double *pclubfaceimpactLateral,
		double *pclubfaceimpactVertical)
{
	I32 res;
	cr_point_t dotLp2d[2];				// Dot fixed points 2d at impact.
	cr_point_t barLp2d0[2];				// bar fixed points 2d at impact.
	cr_point_t barLp2d1[2];				// bar another points 2d at impact.

	cr_point_t dotLp3d;					// Dot fixed point 3d at impact.
	cr_line_t vline3d;					// Vertical line 3d of club face
	cr_vector_t uv;						// directional unit vector of vertical line, Z-axis positive.
	cr_line_t hline3d;					// Horizontal line 3d of club face
	cr_vector_t uh;						// directional unit vector of horizontal line, X-axis positive.

	double t0;

	double clubfaceangle;
	double clubloftangle;
	double clublieangle;
	double clubfaceimpactLateral;
	double clubfaceimpactVertical;

	U32 camIdx;

	//---------------------------------

	
	clubfaceangle 			= 0;
	clubloftangle 			= 0;
	clublieangle 			= 0;
	clubfaceimpactLateral 	= 0;
	clubfaceimpactVertical 	= 0;

	t0 = BARDOT_EXTDATA_IMPACT;
//	t0 = 0;
//	for(t0 = BARDOT_EXTDATA_IMPACT0; t0 < BARDOT_EXTDATA_IMPACT + BARDOT_EXTDATA_IMPACT_D/2.0; t0 += BARDOT_EXTDATA_IMPACT_D) 
	{
		for (camIdx = 0; camIdx < NUM_CAM; camIdx++) {
			ComputeData_Extdata_Points(camIdx, pcm, t0, &dotLp2d[camIdx], &barLp2d0[camIdx], &barLp2d1[camIdx]);
		}

		ComputeData_Extdata_3DLines(piana, &dotLp3d, dotLp2d, barLp2d0, barLp2d1, &hline3d, &vline3d, &uh, &uv);


		ComputeData_Extdata_ClubAngles(&hline3d, &vline3d, &clubfaceangle, &clubloftangle, &clublieangle);	
		// faceimpactLateral , faceimpactVertical
		clubfaceimpactLateral = 0;
		clubfaceimpactVertical = 0;


		{
			// get intersection point of vertical line and horizontal line.
			iana_cam_t	*pic;
			cr_point_t p0, p1;
			cr_point_t *pb3d;				// ball 3d position
			cr_plane_t faceplaneh, faceplanev;

			cr_point_t clubcenterpoint;
			cr_point_t impactpoint;

			cr_vector_t impactoffsetvector;
			cr_point_t vtmp;

			//--
			cr_point_line_line(				// line-line intersection point
					&hline3d,
					&vline3d,
					&p0,					// use p0
					&p1);

			// get club center point  (bellow 0.005 m = 5 mm)
			cr_vector_add(&p0, &dotLp3d, &clubcenterpoint);
			cr_vector_scalar(&clubcenterpoint, 0.5, &clubcenterpoint);
			cr_vector_scalar(&uv, CENTER_ZOFFSET, &vtmp);
			cr_vector_add(&clubcenterpoint, &vtmp, &clubcenterpoint);

			// make faceplane with hline3d and vline3d
			cr_plane_line0line1(&hline3d, &vline3d, &faceplaneh, &faceplanev);

			// get impact point
			pic = piana->pic[0];			// 
			pb3d = &pic->icp.b3d;			// ball 3d position
			cr_footPerpendicular_Plane(pb3d, &faceplaneh, &impactpoint);	// with face plane of H.

			// get deviation vector between center and impact position
			cr_vector_sub(&impactpoint, &clubcenterpoint, &impactoffsetvector); 

			clubfaceimpactLateral = cr_vector_inner(&impactoffsetvector, &uh);
			clubfaceimpactLateral *= 1000.0;		// to mm
			clubfaceimpactVertical = cr_vector_inner(&impactoffsetvector, &uv);
			clubfaceimpactVertical *= 1000.0;		// to mm
		}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" t: %lf faceangel: %lf   loft: %lf lie: %lf Lateral: %lf Vertical: %lf\n",
				t0,
				clubfaceangle, clubloftangle, clublieangle,
				clubfaceimpactLateral,
				clubfaceimpactVertical
				);
	}

	*pclubfaceangle 			= clubfaceangle;
	*pclubloftangle 			= clubloftangle;
	*pclublieangle 				= clublieangle;
	*pclubfaceimpactLateral 	= clubfaceimpactLateral;
	*pclubfaceimpactVertical 	= clubfaceimpactVertical;

	res = 1;
	return res;
}


static I32 ComputeData_Extdata2(
		iana_t *piana, clubmark_t *pcm,
		double *pclubfaceangle,
		double *pclubloftangle,
		double *pclublieangle,
		double *pclubfaceimpactLateral,
		double *pclubfaceimpactVertical)
{
	I32 res;

	I32 index;
	I32 index0, index1;

	U32 camid;
	//--
	index0 = pcm->cmc[0].index0;
	if (index0 > pcm->cmc[0].index0) {
		index0 = pcm->cmc[0].index0;
	}
	index1 = pcm->cmc[0].index1;
	if (index1 < pcm->cmc[1].index1) {
		index1 = pcm->cmc[1].index1;
	}

	res = 0;
	for (index = index1; index >= index0; index--) {
		for (camid = 0; camid < NUM_CAM; camid++) {
			s_barmethod = 0;
			res =  ComputeData_Extdata_Index(
					piana, pcm,
					camid,					// 0, 1...
					index,					// index
					pclubfaceangle,
					pclubloftangle,
					pclublieangle,
					pclubfaceimpactLateral,
					pclubfaceimpactVertical);
			s_barmethod = 1;
			res =  ComputeData_Extdata_Index(
					piana, pcm,
					camid,					// 0, 1...
					index,					// index
					pclubfaceangle,
					pclubloftangle,
					pclublieangle,
					pclubfaceimpactLateral,
					pclubfaceimpactVertical);
			if (res != 0) {
				break;		// successs!!!!!
			}
		}
		if (res != 0) {
			break;
		}
	}

	return res;
}


static I32 ComputeData_Extdata_Index(
		iana_t *piana, clubmark_t *pcm,
		U32 camid,					// 0, 1...
		U32 index,					// index
		double *pclubfaceangle,
		double *pclubloftangle,
		double *pclublieangle,
		double *pclubfaceimpactLateral,
		double *pclubfaceimpactVertical)
{
	I32 res;
	cr_point_t dotLp2d[MAXCAMCOUNT];				// Dot fixed points 2d at impact.
	cr_point_t barLp2d0[MAXCAMCOUNT];				// bar fixed points 2d at impact.
	cr_point_t barLp2d1[MAXCAMCOUNT];				// bar another points 2d at impact.
	double barlineangle;

	cr_point_t dotLp3d;					// Dot fixed point 3d at impact.
	cr_line_t vline3d;					// Vertical line 3d of club face
	cr_vector_t uv;						// directional unit vector of vertical line, Z-axis positive.
	cr_line_t hline3d;					// Horizontal line 3d of club face
	cr_vector_t uh;						// directional unit vector of horizontal line, X-axis positive.

	double t0;
	clubmark_cam_t *pcmc;
	clubmark_cam_t *pcmcO;
	clubmark_Lp_t *pdotLp;

	double clubfaceangle;
	double clubloftangle;
	double clublieangle;
	double clubfaceimpactLateral;
	double clubfaceimpactVertical;

	U32 camidO;			// with regression coeff.

	clubmark_face_t *pcfc;
	clubmark_feature_t	*pcft;

	double nowtd;
	//---------------------------------

	if (camid == 0) {
		camidO= 1;
	} else if (camid == 1) {
		camidO= 0;
	} else {
		res = 0;
		goto func_exit;
	}
	
	pcmc = &pcm->cmc[camid];
	
	clubfaceangle 			= 0;
	clubloftangle 			= 0;
	clublieangle 			= 0;
	clubfaceimpactLateral 	= 0;
	clubfaceimpactVertical 	= 0;


	pcft = &pcmc->cft[index];
	pcfc = &pcmc->face[index];

	if (pcfc->lowerdot.valid == 0 || pcfc->upperline.valid == 0) {
		res = 0;
		goto func_exit;
	}


	nowtd = pcft->tsdiffd;

	pcmcO = &pcm->cmc[camidO];				// OTHER camd.
	if (pcmcO->barLp[0].mavalid == 0
		|| pcmcO->dotLp.mavalid == 0) {		// regression coeff. must be good for OTHER cam.
		res = 0;
		goto func_exit;
	}

	{		// OTHER CAM. REGRESSION-BASED..

		double dx, dy;		
		cr_point_t *pp_;
		cr_vector_t *pm_;

		ComputeData_Extdata_Points(camidO, pcm, nowtd, &dotLp2d[camidO], &barLp2d0[camidO], &barLp2d1[camidO]);

		// Major CAM. Original points
		t0 = nowtd;
		pcmc = &pcm->cmc[camid];
		pdotLp = &pcmc->dotLp;

		pcft = &pcmc->cft[index];
		pcfc = &pcmc->face[index];

		dotLp2d[camid].x = pcfc->lowerdot.cdotL.x;
		dotLp2d[camid].y = pcfc->lowerdot.cdotL.y;
		dotLp2d[camid].z = 0.0;

		pp_ = &pcfc->upperline.clineL.p0;
		pm_ = &pcfc->upperline.clineL.m;

		barLp2d0[camid].x = pp_->x;
		barLp2d0[camid].y = pp_->y;
		barLp2d0[camid].z = 0.0;

		barlineangle = atan2(pm_->x, pm_->y);

		dx = sin(barlineangle) * BARDXDYLENGTH;
		dy = cos(barlineangle) * BARDXDYLENGTH;
		barLp2d1[camid].x = barLp2d0[camid].x + dx;
		barLp2d1[camid].y = barLp2d0[camid].y + dy;
		barLp2d1[camid].z = 0.0;		
	}

	ComputeData_Extdata_3DLines(piana, &dotLp3d, dotLp2d, barLp2d0, barLp2d1, &hline3d, &vline3d, &uh, &uv);

	ComputeData_Extdata_ClubAngles(&hline3d, &vline3d, &clubfaceangle, &clubloftangle, &clublieangle);	
	
	// faceimpactLateral , faceimpactVertical
	clubfaceimpactLateral = 0;
	clubfaceimpactVertical = 0;

	{
		// get intersection point of vertical line and horizontal line.
		iana_cam_t	*pic;
		cr_point_t p0, p1;
		cr_point_t *pb3d;				// ball 3d position
		cr_point_t b3d_shifted;			// shifted ball 3d position, with 
		cr_plane_t faceplaneh, faceplanev;

		cr_point_t clubcenterpoint;
		cr_point_t impactpoint;

		cr_vector_t impactoffsetvector;
		cr_point_t vtmp;

		I32 indexshot;
		cr_point_t *pdotPos, *pdotPosShot;
		cr_vector_t shiftvector;		// 

		//--

		indexshot = pcmc->indexshot;
		pdotPos = &pcfc->lowerdot.cdotL3dE;
		pdotPosShot = &pcmc->face[indexshot].lowerdot.cdotL3dE;
		
		cr_vector_sub(pdotPos, pdotPosShot, &shiftvector);
		
		cr_point_line_line(				// line-line intersection point
				&hline3d,
				&vline3d,
				&p0,					// use p0
				&p1);

		// get club center point  (bellow 0.005 m = 5 mm)
		cr_vector_add(&p0, &dotLp3d, &clubcenterpoint);
		cr_vector_scalar(&clubcenterpoint, 0.5, &clubcenterpoint);
		cr_vector_scalar(&uv, CENTER_ZOFFSET, &vtmp);
		cr_vector_add(&clubcenterpoint, &vtmp, &clubcenterpoint);

		// make faceplane with hline3d and vline3d
		cr_plane_line0line1(&hline3d, &vline3d, &faceplaneh, &faceplanev);

		// get impact point
		pic = piana->pic[0];			// 
		pb3d = &pic->icp.b3d;			// ball 3d position

		cr_vector_add(pb3d, &shiftvector, &b3d_shifted);

		//cr_footPerpendicular_Plane(pb3d, &faceplaneh, &impactpoint);	// with face plane of H.
		cr_footPerpendicular_Plane(&b3d_shifted, &faceplaneh, &impactpoint);	// with face plane of H.

		// get deviation vector between center and impact position
		cr_vector_sub(&impactpoint, &clubcenterpoint, &impactoffsetvector); 

		clubfaceimpactLateral = cr_vector_inner(&impactoffsetvector, &uh);
		clubfaceimpactLateral *= 1000.0;		// to mm
		clubfaceimpactVertical = cr_vector_inner(&impactoffsetvector, &uv);
		clubfaceimpactVertical *= 1000.0;		// to mm
	}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" t: %lf faceangel: %lf   loft: %lf lie: %lf Lateral: %lf Vertical: %lf\n",
			t0,
			clubfaceangle, clubloftangle, clublieangle,
			clubfaceimpactLateral,
			clubfaceimpactVertical
			);

	*pclubfaceangle 			= clubfaceangle;
	*pclubloftangle 			= clubloftangle;
	*pclublieangle 				= clublieangle;
	*pclubfaceimpactLateral 	= clubfaceimpactLateral;
	*pclubfaceimpactVertical 	= clubfaceimpactVertical;

	res = 1;
func_exit:
	return res;
}

static I32 ComputeData_Topbar_Extdata(
		iana_t *piana, clubmark_t *pcm,
		double *pclubfaceangle,
		double *pclubfaceimpactLateral)
{
	I32 res;
//	cr_point_t dotLp2d[2];				// Dot fixed points 2d at impact.
	cr_point_t barLp2d0[MAXCAMCOUNT];				// bar fixed points 2d at impact.
	cr_point_t barLp2d1[MAXCAMCOUNT];				// bar another points 2d at impact.
	double barlineangle[MAXCAMCOUNT];

//	cr_point_t dotLp3d;					// Dot fixed point 3d at impact.
	cr_point_t barLp3d0;				// topbar fixed point 3d at impact.
	cr_point_t barLp3d1;				// bar another point 3d at impact.
//	cr_line_t vline3d;					// Vertical line 3d of club face
//	cr_vector_t uv;						// directional unit vector of vertical line, Z-axis positive.
	cr_line_t topbar3dline;				// TopBar 3d line 3d of club face
	cr_vector_t uh;						// directional unit vector of topbar3dline.

	double t0;
	clubmark_cam_t *pcmc;
//	clubmark_Lp_t *pdotLp;
	clubmark_Lp_t *pbarLp;

	double clubfaceangle;
	double clubloftangle;
	double clublieangle;
	double clubfaceimpactLateral;
	double clubfaceimpactVertical;

	U32 camid;

	//---------------------------------

	
	clubfaceangle 			= 0;
	clubloftangle 			= 0;
	clublieangle 			= 0;
	clubfaceimpactLateral 	= 0;
	clubfaceimpactVertical 	= 0;	

	t0 = TOPBAR_EXTDATA_IMPACT;
//	for(t0 = TOPBAR_EXTDATA_IMPACT0; t0 < TOPBAR_EXTDATA_IMPACT + TOPBAR_EXTDATA_IMPACT_D/2.0; t0 += TOPBAR_EXTDATA_IMPACT_D) 
	{
		for (camid = 0; camid < NUM_CAM; camid++) {
			double xma[3], yma[3], thma[3];
			double dx, dy;
			pcmc = &pcm->cmc[camid];

			pbarLp = &pcmc->barLp[0];
			memcpy(&xma[0], &pbarLp->xma[0], sizeof(double) * 3);
			memcpy(&yma[0], &pbarLp->yma[0], sizeof(double) * 3);
			memcpy(&thma[0], &pbarLp->thma[0], sizeof(double)*3);

			barLp2d0[camid].x = xma[2] * t0*t0 + xma[1] * t0 + xma[0];	
			barLp2d0[camid].y = yma[2] * t0*t0 + yma[1] * t0 + yma[0] + BARLP_DY;
			barLp2d0[camid].z = 0.0;

			barlineangle[camid] = thma[2] * t0*t0 + thma[1] * t0 + thma[0];

			dx = sin(barlineangle[camid]) * BARDXDYLENGTH;
			dy = cos(barlineangle[camid]) * BARDXDYLENGTH;
			barLp2d1[camid].x = barLp2d0[camid].x + dx;
			barLp2d1[camid].y = barLp2d0[camid].y + dy;
			barLp2d1[camid].z = 0.0;
		}

		// get 3d points
		getp3d(piana, &barLp2d0[0], &barLp3d0);		// barLp3d0 is center of topbar line.
		getp3d(piana, &barLp2d1[0], &barLp3d1);

		cr_line_p0p1(&barLp3d0, &barLp3d1, &topbar3dline);  // 3d line 3d of Topbar

		cr_vector_normalization(&topbar3dline.m, &uh);	  // directional unit vector of topbar3dline.
		if (uh.x < 0) {
			cr_vector_scalar(&uh, -1, &uh);
		}

		memcpy(&topbar3dline.p0, &barLp3d0, sizeof(cr_point_t));

		//---
		// TOPBAR line 3d of club face.   faceangle, faceimpactLateral 
		{
			double dx, dy, dxy;

			dx = topbar3dline.m.x;
			dy = topbar3dline.m.y;
			//dz = topbar3dline.m.z;

			dxy = sqrt(dx*dx + dy*dy);

			clubfaceangle = -atan2(dy, dx);
			clubfaceangle = RADIAN2DEGREE(clubfaceangle);
			if (clubfaceangle < -90) {clubfaceangle = clubfaceangle + 180;}
			if (clubfaceangle > +90) {clubfaceangle = clubfaceangle - 180;}

			clubfaceimpactLateral = 0;
		}

		{
			iana_cam_t	*pic;
			//cr_point_t p0, p1;
			cr_point_t *pb3d;				// ball 3d position
			cr_line_t zline3d;					// z-axis line.
			cr_point_t zzz;
			cr_plane_t faceplanetopbar, faceplanezline;
			//cr_plane_t faceplaneh, faceplanev;

			cr_point_t clubcenterpoint;
			cr_point_t impactpoint;

			cr_vector_t impactoffsetvector;
			//cr_point_t vtmp;

			//--
			memcpy(&clubcenterpoint, &barLp3d0, sizeof(cr_point_t));
			// make faceplane with hline3d and vline3d
			zzz.x = barLp3d0.x;
			zzz.y = barLp3d0.y;
			zzz.z = barLp3d0.z + 1.0;
			cr_line_p0p1(&barLp3d0, &zzz, &zline3d);			// z-axis only line.
			
			cr_plane_line0line1(&topbar3dline, &zline3d, &faceplanetopbar, &faceplanezline);

			// get impact point
			pic = piana->pic[0];			// 
			pb3d = &pic->icp.b3d;			// ball 3d position
			cr_footPerpendicular_Plane(pb3d, &faceplanetopbar, &impactpoint);	// with face plane of H.

			// get deviation vector between center and impact position
			cr_vector_sub(&impactpoint, &clubcenterpoint, &impactoffsetvector); 

			clubfaceimpactLateral = cr_vector_inner(&impactoffsetvector, &uh);
			clubfaceimpactLateral *= 1000.0;		// to mm
//			clubfaceimpactVertical = cr_vector_inner(&impactoffsetvector, &uv);
//			clubfaceimpactVertical *= 1000.0;		// to mm
		}
	}

	*pclubfaceangle 			= clubfaceangle;
	//*pclubloftangle 			= clubloftangle;
	//*pclublieangle 				= clublieangle;
	*pclubfaceimpactLateral 	= clubfaceimpactLateral;
	//*pclubfaceimpactVertical 	= clubfaceimpactVertical;

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" t: %lf faceangel: %lf Lateral: %lf \n",
			t0,
			clubfaceangle, 
			clubfaceimpactLateral
			);
	res = 1;
	return res;
}

static I32 ComputeData_Topbar_Extdata2(
		iana_t *piana, clubmark_t *pcm,
		double *pclubfaceangle,
		double *pclubfaceimpactLateral)
{
	I32 res;

	I32 index;
	I32 index0, index1;

	U32 camid;
	//--
	index0 = pcm->cmc[0].index0;
	if (index0 > pcm->cmc[0].index0) {
		index0 = pcm->cmc[0].index0;
	}
	index1 = pcm->cmc[0].index1;
	if (index1 < pcm->cmc[1].index1) {
		index1 = pcm->cmc[1].index1;
	}

	res = 0;
	for (index = index1; index >= index0; index--) {
		for (camid = 0; camid < NUM_CAM; camid++) {
			res =  ComputeData_Topbar_Extdata_Index(
					piana, pcm,
					camid,					// 0, 1...
					index,					// index
					pclubfaceangle,
					pclubfaceimpactLateral);
			if (res != 0) {
				break;		// successs!!!!!
			}
		}
		if (res != 0) {
			break;
		}
	}

	return res;
}


static I32 ComputeData_Topbar_Extdata_Index(
		iana_t *piana, clubmark_t *pcm,
		U32 camid,					// 0, 1...
		U32 index,					// index
		double *pclubfaceangle,
		double *pclubfaceimpactLateral)
{
	I32 res;
	//cr_point_t dotLp2d[2];				// Dot fixed points 2d at impact.
	cr_point_t barLp2d0[MAXCAMCOUNT];				// bar fixed points 2d at impact.
	cr_point_t barLp2d1[MAXCAMCOUNT];				// bar another points 2d at impact.
	double barlineangle[MAXCAMCOUNT];

	//cr_point_t dotLp3d;					// Dot fixed point 3d at impact.
	cr_point_t barLp3d0;				// bar fixed point 3d at impact.
	cr_point_t barLp3d1;				// bar another point 3d at impact.
	//cr_line_t vline3d;					// Vertical line 3d of club face
	//cr_vector_t uv;						// directional unit vector of vertical line, Z-axis positive.
	//cr_line_t hline3d;					// Horizontal line 3d of club face
	cr_line_t topbar3dline;				// TopBar 3d line 3d of club face
	cr_vector_t uh;						// directional unit vector of horizontal line, X-axis positive.

	double t0;
	clubmark_cam_t *pcmc;
	clubmark_cam_t *pcmcO;
//	clubmark_Lp_t *pdotLp;
	clubmark_Lp_t *pbarLp;

	double clubfaceangle;
//	double clubloftangle;
//	double clublieangle;
	double clubfaceimpactLateral;
//	double clubfaceimpactVertical;

	U32 camidO;			// with regression coeff.

	clubmark_face_t *pcfc;
	clubmark_feature_t	*pcft;

	double nowtd;
	//---------------------------------

	if (camid == 0) {
		camidO= 1;
	} else if (camid == 1) {
		camidO= 0;
	} else {
		res = 0;
		goto func_exit;
	}
	
	pcmc = &pcm->cmc[camid];
	
	clubfaceangle 			= 0;
//	clubloftangle 			= 0;
//	clublieangle 			= 0;
	clubfaceimpactLateral 	= 0;
//	clubfaceimpactVertical 	= 0;




	pcft = &pcmc->cft[index];
	pcfc = &pcmc->face[index];

	if (pcfc->upperline.valid == 0) {
		res = 0;
		goto func_exit;
	}

	nowtd = pcft->tsdiffd;

	pcmcO = &pcm->cmc[camidO];				// OTHER camd.
	if (pcmcO->barLp[0].mavalid == 0) {		// regression coeff. must be good for OTHER cam.
		res = 0;
		goto func_exit;
	}

	{		// OTHER CAM. REGRESSION-BASED..
		double xma[3], yma[3], thma[3];
		double dx, dy;

		t0 = nowtd;
		pcmcO = &pcm->cmc[camidO];
		//pdotLp = &pcmcO->dotLp;
		//memcpy(&xma[0], &pdotLp->xma[0], sizeof(double) * 3);
		//memcpy(&yma[0], &pdotLp->yma[0], sizeof(double) * 3);

		//dotLp2d[camidO].x = xma[2] * t0*t0 + xma[1] * t0 + xma[0];
		//dotLp2d[camidO].y = yma[2] * t0*t0 + yma[1] * t0 + yma[0];
		//dotLp2d[camidO].z = 0.0;

		pbarLp = &pcmcO->barLp[0];
		memcpy(&xma[0], &pbarLp->xma[0], sizeof(double) * 3);
		memcpy(&yma[0], &pbarLp->yma[0], sizeof(double) * 3);
		memcpy(&thma[0], &pbarLp->thma[0], sizeof(double)*3);

		barLp2d0[camidO].x = xma[2] * t0*t0 + xma[1] * t0 + xma[0];
		barLp2d0[camidO].y = yma[2] * t0*t0 + yma[1] * t0 + yma[0];
		barLp2d0[camidO].z = 0.0;

		barlineangle[camidO] = thma[2] * t0*t0 + thma[1] * t0 + thma[0];

		dx = sin(barlineangle[camidO]) * BARDXDYLENGTH;
		dy = cos(barlineangle[camidO]) * BARDXDYLENGTH;
		barLp2d1[camidO].x = barLp2d0[camidO].x + dx;
		barLp2d1[camidO].y = barLp2d0[camidO].y + dy;
		barLp2d1[camidO].z = 0.0;
	}

	{		// Major CAM. Original points
		//double xma[3], yma[3], thma[3];
		double dx, dy;
		cr_point_t *pp_;
		cr_vector_t *pm_;

		t0 = nowtd;
		pcmc = &pcm->cmc[camid];
		//pdotLp = &pcmc->dotLp;

		pcft = &pcmc->cft[index];
		pcfc = &pcmc->face[index];

		//dotLp2d[camid].x = pcfc->lowerdot.cdotL.x;
		//dotLp2d[camid].y = pcfc->lowerdot.cdotL.y;
		//dotLp2d[camid].z = 0.0;

		pp_ = &pcfc->upperline.clineL.p0;
		pm_ = &pcfc->upperline.clineL.m;

		barLp2d0[camid].x = pp_->x;
		barLp2d0[camid].y = pp_->y;
		barLp2d0[camid].z = 0.0;

		barlineangle[camid] = atan2(pm_->x, pm_->y);

		dx = sin(barlineangle[camid]) * BARDXDYLENGTH;
		dy = cos(barlineangle[camid]) * BARDXDYLENGTH;
		barLp2d1[camid].x = barLp2d0[camid].x + dx;
		barLp2d1[camid].y = barLp2d0[camid].y + dy;
		barLp2d1[camid].z = 0.0;
	}

	// get 3d points
	//getp3d(piana, &dotLp2d[0], &dotLp3d);	
	getp3d(piana, &barLp2d0[0], &barLp3d0);
	getp3d(piana, &barLp2d1[0], &barLp3d1);

	cr_line_p0p1(&barLp3d0, &barLp3d1, &topbar3dline);  // 3d line 3d of Topbar

	cr_vector_normalization(&topbar3dline.m, &uh);	  // directional unit vector of topbar3dline.
	if (uh.x < 0) {
		cr_vector_scalar(&uh, -1, &uh);
	}

	memcpy(&topbar3dline.p0, &barLp3d0, sizeof(cr_point_t));

	//---
	// TOPBAR line 3d of club face.   faceangle, faceimpactLateral 
	{
		double dx, dy, dxy;

		dx = topbar3dline.m.x;
		dy = topbar3dline.m.y;
		//dz = hline3d.m.z;

		dxy = sqrt(dx*dx + dy*dy);

		clubfaceangle = -atan2(dy, dx);
		clubfaceangle = RADIAN2DEGREE(clubfaceangle);
		if (clubfaceangle < -90) {clubfaceangle = clubfaceangle + 180;}
		if (clubfaceangle > +90) {clubfaceangle = clubfaceangle - 180;}

		clubfaceimpactLateral = 0;
	}


	{
		iana_cam_t	*pic;
		cr_point_t p0, p1;
		cr_point_t *pb3d;				// ball 3d position
		cr_point_t b3d_shifted;			// shifted ball 3d position, with 

		cr_line_t zline3d;					// z-axis line.
		cr_point_t zzz;
		cr_plane_t faceplanetopbar, faceplanezline;	
		
		cr_point_t clubcenterpoint;
		cr_point_t impactpoint;

		cr_vector_t impactoffsetvector;
		//cr_point_t vtmp;

		I32 indexshot;
		cr_point_t *pbarPos, *pbarPosShot;
		cr_vector_t shiftvector;		// 

		//--

		indexshot = pcmc->indexshot;
		pbarPos = &pcfc->upperline.clineLp03dE;
		//pdotPosShot = &pcmc->face[indexshot].lowerdot.cdotL3dE;
		pbarPosShot = &pcmc->face[indexshot].upperline.clineLp03dE;
		cr_vector_sub(pbarPos, pbarPosShot, &shiftvector);
	

		// make faceplane with hline3d and vline3d
		zzz.x = barLp3d0.x;
		zzz.y = barLp3d0.y;
		zzz.z = barLp3d0.z + 1.0;
		cr_line_p0p1(&barLp3d0, &zzz, &zline3d);			// z-axis only line.


		cr_point_line_line(				// line-line intersection point
				&topbar3dline,
				&zline3d,
				&p0,					// use p0
				&p1);

		// get club center point  (bellow 0.005 m = 5 mm)
		cr_vector_add(&p0, &barLp3d0, &clubcenterpoint);
		cr_vector_scalar(&clubcenterpoint, 0.5, &clubcenterpoint);		// (topbarcenter + crosspoint) / 2

		// make faceplane with hline3d and vline3d
		cr_plane_line0line1(&topbar3dline, &zline3d, &faceplanetopbar, &faceplanezline);


		// get impact point
		pic = piana->pic[0];			// 
		pb3d = &pic->icp.b3d;			// ball 3d position

		cr_vector_add(pb3d, &shiftvector, &b3d_shifted);

		//cr_footPerpendicular_Plane(pb3d, &faceplaneh, &impactpoint);	// with face plane of H.
		cr_footPerpendicular_Plane(&b3d_shifted, &faceplanetopbar, &impactpoint);	// with face plane of H.

		// get deviation vector between center and impact position
		cr_vector_sub(&impactpoint, &clubcenterpoint, &impactoffsetvector); 

		clubfaceimpactLateral = cr_vector_inner(&impactoffsetvector, &uh);
		clubfaceimpactLateral *= 1000.0;		// to mm
//		clubfaceimpactVertical = cr_vector_inner(&impactoffsetvector, &uv);
//		clubfaceimpactVertical *= 1000.0;		// to mm
	}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" t: %lf faceangel: %lf Lateral: %lf \n",
			t0,
			clubfaceangle, 
			clubfaceimpactLateral
			);

	*pclubfaceangle 			= clubfaceangle;
	*pclubfaceimpactLateral 	= clubfaceimpactLateral;

	res = 1;
func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      get 3d world coordinated point from 2 normalized cam coordi pts from two camera, one from each.
 *
 *  @param[in]	piana
 *              IANA module handler
 *  @param[in]	point[2]
 *              2 points, one from each cam.
 *  @param[out]	p3d
 *              3d point
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2019/1210
 *******************************************************************************/
I32 getp3d(iana_t *piana, cr_point_t point[2], cr_point_t *p3d)
{
	I32 res;
	iana_cam_t	*pic;
	cr_point_t *pcampos[MAXCAMCOUNT];	// Camera position
	U32 camid;
	cr_line_t  linec2p[MAXCAMCOUNT];
	cr_point_t pts3d[2];

	//--
	for (camid = 0; camid < NUM_CAM; camid++) {
        pic 	= piana->pic[camid];	
		pcampos[camid] = &pic->icp.CamPosL;
		cr_line_p0p1(&point[camid], pcampos[camid], &linec2p[camid]);
	}

	cr_point_line_line(&linec2p[0], &linec2p[1], &pts3d[0], &pts3d[1]);

	memcpy(p3d, &pts3d[0], sizeof(cr_point_t));	

	res = 1;

	return res;
}


// get timestamp and time difference
static I32 GetClubmarkTimestamp(iana_t *piana, clubmark_t *pcm)
{
	I32 res;
	double tsdiff;				// ts_of_cam0 - ts_of_cam1
//	clubmark_cam_t  cmc[2];
	clubmark_cam_t *pcmc;
	clubmark_feature_t	*pcft;
	camif_buffer_info_t *pb;
	U32 camid;
	U32 foundit;
	U32 indexshot;
	U32 i, j;
	U64		ts64;

	U32 indexcount[MAXCAMCOUNT];
	//--
	res = 0;
	pb = NULL;

	
	for (camid = 0; camid < NUM_CAM; camid++) {
#define INDEXCOUNT_ADD	4
		indexcount[camid] = pcm->cmc[camid].index1 + INDEXCOUNT_ADD;
		if (piana->camsensor_category == CAMSENSOR_EYEXO) {
			if (indexcount[camid] > MARKSEQUENCELEN_EYEXO) {
				indexcount[camid] = MARKSEQUENCELEN_EYEXO;
			}
		} else {
			if (indexcount[camid] > MARKSEQUENCELEN) {
				indexcount[camid] = MARKSEQUENCELEN;
			}
		}
	}
	
	for (camid = 0; camid < NUM_CAM; camid++) {
		pcmc = &pcm->cmc[camid];

		for (i = 0; i < indexcount[camid]; i++) {
			for (j = 0; j < 20; j++) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, i, &pb, NULL);
				if (res == 1) {
					break;
				}
				if (piana->opmode == IANA_OPMODE_FILE) {
					break;
				}
				cr_sleep(2);
			}
			if (res == 0) {
				continue;
			}
			ts64 		= MAKEU64(pb->ts_h, pb->ts_l);
			//pcmc->tsd[i] = ts64 /TSSCALE_D; 

			pcft = &pcmc->cft[i];
			pcft->tsd = ts64 /TSSCALE_D; 
			
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%2d] tsd = %lf\n", camid, i, pcmc->tsd[i]);
		}
	}

	tsdiff = 0;
	foundit = 0;
	j = 0;
	for (i = 0; i < indexcount[0]; i++) {
		j = piana->bulkpairindex[i];
		if (j != BULKPAIRINDEX_NONE) {
			foundit = 1;
			break;
		}
	}
	if (foundit) {
		//pcft->tsd = ts64 /TSSCALE_D; 

		//tsdiff = pcm->cmc[0].tsd[i] - pcm->cmc[1].tsd[j];
		tsdiff = pcm->cmc[0].cft[i].tsd - pcm->cmc[1].cft[j].tsd;

		pcmc = &pcm->cmc[0];
		for (i = 0; i < indexcount[0]; i++) {
			pcft = &pcmc->cft[i];
			pcft->tsd = pcft->tsd - tsdiff;				// shift ts of cam0  to cam1
			//pcmc->tsd[i] = pcmc->tsd[i] - tsdiff;				// shift ts of cam0  to cam1
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%2d] shifted tsd = %lf\n", 0, i, pcmc->tsd[i]);
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%2d] shifted tsd = %lf\n", 0, i, pcft->tsd);				// shift ts of cam0  to cam1
		}
	}
	pcm->tsdiff = tsdiff;

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "pcm->tsdiff: %lf (%lf)\n", tsdiff, (tsdiff*1000));

	for (camid = 0; camid < NUM_CAM; camid++) {
		indexshot = piana->bulkshotindex[camid];
		//pcm->tsdshotcam[camid] = pcm->cmc[camid].tsd[indexshot];
		pcm->tsdshotcam[camid] = pcm->cmc[camid].cft[indexshot].tsd;
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] tsdshotcam = %lf at %d\n", camid, pcm->tsdshotcam[camid], indexshot);
	}

	for (camid = 0; camid < NUM_CAM; camid++) {
		pcmc = &pcm->cmc[camid];
		for (i = 0; i < indexcount[camid]; i++) {
			pcft = &pcmc->cft[i];
			pcft->tsdiffd = pcft->tsd - pcm->tsdshotcam[1];
			//pcmc->tsdiffd[i] = pcmc->tsd[i] - pcm->tsdshotcam[1];
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%2d] tsd delta = %lf\n", camid, i, pcmc->tsdiffd[i]);
		}
	}

	res = 1;
	return res;
}


static I32 RecognizeClubmark(iana_t *piana, U32 camid, clubmark_t *pcm)
{
	I32 res;
	U32 i, j;
	camif_buffer_info_t *pb;
	camimageinfo_t     	*pcii;
	iana_cam_t	*pic;

	U08 	*buf;
	U32		width, height;
	U32		width2, height2;
	U32 offset_x, offset_y;
	U64		ts64;

	point_t Pp;
	double Pr;
	I32 bx, by;			// ball position
	I32 sx;
	I32 wb;

	U32 index0; 
	U32 indexshot; 
	U32 index1;

	double	xfact, yfact; 
	I32 sx2;
	I32 wb2;
	U08 *bufBin;
	U08 *bufLabel;
	U08 *bufEdge;
	U08 *bufProc;

	clubmark_seg_t		clm[CLUBMARK_MAXLABEL+1];

	clubmark_dot_t *pcmd;
	clubmark_bar_t *pcmb;

	clubmark_cam_t *pcmc;

	clubmark_feature_t	*pcft;

	//--
	U08 *procimg;
	U32 multitude;
	U32 skip, syncdiv;
	U64 period;
	U64 periodm;
	
	U32 m;

	U32 normalbulk;
	
    //-----------
	pcmc = &pcm->cmc[camid];
	pic 	= piana->pic[camid];	
	Pp.x	= pic->icp.P.x;
	Pp.y	= pic->icp.P.y;

	procimg = pic->pimgFull;


	bufBin 	= pic->pimg[4];
	bufLabel= pic->pimg[3];
	bufEdge= pic->pimg[2];

	normalbulk = pcmc->normalbulk;

	{
		point_t Pp, Pp0;
		point_t Lp, Lp0;
		double dx, dy;


		Pp.x	= pic->icp.P.x;
		Pp.y	= pic->icp.P.y;

		iana_P2L(piana, camid, &Pp, &Lp, 0);
		Lp0.x = Lp.x; Lp0.y = Lp.y + 0.01;				// 1cm
		iana_L2P(piana, camid, &Lp0, &Pp0, 0);

		dx = Pp0.x - Pp.x;
		dy = Pp0.y - Pp.y;
		pcmc->l1cm = sqrt(dx*dx + dy*dy);
		/*
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                "P(%lf, %lf) -> L(%lf, %lf) -> L0(%lf, %lf) -> P0(%lf, %lf) L: %lf\n",
				Pp.x, Pp.y, Lp.x, Lp.y, 
				Lp0.x, Lp0.y, Pp0.x, Pp0.y, 
				//g_l1cm[camid]
				pcmc->l1cm);
		*/
	}


	index0 = pcmc->index0;
	index1 = pcmc->index1;
	indexshot = pcmc->indexshot;

	width = 0; height = 0;
	width2 = 0; height2 = 0;

	res = 0;
	pb = NULL;
	buf = NULL;
	
	for (i = index0; i <= index1; i++) {
		for (j = 0; j < 20; j++) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, i, &pb, &buf);
			if (res == 1) {
				break;
			}
			cr_sleep(2);
		}
		
		if (res == 0) {
			break;
		}
		pcii 	= &pb->cii;
		width 	= pcii->width;
		height 	= pcii->height;

		period = TSSCALE / (U64) pcii->framerate;			// nsec
		if (normalbulk == NORMALBULK_NORMAL) {
			multitude 	= pcii->multitude;
			if (multitude == 0) {
				multitude = 1;
			}
			skip 	= pcii->skip;
			if (skip == 0) {
				skip = 1;
			}
			syncdiv = pcii->syncdiv;

			periodm = period * skip * syncdiv;
			
		} else {
			multitude = 1;
		}

		offset_x= pcii->offset_x;
		offset_y= pcii->offset_y;

		bx 		= (I32)Pp.x - (I32)offset_x;
		by 		= (I32)Pp.y - (I32)offset_y;

		wb 		= bx - width;
		if 		(wb < (I32)(width * 0.6)) {
			wb	= (I32)(width * 0.6);
		}
		wb		= (wb - wb % 4);
		sx		= width - wb - 1;

		xfact = 2.0;
		yfact = 2.0;

		if (camid == 1) 
		{
			xfact = 3.0;
			yfact = 3.0;
		}

		pcmc->xfact = xfact;
		pcmc->yfact = yfact;

		pcft = &pcmc->cft[i];
		pcft->offset_x = offset_x;
		pcft->offset_y = offset_y;

		pcmd = &pcft->cmd;
		pcmb = &pcft->cmb;

		ts64 		= MAKEU64(pb->ts_h, pb->ts_l);
		/*
		pcmc->tsd[i] = ts64 /TSSCALE_D; 
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                "[%d:%2d] tsd = %lf\n", camid, i, pcmc->tsd[i]);
		*/

		EstimateBallPosition(piana, camid, ts64, i, indexshot, &Pp, &Pr);
		
		/*
		memcpy(&pcmc->Pp[i], &Pp, sizeof(point_t));
		pcmc->Pr[i] = Pr;
		*/
		memcpy(&pcft->Pp, &Pp, sizeof(point_t));
		pcft->Pr = Pr;


		for (m = 0; m < multitude; m++) {
			if (multitude == 1) {
				procimg = buf;
			} else {
				procimg = buf + pcii->width * ((m*pcii->height) / multitude);
			}

			BinarizeClubmarkImage(
					piana, camid, 
					pcm, 
					procimg,				// buf,
					pb,
					xfact,
					yfact,
					multitude,

					&width2,
					&height2,
					&sx2,
					&wb2,
					bufBin,
					bufEdge,
					&pcft->Pp,
					pcft->Pr
					//, &pcmc->Pp[i], pcmc->Pr[i]
					);

			if (s_usebufEdge) {
				bufProc = bufEdge;
			} else {
				bufProc = bufBin;
			}
			memcpy(bufLabel, bufProc, width2*height2);
			LabelingClubmark(piana, camid, pcm, width2, height2, sx2, wb2, bufLabel, bufProc, clm);
			SelectClubmark(piana, camid, pcm, width2, height2, (bx*xfact), (by*yfact), (Pr*xfact), 
					xfact, yfact,
					pcmc->l1cm,
					bufProc, clm);

            GetBarDotCount(piana, camid, pcm, clm, i);

#if defined DEBUG_CLUB_MARK
			{
				double x, y, r;
				
                x = Pp.x * xfact - (offset_x * xfact +  0 /*sx2*/);
				y = Pp.y * yfact - (offset_y * yfact +  0);
				r = Pr * xfact;

				mycircleGray((int) x, (int) y, (int) r, 0xFF, (U08 *)pic->pimg[1],  width2 /*wb2*/);
				mycircleGray((int) x, (int) y, (int) r, 0xFF, (U08 *)pic->pimg[2],  width2 /*wb2*/);
				//mycircleGray((int) x, (int) y, (int) r, 0xFF, (U08 *)bufEdge, 		width2/*wb2*/);
				mycircleGray((int) x, (int) y, (int) r, 0xFF, (U08 *)bufProc, 		width2/*wb2*/);

				mycircleGray((int) x, (int) y, (int) r, 0xFF, (U08 *)pic->pimg[0], width2);
			}
			{
				imagesegment_t 	*pis;
				rectObj_t *pro;

				int x[4], y[4];
				U32 i0;

				//--
				for (i0 = 0; i0 <pcmd->count; i0++) {
					clubmark_seg_t	*pclm;

					pclm = &pcmd->clm[i0];

					pis = &pclm->imgseg;
					pro = &pclm->rectObj;

					x[0] = pis->ixl; y[0] = pis->iyu;
					x[1] = pis->ixr; y[1] = pis->iyu;
					x[2] = pis->ixr; y[2] = pis->iyb;
					x[3] = pis->ixl; y[3] = pis->iyb;

					mydrawpolygonGray(x, y, 4, 0x80, bufProc, width2);
				}

				for (i0 = 0; i0 <pcmb->count; i0++) {
					pis = &pcmb->clm[i0].imgseg;
					pro = &pcmb->clm[i0].rectObj;
					x[0] = pis->ixl; y[0] = pis->iyu;
					x[1] = pis->ixr; y[1] = pis->iyu;
					x[2] = pis->ixr; y[2] = pis->iyb;
					x[3] = pis->ixl; y[3] = pis->iyb;

					mydrawpolygonGray(x, y, 4, 0xFF, bufProc, width2);

					{
						U32 k;
						U08 yvalue;
						for (k = 0; k < 4; k++) {
							x[k] = (int)pro->p[k].x; y[k] = (int)pro->p[k].y;
							yvalue = (U08) ((0xFF * (4 - k)) / 4);
							mycircleGray((int)x[k], (int)y[k], 4, yvalue, bufProc, width2);
						}
					}
				}

				for (i0 = 0; i0 <pcmb->count; i0++) {
					clubmark_seg_t	*pclm;
					point_t Pp, Lp;
					I32 index;

					//--
					pclm = &pcmb->clm[i0];
					if (pclm->ccvalid) {
						for (index = 0; index < CLUBMARK_BAR_CLASSCOUNT; index++) {
							Pp.x = pclm->ccP[index].x;
							Pp.y = pclm->ccP[index].y;

							Pp.x = Pp.x / xfact + offset_x;
							Pp.y = Pp.y / yfact + offset_y;

							iana_P2L(piana, camid, &Pp, &Lp, 0);

							pclm->ccL[index].x = Lp.x;
							pclm->ccL[index].y = Lp.y;
							pclm->ccL[index].z = 0;
						}


						for (index = 0; index < CLUBMARK_BAR_CLASSCOUNT; index++) {
							I32 xx, yy;

							//---
							xx = (I32) (pclm->ccP[index].x);
							yy = (I32) (pclm->ccP[index].y);
							//mycircleGray(xx, yy, 2, (U08) (200 - index * 20), bufEdge, width2); 
							mycircleGray(xx, yy, 2, (U08) (200 - index * 20), bufProc, width2); 
						}
					}
				}
			}
			if (piana->himgfunc) {
				//((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf,			width,	height, 0, 1, 2, 3, piana->imguserparam);
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)procimg,			width,	height, 0, 1, 2, 3, piana->imguserparam);
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pic->pimg[0], width2,	height2, 4, 1, 2, 3, piana->imguserparam);
				//((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)bufBin, width2 /*wb2*/,	height2, 4, 1, 2, 3, piana->imguserparam);
				//((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)bufEdge, width2 /*wb2*/,	height2, 2, 1, 2, 3, piana->imguserparam);
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)bufProc, width2 /*wb2*/,	height2, 2, 1, 2, 3, piana->imguserparam);

			}
#endif
		}
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "{{{%d}}}\n", i);
	}

	ExileClubmarkData(piana, camid, pcm, width2, height2, 1, 1);

	GetClubmarkRegion(piana, camid, pcm);

	if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_BARDOT || pcm->clubcalc_method == CLUBCALC_CLUB_MARK_TOPBAR) {
		GetFaceData(piana, camid, 0);
	}

	//	DisplayClubmarkBarDot(piana, camid, pcmc, index0, index1, 1, 0, 1, 0);
	RegressionClubmark(piana, camid, pcm);
	//DisplayClubmarkBarDot(piana, camid, pcm, index0, index1, 1, 1, 1, 1);
	
    if (piana->opmode == IANA_OPMODE_FILE) {
		DisplayClubmarkBarDot(piana, camid, pcm, index0, index1, 1, 1, 1, 1);
	}

	res = 1;

	UNUSED(indexshot);

	return res;
}



int compare_dot(const void *r1, const void *r2) 
{
	int res;

	//--
	point_t *pP1 = (point_t *)r1;
	point_t *pP2 = (point_t *)r2;
	if (pP1->x > pP2->x) {
		res = 1;
	} else if (pP1->x < pP2->x) {
		res = -1;
	} else {
		res = 0;
	}
	return res;
}

int compare_dot_Left(const void *r1, const void *r2) 
{
	int res;

	//--
	point_t *pP1 = (point_t *)r1;
	point_t *pP2 = (point_t *)r2;
	if (pP1->x < pP2->x) {
		res = 1;
	} else if (pP1->x > pP2->x) {
		res = -1;
	} else {
		res = 0;
	}
	return res;
}

int compare_bar(const void *r1, const void *r2) 
{
	int res;

	//--
	point_t *pP1 = (point_t *)r1;
	point_t *pP2 = (point_t *)r2;
	if (pP1->y > pP2->y) {
		res = 1;
	} else if (pP1->y < pP2->y) {
		res = -1;
	} else {
		res = 0;
	}
	return res;
}

int compare_bar_Left(const void *r1, const void *r2) 
{
	int res;

	//--
	point_t *pP1 = (point_t *)r1;
	point_t *pP2 = (point_t *)r2;
	if (pP1->y < pP2->y) {
		res = 1;
	} else if (pP1->y > pP2->y) {
		res = -1;
	} else {
		res = 0;
	}
	return res;
}


static I32 GetFaceData(iana_t *piana, U32 camid, U32 onedotmode)
{
	I32 res;

	clubmark_t *pcm;
	clubmark_cam_t *pcmc;
	clubmark_feature_t	*pcft;
	clubmark_dot_t *pcmd;
	clubmark_bar_t *pcmb;
	clubmark_region_t *pcrd;
	clubmark_region_t *pcrb;
	clubmark_face_t *pcfc;

	imagesegment_t 	*pis;

	point_t Pplower[32];
	point_t Lplower[32];

    U32 index;
	U32 index0; 
	U32 index1;
	
    double	xfact, yfact; 
	U32 offset_x, offset_y;
	U32 lower_for_dot;

	//-----------
	pcm = (clubmark_t *)piana->hcm;
	pcmc = &pcm->cmc[camid];

	xfact = pcmc->xfact;
	yfact = pcmc->yfact;

	index0 = pcmc->index0;
	index1 = pcmc->index1;

	//-- upper line..
	for (index = index0; index <= index1; index++) {
		U32 i0;
		U32 dotcount, /*upperdotcount,*/ lowerdotcount;
		U32 barcount;	// , upperbarcount /*, lowerdotcount*/;
		U32 dotvalid;
		U32 barvalid;
		I32 selectedbar;
		double minx;
		double uppersx, uppersy, upperex, upperey;
		double lowersx, lowersy, lowerex, lowerey;
		point_t *pband;

		pcft = &pcmc->cft[index];
		pcfc = &pcmc->face[index];
		pcmd = &pcft->cmd;
		pcmb = &pcft->cmb;
		pcrd = &pcft->crd;
		pcrb = &pcft->crb;

		offset_x = pcft->offset_x;
		offset_y = pcft->offset_y;
		// get dot count.


		if (piana->Right0Left1 == 0) {		// RIGHT..  20201008
			pband = &pcrb->upperband[0]; 	// Upper for bar

			uppersx = (pcrb->vertex[0].x);
			if (uppersx > pcrb->vertex[1].x) { uppersx = pcrb->vertex[1].x; }

			uppersy = (pcrb->vertex[0].y);
			if (uppersy > pcrb->vertex[1].y) { uppersy = pcrb->vertex[1].y; }

			upperex = (pcrb->vertex[0].x);
			if (upperex < pcrb->vertex[1].x) { upperex = pcrb->vertex[1].x; }

			upperey = (pcrb->vertex[0].y);
			if (upperey < pcrb->vertex[1].y) { upperey = pcrb->vertex[1].y; }
		} else {							// Lower for bar
			pband = &pcrb->lowerband[0];

			uppersx = (pcrb->vertex[2].x);
			if (uppersx > pcrb->vertex[3].x) { uppersx = pcrb->vertex[3].x; }

			uppersy = (pcrb->vertex[2].y);
			if (uppersy > pcrb->vertex[3].y) { uppersy = pcrb->vertex[3].y; }

			upperex = (pcrb->vertex[2].x);
			if (upperex < pcrb->vertex[3].x) { upperex = pcrb->vertex[3].x; }

			upperey = (pcrb->vertex[2].y);
			if (upperey < pcrb->vertex[3].y) { upperey = pcrb->vertex[3].y; }
		}

		if (uppersx > (pband+0)->x) { uppersx = (pband+0)->x; }
		if (uppersx > (pband+1)->x) { uppersx = (pband+1)->x; }

		if (uppersy > (pband+0)->y) { uppersy = (pband+0)->y; }
		if (uppersy > (pband+1)->y) { uppersy = (pband+1)->y; }

		if (upperex < (pband+0)->x) { upperex = (pband+0)->x; }
		if (upperex < (pband+1)->x) { upperex = (pband+1)->x; }

		if (upperey < (pband+0)->y) { upperey = (pband+0)->y; }
		if (upperey < (pband+1)->y) { upperey = (pband+1)->y; }



		if (piana->Right0Left1 == 0) {		// RIGHT..  20201008
			if (onedotmode) {
				lower_for_dot = 0;
			} else {
				lower_for_dot = 1;
			}
		} else {
			if (onedotmode) {
				lower_for_dot = 1;
			} else {
				lower_for_dot = 0;
			}
		}
		if (lower_for_dot) {	// lower for dot
			pband = &pcrd->lowerband[0]; 	// Lower for dot.

			lowersx = (pcrd->vertex[2].x);
			if (lowersx > pcrd->vertex[3].x) 	{ lowersx = pcrd->vertex[3].x; }

			lowersy = (pcrd->vertex[2].y);
			if (lowersy > pcrd->vertex[3].y) 	{ lowersy = pcrd->vertex[3].y; }

			lowerex = (pcrd->vertex[2].x);
			if (lowerex < pcrd->vertex[3].x) 	{ lowerex = pcrd->vertex[3].x; }

			lowerey = (pcrd->vertex[2].y);
			if (lowerey < pcrd->vertex[3].y) 	{ lowerey = pcrd->vertex[3].y; }

		} else {			// upper for dot
			pband = &pcrd->upperband[0];

			lowersx = (pcrd->vertex[0].x);
			if (lowersx > pcrd->vertex[1].x) 	{ lowersx = pcrd->vertex[1].x; }

			lowersy = (pcrd->vertex[0].y);
			if (lowersy > pcrd->vertex[1].y) 	{ lowersy = pcrd->vertex[1].y; }

			lowerex = (pcrd->vertex[0].x);
			if (lowerex < pcrd->vertex[1].x) 	{ lowerex = pcrd->vertex[1].x; }

			lowerey = (pcrd->vertex[0].y);
			if (lowerey < pcrd->vertex[1].y) 	{ lowerey = pcrd->vertex[1].y; }
		}

		if (lowersx > (pband+0)->x) { lowersx = (pband+0)->x; }
		if (lowersx > (pband+1)->x) { lowersx = (pband+1)->x; }

		if (lowersy > (pband+0)->y) { lowersy = (pband+0)->y; }
		if (lowersy > (pband+1)->y) { lowersy = (pband+1)->y; }

		if (lowerex < (pband+0)->x) { lowerex = (pband+0)->x; }
		if (lowerex < (pband+1)->x) { lowerex = (pband+1)->x; }

		if (lowerey < (pband+0)->y) { lowerey = (pband+0)->y; }
		if (lowerey < (pband+1)->y) { lowerey = (pband+1)->y; }

		//-
		dotcount = 0;
		lowerdotcount = 0;
		for (i0 = 0; i0 <pcmd->count; i0++) {
			pis = &pcmd->clm[i0].imgseg;
			if (pcmd->clm[i0].state == CLUBMARK_DOT) {
				dotcount++;
				if (pis->mx > lowersx &&  pis->mx < lowerex
					&& pis->my > lowersy &&  pis->my < lowerey)
				{
					Pplower[lowerdotcount].x = pis->mx;
					Pplower[lowerdotcount].y = pis->my;
					lowerdotcount++;
				}
				//if (piana->Right0Left1 == 0) {		// RIGHT..  20201008
				//	if (pis->mx > lowersx &&  pis->mx < lowerex
				//			&& pis->my > lowersy &&  pis->my < lowerey)
				//	{
				//		Pplower[lowerdotcount].x = pis->mx;
				//		Pplower[lowerdotcount].y = pis->my;
				//		lowerdotcount++;
				//	}
				//} else {		// LEFT
				//	if (pis->mx > uppersx &&  pis->mx < upperex
				//			&& pis->my > uppersy &&  pis->my < upperey)
				//	{
				//		Pplower[lowerdotcount].x = pis->mx;
				//		Pplower[lowerdotcount].y = pis->my;
				//		lowerdotcount++;
				//	}
				//}
			}
		}
		
		pcfc->upperline.valid = 0;
		pcfc->lowerdot.valid = 0;
#define	DOTCOUNT_MIN		1 // 3
#define	LOWERDOTCOUNT_MIN	1 // 2
		if (dotcount >= DOTCOUNT_MIN) {
			dotvalid = 1;
			if (lowerdotcount >= LOWERDOTCOUNT_MIN) {
				qsort(&Pplower[0], lowerdotcount, sizeof(point_t), compare_dot);
				
				for (i0 = 0; i0 < lowerdotcount; i0++) {
					point_t Pp;
					Pp.x = Pplower[i0].x/(xfact) + offset_x; Pp.y = Pplower[i0].y/(yfact) + offset_y;
					Pplower[i0].x = Pp.x;
					Pplower[i0].y = Pp.y;
				
                    iana_P2L(piana, camid, &Pp, &Lplower[i0], 0);
					
                    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                            "[%d : %d / %d] LowerDot P %lf %lf L %lf %lf\n", 
							camid, i0, lowerdotcount, 
							Pplower[i0].x, Pplower[i0].y,
							Lplower[i0].x, Lplower[i0].y);
				}

				{
					memcpy(&pcfc->lowerdot.cdotP, &Pplower[0], sizeof(point_t));
					memcpy(&pcfc->lowerdot.cdotL, &Lplower[0], sizeof(point_t));
				}
				pcfc->lowerdot.valid = 1;
			} else {
				dotvalid = 0;
				pcfc->lowerdot.valid = 0;
			}
		} else {
			pcfc->lowerdot.valid = 0;
		}

#define	BARCOUNT_MIN		1
#define	UPPERBARCOUNT_MIN	1
		//-
		barcount = 0;

		selectedbar = 0;
		minx = 9999;
		for (i0 = 0; i0 <pcmb->count; i0++) {
			pis = &pcmb->clm[i0].imgseg;
			if (pcmb->clm[i0].state == CLUBMARK_BAR) {
				if (barcount == 0 || minx > pis->mx) {
					selectedbar = i0;
					minx = pis->mx;
				}
				barcount++;
			}
		}

		if (barcount >= BARCOUNT_MIN) {
			clubmark_seg_t	*pclm;
			point_t Pp0, Pp1;
			point_t Lp0, Lp1;
			cr_point_t p0, p1;
			cr_line_t  line0;


			double xxP[CLUBMARK_BAR_CLASSCOUNT], yyP[CLUBMARK_BAR_CLASSCOUNT];
			double xxL[CLUBMARK_BAR_CLASSCOUNT], yyL[CLUBMARK_BAR_CLASSCOUNT];
			double dx, dy;
			U32 count;
			double mmP_, bbP_, r2P;
			double mmL_, bbL_, r2L;
			cr_vector_t mP, mL;
			//--
			barvalid = 1;

			pclm = &pcmb->clm[selectedbar];

			count = CLUBMARK_BAR_CLASSCOUNT;
			for (i0 = 0; i0 < count; i0++) {
				Pp0.x = pclm->ccP[i0].x; 
				Pp0.y = pclm->ccP[i0].y;
				Pp0.x = Pp0.x / xfact + offset_x;
				Pp0.y = Pp0.y / yfact + offset_y;
				iana_P2L(piana, camid, &Pp0, &Lp0, 0);
				xxP[i0] = Pp0.x; yyP[i0] = Pp0.y;
				xxL[i0] = Lp0.x; yyL[i0] = Lp0.y;
			}

			dx = xxP[count-1] - xxP[0];
			dy = yyP[count-1] - yyP[0];

			if (dx < 0) dx = -dx;
			if (dy < 0) dy = -dy;
			
			if (dy > dx) {			// |m_p| > 1
				//--
				cr_regression2(yyP, xxP, count, &mmP_, &bbP_, &r2P);
				mP.x = mmP_; mP.y = 1; mP.z = 0;
				
                cr_regression2(xxL, yyL, count, &mmL_, &bbL_, &r2L);
				mL.x = 1; mL.y = mmL_; mL.z = 0;

			} else {
				cr_regression2(xxP, yyP, count, &mmP_, &bbP_, &r2P);
				mP.x = 1; mP.y = mmP_; mP.z = 0;
				
                cr_regression2(yyL, xxL, count, &mmL_, &bbL_, &r2L);
				mL.x = mmL_; mL.y = 1; mL.z = 0;
			}

			{
				I32 selecti;
				double td;

#define 	MARK_LOWERBAR_OFFSET	3 // 1
				if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_BARDOT) {
					selecti = count-MARK_LOWERBAR_OFFSET;
				} else if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_TOPBAR) {
					selecti = count/2;			// CENTER!!! for TOPBAR..
				} else {
					selecti = 0;				// WHAT?
				}

				p1.x = xxP[selecti];
				p1.y = yyP[selecti];

				p1.z = 0;
				cr_vector_normalization(&mP, &mP);
				cr_line_p0m(&p1, &mP, &pcfc->upperline.clineP);

				p1.x = xxL[selecti];
				p1.y = yyL[selecti];

				p1.z = 0;
				cr_vector_normalization(&mL, &mL);
				cr_line_p0m(&p1, &mL, &pcfc->upperline.clineL);

			
				td = pcft->tsdiffd;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                        "Line, [%d %10lf] m: %lf %lf p0_: %lf %lf %lf\n", 
						camid, td,
						mL.x, mL.y,
						p1.x, 
						p1.y, 
						p1.z);
			}

			pcfc->upperline.valid = 1;
			UNUSED(line0);UNUSED(Lp1);UNUSED(Pp1);UNUSED(p0);
		} else {
			pcfc->upperline.valid = 0;
		}

		if (pcfc->upperline.valid || pcfc->lowerdot.valid) {
			pcfc->valid = 1;
		} else {
			pcfc->valid = 0;
		}
	} 	// for (index = index0; index <= index1; index++) 

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");

	res = 1;
	return res;
}



/*!
 ********************************************************************************
 *	@brief      club mark info start.
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2020/0311
 *******************************************************************************/
static I32 OpenClubmarkInfoXml(iana_t *piana)
{
	char pathname[PATHBUFLEN];

	IANA_INFO_CreateFeatureXmlPath(piana, "clubmarkfeature", pathname);
	
	cr_nanoxml_create2(pathname, 0);

	nxmlie(0, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	nxmlie(1, "<clubmark>");

	return 1;
}

/*!
 ********************************************************************************
 *	@brief      club mark info store.
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2020/0311
 *******************************************************************************/
static I32 StoreClubmarkInfoXml(iana_t *piana)
{
	I32 res;
	U32 camid;
	U32 index;

	clubmark_t *pcm;

	U32 marksequencelen;
	//--
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}

	pcm = (clubmark_t *)piana->hcm;
	
	for (camid = 0; camid < NUM_CAM; camid++) {
		iana_cam_t	*pic;
		clubmark_cam_t *pcmc;

		pic 		= piana->pic[camid];
		pcmc = &pcm->cmc[camid];
		nxmlie(1,  "<camera id=\"%d\">", camid);
		{
			cr_point_t *pCamPos;
			cr_point_t *pPb;
			double cr;
			cr_point_t *pb3d;

			pCamPos = &pic->icp.CamPosL;
			nxmlie(0, "<campos>%20.14lf %10.14lf %10.14lf</campos>", pCamPos->x, pCamPos->y, pCamPos->z);

			pPb = &pic->icp.P;
			nxmlie(0, "<ballposP>%10lf %10lf</ballposP>", pPb->x, pPb->y);

			cr  = pic->icp.cr;
			nxmlie(0, "<ballposRP>%10lf</ballposRP>", cr);
			
			pb3d  = &pic->icp.b3d;
			nxmlie(0, "<ballpos3D>%10lf %10lf %10lf</ballpos3D>", pb3d->x, pb3d->y, pb3d->z);

			for (index = 0; index < marksequencelen; index++) {
				clubmark_feature_t	*pcft;
				clubmark_face_t *pcfc;

				pcft = &pcmc->cft[index];
				pcfc = &pcmc->face[index];
				nxmlie(1, "<mark>");
				{
					nxmlie(0, "<index> %d </index>", index);
					nxmlie(0, "<valid> %d </valid>", pcfc->valid);
					nxmlie(0, "<ts64dec> %16.12lf </ts64dec>", pcft->tsdiffd);
					if (pcfc->valid) {
						nxmlie(1, "<upperline>");
						{
							clubmark_upperline_t *pul;

							pul = &pcfc->upperline;
							nxmlie(0, "<valid> %d </valid>", pul->valid);
							if (pul->valid) {
								cr_line_t *pl;
								nxmlie(1, "<lineP>");
								{
									pl = &pul->clineP;
									nxmlie(0, "<direction> %10lf %10lf %10lf </direction>", pl->m.x, pl->m.y, pl->m.z);
									nxmlie(0, "<fixedpoint> %10lf %10lf %10lf </fixedpoint>", pl->p0.x, pl->p0.y, pl->p0.z);
								}
								nxmlie(-1, "</lineP>");

								nxmlie(1, "<lineL>");
								{
									pl = &pul->clineL;
									nxmlie(0, "<direction> %10lf %10lf %10lf </direction>", pl->m.x, pl->m.y, pl->m.z);
									nxmlie(0, "<fixedpoint> %10lf %10lf %10lf </fixedpoint>", pl->p0.x, pl->p0.y, pl->p0.z);
								}
								nxmlie(-1, "</lineL>");
							}
						}
						nxmlie(-1, "</upperline>");

						nxmlie(1, "<lowerdot>");
						{
							clubmark_lowerdot_t  *pld;

							pld = &pcfc->lowerdot;
							nxmlie(0, "<valid> %d </valid>", pld->valid);
							if (pld->valid) {
								nxmlie(0, "<pointP> %10lf %10lf %10lf </pointP>", pld->cdotP.x, pld->cdotP.y, pld->cdotP.z);
								nxmlie(0, "<pointL> %10lf %10lf %10lf </pointL>", pld->cdotL.x, pld->cdotL.y, pld->cdotL.z);
							}
						}
						nxmlie(-1, "</lowerdot>");
					}
				}
				nxmlie(-1, "</mark>");
			}
		}
		nxmlie(-1,  "</camera>");
	}
	res = 1;
	return res;
}


/*!
 ********************************************************************************
 *	@brief      club mark info end.
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2020/0311
 *******************************************************************************/
static I32 CloseClubmarkInfoXml(iana_t *piana)
{
	I32 res;
	nxmlie(-1, "</clubmark>");
	cr_nanoxml_delete(NULL, 0);

	UNUSED(piana);
	res = 1;
	return res;
}



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------


static I32	DisplayClubmarkBarDot(iana_t *piana, U32 camid, 
		clubmark_t *pcm,
		U32 index0, U32 index1, 
		U32 displaydot, 
		U32 displaydotEst,
		U32 displaybar, 
		U32 displaybarEst)
{
	I32 res;
	U32 i, j;
	camif_buffer_info_t *pb;
	camimageinfo_t     	*pcii;
	iana_cam_t	*pic;

	U08 	*buf;
	U32		width, height;
	U32		width2, height2;
	U32 offset_x, offset_y;
	U64		ts64;

	point_t Pp;
	double Pr;
	I32 bx, by;			// ball position
	I32 sx;
	I32 wb;

	double	xfact, yfact; 

	clubmark_dot_t *pcmd;
	clubmark_bar_t *pcmb;


	U08 *pimgdest;
	U08 *pimgBGR;

	clubmark_cam_t *pcmc;
	clubmark_feature_t	*pcft;
	clubmark_face_t *pcfc;

	clubmark_region_t *pcrd;
	clubmark_region_t *pcrb;
	clubmark_region_t *pcrall;
	
	//----------

	pcmc = &pcm->cmc[camid];
	
	pic 	= piana->pic[camid];	
#define MAXIW	1280
#define MAXIH	1024
	pimgBGR = (U08 *) malloc(MAXIW*MAXIH*3);

	res = 0;
	pb = NULL;
	buf = NULL;
	for (i = index0; i <= index1; i++) {

		for (j = 0; j < 20; j++) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, i, &pb, &buf);
			if (res == 1) {
				break;
			}
			cr_sleep(2);
		}
		
		if (res == 0) {
			break;
		}
		pcii 	= &pb->cii;
		width 	= pcii->width;
		height 	= pcii->height;

		offset_x= pcii->offset_x;
		offset_y= pcii->offset_y;

		pcft = &pcmc->cft[i];
		memcpy(&Pp, &pcft->Pp, sizeof(point_t));
		Pr = pcft->Pr;

		bx 		= (I32)Pp.x - (I32)offset_x;
		by 		= (I32)Pp.y - (I32)offset_y;

		wb 		= bx - width;
		if 		(wb < (I32)(width * 0.6)) {
			wb	= (I32)(width * 0.6);
		}
		wb		= (wb - wb % 4);
		sx		= width - wb - 1;

		xfact = pcmc->xfact;
		yfact = pcmc->yfact;

		pcmd = &pcft->cmd;
		pcmb = &pcft->cmb;
		pcfc = &pcmc->face[i];

		ts64 		= MAKEU64(pb->ts_h, pb->ts_l);

		width2  = (U32)(width*xfact + 0.1);
		height2 = (U32) (height*yfact + 0.1);
		{
#ifdef IPP_SUPPORT

			Ipp8u 	*resizebuf;
			int 	bufsize;
			IppiRect sroi, droi;
			IppiSize ssize;

			//--
			ssize.width = width;
			ssize.height = height;

			sroi.x 		= 0;
			sroi.y 		= 0;
			sroi.width 	= width;
			sroi.height	= height;

			droi.x 		= 0;
			droi.y 		= 0;
			droi.width 	= width2;
			droi.height	= height2;

			ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_CUBIC, &bufsize);
			resizebuf = (U08 *)malloc(bufsize);

			pimgdest = pic->pimg[0];
			/*status = */ ippiResizeSqrPixel_8u_C1R(
					(Ipp8u *)buf,
					ssize,
					sroi.width,
					sroi,
					//
					(Ipp8u*) pimgdest,
					droi.width,
					droi,
					//
					xfact,
					yfact,
					0,
					0,
					IPPI_INTER_CUBIC,
					resizebuf
					);
			free(resizebuf);
#else
            cv::Mat cvImgSrc(cv::Size(width, height), CV_8UC1, buf), cvImgResized;

            pimgdest = pic->pimg[0];

            cv::resize(cvImgSrc, cvImgResized, cv::Size(width2, height2), xfact, yfact, cv::INTER_CUBIC);
            memcpy(pimgdest, cvImgResized.data, sizeof(U08) * cvImgResized.cols * cvImgResized.rows);
#endif			
		}


		for (j = 0; j < width2 * height2; j++) {
			U08 c;
			c = *(pimgdest + j);
			*(pimgBGR + j*3 + 0) = c;			// R
			*(pimgBGR + j*3 + 1) = c;			// G
			*(pimgBGR + j*3 + 2) = c;			// B
		}


		{
			double x, y, r;
			
			x = Pp.x * xfact - (offset_x * xfact +  0 /*sx2*/);
			y = Pp.y * yfact - (offset_y * yfact +  0);
			r = Pr * xfact;
			//mycircleGray((int) x, (int) y, (int) r, 0xFF, (U08 *)pimgdest, width2);
			mycircleBGR((int)x, (int)y, (int)r, MKBGR(0xFF,0xFF,0xFF), pimgBGR, width2);
		}
		{
			imagesegment_t 	*pis;
			rectObj_t *pro;

			int x[4], y[4];
			U32 i0;

			//--
			pcrd = &pcft->crd;
			if (displaydot) {
				for (i0 = 0; i0 <pcmd->count; i0++) {
					clubmark_state_t state;

					pis = &pcmd->clm[i0].imgseg;
					pro = &pcmd->clm[i0].rectObj;
					state = pcmd->clm[i0].state;

					if (state != CLUBMARK_DOT && state != CLUBMARK_EXILE) {
						continue;
					}
					x[0] = pis->ixl; y[0] = pis->iyu;
					x[1] = pis->ixr; y[1] = pis->iyu;
					x[2] = pis->ixr; y[2] = pis->iyb;
					x[3] = pis->ixl; y[3] = pis->iyb;

					//mydrawpolygonGray(x, y, 4, 0xFF, pimgdest, width2);
					if (state == CLUBMARK_EXILE) {
						mydrawpolygonBGR(x, y, 4, MKBGR(0xFF, 0x00, 0x00), pimgBGR, width2);
					} else {
						point_t Pp, Lp;
						U32 k;
						Pp.x = 0;
						Pp.y = 0;
						mydrawpolygonBGR(x, y, 4, MKBGR(0x00, 0xFF, 0xFF), pimgBGR, width2);
						for (k = 0; k < 4; k++) {
							x[k] = (int)pro->p[k].x; y[k] = (int)pro->p[k].y;
							Pp.x += x[k];
							Pp.y += y[k];
							//mycircleGray((int)x[k], (int)y[k], 2, 0xFF, pimgdest, width2);
							mycircleBGR((int)x[k], (int)y[k], 2, MKBGR(0x00,0xFF,0x00), pimgBGR, width2);
						}
						mydrawpolygonBGR(x, y, 4, MKBGR(0x30, 0x30, 0xFF), pimgBGR, width2);


						Pp.x /= 4.0; 	Pp.y /= 4.0;
						Pp.x /= xfact; 	Pp.y /= yfact;

						Pp.x = Pp.x + offset_x;
						Pp.y = Pp.y + offset_y;

						iana_P2L(piana, camid, &Pp, &Lp, 0);
						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "__DOT__ <%4d:%4d> %lf %lf\n", i, i0, Lp.x, Lp.y);
					}
				}
				if (pcrd->valid) {
					U32 k;
					I32 xpos[4], ypos[4];
					for (k = 0; k < 4; k++) {
						xpos[k] = (I32)(pcrd->vertex[k].x);
						ypos[k] = (I32)(pcrd->vertex[k].y);
					}
					mydrawpolygonBGR(xpos, ypos, 4, MKBGR(0xFF, 0x00, 0x00), pimgBGR, width2);

					{
						point_t *pvertex;
						double regarea;
						double lenbranch[4];
						point_t Pp;
						point_t Lp[4];
						int ii;

						//--

						pvertex = &pcrd->vertex[0];
						for (ii = 0; ii < 4; ii++) {
							Pp.x = pvertex[ii].x / xfact + offset_x;
							Pp.y = pvertex[ii].y / yfact + offset_y;
							iana_P2L(piana, camid, &Pp, &Lp[ii], 0);
						}

						regarea = calcRectArea(Lp);
						lenbranch[0] = calcDist(Lp+0, Lp+1);
						lenbranch[1] = calcDist(Lp+1, Lp+2);
						lenbranch[2] = calcDist(Lp+2, Lp+3);
						lenbranch[3] = calcDist(Lp+3, Lp+0);
						/*
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                                "[%d %2d] Dot Region (%lf %lf %lf %lf) area: %lf\n", 
								camid, i,
								lenbranch[0]*1000, lenbranch[1]*1000, lenbranch[2]*1000, lenbranch[3]*1000,
								regarea *(1000*1000));
						*/

						xpos[0] = (I32)pcrd->lowerband[1].x;
						ypos[0] = (I32)pcrd->lowerband[1].y;
						xpos[1] = (I32)pcrd->lowerband[0].x;
						ypos[1] = (I32)pcrd->lowerband[0].y;

						xpos[2] = (I32)(pcrd->vertex[2].x);
						ypos[2] = (I32)(pcrd->vertex[2].y);
						xpos[3] = (I32)(pcrd->vertex[3].x);
						ypos[3] = (I32)(pcrd->vertex[3].y);

						//mydrawpolygonBGR(xpos, ypos, 4, MKBGR(0x00, 0xFF, 0xFF), pimgBGR, width2);
						mydrawpolygonBGR(xpos, ypos, 4, MKBGR(0x00, 0x40, 0x40), pimgBGR, width2);

						if (pcfc->lowerdot.valid) {
							point_t p0;

							p0.x = pcfc->lowerdot.cdotP.x;
							p0.y = pcfc->lowerdot.cdotP.y;
							
							p0.x = (p0.x - offset_x) * xfact;
							p0.y = (p0.y - offset_y) * yfact;

							mycircleBGR((int)p0.x, (int)p0.y, 2, MKBGR(0xFF,0x00,0x00), pimgBGR, width2);
						}
					}
				}
			}

			if (displaydotEst) {
				double td;
				double x0_, y0_;
				double xma[3], yma[3];
				point_t Pp, Lp;

				//--
				//td = pcmc->tsdiffd[i];
				td = pcft->tsdiffd;
				memcpy(&xma[0], &pcmc->dotLp.xma[0], sizeof(double)*3);
				x0_ = xma[2]*td*td + xma[1]*td + xma[0];
				memcpy(&yma[0], &pcmc->dotLp.yma[0], sizeof(double)*3);
				y0_ = yma[2]*td*td + yma[1]*td + yma[0];
				Lp.x = x0_; Lp.y = y0_;
				iana_L2P(piana, camid, &Lp, &Pp, 0);

				x0_ = Pp.x;
				x0_ = x0_ - offset_x; 
				x0_ = x0_ * xfact; 

				y0_ = Pp.y;
				y0_ = y0_ - offset_y; 
				y0_ = y0_ * yfact; 

				//mycircleGray((int)x0_, (int)y0_, 2, 0x40, pimgdest, width2);
				mycircleBGR((int)x0_, (int)y0_, 2, MKBGR(0x00,0x00,0xFF), pimgBGR, width2);
			}



			pcrb = &pcft->crb;
			if (displaybar) {
				for (i0 = 0; i0 <pcmb->count; i0++) {
					pis = &pcmb->clm[i0].imgseg;
					pro = &pcmb->clm[i0].rectObj;

					if (pcmb->clm[i0].state != CLUBMARK_BAR) {
						continue;
					}

					x[0] = pis->ixl; y[0] = pis->iyu;
					x[1] = pis->ixr; y[1] = pis->iyu;
					x[2] = pis->ixr; y[2] = pis->iyb;
					x[3] = pis->ixl; y[3] = pis->iyb;

					//mydrawpolygonGray(x, y, 4, 0xFF, pimgdest, width2);
					//mydrawpolygonBGR(x, y, 4, MKBGR(0xFF, 0xFF,0xFF), pimgBGR, width2);
#if 1					// XXXXXXX
					{
						U32 k;
						U08 yvalue;
						point_t Pp, Lp;

						Pp.x = 0;
						Pp.y = 0;
						for (k = 0; k < 4; k++) {
							x[k] = (int)pro->p[k].x; y[k] = (int)pro->p[k].y;
							if (k == 1 || k == 2) {
								Pp.x += x[k];
								Pp.y += y[k];
							}
							yvalue = (U08) ((0xFF * (4 - k)) / 4);
							//mycircleGray((int)x[k], (int)y[k], 4, yvalue, pimgdest, width2);
							mycircleBGR((int)x[k], (int)y[k], 4, MKBGR(yvalue, yvalue, 0), pimgBGR, width2);
						}
						mydrawpolygonBGR(x, y, 4, MKBGR(0x30, 0x30,0xFF), pimgBGR, width2);

						Pp.x /= 2.0; 	Pp.y /= 2.0;
						Pp.x /= xfact; 	Pp.y /= yfact;

						Pp.x = Pp.x + offset_x;
						Pp.y = Pp.y + offset_y;

						iana_P2L(piana, camid, &Pp, &Lp, 0);
						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "__BAR__ <%4d:%4d> %lf %lf\n", i, i0, Lp.x, Lp.y);
					}
#endif

					{
						double barangle;
						double m_, bb_;

						barangle = atan2(pis->a_, -pis->b_);
						barangle = RADIAN2DEGREE(barangle);

						m_ = -pis->a_/pis->b_;

						bb_ = -pis->c_ / pis->b_;

						/*
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Bar angle: %lf\n", barangle);
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%2d> a,b,c: %lf %lf %lf, m_, bb_: %lf %lf\n",i0, pis->a_, pis->b_, pis->c_, m_, bb_);
						*/
					}

					{
						I32 ii;
						clubmark_seg_t	*pclm;
						pclm = &pcmb->clm[i0];
						if (pclm->ccvalid) {
							for (ii = 0; ii < CLUBMARK_BAR_CLASSCOUNT; ii++) {
								I32 xx, yy;

								//---
								xx = (I32) (pclm->ccP[ii].x);
								yy = (I32) (pclm->ccP[ii].y);
								mycircleBGR((int)xx, yy, 2, MKBGR(0x10 + (ii<<4), 
											0x10 + (ii <<4), 0x20), pimgBGR, width2);
							}
						}
					}
				} 	// for (i0 = 0; i0 <pcmb->count; i0++) 
				if (pcrb->valid) {
					U32 k;
					U32 ii;
					I32 xpos[4], ypos[4];
					point_t *pvertex;
					point_t Pp;
					point_t Lp[4];

					//--
					for (k = 0; k < 4; k++) {
						xpos[k] = (I32)(pcrb->vertex[k].x);
						ypos[k] = (I32)(pcrb->vertex[k].y);
					}
					mydrawpolygonBGR(xpos, ypos, 4, MKBGR(0xFF, 0x00, 0x00), pimgBGR, width2);
					{
						pvertex = &pcrb->vertex[0];
						for (ii = 0; ii < 4; ii++) {
							Pp.x = pvertex[ii].x / xfact + offset_x;
							Pp.y = pvertex[ii].y / yfact + offset_y;
							iana_P2L(piana, camid, &Pp, &Lp[ii], 0);
						}
						xpos[2] = (I32)pcrb->upperband[1].x;
						ypos[2] = (I32)pcrb->upperband[1].y;
						xpos[3] = (I32)pcrb->upperband[0].x;
						ypos[3] = (I32)pcrb->upperband[0].y;

						mydrawpolygonBGR(xpos, ypos, 4, MKBGR(0x40, 0x00, 0x40), pimgBGR, width2);
					}

					if (pcfc->upperline.valid) {
						double xx, yy;
						double xx0, yy0;
						double xx1, yy1;

						xx = pcfc->upperline.clineP.p0.x;
						yy = pcfc->upperline.clineP.p0.y;

						xx0 = xx + pcfc->upperline.clineP.m.x * 10;
						yy0 = yy + pcfc->upperline.clineP.m.y * 10;
						xx1 = xx - pcfc->upperline.clineP.m.x * 10;
						yy1 = yy - pcfc->upperline.clineP.m.y * 10;

						xx = (xx - offset_x) * xfact;
						yy = (yy - offset_y) * yfact;

						xx0 = (xx0 - offset_x) * xfact;
						yy0 = (yy0 - offset_y) * yfact;

						xx1 = (xx1 - offset_x) * xfact;
						yy1 = (yy1 - offset_y) * yfact;


						mycircleBGR((int)xx, (int)yy, 5, MKBGR(0xFF, 0x10, 0xFF), pimgBGR, width2);
						mylineBGR((int)xx0, (int)yy0, (int)xx1, (int)yy1, MKBGR(0xFF, 0x10, 0xFF), pimgBGR, width2);
					}
				}
			}

			if (displaybarEst) {
				double td;
				double x0_, y0_;
				double xma[3], yma[3];
				point_t Pp, Lp;
				U32 valid;

				//--
				x0_ = y0_ = 0;
				valid = pcmc->barLp[0].valid[i];
				//td = pcmc->tsdiffd[i];
				td = pcft->tsdiffd;
				memcpy(&xma[0], &pcmc->barLp[0].xma[0], sizeof(double)*3);
				memcpy(&yma[0], &pcmc->barLp[0].yma[0], sizeof(double)*3);

				{
					x0_ = xma[2]*td*td + xma[1]*td + xma[0];
					y0_ = yma[2]*td*td + yma[1]*td + yma[0];
				}
				Lp.x = x0_; Lp.y = y0_;
				iana_L2P(piana, camid, &Lp, &Pp, 0);

				x0_ = Pp.x;
				x0_ = x0_ - offset_x; 
				x0_ = x0_ * xfact; 

				y0_ = Pp.y;
				y0_ = y0_ - offset_y; 
				y0_ = y0_ * yfact; 

				if (valid) {
					mycircleBGR((int)x0_, (int)y0_, 2, MKBGR(0x10, 0x00, 0xFF), pimgBGR, width2);
				} else {
					if (x0_ > -8888 && y0_ > -8888) {
						mycircleBGR((int)x0_, (int)y0_, 2, MKBGR(0xFF, 0x10, 0x00), pimgBGR, width2);
					}
				}
			}
		}

		pcrall = &pcft->crall;
		if (displaydot && displaybar && s_dotregionupper < -9999) {
			if (pcrall->valid) {
				U32 k;
				I32 xpos[4], ypos[4];
				for (k = 0; k < 4; k++) {
					xpos[k] = (I32)(pcrall->vertex[k].x);
					ypos[k] = (I32)(pcrall->vertex[k].y);
				}
				mydrawpolygonBGR(xpos, ypos, 4, MKBGR(0xFF, 0xFF, 0xFF), pimgBGR, width2);
				{
					point_t *pvertex;
					double regarea;
					double lenbranch[4];
					point_t Pp;
					point_t Lp[4];
					int ii;

					//--

					pvertex = &pcrall->vertex[0];
					for (ii = 0; ii < 4; ii++) {
						Pp.x = pvertex[ii].x / xfact + offset_x;
						Pp.y = pvertex[ii].y / yfact + offset_y;
						iana_P2L(piana, camid, &Pp, &Lp[ii], 0);
					}

					regarea = calcRectArea(Lp);
					lenbranch[0] = calcDist(Lp+0, Lp+1);
					lenbranch[1] = calcDist(Lp+1, Lp+2);
					lenbranch[2] = calcDist(Lp+2, Lp+3);
					lenbranch[3] = calcDist(Lp+3, Lp+0);
					/*
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d %2d] All Region (%lf %lf %lf %lf) area: %lf\n", 
							camid, i,
							lenbranch[0]*1000, lenbranch[1]*1000, lenbranch[2]*1000, lenbranch[3]*1000,
							regarea *(1000*1000));
							*/
				}
			} 	// if (pcrall->valid) 
		} 		// if (displaydot && displaybar) 


		if (piana->himgfunc) {
			//((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimgdest, width2,	height2, 0, 1, 2, 3, piana->imguserparam);
			((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimgBGR, width2,	height2, 0, 4, 2, 3, piana->imguserparam);
		}
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "{{{%d}}}\n\n", i);
 	}

	free(pimgBGR);

	res = 1;

	return res;
}

/*!
 ********************************************************************************
 *	@brief      binarize image with threshold calculated by analyzing image
 *
 *  @param[in]	piana
 *              IANA module handler
 *  @param[in]	camid
 *              camera index
 *  @param[in]	pcm
 *              pointer to a club mark
 *  @param[in]	buf
 *              image buffer
 *  @param[in]	pb
 *              buffer info
 *  @param[in]	xfact
 *              ? resizing factor of x of image for processing
 *  @param[in]	yfact
 *              ? resizing factor of y of image for processing
 *  @param[in]	multitude
 *              multitude. the number of stacked images
 *  @param[out?]	pwidth2
 *              ? width of resized image, the result image
 *  @param[out?]	pheight2
 *              ? height of resized image, the result image
 *  @param[out?]	psx2
 *              ? something related to x of start position
 *  @param[out?]	pwb2
 *              ? something related to width of result image
 *  @param[out]	bufOut
 *              resulted image buffer(should be binrized, clubmark-detected)
 *  @param[out]	bufOutEdge
 *              resulted image of mark edge
 *  @param[in]	pPp
 *              ballposition in pixels
 *  @param[in]	Pr
 *              ball radius in pixels
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date
 *******************************************************************************/
static I32 BinarizeClubmarkImage(
		iana_t *piana, U32 camid, 
		clubmark_t *pcm,
		U08 *buf,
		camif_buffer_info_t *pb,
		double xfact,
		double yfact,
		U32 multitude,

		U32 *pwidth2,
		U32 *pheight2,
		I32 *psx2,
		I32 *pwb2,
		U08 *bufOut,
		U08 *bufOutEdge

		, point_t *pPp,
		double Pr
		)
{
#ifdef IPP_SUPPORT
	I32 res;
	//	U32 i;
	//	camif_buffer_info_t *pb;
	camimageinfo_t     	*pcii;
	iana_cam_t	*pic;

	U32		width, height;
	U32		width2, height2;

	U08 *pimgsrc;
	U08 *pimgdest;

	IppiSize roisize;
	U32 threshold;

	U32 offset_x, offset_y;

	point_t Pp;
	I32 bx, by;			// ball position
	I32 sx;
	I32 wb;

	I32 sx2;
	I32 wb2;

	U32 mproioffset;
	IppiSize mproisize;

	//--

	pic 	= piana->pic[camid];	
	Pp.x	= pic->icp.P.x;
	Pp.y	= pic->icp.P.y;


	pcii 	= &pb->cii;
	width 	= pcii->width;
	height 	= pcii->height;

	offset_x= pcii->offset_x;
	offset_y= pcii->offset_y;

	bx 		= (I32)Pp.x - (I32)offset_x;
	by 		= (I32)Pp.y - (I32)offset_y;

	bx 		= (I32)pPp->x - (I32)offset_x;
	by 		= (I32)pPp->y - (I32)offset_y;

	wb 		= bx - width;
	if 		(wb < (I32)(width * 0.6)) {
		wb	= (I32)(width * 0.6);
	}
	wb		= (wb - wb % 4);
	sx		= width - wb - 1;

	width2  = (U32)(width*xfact + 0.1);
	height2 = (U32) (height*yfact + 0.1);
	wb2		= (I32) (wb*xfact + 0.1);
	sx2		= (I32) (width2 - wb2 - 1);

	if (sx2 < 0) {
		res = 0;
		return res;
	}
	pimgsrc = buf;
	{

		Ipp8u 	*resizebuf;
		int 	bufsize;
		IppiRect sroi, droi;
		IppiSize ssize;

		//--
		ssize.width = width;
		ssize.height = height;

		sroi.x 		= 0;
		sroi.y 		= 0;
		sroi.width 	= width;
		//sroi.height	= height;
		sroi.height	= height/multitude;

		droi.x 		= 0;
		droi.y 		= 0;
		droi.width 	= width2;
		droi.height	= height2;

		ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_CUBIC, &bufsize);
		//ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_NN, &bufsize);
		resizebuf = (U08 *)malloc(bufsize);


		pimgdest = pic->pimg[0];
		memset(pimgdest, 0, FULLWIDTH * FULLHEIGHT);
		/*status = */ ippiResizeSqrPixel_8u_C1R(
				(Ipp8u *)buf,
				ssize,
				sroi.width,
				sroi,
				//
				(Ipp8u*) pimgdest,
				droi.width,
				droi,
				//
				xfact,
				yfact*multitude, // yfact,
				0,
				0,
				IPPI_INTER_CUBIC,
				//IPPI_INTER_NN,
				resizebuf
				);
		free(resizebuf);
	}

	//-------------	Binarization
    // calculate threshold
	threshold = GetClubmarkThreshold(piana, camid, pcm, buf, pcii, pPp, Pr);
	Pr;pcm;

	
	if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_TOPBAR) {
		if (camid == 0) {
			threshold = 220; // 230, 240
		} else {
			threshold = 220; // 200
		}
	} else {				// CLUBCALC_CLUB_MARK_BARDOT
		if (camid == 0) {
			threshold = 220; // 200, 240, 250, 150
		} else {
			threshold = 200; // 180, 150
		}
	}
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%d threshold: %d\n", camid, threshold);

	pimgsrc = pimgdest;
	//pimgdest = pic->pimg[1];
	pimgdest = pic->pimg[2];
	memset(pimgdest, 0, FULLWIDTH * FULLHEIGHT);

	//roisize.width = 	width2;
	roisize.width = 	wb2;
	roisize.height = 	height2;
	// ippiThreshold_LTValGTVal_8u_C1R
	ippiCompareC_8u_C1R(
			pimgsrc+sx2, 	width2,	// source
			(Ipp8u)threshold,	
			pimgdest+sx2,	width2,	// binary image. dest.
			roisize, ippCmpGreater
			);

	{	// Morph
		pimgsrc = pimgdest;
		//pimgdest = pic->pimg[2];
		pimgdest = pic->pimg[1];
		//memset(pimgdest, 0, FULLWIDTH * FULLHEIGHT);


#define MPROIOFFSET		3
		mproisize.width 	= wb2 /*width2*/	- 2 * MPROIOFFSET;
		mproisize.height 	= height2 	- 2 * MPROIOFFSET;
		mproioffset			= sx2 + MPROIOFFSET  * width2 + MPROIOFFSET;;

		// Erode 3x3
		ippiErode3x3_8u_C1R(
				pimgsrc		+ mproioffset, width2, // source
				pimgdest	+ mproioffset, width2, // dest
				mproisize
				);	
		ippiErode3x3_8u_C1IR(
				pimgdest  + mproioffset, width, 
				mproisize
				);	

        pimgsrc = pimgdest;
		pimgdest = bufOut;

        // Dilate 3x3
		ippiDilate3x3_8u_C1R(
				pimgsrc		+ mproioffset, width2, // source
				pimgdest	+ mproioffset, width2, // dest
				mproisize
				);	
		ippiDilate3x3_8u_C1IR(
				pimgdest  + mproioffset, width, 
				mproisize
		   );	
		ippiDilate3x3_8u_C1IR(
				pimgdest  + mproioffset, width, 
				mproisize
		   );	


	}

	memset(bufOutEdge, 0xFF, width2*height2);
	ippiSub_8u_C1RSfs(								// C = A - B
			pic->pimg[1]+ mproioffset, width2, // source,		// B
			bufOut		+ mproioffset, width2,	// source,		// A
			bufOutEdge	+ mproioffset, width2,		// C, edge result.
			mproisize, 0);	

	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)bufOut, 		width2,	height2, 2, 1, 2, 3, piana->imguserparam);
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)bufOutEdge,	width2,	height2, 0, 1, 2, 3, piana->imguserparam);
	}

	//bufOutEdge = bufOut - pic->pimg[2];
	//--
	*pwidth2 = width2;
	*pheight2 = height2;
	*psx2 = sx2;
	*pwb2 = wb2;

	res = 1;

	bufOutEdge;
	return res;
#else
    I32 res;
    camimageinfo_t     	*pcii;
    iana_cam_t	*pic;

    U32		width, height;
    U32		width2, height2;

    U08 *pimgsrc;
    U08 *pimgdest;

    U32 threshold;

    U32 offset_x, offset_y;

    point_t Pp;
    I32 bx, by;			// ball position
    I32 sx;
    I32 wb;

    I32 sx2;
    I32 wb2;


    //--
    pic 	= piana->pic[camid];
    Pp.x	= pic->icp.P.x;
    Pp.y	= pic->icp.P.y;


    pcii 	= &pb->cii;
    width 	= pcii->width;
    height 	= pcii->height;

    offset_x= pcii->offset_x;
    offset_y= pcii->offset_y;

    bx 		= (I32)Pp.x - (I32)offset_x;
    by 		= (I32)Pp.y - (I32)offset_y;

    bx 		= (I32)pPp->x - (I32)offset_x;
    by 		= (I32)pPp->y - (I32)offset_y;

    wb 		= bx - width;
    if (wb < (I32)(width * 0.6)) {
        wb	= (I32)(width * 0.6);
    }
    wb		= (wb - wb % 4);
    sx		= width - wb - 1;

    width2  = (U32)(width*xfact + 0.1);
    height2 = (U32)(height*yfact + 0.1);
    wb2		= (I32)(wb*xfact + 0.1);
    sx2		= (I32)(width2 - wb2 - 1);

    if (sx2 < 0) {
        res = 0;
        return res;
    }

    pimgsrc = buf;
    
    {
        cv::Mat cvImgSrc(cv::Size(width, height), CV_8UC1, buf), cvImgMultTop, cvImgResized, cvImgResized2Fullsize;

        cvImgMultTop = cvImgSrc(cv::Rect(0, 0, width, height/multitude));

        cv::resize(cvImgMultTop, cvImgResized, cv::Size(width2, height2), xfact, yfact*multitude, cv::INTER_CUBIC);

        cv::copyMakeBorder(cvImgResized, cvImgResized2Fullsize, 0, FULLHEIGHT-cvImgResized.rows, 0, FULLWIDTH - cvImgResized.cols, cv::BORDER_CONSTANT, 0);

        pimgdest = pic->pimg[0];
        memset(pimgdest, 0, FULLWIDTH * FULLHEIGHT);
        memcpy(pimgdest, cvImgResized2Fullsize.data, sizeof(U08) * cvImgResized2Fullsize.cols * cvImgResized2Fullsize.rows);
    }

    //-------------	Binarization
    // calculate threshold
    threshold = GetClubmarkThreshold(piana, camid, pcm, buf, pcii, pPp, Pr);

    if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_TOPBAR) {
        if (camid == 0) {
            threshold = 220; // 230, 240
        } else {
            threshold = 220; // 200
        }
    } else {				// CLUBCALC_CLUB_MARK_BARDOT
        if (camid == 0) {
            threshold = 220; // 200, 240, 250, 150
        } else {
            threshold = 200; // 180, 150
        }
    }

    pimgsrc = pimgdest;

    {
        cv::Mat cvImgResized(cv::Size(FULLWIDTH, FULLHEIGHT), CV_8UC1, pimgsrc),
            cvImgCutClubArea,
            cvImgBin, cvImgErode, cvImgDilate, cvImgEdge,
            cvImgDilateReturn, cvImgEdgeReturn;
        
        cvImgCutClubArea = cvImgResized(cv::Rect(sx2, 0, wb2, height2));

        cv::threshold(cvImgCutClubArea, cvImgBin, threshold, 255, cv::THRESH_BINARY);

        cv::morphologyEx(cvImgBin, cvImgErode,
            cv::MORPH_ERODE,
            cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)),
            cv::Point(-1, -1), 2, cv::BORDER_ISOLATED);

        cv::morphologyEx(cvImgErode, cvImgDilate,
            cv::MORPH_DILATE,
            cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)),
            cv::Point(-1, -1), 3, cv::BORDER_ISOLATED);

        cv::subtract(cvImgDilate, cvImgErode, cvImgEdge);

        cv::copyMakeBorder(cvImgDilate, cvImgDilateReturn, 0, 0, sx2, 1, cv::BORDER_CONSTANT, 0);
        cv::copyMakeBorder(cvImgEdge, cvImgEdgeReturn, 0, 0, sx2, 1, cv::BORDER_CONSTANT, 0);

        memset(bufOutEdge, 0xFF, width2*height2);

        memcpy(bufOut, cvImgDilateReturn.data, sizeof(U08) * cvImgDilateReturn.cols * cvImgDilateReturn.rows);
        memcpy(bufOutEdge, cvImgEdgeReturn.data, sizeof(U08) * cvImgEdgeReturn.cols * cvImgEdgeReturn.rows);
    }


    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)bufOut, width2, height2, 2, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)bufOutEdge, width2, height2, 0, 1, 2, 3, piana->imguserparam);
    }


    *pwidth2 = width2;
    *pheight2 = height2;
    *psx2 = sx2;
    *pwb2 = wb2;

    res = 1;

    UNUSED(bufOutEdge);
    return res;
#endif
}


static I32 LabelingClubmark(
		iana_t *piana, U32 camid, 
		clubmark_t *pcm,
		U32 width,
		U32 height,
		I32 startx,
		I32 wb,
		U08 *bufLabel,
		U08 *bufBin,
		clubmark_seg_t	clm[]
		)
{
	I32 res;


	U08 *pimg;
	I32 i, j;
	I32 k;
	imagesegment_t 	*pis;
	rectObj_t *pro;
	clubmark_seg_t	*pclm;

	point_t p0[4], p1[4], p2[4], p3[4];

	
#ifdef IPP_SUPPORT
    I32 segmentcount;
    U32 calcoffset;
    
    I32 lbbuffersize;
    U08 *lbbuffer;
    IppiSize lbroisize;
    
    U08 segindex;

	double c0x[CLUBMARK_MAXLABEL], c0y[CLUBMARK_MAXLABEL];
	double c1x[CLUBMARK_MAXLABEL], c1y[CLUBMARK_MAXLABEL];
	//double cex[CLUBMARK_MAXLABEL], cey[CLUBMARK_MAXLABEL];
	
	//--
	lbroisize.width 	= wb 		- LABELING_MARGIN * 2;
	lbroisize.height	= height 	- LABELING_MARGIN * 2;

	calcoffset = startx + LABELING_MARGIN + (LABELING_MARGIN * width);

	ippiLabelMarkersGetBufferSize_8u_C1R(lbroisize, &lbbuffersize);	// Get buffersize
	lbbuffer = (U08 *)malloc(lbbuffersize);

	ippiLabelMarkers_8u_C1IR(
			bufLabel+calcoffset, width, lbroisize, 
			MINLABEL, CLUBMARK_MAXLABEL, 
			//ippiNormInf,
			ippiNormL1,
			&segmentcount,
			lbbuffer);
	free(lbbuffer);

	piana; camid; 

	//-- Segm.
	pimg = bufLabel;
	for (i = 0; i < CLUBMARK_MAXLABEL; i++) {
		pclm = &clm[i];
		pis = &pclm->imgseg;
		pro = &pclm->rectObj;

		memset(pis, 0, sizeof(imagesegment_t));
		pis->state = IMAGESEGMENT_NULL;
		pis->label = i;

		pro = &clm[i].rectObj;
		memset(pro, 0, sizeof(rectObj_t));
	}

	for (i = LABELING_MARGIN; i < (I32)height-LABELING_MARGIN; i++) {
		for (j = startx + LABELING_MARGIN; j < (I32)width-LABELING_MARGIN; j++) {
			segindex = *(pimg + j + i * width);
			if (segindex >= MINLABEL && segindex <= CLUBMARK_MAXLABEL-1) {
				pis = &clm[segindex].imgseg;
				if (pis->state == IMAGESEGMENT_NULL) {
					pis->state  = IMAGESEGMENT_FULL;
					pis->ixl = j;
					pis->iyu = i;
					pis->mx = 0;
					pis->my = 0;
				}

				pis->mx += j;
				pis->my += i;
				pis->pixelcount++;
				if ((I32)pis->ixr < j) 
				{		// xr is biggest..
					pis->ixr = j;
				}
				if ((I32)pis->ixl > j) 
				{		// xl is smallest..
					pis->ixl = j;
				}
				pis->iyb = i;
			}
		}
	}

	for (k = 0; k < CLUBMARK_MAXLABEL; k++) {
		rectObj_t *pro;
		pclm = &clm[k];
		pis = &pclm->imgseg;
		pro = &pclm->rectObj;
		if (pis->state == IMAGESEGMENT_FULL) {
			U32 count;
			I32 segwidth, segheight;
			count = pis->pixelcount;
			segwidth  = pis->ixr - pis->ixl + 1;
			segheight = pis->iyb - pis->iyu + 1;

			ClassifyClubmarkClass(
					pcm,
					pclm,
					pimg,
					k,
					width, 
					height
					);
		}
	}
#else

    double c0x[CLUBMARK_MAXLABEL], c0y[CLUBMARK_MAXLABEL];
    double c1x[CLUBMARK_MAXLABEL], c1y[CLUBMARK_MAXLABEL];

    cv::Mat cvImgSrc(cv::Size(width, height), CV_8UC1, bufLabel), cvImgLabelROI, cvImgLabelled, cvImgLabelledFull;
    cv::Mat cvStats, cvCents;

    int lebelCnt;
    UNUSED(piana); UNUSED(camid);
    // calcoffset = startx + LABELING_MARGIN + (LABELING_MARGIN * width);

    cvImgLabelROI = cvImgSrc(cv::Rect(startx, 0, wb, height));

    lebelCnt = cv::connectedComponentsWithStats(cvImgLabelROI, cvImgLabelled, cvStats, cvCents, 4, CV_32S);
    
    cv::copyMakeBorder(cvImgLabelled, cvImgLabelledFull, 
        0, 0, startx, width - startx - wb,
        cv::BORDER_CONSTANT, 0);
    //-- Segm.
    pimg = bufLabel;
    memcpy(pimg, cvImgLabelledFull.data, sizeof(U08) * cvImgLabelledFull.cols * cvImgLabelledFull.rows);

    for (i = 0; i < CLUBMARK_MAXLABEL; i++) {
        pclm = &clm[i];
        pis = &pclm->imgseg;
        pro = &pclm->rectObj;

        memset(pis, 0, sizeof(imagesegment_t));
        pis->state = IMAGESEGMENT_NULL;
        pis->label = i;

        pro = &clm[i].rectObj;
        memset(pro, 0, sizeof(rectObj_t));
    }

    lebelCnt = lebelCnt < CLUBMARK_MAXLABEL ? lebelCnt : CLUBMARK_MAXLABEL;

    for(i = 1; i < lebelCnt; i ++){
        pclm = &clm[i];

        pis = &pclm->imgseg;
        pis -> state = IMAGESEGMENT_FULL;
        pis -> ixl = cvStats.at<int>(i, 0);
        pis -> iyu = cvStats.at<int>(i, 1);
        pis -> ixr = cvStats.at<int>(i, 0) + cvStats.at<int>(i, 2);
        pis -> iyb = cvStats.at<int>(i, 1) + cvStats.at<int>(i, 3);
        pis -> pixelcount = cvStats.at<int>(i, 4);
    }

    for(i = 0; i < lebelCnt; i++){

        pclm = &clm[i];
        
        ClassifyClubmarkClass(
            pcm,
            pclm,
            pimg,
            i,
            width,
            height
        );

    }
#endif

	//-- Rectangular ..
	for (k = 0; k < CLUBMARK_MAXLABEL; k++) {
		rectObj_t *pro;
		pclm = &clm[k];
		pis = &pclm->imgseg;
		pro = &pclm->rectObj;
		if (pis->state == IMAGESEGMENT_FULL) {
			U32 count;
			count = pis->pixelcount;
			if (count != 0) {
				pis->mx = pis->mx / (double) count;
				pis->my = pis->my / (double) count;
			} else {
				pis->mx = 0;
				pis->my = 0;
			}

			c0x[k] = pis->mx;
			c0y[k] = pis->my;

			i = pis->iyu;
			p0[0].x = p0[1].x = -999;
			p0[0].y = p0[1].y = i;
			p0[2].x = p0[3].x = -999;
			p0[2].y = p0[3].y = i+1;
			for (j = pis->ixl; j <= (I32)pis->ixr; j++) {
				if (*(pimg + j + i * width) == k) {
					if (p0[0].x < 0) {
						p0[0].x = j;
					}
					p0[1].x = j;
				}
				if (*(pimg + j + (i+1) * width) == k) {
					if (p0[2].x < 0) {
						p0[2].x = j;
					}
					p0[3].x = j;
				}
			}

			i = pis->iyb;
			p2[0].x = p2[1].x = -999;
			p2[0].y = p2[1].y = i;
			p2[2].x = p2[3].x = -999;
			p2[2].y = p2[3].y = i-1;
			for (j = pis->ixl; j <= (I32)pis->ixr; j++) {
				if (*(pimg + j + i * width) == k) {
					if (p2[0].x < 0) {
						p2[0].x = j;
					}
					p2[1].x = j;
				}
				if (*(pimg + j + (i-1) * width) == k) {
					if (p2[2].x < 0) {
						p2[2].x = j;
					}
					p2[3].x = j;
				}
			}

			j = pis->ixl;
			p1[0].x = p1[1].x = j;
			p1[0].y = p1[1].y = -999;
			p1[2].x = p1[3].x = j+1;
			p1[2].y = p1[3].y = -999;
			for (i = pis->iyu; i <= (I32)pis->iyb; i++) {
				if (*(pimg + j + i * width) == k) {
					if (p1[0].y < 0) {
						p1[0].y = i;
					}
					p1[1].y = i;
				}
				if (*(pimg + (j+1) + i * width) == k) {
					if (p1[2].y < 0) {
						p1[2].y = i;
					}
					p1[3].y = i;
				}
			}

			j = pis->ixr;
			p3[0].x = p3[1].x = j;
			p3[0].y = p3[1].y = -999;
			p3[2].x = p3[3].x = j-1;
			p3[2].y = p3[3].y = -999;

			for (i = pis->iyu; i <= (I32)pis->iyb; i++) {
				if (*(pimg + j + i * width) == k) {
					if (p3[0].y < 0) {
						p3[0].y = i;
					}
					p3[1].y = i;
				}
				if (*(pimg + (j-1) + i * width) == k) {
					if (p3[2].y < 0) {
						p3[2].y = i;
					}
					p3[3].y = i;
				}
			}
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
            //      "p3 (%d %d), (%d %d), (%d %d), (%d %d)\n",
			//		p3[0].x, p3[0].y, 
			//		p3[1].x, p3[1].y, 
			//		p3[2].x, p3[2].y, 
			//		p3[3].x, p3[3].y);			

			findrect(p0, p1, p2, p3, pro);


			if (pro->vertexlen[pro->maxvertexlenIndex] > 1.0) {
				U32 count;
				count = rectpixelcount(width, bufLabel, bufBin, &clm[k]);

#define MINPIXELCOUNT 10
				if (count > MINPIXELCOUNT) {
					pclm->state = CLUBMARK_UNDECIDED;		// not yet decided.
				} else {
					pclm->state = CLUBMARK_EXILE;		// BAD..
					pclm->exilewhy = CLUBMARK_EXILEWHY_TOOSMALL_RECT;	// TOO SMALL rect.
				}
			} else {
				//pclm->state = CLUBMARK_EXILE;			// bad.. exile
				pclm->state = CLUBMARK_UNDECIDED;		// not yet decided.
			}
//			pro->pixelcount  =  rectpixelcount(width, bufBin, &clm[k]);
//			pro->pixelcount2 =  pis->pixelcount;

			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "seg k %d,  count: %d\n", k, count);
		}
	}


#define IMGHFACT (1.0/2.0) // (2.0/3.0) , 1.0, (1.0/4.0)
#if defined(IMGHFACT)
	if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_TOPBAR) {
		for (k = 0; k < CLUBMARK_MAXLABEL; k++) {
			pclm = &clm[k];
			pis = &pclm->imgseg;
			if (pis->state == IMAGESEGMENT_FULL) {
				I32 imgh;
				I32 count;
				imgh = pis->iyb - (I32)pis->iyu;

				imgh = (I32) (imgh * IMGHFACT);

				pis->mx = 0;
				pis->my = 0;
				count = 0;
				for (i = pis->iyu; i < (I32)pis->iyu + imgh; i++) {
					for (j = pis->ixl; j <= (I32)pis->ixr; j++) {
						if (*(pimg + j + i * width) == k) {
							pis->mx += j;
							pis->my += i;
							count++;
						}
					}
				}
				if (count!=0) {
					pis->mx /= count;
					pis->my /= count;
				} else {
					pis->mx = 0;
					pis->my = 0;
				}
				c1x[k] = pis->mx;
				c1y[k] = pis->my;


				if (s_featuretype == 1) {

#define MXMYMULT	(35/7.5) // (36/7.5), (40/7.5)
					pis->mx = (c1x[k] + (c1x[k]-c0x[k]) * MXMYMULT);
					pis->my = (c1y[k] + (c1y[k]-c0y[k]) * MXMYMULT);
				}
			}
		}
	}
#endif

	for (k = 0; k < CLUBMARK_MAXLABEL; k++) {
		pclm = &clm[k];
		pis = &pclm->imgseg;
		if (pis->state == IMAGESEGMENT_FULL) {
			U32 count;

#define MAXRRR	(128*128)
			double xx[MAXRRR];
			double yy[MAXRRR];
			double ma[2];
			double r2;

			count = 0;
			for (i = pis->iyu; i < (I32)pis->iyb; i++) {
				for (j = pis->ixl; j <= (I32)pis->ixr; j++) {
					if (*(pimg + j + i * width) == k) {
						xx[count] = j;
						yy[count] = i;
						count++;
						if (count >= MAXRRR) {
							break;
						}
					}
				}
				if (count >= MAXRRR) {
					break;
				}
			}
			if (count > 10) {
				cr_regression2(yy, xx, count, &ma[1], &ma[0], &r2);
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]: %lf %lf, %lf\n", k, ma[1], ma[0], r2);
			}
		}
	}
			

	UNUSED(pcm);
	
	res = 1;
//	bufEdge;
	return res;
}


static I32 ClassifyClubmarkClass(	
		clubmark_t *pcm,
		clubmark_seg_t	*pclm,
		U08 *pimg, 
		I32 segindex,
		I32 width, I32 height
		) 
{
	I32 res;
	imagesegment_t 	*pis;
	I32 i, j, k;
	I32 raiseflag;

	double th[CLUBMARK_BAR_CLASSCOUNT+1];
	I32 segwidth, segheight;
	U32 count;

	//--
	pis = &pclm->imgseg;

	count = pis->pixelcount;
	segwidth  = pis->ixr - pis->ixl + 1;
	segheight = pis->iyb - pis->iyu + 1;


#define MIN_BAR_PIXELCOUNT	100
#define MIN_BAR_WIDTH	5
#define MIN_BAR_HEIGHT	5
#define MAX_BAR_WIDTH	200
#define MAX_BAR_HEIGHT	200

	if (count < MIN_BAR_PIXELCOUNT) {
		pclm->ccvalid = 0;
		res = 0;
		goto func_exit;
	}

	if (	(segwidth < MIN_BAR_WIDTH) 	|| (segheight < MIN_BAR_HEIGHT)
		||	(segwidth > MAX_BAR_WIDTH) 	|| (segheight > MAX_BAR_HEIGHT))
	{
		pclm->ccvalid = 0;
		res = 0;
		goto func_exit;
	}


	{	// Get shape a_, b_, c_ of image segment.
		I32 count;
		I32 rectw, recth;
		double sx, sy, sx2, sy2, sxy;
		double a_, b_, c_;

		//--
		sx = 0; sy = 0; sx2 = 0; sy2 = 0; sxy = 0;
		count = 0;
		for (i = pis->iyu; i <= (I32)pis->iyb; i++) {
			for (j = pis->ixl; j <= (I32)pis->ixr; j++) {
				if (*(pimg + j + i * width) == segindex) {
					sx  += j;
					sy  += i;
					sx2 += j*j;
					sy2 += i*i;
					sxy += j*i;

					count++;
				}
			}
		}

		rectw = pis->ixr - pis->ixl + 1;
		recth = pis->iyb - pis->iyu + 1;

#define TINYVALUE	(1e-20)
#define BIGVALUE	(1e20)

		if (rectw > recth) {
			a_ = count * sxy - sx*sy;
			b_= -(count *  sx2 - sx*sx);
		} else {
			a_ = count * sy2 - sy*sy;
			b_ = -(count *  sxy - sx*sy);
		}
		c_ = -(b_ * sy + a_ * sx);
		pis->a_ = a_;
		pis->b_ = b_;
		pis->c_ = c_;

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%2d> a_: %lf b_: %lf c: %lf\n", segindex, a_, b_, c_);
	
		if (fabs(a_) < TINYVALUE && fabs(b_) < TINYVALUE) {
			pclm->ccvalid = 0;
			raiseflag = 0;
			res = 0;
			goto func_exit;
		}
		
		if (fabs(a_) >= fabs(b_)) {	
			raiseflag = 1;				// |m| >= 1
			pclm->markclass = CLUBMARK_MARKCLASS_VERTICAL;
		} else {
			raiseflag = 2;				// |m| < 1
			pclm->markclass = CLUBMARK_MARKCLASS_HORIZONTAL;
		}
	}

	{	// get (xi, yi) points and threshold.
		I32 x0, xM, y0, yM;
		I32 count0, countM;
		double deltav;
		//double m_, p_;
		double a_, b_, c_;
		double c0, cM;
		double xx[CLUBMARK_BAR_CLASSCOUNT+1];
		double yy[CLUBMARK_BAR_CLASSCOUNT+1];
	

		//--
		//m_ = 0;
		//p_ = 0;

		a_ = pis->a_;
		b_ = pis->b_;
		c_ = pis->c_;

		if (raiseflag == 1) {		// |m| >= 1   with yi value.
			// get y0 and yM
			y0 = pis->iyu;
			yM = pis->iyb;
			x0 = 0; xM = 0; count0 = 0; countM = 0;
			for (j = pis->ixl; j <= (I32)pis->ixr; j++) {
				if (*(pimg + j + y0 * width) == segindex) {
					x0 += j;
					count0++;
				}
				if (*(pimg + j + yM * width) == segindex) {
					xM += j;
					countM++;
				}
			}
			if (count0 > 0) {
				x0 /= count0;
			} else {
				x0 = (pis->ixl + pis->ixr)/2;
			}
			if (countM > 0) {
				xM = xM / countM;
			} else {
				xM = (pis->ixl + pis->ixr)/2;
			}

			c0 = -a_ * x0 - b_ * y0;
			cM = -a_ * xM - b_ * yM;
			c_ = (c0+cM) / 2.0;				// :)
		
			deltav = (yM - y0) / (double) CLUBMARK_BAR_CLASSCOUNT;
			for (i = 0; i < CLUBMARK_BAR_CLASSCOUNT+1; i++) {
				yy[i] = y0 + deltav * i;
				xx[i] = (1.0 / a_) * (-b_ * yy[i] - c_);

				th[i] = (yy[i] - (b_ / a_) * xx[i]);
			}
		
		} else {				// |m| < 1
			// get x0 and xM
			x0 = pis->ixl;
			xM = pis->ixr;
			y0 = 0; yM = 0; count0 = 0; countM = 0;
			for (i = pis->iyu; i <= (I32)pis->iyb; i++) {
				if (*(pimg + x0 + i * width) == segindex) {
					y0 += i;
					count0++;
				}
				if (*(pimg + xM + i * width) == segindex) {
					yM += i;
					countM++;
				}
			}
			if (count0 > 0) {
				y0 /= count0;
			} else {
				y0 = (pis->iyu + pis->iyb)/2;
			}
			if (countM > 0) {
				yM /= countM;
			} else {
				yM = (pis->iyu + pis->iyb)/2;
			}

			c0 = -a_ * x0 - b_ * y0;
			cM = -a_ * xM - b_ * yM;
			c_ = (c0+cM) / 2.0;				// :)

			deltav = (xM - x0) / (double)CLUBMARK_BAR_CLASSCOUNT;
			for (i = 0; i < CLUBMARK_BAR_CLASSCOUNT+1; i++) {
				xx[i] = x0 + deltav * i;
				yy[i] = (1.0 / b_) * (-a_ * xx[i] - c_);
				th[i] = (xx[i] - (a_/b_) * yy[i]);
			}
		}
	}


	{
		I32 ccount[CLUBMARK_BAR_CLASSCOUNT];
		I32 index;
		double cx[CLUBMARK_BAR_CLASSCOUNT], cy[CLUBMARK_BAR_CLASSCOUNT];
		double a_, b_, c_;

		//--
		a_ = pis->a_;
		b_ = pis->b_;
		c_ = pis->c_;
		for (index = 0; index < CLUBMARK_BAR_CLASSCOUNT; index++) {
			cx[index]  	 = 0;
			cy[index]  	 = 0;
			ccount[index] = 0;
		}

		for (i = pis->iyu; i <= (I32)pis->iyb; i++) {
			for (j = pis->ixl; j <= (I32)pis->ixr; j++) {
				if (*(pimg + j + i * width) == segindex) {
					double x, y;
					double value;

					//--
					y = i;
					x = j;

					// p = b/a,  m = - a/b
					if (raiseflag == 1) {		// |m| >= 1  
						value = y - (b_/a_) * x;    // y - p x
					} else {					// |m| < 1 
						value = x  - (a_/b_) * y;	// x - (1/p) y
					}

					if (value < th[1]) {
						index = 0;
					} else {
						for (k = 1; k < CLUBMARK_BAR_CLASSCOUNT; k++) {
							index = k;
							if (value >=th[k] && value < th[k+1]) {
								break;
							}
						}
					}

					cx[index] += x;
					cy[index] += y;
					ccount[index]++;
				}
			}
		}

		for (index = 0; index < CLUBMARK_BAR_CLASSCOUNT; index++) {
			if (ccount[index] > 0) {
				cx[index]  	 /= ccount[index];
				cy[index]  	 /= ccount[index];
			} else {
				cx[index]  	 = 0;
				cy[index]  	 = 0;
			}
			pclm->ccP[index].x = cx[index];
			pclm->ccP[index].y = cy[index];
			pclm->ccP[index].z = 0;

		}
	}

	res = 1;
	pclm->ccvalid = 1;

	UNUSED(height);
	UNUSED(pcm);
func_exit:
	return res;
}

static I32 SelectClubmark(
		iana_t *piana, U32 camid, 
		clubmark_t *pcm,
		U32 width,
		U32 height,
		double bx,
		double by,
		double br,

		double xfact, double yfact,
		double l1cm,
		U08 *bufBin,
		clubmark_seg_t	clm[]
		)
{
	I32 res;
	U32 k;
	U32 i;
	double balldistcheck2;
	imagesegment_t 	*pis;
	rectObj_t *pro;
	clubmark_seg_t	*pclm;

	//--
	// Ball position


#define BALLDISTRATE		(0.9) // 1.1, 0.00001?
#define RECTFILLRATE		(0.9) // 0.7, 1.1
#define RECTFILLRATE2		(1.2)
#define RECTBOUNDARYMARGIN	5 // 2, 10
	balldistcheck2 = (br*br)*BALLDISTRATE;

	//-- 
	for (k = 0; k < CLUBMARK_MAXLABEL; k++) {
		pclm= &clm[k];
		pis = &pclm->imgseg;
		pro = &pclm->rectObj;
		if (pis->state == IMAGESEGMENT_FULL) {
			I32 isdot;
			I32 isbar;
			U32 checkgood;

			//pclm->state = CLUBMARK_UNDECIDED;
			if ((pclm->state == CLUBMARK_NULL) || (pclm->state == CLUBMARK_EXILE)) {
				continue;
			}
			isdot = 0;
			isbar = 0;

			checkgood = 1;
			{
				 // 1) Check all vertex distance..
				double dx, dy;		// , dist;
				double degree;
				for (i = 0; i < 4; i++) {
					dx = pro->p[i].x - bx;
					dy = pro->p[i].y - by;


					degree = RADIAN2DEGREE(atan2(dy, dx));
#define DYCHECK_MIN	(4.2 * 4)
					if ( (-dy > (DYCHECK_MIN * l1cm)) && (-degree > (90.0 - 45.0))) {
						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%3d]dy: %lf, degree: %lf\n", k, dy, degree);
						checkgood = 0;
						break;
					}
				}
			}

			if (checkgood == 1) {
				I32 objw, objh;
				// 2) Check Object size
				objw = (I32) (pis->ixr - pis->ixl + 1);
				objh = (I32) (pis->iyb - pis->iyu + 1);

				if (objw > 200 || objh > 200) {
					checkgood = 0;
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                    //      "checkgood: %d at %d  (objw: %d, objh: %d)\n",
					//		checkgood, __LINE__,
					//		objw, objh);
				}
			} else {
		//		continue;
			}

			UNUSED(width);UNUSED(height);


			if (checkgood == 1) {
				double countv, areav;
				// 3) Check Fill rate
				countv = pro->pixelcount;
				areav = pro->areav;


				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                //      "pro->pixelcount: %d\n", pro->pixelcount);
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                //      "(%d %d) (%d %d) (%d %d) (%d %d)\n",
				//	    (int)pro->p[0].x, (int)pro->p[0].y,
				//	    (int)pro->p[1].x, (int)pro->p[1].y,
				//	    (int)pro->p[2].x, (int)pro->p[2].y,
				//	    (int)pro->p[3].x, (int)pro->p[3].y);
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                //      "areav: %lf, vs: %lf\n", areav, (areav* RECTFILLRATE));
				
                if (countv < (areav * RECTFILLRATE)) {
					checkgood = 0;

					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                    //      "checkgood: 0 -> %d at %d  countv: %lf  (vs %lf * %lf = %lf)\n",
					//		checkgood, __LINE__,
					//		countv,
					//		areav, RECTFILLRATE,
					//		(areav* RECTFILLRATE));
				}

				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                //      "checkgood: %d (%lf / %lf) = %lf, (%lf / %lf) = %lf\n",
				//		checkgood,
				//		countv, areav, countv/ areav,
				//		(double)pro->pixelcount2, areav, (double)pro->pixelcount2/ areav);

			} else {
				//continue;
			}

		//	if (checkgood == 1) 
			{
//				I32 isdot;
//				I32 isbar;
				{
//					I32 i0, j0;
					//double mindotlen, maxdotlen;
					//double mindotpixel;
					U32 mindotlen_w, mindotlen_h;
					U32 maxdotlen;
					U32 mindotpixel;
					U32 minlongdot_width;
					U32 minlongdot_height;
//					double dx, dy;
//					double dlen;

#define MINDOTLEN_W	0.2
#define MINDOTLEN_H	0.3
#define MAXDOTLEN	1.0

#define MINDOTPIXEL_CAM0	0.2
#define MINDOTPIXEL_CAM1	0.2

#define MIN_LONGDOTPIXEL_W	0.2

#define MIN_LONGDOTPIXEL_H	0.4
#define MIN_LONGDOTPIXEL	0.4

					mindotlen_w = (U32) (l1cm * MINDOTLEN_W * xfact);
					mindotlen_h = (U32) (l1cm * MINDOTLEN_H * yfact);
					maxdotlen = (U32) (l1cm * MAXDOTLEN * yfact);
					if (camid == 0) {
						mindotpixel = (U32)((l1cm *MINDOTPIXEL_CAM0 * xfact)* (l1cm *MINDOTPIXEL_CAM0 * yfact));
					} else {
						mindotpixel = (U32)((l1cm *MINDOTPIXEL_CAM1 * xfact)* (l1cm *MINDOTPIXEL_CAM1 * yfact));
					}

					minlongdot_width = (U32)(l1cm *MIN_LONGDOTPIXEL_W * xfact);
					minlongdot_height = (U32)(l1cm *MIN_LONGDOTPIXEL_H * yfact);

					isdot = 1;

					if (pro->pixelcount < mindotpixel) {
						isdot = 0;
					} else {
						I32 objw, objh;
						// Check Object size

						objw = (I32) (pis->ixr - pis->ixl + 1);
						objh = (I32) (pis->iyb - pis->iyu + 1);
						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "w, h: %d %d\n", objw, objh);

						if (objw < (I32)mindotlen_w || objh < (I32)mindotlen_h) {
							isdot = 0;
						}

						if (objw > (I32)maxdotlen || objh > (I32)maxdotlen) {
							isdot = 0;
						}

						if (objw < (I32)minlongdot_width || objh < (I32)minlongdot_height) {
							isdot = 0;
						}
						
#define CHECK_X_MARGIN	20
						if (pis->ixr > width - CHECK_X_MARGIN || pis->ixl < CHECK_X_MARGIN) {
							isdot = 0;
						}
					}
				}
				if (isdot) {
					pclm->state = CLUBMARK_DOT;
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "1  DOT: %d\n", k);
				} else  {
					if (checkgood == 1) {
						U32 maxvertexlenIndex;
						double maxvertexlen;
						double vertexdist;

						double minbarthick, maxbarthick;
						double minbarlen, maxbarlen;

						minbarthick = l1cm * MINBARTHICK * xfact;
						maxbarthick = l1cm * MAXBARTHICK * yfact;

						minbarlen = l1cm * MINBARLEN * xfact;
						maxbarlen = l1cm * MAXBARLEN * yfact;


						isbar = 1;

						maxvertexlenIndex = pro->maxvertexlenIndex;
						maxvertexlen = pro->vertexlen[maxvertexlenIndex];
						vertexdist = pro->vertexdist[maxvertexlenIndex];
						{
							I32 j0;
							double vl0, vl1;
							double vl;
							double vertextdist_min;

							j0 = maxvertexlenIndex + 1; j0 = j0 % 4;
							vl0 = pro->vertexlen[j0];

							j0 = j0 + 2; j0 = j0 % 4;
							vl1 = pro->vertexlen[j0];

							if (vl0 < vl1) {
								vl = vl0;
							} else {
								vl = vl1;
							}

#define VDIST_LEN_RATE		(0.8)
#define VDIST_LEN_RATE_M	(0.4)
							if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_BARDOT) {
								vertextdist_min = vl * VDIST_LEN_RATE;
							} else {
								vertextdist_min = vl * VDIST_LEN_RATE_M;
							}

							if (vertexdist < vertextdist_min) {
								isbar = 0;
								/*
								cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                                        "[%d] isbar: %d at %d vertexdist %lf vs %lf\n", camid, isbar, __LINE__,
										vertexdist, vertextdist_min);
								*/
							} else {
								if (
										(vertexdist > minbarthick && vertexdist < maxbarthick) 
										&& (maxvertexlen > minbarlen && maxvertexlen < maxbarlen)
								   ) {
									isbar = 1;
								} else {
									isbar = 0;
								}
								//if (isbar) 
								//{
								//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                                //          "[%d] isbar: %d \n vertexdist: %lf minbarthick: %lf maxbarthick: %lf\n maxvertexlen: %lf minbarlen: %lf maxbarlen: %lf\n", 
								//			camid, isbar, vertexdist, minbarthick, maxbarthick, 
								//		    maxvertexlen, minbarlen, maxvertexlen);
								//}
							}

						}

						//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
						//			"[%d] MINBARTHICK %lf MAXBARTHICK %lf MINBARLEN %lf MAXBARLEN %lf, maxvertexlen: %lf (%lf) , vertexdist: %lf (%lf)     isbar: %d\n", 
						//			camid,
						//			MINBARTHICK, MAXBARTHICK, MINBARLEN, MAXBARLEN,
						//			maxvertexlen,
						//			maxvertexlen / (l1cm * xfact),      //  maxvertexlen / (g_l1cm[camid] * xfact),
						//			vertexdist,
						//			vertexdist / (l1cm * xfact),        //  vertexdist / (g_l1cm[camid] * xfact), isbar);
                        //          isbar);

						if (isbar) {
							pclm->state = CLUBMARK_BAR;
							//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "1  BAR: %d\n", k);
						} 
					} 		// if (checkgood == 1) 
				}			// isdot?
			}

			{
				U32 neetcheckdist;

				//--
				balldistcheck2 = (br*br)*BALLDISTRATE;
				neetcheckdist = 0;
				if (isdot) {
					neetcheckdist = 1;
				}

				if (isbar) {
					if (pcm->clubcalc_method != CLUBCALC_CLUB_MARK_TOPBAR) {
						neetcheckdist = 1;
					} else {
#define BALLDISTRATE_TOPBAR	(0.4)
						balldistcheck2 = (br*br)*BALLDISTRATE_TOPBAR;
						neetcheckdist = 1;
					}
				}

				if (neetcheckdist) {
					double dx, dy, dist;
					//double degree;

					for (i = 0; i < 4; i++) {
						dx = pro->p[i].x - bx;
						dy = pro->p[i].y - by;
						dist = dx*dx + dy*dy;
						if (dist < balldistcheck2) {
							pclm->state = CLUBMARK_EXILE;
							pclm->exilewhy = CLUBMARK_EXILEWHY_TOOCLOSE_BALL;
							break;
						}
					}
				}
			}

			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
            //      "[%d] pclm->state: %d, pis: (%3d, %3d) ~ (%3d, %3d) isdot, isbar: %d, %d checkgood: %d\n", 
			//		k, pclm->state,
			//		pis->ixl, pis->iyu,
			//		pis->ixr, pis->iyb,
			//		isdot, isbar,
			//		checkgood
			//		);
		} 		// if (pis->state == IMAGESEGMENT_FULL)
	}

	res = 1;

	UNUSED(bufBin);
	//camid;
	UNUSED(piana);

	UNUSED(pcm);
	return res;
}

static I32 GetBarDotCount(	
		iana_t *piana, 
		U32 camid, 
		clubmark_t *pcm,
		clubmark_seg_t	clm[],
		U32 index)
{
	I32 res;
	U32 i0;
	U32 dot_count;
	U32 bar_count;

	clubmark_dot_t *pcmd;
	clubmark_bar_t *pcmb;
	imagesegment_t 	*pis;
	rectObj_t *pro;

	clubmark_seg_t	*pclm;
	clubmark_cam_t *pcmc;
	clubmark_feature_t	*pcft;
	//---

	
	pcmc = &pcm->cmc[camid];
	
	dot_count = 0;
	bar_count = 0;
	
	pcft = &pcmc->cft[index];
	pcmd = &pcft->cmd;
	pcmb = &pcft->cmb;
	
	
	for (i0 = 0; i0 < CLUBMARK_MAXLABEL; i0++) {
		pclm = &clm[i0];
		pis = &pclm->imgseg;
		pro = &pclm->rectObj;

		if (pis->state == IMAGESEGMENT_FULL) {
			if (dot_count < MAXCANDIDATE) {
				if (pclm->state == CLUBMARK_DOT) {
					memcpy(&pcmd->clm[dot_count], &clm[i0], sizeof(clubmark_seg_t));
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "DOT: %d => %d\n", i0, dot_count);

					dot_count++;
				}
			}
			if (bar_count < MAXCANDIDATE) {
				if (pclm->state == CLUBMARK_BAR) {
					memcpy(&pcmb->clm[bar_count], &clm[i0], sizeof(clubmark_seg_t));
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BAR: %d => %d\n", i0, bar_count);

					bar_count++;
				}
			}
		}
	}
	pcmd->count = dot_count;
	pcmb->count = bar_count;

	res = 1;

	UNUSED(piana); 
	UNUSED(camid); 
	return res;
}

static I32 ExileClubmarkData(iana_t *piana, U32 camid, 
		clubmark_t *pcm,
		U32 width, U32 height, U32 allowbar, U32 allowdot)
{
	I32 res;
	I32 index;
	I32 indexshot, index0, index1;
	I32 k, i, j;
	I32 i0, j0;

	rectObj_t 		*pro;
	
	cr_point_t p0, p1;
	cr_line_t line01;

	U32 count;
	U32 maxcount;
	cr_line_t maxcountline;

	double dist;
	double checkoutlier_distance;


	double xfact, yfact;
	U32 offset_x0, offset_y0;
	U32 offset_x1, offset_y1;

	clubmark_cam_t *pcmc;
//	clubmark_feature_t	*pcft;

	U32 upperLpValid;
	U32 lowerLpValid;
	point_t upperLp, upperPp;
	point_t lowerLp, lowerPp;
	//-----------------------------------
	pcmc = &pcm->cmc[camid], 

	indexshot = pcmc->indexshot;
	index0 = pcmc->index0;
	index1 = pcmc->index1;
	xfact = pcmc->xfact;
	yfact = pcmc->yfact;

#define CHECK_OUTLIER_DISTANCE		5
#define CHECK_OUTLIER_DISTANCE_CAM0		10 // 5						// 12mm
#define CHECK_OUTLIER_DISTANCE_CAM1		30  // 10, 3						// 25mm 
	upperLpValid = 0;
	upperLp.x = upperLp.y = 0; 
	upperPp.x = upperPp.y = 0; 
	lowerLpValid = 0;
	lowerLp.x = lowerLp.y = 0; 
	lowerPp.x = lowerPp.y = 0;
	if (camid == 0) {
		checkoutlier_distance = 9999;
	} else {
		checkoutlier_distance = 9999;
		if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_BARDOT) {
			clubmark_cam_t *pcmc0;
			clubmark_feature_t	*pcft0;
			clubmark_face_t *pcfc0;
			I32 indexshot_0, index0_0;

			//-----
			pcmc0 = &pcm->cmc[0];					// for CAM0 data
			index0_0 = pcmc0->index0;
			indexshot_0 = pcmc0->indexshot;

			lowerLpValid = 0;
			for (index = indexshot_0-2; index >= index0_0; index--) {
				pcft0 = &pcmc0->cft[index];
				pcfc0 = &pcmc0->face[index];
				if (pcfc0->lowerdot.valid) {
					memcpy(&lowerLp, &pcfc0->lowerdot.cdotL, sizeof(point_t));
					iana_L2P(piana, camid, &lowerLp, &lowerPp, 0);
					lowerLpValid = 1;
					break;
				}
			}

			upperLpValid = 0;
			//for (index = indexshot_0; index >= index0_0; index--) 
			for (index = indexshot_0-2; index >= index0_0; index--) 
			{
				pcft0 = &pcmc0->cft[index];
				pcfc0 = &pcmc0->face[index];
				if (pcfc0->upperline.valid) {
					memcpy(&upperLp, &pcfc0->upperline.clineL.p0, sizeof(point_t));
					iana_L2P(piana, camid, &upperLp, &upperPp, 0);
					upperLpValid = 1;
					break;
				}
			}
		}
	}


#define CHECKJUMPVALUE	3
	//------------------------------
	{ 	// Check DOT and exile..
		clubmark_dot_t *pcmd0, *pcmd1, *pcmd2;
		clubmark_feature_t	*pcft0, *pcft1, *pcft2;


		// Check Dot position
		for (index = index0; index <= index1; index++) {
			point_t Pb;
			double Pr;
			//pcmd0 = &pcmc->cmd[index];

			pcft0 = &pcmc->cft[index];
			pcmd0 = &pcft0->cmd;


			//offset_x0 = pcmd0->offset_x; offset_y0 = pcmd0->offset_y;
			offset_x0 = pcft0->offset_x;
			offset_y0 = pcft0->offset_y;
			if (pcmd0->count) {
				double x0, y0;
				double xmax, ymax;
				double xmin, ymin;

				for (i = 0; i < (I32)pcmd0->count; i++) {
					if (pcmd0->clm[i].state != CLUBMARK_DOT) {
						continue;
					}

					if (allowdot == 0) {			// allow dot?????
						pcmd0->clm[i].state		= CLUBMARK_EXILE;
						pcmd0->clm[i].exilewhy 	= CLUBMARK_EXILEWHY_DOT_PROHIBITED;
						continue;
					}

					//memcpy(&Pb, &pcmc->Pp[index], sizeof(point_t));
					//Pr = pcmc->Pr[index];
					memcpy(&Pb, &pcft0->Pp, sizeof(point_t));
					Pr = pcft0->Pr;

					pro = &pcmd0->clm[i].rectObj;
					x0 = pro->p[0].x + pro->p[1].x + pro->p[2].x + pro->p[3].x;
					y0 = pro->p[0].y + pro->p[1].y + pro->p[2].y + pro->p[3].y;
					p0.x = x0/(4.0 * xfact) + offset_x0; p0.y = y0/(4.0 * yfact) + offset_y0; p0.z = 0;

					{
						U32 k;
						xmax = pro->p[0].x; xmin = pro->p[0].x;
						ymax = pro->p[0].y; ymin = pro->p[0].y;
						for (k = 1; k < 4; k++) {
							if (xmax < pro->p[k].x) { xmax = pro->p[k].x; }
							if (xmin > pro->p[k].x) { xmin = pro->p[k].x; }
							if (ymax < pro->p[k].y) { ymax = pro->p[k].y; }
							if (ymin > pro->p[k].y) { ymin = pro->p[k].y; }
						}
					}

#define DOT_XY_MARGIN	6
					if (
							(xmin < DOT_XY_MARGIN) || (xmax > width - DOT_XY_MARGIN) 
							|| (ymin < DOT_XY_MARGIN) || (ymax > height - DOT_XY_MARGIN) 
					   ) {
						pcmd0->clm[i].state = CLUBMARK_EXILE;
						pcmd0->clm[i].exilewhy = CLUBMARK_EXILEWHY_TOUCHED_EDGE;
						continue;
					}

					{
						double dx, dy, dist;

						dx = (p0.x - Pb.x); dy = (p0.y - Pb.y);
						dist = sqrt(dx*dx + dy*dy);

#define BALLPOS_DOT_DIST_MULT 1.3 // 1.0, 1.5
						if (dist < Pr * BALLPOS_DOT_DIST_MULT) {
							pcmd0->clm[i].state = CLUBMARK_EXILE;
							pcmd0->clm[i].exilewhy = CLUBMARK_EXILEWHY_TOOCLOSE_BALL;
							continue;
						}
					}

					if (allowbar != 0) {				// allow bar... == check dot Y postion 
						if (lowerLpValid) {
							double lowermaxy;
#define LOWERMAXY_MARGIN	20
							//lowermaxy = (lowerPp.y - offset_y0) * yfact;
							lowermaxy = lowerPp.y;
							if (piana->Right0Left1 == 0) {
								lowermaxy = lowermaxy - LOWERMAXY_MARGIN;
								if (p0.y < lowermaxy) {
									pcmd0->clm[i].state = CLUBMARK_EXILE;
									pcmd0->clm[i].exilewhy = CLUBMARK_EXILEWHY_SMALLER_LOWERMAXY1;
									continue;
								}
							} else {		// LEFT
								lowermaxy = lowermaxy + LOWERMAXY_MARGIN;
								if (p0.y > lowermaxy) {
									pcmd0->clm[i].state = CLUBMARK_EXILE;
									pcmd0->clm[i].exilewhy = CLUBMARK_EXILEWHY_SMALLER_LOWERMAXY1;
									continue;
								}
							}
						} else {
#define BALLPOS_DOT_MULT	1.0
							if (piana->Right0Left1 == 0) {
								if (p0.y < Pb.y - Pr*BALLPOS_DOT_MULT) {				// Check y position of DOT
									pcmd0->clm[i].state = CLUBMARK_EXILE;
									pcmd0->clm[i].exilewhy = CLUBMARK_EXILEWHY_SMALLER_LOWERMAXY2;
									continue;
								}
							} else {		// LEFT
								if (p0.y > Pb.y + Pr*BALLPOS_DOT_MULT) {				// Check y position of DOT
									pcmd0->clm[i].state = CLUBMARK_EXILE;
									pcmd0->clm[i].exilewhy = CLUBMARK_EXILEWHY_SMALLER_LOWERMAXY2;
									continue;
								}
							}
						}
					}  // if (allowbar)       == check dot Y postion 
				}
			}
		}

		maxcount = 0;
		for (k = 1; k < CHECKJUMPVALUE; k++) {
			for (index = index0; index <= index1-k; index++) 
			{
				pcft0 = &pcmc->cft[index];
				pcmd0 = &pcft0->cmd;

				offset_x0 = pcft0->offset_x; 
                offset_y0 = pcft0->offset_y;
				pcft1 = &pcmc->cft[index+k];
				pcmd1 = &pcft1->cmd;

				offset_x1 = pcft1->offset_x; 
                offset_y1 = pcft1->offset_y;
				if (pcmd0->count && pcmd1->count) {
					double x0, y0;
					double x1, y1;
					for (i = 0; i < (I32)pcmd0->count; i++) {
						if (pcmd0->clm[i].state != CLUBMARK_DOT) {
							continue;
						}
						pro = &pcmd0->clm[i].rectObj;
						x0 = pro->p[0].x + pro->p[1].x + pro->p[2].x + pro->p[3].x;
						y0 = pro->p[0].y + pro->p[1].y + pro->p[2].y + pro->p[3].y;
						p0.x = x0/(4.0 * xfact) + offset_x0; p0.y = y0/(4.0 * yfact) + offset_y0; p0.z = 0;

						for (j = 0; j < (I32)pcmd1->count; j++) {
							cr_point_t pdiff;
							double difflen;

							if (pcmd1->clm[j].state != CLUBMARK_DOT) {
								continue;
							}

							pro = &pcmd1->clm[j].rectObj;
							x1 = pro->p[0].x + pro->p[1].x + pro->p[2].x + pro->p[3].x;
							y1 = pro->p[0].y + pro->p[1].y + pro->p[2].y + pro->p[3].y;
							p1.x = x1/(4.0 * xfact) + offset_x1; p1.y = y1/(4.0 * yfact) + offset_y1; p1.z = 0;

							cr_vector_sub(&p0, &p1, &pdiff);
							difflen = cr_vector_norm(&pdiff);

#define CHECK_TOOCLOSE_DISTANCE		3
							if (difflen < CHECK_TOOCLOSE_DISTANCE) {
								continue;				// too close... :P
							}

							cr_line_p0p1(&p0, &p1, &line01);		// candidate line.

							count = 0;
							for (i0 = index0; i0 <= index1; i0++) {
							
                                pcft2 = &pcmc->cft[i0];
								pcmd2 = &pcft2->cmd;
								for (j0 = 0; j0 < (I32)pcmd2->count; j0++) {
									pro = &pcmd2->clm[j0].rectObj;
									x0 = pro->p[0].x + pro->p[1].x + pro->p[2].x + pro->p[3].x;
									y0 = pro->p[0].y + pro->p[1].y + pro->p[2].y + pro->p[3].y;
								
									p0.x = x0/(4.0 * xfact) + offset_x0; 
                                    p0.y = y0/(4.0 * yfact) + offset_y0; 
                                    p0.z = 0;

									dist = cr_distance_P_Line(&p0, &line01);
									
                                    if (dist < checkoutlier_distance) {
										count++;
									}
								}
							}

							if (maxcount < count) {
								maxcount = count;
								memcpy(&maxcountline, &line01, sizeof(cr_line_t));
							}
						} 	// for (j = 0; j < pcmd1->count; j++) 
					} 		// for (i = 0; i < pcmd0->count; i++) 
				} 			// if (pcmd0->count && pcmd1->count) 
			} 				// for (index = index0; index <= index1-k; index++) 
		} 					// for (k = 1; k < 3; k++) 


		if (maxcount > 2) {
			for (index = index0; index <= index1; index++) {

				pcft0 = &pcmc->cft[index];
				pcmd0 = &pcft0->cmd;
				
				offset_x0 = pcft0->offset_x; offset_y0 = pcft0->offset_y;
				for (i = 0; i < (I32)pcmd0->count; i++) {
					double x0, y0;

					pro = &pcmd0->clm[i].rectObj;
					x0 = pro->p[0].x + pro->p[1].x + pro->p[2].x + pro->p[3].x;
					y0 = pro->p[0].y + pro->p[1].y + pro->p[2].y + pro->p[3].y;
				
					p0.x = x0/(4.0 * xfact) + offset_x0; 
                    p0.y = y0/(4.0 * yfact) + offset_y0; 
                    p0.z = 0;

					dist = cr_distance_P_Line(&p0, &maxcountline);
					if (dist > checkoutlier_distance) {

						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                        //  "DOT [%d:%d] EXILE (%lf). state %d -> %d\n", 
                        //  index, i, dist, pcmd0->clm[i].state, CLUBMARK_EXILE);

						pcmd0->clm[i].state = CLUBMARK_EXILE;
						pcmd0->clm[i].exilewhy = CLUBMARK_EXILEWHY_OUTLIER;
					} else {

						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "DOT [%d:%d] NNNNN (%lf).\n", index, i, dist);

					}
				}
			}

			for (index = index0; index <= index1; index++) {
				U32 maxpcount;
				U32 maxpcounti;

				pcft0 = &pcmc->cft[index];
				pcmd0 = &pcft0->cmd;

				offset_x0 = pcft0->offset_x; offset_y0 = pcft0->offset_y;
				count = 0;
				maxpcount = 0;
				maxpcounti = 0;
				for (i = 0; i < (I32)pcmd0->count; i++) {
					if (pcmd0->clm[i].state == CLUBMARK_DOT) {
						pro = &pcmd0->clm[i].rectObj;
						if (maxpcount < pro->pixelcount) {
							maxpcount = pro->pixelcount;
							maxpcounti = i;
						}
						count++;
					}
				}

				//if (count > 1) {
				//	for (i = 0; i < (I32)pcmd0->count; i++) {
				//		if (pcmd0->clm[i].state == CLUBMARK_DOT && i != (I32)maxpcounti) {
				//			pcmd0->clm[i].state = CLUBMARK_EXILE;
				//			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "DOT [%d:%d] EXILE .. Winner (%d) takes it all.\n", index, i, maxpcounti);
				//		}
				//	}
				//}
			}
		}
	} 	// Check DOT and exile..

	//------------------------------
	{ 	// Check BAR and exile..
		clubmark_bar_t *pcmb0, *pcmb1, *pcmb2;
		clubmark_feature_t	*pcft0, *pcft1, *pcft2;
		clubmark_seg_t	*pclm0, *pclm1, *pclm2;

		// Check Bar position
		for (index = index0; index <= index1; index++) {
			point_t Pb;
			double Pr;

			pcft0 = &pcmc->cft[index];
			pcmb0 = &pcft0->cmb;

			offset_x0 = pcft0->offset_x; offset_y0 = pcft0->offset_y;
			if (pcmb0->count) {
				double x0, y0;


				for (i = 0; i < (I32)pcmb0->count; i++) {
					double y0_min;

					pclm0 = &pcmb0->clm[i];
					if (pclm0->state != CLUBMARK_BAR) {
						continue;
					}
					if (allowbar == 0) {			// allow bar?????
						pclm0->state = CLUBMARK_EXILE;
						pclm0->exilewhy = CLUBMARK_EXILEWHY_BAR_PROHIBITED;
						continue;
					}
					memcpy(&Pb, &pcft0->Pp, sizeof(point_t));
					Pr = pcft0->Pr;

					//pro = &pcmb0->clm[i].rectObj;
					pro = &pclm0->rectObj;
					x0 = pro->p[1].x + pro->p[2].x;
					y0 = pro->p[1].y + pro->p[2].y;
					x0 = x0/(2.0 * xfact) + offset_x0;
					y0 = y0/(2.0 * yfact) + offset_y0;

#define BALLPOS_BAR_MULT	(1.5)
#define BALLPOS_MBAR_MULT	(6.0)
					if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_TOPBAR) {
						//y0_min = Pb.y - BALLPOS_MBAR_MULT*Pr;
						y0_min = Pb.y - 100;
						if (y0 < y0_min) {
							//pcmb0->clm[i].state = CLUBMARK_EXILE;
							pclm0->state = CLUBMARK_EXILE;
							pclm0->exilewhy = CLUBMARK_EXILEWHY_BAR_TOPBAR_SMALLY;
							continue;
						}
					} else {
						if (upperLpValid) {
							double upperminy;

#define UPPERMINY_MARGIN	40
							upperminy = upperPp.y;
							upperminy = upperminy + UPPERMINY_MARGIN;

							y0_min = upperminy;
						} else {
							y0_min = Pb.y - BALLPOS_BAR_MULT*Pr;
						}
					}

#define BAR_XY_MARGIN	10 // 3, 5 
					if ((pro->p[1].x > width - BAR_XY_MARGIN) || 
                        (pro->p[2].x > width - BAR_XY_MARGIN)) {
						
                        pclm0->state = CLUBMARK_EXILE;
						pclm0->exilewhy = CLUBMARK_EXILEWHY_BAR_TOUCHED_EDGE_X;
						continue;
					}

					if ((pro->p[1].y < BAR_XY_MARGIN) || 
                        (pro->p[2].y < BAR_XY_MARGIN)) {

						pclm0->state = CLUBMARK_EXILE;
						pclm0->exilewhy = CLUBMARK_EXILEWHY_BAR_TOUCHED_EDGE_Y;
						continue;
					}

					{
						if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_BARDOT) {
							if (pclm0->markclass != CLUBMARK_MARKCLASS_HORIZONTAL) {
								pclm0->state = CLUBMARK_EXILE;
								pclm0->exilewhy = CLUBMARK_EXILEWHY_BAR_NOT_HORIZONTAL;
								continue;
							}
						} 

						if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_TOPBAR) {
							if (pclm0->markclass != CLUBMARK_MARKCLASS_VERTICAL) {
								pclm0->state = CLUBMARK_EXILE;
								pclm0->exilewhy = CLUBMARK_EXILEWHY_BAR_NOT_VERTICAL;
								continue;
							}
						} 
					}

                    if (pcm->clubcalc_method != CLUBCALC_CLUB_MARK_TOPBAR) {
						U32 j;
						double dist2b;
						dist2b = 999;
						for (j = 0; j < 4; j++) {
							double dist;
							double dx, dy;
							double x0, y0;

							x0 = pro->p[j].x/xfact + offset_x0;
							y0 = pro->p[j].y/yfact + offset_y0;
							dx = x0 - Pb.x;
							dy = y0 - Pb.y;

							dist = sqrt(dx*dx + dy*dy);
							if (dist2b > dist) {
								dist2b = dist;
							}
						}
#define BALLPOS_BAR_TOOCLOSE_MULT	(1.2)
						if (dist2b < BALLPOS_BAR_TOOCLOSE_MULT*Pr) {
							cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BAR too close to ball. %lf < %lf EXILE\n", dist2b, BALLPOS_BAR_TOOCLOSE_MULT*Pr);
							pclm0->state = CLUBMARK_EXILE;
							pclm0->exilewhy = CLUBMARK_EXILEWHY_BAR_TOOCLOSE_BALL;
							continue;
						}
					}
				} 	// for (i = 0; i < (I32)pcmb0->count; i++) 
			}
		} 			// for (index = index0; index <= index1; index++) 

		if (allowbar != 0) {
			maxcount = 0;
			for (k = 1; k < CHECKJUMPVALUE; k++) {
				for (index = index0; index <= index1-k; index++) {
					pcft0 = &pcmc->cft[index];
					pcmb0 = &pcft0->cmb;
				
					offset_x0 = pcft0->offset_x; offset_y0 = pcft0->offset_y;
				
					pcft1 = &pcmc->cft[index+k];
					pcmb1 = &pcft1->cmb;
				
					offset_x1 = pcft1->offset_x; offset_y1 = pcft1->offset_y;
				
					if (pcmb0->count && pcmb1->count) {
						double x0, y0;
						double x1, y1;
						for (i = 0; i < (I32)pcmb0->count; i++) {
							if (pcmb0->clm[i].state != CLUBMARK_BAR) {
								continue;
							}
							pro = &pcmb0->clm[i].rectObj;
							x0 =  pro->p[1].x + pro->p[2].x;					// p1, p2
							y0 =  pro->p[1].y + pro->p[2].y;
							p0.x = x0/(2.0 * xfact) + offset_x0; p0.y = y0/(2.0 * yfact) + offset_y0; p0.z = 0;

							for (j = 0; j < (I32)pcmb1->count; j++) {
								cr_point_t pdiff;
								double difflen;

								if (pcmb1->clm[j].state != CLUBMARK_BAR) {
									continue;
								}

								pro = &pcmb1->clm[j].rectObj;
								x1 = pro->p[1].x + pro->p[2].x;			// p1, p2
								y1 = pro->p[1].y + pro->p[2].y;
								p1.x = x1/(2.0 * xfact) + offset_x1; p1.y = y1/(2.0 * yfact) + offset_y1; p1.z = 0;

								cr_vector_sub(&p0, &p1, &pdiff);
								difflen = cr_vector_norm(&pdiff);
								if (difflen < CHECK_TOOCLOSE_DISTANCE) {
									continue;				// too close... :P
								}

								cr_line_p0p1(&p0, &p1, &line01);		// candidate line.

								count = 0;
								for (i0 = index0; i0 <= index1; i0++) {
									//pcmb2 = &pcmc->cmb[i0];
									pcft2 = &pcmc->cft[i0];
									pcmb2 = &pcft2->cmb;
									for (j0 = 0; j0 < (I32)pcmb2->count; j0++) {
										pro = &pcmb2->clm[j0].rectObj;
										x0 = pro->p[1].x + pro->p[2].x;
										y0 = pro->p[1].y + pro->p[2].y;
										p0.x = x0/(2.0 * xfact) + offset_x0; p0.y = y0/(2.0 * yfact) + offset_y0; p0.z = 0;

										dist = cr_distance_P_Line(&p0, &line01);
										if (dist < checkoutlier_distance) {
											count++;
										}
									}
								}

								if (maxcount < count) {
									maxcount = count;
									memcpy(&maxcountline, &line01, sizeof(cr_line_t));
								}
							} 	// for (j = 0; j < pcmb1->count; j++) 
						} 		// for (i = 0; i < pcmb0->count; i++) 
					} 			// if (pcmb0->count && pcmb1->count) 
				} 				// for (index = index0; index <= index1-k; index++) 
			} 					// for (k = 1; k < 3; k++) 


			if (maxcount > 2) {
				for (index = index0; index <= indexshot; index++) 
				{

					pcft0 = &pcmc->cft[index];
					pcmb0 = &pcft0->cmb;

					offset_x0 = pcft0->offset_x; offset_y0 = pcft0->offset_y;
				
					for (i = 0; i < (I32)pcmb0->count; i++) {
						double x0, y0;
						pro = &pcmb0->clm[i].rectObj;
						x0 = pro->p[1].x + pro->p[2].x;				// p1, p2
						y0 = pro->p[1].y + pro->p[2].y;
						p0.x = x0/(2.0 * xfact) + offset_x0; p0.y = y0/(2.0 * yfact) + offset_y0; p0.z = 0;

						dist = cr_distance_P_Line(&p0, &maxcountline);
						if (dist > checkoutlier_distance) {
							//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BAR [%d:%d] EXILE (%lf). state %d -> %d\n", index, i, dist, pcmb0->clm[i].state, CLUBMARK_EXILE);
							pcmb0->clm[i].state = CLUBMARK_EXILE;
							pcmb0->clm[i].exilewhy = CLUBMARK_EXILEWHY_BAR_OUTLIER;
						} else {
							//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BAR [%d:%d] NNNNN (%lf).\n", index, i, dist);
						}
					}
				}
			}
		UNUSED(pclm1); UNUSED(pclm2);

			if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_TOPBAR) {			// 20200725.. select the ONE!!!
				for (index = index0; index <= indexshot; index++) {
					clubmark_feature_t	*pcft;
					clubmark_bar_t *pcmb;
					double minx;
					I32 minxi;

					//---
					pcft = &pcmc->cft[index];
					pcmb = &pcft->cmb;

					offset_x0 = pcft->offset_x; offset_y0 = pcft->offset_y;

					minx = 99999.0;
					minxi = -99999;
					for (i = 0; i < MAXCANDIDATE; i++) {
						clubmark_seg_t	*pclm;

						//--
						pclm = &pcmb->clm[i];
						if (pclm->state == CLUBMARK_BAR) {
							rectObj_t 		*pro;
							double x0, y0;

							//--
							pro = &pclm->rectObj;

							x0 = pro->p[0].x + pro->p[1].x + pro->p[2].x + pro->p[3].x;
							y0 = pro->p[0].y + pro->p[1].y + pro->p[2].y + pro->p[3].y;
							p0.x = x0/(4.0 * xfact) + offset_x0; 
							p0.y = y0/(4.0 * yfact) + offset_y0; p0.z = 0;

							if (minx > p0.x) {
								minx = p0.x;
								minxi = i;
							}
						}
					}

					if (minxi >= 0) {
						for (i = 0; i < MAXCANDIDATE; i++) {
							clubmark_seg_t	*pclm;

							//--
							pclm = &pcmb->clm[i];
							if (i == minxi) {
								continue;
							}
							if (pclm->state == CLUBMARK_BAR) {
								pclm->state = CLUBMARK_EXILE;
								pclm->exilewhy = CLUBMARK_EXILEWHY_BAR_YOUARERIGHT;
							}
						}
					}
				}
			} 	 // select the ONE!!!
		} // if (allowbar != 0) 
	} 	// Check BAR and exile..

	res = 1;

#ifdef DEBUG_CLUB_MARK
	{
		clubmark_dot_t *pcmd;
		clubmark_bar_t *pcmb;
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " %d SHOTINDEX: %d\n", camid, indexshot);
		for (index = index0; index <= index1; index++) {
			pcmd = &pcmc->cmd[index];
			if (pcmd->count) {
				for (i = 0; i < (I32)pcmd->count; i++) {
					if (pcmd->clm[i].state == CLUBMARK_DOT) {
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "DOT [%d: %d, %d]\n", camid, index, i);
					}
				}
			}

			pcmb = &pcmc->cmb[index];
			if (pcmb->count) {
				for (i = 0; i < (I32)pcmb->count; i++) {
					if (pcmb->clm[i].state == CLUBMARK_BAR) {
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BAR [%d: %d, %d]\n", camid, index, i);
					}
				}
			}
		}
	}
#endif
	UNUSED(height);
	UNUSED(piana);
	return res;
}




static I32 GetClubmarkRegion(iana_t *piana, U32 camid, clubmark_t *pcm)
{
	I32 res;

	U32 index;
	U32 i0;

	clubmark_cam_t *pcmc;

	clubmark_dot_t *pcmd;
	clubmark_bar_t *pcmb;

	clubmark_feature_t	*pcft;
	
	U32 marksequencelen;
	
	clubmark_region_t *pcrd;
	clubmark_region_t *pcrb;
	clubmark_region_t *pcrall;

	double xfact;
	double yfact;
	
	//----------

	pcmc = &pcm->cmc[camid];

	xfact = pcmc->xfact;
	yfact = pcmc->yfact;

	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}

	for (index = 0; index < marksequencelen; index++) {
		U32 dotinited, barinited;
		point_t *pvertex;
		imagesegment_t 	*pis;
		U32 offset_x, offset_y;


		//--
		pcft = &pcmc->cft[index];
		offset_x = pcft->offset_x;
		offset_y = pcft->offset_y;

		pcmd = &pcft->cmd;
		pcrd = &pcft->crd;
		dotinited = 0;
		pvertex = &pcrd->vertex[0];
		for (i0 = 0; i0 <pcmd->count; i0++) {
			pis = &pcmd->clm[i0].imgseg;
			if (pcmd->clm[i0].state == CLUBMARK_DOT) {
				if (dotinited == 0) {

					pvertex[0].x = pis->ixl;  pvertex[0].y = pis->iyu;  
					pvertex[1].x = pis->ixr;  pvertex[1].y = pis->iyu;  
					pvertex[2].x = pis->ixr;  pvertex[2].y = pis->iyb;  
					pvertex[3].x = pis->ixl;  pvertex[3].y = pis->iyb;  
					dotinited = 1;
				} else {
					if (pvertex[0].x > pis->ixl) { pvertex[0].x = pis->ixl; }
					if (pvertex[0].y > pis->iyu) { pvertex[0].y = pis->iyu; }

					if (pvertex[1].x < pis->ixr) { pvertex[1].x = pis->ixr; }
					if (pvertex[1].y > pis->iyu) { pvertex[1].y = pis->iyu; }

					if (pvertex[2].x < pis->ixr) { pvertex[2].x = pis->ixr; }
					if (pvertex[2].y < pis->iyb) { pvertex[2].y = pis->iyb; }

					if (pvertex[3].x > pis->ixl) { pvertex[3].x = pis->ixl; }
					if (pvertex[3].y < pis->iyb) { pvertex[3].y = pis->iyb; }
				}
			}
		}
		if (dotinited) {
			//point_t *pvertex;
			point_t Pp;
			point_t Lp;
			U32 ii;

			//--

#define XMARGIN	5
#define YMARGIN	5
			pvertex[0].x = pvertex[0].x - XMARGIN;
			pvertex[1].x = pvertex[1].x + XMARGIN;
			pvertex[2].x = pvertex[2].x + XMARGIN;
			pvertex[3].x = pvertex[3].x - XMARGIN;
			if (pvertex[0].x < 0) { pvertex[0].x = 0; }
			if (pvertex[3].x < 0) { pvertex[3].x = 0; }


			pvertex[0].y = pvertex[0].y - XMARGIN;
			pvertex[1].y = pvertex[1].y - XMARGIN;
			pvertex[2].y = pvertex[2].y + XMARGIN;
			pvertex[3].y = pvertex[3].y + XMARGIN;
			if (pvertex[0].y < 0) { pvertex[0].y = 0; }
			if (pvertex[1].y < 0) { pvertex[1].y = 0; }

			
			for (ii = 0; ii < 2; ii++) {
				pvertex = &pcrd->vertex[ii];		// For Upperband
				Pp.x = pvertex->x / xfact + offset_x;
				Pp.y = pvertex->y / yfact + offset_y;
				iana_P2L(piana, camid, &Pp, &Lp, 0);
				Lp.x = Lp.x - s_dotregionupper;	
				iana_L2P(piana, camid, &Lp, &Pp, 0);
				pcrd->upperband[ii].x = (Pp.x - offset_x) * xfact;
				pcrd->upperband[ii].y = (Pp.y - offset_y) * yfact;
			}

			for (ii = 0; ii < 2; ii++) {
				pvertex = &pcrd->vertex[ii+2];		// For Lowerband
				Pp.x = pvertex->x / xfact + offset_x;
				Pp.y = pvertex->y / yfact + offset_y;
				iana_P2L(piana, camid, &Pp, &Lp, 0);
				Lp.x = Lp.x + s_dotregionlower;
				//iana_L2P(piana, camid, &Lp, &pcrd->lowerband[ii], 0); 
				iana_L2P(piana, camid, &Lp, &Pp, 0);
				pcrd->lowerband[ii].x = (Pp.x - offset_x) * xfact;
				pcrd->lowerband[ii].y = (Pp.y - offset_y) * yfact;
			}

			pcrd->valid = 1;
		} else {
			pcrd->valid = 0;
		}

		pcmb = &pcft->cmb;
		pcrb = &pcft->crb;
		barinited = 0;
		pvertex = &pcrb->vertex[0];
		for (i0 = 0; i0 <pcmb->count; i0++) {
			pis = &pcmb->clm[i0].imgseg;
			if (pcmb->clm[i0].state == CLUBMARK_BAR) {
				if (barinited == 0) {

					pvertex[0].x = pis->ixl;  pvertex[0].y = pis->iyu;  
					pvertex[1].x = pis->ixr;  pvertex[1].y = pis->iyu;  
					pvertex[2].x = pis->ixr;  pvertex[2].y = pis->iyb;  
					pvertex[3].x = pis->ixl;  pvertex[3].y = pis->iyb;  
					barinited = 1;
				} else {
					if (pvertex[0].x > pis->ixl) { pvertex[0].x = pis->ixl; }
					if (pvertex[0].y > pis->iyu) { pvertex[0].y = pis->iyu; }

					if (pvertex[1].x < pis->ixr) { pvertex[1].x = pis->ixr; }
					if (pvertex[1].y > pis->iyu) { pvertex[1].y = pis->iyu; }

					if (pvertex[2].x < pis->ixr) { pvertex[2].x = pis->ixr; }
					if (pvertex[2].y < pis->iyb) { pvertex[2].y = pis->iyb; }

					if (pvertex[3].x > pis->ixl) { pvertex[3].x = pis->ixl; }
					if (pvertex[3].y < pis->iyb) { pvertex[3].y = pis->iyb; }
				}
			}
		}
		if (barinited) {
			//point_t *pvertex;
			point_t Pp;
			point_t Lp;
			U32 ii;

			//--

			pvertex[0].x = pvertex[0].x - XMARGIN;
			pvertex[1].x = pvertex[1].x + XMARGIN;
			pvertex[2].x = pvertex[2].x + XMARGIN;
			pvertex[3].x = pvertex[3].x - XMARGIN;
			if (pvertex[0].x < 0) { pvertex[0].x = 0; }
			if (pvertex[3].x < 0) { pvertex[3].x = 0; }


			pvertex[0].y = pvertex[0].y - XMARGIN;
			pvertex[1].y = pvertex[1].y - XMARGIN;
			pvertex[2].y = pvertex[2].y + XMARGIN;
			pvertex[3].y = pvertex[3].y + XMARGIN;
			if (pvertex[0].y < 0) { pvertex[0].y = 0; }
			if (pvertex[1].y < 0) { pvertex[1].y = 0; }


			for (ii = 0; ii < 2; ii++) {
				pvertex = &pcrb->vertex[ii];		// For Upperband
				Pp.x = pvertex->x / xfact + offset_x;
				Pp.y = pvertex->y / yfact + offset_y;
				iana_P2L(piana, camid, &Pp, &Lp, 0);
				Lp.x = Lp.x - s_barregionupper;
				iana_L2P(piana, camid, &Lp, &Pp, 0);
				pcrb->upperband[ii].x = (Pp.x - offset_x) * xfact;
				pcrb->upperband[ii].y = (Pp.y - offset_y) * yfact;
			}

			for (ii = 0; ii < 2; ii++) {
				pvertex = &pcrb->vertex[ii + 2];		// For Lowerband
				Pp.x = pvertex->x / xfact + offset_x;
				Pp.y = pvertex->y / yfact + offset_y;
				iana_P2L(piana, camid, &Pp, &Lp, 0);
				Lp.x = Lp.x + s_barregionlower;
				//iana_L2P(piana, camid, &Lp, &pcrb->lowerband[ii], 0); 
				iana_L2P(piana, camid, &Lp, &Pp, 0);
				pcrb->lowerband[ii].x = (Pp.x - offset_x) * xfact;
				pcrb->lowerband[ii].y = (Pp.y - offset_y) * yfact;
			}

			pcrb->valid = 1;
		} else {
			pcrb->valid = 0;
		}

		pcrall = &pcft->crall;
		if (dotinited && barinited) {
			point_t *pvertexd;

			//--
			pvertex = &pcrall->vertex[0];
			pvertexd = &pcrd->vertex[0];
			memcpy(pvertex, &pcrb->vertex[0], sizeof(point_t) * 4);

			if (pvertex[0].x > pvertexd[0].x) { pvertex[0].x = pvertexd[0].x; }
			if (pvertex[0].y > pvertexd[0].y) { pvertex[0].y = pvertexd[0].y; }

			if (pvertex[1].x < pvertexd[1].x) { pvertex[1].x = pvertexd[1].x; }
			if (pvertex[1].y > pvertexd[1].y) { pvertex[1].y = pvertexd[1].y; }

			if (pvertex[2].x < pvertexd[2].x) { pvertex[2].x = pvertexd[2].x; }
			if (pvertex[2].y < pvertexd[2].y) { pvertex[2].y = pvertexd[2].y; }

			if (pvertex[3].x > pvertexd[3].x) { pvertex[3].x = pvertexd[3].x; }
			if (pvertex[3].y < pvertexd[3].y) { pvertex[3].y = pvertexd[3].y; }

			pcrall->valid = 1;
		} else {
			pcrall->valid = 0;
		}
	} 		// for (index = 0; index < marksequencelen; index++) 

	res = 1;
	return res;
}


static I32 RegressionClubmark(iana_t *piana, U32 camid, clubmark_t *pcm)
{
	I32 res;
	if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_BARDOT) {
		res = RegressionDot(piana, camid, pcm, CR_TRUE);
	} else {
		res = RegressionDot(piana, camid, pcm, CR_FALSE);
	}

	if (pcm->clubcalc_method == CLUBCALC_CLUB_MARK_BARDOT 
			|| pcm->clubcalc_method == CLUBCALC_CLUB_MARK_TOPBAR
		) {
		res = RegressionUpperbar(piana, camid, pcm);
		//res = RegressionBar(piana, camid, pcm);
	} else {
		res = RegressionMbar(piana, camid, pcm);
	}

	return res;
}
 
static I32 RegressionLowerDot_3D(iana_t *piana, clubmark_t *pcm)
{
	I32 res;
	cr_point_t point2d[2];
	cr_point_t p3d[MARKSEQUENCELEN];
	U32 marksequencelen;
	clubmark_cam_t *pcmc0, *pcmc1;
	clubmark_Lp_t *pdotLp;
	clubmark_p3d_t *pdotp3d;
	double xma[3], yma[3], zma[3];

	double xx0[MARKSEQUENCELEN], yy0[MARKSEQUENCELEN], zz0[MARKSEQUENCELEN], tt0[MARKSEQUENCELEN];
	I32 index;
	I32 indexshot, index0;
	I32 count;
	double td;
	U32 reg_method;
	//---------------------------
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}
	
	pcmc0 = &pcm->cmc[0];				// pcmc of center cam
	pdotLp = &pcmc0->dotLp;
	pdotp3d = &pcmc0->dotp3d;
	memcpy(&xma[0], &pdotLp->xma[0], sizeof(double) * 3);
	memcpy(&yma[0], &pdotLp->yma[0], sizeof(double) * 3);

	memset(pdotp3d, 0, sizeof(clubmark_p3d_t));
	pdotp3d->mavalid = 0;

	if (pcmc0->dotLp.mavalid) {
		clubmark_feature_t	*pcft;
		clubmark_face_t *pcfc;
		double xr2, yr2, zr2;

		//--
		pcmc1 = &pcm->cmc[1];				// pcmc of side cam
		index0 = pcmc1->index0;

		indexshot = pcmc1->indexshot;

		count = 0;
		for (index = index0; index <= indexshot; index++) {

			//--
			pcft = &pcmc1->cft[index];
			pcfc = &pcmc1->face[index];
			td = pcft->tsdiffd;

			if (pcfc->lowerdot.valid) {
				point2d[0].x = xma[2] * td*td + xma[1] * td + xma[0];
				point2d[0].y = yma[2] * td*td + yma[1] * td + yma[0];
				point2d[0].z = 0;

				point2d[1].x = pcfc->lowerdot.cdotL.x;
				point2d[1].y = pcfc->lowerdot.cdotL.y;
				point2d[1].z = 0;

				getp3d(piana, &point2d[0], &p3d[count]);
				xx0[count] = p3d[count].x;
				yy0[count] = p3d[count].y;
				zz0[count] = p3d[count].z;
				tt0[count] = td;

				pdotp3d->valid[index] = 1;
				memcpy(&pdotp3d->p3d[index], &p3d[count], sizeof(cr_point_t));

				count++;
			}
		}
		count--;

		td = tt0[count-1];

		if (td < REG_TIME) {
			if (count > 2) {
				reg_method = 2;
			} else {
				reg_method = 0;
			}
		} else {
			if (count > 2) {
				reg_method = 3;
			} else if (count == 2) {
				reg_method = 2;
			} else {
				reg_method = 0;
			}
		}
		if (reg_method == 3) {
			caw_quadraticregression(tt0, xx0, count, &xma[2], &xma[1], &xma[0], &xr2);
			caw_quadraticregression(tt0, yy0, count, &yma[2], &yma[1], &yma[0], &yr2);
			caw_quadraticregression(tt0, zz0, count, &zma[2], &zma[1], &zma[0], &zr2);
		
            memcpy(&pdotp3d->xma[0], &xma[0], sizeof(double)*3);
			memcpy(&pdotp3d->yma[0], &yma[0], sizeof(double)*3);
			memcpy(&pdotp3d->zma[0], &zma[0], sizeof(double)*3);
			
            pdotp3d->mavalid = 1;
		} else if (reg_method == 2) {
			xma[2] = yma[2] = zma[2] = 0;
			
            cr_regression2(tt0, xx0, count, &xma[1], &xma[0], &xr2);
			cr_regression2(tt0, yy0, count, &yma[1], &yma[0], &yr2);
			cr_regression2(tt0, zz0, count, &zma[1], &zma[0], &zr2);
			
            memcpy(&pdotp3d->xma[0], &xma[0], sizeof(double)*3);
			memcpy(&pdotp3d->yma[0], &yma[0], sizeof(double)*3);
			memcpy(&pdotp3d->zma[0], &zma[0], sizeof(double)*3);
			
            pdotp3d->mavalid = 1;
		} else {
			pdotp3d->mavalid = 0;
		}

		memcpy(&pcmc1->dotp3d, pdotp3d, sizeof(clubmark_p3d_t));

		for (index = 0; index < (I32)count; index++) {
			double dx, dy, dz, dxy;
			double angle;
			if (index > 0) {
				dx = p3d[index].x - p3d[index-1].x;
				dy = p3d[index].y - p3d[index-1].y;
				dz = p3d[index].z - p3d[index-1].z;
				dxy = sqrt(dx*dx + dy*dy);
				angle = atan2(dz, dxy);

				angle = RADIAN2DEGREE(angle);
			} else {
				angle = 0;
			}
#ifdef DEBUG_CLUB_MARK
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                "DOT, [%2d] %10lf   %10lf %10lf %10lf, attack  %10lf\n", 
                index, tt0[index],
                p3d[index].x, p3d[index].y, p3d[index].z, 
                angle);
#endif
		}
		res = 1;
	} else {
		res = 0;
	}

	return res;
}



static I32 RegressionUpperbar(iana_t *piana, U32 camid, clubmark_t *pcm)
{
	I32 res;
	I32 index;
	I32 indexshot, index0, index1;
	U32 count;

	double xfact, yfact;
	U32 offset_x, offset_y;
	clubmark_cam_t *pcmc;
	clubmark_feature_t	*pcft;
	
	I32 marksequencelen;

	//-----------------------------------
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}
	
	
	pcmc = &pcm->cmc[camid];
	
	indexshot = pcmc->indexshot;
	index0 = pcmc->index0;
	index1 = pcmc->index1;
	xfact = pcmc->xfact;
	yfact = pcmc->yfact;

	//------------------------------
	{ 	// Regression BAR
		double tt[MARKSEQUENCELEN], xx[MARKSEQUENCELEN], yy[MARKSEQUENCELEN], theta_[MARKSEQUENCELEN];

		// Check Bar position
		pcmc->barLp[0].mavalid = 0;
		for (index = 0; index < marksequencelen; index++) {
			pcmc->barLp[0].valid[index] = 0;
		}
		count = 0;
		for (index = index0; index <= indexshot+1; index++) 
		{
			clubmark_face_t *pcfc;

			//--
			pcft = &pcmc->cft[index];
			pcfc = &pcmc->face[index];
			
			offset_x = pcft->offset_x; offset_y = pcft->offset_y;
			if (pcfc->upperline.valid)
			{
				cr_vector_t *pm_;
				cr_point_t *pp_;

				//--
				pp_ = &pcfc->upperline.clineL.p0;
				pm_ = &pcfc->upperline.clineL.m;
				tt[count] = pcft->tsdiffd;
				xx[count] = pp_->x;
				yy[count] = pp_->y;
				theta_[count] = atan2(pm_->x, pm_->y);

#define 	TT_MIN	(-0.0001)  // (-0.001), (-0.0005)
				if (tt[count] < TT_MIN) {
#ifdef DEBUG_CLUB_MARK
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                        "REGRESSION <%d> %lf %lf %lf %lf\n", 
                        count, 
                        tt[count], xx[count], yy[count],
                        RADIAN2DEGREE(theta_[count]));
#endif
					pcmc->barLp[0].Lp[index].x = pp_->x;
					pcmc->barLp[0].Lp[index].y = pp_->y;
					pcmc->barLp[0].valid[index] = 1;
					count++;
				}
			}
		}
#ifdef DEBUG_CLUB_MARK
		{
			U32 i;

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                "\n\n------------------------\n");
		
        	for (i = 0; i < count; i++) {

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                    "[%4d] %10.6f %10.6f %10.6f %10.6f \n", 
                    i, tt[i], xx[i], yy[i], theta_[i]);
			}

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                "\n\n------------------------\n");
		}
#endif
		{
			U32 mavalid;
			double xma[3], yma[3], thma[3];
			double xr2, yr2, thr2;

			double xx0[MARKSEQUENCELEN], yy0[MARKSEQUENCELEN];
			U32 newcount;
			double maxdistance;

			//--
			xma[2] = xma[1] = xma[0] = 0;
			yma[2] = yma[1] = yma[0] = 0;
			thma[2] = thma[1] = thma[0] = 0;

			xr2 = 0; yr2 = 0;
			thr2 = 0;
			mavalid = 0;
			if (count >= XY_QUADREG_COUNT) {

#define MAXDISTANCE_UPPERBAR	(0.05) // (0.1)
				maxdistance = MAXDISTANCE_UPPERBAR;
				
                removeoutlier(tt, xx, count, xx0, yy0, &newcount, maxdistance);
				caw_quadraticregression(xx0, yy0, newcount, &xma[2], &xma[1], &xma[0], &xr2);
#ifdef DEBUG_CLUB_MARK
				{
					U32 i;
					for (i = 0; i < newcount; i++) {
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                            "XX [%4d] %10.6f %10.6f (%d -> %d)\n", 
                            i, xx0[i], yy0[i], count, newcount);
					}
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                        "---------\n");
				}
#endif

				maxdistance = MAXDISTANCE_UPPERBAR;
				removeoutlier(tt, yy, count, xx0, yy0, &newcount, maxdistance);
				caw_quadraticregression(xx0, yy0, newcount, &yma[2], &yma[1], &yma[0], &yr2);
#ifdef DEBUG_CLUB_MARK
				{
					U32 i;
					for (i = 0; i < newcount; i++) {
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                            "YY [%4d] %10.6f %10.6f (%d -> %d)\n", 
                            i, xx0[i], yy0[i], count, newcount);
					}
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                        "---------\n");
				}
#endif

#define MAXDISTANCE_UPPERBAR_TH	(0.05)
				maxdistance = MAXDISTANCE_UPPERBAR_TH;
				removeoutlier(tt, theta_, count, xx0, yy0, &newcount, maxdistance);
				caw_quadraticregression(xx0, yy0, newcount, &thma[2], &thma[1], &thma[0], &thr2);

#ifdef DEBUG_CLUB_MARK
				{
					U32 i;
					for (i = 0; i < newcount; i++) {
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                            "TH [%4d] %10.6f %10.6f (%d -> %d)\n", 
                            i, xx0[i], yy0[i], count, newcount);
					}
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---------\n");
				}
#endif

                xr2 = quadregression_errcalc(tt, xx, count, &xma[0]);
				yr2 = quadregression_errcalc(tt, yy, count, &yma[0]);
				
                //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #1 mean square Err: %lf %lf \n", xr2, yr2);
				
                if (xr2 > XY_QUADREG_ERR || yr2 > XY_QUADREG_ERR) {
					mavalid = 0;
				} else {
					mavalid = 1;
				}

#define MAXMA2_UPPERBAR	10000
				if (
						(xma[2] < -MAXMA2_UPPERBAR || xma[2] > MAXMA2_UPPERBAR)
						|| (yma[2] < -MAXMA2_UPPERBAR || yma[2] > MAXMA2_UPPERBAR)
				   ) {
					mavalid = 0;
				}
			} 

            if (mavalid == 0) {
				if (count >= XY_REG_COUNT) {
					xma[2] = 0;
					cr_regression2(tt, xx, count, &xma[1], &xma[0], &xr2);
					yma[2] = 0;
					cr_regression2(tt, yy, count, &yma[1], &yma[0], &yr2);

					thma[2] = 0;
					cr_regression2(tt, theta_, count, &thma[1], &thma[0], &thr2);

					xr2 = quadregression_errcalc(tt, xx, count, &xma[0]);
					yr2 = quadregression_errcalc(tt, yy, count, &yma[0]);
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #2 mean square Err: %lf %lf \n", xr2, yr2);
					if (xr2 > XY_QUADREG_ERR || yr2 > XY_QUADREG_ERR) {
						mavalid = 0;
					} else {
						mavalid = 1;
					}

				} else {
					mavalid = 0;
				}
			} 

			pcmc->barLp[0].mavalid = mavalid;
			memcpy(&pcmc->barLp[0].xma[0], &xma[0], sizeof(double)*3);
			memcpy(&pcmc->barLp[0].yma[0], &yma[0], sizeof(double)*3);
			memcpy(&pcmc->barLp[0].thma[0], &thma[0], sizeof(double)*3);

#ifdef DEBUG_CLUB_MARK
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                "[%d] regression valid: %d   xma: %lf %lf %lf, %lf   yma: %lf %lf %lf, %lf   thma: %lf %lf %lf, %lf\n",
                camid, mavalid,
                xma[2], xma[1], xma[0], xr2,
                yma[2], yma[1], yma[0], yr2,
                thma[2], thma[1], thma[0], thr2);
#endif

			for (index = 0; index < marksequencelen; index++) {
				double td;
				U32 valid;
				double x0, y0;
				double x0_, y0_;
				double ex_, ey_;
				double xma[3], yma[3];

				//--
				pcft = &pcmc->cft[index];
				td = pcft->tsdiffd;

				valid = pcmc->barLp[0].valid[index];
				if (valid) {
					x0 = pcmc->barLp[0].Lp[index].x;
					y0 = pcmc->barLp[0].Lp[index].y;
				} else {
					x0 = -1;
					y0 = -1;
				}

				memcpy(&xma[0], &pcmc->barLp[0].xma[0], sizeof(double)*3);
				x0_ = xma[2]*td*td + xma[1]*td + xma[0];
				memcpy(&yma[0], &pcmc->barLp[0].yma[0], sizeof(double)*3);
				y0_ = yma[2]*td*td + yma[1]*td + yma[0];
				if (valid) {
					ex_ = x0_ - x0;
					ey_ = y0_ - y0;
				} else {
					ex_ = 0;
					ey_ = 0;
				}

#ifdef DEBUG_CLUB_MARK
                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                    "[%d] regression result: %2d %10lf  x_, y_: %10lf  %10lf   (%d:  x, y: %10lf %10lf,  %10lf %10lf)\n",  
                    camid, index, td,
                    x0_, y0_,
                    valid,
                    x0, y0, ex_, ey_);
#endif


			}
		}
	}
	UNUSED(piana);
	UNUSED(camid);

	res = 1;
	return res;
}

static I32 RegressionUpperbar_3D(iana_t *piana, clubmark_t *pcm)
{
	I32 res;
	cr_point_t point2d[2];
	cr_point_t p3d[MARKSEQUENCELEN];
	U32 marksequencelen;
	clubmark_cam_t *pcmc0, *pcmc1;
	clubmark_Lp_t *pbarLp;
	clubmark_p3d_t *pbarp3d;
	double xma[3], yma[3], zma[3];

	double xx0[MARKSEQUENCELEN], yy0[MARKSEQUENCELEN], zz0[MARKSEQUENCELEN], tt0[MARKSEQUENCELEN];
	I32 index;
	I32 indexshot, index0;
	U32 count;
	double td;
	U32 reg_method;
	//---------------------------
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}
	
	pcmc0 = &pcm->cmc[0];				// pcmc of center cam
	pbarLp = &pcmc0->barLp[0];
	pbarp3d = &pcmc0->barp3d;
	memcpy(&xma[0], &pbarLp->xma[0], sizeof(double) * 3);
	memcpy(&yma[0], &pbarLp->yma[0], sizeof(double) * 3);

	memset(pbarp3d, 0, sizeof(clubmark_p3d_t));
	pbarp3d->mavalid = 0;

	if (pbarLp->mavalid) {
		clubmark_feature_t	*pcft;
		clubmark_face_t *pcfc;
		double xr2, yr2, zr2;

		//--
		pcmc1 = &pcm->cmc[1];				// pcmc of side cam
		index0 = pcmc1->index0;

		indexshot = pcmc1->indexshot;

		count = 0;
		for (index = index0; index <= indexshot; index++) {

			//--
			pcft = &pcmc1->cft[index];
			pcfc = &pcmc1->face[index];
			td = pcft->tsdiffd;

			if (pcfc->upperline.valid) {
				point2d[0].x = xma[2] * td*td + xma[1] * td + xma[0];
				point2d[0].y = yma[2] * td*td + yma[1] * td + yma[0];
				point2d[0].z = 0;

				point2d[1].x = pcfc->upperline.clineL.p0.x;
				point2d[1].y = pcfc->upperline.clineL.p0.y;
				point2d[1].z = 0;

				getp3d(piana, &point2d[0], &p3d[count]);
				xx0[count] = p3d[count].x;
				yy0[count] = p3d[count].y;
				zz0[count] = p3d[count].z;
				tt0[count] = td;

				pbarp3d->valid[index] = 1;
				memcpy(&pbarp3d->p3d[index], &p3d[count], sizeof(cr_point_t));

				count++;

				if (td > REG_TIME) {
#define GOOD_TD_COUNT	5
					if (count > GOOD_TD_COUNT) {
						break;
					}
				}
			}
		}

		td = tt0[count-1];
		if (td < REG_TIME) {
			if (count > 2) {
				reg_method = 2;
			} else {
				reg_method = 0;
			}
		} else {
			//if (count > 2) {
			//	//reg_method = 3;
			//	reg_method = 2;
			//} 

			if (count >= GOOD_TD_COUNT) {
				reg_method = 3;
				count = GOOD_TD_COUNT;
				//reg_method = 2;
			} 
			else if (count == 2) {
				reg_method = 2;
			} else {
				reg_method = 0;
			}
		}
		if (reg_method == 3) {
			caw_quadraticregression(tt0, xx0, count, &xma[2], &xma[1], &xma[0], &xr2);
			caw_quadraticregression(tt0, yy0, count, &yma[2], &yma[1], &yma[0], &yr2);
			caw_quadraticregression(tt0, zz0, count, &zma[2], &zma[1], &zma[0], &zr2);
			memcpy(&pbarp3d->xma[0], &xma[0], sizeof(double)*3);
			memcpy(&pbarp3d->yma[0], &yma[0], sizeof(double)*3);
			memcpy(&pbarp3d->zma[0], &zma[0], sizeof(double)*3);
			pbarp3d->mavalid = 1;
		} else if (reg_method == 2) {
			xma[2] = yma[2] = zma[2] = 0;
			cr_regression2(tt0, xx0, count, &xma[1], &xma[0], &xr2);
			cr_regression2(tt0, yy0, count, &yma[1], &yma[0], &yr2);
			cr_regression2(tt0, zz0, count, &zma[1], &zma[0], &zr2);
			memcpy(&pbarp3d->xma[0], &xma[0], sizeof(double)*3);
			memcpy(&pbarp3d->yma[0], &yma[0], sizeof(double)*3);
			memcpy(&pbarp3d->zma[0], &zma[0], sizeof(double)*3);
			pbarp3d->mavalid = 1;
		} else {
			pbarp3d->mavalid = 0;
		}

		memcpy(&pcmc1->barp3d, pbarp3d, sizeof(clubmark_p3d_t));

		for (index = 0; index < (I32)count; index++) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BAR, [%2d] %10lf %10lf %10lf\n", 
					index, p3d[index].x, p3d[index].y, p3d[index].z);
		}
		res = 1;
	} else {
		res = 0;
	}

	return res;
}


#if 0 // Depricated
static I32 RegressionBar(iana_t *piana, U32 camid, clubmark_t *pcm)
{
	I32 res;
	U32 i;
	I32 index;
	I32 indexshot, index0, index1;
	rectObj_t 		*pro;
	U32 count;

	double xfact, yfact;
	U32 offset_x, offset_y;
	clubmark_cam_t *pcmc;
	clubmark_feature_t	*pcft;
	
	I32 marksequencelen;

	//-----------------------------------

	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}
	
	
	pcmc = &pcm->cmc[camid];
	
	indexshot = pcmc->indexshot;
	index0 = pcmc->index0;
	index1 = pcmc->index1;
	xfact = pcmc->xfact;
	yfact = pcmc->yfact;

	//------------------------------
	{ 	// Regression BAR
		clubmark_bar_t *pcmb;
		double tt[MARKSEQUENCELEN], xx[MARKSEQUENCELEN], yy[MARKSEQUENCELEN];

		// Check Bar position
		pcmc->barLp[0].mavalid = 0;
		for (index = 0; index < marksequencelen; index++) {
			pcmc->barLp[0].valid[index] = 0;
		}
		count = 0;
		//for (index = index0; index <= index1; index++) 
		for (index = index0; index <= indexshot+1; index++) 
		{
			point_t Pb;
			double Pr;
			double x0, y0;

			//--
			//pcmb = &pcmc->cmb[index];

			pcft = &pcmc->cft[index];
			pcmb = &pcft->cmb;
			
			//offset_x = pcmb->offset_x; offset_y = pcmb->offset_y;
			offset_x = pcft->offset_x; offset_y = pcft->offset_y;
			if (pcmb->count) {
				for (i = 0; i < (I32)pcmb->count; i++) {
					point_t Pp, *pLp;
					if (pcmb->clm[i].state == CLUBMARK_BAR) {
						//memcpy(&Pb, &pcmc->Pp[index], sizeof(point_t));
						//Pr = pcmc->Pr[index];
						memcpy(&Pb, &pcft->Pp, sizeof(point_t));
						Pr = pcft->Pr;

						pro = &pcmb->clm[i].rectObj;

						//x0 = pro->p[0].x + pro->p[1].x + pro->p[2].x + pro->p[3].x;
						//y0 = pro->p[0].y + pro->p[1].y + pro->p[2].y + pro->p[3].y;
						//Pp.x = x0/(4.0 * xfact) + offset_x; Pp.y = y0/(4.0 * yfact) + offset_y;

						x0 = pro->p[1].x + pro->p[2].x;
						y0 = pro->p[1].y + pro->p[2].y;
						Pp.x = x0/(2.0 * xfact) + offset_x; Pp.y = y0/(2.0 * yfact) + offset_y;

						pcmc->barLp[0].valid[index] = 1;
						pLp = &pcmc->barLp[0].Lp[index];
						iana_P2L(piana, camid, &Pp, pLp, 0);

						//tt[count] = pcmc->tsdiffd[index];
						tt[count] = pcft->tsdiffd;
						xx[count] = pLp->x;
						yy[count] = pLp->y;

						count++;
						break;
					}
				}
			}
		}

		{
			U32 mavalid;
			double xma[3], yma[3];
			double xr2, yr2;

			//--
			xma[2] = xma[1] = xma[0] = 0;
			yma[2] = yma[1] = yma[0] = 0;

			xr2 = 0; yr2 = 0;
			mavalid = 0;
			if (count >= XY_QUADREG_COUNT) {
				caw_quadraticregression(tt, xx, count, &xma[2], &xma[1], &xma[0], &xr2);
				caw_quadraticregression(tt, yy, count, &yma[2], &yma[1], &yma[0], &yr2);
				/*
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] QUAD regression xma: %lf %lf %lf, %lf   yma: %lf %lf %lf, %lf\n",
					camid,
					xma[2], xma[1], xma[0], xr2,
					yma[2], yma[1], yma[0], yr2);
				*/
				xr2 = quadregression_errcalc(tt, xx, count, &xma[0]);
				yr2 = quadregression_errcalc(tt, yy, count, &yma[0]);
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" #1 mean square Err: %lf %lf \n", xr2, yr2);

				if (xr2 > XY_QUADREG_ERR || yr2 > XY_QUADREG_ERR) {
					mavalid = 0;
				} else {
					mavalid = 1;
				}
				if (
						(xma[2] < -MAXMA2 || xma[2] > MAXMA2)
						|| (yma[2] < -MAXMA2 || yma[2] > MAXMA2)
				   ) {
					mavalid = 0;
				}
			} 
//			if (xr2 < XY_QUADREG_R2 || yr2 < XY_QUADREG_R2) 
			//if (xr2 > XY_QUADREG_ERR || yr2 > XY_QUADREG_ERR) 
			if (mavalid == 0) 
			{
				if (count >= XY_REG_COUNT) {
					xma[2] = 0;
					cr_regression2(tt, xx, count, &xma[1], &xma[0], &xr2);
					yma[2] = 0;
					cr_regression2(tt, yy, count, &yma[1], &yma[0], &yr2);

					xr2 = quadregression_errcalc(tt, xx, count, &xma[0]);
					yr2 = quadregression_errcalc(tt, yy, count, &yma[0]);
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mean square Err: %lf %lf \n", xr2, yr2);
					if (xr2 > XY_QUADREG_ERR || yr2 > XY_QUADREG_ERR) {
						mavalid = 0;
					} else {
						mavalid = 1;
					}
/*
					if (xr2 < XY_QUADREG_R2 || yr2 < XY_QUADREG_R2) {
						mavalid = 0;
					} else {
						mavalid = 1;
					}
*/
				} else {
					mavalid = 0;
				}
			} 
			//else {
			//	mavalid = 1;
			//}
			pcmc->barLp[0].mavalid = mavalid;
			memcpy(&pcmc->barLp[0].xma[0], &xma[0], sizeof(double)*3);
			memcpy(&pcmc->barLp[0].yma[0], &yma[0], sizeof(double)*3);

			/*
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] regression valid: %d   xma: %lf %lf %lf, %lf   yma: %lf %lf %lf, %lf\n",
					camid, mavalid,
					xma[2], xma[1], xma[0], xr2,
					yma[2], yma[1], yma[0], yr2);
					*/

			for (index = 0; index < marksequencelen; index++) {
				double td;
				U32 valid;
				double x0, y0;
				double x0_, y0_;
				double ex_, ey_;
				double xma[3], yma[3];

				//--
				//td = pcmc->tsdiffd[index];
				pcft = &pcmc->cft[index];
				td = pcft->tsdiffd;

				valid = pcmc->barLp[0].valid[index];
				if (valid) {
					x0 = pcmc->barLp[0].Lp[index].x;
					y0 = pcmc->barLp[0].Lp[index].y;
				} else {
					x0 = -1;
					y0 = -1;
				}

				memcpy(&xma[0], &pcmc->barLp[0].xma[0], sizeof(double)*3);
				x0_ = xma[2]*td*td + xma[1]*td + xma[0];
				memcpy(&yma[0], &pcmc->barLp[0].yma[0], sizeof(double)*3);
				y0_ = yma[2]*td*td + yma[1]*td + yma[0];
				if (valid) {
					ex_ = x0_ - x0;
					ey_ = y0_ - y0;
				} else {
					ex_ = 0;
					ey_ = 0;
				}


				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] regression result: %2d %10lf  x_, y_: %10lf  %10lf   (%d:  x, y: %10lf %10lf,  %10lf %10lf)\n",  
				//		camid, index, 
				//		td,
				//		x0_, y0_,
				//		valid,
				//		x0, y0, ex_, ey_);

			}
		}
	}
	piana;
	camid;

	res = 1;
	return res;
}
#endif

static I32 RegressionMbar(iana_t *piana, U32 camid, clubmark_t *pcm)
{
	I32 res;
	U32 i;
	I32 index;
	I32 indexshot, index0, index1;
	U32 count;
	U32 netcount;

	double xfact, yfact;
	U32 offset_x, offset_y;
	clubmark_cam_t *pcmc;
	clubmark_feature_t	*pcft;

//	double xt, yt;
	double xt_1, yt_1;
	double xt_1_, yt_1_;
	double t_1;

	I32 marksequencelen;
	//-----------------------------------

	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}
	
	
	pcmc = &pcm->cmc[camid];
	
	indexshot = pcmc->indexshot;
	index0 = pcmc->index0;
	index1 = pcmc->index1;
	xfact = pcmc->xfact;
	yfact = pcmc->yfact;

	//------------------------------
	{ 	// Regression BAR
		clubmark_seg_t	*pclm;
		clubmark_bar_t *pcmb;
#define REGMULTIPLE		5
//#define REGMULTIPLE		1
		double tt[MARKSEQUENCELEN*REGMULTIPLE], xx[MARKSEQUENCELEN*REGMULTIPLE], yy[MARKSEQUENCELEN*REGMULTIPLE];

		// Check Bar position
		pcmc->barLp[0].mavalid = 0;
		for (index = 0; index < marksequencelen; index++) {
			pcmc->barLp[0].valid[index] = 0;
		}
		count = 0;
		netcount = 0;
		//for (index = index0; index <= index1; index++) 
		//for (index = index0; index <= indexshot+1; index++) 
		for (index = index0; index <= indexshot; index++) 
		//for (index = index0; index < indexshot-1; index++) 
		{
			//--
			//pcmb = &pcmc->cmb[index];
			pcft = &pcmc->cft[index];
			pcmb = &pcft->cmb;
			//offset_x = pcmb->offset_x; offset_y = pcmb->offset_y;
			offset_x = pcft->offset_x; offset_y = pcft->offset_y;
			if (pcmb->count) {
				for (i = 0; i < pcmb->count; i++) {
					pclm = &pcmb->clm[i];
					if (pclm->state == CLUBMARK_BAR) {
						point_t *pLp;
						pLp = &pcmc->barLp[0].Lp[index];
						//tt[count] = pcmc->tsdiffd[index];
						tt[count] = pcft->tsdiffd;

#if 1
						{
							double x0, xM, y0, yM;
							x0 = pclm->ccL[0].x;
							y0 = pclm->ccL[0].y;

							xM = pclm->ccL[CLUBMARK_BAR_CLASSCOUNT-1].x;
							yM = pclm->ccL[CLUBMARK_BAR_CLASSCOUNT-1].y;

//#define BAR_LEN_ADD			(0.05)
#define BAR_LEN_ADD			(0.02)
/////#define BAR_LEN_ADD			(0.04)
							{
								cr_vector_t p0, pM;
								cr_vector_t pF;

								x0 = pclm->ccL[1].x;
								y0 = pclm->ccL[1].y;

								p0.x = x0; p0.y = y0; p0.z = 0;
								pM.x = xM; pM.y = yM; pM.z = 0;

								cr_vector_sub(&p0, &pM, &pF);

								cr_vector_normalization(&pF, &pF);
								cr_vector_scalar(&pF, BAR_LEN_ADD, &pF);
								cr_vector_add(&pF, &pM, &pF);
								xx[count] = pLp->x = pF.x;
								yy[count] = pLp->y = pF.y;

								pcmc->barLp[0].valid[index] = 1;
							}
						}
#endif

						count++;
						netcount++;
#if defined(REGMULTIPLE)
						if (index != indexshot+1) {
							I32 j;
							for (j = 0; j < REGMULTIPLE-1; j++) {
								//tt[count] = pcmc->tsdiffd[index];
								tt[count] = pcft->tsdiffd;
								xx[count] = pLp->x;
								yy[count] = pLp->y;
								count++;
							}
						}
#endif
						break;
					}
				}
			}
		}

		{
			U32 mavalid;
			double xma[3], yma[3];
			double xma2[2], yma2[2];
			double xr2, yr2;
			double xer2, yer2;

			//--
			xma[2] = xma[1] = xma[0] = 0;
			yma[2] = yma[1] = yma[0] = 0;

			xma2[1] = xma2[0] = 0;
			yma2[1] = yma2[0] = 0;

			xr2 = 0; yr2 = 0;
			xer2 = yer2 = 0;
			mavalid = 0;

			if (netcount >= XY_QUADREG_COUNT) 
			{
				{
					caw_quadraticregression(tt, yy, count, &yma[2], &yma[1], &yma[0], &yr2);
					caw_quadraticregression(tt, xx, count, &xma[2], &xma[1], &xma[0], &xr2);
				}

				/*
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] QUAD regression xma: %lf %lf %lf, %lf   yma: %lf %lf %lf, %lf\n",
					camid,
					xma[2], xma[1], xma[0], xr2,
					yma[2], yma[1], yma[0], yr2);
				*/

				{
					yer2 = quadregression_errcalc(tt, yy, count, &yma[0]);
					xer2 = quadregression_errcalc(tt, xx, count, &xma[0]);
				}

				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mean square Err: %lf %lf \n", xer2, yer2);
				if (xer2 > XY_QUADREG_ERR || yer2 > XY_QUADREG_ERR) {
					mavalid = 0;
				} else {
					mavalid = 1;

					{
						cr_regression2(tt, xx, count, &xma2[1], &xma2[0], &xr2);
						cr_regression2(tt, yy, count, &yma2[1], &yma2[0], &yr2);
					}
				}

			} 

			if (mavalid == 0) 
			{
				if (count >= XY_REG_COUNT) {
					xma[2] = 0;
					yma[2] = 0;

					{
						cr_regression2(tt, xx, count, &xma[1], &xma[0], &xr2);
						cr_regression2(tt, yy, count, &yma[1], &yma[0], &yr2);
						xer2 = quadregression_errcalc(tt, xx, count, &xma[0]);
						yer2 = quadregression_errcalc(tt, yy, count, &yma[0]);
					}

					xma2[1] = xma[1];
					xma2[0] = xma[0];

					yma2[1] = yma[1];
					yma2[0] = yma[0];



					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mean square Err: %lf %lf \n", xer2, yer2);
					if (xer2 > XY_QUADREG_ERR || yer2 > XY_QUADREG_ERR) {
						mavalid = 0;
					} else {
						mavalid = 1;
					}

				} else {
					mavalid = 0;
				}
			} 

			pcmc->barLp[0].mavalid = mavalid;
			memcpy(&pcmc->barLp[0].xma[0], &xma[0], sizeof(double)*3);
			memcpy(&pcmc->barLp[0].yma[0], &yma[0], sizeof(double)*3);

			memcpy(&pcmc->barLp[0].xma2[0], &xma2[0], sizeof(double)*2);
			memcpy(&pcmc->barLp[0].yma2[0], &yma2[0], sizeof(double)*2);

			/*
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] regression valid: %d   xma: %lf %lf %lf, %lf   yma: %lf %lf %lf, %lf\n",
					camid, mavalid,
					xma[2], xma[1], xma[0], xr2,
					yma[2], yma[1], yma[0], yr2);
					*/

			xt_1=yt_1=xt_1_=yt_1_ = -999;
			t_1 = -999999999;
			for (index = 0; index < marksequencelen; index++) {
				double td;
				U32 valid;
				double x0, y0;
				double x0_, y0_;
				double ex_, ey_;
				double td_, etd_;
				double xma[3], yma[3];

				x0_ = y0_ = 0;
				ex_ = ey_ = 0;
				td_ = etd_ = 0;

				//--
				//td = pcmc->tsdiffd[index];
				pcft = &pcmc->cft[index];
				td = pcft->tsdiffd;

				memcpy(&xma[0], &pcmc->barLp[0].xma[0], sizeof(double)*3);
				memcpy(&yma[0], &pcmc->barLp[0].yma[0], sizeof(double)*3);
				valid = pcmc->barLp[0].valid[index];
				x0 = pcmc->barLp[0].Lp[index].x;
				y0 = pcmc->barLp[0].Lp[index].y;

				{
					x0_ = xma[2]*td*td + xma[1]*td + xma[0];
					y0_ = yma[2]*td*td + yma[1]*td + yma[0];
					td_ = td;

					if (valid) {
						ex_ = x0 - x0_;
						ey_ = y0 - y0_;
					} else {
						x0 = -1;
						y0 = -1;
						ex_ = 0;
						ey_ = 0;
					}
					etd_ = 0;
				}
				/*
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d %2d] %10lf %10lf %10lf vs %10lf %10lf %10lf (%d:  %10lf %10lf %10lf)\n",
						camid, index, 
						td, x0, y0, 
						td_, x0_, y0_,
						valid,
						etd_, ex_, ey_);
				*/
				if (valid) {
					if (t_1 > -99999) {
						double dx, dy, dxy;
						double v, v_;

						//--
						dx = x0 - xt_1;
						dy = y0 - yt_1;
						dxy = sqrt(dx*dx + dy*dy);
						v = dxy / (td - t_1);

						dx = x0_- xt_1;
						dy = y0_- yt_1;
						dxy = sqrt(dx*dx + dy*dy);
						v_= dxy / (td - t_1);

						/*
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "v: %10lf   v_ : %10lf\n",
								v, v_);
						*/
					}

					t_1 = td;
					xt_1 = x0;
					yt_1 = y0;
					xt_1_ = x0_;
					yt_1_ = y0_;
				}
			}
		}
	}



	UNUSED(piana);
	UNUSED(camid);

	res = 1;
	return res;
}


static I32 EstimateBallPosition(iana_t *piana, U32 camid, U64 ts64, U32 index, U32 indexshot, point_t *pPp, double *pPr)
{

	double x, y, r;
	iana_cam_t	*pic;
	marksequence_t	*pmks;
	point_t Pp;

	//---
	pic 	= piana->pic[camid];	
	pmks 		= &pic->mks;

	if (indexshot < 1) {
		indexshot = 1;
	}
#define MINPR	5
	Pp.x	= pic->icp.P.x;
	Pp.y	= pic->icp.P.y;
	if (index < indexshot) {
		r = pmks->ballrP[indexshot-1];
		x = Pp.x;
		y = Pp.y;

		if (r < MINPR) {
			r = pic->icp.cr;
		}
	} else {
		r = pmks->ballrP[index];
		x = pmks->ballposP[index].x;
		y = pmks->ballposP[index].y;

		if (x > Pp.x) {
			x = Pp.x;
			y = Pp.y;
		}

		if (r < MINPR) {
			//point_t Pp;
			//double Pr;
			U64 ts64shot;
			//U64 ts64;
			I64 ts64delta;
			double ts64deltad;

			ts64shot 	= pic->ts64shot;
			//ts64 		= MAKEU64(pb->ts_h, pb->ts_l);

			ts64delta = (I64)(ts64 - ts64shot);
			ts64deltad = ts64delta / TSSCALE_D;
			{
				iana_cam_param_t *picp;
				point_t L1, P1;
				point_t L2, P2;
				double lr_;
			//	double lr_, pr_;

				//--
				picp = &piana->pic[camid]->icp;

				//L1.x	= picp->mx_ * ts64deltad + picp->bx_;
				//L1.y	= picp->my_ * ts64deltad + picp->by_;
				//lr_ 	= picp->mr_ * ts64deltad + picp->br_;

				L1.x	= picp->maLx[2] * ts64deltad*ts64deltad + picp->maLx[1] * ts64deltad + picp->maLx[0];
				L1.y	= picp->maLy[2] * ts64deltad*ts64deltad + picp->maLy[1] * ts64deltad + picp->maLy[0];
				lr_		= picp->maLr[2] * ts64deltad*ts64deltad + picp->maLr[1] * ts64deltad + picp->maLr[0];


				iana_L2P(piana, camid, &L1, &P1, 0);

				L2.x = L1.x + lr_;
				L2.y = L1.y;
				iana_L2P(piana, camid, &L2, &P2, 0);

				r = DDIST(P1.x, P1.y, P2.x, P2.y);

				x = P1.x;
				y = P1.y;
			}
		}
	}


	pPp->x = x;
	pPp->y = y;
	*pPr = r;


	return 1;
}



static I32 GetClubmarkThreshold(
		iana_t *piana, U32 camid,
		clubmark_t *pcm,
		U08 *buf,
		camimageinfo_t     	*pcii,
		point_t *pPp, 
		double Pr
		)
{
#ifdef IPP_SUPPORT

	I32 threshold;
	I32 j;
	Ipp32s histo[GRAYLEVELS];
	Ipp32s levels[GRAYLEVELS+1];

	U32 pixelcount;
	U32 bigcount;
	U32 count;
	U32 threshold0;
	IppiSize roisize;


	U32 width, height;
	U32 width2;
	U32 offset_x, offset_y;

	iana_cam_t	*pic;
	point_t Pp;
	I32 bx, by;			// ball position

	//--
	pic   	= piana->pic[camid];
	Pp.x	= pic->icp.P.x;
	Pp.y	= pic->icp.P.y;

	/*
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Pp :%lf %lf -> %lf %lf\n", 
			Pp.x, Pp.y, pPp->x, pPp->y);
	*/


	Pp.x 	= pPp->x; 
	Pp.y 	= pPp->y; 

	width 	= pcii->width;
	height 	= pcii->height;

	offset_x= pcii->offset_x;
	offset_y= pcii->offset_y;

	//bx 		= (I32)Pp.x + (I32)(pic->icp.cr) - (I32)offset_x;
	bx 		= (I32)Pp.x + (I32)(Pr) - (I32)offset_x;
	by 		= (I32)Pp.y - (I32)offset_y;

	if (bx < 0) {
		bx = 0;
	}

	width2 = width - bx;
	/*
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "bx, by: (%d, %d)   w, h: (%d, %d)\n",
			bx, by, width, height);
			*/

	pixelcount = width*height;
	pixelcount = width2*height;
//#define HIGHERRATE	0.05
//#define HIGHERRATE	0.03
#define HIGHERRATE	0.03
	//#define HIGHERRATE	(0.05*0.01)
	bigcount = (U32) (pixelcount * HIGHERRATE);
	roisize.height = height;
	roisize.width = width;
	roisize.width = width2;

	//ippiHistogramEven_8u_C1R(buf, width, roisize, histo, levels, GRAYLEVELS+1, 0, GRAYLEVELS);
	ippiHistogramEven_8u_C1R(buf+bx, width, roisize, histo, levels, GRAYLEVELS+1, 0, GRAYLEVELS);

	/*
	for (j = 0; j <GRAYLEVELS; j++) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%3d] : %d (%lf %%)\n", j, 
				histo[j], 100.0 * (double) histo[j] / pixelcount);
	}
	*/


	count = 0;
	threshold0 = 0;
	for (j = GRAYLEVELS-1; j >= 0; j--) {
		count += histo[j];
		if (count >= bigcount) {
			threshold0 = j;
			break;
		}
	}

	threshold = threshold0;
#define MAXTHRESHOLD	250
//#define MINTHRESHOLD	150
//#define MINTHRESHOLD	200
///////////////////////#define MINTHRESHOLD	180
/////#define MINTHRESHOLD	130
///////////////#define MINTHRESHOLD	120
//#define MINTHRESHOLD	40
//#define MINTHRESHOLD	80
//#define MINTHRESHOLD	50
#define MINTHRESHOLD	70
	if (threshold > MAXTHRESHOLD) {
		threshold = MAXTHRESHOLD;
	}
	if (threshold < MINTHRESHOLD) {
		threshold = MINTHRESHOLD;
	}

	pcm;
	return threshold;
#else
    I32 threshold;
    I32 j;

    U32 pixelcount;
    U32 bigcount;
    U32 count;
    U32 threshold0;


    U32 width, height;
    U32 width2;
    U32 offset_x, offset_y;

    iana_cam_t	*pic;
    point_t Pp;
    I32 bx, by;			// ball position

    //--
    pic   	= piana->pic[camid];

    Pp.x 	= pPp->x;
    Pp.y 	= pPp->y;

    width 	= pcii->width;
    height 	= pcii->height;

    offset_x= pcii->offset_x;
    offset_y= pcii->offset_y;

    bx 		= (I32)Pp.x + (I32)(Pr)-(I32)offset_x;
    by 		= (I32)Pp.y - (I32)offset_y;

    if (bx < 0) {
        bx = 0;
    }

    width2 = width - bx;

    pixelcount = width2*height;

#define HIGHERRATE	0.03 // 0.05, (0.05*0.01)
    bigcount = (U32)(pixelcount * HIGHERRATE);

    cv::Mat cvImgSrc(cv::Size(width, height), CV_8UC1, buf), cvImgCrop;
    cv::MatND cvHisto;
    float channelRange[] ={ 0.0, 256.0 };
    const float *channelRanges ={ channelRange };
    int histo_cnt = GRAYLEVELS;

    cvImgCrop = cvImgSrc(cv::Rect(bx, 0, width2, height));

    cv::calcHist(&cvImgCrop, 1, 0, cv::Mat(), cvHisto, 1, &histo_cnt, &channelRanges, true, false);
    // cnt += histo.at<int>(i, 0);
    
    count = 0;
    threshold0 = 0;
    for (j = GRAYLEVELS-1; j >= 0; j--) {
        //count += (int)cvHisto.at<float>(j);
        count += (int)cvHisto.at<float>(j);
        if (count >= bigcount) {
            threshold0 = j;
            break;
        }
    }

    threshold = threshold0;

#define MAXTHRESHOLD	250
#define MINTHRESHOLD	70 // 40, 50, 80, 120, 130, 150, 180, 200
    if (threshold > MAXTHRESHOLD) {
        threshold = MAXTHRESHOLD;
    }
    if (threshold < MINTHRESHOLD) {
        threshold = MINTHRESHOLD;
    }

    UNUSED(pcm);
    return threshold;
#endif
}
		
		
#if 0 // Not used
static U32 rectobjcalc(
		double x[], 
		double y[], 
		U32 count,
		rectObj_t *pro)
{
	U32 res;
	I32 i, j;

	double dx, dy;
	double d2;
	double dmax;
	I32 i0, i1, i2, i3;
	double a_, b_, c_;			// line..

	double dmaxp, dmaxn;
	//--
	dmax = -999;
	i0 = 0;
	i2 = 1;
	for (i = 0; i < (I32)count; i++) {
		for (j = i+1; j < (I32)count; j++) {
			dx = x[i] - x[j];
			dy = y[i] - y[j];

			d2 = dx*dx + dy*dy;
			if (dmax < d2) {
				dmax = d2;
				i0 = i;
				i2 = j;
			}
		}
	}

	/*
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "max %4d(%lf, %lf) ~  %4d(%lf %lf) : %lf\n",
			i0, x[i0], y[i0],
			i2, x[i2], y[i2],
			dmax);
			*/

	{	// line..
		double dx_, dy_;

		//--
		dx = x[i0] - x[i2];
		dy = y[i0] - y[i2];

		if (dx > 0) { dx_ = dx; } else { dx_ = -dx; }
		if (dy > 0) { dy_ = dy; } else { dy_ = -dy; }

		if (dx_ > dy_) {
			a_ = - dy / dx;
			b_ = 1.0;
		} else {
			a_ = 1.0;
			b_ = - dx / dy;
		}

		c_ = -a_ * x[i0] - b_ * y[i0];
	}

	//
	dmaxp = -999;
	dmaxn = 999;
	i1 = -999;
	i3 = -999;
	for (i = 0; i < (I32)count; i++) {
		d2 = a_ * x[i] + b_ * y[i] + c_;
		if (d2 >= 0.0) {
			if (dmaxp < d2) {
				dmaxp = d2;
				i1 = i;
			}
		} else {
			if (dmaxn > d2) {
				dmaxn = d2;
				i3 = i;
			}
		}
	}

	/*
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "max %4d(%lf, %lf) ~  %4d(%lf %lf)\n",
			i1, x[i1], y[i1],
			i3, x[i3], y[i3]);
			*/

	pro->p[0].x = x[i0]; pro->p[0].y = y[i0];
	pro->p[1].x = x[i1]; pro->p[1].y = y[i1];
	pro->p[2].x = x[i2]; pro->p[2].y = y[i2];
	pro->p[3].x = x[i3]; pro->p[3].y = y[i3];


	//
	res = 1;
	return res;
}
#endif

static double calcpolyarea(double x[], double y[], U32 count)			
{
	double areav;
	U32 i, j;

	areav = 0;
	j = count-1;
	for (i = 0; i < count; i++) {
		areav = areav + (x[j] + x[i]) * (y[j]-y[i]);
		j = i;
	}

	return areav/2.0;
}

static U32 findrect(
		point_t p0[],
		point_t p1[],
		point_t p2[],
		point_t p3[],
		rectObj_t *pro
		)
{
	U32 res;
	U32 index;
	U32 index0;
	U32 index1;
	U32 index2;
	U32 index3;
	double x[4], y[4];
	double areav;
	double maxarea;
	double maxx[4], maxy[4];
	U32 maxindex;

	//--
	maxarea = 0;
	maxindex =0;
	memset(&maxx[0], 0, sizeof(double)*4);
	memset(&maxy[0], 0, sizeof(double)*4);
	for (index = 0; index < 256; index++) {
		index0 = (index>>0) & 0x03;
		index1 = (index>>2) & 0x03;
		index2 = (index>>4) & 0x03;
		index3 = (index>>6) & 0x03;
		x[0] = p0[index0].x;
		y[0] = p0[index0].y;

		x[1] = p1[index1].x;
		y[1] = p1[index1].y;

		x[2] = p2[index2].x;
		y[2] = p2[index2].y;

		x[3] = p3[index3].x;
		y[3] = p3[index3].y;

		areav = calcpolyarea(x, y, 4);

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%d :  (%4lf, %4lf) ~ (%4lf, %4lf) ~ (%4lf, %4lf) ~ (%4lf, %4lf)\n %lf vs %lf\n", index, 
		//		x[0], y[0],
		//		x[1], y[1],
		//		x[2], y[2],
		//		x[3], y[3],
		//		areav, maxarea);
		if (maxarea < areav) {
			maxarea = areav;
			maxindex = index;
			memcpy(&maxx[0], &x[0], sizeof(double)*4);
			memcpy(&maxy[0], &y[0], sizeof(double)*4);
		}
	}
	for (index = 0; index < 4; index++) {
		pro->p[index].x = maxx[index];
		pro->p[index].y = maxy[index];
	}
	pro->areav = maxarea;
	pro->pixelcount = 0;

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "MAX. %d :  (%4lf, %lf) ~ (%4lf, %4lf) ~ (%4lf, %4lf) ~ (%lf, %lf)\n %lf\n",
	//		maxindex, 
	//		x[0], y[0],
	//		x[1], y[1],
	//		x[2], y[2],
	//		x[3], y[3],
	//		maxarea
	//		);
#if defined(_WIN32)
#pragma warning(disable:4457)
#endif
	{
		U32 i, j, k;
		cr_point_t p0, p1, p2;
		double maxvertexlen;
		U32 maxvertexlanIndex;

		//--
		maxvertexlen = 0;
		maxvertexlanIndex = 0;
		for (i = 0; i < 4; i++) {
			j = i+1; j = j % 4;
			k = j+1; k = k % 4;

			p0.x = pro->p[i].x;
			p0.y = pro->p[i].y;
			p0.z = 0;
			p1.x = pro->p[j].x;
			p1.y = pro->p[j].y;
			p1.z = 0;
			p2.x = pro->p[k].x;
			p2.y = pro->p[k].y;
			p2.z = 0;

			cr_line_p0p1( &p0, &p1, &pro->vertex[i]);					// vertex line.
			pro->vertexlen[i] = cr_vector_distance(&p0, &p1);			// length of vertex
			pro->vertexdist[i] = cr_distance_P_Line(&p2, &pro->vertex[i]);	// distance btw vertex and next edge point
			if (maxvertexlen < pro->vertexlen[i]) {
				maxvertexlen = pro->vertexlen[i];
				maxvertexlanIndex = i;
			}
		}
		pro->maxvertexlenIndex = maxvertexlanIndex;
	
#if 1
		// re-ordering edge points.
		if (maxvertexlen > 1) {
			double dx, dy;
			double dx_, dy_;
			double a_, b_, c_;			// line..
			point_t p0123[4];
			double vertexlen[4];
			double vertexdist[4];
			double dtmp;

			//--
			
			for (i = 0; i < 4; i++) {
				memcpy(&p0123[i], &pro->p[i], sizeof(point_t));
				vertexlen[i] = pro->vertexlen[i];
				vertexdist[i] = pro->vertexdist[i];

				x[i] = pro->p[i].x;
				y[i] = pro->p[i].y;
			}

			// 1) select maxvertext
			i = pro->maxvertexlenIndex;
			j = (i + 1)%4;

			k = (i + 2)%4;

			// 2) make maxvertex line
			dx = x[i] - x[j];
			dy = y[i] - y[j];

			if (dx > 0) { dx_ = dx; } else { dx_ = -dx; }
			if (dy > 0) { dy_ = dy; } else { dy_ = -dy; }

			if (dx_ > dy_) {
				a_ = - dy / dx;
				b_ = 1.0;
			} else {
				a_ = 1.0;
				b_ = - dx / dy;
			}

			c_ = -a_ * x[i] - b_ * y[i];


			// 3) calc
			//dtmp = a_ * x[k] + b_ * y[k] + c_;
			dtmp = a_ * x[(i+3)%4] + b_ * y[(i+3)%4] + c_;

			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "dtmp = %lf, i: %d, k: %d\n", dtmp, i, k);
			if (dtmp < 0) {		// maxvertex is upper line.
				for (j = 0; j < 4; j++) {
					memcpy(&pro->p[j], &p0123[(i+j)%4], sizeof(point_t)); 
					pro->vertexlen[j] = vertexlen[(i+j)%4];
					pro->vertexdist[j] = vertexdist[(i+j)%4];
				}
				pro->maxvertexlenIndex = 0;

			} else {			// maxvertex is under line

				for (j = 0; j < 4; j++) {
					memcpy(&pro->p[j], &p0123[(k+j)%4], sizeof(point_t)); 
					pro->vertexlen[j] = vertexlen[(k+j)%4];
					pro->vertexdist[j] = vertexdist[(k+j)%4];
				}
				pro->maxvertexlenIndex = 2;
			}
		}
#endif
	}
#if defined(_WIN32)
#pragma warning(default:4457)
#endif
	res = 1;
	return res;
}

static U32 isinside(
		point_t	  *pinP,			// 
		rectObj_t *pro
		)
{
	U32 res;
	I32 crosses;
	I32 i, j;
	double xi, xj;
	double yi, yj;
	double atX;
	double x, y;
	
	//--
#define DIFFYIYJ_MIN	(1e-5)

	x = pinP->x;
	y = pinP->y;
	crosses = 0;
	for(i = 0 ; i < 4; i++) {
		j = (i+1) % 4;

		xi = pro->p[i].x; yi = pro->p[i].y;
		xj = pro->p[j].x; yj = pro->p[j].y;

		if ((yi > yj - DIFFYIYJ_MIN) && (yi < yj + DIFFYIYJ_MIN)) {		// too small difference.
			yi = yj + DIFFYIYJ_MIN;
		}
		
		if( (yi > y) != (yj > y) ) {
			atX = (xj - xi) * (y-yi) / (yj - yi) + xi;
			if(x < atX) {
				crosses++;
			}
		}
	}
	if ((crosses & 1) == 0) {		// EVEN: External
		res = 0;
	} else {					// ODD : Internal.
		res = 1;				
	}
	return res;
}


static U32 rectpixelcount(				// pixel count in the rectangular
		U32 width,
		U08 *bufLabel,
		U08 *bufBin,
		clubmark_seg_t	*pclm
		)
{
	I32 i, j;

	U08 segindex;
	U32 count;
	U32 count2;
	U32 isin;
	imagesegment_t 	*pis;
	rectObj_t 		*pro;
	point_t	inP;
	//--

	count = 0;
	count2 = 0;
	pis = &pclm->imgseg;
	pro = &pclm->rectObj;
	segindex = (U08) pis->label;
	for (i = pis->iyu; i <= (I32)pis->iyb; i++) {
		I32 segstart;
		I32 segend;

		for (j = pis->ixl; j <= (I32)pis->ixr; j++) {
			if (*(bufBin + j + i * width))
			{
				inP.x = j; inP.y = i;
				isin = isinside(&inP, pro);
				if (isin) {
					count++;
				}
			}
		}

		segstart = pis->ixr;
		for (j = pis->ixl; j <= (I32)pis->ixr; j++) {
			if (*(bufLabel + j + i * width) == segindex) {
				segstart = j;
				break;
			}
		}

		segend  = pis->ixl;
		for (j = pis->ixr; j >= (I32)pis->ixl; j--) {
			if (*(bufLabel + j + i * width) == segindex) {
				segend = j;
				break;
			}
		}
		if (i != (I32)pis->iyu && i != (I32)pis->iyb) {
			if (segstart < segend) {
				count2 = count2 + (segend - segstart - 1);
			}
		}
	}

	pro->pixelcount =  count;
	pro->pixelcount2 =  count2;
	return count;
}


double quadregression_errcalc(double xx[], double yy[], U32 count, double ma[3])
{
	double meanerr;
	double yy_;
	double ey_;
	U32 i;

	meanerr = 0;
	for (i = 0; i < count; i++) {
		yy_ = ma[2]*xx[i]*xx[i] + ma[1]*xx[i] + ma[0];
		ey_ = yy[i] - yy_;
		meanerr += ey_*ey_;
	}

	meanerr = (meanerr / count);
	meanerr = sqrt(meanerr);

	return meanerr;
}

static double calcRectArea(point_t vertex[4]) 
{
	//Area(rect(p0p1p2p3)) = Area(try(p0p1p2)) + Area(tri(p0p2p3))
	double s;
	point_t tri0[3], tri1[3];


	memcpy(&tri0[0], &vertex[0], sizeof(point_t) * 3);

	memcpy(&tri1[0], &vertex[0], sizeof(point_t) * 1);
	memcpy(&tri1[1], &vertex[2], sizeof(point_t) * 2);

	s = calcTriArea(&tri0[0]) + calcTriArea(&tri1[0]); 

	return s;
}

static double calcTriArea(point_t vertex[3])
{
	double s;
	//regarea(tri) = |(x1-x0)(y2-y0) - (x2-x0)(y1-y0)|/2
	s = (vertex[1].x - vertex[0].x) * (vertex[2].y - vertex[0].y) - (vertex[2].x - vertex[0].x) * (vertex[1].y - vertex[0].y);
	if (s < 0) { s = -s;}

	return s;
}

static double calcDist(point_t *pvertex0, point_t *pvertex1)
{
	double dx, dy, dlen;

	dx = pvertex0->x - pvertex1->x;
	dy = pvertex0->y - pvertex1->y;

	dlen = sqrt(dx*dx + dy*dy);

	return dlen;
}

//rename it. this does not calculate general club data.
static I32 calcclubdata(cr_vector_t *pclubvect, double *pclubspeed, double *pclubpathangle, double *pattackangle)
{
	I32 res;
	double clubspeed;
	double clubpathangle;
	double clubattack;

	double x_, y_, z_;
	double xy_;

	//--
	clubspeed = cr_vector_norm(pclubvect);
	x_ =pclubvect->x;
	y_ =pclubvect->y;
	z_ =pclubvect->z;

	xy_ = sqrt(x_*x_ + y_*y_);

	clubpathangle = atan2(x_, y_);
	clubpathangle = RADIAN2DEGREE(clubpathangle);

	if (clubpathangle < -90) {clubpathangle = clubpathangle + 180;}
	if (clubpathangle > +90) {clubpathangle = clubpathangle - 180;}

	clubattack = atan2(z_, xy_);
	clubattack = RADIAN2DEGREE(clubattack);

	if (clubattack < -90) {clubattack = clubattack + 180;}
	if (clubattack > +90) {clubattack = clubattack - 180;}

	*pclubspeed = clubspeed;
	*pclubpathangle = clubpathangle;
	*pattackangle = clubattack;

	res = 1;
	return res;
}

static I32 removeoutlier(double xx[], double yy[], U32 count, double xx0[], double yy0[], U32 *pnewcount, double maxdistance)
{
	I32 res;
	U32 *isvalid;
	U32 *calcindex;
	U32 newcount;
	double mdist;
	U32 maxdistindex;

	U32 i;
	//----
	isvalid = (U32 *) malloc(count * sizeof(U32));
	calcindex = (U32 *) malloc(count * sizeof(U32));
	for (i = 0; i < count; i++) {
		isvalid[i] = 1;
	}
	newcount = count;

	while(newcount >= 3) {
		U32 invalidindex;
		newcount = 0;
		for (i = 0; i < count; i++) {
			if (isvalid[i]) {
				xx0[newcount] = xx[i];
				yy0[newcount] = yy[i];
				calcindex[newcount] = i;
				newcount++;
			}
		}
		if (newcount <= 3) {
			break;
		}
		getmaxdistance(xx0, yy0, newcount, &maxdistindex, &mdist);
		if (mdist > maxdistance) {

			invalidindex = calcindex[maxdistindex];
			isvalid[invalidindex] = 0;
		} else {
			break;
		}
	}

	free(isvalid);
	free(calcindex);

	*pnewcount = newcount;

	res = 1;
	return res;
}


static I32 getmaxdistance(double xx[], double yy[], U32 count, U32 *pmaxdistindex, double *pmaxdistance)
{
	I32 res;
	double ma[3];
	I32 calcindex;

	double *xx0, *yy0;
	double y_;
	double err_, maxerr_;
	double r2;
	U32 maxerri, maxerrj;
	U32 i, j;


	//--

	maxerr_ = 0;
	maxerri = 0;
	maxerrj = 0;
	xx0 = (double *) malloc(count * sizeof(double));
	yy0 = (double *) malloc(count * sizeof(double));
	for (i = 0; i < count; i++) {
		calcindex = 0;
		for (j = 0; j < count; j++) {
			if (j == i) {
				continue;
			}
			xx0[calcindex] = xx[j];
			yy0[calcindex] = yy[j];
			calcindex++;
		}
		cr_regression2(xx0, yy0, calcindex, &ma[1], &ma[0], &r2);

		for (j = 0; j < count; j++) {
			y_ = ma[1] * xx[j] + ma[0];
			err_ = yy[j] - y_;
			if (err_ < 0) err_ = - err_;
			if (maxerr_ < err_) {
				maxerr_ = err_;
				maxerrj = j;
				maxerri = i;
			}
		}
	}
	free(xx0);
	free(yy0);

	*pmaxdistindex = maxerrj;
	*pmaxdistance = maxerr_;

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "maxdist (%d, %d) : %lf\n", maxerri, maxerrj, maxerr_);
	res = 1;
	return res;
}


// TODO: need refactoring these new functions!!!
I32 club_mark_revive_dot(iana_t *piana, U32 camid, clubmark_t *pcm)
{
	I32 res;

	clubmark_cam_t *pcmc;

	U32 index0, index1;
	U32 index;
	U32 maxdotcount;
	U32 onedotmode;

	//--
	pcmc = &pcm->cmc[camid]; 
	index0 = pcmc->index0;
	index1 = pcmc->index1;

	maxdotcount = 0;
	for (index = index0; index <= index1; index++) {
		clubmark_dot_t *pcmd0;
		U32 i;
		U32 dotcount;

		pcmd0 = &pcmc->cft[index].cmd;
		dotcount = 0;
		for (i = 0; i < (I32)pcmd0->count; i++) {
			if (pcmd0->clm[i].state == CLUBMARK_EXILE) {
				clubmark_exilewhy_t exilewhy;

				exilewhy = pcmd0->clm[i].exilewhy;
				if (
						exilewhy == CLUBMARK_EXILEWHY_SMALLER_LOWERMAXY1
						||exilewhy == CLUBMARK_EXILEWHY_SMALLER_LOWERMAXY2
					) {
					pcmd0->clm[i].state = CLUBMARK_DOT;						// revive.. 
				}
			}
			if (pcmd0->clm[i].state == CLUBMARK_DOT) {
				dotcount++;
			}
		}
		if (maxdotcount < dotcount) {
			maxdotcount = dotcount;
		}
	}
//#define MIN_DOTCOUNT_Q_CLUB	3
#define MIN_DOTCOUNT_Q_CLUB	4
	if (maxdotcount < MIN_DOTCOUNT_Q_CLUB) {
		onedotmode = 1;
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ONEDOT mode with maxdotcount: %d\n", maxdotcount);
		// onedot mode.
	} else {
		onedotmode = 0;
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Q__DOT mode with maxdotcount: %d\n", maxdotcount);
		// Q dot mode. :P
	}

	GetClubmarkRegion(piana, camid, pcm);
	GetFaceData(piana, camid, onedotmode);
	RegressionClubmark(piana, camid, pcm);
	DisplayClubmarkBarDot(piana, camid, pcm, index0, index1, 1, 1, 1, 1);

	res = 1;

	UNUSED(piana);
	return res;
}

I32 clubspeed_dot_3d_mult(iana_t *piana, clubmark_t *pcm, double *pmultvalue)
{
	I32 res;
	U32 camid;
	double dx;
	double multv;
	U32 done;

	//--
	dx = -999;
	done = 0;
	for (camid = 0; camid < NUM_CAM; camid++) {
		clubmark_cam_t *pcmc;
		clubmark_feature_t	*pcft;
		clubmark_face_t *pcfc;
		point_t Lb;
		I32 index0, index1;
		I32 index;

		//--
		pcmc = &pcm->cmc[camid];
		index0 = pcmc->index0;
		index1 = pcmc->index1;

		pcft = &pcmc->cft[index1];
		iana_P2L(piana, camid, &pcft->Pp, &Lb, 0);

		for (index = index1; index >= index0; index--) {
			point_t Ld;

			pcfc = &pcmc->face[index];
			if (pcfc->lowerdot.valid) {
				Ld.x = pcfc->lowerdot.cdotL.x;
				Ld.y = pcfc->lowerdot.cdotL.y;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d %d] ball: %lf %lf    dot: %lf %lf\n",
						camid, index, Lb.x, Lb.y, Ld.x, Ld.y);
				dx = Lb.x - Ld.x;
				done = 1;
				break;
			}
		}
		if (done) {
			break;
		}
	}

	if (done) {

		if (piana->Right0Left1 == 0) {		// RIGHT..  20201008
			dx = dx;
		} else {
			dx = -dx;
		}

		if (dx < -BALLDIAMETER/10.0) {
			multv = LOWERDOT_CLUBSPEED_MULT0; 
		} else if (dx < 0.0) {
			multv = LINEAR_SCALE(dx, (-BALLDIAMETER/10.0), LOWERDOT_CLUBSPEED_MULT0, 0.0, LOWERDOT_CLUBSPEED_MULT);
		} else {
			multv = LOWERDOT_CLUBSPEED_MULT; 
		}

		*pmultvalue = multv;
	} else {
		*pmultvalue = LOWERDOT_CLUBSPEED_MULT;
	}

	res = 1;

	return res;
}


I32 clubspeed_dot_2d_mult(iana_t *piana, clubmark_t *pcm, double *pmultvalue)
{
	I32 res;
	U32 camid;
	double dx;
	double multv;
	U32 done;

	//--
	dx = -999;
	done = 0;
	for (camid = 0; camid < NUM_CAM; camid++) {
		clubmark_cam_t *pcmc;
		clubmark_feature_t	*pcft;
		clubmark_face_t *pcfc;
		point_t Lb;
		I32 index0, index1;
		I32 index;

		//--
		pcmc = &pcm->cmc[camid];
		index0 = pcmc->index0;
		index1 = pcmc->index1;

		pcft = &pcmc->cft[index1];
		iana_P2L(piana, camid, &pcft->Pp, &Lb, 0);

		for (index = index1; index >= index0; index--) {
			point_t Ld;

			pcfc = &pcmc->face[index];
			if (pcfc->lowerdot.valid) {
				Ld.x = pcfc->lowerdot.cdotL.x;
				Ld.y = pcfc->lowerdot.cdotL.y;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d %d] ball: %lf %lf    dot: %lf %lf\n",
						camid, index, Lb.x, Lb.y, Ld.x, Ld.y);
				dx = Lb.x - Ld.x;
				done = 1;
				break;
			}
		}
		if (done) {
			break;
		}
	}

	if (done) {
#define LOWERDOT_2D_CLUBSPEED_MULT0	0.95
		if (piana->Right0Left1 == 0) {		// RIGHT..  20201008
			dx = dx;
		} else {
			dx = -dx;
		}

		if (dx < -BALLDIAMETER/10.0) {
			multv = LOWERDOT_2D_CLUBSPEED_MULT0; 
		} else if (dx < 0.0) {
			multv = LINEAR_SCALE(dx, (-BALLDIAMETER/10.0), LOWERDOT_2D_CLUBSPEED_MULT0, 0.0, LOWERDOT_2D_CLUBSPEED_MULT);
		} else {
			multv = LOWERDOT_2D_CLUBSPEED_MULT; 
		}

		*pmultvalue = multv;
	} else {
		*pmultvalue = LOWERDOT_2D_CLUBSPEED_MULT;
	}

	res = 1;

	return res;
}


#if defined (__cplusplus)
}
#endif

