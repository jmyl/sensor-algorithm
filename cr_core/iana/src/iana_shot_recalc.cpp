/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA shot Re-calc.
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2020 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_shot_recalc.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2020/05/26 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <math.h>
#include <string.h>

#include "cr_common.h"

#include "cr_thread.h"
#include "cr_osapi.h"
#include "cr_nanoxml.h"

#include "iana.h"
// #include "iana_tool.h"
#include "iana_coordinate.h"

#include "cr_regression.h"
#include "iana_shot_recalc.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
// #define DEBUG_SHOT_RECALC
#if defined(_WIN32)
#pragma warning(disable:4456)
#endif
#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------
 *	Description	: local functions
 -----------------------------------------------------------------------------*/

static int vmagcompare(const void *r1, const void *r2) 
{
  double v1 = *(double *)r1;
  double v2 = *(double *)r2;

  //--
  if (v1 < v2) { return 1; }
  if (v1 > v2) {return -1; }
  return 0;
}



/*!
 ********************************************************************************
 *	@brief      computes points in local coordinates
 *
 *  @param[in]	piana
 *              IANA module handler
 *  @param[in]	camid
 *              camera index - 0:master/center, 1:slave/side
 *  @param[out]	bpP
 *              ball position in pixels
 *  @param[out]	bpL
 *              ball position in local coordinate
 *  @param[out]	tsd
 *              timestamp array, measured from the shot moment. 
 *  @param[out]	bpvalid
 *              array of validity of ball position
 *  @param[out]	pbpvalidcount
 *              the number of valid balls
 *  @param[in]	seqcount
 *              number of (image)sequcence
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2020/05/26
 *******************************************************************************/
//why do we need this many inputs? remove some if the variables are used in this function only.
static I32 getLocalPoints(iana_t *piana, U32 camid,
	cr_point_t bpP[],
	cr_point_t bpL[],
	double tsd[],
	U32 bpvalid[],
	U32 *pbpvalidcount,
	U32 seqcount)
{
	I32 res;

	iana_cam_t			*pic;
	marksequence_t		*pmks;
	ballmarkinfo_t		*pbmi;
	U64 ts64shot;

	U32 validcount;
	U32 i;
	//---------
	pic 		= piana->pic[camid];
	pmks 		= &pic->mks;
	pbmi		= &pic->bmi[0];
	ts64shot 	= pic->ts64shot;

	validcount = 0;
	memset(&bpvalid[0], 0, sizeof(U32)*MARKSEQUENCELEN);		// NOT valid yet.

	for (i = 0; i < seqcount; i++) {			// Get Valid Points... with Distance.
		I64 ts64delta;
		point_t PpRaw, Pp;
		point_t LpRaw, Lp;
		double dx, dy, dist;

		//---
		ts64delta = pbmi[i].ts64 - ts64shot;
		tsd[i] = (double) (ts64delta / TSSCALE_D);

		if (pbmi[i].valid == 0) {
			bpvalid[i] = 0;
		}

		if (ts64delta < 0) {
			bpvalid[i] = 0;
			continue;
		}

		// Check Valid
		PpRaw.x = pmks->ballposP2Raw[i].x;
		PpRaw.y = pmks->ballposP2Raw[i].y;
		Pp.x    = pmks->ballposP[i].x;
		Pp.y    = pmks->ballposP[i].y;

		iana_P2L(piana, camid, &PpRaw, &LpRaw, 0);
		iana_P2L(piana, camid, &Pp, &Lp, 0);

		dx = LpRaw.x - Lp.x; dy = LpRaw.y - Lp.y;
		dist = sqrt(dx*dx + dy*dy);

#define YYY  0.01
		if (dist < YYY) {
			memcpy(&bpP[i], &PpRaw, sizeof(point_t));
			memcpy(&bpL[i], &LpRaw, sizeof(point_t));
			tsd[i] = (double) (ts64delta / TSSCALE_D);
			bpvalid[i] = 1;
			validcount++;
		}
	}

	*pbpvalidcount = validcount;

	res = 1;
	return res;
}



#if 0	// Not used
/*!
 ********************************************************************************
 *	@brief      re-calculate the local line, which is the line passing all local ballpoints.
 *
 *  @param[in]	bpL
 *              ball position in local coordinate
 *  @param[in]	valid
 *              validities of (local)ball positions
 *  @param[out]	ploocalline
 *              pointer to the shot line, which passes all local ballpoints
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2020/05/26
 *******************************************************************************/
// not used
static I32 recalc_shot_localline(cr_point_t bpL[], U32 valid[], cr_line_t *plocalline)
{
	I32 res;
	U32 count;
	U32 i;
	double xx[MARKSEQUENCELEN], yy[MARKSEQUENCELEN];
	cr_point_t p0;

	//--
	count = 0;
	p0.x = p0.y = p0.z = 0;
	for (i = 0; i < MARKSEQUENCELEN; i++) {
		if (valid[i] != 0) {
			xx[count] = bpL[i].x;
			yy[count] = bpL[i].y;
			p0.x += xx[count];
			p0.y += yy[count];
			count++;
		}
	}

	if (count > 2) {
		double ma[2];
		cr_vector_t m;
		
		//---
		p0.x = p0.x / count;
		p0.y = p0.y / count;
		
		res = cr_regression(yy, xx, count, &ma[1], &ma[0]);
		m.v[0] = ma[1];
		m.v[1] = 1.0;
		m.v[2] = 0.0;

		cr_line_p0m(&p0, &m, plocalline);
		cr_vector_normalization(&plocalline->m, &plocalline->m);
		if (plocalline->m.y < 0) {
			cr_vector_scalar(&plocalline->m, -1, &plocalline->m);
		}

		res = 1;
	} else {
		res = 0;
	}

	return res;
}

#endif

/*!
 ********************************************************************************
 *	@brief      re-calculate the local line, which is the line passing all local ballpoints, with RANSAC algorithm.
 *
 *  @param[in]	bpL
 *              ball position in local coordinate
 *  @param[in]	valid
 *              validities of (local)ball positions
 *  @param[out]	ploocalline
 *              pointer to the shot line, which passes all local ballpoints
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2020/05/26
 *******************************************************************************/
static I32 recalc_shot_localline_RANSAC(cr_point_t bpL[], U32 valid[], cr_line_t *plocalline)
{
	I32 res;
	U32 count;
	U32 i, j, k, n;
	double xx[MARKSEQUENCELEN], yy[MARKSEQUENCELEN];
	//--
	count = 0;
	for (i = 0; i < MARKSEQUENCELEN; i++) {
		if (valid[i] != 0) {
			xx[count] = bpL[i].x;
			yy[count] = bpL[i].y;
			count++;
		}
	}

	if (count > 2) {
		cr_line_t li_min, li;
		double distsum_min;

		//--
		distsum_min = 9999;
		memset(&li_min, 0, sizeof(cr_line_t));
		for (i = 0; i < count; i++) {
			cr_point_t pi, pj, pk;
			double distsum;

			//---
			for (n = 2; n < count-1; n++) {
				j = (i + count-n) % count;					// p(i), p(j)

				pi.x = xx[i]; pi.y = yy[i]; pi.z = 0;
				pj.x = xx[j]; pj.y = yy[j]; pj.z = 0;

				cr_line_p0p1(&pi, &pj, &li);				// line btw p0 and p1

				distsum = 0;
				for (k = 0; k < count; k++) {
					double dist;
					pk.x = xx[k]; pk.y = yy[k]; pk.z = 0;
					dist = cr_distance_P_Line(&pk, &li);
					distsum += dist;
					//distsum += dist*dist;
				}
				if (distsum_min > distsum) {
					distsum_min = distsum;
					memcpy(&li_min, &li, sizeof(cr_line_t));
				}
			}
		}
		memcpy(plocalline, &li_min, sizeof(cr_line_t));
		cr_vector_normalization(&plocalline->m, &plocalline->m);
		if (plocalline->m.y < 0) {
			cr_vector_scalar(&plocalline->m, -1, &plocalline->m);
		}
		res = 1;
	} else {
		res = 0;
	}

	return res;
}



/*!
 ********************************************************************************
 *	@brief      re-calculate the shot plane, which is the plane passing all ballpoints and the camera
 *
 *  @param[in]	piana
 *              IANA module handler
 *  @param[in]	camid
 *              camera index - 0:master/center, 1:slave/side
 *  @param[in]	bpL
 *              ball position in local coordinate
 *  @param[in]	valid
 *              validities of (local)ball positions
 *  @param[out]	ploocalline
 *              pointer to the shot line, which passes all local ballpoints
 *  @param[out]	pshitplane
 *              pointer to the resulted shot plane
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2020/05/26
 *******************************************************************************/
static I32 recalc_shot_plane(iana_t *piana, U32 camid, 
		cr_point_t bpL[], U32 valid[],
		cr_line_t *plocalline,
		cr_plane_t *pshotplane)
{
	I32 res;
	iana_cam_t	*pic;
	iana_cam_param_t *picp;
	cr_point_t *pCamPosL;
	cr_line_t localline;

	//--
	pic 	= piana->pic[camid];
	picp 	= &pic->icp;
	pCamPosL= &picp->CamPosL;

	//----
    // res = recalc_shot_localline(bpL, valid, plocalline);
	// res = recalc_shot_localline(bpL, valid, &localline);
	res = recalc_shot_localline_RANSAC(bpL, valid, &localline);
	memcpy(plocalline, &localline, sizeof(cr_line_t));

	if (res == 1) {
		cr_plane_p0line(		// Make plane with p0 and line
				pCamPosL,			// mid point.. 
				plocalline,	// local line
				pshotplane);
	}
	return res;
}


/*!
 ********************************************************************************
 *	@brief      re-calculate the trajectory line, which is the line passing all (real)ballpoints.
                the trajectory line is the intersection line of two shot planes
 *
 *  @param[in]	piana
 *              IANA module handler
 *  @param[in]	shotplane
 *              array of the two shotplanes
 *  @param[out]	ptrajline
 *              pointer to the trajectory line
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2020/05/26
 *******************************************************************************/
static I32 recalc_shot_trajline(iana_t *piana, cr_plane_t shotplane[], cr_line_t *ptrajline)
{
	I32 res;

	res = cr_line_plane_plane(
			&shotplane[0],
			&shotplane[1],
			ptrajline);

	UNUSED(piana);
	return res;
}

/*!
 ********************************************************************************
 *	@brief      re-calculate(refine) the ball positions using trajectory line.
 *
 *  @param[in]	piana
 *              IANA module handler
 *  @param[in]	camid
 *              camera index - 0:master/center, 1:slave/side
 *  @param[in]	ptrajline
 *              pointer to the trajectory line
 *  @param[in]	bpL
 *              pre computed ball positions in local coordinates
 *  @param[in]	valid
 *              array of validities of ball positions
 *  @param[out]	bpL3d
 *              refined ball positions in 3d coordi. (WARN: this is not a position in local coordinates)
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2020/05/26
 *******************************************************************************/
static I32 recalc_shot_3dpoint_trajline(
		iana_t *piana, U32 camid,
		cr_line_t *ptrajline,
		cr_point_t bpL[], U32 valid[],
		cr_point_t bpL3d[])
{
	I32 res;
	iana_cam_t	*pic;
	cr_point_t *pCamPosL;
	cr_line_t   cambline;	// camera to ball line
	U32 i;

	//--
	pic = piana->pic[camid];
	pCamPosL = &pic->icp.CamPosL;
	for (i = 0; i < MARKSEQUENCELEN; i++) {
		if (valid[i]) {
			cr_point_t b3dTraj, b3dCamline;
			cr_line_p0p1(&bpL[i], pCamPosL, &cambline);	// make camera to ball line
			res =  cr_point_line_line(&cambline, ptrajline, &b3dCamline, &b3dTraj);
			memcpy(&bpL3d[i], &b3dTraj, sizeof(cr_point_t));
		}
	}
	res = 1;
	return res;
}


/*!
 ********************************************************************************
 *	@brief      CAM shot recalc.
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2020/05/26
 *******************************************************************************/
I32 iana_shot_recalc(iana_t *piana)
{
	I32 res;

	I32 marksequencelen;
	U32 camid;

	I32 i;

	cr_point_t bpP[MAXCAMCOUNT][MARKSEQUENCELEN];
	cr_point_t bpL[MAXCAMCOUNT][MARKSEQUENCELEN];
	cr_point_t bpL3d[MAXCAMCOUNT][MARKSEQUENCELEN];

	U32 bpvalid[MAXCAMCOUNT][MARKSEQUENCELEN];
	U32 bpvalidcount[MAXCAMCOUNT];
	double tsd[MAXCAMCOUNT][MARKSEQUENCELEN];
	double vmagarray[MARKSEQUENCELEN*2];

	cr_line_t localline[MAXCAMCOUNT];
	cr_plane_t shotplane[MAXCAMCOUNT];
	cr_line_t trajline;

	//--------------------------------------------------
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}
	
	memset(&bpP[0][0], 0, sizeof(bpP));
	memset(&bpL[0][0], 0, sizeof(bpL));
	memset(&tsd[0][0], 0, sizeof(tsd));

	//  1) Get Valid L-points 
	for (camid = 0; camid < NUM_CAM; camid++) {
		getLocalPoints(piana, camid,
				&bpP[camid][0],
				&bpL[camid][0],
				&tsd[camid][0],
				&bpvalid[camid][0],
				&bpvalidcount[camid],
				marksequencelen);

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n\n----------------------------------\n");
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] count: %d\n", camid, bpvalidcount[camid]);

		for (i = 0; i < marksequencelen; i++) {			// Get Valid Points... with Distance.
			//if (tsd[camid][i] < 0.004 || tsd[camid][i] > 0.007) {
			//	bpvalid[camid][i] = 0;
			//}
#ifdef DEBUG_SHOT_RECALC
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
					"<%4d:%10.4lf:%d> %10.4lf %10.4lf %10.4lf %10.4lf\n", 
					i, tsd[camid][i], bpvalid[camid][i],
					bpP[camid][i].x, 
					bpP[camid][i].y, 
					bpL[camid][i].x, 
					bpL[camid][i].y);
#endif
		}
	}

	// 2) get localline and shotplane
#define RECALCVALIDCOUNT 3 // 4
	if (bpvalidcount[0] >= RECALCVALIDCOUNT && bpvalidcount[1] > RECALCVALIDCOUNT) {
		for (camid = 0; camid < NUM_CAM; camid++) {
			recalc_shot_plane(
                piana, camid, 
                &bpL[camid][0], &bpvalid[camid][0],
                &localline[camid], &shotplane[camid]);

#ifdef DEBUG_SHOT_RECALC
			for (i = 0; i < marksequencelen; i++) {
				if (bpvalid[camid][i]) {
					cr_point_t *pP;
					double distP, distL;

					pP = &bpL[camid][i];
					distL = cr_distance_P_Line(pP, &localline[camid]);
					distP = cr_distance_P_Plane(pP, &shotplane[camid]);

					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,
						"[%d %d %10.4lf] %10.4lf %10.4lf %10.4lf distL: %10.4lf distP: %10lf\n",
						camid, i, tsd[camid][i],
						pP->x,
						pP->y,
						pP->z,
						distL, distP);
				}
			}
#endif

		}
	} else {
		res = 0;
		goto func_exit;
	}

#ifdef DEBUG_SHOT_RECALC
	{
		double angle;
		angle = cr_vector_angle(
			&shotplane[0].n,
			&shotplane[1].n);

		angle = RADIAN2DEGREE(angle);

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,
			"angle btw shotplane: %lf\n", angle);
	}

#endif

	// 3) get Trajectory line
	res = recalc_shot_trajline(piana, &shotplane[0], &trajline);

	if (res == 0) {
		goto func_exit;
	}

	// 4) get 3D points on the Trajectory line
	for (camid = 0; camid < NUM_CAM; camid++) {
		recalc_shot_3dpoint_trajline(piana, camid, 
				&trajline, 
				&bpL[camid][0],
				&bpvalid[camid][0],
				&bpL3d[camid][0]);
	}

	// 5) calc new vmag, incline, azimuth.. :P
	{
		iana_shotresult_t *psr, *psr2;

		double incline, azimuth;
		double vmag;
		cr_vector_t *pdirection;

		U32 prevvalid;
		double t0, t1;
		double x0, y0, z0;
		double x1, y1, z1;
		double dx, dy, dz, dxy, dxyz;
		double dt;
		U32 count;
		double vv, vsum;

		//---
		psr	= &piana->shotresultdata;
		psr2= &piana->shotresultdata2;


		pdirection = &trajline.m;
		dx = pdirection->x;
		dy = pdirection->y;
		dz = pdirection->z;

		dxy = sqrt(dx*dx + dy*dy);

		azimuth = atan2(dx, dy);
		azimuth = RADIAN2DEGREE(azimuth);
		if (azimuth > +90) {
			azimuth = azimuth - 180;
		}
		if (azimuth < -90) {
			azimuth = azimuth + 180;
		}
		

		incline = atan2(dz, dxy);
		incline = RADIAN2DEGREE(incline);
		if (incline > +90) {
			incline = incline - 180;
		}
		if (incline < -90) {
			incline = incline + 180;
		}
		

		x0 = 0; y0 = 0; z0 = 0; t0 = 0;
		vsum = 0;
		count = 0;
		for (camid = 0; camid < NUM_CAM; camid++) {
			prevvalid = 0;
			for (i = 0; i < marksequencelen; i++) {
				if (bpvalid[camid][i]) {
					x1 = bpL3d[camid][i].x;
					y1 = bpL3d[camid][i].y;
					z1 = bpL3d[camid][i].z;
					t1 = tsd[camid][i];
					if (prevvalid) {
						dx = x1 - x0; dy = y1 - y0; dz = z1 - z0;
						dxyz = sqrt(dx*dx + dy*dy + dz*dz);
						dt = t1 - t0;
						vv = dxyz / dt;
						vmagarray[count] = vv;
						vsum += vv;
						count++;
#ifdef DEBUG_SHOT_RECALC
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d %2d] d : %10.6lf %10.6lf %10.6lf v: %10.6lf (%10.6lf, %10.6lf)\n",
								camid, i, 
								dx, dy, dz,
								vv, vsum, vsum/count);
#endif
					} else {
						prevvalid = 1;
					}

					x0 = x1;
					y0 = y1;
					z0 = z1;
					t0 = t1;
				}
			}
		}
		vmag = vsum / count;

		{	// 20200818.. 
			double dinc;

			dinc = fabs(psr->incline - incline);

#define MAXDINC	15.0 // 5.0, 10.0
			if (dinc > MAXDINC) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "dinc: %lf too big. \n", dinc);
				incline = psr->incline;
				azimuth = psr->azimuth;
			}
		}

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n\n recalc vmag: %lf -> %lf incline: %lf -> %lf   azimuth: %lf -> %lf\n",
				psr->vmag, vmag, 
				psr->incline, incline, 
				psr->azimuth, azimuth);

		qsort( (void *) &vmagarray[0], count, sizeof(double), vmagcompare);
		/*
		for (i = 0; i < (I32)count; i++) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "vmag[%2d] = %lf\n", i, vmagarray[i]);
		}
		*/

		//vmag = vmagarray[count/2];
		vmag = vmagarray[(count*2)/5];
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n\n recalc recalc vmag: -> %lf\n", vmag);


		//if (piana->camsensor_category == CAMSENSOR_Z3) {
		//	incline = psr->incline;					// 20200816.. :P  TODO: re-check incline. :P
		//} 

		if (piana->camsensor_category == CAMSENSOR_Z3) { // 20200901
			double incline2;

			//--
			// incline  0 -> 0
			//          5 -> 6
			//          10 -> 11
			//          20 -> 21
			//          30 -> 30
			//          40 -> 40 ...


			if (incline < 0) {
				incline2 = incline;
			} else if (incline < 5.0) {
				incline2 = LINEAR_SCALE(incline,  0.0,  0.0,  5.0,  6.0);
			} else if (incline < 10.0) {
				incline2 = LINEAR_SCALE(incline,  5.0,  6.0, 10.0, 11.0);
			} else if (incline < 20.0) {
				incline2 = LINEAR_SCALE(incline, 10.0, 11.0, 20.0, 21.0);
			} else if (incline < 30.0) {
				incline2 = LINEAR_SCALE(incline, 20.0, 21.0, 30.0, 30.0);
			} else {
				incline2 = incline;
			}
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n\n incline  %lf -> %lf\n", incline, incline2);

			incline = incline2;
		}

        // Later, We may need to delete this, or need support data, document or some comments to explain this
		if (piana->camsensor_category == CAMSENSOR_EYEXO) { // 20200907
			double deltainc;
			double multinc;
			double z;

			iana_cam_t	*pic;

			//-----
			pic = piana->pic[0];

			//--
			// deltainc  0 -> 0
			//          5 -> 0.5
			//          10 -> 1.0
			//          20 -> 1.0
			//          30 -> 1.0
			//          40 -> 0.0

			// z 0: mult: 0.5
			// z 0.07: mult: 1.0

			if (incline < 0) {
				deltainc = 0;
			}
			else if (incline < 5.0) {
				deltainc = LINEAR_SCALE(incline, 0.0, 0.0, 5.0, 0.5);
			}
			else if (incline < 10.0) {
				deltainc = LINEAR_SCALE(incline, 5.0, 0.5, 10.0, 0.7);
			}
			else if (incline < 20.0) {
				deltainc = LINEAR_SCALE(incline, 10.0, 0.7, 20.0, 0.7);
			}
			else if (incline < 30.0) {
				deltainc = LINEAR_SCALE(incline, 20.0, 0.7, 30.0, 0.7);
			}
			else if (incline < 40.0) {
				deltainc = LINEAR_SCALE(incline, 30.0, 0.7, 40.0, 0.7);
			} else {
				deltainc = 0.7;
			}

			z = pic->icp.L.x;
			if (z < 0) {
				multinc = 0.5;
			}
			else if (z < 0.07) {
				multinc = LINEAR_SCALE(z, 0.0, 0.5, 0.07, 1.0);
			} else {
				multinc = 1.0;
			}

			deltainc = deltainc * multinc;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n\n incline  %lf -> %lf\n", incline, incline + deltainc);

			incline = incline + deltainc;
		}
		
		psr->vmag = vmag;
		psr->incline = incline;
		psr->azimuth = azimuth;

		psr2->vmag = vmag;
		psr2->incline = incline;
		psr2->azimuth = azimuth;

	}

	res = 1;
func_exit:
	return res;
}

#if defined (__cplusplus)
}
#endif



