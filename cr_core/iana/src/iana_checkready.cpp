/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA check ready
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_checkready.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2016/02/21 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <math.h>
#include <string.h>

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"
#include "cr_nanoxml.h"

#include "scamif.h"
//#include "scamif_main.h"
#include "scamif_buffer.h"

#include "iana.h"
#include "iana_work.h"
#include "iana_implement.h"
#include "iana_tool.h"
#include "iana_coordinate.h"
#include "iana_cam.h"
#include "iana_shot.h"
#include "iana_configure.h"

//#include "iana_cam_implement.h"
#include "iana_checkready.h"
#include "iana_balldetect.h"

#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#else
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#endif

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/

I32 iana_checkready_ball(iana_t *piana, U32 camid, iana_ballcandidate_t bc[MAXCANDIDATE], U32 *pbcount, U08 **pbuf);
I32 calc_circle_mean_stddev(iana_ballcandidate_t *pbc, U08 *pimgbuf, U32 width, U32 height);
I32 refineBunkerRoughArea(iana_t *piana, U32 camid, U08 *pbuf, cr_rect_t rectArea, double xfact, double yfact, I32 width, I32 height);
I32 iana_run_start_LITE(iana_t *piana);


static void GetReadyBallInfo(iana_t *piana, U32 bcount[2], iana_ballcandidate_t bc[2][MAXCANDIDATE])
{
	U32 i0, i1;
	//U32 iter;
//	ballcandidatepair_t	bp[MAXCANDIDATE *MAXCANDIDATE ];
	iana_ballcandidate_t *pbc0, *pbc1;

	cr_point_t p0, p1;
	cr_point_t CamPos0, CamPos1;
	cr_line_t line0, line1;
	cr_point_t ballpos0, ballpos1;

	U32 paircount;
	U32 camidCheckReady;
	U32 camidOther;	

	ballcandidatepair_t	bp[MAXCANDIDATE *MAXCANDIDATE ];

	iana_veteran_t *pvtr[2];

	camidCheckReady = piana->camidCheckReady;
	camidOther = piana->camidOther;		

	//---
	memset(&bp[0], 0, sizeof(ballcandidatepair_t) * MAXCANDIDATE);
	paircount = 0;

	memcpy(&CamPos0, &piana->pic[camidCheckReady]->icp.CamPosL, sizeof(cr_point_t));
	memcpy(&CamPos1, &piana->pic[camidOther]->icp.CamPosL, sizeof(cr_point_t));

	for (i0 = 0; i0 < bcount[camidCheckReady]; i0++) {
		if (bc[camidCheckReady][i0].cexist == IANA_BALL_EXIST) {
			for (i1 = 0; i1 < bcount[camidOther]; i1++) {
				if (bc[camidOther][i1].cexist == IANA_BALL_EXIST) {
					int checkzposgood;
					double dist, dx, dy, dz;
					//--
			//			cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "(%d:%d), (%d:%d) exist\n",
			//					piana->camidCheckReady, i0,
			//					piana->camidOther, i1);
					bp[paircount].index[camidCheckReady] = i0;
					bp[paircount].index[camidOther] = i1;

					pbc0 = &bc[camidCheckReady][i0];
					pbc1 = &bc[camidOther][i1];

					memcpy(&bp[paircount].bc[camidCheckReady], pbc0, sizeof(iana_ballcandidate_t));
					memcpy(&bp[paircount].bc[camidOther], pbc1, sizeof(iana_ballcandidate_t));

					p0.x = pbc0->L.x; p0.y = pbc0->L.y; p0.z = 0;
					p1.x = pbc1->L.x; p1.y = pbc1->L.y; p1.z = 0;

					cr_line_p0p1(&p0, &CamPos0, &line0);
					cr_line_p0p1(&p1, &CamPos1, &line1);
						
					cr_point_line_line(&line0, &line1, &ballpos0, &ballpos1);

					dx = ballpos0.x - ballpos1.x;
					dy = ballpos0.y - ballpos1.y;
					dz = ballpos0.z - ballpos1.z;
					dist = sqrt(dx*dx+dy*dy+dz*dz);
						
					cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
								"<%2d %2d> C0: %8lf %8lf %8lf (%8lf %8lf) C1:%8lf %8lf %8lf (%8lf %8lf)   (Dist %8lf, %8lf %8lf %8lf)\n",
								i0, i1,
								ballpos0.x, ballpos0.y, ballpos0.z, p0.x, p0.y,
								ballpos1.x, ballpos1.y, ballpos1.z, p1.x, p1.y,
								dist, dx, dy, dz
								);

					//----
					checkzposgood = 1;
					{	// Check Z position range
						FILE *fp;
						int mode;
						double minz, maxz;

							//---
#define BALL_MINZ_Z3	(-0.05)
#define BALL_MAXZ_Z3	( 0.15)

#define BALL_MINZ	(-0.10)
#define BALL_MAXZ	( 0.20)

						mode = 1;
						//mode = 0;
						if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
							minz = BALL_MINZ_Z3;
							maxz = BALL_MAXZ_Z3;
						} else {
							minz = BALL_MINZ;
							maxz = BALL_MAXZ;
						}

						fp = cr_fopen("ballz.txt", "r");

						if (fp) {
							int ret;
							double dtmp;
							ret = fscanf(fp, "%d", &mode);
							fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
							if (ret == 1 && mode == 1) {
								//--
								// MIN Z value
								ret = fscanf(fp, "%lf", &dtmp);
								fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
								if (ret == 1) {
									minz = dtmp;

									// MAX Z Value
									ret = fscanf(fp, "%lf", &dtmp);
									fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
									if (ret == 1) {
										maxz = dtmp;
									}
								}
							}
							cr_fclose(fp);
							//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mode: %d  minz: %lf maxz: %lf\n", mode, minz, maxz);
						}
			
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ballpos %lf %lf %lf \n", 
									ballpos0.x,
									ballpos0.y,
									ballpos0.z);
						if (mode == 1) {
							// Check Z position range..
							if (ballpos0.z < minz) {
								cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "minz, ballpos z %lf < %lf \n", ballpos0.z, minz);
								checkzposgood = 0;
							} else if (ballpos0.z > maxz) {
								cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "maxz, ballpos z %lf > %lf \n", ballpos0.z, maxz);
								checkzposgood = 0;
							}
						}
					}
					//----

//#define MINDIST_BALLEXIST	0.005
//#define MINDIST_BALLEXIST	0.01
//#define MINDIST_BALLEXIST	0.020
#define MINDIST_BALLEXIST	0.025
#define MINDIST_BALLEXIST_P3V2	0.05

					{
						double mindist_ballexist;

						if (piana->camsensor_category == CAMSENSOR_P3V2) {
							mindist_ballexist = MINDIST_BALLEXIST_P3V2;
						} else {
							mindist_ballexist = MINDIST_BALLEXIST;
						}
						if (dist < mindist_ballexist && checkzposgood) {
							U32 checkgood;
//#define CHECK_BALL_RADIUS
#if defined(CHECK_BALL_RADIUS)
							double Pr0, Pr1;
							double Pr0_, Pr1_;
							double Lr0_, Lr1_;
							double ratio0, ratio1;							
						
							Pr0 = pbc0->cr;
							Pr1 = pbc1->cr;
					
							iana_ballradius(piana, camidCheckReady, &ballpos0, &Pr0_, &Lr0_);
							iana_ballradius(piana, camidOther, &ballpos0, &Pr1_, &Lr1_);
							
							ratio0 = Pr0 / Pr0_;
							ratio1 = Pr1 / Pr1_;
							cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" Pr0: %lf Pr0_: %lf ratio0: %lf\n", Pr0, Pr0_, ratio0);
							cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" Pr1: %lf Pr1_: %lf ratio1: %lf\n", Pr1, Pr1_, ratio1);
#define RATIO_MIN	0.9
#define RATIO_MAX	1.5
							checkgood = 0;
							if (ratio0 > RATIO_MIN && ratio0 < RATIO_MAX) {
								if (ratio1 >  RATIO_MIN && ratio1 < RATIO_MAX) {
									checkgood = 1;
								}
							}
#else
							checkgood = 1;
#endif
							if (checkgood) {
								bp[paircount].state = IANA_BALL_EXIST;
								memcpy(&bp[paircount].b3d, &ballpos0, sizeof(cr_point_t));
								cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "paircount: %d with dist: %lf\n\n", paircount, dist);
								paircount++;
							}
						}
					}
					if (paircount >= MAXCANDIDATE *MAXCANDIDATE) {
						break;		// for (i1 = 0; i1 < bcount[camidOther]; i1++) 
					}
				}
			} 	// for (i1 = 0; i1 < bcount[camidOther]; i1++) 
		}
		if (paircount >= MAXCANDIDATE *MAXCANDIDATE) {
			break;
		}
	}	//  for (i0 = 0; i0 < bcount[camidCheckReady]; i0++) 
		//--

	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "TOTAL paircount: %d\n", paircount);


	{
		//		U32	shotarea;			// Primary shot area.
		U32 i, j;
		U32 isInTeeflag, isInIronflag, isInPutterflag;
		double rlx, rly;				// reference Point
		U32 useReference;
		//		shotarea = piana->shotarea;

		isInTeeflag 	= 0;
		isInIronflag 	= 0;
		isInPutterflag 	= 0;

		for (i = IANA_AREA_TEE; i <= IANA_AREA_PUTTER; i++) {
			// Get nearest ball position
			double mindist;
			double dist;
			I32 goodIndex;

			//pvtr = &piana->ic[camid].vtr[i];
			pvtr[camidCheckReady] 	= &piana->pic[camidCheckReady]->vtr[i];
			pvtr[camidOther] 		= &piana->pic[camidOther]->vtr[i];

			rlx = 0; rly = 0;
			if (pvtr[camidCheckReady]->state == IANA_VETERAN_APPEAR || pvtr[camidCheckReady]->state == IANA_VETERAN_EXIST) {
				rlx = pvtr[camidCheckReady]->b3d.x;
				rly = pvtr[camidCheckReady]->b3d.y;
				useReference = 1;
			} else if (piana->pic[camidCheckReady]->vtrPrev[i].state == IANA_VETERAN_EXIST) {
//				rlx =  pic->vtrPrev[i].L.x;
//				rly =  pic->vtrPrev[i].L.y;
				rlx = piana->pic[camidCheckReady]->vtrPrev[i].b3d.x;
				rly = piana->pic[camidCheckReady]->vtrPrev[i].b3d.y;
				useReference = 1;
			} else {
				useReference = 0;
			}

			if (useReference) {
				mindist = 1.0e10;
				goodIndex = -1;
				for (j = 0; j < paircount; j++) {
					ballcandidatepair_t	*pbp;
					pbp =  &bp[j];
					//isInTeeflag 	= iana_isInTee(piana, pbp->b3d.x, pbp->b3d.y);
					//isInIronflag 	= iana_isInIron(piana, pbp->b3d.x, pbp->b3d.y);
					//isInPutterflag 	= iana_isInPutter(piana, pbp->b3d.x, pbp->b3d.y);

					isInTeeflag = iana_isInTee(piana, &pbp->b3d);
					isInIronflag = iana_isInIron(piana, &pbp->b3d);
					isInPutterflag = iana_isInPutter(piana, &pbp->b3d);

					//----
					if ( (isInTeeflag && i != IANA_AREA_TEE)	|| (!isInTeeflag && i == IANA_AREA_TEE)) {
						continue;
					}

					if (i == IANA_AREA_IRON && isInIronflag == 0) {
						continue;
					}

					if (i == IANA_AREA_PUTTER && isInPutterflag == 0) {
						continue;
					}

					dist = DDIST(rlx, rly, pbp->b3d.x, pbp->b3d.y);
					if (mindist > dist) {
						mindist = dist;
						goodIndex = j;
					}
				}
//					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" mindist: %lf  goodIndex: %d\n", mindist, goodIndex);
//#define MAXDIST4READY	0.01
#define MAXDIST4READY	0.05
				if (mindist < MAXDIST4READY) {	// Match.. 
//					iana_ballcandidate_t *pbc0, *pbc1;
					pbc0 =&bp[goodIndex].bc[camidCheckReady];
					pbc1 =&bp[goodIndex].bc[camidOther];
							
					memcpy(&pvtr[camidCheckReady]->P,	&pbc0->P, sizeof(cr_point_t));
					pvtr[camidCheckReady]->cr   	= pbc0->cr;
					pvtr[camidCheckReady]->cer   	= pbc0->cer;
					memcpy(&pvtr[camidCheckReady]->L,	&pbc0->L, sizeof(cr_point_t));
					pvtr[camidCheckReady]->lr 		= pbc0->lr;
					memcpy(&pvtr[camidCheckReady]->b3d, &bp[goodIndex].b3d, sizeof(cr_point_t));

					pvtr[camidCheckReady]->mean 	= pbc0->mean;
					pvtr[camidCheckReady]->stddev	= pbc0->stddev;
					pvtr[camidCheckReady]->meanH 	= pbc0->meanH;
					pvtr[camidCheckReady]->stddevH	= pbc0->stddevH;

					memcpy(&pvtr[camidOther]->P, 	&pbc1->P, sizeof(cr_point_t));
					pvtr[camidOther]->cr   			= pbc1->cr;
					pvtr[camidOther]->cer   		= pbc1->cer;
					memcpy(&pvtr[camidOther]->L,	&pbc1->L, sizeof(cr_point_t));
					pvtr[camidOther]->lr 			= pbc1->lr;
					memcpy(&pvtr[camidOther]->b3d, &bp[goodIndex].b3d, sizeof(cr_point_t));
					
					pvtr[camidOther]->mean 			= pbc1->mean;
					pvtr[camidOther]->stddev 		= pbc1->stddev;
					pvtr[camidOther]->meanH 		= pbc1->meanH;
					pvtr[camidOther]->stddevH 		= pbc1->stddevH;


					if (pvtr[camidCheckReady]->state == IANA_VETERAN_APPEAR || pvtr[camidCheckReady]->state == IANA_VETERAN_EXIST) {
						pvtr[camidCheckReady]->count++;
					} else {
						pvtr[camidCheckReady]->state = IANA_VETERAN_APPEAR;
						pvtr[camidCheckReady]->count = 1;
					}
#define PROMOTION2EXIST	2
					if (pvtr[camidCheckReady]->count >= PROMOTION2EXIST) {
						pvtr[camidCheckReady]->state = IANA_VETERAN_EXIST;
					}

					if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
						pvtr[camidCheckReady]->state = IANA_VETERAN_EXIST;
					}
				} else {
					pvtr[camidCheckReady]->state = IANA_VETERAN_EMPTY;
					pvtr[camidCheckReady]->count = 0;

					piana->pic[camidCheckReady]->vtrPrev[i].state = IANA_VETERAN_EMPTY;
					piana->pic[camidCheckReady]->vtrPrev[i].count = 0;
				}
			} else { // useReference == 0
				goodIndex = -1;
				for (j = 0; j < paircount; j++) {
					ballcandidatepair_t	*pbp;
					pbp =  &bp[j];
					//isInTeeflag 	= iana_isInTee(piana, pbp->b3d.x, pbp->b3d.y);
					//isInIronflag 	= iana_isInIron(piana, pbp->b3d.x, pbp->b3d.y);
					//isInPutterflag 	= iana_isInPutter(piana, pbp->b3d.x, pbp->b3d.y);

					isInTeeflag = iana_isInTee(piana, &pbp->b3d);
					isInIronflag = iana_isInIron(piana, &pbp->b3d);
					isInPutterflag = iana_isInPutter(piana, &pbp->b3d);


					if (i == IANA_AREA_TEE) {
						if (isInTeeflag) {
							goodIndex = j;
							break;
						}
					} else if (i == IANA_AREA_IRON) {
						if (!isInTeeflag && isInIronflag) {
							goodIndex = j;
						}
					} else if (i == IANA_AREA_PUTTER) {
						if (!isInTeeflag && isInPutterflag) {
							goodIndex = j;
						}
					}
				}
				if (goodIndex >= 0) {					// Found new ball
//					iana_ballcandidate_t *pbc0, *pbc1;
					pbc0 =&bp[goodIndex].bc[camidCheckReady];
					pbc1 =&bp[goodIndex].bc[camidOther];					
					
					memcpy(&pvtr[camidCheckReady]->P,	&pbc0->P, sizeof(cr_point_t));
					pvtr[camidCheckReady]->cr   	= pbc0->cr;
					pvtr[camidCheckReady]->cer   	= pbc0->cer;
					memcpy(&pvtr[camidCheckReady]->L,	&pbc0->L, sizeof(cr_point_t));
					pvtr[camidCheckReady]->lr 		= pbc0->lr;
					memcpy(&pvtr[camidCheckReady]->b3d, &bp[goodIndex].b3d, sizeof(cr_point_t));

					memcpy(&pvtr[camidOther]->P, 	&pbc1->P, sizeof(cr_point_t));
					pvtr[camidOther]->cr   			= pbc1->cr;
					pvtr[camidOther]->cer   		= pbc1->cer;
					memcpy(&pvtr[camidOther]->L,	&pbc1->L, sizeof(cr_point_t));
					pvtr[camidOther]->lr 			= pbc1->lr;
					memcpy(&pvtr[camidOther]->b3d, &bp[goodIndex].b3d, sizeof(cr_point_t));
						
					pvtr[camidCheckReady]->state = IANA_VETERAN_APPEAR;
					pvtr[camidCheckReady]->count = 1;
				} else {								// No ball at all. -_-;
					pvtr[camidCheckReady]->state = IANA_VETERAN_EMPTY;
					pvtr[camidCheckReady]->count = 0;
				}
			}
		} 	// for (i = IANA_AREA_TEE; i <= IANA_AREA_PUTTER; i++) 
	}
}

	
	
static void ChangeState2Ready(iana_t *piana)
{
	U32 processarea;
	U32 shotarea;
	U32 allowTee;
	U32 allowIron;
	U32 allowPutter;
	iana_veteran_t *pvtr;
	U32 camidCheckReady;
	U32 camidOther;

	//--
	allowTee 	= piana->allowedarea[IANA_AREA_TEE];
	allowIron 	= piana->allowedarea[IANA_AREA_IRON];
	allowPutter	= piana->allowedarea[IANA_AREA_PUTTER];

	camidCheckReady = piana->camidCheckReady;
	camidOther = piana->camidOther;	

#if defined(USE_SHOTAREARUNMODE)
	if (piana->shotarearunmode == 1) {
		if (allowTee || allowIron) {
			allowTee = 1;
			allowIron = 1;
		}
	}
#endif
	processarea = IANA_AREA_NULL;
	shotarea    = piana->shotarea;				// 

	if (shotarea >= IANA_AREA_TEE && shotarea <= IANA_AREA_PUTTER) {
		if (piana->camsensor_category == CAMSENSOR_P3V2) {
			if (piana->pic[camidCheckReady]->vtr[shotarea].state == IANA_VETERAN_EXIST) {
				processarea = shotarea;
			}
		
			if (shotarea == IANA_AREA_TEE || shotarea == IANA_AREA_IRON) {				
				if (allowIron && allowTee) {
					if (piana->pic[camidCheckReady]->vtr[IANA_AREA_IRON].state == IANA_VETERAN_EXIST) {	// Check Iron area
						processarea = IANA_AREA_IRON;			// I prefer IRON 
					} else if (piana->pic[camidCheckReady]->vtr[IANA_AREA_TEE].state == IANA_VETERAN_EXIST) {	// Check TEE area
						processarea = IANA_AREA_TEE;
					}
				}
			}
		} else {
			if (shotarea == IANA_AREA_PUTTER) {
				processarea = shotarea; 
			} else {
				if (piana->pic[camidCheckReady]->vtr[shotarea].state == IANA_VETERAN_EXIST) {
					processarea = shotarea;
				}
				else {
					if (shotarea == IANA_AREA_TEE) {
						if (allowIron && piana->pic[camidCheckReady]->vtr[IANA_AREA_IRON].state == IANA_VETERAN_EXIST) {	// Check Iron area
							processarea = IANA_AREA_IRON;
						}
					}
					if (shotarea == IANA_AREA_IRON) {
						if (allowTee && piana->pic[camidCheckReady]->vtr[IANA_AREA_TEE].state == IANA_VETERAN_EXIST) { // Check Iron area
							processarea = IANA_AREA_TEE;
						}
					}
				}
			}
		}

			if (processarea >= IANA_AREA_TEE && processarea <= IANA_AREA_PUTTER) {
				U32 refsize;
				cr_rect_t *prect;
				U08 *ptr;
				//U32 i;
				I32 i;

				//-- Update ready ball position and size
				pvtr = NULL;
				for (i = 0; i < NUM_CAM; i++) {
					pvtr = &piana->pic[i]->vtr[processarea];
					memcpy(&piana->pic[i]->icp.P, &pvtr->P, sizeof(cr_point_t));
					piana->pic[i]->icp.cr = pvtr->cr;
					memcpy(&piana->pic[i]->icp.L, &pvtr->L, sizeof(cr_point_t));
					memcpy(&piana->pic[i]->icp.b3d, &pvtr->b3d, sizeof(cr_point_t));

					piana->pic[i]->icp.mean = pvtr->mean;
					piana->pic[i]->icp.stddev = pvtr->stddev;
					piana->pic[i]->icp.meanH = pvtr->meanH;
					piana->pic[i]->icp.stddevH = pvtr->stddevH;

				}

				pvtr = &piana->pic[camidCheckReady]->vtr[processarea];
				//--
				{
					refsize = (U32) (pvtr->cr * 2 * 1.1);			// 10%.. 
					refsize = ((refsize + 3) / 4) * 4;				// Mutiple number of 4
					if (refsize > BALLIMAGE_REF_SIZE) {
						refsize = BALLIMAGE_REF_SIZE;
					}
					cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "refsize: %d vs %d\n", refsize, BALLIMAGE_REF_SIZE);

					prect = &piana->pic[camidCheckReady]->rectBallImg;

					prect->left  	= (long) (pvtr->P.x - refsize/2);
					prect->right  	= prect->left + refsize - 1;

					prect->top  	= (long) (pvtr->P.y - refsize/2);
					//prect->bottom 	= (long) (pvtr->P.y + refsize/2);
					prect->bottom 	= prect->top + refsize - 1;
					if (prect->left < 0) { prect->left = 0; }
					if (prect->right > FULLWIDTH-1) { prect->right = FULLWIDTH-1; }
					if (prect->top < 0) { prect->top = 0; }
					if (prect->bottom > FULLHEIGHT-1) { prect->right = FULLHEIGHT-1; }

					// Backup ball image
					ptr = &piana->pic[camidCheckReady]->BallImg[0];
					memset(ptr, 0, sizeof(U08) * BALLIMAGE_REF_SIZE * BALLIMAGE_REF_SIZE);
					piana->pic[camidCheckReady]->bih = piana->pic[camidCheckReady]->biw = refsize;
					//for (i = prect->top; i <= (U32)prect->bottom; i++) 
					//for (i = prect->top; i < (U32)prect->bottom; i++) 
#if 1
					for (i = prect->top; i < (I32)prect->bottom; i++) 
					{
						//memcpy(ptr, buf[camidCheckReady] + i * FULLWIDTH + prect->left, sizeof(U08) * refsize);
						memset(ptr, 0, sizeof(U08) * refsize);
						ptr += refsize*sizeof(U08);
					}
#endif
				}

				piana->readygoodcount++;

				cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "readygoodcount: %d/%d\n", piana->readygoodcount,piana->readygoodcountGOOD);

#if 1
				{
					U32 readygoodcountGOOD;

					if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
						if (processarea == IANA_AREA_TEE) {
#define READYGOODCOUNTGOOD_ADD_TEE	0
//#define READYGOODCOUNTGOOD_ADD_TEE	3
							readygoodcountGOOD = piana->readygoodcountGOOD + READYGOODCOUNTGOOD_ADD_TEE;
						} else {
							readygoodcountGOOD = piana->readygoodcountGOOD;
						}
					} else {
						readygoodcountGOOD = piana->readygoodcountGOOD;
					}
					if (piana->readygoodcount >= readygoodcountGOOD) {
						// 2) Update state
						piana->runstate	= IANA_RUN_READY;
						piana->processarea = processarea;
						piana->readygoodcount = 0;
						piana->readystarttick = cr_gettickcount();

						piana->updategainCount = 0;
						piana->updategainTick = cr_gettickcount();

						//#define TOOL_IMG_3
#if defined(TOOL_IMG_3)
						if (piana->himgfunc) {
							((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)&pic[camidCheckReady]->BallImg[0], refsize, refsize, 3 /*4*/, 1, 2, 3, piana->imguserparam);
						}
						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Ready. processarea: %d\n", processarea);
#endif
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Ready. processarea: %d\n", processarea);
					}
				}
#endif


			} else {
				piana->readygoodcount = 0;
				//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d readygoodcount ZERO.\n", __LINE__);  // HERE... 
			}
		} else {
			piana->readygoodcount = 0;
			//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" %d readygoodcount ZERO.\n", __LINE__);

		}
	}


/*!
 ********************************************************************************
 *	@brief      CAM check ball ready
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		Terminate result. 
 *
 *	@author	    yhsuk
 *  @date       2015/12/20
 *******************************************************************************/
I32 iana_checkready(iana_t *piana)	
{
	I32 res;
	iana_cam_t	*pic[MAXCAMCOUNT];

	U08 *buf[MAXCAMCOUNT];
	U32 camidCheckReady;
	U32 camidOther;

	iana_ballcandidate_t bc[MAXCAMCOUNT][MAXCANDIDATE];	

	U32 bcount[MAXCAMCOUNT];
	U32 m;

	point_t Lp0;			// ball position of CheckReady cam.

	U32 i;

	//--------------------------------------------------------------------
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with hiana: %p\n", (U32) piana);

	if (piana->testmode == 1) {		// FAKE RUN..
	// 2) Update state
		piana->runstate = IANA_RUN_READY;
		piana->processarea = 1;
		piana->readygoodcount = 0;
		piana->readystarttick = cr_gettickcount();

		piana->updategainCount = 0;
		piana->updategainTick = cr_gettickcount();
		res = 1;
		goto func_exit;
	}

//	Pp0.x = Pp0.y = 0;
	Lp0.x = Lp0.y = 0;
	m = 0;
	//--

	camidCheckReady = piana->camidCheckReady;
	camidOther = piana->camidOther;

	for (i = 0; i < NUM_CAM; i++) {
		bcount[i] = 0; 
		pic[i] = piana->pic[i];
		bcount[i] = 0;
		buf[i] = NULL;
	}

	res = iana_checkready_ball(piana, camidCheckReady, bc[camidCheckReady], &bcount[camidCheckReady], &buf[camidCheckReady]);
//	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]res: %d buf[camidCheckReady]: %08x\n", camidCheckReady, res, (U32)buf[camidCheckReady]);
	if (res == 0 || buf[camidCheckReady] == NULL) {
//		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]res: %d buf[camidCheckReady]: %08x\n", camidCheckReady, res, (U32)buf[camidCheckReady]);
		goto func_exit;
	}

	if (bcount[camidCheckReady] == 0) {
//		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bcount = 0\n", camidCheckReady, bcount[camidCheckReady]);
		goto func_exit;
	}


	res = iana_checkready_ball(piana, camidOther, bc[camidOther], &bcount[camidOther], &buf[camidOther]);
	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		if (bcount[camidOther] > 1) {
			I32 ii;
			double minPy;
			I32 minPyi;

			minPy = 99999;
			minPyi = -999;
			for (ii = 0; ii < MAXCANDIDATE; ii++) {
				if (bc[camidOther][ii].cexist == IANA_BALL_EXIST) {
					if (minPy > bc[camidOther][ii].P.y) {
						minPy = bc[camidOther][ii].P.y;
						minPyi = ii;
					}
				}
			}

			if (minPyi >= 0) {
				for (ii = 0; ii < MAXCANDIDATE; ii++) {
					if (bc[camidOther][ii].cexist == IANA_BALL_EXIST) {
						if (minPyi != ii) {
							bc[camidOther][ii].cexist = IANA_BALL_EMPTY;
						}
					}
				}
			}
		}
	}

	//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"(%d) %d count: %d,   %d count: %d\n", __LINE__,
	//		camidCheckReady, bcount[camidCheckReady], camidOther, bcount[camidOther]);

	if (bcount[camidCheckReady] != 0 && bcount[camidOther] == 0) {
		//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bcount = 0\n", camidOther, bcount[camidOther]);
		res = 1;
		goto func_exit;
	}

	GetReadyBallInfo(piana, bcount, bc);

	// 5) Check and change To Ready state
	ChangeState2Ready(piana);

	if (piana->testmode == 1) {		// FAKE RUN..
		// 2) Update state
		piana->runstate = IANA_RUN_READY;
		piana->processarea = 1;
		piana->readygoodcount = 0;
		piana->readystarttick = cr_gettickcount();

		piana->updategainCount = 0;
		piana->updategainTick = cr_gettickcount();
	}

	res = 1;
func_exit:
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res %d\n", res);
	return res;
}

I32 iana_checkready_ball(iana_t *piana, U32 camid, iana_ballcandidate_t bc[MAXCANDIDATE], U32 *pbcount, U08 **pbuf)
{
#ifdef IPP_SUPPORT
	I32 res;
	camif_buffer_info_t *pb;
	iana_cam_t	*pic;
	camimageinfo_t	*pcii;
	U32 multitude;

	U08 *buf;
	U08 *procimg;
	U32 rindex;

	IppiSize ssize;
	IppiRect sroi, droi;
	int sstep, dstep;
	IppStatus status;
	U32 bcount;

	double	xfact, yfact; 
	cr_rect_t rectArea;


	U32 goodkind;
	U32 m;

	U32 width, height;
	U32 ii;

//	point_t Pp0, Lp0;			// ball position of CheckReady cam.

	//--
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with hiana: %p\n", (U32) piana);

	//--
	// 1) get image
	pic   =piana->pic[camid];
	buf = NULL;
	pcii = NULL;
	width = 1280;
	height = 1024;

	res = scamif_imagebuf_peek(piana->hscamif, camid, NORMALBULK_NORMAL, NULL, NULL, NULL);
	if (res) {		// If there is any available frame.
		res = scamif_imagebuf_read_latest(piana->hscamif, camid, NORMALBULK_NORMAL, &pb, &buf, &rindex);	
		piana->lastrindex[camid] = rindex;
		pcii = &pb->cii;
		width = pcii->width;
		height = pcii->height;

		//if (piana->himgfunc) {
		//	((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf, width, height, 3, 1, 2, 3, piana->imguserparam); 
		//}
	}


	if (res == 0 || pcii == NULL) {	// No image data yet.
	//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d, pcii: 0x%08x\n", res, (int) pcii);
		goto func_exit;
	}

	multitude 	= pcii->multitude;
	if (multitude == 0) {
		multitude = 1;
	}

	m = 0;

	procimg = pic->pimgFull;

	// 2) Resize to 0.5x0.5         (1280x1024 to 640x512)
	{
		double	xshift, yshift; 
		Ipp8u 	*resizebuf;
		int 	bufsize;

		//--
		//memcpy(&rectArea, &piana->ic[camid].icp.rectArea, sizeof(cr_rect_t));
#if defined(V2000_TEST)
		memcpy(&rectArea, &pic->icp.rectAreaExt, sizeof(cr_rect_t));				// 2018 11/22.. 
//		memcpy(&rectArea, &pic->icp.rectArea, sizeof(cr_rect_t));
#else
		memcpy(&rectArea, &pic->icp.rectAreaExt, sizeof(cr_rect_t));
//		memcpy(&rectArea, &pic->icp.rectArea, sizeof(cr_rect_t));
#endif


		xfact = 0.5;
		yfact = (0.5*multitude);

		iana_setReadyRunBallsizeMinMax(piana, camid, xfact);

		//		xfact 		= 0.5;
		//		yfact 		= 0.5;
		xshift 		= 0;
		yshift 		= 0;

		ssize.width = FULLWIDTH;
		ssize.height = FULLHEIGHT / multitude;

		//sroi.x 		= rectArea.left;
		//sroi.y 		= rectArea.top;

		sroi.x 		= 0;
		sroi.y 		= 0;
		sroi.width 	= (rectArea.right - rectArea.left + 1);
		sroi.height	= (rectArea.bottom   - rectArea.top  + 1) / multitude;

		////cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__" rectArea: (%d,%d) ~ (%d,%d)\n", rectArea.left, rectArea.top, rectArea.right, rectArea.top);
		//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" rectArea: (%d,%d) ~ (%d,%d)\n", rectArea.left, rectArea.top, rectArea.right, rectArea.bottom);
		//droi.x 		= rectArea.left * xfact;
		//droi.y 		= rectArea.top * yfact;

		droi.x 		= 0;
		droi.y 		= 0;
		droi.width 	= (int) (sroi.width * xfact + 0.001);
		droi.height	= (int) (sroi.height * yfact + 0.001);

		if (sroi.width  > FULLWIDTH || sroi.height > FULLHEIGHT) {
			res = 0;
			goto func_exit;
			//break;
		}

		sstep 		= FULLWIDTH;
		dstep		= droi.width;

		ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_CUBIC, &bufsize);
		resizebuf = (U08 *)malloc(bufsize);
		status = ippiResizeSqrPixel_8u_C1R(
				(Ipp8u *)buf + rectArea.left + ((rectArea.top / multitude) * ssize.width)  + ssize.width * ssize.height * m,
				ssize,
				sstep,
				sroi,
				//
				(Ipp8u*) procimg,
				dstep,
				droi,
				//
				xfact,
				yfact,
				xshift,
				yshift,
				IPPI_INTER_CUBIC,
				resizebuf
				);
		free(resizebuf);
#if defined(ROUGHBUNKER_COMPENSATION)
		if (piana->camsensor_category == CAMSENSOR_P3V2) {
			if (camid == 0) {
				refineBunkerRoughArea(piana, camid, procimg, rectArea, xfact, yfact, droi.width, droi.height);
			}

		}
#endif   //defined(ROUGHBUNKER_COMPENSATION)

		if (piana->himgfunc) {
			((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)(
				(char *)procimg, droi.width, droi.height, 3 /*0*/, 1, 2, 3, piana->imguserparam);
		}

	}

	// 3) Get Ball positions, if any.
	{
		double cermax;
		double crmin;
		double crmax;
		double crminratio;
		double crmaxratio;
		//U32 i;

		U32 checkready_multv;
		U32 checkready_expn;
		double thstdmult;

		//---------
		iana_setReadyRunBallsizeMinMax(piana, camid, xfact);

		crmaxratio = IANA_BALLDETECT_GetMaxCircleRatio(piana->camsensor_category, camid);
		crminratio = IANA_BALLDETECT_GetMinCircleRatio(piana->camsensor_category, camid);	

		crmin = (piana->pic[camid]->icp.RunBallsizePMin / 2.0) * crminratio;
		crmax = (piana->pic[camid]->icp.RunBallsizePMax / 2.0) * crmaxratio;


#define BALLSIZEMAXMULT	1.2
		crmax *= BALLSIZEMAXMULT;
		//cermax = piana->ic[camid].icp.CerBallexist;
		cermax = pic->icp.CerBallexist;


		memset(bc, 0, sizeof(bc));
		bcount = 0;
		// get Candidate
#define CHECKREADY_MAXTH	40
		//#define CHECKREADY_MINTH	20
#define CHECKREADY_MINTH	30

#define CHECKREADY_MULTV	3
#define CHECKREADY_EXPN		1				
		//  -> 3 * 2^-1 = 1.5

		//#define CHECKREADY_MULTV	2
		//#define CHECKREADY_EXPN		0				
		//  -> 2
#if 0
		iana_getballcandidator(piana, camid, procimg, droi.width, droi.height, droi.width, &bc[0], &bcount, 
				CHECKREADY_MAXTH, CHECKREADY_MINTH, CHECKREADY_MULTV, CHECKREADY_EXPN, cermax);
#endif

#if 1
#define HALFWINSIZE_CHECKREADY	16

		//#define THSTDMULT_		0.5
		//#define THSTDMULT_		0.7
		//#define THSTDMULT_		1.0
		//#define THSTDMULT_		1.5
#define THSTDMULT_		2.0
		//g_zzz = 1;
		//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[1-2] m: %lf\n", piana->ic[0].icp.bAp2l.mat[0]);		// CHECKME

//#define CHECKREADY_MULTV_Z3		1
//#define CHECKREADY_EXPN_Z3		0				
//#define THSTDMULT__Z3			1.0

		
#define CHECKREADY_MULTV_Z3		3
#define CHECKREADY_EXPN_Z3		2				
#define THSTDMULT__Z3			1.5

#define CUTMEAN_CHECKREADY	0.5   // 20190420, yhsuk
		if (piana->camsensor_category == CAMSENSOR_Z3||piana->camsensor_category == CAMSENSOR_EYEXO) {
//#define THSTMULT_COUNT	5
#define THSTMULT_COUNT	2
			//U32 ii;
			U32 thstmult_count;

//			double thstmult_[THSTMULT_COUNT] 		= {2.0, 1.5, 1.0, 0.8, 0.4}; 
			//double thstmult_PUTTER_[THSTMULT_COUNT] = {2.0, 1.5, 1.0, 0.8, 0.4}; 
//			double thstmult_PUTTER_[THSTMULT_COUNT] = {1.5, 1.0, 0.5, 0.3, 0.1}; 

			//double thstmult_[THSTMULT_COUNT] 		= {2.0, 1.5};
			//double thstmult_PUTTER_[THSTMULT_COUNT] = {2.0, 1.5};
			double thstmult_[THSTMULT_COUNT] 		= {0.8, 1.5};
			double thstmult_PUTTER_[THSTMULT_COUNT] = {0.8, 1.5};


			//--
			checkready_multv = CHECKREADY_MULTV_Z3;
			checkready_expn = CHECKREADY_EXPN_Z3;
			thstmult_count = THSTMULT_COUNT;
			for (ii = 0; ii < (int)thstmult_count; ii++) {
				if (piana->processarea == IANA_AREA_PUTTER) {
					thstdmult = thstmult_PUTTER_[ii];
				} else {
					thstdmult = thstmult_[ii];
				}
				iana_getballcandidatorAdapt
					(piana, camid, procimg, droi.width, droi.height, droi.width, &bc[0], &bcount, 
					 HALFWINSIZE_CHECKREADY, 
					 checkready_multv, checkready_expn,
					 thstdmult,
					 cermax
					 ,CUTMEAN_CHECKREADY	// 20190420, yhsuk
					);
				if (bcount > 0) {
					break;
				}
			}
		} else {
			if (piana->camsensor_category == CAMSENSOR_P3V2) {
				checkready_multv = 1;
				checkready_expn = 0;
			} else {
				checkready_multv = CHECKREADY_MULTV;
				checkready_expn = CHECKREADY_EXPN;
			}
			thstdmult = THSTDMULT_;
			iana_getballcandidatorAdapt
				(piana, camid, procimg, droi.width, droi.height, droi.width, &bc[0], &bcount, 
				 HALFWINSIZE_CHECKREADY, 
				 checkready_multv, checkready_expn,
				 thstdmult,
				 cermax
				 ,CUTMEAN_CHECKREADY	// 20190420, yhsuk
				);
			//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "after Adapt, bcount: %d, cermax: %lf\n",
			//		bcount, cermax);
			if (bcount == 0) {
				iana_getballcandidatorAdapt
					(piana, camid, procimg, droi.width, droi.height, droi.width, &bc[0], &bcount, 
					 HALFWINSIZE_CHECKREADY, 
					 checkready_multv, checkready_expn,
					 (thstdmult * 2.0),
					 cermax
					 ,CUTMEAN_CHECKREADY	// 20190420, yhsuk
					);
				//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "after 2nd. Adapt, bcount: %d, cermax: %lf\n",
				//		bcount, cermax);
			}
		}
//#define USECANNY
//#define GOODCANNY

		//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[1-3] m: %lf\n", piana->ic[0].icp.bAp2l.mat[0]);		// CHECKME

#if 0
		I32 iana_getballcandidatorCanny(
				iana_t *piana, U32 camid, U08 *buf, 
				U32 width, U32 height, 			// ROI width
				U32 width2,						// Image width for buf
				iana_ballcandidate_t bc[MAXCANDIDATE], U32 *pbcount,
				U32 maxth,
				U32 minth,
				U32 multv,
				U32 nexp,
				double cermax
				)
#endif

#if defined(USECANNY)
			goodkind = 0;
		if (bcount == 0) {
			//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[1-4] m: %lf\n", piana->ic[0].icp.bAp2l.mat[0]);		// CHECKME
			iana_getballcandidatorCanny
				(piana, camid, procimg, 
				 droi.width, droi.height, 
				 droi.width, 
				 &bc[0], &bcount, 
				 100,			// fake
				 10,			// fake
				 CHECKREADY_MULTV, CHECKREADY_EXPN,
				 cermax);
			//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "after Adapt, Canny: %d, cermax: %lf\n",
			//		bcount, cermax);
			//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[1-5] m: %lf\n", piana->ic[0].icp.bAp2l.mat[0]);		// CHECKME
			if (bcount > 0) {
#if defined(GOODCANNY)
				goodkind = 1;
#else
				goodkind = 2;
#endif
			}
		} else {
			goodkind = 1;
		}
#else
		if (bcount == 0) {
			goodkind = 0;
		} else {
			goodkind = 1;
		}
#endif
		//g_zzz = 0;
#endif

		// refine ball candidate
		if (bcount > 0) {
			//U32 ii;
			if (goodkind != 1) {
				cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "goodkind: %d.\n", goodkind);
			}
			for (ii = 0; ii < bcount; ii++) {
#if defined (USE_IANA_BALL)
				if (bc[ii].cexist == IANA_BALL_EXIST) 
#else
				if (bc[ii].cexist)
#endif
				{
					if (bc[ii].cer > cermax || bc[ii].cr < crmin || bc[ii].cr > crmax) {	// Filtering..
						cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] cer: %lf,  cr: %lf (crmin: %lf crmax: %lf)\n", ii, bc[ii].cer, bc[ii].cr,
								crmin, crmax);
#if defined (USE_IANA_BALL)
						bc[ii].cexist = IANA_BALL_EMPTY;
#else
						bc[ii].cexist = 0;
#endif
						continue;
					} else {
						point_t Pp, Lp;

						//-- Converse to Pixel point.
						/*
						   bc[ii].cx = (bc[ii].cx / xfact) + rectArea.left;
						   bc[ii].cy = (bc[ii].cy / yfact) + rectArea.top;
						   */






						bc[ii].P.x = (bc[ii].P.x / xfact) + rectArea.left;
#define MULTITUDE_FRACTION
#if defined(MULTITUDE_FRACTION)
						bc[ii].P.y = (bc[ii].P.y + 0.5) * multitude / yfact + rectArea.top;
#else
						bc[ii].P.y = (bc[ii].P.y / (yfact/multitude)) + rectArea.top;
#endif
						bc[ii].P.z = 0.0;

						bc[ii].cr /= xfact;

						//-- Converse to Local point.
						Pp.x = bc[ii].P.x;
						Pp.y = bc[ii].P.y;
						//								iana_P2L(piana, camid, &Pp, &Lp, 1);
						iana_P2L(piana, camid, &Pp, &Lp, 0);
						/*
						   bc[ii].lx = Lp.x;
						   bc[ii].ly = Lp.y;
						   */
						bc[ii].L.x = Lp.x;
						bc[ii].L.y = Lp.y;
						bc[ii].L.z = 0.0;

#if 0
						{
							U32 cx, cy, cr;
							// get ball brightness
							cx = (U32) Pp.x;
							cy = (U32) Pp.y;
							cr = (U32) bc[ii].cr;

//							brightness = iana_get_ballbrightness(piana, camid, buf, width, height, Pp.x, Pp.y, bc[ii].cr);
							if (piana->himgfunc) {
								mycircleGray((int)cx, (int)cy, (int)cr, 0xFF, (U08 *)buf, width);

								((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)(
									(char *)buf,
									width, height,
									3 /*0*/, 1, 2, 3, piana->imguserparam);
							}
						}
#endif
					}
				}
			}
		}
	}
//__debugbreak();
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" camid: %d\n", camid);
	for (ii = 0; ii < bcount; ii++) {
		if (bc[ii].cexist == IANA_BALL_EXIST) {
			calc_circle_mean_stddev(&bc[ii], buf, width, height);
		}
	}

	if (piana->himgfunc) {
		//U32 ii;
		for (ii = 0; ii < bcount; ii++) {
			if (bc[ii].cexist == IANA_BALL_EXIST) {
				U32 cx, cy, cr;


				cx = (U32) bc[ii].P.x; 
				cy = (U32) bc[ii].P.y; 
				cr = (U32) bc[ii].cr;
				mycircleGray(cx, cy, (int)cr, 0xFF, (U08 *)buf, width);
			}
		}
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf, width, height, 3, 1, 2, 3, piana->imguserparam); 
	}

	res = 1;
	*pbcount = bcount;
	*pbuf = buf;

func_exit:
	return res;
#else
    I32 res;
    camif_buffer_info_t *pb;
    iana_cam_t	*pic;
    camimageinfo_t	*pcii;
    U32 multitude;

    U08 *buf;
    U08 *procimg;
    U32 rindex;

    int singlewidth_mult , singleheight_mult;
    U32 bcount;

    double	xfact, yfact;
    cr_rect_t rectArea;

    U32 goodkind;
    U32 m;

    U32 width, height;
    U32 ii;

    int width_resize, height_resize;

    pic   =piana->pic[camid];
    buf = NULL;
    pcii = NULL;
    width = 1280;
    height = 1024;

    res = scamif_imagebuf_peek(piana->hscamif, camid, NORMALBULK_NORMAL, NULL, NULL, NULL);
    if (res) {		// If there is any available frame.
        res = scamif_imagebuf_read_latest(piana->hscamif, camid, NORMALBULK_NORMAL, &pb, &buf, &rindex);
        piana->lastrindex[camid] = rindex;
        pcii = &pb->cii;
        width = pcii->width;
        height = pcii->height;
    }

    if (res == 0 || pcii == NULL) {	// No image data yet.
        goto func_exit;
    }

    multitude 	= pcii->multitude;
    if (multitude == 0) {
        multitude = 1;
    }

    m = 0;

    procimg = pic->pimgFull;

    {
        double	xshift, yshift;
    
        memcpy(&rectArea, &pic->icp.rectAreaExt, sizeof(cr_rect_t));

        xfact = 0.5;
        yfact = (0.5*multitude);

        iana_setReadyRunBallsizeMinMax(piana, camid, xfact);

        xshift 		= 0;
        yshift 		= 0;

        singlewidth_mult = FULLWIDTH; 
        singleheight_mult = FULLHEIGHT;
    }

    {
        // no matter whether singleheight_mult is FULLWIDTH or FULLWIDTH/multitude
        // The former case is full buffer image so that all multiteded image and the latter case is a slice.
        // But it does not matter because we'll gonna crop from top image only now.
        cv::Mat src(cv::Size(singlewidth_mult, singleheight_mult), CV_8UC1, buf), cropped, dst;

        int offsetX, offsetY;
        int width_crop, height_crop;

        width_crop = (rectArea.right - rectArea.left + 1);
        height_crop = (rectArea.bottom   - rectArea.top  + 1) / multitude;
        
        if (width_crop > singlewidth_mult || height_crop > singleheight_mult) {
            res = 0;
            goto func_exit;
        }


        width_resize = (int)(width_crop * xfact + 0.001);
        height_resize = (int)(height_crop * yfact + 0.001);

        offsetX = rectArea.left;
        offsetY = (rectArea.top / multitude);
        cropped = src(cv::Rect(offsetX, offsetY, width_crop, height_crop));
        cv::resize(src, dst, cv::Size(width_resize, height_resize), xfact, yfact, cv::INTER_CUBIC);
        memcpy(procimg, dst.data, sizeof(U08)*dst.cols*dst.rows);

        if (piana->himgfunc) {
            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)(
                (char *)procimg, dst.cols, dst.rows, 3 /*0*/, 1, 2, 3, piana->imguserparam);
        }
    }


    {
        double cermax;
        double crmin;
        double crmax;
        double crminratio;
        double crmaxratio;

        U32 checkready_multv;
        U32 checkready_expn;
        double thstdmult;

        //---------
        iana_setReadyRunBallsizeMinMax(piana, camid, xfact);

        if (piana->camsensor_category == CAMSENSOR_EYEXO) {
            crminratio = CRMINRATIO_EYEXO;
            crmaxratio = CRMAXRATIO_EYEXO;
        } else if (piana->camsensor_category == CAMSENSOR_P3V2) {
            crminratio = CRMINRATIO;
            if (camid == 0) {
                crmaxratio = CRMAXRATIO_P3V2_0;			// camid 0
            } else {
                crmaxratio = CRMAXRATIO_P3V2_1;			// camid 1
            }
        } else {
            crminratio = CRMINRATIO;
            crmaxratio = CRMAXRATIO;
        }
        crmin = (piana->pic[camid]->icp.RunBallsizePMin / 2.0) * crminratio;
        crmax = (piana->pic[camid]->icp.RunBallsizePMax / 2.0) * crmaxratio;


#define BALLSIZEMAXMULT	1.2
        crmax *= BALLSIZEMAXMULT;
        cermax = pic->icp.CerBallexist;

        memset(bc, 0, sizeof(iana_ballcandidate_t)*MAXCANDIDATE);
        bcount = 0;
#define CHECKREADY_MAXTH	40
#define CHECKREADY_MINTH	30
#define CHECKREADY_MULTV	3
#define CHECKREADY_EXPN		1				

#define HALFWINSIZE_CHECKREADY	16
#define THSTDMULT_		2.0
#define CHECKREADY_MULTV_Z3		3
#define CHECKREADY_EXPN_Z3		2				
#define THSTDMULT__Z3			1.5
#define CUTMEAN_CHECKREADY	0.5
#define THSTMULT_COUNT	2
        if (piana->camsensor_category == CAMSENSOR_Z3 || piana->camsensor_category == CAMSENSOR_EYEXO) {
            
            U32 thstmult_count;
            double thstmult_[THSTMULT_COUNT] 		= { 0.8, 1.5 };
            double thstmult_PUTTER_[THSTMULT_COUNT] = { 0.8, 1.5 };

            //--
            checkready_multv = CHECKREADY_MULTV_Z3;
            checkready_expn = CHECKREADY_EXPN_Z3;
            thstmult_count = THSTMULT_COUNT;
            for (ii = 0; ii < thstmult_count; ii++) {
                if (piana->processarea == IANA_AREA_PUTTER) {
                    thstdmult = thstmult_PUTTER_[ii];
                } else {
                    thstdmult = thstmult_[ii];
                }

                iana_getballcandidatorAdapt(
                    piana, camid, procimg, 
                    width_resize, height_resize, width_resize, 
                    &bc[0], &bcount,
                    HALFWINSIZE_CHECKREADY,
                    checkready_multv, checkready_expn,
                    thstdmult,
                    cermax, 
                    CUTMEAN_CHECKREADY	// 20190420, yhsuk
                );

                if (bcount > 0) {
                    break;
                }
            }
        } else {
            if (piana->camsensor_category == CAMSENSOR_P3V2) {
                checkready_multv = 1;
                checkready_expn = 0;
            } else {
                checkready_multv = CHECKREADY_MULTV;
                checkready_expn = CHECKREADY_EXPN;
            }
            thstdmult = THSTDMULT_;

            iana_getballcandidatorAdapt(
                piana, camid, procimg, 
                width_resize, height_resize, width_resize,
                &bc[0], &bcount,
                HALFWINSIZE_CHECKREADY,
                checkready_multv, checkready_expn,
                thstdmult,
                cermax, 
                CUTMEAN_CHECKREADY	// 20190420, yhsuk
            );
            
            if (bcount == 0) {
                iana_getballcandidatorAdapt(
                    piana, camid, procimg, 
                    width_resize, height_resize, width_resize, &bc[0], &bcount,
                    HALFWINSIZE_CHECKREADY,
                    checkready_multv, checkready_expn,
                    (thstdmult * 2.0),
                    cermax, 
                    CUTMEAN_CHECKREADY	// 20190420, yhsuk
                );
            }
        }

    #if defined(USECANNY)
            goodkind = 0;
        if (bcount == 0) {
            iana_getballcandidatorCanny
            (piana, camid, procimg,
                width_resize, height_resize,
                width_resize,
                &bc[0], &bcount,
                100,			// fake
                10,			// fake
                CHECKREADY_MULTV, CHECKREADY_EXPN,
                cermax);
            if (bcount > 0) {
#if defined(GOODCANNY)
                goodkind = 1;
#else
                goodkind = 2;
#endif
            }
        } else {
            goodkind = 1;
        }
    #else
            if (bcount == 0) {
                goodkind = 0;
            } else {
                goodkind = 1;
            }
    #endif

        if (bcount > 0) {
            if (goodkind != 1) {
                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "goodkind: %d.\n", goodkind);
            }
            for (ii = 0; ii < bcount; ii++) {
                if (bc[ii].cexist == IANA_BALL_EXIST) {
                    if (bc[ii].cer > cermax || bc[ii].cr < crmin || bc[ii].cr > crmax) {	// Filtering..
                        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
                            "[%d] cer: %lf,  cr: %lf (crmin: %lf crmax: %lf)\n", 
                            ii, bc[ii].cer, bc[ii].cr, crmin, crmax);
                        bc[ii].cexist = IANA_BALL_EMPTY;
                        continue;
                    } else {
                        point_t Pp, Lp;

                        //-- Converse to Pixel point.
                        bc[ii].P.x = (bc[ii].P.x / xfact) + rectArea.left;
#define MULTITUDE_FRACTION
#if defined(MULTITUDE_FRACTION)
                        bc[ii].P.y = (bc[ii].P.y + 0.5) * multitude / yfact + rectArea.top;
#else
                        bc[ii].P.y = (bc[ii].P.y / (yfact/multitude)) + rectArea.top;
#endif
                        bc[ii].P.z = 0.0;

                        bc[ii].cr /= xfact;

                        //-- Converse to Local point.
                        Pp.x = bc[ii].P.x;
                        Pp.y = bc[ii].P.y;
                        //								iana_P2L(piana, camid, &Pp, &Lp, 1);
                        iana_P2L(piana, camid, &Pp, &Lp, 0);

                        bc[ii].L.x = Lp.x;
                        bc[ii].L.y = Lp.y;
                        bc[ii].L.z = 0.0;
                    }
                }
            }
        }
    }

    for (ii = 0; ii < bcount; ii++) {
        if (bc[ii].cexist == IANA_BALL_EXIST) {
            calc_circle_mean_stddev(&bc[ii], buf, width, height);
        }
    }

	bcount = 0;
	for (ii = 0; ii < MAXCANDIDATE; ii++) {
		if (bc[ii].cexist == IANA_BALL_EXIST) {
			bcount ++;
		}
	}
	
    if (piana->himgfunc) {
        for (ii = 0; ii < bcount; ii++) {
            if (bc[ii].cexist == IANA_BALL_EXIST) {
                U32 cx, cy, cr;

                cx = (U32)bc[ii].P.x;
                cy = (U32)bc[ii].P.y;
                cr = (U32)bc[ii].cr;
                mycircleGray(cx, cy, (int)cr, 0xFF, (U08 *)buf, width);
            }
        }
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, width, height, 3, 1, 2, 3, piana->imguserparam);
    }

    res = 1;
    *pbcount = bcount;
    *pbuf = buf;

    func_exit:
    return res;
#endif
}


I32 refineBunkerRoughArea(iana_t *piana, U32 camid, U08 *pbuf, cr_rect_t rectArea, double xfact, double yfact, I32 width, I32 height)
{
	I32 res;
#if 1
	if (*(piana->pRoughBunkerMat)) {
		I32 sx, sy, ex, ey;
		I32 www, hhh;
		cr_rectL_t *prtL;
		point_t Lp, Pp;
		//U32 offset_x, offset_y;
#ifdef IPP_SUPPORT
		Ipp64f mean;				// Ipp64f == double
		Ipp64f stdev;		

		I32 subC0;
		Ipp8u subC;
		IppiSize roisize;
#else
        double mean;				// Ipp64f == double
        double stdev;

        I32 subC0;
        U08 subC;
        cv::Size roisize;
#endif

		//--
		prtL = piana->prectLBunker;

		Lp.x = prtL->sx; 
        Lp.y = prtL->sy;  // P0
		iana_L2P(piana, camid, &Lp, &Pp, 0);

		sx = (I32)Pp.x; 
        sy = (I32)Pp.y;
		ex = (I32)Pp.x; 
        ey = (I32)Pp.y;

		Lp.x = prtL->ex; Lp.y = prtL->sy;	// P1
		iana_L2P(piana, camid, &Lp, &Pp, 0);
		if (sx > Pp.x) sx = (I32)Pp.x; 
        if (ex < Pp.x) ex = (I32)Pp.x;
		if (sy > Pp.y) sy = (I32)Pp.y; 
        if (ey < Pp.y) ey = (I32)Pp.y;

		Lp.x = prtL->ex; Lp.y = prtL->ey;	// P2
		iana_L2P(piana, camid, &Lp, &Pp, 0);
		if (sx > Pp.x) sx = (I32)Pp.x; 
        if (ex < Pp.x) ex = (I32)Pp.x;
		if (sy > Pp.y) sy = (I32)Pp.y; 
        if (ey < Pp.y) ey = (I32)Pp.y;

		Lp.x = prtL->sx; Lp.y = prtL->ey;	// P3
		iana_L2P(piana, camid, &Lp, &Pp, 0);
		if (sx > Pp.x) sx = (I32)Pp.x; 
        if (ex < Pp.x) ex = (I32)Pp.x;
		if (sy > Pp.y) sy = (I32)Pp.y; 
        if (ey < Pp.y) ey = (I32)Pp.y;

		sx = (I32)((sx - rectArea.left)*xfact); 
        ex = (I32)((ex - rectArea.left)*xfact);
		sy = (I32)((sy - rectArea.top)*yfact); 
        ey = (I32)((ey - rectArea.top)*yfact);

		if (sx < 0) sx = 0; 
        if (sx >= width) sx = width-1;
		
        if (ex < 0) ex = 0; 
        if (ex >= width) ex = width-1;
		
        if (sy < 0) sy = 0; 
        if (sy >= height) sy = height - 1;

        if (ey < 0) ey = 0; 
        if (ey >= height) ey = height - 1;

		hhh = ey-sy+1;
		www = ex-sx+1;


		roisize.width = www;
		roisize.height = hhh;
#ifdef IPP_SUPPORT
		ippiMean_StdDev_8u_C1R(pbuf, width, roisize, &mean, &stdev);
//#define MEAN_MULT	5
#define MEAN_MULT	2
//#define MEAN_MULT	1
		subC0 = (I32)(mean * MEAN_MULT);
#define MAXSUBC	200
		if (subC0 > MAXSUBC) {
			subC = MAXSUBC;
		} else {
			subC = (Ipp8u) subC0;
		}

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" mean: %lf, subC: %d\n", mean, subC);
		ippiSubC_8u_C1IRSfs(
				(Ipp8u)(subC),
				pbuf + (sx + sy * width), width,
				roisize, 0);
#else
        {

#define MEAN_MULT	2
#define MAXSUBC	200

            cv::Mat cvImgSrc(cv::Size(width, height), CV_8UC1, pbuf), cvImgSub;
            cv::Mat cvMean, cvStdev;

            cv::meanStdDev(cvImgSrc, cvMean, cvStdev);
            
            subC0 = (I32)(cvMean.at<double>(0) * MEAN_MULT);
            
            if (subC0 > MAXSUBC) {
                subC = MAXSUBC;
            } else {
                subC = (U08)subC0;
            }

            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" mean: %lf, subC: %d\n", cvMean.at<double>(0), subC);

            cvImgSub = cvImgSrc - subC;
            memcpy(pbuf, cvImgSub.data, sizeof(U08) * width * height);
            UNUSED(mean); UNUSED(stdev);
        }
#endif
	}
#endif

	res = 1;

	return res;
}

I32 calc_circle_mean_stddev(iana_ballcandidate_t *pbc, U08 *pimgbuf, U32 width, U32 height)
{
	I32 res;
	I32 cx, cy, cr;
	I32 sx, sy, ex, ey;

	I32 widthr, heightr;
	I32 distr2;
	I32 sumr, sumr2;
	I32 countr;
	double meanr, stddevr;

	//I32 widthHr, heightHr;
	I32 distHr2;
	I32 sumHr, sumHr2;
	I32 countHr;
	double meanHr, stddevHr;

	I32 x, y;
	I32 dx2, dy2;
	I32 distd2;
	I32 value;

	//---

	cx = (I32)pbc->P.x;
	cy = (I32)pbc->P.y;
	cr = (I32)pbc->cr;

	sx = cx - cr;
	sy = cy - cr;

	ex = cx + cr;
	ey = cy + cr;

	if (sx < 0) sx = 0;
	if (sy < 0) sy = 0;

	if (ex >= (I32)width) ex = width-1;
	if (ey >= (I32)height) ey = height-1;

	widthr = ex - sx +1;
	heightr = ey - sy +1;
#define MINRATIO	(0.8)
	if ((widthr < (I32) (2*cr * MINRATIO)) || (heightr < (I32) (2*cr * MINRATIO))) {
		pbc->mean = 0; pbc->stddev = 0;
		pbc->meanH = 0; pbc->stddevH = 0;
		res = 0;
		goto func_exit;
	}

	sumr = 0; sumr2 = 0;
	sumHr = 0; sumHr2 = 0;
	countr = 0; countHr = 0;
	distr2 = cr*cr;
	distHr2 = cr*cr/4;
	for (y = sy; y <= ey; y++) {
		dy2 = (y - cy) * (y - cy);
		for (x = sx; x <= ex; x++) {
			dx2 = (x - cx) * (x - cx);
			distd2 = dx2 + dy2;

			if (distd2 <= distr2) {
				value = (U32)*(pimgbuf + x + y * width);
				sumr = sumr + value;
				sumr2 = sumr2 + value*value;
				countr++;

				if (distd2 < distHr2) {
					sumHr = sumHr + value;
					sumHr2 = sumHr2 + value*value;
					countHr++;
				}
			}
		}
	}

	if (
		countr < ((widthr*MINRATIO) * (heightr*MINRATIO))
		|| countHr < ((widthr*MINRATIO/(2.5)) * (heightr*MINRATIO/ (2.5)))
		) {
		pbc->mean = 0; pbc->stddev = 0;
		pbc->meanH = 0; pbc->stddevH = 0;
		res = 0;
		goto func_exit;
	}

#define MINCOUNTR 2
	if (countr < MINCOUNTR) {
		pbc->mean = 0; pbc->stddev = 0;
		pbc->meanH = 0; pbc->stddevH = 0;
		res = 0;
		goto func_exit;
	}
	meanr = (double)sumr / countr;
	stddevr = sumr2/countr - meanr*meanr;
	if (stddevr > 0) {
		stddevr = sqrt(stddevr);
	} else {
		stddevr = 0;
	}

	meanHr = (double)sumHr / countHr;
	stddevHr = sumHr2/countHr - meanHr*meanHr;
	if (stddevHr > 0) {
		stddevHr = sqrt(stddevHr);
	} else {
		stddevHr = 0;
	}

	pbc->mean = meanr; pbc->stddev = stddevr;
	pbc->meanH = meanHr; pbc->stddevH = stddevHr;

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" meanr : %10lf stddevr : %10lf\n", meanr, stddevr);
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" meanHr: %10lf stddevHr: %10lf\n", meanHr, stddevHr);
	res = 1;

func_exit:
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
	return res;
}
		


/*!
 ********************************************************************************
 *	@brief      CAM  ball ready state
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		Terminate result. 
 *
 *	@author	    yhsuk
 *  @date       2015/12/23
 *******************************************************************************/
I32 IANA_Prepare4Ready(iana_t *piana)
{
	double lx, ly;
	iana_cam_t	*pic;
	U32 camid;
	U32 camid_;
	U32 ox, oy;
	point_t Lp;

	U32 i;
	//----
	camid = piana->camidCheckReady;
	pic   =piana->pic[camid];

	lx = pic->icp.L.x;
	ly = pic->icp.L.y;

	iana_readyarea(piana, camid, lx, ly, &ox, &oy);
	piana->pic[camid]->offset_x = ox;
	piana->pic[camid]->offset_y = oy;
	//if (camid == 0) {
	//	piana->ic[camid].runProcessingWidth	= CAM0_RUN_PROCESSING_WIDTH;
	//	piana->ic[camid].runProcessingHeight	= CAM0_RUN_PROCESSING_HEIGHT;
	//} else {
	//	piana->ic[camid].runProcessingWidth	= CAM1_RUN_PROCESSING_WIDTH;
	//	piana->ic[camid].runProcessingHeight	= CAM1_RUN_PROCESSING_HEIGHT;
	//}
		
	//	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%d>: (%d, %d) <- (%lf, %lf)\n", camid, ox, oy, lx, ly);


	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "lx ly: %lf %lf\n", lx, ly);

	piana->checkreadypositioncount = 0;

	camid_ = 1 - camid;	// TODO: change code for extension(up to 4 cam)
	pic   =piana->pic[camid_];
	lx = pic->icp.L.x;
	ly = pic->icp.L.y;

	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "==>  lx ly: %lf %lf\n", lx, ly);

	iana_readyarea(piana, camid_, lx, ly, &ox, &oy);
	piana->pic[camid_]->offset_x = ox;
	piana->pic[camid_]->offset_y = oy;


//	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%d>: (%d, %d) <- (%lf, %lf)\n", camid_, ox, oy, lx, ly);
	piana->currentcamconf = IANA_CAMCONF_CHANGEME;
	iana_cam_reconfig(piana, 1);		// configure camera..
	piana->runindexcount = 0;
	piana->readymercycount = 0;
	piana->goodreadytick = cr_gettickcount();

	Lp.x = lx;
	Lp.y = ly;
//#define DO_CONFIG_CONVERSION

#if defined(DO_CONFIG_CONVERSION)
	iana_config_conversion2(piana, &Lp);
#endif


	for (i = 0; i < MARKSEQUENCELEN; i++) {
		piana->bulkpairindex[i] = BULKPAIRINDEX_NONE;
	}

	for (camid = 0; camid < NUM_CAM; camid++) {
		pic = piana->pic[camid];

		pic->icp.mean = 0;
		pic->icp.stddev = 0;
		pic->icp.meanH = 0;
		pic->icp.stddevH = 0;
	}
	return 1;
}


// ATTACK CAM..
/*!
 ********************************************************************************
 *	@brief      CAM  Get Ready area
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @param[in]	camid
 *              Camera ID
 *  @param[in]	lx
 *              center x, local position
 *  @param[in]	ly
 *              center y, local position
 *  @param[out]	pox
 *              offset x
 *  @param[out]	poy
 *              offset y
 *  @return		Terminate result. 
 *
 *	@author	    yhsuk
 *  @date       2015/12/23
 *******************************************************************************/
//static int s_y0 = -120;
//static int s_y1 = -100;
I32 iana_readyarea(iana_t *piana, U32 camid, double lx, double ly, U32 *pox, U32 *poy)
{
	I32 ox, oy;
	iana_cam_t	*pic;
	point_t Lp;
	point_t Pp;

	I32 offsetoffset_x;
	I32 offsetoffset_y;
	I32 run_width;
	I32 run_height;

	iana_setup_t *pisetup;
	iana_cam_setup_t *picsetup;
	
	I32 rrr;
	//----
//	camid = piana->camidCheckReady;
	pic   =piana->pic[camid];

	Lp.x = lx;
	Lp.y = ly;
	rrr = iana_L2P(piana, camid, &Lp, &Pp, 0);

//	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "READYAREA [%d] Lp: %lf %lf, Pp: %lf %lf rrr: %d\n", camid, Lp.x, Lp.y, Pp.x, Pp.y, rrr);

	ox = (I32) Pp.x;
	oy = (I32) Pp.y;

#if 0
#define OFFSETOFFSET_X		(-420)
#define OFFSETOFFSET_Y0		(-120)				// offset for Center Cam
#define OFFSETOFFSET_Y1		(-100)				// offset for Side Cam

#define RUN_WIDTH			480
#define RUN_HEIGHT			240


#define OFFSETOFFSET_X_PUTTER		(-560)
#define OFFSETOFFSET_Y0_PUTTER		(-240)				// offset for Center Cam
#define OFFSETOFFSET_Y1_PUTTER		(-240)				// offset for Side Cam

#define RUN_WIDTH_PUTTER			640
#define RUN_HEIGHT_PUTTER			480
#endif

	offsetoffset_x	= 0;
	offsetoffset_y	= 0;
	run_width		= 0;
	run_height		= 0;

#if 1
	pisetup = &piana->isetup;
	picsetup = &pisetup->icsetup[camid];

	if (piana->processarea == IANA_AREA_PUTTER) {
		offsetoffset_x	= picsetup->offsetoffset_x_putter;
		offsetoffset_y	= picsetup->offsetoffset_y_putter;
		run_width		= picsetup->run_width_putter;
		run_height		= picsetup->run_height_putter;
#if 0
		if (camid == 0) {
			offsetoffset_x	= OFFSETOFFSET_X_PUTTER;
			offsetoffset_y	= OFFSETOFFSET_Y0_PUTTER;
			run_width		= RUN_WIDTH_PUTTER;
			run_height		= RUN_HEIGHT_PUTTER;
		} else if (camid == 1) {
			offsetoffset_x	= OFFSETOFFSET_X_PUTTER;
			offsetoffset_y	= OFFSETOFFSET_Y1_PUTTER;
			run_width		= RUN_WIDTH_PUTTER;
			run_height		= RUN_HEIGHT_PUTTER;
		} else if (camid == 2) {		// ATTACKCAM
			offsetoffset_x	= OFFSETOFFSET_XATTCAM;
			offsetoffset_y	= OFFSETOFFSET_YATTCAM;
			run_width		= ATTCAM_RUN_WIDTH;
			run_height		= ATTCAM_RUN_HEIGHT;
		}
#endif

	} else {
		offsetoffset_x	= picsetup->offsetoffset_x;
		offsetoffset_y	= picsetup->offsetoffset_y;
		run_width		= picsetup->run_width;
		run_height		= picsetup->run_height;
#if 0
		if (camid == 0) {
			offsetoffset_x	= OFFSETOFFSET_X0;
			offsetoffset_y	= OFFSETOFFSET_Y0;
			run_width		= CAM0_RUN_WIDTH;
			run_height		= CAM0_RUN_HEIGHT;
		} else if (camid == 1) {
			offsetoffset_x	= OFFSETOFFSET_X1;
			offsetoffset_y	= OFFSETOFFSET_Y1;
			run_width		= CAM1_RUN_WIDTH;
			run_height		= CAM1_RUN_HEIGHT;
		} else if (camid == 2) {		// ATTACKCAM
			offsetoffset_x	= OFFSETOFFSET_XATTCAM;
			offsetoffset_y	= OFFSETOFFSET_YATTCAM;
			run_width		= ATTCAM_RUN_WIDTH;
			run_height		= ATTCAM_RUN_HEIGHT;
		}
#endif
	}
#endif

#if 0		// NOT yet....
	offsetoffset_x	= OFFSETOFFSET_X;
	offsetoffset_y0	= OFFSETOFFSET_Y0;
	offsetoffset_y1 = OFFSETOFFSET_Y1;
	run_width		= RUN_WIDTH;
	run_height		= RUN_HEIGHT;
#endif

//	if ((piana->useLeftHanded != 0) && (piana->Right0Left1 == 1)) 			// LEFT-HANDED?
	if (piana->Right0Left1 == 1) 			// LEFT-HANDED?
	{
		// |oo_yL| + |oo_y| = h   ->  (-oo_yL) + (-oo_y) = h  -> oo_yL = -h - oo_y
		offsetoffset_y = -run_height - offsetoffset_y;
	} 
	ox = ox + offsetoffset_x;
	oy = oy + offsetoffset_y;
	ox = ((ox+(16-1))/16) * 16;


//	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "READYAREA [%d] ox, oy: %d, %d\n", camid, ox, oy);


	if (ox < 16) {
		ox = 16;
	}
	if (ox > 1280 - run_width - 16) {
		ox = 1280 - run_width - 16;
	}

	if (oy < 16) {
		oy = 16;
	}
	if (oy > 1024 - run_height - 16) {
		oy = 1024 - run_height - 16;
	}

//	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-------------> ox, oy: %d, %d\n", ox, oy);
	*pox = (U32) ox;
	*poy = (U32) oy;

	return 1;
}



/*!
 ********************************************************************************
 *	@brief      CAM check POWERSAVING
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @param[in]	checkchange
 *              check change or update background image only
 *  @return		changed. 
 *
 *	@author	    yhsuk
 *  @date       2017/0921
 *******************************************************************************/
I32 iana_checkpowersaving(iana_t *piana, U32 checkchange)
{
#ifdef IPP_SUPPORT

	I32 res;
	camif_buffer_info_t *pb;
	iana_cam_t	*pic;
	camimageinfo_t	*pcii;
	U32 multitude;
	
	U08 *buf;
	U08 *procimg;
	U32 rindex;
	U32 camid;

	IppiSize ssize;
	IppiRect sroi, droi;
	int sstep, dstep;
	IppStatus status;
//	iana_ballcandidate_t bc[MAXCANDIDATE];
//	U32 bcount;

	double	xfact, yfact; 
	cr_rect_t rectArea;

	double	xshift, yshift; 
	Ipp8u 	*resizebuf;
	int 	bufsize;
	I32 i;

/*
	static int iii = 0;
	static int jjj = 0;
	*/




	U32 m;

//	U32 goodkind;
	
	//--
	// 1) get image

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with hiana: %p, checkchange: %d\n",  piana, checkchange);

	camid = piana->camidCheckReady;
	pic   =piana->pic[camid];

	buf = NULL;
	res = 0;

#define CHECKPEEKCOUNT	50
#define CHECKPEEKSLEEP	10
	for (i = 0; i < CHECKPEEKCOUNT; i++) {
		res = scamif_imagebuf_peek(piana->hscamif, camid, NORMALBULK_NORMAL, NULL, NULL, NULL);
		if (res) {
			if (i != 0) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__" call scamif_imagebuf_peek() success, i: %d\n", i);
			}
			break;
		}
		cr_sleep(CHECKPEEKSLEEP);
	}
	pcii = NULL;
	if (res) {		// If there is any available frame.
		res = scamif_imagebuf_read_latest(piana->hscamif, camid, NORMALBULK_NORMAL, &pb, &buf, &rindex);	
		pcii = &pb->cii;
		if (pcii == NULL) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__" scamif_imagebuf_read_latest() pcii: NULL\n");
		}
	} else {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__" scamif_imagebuf_peek() res: %d\n", res);
	}



//	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" --=-- checkchange: %d\n", checkchange);
	if (res == 0 || pcii == NULL) {	// No image data yet.
		res = -1;
		goto func_exit;
	}

	multitude 	= pcii->multitude;
	if (multitude == 0) {
		multitude = 1;
	}


	res = 0;
	for (m = 0; m < 1 /* multitude */; m++) {
		if (checkchange == 0) {
			procimg = pic->prefimg;
		} else {
			procimg = pic->pimgFull;
		}

		// 2) Resize to 0.5x0.5         (1280x1024 to 640x512)
		{

			//--

#if defined(V2000_TEST)
			memcpy(&rectArea, &pic->icp.rectAreaExt, sizeof(cr_rect_t));				// 2018 11/22.. 
#else
			memcpy(&rectArea, &pic->icp.rectAreaExt, sizeof(cr_rect_t));				// 2018 11/22.. 
			//memcpy(&rectArea, &pic->icp.rectArea, sizeof(cr_rect_t));
#endif
			xfact = 0.5;
			yfact = (0.5*multitude);

			iana_setReadyRunBallsizeMinMax(piana, camid, xfact);

			xshift 		= 0;
			yshift 		= 0;

			ssize.width = FULLWIDTH;
			ssize.height = FULLHEIGHT / multitude;

			sroi.x 		= 0;
			sroi.y 		= 0;
			sroi.width 	= (rectArea.right - rectArea.left + 1);
			sroi.height	= (rectArea.bottom   - rectArea.top  + 1) / multitude;

			droi.x 		= 0;
			droi.y 		= 0;
			droi.width 	= (int) (sroi.width * xfact + 0.001);
			droi.height	= (int) (sroi.height * yfact + 0.001);

			sstep 		= FULLWIDTH;
			dstep		= droi.width;


			ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_CUBIC, &bufsize);
			resizebuf = (U08 *)malloc(bufsize);

			status = ippiResizeSqrPixel_8u_C1R(
					(Ipp8u *)buf + rectArea.left + ((rectArea.top / multitude) * ssize.width)  + ssize.width * ssize.height * m,
					ssize,
					sstep,
					sroi,
					//
					(Ipp8u*) procimg,
					dstep,
					droi,
					//
					xfact,
					yfact,
					xshift,
					yshift,
					IPPI_INTER_CUBIC,
					resizebuf
					);
			free(resizebuf);

			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)(
					(char *)procimg, droi.width, droi.height, 0, 1, 2, 3, piana->imguserparam);
			}
		}

		if (checkchange == 0) {
			res = 0;
			break;
		}


		{
			Ipp64f mean;				// Ipp64f == double
			Ipp64f stdev;
			IppiSize roisize;
			Ipp8u	*diffbuf;
			U32 width, height;

			int j;

			int count[6];

			int v;
			int weightedcount;
			//--

			width = droi.width;
			height = droi.height;
			
			roisize.width = width;
			roisize.height = height;
			

			diffbuf = (Ipp8u *) malloc((U32) ((width+1) * (height+1)));
			ippiAbsDiff_8u_C1R(								// C = |A - B|
					pic->prefimg,			width,					// B
					pic->pimgFull, 			width,					// A
					(Ipp8u *)diffbuf, 		width, 				// C, result.
					roisize);


			ippiMean_StdDev_8u_C1R(diffbuf, width, roisize, &mean, &stdev);


			for (i = 0; i < 6; i++) {
				count[i] = 0;
			}

			for (i = 0; i < (int)height; i++) {
				for (j = 0; j < (int)width; j++) {
					v = (int) *(diffbuf + i*width + j);

					if (v < 10) {
						count[0]++;
					} else if (v < 20) {
						count[1]++;
					} else if (v < 30) {
						count[2]++;
					} else if (v < 40) {
						count[3]++;
					} else if (v < 50) {
						count[4]++;
					} else {
						count[5]++;
					}
				}
			}
			free(diffbuf);

			weightedcount = count[2] / 2 + count[3] + count[4] * 2 + count[5] * 5;
#if 0
			cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" mean: %lf stdev: %lf\n",
				mean, stdev);

			cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" c[0]: (%4d %lf) c[1]: (%4d %lf) c[2]: (%4d %lf) c[3]: (%4d %lf) c[4]: (%4d %lf) --  %d\n",
					count[0], (double)count[0] / (width*height),
					count[1], (double)count[1] / (width*height),
					count[2], (double)count[2] / (width*height),
					count[3], (double)count[3] / (width*height),
					count[4], (double)count[4] / (width*height),
					weightedcount
					);
#endif
#define WEIGHTEDCOUNTMIN	1000
			if (weightedcount > WEIGHTEDCOUNTMIN) {
				res = 1;
			} else {
				res = 0;
			}
		}
	} 	// for (m = 0; m < multitude; m++) 
func_exit:
//	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
//#define DEBUG_RES
#if defined(DEBUG_RES)
	{
		static int iii = 0;

		if (++iii > 20) {
			iii = 0;
			res = -1;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__" set res = %d\n", res);

		}
	}
#endif

	
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res: %d\n", res);

	return res;
#else
    I32 res;
    camif_buffer_info_t *pb;
    iana_cam_t	*pic;
    camimageinfo_t	*pcii;
    U32 multitude;

    U08 *buf;
    U08 *procimg;
    U32 rindex;
    U32 camid;

    int width_resize, height_resize;

    double	xfact, yfact;
    cr_rect_t rectArea;

    I32 i;

    //--
    // 1) get image

    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"++ with hiana: %p, checkchange: %d\n", piana, checkchange);

    camid = piana->camidCheckReady;
    pic   =piana->pic[camid];

    buf = NULL;
    res = 0;

#define CHECKPEEKCOUNT	50
#define CHECKPEEKSLEEP	10
    for (i = 0; i < CHECKPEEKCOUNT; i++) {
        res = scamif_imagebuf_peek(piana->hscamif, camid, NORMALBULK_NORMAL, NULL, NULL, NULL);
        if (res) {
            if (i != 0) {
                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__" call scamif_imagebuf_peek() success, i: %d\n", i);
            }
            break;
        }
        cr_sleep(CHECKPEEKSLEEP);
    }

    pcii = NULL;
    if (res) {		// If there is any available frame.
        res = scamif_imagebuf_read_latest(piana->hscamif, camid, NORMALBULK_NORMAL, &pb, &buf, &rindex);
        pcii = &pb->cii;
        if (pcii == NULL) {
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__" scamif_imagebuf_read_latest() pcii: NULL\n");
        }
    } else {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__" scamif_imagebuf_peek() res: %d\n", res);
    }

    if (res == 0 || pcii == NULL) {	// No image data yet.
        res = -1;
        goto func_exit;
    }

    multitude 	= pcii->multitude;
    if (multitude == 0) {
        multitude = 1;
    }

    res = 0;

    if (checkchange == 0) {
        procimg = pic->prefimg;
    } else {
        procimg = pic->pimgFull;
    }

    // 2) Resize to 0.5x0.5         (1280x1024 to 640x512)
    {
        int width, height, width_crop, height_crop;
        //--
        memcpy(&rectArea, &pic->icp.rectAreaExt, sizeof(cr_rect_t));				// 2018 11/22.. 
        xfact = 0.5;
        yfact = (0.5 * multitude);

        iana_setReadyRunBallsizeMinMax(piana, camid, xfact);

        width = FULLWIDTH;
        height = FULLHEIGHT / multitude;

        width_crop 	= (rectArea.right - rectArea.left + 1);
        height_crop	= (rectArea.bottom   - rectArea.top  + 1) / multitude;

        width_resize 	= (int)(width_crop * xfact + 0.001);
        height_resize	= (int)(height_crop * yfact + 0.001);

        {
            cv::Mat src(cv::Size(width, height), CV_8UC1, buf), cropped, dst;
            int offsetX, offsetY;
            offsetX = rectArea.left;
            offsetY = rectArea.top / multitude;
            cropped = src(cv::Rect(offsetX, offsetY, width_crop, height_crop));
            cv::resize(src, dst, cv::Size(width_resize, height_resize), xfact, yfact, cv::INTER_CUBIC);
            memcpy(procimg, dst.data, sizeof(U08) * dst.cols * dst.rows);
        }

        if (piana->himgfunc) {
            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)(
                (char *)procimg, width_resize, height_resize, 0, 1, 2, 3, piana->imguserparam);
        }
    }

    if (checkchange == 0) {
        res = 0;
        goto func_exit;
    }

    {
        int j;

        int count[6];

        int v;
        int weightedcount;
        
        cv::Mat cvDiff1(cv::Size(width_resize, height_resize), CV_8UC1, pic->prefimg),
            cvDiff2(cv::Size(width_resize, height_resize), CV_8UC1, pic->pimgFull),
            cvDiffRes;
        // cv::Mat cvMean, cvStdev;

        cv::absdiff(cvDiff2, cvDiff1, cvDiffRes);
        // cv::meanStdDev(cvDiffRes, cvMean, cvStdev);

        //--
        for (i = 0; i < 6; i++) {
            count[i] = 0;
        }

        for (i = 0; i < (int)cvDiffRes.cols; i++) {
            for (j = 0; j < (int)cvDiffRes.rows; j++) {
                v = (int)cvDiffRes.at<U08>(i, j);

                if (v < 10) {
                    count[0]++;
                } else if (v < 20) {
                    count[1]++;
                } else if (v < 30) {
                    count[2]++;
                } else if (v < 40) {
                    count[3]++;
                } else if (v < 50) {
                    count[4]++;
                } else {
                    count[5]++;
                }
            }
        }

        weightedcount = count[2] / 2 + count[3] + count[4] * 2 + count[5] * 5;
    
#define WEIGHTEDCOUNTMIN	1000
        if (weightedcount > WEIGHTEDCOUNTMIN) {
            res = 1;
        } else {
            res = 0;
        }
    }
    
func_exit:

    //	cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d\n", res);
    //#define DEBUG_RES
    #if defined(DEBUG_RES)
    {
        static int iii = 0;

        if (++iii > 20) {
            iii = 0;
            res = -1;
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__" set res = %d\n", res);

        }
    }
    #endif


    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_VERB, __FUNCTION__"-- with res: %d\n", res);

    return res;
#endif
}




#if defined (__cplusplus)
}
#endif
