/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA shot Information store
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_info.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2016/03/21 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <math.h>
#include <string.h>

#if defined(_WIN32)
#include <direct.h>
#endif
#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"
#include "cr_nanoxml.h"

#include "scamif.h"
#include "scamif_buffer.h"

#include "iana.h"
#include "iana_work.h"
#include "iana_implement.h"
#include "iana_tool.h"
#include "iana_cam.h"
#include "iana_checkready.h"
#include "iana_balldetect.h"
#include "iana_ready.h"
#include "iana_info.h"
#include "iana_shot.h"

#if defined(_WIN32)
#include <tchar.h>
#endif
/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

#define INFOFILE "infomode.txt"

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/

/*!
 ********************************************************************************
 *	@brief      Init info, make directory 
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuks
 *  @date       2016/03/21
 *******************************************************************************/
I32 iana_info_init(HAND hiana, U32 goodshot, U32 checkme)
{
	iana_t *piana;
	iana_info_t	*piif;
	U32 infomode;
	U32 useinfofile;

	//--
	piana = (iana_t *)hiana;
	piif = &piana->iif;
	memcpy(&piif->guid[0], (char *) &piana->shotguid, sizeof(CR_GUID));

	useinfofile = 1;


	
	if (useinfofile) 
	{
		FILE *fp;
		infomode = piana->infomode;
		fp = cr_fopen(INFOFILE, "r");

		if (fp) {

			fscanf(fp, "%d", &infomode);

			cr_fclose(fp);

			if (infomode >= IANA_INFO_INFO_IMAGE) {
				infomode = IANA_INFO_INFO_IMAGE;
			}
		}
		piana->infomode = infomode;
	} else {
		piana->infomode = infomode = IANA_INFO_NO;
	}

	if ((infomode == IANA_INFO_INFO_IMAGE) || (infomode == IANA_INFO_INFO_CHECK && checkme != 0) )
	{
		char buf[1024];

		//---
		if (g_datapath.mode == 0)  {
#if defined(_WIN32)			
			saveimageprepare(".\\", (char *)(&piana->shotdatapath[0]), goodshot, checkme);
#else
			saveimageprepare("./", (char *)(&piana->shotdatapath[0]), goodshot, checkme);
#endif
		} else {
			saveimageprepare(g_datapath.sDataDir, (char *)(&piana->shotdatapath[0]), goodshot, checkme);
		}
		memcpy(&piif->infopath[0], &piana->shotdatapath[0], 1024);

#if defined(_WIN32)
		sprintf(buf, "%s\\info.xml", piif->infopath);
#else
		sprintf(buf, "%s/info.xml", piif->infopath);
#endif
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Info data path: %s\n", piif->infopath);
		cr_nanoxml_create2(buf, 0);
	}


	if (piana->infomode == IANA_INFO_INFO_IMAGE) {
		piana->info_saveimage = 1;
	} else if (piana->infomode == IANA_INFO_INFO_CHECK) {
		if (checkme) {
			piana->info_saveimage = 1;
		} else {
			piana->info_saveimage = 0;
		}
	} else {
		piana->info_saveimage = 0;
	}


//#define ENCDATASTORE

#if defined(ENCDATASTORE)
	{
		cr_mkdir("TMPDATA");
		cr_nanoxml_create2("TMPDATA\\info.bin", 1);
	}
#endif

	return 1;
}

/*!
 ********************************************************************************
 *	@brief      Start info
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2016/03/21
 *******************************************************************************/
I32 iana_info_start(HAND hiana, U32 goodshot, U32 checkme)
{
	iana_t *piana;
	iana_info_t	*piif;

	//--
	piana = (iana_t *)hiana;
	piif = &piana->iif;
	iana_info_init((HAND)piana, goodshot, checkme);
	//if (piana->infomode != IANA_INFO_NO) 
	{
		U32 camid;
		char buf[1024];
		CR_GUID *pguid;


		//---
		nxmlie(0, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		nxmlie(1, "<SHOTINFO>");

		nxmlie(1, "<sensorinfo>");
		{
			nxmlie(0, "<id> %d </id>", CAMSENSOR_CATEGORY);
			nxmlie(0, "<version>%d.%d.%d</version>", CAMSENSOR_MAJOR, CAMSENSOR_MINOR, CAMSENSOR_BUILD);
			nxmlie(0, "<date>%s %s</date>", __DATE__, __TIME__);
		}
		nxmlie(-1, "</sensorinfo>");

		{
			CR_Time_t st;
			OSAL_SYS_GetLocalTime( &st);
			nxmlie(0, "<time> %4d-%02d-%02d %02d:%02d:%02d </time>",
					(U32) st.wYear,
					(U32) st.wMonth,
					(U32) st.wDay,
					(U32) st.wHour,
					(U32) st.wMinute,
					(U32) st.wSecond);
		}

		pguid = (CR_GUID *) piif->guid;
		sprintf(buf,
				"%08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X",
				(U32) pguid->Data1,  
				(U32) pguid->Data2,  
				(U32) pguid->Data3,  
				(U32) pguid->Data4[0],   
				(U32) pguid->Data4[1],   
				(U32) pguid->Data4[2],   
				(U32) pguid->Data4[3],   
				(U32) pguid->Data4[4],   
				(U32) pguid->Data4[5],   
				(U32) pguid->Data4[6],   
				(U32) pguid->Data4[7]
			   );
		nxmlie(0, "<guid>%s</guid>", buf);

		nxmlie(0, "<camsensor_category>%d</camsensor_category>", piana->camsensor_category);

		nxmlie(0, "<UseLeftHanded>%d</UseLeftHanded>", piana->useLeftHanded);
		nxmlie(0, "<LeftHandedType>%d</LeftHandedType>", piana->leftHandedType);

		nxmlie(0, "<CurrentHanded> %d </CurrentHanded>", piana->Right0Left1);						// Right 0, Left 1

		nxmlie(0, "<CamPosEstimatedLValid> %d </CamPosEstimatedLValid>", piana->RS_CamPosEstimatedLValid);
		nxmlie(0, "<LS_CamPosEstimatedLValid> %d </LS_CamPosEstimatedLValid>", piana->LS_CamPosEstimatedLValid);

		nxmlie(0, "<UseProjectionPlane> %d </UseProjectionPlane>", piana->RS_UseProjectionPlane);
		nxmlie(0, "<LS_UseProjectionPlane> %d </LS_UseProjectionPlane>", piana->LS_UseProjectionPlane);

		nxmlie(0, "<UseAttackCam> %d </UseAttackCam>", piana->RS_UseAttackCam);
		nxmlie(0, "<LS_UseAttackCam> %d </LS_UseAttackCam>", piana->LS_UseAttackCam);


		nxmlie(1, "<config>");
		{
			cr_rectL_t *prectLTee;
			cr_rectL_t *prectLIron;
			cr_rectL_t *prectLPutter;
			
			prectLTee 		= piana->prectLTee;
			prectLIron 		= piana->prectLIron;
			prectLPutter 	= piana->prectLPutter;

			nxmlie(0, "<teearea> %lf %lf %lf %lf </teearea>", 
					prectLTee->sx, prectLTee->sy, prectLTee->ex, prectLTee->ey);
			nxmlie(0, "<ironarea> %lf %lf %lf %lf </ironarea>", 
					prectLIron->sx, prectLIron->sy, prectLIron->ex, prectLIron->ey);
			nxmlie(0, "<putterarea> %lf %lf %lf %lf </putterarea>", 
					prectLPutter->sx, prectLPutter->sy, prectLPutter->ex, prectLPutter->ey);
		}
		nxmlie(-1, "</config>");

		nxmlie(1,  "<cameras>");
		for (camid = 0; camid < NUM_CAM; camid++) {
			iana_cam_t			*pic;
			iana_cam_param_t 	*picp;
			cr_point_t 			*pCamPosL;
			biquad_t			*pbq;
			U64					*p64;

			nxmlie(1,  "<camera id=\"%d\">", camid);

			nxmlie(0, "<processmode>%d</processmode>", piana->processmode[camid]);	//
			
			pic 	= piana->pic[camid];
			picp 	= &pic->icp;
			pCamPosL= &picp->CamPosL;


			nxmlie(0, "<ce>%lf</ce>", picp->CerBallexist);
			nxmlie(0, "<cs>%lf</cs>", picp->CerBallshot);
			nxmlie(0, "<bmax>%lf</bmax>", picp->BallsizeMax); 
			nxmlie(0, "<bmin>%lf</bmin>", picp->BallsizeMin);

			nxmlie(0,  "<position> %10.6lf %10.6lf %10.6lf </position>",
					pCamPosL->x,
					pCamPosL->y,
					pCamPosL->z);

//			nxmlie(0,  "<ValidpositionEstimated> %d </ValidpositionEstimated>", picp->CamPosEstimatedLValid);
			if (*piana->pCamPosEstimatedLValid) {
				nxmlie(0,  "<positionEstimated> %10.6lf %10.6lf %10.6lf </positionEstimated>",
						picp->CamPosEstimatedL.x,
						picp->CamPosEstimatedL.y,
						picp->CamPosEstimatedL.z);
			}

			if (*piana->pUseProjectionPlane) {
				nxmlie(0,  "<ProjectionPlaneCenter> %10.6lf %10.6lf %10.6lf </ProjectionPlaneCenter>",
						picp->ProjectionPlaneCenter.x,
						picp->ProjectionPlaneCenter.y,
						picp->ProjectionPlaneCenter.z);
			}

			{
				cameraintrinsic_t *pcip;
				pcip = &picp->cameraintrinsic;
				nxmlie(1, "<cameraintrinsic id = \"%d\">", pcip->id);
				{
					nxmlie(0, "<intrinsicparam> %le %le %le %le </intrinsicparam>", 
							pcip->cameraParameter[0],
                            pcip->cameraParameter[1],
                            pcip->cameraParameter[2],
                            pcip->cameraParameter[3]);
					nxmlie(0, "<distortparam> %le %le %le %le %le </distortparam>", 
							pcip->distCoeffs[0],
                            pcip->distCoeffs[1],
                            pcip->distCoeffs[2],
                            pcip->distCoeffs[3],
                            pcip->distCoeffs[4] 
							);
				}
				nxmlie(-1, "</cameraintrinsic>");
			}

			{		// Extrinsic...
				cameraextrinsic_t *pcep;

				//--
				pcep = &picp->cep;
				nxmlie(1,  "<ExtrinsicParams>");
				{
					//U64			*p64;

					nxmlie(0, "<ExtrinsicParamValid>%d</ExtrinsicParamValid>", picp->CamExtrinsicValid);
					if (picp->CamExtrinsicValid) {
						//cameraextrinsic_t *pcep;
						//pcep = &picp->cep;
						{
							nxmlie(1, "<RotationVector>");
							{
								p64 = (U64 *)&pcep->rotationVector[0]; 		//double rotationVector[3];		

								sprintf(buf, "0x%08llx 0x%08llx 0x%08llx", *(p64+ 0), *(p64+ 1), *(p64+ 2));
								nxmlie(0, buf);
							}
							nxmlie(-1, "</RotationVector>");

							nxmlie(1, "<TranslationVector>");
							{
								p64 = (U64 *)&pcep->translationVector[0]; //double translationVector[3];		

								sprintf(buf, "0x%08llx 0x%08llx 0x%08llx", *(p64+ 0), *(p64+ 1), *(p64+ 2));
								nxmlie(0, buf);
							}
							nxmlie(-1, "</TranslationVector>");

							nxmlie(1, "<rotationMatrix>");					
							{
								p64 = (U64 *)&pcep->r[0][0]; 			//double r[3][3];	// Rodridues matrix

								sprintf(buf, "0x%08llx 0x%08llx 0x%08llx", *(p64+ 0), *(p64+ 1), *(p64+ 2));
								nxmlie(0, buf);
								sprintf(buf, "0x%08llx 0x%08llx 0x%08llx", *(p64+ 3), *(p64+ 4), *(p64+ 5));
								nxmlie(0, buf);
								sprintf(buf, "0x%08llx 0x%08llx 0x%08llx", *(p64+ 6), *(p64+ 7), *(p64+ 8));
								nxmlie(0, buf);
							}
							nxmlie(-1, "</rotationMatrix>");

							nxmlie(1, "<ExtMat>");					
							{
								p64 = (U64 *)&pcep->extMat[0][0]; 			//double extMat[4][4];			// [[R|t];[0 0 0 1]]

								sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 0), *(p64+ 1), *(p64+ 2), *(p64+ 3));
								nxmlie(0, buf);
								sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 4), *(p64+ 5), *(p64+ 6), *(p64+ 7));
								nxmlie(0, buf);
								sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 8), *(p64+ 9), *(p64+10), *(p64+11));
								nxmlie(0, buf);
								sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+12), *(p64+13), *(p64+14), *(p64+15));
								nxmlie(0, buf);
							}
							nxmlie(-1, "</ExtMat>");

							nxmlie(1, "<InvExtMat>");					
							{
								p64 = (U64 *)&pcep->InvextMat[0][0]; 		//double InvextMat[4][4];

								sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 0), *(p64+ 1), *(p64+ 2), *(p64+ 3));
								nxmlie(0, buf);
								sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 4), *(p64+ 5), *(p64+ 6), *(p64+ 7));
								nxmlie(0, buf);
								sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 8), *(p64+ 9), *(p64+10), *(p64+11));
								nxmlie(0, buf);
								sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+12), *(p64+13), *(p64+14), *(p64+15));
								nxmlie(0, buf);
							}
							nxmlie(-1, "</InvExtMat>");
						}
					}
				}
				nxmlie(-1,  "</ExtrinsicParams>");
			}


			//pbq = &picp->bAp2l;
			pbq = &picp->bAp2l_raw;
			p64 = (U64 *)&pbq->mat[0];
			nxmlie(1, "<transmat>");
			/*sprintf(buf, "%08llx %08llx %08llx %08llx %08llx %08llx %08llx %08llx %08llx %08llx %08llx %08llx </transmat>",
			 *(p64+ 0), *(p64+ 1), *(p64+ 2), *(p64+ 3), *(p64+ 4), *(p64+ 5),
			 *(p64+ 6), *(p64+ 7), *(p64+ 8), *(p64+ 9), *(p64+10), *(p64+11));
			 */
			sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 0), *(p64+ 1), *(p64+ 2), *(p64+ 3));
			nxmlie(0, buf);
			sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 4), *(p64+ 5), *(p64+ 6), *(p64+ 7));
			nxmlie(0, buf);
			sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 8), *(p64+ 9), *(p64+10), *(p64+11));
			nxmlie(0, buf);
			nxmlie(-1, "</transmat>");

			nxmlie(1, "<transmat>");
			//pbq = &picp->bAl2p;
			pbq = &picp->bAl2p_raw;
			p64 = (U64 *)&pbq->mat[0];
			sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 0), *(p64+ 1), *(p64+ 2), *(p64+ 3));
			nxmlie(0, buf);
			sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 4), *(p64+ 5), *(p64+ 6), *(p64+ 7));
			nxmlie(0, buf);
			sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 8), *(p64+ 9), *(p64+10), *(p64+11));
			nxmlie(0, buf);
			nxmlie(-1, "</transmat>");

			if (picp->quad_UdValid) {			// Cali param for Undistorted ...

				pbq = &picp->bAp2l_Ud_raw;
				p64 = (U64 *)&pbq->mat[0];
				nxmlie(1, "<transmat>");
				/*sprintf(buf, "%08llx %08llx %08llx %08llx %08llx %08llx %08llx %08llx %08llx %08llx %08llx %08llx </transmat>",
				 *(p64+ 0), *(p64+ 1), *(p64+ 2), *(p64+ 3), *(p64+ 4), *(p64+ 5),
				 *(p64+ 6), *(p64+ 7), *(p64+ 8), *(p64+ 9), *(p64+10), *(p64+11));
				 */
				sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 0), *(p64+ 1), *(p64+ 2), *(p64+ 3));
				nxmlie(0, buf);
				sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 4), *(p64+ 5), *(p64+ 6), *(p64+ 7));
				nxmlie(0, buf);
				sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 8), *(p64+ 9), *(p64+10), *(p64+11));
				nxmlie(0, buf);
				nxmlie(-1, "</transmat>");

				nxmlie(1, "<transmat>");
				pbq = &picp->bAl2p_Ud_raw;
				p64 = (U64 *)&pbq->mat[0];
				sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 0), *(p64+ 1), *(p64+ 2), *(p64+ 3));
				nxmlie(0, buf);
				sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 4), *(p64+ 5), *(p64+ 6), *(p64+ 7));
				nxmlie(0, buf);
				sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *(p64+ 8), *(p64+ 9), *(p64+10), *(p64+11));
				nxmlie(0, buf);
				nxmlie(-1, "</transmat>");
			}

			{
				U32 count;
				U32 i;
				lpdatapair_t *plp;

				//--
				count = picp->lpdatapaircount;	
				if (count > 0 && count <= LPCOUNTMAX) {
					nxmlie(1, "<pixellocalpoints count = \"%d\">", count);
					plp = &piana->pic[camid]->icp.lpdatapair[0];
					for (i = 0; i < count; i++) {
						U64 *plx, *ply, *ppx, *ppy;
						plx = (U64 *)&plp[i].Lp.x;
						ply = (U64 *)&plp[i].Lp.y;
						ppx = (U64 *)&plp[i].Pp.x;
						ppy = (U64 *)&plp[i].Pp.y;

						sprintf(buf, "0x%08llx 0x%08llx 0x%08llx 0x%08llx", *plx, *ply, *ppx, *ppy);
						nxmlie(0, buf);
					}
					nxmlie(-1, "</pixellocalpoints>");
				}
			}
			nxmlie(-1,  "</camera>");
		}
		nxmlie(-1, "</cameras>");
	}
	return 1;
}

/*!
 ********************************************************************************
 *	@brief      cam run config
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2016/06/30
 *******************************************************************************/
I32 iana_info_camrunconfig(HAND hiana)
{
	U32 camid;
	iana_t *piana;
	
	//--
	piana = (iana_t *)hiana;

	//if (piana->infomode == IANA_INFO_NO) 
	//{
	//	goto func_exit;
	//}
	//--
	nxmlie(1, "<camrunconfig>");
	{
	
		nxmlie(0, "<checkreadycamid>%d</checkreadycamid>", piana->camidCheckReady);
		nxmlie(0, "<othercamid>%d</othercamid>", piana->camidOther);
		nxmlie(0, "<ts64delta>0x%I64x</ts64delta>", piana->ts64delta);
		nxmlie(0, "<ts64deltadec>%16lld %lf</ts64deltadec>", piana->ts64delta, (piana->ts64delta/1000000.0));
		nxmlie(0, "<allowTee>%d</allowTee>", 		piana->allowedarea[IANA_AREA_TEE]);
		nxmlie(0, "<allowIron>%d</allowIron>", 		piana->allowedarea[IANA_AREA_IRON]);
		nxmlie(0, "<allowPutter>%d</allowPutter>",	piana->allowedarea[IANA_AREA_PUTTER]);
		nxmlie(0, "<processarea>%d</processarea>",	piana->processarea);
		nxmlie(0, "<shotarea>%d</shotarea>",		piana->shotarea);

		for (camid = 0; camid < NUM_CAM; camid++) {
			iana_camconf_t *picf;

			picf = &piana->pic[camid]->icf;
			nxmlie(1, "<camera id = \"%d\">", camid);
			{
				nxmlie(1, "<config>");
				{
					nxmlie(0, "<width>%d</width>", picf->width);
					nxmlie(0, "<height>%d</height>", picf->height);
					nxmlie(0, "<offset_x>%d</offset_x>", picf->offset_x);
					nxmlie(0, "<offset_y>%d</offset_y>", picf->offset_y);
					nxmlie(0, "<framerate>%d</framerate>", picf->framerate);
					nxmlie(0, "<gain>%d</gain>", picf->gain);
					nxmlie(0, "<exposure>%d</exposure>", picf->exposure);
					nxmlie(0, "<skip>%d</skip>", picf->skip);
					nxmlie(0, "<multitude>%d</multitude>", picf->multitude);
					nxmlie(0, "<syncdiv>%d</syncdiv>", picf->syncdiv);
				}
				nxmlie(-1, "</config>");
				nxmlie(1, "<runconfig>");
				{
					nxmlie(0, "<runprocessingwidth>%d</runprocessingwidth>", piana->pic[camid]->runProcessingWidth);
					nxmlie(0, "<runprocessingheight>%d</runprocessingheight>", piana->pic[camid]->runProcessingHeight);
					nxmlie(0, "<rungain0>%d</rungain0>", piana->pic[camid]->rungain0);
					nxmlie(0, "<rungain>%d</rungain>", piana->pic[camid]->rungain);
				}
				nxmlie(-1, "</runconfig>");


			}
			nxmlie(-1, "</camera>");
		}
	}
	nxmlie(-1, "</camrunconfig>");

//func_exit:
	return 1;
}


/*!
 ********************************************************************************
 *	@brief      ready info and image
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2016/06/30
 *******************************************************************************/
I32 iana_info_ready(HAND hiana)
{
	U32 camid;
	U32 res;
	iana_t *piana;
	piana = (iana_t *)hiana;

	//--
	//if (piana->infomode == IANA_INFO_NO) 
	//{
	//	goto func_exit;
	//}

	nxmlie(1, "<ready>");
	{
		for (camid = 0; camid < NUM_CAM; camid++) {
			U32 rindexshot;
			U32 rindex;
			U32 i;
			iana_cam_t	*pic;
			U08 *buf;
			camif_buffer_info_t *pb;
			I32 preshotimagecount;

			pic = piana->pic[camid];
			rindexshot = pic->rindexshot;

//////////#define READYIMAGE_PRESHOT	5
///////////////////#define READYIMAGE_PRESHOT	4
//////////#define READYIMAGE_PRESHOT	8
////////#define READYIMAGE_PRESHOT	4
//////////#define READYIMAGE_PRESHOT	2

//#define INFOIMAGECOUNT		24
#define PRESHOTIMAGE_PUTTER_COUNT	10
#define PRESHOTIMAGECOUNT	8

			
			nxmlie(1, "<camera id=\"%d\">", camid);
			{
				nxmlie(1, "<start>");
				{
					nxmlie(0, "<ballpos>%f %f %f</ballpos>", pic->icp.L.x, pic->icp.L.y, pic->icp.L.z);
					nxmlie(0, "<ballposP>%f %f %f</ballposP>", pic->icp.P.x, pic->icp.P.y, pic->icp.P.z);
					nxmlie(0, "<ballposB>%f %f %f</ballposB>", pic->icp.b3d.x, pic->icp.b3d.y, pic->icp.b3d.z);
				}
				nxmlie(-1, "</start>");
				//if (piana->infomode == IANA_INFO_INFO_IMAGE) 
				if (piana->info_saveimage)
				{
					nxmlie(1, "<images>");
					{
						U32 imagecount;
						if (piana->processarea == IANA_AREA_PUTTER) {
							imagecount = INFOIMAGE_PUTTER_COUNT;
							preshotimagecount = PRESHOTIMAGE_PUTTER_COUNT;
						} else {
							imagecount = INFOIMAGE_COUNT;
							preshotimagecount = PRESHOTIMAGECOUNT;
						}



//						for (i = 0; i < READCOUNT_CANDIDATE + READYIMAGE_PRESHOT; i++) 
						for (i = 0; i < imagecount /*INFOIMAGECOUNT*/; i++) 
						{
//							rindex = (rindexshot + i - READYIMAGE_PRESHOT);
							rindex = (rindexshot + i - PRESHOTIMAGECOUNT);

							res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_NORMAL, rindex, &pb, &buf);

							if (res) {
								nxmlie(1, "<image>");
								{
									U64 ts64;
									U08 filename[1024];
									camimageinfo_t	*pcii;

									pcii = &pb->cii;
									ts64 = MAKEU64(pb->ts_h, pb->ts_l);
									if (piana->iif.fileformat == 1) {					// 0: Bitmat, 1: jpeg
										//saveimageJpeg(piana->hjpeg, piana->iif.qvalue, (char *)&piana->shotdatapath[0], "r", (char *)buf, pb->cii.width, pb->cii.height, camid, i, (char *)&filename[0]);
										saveimageJpeg(piana->hjpeg, 100, (char *)&piana->shotdatapath[0], "r", (char *)buf, pb->cii.width, pb->cii.height, camid, i, (char *)&filename[0]);
									} else //if (piana->iif.fileformat == 0) // 0: Bitmat, 1: jpeg
									{					
										saveimage3((char *)&piana->shotdatapath[0], "r", (char *)buf, pb->cii.width, pb->cii.height, camid, i, (char *)&filename[0]);
									}
									nxmlie(0, "<index>%d</index>", rindex);
									nxmlie(0, "<ts64>0x%I64x</ts64>", ts64);
									nxmlie(0, "<ts64dec>%16lld %lf</ts64dec>", ts64, (ts64/1000000.0));
									nxmlie(0, "<offset_x>%d</offset_x>", 	pcii->offset_x);
									nxmlie(0, "<offset_y>%d</offset_y>", 	pcii->offset_y);

									nxmlie(0, "<filename>%s</filename>", filename);
								}
								nxmlie(-1, "</image>");
							}
						}
					}
					nxmlie(-1, "</images>");
				}
			}
			nxmlie(-1, "</camera>");
		} 		// for (camid = 0; camid < 2; camid++) 
	}
	nxmlie(-1, "</ready>");

//func_exit:

	return 1;
}


/*!
 ********************************************************************************
 *	@brief      shot info
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2016/07/01
 *******************************************************************************/
I32 iana_info_shot(HAND hiana)
{
	U32 camid;
	U32 res;
	iana_t *piana;
	piana = (iana_t *)hiana;

	//--
	//if (piana->infomode == IANA_INFO_NO) 
	//{
	//	goto func_exit;
	//}

	nxmlie(1, "<shot>");
//	if (piana->shotresultflag == 0) {
//		nxmlie(0, "<goodshot>%d</goodshot>", piana->shotresultflag); 
//	} else 
	{
		nxmlie(0, "<goodshot>%d</goodshot>", piana->shotresultflag); 
		for (camid = 0; camid < NUM_CAM; camid++) {
			U32 rindexshot;
			iana_cam_t	*pic;
			U08 *buf;
			camif_buffer_info_t *pb;
			U64 ts64;

			pic = piana->pic[camid];
			rindexshot = pic->rindexshot;

			{
				nxmlie(1, "<start camid = \"%d\">", camid);
				{					
					res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_NORMAL, rindexshot, &pb, &buf);
					 
					//ts64 = MAKEU64(pb->ts_h, pb->ts_l);
					ts64 = pic->ts64shot;
					nxmlie(0, "<ts64>0x%I64x</ts64>", ts64);
					nxmlie(0, "<ts64dec>%16lld %lf</ts64dec>", ts64, (ts64/1000000.0));
					nxmlie(0, "<index>%d</index>", rindexshot);
					nxmlie(0, "<ballpos>%f %f %f</ballpos>", pic->icp.L.x, pic->icp.L.y, pic->icp.L.z);
					nxmlie(0, "<ballposP>%f %f %f</ballposP>", pic->icp.P.x, pic->icp.P.y, pic->icp.P.z);
					nxmlie(0, "<ballposB>%f %f %f</ballposB>", pic->icp.b3d.x, pic->icp.b3d.y, pic->icp.b3d.z);
					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] ts64: %016llx(%lf)\n", camid, ts64, ts64/TSSCALE_D);

				}
				nxmlie(-1, "</start>");
			}
		}

		nxmlie(1, "<shotcalc>");
		{


		}
		nxmlie(-1, "</shotcalc>");

	}
	nxmlie(-1, "</shot>");

//func_exit:

	return 1;
}

/*!
 ********************************************************************************
 *	@brief      spin info and bulk image
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2016/07/01
 *******************************************************************************/
I32 iana_info_spin(HAND hiana)
{
	U32 camid;
	U32 res;
	iana_t *piana;
	piana = (iana_t *)hiana;

	//--
	//if (piana->infomode == IANA_INFO_NO) 
	//{
	//	goto func_exit;
	//}
	//u

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++\n");
	nxmlie(1, "<spin>");
	{
 		nxmlie(1, "<bulkimages>");
		{
			for (camid = 0; camid < NUM_CAM; camid++) {
				U32 		i;
				U32 		rindexshot;
				iana_cam_t	*pic;
				U32 		firstbi;
				U32 		rindex;
				U08 		*buf;
				camif_buffer_info_t *pb;
				camimageinfo_t	*pcii;
//				U64 		ts64;

				//--

				nxmlie(1,  "<camera id=\"%d\">", camid);
				{
					unsigned int bulkimagecount;
					pic = piana->pic[camid];
					rindexshot = pic->rindexshot;
					firstbi = pic[0].firstbulkindex;
					if (piana->camsensor_category == CAMSENSOR_EYEXO) {
						bulkimagecount = MARKSEQUENCELEN_EYEXO;
					} else {
						bulkimagecount = MARKSEQUENCELEN;
					}
					for (i = 0; i < bulkimagecount; i++) {
						rindex = i + firstbi;

						res =  scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, rindex, &pb, &buf);

						if (res == 0) {
							cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail..Break\n");
							break;
						}
						pcii = &pb->cii;
						if (i == 0) {		// First frame..
						//	pcii = &pb->cii;
							nxmlie(1, "<config>");
							{
								nxmlie(0, "<width>%d</width>", 			pcii->width);
								nxmlie(0, "<height>%d</height>", 		pcii->height);
								nxmlie(0, "<offset_x>%d</offset_x>", 	pcii->offset_x);
								nxmlie(0, "<offset_y>%d</offset_y>", 	pcii->offset_y);
								nxmlie(0, "<framerate>%d</framerate>", 	pcii->framerate);
								nxmlie(0, "<gain>%d</gain>", 			pcii->gain);
								nxmlie(0, "<exposure>%d</exposure>", 	pcii->exposure);
								nxmlie(0, "<skip>%d</skip>", 			pcii->skip);
								nxmlie(0, "<multitude>%d</multitude>", 	pcii->multitude);
								nxmlie(0, "<syncdiv>%d</syncdiv>", 		pcii->syncdiv);
							}
							nxmlie(-1, "</config>");
						}

						//if (piana->infomode == IANA_INFO_INFO_IMAGE) 
						if (piana->info_saveimage)
						{
							nxmlie(1, "<image>");
							{
								U64 ts64;
								U08 filename[1024];

								//--
								ts64 = MAKEU64(pb->ts_h, pb->ts_l);

								if (piana->iif.fileformat == 1) {					// 0: Bitmat, 1: jpeg
									//saveimageJpeg(piana->hjpeg, piana->iif.qvalue, (char *)&piana->shotdatapath[0], "b", (char *)buf, pb->cii.width, pb->cii.height, camid, i, (char *)&filename[0]);
									saveimageJpeg(piana->hjpeg, 100, (char *)&piana->shotdatapath[0], "b", (char *)buf, pb->cii.width, pb->cii.height, camid, i, (char *)&filename[0]);
								} else //if (piana->iif.fileformat == 0) // 0: Bitmat, 1: jpeg
								{					
									saveimage3((char *)&piana->shotdatapath[0], "b", (char *)buf, pb->cii.width, pb->cii.height, camid, i, (char *)&filename[0]);
								}

								nxmlie(0, "<index>%d</index>", rindex);
								nxmlie(0, "<ts64>0x%I64x</ts64>", ts64);
								nxmlie(0, "<ts64dec>%16lld %lf</ts64dec>", ts64, (ts64/1000000.0));
								nxmlie(0, "<offset_x>%d</offset_x>", 	pcii->offset_x);
								nxmlie(0, "<offset_y>%d</offset_y>", 	pcii->offset_y);

								nxmlie(0, "<filename>%s</filename>", filename);
							}
							nxmlie(-1, "</image>");
						} else {
							break;
						}
					} 	// for (i = 0; i < MARKSEQUENCELEN; i++) 
				}
				nxmlie(-1,  "</camera>");
			} 			// for (camid = 0; camid < 2; camid++)
		}
		nxmlie(-1, "</bulkimages>");
	}
	nxmlie(-1, "</spin>");

//func_exit:
	//if (piana->infomode == IANA_INFO_INFO_IMAGE) 
	if (piana->info_saveimage)
	{
		for (camid = 0; camid < NUM_CAM; camid++) {
			if (piana->processmode[camid] == CAMPROCESSMODE_BALLCLUB) {
				char filename[PATHBUFLEN];
				int width;
				int height;

				iana_cam_t	*pic;

				//--
				pic 		= piana->pic[camid];
				width 		= piana->clubball_shotresult[camid].width;
				height 		= piana->clubball_shotresult[camid].height;
#define CLIBFILE_MINWH	64
				if (width >= CLIBFILE_MINWH && height >= CLIBFILE_MINWH) {
					//sprintf(filename, "%s\\%s%d.jpg", piana->shotdatapath, CLUBFILENAME, camid);
					sprintf(filename, "%s%d.jpg", CLUBFILENAME, camid);
					//saveimageJpeg2(piana->hjpeg, 
					//		CLUBFILEQVALUE,
					//		(char *)piana->shotdatapath,
					//		filename,
					//		(char *)pic->pimgClubOverlap,
					//		width, 
					//		height);

					saveimageJpegBGR(piana->hjpeg, 
							CLUBFILEQVALUE,
							(char *)piana->shotdatapath,
							filename,
							(char *)pic->pimgClubOverlap,
							width, 
							height);
				}
			}
		}
	}

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"--\n");
	return 1;
}

void IANA_INFO_WriteShotResultToXml(iana_shotresult_t *psr, double spinmag2d, double spinaxis2d)
{
	nxmlie(0, "<vmag>%f</vmag>", 		(float)psr->vmag);
	nxmlie(0, "<incline>%f</incline>", 	(float)psr->incline);
	nxmlie(0, "<azimuth>%f</azimuth>",	(float)psr->azimuth);

	// Club data
	nxmlie(0, "<clubpath>%f</clubpath>",(float)psr->clubpath);
	nxmlie(0, "<Assurance_clubpath>%d</Assurance_clubpath>", psr->Assurance_clubpath);
	nxmlie(0, "<clubfaceangle>%f</clubfaceangle>",(float)psr->clubfaceangle);
	nxmlie(0, "<Assurance_clubfaceangle>%d</Assurance_clubfaceangle>",psr->Assurance_clubfaceangle);
	nxmlie(0, "<clubspeed_B>%f</clubspeed_B>",(float)psr->clubspeed_B);
	nxmlie(0, "<Assurance_clubspeed_B>%d</Assurance_clubspeed_B>",psr->Assurance_clubspeed_B);
	nxmlie(0, "<clubspeed_A>%f</clubspeed_A>",(float)psr->clubspeed_A);
	nxmlie(0, "<Assurance_clubspeed_A>%d</Assurance_clubspeed_A>",psr->Assurance_clubspeed_A);

	nxmlie(0, "<clubattackangle>%f</clubattackangle>",(float)psr->clubattackangle);
	nxmlie(0, "<Assurance_clubattackangle>%d</Assurance_clubattackangle>",psr->Assurance_clubattackangle);
	nxmlie(0, "<clubloftangle>%f</clubloftangle>",(float)psr->clubloftangle);
	nxmlie(0, "<Assurance_clubloftangle>%d</Assurance_clubloftangle>",psr->Assurance_clubloftangle);
	nxmlie(0, "<clublieangle>%f</clublieangle>",(float)psr->clublieangle);
	nxmlie(0, "<Assurance_clublieangle>%d</Assurance_clublieangle>",psr->Assurance_clublieangle);
	nxmlie(0, "<clubfaceimpactLateral>%f</clubfaceimpactLateral>",(float)psr->clubfaceimpactLateral);
	nxmlie(0, "<Assurance_clubfaceimpactLateral>%d</Assurance_clubfaceimpactLateral>",psr->Assurance_clubfaceimpactLateral);
	nxmlie(0, "<clubfaceimpactVertical>%f</clubfaceimpactVertical>",(float)psr->clubfaceimpactVertical);
	nxmlie(0, "<Assurance_clubfaceimpactVertical>%d</Assurance_clubfaceimpactVertical>",psr->Assurance_clubfaceimpactVertical);
	// End of Club data

	nxmlie(0, "<backspin>%f</backspin>",(float)psr->backspin);
	nxmlie(0, "<sidespin>%f</sidespin>",(float)psr->sidespin);
	nxmlie(0, "<spinmag>%f</spinmag>",	(float)psr->spinmag);
	nxmlie(0, "<spinaxis>%f %f %f</spinaxis>",	
			(float)psr->axis.x,
			(float)psr->axis.y,
			(float)psr->axis.z);
	nxmlie(0, "<spinmag2d>%f</spinmag2d>",	(float)spinmag2d);
	nxmlie(0, "<spinaxis2d>%f</spinaxis2d>",	(float)spinaxis2d);
}

/*!
 ********************************************************************************
 *	@brief      shot result info
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2016/07/01
 *******************************************************************************/
I32 iana_info_shotresult(HAND hiana)
{
	iana_t *piana;
	piana = (iana_t *)hiana;
	iana_shotresult_t *psr;
	double spinmag2d;
	double spinaxis2d;

	//--
	//if (piana->infomode == IANA_INFO_NO) 
	//{
	//	goto func_exit;
	//}

	psr = &piana->shotresultdata;
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "2.. vmag: %lf\n", psr->vmag);

	spinmag2d 	= psr->backspin * psr->backspin + psr->sidespin * psr->sidespin;
	spinmag2d 	= sqrt(spinmag2d);
	spinaxis2d 	= atan2(psr->sidespin, psr->backspin);	
	spinaxis2d 	= spinaxis2d *180.0 / 3.141592;

	nxmlie(1, "<shotresult>");

	IANA_INFO_WriteShotResultToXml(psr, spinmag2d, spinaxis2d);
	
	nxmlie(-1, "</shotresult>");
	
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		psr = &piana->shotresultdata2;
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "2.. vmag: %lf\n", psr->vmag);

		spinmag2d 	= psr->backspin * psr->backspin + psr->sidespin * psr->sidespin;
		spinmag2d 	= sqrt(spinmag2d);
		spinaxis2d 	= atan2(psr->sidespin, psr->backspin);	
		spinaxis2d 	= spinaxis2d *180.0 / 3.141592;

		nxmlie(1, "<shotresult2>");

		IANA_INFO_WriteShotResultToXml(psr, spinmag2d, spinaxis2d);

		nxmlie(-1, "</shotresult2>");
	}

//func_exit:

	return 1;
}


/*!
 ********************************************************************************
 *	@brief      feature information..
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2019/11/12
 *******************************************************************************/
I32 iana_info_feature(HAND hiana)
{
	U32 camid;
	U32 res;
	iana_t *piana;

	//---
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++\n");
	piana = (iana_t *)hiana;

	res = 1;
	nxmlie(1, "<features>");
	{
		nxmlie(0, "<ransac_id>%d</ransac_id>", piana->ransac_id);
		nxmlie(0, "<ransac_seed>%d</ransac_seed>", piana->ransac_seed);
		for (camid = 0; camid < NUM_CAM; camid++) {
			U32 seqnum;
			U32 i;
			marksequence_t	*pmks;
			iana_cam_t		*pic;
			char buf[1024];
			U64	*p64;

			//--
			pic 		= piana->pic[camid];
			pmks 		= &pic->mks;

			nxmlie(1,  "<camera id=\"%d\">", camid);
			{
				markelement2_t *pmke2d;
				markelement2_t *pmke2m;
				cr_point_t	*pCamPos;
				I32 marksequencelen;

				//--
				pCamPos = &pic->icp.CamPosL;
				nxmlie(0, "<campos>%20.14lf %10.14lf %10.14lf</campos>", pCamPos->x, pCamPos->y, pCamPos->z);

				if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
					marksequencelen = MARKSEQUENCELEN_EYEXO;
				} else {
					marksequencelen = MARKSEQUENCELEN;
				}

				for (seqnum = 0; seqnum < (U32)marksequencelen; seqnum++) {
					pmke2d = &pmks->mke2Dimple[seqnum];
					pmke2m = &pmks->mke2Mark[seqnum];

					if (pmke2d->dotcount > 0) {
						nxmlie(1, "<feature>");
						{
							point_t			*pPb;
							cr_point_t		*pL3db;
							double			pr_;

							//--
							pPb 	= &pmks->ballposP2[seqnum];			// Ball P position
							pL3db 	= &pmks->ballposL3D[seqnum];
							pr_ 	= pmks->ballrP2[seqnum];
							nxmlie(0, "<index>%d</index>", seqnum);
							nxmlie(0, "<ballposP>%20.14lf %10.14lf</ballposP>", pPb->x, pPb->y);
							nxmlie(0, "<ballRP>%20.15lf</ballRP>", pr_);
							nxmlie(0, "<ballpos3D>%20.14lf %10.14lf %10.14lf</ballpos3D>", pL3db->x, pL3db->y, pL3db->z);
							nxmlie(1,  "<dimple count=\"%d\">", pmke2d->dotcount);
							{
								for (i =0; i < pmke2d->dotcount; i++) {
#if 1
									p64 = (U64 *)&pmke2d->L3bc[i];
									sprintf(buf, "<vect index=\"%d\"> 0x%08llx 0x%08llx 0x%08llx    %20.18lf %20.18lf %20.18lf </vect>",
											i,
											*(p64+ 0), *(p64+ 1), *(p64+ 2),
											pmke2d->L3bc[i].x, 
											pmke2d->L3bc[i].y, 
											pmke2d->L3bc[i].z);
									nxmlie(0, buf);
#endif

								}
							}
							nxmlie(-1, "</dimple>");

							nxmlie(1,  "<othermark count=\"%d\">", pmke2m->dotcount);
							{
								for (i =0; i < pmke2m->dotcount; i++) {
#if 1
									p64 = (U64 *)&pmke2m->L3bc[i];

									sprintf(buf, "<vect index=\"%d\"> 0x%08llx 0x%08llx 0x%08llx    %20.18lf %20.18lf %20.18lf </vect>",
											i,
											*(p64+ 0), *(p64+ 1), *(p64+ 2),
											pmke2m->L3bc[i].x, 
											pmke2m->L3bc[i].y, 
											pmke2m->L3bc[i].z);

									nxmlie(0, buf);
#endif

								}
							}
							nxmlie(-1, "</othermark>");
						}
						nxmlie(-1, "</feature>");
					}
				}
			} 
			nxmlie(-1,  "</camera>");
		}
	}
	nxmlie(-1, "</features>");

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"--\n");
	return res;
}


/*!
 ********************************************************************************
 *	@brief      get feature information XML file path
 *
 *  @param[in]	hIana
 *              IANA module handle
 *  @param[in]	fileName
 *              XML file name 
 *  @param[out]	pathName
 *              XML file path including XML file name
 *
 *	@author	    yhsuk
 *  @date       2019/11/16
 *******************************************************************************/
void IANA_INFO_CreateFeatureXmlPath(HAND hIana, char *fileName, char *pathName)
{
	I32 len = PATHBUFLEN;
	CHR wFileName[PATHBUFLEN];	
	char rootDir[PATHBUFLEN];
	char targetDir[PATHBUFLEN];
	char currentPathName[PATHBUFLEN];
	char strName[PATHBUFLEN];

	I32 i;
	I32 foundit;

	iana_t *pIana = (iana_t *)hIana;

#if defined(_WIN32)		
	cr_sprintf(wFileName, _T("%s%s\\FEATURE"), pIana->szDrive, pIana->szDir);
#else
	cr_sprintf(wFileName, _T("%s%s/FEATURE"), pIana->szDrive, pIana->szDir);
#endif
	OSAL_STR_ConvertUnicode2MultiByte(wFileName, rootDir, len);
	cr_mkdir(rootDir);


	foundit = 0;
	OSAL_FILE_GetCurrentWorkDir(PATHBUFLEN, &currentPathName[0]);
	len = (I32)strlen(currentPathName);
	for (i = len-1; i > 0; i--) {
#if defined(_WIN32)					
		if (currentPathName[i] == '\\') {
#else			
		if (currentPathName[i] == '/') {
#endif
			foundit = 1;
			break;
		}
	}
	if (foundit) {
		sprintf(strName, "%s", &currentPathName[i+1]);
	} else {			// WHAT?
		len = (I32)strlen((char *)&pIana->shottime[0]);
		if (len > 19) {
			int count;
			char strdate[64];
			char strtime[64];
			char strtmp0[64], strtmp1[64], strtmp2[64];
			char strtmp3[64];

			//-
			strdate[0] = 0;
			strtime[0] = 0;
			sscanf((char *)&pIana->shottime[0], "%s %s", &strdate[0], &strtime[0]);

			count = sscanf(&strdate[0], "%4s-%2s-%2s", &strtmp0[0], &strtmp1[0], &strtmp2[0]);
			if (count == 3) {
				sprintf(strtmp3, "data_%4s%2s%2s", strtmp0, strtmp1, strtmp2);
			} else {
				sprintf(strtmp3, "data");
			}

			count = sscanf(&strtime[0], "%2s:%2s:%2s", &strtmp0[0], &strtmp1[0], &strtmp2[0]);
			if (count == 3) {
				sprintf(strName, "%s_%2s%2s_%2s00", strtmp3, strtmp0, strtmp1, strtmp2);
			} else {
				sprintf(strName, "%s", strtmp3);
			}
		} else {
			sprintf(strName, "data");
		}
	}
#if defined(_WIN32)
	sprintf(targetDir, "%s\\%s", rootDir, strName);
#else
	sprintf(targetDir, "%s/%s", rootDir, strName);
#endif
	cr_mkdir(targetDir);			// Feauture directoty..

#if defined(_WIN32)
	sprintf(pathName, "%s\\%s.xml", targetDir, fileName);
#else
	sprintf(pathName, "%s/%s.xml", targetDir, fileName);
#endif

}	

/*!
 ********************************************************************************
 *	@brief      feature information to seperate file.
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2019/11/16
 *******************************************************************************/
I32 iana_info_feature2(HAND hiana)
{	
	char pathname[PATHBUFLEN];

	IANA_INFO_CreateFeatureXmlPath(hiana, "feature", pathname);

	cr_nanoxml_create2(pathname, 0);

	nxmlie(0, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
	
//	nxmlie(1, "<shotfeature>");
	iana_info_feature(hiana);
//	nxmlie(-1, "</shotfeature>");
	cr_nanoxml_delete(NULL, 0);

	return 1;
}


/*!
 ********************************************************************************
 *	@brief      End info
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @return		0: fail, 1: good.
 *
 *	@author	    yhsuk
 *  @date       2016/03/21
 *******************************************************************************/
I32 iana_info_end(HAND hiana)
{
	iana_t *piana;
	iana_info_t	*piif;

	//--
	piana = (iana_t *)hiana;
	piif = &piana->iif;

	//if (piana->infomode != IANA_INFO_NO) 
	{
		nxmlie(-1, "</SHOTINFO>");
		cr_nanoxml_delete(NULL, 0);
		cr_nanoxml_delete(NULL, 1);
	}

	
	return 1;
}


#if defined (__cplusplus)
}
#endif
