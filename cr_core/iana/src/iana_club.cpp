/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA club calc.
                                                                                
  	@section copyright_notice COPYRIGHT NOTICE
  	Copyright (c) 2015 ~ 2017 by Creatz Inc. 
  	All Rights Reserved. \n
  	Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	@section file_information FILE CREATION INFORMATION
	@file   iana_club.cpp
	@brief  IANA
	@author Original: by yhsuk
	@date   2017/01/02 First Created

	@section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
//#define IPP_SUPPORT
/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <math.h>
#include <vector>

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"
#include "cr_nanoxml.h"

#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#else
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#endif

#include "scamif.h"
#include "scamif_buffer.h"

#include "iana.h"
#include "iana_work.h"
#include "iana_implement.h"
#include "iana_cam.h"
#include "iana_cam_implement.h"
#include "iana_balldetect.h"
#include "iana_shot.h"
#include "iana_tool.h"
#include "iana_spin.h"
#include "iana_spin_ballclub.h"
//#include "iana_club_attack.h"

#include "cr_regression.h"
#include "iana_coordinate.h"

#include "iana_club.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define PRESHOTCOUNT	16 // 8

#define POSTSHOTCOUNT	16 // 1, 4, 8, 20, 32

#define PRESHOTCOUNT_P3V2	8
#define POSTSHOTCOUNT_P3V2	8

#define P3V2SHOTFILEIRON		"P3shotmodifyIRON.txt"
#define P3V2SHOTFILEDRIVER		"P3shotmodifyDRIVER.txt"

#define SIDECONFFILE	"p3calc.txt"

#define CLUBIMG_GAMMA

#define ALPHA	(4.0e-9) // (1.0e-8), (1.0e-9), (1.0e-10)

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

typedef struct _imagemeanstd {
	I32 index;
	I32 m;

	double mean;
	double stddev;
} imagemeanstd_t;

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

static double g_alpha = ALPHA;
static int s_proccessarea;

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
I32 iana_clubpath_cam_param_lite(iana_t *piana, U32 camid, U32 normalbulk);
I32 check_isIron(iana_t *piana, U32 camid);

// STATIC FUNCTION DECLARATIONS
 /*!
 ********************************************************************************
 *   @ingroup    IANA_CLUB
 *	@brief      The main function to calculate clubpath and club speed for P3-V2. This uses any club stickers but only detects club face.
 *               With the relative positions of clubhead, detect toe and heel positions and then extract features and computes club informations.
 *
 *   @param[in]	piana       IANA module handle
 *   @param[in]	camid       camera id
 *   @param[in]	normalbulk  Normal or Bulk
 *
 *   @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *   @date       2020/03/20
 *******************************************************************************/
static I32 GetClubPath(iana_t *piana, U32 camid, U32 normalbulk);

/*!
********************************************************************************
*   @ingroup    IANA_CLUB
*	@brief      Get club parameters(HS Note:I'm not sure what does PARAMETERs mean and their members. Just reading something with scamif?) and do some basic image processing
*               Here we resize the stacked(multiplituded) images to the normal size.
*   @param[in]	piana       IANA module handle
*   @param[in]	camid       camera id
*   @param[in]	normalbulk  Normal or Bulk
*
*   @return		1: good, 0: no-good
*
*	@author	    yhsuk
*   @date       2020/03//22
*******************************************************************************/
static I32 GetClubPath_Param(iana_t *piana, U32 camid, U32 normalbulk);

/*!
********************************************************************************
*   @ingroup    IANA_CLUB
*	@brief      The main P3V2 club image processing module.\n
*               Use optical flow to detect club path and then Hough Line Transform to detect shaft line. Some other calculations follow.\n
*               TODO: We should split image processing functions in the _Position_calculation[*] so that  [*] becomes to contain the computation precesses only.
*               Then bind image processing procedures to one function, like GetClubPath_Position_ImgProc
*   @param[in]	piana       IANA module handle
*   @param[in]	camid       camera id
*   @param[in]	normalbulk  Normal or Bulk
*
*   @return		1: good, 0: no-good
*
*	@author	    yhsuk
*   @date       2020/03/23
*******************************************************************************/
static I32 GetClubPath_Position(iana_t *piana, U32 camid, U32 normalbulk);

#ifdef IPP_SUPPORT
/*!
********************************************************************************
*   @ingroup    IANA_CLUB
*	@brief      CAM Get Club Image processing
*
*   @param[in]	piana
*               IANA module handle
*   @param[in]	camid
*               camera id
*   @param[in]	pimgbuf
*               Image buffer
*   @param[in]	width
*               image width
*   @param[in]	height
*               image height
*   @param[out]	ptoepos
*  			    toe position
*   @param[out]	pheelpos
*  			    heel position
*   @param[out]	pshaftpos
*  			    one of shaft position
*
*   @return		1: good, 0: no-good
*
*	@author	    yhsuk
*   @date       2020/0323
*******************************************************************************/
static I32 GetClubPath_Position_Computation(
    iana_t *piana, U32 camid, 
    U08 *pimgbuf, U32 width, U32 height, point_t *ptoepos, 
	point_t *pheelpos, 
	point_t *pshaftpos, 
	double *pshaftangle,
	point_t *ptoeshaftpos);
#else
/*!
********************************************************************************
*   @ingroup    IANA_CLUB
*	@brief      Detect shaft line with transposed image using HouyghLineDetection. /n
*               Then extract toe and heel position of clubhead and some additional computations.
*
*   @param[in]	piana           IANA module handle
*   @param[in]	camid           camera id
*   @param[in]	imgTrans        First transposed image: TODO - add detailed description
*   @param[in]	imgTrans2       Second transposed image: TODO - add detailed description
*   @param[out]	ptoepos         toe position
*   @param[out]	pheelpos        heel position
*   @param[out]	pshaftpos       shaft position??
*   @param[out]	pshaftangle     shaft angle??
*   @param[out]	ptoeshaftpos    toe-shaft position??
*
*   @return		1: good, 0: no-good
*
*	@author	    Hyeonseok choi
*   @date       2021/02/24
*******************************************************************************/
static I32 GetClubPath_Position_Computation(
    iana_t *piana, U32 camid,
    cv::Mat imgTrans, cv::Mat imgTrans2,
    point_t *ptoepos,
    point_t *pheelpos,
    point_t *pshaftpos,
    double *pshaftangle,
    point_t *ptoeshaftpos);

/*!
********************************************************************************
*   @ingroup    IANA_CLUB
*	@brief      A part of P3-V2 club image processing. Mainly, transpose the input image.
*
*   @param[in]	piana       IANA module handle
*   @param[in]	camid       camera id
*   @param[in]	imgBuf      The imput image: TODO - add detailed description
*   @param[out]	imgTrans    First transposed image: TODO - add detailed description
*   @param[out]	imgTrans2   Second transposed image: TODO - add detailed description
*
*   @return		1: good, 0: no-good
*
*	@author	    Hyeonseok choi
*   @date       2021/02/24
*******************************************************************************/
static I32 GetClubPath_Position_ImgProc(iana_t* piana, U32 camid, cv::Mat imgBuf, cv::Mat imgTrans, cv::Mat imgTrans2);

#endif



/*!
********************************************************************************
*	@brief      CAM Club Header position check and exile..
*
*   @param[in]	piana   IANA module handle
*   @param[in]	camid   camera id
*
*   @return		1: good, 0: no-good
*
*	@author	    yhsuk
*   @date       2020/0326
*******************************************************************************/
static I32 GetClubPath_Position_Exile(iana_t *piana, U32 camid);

/*!
********************************************************************************
*	@brief      CAM Club Header position Regression
*
*   @param[in]	piana   IANA module handle
*   @param[in]	camid   camera id
*
*   @return		1: good, 0: no-good
*
*	@author	    yhsuk
*   @date       2020/03/26
*******************************************************************************/
static I32 GetClubPath_Position_Regression(iana_t *piana, U32 camid);


static I32 DisplayClubPath(iana_t *piana, U32 camid, U32 normalbulk);

/*!
********************************************************************************
*	@brief      CAM Club 3D heel position
*
*   @param[in]	piana
*               IANA module handle
*   @param[in]	camid
*               camera id
*
*   @return		1: good, 0: no-good
*
*	@author	    yhsuk
*   @date       2020/0326
*******************************************************************************/
//static I32 iana_clubpath_cam_3d_heelposition(iana_t *piana);

/**
********************************************************************************
*   @ingroup    IANA_CLUB
*   @brief      Get toe position of club head from shaftpoints, S(tart)points, E(nd)points.\n
*               Spoints 중에 픽셀갯수가 기준치 이상인 것을 최대 DETECTCHECKCOUNT개 선택하고 가장 멀리 있는 값을 toeshaftpoint로 지정
*   @param[in]	width       Width of image
*   @param[in]	height      Height of image
*   @param[in]	shaftpoint  Detected shaft points. by hough line detection
*   @param[in]	Spoint      Start points (of what? and how to get them)
*   @param[in]	Epoint      End points (of what? and how to get them)
*   @param[in]	pixelcount  Number of pixels    (of what? and how to get them)
*   @param[out]	ptoepos
*   @param[out]	ptoeindex
*   @param[out]	ptoeshaftpos
*
*   @return		1: good, 0: no-good
*
*	@author	    yhsuk
*   @date       unknown
*******************************************************************************/
static I32 toeposition(
		U32 width, U32 height,
		point_t shaftpoint[],
		point_t Spoint[],			// start points for every shaft point.
		point_t Epoint[],			// End points for every shaft point.
		U32 pixelcount[],
		point_t *ptoepo,
		U32 *ptoeindex,
		point_t *ptoeshaftpos
		///		
	);

static I32 heelposition(
		iana_t *piana, U32 camid,
		U32 width, U32 height,
		point_t *ptoepos,
		double m_, double b_,		// shaft line..
		double theta,				// angle of shaft line
		U08 *pimgT,
		point_t *pheelpos
		);

/*----------------------------------------------------------------------------
 *	Description	: Define the functions
 -----------------------------------------------------------------------------*/
I32 iana_club_estimate(iana_t *piana)
{
	I32 res;
	U32 camid;

	iana_shotresult_t *psrA;
	iana_shotresult_t *psr;
	iana_shotresult_t *pcsr;
	
	//--------------------------------------------------
	psrA = &piana->shotresultdata;
	for (camid = 0; camid < NUM_CAM; camid++) {
		double vmag;
		double incline;
		double azimuth;
		double sidespin;
		double backspin;

		double clubspeed_A;
		double clubspeed_B;
		double clubpath;
		double clubfaceangle;

		double ctov;					// club to ball vmag
		double vtoc_m;					// ball vmag to club
		double vtoc_b;					// ball vmag to club
		double btoa;					// club, before to after

		double delta_angle;

		//--
		psr		= &piana->shotresult[camid];		
		pcsr	= &piana->clubball_shotresult[camid];
		vmag	= psr->vmag;
		incline = psr->incline;
		azimuth = psr->azimuth;
		sidespin = psr->sidespin;
		backspin = psr->backspin;
		//-- 1) Club speed, Before/After
		if (piana->shotarea == IANA_AREA_TEE) {
			// TEE 
			vtoc_b = 0.0;
			if (incline < 12) {
				vtoc_m = 0.75;
			} else if (incline < 15) {
				vtoc_m = LINEAR_SCALE(incline, 12.0,0.75, 15.0, 0.70);
				vtoc_b = 0;
			} else if (incline < 20) {
				vtoc_m = LINEAR_SCALE(incline, 15.0,0.70, 20.0, 0.71);
				vtoc_b = 0;
			} else if (incline < 25) {
				vtoc_m = LINEAR_SCALE(incline, 20.0,0.71, 25.0, 0.80);
				vtoc_b = 0;
			} else if (incline < 40) {
				vtoc_m = LINEAR_SCALE(incline, 25.0,0.80, 40.0, 1.10);
			} else if (incline < 60) {
				vtoc_m = LINEAR_SCALE(incline, 40.0,1.10, 60.0, 1.20);
			} else {
				vtoc_m = 1.20;
			}
			ctov = 1.0 / vtoc_m;

		} else {
			// IRON 
			vtoc_b = 0.0;
			if (incline < 12) {
				vtoc_m = 0.75;
			} else if (incline < 15) {
				vtoc_m = LINEAR_SCALE(incline, 12.0,0.75, 15.0, 0.70);
				vtoc_b = 0;
			} else if (incline < 20) {
				vtoc_m = LINEAR_SCALE(incline, 15.0,0.70, 20.0, 0.71);
				vtoc_b = 0;
			} else if (incline < 25) {
				vtoc_m = LINEAR_SCALE(incline, 20.0,0.71, 25.0, 0.85);
				vtoc_b = 0;
			} else if (incline < 40) {
				vtoc_m = LINEAR_SCALE(incline, 25.0,0.85, 40.0, 1.15);
			} else if (incline < 60) {
				vtoc_m = LINEAR_SCALE(incline, 40.0,1.15, 60.0, 1.20);
			} else {
				vtoc_m = 1.20;
			}
			ctov = 1.0 / vtoc_m;

		}


		{
			double spineffect;
			double inclineeffect;

			if (incline < 2) {
				inclineeffect = 0.00;
			}
			else if (incline < 10) {
				inclineeffect = LINEAR_SCALE(incline, 2.0, 0.00, 10.0, 0.07);
			}
			else if (incline < 20) {
				inclineeffect = LINEAR_SCALE(incline, 10.0, 0.07, 20.0, 0.05);
			}
			else if (incline < 40) {
				inclineeffect = LINEAR_SCALE(incline, 20.0, 0.05, 40.0, 0.01);
			}
			else if (incline < 50) {
				inclineeffect = LINEAR_SCALE(incline, 40.0, 0.01, 50.0, -0.1);
			}
			else if (incline < 60) {
				inclineeffect = LINEAR_SCALE(incline, 50.0, -0.1, 60.0, -0.2);
			}
			else {
				inclineeffect = -0.2;
			}

			inclineeffect += 0.1;		// 20191016

			if (backspin < 1000) {
				spineffect = 0.00;
			}
			else if (backspin < 2000) {
				spineffect = LINEAR_SCALE(backspin, 1000, -0.01, 2000, 0.00);
			}
			else if (backspin < 3000) {
				spineffect = LINEAR_SCALE(backspin, 2000, 0.00, 3000, 0.01);
			}
			else if (backspin < 4000) {
				spineffect = LINEAR_SCALE(backspin, 3000, 0.01, 4000, 0.00);
			}
			else if (backspin < 5000) {
				spineffect = LINEAR_SCALE(backspin, 4000, 0.00, 5000, -0.05);
			}
			else if (backspin < 8000) {
				spineffect = LINEAR_SCALE(backspin, 5000, -0.05, 8000, -0.10);
			} else {
				spineffect = -0.10;
			}

			ctov += (inclineeffect + spineffect);

			if (ctov <(1.0/0.96)) { 
				ctov = (1.0 / 0.96) + (ctov / 100.0);
			} 

		}


		if (incline < 20) {
			btoa = LINEAR_SCALE(incline,  0.0, 0.7, 20.0, 0.75);
		} else if (incline < 50) {
			btoa = LINEAR_SCALE(incline, 20.0, 0.75, 50.0, 0.80);
		} else if (incline < 60) {
			btoa = LINEAR_SCALE(incline, 50.0,0.80, 60.0, 0.85);
		} else {
			btoa = 0.85;
		}

		clubspeed_B = vmag / ctov;
		clubspeed_A = clubspeed_B * btoa;

		//-- 2) Club Path
		// DELTA_ANGLE = (BALL_ANGLE - CLUB_ANGLE)
		// sidespin = DELTA_ANGLE * clubspeed_B * 5;
		// DELTA_ANGLE = sidespin / (clubspeed_B*5);
		// -> B_A - C_A = sidespin / (clubspeed_B*5);
		// -> C_A = B_A - sidespin / (clubspeed_B*5);

#define MIN_CLUBSPEED_B	1.0
#define DELTA_ANGLE_1	1.0

		if (clubspeed_B < MIN_CLUBSPEED_B) {
			//delta_angle = sidespin / 5;
			delta_angle = sidespin / 10;
		} else {
			double spinmult;
			double mmm;
			if (incline < 20) {
				spinmult = 5.0;
			} else if (incline < 30) {
				spinmult = LINEAR_SCALE(incline,  20.0, 5.0, 30.0, 4.5);
			} else if (incline < 50) {
				spinmult = LINEAR_SCALE(incline,  30.0, 4.5, 50.0, 4.0);
			} else {
				spinmult = 4.0;
			}

			if (clubspeed_B < 10) {
				mmm = 3.0;
			} else 	if (clubspeed_B < 20) {
				mmm = LINEAR_SCALE(clubspeed_B,  10.0, 3.0, 20.0, 2.5);
			} else 	if (clubspeed_B < 30) {
				mmm = LINEAR_SCALE(clubspeed_B,  20.0, 2.5, 30.0, 2.0);
			} else {
				mmm = 2.0;
			}
			spinmult *= mmm;

			delta_angle = sidespin / (clubspeed_B * spinmult);

	
		}

		clubpath = azimuth - delta_angle;
		
		//-- 3) Club fath
		clubfaceangle	= (10.0 * azimuth - clubpath) / 9.0;


		//--
		psrA->clubspeed_B = psr->clubspeed_B = clubspeed_B;
		psrA->clubspeed_A = psr->clubspeed_A = clubspeed_A;
		psrA->clubpath = psr->clubpath    = clubpath;
		psrA->clubfaceangle = psr->clubfaceangle    = clubfaceangle;

#define CLUBSPEED_MULT	1.08
		psr->clubspeed_B *= CLUBSPEED_MULT;
		psr->clubspeed_A *= CLUBSPEED_MULT;


		psrA->clubspeed_B = psr->clubspeed_B;
		psrA->clubspeed_A = psr->clubspeed_A;


		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "clubspeed: %10lf, %10lf   vs %10lf, %10lf\n",
		//		psr->clubspeed_B, psr->clubspeed_A, pcsr->clubspeed_B, pcsr->clubspeed_A);
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "clubpath: %10lf vs %10lf\n",
		//		psr->clubpath, pcsr->clubpath);

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "vmag incline azimuth sidespin backspin clubspeedB clubspeedA clubpath clubfaceangle      clubspeedB clubspeedA clubpath\n");
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%10f %10f %10f %10f %10f %10f %10f %10f %10f %10f %10f %10f\n",
			psr->vmag, psr->incline, psr->azimuth, psr->sidespin, psr->backspin, psr->clubspeed_B, psr->clubspeed_A, psr->clubpath, psr->clubfaceangle, pcsr->clubspeed_B, pcsr->clubspeed_A, pcsr->clubpath);
		//		psr->clubspeed_B, psr->clubspeed_A, pcsr->clubspeed_B, pcsr->clubspeed_A);

	}



	res = 1;

	return res;
}


I32 iana_club_spin_estimate(
		iana_t *piana,
		U32 camid,
		double angleoffset
		)
{
	I32 res;
	double vmag;
	double incline;
	double azimuth;
	double vmagORG;
	double inclineORG;
	double azimuthORG;

	double backspin;
	double beta;

	double sidespin;
	double anglediff;
	double gamma;
	double sidespin_mult;

	double sidespin_gear;

	double gearoffset;
	double clubspeed;
	double clubpath;
	double clubfaceangle;

	iana_cam_t	*pic;
	clubpath_context_t 	*pcpc;
	iana_shotresult_t *psr, *psr0;
	
	U32 isIron;
	double gear_coeff;


	double offsetClubpath;			// +: Hook,  -: Slice
	double multSidespin;
	double multClubspeed;
	//------

	psr	= &piana->shotresultdata;
	psr0	= &piana->shotresult[camid];		
	pic 	= piana->pic[camid];
	pcpc 	= &pic->cpc;

	memcpy(psr0, psr, sizeof(iana_shotresult_t));

	vmagORG 	= vmag = psr->vmag;
	inclineORG 	= incline = psr->incline;
	azimuthORG 	= azimuth = psr->azimuth;
	clubspeed 	= psr->clubspeed_B;
	clubpath 	= psr->clubpath;

	gearoffset = pcpc->gearoffset;
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n\ngearoffset: %lf   facelen: %lf\n\n\n", pcpc->gearoffset, pcpc->h2facelen);

	isIron = check_isIron(piana, camid);
#if 0
//#define FACELEN_IRON	(0.075)
#define FACELEN_IRON	(0.095)			// 20200813
	if (pcpc->h2facelen < FACELEN_IRON) {
		isIron = 1;
	} else {
#define BALLPOS_HEIGHT_TEE	0.03
		if (pic->icp.b3d.z < BALLPOS_HEIGHT_TEE) {
			isIron = 1;
		} else {
			isIron = 0;		// DRIVER...... 
		}
	}

	if (isIron) {
		pcpc->shotarea = IANA_AREA_IRON;
		s_proccessarea = IANA_AREA_IRON;
	} else {
		pcpc->shotarea = IANA_AREA_TEE;
		s_proccessarea = IANA_AREA_TEE;
	}
#endif

//-------
#if 1 // TODO: PH3 duplicated code . fixme after P3 stable code merge
	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		double vmagadd, azimuthadd, inclineadd;
		double vmagmult, azimuthmult, inclinemult;

		//--

		// 20200915
//#define VMAG_MULT_IRON_SG		1.05
//#define INCLINE_MULT_IRON_SG	1.015

//#define VMAG_MULT_DRIVER_SG		1.0
//#define INCLINE_MULT_DRIVER_SG	1.025

#define VMAG_MULT_IRON_SG		1.0
#define INCLINE_MULT_IRON_SG	1.025

#define VMAG_MULT_DRIVER_SG		1.05
#define INCLINE_MULT_DRIVER_SG	1.015


		vmagadd = 0;
		azimuthadd = 0;
		inclineadd = 0;
		vmagmult = 1.0;
		azimuthmult = 1.0;
		inclinemult = 1.0;
		if (isIron) {
			vmagmult = VMAG_MULT_IRON_SG;
			inclinemult = INCLINE_MULT_IRON_SG;
		} else {
			vmagmult = VMAG_MULT_DRIVER_SG;
			inclinemult = INCLINE_MULT_DRIVER_SG;
		}

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "P3SHOT isIron: %d vmag: %lf -> %lf (%lf, %lf) incline: %lf -> %lf (%lf, %lf) azimuth: %lf -> %lf (%lf, %lf)\n",
			isIron,
			vmag, vmagadd, vmagmult, (vmag + vmagadd) * vmagmult,
			incline, inclineadd, inclinemult, (incline + inclineadd) * inclinemult,
			azimuth, azimuthadd, azimuthmult, (azimuth + azimuthadd) * azimuthmult);

		vmag = (vmag + vmagadd) * vmagmult;
		incline = (incline + inclineadd) * inclinemult;
		azimuth = (azimuth + azimuthadd) * azimuthmult;
	}


#if defined(P3V2SHOTFILEIRON) && defined(P3V2SHOTFILEDRIVER)
		if (piana->camsensor_category == CAMSENSOR_P3V2) {

			U32 shotmode;
			FILE *fp;
			double vmagadd, azimuthadd, inclineadd;
			double vmagmult, azimuthmult, inclinemult;

			//--

			vmagadd = 0;
			azimuthadd = 0;
			inclineadd = 0;
			vmagmult = 1.0;
			azimuthmult = 1.0;
			inclinemult = 1.0;

			if (isIron) {
				fp = cr_fopen(P3V2SHOTFILEIRON, "r");
			} else {
				fp = cr_fopen(P3V2SHOTFILEDRIVER, "r");
			}
			if (fp) {
				fscanf(fp, "%d", &shotmode);
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
				if (shotmode == 1) {
					fscanf(fp, "%lf", &vmagadd);
					fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
					fscanf(fp, "%lf", &vmagmult);
					fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
					fscanf(fp, "%lf", &inclineadd);
					fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
					fscanf(fp, "%lf", &inclinemult);
					fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
					fscanf(fp, "%lf", &azimuthadd);
					fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
					fscanf(fp, "%lf", &azimuthmult);
				}
				cr_fclose(fp);
			}

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "P3SHOT isIron: %d vmag: %lf -> %lf (%lf, %lf) incline: %lf -> %lf (%lf, %lf) azimuth: %lf -> %lf (%lf, %lf)\n", 
					isIron, 
					vmag, vmagadd, vmagmult, (vmag + vmagadd) * vmagmult,
					incline, inclineadd, inclinemult, (incline + inclineadd) * inclinemult,
					azimuth, azimuthadd, azimuthmult, (azimuth + azimuthadd) * azimuthmult);

			vmag = (vmag + vmagadd) * vmagmult;
			incline = (incline + inclineadd) * inclinemult;
			azimuth = (azimuth + azimuthadd) * azimuthmult;
		}
#endif
#endif
// TODO: duplicated////////////////////////////////////////////////////////////////////

//--------------------------------------------
	offsetClubpath = 0.0;
	multSidespin = 1.0;
	multClubspeed = 1.0;

#if defined(SIDECONFFILE)

//--------------------------------
//1			// runmode
//0.0		// oT  clubpath Offset for Tee.  + : Hook,   - : Slice			
//1.0 		// mT  sidespin Mult for Tee.
//1.0 		// mcT clubspeed Mult for Tee.
//0.0		// oI  clubpath Offset for Iron.
//1.0		// mI  sidespin Mult for Iron.
//1.0		// mcI clubspeed Mult for Iron.
//--------------------------------

	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		FILE *fp;

		fp = cr_fopen(SIDECONFFILE, "r");

		if (fp) {
			float	offsetTEE_; 
			float	multTEE_;
			float	multCTEE_;
			float	offsetIRON_; 
			float	multIRON_;
			float	multCIRON_;
			I32		calcmode;

			calcmode = 0;

			fscanf(fp, "%d", &calcmode);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

			fscanf(fp, "%f", &offsetTEE_);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
			fscanf(fp, "%f", &multTEE_);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
			fscanf(fp, "%f", &multCTEE_);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'


			fscanf(fp, "%f", &offsetIRON_);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
			fscanf(fp, "%f", &multIRON_);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
			fscanf(fp, "%f", &multCIRON_);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

			cr_fclose(fp);

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "--------------------------\n");
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "calcmode  : %lf\n", 	calcmode);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "offsetTEE_: %lf\n", 	offsetTEE_);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "multTEE_  : %lf\n", 	multTEE_);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "multCTEE_ : %lf\n", 	multCTEE_);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "offsetIRON_:%lf\n", 	offsetIRON_);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "multICRON_: %lf\n", 	multCIRON_);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "--------------------------\n");

			if (calcmode == 1)
			{
				if (isIron) {
					offsetClubpath = offsetIRON_;
					multSidespin = multIRON_;
					multClubspeed = multCIRON_;
				} else {
					offsetClubpath = offsetTEE_;
					multSidespin = multTEE_;
					multClubspeed = multCTEE_;
				}
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "isIron: %d, offsetClubpath: %lf multSidespin: %lf multClubspeed\n",
						isIron, offsetClubpath, multSidespin, multClubspeed);

			}
		}

	}
#endif
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "clubspeed: %lf -> %lf\n", clubspeed, clubspeed * multClubspeed);
	clubspeed = clubspeed * multClubspeed;
//--------------------------------------------
	//-- Backspin;
#if 0
	if (incline < 20.0) {
		beta = 5.0;
	} else if (incline < 30.0) {
		beta = LINEAR_SCALE(incline, 20.0, 5.0, 30.0, 4.0);
	} else {
		beta = 4.0;
	}
#endif
#if 0
	if (incline < 15.0) {
		beta = 6.0;
	} else if (incline < 20.0) {
		beta = LINEAR_SCALE(incline, 15.0, 6.0, 20.0, 5.0);
	} else if (incline < 30.0) {
		beta = LINEAR_SCALE(incline, 20.0, 5.0, 30.0, 4.0);
	} else {
		beta = 4.0;
	}
#endif
#if 0				// comment out, 20200522
	if (isIron) {	// IRON?
		if (incline < 15.0) {
			beta = 6.0;
		} else if (incline < 20.0) {
			beta = LINEAR_SCALE(incline, 15.0, 6.0, 20.0, 8.0);
		} else if (incline < 30.0) {
			beta = LINEAR_SCALE(incline, 20.0, 8.0, 30.0, 7.0);
		} else if (incline < 50.0) {
			beta = LINEAR_SCALE(incline, 30.0, 7.0, 50.0, 6.0);
		} else {
			beta = 6.0;
		}
	} else {		// DRIVER?
		if (incline < 15.0) {
			beta = 4.0;
		} else if (incline < 20.0) {
			//beta = LINEAR_SCALE(incline, 15.0, 4.0, 20.0, 5.0);
			beta = LINEAR_SCALE(incline, 15.0, 4.0, 20.0, 5.0);
		} else if (incline < 30.0) {
			beta = LINEAR_SCALE(incline, 20.0, 5.0, 30.0, 5.0);
		} else if (incline < 50.0) {
			beta = LINEAR_SCALE(incline, 30.0, 5.0, 50.0, 4.0);
		} else {
			beta = 4.0;
		}
	}
#endif

#if 0				// Add.  20200522
	if (isIron) {	// IRON?
		if (incline < 15.0) {
			beta = 7.0;
		} else if (incline < 20.0) {
			beta = LINEAR_SCALE(incline, 15.0, 7.0, 20.0, 7.5);
		} else if (incline < 30.0) {
			beta = LINEAR_SCALE(incline, 20.0, 7.5, 30.0, 6.5);
		} else if (incline < 50.0) {
			beta = LINEAR_SCALE(incline, 30.0, 6.5, 50.0, 6.0);
		} else {
			beta = 6.0;
		}
	} else {		// DRIVER?
		if (incline < 15.0) {
			beta = 4.5;
		} else if (incline < 20.0) {
			beta = LINEAR_SCALE(incline, 15.0, 4.5, 20.0, 5.0);
		} else if (incline < 30.0) {
			beta = LINEAR_SCALE(incline, 20.0, 5.0, 30.0, 5.0);
		} else if (incline < 50.0) {
			beta = LINEAR_SCALE(incline, 30.0, 5.0, 50.0, 4.0);
		} else {
			beta = 4.0;
		}
	}
#endif
#if 0				// Change.  20200527
	if (isIron) {	// IRON?
		if (incline < 15.0) {
			beta = 7.0;
		} else if (incline < 20.0) {
			beta = LINEAR_SCALE(incline, 15.0, 7.0, 20.0, 8.0);
		} else if (incline < 30.0) {
			beta = LINEAR_SCALE(incline, 20.0, 8.0, 30.0, 7.0);
		} else if (incline < 50.0) {
			beta = LINEAR_SCALE(incline, 30.0, 7.0, 50.0, 6.0);
		} else {
			beta = 6.0;
		}
	} else {		// DRIVER?
		if (incline < 10.0) {
			beta = 4.3;
		} else if (incline < 15.0) {
			beta = LINEAR_SCALE(incline, 10.0, 4.3, 15.0, 4.4);
		} else if (incline < 20.0) {
			beta = LINEAR_SCALE(incline, 15.0, 4.4, 20.0, 4.5);
		} else if (incline < 30.0) {
			beta = LINEAR_SCALE(incline, 20.0, 4.5, 30.0, 5.0);
		} else if (incline < 50.0) {
			beta = LINEAR_SCALE(incline, 30.0, 5.0, 50.0, 4.0);
		} else {
			beta = 4.0;
		}
	}
#endif
#if 1				// Change.  20200819
	if (isIron) {	// IRON?
		if (incline < 15.0) {
			beta = 6.5;
		} else if (incline < 20.0) {
			beta = LINEAR_SCALE(incline, 15.0, 6.5, 20.0, 7.0);
		} else if (incline < 30.0) {
			beta = LINEAR_SCALE(incline, 20.0, 7.0, 30.0, 6.0);
		} else if (incline < 50.0) {
			beta = LINEAR_SCALE(incline, 30.0, 6.0, 50.0, 5.5);
		} else {
			beta = 5.5;
		}
	} else {		// DRIVER?
		if (incline < 10.0) {
			beta = 5.0;
		} else if (incline < 15.0) {
			beta = LINEAR_SCALE(incline, 10.0, 5.0, 15.0, 5.5);
		} else if (incline < 20.0) {
			beta = LINEAR_SCALE(incline, 15.0, 5.5, 20.0, 5.0);
		} else if (incline < 30.0) {
			beta = LINEAR_SCALE(incline, 20.0, 5.0, 30.0, 5.5);
		} else if (incline < 50.0) {
			beta = LINEAR_SCALE(incline, 30.0, 5.5, 50.0, 5.0);
		} else {
			beta = 5.0;
		}
	}
#endif
	{
		double inclinefactor;
		double speedvalue;
		double smashfactor;
		double backspin_mult;

//#define INCLINEFACTOR_MIN	5.0
#define INCLINEFACTOR_MIN	7.0
//		inclinefactor = incline;

		if (isIron) {	// IRON?
			if (incline < INCLINEFACTOR_MIN) {
				inclinefactor = INCLINEFACTOR_MIN;
			} else {
				inclinefactor = incline;
			}
		} else {
			// for 20201202 shot data
#define INCLINEFACTOR_MIN_TEE	5.0
			if (incline < INCLINEFACTOR_MIN_TEE) {
				inclinefactor = INCLINEFACTOR_MIN_TEE;
			} else if (incline < 8) {
				inclinefactor = LINEAR_SCALE(incline, INCLINEFACTOR_MIN_TEE, INCLINEFACTOR_MIN_TEE, 8.0, 8.0);
			} else if (incline < 10) {
				inclinefactor = LINEAR_SCALE(incline, 8.0, 8.0, 10.0, 10.0);
			} else if (incline < 15) {
				inclinefactor = LINEAR_SCALE(incline, 10.0, 10.0, 15.0, 14.0);
			} else if (incline < 20) {
				inclinefactor = LINEAR_SCALE(incline, 15.0, 14.0, 20.0, 17.0);
			} else if (incline < 40) {
				inclinefactor = LINEAR_SCALE(incline, 20.0, 17.0, 40.0, 30.0);
			} else if (incline < 50) {
				inclinefactor = LINEAR_SCALE(incline, 40.0, 30.0, 50.0, 40.0);
			} else {
				inclinefactor = 40.0;
			}
#define INCLINEFACTOR_1202		0.90
			inclinefactor = inclinefactor * INCLINEFACTOR_1202;

		}
#define MINCLUBSPEEDRATIO	0.5
		if (clubspeed < vmag * MINCLUBSPEEDRATIO) {
			speedvalue = vmag * MINCLUBSPEEDRATIO;
		} else {
			speedvalue = clubspeed;
		}

#define BACKSPIN_VMAG_CHECK_MIN	60.0

		backspin_mult = 1.0;
		if (isIron == 0) { 			// TEE shot..
			if (vmag > BACKSPIN_VMAG_CHECK_MIN) {
				smashfactor = vmag / speedvalue;

		// 20201211
				if (smashfactor < 1.3) {
					backspin_mult = 1.0;
				} else if (smashfactor < 1.4) {
					backspin_mult = LINEAR_SCALE(smashfactor,	1.30, 1.0, 1.40, 1.02);
				} else if (smashfactor < 1.45) {
					backspin_mult = LINEAR_SCALE(smashfactor,	1.40, 1.02, 1.45, 1.01);
				} else if (smashfactor < 1.50) {
					backspin_mult = LINEAR_SCALE(smashfactor,	1.45, 1.01, 1.50, 0.95);
				} else {
					backspin_mult = 0.95;
				}

				{ 
					double backspin_mult2;

					// 20201229
					if (vmag < 60.0) {
						backspin_mult2 = 1.20;
					} else if (vmag < 65.0) {
						backspin_mult2 = LINEAR_SCALE(vmag,	60.0, 1.20, 65.0, 1.25);
					} else if (vmag < 70.0) {
						backspin_mult2 = LINEAR_SCALE(vmag,	65.0, 1.25, 70.0, 1.24);
					} else if (vmag < 75.0) {
						backspin_mult2 = LINEAR_SCALE(vmag,	70.0, 1.24, 75.0, 1.20);
					} else {
						backspin_mult2 = 1.20;
					}
					backspin_mult = backspin_mult *  backspin_mult2;
				}
			} else {
				smashfactor = vmag / speedvalue;
				if (smashfactor < 1.3) {
					backspin_mult = 1.0;
				} else if (smashfactor < 1.4) {
					backspin_mult = LINEAR_SCALE(smashfactor,	1.30, 0.97, 1.40, 0.97);
				} else if (smashfactor < 1.45) {
					backspin_mult = LINEAR_SCALE(smashfactor,	1.40, 0.97, 1.45, 0.92);
				} else if (smashfactor < 1.50) {
					backspin_mult = LINEAR_SCALE(smashfactor,	1.45, 0.92, 1.50, 0.85);
				} else {
					backspin_mult = 0.85;
				}

				{ 
					double backspin_mult2;

					// 20200428
					if (vmag < 60.0) {
						backspin_mult2 = 1.0;
					} else if (vmag < 65.0) {
						backspin_mult2 = LINEAR_SCALE(vmag,	60.0, 0.98, 65.0, 0.98);
					} else if (vmag < 70.0) {
						backspin_mult2 = LINEAR_SCALE(vmag,	65.0, 0.98, 70.0, 0.93);
					} else if (vmag < 75.0) {
						backspin_mult2 = LINEAR_SCALE(vmag,	70.0, 0.93, 75.0, 0.90);
					} else {
						backspin_mult2 = 0.90;
					}

					backspin_mult = backspin_mult *  backspin_mult2;


					if (vmag < 10) {				// 20200511
						backspin_mult2 = 1.1;
					} else if (vmag < 20) {
						backspin_mult2 = LINEAR_SCALE(vmag,	10.0, 1.10, 20.0, 1.13);
					} else if (vmag < 40) {
						backspin_mult2 = LINEAR_SCALE(vmag,	20.0, 1.12, 40.0, 1.20);
					} else if (vmag < 50) {
						backspin_mult2 = LINEAR_SCALE(vmag,	40.0, 1.20, 50.0, 1.25);
					} else if (vmag < 75.0) {
						backspin_mult2 = 1.25;
					}
					backspin_mult = backspin_mult *  backspin_mult2;
				}
			}
		} else {			// Iron shot
			backspin_mult = 1.1;
		}
		backspin = inclinefactor * speedvalue * beta * backspin_mult;
	}

#define SIDESPIN_IRON_MULT				0.5

//#define SIDESPIN_IRON_MULT_HOOK			0.35
//
//#define SIDESPIN_IRON_MULT_HOOK			0.4
//#define SIDESPIN_IRON_MULT_SLICE		0.5

//#define SIDESPIN_IRON_MULT_HOOK			0.3
//#define SIDESPIN_IRON_MULT_SLICE		0.4

///////#define SIDESPIN_IRON_MULT_HOOK			0.35				// 20200522
//#define SIDESPIN_IRON_MULT_SLICE		0.45
//#define SIDESPIN_IRON_MULT_SLICE		0.55				// 20200612
//#define SIDESPIN_IRON_MULT_SLICE		0.50				// 20200612
////////#define SIDESPIN_IRON_MULT_SLICE		0.40				// 20200706
//
//#define SIDESPIN_IRON_MULT_HOOK			0.50			// 20200903
//#define SIDESPIN_IRON_MULT_SLICE		0.60			// 20200903

#define SIDESPIN_IRON_MULT_HOOK			0.60			// 20201202
#define SIDESPIN_IRON_MULT_SLICE		0.70			// 20201202

//#define SIDESPIN_IRON_offset			1.5
//#define SIDESPIN_IRON_offset			1.7
//#define SIDESPIN_IRON_offset			2.0			// 20200518
//#define SIDESPIN_IRON_offset			1.9			// 20200701
//#define SIDESPIN_IRON_offset			1.7			// 20200903
//#define SIDESPIN_IRON_offset			2.4			// 20200910

//#define SIDESPIN_IRON_offset			3.0			// 20200911
#define SIDESPIN_IRON_offset			4.0			// 20201221

//#define SIDESPIN_TEE_MULT		1.0
//#define SIDESPIN_TEE_MULT_HOOK			0.8
//#define SIDESPIN_TEE_MULT_SLICE			1.0

/*
#define SIDESPIN_TEE_MULT_HOOK			0.85
#define SIDESPIN_TEE_MULT_SLICE			1.0
*/
// 20200527
#define SIDESPIN_TEE_MULT_HOOK			0.75
//#define SIDESPIN_TEE_MULT_SLICE			0.95
#define SIDESPIN_TEE_MULT_SLICE			1.00			// 20200612


//#define SIDESPIN_TEE_offset		1.5
//#define SIDESPIN_TEE_offset				2.0			// 20200910
//#define SIDESPIN_TEE_offset				3.0			// 20200911
#define SIDESPIN_TEE_offset				3.7			// 20210120

	//-- sidespin
	anglediff = azimuth - clubpath;					// + : slice, -: Hook.

	anglediff += angleoffset;

	if (isIron) {
		anglediff += SIDESPIN_IRON_offset;
	} else {
		anglediff += SIDESPIN_TEE_offset;
	}



	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "anglediff: %lf -> %lf\n", anglediff, anglediff-offsetClubpath);
	anglediff =  anglediff-offsetClubpath;




	//gamma = 4.0;
	//gamma = 8.0;			// 0402
	gamma = 6.0;			// 0402
	sidespin = anglediff * clubspeed * gamma;

	if (isIron) {
		if (anglediff < 0) {
			sidespin_mult = SIDESPIN_IRON_MULT_HOOK;
		} else {
			sidespin_mult = SIDESPIN_IRON_MULT_SLICE;
		}

		{
			double sidespin_mult2;
			if (anglediff < -4.0) {				// HOOK
				sidespin_mult2 = 0.7;
			} else if (anglediff < -3.0) {				// HOOK
				sidespin_mult2 = LINEAR_SCALE(anglediff, -4.0, 0.7, -3.0, 0.8);
			} else if (anglediff < -1.0) {				// HOOK
				sidespin_mult2 = LINEAR_SCALE(anglediff, -3.0, 0.8, -1.0, 0.9);
			} else if (anglediff < 0) {
				sidespin_mult2 = LINEAR_SCALE(anglediff, -1.0, 0.9,  0.0, 1.0);
			} else if (anglediff < 1.0) {
				sidespin_mult2 = LINEAR_SCALE(anglediff,  0.0, 1.0,  1.0, 0.9);
			} else if (anglediff < 3.0) {
				sidespin_mult2 = LINEAR_SCALE(anglediff,  1.0, 0.9,  3.0, 0.8);
			} else if (anglediff < 4.0) {
				sidespin_mult2 = LINEAR_SCALE(anglediff,  3.0, 0.8,  4.0, 0.7);
			} else {
				sidespin_mult2 = 0.7;
			}
			sidespin_mult = sidespin_mult * sidespin_mult2;
		}
		{	// 20201202.. 
			double sidespin_mult3;
			if (incline < 20.0) {
				sidespin_mult3 = 1.3;
			} else if (incline < 30) {
				sidespin_mult3 = LINEAR_SCALE(incline, 20.0, 1.3, 30, 1.0);
			} else if (incline < 40) {
				sidespin_mult3 = LINEAR_SCALE(incline, 30.0, 1.0, 40, 0.8);
			} else if (incline < 60) {
				sidespin_mult3 = LINEAR_SCALE(incline, 40.0, 0.8, 60, 0.6);
			} else {
				sidespin_mult3 = 0.6;
			}
			sidespin_mult = sidespin_mult * sidespin_mult3;
		}
	} else {
		if (anglediff < 0) {
			sidespin_mult = SIDESPIN_TEE_MULT_HOOK;
		} else {
			sidespin_mult = SIDESPIN_TEE_MULT_SLICE;
		}
	}


	sidespin = sidespin * sidespin_mult;


//#define GEAR_COEFF	(-1444.0)
#define GEAR_COEFF_DRIVER	(-350.0)			// 20210120
#define GEAR_COEFF_IRON		(-50.0)				// 20210120
	if (isIron) {
		gear_coeff = GEAR_COEFF_IRON;
	} else {
		gear_coeff = GEAR_COEFF_DRIVER;
	}
	sidespin_gear = gear_coeff * vmag * gearoffset;

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "backspin: %lf,    sidespin: %lf (%lf + sidespin_gear(%lf))\n",
			backspin, 
			(sidespin+sidespin_gear),
			sidespin, 
			sidespin_gear);



	psr0->sidespin = sidespin + sidespin_gear;
	psr0->backspin = backspin;

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "sidespin: %lf -> %lf\n", psr0->sidespin, psr0->sidespin*multSidespin);
	psr0->sidespin = psr0->sidespin*multSidespin;

	//-- Club fath
	clubfaceangle	= (10.0 * azimuth - clubpath) / 9.0;

	psr0->clubfaceangle    = clubfaceangle;


	//-- Ball data restore..
//	psr0->vmag 		= vmag;
//	psr0->incline 	= incline;
//	psr0->azimuth 	= azimuth;

//	psr0->vmag 		= vmagORG;
//	psr0->incline 	= inclineORG;
//	psr0->azimuth 	= azimuthORG;

	psr0->clubspeed_B = clubspeed;

	memcpy(psr, psr0, sizeof(iana_shotresult_t));

	res = 1;
	return res;
}



static U32 s_normalbulk = NORMALBULK_NORMAL;
//static U32 s_normalbulk = NORMALBULK_BULK;
I32 iana_club_calc(iana_t *piana)	
{
	I32 res;
	U32 normalbulk;
	U32 camid;

	//--

    //	normalbulk = NORMALBULK_NORMAL;
	normalbulk = s_normalbulk;

	//-
	res = GetClubPath(piana, 0, normalbulk);

	//-
	res = iana_clubpath_cam_param_lite(piana, 1, normalbulk);				// for club image store...:P
	{
		iana_cam_t	*pic;
		clubpath_context_t 	*pcpc;
		iana_shotresult_t *psr, *psr0;

		//--
		camid = 0;
		psr	= &piana->shotresultdata;
		psr0	= &piana->shotresult[camid];		
		pic 	= piana->pic[camid];
		pcpc 	= &pic->cpc;

		memcpy(psr0, psr, sizeof(iana_shotresult_t));
		psr0->clubspeed_B = pcpc->h2clubspeed_B;
		psr0->clubpath = pcpc->h2clubpath;

		pcpc->shotarea = IANA_AREA_NULL;


		{
			double smashfactor;
			double clubspeed_B;
			double smashfactorN;
			double clubspeed_BN;

			clubspeed_B = psr0->clubspeed_B;
#define		CLUBSPEED_MIN	(0.5)
			if (clubspeed_B < CLUBSPEED_MIN) {
				clubspeed_B = CLUBSPEED_MIN;
			}

			smashfactor = psr0->vmag / clubspeed_B;

			if (s_proccessarea == IANA_AREA_IRON) {
#if 1			// 20200706
				if (smashfactor < 1.35) {
					smashfactorN = smashfactor;
				} else if (smashfactor < 1.40) {
					smashfactorN = LINEAR_SCALE(smashfactor, 1.35, 1.35, 1.40, 1.38);
				} else if (smashfactor < 1.45) {
					smashfactorN = LINEAR_SCALE(smashfactor, 1.40, 1.38, 1.45, 1.42);
				} else if (smashfactor < 1.50) {
					smashfactorN = LINEAR_SCALE(smashfactor, 1.45, 1.42, 1.50, 1.47);
				} else if (smashfactor < 1.55) {
					smashfactorN = LINEAR_SCALE(smashfactor, 1.50, 1.47, 1.55, 1.52);
				} else if (smashfactor < 1.60) {
					smashfactorN = LINEAR_SCALE(smashfactor, 1.55, 1.52, 1.60, 1.49);
				} else if (smashfactor < 2.00) {
					smashfactorN = LINEAR_SCALE(smashfactor, 1.60, 1.49, 2.00, 1.40);
				} else {
					smashfactorN = 1.40;
				}
#endif
			} else {	// TEE?
				if (smashfactor < 1.45) {
					smashfactorN = smashfactor;
				} else if (smashfactor < 1.51) {
					smashfactorN = LINEAR_SCALE(smashfactor, 1.45, 1.45, 1.51, 1.51);
				} else if (smashfactor < 2.00) {
					smashfactorN = LINEAR_SCALE(smashfactor, 1.51, 1.51, 2.00, 1.53);
				} else {
					smashfactorN = 1.53;
				}

			}

			clubspeed_BN = psr0->vmag / smashfactorN;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "SMASHFACTORN   %lf -> %lf,  clubspeed: %lf -> %lf\n", 
					smashfactor,
					smashfactorN,
					clubspeed_B, 
					clubspeed_BN);
			psr0->clubspeed_B = clubspeed_BN;
		}

		{
			U32 camidReady;
			cr_point_t *pb3d;
			U32 inBunker, inRough;

			//--
			camidReady = piana->camidCheckReady;
			pb3d = &piana->pic[camidReady]->icp.b3d;

			if (iana_isInBunker(piana, pb3d)) {
				inBunker = 1;
			} else {
				inBunker = 0;
			}

			if (iana_isInRough(piana, pb3d)) {
				inRough = 1;
			} else {
				inRough = 0;
			}

			if (inBunker || inRough) {
				double clubpath;
				double azimuth;
				double pathdelta;

				//--
				azimuth = psr0->azimuth;
				clubpath = psr0->clubpath;

				pathdelta = azimuth - clubpath;

#define CLUBDELTA_IN_BUNKER	5
#define CLUBDELTA_IN_ROUGH	7

				if (inBunker) {
					if (pathdelta < -CLUBDELTA_IN_BUNKER) {
						pathdelta = -CLUBDELTA_IN_BUNKER;
					}

					if (pathdelta > CLUBDELTA_IN_BUNKER) {
						pathdelta = CLUBDELTA_IN_BUNKER;
					}
				}
				if (inRough) {
					if (pathdelta < -CLUBDELTA_IN_ROUGH) {
						pathdelta = -CLUBDELTA_IN_ROUGH;
					}

					if (pathdelta > CLUBDELTA_IN_ROUGH) {
						pathdelta = CLUBDELTA_IN_ROUGH;
					}
				}

				clubpath = azimuth - pathdelta;

				psr0->clubpath = clubpath;
			}
		}
		memcpy(psr, psr0, sizeof(iana_shotresult_t));
	}

	//res = GetClubPath(piana, 1, normalbulk);
	UNUSED(camid);

	res = 1;
	return res;
}

static I32 GetClubPath(iana_t *piana, U32 camid, U32 normalbulk)	
{
	I32 res;
	iana_cam_t	*pic;
	clubpath_context_t 	*pcpc;

	res = GetClubPath_Param(piana, camid, normalbulk);
	res = GetClubPath_Position(piana, camid, normalbulk);

	pic 	= piana->pic[camid];
	pcpc 	= &pic->cpc;	

	free(pcpc->prefimgNoBall);

	pcpc->prefimgNoBall = NULL;

	res = 1;

	return res;
}


static int imagemeanstdCompare(const void *r1, const void *r2) 
{
  imagemeanstd_t *pimms1 = (imagemeanstd_t *)r1;
  imagemeanstd_t *pimms2 = (imagemeanstd_t *)r2;
  double m1, m2;

  //--
  m1 = pimms1->mean;
  m2 = pimms2->mean;
  //if (m1 < m2) { return 1; }
  //if (m1 > m2) {return -1; }
  if (m1 > m2) { return 1; }
  if (m1 < m2) {return -1; }
  return 0;
}

static int doubledataCompare(const void *r1, const void *r2) 
{
  double *pdv1 = (double *)r1;
  double *pdv2 = (double *)r2;

  //--
  //if (m1 < m2) { return 1; }
  //if (m1 > m2) {return -1; }
  if (*pdv1 < *pdv2) { return 1; }
  if (*pdv1 > *pdv2) {return -1; }
  return 0;
}

static I32 GetClubPath_Param(iana_t *piana, U32 camid, U32 normalbulk)	
{
#ifdef IPP_SUPPORT
	I32 res;
	iana_cam_t	*pic;
	camimageinfo_t     *pcii;
	camif_buffer_info_t *pb;
	clubpath_context_t 	*pcpc;

	U32 multitude;
	U32	width;
	U32	height;
	U32	offset_x;
	U32	offset_y;
	
	U32 skip, syncdiv;
	U64 period;
	U64 periodm;


	U08 *procimg;
	I32 j;
	U32 m;
	//I32 index;

	U08	*buf;
	U08	*buf0;
	U32	rindex;
	U32 frame;
	U32 frameaftershot;

	I32 r0, r1;
	I32 r2;

	U64 ts64;
	U64 ts64Mt;


	I32 marksequencelen;

	U32 detected;


	U32 preshotcount;
	U32 postshotcount;
#define MAXMS	63
	imagemeanstd_t imms[MAXMS];
	imagemeanstd_t imms0[MAXMS];


	camif_buffergroup_t *pbg;
	U32 buffercount;


	//--------------------
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++\n");

	res		= 1;

	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}

	if (normalbulk == NORMALBULK_NORMAL) {
		// 
	} else {
		normalbulk = NORMALBULK_BULK;
	}

	pic 	= piana->pic[camid];
	pcpc 	= &pic->cpc;
	memset(pcpc, 0, sizeof(clubpath_context_t));

	buf 	= NULL;
	pb 		= NULL;

	offset_x = 0;
	offset_y = 0;

	{
		pcpc->h2count = 0;
		memset(&pcpc->h2state[0], 0, sizeof(U32) * CLUBDATACOUNT);
	}

	res = scamif_imagebuf_peek_latest(piana->hscamif, camid, normalbulk, &pb, NULL, NULL);
	if (res == 0) {
		res = 0;
		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" club fail %d res: %d\n", __LINE__, res);
		goto func_exit;
	}

	pbg = NULL;
	scamif_imagebuf_groupinfo2(piana->hscamif, camid, normalbulk, &pb, &pbg);
	buffercount = pbg->count;				// buffer slot count

	//---
	pcii 		= &pb->cii;
	multitude   = pcii->multitude;
	if (multitude < 1) {
		multitude = 1;
	}
	width 		= pcii->width;
	height 		= pcii->height;

	syncdiv = pcii->syncdiv;	if (syncdiv == 0) {	syncdiv = 1;}
	skip = pcii->skip;			if (skip == 0) {skip = 1;}
	period = TSSCALE / (U64) pcii->framerate;			// nsec
	periodm = period * skip * syncdiv;

	pcpc->sx = 0;
	pcpc->ex = width-1;

	pcpc->width = width;
	pcpc->height = height;
	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		preshotcount = PRESHOTCOUNT_P3V2;
		postshotcount = POSTSHOTCOUNT_P3V2;
	} else {
		preshotcount = PRESHOTCOUNT;
		postshotcount = POSTSHOTCOUNT;
	}
	//---
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" start..\n");


	pcpc->prefimgNoBall = (U08 *)malloc(width * height);
	if (normalbulk == NORMALBULK_NORMAL) {
		//		camif_buffer_info_t *pb;
		//U32 count;

		r0 = pic->rindexshot - preshotcount / multitude - 1;
		r1 = pic->rindexshot + postshotcount / multitude;

		r0 += buffercount;
		r1 += buffercount;
		pcpc->shotindex 	=  pic->rindexshot;
#define R1_ADD	10
		r2 = r1 + R1_ADD;
	} else {
		r0 = pic->rindexshotbulk - preshotcount;
		if (r0 < 0) { r0 = 0; }
		r1 = pic->rindexshotbulk + postshotcount;
		if (r1 >= marksequencelen) { r1 = marksequencelen - 1; }

		pcpc->shotindex 	=  pic->rindexshotbulk;
		r2 = marksequencelen - 1; 
	}

	pcpc->startindex 	=  r0;
	pcpc->endindex 		=  r1;


	frame = 0;
	detected = 0;
	buf0 = NULL;
	//for (rindex = rindex0; rindex < rindex1; rindex++) 
	frameaftershot = 0;
	memset(&imms[0], 0, sizeof(imagemeanstd_t)*MAXMS);
	memset(&imms0[0], 0, sizeof(imagemeanstd_t)*MAXMS);
	for (rindex = (U32)r0; rindex < (U32)r2 ; rindex++) 
	{

		// 1) get image buffer
		if (piana->opmode == IANA_OPMODE_FILE) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
			if (res == 0) {
				break;
			}
		} else {
			//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bulk, img read\n", i);
			for (j = 0; j < 40; j++) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
				if (res == 1) {
					break;
				}
				cr_sleep(2);
			}
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read [%d:%d]\n", i, j);
			if (res == 0) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", rindex, j);
				break;
				//continue;
			}
		}
		if (res == 0) {
			//XXX
		}
		if (rindex == (U32)r0) {
			buf0 = buf;
		}

		if (rindex < (U32)(r2-2)) {
			continue;
		}

		ts64 = MAKEU64(pb->ts_h, pb->ts_l);

		pcii 		= &pb->cii;
		offset_x 	= pcii->offset_x;
		offset_y 	= pcii->offset_y;

		for (m = 0; m < multitude; m++) {
			ts64Mt = ts64 - (periodm * (multitude - m -1));
			if (frame != 0 && detected == 0 
#define TS64NEIBORHOOD	1000
					&& (ts64Mt >= pic->ts64shot - TS64NEIBORHOOD)
			   ) {			// shot!!
				U32 index;
				res = scamif_imagebuf_trimindex2(piana->hscamif, camid, normalbulk, rindex, &index);
				//pcpc->shotindex = index;
				pcpc->shotindex = rindex;
				pcpc->shotm 	= m;
				pcpc->shotframe = frame;
				detected = 1;

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d] shotindex: %d  m : %d   ts64Mt: %llu (%lf)\n",
						camid, index, m, ts64Mt, (ts64Mt / TSSCALE_D));
			}

			{
				I32 x1, y1;
				I32 x2, y2;

				x1 = (I32) (pic->icp.P.x - offset_x);
				y1 = 0;

				x2 = x1;
				y2 = 1023;

				
				procimg = pic->pimgFull;
				memcpy(procimg, (Ipp8u *)buf + pcii->width * ((m*pcii->height) / multitude), width * (height / multitude));
				if (piana->opmode == IANA_OPMODE_FILE) {
					mylineGrayV(x1, y1, x2, y2, 0xFF, procimg, width, 4);
				}

				if (detected) {
					IppiSize roisize, roisizeH;
					I32 widthH;
					I32 bx;
					Ipp64f mean0, stddev0;				// Ipp64f == double
					Ipp64f mean1, stddev1;				// Ipp64f == double
					U32 index;

					//--
					roisize.width = width;
					roisize.height = height/multitude;

					bx = (I32) (pic->icp.P.x - offset_x);
					widthH = width - bx;

					roisizeH.width = widthH;
					roisizeH.height = height/multitude;

					ippiMean_StdDev_8u_C1R(procimg + bx , width, roisizeH, &mean0, &stddev0);

					ippiMean_StdDev_8u_C1R(procimg, width, roisize, &mean1, &stddev1);


					res = scamif_imagebuf_trimindex2(piana->hscamif, camid, normalbulk, rindex, &index);
					imms[frameaftershot].index = index;
					imms[frameaftershot].m = m;
					imms[frameaftershot].mean = mean0;
					imms[frameaftershot].stddev = stddev0;
					frameaftershot++;

					//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d: %4d(%4d),%2d] FULL: %10.6lf %10.6lf  NET: %10.6lf %10.6lf\n", camid, rindex, index, m,
					//		mean1, stddev1,
					//		mean0, stddev0);
				}

				if (piana->himgfunc) {
					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)(procimg), width, (height/multitude), 4, 1, 2, 3, piana->imguserparam);
				}

			}
			frame++;
		}
 
	}

	if (detected == 0) {
		res = 0;
		goto func_exit;
	}

	{
		I32 iter;
		imagemeanstd_t *pimms;
		//U32 count;
		//double mean;
		U32 selecti, selectm;
		double minmean, maxmean;
		double selectmean;


		memcpy(&imms0[0], &imms[0], sizeof(imagemeanstd_t) * MAXMS);
		qsort( (void *) &imms[0], (frameaftershot), sizeof(imagemeanstd_t), imagemeanstdCompare);

		/*
		for (iter = 0; iter < (I32)(frameaftershot); iter++) {
			pimms = &imms[iter];
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d %4d, %2d] mean: %lf  stddev: %lf\n",
			//		camid, 
			//		pimms->index,
			//		pimms->m,
			//		pimms->mean,
			//		pimms->stddev);
		}
		*/

		minmean = imms[0].mean;
		//minmean = imms[4].mean;
		maxmean = imms[frameaftershot-1].mean;

#define MEANP	(0.3)
		selectmean = ((1-MEANP) * minmean + MEANP*maxmean);

		pimms = &imms[frameaftershot-1];
		selecti = pimms->index;
		selectm = pimms->m;
		for (iter = 0; iter < (I32)(frameaftershot); iter++) {
			pimms = &imms0[frameaftershot - 1 - iter];
			if (pimms->mean < selectmean) {
				selecti = pimms->index;
				selectm = pimms->m;
				/*
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d %4d, %2d] select.. mean: %lf  stddev: %lf\n",
						camid, 
						pimms->index,
						pimms->m,
						pimms->mean,
						pimms->stddev);
				*/
				break;
			}
		}



		for (j = 0; j < 40; j++) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, selecti, &pb, &buf);
			if (res == 1) {
				break;
			}
			cr_sleep(2);
		}

		{
			point_t Pp;
			double Pr;
			I32 sx, sy, ex, ey;
			I32 imgoffset;
			IppiSize roisize;
			I32 yy;

			//--
			Pp.x = pic->icp.P.x;
			Pp.y = pic->icp.P.y;
			Pr = pic->icp.cr;

			sx = (I32) (Pp.x-Pr - offset_x);
			sy = (I32) (Pp.y-Pr - offset_y);
			//ex = (I32) (Pp.x+Pr - offset_x);
			ex = (I32) (Pp.x+Pr*4 - offset_x);
			ey = (I32) (Pp.y+Pr - offset_y);

			roisize.width  = (I32) (Pr*2);
			roisize.height = (I32) (Pr*2);
			imgoffset = sx + sy * width;

			procimg = pcpc->prefimgNoBall;
			if (multitude == 1) {
				memcpy(procimg, buf, width*height);
				for (yy = sy; yy <= ey; yy++) {
					memcpy(procimg + sx + yy * width, buf0 + sx + yy* width, (ex - sx));
				}
			} else {
				double		xfact, yfact; 
				IppiSize 	ssize;
				IppiRect 	sroi, droi;
				int sstep, dstep;
				IppStatus 	status;
				Ipp8u 		*resizebuf;
				int 		bufsize;

				//---
				xfact 		= 1.0;
				yfact 		= 1.0 * multitude;
				ssize.width 	= width;
				ssize.height 	= height;

				sroi.x 		= 0;
				sroi.y 		= 0;
				sroi.width 	= width;
				sroi.height	= height/multitude;

				droi.x 		= 0;
				droi.y 		= 0;
				droi.width 	= (int) (sroi.width * xfact + 0.1);
				droi.height	= (int) (sroi.height * yfact + 0.1);

				sstep 		= sroi.width;
				dstep		= droi.width;

				ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_CUBIC, &bufsize);
				resizebuf = (U08 *)malloc(bufsize);


				status = ippiResizeSqrPixel_8u_C1R(
						(Ipp8u *)buf + pcii->width * ((selectm*pcii->height) / multitude),
						ssize,
						sstep,
						sroi,
						//
						(Ipp8u*) procimg,
						dstep,
						droi,
						//
						xfact,
						yfact,
						0,		// xshift,
						0,		// yshift,
						IPPI_INTER_CUBIC,
						resizebuf
						);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] selecti, selectm: %d, %d\n", camid, selecti, selectm);

				ssize.height= height;
#define EY_PR	4
				sroi.x 		= 0;
				sroi.y 		= 0;
				
				sroi.width 	= ex;
				//sroi.height	= (ey + (height - ey) / 2)/multitude;
				sroi.height	= (ey + ((height - ey) *3)/ 4)/multitude;

				droi.x 		= 0;
				droi.y 		= 0;
				droi.width 	= (int) (sroi.width * xfact + 0.1);
				droi.height	= (int) (sroi.height * yfact + 0.1);
				//memset(procimg, 0, width*height);
				status = ippiResizeSqrPixel_8u_C1R(
						//(Ipp8u *)buf0 + pcii->width * ((m*pcii->height) / multitude) + sx + (sy/multitude) * pcii->width,
						(Ipp8u *)buf0,
						ssize,
						sstep,
						sroi,
						//
						(Ipp8u*) procimg,

						dstep,
						droi,
						//
						xfact,
						yfact,
						0,		// xshift,
						0,		// yshift,
						IPPI_INTER_CUBIC,
						resizebuf
						);

				free(resizebuf);
			}

			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)procimg, width, height, 4, 1, 2, 3, piana->imguserparam);
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf0, width, height, 0, 1, 2, 3, piana->imguserparam);
			}

		}
	}

func_exit:
	return res;
#else
	I32 res;
	iana_cam_t	*pic;
	camimageinfo_t     *pcii;
	camif_buffer_info_t *pb;
	clubpath_context_t 	*pcpc;

	U32 multitude;
	U32	width;
	U32	height;
	U32	offset_x;
	U32	offset_y;
	
	U32 skip, syncdiv;
	U64 period;
	U64 periodm;


	U08 *procimg;
	I32 j;
	U32 m;

	U08	*buf;
	U08	*buf0;
	U32	rindex;
	U32 frame;
	U32 frameaftershot;

	I32 r0, r1;
	I32 r2;

	U64 ts64;
	U64 ts64Mt;

	I32 marksequencelen;

	U32 detected;

	U32 preshotcount;
	U32 postshotcount;

#define MAXMS	63
	imagemeanstd_t imms[MAXMS];
	imagemeanstd_t imms0[MAXMS];

	camif_buffergroup_t *pbg;
	U32 buffercount;

	//--------------------
	res		= 1;

	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}

	if (normalbulk == NORMALBULK_NORMAL) {
		// 
	} else {
		normalbulk = NORMALBULK_BULK;
	}

	pic 	= piana->pic[camid];
	pcpc 	= &pic->cpc;
	memset(pcpc, 0, sizeof(clubpath_context_t));

	buf 	= NULL;
	pb 		= NULL;

	offset_x = 0;
	offset_y = 0;

	pcpc->h2count = 0;
	memset(&pcpc->h2state[0], 0, sizeof(U32) * CLUBDATACOUNT);

	res = scamif_imagebuf_peek_latest(piana->hscamif, camid, normalbulk, &pb, NULL, NULL);
	if (res == 0) {
		res = 0;
		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" club fail %d res: %d\n", __LINE__, res);
		goto func_exit;
	}

	pbg = NULL;
	scamif_imagebuf_groupinfo2(piana->hscamif, camid, normalbulk, &pb, &pbg);
	buffercount = pbg->count;				// buffer slot count

	//---
	pcii 		= &pb->cii;
	multitude   = pcii->multitude;
    syncdiv = pcii->syncdiv;
    skip = pcii->skip;

    if (multitude < 1) { multitude = 1;	}
    if (syncdiv == 0) { syncdiv = 1; }
    if (skip == 0) { skip = 1; }

	width 		= pcii->width;
	height 		= pcii->height;
	
    period = TSSCALE / (U64) pcii->framerate;			// nsec
	periodm = period * skip * syncdiv;

	pcpc->sx = 0;
	pcpc->ex = width-1;

	pcpc->width = width;
	pcpc->height = height;

    //---
    pcpc->prefimgNoBall = (U08 *)malloc(width * height);

    frame = 0;
    detected = 0;
    buf0 = NULL;
    frameaftershot = 0;

    memset(&imms[0], 0, sizeof(imagemeanstd_t)*MAXMS);
    memset(&imms0[0], 0, sizeof(imagemeanstd_t)*MAXMS);

    if (piana->camsensor_category == CAMSENSOR_P3V2) {
		preshotcount = PRESHOTCOUNT_P3V2;
		postshotcount = POSTSHOTCOUNT_P3V2;
	} else {
		preshotcount = PRESHOTCOUNT;
		postshotcount = POSTSHOTCOUNT;
	}

	if (normalbulk == NORMALBULK_NORMAL) {
		r0 = pic->rindexshot - preshotcount / multitude - 1;
		r1 = pic->rindexshot + postshotcount / multitude;

		r0 += buffercount;
		r1 += buffercount;
		pcpc->shotindex 	=  pic->rindexshot;
#define R1_ADD	10
		r2 = r1 + R1_ADD;
	} else {
		r0 = pic->rindexshotbulk - preshotcount;
		if (r0 < 0) { 
            r0 = 0; 
        }
		r1 = pic->rindexshotbulk + postshotcount;
		if (r1 >= marksequencelen) { 
            r1 = marksequencelen - 1; 
        }

		pcpc->shotindex 	=  pic->rindexshotbulk;
		r2 = marksequencelen - 1; 
	}

	pcpc->startindex 	=  r0;
	pcpc->endindex 		=  r1;
    	
    // Hyeons Note: draw line(don't know the reason) and save img mean stddev.
    for (rindex = (U32)r0; rindex < (U32)r2 ; rindex++) {
		// 1) get image buffer
		if (piana->opmode == IANA_OPMODE_FILE) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
			if (res == 0) {
				break;
			}
		} else {
			for (j = 0; j < 40; j++) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
				if (res == 1) {
					break;
				}
				cr_sleep(2);
			}
			if (res == 0) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", rindex, j);
				break;
			}
		}
		if (res == 0) {
            // Don't we need break?
		}

		if (rindex == (U32)r0) {
			buf0 = buf;
		}

		if (rindex < (U32)(r2-2)) {
			continue;
		}

		ts64 = MAKEU64(pb->ts_h, pb->ts_l);

		pcii 		= &pb->cii;
		offset_x 	= pcii->offset_x;
		offset_y 	= pcii->offset_y;

		for (m = 0; m < multitude; m++) {
			ts64Mt = ts64 - (periodm * (multitude - m -1));

#define TS64NEIBORHOOD	1000
			if (frame != 0 && 
                detected == 0 && 
                (ts64Mt >= pic->ts64shot - TS64NEIBORHOOD)) {			// shot!!
				U32 index;
				res = scamif_imagebuf_trimindex2(piana->hscamif, camid, normalbulk, rindex, &index);
				//pcpc->shotindex = index;
				pcpc->shotindex = rindex;
				pcpc->shotm 	= m;
				pcpc->shotframe = frame;
				detected = 1;

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d] shotindex: %d  m : %d   ts64Mt: %llu (%lf)\n",
						camid, index, m, ts64Mt, (ts64Mt / TSSCALE_D));
			}

            procimg = pic->pimgFull;
			{
                cv::Rect sliceArea(0, (m*pcii->height) / multitude, width, (height / multitude));
                cv::Mat sliceImg(sliceArea.size(), CV_8UC1, procimg);

				I32 x1, y1;
				I32 x2, y2;

				x1 = (I32) (pic->icp.P.x - offset_x);
				y1 = 0;

				x2 = x1;
				y2 = 1023;

                cv::Mat(cv::Size(width, height), CV_8UC1, buf)(sliceArea).copyTo(sliceImg);
				
                if (piana->opmode == IANA_OPMODE_FILE) {
					mylineGrayV(x1, y1, x2, y2, 0xFF, procimg, width, 4);
				}

				if (detected) {
					I32 bx;
                    cv::Mat mean0, mean1, stdev0, stdev1;
					U32 index;

					//--
					bx = (I32) (pic->icp.P.x - offset_x);

                    cv::meanStdDev(sliceImg(cv::Range(0, sliceImg.rows), cv::Range(bx, width)), mean0, stdev0);
                    cv::meanStdDev(sliceImg, mean1, stdev1);

					res = scamif_imagebuf_trimindex2(piana->hscamif, camid, normalbulk, rindex, &index);
					imms[frameaftershot].index = index;
					imms[frameaftershot].m = m;
					imms[frameaftershot].mean = mean0.at<double>(0);
					imms[frameaftershot].stddev = stdev0.at<double>(0);
					frameaftershot++;
				}

				if (piana->himgfunc) {
					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)(procimg), width, (height/multitude), 4, 1, 2, 3, piana->imguserparam);
				}
			}
			frame++;
		}
	}

	if (detected == 0) {
		res = 0;
		goto func_exit;
	}

	{
		I32 iter;
		imagemeanstd_t *pimms;
		U32 selecti, selectm;
		double minmean, maxmean;
		double selectmean;


		memcpy(&imms0[0], &imms[0], sizeof(imagemeanstd_t) * MAXMS);
		qsort( (void *) &imms[0], (frameaftershot), sizeof(imagemeanstd_t), imagemeanstdCompare);

		minmean = imms[0].mean;
		maxmean = imms[frameaftershot-1].mean;

#define MEANP	(0.3)
		selectmean = ((1-MEANP) * minmean + MEANP*maxmean);

		pimms = &imms[frameaftershot-1];
		selecti = pimms->index;
		selectm = pimms->m;
		for (iter = 0; iter < (I32)(frameaftershot); iter++) {
			pimms = &imms0[frameaftershot - 1 - iter];
			if (pimms->mean < selectmean) {
				selecti = pimms->index;
				selectm = pimms->m;
				break;
			}
		}

		for (j = 0; j < 40; j++) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, selecti, &pb, &buf);
			if (res == 1) {
				break;
			}
			cr_sleep(2);
		}

		{
            cv::Size bufSize(width, height);
            cv::Mat procImg(bufSize, CV_8UC1, pcpc->prefimgNoBall),
                imgBuf(bufSize, CV_8UC1, buf),
                imgBuf0(bufSize, CV_8UC1, buf0);

			point_t Pp;
			double Pr;
			I32 sx, sy, ex, ey;

			//--
			Pp.x = pic->icp.P.x;
			Pp.y = pic->icp.P.y;
			Pr = pic->icp.cr;

			sx = (I32) (Pp.x - Pr - offset_x);
			sy = (I32) (Pp.y - Pr - offset_y);
			ex = (I32) (Pp.x + Pr*4 - offset_x);
			ey = (I32) (Pp.y + Pr - offset_y);

			procimg = pcpc->prefimgNoBall;
			if (multitude == 1) {
                cv::Rect copyRect(sx, sy, ex-sx, ey-sy);

                imgBuf.copyTo(procImg);
                imgBuf0(copyRect).copyTo(procImg(copyRect));

                if (piana->himgfunc) {
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)procImg.data, width, height, 2, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, width, height, 0, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf0, width, height, 1, 1, 2, 3, piana->imguserparam);
                }

			} else {
                cv::Rect multSliceROI(0, selectm*height/multitude, width, height/multitude);
                cv::Rect multSliceROI0(0, 0, ex, (ey+(height-ey)*3/4)/multitude);
                cv::Size dstSize(width, height);
                cv::Rect dstROI0(0, 0, multSliceROI0.width, multSliceROI0.height*multitude);

				double		xfact, yfact; 

                xfact 		= 1.0; // 
                yfact 		= 1.0 * multitude; // 

                cv::resize(imgBuf(multSliceROI), procImg, dstSize, xfact, yfact, cv::INTER_CUBIC);
                cv::resize(imgBuf0(multSliceROI0), procImg(dstROI0), dstROI0.size(), xfact, yfact, cv::INTER_CUBIC);
			}

			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)procimg, width, height, 4, 1, 2, 3, piana->imguserparam);
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf0, width, height, 0, 1, 2, 3, piana->imguserparam);
			}

		}
	}

func_exit:
	return res;
#endif
}


I32 iana_clubpath_cam_param_lite(iana_t *piana, U32 camid, U32 normalbulk)
{
	I32 res;
	iana_cam_t	*pic;
	camimageinfo_t     *pcii;
	camif_buffer_info_t *pb;
	clubpath_context_t 	*pcpc;

	U32 multitude;
	U32	width;
	U32	height;
	U32	offset_x;
	U32	offset_y;
	
	U32 skip, syncdiv;
	U64 period;
	U64 periodm;

	I32 marksequencelen;
	U08	*buf;

	//--------------------
	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++\n");

	res		= 1;

	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}

	if (normalbulk == NORMALBULK_NORMAL) {
		// 
	} else {
		normalbulk = NORMALBULK_BULK;
	}

	pic 	= piana->pic[camid];
	pcpc 	= &pic->cpc;
	memset(pcpc, 0, sizeof(clubpath_context_t));

	buf 	= NULL;
	pb 		= NULL;

	offset_x = 0;
	offset_y = 0;

	{
		pcpc->h2count = 0;
		memset(&pcpc->h2state[0], 0, sizeof(U32) * CLUBDATACOUNT);
	}

	res = scamif_imagebuf_peek_latest(piana->hscamif, camid, normalbulk, &pb, NULL, NULL);
	if (res == 0) {
		res = 0;
		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" club fail %d res: %d\n", __LINE__, res);
		goto func_exit;
	}

	//---
	pcii 		= &pb->cii;
	multitude   = pcii->multitude;
	if (multitude < 1) {
		multitude = 1;
	}
	width 		= pcii->width;
	height 		= pcii->height;

	syncdiv = pcii->syncdiv;	if (syncdiv == 0) {	syncdiv = 1;}
	skip = pcii->skip;			if (skip == 0) {skip = 1;}
	period = TSSCALE / (U64) pcii->framerate;			// nsec
	periodm = period * skip * syncdiv;

	pcpc->sx = 0;
	pcpc->ex = width-1;

	pcpc->width = width;
	pcpc->height = height;

	res = 1;

func_exit:
	return res;
}

static I32 GetClubPath_Position(iana_t *piana, U32 camid, U32 normalbulk)	
{
#ifdef IPP_SUPPORT
    I32 res;
    iana_cam_t	*pic;
    camimageinfo_t     *pcii;
    camif_buffer_info_t *pb;
    clubpath_context_t 	*pcpc;

    U32 multitude;
    U32	width;
    U32	height;
    U32	offset_x;
    U32	offset_y;
    double		xfact, yfact;

    U32 skip, syncdiv;
    U64 period;
    U64 periodm;


    I32 j;
    U08 *procimg;
    U08 *pimgDiff;
    U08 *pimgAbs;
    U08 *pimgRef;
    //I32 index;

    U08	*buf;
    U32	rindex;
    U32 m;

    U64 ts64;
    U64 ts64Mt;

    //double xx[1024], yy[1024];
    //double tt[1024];
    //double maxy[3];
    U32 rcount;
    camif_buffergroup_t *pbg;
    U32 buffercount;

    U32 done_Ref_gamma;


    //--------------------
    //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"++\n");

    pic 	= piana->pic[camid];
    pcpc 	= &pic->cpc;

    res = 0;
    buf = NULL;
    pb = NULL;

    pimgDiff= pic->pimg[0];
    pimgAbs = pic->pimg[1];
    pimgRef	= pcpc->prefimgNoBall;

    rcount = 0;
    //maxy[2] = maxy[1] = maxy[0]=0;
    pbg = NULL;
    scamif_imagebuf_groupinfo2(piana->hscamif, camid, normalbulk, &pb, &pbg);
    buffercount = pbg->count;				// buffer slot count
#if defined(CLUBIMG_GAMMA)
    done_Ref_gamma = 0;
#endif

    for (rindex = pcpc->startindex; rindex <= (U32)pcpc->endindex; rindex++) {
        // 1) get image buffer
        if (piana->opmode == IANA_OPMODE_FILE) {
            res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
            if (res == 0) {
                break;
            }
        } else {
            //		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bulk, img read\n", i);
            for (j = 0; j < 40; j++) {
                res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
                if (res == 1) {
                    break;
                }
                cr_sleep(2);
            }
            //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read [%d:%d]\n", i, j);
            if (res == 0) {
                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", rindex, j);
                break;
                //continue;
            }
        }
        if (res == 0) {
            //XXX
        }

        {	// Image parameters for each frame
            pcii 		= &pb->cii;
            multitude   = pcii->multitude;
            if (multitude < 1) {
                multitude = 1;
            }
            width 		= pcii->width;
            height 		= pcii->height;

            syncdiv = pcii->syncdiv;	if (syncdiv == 0) { syncdiv = 1; }
            skip = pcii->skip;			if (skip == 0) { skip = 1; }
            period = TSSCALE / (U64)pcii->framerate;			// nsec
            periodm = period * skip * syncdiv;

            offset_x 	= pcii->offset_x;
            offset_y 	= pcii->offset_y;
            xfact 		= 1.0;
            yfact 		= 1.0 * multitude;

            ts64 = MAKEU64(pb->ts_h, pb->ts_l);
        }

#if defined(CLUBIMG_GAMMA)
        if (done_Ref_gamma == 0) {
            gamma_correction(pimgRef, pimgRef, width, height);
            done_Ref_gamma = 1;
        }
#endif
        for (m = 0; m < multitude; m++) {
            IppiSize roisize;
            ts64Mt = ts64 - (periodm * (multitude - m -1));

            if (multitude == 1) {
                procimg = buf;
            } else {
                IppiSize 	ssize;
                IppiRect 	sroi, droi;
                int sstep, dstep;
                IppStatus 	status;
                Ipp8u 		*resizebuf;
                int 		bufsize;

                //---
                ssize.width 	= width;
                ssize.height 	= height;

                sroi.x 		= 0;
                sroi.y 		= 0;
                sroi.width 	= width;
                sroi.height	= height/multitude;

                droi.x 		= 0;
                droi.y 		= 0;
                droi.width 	= (int)(sroi.width * xfact + 0.1);
                droi.height	= (int)(sroi.height * yfact + 0.1);

                sstep 		= sroi.width;
                dstep		= droi.width;

                ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_CUBIC, &bufsize);
                resizebuf = (U08 *)malloc(bufsize);

                procimg = pic->pimgFull;
                //memcpy(procimg, (Ipp8u *)buf + pcii->width * ((m*pcii->height) / multitude), width * (height / multitude));
                status = ippiResizeSqrPixel_8u_C1R(
                    (Ipp8u *)buf + pcii->width * ((m*pcii->height) / multitude),
                    ssize,
                    sstep,
                    sroi,
                    //
                    (Ipp8u*)procimg,
                    dstep,
                    droi,
                    //
                    xfact,
                    yfact,
                    0,		// xshift,
                    0,		// yshift,
                    IPPI_INTER_CUBIC,
                    resizebuf
                );
                free(resizebuf);
            }

#if defined(CLUBIMG_GAMMA)
            gamma_correction(procimg, procimg, width, height);
#endif
            roisize.width 	= width;
            roisize.height 	= height;
            ippiSub_8u_C1RSfs(					// C = A - B
                pimgRef, width,				// B
                procimg, width,				// A
                pimgDiff, width, 					// C, result.
                roisize, 0);
            ippiAbsDiff_8u_C1R(								// C = |A - B|
                pimgRef, width,				// B
                procimg, width,				// A
                pimgAbs, width, 					// C, result.
                roisize);
#if 0
            ippiAdd_8u_C1IRSfs(					// A = A + B
                pimgDiff, width,		// B
                pimgAbs, width,		// A
                roisize, 0);
#endif

#if 1
            {
                IppiSize roisizeShadow;
                //#define SHADOW_SHADOW_HEIGHT	100
                //#define SHADOW_SHADOW_HEIGHT	50
#define SHADOW_SHADOW_HEIGHT	60
#define SHADOW_SHADOW_SHIFT		20
//#define SHADOW_SHADOW_SHIFT		30
                roisizeShadow.width = width-SHADOW_SHADOW_SHIFT;
                roisizeShadow.height = height-SHADOW_SHADOW_HEIGHT;


                if (piana->himgfunc) {
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pimgAbs), width, height, 0, 1, 2, 3, piana->imguserparam);
                }

                memcpy(pimgDiff, pimgAbs, width*height);
                ippiSub_8u_C1RSfs(					// C = A - B
                    pimgAbs, width,				// B
                    pimgAbs+(width*SHADOW_SHADOW_HEIGHT + SHADOW_SHADOW_SHIFT), width,		// A
                    pimgDiff+(width*SHADOW_SHADOW_HEIGHT + SHADOW_SHADOW_SHIFT), width, 					// C, result.
                    roisizeShadow, 0);

                {
                    IppiSize roisizeResidual;


                    roisizeResidual.width = width;
                    roisizeResidual.height = SHADOW_SHADOW_HEIGHT;

#define MULC_FOR_RESIDUAL 3
#define EXPN_FOR_RESIDUAL 3
                    ippiMulC_8u_C1IRSfs(
                        MULC_FOR_RESIDUAL,
                        pimgDiff,
                        width,
                        roisizeResidual,
                        EXPN_FOR_RESIDUAL
                    );
                }





                memcpy(pimgAbs, pimgDiff, width*height);
                if (piana->himgfunc) {
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pimgAbs), width, height, 2, 1, 2, 3, piana->imguserparam);
                }
        }
#endif


            {
                U32 x1, y1;
                U32 pr;

                x1 = (I32)(pic->icp.P.x - offset_x);
                y1 = (I32)(pic->icp.P.y - offset_y);
                pr = (I32)(pic->icp.cr);

                mycircleGray4(x1, y1, pr, 0x00, pimgAbs, width);		// Remove Ball.. 
            }
            {
                I32 x1, y1;
                I32 pr;
                I64 t64delta;
                double ma[3];
                double td;
                double xd1, yd1;
                iana_cam_param_t 	*picp;

                //---
                picp 	= &pic->icp;

                t64delta = (I64)(ts64Mt - pic->ts64shot);
                td = (double)(t64delta / TSSCALE_D);

                if (td > 0) {
                    memcpy(&ma[0], &picp->maPx[0], sizeof(double)*3);
                    xd1 = ma[2]*td*td + ma[1]*td + ma[0];
                    memcpy(&ma[0], &picp->maPy[0], sizeof(double)*3);
                    yd1 = ma[2]*td*td + ma[1]*td + ma[0];
                    memcpy(&ma[0], &picp->maPr[0], sizeof(double)*3);
                    pr    = (I32)(ma[2]*td*td + ma[1]*td + ma[0]);

                    x1 = (I32)(xd1 - offset_x);
                    y1 = (I32)(yd1 - offset_y);
                    mycircleGray4(x1, y1, pr, 0x00, pimgAbs, width);		// Remove Ball.. 
                }
            }


            if (piana->himgfunc) {
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(procimg), width, height, 0, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pimgDiff), width, height, 2, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pimgAbs), width, height, 4, 1, 2, 3, piana->imguserparam);
            }


            {
                U32 shotindex_trim;
                U32 rindex_trim;
                point_t toepos, heelpos, shaftpos;
                point_t toeshaftpos;
                double shaftangle;


                res = GetClubPath_Position_Computation(piana, camid, pimgAbs, width, height, &toepos, &heelpos, &shaftpos, &shaftangle, &toeshaftpos);

                if (res == 0) {
                    continue;
                }
                res = scamif_imagebuf_trimindex2(piana->hscamif, camid, normalbulk, rindex, &rindex_trim);
                res = scamif_imagebuf_trimindex2(piana->hscamif, camid, normalbulk, pcpc->shotindex, &shotindex_trim);
                if (
                    (
                        rindex_trim < shotindex_trim
                        || rindex_trim > shotindex_trim + buffercount/2				// Check Rolling over... 
                        )
                    ||
                    (
                        rindex_trim == shotindex_trim && m < pcpc->shotm
                        )
                    ) {
                    point_t Pptoe, Lptoe;
                    point_t Ppheel, Lpheel;
                    point_t Ppshaftpos, Lpshaftpos;
                    point_t Pptoeshaft, Lptoeshaft;
                    I64 ts64diff;

                    Pptoe.x = toepos.x + offset_x;
                    Pptoe.y = toepos.y + offset_y;
                    iana_P2L(piana, camid, &Pptoe, &Lptoe, 0);

                    Ppheel.x = heelpos.x + offset_x;
                    Ppheel.y = heelpos.y + offset_y;
                    iana_P2L(piana, camid, &Ppheel, &Lpheel, 0);

                    Pptoeshaft.x = toeshaftpos.x + offset_x;
                    Pptoeshaft.y = toeshaftpos.y + offset_y;
                    iana_P2L(piana, camid, &Pptoeshaft, &Lptoeshaft, 0);

                    Ppheel.x = heelpos.x + offset_x;
                    Ppheel.y = heelpos.y + offset_y;



                    if (camid == 0) {
                        //#define LPHEEL_0_X_ADD	(-0.01)
                        //#define LPHEEL_0_X_ADD	(-0.005)
                        //#define LPHEEL_0_Y_ADD	(-0.005)
#define LPHEEL_0_X_ADD	0
#define LPHEEL_0_Y_ADD	0
                        Lpheel.x = Lpheel.x + LPHEEL_0_X_ADD;
                        Lpheel.y = Lpheel.y + LPHEEL_0_Y_ADD;
                    }

                    Ppshaftpos.x = shaftpos.x + offset_x;
                    Ppshaftpos.y = shaftpos.y + offset_y;
                    iana_P2L(piana, camid, &Ppshaftpos, &Lpshaftpos, 0);

                    ts64diff = ((I64)ts64Mt - (I64)pic->ts64shot);

                    pcpc->h2state[rcount] 	= IANA_CLUB_EXIST;			// IANA_CLUB_EXILE;
                    pcpc->h2ts[rcount] 		= ts64diff / TSSCALE_D;
                    //pcpc->h2x[rcount] 		= Lp.x;
                    //pcpc->h2y[rcount] 		= Lp.y;
                    pcpc->h2toepos[rcount].x = Lptoe.x;
                    pcpc->h2toepos[rcount].y = Lptoe.y;

                    pcpc->h2toeshaftpos[rcount].x = Lptoeshaft.x;
                    pcpc->h2toeshaftpos[rcount].y = Lptoeshaft.y;



                    pcpc->h2heelpos[rcount].x = Lpheel.x;
                    pcpc->h2heelpos[rcount].y = Lpheel.y;

                    pcpc->h2shaftpos[rcount].x = Lpshaftpos.x;
                    pcpc->h2shaftpos[rcount].y = Lpshaftpos.y;

                    pcpc->h2shaftangle[rcount] 	= shaftangle;
                    pcpc->h2count = rcount;

                    pcpc->h2shaftangle[rcount] 	= shaftangle;

                    {// make shaft line with heel point 
                        cr_vector_t p0, p1, mm;

                        //--
                        p0.x = Lpheel.x; p0.y = Lpheel.y; p0.z = 0;
                        p1.x = Lpshaftpos.x; p1.y = Lpshaftpos.y; p1.z = 0;

                        cr_vector_sub(&p0, &p1, &mm);			// p0: start point, p1: end point, m: result vector
                        cr_line_p0m(&p0, &mm, &pcpc->h2shaftline[rcount]);		// make shaft line with heel point 
                    }


                    //#define PRIODM_MULT_MAX	5
                    //#define PRIODM_MULT_MAX	2
#define PRIODM_MULT_MAX	3
//#define PRIODM_MULT_MAX	10
//#define PRIODM_MULT_MAX	7
                    if (ts64diff < -(I64)(periodm * PRESHOTCOUNT)) {
                        // DO NOTHING..
                    } else if (ts64diff < (I64)periodm * PRIODM_MULT_MAX) {
                        rcount++;
                    } else {
                        break;
                    }

                    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] (%4d-%2d / %4d-%2d) %10lf TOE  %10lf %10lf =>  %10lf %10lf  HEEL %10lf %10lf =>  %10lf %10lf  SHAFT %10lf %10lf =>  %10lf %10lf  (%10lf)\n",
                        camid, rindex, m,
                        (U32)pcpc->shotindex, (U32)pcpc->shotm, (ts64diff/TSSCALE_D),
                        Pptoe.x, Pptoe.y, Lptoe.x, Lptoe.y,
                        Ppheel.x, Ppheel.y, Lpheel.x, Lpheel.y,
                        Ppshaftpos.x, Ppshaftpos.y, Lpshaftpos.x, Lpshaftpos.y,
                        shaftangle);
                }
            }
    } 	// for (m = 0; m < multitude; m++) 
} 	// for (rindex = pcpc->startindex; rindex <= (U32)pcpc->endindex; rindex++) 

    GetClubPath_Position_Exile(piana, camid);
    //#define RCOUNT_DISCARD	2
#define RCOUNT_DISCARD	1
    rcount = pcpc->h2count - RCOUNT_DISCARD;
    pcpc->h2count = rcount;


    if (rcount > 3) {
        U32 isIron;
        res = GetClubPath_Position_Regression(piana, camid);

        if (piana->opmode == IANA_OPMODE_FILE) {
            res = DisplayClubPath(piana, camid, normalbulk);
        }

        isIron = check_isIron(piana, camid);

        if (pcpc->h2valid) {
            point_t p0, p1;
            //double x0, y0;
            //double x1, y1;
            double t0, t1;
            double angle;
            double clubspeed;
            double dx, dy, dlen;
            double x0_, y0_, t0_;
            double vv[CLUBDATACOUNT];
            U32 vvcount;
            double vv1[CLUBDATACOUNT];
            U32 vv1count;

            //#define TS	(-0.002)
#define TS	(-0.005)
//#define TS	(-0.010)
#define TE	( 0.000)
#define TD	(0.0005)


#define TE_A	(0.001)
#define TD_A	(0.0005)



#if 1
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "============ weighted sum of toe and heel.\n");
            t0 = TS - TD;
            p0.x = pcpc->h2xma[2] * t0*t0 + pcpc->h2xma[1] * t0 + pcpc->h2xma[0];
            p0.y = pcpc->h2yma[2] * t0*t0 + pcpc->h2yma[1] * t0 + pcpc->h2yma[0];
            x0_ = p0.x;
            y0_ = p0.y;
            t0_ = t0;
            vvcount = 0;
            for (t1 = TS; t1 < TE; t1 += TD) {
                double dtmp0, dtmp1;

                p1.x = pcpc->h2xma[2] * t1*t1 + pcpc->h2xma[1] * t1 + pcpc->h2xma[0];
                p1.y = pcpc->h2yma[2] * t1*t1 + pcpc->h2yma[1] * t1 + pcpc->h2yma[0];
                dx = p1.x - p0.x;
                dy = p1.y - p0.y;
                dlen = sqrt(dx*dx + dy*dy);
                angle = RADIAN2DEGREE(atan2(dx, dy));
                clubspeed = dlen / (t1 - t0);

                dx = p1.x - x0_;
                dy = p1.y - y0_;
                dtmp0 = sqrt(dx*dx + dy*dy);
                dtmp1= dtmp0 / (t1 - t0_);
                //vv[vvcount] = dtmp1;
                vv[vvcount] = clubspeed;

                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d  %10lf] weighted sum of toe and heel.  angle: %10lf clubspeed: %10lf, clubspeedtotal = %10lf\n",
                    camid, t1, angle, clubspeed, dtmp1);
                p0.x = p1.x; p0.y = p1.y;
                t0 = t1;
                vvcount++;
            }
#endif
#if 1
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "============ heel.\n");
            // heel.
            t0 = TS - TD;
            p0.x = pcpc->h2heelxma[2] * t0*t0 + pcpc->h2heelxma[1] * t0 + pcpc->h2heelxma[0];
            p0.y = pcpc->h2heelyma[2] * t0*t0 + pcpc->h2heelyma[1] * t0 + pcpc->h2heelyma[0];

            vv1count = 0;
            for (t1 = TS; t1 < TE; t1 += TD) {
                p1.x = pcpc->h2heelxma[2] * t1*t1 + pcpc->h2heelxma[1] * t1 + pcpc->h2heelxma[0];
                p1.y = pcpc->h2heelyma[2] * t1*t1 + pcpc->h2heelyma[1] * t1 + pcpc->h2heelyma[0];
                dx = p1.x - p0.x;
                dy = p1.y - p0.y;
                dlen = sqrt(dx*dx + dy*dy);
                angle = RADIAN2DEGREE(atan2(dx, dy));
                clubspeed = dlen / (t1 - t0);
                vv1[vv1count] = clubspeed;
                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d  %10lf] heel. angle: %10lf clubspeed1: %10lf\n", camid, t1, angle, clubspeed);
                p0.x = p1.x; p0.y = p1.y;
                t0 = t1;
                vv1count++;
            }
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---------------\n");
#endif


            //--
            t0 = TE_A - TD_A;
            p0.x = pcpc->h2xma[2] * t0*t0 + pcpc->h2xma[1] * t0 + pcpc->h2xma[0];
            p0.y = pcpc->h2yma[2] * t0*t0 + pcpc->h2yma[1] * t0 + pcpc->h2yma[0];

            t1 = TE_A;

            p1.x = pcpc->h2xma[2] * t1*t1 + pcpc->h2xma[1] * t1 + pcpc->h2xma[0];
            p1.y = pcpc->h2yma[2] * t1*t1 + pcpc->h2yma[1] * t1 + pcpc->h2yma[0];
            dx = p1.x - p0.x;
            dy = p1.y - p0.y;
            dlen = sqrt(dx*dx + dy*dy);
            angle = RADIAN2DEGREE(atan2(dx, dy));
            if (angle > 90) {
                angle = 180 - angle;
            }
            if (angle < -90) {
                angle = 180 + angle;
            }

            if (angle > 50) {
                angle = 50;
            }
            if (angle < -50) {
                angle = -50;
            }
            pcpc->h2clubpath = angle;

            clubspeed = dlen / (t1 - t0);
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d  %10lf] weighted sum of toe and heel. angle: %10lf clubspeed: %10lf\n", camid, t1, angle, clubspeed);

            //--
            //t1 = TE;
            //clubspeed = pcpc->h2clubspeedma[2] * t1*t1 + pcpc->h2clubspeedma[1] * t1 + pcpc->h2clubspeedma[0];

            //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "  ---> clubspeed: %10lf\n", clubspeed);
            //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "  ---> clubspeed: %10lf\n", pcpc->h2clubspeed_B);
            //pcpc->h2clubspeed_B = clubspeed;

#if 1					// weighted sum of toe and heel. 
            {
                U32 centerindex;
                double vv0[CLUBDATACOUNT];

                //--
                memcpy(&vv0[0], &vv[0], sizeof(double)*vvcount);
                qsort((void *)&vv0[0], vvcount, sizeof(double), doubledataCompare);
                centerindex = vvcount/2;						// 0.0.29.. :P
                //centerindex = ((vvcount * 2) / 3);					// 0.0.28
                //centerindex = ((vvcount * 1) / 3);				// 0.0.27
                pcpc->h2clubspeed_B = vv0[centerindex];
            }
#endif

#if 0				// heel. 
            {
                U32 centerindex;
                double vv0[CLUBDATACOUNT];

                //--
                memcpy(&vv0[0], &vv1[0], sizeof(double)*vvcount);
                qsort((void *)&vv0[0], vv1count, sizeof(double), doubledataCompare);
                //centerindex = ((vv1count * 2) / 3);
                centerindex = (vv1count/2);
                pcpc->h2clubspeed_B = vv0[centerindex];
            }
#endif


            //#define CLUBSPEED_RATIO_IRON (1.03)
            //#define CLUBSPEED_RATIO_IRON (1.04)
            ////#define CLUBSPEED_RATIO_IRON (1.06)
#define CLUBSPEED_RATIO_IRON (1.065)	// 20200903
            //#define	CLUBSPEED_RATIO_DRIVER (1/1.04)
/////#define	CLUBSPEED_RATIO_DRIVER (1/1.03)
#define	CLUBSPEED_RATIO_DRIVER (1/1.015)	// 20200903
//#define	CLUBSPEED_RATIO_DRIVER (1/100.0)

            // SG's suggestion.. 20200915 
#define CLUBSPEED_MULT_DRIVER_SG		1.045					// 20200915. 
#define CLUBSPEED_MULT_IRON_SG			1.025					// 20200915. 
            if (isIron) {
                pcpc->h2clubspeed_B = pcpc->h2clubspeed_B * CLUBSPEED_RATIO_IRON;
                pcpc->h2clubspeed_B = pcpc->h2clubspeed_B * CLUBSPEED_MULT_IRON_SG;			// 20200915
            } else {
                pcpc->h2clubspeed_B = pcpc->h2clubspeed_B * CLUBSPEED_RATIO_DRIVER;
                pcpc->h2clubspeed_B = pcpc->h2clubspeed_B * CLUBSPEED_MULT_DRIVER_SG;			// 20200915
            }

            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " isIron:%d ---> clubspeed: %10lf\n", isIron, pcpc->h2clubspeed_B);

            //--
            {	// get center point, hit point
                point_t toepos;
                point_t heelpos;
                point_t centerpos;
                cr_point_t cp0, cp1;
                //cr_line_t lineface;
                //cr_line_t lineshot;

                double thetashot;
                //double dx, dy;
                double dlenheel;		//  , dlen;
                double gearoffset;

                //--
                // 1) line between toepos and heelpos at t0.
                t0 = 0;
                cp0.x = toepos.x = pcpc->h2toexma[2] * t0*t0 + pcpc->h2toexma[1] * t0 + pcpc->h2toexma[0];
                cp0.y = toepos.y = pcpc->h2toeyma[2] * t0*t0 + pcpc->h2toeyma[1] * t0 + pcpc->h2toeyma[0];
                cp0.z = 0;

                cp1.x = heelpos.x = pcpc->h2heelxma[2] * t0*t0 + pcpc->h2heelxma[1] * t0 + pcpc->h2heelxma[0];
                cp1.y = heelpos.y = pcpc->h2heelyma[2] * t0*t0 + pcpc->h2heelyma[1] * t0 + pcpc->h2heelyma[0];
                cp1.z = 0;

                cr_line_p0p1(&cp0, &cp1, &pcpc->h2lineface);

                centerpos.x = (toepos.x + heelpos.x)/2.0;
                centerpos.y = (toepos.y + heelpos.y)/2.0;

                // 2) line shot 
                cp0.x = pic->icp.L.x;
                cp0.y = pic->icp.L.y;
                cp0.z = 0;

                thetashot = M_PI/2.0 - DEGREE2RADIAN(pcpc->h2clubpath);

                cp1.x = cp0.x + cos(thetashot);
                cp1.y = cp0.y + sin(thetashot);
                cp1.z = 0;

                cr_line_p0p1(&cp0, &cp1, &pcpc->h2lineshot);

                // 3) intersect point.

                cr_point_line_line(&pcpc->h2lineface, &pcpc->h2lineshot, &cp0, &cp1);


                pcpc->h2hitpos.x = (cp0.x + cp1.x)/2.0;
                pcpc->h2hitpos.y = (cp0.y + cp1.y)/2.0;

                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "TOE: %10lf %10lf   HEEL: %10lf %10lf hit potion: %10lf %10lf and %10lf %10lf vs %10lf %10lf\n",
                    toepos.x, toepos.y,
                    heelpos.x, heelpos.y,
                    cp0.x, cp0.y,
                    cp1.x, cp1.y,
                    centerpos.x, centerpos.y);

                /*
                dx = toepos.x - pcpc->h2hitpos.x;
                dy = toepos.y - pcpc->h2hitpos.y;
                dlentoe = sqrt(dx*dx + dy*dy);
                */

                dx = heelpos.x - pcpc->h2hitpos.x;
                dy = heelpos.y - pcpc->h2hitpos.y;
                dlenheel = sqrt(dx*dx + dy*dy);

                dx = heelpos.x - toepos.x;
                dy = heelpos.y - toepos.y;
                dlen = sqrt(dx*dx + dy*dy);

                gearoffset = dlenheel - dlen/2.0;

                pcpc->gearoffset = gearoffset;
                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "dlenheel: %lf dlen: %lf gearoffset: %lf\n", dlenheel, dlen, gearoffset);

            }
            }
        } else {
        res = 0;
    }
    res = 1;
    //func_exit:
    return res;
#else
    I32 res;
	iana_cam_t	*pic;
	camimageinfo_t     *pcii;
	camif_buffer_info_t *pb;
	clubpath_context_t 	*pcpc;

	U32 multitude;
	U32	width;
	U32	height;
	U32	offset_x;
	U32	offset_y;
	double		xfact, yfact;
    cv::Size bufSize, bufSizeTrans;
	
	U32 skip, syncdiv;
	U64 period;
	U64 periodm;


	I32 j;
    cv::Mat imgBuf, imgResized, imgDiff, imgAbsDiff, imgRef, imgTrans, imgTrans2;
	U08 *pimgRef;

	U08	*buf;
	U32	rindex;
	U32 m;

	U64 ts64;
	U64 ts64Mt;

	U32 rcount;
	camif_buffergroup_t *pbg;
	U32 buffercount;

	U32 done_Ref_gamma;

	
	//--------------------
	pic 	= piana->pic[camid];
	pcpc 	= &pic->cpc;

	res = 0;
	buf = NULL;
	pb = NULL;

	pimgRef	= pcpc->prefimgNoBall;

	rcount = 0;
	pbg = NULL;
	
    scamif_imagebuf_groupinfo2(piana->hscamif, camid, normalbulk, &pb, &pbg);
	
    buffercount = pbg->count;				// buffer slot count

#if defined(CLUBIMG_GAMMA)
	done_Ref_gamma = 0;
#endif

	for (rindex = pcpc->startindex; rindex <= (U32)pcpc->endindex; rindex++) {
		// 1) get image buffer
		if (piana->opmode == IANA_OPMODE_FILE) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
			if (res == 0) {
				break;
			}
		} else {
			for (j = 0; j < 40; j++) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
				if (res == 1) {
					break;
				}
				cr_sleep(2);
			}
			if (res == 0) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", rindex, j);
				break;
			}
		}
		if (res == 0) {
			//XXX
		}

		{	// Image parameters for each frame
			pcii 		= &pb->cii;
			multitude   = pcii->multitude;             
            syncdiv = pcii->syncdiv;	
            skip = pcii->skip;			

            if (multitude < 1) { multitude = 1; }
            if (syncdiv == 0) { syncdiv = 1; }
            if (skip == 0) { skip = 1; }
			
            period = TSSCALE / (U64) pcii->framerate;			// nsec
			periodm = period * skip * syncdiv;

            width 		= pcii->width;
            height 		= pcii->height;

            bufSize = cv::Size(width, height);
            bufSizeTrans = cv::Size(height, width);

			offset_x 	= pcii->offset_x;
			offset_y 	= pcii->offset_y;
			xfact 		= 1.0;
			yfact 		= 1.0 * multitude;

			ts64 = MAKEU64(pb->ts_h, pb->ts_l);

            imgBuf = cv::Mat(bufSize, CV_8UC1, buf);
            imgResized = cv::Mat(bufSize, CV_8UC1, pic->pimgFull);
            imgDiff = cv::Mat(bufSize, CV_8UC1, pic->pimg[0]);
            imgAbsDiff = cv::Mat(bufSize, CV_8UC1, pic->pimg[1]);
            imgRef = cv::Mat(bufSize, CV_8UC1, pcpc->prefimgNoBall);
            imgTrans = cv::Mat(bufSizeTrans, CV_8UC1, pic->pimg[2]);
            imgTrans2 = cv::Mat(bufSizeTrans, CV_8UC1, pic->pimg[3]);
		}

#if defined(CLUBIMG_GAMMA)
		if (done_Ref_gamma == 0) {
			gamma_correction(pimgRef, pimgRef, width, height);
			done_Ref_gamma = 1;
            if (piana->himgfunc) {
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(imgRef.data), width, height, 0, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pcpc->prefimgNoBall), width, height, 1, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pimgRef), width, height, 2, 1, 2, 3, piana->imguserparam);
            }

		}
#endif
		for (m = 0; m < multitude; m++) {
			ts64Mt = ts64 - (periodm * (multitude - m -1));

			if (multitude == 1) {
                imgBuf.copyTo(imgResized);
			} else {
                cv::Rect multitudeSliceROI(0, m*height/multitude, width, height/multitude);
                cv::resize(imgBuf(multitudeSliceROI), imgResized, bufSize, xfact, yfact, cv::INTER_CUBIC);
			}

#if defined(CLUBIMG_GAMMA)
			gamma_correction((U08*)imgResized.data, (U08*)imgResized.data, width, height);
#endif
            cv::subtract(imgResized, imgRef, imgDiff);
            cv::absdiff(imgResized, imgRef, imgAbsDiff);


            if (piana->himgfunc) {
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(imgDiff.data), width, height, 0, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pic->pimg[0]), width, height, 1, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(imgAbsDiff.data), width, height, 2, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pic->pimg[1]), width, height, 3, 1, 2, 3, piana->imguserparam);
            }
			{
#define SHADOW_SHADOW_HEIGHT	60 // 50, 100
#define SHADOW_SHADOW_SHIFT		20 // 30
                cv::Rect ROIShadowBR(SHADOW_SHADOW_SHIFT, SHADOW_SHADOW_HEIGHT, width-SHADOW_SHADOW_SHIFT, height - SHADOW_SHADOW_HEIGHT),
                    ROIShadowUL(0, 0, width-SHADOW_SHADOW_SHIFT, height - SHADOW_SHADOW_HEIGHT);

                imgAbsDiff.copyTo(imgDiff);

                cv::subtract(imgAbsDiff(ROIShadowBR), imgAbsDiff(ROIShadowUL), imgDiff(ROIShadowBR));

                if (piana->himgfunc) {
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(imgDiff.data), width, height, 0, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pic->pimg[0]), width, height, 1, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(imgAbsDiff.data), width, height, 2, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pic->pimg[1]), width, height, 3, 1, 2, 3, piana->imguserparam);
                }

				{
                    cv::Rect residualROI(0, 0, width, SHADOW_SHADOW_HEIGHT);

#define MULC_FOR_RESIDUAL 3
#define EXPN_FOR_RESIDUAL 3
                    imgAbsDiff(residualROI) = imgDiff(residualROI) * MULC_FOR_RESIDUAL / pow(2.0, EXPN_FOR_RESIDUAL);
				}

                if (piana->himgfunc) {
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(imgDiff.data), width, height, 0, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pic->pimg[0]), width, height, 1, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(imgAbsDiff.data), width, height, 2, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pic->pimg[1]), width, height, 3, 1, 2, 3, piana->imguserparam);
                }
			}

			{
				U32 x1, y1;
				U32 pr;

				x1 = (I32) (pic->icp.P.x - offset_x);
				y1 = (I32) (pic->icp.P.y - offset_y);
				pr = (I32) (pic->icp.cr);

				mycircleGray4(x1, y1, pr, 0x00, (U08*)imgAbsDiff.data, width);		// Remove Ball.. 
			}

			{
				I32 x1, y1;
				I32 pr;
				I64 t64delta;
				double ma[3];
				double td;
				double xd1, yd1;
				iana_cam_param_t 	*picp;

				//---
				picp 	= &pic->icp;

				t64delta = (I64)(ts64Mt - pic->ts64shot);
				td = (double) (t64delta / TSSCALE_D);

				if (td > 0) {
					memcpy(&ma[0], &picp->maPx[0], sizeof(double)*3);
					xd1 = ma[2]*td*td + ma[1]*td + ma[0];
					memcpy(&ma[0], &picp->maPy[0], sizeof(double)*3);
					yd1 = ma[2]*td*td + ma[1]*td + ma[0];
					memcpy(&ma[0], &picp->maPr[0], sizeof(double)*3);
					pr    = (I32) (ma[2]*td*td + ma[1]*td + ma[0]);

					x1 = (I32) (xd1 - offset_x);
					y1 = (I32) (yd1 - offset_y);
					mycircleGray4(x1, y1, pr, 0x00, (U08*)imgAbsDiff.data, width);		// Remove Ball.. 
				}
			}


			if (piana->himgfunc) {
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(imgResized.data), width, height, 0, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(imgDiff.data), width, height, 2, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(imgAbsDiff.data), width, height, 4, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pic->pimgFull), width, height, 1, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pic->pimg[0]), width, height, 3, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pic->pimg[1]), width, height, 5, 1, 2, 3, piana->imguserparam);
			}

            GetClubPath_Position_ImgProc(piana, camid, imgAbsDiff, imgTrans, imgTrans2); // 이름 바꿔야 함. 그냥 transpose+알파 만 하는 함수

			{
				U32 shotindex_trim;
				U32 rindex_trim;
				point_t toepos, heelpos, shaftpos;
				point_t toeshaftpos;
				double shaftangle;

                // this contains Hough Line transform, extracting toe and heel positions.
				res = GetClubPath_Position_Computation(piana, camid, imgTrans, imgTrans2, &toepos, &heelpos, &shaftpos, &shaftangle, &toeshaftpos);

				if (res == 0) {
					continue;
				}
				res = scamif_imagebuf_trimindex2(piana->hscamif, camid, normalbulk, rindex, &rindex_trim);
				res = scamif_imagebuf_trimindex2(piana->hscamif, camid, normalbulk, pcpc->shotindex, &shotindex_trim);
				
                if ((rindex_trim < shotindex_trim || 
                    rindex_trim > shotindex_trim + buffercount/2)				// Check Rolling over... 
					|| 
					(rindex_trim == shotindex_trim && m < pcpc->shotm)) {

					point_t Pptoe, Lptoe;
					point_t Ppheel, Lpheel;
					point_t Ppshaftpos, Lpshaftpos;
					point_t Pptoeshaft, Lptoeshaft;
					I64 ts64diff;

					Pptoe.x = toepos.x + offset_x;
					Pptoe.y = toepos.y + offset_y;
					iana_P2L(piana, camid, &Pptoe, &Lptoe, 0);

					Ppheel.x = heelpos.x + offset_x;
					Ppheel.y = heelpos.y + offset_y;
					iana_P2L(piana, camid, &Ppheel, &Lpheel, 0);

					Pptoeshaft.x = toeshaftpos.x + offset_x;
					Pptoeshaft.y = toeshaftpos.y + offset_y;
					iana_P2L(piana, camid, &Pptoeshaft, &Lptoeshaft, 0);

					Ppheel.x = heelpos.x + offset_x;
					Ppheel.y = heelpos.y + offset_y;

#define LPHEEL_0_X_ADD	0
#define LPHEEL_0_Y_ADD	0
					if (camid == 0) {
						Lpheel.x = Lpheel.x + LPHEEL_0_X_ADD;
						Lpheel.y = Lpheel.y + LPHEEL_0_Y_ADD;
					}

					Ppshaftpos.x = shaftpos.x + offset_x;
					Ppshaftpos.y = shaftpos.y + offset_y;
					iana_P2L(piana, camid, &Ppshaftpos, &Lpshaftpos, 0);

					ts64diff = ((I64)ts64Mt - (I64)pic->ts64shot);

					pcpc->h2state[rcount] 	= IANA_CLUB_EXIST;			// IANA_CLUB_EXILE;
					pcpc->h2ts[rcount] 		= ts64diff / TSSCALE_D;
					
                    pcpc->h2toepos[rcount].x = Lptoe.x;
					pcpc->h2toepos[rcount].y = Lptoe.y;

					pcpc->h2toeshaftpos[rcount].x = Lptoeshaft.x;
					pcpc->h2toeshaftpos[rcount].y = Lptoeshaft.y;



					pcpc->h2heelpos[rcount].x = Lpheel.x;
					pcpc->h2heelpos[rcount].y = Lpheel.y;

					pcpc->h2shaftpos[rcount].x = Lpshaftpos.x;
					pcpc->h2shaftpos[rcount].y = Lpshaftpos.y;

					pcpc->h2shaftangle[rcount] 	= shaftangle;
					pcpc->h2count = rcount;

					pcpc->h2shaftangle[rcount] 	= shaftangle;

					{ // make shaft line with heel point 
						cr_vector_t p0, p1, mm;

						//--
						p0.x = Lpheel.x; p0.y = Lpheel.y; p0.z = 0;
						p1.x = Lpshaftpos.x; p1.y = Lpshaftpos.y; p1.z = 0;

						cr_vector_sub(&p0, &p1, &mm);			// p0: start point, p1: end point, m: result vector
						cr_line_p0m(&p0, &mm, &pcpc->h2shaftline[rcount]);		// make shaft line with heel point 
					}


#define PRIODM_MULT_MAX	3 // 2, 5, 7, 10
					if (ts64diff < -(I64)(periodm * PRESHOTCOUNT)) {
						// DO NOTHING..
					} else if (ts64diff < (I64)periodm * PRIODM_MULT_MAX) {
						rcount++;
					} else {
						break;
					}

					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] (%4d-%2d / %4d-%2d) %10lf TOE  %10lf %10lf =>  %10lf %10lf  HEEL %10lf %10lf =>  %10lf %10lf  SHAFT %10lf %10lf =>  %10lf %10lf  (%10lf)\n", 
							camid, rindex, m,	
							(U32)pcpc->shotindex, (U32)pcpc->shotm, (ts64diff/TSSCALE_D), 
							Pptoe.x, Pptoe.y, Lptoe.x, Lptoe.y,
							Ppheel.x, Ppheel.y, Lpheel.x, Lpheel.y,
							Ppshaftpos.x, Ppshaftpos.y, Lpshaftpos.x, Lpshaftpos.y,
							shaftangle);
				}
			}
		} 	// for (m = 0; m < multitude; m++) 
	} 	// for (rindex = pcpc->startindex; rindex <= (U32)pcpc->endindex; rindex++)
    // Refact PH3:  position_computation으로? 아니면 UpdateClubPathContext함수로?
    //              ClubPathContext를 init하는 block으로 보임. 이후 exile하고 추가 계산해서 최종 context의 값을 계산하는 듯

	GetClubPath_Position_Exile(piana, camid);

#define RCOUNT_DISCARD	1 // 2
	rcount = pcpc->h2count - RCOUNT_DISCARD;
	pcpc->h2count = rcount;

	if (rcount > 3) {
		U32 isIron;
		res = GetClubPath_Position_Regression(piana, camid);

		if (piana->opmode == IANA_OPMODE_FILE) {
			res = DisplayClubPath(piana, camid, normalbulk);
		}

		isIron = check_isIron(piana, camid);

		if (pcpc->h2valid) {
			point_t p0, p1;
			double t0, t1;
			double angle;
			double clubspeed;
			double dx, dy, dlen;
			double x0_, y0_, t0_;
			double vv[CLUBDATACOUNT];
			U32 vvcount;
			double vv1[CLUBDATACOUNT];
			U32 vv1count;

#define TS	(-0.005) // (-0.002), (-0.010)
#define TE	( 0.000)
#define TD	(0.0005)

#define TE_A	(0.001)
#define TD_A	(0.0005)

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "============ weighted sum of toe and heel.\n");
			t0 = TS - TD;
			p0.x = pcpc->h2xma[2] * t0*t0 + pcpc->h2xma[1] * t0 + pcpc->h2xma[0];
			p0.y = pcpc->h2yma[2] * t0*t0 + pcpc->h2yma[1] * t0 + pcpc->h2yma[0];
			x0_ = p0.x;
			y0_ = p0.y;
			t0_ = t0;
			vvcount = 0;
			for (t1 = TS; t1 < TE; t1 += TD) 
			{
				double dtmp0, dtmp1;

				p1.x = pcpc->h2xma[2] * t1*t1 + pcpc->h2xma[1] * t1 + pcpc->h2xma[0];
				p1.y = pcpc->h2yma[2] * t1*t1 + pcpc->h2yma[1] * t1 + pcpc->h2yma[0];
				dx = p1.x - p0.x;
				dy = p1.y - p0.y;
				dlen = sqrt(dx*dx + dy*dy);
				angle = RADIAN2DEGREE(atan2(dx, dy));
				clubspeed = dlen / (t1 - t0);

				dx = p1.x - x0_;
				dy = p1.y - y0_;
				dtmp0 = sqrt(dx*dx + dy*dy);
				dtmp1= dtmp0 / (t1 - t0_);
				//vv[vvcount] = dtmp1;
				vv[vvcount] = clubspeed;

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d  %10lf] weighted sum of toe and heel.  angle: %10lf clubspeed: %10lf, clubspeedtotal = %10lf\n", 
						camid, t1, angle, clubspeed, dtmp1);
				p0.x = p1.x; p0.y = p1.y;
				t0 = t1;
				vvcount++;
			}

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "============ heel.\n");
			// heel.
			t0 = TS - TD;
			p0.x = pcpc->h2heelxma[2] * t0*t0 + pcpc->h2heelxma[1] * t0 + pcpc->h2heelxma[0];
			p0.y = pcpc->h2heelyma[2] * t0*t0 + pcpc->h2heelyma[1] * t0 + pcpc->h2heelyma[0];

			vv1count = 0;
			for (t1 = TS; t1 < TE; t1 += TD) {
				p1.x = pcpc->h2heelxma[2] * t1*t1 + pcpc->h2heelxma[1] * t1 + pcpc->h2heelxma[0];
				p1.y = pcpc->h2heelyma[2] * t1*t1 + pcpc->h2heelyma[1] * t1 + pcpc->h2heelyma[0];
				dx = p1.x - p0.x;
				dy = p1.y - p0.y;
				dlen = sqrt(dx*dx + dy*dy);
				angle = RADIAN2DEGREE(atan2(dx, dy));
				clubspeed = dlen / (t1 - t0);
				vv1[vv1count] = clubspeed;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d  %10lf] heel. angle: %10lf clubspeed1: %10lf\n",camid, t1, angle, clubspeed);
				p0.x = p1.x; p0.y = p1.y;
				t0 = t1;
				vv1count++;
			}
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "---------------\n");

			//--
			t0 = TE_A - TD_A;
			p0.x = pcpc->h2xma[2] * t0*t0 + pcpc->h2xma[1] * t0 + pcpc->h2xma[0];
			p0.y = pcpc->h2yma[2] * t0*t0 + pcpc->h2yma[1] * t0 + pcpc->h2yma[0];

			t1 = TE_A;

			p1.x = pcpc->h2xma[2] * t1*t1 + pcpc->h2xma[1] * t1 + pcpc->h2xma[0];
			p1.y = pcpc->h2yma[2] * t1*t1 + pcpc->h2yma[1] * t1 + pcpc->h2yma[0];
			dx = p1.x - p0.x;
			dy = p1.y - p0.y;
			dlen = sqrt(dx*dx + dy*dy);
			angle = RADIAN2DEGREE(atan2(dx, dy));
			if (angle > 90) {
				angle = 180 - angle;
			}
			if (angle < -90) {
				angle = 180 + angle;
			}

			if (angle > 50) {
				angle = 50;
			}
			if (angle < -50) {
				angle = -50;
			}
			pcpc->h2clubpath = angle;

			clubspeed = dlen / (t1 - t0);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d  %10lf] weighted sum of toe and heel. angle: %10lf clubspeed: %10lf\n",camid, t1, angle, clubspeed);

            // weighted sum of toe and heel. 
			{
				U32 centerindex;
				double vv0[CLUBDATACOUNT];

				//--
				memcpy(&vv0[0], &vv[0], sizeof(double)*vvcount);
				qsort( (void *) &vv0[0], vvcount, sizeof(double), doubledataCompare);
                centerindex = vvcount/2;						// 0.0.29.. :P
                pcpc->h2clubspeed_B = vv0[centerindex];
            }

#define CLUBSPEED_RATIO_IRON (1.065)// 20200903 // 1.03, 1.04, 1.06
#define	CLUBSPEED_RATIO_DRIVER (1/1.015)	// 20200903 // (1/1.04), (1/1.03)

// SG's suggestion.. 20200915 
#define CLUBSPEED_MULT_DRIVER_SG		1.045					// 20200915. 
#define CLUBSPEED_MULT_IRON_SG			1.025					// 20200915. 
			if (isIron) {
				pcpc->h2clubspeed_B = pcpc->h2clubspeed_B * CLUBSPEED_RATIO_IRON;
				pcpc->h2clubspeed_B = pcpc->h2clubspeed_B * CLUBSPEED_MULT_IRON_SG;			// 20200915
			} else {
				pcpc->h2clubspeed_B = pcpc->h2clubspeed_B * CLUBSPEED_RATIO_DRIVER;
				pcpc->h2clubspeed_B = pcpc->h2clubspeed_B * CLUBSPEED_MULT_DRIVER_SG;			// 20200915
			}

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " isIron:%d ---> clubspeed: %10lf\n", isIron, pcpc->h2clubspeed_B);

			//--
			{	// get center point, hit point
				point_t toepos;
				point_t heelpos;
				point_t centerpos;
				cr_point_t cp0, cp1;

				double thetashot;
				double dlenheel;		//  , dlen;
				double gearoffset;

				//--
				// 1) line between toepos and heelpos at t0.
				t0 = 0;
				cp0.x = toepos.x = pcpc->h2toexma[2] * t0*t0 + pcpc->h2toexma[1] * t0 + pcpc->h2toexma[0];
				cp0.y = toepos.y = pcpc->h2toeyma[2] * t0*t0 + pcpc->h2toeyma[1] * t0 + pcpc->h2toeyma[0];
				cp0.z = 0;

				cp1.x = heelpos.x = pcpc->h2heelxma[2] * t0*t0 + pcpc->h2heelxma[1] * t0 + pcpc->h2heelxma[0];
				cp1.y = heelpos.y = pcpc->h2heelyma[2] * t0*t0 + pcpc->h2heelyma[1] * t0 + pcpc->h2heelyma[0];
				cp1.z = 0;

				cr_line_p0p1(&cp0, &cp1, &pcpc->h2lineface);

				centerpos.x = (toepos.x + heelpos.x)/2.0;
				centerpos.y = (toepos.y + heelpos.y)/2.0;

				// 2) line shot 
				cp0.x = pic->icp.L.x;
				cp0.y = pic->icp.L.y;
				cp0.z = 0;

				thetashot = M_PI/2.0 - DEGREE2RADIAN(pcpc->h2clubpath);

				cp1.x = cp0.x + cos(thetashot);
				cp1.y = cp0.y + sin(thetashot);
				cp1.z = 0;

                cr_line_p0p1(&cp0, &cp1, &pcpc->h2lineshot);

				// 3) intersect point.
				cr_point_line_line(&pcpc->h2lineface, &pcpc->h2lineshot, &cp0, &cp1);

				pcpc->h2hitpos.x = (cp0.x + cp1.x)/2.0;
				pcpc->h2hitpos.y = (cp0.y + cp1.y)/2.0;

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "TOE: %10lf %10lf   HEEL: %10lf %10lf hit potion: %10lf %10lf and %10lf %10lf vs %10lf %10lf\n",
						toepos.x, toepos.y,
						heelpos.x, heelpos.y,
						cp0.x, cp0.y,
						cp1.x, cp1.y,
						centerpos.x, centerpos.y);

				dx = heelpos.x - pcpc->h2hitpos.x;
				dy = heelpos.y - pcpc->h2hitpos.y;
				dlenheel = sqrt(dx*dx + dy*dy);

				dx = heelpos.x - toepos.x;
				dy = heelpos.y - toepos.y;
				dlen = sqrt(dx*dx + dy*dy);

				gearoffset = dlenheel - dlen/2.0;

				pcpc->gearoffset = gearoffset;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "dlenheel: %lf dlen: %lf gearoffset: %lf\n", dlenheel, dlen, gearoffset);

			}
		}
	} else {
		res = 0;
	}
	res = 1;
//func_exit:
	return res;
#endif
}

#ifdef IPP_SUPPORT
static I32 GetClubPath_Position_Computation(iana_t *piana, U32 camid, U08 *pimgbuf, U32 width, U32 height, 
		point_t *ptoepos, 					
		point_t *pheelpos, 
		point_t *pshaftpos, 
		double *pshaftangle,
		point_t *ptoeshaftpos)
{
    I32 res;

    IppPointPolar delta;
    int maxLineCount;
    int BufSize;
    Ipp8u* pBuffer;
#define MAXLINECOUNT_	4
    IppPointPolar Line[MAXLINECOUNT_];
    int LineCount;
    IppPointPolar dstRoi[2];
    IppStatus ippstatus;

    IppiSize roisize;
    U32 imgoffset;

    IppiSize roisizeT;
    U32 imgoffsetT;
    U32	widthT;
    U32	heightT;
    U08 *pimgT;
    U08 *pimgT2;

    U32 i;
    U32 detected;
    double shaftangle;
    point_t toepos;
    U32 toeindex;
    point_t toeshaftpos;
    point_t heelpos;
    point_t shaftpos;		// one of shaft line point.
    //I32 hx, hy;
    double m_, b_;
    double theta, rho;
    I32 xygood;
    U32 pixelcount[FULLWIDTH];
    point_t shaftpoint[FULLWIDTH];		// shaft points.
    point_t Spoint[FULLWIDTH];			// start points for every shaft point.
    point_t Epoint[FULLWIDTH];			// End points for every shaft point.


    double from_rho, to_rho;
    iana_cam_t	*pic;
    clubpath_context_t 	*pcpc;
    //---------

    pic 	= piana->pic[camid];
    pcpc 	= &pic->cpc;

    roisize.width 	= width;
    roisize.height 	= height;
    imgoffset 		= 0;

    widthT 			= (height/4) * 4;
    heightT 		= width;
    roisizeT.width 	= widthT;
    roisizeT.height = heightT;
    imgoffsetT 		= 0;

    //hy = 0;			// 
    //hx = 0;
    shaftangle = 0;

    toepos.x = toepos.y = 0;
    toeshaftpos.x = toeshaftpos.y = 0;
    heelpos.x = heelpos.y = 0;

    //#define FROM_RHO		1
    //#define FROM_DEGREE		20
    //#define FROM_RHO		-999
#define FROM_RHO		1
    //#define FROM_DEGREE		0
#define FROM_DEGREE		20

    //#define TO_RHO			200
    //#define TO_DEGREE		120
//#define TO_RHO			1024
#define TO_RHO			700
//#define TO_DEGREE		160
//#define TO_DEGREE		100
#define TO_DEGREE		80

#define DELTA_RHO		2.0
//#define DELTA_RHO		1.0
//#define DELTA_DEGREE	2.0
#define DELTA_DEGREE	2.0

    from_rho = 10;
    to_rho = sqrt((double)(widthT*widthT + heightT*heightT));

    if (camid == 0) {
        //dstRoi[0].rho 	= (float) FROM_RHO;
        dstRoi[0].rho 	= (float)from_rho;
        dstRoi[0].theta = (Ipp32f)(float)DEGREE2RADIAN(FROM_DEGREE);

        //dstRoi[1].rho 	= (float) TO_RHO;
        dstRoi[1].rho 	= (float)to_rho;
        dstRoi[1].theta = (Ipp32f)(float)DEGREE2RADIAN(TO_DEGREE);

        delta.rho 	= (float)DELTA_RHO;
        delta.theta = (float)DEGREE2RADIAN(DELTA_DEGREE);

#define ANGLERANGE_COUNT	3
        if (pcpc->h2count >= ANGLERANGE_COUNT) {
            double ca0, ca1;
            U32 currentindex;

            currentindex = pcpc->h2count;
#define ANGLE_DELTA0	10
#define ANGLE_DELTA1	30
            ca0 = pcpc->h2shaftangle[currentindex] - ANGLE_DELTA0;
            ca1 = pcpc->h2shaftangle[currentindex] +  ANGLE_DELTA1;
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%d ca0: %lf, ca1: %lf\n", currentindex, ca0, ca1);
            dstRoi[0].theta = (Ipp32f)(float)DEGREE2RADIAN(ca0);
            dstRoi[1].theta = (Ipp32f)(float)DEGREE2RADIAN(ca1);
        }
    } else {
        //dstRoi[0].rho 	= (float) FROM_RHO;
        dstRoi[0].rho 	= (float)from_rho;
        dstRoi[0].theta = (Ipp32f)(float)DEGREE2RADIAN(40);

        //dstRoi[1].rho 	= (float) TO_RHO;
        dstRoi[1].rho 	= (float)to_rho;
        dstRoi[1].theta = (Ipp32f)(float)DEGREE2RADIAN((180-40));

        delta.rho 	= (float)DELTA_RHO;
        delta.theta = (float)DEGREE2RADIAN(DELTA_DEGREE);

    }
    maxLineCount = MAXLINECOUNT_;

    ippstatus = ippiHoughLineGetSize_8u_C1R(
        roisizeT,
        delta,
        maxLineCount,
        &BufSize);

    //pBuffer = ippsMalloc_8u(BufSize);
    pBuffer = (U08 *)malloc(BufSize);
    pimgT = (U08 *)malloc(width * height);
    pimgT2 = (U08 *)malloc(width * height);

    memset(pimgT, 0, width*height);
    memset(pimgT2, 0, width*height);
    //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "### h  #2\n");

    if (camid == 111111111) {
        memcpy(pimgT, pimgbuf, width*height);
    } else {
        U32	height0;
        IppiSize roisize0;

        //--
        height0 = (height/4)*4;
        roisize0.height = height0;
        roisize0.width = width;
        ippstatus = ippiTranspose_8u_C1R(
            pimgbuf + imgoffset, width,
            pimgT 	+ imgoffsetT, widthT, // height0, //widthT0,
            roisize0 	// roisize
        );
    }

    //#define HOUGHTHRESHOLD_	30
#define HOUGHTHRESHOLD_	15


    {				// get Shaft with Hough Line detecetion
        IppiSize roisizeMT;
        //IppiSize roisizeHoughT;
        double mean, stddev;
        double threshold;
        //#define DDDD	4
#define DDDD	5
        roisizeMT.width	 = widthT - (DDDD*2);
        roisizeMT.height = heightT - (DDDD*2);

        imgoffsetT = DDDD + DDDD * widthT;

        //#define MULC_FOR_CLUB	3
#define MULC_FOR_CLUB	4
//#define MULC_FOR_CLUB	5
        ippiMulC_8u_C1IRSfs(
            MULC_FOR_CLUB,
            pimgT,
            widthT,
            roisizeT,
            0
        );

        ippiFilterMedianCross_8u_C1R(
            pimgT  + imgoffsetT, widthT, // source
            pimgT2 + imgoffsetT, widthT, // dest
            roisizeMT,
            ippMskSize5x5 //ippMskSize3x3
        );


        ippiMean_StdDev_8u_C1R(pimgT2, widthT, roisizeT, &mean, &stddev);
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "#1 mean: %lf, stddev: %lf\n", mean, stddev);
        {
            IppiSize roisize2;
            roisize2.width 	= widthT/2;
            roisize2.height	= heightT;
            ippiMean_StdDev_8u_C1R(pimgT2+widthT/2, widthT, roisize2, &mean, &stddev);
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "#2 mean: %lf, stddev: %lf\n", mean, stddev);
        }
        //#define MSTDK	1.0	
        ///////#define MSTDK	0.9
#define MSTDK	0.5
//#define MSTDK	0.7
//#define MSTDK	0.5
//#define MSTDK	0.2
        //#define MSTDK	0
        threshold = mean + stddev * MSTDK;
#define MIN_THR	30
        //#define MIN_THR	40
#define MAX_THR	200

#undef MIN_THR
#undef MAX_THR
#define MIN_THR	20
#define MAX_THR	40
//#define MAX_THR	50
//#define MAX_THR	60

        if (threshold < MIN_THR) {
            threshold = MIN_THR;
        }
        if (threshold > MAX_THR) {
            threshold = MAX_THR;
        }
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mean: %lf, stddev: %lf, threshold: %lf\n", mean, stddev, threshold);
        ippiCompareC_8u_C1R(
            pimgT2, widthT, (Ipp8u)threshold,	// source
            pimgT, widthT,	// binary image. dest.
            roisizeT, ippCmpGreater
        );

        /*
           ippiErode3x3_8u_C1R(
           pimgT +4 + 4 * widthT, widthT, // source
           pimgT2  +4 + 4 * widthT, widthT, // dest
           roisizeMT
           );
           */
        ippiErode3x3_8u_C1IR(
            pimgT + imgoffsetT, // dest
            widthT,
            roisizeMT
        );
        ippiErode3x3_8u_C1IR(
            pimgT + imgoffsetT, // dest
            widthT,
            roisizeMT
        );
        ippiDilate3x3_8u_C1IR(
            pimgT + imgoffsetT, // dest
            widthT,
            roisizeMT
        );

        {
            U08 *pb0, *pb1;
            memset(pimgT, 0, widthT * (DDDD+2));
            memset(pimgT + (heightT - (DDDD+2)) * widthT-1, 0, widthT * (DDDD+2));

            for (i = 0; i < heightT; i++) {
                pb0 = pimgT + i * widthT;
                pb1 = pimgT + (i+1) * widthT - (DDDD+2);
                memset(pb0, 0, (DDDD+2));
                memset(pb1, 0, (DDDD+2));
            }
        }

        {
            memset(pimgT2, 0, widthT*heightT);
            for (i = 0; i < heightT; i++) {
                memcpy(pimgT2 + widthT/2 + i * widthT, pimgT + widthT/2 + i * widthT, widthT/2);
            }
        }

        if (piana->himgfunc) {
            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pimgT), widthT, heightT, 0, 1, 2, 3, piana->imguserparam);
            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pimgT2), widthT, heightT, 1, 1, 2, 3, piana->imguserparam);
            //((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pimgbuf), width, height, 2, 1, 2, 3, piana->imguserparam);
        }

        ippstatus = ippiHoughLine_Region_8u32f_C1R(
            //pimgT2 + imgoffsetT, 
            pimgT2,
            widthT,
            roisizeMT,
            Line,
            dstRoi,
            MAXLINECOUNT_,
            &LineCount,
            delta,
            HOUGHTHRESHOLD_,						// threshold
            pBuffer);

    }

    free(pBuffer);

    //LineCount = 1;
    xygood = 0;
    detected = 0;
    memset(&pixelcount[0], 0, sizeof(U32) * FULLWIDTH);
    memset(&Spoint[0], 0, sizeof(point_t) * FULLWIDTH);
    memset(&Epoint[0], 0, sizeof(point_t) * FULLWIDTH);
    memset(&shaftpos, 0, sizeof(point_t));
    if (LineCount > 0) {
        //	for (i = 0; i < (U32)LineCount; i++) {
        point_t p1, p2;
        point_t pp[2];

        //----
        theta 	= Line[0].theta;
        //#define RHO_OFFSET	5
        rho 	= Line[0].rho;
#if defined(RHO_OFFSET)
        rho 	= rho + RHO_OFFSET;
#endif
        m_ 		= -1 / tan(theta);
        b_ 		= (rho / sin(theta));
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "theta: %lf rho: %lf m_: %lf b_: %lf\n", theta, rho, m_, b_);
#define M_SMALLVALUE	(1e-5)
        if (m_ > -M_SMALLVALUE && m_ < M_SMALLVALUE) {
            xygood = 0;
        } else {
            //-- Get start point of shaft line	(contact with lower branch of box)
            p1.y = (I32)heightT-1;
            p1.x = (I32)((p1.y - b_) / m_);
            //p1.x = (I32)((p1.y - b_) / m_) + widthT/2;
            if (p1.x >= 0 && (I32)p1.x < (I32)widthT) {
                pp[xygood].x = p1.x;
                pp[xygood].y = p1.y;
                xygood++;
            } else {
                p1.x = 0;
                p1.y = (m_ * p1.x + b_);
                if (p1.y >= 0 && (I32)p1.y < (I32)heightT) {
                    pp[xygood].x = p1.x;
                    pp[xygood].y = p1.y;
                    xygood++;
                }
            }

            //-- Get end point of shaft line	(contact with upper branch of box)
            if (xygood == 1) {
                p1.y = 0;
                p1.x = ((p1.y - b_) / m_);
                if (p1.x >= 0 && (I32)p1.x < (I32)widthT) {
                    pp[xygood].x = p1.x;
                    pp[xygood].y = p1.y;
                    xygood++;
                } else {
                    p1.x = widthT-1;
                    p1.y = (m_ * p1.x + b_);
                    if (p1.y >= 0 && (I32)p1.y < (I32)heightT) {
                        pp[xygood].x = p1.x;
                        pp[xygood].y = p1.y;
                        xygood++;
                    }
                }
            }
        }

        if (xygood == 2) {
            shaftangle = RADIAN2DEGREE(theta);

            if (theta > DEGREE2RADIAN(10.0) && theta < DEGREE2RADIAN(170.0)) { // proper range of shaft angle
                for (i = 0; i < widthT; i++) {		// get pixel count among shaft.
                    I32 h;
                    I32 data;
                    p1.x = i;
                    p1.y = (I32)(m_ * p1.x + b_);
                    //#define HLINE	40
#define HLINE0	40
//#define HLINE1	10
#define HLINE1	15
                    shaftpoint[i].x = p1.x;
                    shaftpoint[i].y = p1.y;
                    for (h = -HLINE0; h < HLINE1; h++) {
                        p2.x = (I32)(h * cos(theta) + p1.x);
                        p2.y = (I32)(h * sin(theta) + p1.y);

                        if ((p2.x >= 0 && (I32)p2.x < (I32)widthT) && (p2.y >= 0 && (I32)p2.y < (I32)heightT)) {
                            data = (U32)*(pimgT + (I32)p2.x + (I32)p2.y * widthT);
                            if (data) {
                                if (pixelcount[i] == 0) {
                                    Spoint[i].x = p2.x;
                                    Spoint[i].y = p2.y;
                                }
                                Epoint[i].x = p2.x;
                                Epoint[i].y = p2.y;
                                pixelcount[i]++;
                            } else {
#define MINPIXEL_PIXEL	10
                                if (pixelcount[i] < MINPIXEL_PIXEL) {		// clear.. :P
                                    pixelcount[i] = 0;
                                }
                            }
                        } else {
                            //
                        }
                    }
                }


                res = toeposition(
                    widthT, heightT,
                    &shaftpoint[0],
                    &Spoint[0],			// start points for every shaft point.
                    &Epoint[0],			// End points for every shaft point.
                    &pixelcount[0],
                    &toepos,
                    &toeindex,
                    &toeshaftpos
                );
                if (res) {
                    double thetas_;
                    detected = 1;
                    // get heel position

                    res = heelposition(
                        piana, camid,
                        widthT, heightT,
                        &toepos,
                        m_, b_,
                        theta,
                        pimgT,
                        &heelpos
                    );
                    thetas_ = M_PI/2.0 - DEGREE2RADIAN(shaftangle);
#define SHAFTBRANCH	50.0
                    shaftpos.x = heelpos.x + SHAFTBRANCH * cos(thetas_);
                    //shaftpos.y = heelpos.y + SHAFTBRANCH * sin(thetas_);
                    shaftpos.y = heelpos.y - SHAFTBRANCH * sin(thetas_);
                }

                if (piana->opmode == IANA_OPMODE_FILE) {
                    for (i = 0; i < widthT; i++) {
                        I32 h;
                        I32 data;

                        //--
                        h = pixelcount[i];
                        if (h > 0) {
                            I32 x1, y1;
                            I32 x2, y2;
                            x1 = i;
                            y1 = (I32)(m_ * x1 + b_);

                            x2 = (I32)(h * cos(theta) + x1);
                            y2 = (I32)(h * sin(theta) + y1);
                            if ((x2 >= 0 && x2 < (I32)widthT) && (y2 >= 0 && y2 < (I32)heightT)) {
                                data = *(pimgT + x2 + y2 * widthT);
                                data = 0xFF - data;
                                *(pimgT + x2 + y2 * widthT) = (U08)data;
                            }
                        }
                    }
                }
            }
            if (piana->opmode == IANA_OPMODE_FILE) {
                if (detected) {		// Display Toe points
                    I32 x1, y1;
                    //I32 x2, y2;
                    I32 sx, sy, ex, ey;
                    //double m2_, b2_;

                    //--- toe
                    x1 = (I32)toepos.x;
                    y1 = (I32)toepos.y;
                    //m2_ = tan(theta);
                    //b2_ = (-x1 * m2_ + y1);

                    //x2 = (I32) (-(b2_-b_) / (m2_ - m_));
                    //y2 = (I32) (m2_ * x2 + b2_);

                    sx = x1 - 4;
                    sy = y1 - 4;
                    ex = x1 + 4;
                    ey = y1 + 4;

                    //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "toe %d %d    %d %d with m_: %lf,  m2_: %lf\n", x1, y1, x2, y2, m_, m2_);

                    myrectGray(sx, sy, ex, ey, 0xFF, pimgT, widthT);


                    //--   toeshaft
                    x1 = (I32)toeshaftpos.x;
                    y1 = (I32)toeshaftpos.y;


                    sx = x1 - 4;
                    sy = y1 - 4;
                    ex = x1 + 4;
                    ey = y1 + 4;
                    myrectGray(sx, sy, ex, ey, 0x20, pimgT, widthT);

                    //-- heel
                    x1 = (I32)heelpos.x;
                    y1 = (I32)heelpos.y;

                    sx = x1 - 4;
                    sy = y1 - 4;
                    ex = x1 + 4;
                    ey = y1 + 4;

                    myrectGray(sx, sy, ex, ey, 0xBB, pimgT, widthT);

                    //--
                    x1 = (I32)shaftpos.x;
                    y1 = (I32)shaftpos.y;

                    sx = x1 - 4;
                    sy = y1 - 4;
                    ex = x1 + 4;
                    ey = y1 + 4;

                    myrectGray(sx, sy, ex, ey, 0x44, pimgT, widthT);
                }
            }
        }
        {	// Display Shaft line.
            mylineGray((I32)pp[0].x, (I32)pp[0].y, (I32)pp[1].x, (I32)pp[1].y, 0xFF, pimgT, widthT);
        }
        //}
    }

    if (detected) {
        //#define TOEPOS_MARGIN	20
#define TOEPOS_MARGIN	40
        if (toepos.y > heightT - TOEPOS_MARGIN) {
            detected = 0;
            res = 0;
        } else {
            ptoepos->x 	= toepos.y; //  Transpose.. :P
            ptoepos->y 	= toepos.x;

            ptoeshaftpos->x 	= toeshaftpos.y; //  Transpose.. :P
            ptoeshaftpos->y 	= toeshaftpos.x;

            pheelpos->x = heelpos.y; //  Transpose.. :P
            pheelpos->y = heelpos.x;
            pshaftpos->x= shaftpos.y;	// Transpose.. :D
            pshaftpos->y= shaftpos.x;

            *pshaftangle = shaftangle;
            res = 1;
        }
    } else {
        res = 0;
    }

    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(pimgT), widthT, heightT, 0, 1, 2, 3, piana->imguserparam);
    }
    free(pimgT);
    free(pimgT2);

    //
    //piana; camid; phx; phy; pshaftangle;

    return res;
#else
static I32 GetClubPath_Position_Computation(
    iana_t *piana, U32 camid,
    cv::Mat imgTrans, cv::Mat imgTrans2,
    point_t *ptoepos,
    point_t *pheelpos,
    point_t *pshaftpos,
    double *pshaftangle,
    point_t *ptoeshaftpos)
{
	I32 res;

    BOOL foundLine;

	U32	widthT, heightT;

	U32 i;
	U32 toeDetected, heelDetected;
	double shaftangle;
    U32 toeindex;
	point_t toepos;
	point_t toeshaftpos;
	point_t heelpos;
	point_t shaftpos;		// one of shaft line point.
	
    double m_, b_;
	double theta, rho;
	I32 xygood;

	U32 pixelcount[FULLWIDTH];
	point_t shaftpoint[FULLWIDTH];		// shaft points.
	point_t Spoint[FULLWIDTH];			// start points for every shaft point.
	point_t Epoint[FULLWIDTH];			// End points for every shaft point.

    iana_cam_t	*pic;
	clubpath_context_t 	*pcpc;
    U08* pimgT;
	//---------

	pic 	= piana->pic[camid];
	pcpc 	= &pic->cpc;

    pimgT = (U08*)imgTrans.data;
    widthT = imgTrans.cols;
    heightT = imgTrans.rows;

	shaftangle = 0;
    {				// get Shaft with Hough Line detecetion
        double fromRho, toRho;
        double fromTheta, toTheta;
        double deltaRho, deltaTheta;
        std::vector<cv::Vec3f> myLines;

#define FROM_RHO		1
#define FROM_DEGREE		20 // 0

#define TO_RHO			700 // 200, 1024
#define TO_DEGREE		80 // 120, 160, 100

#define DELTA_RHO		2.0 // 1.0
#define DELTA_DEGREE	2.0 // 2.0

        fromRho = 10;
        toRho = sqrt((double)(widthT*widthT + heightT*heightT));

        if (camid == 0) {
            fromTheta = (double)DEGREE2RADIAN(FROM_DEGREE);

            toTheta = (double)DEGREE2RADIAN(TO_DEGREE);

            deltaRho 	= (double)DELTA_RHO;
            deltaTheta = (double)DEGREE2RADIAN(DELTA_DEGREE);

#define ANGLERANGE_COUNT	3
            if (pcpc->h2count >= ANGLERANGE_COUNT) {
                double ca0, ca1;
                U32 currentindex;

                currentindex = pcpc->h2count;
#define ANGLE_DELTA0	10
#define ANGLE_DELTA1	30
                ca0 = pcpc->h2shaftangle[currentindex] - ANGLE_DELTA0;
                ca1 = pcpc->h2shaftangle[currentindex] +  ANGLE_DELTA1;
                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%d ca0: %lf, ca1: %lf\n", currentindex, ca0, ca1);

                fromTheta = (double)DEGREE2RADIAN(ca0);
                toTheta = (double)DEGREE2RADIAN(ca1);
            }
        } else {
            fromTheta = (double)DEGREE2RADIAN(40);

            toTheta = (double)DEGREE2RADIAN((180-40));

            deltaRho 	= (double)DELTA_RHO;
            deltaTheta = (double)DEGREE2RADIAN(DELTA_DEGREE);
        }

#define HOUGHTHRESHOLD_	15 // 30.0
        cv::HoughLines(imgTrans2, myLines,
            deltaRho, deltaTheta,
            HOUGHTHRESHOLD_, 0, 0,
            fromTheta, toTheta);

        foundLine = false;
        theta = 0;
        rho = 0;

        for (i=0; i<myLines.size(); i++) {
            rho = myLines[i][0];
            theta = myLines[i][1];
            if (fromRho < rho && rho < toRho) {
                foundLine = true;
                break;
            }
        }
    }

	xygood = 0;
    toeDetected = heelDetected = 0;
	memset(&pixelcount[0], 0, sizeof(U32) * FULLWIDTH);
	memset(&Spoint[0], 0, sizeof(point_t) * FULLWIDTH);
	memset(&Epoint[0], 0, sizeof(point_t) * FULLWIDTH);
	memset(&shaftpos, 0, sizeof(point_t));

    toepos.x = toepos.y = 0;
    toeshaftpos.x = toeshaftpos.y = 0;
    heelpos.x = heelpos.y = 0;

	if (foundLine) {
		point_t p1, p2;
		point_t pp[2] = {0};

		//----
#if defined(RHO_OFFSET)
		rho 	= rho + RHO_OFFSET;
#endif
		m_ 		= - 1 / tan(theta);
		b_ 		= (rho / sin(theta));
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "theta: %lf rho: %lf m_: %lf b_: %lf\n", theta, rho, m_, b_);

#define M_SMALLVALUE	(1e-5)
		if (m_ > -M_SMALLVALUE && m_ < M_SMALLVALUE) { 
			xygood = 0;
		} else {
			//-- Get start point of shaft line	(contact with lower branch of box)
			p1.y = (I32) heightT-1;
			p1.x = (I32)((p1.y - b_) / m_);
			if (p1.x >= 0 && (I32)p1.x < (I32)widthT) {
				pp[xygood].x = p1.x;
				pp[xygood].y = p1.y;
				xygood++;
			} else {
				p1.x = 0;
				p1.y = ( m_ * p1.x + b_);
				if (p1.y >= 0 && (I32)p1.y < (I32)heightT) {
					pp[xygood].x = p1.x;
					pp[xygood].y = p1.y;
					xygood++;
				}
			}

			//-- Get end point of shaft line	(contact with upper branch of box)
			if (xygood == 1) {
				p1.y = 0;
				p1.x = ((p1.y - b_) / m_);
				if (p1.x >= 0 && (I32)p1.x < (I32)widthT) {
					pp[xygood].x = p1.x;
					pp[xygood].y = p1.y;
					xygood++;
				} else {
					p1.x = widthT-1;
					p1.y = ( m_ * p1.x + b_);
					if (p1.y >= 0 && (I32)p1.y < (I32)heightT) {
						pp[xygood].x = p1.x;
						pp[xygood].y = p1.y;
						xygood++;
					}
				}
			}
		}

		if (xygood == 2) {
			shaftangle = RADIAN2DEGREE(theta);

			if (theta > DEGREE2RADIAN(10.0) && theta < DEGREE2RADIAN(170.0)) { // proper range of shaft angle
				for (i = 0; i < widthT; i++) {		// get pixel count among shaft.
					I32 h;
					I32 data;
					p1.x = i;
					p1.y = (I32) (m_ * p1.x + b_);

#define HLINE0	40
#define HLINE1	15 // 10
					shaftpoint[i].x = p1.x;
					shaftpoint[i].y = p1.y;
					for (h = -HLINE0; h < HLINE1; h++) {
						p2.x = (I32) (h * cos(theta) + p1.x);
						p2.y = (I32) (h * sin(theta) + p1.y);

						if ((p2.x >= 0 && (I32)p2.x < (I32)widthT) && (p2.y >= 0 && (I32)p2.y < (I32)heightT)) {
							data = (U32)*(pimgT + (I32)p2.x + (I32)p2.y * widthT);

							if (data) {
								if (pixelcount[i] == 0) {
									Spoint[i].x = p2.x;
									Spoint[i].y = p2.y;
								}

								Epoint[i].x = p2.x;
								Epoint[i].y = p2.y;
								pixelcount[i]++;
							} else {

#define MINPIXEL_PIXEL	10
								if (pixelcount[i] < MINPIXEL_PIXEL) {		// clear.. :P
									pixelcount[i] = 0;
								}
							}
						} else {
							//
						}
					}
				}

                toeDetected = toeposition(
						widthT, heightT,
						&shaftpoint[0],
						&Spoint[0],			// start points for every shaft point.
						&Epoint[0],			// End points for every shaft point.
						&pixelcount[0],
						&toepos,
						&toeindex,
						&toeshaftpos
						);

				if (toeDetected) {
					double thetas_;

                    // get heel position
					heelDetected = heelposition(
							piana, camid,
							widthT, heightT,
							&toepos,
							m_, b_,
							theta,
							pimgT,
							&heelpos
							);

					thetas_ = M_PI/2.0 - DEGREE2RADIAN(shaftangle);

#define SHAFTBRANCH	50.0
					shaftpos.x = heelpos.x + SHAFTBRANCH * cos(thetas_);
					shaftpos.y = heelpos.y - SHAFTBRANCH * sin(thetas_);
				}

				if (piana->opmode == IANA_OPMODE_FILE) {
					for (i = 0; i < widthT; i++) {
						I32 h;
						I32 data;

						//--
						h = pixelcount[i];
						if (h > 0) {
							I32 x1, y1;
							I32 x2, y2;
							x1 = i;
							y1 = (I32) (m_ * x1 + b_);

							x2 = (I32) (h * cos(theta) + x1);
							y2 = (I32) (h * sin(theta) + y1);
							if ((x2 >= 0 && x2 < (I32)widthT) && (y2 >= 0 && y2 < (I32)heightT)) {
								data = *(pimgT + x2 + y2 * widthT);
								data = 0xFF - data;
								*(pimgT + x2 + y2 * widthT) = (U08) data;
							}
						}
					}
				}
			}

			if (piana->opmode == IANA_OPMODE_FILE) {
				if (toeDetected && heelDetected) {		// Display Toe points
					I32 x1, y1;
					I32 sx, sy, ex, ey;

					//--- toe
                    x1 = (I32) toepos.x;
					y1 = (I32) toepos.y;

					sx = x1 - 4;
					sy = y1 - 4;
					ex = x1 + 4;
					ey = y1 + 4;

                    cv::rectangle(imgTrans, cv::Rect(sx, sy, ex-sx, ey-sy), 0xFF);
					//myrectGray(sx, sy, ex, ey, 0xFF, pimgT, widthT);

					//--   toeshaft
					x1 = (I32) toeshaftpos.x;
					y1 = (I32) toeshaftpos.y;

					sx = x1 - 4;
					sy = y1 - 4;
					ex = x1 + 4;
					ey = y1 + 4;

                    cv::rectangle(imgTrans, cv::Rect(sx, sy, ex-sx, ey-sy), 0x20);
					//myrectGray(sx, sy, ex, ey, 0x20, pimgT, widthT);

					//-- heel
					x1 = (I32) heelpos.x;
					y1 = (I32) heelpos.y;

					sx = x1 - 4;
					sy = y1 - 4;
					ex = x1 + 4;
					ey = y1 + 4;

                    cv::rectangle(imgTrans, cv::Rect(sx, sy, ex-sx, ey-sy), 0xBB);
					// myrectGray(sx, sy, ex, ey, 0xBB, pimgT, widthT);

					//--
					x1 = (I32) shaftpos.x;
					y1 = (I32) shaftpos.y;

					sx = x1 - 4;
					sy = y1 - 4;
					ex = x1 + 4;
					ey = y1 + 4;

                    cv::rectangle(imgTrans, cv::Rect(sx, sy, ex-sx, ey-sy), 0x44);
					// myrectGray(sx, sy, ex, ey, 0x44, pimgT, widthT);
				}
			}
		}

		{ // Display Shaft line.
            cv::line(imgTrans, cv::Point((I32)pp[0].x, (I32)pp[0].y), cv::Point((I32)pp[1].x, (I32)pp[1].y), 0xFF);
            //mylineGray((I32)pp[0].x, (I32)pp[0].y, (I32)pp[1].x, (I32)pp[1].y, 0xFF, pimgT, widthT);
		}
	}

	if (toeDetected && heelDetected) {
#define TOEPOS_MARGIN	40 // 20
		if (toepos.y > heightT - TOEPOS_MARGIN) {
			res = 0;
		} else {
			ptoepos->x 	= toepos.y; //  Transpose.. :P
			ptoepos->y 	= toepos.x;

			ptoeshaftpos->x 	= toeshaftpos.y; //  Transpose.. :P
			ptoeshaftpos->y 	= toeshaftpos.x;

			pheelpos->x = heelpos.y; //  Transpose.. :P
			pheelpos->y = heelpos.x;
			pshaftpos->x= shaftpos.y;	// Transpose.. :D
			pshaftpos->y= shaftpos.x;

			*pshaftangle = shaftangle;
			res = 1;
		}
	} else {
		res = 0;
	}

	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)(pimgT), widthT, heightT, 0, 1, 2, 3, piana->imguserparam);
	}
	return res;
#endif
}

#ifndef IPP_SUPPORT
static I32 GetClubPath_Position_ImgProc(iana_t* piana, U32 camid, cv::Mat imgBuf, cv::Mat imgTrans, cv::Mat imgTrans2)
{
    I32 res;

    U32	width, height, widthT, heightT;

    cv::Size imgSize = imgBuf.size();
    cv::Mat imgMorphed;

    iana_cam_t	*pic;
    clubpath_context_t 	*pcpc;
    //---------

    pic 	= piana->pic[camid];
    pcpc 	= &pic->cpc;

    width = heightT = imgSize.width;
    height = widthT = imgSize.height;

    cv::transpose(imgBuf, imgTrans);

    {
        double threshold;

        cv::Mat cvMean, cvStddev;

#define DDDD	5 // 4
#define MULC_FOR_CLUB	4 // 3, 5
        imgTrans *= MULC_FOR_CLUB;
        cv::medianBlur(imgTrans, imgTrans2, 5);
        cv::meanStdDev(imgTrans2(cv::Rect(widthT/2, 0, widthT/2, heightT)), cvMean, cvStddev);

#define MSTDK	0.5 // 1.0, 0.9, 0.7, 0.5, 0.2
        threshold = cvMean.at<double>(0) + cvStddev.at<double>(0) * MSTDK;

#define MIN_THR	30 // 50
#define MAX_THR	40 // 60
        if (threshold < MIN_THR) {
            threshold = MIN_THR;
        }
        if (threshold > MAX_THR) {
            threshold = MAX_THR;
        }

        cv::threshold(imgTrans2, imgTrans, threshold, 255.0, cv::THRESH_BINARY);
        {
            cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3), cv::Point(-1, -1));
            cv::morphologyEx(imgTrans, imgMorphed, cv::MORPH_ERODE, kernel, cv::Point(-1, -1), 2, cv::BORDER_ISOLATED);
            cv::morphologyEx(imgMorphed, imgTrans, cv::MORPH_DILATE, kernel, cv::Point(-1, -1), 1, cv::BORDER_ISOLATED);
        }

        // make some frame to imgmorphed and save it to imgtrans
        memset(imgTrans.data, 0, width*height);
        imgTrans = 0;

#define FRAME cv::Rect(DDDD+2, DDDD+2, height-(DDDD+2)*2, width-(DDDD+2)*2)
        imgMorphed(FRAME).copyTo(imgTrans(FRAME));

        // make some frame to imgtrans and save it to imgtrans2
        memset(imgTrans2.data, 0, width*height);
        imgTrans2 = 0;

#define FRAME2 cv::Rect(height/2, 0, height/2, width)
        imgTrans(FRAME2).copyTo(imgTrans2(FRAME2));

        if (piana->himgfunc) {
            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(imgTrans.data), widthT, heightT, 0, 1, 2, 3, piana->imguserparam);
            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(imgTrans2.data), widthT, heightT, 1, 1, 2, 3, piana->imguserparam);
            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(imgMorphed.data), widthT, heightT, 2, 1, 2, 3, piana->imguserparam);
        }
    }
    res = 1;
    return res;
}
#endif

static I32 toeposition(
	U32 width, U32 height,
	point_t shaftpoint[],
	point_t Spoint[],			// start points for every shaft point.
	point_t Epoint[],			// End points for every shaft point.
	U32 pixelcount[],
	point_t *ptoepos,
	U32 *ptoeindex,
    point_t *ptoeshaftpos)
{						// get toe point candidate
	I32 res;
	I32 i;
	U32 checkcount;
	double maxdist;
	U32 maxdistindex;
	double dx, dy, dist;
	U32 detected;

	//--
	detected = 0;

	checkcount = 0;
	maxdist = 0;
	maxdistindex = 0;
//	ptoepos->x = 0;			// toe position.
//	ptoepos->y = 0;			// toe position.
	for (i = 0; i < (I32)width; i++) {
#define DETECTPIXELCOUNT	10
//#define DETECTCHECKCOUNT	15
#define DETECTCHECKCOUNT	30
		if (detected == 0) {
			if (pixelcount[i] > DETECTPIXELCOUNT) {
				detected = 1;

			}
		}

		if (detected) {
			if (pixelcount[i] > DETECTPIXELCOUNT) {
				if (checkcount < DETECTCHECKCOUNT) {
					dx = Spoint[i].x - shaftpoint[i].x;
					dy = Spoint[i].y - shaftpoint[i].y;

					dist = sqrt(dx*dx + dy*dy);
					if (maxdist < dist) {
						maxdist = dist;
						maxdistindex = i;

						ptoepos->x = Spoint[i].x;
						ptoepos->y = Spoint[i].y;

						ptoeshaftpos->x = shaftpoint[i].x;
						ptoeshaftpos->y = shaftpoint[i].y;
					}
				} else {
					break;
				}
			}
			checkcount++;
		}
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d %4d] pcount: %d\n", camid, i,  pixelcount[i]);
	}
	if (detected) {
		*ptoeindex = maxdistindex;
		res = 1;
	} else {
		res = 0;
	}

	UNUSED(Epoint); UNUSED(height);
	return res;
}


static I32 heelposition(
		iana_t *piana, U32 camid,
		U32 width, U32 height,
		point_t *ptoepos,
		double m_, double b_,		// shaft line..
		double theta,				// angle of shaft line
		U08 *pimgT,
		point_t *pheelpos
		)
{
	I32 res;

	iana_cam_t	*pic;

	point_t Lp0, Lp1;
	point_t Pp0, Pp1;
	double llen0, llen1;				// minumum/maximum club face size
	double dx, dy;
	double dist;					// distance between shaft line and toe point.
	double phi0, phi1;

	//-----
	pic 	= piana->pic[camid];

//#define CLUBFACELEN0		(0.08)
//#define CLUBFACELEN0		(0.04)
#define CLUBFACELEN0		(0.05)
//#define CLUBFACELEN1		(0.14)
#define CLUBFACELEN1		(0.18)
#define CLUBFACELEN_DELTA	(0.02)

	Lp0.x = pic->icp.L.x;
	Lp0.y = pic->icp.L.y;

	//-- minimum club face size;
	Lp1.x = Lp0.x;
	Lp1.y = Lp0.y + CLUBFACELEN0;

	iana_L2P(piana, camid, &Lp0, &Pp0, 0);
	iana_L2P(piana, camid, &Lp1, &Pp1, 0);
	dx = Pp0.x - Pp1.x;
	dy = Pp0.y - Pp1.y;
	llen0 = sqrt(dx*dx + dy*dy);		

	//-- maximum club face size;
	Lp1.x = Lp0.x;
	Lp1.y = Lp0.y + CLUBFACELEN1;

	iana_L2P(piana, camid, &Lp0, &Pp0, 0);
	iana_L2P(piana, camid, &Lp1, &Pp1, 0);
	dx = Pp0.x - Pp1.x;
	dy = Pp0.y - Pp1.y;
	llen1 = sqrt(dx*dx + dy*dy);

	//-- distance between shaft line and toe point.
	//    |mx1 - y1 + b| / sqrt(m^2 + 1)
	dist = (m_ * ptoepos->x - ptoepos->y + b_) / sqrt(m_*m_ + 1.0);
	if (dist < 0) dist = -dist;

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] dist: %10lf,  llen0: %10lf,  llen1: %10lf\n", camid, dist, llen0, llen1);

#define DIST_MIN	5.0
	if (dist < DIST_MIN) {
		double llen;
		llen = llen0;
		pheelpos->x = ptoepos->x + llen * sin(theta);
		pheelpos->y = ptoepos->y - llen * cos(theta);
	} else {
		I32 x1, y1;
		double phi;
		I32 count;
		I32 maxcount;
		double maxcountphiphi;

		//--
		if (llen0 > dist) {
			phi0 = acos(dist / llen0);
		} else {
			phi0 = 0;
		}
		if (llen1 > dist) {
			phi1 = acos(dist / llen1);
		} else {
			phi1 = 0;
		}

		/*
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] phi0: %10lf(%10lf), phi1: %10lf(%10lf)\n", 
				camid, phi0, RADIAN2DEGREE(phi0), phi1, RADIAN2DEGREE(phi1));
				*/

#define TOE_Y_OFFSET	3
		x1 = (I32) ptoepos->x;
		y1 = (I32) ptoepos->y+ TOE_Y_OFFSET;

		maxcount = 0;
		maxcountphiphi = 0;
		for (phi = phi0; phi < phi1; phi += DEGREE2RADIAN(5)) {
			I32 x2, y2;
			double m2_, b2_;
			//double dx, dy, 
			double dlen;
			double phiphi;
			I32 ilen;
			I32 x0, y0;
			I32 ii;
			//---
//			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "phi: %lf (%lf ~ %lf)\n", phi, phi0, phi1);

			phiphi = -phi + theta;

//#define SMALL_PHIPHI	(1e-5)
#define SMALL_PHIPHI	(1e-2)
			if (phiphi > M_PI/2.0 - SMALL_PHIPHI &&  phiphi < M_PI/2.0 + SMALL_PHIPHI) {
				continue;		// -_-;
			}
			
			//m2_ = tan(-phi + theta);
			m2_ = tan(phiphi);
			b2_ = (-x1 * m2_ + y1);

			x2 = (I32) (-(b2_-b_) / (m2_ - m_));
			y2 = (I32) (m2_ * x2 + b2_);

			dx = x1-x2; dy = y1-y2;
			dlen = sqrt(dx*dx + dy*dy);
			ilen = (I32) dlen;

			count = 0;
			for (ii = 0; ii < ilen; ii++) {
				//x0 = (I32)(x1 + ii * cos(-phi + theta));
				//y0 = (I32)(y1 + ii * sin(-phi + theta));
				x0 = (I32)(x1 + ii * cos(phiphi));
				y0 = (I32)(y1 + ii * sin(phiphi));
				if (*(pimgT + y0*width + x0)) {
					count++;
				}
			}
			if (maxcount < count) {
				maxcount = count;
				maxcountphiphi = phiphi;
			}
		}
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "phi max: %lf(%lf) width count %d\n", maxcountphiphi, RADIAN2DEGREE(maxcountphiphi), maxcount);

		{
			I32 x2, y2;
			double m2_, b2_;
			double phiphi;
			//
			//---

			phiphi = maxcountphiphi;
			m2_ = tan(phiphi);
			b2_ = (-x1 * m2_ + y1);

			x2 = (I32) (-(b2_-b_) / (m2_ - m_));
			y2 = (I32) (m2_ * x2 + b2_);

			pheelpos->x = x2;
			pheelpos->y = y2;

			if (x2 > 0 && x2 < (I32)width && y2 > 0 && y2 < (I32)height) {
#ifdef IPP_SUPPORT
				mylineGray(x1, y1, x2, y2, 0x80, pimgT, width);
#else
                cv::Mat img(cv::Size(width, height), CV_8UC1, pimgT);
                cv::line(img, cv::Point(x1, y1), cv::Point(x2, y2), 0x80);
#endif
			}
		}
		//mylineGray(x1, y1, x2, y2, 0x80, pimgT, width);

	}

	UNUSED(height);
	res = 1;

	return res;
}

static I32 GetClubPath_Position_Exile(iana_t *piana, U32 camid)	
{
	I32 res;
	I32 i;
	iana_cam_t	*pic;
	clubpath_context_t 	*pcpc;

	U32 count;
	double xx[CLUBDATACOUNT], yy[CLUBDATACOUNT];
	double tt[CLUBDATACOUNT];
	U32	   index[CLUBDATACOUNT];
	U32 docheck;
	//---

	pic 	= piana->pic[camid];
	pcpc 	= &pic->cpc;


	//--
#define H2COUNT_MINCOUNT	4


	do {
		U32 exilecode;
		U32 removed;

		count = 0;
		docheck = 0;
		for (i = 0; i < CLUBDATACOUNT; i++) {
			if (pcpc->h2state[i] == IANA_CLUB_EXIST) {
				xx[count] = pcpc->h2toepos[i].x;
				yy[count] = pcpc->h2toepos[i].y;
				tt[count] = pcpc->h2ts[i];
				index[count] = i;
				count++;
			}
		}

		pcpc->h2count = count;

		if (count < H2COUNT_MINCOUNT) {
			break;
		}

		removed =  IANA_CLUB_RemoveOutlier(tt, xx, count, &exilecode);
		if (removed) {
			docheck = 1;
			count--;
			pcpc->h2state[index[exilecode]] = IANA_CLUB_EXILE;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "XX, exile %d (%d)\n", index[exilecode], exilecode);

			if (count < H2COUNT_MINCOUNT) {
				//res = 0;
				break;
			} else {
				continue;
			}
		}

		removed =  IANA_CLUB_RemoveOutlier(tt, yy, count, &exilecode);
		if (removed) {
			docheck = 1;
			count--;
			pcpc->h2state[index[exilecode]] = IANA_CLUB_EXILE;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "YY, exile %d (%d)\n", index[exilecode], exilecode);

			if (count < H2COUNT_MINCOUNT) {
				break;
			} else {
				continue;
			}
		}
	} while (docheck);

	if (count < H2COUNT_MINCOUNT) {
		pcpc->h2valid = 0;
		res = 0;
	} else {
		pcpc->h2valid = 1;
		res = 1;
	}


	return res;
}

static I32 GetClubPath_Position_Regression(iana_t *piana, U32 camid)			
{
	I32 res;
	U32 i, j;

	iana_cam_t	*pic;
	clubpath_context_t 	*pcpc;

	U32 count;
#define CLUBDATAMULT	4
//#define CLUBDATAMULT	2
//#define CLUBDATAMULT	1
//#define CLUBDATAMULT	10
	double toexx[CLUBDATACOUNT*CLUBDATAMULT], toeyy[CLUBDATACOUNT*CLUBDATAMULT];
	double toeshaftxx[CLUBDATACOUNT*CLUBDATAMULT], toeshaftyy[CLUBDATACOUNT*CLUBDATAMULT];
	double heelxx[CLUBDATACOUNT*CLUBDATAMULT], heelyy[CLUBDATACOUNT*CLUBDATAMULT];
	double xx[CLUBDATACOUNT*CLUBDATAMULT], yy[CLUBDATACOUNT*CLUBDATAMULT];
	double tt[CLUBDATACOUNT*CLUBDATAMULT];

	double ma[3];
	double r2;
	double x0, y0, t0;
	double xxm[CLUBDATACOUNT*CLUBDATAMULT];
	double yym[CLUBDATACOUNT*CLUBDATAMULT];

#define H2COUNT_QUAD_MIN	4
#define H2COUNT_LINEAR_MIN	3
	//--
	pic 	= piana->pic[camid];
	pcpc 	= &pic->cpc;

	if (pcpc->h2valid == 0) {
		res = 0;
		goto func_exit;
	}

	count = 0;
	x0 = y0 = t0 = 0;
	for (i = 0; i < CLUBDATACOUNT; i++) {
		if (pcpc->h2state[i] == IANA_CLUB_EXIST) {
			double x1, y1, t1;
			//double dx, dy, dt, dlen;
			//double clubspeed, angle;
			I32 multcount;

			//--
			if (pcpc->h2ts[i] < 0) {
				multcount = CLUBDATAMULT; 
			} else {
				multcount = 1;
			}
			for (j = 0; j < (U32)multcount; j++) {
				heelxx[count] = pcpc->h2heelpos[i].x;
				heelyy[count] = pcpc->h2heelpos[i].y;

				toexx[count] = pcpc->h2toepos[i].x;
				toeyy[count] = pcpc->h2toepos[i].y;

				toeshaftxx[count] = pcpc->h2toeshaftpos[i].x;
				toeshaftyy[count] = pcpc->h2toeshaftpos[i].y;

				//#define TOEHEEL_PVALUE	0.7
				//#define TOEHEEL_PVALUE	0.9
//#define TOEHEEL_PVALUE	1.1
#define TOEHEEL_PVALUE	1
				x1 = xx[count] = pcpc->h2toepos[i].x * TOEHEEL_PVALUE + pcpc->h2heelpos[i].x * (1-TOEHEEL_PVALUE);
				y1 = yy[count] = pcpc->h2toepos[i].y * TOEHEEL_PVALUE + pcpc->h2heelpos[i].y * (1-TOEHEEL_PVALUE);


				// toeshaft.. 
#if 1
				x1 = xx[count] = pcpc->h2toeshaftpos[i].x;
				y1 = yy[count] = pcpc->h2toeshaftpos[i].y;
#endif

				t1 = tt[count] = pcpc->h2ts[i];

				if (count == 0) {
					x0 = x1;
					y0 = y1;
					t0 = t1;
				}

//#define POSTSHOT_TIME	(0.0)
//#define POSTSHOT_TIME	(0.01)
//#define POSTSHOT_TIME	(0.005)
#define POSTSHOT_TIME	(0.002)
				if (pcpc->h2ts[i] < POSTSHOT_TIME) 
				{
					x0 = x1;
					y0 = y1;
					t0 = t1;
					count++;
				} else {
					break;
				}
			}

			if (pcpc->h2ts[i] > POSTSHOT_TIME) {
				break;
			}
		}
	}

#define DISCARD_COUNT	2
	memset(&pcpc->h2clubspeedma[0], 0, sizeof(double) * 3);

	if (count < H2COUNT_LINEAR_MIN) {
		pcpc->h2valid = 0;
		res = 0;
		goto func_exit;
	} else if (count < H2COUNT_QUAD_MIN) {
		//
		ma[2] = 0;
		res = cr_regression(tt, xx, count, &ma[1], &ma[0]);
		memcpy(&pcpc->h2xma[0], &ma[0], sizeof(double) * 3);

		ma[2] = 0;
		res = cr_regression(tt, yy, count, &ma[1], &ma[0]);
		memcpy(&pcpc->h2yma[0], &ma[0], sizeof(double) * 3);

		// toe
		ma[2] = 0;
		res = cr_regression(tt, toexx, count, &ma[1], &ma[0]);
		memcpy(&pcpc->h2toexma[0], &ma[0], sizeof(double) * 3);

		ma[2] = 0;
		res = cr_regression(tt, toeyy, count, &ma[1], &ma[0]);
		memcpy(&pcpc->h2toeyma[0], &ma[0], sizeof(double) * 3);

		// toeshaft
		ma[2] = 0;
		res = cr_regression(tt, toeshaftxx, count, &ma[1], &ma[0]);
		memcpy(&pcpc->h2toeshaftxma[0], &ma[0], sizeof(double) * 3);

		ma[2] = 0;
		res = cr_regression(tt, toeshaftyy, count, &ma[1], &ma[0]);
		memcpy(&pcpc->h2toeshaftyma[0], &ma[0], sizeof(double) * 3);
		// heel
		ma[2] = 0;
		res = cr_regression(tt, heelxx, count, &ma[1], &ma[0]);
		memcpy(&pcpc->h2heelxma[0], &ma[0], sizeof(double) * 3);

		ma[2] = 0;
		res = cr_regression(tt, heelyy, count, &ma[1], &ma[0]);
		memcpy(&pcpc->h2heelyma[0], &ma[0], sizeof(double) * 3);

		pcpc->h2valid = 1;
		res = 1;
	} else 
	{
		U32 calcindexoffset;
		U32 calcindexcount;

		//--
		// count = 12, CALCCOUNTMAX = 10
		// calcindexoffset = count - CALCCOUNTMAX = 2
		// 2, 3, 4, ..., 11  (calccount = 8)
//#define CALCCOUNTMAX 	10
#define CALCCOUNTMAX 	8
//#define CALCCOUNTMAX 	6

#undef  CALCCOUNTMAX
#define CALCCOUNTMAX 	999
		if (count > CALCCOUNTMAX) {
			calcindexoffset = count - CALCCOUNTMAX;
			calcindexcount = CALCCOUNTMAX;
		} else {
			calcindexoffset = 0;
			calcindexcount = count;
		}
		//

		caw_quadraticregression_a2_constraints(tt+calcindexoffset, xx+calcindexoffset, calcindexcount, //rcount-2, 
				g_alpha,
				&ma[2],
				&ma[1],
				&ma[0],
				&r2);

		memcpy(&pcpc->h2xma[0], &ma[0], sizeof(double) * 3);

		// toe
		caw_quadraticregression_a2_constraints(tt+calcindexoffset, toexx+calcindexoffset, calcindexcount, 
				g_alpha,
				&ma[2],
				&ma[1],
				&ma[0],
				&r2);
		memcpy(&pcpc->h2toexma[0], &ma[0], sizeof(double) * 3);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "TOE x.  %lf %lf %lf  r2: %lf\n", ma[2], ma[1], ma[0], r2);

		caw_quadraticregression_a2_constraints(tt+calcindexoffset, toeshaftxx+calcindexoffset, calcindexcount, 
				g_alpha,
				&ma[2],
				&ma[1],
				&ma[0],
				&r2);
		memcpy(&pcpc->h2toeshaftxma[0], &ma[0], sizeof(double) * 3);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "toeshaft x.  %lf %lf %lf  r2: %lf\n", ma[2], ma[1], ma[0], r2);
		// heel
		caw_quadraticregression_a2_constraints(tt+calcindexoffset, heelxx+calcindexoffset, calcindexcount, //rcount-2, 
				g_alpha,
				&ma[2],
				&ma[1],
				&ma[0],
				&r2);
		memcpy(&pcpc->h2heelxma[0], &ma[0], sizeof(double) * 3);



		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "__ xx vs tt   %10lf %10lf %10lf\n", pcpc->h2xma[2], pcpc->h2xma[1], pcpc->h2xma[0]); 


//#define TOOBIGMA0	100
//#define TOOBIGMA	700

#define TOOBIGMA0	500
#define TOOBIGMA	1000

		if (pcpc->h2xma[2] > TOOBIGMA0
				|| pcpc->h2xma[2] < - TOOBIGMA
				) {
			U32 calccount;

			//-
//#define CUTOFFCOUNT 3
//#define CUTOFFCOUNT 2
#define CUTOFFCOUNT 1
			if (count > H2COUNT_LINEAR_MIN + CUTOFFCOUNT) {
				calccount = count-CUTOFFCOUNT;
			} else {
				calccount = count;
			}
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "XXX CUTOFF %d -> %d\n", count, calccount);

			//
			ma[2] = 0;
			res = cr_regression(tt, xx, calccount, &ma[1], &ma[0]);
			memcpy(&pcpc->h2xma[0], &ma[0], sizeof(double) * 3);

			// toe
			ma[2] = 0;
			res = cr_regression(tt, toexx, calccount, &ma[1], &ma[0]);
			memcpy(&pcpc->h2toexma[0], &ma[0], sizeof(double) * 3);

			// toeshaft
			ma[2] = 0;
			res = cr_regression(tt, toeshaftxx, calccount, &ma[1], &ma[0]);
			memcpy(&pcpc->h2toeshaftxma[0], &ma[0], sizeof(double) * 3);

			// heel
			ma[2] = 0;
			res = cr_regression(tt, heelxx, calccount, &ma[1], &ma[0]);
			memcpy(&pcpc->h2heelxma[0], &ma[0], sizeof(double) * 3);
		}

		caw_quadraticregression_a2_constraints(tt+calcindexoffset, yy+calcindexoffset, calcindexcount,
				g_alpha,
				&ma[2],
				&ma[1],
				&ma[0],
				&r2);
		memcpy(&pcpc->h2yma[0], &ma[0], sizeof(double) * 3);

		// toe
		caw_quadraticregression_a2_constraints(tt+calcindexoffset, toeyy+calcindexoffset, calcindexcount,
				g_alpha,
				&ma[2],
				&ma[1],
				&ma[0],
				&r2);
		memcpy(&pcpc->h2toeyma[0], &ma[0], sizeof(double) * 3);

		// toeshaft
		caw_quadraticregression_a2_constraints(tt+calcindexoffset, toeshaftyy+calcindexoffset, calcindexcount,
				g_alpha,
				&ma[2],
				&ma[1],
				&ma[0],
				&r2);
		memcpy(&pcpc->h2toeshaftyma[0], &ma[0], sizeof(double) * 3);





		//
		caw_quadraticregression_a2_constraints(tt+calcindexoffset, heelyy+calcindexoffset, calcindexcount,
				g_alpha,
				&ma[2],
				&ma[1],
				&ma[0],
				&r2);
		memcpy(&pcpc->h2heelyma[0], &ma[0], sizeof(double) * 3);


		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "__ yy vs tt   %10lf %10lf %10lf\n", pcpc->h2yma[2], pcpc->h2yma[1], pcpc->h2yma[0]); 
		if (pcpc->h2yma[2] < - TOOBIGMA
				) {
			U32 calccount;

			//-
//#define CUTOFFCOUNT 3
#define CUTOFFCOUNT 1
			if (count > H2COUNT_LINEAR_MIN + CUTOFFCOUNT) {
				calccount = count-CUTOFFCOUNT;
			} else {
				calccount = count;
			}
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "YYY CUTOFF %d -> %d\n", count, calccount);

			//
			ma[2] = 0;
			res = cr_regression(tt, yy, calccount, &ma[1], &ma[0]);
			memcpy(&pcpc->h2yma[0], &ma[0], sizeof(double) * 3);

			// toe
			ma[2] = 0;
			res = cr_regression(tt, toeyy, calccount, &ma[1], &ma[0]);
			memcpy(&pcpc->h2toeyma[0], &ma[0], sizeof(double) * 3);

			// toeshaft
			ma[2] = 0;
			res = cr_regression(tt, toeshaftyy, calccount, &ma[1], &ma[0]);
			memcpy(&pcpc->h2toeshaftyma[0], &ma[0], sizeof(double) * 3);

			// heel
			ma[2] = 0;
			res = cr_regression(tt, heelyy, calccount, &ma[1], &ma[0]);
			memcpy(&pcpc->h2heelyma[0], &ma[0], sizeof(double) * 3);
		}
		
		
		{
			double hx, hy;
			double tx, ty;
			double dx, dy, dlen;

			hx = pcpc->h2heelxma[0];
			hy = pcpc->h2heelyma[0];

			tx = pcpc->h2toexma[0];
			ty = pcpc->h2toeyma[0];


			dx = hx-tx;
			dy = hy-ty;
			dlen = sqrt(dx*dx + dy*dy);
			pcpc->h2facelen = dlen;
		}
		pcpc->h2valid = 1;
		res = 1;

		UNUSED(xxm);UNUSED(yym);
	}

	if (res == 1) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "xx vs tt   %10lf %10lf %10lf\n", pcpc->h2xma[2], pcpc->h2xma[1], pcpc->h2xma[0]); 
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "yy vs tt   %10lf %10lf %10lf\n", pcpc->h2yma[2], pcpc->h2yma[1], pcpc->h2yma[0]); 
	}

func_exit:
	return res;
}

#if 0
static I32 iana_clubpath_cam_3d_heelposition(iana_t *piana)
{
	I32 res;
	iana_cam_t	*pic0, *pic1;
	clubpath_context_t 	*pcpc0, *pcpc1;
	I32 i, j;
	I32 i2j;
	I32 foundit;

	//--
	pic0 	= piana->pic[0];
	pic1 	= piana->pic[1];

	pcpc0 	= &pic0->cpc;
	pcpc1 	= &pic1->cpc;

	for (i = 0; i < CLUBDATACOUNT; i++) {
		pcpc0->h2heel3dposvalid[i] = 0;		// NOT VALID.
		pcpc1->h2heel3dposvalid[i] = 0;		// NOT VALID.
		memset(&pcpc0->h2heel3dpos[i], 0, sizeof(cr_point_t));
		memset(&pcpc1->h2heel3dpos[i], 0, sizeof(cr_point_t));
	}

	i2j = -999999;
	foundit = 0;
	for (i = 0; i < CLUBDATACOUNT; i++) {
		if ((pcpc0->h2state[i] == IANA_CLUB_EXIST || pcpc0->h2state[i] == IANA_CLUB_EXILE)) {
			double ts0, ts1;
			ts0 = pcpc0->h2ts[i];
			for (j = 0; j < CLUBDATACOUNT; j++) {
				if ((pcpc1->h2state[j] == IANA_CLUB_EXIST || pcpc1->h2state[j] == IANA_CLUB_EXILE)) {
					ts1 = pcpc1->h2ts[j];
#define TSDIFF_MIN	(0.0001)
					if (ts0 > ts1 - TSDIFF_MIN && ts0 < ts1 + TSDIFF_MIN) {
						i2j = j - i;
						foundit = 1;
						break;
					}
				}
			}
			if (foundit) {
				break;
			}
		}
	}

	if (foundit == 0) {
		res = 0;
		goto func_exit;
	}

	for (i = 0; i < CLUBDATACOUNT; i++) {
		j = i + i2j;
		if (j < 0 || j > CLUBDATACOUNT-1) {
			continue;
		}
		if (
			(pcpc0->h2state[i] == IANA_CLUB_EXIST || pcpc0->h2state[i] == IANA_CLUB_EXILE)
			&& 	(pcpc1->h2state[j] == IANA_CLUB_EXIST || pcpc1->h2state[j] == IANA_CLUB_EXILE)
		   ) {
			cr_line_t *pshaftline0, *pshaftline1;
			cr_plane_t plane0, plane1;
			cr_point_t *pcampos0, *pcampos1;
			cr_line_t camtoheel3d;
			cr_line_t shaftline3d;
			cr_point_t heelpoint3d0, heelpoint3d1;
			double dist;

			//--
			pshaftline0 = &pcpc0->h2shaftline[i]; 			// shaft plane0
			pcampos0 = &pic0->icp.CamPosL;
			cr_plane_p0line(pcampos0, pshaftline0, &plane0);

			pshaftline1 = &pcpc1->h2shaftline[j]; 			// shaft plane1
			pcampos1 = &pic1->icp.CamPosL;
			cr_plane_p0line(pcampos1, pshaftline1, &plane1);

			cr_line_plane_plane(&plane0, &plane1, &shaftline3d); // make shaft 3d line
//			cr_line_plane_plane(&plane0, &plane1, &pcpc0->shaftline3d); // make shaft 3d line
			memcpy(&pcpc0->h2shaft3dline[i], &shaftline3d, sizeof(cr_line_t));
			memcpy(&pcpc1->h2shaft3dline[j], &shaftline3d, sizeof(cr_line_t));

			heelpoint3d0.x = pcpc0->h2heelpos[i].x;				// get 3d heelposition with CAM0
			heelpoint3d0.y = pcpc0->h2heelpos[i].y;
			heelpoint3d0.z = 0;

			cr_line_p0p1(pcampos0, &heelpoint3d0, &camtoheel3d);
			cr_point_line_line(&camtoheel3d, &shaftline3d, &heelpoint3d0, &heelpoint3d1);

			dist = cr_vector_distance(&heelpoint3d0, &heelpoint3d1);
//			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "dist: %lf\n", dist);
#define HEEL_DIST_MIN	0.001
//#define HEEL_DIST_MIN	0.005
			if (dist < HEEL_DIST_MIN) {
				pcpc0->h2heel3dposvalid[i] = 1;
				pcpc1->h2heel3dposvalid[j] = 1;

				memcpy(&pcpc0->h2heel3dpos[i], &heelpoint3d0, sizeof(cr_point_t));
				memcpy(&pcpc1->h2heel3dpos[j], &heelpoint3d0, sizeof(cr_point_t));
			}

		}

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d:%2d] <%10.5lf %10.5lf> (%d %d) %10.5lf %10.5lf %10.5lf\n", i, j,
			pcpc0->h2ts[i]*1000.0, pcpc1->h2ts[j]*1000.0, 
			(pcpc0->h2state[i] == IANA_CLUB_EXIST),
			(pcpc1->h2state[j] == IANA_CLUB_EXIST),
			pcpc0->h2heel3dpos[i].x,
			pcpc0->h2heel3dpos[i].y,
			pcpc0->h2heel3dpos[i].z);

	}
	res = 1;

func_exit:
	return res;
}
#endif
/**
*   @ingroup IANA_CLUB
*/
I32 IANA_CLUB_RemoveOutlier(double xx[], double yy[], U32 count, U32 *pexilecode)		
{	// remove outlier... XX
	I32 res;

	U32 i, j, k;

	double m_, b_;
	double x0, y0, y0_;
	double dx, dy;
	double m_0, b_0;
	double max_e;
	U32  max_e_code;
	U32 max_count;

	//--
	max_count = 0;
	m_0 = 0;
	b_0 = 0;
	for (i = 0; i < count; i++) {			// calc optimum m_, b_
		U32 goodcount;
		double e_y0;

		//--
		j = (i + 2) % count;
		dx = xx[i] - xx[j];
		dy = yy[i] - yy[j];
#define SMALL_DX	(1e-10)
		if (dx > -SMALL_DX && dx < SMALL_DX) {
			continue;
		}
		
		m_ = dy / dx;
		b_ = - m_ * xx[i] + yy[i];

		goodcount = 0;
		for (k = 0; k < count; k++) {
			x0 = xx[k];
			y0 = yy[k];
			y0_ = m_ * x0 + b_;

			e_y0 = y0 - y0_;
//#define OUTLIER_RANGE	0.01	
#define OUTLIER_RANGE	0.04
			if (e_y0 > -OUTLIER_RANGE  && e_y0 < OUTLIER_RANGE) {
				goodcount++;
			}
		}

		if (max_count < goodcount) {
			max_count = goodcount;
			m_0 = m_;
			b_0 = b_;
		}
	}

	max_e = 0;
	max_e_code = 0;
	for (i = 0; i < count; i++) {		// select distance..
		double e_y0;

		//--
		x0 = xx[i];
		y0 = yy[i];
		y0_ = m_0 * x0 + b_0;

		e_y0 = y0 - y0_;

		if (e_y0 < 0) e_y0 = -e_y0;
		if (e_y0 > max_e) {
			max_e = e_y0;
			max_e_code = i;
		}
	}
//#define MAX_E	0.01
#define MAX_E	0.04
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "____, %lf:%d\n", max_e, max_e_code);
	if (max_e > MAX_E) {
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "EXILE, ___, %lf:%d\n", max_e, max_e_code);
		*pexilecode = max_e_code;
		res = 1;
	} else {
		res = 0;
	}

	return res;
}


I32 IANA_CLUB_ModifyShot4P3V2(		
		double *pvmag,
		double *pincline,
		U32 isIron)
{
	I32 res;
	double vmag;
	double incline;
/********
1) For Iron 
	
Incline
   20 -> 20
   25 -> 27
   30 -> 33
   40 -> 40

Vmag
	15 -> 15
	20 -> 22
	30 -> 31
	40 -> 41
	50 -> 51
	60 -> 61
	70 -> 70
2) Slice spin
   20% increase.. :)
***************/
	
	//--
	vmag 	= *pvmag;
	incline = *pincline;

	if (isIron) {
		//-
		if (incline < 20.0) {

		} else if (incline < 25.0) {
			incline = LINEAR_SCALE(incline, 20.0, 20.0, 25.0, 27.0);
		} else if (incline < 30.0) {
			incline = LINEAR_SCALE(incline, 25.0, 27.0, 30.0, 33.0);
		} else if (incline < 40.0) {
			incline = LINEAR_SCALE(incline, 30.0, 33.0, 40.0, 40.0);
		} else {

		}

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "incline: %lf -> %lf\n", *pincline,  incline);
		*pincline = incline;

		//-
		if (vmag < 15.0) {

		} else if (vmag < 20.0) {
			vmag = LINEAR_SCALE(vmag, 15.0, 15.0, 20.0, 22.0);
		} else if (vmag < 30.0) {
			vmag = LINEAR_SCALE(vmag, 20.0, 22.0, 30.0, 31.0);
		} else if (vmag < 40.0) {
			vmag = LINEAR_SCALE(vmag, 30.0, 31.0, 40.0, 41.0);
		} else if (vmag < 60.0) {
			vmag = LINEAR_SCALE(vmag, 40.0, 41.0, 60.0, 61.0);
		} else if (vmag < 70.0) {
			vmag = LINEAR_SCALE(vmag, 60.0, 61.0, 70.0, 71.0);
		}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "vmag: %lf -> %lf\n", *pvmag,  vmag);
		*pvmag = vmag;
	}
	res = 1;
	return res;
}


static I32 DisplayClubPath(iana_t *piana, U32 camid, U32 normalbulk)
{
#ifdef IPP_SUPPORT
    I32 res;
    iana_cam_t	*pic;
    camimageinfo_t     *pcii;
    camif_buffer_info_t *pb;
    clubpath_context_t 	*pcpc;

    U32 multitude;
    U32	width;
    U32	height;
    U32	offset_x;
    U32	offset_y;
    double		xfact, yfact;

    U32 skip, syncdiv;
    U64 period;
    U64 periodm;


    I32 j;
    U08 *procimg;
    //I32 index;

    U08	*buf;
    U32	rindex;
    U32 m;

    U64 ts64;
    U64 ts64Mt;

    U32 rcount;
    camif_buffergroup_t *pbg;
    U32 buffercount;

    //----
    pic 	= piana->pic[camid];
    pcpc 	= &pic->cpc;

    res = 0;
    buf = NULL;
    pb = NULL;

    rcount = 0;
    pbg = NULL;
    scamif_imagebuf_groupinfo2(piana->hscamif, camid, normalbulk, &pb, &pbg);
    buffercount = pbg->count;				// buffer slot count


    for (rindex = pcpc->startindex; rindex <= (U32)pcpc->endindex; rindex++) {
        // 1) get image buffer
        if (piana->opmode == IANA_OPMODE_FILE) {
            res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
            if (res == 0) {
                break;
            }
        } else {
            //		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bulk, img read\n", i);
            for (j = 0; j < 40; j++) {
                res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
                if (res == 1) {
                    break;
                }
                cr_sleep(2);
            }
            //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read [%d:%d]\n", i, j);
            if (res == 0) {
                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", rindex, j);
                break;
                //continue;
            }
        }
        if (res == 0) {
            continue;
        } else {
            {	// Image parameters for each frame
                pcii 		= &pb->cii;
                multitude   = pcii->multitude;
                if (multitude < 1) {
                    multitude = 1;
                }
                width 		= pcii->width;
                height 		= pcii->height;

                syncdiv = pcii->syncdiv;	if (syncdiv == 0) { syncdiv = 1; }
                skip = pcii->skip;			if (skip == 0) { skip = 1; }
                period = TSSCALE / (U64)pcii->framerate;			// nsec
                periodm = period * skip * syncdiv;

                offset_x 	= pcii->offset_x;
                offset_y 	= pcii->offset_y;
                xfact 		= 1.0;
                yfact 		= 1.0 * multitude;

                ts64 = MAKEU64(pb->ts_h, pb->ts_l);
            }
        }
#if 1
        for (m = 0; m < multitude; m++) {
            I64 ts64diff;
            double ts64diffd;
            double tsdiffdiffd;
            I32 rcount_select;

            //IppiSize roisize;
            ts64Mt = ts64 - (periodm * (multitude - m -1));

            if (multitude == 1) {
                procimg = buf;
            } else {
                IppiSize 	ssize;
                IppiRect 	sroi, droi;
                int sstep, dstep;
                IppStatus 	status;
                Ipp8u 		*resizebuf;
                int 		bufsize;


                //---
                ssize.width 	= width;
                ssize.height 	= height;

                sroi.x 		= 0;
                sroi.y 		= 0;
                sroi.width 	= width;
                sroi.height	= height/multitude;

                droi.x 		= 0;
                droi.y 		= 0;
                droi.width 	= (int)(sroi.width * xfact + 0.1);
                droi.height	= (int)(sroi.height * yfact + 0.1);

                sstep 		= sroi.width;
                dstep		= droi.width;

                ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_CUBIC, &bufsize);
                resizebuf = (U08 *)malloc(bufsize);

                procimg = pic->pimgFull;
                //memcpy(procimg, (Ipp8u *)buf + pcii->width * ((m*pcii->height) / multitude), width * (height / multitude));
                status = ippiResizeSqrPixel_8u_C1R(
                    (Ipp8u *)buf + pcii->width * ((m*pcii->height) / multitude),
                    ssize,
                    sstep,
                    sroi,
                    //
                    (Ipp8u*)procimg,
                    dstep,
                    droi,
                    //
                    xfact,
                    yfact,
                    0,		// xshift,
                    0,		// yshift,
                    IPPI_INTER_CUBIC,
                    resizebuf
                );
                free(resizebuf);
            }


            ts64diff = ((I64)ts64Mt - (I64)pic->ts64shot);
            ts64diffd = ts64diff/TSSCALE_D;

            rcount_select = -999;
            for (rcount = 0; rcount < CLUBDATACOUNT; rcount++) {
                if (pcpc->h2state[rcount] == IANA_CLUB_EXIST) {
                    tsdiffdiffd = pcpc->h2ts[rcount] - ts64diffd;
                    if (fabs(tsdiffdiffd) < (0.001 * 0.3)) {
                        rcount_select = rcount;
                        break;
                    }
                }
            }

            //	if (rcount_select >= 0) 
            {
                point_t Pptoe, Lptoe;
                point_t Pptoeshaft, Lptoeshaft;
                point_t Ppheel, Lpheel;
                point_t Ppshaft, Lpshaft;
                I32 x1, y1;
                I32 x2, y2;
                double td;

                //----
                td = ts64diffd;

                //-- Toe
                Lptoe.x = pcpc->h2toepos[rcount].x;
                Lptoe.y = pcpc->h2toepos[rcount].y;


                iana_L2P(piana, camid, &Lptoe, &Pptoe, 0);

                if (rcount_select >= 0) {
                    x1 = (I32)(Pptoe.x - offset_x);
                    y1 = (I32)(Pptoe.y - offset_y);

                    mycircleGray(x1, y1, 5, 0xFF, procimg, width);
                }

                Lptoe.x = pcpc->h2toexma[2] * td*td + pcpc->h2toexma[1] * td + pcpc->h2toexma[0];
                Lptoe.y = pcpc->h2toeyma[2] * td*td + pcpc->h2toeyma[1] * td + pcpc->h2toeyma[0];
                iana_L2P(piana, camid, &Lptoe, &Pptoe, 0);

                x1 = (I32)(Pptoe.x - offset_x);
                y1 = (I32)(Pptoe.y - offset_y);
#define RECT_WIDTH	4
#define RECT_WIDTH_H	(RECT_WIDTH/2)

                x2 = x1 + RECT_WIDTH_H; y2 = y1 + RECT_WIDTH_H;
                x1 = x1 - RECT_WIDTH_H; y1 = y1 - RECT_WIDTH_H;

                myrectGray(x1, y1, x2, y2, 0xFF, procimg, width);

                //-- Toeshaft
                Lptoeshaft.x = pcpc->h2toeshaftpos[rcount].x;
                Lptoeshaft.y = pcpc->h2toeshaftpos[rcount].y;


                iana_L2P(piana, camid, &Lptoeshaft, &Pptoeshaft, 0);

                if (rcount_select >= 0) {
                    x1 = (I32)(Pptoeshaft.x - offset_x);
                    y1 = (I32)(Pptoeshaft.y - offset_y);

                    mycircleGray(x1, y1, 5, 0x80, procimg, width);
                }

                Lptoeshaft.x = pcpc->h2toeshaftxma[2] * td*td + pcpc->h2toeshaftxma[1] * td + pcpc->h2toeshaftxma[0];
                Lptoeshaft.y = pcpc->h2toeshaftyma[2] * td*td + pcpc->h2toeshaftyma[1] * td + pcpc->h2toeshaftyma[0];
                iana_L2P(piana, camid, &Lptoeshaft, &Pptoeshaft, 0);

                x1 = (I32)(Pptoeshaft.x - offset_x);
                y1 = (I32)(Pptoeshaft.y - offset_y);
#define RECT_WIDTH	4
#define RECT_WIDTH_H	(RECT_WIDTH/2)

                x2 = x1 + RECT_WIDTH_H; y2 = y1 + RECT_WIDTH_H;
                x1 = x1 - RECT_WIDTH_H; y1 = y1 - RECT_WIDTH_H;

                myrectGray(x1, y1, x2, y2, 0x80, procimg, width);



                //-- Heel
                Lpheel.x = pcpc->h2heelpos[rcount].x;
                Lpheel.y = pcpc->h2heelpos[rcount].y;


                iana_L2P(piana, camid, &Lpheel, &Ppheel, 0);
                iana_L2P(piana, camid, &Lpheel, &Ppheel, 0);
                iana_L2P(piana, camid, &Lpshaft, &Ppshaft, 0);


                if (rcount_select >= 0) {
                    x1 = (I32)(Ppheel.x - offset_x);
                    y1 = (I32)(Ppheel.y - offset_y);

                    mycircleGray(x1, y1, 5, 0x80, procimg, width);
                }

                Lpheel.x = pcpc->h2heelxma[2] * td*td + pcpc->h2heelxma[1] * td + pcpc->h2heelxma[0];
                Lpheel.y = pcpc->h2heelyma[2] * td*td + pcpc->h2heelyma[1] * td + pcpc->h2heelyma[0];
                iana_L2P(piana, camid, &Lpheel, &Ppheel, 0);

                x1 = (I32)(Ppheel.x - offset_x);
                y1 = (I32)(Ppheel.y - offset_y);

                x2 = x1 + RECT_WIDTH_H; y2 = y1 + RECT_WIDTH_H;
                x1 = x1 - RECT_WIDTH_H; y1 = y1 - RECT_WIDTH_H;

                myrectGray(x1, y1, x2, y2, 0x80, procimg, width);

                //-- Shaft
                Lpshaft.x = pcpc->h2shaftpos[rcount].x;
                Lpshaft.y = pcpc->h2shaftpos[rcount].y;

                iana_L2P(piana, camid, &Lpshaft, &Ppshaft, 0);


                if (rcount_select >= 0) {
                    x1 = (I32)(Ppshaft.x - offset_x);
                    y1 = (I32)(Ppshaft.y - offset_y);

                    mycircleGray(x1, y1, 3, 0x80, procimg, width);
                }

                if (piana->himgfunc) {
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(procimg), width, height, 0, 1, 2, 3, piana->imguserparam);
                }

                y1 = y1 - 5;// fake.
            }
        } 	// for (m = 0; m < multitude; m++) 
#endif
    } 	// for (rindex = pcpc->startindex; rindex <= (U32)pcpc->endindex; rindex++) 

    return res;
#else
	I32 res;
	iana_cam_t	*pic;
	camimageinfo_t     *pcii;
	camif_buffer_info_t *pb;
	clubpath_context_t 	*pcpc;

	U32 multitude;
	U32	width;
	U32	height;
	U32	offset_x;
	U32	offset_y;
	
	U32 skip, syncdiv;
	U64 period;
	U64 periodm;

	I32 j;

    cv::Mat procImg;
	U08 *procimg;

	U08	*buf;
	U32	rindex;
	U32 m;

	U64 ts64;
	U64 ts64Mt;

	U32 rcount;
	camif_buffergroup_t *pbg;
	
	//----
	pic 	= piana->pic[camid];
	pcpc 	= &pic->cpc;

	res = 0;
	buf = NULL;
	pb = NULL;

	rcount = 0;
	pbg = NULL;
	scamif_imagebuf_groupinfo2(piana->hscamif, camid, normalbulk, &pb, &pbg);

	for (rindex = pcpc->startindex; rindex <= (U32)pcpc->endindex; rindex++) {
		// 1) get image buffer
		if (piana->opmode == IANA_OPMODE_FILE) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
			if (res == 0) {
				break;
			}
		} else {
			//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bulk, img read\n", i);
			for (j = 0; j < 40; j++) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, normalbulk, rindex, &pb, &buf);
				if (res == 1) {
					break;
				}
				cr_sleep(2);
			}
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read [%d:%d]\n", i, j);
			if (res == 0) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", rindex, j);
				break;
				//continue;
			}
		}
		if (res == 0) {
			continue;
		} else {
            // Image parameters for each frame
			pcii 		= &pb->cii;

			multitude   = pcii->multitude;  if (multitude < 1) { multitude = 1; }
			syncdiv = pcii->syncdiv;	if (syncdiv == 0) {	syncdiv = 1;}
			skip = pcii->skip;			if (skip == 0) {skip = 1;}
			
            period = TSSCALE / (U64) pcii->framerate;			// nsec
			periodm = period * skip * syncdiv;

            width 		= pcii->width;
            height 		= pcii->height;
			offset_x 	= pcii->offset_x;
			offset_y 	= pcii->offset_y;

			ts64 = MAKEU64(pb->ts_h, pb->ts_l);
		}

		for (m = 0; m < multitude; m++) {
			I64 ts64diff;
			double ts64diffd;
			double tsdiffdiffd;
			I32 rcount_select;

			ts64Mt = ts64 - (periodm * (multitude - m -1));

			if (multitude == 1) {
                procImg = cv::Mat(cv::Size(width, height), CV_8UC1, buf);
			} else {
                cv::Mat imgBuf(cv::Size(width, height), CV_8UC1, buf);
                cv::Rect sliceROI(0, m*height/multitude, width, height/multitude);
                cv::resize(imgBuf(sliceROI), procImg, cv::Size(width, height), (double)1, (double)multitude, cv::INTER_CUBIC);
			}

            procimg = (U08*)procImg.data;

			ts64diff = ((I64)ts64Mt - (I64)pic->ts64shot);
			ts64diffd = ts64diff/TSSCALE_D;

			rcount_select = -999;
			for (rcount = 0; rcount < CLUBDATACOUNT; rcount++) {
				if (pcpc->h2state[rcount] == IANA_CLUB_EXIST) {
					tsdiffdiffd = pcpc->h2ts[rcount] - ts64diffd;
					if (fabs(tsdiffdiffd) < (0.001 * 0.3)) {
						rcount_select = rcount;
						break;
					}
				}
			}

		//	if (rcount_select >= 0) 
			{
				point_t Pptoe, Lptoe;
				point_t Pptoeshaft, Lptoeshaft;
				point_t Ppheel, Lpheel;
				point_t Ppshaft, Lpshaft;
				I32 x1, y1;
				I32 x2, y2;
				double td;

				//----
				td = ts64diffd;

				//-- Toe
				Lptoe.x = pcpc->h2toepos[rcount].x;
				Lptoe.y = pcpc->h2toepos[rcount].y;


				iana_L2P(piana, camid, &Lptoe, &Pptoe, 0);

				if (rcount_select >= 0) {
					x1 = (I32) (Pptoe.x - offset_x);
					y1 = (I32) (Pptoe.y - offset_y);

					mycircleGray(x1, y1, 5, 0xFF, procimg, width);
				}

				Lptoe.x = pcpc->h2toexma[2] * td*td + pcpc->h2toexma[1] * td + pcpc->h2toexma[0];
				Lptoe.y = pcpc->h2toeyma[2] * td*td + pcpc->h2toeyma[1] * td + pcpc->h2toeyma[0];
				iana_L2P(piana, camid, &Lptoe, &Pptoe, 0);

				x1 = (I32) (Pptoe.x - offset_x);
				y1 = (I32) (Pptoe.y - offset_y);
#define RECT_WIDTH	4
#define RECT_WIDTH_H	(RECT_WIDTH/2)

				x2 = x1 + RECT_WIDTH_H; y2 = y1 + RECT_WIDTH_H;
				x1 = x1 - RECT_WIDTH_H; y1 = y1 - RECT_WIDTH_H;

				myrectGray(x1, y1, x2, y2, 0xFF, procimg, width);

				//-- Toeshaft
				Lptoeshaft.x = pcpc->h2toeshaftpos[rcount].x;
				Lptoeshaft.y = pcpc->h2toeshaftpos[rcount].y;


				iana_L2P(piana, camid, &Lptoeshaft, &Pptoeshaft, 0);

				if (rcount_select >= 0) {
					x1 = (I32) (Pptoeshaft.x - offset_x);
					y1 = (I32) (Pptoeshaft.y - offset_y);

					mycircleGray(x1, y1, 5, 0x80, procimg, width);
				}

				Lptoeshaft.x = pcpc->h2toeshaftxma[2] * td*td + pcpc->h2toeshaftxma[1] * td + pcpc->h2toeshaftxma[0];
				Lptoeshaft.y = pcpc->h2toeshaftyma[2] * td*td + pcpc->h2toeshaftyma[1] * td + pcpc->h2toeshaftyma[0];
				iana_L2P(piana, camid, &Lptoeshaft, &Pptoeshaft, 0);

				x1 = (I32) (Pptoeshaft.x - offset_x);
				y1 = (I32) (Pptoeshaft.y - offset_y);
#define RECT_WIDTH	4
#define RECT_WIDTH_H	(RECT_WIDTH/2)

				x2 = x1 + RECT_WIDTH_H; y2 = y1 + RECT_WIDTH_H;
				x1 = x1 - RECT_WIDTH_H; y1 = y1 - RECT_WIDTH_H;

				myrectGray(x1, y1, x2, y2, 0x80, procimg, width);



				//-- Heel
				Lpheel.x = pcpc->h2heelpos[rcount].x;
				Lpheel.y = pcpc->h2heelpos[rcount].y;


				iana_L2P(piana, camid, &Lpheel, &Ppheel, 0);
				iana_L2P(piana, camid, &Lpheel, &Ppheel, 0);
				iana_L2P(piana, camid, &Lpshaft, &Ppshaft, 0);


				if (rcount_select >= 0) {
					x1 = (I32) (Ppheel.x - offset_x);
					y1 = (I32) (Ppheel.y - offset_y);

					mycircleGray(x1, y1, 5, 0x80, procimg, width);
				}

				Lpheel.x = pcpc->h2heelxma[2] * td*td + pcpc->h2heelxma[1] * td + pcpc->h2heelxma[0];
				Lpheel.y = pcpc->h2heelyma[2] * td*td + pcpc->h2heelyma[1] * td + pcpc->h2heelyma[0];
				iana_L2P(piana, camid, &Lpheel, &Ppheel, 0);

				x1 = (I32) (Ppheel.x - offset_x);
				y1 = (I32) (Ppheel.y - offset_y);

				x2 = x1 + RECT_WIDTH_H; y2 = y1 + RECT_WIDTH_H;
				x1 = x1 - RECT_WIDTH_H; y1 = y1 - RECT_WIDTH_H;

				myrectGray(x1, y1, x2, y2, 0x80, procimg, width);

				//-- Shaft
				Lpshaft.x = pcpc->h2shaftpos[rcount].x;
				Lpshaft.y = pcpc->h2shaftpos[rcount].y;

				iana_L2P(piana, camid, &Lpshaft, &Ppshaft, 0);


				if (rcount_select >= 0) {
					x1 = (I32) (Ppshaft.x - offset_x);
					y1 = (I32) (Ppshaft.y - offset_y);

					mycircleGray(x1, y1, 3, 0x80, procimg, width);
				}

				if (piana->himgfunc) {
					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)(procimg), width, height, 0, 1, 2, 3, piana->imguserparam);
				}

				y1 = y1 - 5;// fake.
			}
		} 	// for (m = 0; m < multitude; m++) 
	} 	// for (rindex = pcpc->startindex; rindex <= (U32)pcpc->endindex; rindex++) 

	return res;
#endif
}


I32 check_isIron(iana_t *piana, U32 camid)		
{
	I32 isIron;
	iana_cam_t	*pic;
	clubpath_context_t 	*pcpc;


	//--
	pic 	= piana->pic[camid];
	pcpc 	= &pic->cpc;
//#define FACELEN_IRON	(0.075)
#define FACELEN_IRON	(0.095)			// 20200813
	if (pcpc->h2facelen < FACELEN_IRON) {
		isIron = 1;
	} else {
#define BALLPOS_HEIGHT_TEE	0.03
		if (pic->icp.b3d.z < BALLPOS_HEIGHT_TEE) {
			isIron = 1;
		} else {
			isIron = 0;		// DRIVER...... 
		}
	}

	if (isIron) {
		pcpc->shotarea = IANA_AREA_IRON;
		s_proccessarea = IANA_AREA_IRON;
	} else {
		pcpc->shotarea = IANA_AREA_TEE;
		s_proccessarea = IANA_AREA_TEE;
	}

	return isIron;
}



#if defined (__cplusplus)
}
#endif



