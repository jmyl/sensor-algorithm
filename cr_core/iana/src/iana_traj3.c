/********************************************************************************
                                                                                
                   Creatz Golf sensor 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 by Creatz Inc.
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc. \n

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_traj.c
	 @brief  Golf Trajectory.
	 @author YongHo Suk                                 
	 @date   2011/10/05 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#include <string.h>

#include "cr_common.h"

#define _USE_MATH_DEFINES								// for using M_PI
#include <math.h>

#include "iana_adapt.h"

#include "cr_osapi.h"
#include "cr_interpolation.h"

#include "iana_traj.h"





/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#if defined( _MSC_VER ) && (_MSC_VER <= 1200)    // 1200 = MSVC 6.0.
#define __FUNCTION__
#endif

#define AIRPRESSURE_PSI_DEFAULT	(14.696)

#define AIRPRESSURE_PA_DEFAULT	(101325.0)

#define AIRPRESSURE_PSI2PA(x) ((x) * AIRPRESSURE_PA_DEFAULT / AIRPRESSURE_PSI_DEFAULT)
#define AIRPRESSURE_PA2PSI(x) ((x) * AIRPRESSURE_PSI_DEFAULT / AIRPRESSURE_PA_DEFAULT)

#define	METER2FEET(x)	((x)*3.2808399)
//#define	FEET2METER(x)	((x)/3.2808399)
#define	FEET2METER(x)	((x)*0.304799999536)
#define	MPS2MPH(x)		((x)*2.236936330795288)


#define TRAJ_SMALLVALUE	(1e-5)

#define LINEAR_SCALE(x,x0,y0,x1,y1) ( ( ((double) (((y1) - (y0)))) / ((double) ((x1) - (x0))) )*((x)-(x0)) + (y0) )


/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*

             ^               
             |Z               
             |              .
             |          .       .
             |       .            .
             |     .               .
             |   .                  .
             | .                     .
             |------------------------.----->		Y
            /                          .
           /
		  /
		 V  X

*/


/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/
#if defined (__cplusplus)
extern "C" {
#endif

typedef struct data4d_ {
	union {
		struct {
			double data[4];
		};
		struct {
			double x;
			double y;
			double z;
			double w;
		};
	};
} data4d_t;

typedef struct velpos8d_ {
	union {
		struct {
			double data[8];
		};
		struct {
			//
			double Vx;
			double Vxy;
			double Vz;
			double V_what;
			//
			double Px;
			double Py;
			double Pz;
			double P_what;
		};
	};
} velpos8d_t;

typedef struct iana_traj_scapegoat_ {
	//---
	// Initial data
	iana_traj_data_t td0;		// 
	double startpoint0[3];		// start point, x, y, z.  [m]
	double timedelta;			// seconds
	double spinmagOrg;		// use in sqrt()

	// parameters..
	double spinmag_mult;		// use in sqrt()
	double spinmag_scaling_ukunit;		// spin magnitude scaling coefficeint, unknown unit..
	double airPressurePsi_144__288;

	// current working data
	double vmag;				// [m/s]
	double vmag_mph;			// [mile per h]
	double incline;				// [deg]
	double azimuth;				// [deg]
	double backspin;			// [rpm]
	double sidespin;			// [rpm]
	double ballPos[3];			// point, x, y, z.  [m]
	double ballVel[3];			// point, x, y, z.  [m]
	double totaltime;			// seconds [s]
	double winddir;				// Wind direction.   [degree]
	double windmag;				// Wind speed. [m]

	double spinmag;				// [rpm]
	double spinaxis;			// [degree]
	
	// 
	double windY_fps;		// Wind, Y-directional , [feet per sec]
	double windX_fps;		// Wind, X-directional , [feet per sec]


	// Calc data
	velpos8d_t velpos;			
	// [feet per sec]		[0]: Vx, [1]: Vxy, [2]: Vz, [3]: V_what
	// [feet]				[4]: Px, [5]: Py,  [6]: Pz, [3]: P_what

	double vmagCalc;
	double posCalc[3];	
	double velCalc[3];	

//	double inclineCalc;
//	double azimuthCalc;

	double spinmagCalc;
	double spinaxisCalc;
//	double backspinrateCalc;
//	double sidespinrateCalc;

	double S;

} iana_traj_scapegoat_t;



double g_airpressure_psi = AIRPRESSURE_PSI_DEFAULT;
double g_altitude	 = 0.0;


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/
#if 0
#define BACKSPIN_V

static void traj_rungekutta4_3(HAND htraj, double timedelta, I32 category, double spin_halflife, double fm_mult, double fmy_positive_mult);	
static void traj_RK_RightHandSide_3(HAND htraj,
                             double *q, 
							 double *deltaQ, 
							 double td, 
                             double qScale, 
							 double *dq,
							 double t,
							 I32 category
							 , double spin_halflife, double fm_mult, double fmy_positive_mult);	
#endif



I32 iana_traj_scapegoat_airpressure_psi(double airpressure_psi);
I32 iana_traj_scapegoat_airpressure_Pa(double airpressure_Pa);
I32 iana_traj_scapegoat_airpressure_altitude(double altitude);


void iana_traj_scapegoat( HAND htraj, double  timedelta, iana_traj_data_t *ptd, iana_traj_result_t *ptr);
void iana_traj_scapegoat_init( HAND htraj, double  timedelta, iana_traj_data_t *ptd, iana_traj_result_t *ptr); 
void iana_traj_scapegoat_calc( HAND htraj, iana_traj_result_t *ptr); 
void traj_scapegoat_calc( HAND htraj, U32 index, iana_traj_result_t *ptr);
void traj_scapegoat_rungekutar4( HAND htraj, U32 index); 
void CalcSpin_RK4_RHS_perhaps(
		velpos8d_t *pvelpos_prime,			// 
		velpos8d_t *pvelpos_in,
		double value_0d015393805,			// what?
		double value_0d1012500,					// what? what?
		double airPressurePsi_144__288,

		double spinmag,
		double spinmag_scaling_unknown,
		double rz,
		double rx,
		double windY_fps,
		double windX_fps,
		double timeInSec);

U32 traj_scapegoat_update( HAND htraj, U32 index, iana_traj_result_t *ptr);
U32 iana_traj_scapegoat_update_workingdata(HAND htraj, U32 index);
U32 iana_traj_scapegoat_update_result(HAND htraj, U32 index, iana_traj_result_t *ptr);

/*!
 ********************************************************************************
 *	@brief      create trajectory object
 *  @return		handle of trajectory object
 *
 *	@author	    yhsuk
 *  @date       2009/11/08
 *******************************************************************************/
iana_traj_t *iana_traj_scapegoat_create(void)
{
	iana_traj_scapegoat_t	      *ptraj_scapegoat;

	ptraj_scapegoat = (iana_traj_scapegoat_t *) malloc (sizeof(iana_traj_scapegoat_t));
	if (ptraj_scapegoat == NULL) {
		goto func_exit;
	}

	memset(ptraj_scapegoat, 0, sizeof(iana_traj_scapegoat_t));

func_exit:
	return (HAND)ptraj_scapegoat;
}



/*!
 ********************************************************************************
 *	@brief      delete trajectory object
 *	@param[in]  htraj  handle of trajectory object
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2009/11/08
 *******************************************************************************/
void iana_traj_scapegoat_delete(HAND htraj)
{
	if (htraj) {
		free(htraj);
	}
}

/*!
 ********************************************************************************
 *	@brief      set airpressure in psi
 *	@param[in]  airpressure	in psi
 *	@param[out] result    0: set to default, 1; good.
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2019/0805
 *******************************************************************************/
I32 iana_traj_scapegoat_airpressure_psi(double airpressure_psi)
{
	I32 res;
	if (airpressure_psi < AIRPRESSURE_PSI_DEFAULT / 10
		|| airpressure_psi > AIRPRESSURE_PSI_DEFAULT * 10) {		// check range... -_-;

		g_airpressure_psi = AIRPRESSURE_PSI_DEFAULT;
		res = 0;
	} else {
		g_airpressure_psi = airpressure_psi;
		res = 1;
	}

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" airpressure: %lf psi\n", airpressure_psi);
	return res;
}

/*!
 ********************************************************************************
 *	@brief      set airpressure in Pa
 *	@param[in]  airpressure	in Pa
 *	@param[out] result    0: set to default, 1; good.
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2019/1026
 *******************************************************************************/
I32 iana_traj_scapegoat_airpressure_Pa(double airpressure_Pa)
{
	I32 res;
	double airpressure_psi;

	airpressure_psi = AIRPRESSURE_PA2PSI(airpressure_Pa);

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" airpressure: %lf Pa, %lf psi\n", 
			airpressure_Pa, airpressure_psi);
	res = iana_traj_scapegoat_airpressure_psi(airpressure_psi);

	return res;
}


/*!
 ********************************************************************************
 *	@brief      set airpressure in altitude
 *	@param[in]  altitude in m
 *	@param[out] result    0: set to default, 1; good.
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2019/1119
 *******************************************************************************/
I32 iana_traj_scapegoat_airpressure_altitude(double altitude_m)
{
	I32 res;
	double airpressure_psi;

	//--
	if (altitude_m < 0.0) {
		altitude_m  = 0.0;
	}
#define MAXALTITUDE_M	8848						// altitude of Averest
	if (altitude_m > MAXALTITUDE_M) {
		altitude_m = MAXALTITUDE_M;
	} 

	airpressure_psi = AIRPRESSURE_PSI_DEFAULT * exp(-altitude_m / 8434.7);		// See wikipedia.. Barometric formula

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" altitide: %lf airpressure: %lf psi\n", 
			altitude_m, airpressure_psi);

	iana_traj_scapegoat_airpressure_psi(airpressure_psi);
	g_altitude	 = altitude_m;

	res = 1;
	return res;
}



/*!
 ********************************************************************************
 *	@brief      get trajectory     using scapegoat algorithm. :P
 *	@param[in]  htraj		handle of trajectory object
 *	@param[in]  timedelta	time resolution for trajectory (sec)
 *	@param[in]  ptd			input data for trajectory calculation
 *	@param[out]  ptr		result of trajectory calculation
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2019/08/05
 *******************************************************************************/
void iana_traj_scapegoat(
		HAND htraj,
		double  timedelta,
		iana_traj_data_t *ptd,
		iana_traj_result_t *ptr)
{
	iana_traj_scapegoat_t	*ptraj; 

	//------------------------------------
	ptraj = (iana_traj_scapegoat_t *) htraj; 

	// Init param, shot data, working data..
	iana_traj_scapegoat_init(htraj, timedelta, ptd, ptr);

	// Calc Trajectory..

	
	iana_traj_scapegoat_calc(ptraj, ptr);				// Calc trajecry.. :)

	// DO ANYTHINELSE??
}

/*!
 ********************************************************************************
 *	@brief      Init param, shot data and working data
 *	@param[in]  htraj		handle of trajectory object
 *	@param[in]  timedelta	time resolution for trajectory (sec)
 *	@param[in]  ptd			input data for trajectory calculation
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2019/08/05
 *******************************************************************************/
void iana_traj_scapegoat_init(
		HAND htraj,
		double  timedelta,
		iana_traj_data_t *ptd,
		iana_traj_result_t *ptr
		)
{
	iana_traj_scapegoat_t	*ptraj; 

	//------------------------------------
	ptraj = (iana_traj_scapegoat_t *) htraj; 

	// Parameters..
	ptraj->spinmag_mult	= 1.0;				// You could change spin-effect by set proper value of spin_mag_mult.
	//ptraj->spinmag_mult	= 1.0005;				// You could change spin-effect by set proper value of spin_mag_mult.
	{ 	// spin magnitude scaling coefficeint, unknown unit..
		double spinmag_scaling;
		spinmag_scaling = ptraj->spinmag_mult / 60.0;	// rpm -> rps
		spinmag_scaling = spinmag_scaling * 0.43982297;	// What unit? -_-;

		ptraj->spinmag_scaling_ukunit = spinmag_scaling;
	}
	ptraj->airPressurePsi_144__288 = g_airpressure_psi * 144.0 / 28803.6;

	// set initial values
	memcpy(&ptraj->td0, ptd, sizeof(iana_traj_data_t));
	ptraj->startpoint0[0] = 0.0; ptraj->startpoint0[1] = 0.0; ptraj->startpoint0[2] = 0.0;
	ptraj->timedelta = timedelta;

	// Initiate Working data
	ptraj->vmag			= ptd->vmag;
	if (ptraj->vmag < 1.0) {
		ptraj->vmag = 1.0;
	}
	if (ptraj->vmag > 150.0) {
		ptraj->vmag = 150.0;
	}
	ptraj->vmag_mph		= MPS2MPH(ptraj->vmag);
	ptraj->incline		= ptd->incline;
	if (ptraj->incline < -10.0) {
		ptraj->incline = -10.0;
	}
	if (ptraj->incline > 75.0) {		// 
		ptraj->incline = 75.0;
	}
	ptraj->azimuth		= ptd->azimuth;
	if (ptraj->azimuth < -80.0) {
		ptraj->azimuth = -80.0;
	}
	if (ptraj->azimuth > 80.0) {		// 
		ptraj->azimuth = 80.0;
	}

	ptraj->backspin		= ptd->backspin;
	ptraj->sidespin		= ptd->sidespin;

	if (ptraj->sidespin > 15000.0) {
		ptraj->sidespin = 15000.0;
	}


	ptraj->ballPos[0] = 0.0; ptraj->ballPos[1] = 0.0; ptraj->ballPos[2] = 0.0;
	{
		double velX, velY, velZ, velXY;

		velZ 	= ptraj->vmag * sin(DEGREE2RADIAN(ptraj->incline));
		velXY 	= ptraj->vmag * cos(DEGREE2RADIAN(ptraj->incline));

		velX	= velXY * sin(DEGREE2RADIAN(ptraj->azimuth));
		velY	= velXY * cos(DEGREE2RADIAN(ptraj->azimuth));

		ptraj->ballVel[0] = velX;
		ptraj->ballVel[1] = velY;
		ptraj->ballVel[2] = velZ;
	}
	ptraj->totaltime	= IANA_RESULT_COUNT_MAX * timedelta;			// temp.
	ptraj->winddir		= ptd->windangle;
	ptraj->windmag		= ptd->windmag;									// END of ptd


	{
		double backspin, sidespin;
		double spinmag, spinaxis;

		backspin = ptraj->backspin;
		sidespin = ptraj->sidespin;

		spinmag = sqrt(backspin*backspin + sidespin*sidespin);

		if (spinmag < TRAJ_SMALLVALUE) {
			spinmag = TRAJ_SMALLVALUE;
			spinaxis = 0;
		} else {
			spinaxis = atan2(DEGREE2RADIAN(sidespin), DEGREE2RADIAN(backspin));		// tan(sidespin/backspin)
			spinaxis = RADIAN2DEGREE(spinaxis);
		}
		ptraj->spinmag = spinmag;
		ptraj->spinaxis = spinaxis;
		
		ptraj->spinmagOrg = spinmag;
	}
	{
		double windY, windX;

		windY= ptraj->windmag * cos(DEGREE2RADIAN(ptraj->winddir)); 
		windX= ptraj->windmag * sin(DEGREE2RADIAN(ptraj->winddir)); 

		ptraj-> windY_fps = METER2FEET(windY);		// m/s to feet/s
		ptraj-> windX_fps = METER2FEET(windX);		// m/s to feet/s
	}
	//
	{
		//ptraj->velpos.Vx = ptraj->ballVel[0];
		ptraj->velpos.Vx = 0.0;			// what?
		ptraj->velpos.Vxy = METER2FEET(sqrt(ptraj->ballVel[0] * ptraj->ballVel[0] + ptraj->ballVel[1] * ptraj->ballVel[1]));
		ptraj->velpos.Vz = METER2FEET(ptraj->ballVel[2]);
		ptraj->velpos.V_what = 0;

		ptraj->velpos.Px = METER2FEET(ptraj->ballPos[0]);
		ptraj->velpos.Py = METER2FEET(ptraj->ballPos[1]);
		ptraj->velpos.Pz = METER2FEET(ptraj->ballPos[2]);
		ptraj->velpos.P_what = 0;
	}
	ptraj->vmagCalc		= ptraj->vmag;

	ptraj->posCalc[0]	= ptraj->ballPos[0];	// Px
	ptraj->posCalc[1]	= ptraj->ballPos[1];	// Py
	ptraj->posCalc[2]	= ptraj->ballPos[2];	// Pz

	ptraj->velCalc[0]	= ptraj->ballVel[0];	// Vx
	ptraj->velCalc[1]	= ptraj->ballVel[1];	// Vy
	ptraj->velCalc[2]	= ptraj->ballVel[2];	// Vz

	ptraj->spinmagCalc 	= ptraj->spinmag;
	ptraj->spinaxisCalc	= ptraj->spinaxis;
//	ptraj->backspinCalc = ptraj->backspin;

//	ptraj->sidespinrateCalc = sin(DEGREE2RADIAN(ptraj->spinaxis));
//	ptraj->backspinrateCalc = cos(DEGREE2RADIAN(ptraj->spinaxis));


//
	{
		ptraj = (iana_traj_scapegoat_t *) htraj; 
		ptr->count = 0;
		ptr->t[0] = 0;
		ptr->x[0] = ptraj->ballPos[0];		// x (side)
		ptr->y[0] = ptraj->ballPos[1];		// y (target)
		ptr->z[0] = ptraj->ballPos[2];		// z (height)

		ptr->vx[0] = ptraj->ballVel[0];		// x (side)
		ptr->vy[0] = ptraj->ballVel[1];		// y (target)
		ptr->vz[0] = ptraj->ballVel[2];		// z (height)

		ptr->sidespin[0] = sin(DEGREE2RADIAN(ptraj->spinaxis)) * ptraj->spinmag;;
		ptr->backspin[0] = cos(DEGREE2RADIAN(ptraj->spinaxis)) * ptraj->spinmag;;
		ptr->rollspin[0] = 0;

		ptr->wx[0]		= 0;
		ptr->wy[0]		= 0;
		ptr->wz[0]		= 0;
	}
}

/*!
 ********************************************************************************
 *	@brief      Let's calc..
 *	@param[in]  htraj		handle of trajectory object
 *	@param[out]  ptr		result of trajectory calculation
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2019/08/05
 *******************************************************************************/
void iana_traj_scapegoat_calc(
		HAND htraj,
		iana_traj_result_t *ptr)
{
	U32 i;
	U32 hitindex;

	iana_traj_scapegoat_t * ptraj; 

	U32 bdetectGround;
	U32 afterGround;
	double totalDist;
//#define CALCCOUNTMORE	40
#define CALCCOUNTMORE	100

	//----
	ptraj = (iana_traj_scapegoat_t *) htraj; 

	bdetectGround = 0;
	afterGround = 0;
	totalDist = -1.0;
	hitindex = 0;

	for (i = 1; i < IANA_RESULT_COUNT_MAX; i++) {
		traj_scapegoat_calc(htraj, i, ptr);
		
		if (i > 1 && bdetectGround == 0) { 
			totalDist = ptr->y[i];
			hitindex = i;
			if(ptr->z[i] < 0.0) {
				bdetectGround = 1;
				afterGround = 0;
			}
		} 

		if (bdetectGround) {
			afterGround++;
			if (afterGround > CALCCOUNTMORE) {
				break;
			}
		}
	}

	ptr->distance 	= totalDist;	// distance from hit position, (m)

	ptr->height 		= 0;//Check..		// max height. (m)
	ptr->side 			= 0;//Check..	// max side deviation (m)

	ptr->last_vmag 		= 0;// Check..
	ptr->last_azimuth	= 0;// Check..
	ptr->last_incline	= 0;// Check..
	ptr->last_sidespin	= 0;// Check..
	ptr->last_backspin	= 0;// Check..

	ptr->timedelta 	= ptraj->timedelta;					// seconds
	ptr->flighttime = (hitindex + 1) *ptraj->timedelta;	// seconds
	ptr->count 		= i-1;
}

/*!
 ********************************************************************************
 *	@brief      CALC..
 *	@param[in]  htraj		handle of trajectory object
 *	@param[in]  index		frame index
 *	@param[out]  ptr		result of trajectory calculation
 *	@return		none
 *
 *	@author	    yhsuk
 *  @date       2019/08/05
 *******************************************************************************/

void traj_scapegoat_calc(
		HAND htraj,
		U32 index,
		iana_traj_result_t *ptr)
{
	traj_scapegoat_rungekutar4(htraj, index);
	traj_scapegoat_update(htraj, index, ptr);
}


//iana_traj_result_t *ptr

void traj_scapegoat_rungekutar4(
		HAND htraj,
		U32 index)

{
	double timeInSec;
//	double timeInSec_2nd;

	velpos8d_t velpos_in;	
	velpos8d_t velpos_prime;

	velpos8d_t velpos_k1h;	
	velpos8d_t velpos_k2h;	
	velpos8d_t velpos_k3h;	
	velpos8d_t velpos_k4h;	

	iana_traj_scapegoat_t * ptraj; 

	double rx, rz;
	double timeDelta;
	U32 i;
	//---
	ptraj = (iana_traj_scapegoat_t *) htraj; 

	timeDelta 		= ptraj->timedelta;	
	timeInSec 		= timeDelta * index;
//	timeInSec_2nd 	= timedelta * index;

//	posZ_feet = ptraj->ballPos.data[2];			// [feet]				[0]: posX, [1]: posY, [2]: PosZ, [3]: void

	memcpy(&velpos_in, 	&ptraj->velpos, sizeof(velpos8d_t));

	rz = -sin(DEGREE2RADIAN(ptraj->spinaxis));
	rx = cos(DEGREE2RADIAN(ptraj->spinaxis));

	//   k1 = f(tn, yn);
	CalcSpin_RK4_RHS_perhaps(
			&velpos_prime,
			&velpos_in,					// yn
			0.015393805,				// what?
			0.1012500,					// what? what?
			ptraj->airPressurePsi_144__288,

			ptraj->spinmag,
			ptraj->spinmag_scaling_ukunit,

			rz,
			rx,
			ptraj->windY_fps,
			ptraj->windX_fps,
			timeInSec);					// tn
	for (i = 0; i < 8; i++) {
		velpos_k1h.data[i] 	= velpos_prime.data[i] * timeDelta;						// k1 * h
		velpos_in.data[i] 	= velpos_k1h.data[i] / 2.0 + ptraj->velpos.data[i]; 	// k1 * h/2 + yn
	}

	//   k2 = f(tn +1/2 h, yn + 1/2h k1);
	CalcSpin_RK4_RHS_perhaps(
			&velpos_prime,
			&velpos_in,					// k1 * h/2 + yn
			0.015393805,				// what?
			0.1012500,					// what? what?
			ptraj->airPressurePsi_144__288,

			ptraj->spinmag,
			ptraj->spinmag_scaling_ukunit,

			rz,
			rx,
			ptraj->windY_fps,
			ptraj->windX_fps,
			timeInSec + (timeDelta * 0.5));	// tn + h/2
	for (i = 0; i < 8; i++) {
		velpos_k2h.data[i] 	= velpos_prime.data[i] * timeDelta;						// k2 * h
		velpos_in.data[i] 	= velpos_k2h.data[i] / 2.0 + ptraj->velpos.data[i]; 	// k2 * h/2 + yn
	}

	//   k3 = f(tn +1/2 h, yn + 1/2h k2);
	CalcSpin_RK4_RHS_perhaps(
			&velpos_prime,
			&velpos_in,					// k2 * h/2 + yn
			0.015393805,				// what?
			0.1012500,					// what? what?
			ptraj->airPressurePsi_144__288,

			ptraj->spinmag,
			ptraj->spinmag_scaling_ukunit,

			rz,
			rx,
			ptraj->windY_fps,
			ptraj->windX_fps,
			timeInSec + (timeDelta * 0.5));	// tn + h/2
	for (i = 0; i < 8; i++) {
		velpos_k3h.data[i] 	= velpos_prime.data[i] * timeDelta;						// k3 * h
		velpos_in.data[i] 	= velpos_k3h.data[i] + ptraj->velpos.data[i]; 	// k3 * h + yn
	}

	//   k4 = f(tn + h,    yn +    h k3);
	CalcSpin_RK4_RHS_perhaps(
			&velpos_prime,
			&velpos_in,					// k3 * h + yn
			0.015393805,				// what?
			0.1012500,					// what? what?
			ptraj->airPressurePsi_144__288,

			ptraj->spinmag,
			ptraj->spinmag_scaling_ukunit,

			rz,
			rx,
			ptraj->windY_fps,
			ptraj->windX_fps,
			timeInSec + timeDelta);	// tn + h

	for (i = 0; i < 8; i++) {
		velpos_k4h.data[i] 	= velpos_prime.data[i] * timeDelta;						// k4 * h
	}


	// yn+1 = yn + 1/6 h * (k1 + 2k2 + 2k3 + k4)
	//      = yn + 1/6 (k1*h + 2 *(k2*h + k3*h) + k4 * h)
	for (i = 0; i < 8; i++) {
		ptraj->velpos.data[i] = ptraj->velpos.data[i] 
				+ (velpos_k1h.data[i] + 2*velpos_k2h.data[i] + 2*velpos_k3h.data[i] + velpos_k4h.data[i]) / 6.0;
	}
	{
		i = i;
	}
}



void CalcSpin_RK4_RHS_perhaps(
		velpos8d_t *pvelpos_prime,			// 
		velpos8d_t *pvelpos_in,
		double value_0d015393805,			// what?
		double value_0d1012500,					// what? what?
		double airPressurePsi_144__288,

		double spinmag,
		double spinmag_scaling_unknown,
		double rz,   			// sidespinrate, 
		double rx,				// backspinrate,
		double windY_fps,
		double windX_fps,
		double timeInSec)
{
	double airPressure;
	double value_20d0 = 20.0;					// value_20d0;
	double Vx, Vxy, Vz;
	double Vmag;

	double somevalue_div_vmag;
	double timesec_div_20d0;
	double timedecay;
	double timedecay_spinmag_somevalue;
	double timedecay_spinmag_somevalue_div_vmag;

	double cl_prevalue;

	double log____timedecay_spinmag_somevalue_xmm1;
	double log____timedecay_spinmag_somevalue;

	double tmp_xmm0;

	double cl_perhaps;

	double value_pow_10_Neg5;

	double tmp_xmm1;

	double log_vmag_multsomevalue;
//	double log_vmag_multsomevalue_sq_xmm0;
	double log_vmag_multsomevalue_sq;

	double timedecay_spinmag_somevalue_div_vmag_pow_2d5__xmm0;
	double timedecay_spinmag_somevalue_div_vmag_pow_2d5;

	double fmy_coeff;
	
	double Vxy_mult_value;

	double spinshift;		// geust
	//---------------------------------------------
	airPressure = airPressurePsi_144__288;
	value_20d0 = 20.0;					// value_20d0;

	Vx = pvelpos_in->Vx;
	Vx = Vx - windX_fps;	

	Vxy = pvelpos_in->Vxy;
	Vxy = Vxy - windY_fps;

	Vz  = pvelpos_in->Vz;
	
	Vmag = sqrt(Vx*Vx + Vxy*Vxy + Vz*Vz);
	//somevalue_div_vmag = 32.174 / (Vmag * 0.10250);			// what?
	somevalue_div_vmag = 32.174 / (Vmag * value_0d1012500);			// what?
	////somevalue_div_vmag = somevalue_div_vmag * 1.0005;
	//somevalue_div_vmag = somevalue_div_vmag * 1.001;

	timesec_div_20d0 = timeInSec / value_20d0;

	//somevalue_div_vmag = somevalue_div_vmag_xmm1;
	timedecay = exp(-timesec_div_20d0);

	//spinshift = spinmag - 1000;
	//if (spinshift < 200) {
	//	spinshift = 200;
	//}
	{
		// 0 ~ 1500:  0 ~ 1600
		// 1600 ~  :  + 100
		if (spinmag < 1500) {
			spinshift = LINEAR_SCALE(spinmag, 0, 0, 1500.0, 1600.0);
		} else {
			spinshift = spinmag +100;
		}
	}
	
	
	
	
	//timedecay_spinmag_somevalue = timedecay * spinmag * spinmag_scaling_unknown;	  // spinmag * (1/60.0 * 0.43982297)
	timedecay_spinmag_somevalue = timedecay * spinshift * spinmag_scaling_unknown;	  // spinmag * (1/60.0 * 0.43982297)

	timedecay_spinmag_somevalue_div_vmag = timedecay_spinmag_somevalue / Vmag;

	cl_prevalue = (airPressure * 0.5) * Vmag * Vmag / 32.174;  			//v30,    0.5 * airPressure * Vmag*Vmag / 32.174
	log____timedecay_spinmag_somevalue_xmm1 = log(timedecay_spinmag_somevalue_div_vmag);			// loge()

//	v32 = v31;
	log____timedecay_spinmag_somevalue= log____timedecay_spinmag_somevalue_xmm1;

	tmp_xmm0 =  
		(
		 0.073029 
		 - timedecay_spinmag_somevalue_div_vmag * 0.63120002 * log____timedecay_spinmag_somevalue_xmm1 
		 + timedecay_spinmag_somevalue_div_vmag * 0.39629999 * timedecay_spinmag_somevalue_div_vmag
		 ) * 1.02;
	//tmp_xmm0 *= 1.03;
	//tmp_xmm0 /= 1.03;

	cl_perhaps = tmp_xmm0 * cl_prevalue * value_0d015393805;			// v34
	value_pow_10_Neg5 = pow(10.0, -5.0);

	tmp_xmm1= value_pow_10_Neg5;
	tmp_xmm1 = tmp_xmm1 * (Vmag * 0.14) / 0.00015599999;		// v36

	log_vmag_multsomevalue = log(tmp_xmm1);			// loge()
//	log_vmag_multsomevalue_sq_xmm0 = pow(log_vmag_multsomevalue, 2.0);							// v38
//	log_vmag_multsomevalue_sq= log_vmag_multsomevalue_sq_xmm0;									// v39
	log_vmag_multsomevalue_sq = pow(log_vmag_multsomevalue, 2.0);							// v39

	timedecay_spinmag_somevalue_div_vmag_pow_2d5__xmm0 = pow( timedecay_spinmag_somevalue_div_vmag, 2.5);						// v40
	timedecay_spinmag_somevalue_div_vmag_pow_2d5 = timedecay_spinmag_somevalue_div_vmag_pow_2d5__xmm0;			// v36 = v40

	Vz 					= pvelpos_in->Vz;  			// v41
	pvelpos_prime->Pz 	= Vz;		// 

	fmy_coeff = 
		(
		 (
		  (
		   ((timedecay_spinmag_somevalue_div_vmag_pow_2d5 * 0.032249998) + (0.49070999 - (log_vmag_multsomevalue_sq * 0.1208))) 
		   + (log____timedecay_spinmag_somevalue * 0.1134)
		  ) * 0.92
		 ) * cl_prevalue
		) * value_0d015393805;
	//fmy_coeff *= 1.05;			// LONG
	//fmy_coeff *= 1.02;			// LONG
	//fmy_coeff /= 1.05;			// LONG
	// v42
// 0 0  : Bad.. 	
// 1 1  : flip..
// 1 0	: GOOd.
	tmp_xmm1 = ((Vx * rz) - Vz * rx) * fmy_coeff;				// for calc fmy
	
	Vxy_mult_value = fmy_coeff * Vxy;		// v43

 	pvelpos_prime->Vxy 	=  (tmp_xmm1 - (cl_perhaps * Vxy)) * somevalue_div_vmag;
 	pvelpos_prime->Vx 	=  (-cl_perhaps * Vx - Vxy_mult_value * rz) * somevalue_div_vmag;
 	pvelpos_prime->Vz 	=  -32.174 - (cl_perhaps * Vz - Vxy_mult_value * rx) * somevalue_div_vmag;
								// -32.174 : gravity acc. in feet/s^2 (= 9.8066 m/s^2)

	pvelpos_prime->Py 	= pvelpos_in->Vxy;
	pvelpos_prime->Px 	= pvelpos_in->Vx;
	pvelpos_prime->P_what 	= Vmag;

}


U32 traj_scapegoat_update(
		HAND htraj,
		U32 index,
		iana_traj_result_t *ptr)
	{
	U32 res;

	res = iana_traj_scapegoat_update_workingdata(htraj, index);
	res = iana_traj_scapegoat_update_result(htraj, index, ptr);

	return res;
}
	

U32 iana_traj_scapegoat_update_workingdata(HAND htraj, U32 index)
{
	/*

	double vmag
	double velX, velY, velZ, velXY;
	double incilne, azimuth;
	double spinmag;
	double spinaxis;
	*/

	U32 res;
	iana_traj_scapegoat_t	*ptraj; 

	double azimuth_radian;
	double cos_azimuth, sin_azimuth;

	double Px_delta, Py_delta, Pz_delta;
	double Px_0, Py_0, Pz_0;

	double Vx, Vy, Vz;
	double Vx_0, Vy_0, Vz_0;

	double sm_v0, sm_v1, sm_v2;
	double spinmag_next;

	//-----------------------------------------------
	if (htraj == NULL) {
		res = 0;
		goto func_exit;
	}
	ptraj = (iana_traj_scapegoat_t *) htraj; 

	//-------------- Update Calc'ed data
	Px_delta = ptraj->velpos.Px - METER2FEET(ptraj->startpoint0[0]); // 시작점의 X 와 현재의 X 와의 차이 [feet]
	Py_delta = ptraj->velpos.Py - METER2FEET(ptraj->startpoint0[1]); // 시작점의 Y 와 현재의 Y 와의 차이 [feet]
	Pz_delta = ptraj->velpos.Pz - METER2FEET(ptraj->startpoint0[2]); // 시작점의 Z 와 현재의 Z 와의 차이 [feet]
	
	azimuth_radian = DEGREE2RADIAN(ptraj->azimuth);
	cos_azimuth = cos(azimuth_radian);	// Vy_ratio
	sin_azimuth = sin(azimuth_radian); // Vx_ratio
#define DO_ROTATION	
#if defined(DO_ROTATION)
	Px_0 = sin_azimuth * Py_delta + cos_azimuth * Px_delta; 
	Px_0 += ptraj->startpoint0[0];
	ptraj->posCalc[0] = FEET2METER(Px_0);

	Py_0 = cos_azimuth * Py_delta - sin_azimuth * Px_delta;			// v59
	Py_0 += ptraj->startpoint0[1];
	ptraj->posCalc[1] = FEET2METER(Py_0);

	Pz_0 = Pz_delta + METER2FEET(ptraj->startpoint0[2]);
	Pz_0 += ptraj->startpoint0[2];
	ptraj->posCalc[2] = FEET2METER(Pz_0);
#else
	Px_0 = Px_delta;
	Px_0 += ptraj->startpoint0[0];
	ptraj->posCalc[0] = FEET2METER(Px_0);

	Py_0 = Py_delta;
	Py_0 += ptraj->startpoint0[1];
	ptraj->posCalc[1] = FEET2METER(Py_0);

	Pz_0 = Pz_delta + METER2FEET(ptraj->startpoint0[2]);
	Pz_0 += ptraj->startpoint0[2];
	ptraj->posCalc[2] = FEET2METER(Pz_0);
#endif

	//--
	Vx = ptraj->velpos.Vx;
	Vy = ptraj->velpos.Vxy;
	Vz = ptraj->velpos.Vz;

#if defined(DO_ROTATION)
	Vx_0 = sin_azimuth * Vy + cos_azimuth * Vx;
	ptraj->velCalc[0] 	= FEET2METER(Vx_0);

	Vy_0 = cos_azimuth * Vy - sin_azimuth * Vx;
	ptraj->velCalc[1] 	= FEET2METER(Vy_0);

	Vz_0 = Vz;
	ptraj->velCalc[2] 	= FEET2METER(Vz_0);
#else
	Vx_0 = Vx;
	ptraj->velCalc[0] 	= FEET2METER(Vx_0);

	Vy_0 = Vy;
	ptraj->velCalc[1] 	= FEET2METER(Vy_0);

	Vz_0 = Vz;
	ptraj->velCalc[2] 	= FEET2METER(Vz_0);


#endif

	ptraj->vmagCalc 	= FEET2METER(sqrt(Vx_0*Vx_0 + Vy_0*Vy_0 + Vz_0*Vz_0));

	//--
	sm_v0 = -2.0 * pow(10.0, -5.0);
	sm_v1 = (sm_v0 * ptraj->velpos.P_what) / 0.069972001;
	sm_v2 = exp(sm_v1);		// xmm0, v63

	//spinmag_next 		= (sm_v2 * ptraj->spinmag);
	spinmag_next 		= (sm_v2 * ptraj->spinmagOrg);
	ptraj->spinmagCalc 	= spinmag_next;
	ptraj->S 			= ptraj->velpos.P_what;

	//---------------------- Update working data
	ptraj->vmag = ptraj->vmagCalc;
	ptraj->vmag_mph = ptraj->vmag = ptraj->vmagCalc;
	memcpy(&ptraj->ballPos[0], &ptraj->posCalc[0], sizeof(double)*3);
	memcpy(&ptraj->ballVel[0], &ptraj->velCalc[0], sizeof(double)*3);
	ptraj->totaltime = ptraj->timedelta * index;
	ptraj->spinmag = ptraj->spinmagCalc;	
	ptraj->spinaxis = ptraj->spinaxisCalc;	

	res = 1;
func_exit:
	return res;
}

U32 iana_traj_scapegoat_update_result(
		HAND htraj,
		U32 index,
		iana_traj_result_t *ptr)
{
	U32 res;
	U32 i;
	iana_traj_scapegoat_t	*ptraj; 

	double backspin, sidespin;
	double spinmag;
	double spinaxis;
	//---

	ptraj = (iana_traj_scapegoat_t *) htraj; 
	i = index;
	ptr->count = index;
	ptr->t[i] = i * ptraj->timedelta;		// time
	ptr->x[i] = ptraj->ballPos[0];		// x (side)
	ptr->y[i] = ptraj->ballPos[1];		// y (target)
	ptr->z[i] = ptraj->ballPos[2];		// z (height)

	ptr->vx[i] = ptraj->ballVel[0];		// x (side)
	ptr->vy[i] = ptraj->ballVel[1];		// y (target)
	ptr->vz[i] = ptraj->ballVel[2];		// z (height)

	spinmag = ptraj->spinmag;
	//spinmag = ptraj->spinmagOrg;
	spinaxis = ptraj->spinaxis;

	sidespin = sin(DEGREE2RADIAN(spinaxis)) * spinmag;
	backspin = cos(DEGREE2RADIAN(spinaxis)) * spinmag;

	ptr->sidespin[i] = sidespin;
	ptr->backspin[i] = backspin;
	ptr->rollspin[i] = 0;

	ptr->wx[i]		= 0;
	ptr->wy[i]		= 0;
	ptr->wz[i]		= 0;

	res = 1;

	return res;
}




#if defined (__cplusplus)
}
#endif

