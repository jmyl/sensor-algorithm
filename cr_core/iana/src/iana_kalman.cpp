/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA KALMAN Filter
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2020 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_kalman.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2020/02/24 Spin calc.

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <math.h>
#include <string.h>

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"

#include "iana.h"

#include "iana_kalman.h"
/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

extern int g_ccam_lite;
/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
I32 ludcmp(double *pA, I32 n, I32 *pindx, double *d);
I32 lubksb(double *pA, I32 n, I32 *indx, double *pb);
I32 makeQ(double *pQ, double s2w, double dt, U32 kftype);		// state process noise matrix
I32 makeR(double *pR, double s2w, double dt, U32 kftype);		// Observation noise matrix
I32 makeF(double *pF, double dt, U32 kftype);					// state transition matrix
I32 makeH(double *pH);											// Observation matrix


I32 multscalarMat(double v, double *pA, double *pB, I32 n0, I32 n1);
I32 invMat(double *pA, double *pB, I32 n0);
I32 zeroMat(double *pA, I32 n0, I32 n1);
I32 identMat(double *pA, I32 n0);
I32 addMat(double *pA, double *pB, double *pC, I32 n0, I32 n1);
I32 subMat(double *pA, double *pB, double *pC, I32 n0, I32 n1);
I32 multMat(double *pA, double *pB, double *pC, I32 n0, I32 n1, I32 n2);
I32 transMat(double *pA, double *pB, I32 n0, I32 n1);

I32 calcKalmanGain(HAND hkm);
I32 calcUpdateEstimate(HAND hkm, double *observ, U32 bvalid);
I32 calcUpdateCovariance(HAND hkm);
I32 calcProjectState(HAND hkm);
I32 calcProjectCovariance(HAND hkm);






/*!
 ********************************************************************************
 *	@brief      CAM KALMAN filter create
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		KALMAN filter handler
 *
 *	@author	    yhsuk
 *  @date       2020/0224
 *******************************************************************************/
HAND iana_kalman_create()
{
	iana_kalman_t *pkm;

	//--
	pkm = (iana_kalman_t *) malloc(sizeof(iana_kalman_t));
	if (pkm) {
		memset((HAND)pkm, 0, sizeof(iana_kalman_t));
	}

	return (HAND) pkm;
}


/*!
 ********************************************************************************
 *	@brief      CAM KALMAN filter delete
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		KALMAN filter handler
 *
 *	@author	    yhsuk
 *  @date       2020/0224
 *******************************************************************************/
I32 iana_kalman_delete(HAND hkm)
{
	I32 res;

	if (hkm) {
		free(hkm);
	}

	res = 1;

	return res;
}

/*!
 ********************************************************************************
 *	@brief      CAM KALMAN filter initialize
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		0: BAD, 1: GOOD.
 *
 *	@author	    yhsuk
 *  @date       2020/0224
 *******************************************************************************/
I32 iana_kalman_init(
		HAND hkm, 
		U32 kftype,
		U32 type32_0_type64_1,	// 0: float, 1: double
		double *state0,		// initial state vector
		double *p0,				// initial covariance matrix
		double *F,				// initial state transition matrix
		double *H,				// initial Observation matrix
		double *Q,				// initial state process noise matrix (NULL:?)
		double s2w,				// initial state process variance
		double *R,				// initial Observation noise matrix	  (NULL:?)
		double s2v,				// initial observation noise variance
		double dt)
{
	I32 res;
	iana_kalman_t *pkm;
	//--

	if (hkm == NULL) {
		res = 0;
		goto func_exit;
	}

	if (kftype != KFTYPE_CP
		&& kftype != KFTYPE_CV
		&& kftype != KFTYPE_CA)
	{
		res = 0;
		goto func_exit;
	}

	pkm = (iana_kalman_t *) hkm;

	pkm->kftype = kftype;
	pkm->type32_0_type64_1 = type32_0_type64_1;

	memcpy(pkm->state,  state0, sizeof(double) * KFSTATE);
	memcpy(pkm->state_, state0, sizeof(double) * KFSTATE);

	memcpy(pkm->P,  p0, sizeof(double) * KFSTATE * KFSTATE);
	memcpy(pkm->P_, p0, sizeof(double) * KFSTATE * KFSTATE);

	if (F) {
		memcpy(pkm->F,  F,  sizeof(double) * KFSTATE * KFSTATE);
	} else {
		makeF(&pkm->F[0][0], dt, kftype);
	}
	if (H) {
		memcpy(&pkm->H[0][0],  H,  sizeof(double) * KFOBSERVE * KFSTATE);
	} else {
		makeH(&pkm->H[0][0]);
	}

	if (Q) {
		memcpy(&pkm->Q[0][0],  Q,  sizeof(double) * KFSTATE * KFSTATE);
	} else {
		makeQ(&pkm->Q[0][0], s2w, dt, kftype);
	}
	pkm->s2w = s2w;

	if (R) {
		memcpy(&pkm->R[0][0],  R,  sizeof(double) * KFOBSERVE * KFOBSERVE);
	} else {
		makeR(&pkm->R[0][0], s2v, dt, kftype);
	}

	pkm->dt = dt;
	res = 1;

func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      CAM KALMAN filter update
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		0: BAD, 1: GOOD.
 *
 *	@author	    yhsuk
 *  @date       2020/0224
 *******************************************************************************/
I32 iana_kalman_update(HAND hkm, double *observ, U32 bvalid)
{
	I32 res;

	//--
	// 1) Kalman Gain update
	// Kk = Pk_ HT (H Pk_ HT + R)^-1
	res = calcKalmanGain(hkm);
	
	// 2) Update Estimate
	// x^_k = x^_k_ + Kk (zk - HX^_k_)
	res = calcUpdateEstimate(hkm, observ, bvalid);

	// 3) Update Covariance
	// Pk = (I-Kk H) Pk_
	res = calcUpdateCovariance(hkm);

	// 3) Project to k+1
	// x^_k+1_ = F x^_k
	res = calcProjectState(hkm);

	// Pk+1_ = F Pk FT + Q
	res = calcProjectCovariance(hkm);

	res = 1;
	return res;
}



I32 iana_kalman_make_init(
				double pos[],
				double vel[],
				double acc[],
				double posseq[],
				U32    count,
				double state0[],
				double p0[])
{
	I32 res;
	U32 i, j;
	double x0[KFSTATE];
	double xm0[KFSTATE];
	double xm0T[KFSTATE];
	double xm0_xm0T[KFSTATE][KFSTATE];
	double xm0_xm0T_sum[KFSTATE][KFSTATE];



	state0[0] = pos[0];
	state0[1] = vel[0];
	state0[2] = acc[0];

	state0[3] = pos[1];
	state0[4] = vel[1];
	state0[5] = acc[1];

	state0[6] = pos[2];
	state0[7] = vel[2];
	state0[8] = acc[2];

	
	zeroMat(&x0[0], KFSTATE, 1);
	for (i = 0; i < count; i++) {
		x0[0] += posseq[i*3+0];
		x0[1] = 0;
		x0[2] = 0;
		x0[3] += posseq[i*3+1];
		x0[4] = 0;
		x0[5] = 0;
		x0[6] += posseq[i*3+2];
		x0[7] = 0;
		x0[8] = 0;
	}


	for (i = 0; i < KFSTATE; i++) {
		x0[i] = x0[i] / (double) count;
	}

	zeroMat(&xm0_xm0T_sum[0][0], KFSTATE, KFSTATE);
			
	for (i = 0; i < count; i++) {
		xm0[0] = posseq[i*3+0];
		xm0[1] = 0;
		xm0[2] = 0;
		xm0[3] = posseq[i*3+1];
		xm0[4] = 0;
		xm0[5] = 0;
		xm0[6] = posseq[i*3+2];
		xm0[7] = 0;
		xm0[8] = 0;

		for (j = 0; j < KFSTATE; j++) {
			xm0[j] -=  x0[j];
			xm0T[j] = xm0[j];
		}

		multMat(&xm0[0], &xm0T[0], &xm0_xm0T[0][0], KFSTATE, 1, KFSTATE);
		addMat(&xm0_xm0T[0][0], &xm0_xm0T_sum[0][0], &xm0_xm0T_sum[0][0], KFSTATE, KFSTATE);
	}
	multscalarMat((1.0/count), &xm0_xm0T_sum[0][0], &p0[0], KFSTATE, KFSTATE);
	res = 1;

	return res;
}




I32 calcKalmanGain(HAND hkm)
{
	// Kk = Pk_ HT (H Pk_ HT + R)^-1
	I32 res;
	iana_kalman_t *pkm;

	double HT[KFSTATE][KFOBSERVE];
	double P_HT[KFSTATE][KFOBSERVE];
	double HP_HT[KFOBSERVE][KFOBSERVE];
	double HP_HTpR[KFOBSERVE][KFOBSERVE];
	double HP_HTpR_inv[KFOBSERVE][KFOBSERVE];

	//---
	pkm = (iana_kalman_t *) hkm;

	transMat(&pkm->H[0][0], &HT[0][0], KFSTATE, KFOBSERVE);

	multMat(&pkm->P_[0][0], &HT[0][0], &P_HT[0][0], KFSTATE, KFSTATE, KFOBSERVE);

	multMat(&pkm->H[0][0], &P_HT[0][0], &HP_HT[0][0], KFOBSERVE, KFSTATE, KFOBSERVE);	// H P_ HT
	addMat(&HP_HT[0][0], &pkm->R[0][0], &HP_HTpR[0][0], KFOBSERVE, KFOBSERVE);	

	invMat(&HP_HTpR[0][0], &HP_HTpR_inv[0][0], KFOBSERVE);

	multMat(&P_HT[0][0], &HP_HTpR_inv[0][0], &pkm->K[0][0], KFSTATE, KFOBSERVE, KFOBSERVE);	// P_ HT (...)^-1

	res = 1;
	return res;
}


I32 calcUpdateEstimate(HAND hkm, double *observ, U32 bvalid)
{
	// x^_k = x^_k_ + Kk (zk - Hx^_k_)
	I32 res;
	iana_kalman_t *pkm;
	double Hx_k_[KFOBSERVE];
	double KF_innov[KFSTATE];		// state addition..

	//---------
	pkm = (iana_kalman_t *) hkm;

	if (bvalid) {
		multMat(&pkm->H[0][0], 	&pkm->state_[0], 		&Hx_k_[0], 				KFOBSERVE, 	KFSTATE, 	1);
		subMat(observ, 			&Hx_k_[0], 				&pkm->innovation[0], 	KFOBSERVE, 	1);
		multMat(&pkm->K[0][0], 	&pkm->innovation[0], 	&KF_innov[0], 			KFSTATE, 	KFOBSERVE, 	1);
		addMat(&pkm->state_[0],	&KF_innov[0], 			&pkm->state[0], 		KFSTATE, 	1);
	} else {
		memset(&pkm->innovation[0], 0, sizeof(double) * KFOBSERVE);
		memcpy(&pkm->state[0], &pkm->state_[0],	sizeof(double) * KFOBSERVE);
	}

	res = 0;
	return res;
}


I32 calcUpdateCovariance(HAND hkm)
{
	// Pk = (I-Kk H) Pk_
	I32 res;
	iana_kalman_t *pkm;
	double iMat[KFSTATE][KFSTATE];
	double KH[KFSTATE][KFSTATE];
	double I_KH[KFSTATE][KFSTATE];

	//---------
	pkm = (iana_kalman_t *) hkm;

	identMat(&iMat[0][0], KFSTATE);
	multMat(&pkm->K[0][0], &pkm->H[0][0], &KH[0][0], KFSTATE, KFOBSERVE, KFSTATE);

	subMat(&iMat[0][0], &KH[0][0], &I_KH[0][0], KFSTATE, KFSTATE);
	subMat(&I_KH[0][0], &pkm->P_[0][0], &pkm->P[0][0], KFSTATE, KFSTATE);

	res = 1;
	return res;
}

I32 calcProjectState(HAND hkm)
{
	// x^_k+1_ = F x^_k
	I32 res;
	iana_kalman_t *pkm;
	//---------
	pkm = (iana_kalman_t *) hkm;

	multMat(&pkm->F[0][0], &pkm->state[0], &pkm->state_[0], KFSTATE, KFSTATE, 1);

	res = 1;
	return res;
}


I32 calcProjectCovariance(HAND hkm)
{
	// Pk+1_ = F Pk FT + Q
	I32 res;
	iana_kalman_t *pkm;
	
	double FT[KFSTATE][KFSTATE];
	double FP[KFSTATE][KFSTATE];
	double FPFT[KFSTATE][KFSTATE];
	
	//---------
	pkm = (iana_kalman_t *) hkm;

	transMat(&pkm->F[0][0], &FT[0][0], KFSTATE, KFSTATE);
	multMat(&pkm->F[0][0], &pkm->P[0][0], &FP[0][0], KFSTATE, KFSTATE, KFSTATE);

	multMat(&FP[0][0], &FT[0][0], &FPFT[0][0], KFSTATE, KFSTATE, KFSTATE);
			
	addMat(&FPFT[0][0], &pkm->Q[0][0], &pkm->P_[0][0], KFSTATE, KFSTATE);

	res = 1;
	return res;
}


//------------
I32 makeQ(double *pQ, double s2w, double dt, U32 kftype)		// state process noise matrix
{
	I32 res;
//	I32 i, j, k;
	double Q[KFSTATE][KFSTATE];
	double Ck[KFSTATE][KFSTATE];
	double CkT[KFSTATE][KFSTATE];

	double t1;
	double t2_2;
	double t3_6;

	//--


	t1 = dt;
	t2_2 = dt*dt/2.0;
	t3_6 = dt*dt*dt/6.0;

	memset(&Ck[0][0], 0, sizeof(double) * KFSTATE*KFSTATE);
	if (kftype == KFTYPE_CP) {
		Ck[0][0] = t1;

		Ck[3][1] = t1;

		Ck[6][2] = t1;
	} else if (kftype == KFTYPE_CV) {
		Ck[0][0] = t2_2;
		Ck[1][0] = t1;

		Ck[3][1] = t2_2;
		Ck[4][1] = t1;

		Ck[6][2] = t2_2;
		Ck[7][2] = t1;

	} else if (kftype == KFTYPE_CA) {
		Ck[0][0] = t3_6;
		Ck[1][0] = t2_2;
		Ck[2][0] = t1;

		Ck[3][1] = t3_6;
		Ck[4][1] = t2_2;
		Ck[5][1] = t1;

		Ck[6][2] = t3_6;
		Ck[7][2] = t2_2;
		Ck[8][2] = t1;
	} else {
		res = 0;
		goto func_exit;
	}


	transMat(&Ck[0][0], &CkT[0][0], KFSTATE, KFSTATE);
	multMat(&Ck[0][0], &CkT[0][0], &Q[0][0], KFSTATE, KFSTATE, KFSTATE);
	multscalarMat(s2w, &Q[0][0], pQ, KFSTATE, KFSTATE);
#if 0
	memset(&Q[0][0], 0, sizeof(double) * KFSTATE*KFSTATE);
	for (i = 0; i < KFSTATE; i++) {
		for (j = 0; j < KFSTATE; j++) {
			Q[i][j] = 0;
			for (k = 0; k < KFSTATE; k++) {
				Q[i][j] += Ck[i][k] * Ck[k][j];
			}
			Q[i][j] *= s2w;				// 
		}
	}
	memcpy(pQ, &Q[0][0], sizeof(double) * KFSTATE*KFSTATE);
#endif

	res = 1;
func_exit:
	return res;
}


I32 makeR(double *pR, double s2w, double dt, U32 kftype)
{
	I32 res;
	I32 i;

	double R[KFOBSERVE][KFOBSERVE];

	memset(&R[0][0], 0, sizeof(double) * KFOBSERVE*KFOBSERVE);

	for (i = 0; i < KFOBSERVE; i++) {
		R[i][i] = s2w;
	}

	memcpy(pR, &R[0][0], sizeof(double) * KFOBSERVE*KFOBSERVE);

	UNUSED(dt);UNUSED(kftype);
	res = 1;
	return res;
}


I32 makeF(double *pF, double dt, U32 kftype)		// state transition matrix
{
	I32 res;
	double F[KFSTATE][KFSTATE];

	double t1;
	double t2_2;
	double t3_6;

	//--


	t1 = dt;
	t2_2 = dt*dt/2.0;
	t3_6 = dt*dt*dt/6.0;

	memset(&F[0][0], 0, sizeof(double) * KFSTATE*KFSTATE);
	if (kftype == KFTYPE_CP) {
		F[0][0] = 1;

		F[3][3] = 1;

		F[6][2] = 1;
	} else if (kftype == KFTYPE_CV) {
		F[0][0] = 1; F[0][1] = t1;
		             F[1][1] = 1;

		F[3][3] = 1; F[3][4] = t1;
		             F[4][4] = 1;

		F[6][6] = 1; F[6][7] = t1;
		             F[7][7] = 1;
	} else if (kftype == KFTYPE_CA) {
		F[0][0] = 1; F[0][1] = t1; F[0][2] = t2_2;
		             F[1][1] = 1;  F[1][2] = t1;

		F[3][3] = 1; F[3][4] = t1; F[3][5] = t2_2;
		             F[4][4] = 1;  F[4][5] = t1;

		F[6][6] = 1; F[6][7] = t1; F[6][8] = t2_2;
		             F[7][7] = 1;  F[7][8] = t1;
	} else {
		res = 0;
		goto func_exit;
	}

	memcpy(pF, &F[0][0], sizeof(double) * KFSTATE * KFSTATE);

	res = 1;
func_exit:
	return res;
}

I32 makeH(double *pH)		// Observation matrix
{
	I32 res;
	double H[KFOBSERVE][KFSTATE];

	memset(&H[0][0], 0, sizeof(double) * KFOBSERVE*KFSTATE);

	H[0][0] = 1;
	H[1][3] = 1;
	H[2][6] = 1;

	memcpy(pH, &H[0][0], sizeof(double) * KFOBSERVE*KFSTATE);

	res = 1;
	return res;
}


//-------------------------
I32 multMat(double *pA, double *pB, double *pC, I32 n0, I32 n1, I32 n2)
	// C(n0xn2) = A(n0xn1) * B(n1xn2)
{
	I32 res;
	I32 i, j, k;
	double dtmp;

	for (i = 0; i < n0; i++) {
		for (j = 0; j < n2; j++) {
			dtmp = 0;
			for (k = 0; k < n1; k++) {
				dtmp = dtmp + (*(pA + i*n1 + k)) * (*(pB + k*n2 + j));
			}
			*(pC + i*n2 + j) = dtmp;
		}
	}

	res = 1;
	return res; 
}


I32 multscalarMat(double v, double *pA, double *pB, I32 n0, I32 n1)
	// B(n0xn1) = v * A(n0xn1)
{
	I32 res;
	I32 i, j;
	double dtmp;

	for (i = 0; i < n0; i++) {
		for (j = 0; j < n1; j++) {
			dtmp = (*(pA + i*n1 + j)) * v;
			*(pB + i*n1 + j) = dtmp;
		}
	}

	res = 1;
	return res; 
}

I32 addMat(double *pA, double *pB, double *pC, I32 n0, I32 n1)
	// C(n0xn1) = A(n0xn1) + B(n0xn1)
{
	I32 res;
	I32 i, j;
	double dtmp;

	for (i = 0; i < n0; i++) {
		for (j = 0; j < n1; j++) {
			dtmp = (*(pA + i*n1 + j)) + (*(pB + i*n1 + j));
			*(pC + i*n1 + j) = dtmp;
		}
	}

	res = 1;
	return res; 
}

I32 subMat(double *pA, double *pB, double *pC, I32 n0, I32 n1)
	// C(n0xn1) = A(n0xn1) - B(n0xn1)
{
	I32 res;
	I32 i, j;
	double dtmp;

	for (i = 0; i < n0; i++) {
		for (j = 0; j < n1; j++) {
			dtmp = (*(pA + i*n1 + j)) - (*(pB + i*n1 + j));
			*(pC + i*n1 + j) = dtmp;
		}
	}

	res = 1;
	return res; 
}

double detMat(double *pA, I32 n0)
	// |A(n0xn0)|
{
	double ret;
	double *pA1;
	double d;

	I32 *indx;
	//----
	indx = (I32 *) malloc(sizeof(I32)*n0);
	pA1 = (double *) malloc(sizeof(double)*n0*n0);
	memcpy(pA1, pA, sizeof(double) * n0 * n0);

	ret = ludcmp(pA1,n0,indx,&d); 	// Decompose the matrix just once.

	free(indx);
	free(pA1);
	return ret;
}

//with VS2005,  calc time for 9x9 matrix: 0.009703 msec
//with VS2017,  calc time for 9x9 matrix: 0.001788 msec

#define TINY 1.0e-20
I32 invMat(double *pA, double *pB, I32 n0)
	// Inv(A(n0xn0))
{
	I32 ret;
	//double **a,**y,d,*col;
	double d,*col;
	double *pA1;

	I32 i,j,*indx;

	indx = (I32 *) malloc(sizeof(I32)*n0);
	col = (double *) malloc(sizeof(double)*n0);
	pA1 = (double *) malloc(sizeof(double)*n0*n0);
	memcpy(pA1, pA, sizeof(double) * n0 * n0);

	ret = ludcmp(pA1,n0,indx,&d); 	// Decompose the matrix just once.
	for(j=0;j<n0;j++) { 		// Find inverse by columns.
		d *= *(pA1 + j*n0 + j);
	}
//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "d: %lf\n", d);
	if (ret == 0) {
		goto func_exit;
	}

	if (d > -TINY && d < TINY) {
		ret = 0;
		goto func_exit;
	}

	for(j=0;j<n0;j++) { 		// Find inverse by columns.
		for(i=0;i<n0;i++) {
			col[i]=0.0;
		}
		col[j]=1.0;
		lubksb(pA1,n0,indx,col);
		for(i=0;i<n0;i++) {
			*(pB + i*n0 + j) = col[i];
		}
	}

func_exit:
	free(indx);
	free(col);
	free(pA1);
	return ret;
}

I32 zeroMat(double *pA, I32 n0, I32 n1)
{
	I32 res;
	I32 i, j;

	//--
	for (i = 0; i < n0; i++) {
		for (j = 0; j < n1; j++) {
			*(pA + i*n1 + j) = 0;
		}
	}

	res = 1;

	return res;
}

I32 identMat(double *pA, I32 n0)
{
	I32 res;
	I32 i;

	//--
	zeroMat(pA, n0, n0);
	for (i = 0; i < n0; i++) {
		*(pA + i*n0 + i) = 1;
	}

	res = 1;

	return res;
}



I32 ludcmp(double *pA, I32 n, I32 *pindx, double *d)
{
	I32 res;
	I32 i, imax, j, k;
	double big, dum, sum, temp;
	double *vv;

	//--
	vv = (double *) malloc(sizeof(double)*n);
	*d=1.0;
	for (i=0;i<n;i++) {
		big=0.0;
		for (j=0;j<n;j++)
			if ((temp=fabs(
							*(pA + i * n + j)
							)) > big) big=temp;
		if (big == 0.0) {
			res = 0;
			goto func_exit;
			//nrerror("Singular matrix in routine ludcmp");
		}

		vv[i]=1.0/big;
	}
	for (j=0;j<n;j++) {
		for (i=0;i<j;i++) {
			//sum=a[i][j];
			sum=*(pA + i * n + j);

			for (k=0;k<i;k++) {
				//sum -= a[i][k]*a[k][j];
				sum -= (*(pA + i * n + k)) * (*(pA + k * n + j));
			}
			//a[i][j]=sum;
			*(pA + i * n + j) = sum;
		}
		big=0.0;
		imax = 0;
		for (i=j;i<n;i++) {
			//sum=a[i][j];
			sum = *(pA + i * n + j);
			for (k=0;k<j;k++) {
				//sum -= a[i][k]*a[k][j];
				sum -= (*(pA + i * n + k)) * (*(pA + k * n + j));
			}
			//a[i][j]=sum;
			*(pA + i * n + j) = sum;
			if ( (dum=vv[i]*fabs(sum)) >= big) {
				big=dum;
				imax=i;
			}
		}
		if (j != imax) {
			for (k=0;k<n;k++) {
				//dum=a[imax][k];
				dum= *(pA+imax*n+k);
				//a[imax][k]=a[j][k];
				*(pA+imax*n+k) = *(pA+j*n+k);
				//a[j][k]=dum;
				*(pA+j*n+k) = dum;
			}
			*d = -(*d);
			vv[imax]=vv[j];
		}
		pindx[j]=imax;
		/*
		if (a[j][j] == 0.0) {
			a[j][j]=TINY;
		}
		*/
		if (*(pA+j*n+j) == 0.0) {
			*(pA+j*n+j) = TINY;
		}
		
		if (j != n-1) {
			/*
			dum=1.0/(a[j][j]);
			for (i=j+1;i<n;i++) a[i][j] *= dum;
			*/
			dum=1.0/(*(pA+n*j+j));

			for (i=j+1;i<n;i++) {
				*(pA+n*i+j) *= dum;
			}
		}
	}
	res = 1;
func_exit:
	free(vv);
	return res;
}


I32 lubksb(
		double *pA,
		I32 n,
		I32 *indx,
		double *pb)
{
	I32 res;
	I32 i,ii=-1,ip,j;
	double sum;

	for (i=0;i<n;i++) {
		ip=indx[i];
		sum=pb[ip];
		pb[ip]=pb[i];
		if (ii!=-1) {
			for (j=ii;j<i;j++) {
				//sum -= a[i][j]*b[j];
				sum -= (*(pA + i*n + j)) * pb[j];
			}
		} else if (sum) ii=i;
		pb[i]=sum;
	}
	for (i=n-1;i >= 0;i--) {
		sum=pb[i];
		for (j=i+1;j<n;j++) {
			//sum -= a[i][j]*b[j];
			sum -= (*(pA + i*n + j)) * pb[j];
		}
		//b[i]=sum/a[i][i];
		pb[i]=sum/ (*(pA + i*n + i));
	}

	res = 1;

	return res;
}



I32 transMat(double *pA, double *pB, I32 n0, I32 n1)
{
	I32 ret;
	I32 i, j;
	double dtmp;

	for (i = 0; i < n0; i++) {
		for (j = 0; j < n1; j++) {
			dtmp = (*(pA + i*n1 + j));
			*(pB + j*n0 + i) = dtmp;
		}
	}

	ret = 1;
	return ret;
}


#if defined (__cplusplus)
}
#endif


// Not used
#if 0
//----------------------------------------------------------------------------------
int mmmTEST()
{
	I32 i, j;
#if 0
	double A[3*3] = {
		2,3, -1,
		4, 4, -3,
		-2, 3, -1
	};
	double IA[3*3];
	double II[3*3];
#endif

#if 1
#define NNN	9
//#define NNN	3
	double A[NNN*NNN];
	double IA[NNN*NNN];
	double II[NNN*NNN];
#endif
	//--
	for (i = 0; i < NNN; i++) {
		for (j = 0; j < NNN; j++) {
			A[i * NNN + j] = rand() % 1234;
//				i*i - j - i + 1;
		}
	}

	{
		I32 k;
		U32 t0, t1;
#define TRY_K	1000000
		t0 = cr_gettickcount();
		for (k = 0; k < TRY_K; k++) {
			srand(cr_gettickcount());
			invMat(&A[0], &IA[0], NNN);
		}
		t1 = cr_gettickcount();

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "t: %d ~ %d (%d), %lf msec\n", t0, t1, (t1-t0), ((double)(t1-t0))/TRY_K);

	}

	multMat(&A[0], &IA[0], &II[0], NNN, NNN, NNN);

	for (i = 0; i < NNN; i++) {
		for (j = 0; j < NNN; j++) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%lf ", A[NNN*i +j]);
		}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
	}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");


	for (i = 0; i < NNN; i++) {
		for (j = 0; j < NNN; j++) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%lf ", IA[NNN*i +j]);
		}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
	}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");

	for (i = 0; i < NNN; i++) {
		for (j = 0; j < NNN; j++) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%lf ", II[NNN*i +j]);
		}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
	}
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");

	return 0;
}
//-----------------------------------------------------------------------------------

#endif

