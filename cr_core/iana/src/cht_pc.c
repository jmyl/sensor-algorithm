#if 0
//#include <stdlib.h>
//#include <string.h>		// for use of memset()
#include "xil_types.h"
//#include "xil_cache.h"	// for use of Xil_DCacheInvalidateRange()
#include "xil_io.h"		// for xil_printf
//#include "FreeRTOS.h"	// for use of pvPortMalloc() and vPortFree()
#include "math.h"	// for use of sqrt(), Should include "m" option in compile setting.
#endif

#if 1
#include <math.h>
#include "cr_common.h"
#endif

#if defined(__linux__)
#ifndef min
#define min(a, b) (((a) < (b)) ? (a) : (b))
#endif
#ifndef max
#define max(a, b) (((a) > (b)) ? (a) : (b))
#endif
#endif

/*
	Example of usage of cht_refine()

	uint8_t image[1024][1280];
	uint16_t circle_x, circle_y;
	uint8_t circle_r;
	uint8_t circle_found;

	circle_found = cht_refine(&circle_x, &circle_y, &circle_r,	: circle_x와 circle_y와 circle_r로 결과값을 넘겨줍니다. circle의 x,y좌표,반지름이 결과값입니다.
			(uint8_t *)image,	: 이미지의 주소입니다.
			1280,				: 이미지의 x방향 사이즈입니다. 예를 들어 이미지가 1280x1024라면 xstride는 1280입니다.
			40, 50,				: 찾고자 하는 circle의 반지름 범위입니다. 찾고자 하는 원이 90x90이라면 반지름이 45이므로 40~50정도로 설정하면 됩니다. 너무 넓게 설정하면 느려집니다.
			320, 256, 959, 767,	: 이미지 중에서 circle을 찾고자 하는 ROI범위입니다. 1280x1024 이미지 중 320~960, 256~768 안에서만 circle을 찾고자 한다면 320, 256, 959, 767로 설정하면 됩니다. 너무 크게 설정하면 느려집니다.
			256,				: 이미지의 Gradient threshold입니다. Gradient가 얼마 이상이 되어야 Edge로 인정할 것이냐는 값인데 256정도에서 시작해서 Contrast가 좋은 이미지는 이 값을 올려도 되고 Contrast가 좋지 않은 이미지는 이 값을 내려야 합니다. 너무 작게 설정하면 느려집니다.
			32);				: 원을 판단하는 Voting threshold입니다. Circle edge 중 어느 정도가 보여야 원으로 인정해 줄 것이냐는 Threshold값입니다. 이 값에 따라 이 함수의 리턴값(circle_found)이 달라집니다. 반원보다 작은 원을 circle로 인정하려면 이 값을 내리면 됩니다.

	만약 이미 발견된 원의 Coarse 좌표가 있고 이것을 좀더 정확하게 Refine하고 싶다면 아래처럼 적용할 수 있습니다.

	#define MARGIN 16	// Coarse좌표의 정확도가 좋지 않으면 키우면 되고, 속도가 너무 느리다면 줄이면 됨
	cht_refine(&circle_x, &circle_y, &circle_r,
			(uint8_t *)image,
			1280,
			40, 50,
			circle_x-circle_r-MARGIN, circle_y-circle_r-MARGIN, circle_x+circle_r+MARGIN, circle_y+circle_r+MARGIN,
			256,
			0);

	circle_x, circle_y, circle_r이 coarse하게 검출한 원의 정보라면 위의 함수를 실행하면 circle_x, circle_y, circle_r이 fine하게 검출된 원의 정보로 업데이트됩니다.

	만약 위와 같이 circle을 fine하게 검출하는 용도로만 사용한다면 아래 #define된 부분의 MAX_IMG_XSIZE와 MAX_IMG_YSIZE를 줄일 수 있습니다.
	예를 들어 ROI크기가 MARGIN 포함 최대 128x128이라면 MAX_IMG_XSIZE와 MAX_IMG_YSIZE를 128로 바꾸면 메모리가 절약됩니다.

	MAX_R_MINUS_MIN_R은 찾고자 하는 circle의 반지름 범위(max_r - min_r)입니다.
	예를 들어, 이 함수를 사용할 때 반지름 30~40사이의 원을 찾을 수도 있고, 반지름 35~50 사이의 원을 찾을수도 있다면 이 값을 15로 세팅하시면 됩니다.
	이 값에 따라 메모리(circle_vote)를 할당해 놓는 용도입니다.
*/

#define MAX_R_MINUS_MIN_R (16)	// This means that max_r - min_r should be less than 16.
#define MAX_IMG_XSIZE (1280)
#define MAX_IMG_YSIZE (1024)

#if 0
static uint8_t image[MAX_IMG_XSIZE*MAX_IMG_YSIZE];
static uint32_t circle_vote[MAX_IMG_XSIZE*MAX_IMG_YSIZE][MAX_R_MINUS_MIN_R + 2];

uint8_t cht_refine(uint16_t *circle_x, uint16_t *circle_y, uint8_t *circle_r, // outputs of this function
	uint8_t *image_original, uint16_t xstride, uint8_t min_r, uint8_t max_r,	// input parameters
	uint16_t cht_xroi_start, uint16_t cht_yroi_start, uint16_t cht_xroi_end, uint16_t cht_yroi_end, // input parameters
	int32_t threshold_gradient, uint32_t threshold_vote)	// input parameters
#endif

static U08 image[MAX_IMG_XSIZE*MAX_IMG_YSIZE];
static U32 circle_vote[MAX_IMG_XSIZE*MAX_IMG_YSIZE][MAX_R_MINUS_MIN_R + 2];

U08 cht_refine(U16 *circle_x, U16 *circle_y, U08 *circle_r, // outputs of this function
	U08 *image_original, U16 xstride, U08 min_r, U08 max_r,	// input parameters
	U16 cht_xroi_start, U16 cht_yroi_start, U16 cht_xroi_end, U16 cht_yroi_end, // input parameters
	I32 threshold_gradient, U32 threshold_vote)	// input parameters
{
#if 0
	uint32_t index;
	int32_t x, y;
	int32_t gx, gy, R;
	int32_t max_grad = 0;
	uint16_t max_vote=0;
	int16_t max_vote_x, max_vote_y, max_vote_r;

	uint16_t xsize, ysize;
#endif
#if 1
	U32 index;
	I32 x, y;
	I32 gx, gy, R;
	I32 max_grad = 0;
	U16 max_vote=0;
	I16 max_vote_x, max_vote_y, max_vote_r;

	U16 xsize, ysize;
#endif

	max_vote_x = 0; max_vote_y = 0; max_vote_r = 0;

	xsize = cht_xroi_end - cht_xroi_start + 1;
	ysize = cht_yroi_end - cht_yroi_start + 1;

	// Initialize
	for(y=0; y< ysize; y++){
		index = (y+cht_yroi_start)*xstride + cht_xroi_start;
		for(x=0; x< xsize; x++){
			image[y*xsize+x] = image_original[index + x];
			for(R=0; R<=max_r-min_r+2; R++) circle_vote[y*xsize+x][R] = 0;
		}
	}
	//-----------------------------------------------
	// Calculate 5x5 gradient and Voting
	//-----------------------------------------------
	for(y=2; y<ysize-2; y++)
	{
		for(x=2; x<xsize-2; x++)
		{
			// Original calculation
			/*gx = image[y][x+4] + image[y+1][x+4] + image[y+2][x+4]*2 + image[y+3][x+4] + image[y+4][x+4];
			gx += image[y][x+3]*2 + image[y+1][x+3]*2 + image[y+2][x+3]*4 + image[y+3][x+3]*2 + image[y+4][x+3]*2;
			gx -= image[y][x+1]*2 + image[y+1][x+1]*2 + image[y+2][x+1]*4 + image[y+3][x+1]*2 + image[y+4][x+1]*2;
			gx -= image[y][x] + image[y+1][x] + image[y+2][x]*2 + image[y+3][x] + image[y+4][x];

			gy = image[y+4][x] + image[y+4][x+1] + image[y+4][x+2]*2 + image[y+4][x+3] + image[y+4][x+4];
			gy += image[y+3][x]*2 + image[y+3][x+1]*2 + image[y+3][x+2]*4 + image[y+3][x+3]*2 + image[y+3][x+4]*2;
			gy -= image[y+1][x]*2 + image[y+1][x+1]*2 + image[y+1][x+2]*4 + image[y+1][x+3]*2 + image[y+1][x+4]*2;
			gy -= image[y][x] + image[y][x+1] + image[y][x+2]*2 + image[y][x+3] + image[y][x+4];*/

			//--------------------------------
			// Optimized gradient calculation
			gx=0; gy=0;
			index = (y-2) * xsize + (x-2);

			gx += image[index+4] + image[index+3]*2 - image[index+1]*2 - image[index];
			gy -= image[index] + image[index+1] + image[index+2]*2 + image[index+3] + image[index+4];

			index += xsize;
			gx += image[index+4] + image[index+3]*2 - image[index+1]*2 - image[index];
			gy -= image[index]*2 + image[index+1]*2 + image[index+2]*4 + image[index+3]*2 + image[index+4]*2;

			index += xsize;
			gx += image[index+4]*2 + image[index+3]*4 - image[index+1]*4 - image[index]*2;

			index += xsize;
			gx += image[index+4] + image[index+3]*2 - image[index+1]*2 - image[index];
			gy += image[index]*2 + image[index+1]*2 + image[index+2]*4 + image[index+3]*2 + image[index+4]*2;

			index += xsize;
			gx += image[index+4] + image[index+3]*2 - image[index+1]*2 - image[index];
			gy += image[index] + image[index+1] + image[index+2]*2 + image[index+3] + image[index+4];
			//--------------------------------

			if(gx * gx + gy * gy > threshold_gradient * threshold_gradient)
			{
#if 0
				if(max_grad < gx * gx + gy * gy) max_grad = gx * gx + gy * gy;

				double grad = sqrt((double)(gx * gx + gy * gy));
				double grad_x = ((double)gx)/grad;
				double grad_y = ((double)gy)/grad;

				int16_t xshift, yshift;
#endif
				double grad;
				double grad_x;
				double grad_y;

				I16 xshift, yshift;

				if(max_grad < gx * gx + gy * gy) max_grad = gx * gx + gy * gy;

				grad = sqrt((double)(gx * gx + gy * gy));
				grad_x = ((double)gx)/grad;
				grad_y = ((double)gy)/grad;


				for(R=min_r; R<=max_r; R++)
				{
#if 0
					uint8_t Roffset;
					xshift = (int16_t)( ((double)R) * grad_x );
					yshift = (int16_t)( ((double)R) * grad_y );
#endif
					U08 Roffset;
					xshift = (I16)( ((double)R) * grad_x );
					yshift = (I16)( ((double)R) * grad_y );

					//if((y+yshift >=0) & (y+yshift < ysize) & (x+xshift >=0) & (x+xshift < xsize))
					// 		circle_vote[y+yshift][x+xshift][R-min_r] ++;	// Single voting
					// But it prefers 3x3x3 voting as below to do more filtering
					if((y+yshift >=1) & (y+yshift < ysize-1) & (x+xshift >=1) & (x+xshift < xsize-1))
					//for(uint8_t Roffset=0; Roffset<=2; Roffset++)
					for(Roffset=0; Roffset<=2; Roffset++)
					{
						index = ((U32)(y+yshift-1))*xsize +(x+xshift-1);
						if((circle_vote[index][R-min_r+Roffset] ++)>max_vote) {max_vote = (U16)circle_vote[index][R-min_r+Roffset]; max_vote_x=(U16)(x+xshift-1); max_vote_y=(U16)(y+yshift-1); max_vote_r=(U16)(R+Roffset-1);};
						if((circle_vote[index+1][R-min_r+Roffset] ++)>max_vote) {max_vote = (U16)circle_vote[index+1][R-min_r+Roffset]; max_vote_x=(U16)(x+xshift); max_vote_y=(U16)(y+yshift-1); max_vote_r=(U16)(R+Roffset-1);};
						if((circle_vote[index+2][R-min_r+Roffset] ++)>max_vote) {max_vote = (U16)circle_vote[index+2][R-min_r+Roffset]; max_vote_x=(U16)(x+xshift+1); max_vote_y=(U16)(y+yshift-1); max_vote_r=(U16)(R+Roffset-1);};

						index += xsize;
						if((circle_vote[index][R-min_r+Roffset] ++)>max_vote) {max_vote = (U16)circle_vote[index][R-min_r+Roffset]; max_vote_x=(U16)(x+xshift-1); max_vote_y=(U16)(y+yshift); max_vote_r=(U16)(R+Roffset-1);};
						if((circle_vote[index+1][R-min_r+Roffset] ++)>max_vote) {max_vote = (U16)circle_vote[index+1][R-min_r+Roffset]; max_vote_x=(U16)(x+xshift); max_vote_y=(U16)(y+yshift); max_vote_r=(U16)(R+Roffset-1);};
						if((circle_vote[index+2][R-min_r+Roffset] ++)>max_vote) {max_vote = (U16)circle_vote[index+2][R-min_r+Roffset]; max_vote_x=(U16)(x+xshift+1); max_vote_y=(U16)(y+yshift); max_vote_r=(U16)(R+Roffset-1);};

						index += xsize;
						if((circle_vote[index][R-min_r+Roffset] ++)>max_vote) {max_vote = (U16)circle_vote[index][R-min_r+Roffset]; max_vote_x=(U16)(x+xshift-1); max_vote_y=(U16)(y+yshift+1); max_vote_r=(U16)(R+Roffset-1);};
						if((circle_vote[index+1][R-min_r+Roffset] ++)>max_vote) {max_vote = (U16)circle_vote[index+1][R-min_r+Roffset]; max_vote_x=(U16)(x+xshift); max_vote_y=(U16)(y+yshift+1); max_vote_r=(U16)(R+Roffset-1);};
						if((circle_vote[index+2][R-min_r+Roffset] ++)>max_vote) {max_vote = (U16)circle_vote[index+2][R-min_r+Roffset]; max_vote_x=(U16)(x+xshift+1); max_vote_y=(U16)(y+yshift+1); max_vote_r=(U16)(R+Roffset-1);};
					}
				}
			}
		}
	}
	//-----------------
	// Result
	if(max_vote *27 > (U16)(threshold_vote * max_vote_r)){
		*circle_r = (U08)max_vote_r;
		*circle_x = max_vote_x + cht_xroi_start;
		*circle_y = max_vote_y + cht_yroi_start;
		//xil_printf("circle found: %u %u %u %u %lu\r\n", *circle_x, *circle_y, *circle_r, max_vote, max_grad);
		return 1;
	} else {
		*circle_r = 0;
		*circle_x = 0;
		*circle_y = 0;
		//xil_printf("circle not found: %u %u %u %u %lu\r\n", *circle_x, *circle_y, *circle_r, max_vote, max_grad);
		return 0;
	}
}

U08 cht_refine_double(double *circle_x, double *circle_y, double *circle_r, // outputs of this function
	U08 *image_original, U16 xstride, U08 min_r, U08 max_r,	// input parameters
	U16 cht_xroi_start, U16 cht_yroi_start, U16 cht_xroi_end, U16 cht_yroi_end, // input parameters
	I32 threshold_gradient, U32 threshold_vote)	// input parameters
{
	U32 index;
	I32 x, y;
	I32 gx, gy, R;
	I32 max_grad = 0;
	U16 max_vote = 0;
	I16 max_vote_x, max_vote_y, max_vote_r;

	U16 xsize, ysize;

	max_vote_x = 0; max_vote_y = 0; max_vote_r = 0;

	xsize = cht_xroi_end - cht_xroi_start + 1;
	ysize = cht_yroi_end - cht_yroi_start + 1;

	// Initialize
	for (y = 0; y < ysize; y++) {
		index = (y + cht_yroi_start)*xstride + cht_xroi_start;
		for (x = 0; x < xsize; x++) {
			image[y*xsize + x] = image_original[index + x];
			for (R = 0; R <= max_r - min_r + 2; R++) circle_vote[y*xsize + x][R] = 0;
		}
	}
	//-----------------------------------------------
	// Calculate 5x5 gradient and Voting
	//-----------------------------------------------
	for (y = 2; y < ysize - 2; y++)
	{
		for (x = 2; x < xsize - 2; x++)
		{
			// Original calculation
			/*gx = image[y][x+4] + image[y+1][x+4] + image[y+2][x+4]*2 + image[y+3][x+4] + image[y+4][x+4];
			gx += image[y][x+3]*2 + image[y+1][x+3]*2 + image[y+2][x+3]*4 + image[y+3][x+3]*2 + image[y+4][x+3]*2;
			gx -= image[y][x+1]*2 + image[y+1][x+1]*2 + image[y+2][x+1]*4 + image[y+3][x+1]*2 + image[y+4][x+1]*2;
			gx -= image[y][x] + image[y+1][x] + image[y+2][x]*2 + image[y+3][x] + image[y+4][x];

			gy = image[y+4][x] + image[y+4][x+1] + image[y+4][x+2]*2 + image[y+4][x+3] + image[y+4][x+4];
			gy += image[y+3][x]*2 + image[y+3][x+1]*2 + image[y+3][x+2]*4 + image[y+3][x+3]*2 + image[y+3][x+4]*2;
			gy -= image[y+1][x]*2 + image[y+1][x+1]*2 + image[y+1][x+2]*4 + image[y+1][x+3]*2 + image[y+1][x+4]*2;
			gy -= image[y][x] + image[y][x+1] + image[y][x+2]*2 + image[y][x+3] + image[y][x+4];*/

			//--------------------------------
			// Optimized gradient calculation
			gx = 0; gy = 0;
			index = (y - 2) * xsize + (x - 2);

			gx += image[index + 4] + image[index + 3] * 2 - image[index + 1] * 2 - image[index];
			gy -= image[index] + image[index + 1] + image[index + 2] * 2 + image[index + 3] + image[index + 4];

			index += xsize;
			gx += image[index + 4] + image[index + 3] * 2 - image[index + 1] * 2 - image[index];
			gy -= image[index] * 2 + image[index + 1] * 2 + image[index + 2] * 4 + image[index + 3] * 2 + image[index + 4] * 2;

			index += xsize;
			gx += image[index + 4] * 2 + image[index + 3] * 4 - image[index + 1] * 4 - image[index] * 2;

			index += xsize;
			gx += image[index + 4] + image[index + 3] * 2 - image[index + 1] * 2 - image[index];
			gy += image[index] * 2 + image[index + 1] * 2 + image[index + 2] * 4 + image[index + 3] * 2 + image[index + 4] * 2;

			index += xsize;
			gx += image[index + 4] + image[index + 3] * 2 - image[index + 1] * 2 - image[index];
			gy += image[index] + image[index + 1] + image[index + 2] * 2 + image[index + 3] + image[index + 4];
			//--------------------------------

			if (gx * gx + gy * gy > threshold_gradient * threshold_gradient)
			{
				double grad = sqrt((double)(gx * gx + gy * gy));			
				double grad_x;
				double grad_y;
				I16 xshift, yshift;


				if (max_grad < gx * gx + gy * gy) max_grad = gx * gx + gy * gy;

				grad_x = ((double)gx) / grad;
				grad_y = ((double)gy) / grad;



				for (R = min_r; R <= max_r; R++)
				{
					U08 Roffset;
					xshift = (I16)(((double)R) * grad_x);
					yshift = (I16)(((double)R) * grad_y);

					//if((y+yshift >=0) & (y+yshift < ysize) & (x+xshift >=0) & (x+xshift < xsize))
					// 		circle_vote[y+yshift][x+xshift][R-min_r] ++;	// Single voting
					// But it prefers 3x3x3 voting as below to do more filtering
					if ((y + yshift >= 1) & (y + yshift < ysize - 1) & (x + xshift >= 1) & (x + xshift < xsize - 1))						
						for (Roffset = 0; Roffset <= 2; Roffset++)
						{
							index = ((U32)(y + yshift - 1))*xsize + (x + xshift - 1);
							if ((circle_vote[index][R - min_r + Roffset] ++) > max_vote) { max_vote = (U16)circle_vote[index][R - min_r + Roffset]; max_vote_x = (U16)(x + xshift - 1); max_vote_y = (U16)(y + yshift - 1); max_vote_r = (U16)(R - min_r + Roffset); };
							if ((circle_vote[index + 1][R - min_r + Roffset] ++) > max_vote) { max_vote = (U16)circle_vote[index + 1][R - min_r + Roffset]; max_vote_x = (U16)(x + xshift); max_vote_y = (U16)(y + yshift - 1); max_vote_r = (U16)(R - min_r + Roffset); };
							if ((circle_vote[index + 2][R - min_r + Roffset] ++) > max_vote) { max_vote = (U16)circle_vote[index + 2][R - min_r + Roffset]; max_vote_x = (U16)(x + xshift + 1); max_vote_y = (U16)(y + yshift - 1); max_vote_r = (U16)(R - min_r + Roffset); };

							index += xsize;
							if ((circle_vote[index][R - min_r + Roffset] ++) > max_vote) { max_vote = (U16)circle_vote[index][R - min_r + Roffset]; max_vote_x = (U16)(x + xshift - 1); max_vote_y = (U16)(y + yshift); max_vote_r = (U16)(R - min_r + Roffset); };
							if ((circle_vote[index + 1][R - min_r + Roffset] ++) > max_vote) { max_vote = (U16)circle_vote[index + 1][R - min_r + Roffset]; max_vote_x = (U16)(x + xshift); max_vote_y = (U16)(y + yshift); max_vote_r = (U16)(R - min_r + Roffset); };
							if ((circle_vote[index + 2][R - min_r + Roffset] ++) > max_vote) { max_vote = (U16)circle_vote[index + 2][R - min_r + Roffset]; max_vote_x = (U16)(x + xshift + 1); max_vote_y = (U16)(y + yshift); max_vote_r = (U16)(R - min_r + Roffset); };

							index += xsize;
							if ((circle_vote[index][R - min_r + Roffset] ++) > max_vote) { max_vote = (U16)circle_vote[index][R - min_r + Roffset]; max_vote_x = (U16)(x + xshift - 1); max_vote_y = (U16)(y + yshift + 1); max_vote_r = (U16)(R - min_r + Roffset); };
							if ((circle_vote[index + 1][R - min_r + Roffset] ++) > max_vote) { max_vote = (U16)circle_vote[index + 1][R - min_r + Roffset]; max_vote_x = (U16)(x + xshift); max_vote_y = (U16)(y + yshift + 1); max_vote_r = (U16)(R - min_r + Roffset); };
							if ((circle_vote[index + 2][R - min_r + Roffset] ++) > max_vote) { max_vote = (U16)circle_vote[index + 2][R - min_r + Roffset]; max_vote_x = (U16)(x + xshift + 1); max_vote_y = (U16)(y + yshift + 1); max_vote_r = (U16)(R - min_r + Roffset); };
						}
				}
			}
		}
	}
	//-----------------
	// Result
	if ((max_vote * 27 > (U16)(threshold_vote * max_vote_r))&(max_vote_y>=2)&(max_vote_x>=2)&(max_vote_y + 2 < ysize)&& (max_vote_x + 2 < xsize)) {
		// Weighted Sum 5x5x5
		U32 wsum = 0;
		U64 wsum_x = 0, wsum_y = 0;
		for (y = max_vote_y - 2; y <= max_vote_y + 2; y++) {
			for (x = max_vote_x - 2; x <= max_vote_x + 2; x++) {
				for (R = max(max_vote_r - 2, 0); R <= min(max_vote_r + 2, max_r - min_r + 2); R++) {
					wsum += circle_vote[y * xsize + x][R];
					wsum_x += circle_vote[y * xsize + x][R] * x;
					wsum_y += circle_vote[y * xsize + x][R] * y;					
				}
			}
		}

		*circle_r = ((double)(max_vote_r + min_r - 1));
		*circle_x = ((double)wsum_x) / ((double)wsum) + (double)cht_xroi_start;
		*circle_y = ((double)wsum_y) / ((double)wsum) + (double)cht_yroi_start;
		//xil_printf("circle found: %u %u %u %u %lu\r\n", *circle_x, *circle_y, *circle_r, max_vote, max_grad);
		return 1;
	}
	else {
		*circle_r = 0.0;
		*circle_x = 0.0;
		*circle_y = 0.0;
		//xil_printf("circle not found: %u %u %u %u %lu\r\n", *circle_x, *circle_y, *circle_r, max_vote, max_grad);
		return 0;
	}
}

I32 iana_getballcandidator_cht_(
	U08 *buf,
	U32 width, U32 height, 			// ROI width
	U32 width2,						// Image width for buf

	double *circle_x_double,
	double *circle_y_double,
	double *circle_r_double,

	U32 min_r, U32 max_r,
	I32 threshold_gradient, U32 threshold_vote)
{
	I32 res;
	double circle_x, circle_y, circle_r;

	U16 cht_xroi_start;
	U16 cht_yroi_start;
	U16 cht_xroi_end;
	U16 cht_yroi_end;

	//--
	cht_xroi_start = 0;
	cht_yroi_start = 0;
	cht_xroi_end = (U16)(width - 1);
	cht_yroi_end = (U16)(height - 1);

#define CHT0

//#define CHT1

#if defined(CHT0)
	res = (I32)cht_refine_double(
		&circle_x, &circle_y, &circle_r, // outputs of this function
		buf, (U16)width2, (U08)min_r, (U08)max_r,	// input parameters
		cht_xroi_start, cht_yroi_start, cht_xroi_end, cht_yroi_end, // input parameters
		threshold_gradient, threshold_vote);
#endif
#if defined(CHT1)
	uint8_t cht_pc_template_match(uint16_t *circle_x, uint16_t *circle_y, uint8_t *circle_r, // outputs of this function
		uint8_t *image_original, uint16_t xstride,
		uint16_t cht_xroi_start, uint16_t cht_yroi_start, uint16_t cht_xroi_end, uint16_t cht_yroi_end); 

#endif

	if (res) {
		*circle_x_double = circle_x;
		*circle_y_double = circle_y;
		*circle_r_double = circle_r;
	}
	return res;
}