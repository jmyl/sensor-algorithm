/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2019 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_security.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2019/12/20 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
/*
#include <stdlib.h>
#include <stdio.h>
#include <tchar.h>
#include <direct.h>
*/


#include "cr_common.h"
#include "iana_security_implement.h"
#include "iana_security.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
/*!
 ********************************************************************************
 *	@brief      Create IANA security module
 *
 *  @return		hand
 *              IANA security handle
 *
 *	@author	    yhsuk
 *  @date       2019/04/02
 *******************************************************************************/

HAND iana_security_create(HAND hscamif)
{
	iana_security_t *pisc;
	pisc = (iana_security_t *) malloc(sizeof(iana_security_t));
	pisc->hscamif = hscamif;

	return (HAND) pisc;
}

/*!
 ********************************************************************************
 *	@brief      Delete IANA security module
 *
 *  @param[in]	h
 *              IANA security module handle
 *  @return		Terminate result. 
 *
 *	@author	    yhsuk
 *  @date       2019/04/02
 *******************************************************************************/
I32 iana_security_delete(HAND h) 
{
	I32 res;


	res = 1;
	if (h) {
		free(h);
	}

	return res;
}

/*!
 ********************************************************************************
 *	@brief      IANA security module. get hscamif
 *
 *  @return		hand
 *              IANA security handle
 *
 *	@author	    yhsuk
 *  @date       2019/04/02
 *******************************************************************************/
I32 iana_security_hscamif_set(HAND h, HAND hscamif)
{
	I32 res;
	iana_security_t *pisc;
	pisc = (iana_security_t *) h;
	if (pisc) {
		pisc->hscamif = hscamif;
		res = 1;
	} else {
		res = 0;
	}

	return res;
}

I32 iana_security_hscamif_get(HAND h, HAND *phscamif)
{
	I32 res;
	iana_security_t *pisc;
	pisc = (iana_security_t *) h;
	if (pisc) {
		*phscamif = pisc->hscamif;
		res = 1;
	} else {
		res = 0;
	}

	return res;
}


/*!
 ********************************************************************************
 *	@brief      Get pcinfo. (mainboard serial)
 *
 *  @param[in]	h
 *              IANA security module handle
 *  @param[out]	bdserial
 *              Motherboard Seiral String. max 32 bytes.
 *  @return		result.   1: GOOD. 0: FAIL.
 *
 *	@author	    yhsuk
 *  @date       2019/04/02
 *******************************************************************************/
I32 iana_security_pcinfo_mainboard_serial_read(
		HAND h,
		U08 bdserial[32])
{
	I32 res;

	//----
	if (h == NULL) {
		res = 0;
	} 
	if (baseBoardSerialHddReadHash(bdserial, NULL, NULL)) {
		res = 1;
	} else {
		res = 0;
	}

	return res;
}


/*!
 ********************************************************************************
 *	@brief      Get pcinfo. (HDD serial)
 *
 *  @param[in]	h
 *              IANA security module handle
 *  @param[out]	hddserial
 *              1st HDD Seiral String. max 32 bytes.
 *  @return		result.   1: GOOD. 0: FAIL.
 *
 *	@author	    yhsuk
 *  @date       2019/04/02
 *******************************************************************************/
I32 iana_security_pcinfo_hdd_serial_read(
		HAND h,
	U08 hddserial[32])
{
	I32 res;

	//----
	if (h == NULL) {
		res = 0;
	} else {
		if (baseBoardSerialHddReadHash(NULL, hddserial, NULL)) {
			res = 1;
		} else {
			res = 0;
		}
	}

	return res;
}

/*!
 ********************************************************************************
 *	@brief      Make GUID.
 *
 *  @param[in]	h
 *              IANA security module handle
 *  @param[out]	guid. 8bytes
 *
 *  @return		result.   1: GOOD. 0: FAIL.
 *
 *	@author	    yhsuk
 *  @date       2019/04/02
 *******************************************************************************/
I32 iana_security_makeguid(HAND h, U32 guid[4])
{
	OSAL_SYS_GenerateGUID((char *)guid);

	UNUSED(h);
	return 1;
}


/*!
 ********************************************************************************
 *	@brief      Get pcinfo. mainboard serial, HDD serial, HASH code. 
 *
 *  @param[in]	h
 *              IANA security module handle
 *  @param[out]	bdserial
 *              Motherboard Seiral String. max 32 bytes.
 *  @param[out]	hddserial
 *              Motherboard Seiral String. max 32 bytes.
 *  @param[out]	hashcode
 *              HASH CODE. (sha256 + SOME other..) 32 bytes.
 *  @return		result.   1: GOOD. 0: FAIL.
 *
 *	@author	    yhsuk
 *  @date       2019/04/02
 *******************************************************************************/
I32 iana_security_pcinfo_mb_hdd_hash(
	HAND h,
	U08 bdserial[32],
	U08 hddserial[32],
	U08 hashcode[32]
		)
{
	I32 res;
	//----
	if (baseBoardSerialHddReadHash(bdserial, hddserial, hashcode)) {
		res = 1;
	} else {
		res = 0;
	}

	UNUSED(h);
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Read H/W Board (ZCAM) Infomation 
 *
 *  @param[in]	h
 *              IANA security module handle
 *  @param[out]	phbi
 *              Hboard information
 *  @return		result.   1: GOOD. 0: FAIL.
 *
 *	@author	    yhsuk
 *  @date       2019/04/02
 *******************************************************************************/
I32 iana_security_hboardinfo_read(
		HAND h,
		HAND hbi)
{
	I32 res;
	//----
	res = security_hboardinfo_read(h, (U08 *)hbi, NULL);

	return res;
}




/*!
 ********************************************************************************
 *	@brief      Write H/W Board (ZCAM) Infomation 
 *
 *  @param[in]	h
 *              IANA security module handle
 *  @param[in]	phbi
 *              Hboard information
 *  @return		result.   1: GOOD. 0: FAIL.
 *
 *	@author	    yhsuk
 *  @date       2019/04/02
 *******************************************************************************/
I32 iana_security_hboardinfo_write(
		HAND h,
		HAND /*hboardinfo_t	*/phbi)
{
	I32 res;
	//----
	res = security_hboardinfo_write(h, (U08 *)phbi, NULL);

	return res;
}

/*!
 ********************************************************************************
 *	@brief      Read H/W PCINFO (ZCAM) Infomation 
 *
 *  @param[in]	h
 *              IANA security module handle
 *  @param[out]	phbi
 *              pcinfo information
 *  @return		result.   1: GOOD. 0: FAIL.
 *
 *	@author	    yhsuk
 *  @date       2019/04/02
 *******************************************************************************/
I32 iana_security_pcinfo_read(HAND h, void *ppcinfo)
{
	I32 res;
	//----
	res = security_pcinfo_read(h, ppcinfo);

	return res;
}

/*!
 ********************************************************************************
 *	@brief      Write H/W PCINFO (ZCAM) Infomation 
 *
 *  @param[in]	h
 *              IANA security module handle
 *  @param[in]	phbi
 *              pcinfo information
 *  @return		result.   1: GOOD. 0: FAIL.
 *
 *	@author	    yhsuk
 *  @date       2019/04/02
 *******************************************************************************/
I32 iana_security_pcinfo_write(HAND h, void *ppcinfo)
{
	I32 res;
	//----
	res = security_pcinfo_write(h, ppcinfo);

	return res;
}
	

/*!
 ********************************************************************************
 *	@brief      Make PCINFO Infomation 
 *
 *  @param[in]	h
 *              IANA security module handle
 *  @param[out]	phbi
 *              pcinfo information
 *  @return		result.   1: GOOD. 0: FAIL.
 *
 *	@author	    yhsuk
 *  @date       2019/04/09
 *******************************************************************************/

I32 iana_security_pcinfo_make(HAND h, void *ppcinfo)
{
	I32 res;

	//----
	res = security_pcinfo_make(h, ppcinfo);
	return res;
}

								  
								  
								  
/*!
 ********************************************************************************
 *	@brief      Runcode check.
 *
 *  @param[in]	h
 *              IANA security module handle
 *  @param[out]	presult
 *              result.
 *  @return		result.   1: GOOD. 0: FAIL.
 *
 *	@author	    yhsuk
 *  @date       2019/04/09
 *******************************************************************************/
I32 iana_security_runcode_check(HAND h, U32 *presult)
{
	I32 res;

	res = security_runcode_check(h, presult);
	return res;
}
								

/*!
 ********************************************************************************
 *	@brief      Runcode update.
 *
 *  @param[in]	h
 *              IANA security module handle
 *  @param[in]	good1bad0
 *              1; Good, 0: Bad.
 *  @return		result.   1: GOOD. 0: FAIL.
 *
 *	@author	    yhsuk
 *  @date       2019/04/09
 *******************************************************************************/
I32 iana_security_runcode_update(HAND h, U32 good1bad0)
{
	I32 res;
	res = security_runcode_update(h, good1bad0);
	return res;
}





/*!
 ********************************************************************************
 *	@brief      CPU seiral ID Read.
 *
 *  @param[in]	h
 *              IANA security module handle
 *  @param[out]	cpusid
 *              
 *  @return		result.   1: GOOD. 0: FAIL.
 *
 *	@author	    yhsuk
 *  @date       2019/04/09
 *******************************************************************************/
I32 iana_security_cpusid_read(HAND h, U32 cpusid[4])
{
	I32 res;
	res = security_cpusid_read(h, cpusid);
	return res;
}



/*!
 ********************************************************************************
 *	@brief      CPU Board Serial Num Read
 *
 *  @param[in]	h
 *              IANA security module handle
 *  @param[out]	cpubdserial
 *              
 *  @return		result.   1: GOOD. 0: FAIL.
 *
 *	@author	    yhsuk
 *  @date       2019/0502
 *******************************************************************************/
I32 iana_security_cpubdserial_read(HAND h, U32 cpubdserial[4])
{
	I32 res;
	res = security_cpubdserial_read(h, cpubdserial);
	return res;
}

#if defined (__cplusplus)
}
#endif


