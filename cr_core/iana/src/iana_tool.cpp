/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA tool
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_tool.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2016/02/21 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/


/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/

#include <math.h>
#include <string.h>

#if defined(_WIN32)
//#include <tchar.h>
#include <direct.h>
#endif

#ifdef IPP_SUPPORT
#include <ipp.h>
#else
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#endif

#include "cr_common.h"
#include "cr_thread.h"
#include "cr_math.h"
#include "cr_osapi.h"

#include "scamif.h"
//#include "scamif_main.h"

#include "iana.h"
#include "iana_work.h"
#include "iana_implement.h"
#include "iana_cam.h"
#include "iana_cam_implement.h"
#include "iana_tool.h"
#include "iana_coordinate.h"


#include "cr_cam_jpeg.h"

#if defined (__cplusplus)
extern "C" {
#endif
/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
//#define		CALI_PLATE_TEST	0.05
//#define		CALI_PLATE_TEST	0.005				// <--- !!!!
/*----------------------------------------------------------------------------
 *	Description	: defines datatype
 -----------------------------------------------------------------------------*/

typedef enum _IANA_COLOR_TYPE_t{
	COLOR_TYPE_eGRAY = 0,
	COLOR_TYPE_eBGR,		
} IANA_COLOR_TYPE_t;


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/
#ifdef IPP_SUPPORT
static Ipp32s GammaFwdLUT[] = 
#else
static int GammaFwdLUT[] =
#endif
{
	0, 4, 9, 14, 18,  23, 27, 30, 34, 37,
	40, 43, 46, 48, 51,  53, 55, 58, 60, 62,
	64, 66, 68, 70, 72,  73, 75, 77, 78, 80,
	82, 83, 85, 86, 88,  89, 91, 92, 94, 95,
	97, 98, 99, 101, 102,  103, 104, 106, 107, 108,
	109, 111, 112, 113, 114,  115, 116, 118, 119, 120,
	121, 122, 123, 124, 125,  126, 127, 128, 129, 130,
	131, 132, 133, 134, 135,  136, 137, 138, 139, 140,
	141, 142, 143, 144, 145,  146, 147, 147, 148, 149,
	150, 151, 152, 153, 154,  154, 155, 156, 157, 158,
	159, 159, 160, 161, 162,  163, 164, 164, 165, 166,
	167, 168, 168, 169, 170,  171, 171, 172, 173, 174,
	174, 175, 176, 177, 177,  178, 179, 180, 180, 181,
	182, 182, 183, 184, 185,  185, 186, 187, 187, 188,
	189, 189, 190, 191, 191,  192, 193, 193, 194, 195,
	195, 196, 197, 197, 198,  199, 199, 200, 201, 201,
	202, 203, 203, 204, 205,  205, 206, 206, 207, 208,
	208, 209, 209, 210, 211,  211, 212, 213, 213, 214,
	214, 215, 216, 216, 217,  217, 218, 218, 219, 220,
	220, 221, 221, 222, 223,  223, 224, 224, 225, 225,
	226, 227, 227, 228, 228,  229, 229, 230, 230, 231,
	232, 232, 233, 233, 234,  234, 235, 235, 236, 236,
	237, 238, 238, 239, 239,  240, 240, 241, 241, 242,
	242, 243, 243, 244, 244,  245, 245, 246, 246, 247,
	247, 248, 248, 249, 249,  250, 251, 251, 252, 252,
	253, 253, 254, 254, 255,  255};


#ifdef IPP_SUPPORT
static Ipp32s GammaInvLUT[] = 
#else
static int GammaInvLUT[] =
#endif
{
	0, 0, 0, 1, 1,  1, 1, 2, 2, 2,
	2, 2, 3, 3, 3,  3, 4, 4, 4, 4,
	4, 5, 5, 5, 5,  6, 6, 6, 6, 7,
	7, 7, 7, 8, 8,  8, 9, 9, 9, 10,
	10, 10, 11, 11, 11,  12, 12, 13, 13, 13,
	14, 14, 15, 15, 15,  16, 16, 17, 17, 18,
	18, 19, 19, 20, 20,  21, 21, 22, 22, 23,
	23, 24, 24, 25, 25,  26, 27, 27, 28, 28,
	29, 30, 30, 31, 31,  32, 33, 33, 34, 35,
	35, 36, 37, 37, 38,  39, 40, 40, 41, 42,
	43, 43, 44, 45, 46,  46, 47, 48, 49, 50,
	51, 51, 52, 53, 54,  55, 56, 57, 57, 58,
	59, 60, 61, 62, 63,  64, 65, 66, 67, 68,
	69, 70, 71, 72, 73,  74, 75, 76, 77, 78,
	79, 80, 81, 82, 83,  84, 85, 86, 88, 89,
	90, 91, 92, 93, 94,  96, 97, 98, 99, 100,
	102, 103, 104, 105, 107,  108, 109, 110, 112, 113,
	114, 116, 117, 118, 119,  121, 122, 124, 125, 126,
	128, 129, 130, 132, 133,  135, 136, 138, 139, 140,
	142, 143, 145, 146, 148,  149, 151, 152, 154, 155,
	157, 158, 160, 162, 163,  165, 166, 168, 170, 171,
	173, 174, 176, 178, 179,  181, 183, 184, 186, 188,
	190, 191, 193, 195, 197,  198, 200, 202, 204, 205,
	207, 209, 211, 213, 214,  216, 218, 220, 222, 224,
	226, 228, 229, 231, 233,  235, 237, 239, 241, 243,
	245, 247, 249, 251, 253,  255};


#ifdef IPP_SUPPORT
static Ipp32s GammaLevels[] =
#else
static int GammaLevels[] =
#endif
{
	0, 1, 2, 3, 4,  5, 6, 7, 8, 9,
	10, 11, 12, 13, 14,  15, 16, 17, 18, 19,
	20, 21, 22, 23, 24,  25, 26, 27, 28, 29,
	30, 31, 32, 33, 34,  35, 36, 37, 38, 39,
	40, 41, 42, 43, 44,  45, 46, 47, 48, 49,
	50, 51, 52, 53, 54,  55, 56, 57, 58, 59,
	60, 61, 62, 63, 64,  65, 66, 67, 68, 69,
	70, 71, 72, 73, 74,  75, 76, 77, 78, 79,
	80, 81, 82, 83, 84,  85, 86, 87, 88, 89,
	90, 91, 92, 93, 94,  95, 96, 97, 98, 99,
	100, 101, 102, 103, 104,  105, 106, 107, 108, 109,
	110, 111, 112, 113, 114,  115, 116, 117, 118, 119,
	120, 121, 122, 123, 124,  125, 126, 127, 128, 129,
	130, 131, 132, 133, 134,  135, 136, 137, 138, 139,
	140, 141, 142, 143, 144,  145, 146, 147, 148, 149,
	150, 151, 152, 153, 154,  155, 156, 157, 158, 159,
	160, 161, 162, 163, 164,  165, 166, 167, 168, 169,
	170, 171, 172, 173, 174,  175, 176, 177, 178, 179,
	180, 181, 182, 183, 184,  185, 186, 187, 188, 189,
	190, 191, 192, 193, 194,  195, 196, 197, 198, 199,
	200, 201, 202, 203, 204,  205, 206, 207, 208, 209,
	210, 211, 212, 213, 214,  215, 216, 217, 218, 219,
	220, 221, 222, 223, 224,  225, 226, 227, 228, 229,
	230, 231, 232, 233, 234,  235, 236, 237, 238, 239,
	240, 241, 242, 243, 244,  245, 246, 247, 248, 249,
	250, 251, 252, 253, 254,  255};


/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares function prototype 
 -----------------------------------------------------------------------------*/
I32 iana_L2P2(iana_t *piana, U32 camid, point_t *pLp, point_t *pPp);
I32 iana_LP2P2(iana_t *piana, U32 camid, point_t *pLp, cr_plane_t *pPt, point_t *pPp);
I32 iana_L2P2PP(iana_t *piana, U32 camid, cr_point_t *pLcpPP, point_t *pPp);
I32 iana_L2P_Ud(iana_t *piana, U32 camid, point_t *pLp, point_t *pPp);

I32 iana_P2L2(iana_t *piana, U32 camid, point_t *pPp, point_t *pLp);
I32 iana_P2LP2(iana_t *piana, U32 camid, point_t *pPp, point_t *pLp, cr_plane_t *pPt);
I32 iana_P2L2PP(iana_t *piana, U32 camid, point_t *pPp, cr_point_t *pLcpPP); 
I32 iana_P2L_Ud(iana_t *piana, U32 camid, point_t *pLp, point_t *pPp);


U32 iana_Ptr2TargetPlanePtr(
		cr_point_t *pPtr,			// Point (in)
		cr_point_t *pPtrTarget,		// Point on the Target
		cr_point_t *pCamPos, 		// Camera position
		cr_plane_t *pPlaneTarget	// Target Plane
		);


/*----------------------------------------------------------------------------
 *	Description	: local functions
 -----------------------------------------------------------------------------*/
inline void mydotgray(I32 x, I32 y, U08 value, U08 *buf, U32 width)
{
	if (x >= 0 && x < (I32)width && y >= 0) {
		MYDOTGRAY(x, y, value, buf, width);
	}
}

inline void mydotBGR(int x, int y, U32 bgrvalue, U08 *buf, int width)
{
	int lpos;
	U08 b, g, r;

	if (x >= 0 && x < width && y >= 0) {
		b = (U08)((bgrvalue >> 16)& 0xFF);
		g = (U08)((bgrvalue >>  8)& 0xFF);
		r = (U08)((bgrvalue >>  0)& 0xFF);

		lpos = x + y * width;

		*(U08 *)(buf + lpos * 3 + 0) = b;
		*(U08 *)(buf + lpos * 3 + 1) = g;
		*(U08 *)(buf + lpos * 3 + 2) = r;
	}
}


static void DrawLine(int x1, int y1, int x2, int y2, U32 value, U08 *buf, int width, IANA_COLOR_TYPE_t color_mode)
{
	int      dx, dy;
	int      p_value;
	int      inc_2dy;
	int      inc_2dydx;
	int      inc_value;
	int      ndx;

	//---
	if (x1 < 0) x1 = 0;//  if (x1 >= width) x1 = width-1;
	if (x2 < 0) x2 = 0;//  if (x2 >= width) x2 = width-1;
	if (y1 < 0) y1 = 0;
	if (y2 < 0) y2 = 0;

	dx       = abs( x2 -x1);
	dy       = abs( y2 -y1);

	if ( dy <= dx) {
		inc_2dy     = 2 * dy;
		inc_2dydx   = 2 * ( dy - dx);

		if ( x2 < x1) {
			ndx   = x1;
			x1    = x2;
			x2    = ndx;

			ndx   = y1;
			y1    = y2;
			y2    = ndx;
		}
		if ( y1 < y2)  {
			inc_value   = 1;
		} else {          
			inc_value   = -1;
		}

		if(color_mode == COLOR_TYPE_eBGR) {
			mydotBGR(x1,y1,value,buf,width);
		} else { /* Gray */
			mydotgray(x1,y1,(U08)value,buf,width);
		}

		p_value  = 2 * dy - dx;    
		//for (ndx = x1; ndx < x2; ndx++)
		for (ndx = x1; ndx <= x2; ndx++)
		{
			if ( 0 > p_value)  {  
				p_value  += inc_2dy;
			} else 	{
				p_value  += inc_2dydx;
				y1       += inc_value;
			}
			
			if(color_mode == COLOR_TYPE_eBGR) {
				mydotBGR(ndx,y1,value,buf,width);
			} else { /* Gray */
				mydotgray(ndx,y1,(U08)value,buf,width);
			}
		}
	}
	else 	{
		inc_2dy     = 2 * dx;
		inc_2dydx   = 2 * ( dx - dy);

		if ( y2 < y1) {
			ndx   = y1;
			y1    = y2;
			y2    = ndx;

			ndx   = x1;
			x1    = x2;
			x2    = ndx;
		}
		if ( x1 < x2)  {
			inc_value   = 1;
		} else  {          
			inc_value   = -1;
		}
		
		if(color_mode == COLOR_TYPE_eBGR) {
			mydotBGR(x1,y1,value,buf,width);
		} else { /* Gray */
			mydotgray(x1,y1,(U08)value,buf,width);
		}

		p_value  = 2 * dx - dy;    
		//for (ndx = y1; ndx < y2; ndx++)
		for (ndx = y1; ndx <= y2; ndx++)
		{
			if ( 0 > p_value) {   
				p_value  += inc_2dy;
			} else 	{
				p_value  += inc_2dydx;
				x1       += inc_value;
			}
			
			if(color_mode == COLOR_TYPE_eBGR) {
				mydotBGR(x1,ndx,value,buf,width);
			} else { /* Gray */
				mydotgray(x1,ndx,(U08)value,buf,width);
			}
		}
	}
}  

static void DrawLineH(int x1, int y1, int x2, int y2, U32 value, U08 *buf, int width, int lineh, IANA_COLOR_TYPE_t color_mode)
{
	int     dx, dy;
	int     p_value;
	int     inc_2dy;
	int     inc_2dydx;
	int     inc_value;
	int     ndx;
	int		i;
	int		hh;

	//---
	hh = lineh/2;
	if (x1 < hh) x1 = hh; //  if (x1 >= width) x1 = width-1;
	if (x2 < hh) x2 = hh; //  if (x2 >= width) x2 = width-1;
	if (y1 < 0) y1 = 0;
	if (y2 < 0) y2 = 0;

	dx       = abs( x2 -x1);
	dy       = abs( y2 -y1);

	if ( dy <= dx) 	{
		inc_2dy     = 2 * dy;
		inc_2dydx   = 2 * ( dy - dx);

		if ( x2 < x1) {
			ndx   = x1;
			x1    = x2;
			x2    = ndx;

			ndx   = y1;
			y1    = y2;
			y2    = ndx;
		}
		if ( y1 < y2)  {
			inc_value   = 1;
		} else {           
			inc_value   = -1;
		}

		if(color_mode == COLOR_TYPE_eBGR) {
			for (i = -hh; i <= hh; i++) {
				mydotBGR(x1+i,y1,value,buf,width);
			}
		} else { /* Gray */
			for (i = -hh; i <= hh; i++) {
				mydotgray(x1+i,y1,(U08)value,buf,width);
			}
		}


		p_value  = 2 * dy - dx;    

		for (ndx = x1; ndx <= x2; ndx++)
		{
			if ( 0 > p_value)    {
				p_value  += inc_2dy;
			} else {
				p_value  += inc_2dydx;
				y1       += inc_value;
			}
			if(color_mode == COLOR_TYPE_eBGR) {
				for (i = -hh; i <= hh; i++) {
					mydotBGR(ndx+i,y1,value,buf,width);
				}
			} else { /* Gray */
				for (i = -hh; i <= hh; i++) {
					mydotgray(ndx+i,y1,(U08)value,buf,width);
				}
			}
		}
	}
	else
	{
		inc_2dy     = 2 * dx;
		inc_2dydx   = 2 * ( dx - dy);

		if ( y2 < y1) {
			ndx   = y1;
			y1    = y2;
			y2    = ndx;

			ndx   = x1;
			x1    = x2;
			x2    = ndx;
		}
		if ( x1 < x2)  {
			inc_value   = 1;
		} else {           
			inc_value   = -1;
		}

		if(color_mode == COLOR_TYPE_eBGR) {
			for (i = -hh; i <= hh; i++) {
				mydotBGR(x1+i,y1,value,buf,width);
			}
		} else { /* Gray */
			for (i = -hh; i <= hh; i++) {
				mydotgray(x1+i,y1,(U08)value,buf,width);
			}
		}
		p_value  = 2 * dx - dy;    
		for (ndx = y1; ndx <= y2; ndx++)
		{
			if ( 0 > p_value)    {
				p_value  += inc_2dy;
			} else 	{
				p_value  += inc_2dydx;
				x1       += inc_value;
			}
			if(color_mode == COLOR_TYPE_eBGR) {
				//mydotBGR(x1,ndx,value,buf,width);
				for (i = -hh; i <= hh; i++) {
					mydotBGR(x1+i,ndx,value,buf,width);
				}
			} else { /* Gray */
				mydotgray(x1,ndx,(U08)value,buf,width);
				for (i = -hh; i <= hh; i++) {
					mydotgray(x1+i,ndx,(U08)value,buf,width);
				}
			}
		}
	}
}  


static void DrawLineV(int x1, int y1, int x2, int y2, U32 value, U08 *buf, int width, int linev, IANA_COLOR_TYPE_t color_mode)
{
	int     dx, dy;
	int     p_value;
	int     inc_2dy;
	int     inc_2dydx;
	int     inc_value;
	int     ndx;
	int		i;
	int		hv;

	//---
	hv = linev/2;
	if (x1 < 0) x1 = 0; //  if (x1 >= width) x1 = width-1;
	if (x2 < 0) x2 = 0; //  if (x2 >= width) x2 = width-1;
	if (y1 < hv) y1 = hv;
	if (y2 < hv) y2 = hv;

	dx       = abs( x2 -x1);
	dy       = abs( y2 -y1);

	if ( dy <= dx)
	{
		inc_2dy     = 2 * dy;
		inc_2dydx   = 2 * ( dy - dx);

		if ( x2 < x1) {
			ndx   = x1;
			x1    = x2;
			x2    = ndx;

			ndx   = y1;
			y1    = y2;
			y2    = ndx;
		}
		if ( y1 < y2) { 
			inc_value   = 1;
		} else {           
			inc_value   = -1;
		}

		if(color_mode == COLOR_TYPE_eBGR) {
			for (i = -hv; i <= hv; i++) {
				mydotBGR(x1,y1+i,value,buf,width);
			}
		} else { /* Gray */
			for (i = -hv; i <= hv; i++) {
				mydotgray(x1,y1+i,(U08)value,buf,width);
			}
		}


		p_value  = 2 * dy - dx;    
		for (ndx = x1; ndx <= x2; ndx++)
		{
			if ( 0 > p_value)    {
				p_value  += inc_2dy;
			} else {
				p_value  += inc_2dydx;
				y1       += inc_value;
			}

			if(color_mode == COLOR_TYPE_eBGR) {
				for (i = -hv; i <= hv; i++) {
					mydotBGR(ndx,y1+i,value,buf,width);
				}
			} else { /* Gray */
				for (i = -hv; i <= hv; i++) {
					mydotgray(ndx,y1+i,(U08)value,buf,width);
				}
			}
		}
	}
	else
	{
		inc_2dy     = 2 * dx;
		inc_2dydx   = 2 * ( dx - dy);

		if ( y2 < y1) {
			ndx   = y1;
			y1    = y2;
			y2    = ndx;

			ndx   = x1;
			x1    = x2;
			x2    = ndx;
		}
		if ( x1 < x2)  {
			inc_value   = 1;
		} else {           
			inc_value   = -1;
		}
		if(color_mode == COLOR_TYPE_eBGR) {
			for (i = -hv; i <= hv; i++) {
				mydotBGR(x1,y1+i,value,buf,width);
			}
		} else { /* Gray */
			for (i = -hv; i <= hv; i++) {
				mydotgray(x1,y1+i,(U08)value,buf,width);
			}
		}
		p_value  = 2 * dx - dy;    
		for (ndx = y1; ndx <= y2; ndx++)
		{
			if ( 0 > p_value)    {
				p_value  += inc_2dy;
			} else {
				p_value  += inc_2dydx;
				x1       += inc_value;
			}
			if(color_mode == COLOR_TYPE_eBGR) {
				//mydotBGR(x1,ndx,bgrvalue,buf,width);
				for (i = -hv; i <= hv; i++) {
					mydotBGR(x1,ndx+i,value,buf,width);
				}
			} else { /* Gray */
				mydotgray(x1,ndx,(U08)value,buf,width);
				for (i = -hv; i <= hv; i++) {
					mydotgray(x1,ndx+i,(U08)value,buf,width);
				}
			}
		}
	}
}  





void mycircle(int xc, int yc, int r, int rgb, U08 *buf, int width)
{
	int x, y, Pk;
	U08 rr, gg, bb;

	//--

	bb = (U08) ((rgb >> 0) & 0xFF);
	gg = (U08) ((rgb >> 8) & 0xFF);
	rr = (U08) ((rgb >>16) & 0xFF);

	x=0;
	y=r;
	Pk=1-r;

	mycirclepixel(xc,yc,x,y, rr, gg, bb, buf, width);
	
	while(x<y) {
		if(Pk<0) {
			x++;
			Pk=Pk+(2*x)+1;
		} else {
			x++;
			y--;
			Pk=Pk+(2*x)-(2*y)+1;
		}
		mycirclepixel(xc,yc,x,y, rr, gg, bb, buf, width);
	}
}

void mycirclepixel(int xc,int yc,int x,int y, U08 rr, U08 gg, U08 bb, U08 *buf, int width)
{
	mycircleputpixel(xc+x,yc+y,rr, gg, bb, buf, width);
	mycircleputpixel(xc+y,yc+x,rr, gg, bb, buf, width);
	mycircleputpixel(xc-y,yc+x,rr, gg, bb, buf, width);
	mycircleputpixel(xc-x,yc+y,rr, gg, bb, buf, width);
	mycircleputpixel(xc-x,yc-y,rr, gg, bb, buf, width);
	mycircleputpixel(xc-y,yc-x,rr, gg, bb, buf, width);
	mycircleputpixel(xc+y,yc-x,rr, gg, bb, buf, width);
	mycircleputpixel(xc+x,yc-y,rr, gg, bb, buf, width);
}

void mycircleputpixel(int x,int y, U08 rr, U08 gg, U08 bb, U08 *buf, int width)
{
	U08 *ptr = (buf + y * width * 3 + x * 3);

	*ptr++ = rr;
	*ptr++ = gg;
	*ptr++ = bb;
}


void mycircleGray(int xc, int yc, int r, U08 value, U08 *buf, int width)
{
	int x, y, Pk;

	//--

	x = 0;
	y = r;
	Pk = 1 - r;

	mycirclepixelGray(xc, yc, x, y, value, buf, width);

	while (x<y) {
		if (Pk<0) {
			x++;
			Pk = Pk + (2 * x) + 1;
		}
		else {
			x++;
			y--;
			Pk = Pk + (2 * x) - (2 * y) + 1;
		}
		mycirclepixelGray(xc, yc, x, y, value, buf, width);
	}
}


void mycircleGrayInv(int xc, int yc, int r, U08 *buf, int width)
{
	int x, y, Pk;

	//--

	x = 0;
	y = r;
	Pk = 1 - r;

	mycirclepixelGrayInv(xc, yc, x, y, buf, width);

	while (x<y) {
		if (Pk<0) {
			x++;
			Pk = Pk + (2 * x) + 1;
		}
		else {
			x++;
			y--;
			Pk = Pk + (2 * x) - (2 * y) + 1;
		}
		mycirclepixelGrayInv(xc, yc, x, y, buf, width);
	}
}




void mycirclepixelGray(int xc, int yc, int x, int y, U08 value, U08 *buf, int width)
{
	mycircleputpixelGray(xc + x, yc + y, value, buf, width);
	mycircleputpixelGray(xc + y, yc + x, value, buf, width);
	mycircleputpixelGray(xc - y, yc + x, value, buf, width);
	mycircleputpixelGray(xc - x, yc + y, value, buf, width);
	mycircleputpixelGray(xc - x, yc - y, value, buf, width);
	mycircleputpixelGray(xc - y, yc - x, value, buf, width);
	mycircleputpixelGray(xc + y, yc - x, value, buf, width);
	mycircleputpixelGray(xc + x, yc - y, value, buf, width);
}

void mycirclepixelGrayInv(int xc, int yc, int x, int y, U08 *buf, int width)
{
	mycircleputpixelGrayInv(xc + x, yc + y, buf, width);
	mycircleputpixelGrayInv(xc + y, yc + x, buf, width);
	mycircleputpixelGrayInv(xc - y, yc + x, buf, width);
	mycircleputpixelGrayInv(xc - x, yc + y, buf, width);
	mycircleputpixelGrayInv(xc - x, yc - y, buf, width);
	mycircleputpixelGrayInv(xc - y, yc - x, buf, width);
	mycircleputpixelGrayInv(xc + y, yc - x, buf, width);
	mycircleputpixelGrayInv(xc + x, yc - y, buf, width);
}





void mycircleputpixelGray(int x, int y, U08 value, U08 *buf, int width)
{
#if defined(_WIN32)
	__try {
#endif	
		if (x < 0 || y < 0 || x > width) {
		} else {
			U08 *ptr = (buf + y * width + x);

			*ptr = value;
		}
#if defined(_WIN32)		
	} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
				|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
				|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
				|| GetExceptionCode()== EXCEPTION_FLT_DIVIDE_BY_ZERO
				){
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"(%d) : __except !!!\n", __LINE__);
	}
#endif
}

void mycircleputpixelGrayInv(int x, int y, U08 *buf, int width)
{
	I32 itmp;
#if defined(_WIN32)	
	__try {
#endif	
		if (x < 0 || y < 0 || x > width) {
		} else {
			U08 *ptr = (buf + y * width + x);

			itmp = *ptr;
			itmp = 0xFF - itmp;
			*ptr = (U08) itmp;
		}
#if defined(_WIN32)		
	} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
				|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
				|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
				|| GetExceptionCode()== EXCEPTION_FLT_DIVIDE_BY_ZERO
				){
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"(%d) : __except !!!\n", __LINE__);
	}
#endif
}


void mycircleGray2(int xc, int yc, int r, U08 value, U08 *buf, int width)
{
	int x, y, Pk;

	//--

	x = 0;
	y = r;
	Pk = 1 - r;

	mycirclepixelGray2(xc, yc, x, y, r, value, buf, width);

	while (x<y) {
		if (Pk<0) {
			x++;
			Pk = Pk + (2 * x) + 1;
		}
		else {
			x++;
			y--;
			Pk = Pk + (2 * x) - (2 * y) + 1;
		}
		mycirclepixelGray2(xc, yc, x, y, r, value, buf, width);
	}
}



// NOUSE
void mycircleGray3(int xc, int yc, int r0, int r1, U08 value, U08 *buf, int width)
{
	int i, j;
	int x0, y0;

	//--
	mycircleGray2(xc, yc, r0, value, buf, width);

	for (i = -r1; i <= -r0; i++) {
		for (j = -r1; j <= r1; j++) {
			x0 = xc+i;
			y0 = yc+j;
			if (x0 >= 0 &&  x0 < width && y0 >= 0) {
				mycircleputpixelGray(x0, y0, value, buf, width);
			}
			//mycircleputpixelGray(xc+i,yc+j, value, buf, width);
		}
	}

	for (i = -r0; i <= r0; i++) {
		for (j = r0; j <= r1; j++) {

			x0 = xc+i;
			y0 = yc+j;
			if (x0 >= 0 &&  x0 < width && y0 >= 0) {
				mycircleputpixelGray(x0, y0, value, buf, width);
			}
			//mycircleputpixelGray(xc+i,yc+j, value, buf, width);
		}
	}

	for (i = r0; i <= r1; i++) {
		for (j = -r1; j <= r1; j++) {
			x0 = xc+i;
			y0 = yc+j;
			if (x0 >= 0 &&  x0 < width && y0 >= 0) {
				mycircleputpixelGray(x0, y0, value, buf, width);
			}
			//mycircleputpixelGray(xc+i,yc+j, value, buf, width);
			//mycircleputpixelGray(xc+i,yc+j, value, buf, width);
		}
	}

	for (i = -r0; i <= r0; i++) {
		for (j = -r1; j <= -r0; j++) {
			x0 = xc+i;
			y0 = yc+j;
			if (x0 >= 0 &&  x0 < width && y0 >= 0) {
				mycircleputpixelGray(x0, y0, value, buf, width);
				//mycircleputpixelGray(xc+i,yc+j, value, buf, width);
			}
		}
	}

}



void mycirclepixelGray2(int xc, int yc, int x, int y, int r, U08 value, U08 *buf, int width)
{
	mycircleputpixelGray2(xc + x, yc, + y, r, value, buf, width);
	mycircleputpixelGray2(xc + y, yc, + x, r, value, buf, width);
	mycircleputpixelGray2(xc - y, yc, + x, r, value, buf, width);
	mycircleputpixelGray2(xc - x, yc, + y, r, value, buf, width);
	mycircleputpixelGray2(xc - x, yc, - y, r, value, buf, width);
	mycircleputpixelGray2(xc - y, yc, - x, r, value, buf, width);
	mycircleputpixelGray2(xc + y, yc, - x, r, value, buf, width);
	mycircleputpixelGray2(xc + x, yc, - y, r, value, buf, width);
}
void mycircleputpixelGray2(int x, int yc, int y, int r, U08 value, U08 *buf, int width)
{
	int i;
	U08 *ptr;
#if defined(_WIN32)
	__try {
#endif	
		ptr = (buf + y * width + x);

		if (y >= 0) {
			for (i = y; i <= r; i++) {
				ptr = (buf + (yc + i) * width + x);
				*ptr = value;
			}
		} else {
			for (i = y; i >= -r; i--) {
				ptr = (buf + (yc + i) * width + x);
				*ptr = value;
			}
		}
#if defined(_WIN32)		
	} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
				|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
				|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
				|| GetExceptionCode()== EXCEPTION_FLT_DIVIDE_BY_ZERO
				){
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"(%d) : __except !!!\n", __LINE__);
	}
#endif
}

void mycircleGray4(int xc, int yc, int r, U08 value, U08 *buf, int width)
{
	int x, y, Pk;

	//--

	x = 0;
	y = r;
	Pk = 1 - r;
	if (xc < r || xc > width - r)
	{
		return;
	}
	if (yc < r)
	{
		return;
	}

	mycirclepixelGray4(xc, yc, x, y, value, buf, width);

	while (x<y) {
		if (Pk<0) {
			x++;
			Pk = Pk + (2 * x) + 1;
		}
		else {
			x++;
			y--;
			Pk = Pk + (2 * x) - (2 * y) + 1;
		}
		mycirclepixelGray4(xc, yc, x, y, value, buf, width);
	}
}

void mycirclepixelGray4(int xc, int yc, int x, int y, U08 value, U08 *buf, int width)
{
	mycircleputpixelGray4(xc + x, yc, + y, value, buf, width);
	mycircleputpixelGray4(xc + y, yc, + x, value, buf, width);
	mycircleputpixelGray4(xc - y, yc, + x, value, buf, width);
	mycircleputpixelGray4(xc - x, yc, + y, value, buf, width);
	mycircleputpixelGray4(xc - x, yc, - y, value, buf, width);
	mycircleputpixelGray4(xc - y, yc, - x, value, buf, width);
	mycircleputpixelGray4(xc + y, yc, - x, value, buf, width);
	mycircleputpixelGray4(xc + x, yc, - y, value, buf, width);
}
void mycircleputpixelGray4(int x, int yc, int y, U08 value, U08 *buf, int width)
{
	int i;
	U08 *ptr;
#if defined(_WIN32)
	__try {
#endif	
		ptr = (buf + y * width + x);

		if (y >= 0) {
			for (i = 0; i <= y; i++) {
				ptr = (buf + (yc + i) * width + x);
				*ptr = value;
			}
		} else {
			for (i = 0; i >= y; i--) {
				ptr = (buf + (yc + i) * width + x);
				*ptr = value;
			}
		}
#if defined(_WIN32)		
	} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
			|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
			|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
			|| GetExceptionCode()== EXCEPTION_FLT_DIVIDE_BY_ZERO
			){
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"(%d) : __except !!!\n", __LINE__);
	}
#endif
}


void myrectpixelGray(int sx, int sy, int ex, int ey, U08 value, U08 *buf, int width)
{
	int x, y;
#if defined(_WIN32)
	__try {
#endif	
		for (x = sx; x <= ex; x++) {
			*(buf + sy* width + x) = value;
			*(buf + ey* width + x) = value;
		}

		for (y = sy; y <= ey; y++) {
			*(buf + y* width + sx) = value;
			*(buf + y* width + ex) = value;
		}
#if defined(_WIN32)		
	} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
				|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
				|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
				|| GetExceptionCode()== EXCEPTION_FLT_DIVIDE_BY_ZERO
				){
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"(%d) : __except !!!\n", __LINE__);
	}
#endif
}


void mylineGray(int x1, int y1, int x2, int y2, U08 value, U08 *buf, int width)
{
	DrawLine(x1, y1, x2, y2, value, buf, width, COLOR_TYPE_eGRAY);
}  

void mylineGrayH(int x1, int y1, int x2, int y2, U08 value, U08 *buf, int width, int lineh)
{
	DrawLineH(x1, y1, x2, y2, value, buf, width, lineh, COLOR_TYPE_eGRAY);
}  


void mylineGrayV(int x1, int y1, int x2, int y2, U08 value, U08 *buf, int width, int linev)
{
	DrawLineV(x1, y1, x2, y2, value, buf, width, linev, COLOR_TYPE_eGRAY);
}  


void myrectGray(int x1, int y1, int x2, int y2, U08 value, U08 *buf, int width)
{
	/*
	mylineGray(x1, y1, x1, y2, value, buf, width);
	mylineGray(x1, y1, x2, y1, value, buf, width);
	mylineGray(x2, y2, x1, y2, value, buf, width);
	mylineGray(x2, y2, x2, y1, value, buf, width);
	*/
//	int i;
	int x[4], y[4];

	x[0] = x1; y[0] = y1;
	x[1] = x2; y[1] = y1;
	x[2] = x2; y[2] = y2;
	x[3] = x1; y[3] = y2;

	mydrawpolygonGray(x, y, 4, value, buf, width);
}

void mydrawpolylineGray(int x[], int y[], int pointcount, U08 value, U08 *buf, int width)
{
	int i;

	if (pointcount > 1) {
		for (i = 0; i < pointcount-1; i++) {
			mylineGray(x[i], y[i], x[i+1], y[i+1], value, buf, width);
		}
	}
}

void mydrawpolygonGray(int x[], int y[], int pointcount, U08 value, U08 *buf, int width)
{
	int i;

	if (pointcount > 1) {
		for (i = 0; i < pointcount-1; i++) {
			mylineGray(x[i], y[i], x[i+1], y[i+1], value, buf, width);
		}
		mylineGray(x[pointcount-1], y[pointcount-1], x[0], y[0], value, buf, width);
	}
}



void mycircleBGR(int xc, int yc, int r, U32 bgrvalue, U08 *buf, int width)
{
	int x, y, Pk;

	//--

	x = 0;
	y = r;
	Pk = 1 - r;

	mycirclepixelBGR(xc, yc, x, y, bgrvalue, buf, width);

	while (x<y) {
		if (Pk<0) {
			x++;
			Pk = Pk + (2 * x) + 1;
		}
		else {
			x++;
			y--;
			Pk = Pk + (2 * x) - (2 * y) + 1;
		}
		mycirclepixelBGR(xc, yc, x, y, bgrvalue, buf, width);
	}
}




void mycircleBGRInv(int xc, int yc, int r, U08 *buf, int width)
{
	int x, y, Pk;

	//--

	x = 0;
	y = r;
	Pk = 1 - r;

	mycirclepixelBGRInv(xc, yc, x, y, buf, width);

	while (x<y) {
		if (Pk<0) {
			x++;
			Pk = Pk + (2 * x) + 1;
		}
		else {
			x++;
			y--;
			Pk = Pk + (2 * x) - (2 * y) + 1;
		}
		mycirclepixelBGRInv(xc, yc, x, y, buf, width);
	}
}




void mycirclepixelBGR(int xc, int yc, int x, int y, U32 bgrvalue, U08 *buf, int width)
{
	mycircleputpixelBGR(xc + x, yc + y, bgrvalue, buf, width);
	mycircleputpixelBGR(xc + y, yc + x, bgrvalue, buf, width);
	mycircleputpixelBGR(xc - y, yc + x, bgrvalue, buf, width);
	mycircleputpixelBGR(xc - x, yc + y, bgrvalue, buf, width);
	mycircleputpixelBGR(xc - x, yc - y, bgrvalue, buf, width);
	mycircleputpixelBGR(xc - y, yc - x, bgrvalue, buf, width);
	mycircleputpixelBGR(xc + y, yc - x, bgrvalue, buf, width);
	mycircleputpixelBGR(xc + x, yc - y, bgrvalue, buf, width);
}

void mycirclepixelBGRInv(int xc, int yc, int x, int y, U08 *buf, int width)
{
	mycircleputpixelBGRInv(xc + x, yc + y, buf, width);
	mycircleputpixelBGRInv(xc + y, yc + x, buf, width);
	mycircleputpixelBGRInv(xc - y, yc + x, buf, width);
	mycircleputpixelBGRInv(xc - x, yc + y, buf, width);
	mycircleputpixelBGRInv(xc - x, yc - y, buf, width);
	mycircleputpixelBGRInv(xc - y, yc - x, buf, width);
	mycircleputpixelBGRInv(xc + y, yc - x, buf, width);
	mycircleputpixelBGRInv(xc + x, yc - y, buf, width);
}





void mycircleputpixelBGR(int x, int y, U32 bgrvalue, U08 *buf, int width)
{
	U08 r, g, b;
#if defined(_WIN32)	
	__try {
#endif	
		if (x < 0 || y < 0 || x > width) {
		} else {
			U08 *ptr;
			//U08 *ptr = (buf + y * width + x);
			b = (U08) ((bgrvalue >> 16) & 0xFF);
			g = (U08) ((bgrvalue >>  8) & 0xFF);
			r = (U08) ((bgrvalue >>  0) & 0xFF);

			ptr = buf + (y * width + x) * 3;

			*(ptr + 0) = b;
			*(ptr + 1) = g;
			*(ptr + 2) = r;
		}
#if defined(_WIN32)		
	} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
			|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
			|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
			|| GetExceptionCode()== EXCEPTION_FLT_DIVIDE_BY_ZERO
			){
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"(%d) : __except !!!\n", __LINE__);
	}
#endif
}

void mycircleputpixelBGRInv(int x, int y, U08 *buf, int width)
{
	U08 r, g, b;
#if defined(_WIN32)	
	__try{
#endif	
		if (x < 0 || y < 0 || x > width) {
		} else {
			U08 *ptr;
			ptr = buf + (y * width + x) * 3;

			b = *(ptr + 0);
			g = *(ptr + 1);
			r = *(ptr + 2);

			b = 0xFF - b;
			g = 0xFF - g;
			r = 0xFF - r;

			*(ptr + 0) = b;
			*(ptr + 1) = g;
			*(ptr + 2) = r;
		}
#if defined(_WIN32)		
	} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
				|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
				|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
				|| GetExceptionCode()== EXCEPTION_FLT_DIVIDE_BY_ZERO
				){
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"(%d) : __except !!!\n", __LINE__);
	}
#endif
}

// NOUSE
void mycircleBGR2(int xc, int yc, int r, U32 bgrvalue, U08 *buf, int width)
{
	int x, y, Pk;

	//--

	x = 0;
	y = r;
	Pk = 1 - r;

	mycirclepixelBGR2(xc, yc, x, y, r, bgrvalue, buf, width);

	while (x<y) {
		if (Pk<0) {
			x++;
			Pk = Pk + (2 * x) + 1;
		}
		else {
			x++;
			y--;
			Pk = Pk + (2 * x) - (2 * y) + 1;
		}
		mycirclepixelBGR2(xc, yc, x, y, r, bgrvalue, buf, width);
	}
}



// NOUSE
void mycircleBGR3(int xc, int yc, int r0, int r1, U32 bgrvalue, U08 *buf, int width)
{
	int i, j;

	//--
	mycircleBGR2(xc, yc, r0, bgrvalue, buf, width);

	for (i = -r1; i <= -r0; i++) {
		for (j = -r1; j <= r1; j++) {
			mycircleputpixelBGR(xc+i,yc+j, bgrvalue, buf, width);
		}
	}

	for (i = -r0; i <= r0; i++) {
		for (j = r0; j <= r1; j++) {
			mycircleputpixelBGR(xc+i,yc+j, bgrvalue, buf, width);
		}
	}

	for (i = r0; i <= r1; i++) {
		for (j = -r1; j <= r1; j++) {
			mycircleputpixelBGR(xc+i,yc+j, bgrvalue, buf, width);
		}
	}

	for (i = -r0; i <= r0; i++) {
		for (j = -r1; j <= -r0; j++) {
			mycircleputpixelBGR(xc+i,yc+j, bgrvalue, buf, width);
		}
	}

}




// NOUSE
void mycirclepixelBGR2(int xc, int yc, int x, int y, int r, U32 bgrvalue, U08 *buf, int width)
{
	mycircleputpixelBGR2(xc + x, yc, + y, r, bgrvalue, buf, width);
	mycircleputpixelBGR2(xc + y, yc, + x, r, bgrvalue, buf, width);
	mycircleputpixelBGR2(xc - y, yc, + x, r, bgrvalue, buf, width);
	mycircleputpixelBGR2(xc - x, yc, + y, r, bgrvalue, buf, width);
	mycircleputpixelBGR2(xc - x, yc, - y, r, bgrvalue, buf, width);
	mycircleputpixelBGR2(xc - y, yc, - x, r, bgrvalue, buf, width);
	mycircleputpixelBGR2(xc + y, yc, - x, r, bgrvalue, buf, width);
	mycircleputpixelBGR2(xc + x, yc, - y, r, bgrvalue, buf, width);
}
// NOUSE
void mycircleputpixelBGR2(int x, int yc, int y, int r, U32 bgrvalue, U08 *buf, int width)
{
	int i;
	U08 *ptr;
	U08 r_, g_, b_;

	//--
	b_ = (U08) ((bgrvalue >> 16) & 0xFF);
	g_ = (U08) ((bgrvalue >>  8) & 0xFF);
	r_ = (U08) ((bgrvalue >>  0) & 0xFF);
	ptr = (buf + y * width + x);

	if (y >= 0) {
		for (i = y; i <= r; i++) {
//			ptr = (buf + (yc + i) * width + x);
//			*ptr = value;
			ptr = (buf + ((yc + i) * width + x)*3);
			*(ptr + 0) = b_;
			*(ptr + 1) = g_;
			*(ptr + 2) = r_;
		}
	} else {
		for (i = y; i >= -r; i--) {
			//ptr = (buf + (yc + i) * width + x);
			//*ptr = value;

			ptr = (buf + ((yc + i) * width + x)*3);
			*(ptr + 0) = b_;
			*(ptr + 1) = g_;
			*(ptr + 2) = r_;
		}
	}
}

void mycircleBGR4(int xc, int yc, int r, U32 bgrvalue, U08 *buf, int width)
{
	int x, y, Pk;

	//--

	x = 0;
	y = r;
	Pk = 1 - r;

	mycirclepixelBGR4(xc, yc, x, y, bgrvalue, buf, width);

	while (x<y) {
		if (Pk<0) {
			x++;
			Pk = Pk + (2 * x) + 1;
		}
		else {
			x++;
			y--;
			Pk = Pk + (2 * x) - (2 * y) + 1;
		}
		mycirclepixelBGR4(xc, yc, x, y, bgrvalue, buf, width);
	}
}

void mycirclepixelBGR4(int xc, int yc, int x, int y, U32 bgrvalue, U08 *buf, int width)
{
	mycircleputpixelBGR4(xc + x, yc, + y, bgrvalue, buf, width);
	mycircleputpixelBGR4(xc + y, yc, + x, bgrvalue, buf, width);
	mycircleputpixelBGR4(xc - y, yc, + x, bgrvalue, buf, width);
	mycircleputpixelBGR4(xc - x, yc, + y, bgrvalue, buf, width);
	mycircleputpixelBGR4(xc - x, yc, - y, bgrvalue, buf, width);
	mycircleputpixelBGR4(xc - y, yc, - x, bgrvalue, buf, width);
	mycircleputpixelBGR4(xc + y, yc, - x, bgrvalue, buf, width);
	mycircleputpixelBGR4(xc + x, yc, - y, bgrvalue, buf, width);
}

void mycircleputpixelBGR4(int x, int yc, int y, U32 bgrvalue, U08 *buf, int width)
{
	int i;
	U08 *ptr;
	U08 r, g, b;
#if defined(_WIN32)
	__try{
#endif	
		ptr = (buf + (y * width + x)*3);

		b = (U08) ((bgrvalue >> 16) & 0xFF);
		g = (U08) ((bgrvalue >>  8) & 0xFF);
		r = (U08) ((bgrvalue >>  0) & 0xFF);
		if (y >= 0) {
			for (i = 0; i <= y; i++) {
				//			ptr = (buf + (yc + i) * width + x);
				//			*ptr = value;
				ptr = (buf + ((yc + i) * width + x)*3);
				*(ptr + 0) = b;
				*(ptr + 1) = g;
				*(ptr + 2) = r;
			}
		} else {
			for (i = 0; i >= y; i--) {
				//			ptr = (buf + (yc + i) * width + x);
				//			*ptr = value;
				ptr = (buf + ((yc + i) * width + x)*3);
				*(ptr + 0) = b;
				*(ptr + 1) = g;
				*(ptr + 2) = r;
			}
		}
#if defined(_WIN32)		
	} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
			|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
			|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
			|| GetExceptionCode()== EXCEPTION_FLT_DIVIDE_BY_ZERO
			){
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"(%d) : __except !!!\n", __LINE__);
	}
#endif
}

void mycircleBGRAdd4(int xc, int yc, int r, U32 bgrvalue, U08 *buf, int width)
{
	int x, y, Pk;

	//--

	x = 0;
	y = r;
	Pk = 1 - r;

	mycirclepixelBGRAdd4(xc, yc, x, y, bgrvalue, buf, width);

	while (x<y) {
		if (Pk<0) {
			x++;
			Pk = Pk + (2 * x) + 1;
		}
		else {
			x++;
			y--;
			Pk = Pk + (2 * x) - (2 * y) + 1;
		}
		mycirclepixelBGRAdd4(xc, yc, x, y, bgrvalue, buf, width);
	}
}

void mycirclepixelBGRAdd4(int xc, int yc, int x, int y, U32 bgrvalue, U08 *buf, int width)
{
	mycircleputpixelBGRAdd4(xc + x, yc, + y, bgrvalue, buf, width);
	mycircleputpixelBGRAdd4(xc + y, yc, + x, bgrvalue, buf, width);
	mycircleputpixelBGRAdd4(xc - y, yc, + x, bgrvalue, buf, width);
	mycircleputpixelBGRAdd4(xc - x, yc, + y, bgrvalue, buf, width);
	mycircleputpixelBGRAdd4(xc - x, yc, - y, bgrvalue, buf, width);
	mycircleputpixelBGRAdd4(xc - y, yc, - x, bgrvalue, buf, width);
	mycircleputpixelBGRAdd4(xc + y, yc, - x, bgrvalue, buf, width);
	mycircleputpixelBGRAdd4(xc + x, yc, - y, bgrvalue, buf, width);
}

void mycircleputpixelBGRAdd4(int x, int yc, int y, U32 bgrvalue, U08 *buf, int width)
{
	int i;
	U08 *ptr;
	U08 r, g, b;
	U32 ir, ig, ib;
	U32 rgbsum0, rgbsum1;
#if defined(_WIN32)
	__try {
#endif	
		ptr = (buf + (y * width + x)*3);

		b = (U08) ((bgrvalue >> 16) & 0xFF);
		g = (U08) ((bgrvalue >>  8) & 0xFF);
		r = (U08) ((bgrvalue >>  0) & 0xFF);

		if (y >= 0) {
			for (i = 0; i <= y; i++) {
				ptr = (buf + ((yc + i) * width + x)*3);
				ib = *(ptr + 0);
				ig = *(ptr + 1);
				ir = *(ptr + 2);

				rgbsum0 = (ib+ig+ir);
				ib = b + ib;
				ig = g + ig;
				ir = r + ir;

				rgbsum1 = (ib+ig+ir);
				ib = (ib * rgbsum0) / rgbsum1;
				ig = (ig * rgbsum0) / rgbsum1;
				ir = (ir * rgbsum0) / rgbsum1;

				if (ib > 0xFF) ib = 0xFF;
				if (ig > 0xFF) ig = 0xFF;
				if (ir > 0xFF) ir = 0xFF;
				/*
				   ib = b + ib;	if (ib > 0xFF) ib = 0xFF;
				   ig = g + ig;	if (ig > 0xFF) ig = 0xFF;
				   ir = r + ir;	if (ir > 0xFF) ir = 0xFF;
				   */

				*(ptr + 0) = (U08) ib;
				*(ptr + 1) = (U08) ig;
				*(ptr + 2) = (U08) ir;
			}
		} else {
			for (i = 0; i >= y; i--) {
				ptr = (buf + ((yc + i) * width + x)*3);
				ib = *(ptr + 0);
				ig = *(ptr + 1);
				ir = *(ptr + 2);

				rgbsum0 = (ib+ig+ir);
				ib = b + ib;
				ig = g + ig;
				ir = r + ir;

				rgbsum1 = (ib+ig+ir);
				ib = (ib * rgbsum0) / rgbsum1;
				ig = (ig * rgbsum0) / rgbsum1;
				ir = (ir * rgbsum0) / rgbsum1;

				if (ib > 0xFF) ib = 0xFF;
				if (ig > 0xFF) ig = 0xFF;
				if (ir > 0xFF) ir = 0xFF;
				/*
				   ib = b + ib;	if (ib > 0xFF) ib = 0xFF;
				   ig = g + ig;	if (ig > 0xFF) ig = 0xFF;
				   ir = r + ir;	if (ir > 0xFF) ir = 0xFF;
				   */

				*(ptr + 0) = (U08) ib;
				*(ptr + 1) = (U08) ig;
				*(ptr + 2) = (U08) ir;
			}
		}
#if defined(_WIN32)		
	} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
				|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
				|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
				|| GetExceptionCode()== EXCEPTION_FLT_DIVIDE_BY_ZERO
				){
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"(%d) : __except !!!\n", __LINE__);
	}
#endif
}
//ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ


void mylineBGR(int x1, int y1, int x2, int y2, U32 bgrvalue, U08 *buf, int width)
{
	DrawLine(x1, y1, x2, y2, bgrvalue, buf, width, COLOR_TYPE_eBGR);
}  


void mylineBGRH(int x1, int y1, int x2, int y2, U32 bgrvalue, U08 *buf, int width, int lineh)
{ 
	DrawLineH(x1, y1, x2, y2, bgrvalue, buf, width, lineh, COLOR_TYPE_eBGR);
}

void mylineBGRV(int x1, int y1, int x2, int y2, U32 bgrvalue, U08 *buf, int width, int linev)
{
	DrawLineV(x1, y1, x2, y2, bgrvalue, buf, width, linev, COLOR_TYPE_eBGR);
}

void myrectBGR(int x1, int y1, int x2, int y2, U32 bgrvalue, U08 *buf, int width)
{
	/*
	mylineBGR(x1, y1, x1, y2, bgrvalue, buf, width);
	mylineBGR(x1, y1, x2, y1, bgrvalue, buf, width);
	mylineBGR(x2, y2, x1, y2, bgrvalue, buf, width);
	mylineBGR(x2, y2, x2, y1, bgrvalue, buf, width);
	*/

//	int i;
	int x[4], y[4];

	x[0] = x1; y[0] = y1;
	x[1] = x2; y[1] = y1;
	x[2] = x2; y[2] = y2;
	x[3] = x1; y[3] = y2;

	mydrawpolygonBGR(x, y, 4, bgrvalue, buf, width);

}



void mydrawpolylineBGR(int x[], int y[], int pointcount, U32 bgrvalue, U08 *buf, int width)
{
	int i;

	if (pointcount > 1) {
		for (i = 0; i < pointcount-1; i++) {
			mylineBGR(x[i], y[i], x[i+1], y[i+1], bgrvalue, buf, width);
		}
	}
}

void mydrawpolygonBGR(int x[], int y[], int pointcount, U32 bgrvalue, U08 *buf, int width)
{
	int i;

	if (pointcount > 1) {
		for (i = 0; i < pointcount-1; i++) {
			mylineBGR(x[i], y[i], x[i+1], y[i+1], bgrvalue, buf, width);
		}
		mylineBGR(x[pointcount-1], y[pointcount-1], x[0], y[0], bgrvalue, buf, width);
	}
}




//----------------------------------------------------------------------------------------
//void saveimageprepare(char filepath[1024], U32 goodshot, U32 checkme)
void saveimageprepare(char *dirpath, char *filepath, U32 goodshot, U32 checkme)
{
	CR_Time_t st;
	char filepath2[1024];

	//---

	if (g_datapath.mode == 0) {
		cr_mkdir("SCDATA");

		if (checkme) {
#if defined(_WIN32)				
			sprintf(filepath2, "SCDATA\\CS");
#else
			sprintf(filepath2, "SCDATA/CS");
#endif

		} else {
			if (goodshot) {
#if defined(_WIN32)					
				sprintf(filepath2, "SCDATA\\GS");
#else
				sprintf(filepath2, "SCDATA/GS");
#endif

			} else {
#if defined(_WIN32)				
				sprintf(filepath2, "SCDATA\\NG");
#else
				sprintf(filepath2, "SCDATA/NG");
#endif
			}
		}
	} else {
#if defined(_WIN32)	
		sprintf(filepath2, "%s\\SCDATA", dirpath);
#else
		sprintf(filepath2, "%s/SCDATA", dirpath);
#endif
		cr_mkdir(filepath2);

		if (checkme) {
#if defined(_WIN32)				
				sprintf(filepath2, "%s\\%s", dirpath, "SCDATA\\CS");
#else
				sprintf(filepath2, "%s/%s", dirpath, "SCDATA/CS");
#endif

		} else {
			if (goodshot) {
#if defined(_WIN32)					
				sprintf(filepath2, "%s\\%s", dirpath, "SCDATA\\GS");
#else
				sprintf(filepath2, "%s/%s", dirpath, "SCDATA/GS");
#endif

			} else {
#if defined(_WIN32)				
				sprintf(filepath2, "%s\\%s", dirpath, "SCDATA\\NG");
#else
				sprintf(filepath2, "%s/%s", dirpath, "SCDATA/NG");
#endif

			}
		}
	}
	
	OSAL_SYS_GetLocalTime( &st);

	cr_mkdir(filepath2);
#if defined(_WIN32)	
	sprintf(filepath, "%s\\data_%4d%02d%02d_%02d%02d_%02d%02d", filepath2,
#else
	sprintf(filepath, "%s/data_%4d%02d%02d_%02d%02d_%02d%02d", filepath2,
#endif	
		(int) st.wYear,
		(int) st.wMonth,
		(int) st.wDay,
		(int) st.wHour,
		(int) st.wMinute,
		(int) st.wSecond,
		(int) (st.wMilliseconds/10));


	cr_mkdir(filepath);
}

void saveimageBGR(char path[], char *filename, char *buf, int width, int height)
{
	char fn[1024];
#if defined(_WIN32)	
	sprintf(fn, "%s\\%s", path, filename);
#else
	sprintf(fn, "%s/%s", path, filename);
#endif
	bmpout(fn, buf, width, height, 0, 0);
}


void saveimageRGB(char path[], char *filename, char *rgbbuf, int width, int height)
{
	U08 *bgrbuf;
	U08 r, g, b;
	I32 i;

	bgrbuf = (U08 *) malloc(width*height*3);

	for (i = 0; i < width*height; i++) {
		b = *(rgbbuf + i*3 + 0);
		g = *(rgbbuf + i*3 + 1);
		r = *(rgbbuf + i*3 + 2);


		*(bgrbuf + i*3 + 0) = r;
		*(bgrbuf + i*3 + 1) = g;
		*(bgrbuf + i*3 + 2) = b;
	}
	saveimageBGR(path, filename, rgbbuf, width, height);
}

void saveimage(char path[], char *buf, int width, int height, int camid, int cnt)
{
	char fn[1024];
#if defined(_WIN32)		
	sprintf(fn, "%s\\c%d_%04d.bmp", path, camid, cnt);
#else
	sprintf(fn, "%s/c%d_%04d.bmp", path, camid, cnt);
#endif

	bmpout(fn, buf, width, height, 1, 0);
}


void saveimage2(char path[], char *buf, int width, int height, int camid, int cnt)
{
	char fn[1024];
#if defined(_WIN32)		
	sprintf(fn, "%s\\d%d_%04d.bmp", path, camid, cnt);
#else
	sprintf(fn, "%s/d%d_%04d.bmp", path, camid, cnt);
#endif
	bmpout(fn, buf, width, height, 1, 0);
}


void saveimage3(char path[], char prefix[], char *buf, int width, int height, int camid, int cnt,  char *filename)
{
	char fn[1024];
	sprintf(filename, "%s%d_%04d.bmp",prefix, camid, cnt);
#if defined(_WIN32)	
	sprintf(fn, "%s\\%s", path, filename);
#else
	sprintf(fn, "%s/%s", path, filename);
#endif
	bmpout(fn, buf, width, height, 1, 0);
}

void saveimageJpeg(HAND hjpeg, U32 qvalue, char path[], char prefix[], char *buf, int width, int height, int camid, int cnt,  char *filename)
{
	sprintf(filename, "%s%d_%04d.jpg",prefix, camid, cnt);
	saveimageJpeg2(hjpeg, qvalue, path, filename, buf, width, height);
}


void saveimageJpeg2(HAND hjpeg, U32 qvalue, char path[], char filename[], char *buf, int width, int height)
{
#if defined(_WIN32)
	__try {
#endif	
		U08 *jpBuf;
		U32 jpSize;

		//---
		jpBuf = cr_cam_jpeg_Y_encode(	// NULL: Fail, NotNull: Sucess and result buffer ptr
				hjpeg,
				&jpSize,	// Output, Jpeg bitstream length (0: Fail encoding)
				(U08 *)buf,		// Input, Y
				width,		// Image width
				height,		// Image height
				qvalue		// Compression qvalue
				); 

		if (jpBuf != NULL) {
			FILE *fp;
			char fn[1024];

			//---
#if defined(_WIN32)				
			sprintf(fn, "%s\\%s", path, filename);
#else
			sprintf(fn, "%s/%s", path, filename);
#endif
			fp = cr_fopen(fn, "wb");

			if (fp) {
				fwrite(jpBuf, sizeof(unsigned char), jpSize, fp);
				fclose(fp);
			}
		}
#if defined(_WIN32)		
	} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
			|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
			|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
			|| GetExceptionCode()== EXCEPTION_FLT_DIVIDE_BY_ZERO
			){
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"(%d) : __except !!!\n", __LINE__);
	}
#endif
	return;
}




void saveimageJpegRGB(HAND hjpeg, U32 qvalue, char path[], char filename[], char *rgbbuf, int width, int height)
{
#if defined(_WIN32)
	__try {
#endif	
		I32 res;
		U08 *jpBuf;
		U32 jpSize;

		//---
		jpBuf = (U08 *) malloc(1024*1024);
/*
		jpBuf = cr_cam_jpeg_Y_encode(	// NULL: Fail, NotNull: Sucess and result buffer ptr
				hjpeg,
				&jpSize,	// Output, Jpeg bitstream length (0: Fail encoding)
				(U08 *)buf,		// Input, Y
				width,		// Image width
				height,		// Image height
				qvalue		// Compression qvalue
				); 
*/
		res = cr_cam_jpeg_RGB_encode(	// 0: success, -1: fail.
			hjpeg,
			jpBuf,		// Output, Jpeg bitstream
			&jpSize,	// Output, Jpeg bitstream length (0: Fail encoding)
			(U08 *)rgbbuf,	// Input, RGB
			width,		// Image width
			height,		// Image height
			qvalue		// Compression qvalue
		);




		if (jpBuf != NULL) {
			FILE *fp;
			char fn[1024];

			//---
#if defined(_WIN32)				
			sprintf(fn, "%s\\%s", path, filename);
#else
			sprintf(fn, "%s\\%s", path, filename);
#endif
			fp = cr_fopen(fn, "wb");
			if (fp) {
				fwrite(jpBuf, sizeof(unsigned char), jpSize, fp);
				fclose(fp);
			}

			free(jpBuf);
		}
#if defined(_WIN32)
	} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
			|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
			|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
			|| GetExceptionCode()== EXCEPTION_FLT_DIVIDE_BY_ZERO
			){
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"(%d) : __except !!!\n", __LINE__);
	}
#endif
	return;
}

void saveimageJpegBGR(HAND hjpeg, U32 qvalue, char path[], char filename[], char *bgrbuf, int width, int height)
{
	U08 *rgbbuf;
	U08 r, g, b;
	I32 i;

	rgbbuf = (U08 *) malloc(width*height*3);

	for (i = 0; i < width*height; i++) {
		b = *(bgrbuf + i*3 + 0);
		g = *(bgrbuf + i*3 + 1);
		r = *(bgrbuf + i*3 + 2);


		*(rgbbuf + i*3 + 0) = r;
		*(rgbbuf + i*3 + 1) = g;
		*(rgbbuf + i*3 + 2) = b;
	}

	saveimageJpegRGB(hjpeg, qvalue, path, filename, (char *)rgbbuf, width, height);

	free(rgbbuf);
}




void bmpout(char *fn, char *buf, int width, int height, int isgray, int isencrypt)
{
	FILE *fp;

	unsigned int bitcount;
	unsigned int hsize;
	unsigned int bsize;
	unsigned int imagesize;
	BYTE *pbuf;
	BYTE *pimg;

	LPBITMAPINFOHEADER lpbmi;
	BITMAPFILEHEADER* pbmf;
	U32 s0, s1, s2;


	//----
//	creu_trace(CREU_MSG_AREA_GENERAL, CREU_MSG_LEVEL_INFO, "bmpout 1\n");

	if (isgray) {
		bitcount = 8;
		imagesize = height * ((width * bitcount / 8 + 3) & ~3);

		/*
		bsize = sizeof(BITMAPFILEHEADER) 
			+ sizeof(BITMAPINFOHEADER) 
			+ sizeof(RGBQUAD)*(1<<bitcount)
			+ imagesize;
			*/
		//hsize = sizeof(BITMAPFILEHEADER) 
		//	+ sizeof(BITMAPINFOHEADER) 
		//	+ sizeof(RGBQUAD)*(1<<bitcount);

		s0 = sizeof(BITMAPFILEHEADER);
		s1 = sizeof(BITMAPINFOHEADER);
		s2 = sizeof(RGBQUAD);

		hsize = s0 + s1 + s2 * (1<<bitcount);


		bsize = hsize + imagesize;
	} else {
		bitcount = 24;
		imagesize = height * ((width * bitcount / 8 + 3) & ~3);

		/*
		bsize = sizeof(BITMAPFILEHEADER) 
			+ sizeof(BITMAPINFOHEADER) 
			+ imagesize;
			*/
		hsize = sizeof(BITMAPFILEHEADER) 
			+ sizeof(BITMAPINFOHEADER);
		bsize = hsize + imagesize;
	}

	//pbuf = (BYTE *) malloc(bsize);
	pbuf = (BYTE *) malloc(hsize);

//	creu_trace(CREU_MSG_AREA_GENERAL, CREU_MSG_LEVEL_INFO, "bmpout 2\n");
	if (pbuf == NULL) {
		// what?
	}
//	memset(pbuf, 0, bsize);
//	creu_trace(CREU_MSG_AREA_GENERAL, CREU_MSG_LEVEL_INFO, "bmpout 3\n");
	if (isgray) {
		RGBQUAD* pPal = (RGBQUAD*)((BYTE*)pbuf + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER));
		for( int i = 0 ; i < 256 ; i++ ) {
			pPal->rgbBlue  = (BYTE)i;
			pPal->rgbGreen = (BYTE)i;
			pPal->rgbRed   = (BYTE)i;
			pPal->rgbReserved = 0;
			pPal++;
		}
	}

	//---
	pbmf = (BITMAPFILEHEADER*) pbuf;
	pbmf->bfType = 0x4D42;
	pbmf->bfSize = bsize;
	pbmf->bfReserved2 = 0;
//	pbmf->bfOffBits   = (DWORD)(sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER));
	pbmf->bfOffBits   = (DWORD) hsize;

	lpbmi = (LPBITMAPINFOHEADER) (pbuf + sizeof(BITMAPFILEHEADER));

	lpbmi->biSize			= sizeof (BITMAPINFOHEADER);
	lpbmi->biWidth			= width;
	lpbmi->biHeight			= -height;
	lpbmi->biPlanes			= 1;
	lpbmi->biBitCount		= (WORD)bitcount;
	lpbmi->biCompression	= BI_RGB;
	lpbmi->biSizeImage		= imagesize;
	lpbmi->biXPelsPerMeter	= 0;
	lpbmi->biYPelsPerMeter	= 0;
	lpbmi->biClrUsed		= 0;
	lpbmi->biClrImportant	= 0;

	pimg = pbuf + sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

//	creu_trace(CREU_MSG_AREA_GENERAL, CREU_MSG_LEVEL_INFO, "bmpout 4\n");
//	memcpy(pimg, rgbbuf, imagesize);
//	creu_trace(CREU_MSG_AREA_GENERAL, CREU_MSG_LEVEL_INFO, "bmpout 5\n");
	fp = cr_fopen(fn, "wb");
	if (isencrypt) {
		U32 i;
		U32 seed;
		CR_Time_t st;

		U32 header[3];					// [seed] [r|m|d|h] [~1]
		U32 p0, p1, p2, p3;
		U08 *fbufEnc;
		HAND hrr;

		U08 xdata;

		OSAL_SYS_GetLocalTime( &st);
		//--- Make Seed and header
		srand(cr_gettickcount());

		p0 = (rand() & 0xFF) ;
		p1 = st.wMonth;
		p2 = st.wDay;
		p3 = st.wHour;
		header[0] = cr_rand_mkseed(p0, p1, p2, p3);
		header[1] = ((p0 << 24) & 0xFF000000) 					// RAND
			| ((p1 << 16) & 0x00FF0000) 					// month
			| ((p2 <<  8) & 0x0000FF00) 					// date
			| ((p3 <<  0) & 0x000000FF);					// hour
		header[2] = (~header[1]) ^ header[0];				// check..
		fwrite(&header[0] , sizeof(U32), 3, fp);
		//length = (sizeof(U32) * 3);

		seed = header[0];

		hrr = cr_rand_create(seed);
		for (i = 0; i < (sizeof(U32) * 3); i++) {
			cr_rand_gendataBYTE(hrr);		// discard... :P
		}

		fbufEnc = (U08 *) malloc(bsize);

		for (i = 0; i < (bsize-imagesize); i++) {
			xdata = cr_rand_gendataBYTE(hrr);
			*(fbufEnc+i) = *(pbuf+i) ^ xdata;
		}

		for (i = 0; i < imagesize; i++) {
			xdata = cr_rand_gendataBYTE(hrr);
			*(fbufEnc + i + (bsize-imagesize)) = *(buf+i) ^ xdata;
		}
		fwrite(fbufEnc, 1, bsize, fp);
		cr_rand_delete(hrr);
		free(fbufEnc);
	} else {
		//	creu_trace(CREU_MSG_AREA_GENERAL, CREU_MSG_LEVEL_INFO, "bmpout 6\n");
		fwrite(pbuf, 1, bsize - imagesize, fp);
		fwrite(buf, 1, imagesize, fp);
	}
//	fwrite(pbuf, 1, bsize, fp);
	fclose(fp);

	free(pbuf);
//	creu_trace(CREU_MSG_AREA_GENERAL, CREU_MSG_LEVEL_INFO, "bmpout 7\n");
	return ;
}

U32 bmpGrayLoad(char *fn, char *buf, int width, int height)
{	// Quick and dirty
	U32 res;

	int bitcount;
	int hsize;

	U32 s0, s1, s2;


	FILE *fp;

	//--
	fp = cr_fopen(fn, "rb");
	if (fp == NULL) {
		res = 0;
		goto func_exit;
	}

	bitcount = 8;

	//hsize = sizeof(BITMAPFILEHEADER) 
	//	+ sizeof(BITMAPINFOHEADER) 
	//	+ sizeof(RGBQUAD)*(1<<bitcount);

	s0 = sizeof(BITMAPFILEHEADER);
	s1 = sizeof(BITMAPINFOHEADER);
	s2 = sizeof(RGBQUAD);

	hsize = s0 + s1 + s2 * (1<<bitcount);



	fseek(fp, hsize, SEEK_SET);

	fread(buf, 1, width*height, fp);
	fclose(fp);

	res = 1;

func_exit:
	return res;
}

//--
U32 jpegLoad(HAND hjpeg, char *fn, char *buf, int width, int height)		// Load and decode.
{	
	U32 res;
	int len, readlen;
	int imgsize, imgsizeY;
	char *binbuf;
	char *imgbuf;
	U32 width0, height0;
	FILE *fp;

	//--
	fp = cr_fopen(fn, "rb");
	if (fp == NULL) {
		res = 0;
		goto func_exit;
	}

	fseek(fp, 0, SEEK_END);
	len = ftell(fp);

	fseek(fp, 0, SEEK_SET);
//#define MAXFILESIZE	(200 * 1024)
#define MAXFILESIZE	(512 * 1024)
#define MAXIMAGESIZE	(2048 * 1024 * 3)
	if (len > MAXFILESIZE) {
		fclose(fp);
		res = 0;
		goto func_exit;
	}

	binbuf = (char *) malloc((len + 1024) * sizeof(char));
#define MAXIMAGESIZE	(2048 * 1024 * 3)
	if (binbuf == NULL) {
		fclose(fp);
		res = 0;
		goto func_exit;
	}

	imgsizeY = width * height;
//	imgsize  = (imgsizeY * 3) / 2;
	imgsize  = (imgsizeY * 3);

	//--
	readlen = (int)fread(binbuf, sizeof(char), len, fp);
	fclose(fp);

	if (readlen != len) {
		free(binbuf);
		res = 0;
		goto func_exit;
	}


	// 0: success, -1: fail.
	if (cr_cam_jpeg_headerparse(
			hjpeg,
			(U08 *)binbuf,		// input, Jpeg bitstream
			len,		// input, Jpeg bitstream length (0: Fail encoding)
			&width0,	// output, Image width
			&height0	// output, Image height
			) == -1) {
		free(binbuf);
		res = 0;
		goto func_exit;
	}

	if (width != (int)width0 || height != (int)height0) {
		free(binbuf);
		res = 0;
		goto func_exit;
	}

	imgbuf = (char *) malloc(imgsize);
	if (imgbuf == NULL) {
		free(binbuf);
		res = 0;
		goto func_exit;
	}

	cr_cam_jpeg_decompress(
			hjpeg,
			(U08 *)imgbuf,	// output, YUV buffer
			(U08 *)binbuf,	// input, Jpeg bitstream
			len		// input, Jpeg bitstream length (0: Fail encoding)
			);

	memcpy(buf, imgbuf, imgsizeY);
	free(imgbuf);
	free(binbuf);

	res = 1;
func_exit:
	if(res !=1) 	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s: (%d) : error !!!\n", __func__, __LINE__);
	}
	return res;
}

void histogram_equalization(U08 *bufsrc, U08 *bufdest,
		U32 width, U32 height)
{
#ifdef IPP_SUPPORT
#define GrayLevels 256
	Ipp32s histo[GrayLevels];
	Ipp32f s[GrayLevels];
	Ipp32s levels[GrayLevels+1], values[GrayLevels+1];
	IppiSize imgSize;

	//---
	imgSize.width = width;
	imgSize.height = height;

	//calculate histogram
	ippiHistogramEven_8u_C1R(bufsrc, width, imgSize, histo, levels, GrayLevels+1, 0, GrayLevels);
	for (int i = 0; i < GrayLevels; i ++) {
		if (i > 0) {
			s[i] = float(histo[i-1]) / imgSize.width / imgSize.height;
			s[i] = s[i] + s[i-1];
		} else {
			s[i] = 0;
		}
		values[i] = Ipp32s(s[i] * (GrayLevels-1));
	}
	values[GrayLevels] = GrayLevels;

	//LUT
	ippiLUT_8u_C1R(bufsrc, width, bufdest, width, imgSize, values, levels, GrayLevels+1);
	
#else
    cv::Mat cvImgSrc(cv::Size(width, height), CV_8UC1, bufsrc), cvImgDst;
    cv::equalizeHist(cvImgSrc, cvImgDst);
    memcpy(bufdest, cvImgDst.data, sizeof(U08) * width * height);
#endif
}





void gamma_correction(U08 *bufsrc, U08 *bufdest,
		U32 width, U32 height)
{
#ifdef IPP_SUPPORT
	IppiSize imgSize;
	// gamma correction. :P
	imgSize.width = width;
	imgSize.height = height;

	//LUT
	ippiLUT_8u_C1R(bufsrc, width, bufdest, width, imgSize, GammaFwdLUT, GammaLevels, 256);
#else
    cv::Mat cvImgSrc(cv::Size(width, height), CV_8UC1, bufsrc), cvImgDst;
    cv::Mat lut(cv::Size(1, 256), CV_32SC1, GammaFwdLUT), lutChar;
    lut.convertTo(lutChar, CV_8UC1);
    cv::LUT(cvImgSrc, lutChar, cvImgDst);
    memcpy(bufdest, cvImgDst.data, sizeof(U08) * cvImgDst.cols * cvImgDst.rows);
#endif
}


I32 getMulcScale(
		float fmulc,
		U32 *pmulc,
		U32 *pscale)
{
	I32 res;
	U32 mulc;
	U32 scale;

	//------------
	res = 1;
	if (fmulc < 1.0) {
		mulc = (U32)(256 * fmulc);
		scale = 8;
	} else if (fmulc < 2.0) {
		mulc = (U32)(128 * fmulc);
		scale = 7;
	} else if (fmulc < 4.0) {
		mulc =  (U32)(64 * fmulc);
		scale = 6;
	} else if (fmulc < 8.0) {
		mulc =  (U32)(32 * fmulc);
		scale = 5;
	} else if (fmulc < 16.0) {
		mulc =  (U32)(16 * fmulc);
		scale = 4;
	} else if (fmulc < 32.0) {
		mulc =   (U32)(8 * fmulc);
		scale = 3;
	} else if (fmulc < 64.0) {
		mulc =   (U32)(4 * fmulc);
		scale = 2;
	} else if (fmulc < 128.0) {
		mulc =   (U32)(2 * fmulc);
		scale = 1;
	} else if (fmulc < 256.0) {
		mulc =   (U32)(1 * fmulc);
		scale = 0;
	} else {
		mulc = 1;
		scale = 0;
		res = 0;
	}

	*pmulc = mulc;
	*pscale = scale;
	return res;
}


U32 iana_readinfo_filename(HAND hiana, const char *infofile)
{
	U32 res;
	iana_t *piana;

//---
	if (hiana != NULL) {
		piana = (iana_t *) hiana;
		sprintf(piana->readinfofile, infofile);
		memset(piana->simresultfile, 0, sizeof(piana->simresultfile));
		res = 1;
	} else {
		res = 0;
	}

	return res;
}


U32 iana_simresult_filename(HAND hiana, const char *resultfile)
{
	U32 res;
	iana_t *piana;

	//---
	if (hiana != NULL) {
		piana = (iana_t *)hiana;	
		sprintf(piana->simresultfile, resultfile);
		res = 1;
	}
	else {
		res = 0;
	}


	return res;
}

////////////////////////////////////////////////



//////////////// For test application ////////////////////////
U32 jpegLoad2(HAND hjpeg, char *fn, char *buf, int *pwidth, int *pheight)		// Load and decode.
{	
	U32 res;
	int len, readlen;
	int imgsize, imgsizeY;
	char *binbuf;
	char *imgbuf;
	U32 width0, height0;
	FILE *fp;

	//--
	fp = cr_fopen(fn, "rb");
	if (fp == NULL) {
		res = 0;
		goto func_exit;
	}

	fseek(fp, 0, SEEK_END);
	len = ftell(fp);

	fseek(fp, 0, SEEK_SET);
//#define MAXFILESIZE	(200 * 1024)
#define MAXFILESIZE	(512 * 1024)
#define MAXIMAGESIZE	(2048 * 1024 * 3)
	if (len > MAXFILESIZE) {
		fclose(fp);
		res = 0;
		goto func_exit;
	}

	binbuf = (char *) malloc((len + 1024) * sizeof(char));
#define MAXIMAGESIZE	(2048 * 1024 * 3)
	if (binbuf == NULL) {
		fclose(fp);
		res = 0;
		goto func_exit;
	}




	//--
	readlen = (int)fread(binbuf, sizeof(char), len, fp);
	fclose(fp);

	if (readlen != len) {
		free(binbuf);
		res = 0;
		goto func_exit;
	}


	// 0: success, -1: fail.
	if (cr_cam_jpeg_headerparse(
			hjpeg,
			(U08 *)binbuf,		// input, Jpeg bitstream
			len,		// input, Jpeg bitstream length (0: Fail encoding)
			&width0,	// output, Image width
			&height0	// output, Image height
			) == -1) {
		free(binbuf);
		res = 0;
		goto func_exit;
	}



	imgsizeY = width0 * height0;
//	imgsize  = (imgsizeY * 3) / 2;
	imgsize  = (imgsizeY * 3);

	*pwidth = width0;
	*pheight = height0;

	//if (width != (int)width0 || height != (int)height0) {
	//	free(binbuf);
	//	res = 0;
	//	goto func_exit;
	//}

	imgbuf = (char *) malloc(imgsize);
	if (imgbuf == NULL) {
		free(binbuf);
		res = 0;
		goto func_exit;
	}

	cr_cam_jpeg_decompress(
			hjpeg,
			(U08 *)imgbuf,	// output, YUV buffer
			(U08 *)binbuf,	// input, Jpeg bitstream
			len		// input, Jpeg bitstream length (0: Fail encoding)
			);

	memcpy(buf, imgbuf, imgsizeY);
	free(imgbuf);
	free(binbuf);

	res = 1;
func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      Get Ball radius
 *
 *  @param[in]	piana
 *              IANA module handle
 *  @param[in]	camid
 *              Camera id
 *  @param[in]	pBp
 *              Ball 3d position 
 *	@param[out] pPr              
 *	            P- radius
 *	@param[out] pLr
 *				Local radius  (Projected to Z=0 plane)
 *
 *  @return		0: Fail. 
 *  			1: Success.
 *
 *	@author	    yhsuk
 *  @date       2021/01/25
 *******************************************************************************/
I32 iana_ballradius(iana_t *piana, U32 camid, cr_point_t *pBp, double *pPr, double *pLr)
{
	I32 res;

	iana_cam_t	*pic;
	cr_point_t *pcampos;

	cr_vector_t vbc;			// pb - pc,  view vector 
	cr_vector_t vbc_XY;			// XY only of vbc
	cr_vector_t vbc_XY_unit;	// unit vector for vbc_XY;
	double dx, dy, dz;
	double h;
	double d, dL;
	double theta;
	double r, r_, rL;
	double rP;
	
	cr_point_t PbL, PbrL; 
	point_t Lb, Lbr;
	point_t Pb, Pbr; 

	//---
	pic   =piana->pic[camid];
	pcampos = &pic->icp.CamPosL;


	cr_vector_make2(pcampos, pBp, &vbc);			// pcampos: start point, pBp: end point
	vbc_XY.x = vbc.x;
	vbc_XY.y = vbc.y;
	vbc_XY.z = 0;

	dz = -vbc.z;			// always PLUS.

	if (dz < 0) {
		// what?
		res = 0;
		goto func_exit;
	}

	h  = pcampos->z;
	if (h < 0) {
		// what?
		res = 0;
		goto func_exit;
	}

	if (h < dz) {
		// what?
		res = 0;
		goto func_exit;
	}

	d  = cr_vector_norm(&vbc_XY);
	cr_vector_scalar(&vbc_XY, (1.0/d), &vbc_XY_unit);

	theta = atan(d/dz);

	r = BALLRADIUS;
	r_ = r / cos(theta);
	rL = r_ * h / dz;				// BALL radius projected to Z=0 plane.

	dL = d * h / dz;

	cr_vector_scalar(&vbc_XY_unit, dL, &PbL);
	PbL.z = -h;
	cr_vector_add(pcampos, &PbL, &PbL);
	Lb.x = PbL.x;
	Lb.y = PbL.y;

	cr_vector_scalar(&vbc_XY_unit, (dL+rL), &PbrL);
	PbrL.z = -h;
	cr_vector_add(pcampos, &PbrL, &PbrL);
	Lbr.x = PbrL.x;
	Lbr.y = PbrL.y;


	iana_L2P(piana, camid, &Lb, &Pb, 0);
	iana_L2P(piana, camid, &Lbr, &Pbr, 0);

	dx = Pb.x - Pbr.x;
	dy = Pb.y - Pbr.y;
	rP = sqrt(dx*dx + dy*dy);

	*pPr = rP;
	*pLr = rL;
	res = 1;

func_exit:

	return res;
}



#if defined (__cplusplus)
}
#endif




