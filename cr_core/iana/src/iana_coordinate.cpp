/*!
*******************************************************************************
                   Creatz Coordinate

    @section copyright_notice COPYRIGHT NOTICE
    Copyright (c) 2011 by Creatz Inc.
    All Rights Reserved. \n
    Do not duplicate without prior written consent of Creatz Inc.

*******************************************************************************
    @section file_information FILE CREATION INFORMATION
    @file   iana_coordinate.h
    @brief  Coordinate transform
    @author Hyeonseok Choi
    @date   2021/02/03 First Created

    @section checkin_information LATEST CHECK-IN INFORMATION
    $Rev$
    $Author$
    $Date$
*******************************************************************************/

/*----------------------------------------------------------------------------
    Description	: defines referenced header files
-----------------------------------------------------------------------------*/
#include <math.h>
#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#else
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#endif

#include "cr_common.h"
#include "cr_matrix.h"
#include "cr_geometric.h"
#include "iana_coordinate.h"

#include "assert.h"

/*----------------------------------------------------------------------------
*	Description	: defines Macros and definitions
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*	Description	: type definition
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*	Description	: static variable declaration
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*	Description	: external and internal global & static variable declaration
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*	Description	: declares the external function prototype
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*	Description	: declares the static function prototype
-----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*!
********************************************************************************
*	@brief      coordinate transform from Pixel to Local
*
*   @param[in]	piana
*               IANA module handle
*   @param[in]	camid
*               Camera id
*	@param[in]  pPp
*				Pixel points, input
*	@param[out] pLp
*				Local points, output
*	@param[in]  undistorted
*				0: NOT undistorted.  1: Distorted.
*
*   @return		0: Fail. Numerical calculation error.
*  			    1: Success.
*
*	@author	    yhsuk
*   @date       2016/02/21
*******************************************************************************/
I32 iana_P2L(
    iana_t *piana,
    U32 camid,
    point_t *pPp,
    point_t *pLp,
    U32 undistorted)
{
    I32 res;

    //---
    if (piana->pic[camid]->icp.CamExtrinsicValid/* == 1818*/) {
#define ZVALUE_P2L	3.0
        //#define ZVALUE_P2L	0.5
        res = iana_P2L_EP(piana, camid, ZVALUE_P2L, pPp, pLp);		// 0.5 ...? 
    } else {
        res = iana_P2L0(piana, camid, pPp, pLp, undistorted);
    }
    return res;
}


I32 iana_P2L0(
    iana_t *piana,
    U32 camid,
    point_t *pPp,
    point_t *pLp,
    U32 undistorted)
{
    I32 res;
    iana_cam_param_t *picp;
    point_t Pp0;

    //---
    picp = &piana->pic[camid]->icp;

    Pp0.x = pPp->x;
    Pp0.y = pPp->y;

    if (Pp0.x >= FULLWIDTH) {
        Pp0.x  = FULLWIDTH-1;
    }
    if (Pp0.y >= FULLHEIGHT) {
        Pp0.y  = FULLHEIGHT-1;
    }


#if 0
    if (camid >= 0 && camid < 3) {
        if (undistorted) {
            cr_bi_quadratic(&picp->bAp2l_Ud, pPp, &Lp);
        } else {
            if (picp->bCamparamValid && picp->quad_UdValid) {
                point_t undistPnt;

                cr_undistortPoint(pPp, &undistPnt, picp->cameraintrinsic.cameraParameter, picp->cameraintrinsic.distCoeffs);			// to Undisorted....
                cr_bi_quadratic(&picp->bAp2l_Ud, &undistPnt, &Lp);
            } else {
                cr_bi_quadratic(&picp->bAp2l, pPp, &Lp);
            }
        }

        if (piana->UseProjectionPlane) {
            cr_point_t crLpp;
            double z;

            res = cr_plane_xy2z(&picp->ProjectionPlane, Lp.x, Lp.y, &z);
            crLpp.x = Lp.x; crLpp.y = Lp.y; crLpp.z = z;	// point on the projection plane

            if (res) {
                cr_point_t crLpz;
                cr_vector_t n;
                cr_point_t org;
                cr_plane_t xyplane;

                cr_line_t  ctoppline;			// line passes camera point to point on the Projection plane

                //---
                n.x = n.y = 0; n.z = 1;
                org.x = org.y = org.z = 0;
                cr_plane_p0n(&org, &n, &xyplane);			// xy plane.. :)


                if (piana->CamPosEstimatedLValid) {
                    cr_line_p0p1(&picp->CamPosEstimatedL, &crLpp, &ctoppline);
                } else {
                    cr_line_p0p1(&picp->CamPosL, &crLpp, &ctoppline);
                }


                cr_line_p0p1(&picp->CamPosEstimatedL, &crLpp, &ctoppline);

                cr_point_line_plane(&ctoppline, &xyplane, &crLpz);
                pLp->x = crLpz.x;
                pLp->y = crLpz.y;
            }
        } else {
            pLp->x = Lp.x;
            pLp->y = Lp.y;
            res = 1;
        }
    } else {
        res = 0;
    }
#endif

#if 1
    if (camid >= 0 && camid < MAXCAMCOUNT) {
        point_t Pp;
        if (*piana->pUseProjectionPlane) {
            if (undistorted) {
                cr_distortPoint(&Pp, &Pp, picp->cameraintrinsic.cameraParameter, picp->cameraintrinsic.distCoeffs);
            } else {
                Pp.x = Pp0.x;
                Pp.y = Pp0.y;
            }

            res = iana_P2L2(
                piana,
                camid,
                &Pp,
                pLp);
        } else {
            if (undistorted) {
                cr_bi_quadratic(&picp->bAp2l_Ud, &Pp0, pLp);
            } else {
                cr_bi_quadratic(&picp->bAp2l, &Pp0, pLp);
            }
        }

        res = 1;
    } else {
        res = 0;
    }
#endif

#define MAXLXY	5
    {
        if (pLp->x > MAXLXY) {
            pLp->x = MAXLXY;
        }
        if (pLp->x < -MAXLXY) {
            pLp->x = -MAXLXY;
        }

        if (pLp->y > MAXLXY) {
            pLp->y = MAXLXY;
        }
        if (pLp->y < -MAXLXY) {
            pLp->y = -MAXLXY;
        }
    }
    return res;
}

// ATTACKCAM
/*!
    ********************************************************************************
    *	@brief      coordinate transform from Local to Pixel
    *  @param[in]	piana
    *              IANA module handle
    *  @param[in]	camid
    *              Camera id
    *	@param[out] pLp
    *					Local points, input
    *	@param[in]  pPp
    *					Pixel points, output
    *	@param[in]  undistorted
    *					0: NOT undistorted.  1: Distorted.
    *  @return		0: Fail. Numerical calculation error.
    *  			1: Success.
    *
    *	@author	    yhsuk
    *  @date       2016/02/21
    *******************************************************************************/
I32 iana_L2P(
    iana_t *piana,
    U32 camid,
    point_t *pLp,
    point_t *pPp,
    U32 undistorted)
{
    I32 res;

    //---
//	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] CamExtrinsicValid: %d  pic[]: %p\n", camid, piana->pic[camid]->icp.CamExtrinsicValid, piana->pic[camid]); 
    if (piana->pic[camid]->icp.CamExtrinsicValid /*==1818*/) {
        res = iana_L2P_EP(piana, camid, pLp, pPp, NULL);

        if (pPp->x >= FULLWIDTH) {
            pPp->x  = FULLWIDTH-1;
        }
        if (pPp->y >= FULLHEIGHT) {
            pPp->y  = FULLHEIGHT-1;
        }
    } else {
        res = iana_L2P0(piana, camid, pLp, pPp, undistorted);
    }
    return res;
}




I32 iana_L2P0(
    iana_t *piana,
    U32 camid,
    point_t *pLp,
    point_t *pPp,
    U32 undistorted)
{
    I32 res;
    iana_cam_param_t *picp;
    //	point_t Lp;

    picp = &piana->pic[camid]->icp;
#if 0
    if (camid >= 0 && camid < 3) {
        if (piana->UseProjectionPlane) {
            cr_line_t  ctoxyline;			// line passes camera point and xy-plane
            cr_point_t crLpz;
            cr_point_t crLpp;

            //----
            crLpz.x = pLp->x; crLpz.y = pLp->y; crLpz.z = 0;

            cr_line_p0p1(&picp->CamPosEstimatedL, &crLpz, &ctoxyline);
            cr_point_line_plane(&ctoxyline, &picp->ProjectionPlane, &crLpp);

            Lp.x = crLpp.x;
            Lp.y = crLpp.y;
        } else {
            Lp.x = pLp->x;
            Lp.y = pLp->y;
        }

        if (undistorted) {
            cr_bi_quadratic(&picp->bAl2p_Ud, &Lp, pPp);
        } else {
            if (picp->bCamparamValid && piana->ic[camid].icp.quad_UdValid) {
                point_t undistPnt;
                cr_bi_quadratic(&picp->bAl2p_Ud, &Lp, &undistPnt);
                cr_distortPoint(&undistPnt, pPp, picp->cameraintrinsic.cameraParameter, picp->cameraintrinsic.distCoeffs);			// to Undisorted....
            } else {
                cr_bi_quadratic(&picp->bAl2p, &Lp, pPp);
            }
        }
        res = 1;
    } else {
        res = 0;
    }
#endif

#if 1
    if (camid >= 0 && camid < MAXCAMCOUNT) {
        point_t Pp;
        if (*piana->pUseProjectionPlane) {
            res = iana_L2P2(
                piana,
                camid,
                pLp,
                &Pp);
            if (undistorted) {
                cr_undistortPoint(&Pp, pPp, picp->cameraintrinsic.cameraParameter, picp->cameraintrinsic.distCoeffs);
            } else {
                pPp->x = Pp.x;
                pPp->y = Pp.y;
            }
        } else {
            if (undistorted) {
                cr_bi_quadratic(&picp->bAl2p_Ud, pLp, pPp);
            } else {
                cr_bi_quadratic(&picp->bAl2p, pLp, pPp);
            }
        }

        res = 1;
    } else {
        res = 0;
    }
#endif

    if (pPp->x >= FULLWIDTH) {
        pPp->x  = FULLWIDTH-1;
    }
    if (pPp->y >= FULLHEIGHT) {
        pPp->y  = FULLHEIGHT-1;
    }

    return res;

}

/*!
    ********************************************************************************
    *	@brief      coordinate transform from Local to Pixel, using Project Plane trans.
    *  @param[in]	piana
    *              IANA module handle
    *  @param[in]	camid
    *              Camera id
    *	@param[in] pLp
    *					Local points, input
    *	@param[out]  pPp
    *					Pixel points, distorted, output
    *  @return		0: Fail. Numerical calculation error.
    *  			1: Success.
    *
    *	@author	    yhsuk
    *  @date       2018/0826
    *******************************************************************************/
I32 iana_L2P2(
    iana_t *piana,
    U32 camid,
    point_t *pLp,
    point_t *pPp)
{
    I32 res;
    cr_plane_t xyplane;

    cr_vector_t nv;
    cr_point_t origin;

    //---------
    //picp = &piana->ic[camid].icp;

    // 1) make xyplane:  Z = 0 plane
    nv.x = 0; nv.y = 0; nv.z = 1;
    origin.x = 0; origin.y = 0; origin.z = 0;

#if defined(CALI_PLATE_TEST)
    origin.z = CALI_PLATE_TEST;
#endif
    cr_plane_p0n(&origin, &nv, &xyplane);

    // 2) pP with pLp and xyplane
    res = iana_LP2P2(piana, camid, pLp, &xyplane, pPp);		// Convert Lcp to Pp

    return res;
}

/*!
    ********************************************************************************
    *	@brief      coordinate transform from Local on the plane to Pixel point
    *  @param[in]	piana
    *              IANA module handle
    *  @param[in]	camid
    *              Camera id
    *	@param[in] pLp
    *					Local points, input
    *	@param[in] pPt
    *					target plane contains pLp
    *	@param[out]  pPp
    *					Pixel points, distorted, output
    *  @return		0: Fail. Numerical calculation error.
    *  			1: Success.
    *
    *	@author	    yhsuk
    *  @date       2018/0826
    *******************************************************************************/
I32 iana_LP2P2(
    iana_t *piana,
    U32 camid,
    point_t *pLp,
    cr_plane_t *pPt,
    point_t *pPp)
{
    I32 res;
    cr_point_t Lcp;
    cr_point_t LcpPP;

    cr_point_t *pCamPosition; 		// Camera position
    cr_plane_t *pProjectionPlane;	// Target Plane (Projection Plane)	
    iana_cam_param_t *picp;
    double z;

    //---------
    // 1) Get Lcp point from pLp and pPt
    res = cr_plane_xy2z(pPt, pLp->x, pLp->y, &z);

    Lcp.x = pLp->x;
    Lcp.y = pLp->y;
    Lcp.z = z;

    // 2) get point on the Project Plane 
    picp 				= &piana->pic[camid]->icp;

    pCamPosition 		= &picp->CamPosEstimatedL;
    pProjectionPlane 	= &picp->ProjectionPlane;

    res = iana_Ptr2TargetPlanePtr(
        &Lcp,			// Point.. (in)
        &LcpPP,			// Point on the Target Plane (Projection plane)
        pCamPosition, 	// Camera position
        pProjectionPlane	// Target Plane (Projection Plane)
    );

    // 3) point on the Project plane to Pp, distorted
    res = iana_L2P2PP(piana, camid, &LcpPP, pPp);

    return res;
}


/*!
    ********************************************************************************
    *	@brief      from Local on the PROJECTION plane to Pixel point
    *  @param[in]	piana
    *              IANA module handle
    *  @param[in]	camid
    *              Camera id
    *	@param[in]  pLcpPP
    *					Local points, input
    *	@param[out] pPp
    *					Pixel points, distorted, output
    *  @return		0: Fail. Numerical calculation error.
    *  			1: Success.
    *
    *	@author	    yhsuk
    *  @date       2018/0826
    *******************************************************************************/
I32 iana_L2P2PP(iana_t *piana, U32 camid, cr_point_t *pLcpPP, point_t *pPp)
{
    I32 res;
    point_t LpPP;
    point_t Pd_Ud;
    iana_cam_param_t *picp;

    //----
    LpPP.x = pLcpPP->x;
    LpPP.y = pLcpPP->y;
    res = iana_L2P_Ud(piana, camid, &LpPP, &Pd_Ud);

    picp = &piana->pic[camid]->icp;
    cr_distortPoint(&Pd_Ud, pPp, picp->cameraintrinsic.cameraParameter, picp->cameraintrinsic.distCoeffs);

    return res;
}


/*!
    ********************************************************************************
    *	@brief      coordinate transform from Local on the Projection Plane to pP, undistorted.
    *  @param[in]	piana
    *              IANA module handle
    *  @param[in]	camid
    *              Camera id
    *	@param[in] pLp
    *					Local points on the Projection Plane
    *	@param[out]  pPp_Ud
    *					Pixel points, output
    *  @return		0: Fail. Numerical calculation error.
    *  			1: Success.
    *
    *	@author	    yhsuk
    *  @date       2016/02/21
    *******************************************************************************/
I32 iana_L2P_Ud(
    iana_t *piana,
    U32 camid,
    point_t *pLpPP,
    point_t *pPp)
{
    I32 res;
    iana_cam_param_t *picp;

    picp = &piana->pic[camid]->icp;
    cr_bi_quadratic(&picp->bAl2p_Ud, pLpPP, pPp);

    res = 1;
    return res;
}




/*!
    ********************************************************************************
    *	@brief      coordinate transform from Pixel to Local point on the xy-plane
    *  @param[in]	piana
    *              IANA module handle
    *  @param[in]	camid
    *              Camera id
    *	@param[in]  pPp
    *					Pixel points, distorted, input
    *	@param[out] pLp
    *					Local points, output
    *  @return		0: Fail. Numerical calculation error.
    *  			1: Success.
    *
    *	@author	    yhsuk
    *  @date       2018/0826
    *******************************************************************************/
I32 iana_P2L2(
    iana_t *piana,
    U32 camid,
    point_t *pPp,
    point_t *pLp)
{
    I32 res;
    cr_plane_t xyplane;

    cr_vector_t nv;
    cr_point_t origin;

    //---------
    // 1) make xyplane:  Z = 0 plane
    nv.x = 0; nv.y = 0; nv.z = 1;
    origin.x = 0; origin.y = 0; origin.z = 0;
#if defined(CALI_PLATE_TEST)
    origin.z = CALI_PLATE_TEST;
#endif
    cr_plane_p0n(&origin, &nv, &xyplane);

    // 2) pP with pLp and xyplane
    res = iana_P2LP2(piana, camid, pPp, pLp, &xyplane);		// Convert pPp to pLp with target xyplane

    return res;
}

/*!
    ********************************************************************************
    *	@brief      coordinate transform from Pixel point to Local point on the target plane
    *  @param[in]	piana
    *              IANA module handle
    *  @param[in]	camid
    *              Camera id
    *	@param[in]  pPp
    *				Pixel points, distorted, input
    *	@param[out] pLp
    *				Local points, output
    *	@param[in]  pPt
    *				target plane contains pLp
    *  @return		0: Fail. Numerical calculation error.
    *  			1: Success.
    *
    *	@author	    yhsuk
    *  @date       2018/0826
    *******************************************************************************/
I32 iana_P2LP2(
    iana_t *piana,
    U32 camid,
    point_t *pPp,
    point_t *pLp,
    cr_plane_t *pPt)
{
    I32 res;
    cr_point_t LcpPP;
    cr_point_t Lcp;
    cr_point_t *pCamPosition; 		// Camera position
    iana_cam_param_t *picp;

    //---------
    // 1) Pp (Distorted) to LcpPP on the Projection Plane
    res = iana_P2L2PP(piana, camid, pPp, &LcpPP);

    // 2) LcpPP to Lcp, target plane
    picp = &piana->pic[camid]->icp;
    pCamPosition = &picp->CamPosEstimatedL;
    iana_Ptr2TargetPlanePtr(
        &LcpPP,			// Point.. on the Projection Plane (in)
        &Lcp,			// Point on the Target plane
        pCamPosition, 	// Camera position
        pPt				// Target Plane
    );

    // 2) Lcp to pLp, discard z.
    pLp->x = Lcp.x;
    pLp->y = Lcp.y;

    return res;
}


/*!
    ********************************************************************************
    *	@brief      from Pixel Point to Local on the PROJECTION plane
    *  @param[in]	piana
    *              IANA module handle
    *  @param[in]	camid
    *              Camera id
    *	@param[in] pPp
    *					Pixel points, distorted, input
    *	@param[out]  pLcpPP
    *					Local points, on the PROJECTION PLANE, input
    *  @return		0: Fail. Numerical calculation error.
    *  			1: Success.
    *
    *	@author	    yhsuk
    *  @date       2018/0826
    *******************************************************************************/
I32 iana_P2L2PP(iana_t *piana, U32 camid,
    point_t *pPp,
    cr_point_t *pLcpPP)
{
    I32 res;
    point_t LpPP;
    point_t Pd_Ud;
    iana_cam_param_t *picp;
    cr_plane_t *pProjectionPlane;	// Target Plane (Projection Plane)	
    double z;

    //----
    // 1) pPp to Pp_Ud  (Undistorted)
    picp = &piana->pic[camid]->icp;
    cr_undistortPoint(pPp, &Pd_Ud, picp->cameraintrinsic.cameraParameter, picp->cameraintrinsic.distCoeffs);	// to Undisorted....

    // 2) Pp_Ud (Undistorted point) to LpPP on the Projection Plane
    res = iana_P2L_Ud(piana, camid, &Pd_Ud, &LpPP);

    // 3) Get LcpPP point from pLpPP and ProjectionPlane
    pProjectionPlane 	= &picp->ProjectionPlane;
    cr_plane_xy2z(pProjectionPlane, LpPP.x, LpPP.y, &z);

    pLcpPP->x = LpPP.x;
    pLcpPP->y = LpPP.y;
    pLcpPP->z =      z;
    return res;
}


/*!
    ********************************************************************************
    *	@brief      coordinate transform from pPp_Ud (Undistorted) to Local on pP on the Projection Plane
    *  @param[in]	piana
    *              IANA module handle
    *  @param[in]	camid
    *              Camera id
    *	@param[in] pPp_Ud
    *				Pixel points, undistorted, input
    *	@param[out] 	pLpPP
    *				Local points on the Projection Plane
    *  @return		0: Fail. Numerical calculation error.
    *  			1: Success.
    *
    *	@author	    yhsuk
    *  @date       2016/02/21
    *******************************************************************************/
I32 iana_P2L_Ud(
    iana_t *piana,
    U32 camid,
    point_t *pPp_Ud,
    point_t *pLpPP)
{
    I32 res;
    iana_cam_param_t *picp;

    //----
    picp = &piana->pic[camid]->icp;
    cr_bi_quadratic(&picp->bAp2l_Ud, pPp_Ud, pLpPP);

    res = 1;
    return res;
}

//ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
/*!
    ********************************************************************************
    *	@brief      Is in Tee area?
    *
    *  @param[in]	piana
    *              IANA module handle
    *	@param[in]  x
    *				x, Local coordinate
    *	@param[in]  y
    *				y, Local coordinate
    *
    *  @return		1: In.
    *  			0: Out.
    *
    *	@author	    yhsuk
    *  @date       2016/02/21
    *******************************************************************************/
I32 iana_isInTee(iana_t *piana, cr_point_t *p3d)
{
    I32 res;


    if (piana->teedotmode) {
        iana_cam_t	*pic;
        cr_point_t *pteeTop3d;
        cr_point_t 	onTee3d;
        double maxdist;
        double dx, dy, dz, dist;

        //--
        pic  = piana->pic[0];	// 
        pteeTop3d = &pic->teeTop3d;

        onTee3d.x = pteeTop3d->x;
        onTee3d.y = pteeTop3d->y;
        if (p3d->z < 0) {
            onTee3d.z = 0;
        } else if (p3d->z > pteeTop3d->z) {
            onTee3d.z = pteeTop3d->z;
        } else {
            onTee3d.z = p3d->z;
        }

        //#define MAXDIST2TEE0	0.05
        //#define MAXDIST2TEE1	0.03
        //#define HEIGHTTEE4TEE0	0.02

#define MAXDIST2TEE0	0.05
#define MAXDIST2TEE1	0.03
#define HEIGHTTEE4TEE0	0.02


        if (p3d->z > HEIGHTTEE4TEE0) {
            maxdist = MAXDIST2TEE0;
        } else {
            maxdist = MAXDIST2TEE1;
        }

        dx = (p3d->x - onTee3d.x);
        dy = (p3d->y - onTee3d.y);
        dz = (p3d->z - onTee3d.z);

        dist = sqrt(dx*dx + dy*dy + dz*dz);

        if (dist < maxdist) {
            res = 1;
        } else {
            res = 0;
        }
        /*
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "dist ballpos %lf %lf %lf  vs Tee %lf %lf %lf = %lf (vs %lf)  res: %d\n",
                p3d->x, p3d->y, p3d->z, onTee3d.x, onTee3d.y, onTee3d.z, dist, maxdist, res);
        */
    } else {
        //res = iana_isInTeeL(piana, p3d->x, p3d->z);   // BAD..  T_T;   Who coded this XXX?     by yhsuk. 0903
        res = iana_isInTeeL(piana, p3d->x, p3d->y);
    }

    return res;
}




I32 iana_isInTeeL(iana_t *piana, double x, double y)
{
    I32 res;
    cr_rectL_t *prtL;

    //prtL = &piana->rectLTee;
    prtL = piana->prectLTee;
    if ((prtL->sx < x && x < prtL->ex) && (prtL->sy < y && y < prtL->ey)) {
        res = 1;
    } else {
        res = 0;
    }
    return res;
}

/*!
    ********************************************************************************
    *	@brief      Is in Iron area?
    *
    *  @param[in]	piana
    *              IANA module handle
    *	@param[in]  x
    *				x, Local coordinate
    *	@param[in]  y
    *				y, Local coordinate
    *
    *  @return		1: In.
    *  			0: Out.
    *
    *	@author	    yhsuk
    *  @date       2016/02/21
    *******************************************************************************/
I32 iana_isInIron(iana_t *piana, cr_point_t *p3d)
{
    I32 res;

    res = iana_isInIronL(piana, p3d->x, p3d->y);

    return res;
}
I32 iana_isInIronL(iana_t *piana, double x, double y)
{
    I32 res;
    cr_rectL_t *prtL;

    prtL = piana->prectLIron;
    if ((prtL->sx < x && x < prtL->ex) && (prtL->sy < y && y < prtL->ey)) {
        res = 1;
    } else {
        res = 0;
    }
    return res;
}

/*!
    ********************************************************************************
    *	@brief      Is in Putter area?
    *
    *  @param[in]	piana
    *              IANA module handle
    *	@param[in]  x
    *				x, Local coordinate
    *	@param[in]  y
    *				y, Local coordinate
    *
    *  @return		1: In.
    *  			0: Out.
    *
    *	@author	    yhsuk
    *  @date       2016/02/21
    *******************************************************************************/
I32 iana_isInPutter(iana_t *piana, cr_point_t *p3d)
{
    I32 res;

    res = iana_isInPutterL(piana, p3d->x, p3d->y);

    return res;
}



I32 iana_isInPutterL(iana_t *piana, double x, double y)
{
    I32 res;
    cr_rectL_t *prtL;

    prtL = piana->prectLPutter;
    if ((prtL->sx < x && x < prtL->ex) && (prtL->sy < y && y < prtL->ey)) {
        res = 1;
    } else {
        res = 0;
    }
    return res;
}



/*!
********************************************************************************
*	@brief      Is in Bunker area?
*
*   @param[in]	piana
*              IANA module handle
*	@param[in]  x
*				x, Local coordinate
*	@param[in]  y
*				y, Local coordinate
*
*   @return		1: In.
*  			0: Out. (or Bunker area is NOT activated)
*
*	@author	    yhsuk
*   @date       2020/0924
*******************************************************************************/
static int s_InBunker = 0;    // Don't change,  1: in Bunker,  -1: NOT in Bunker

I32 iana_isInBunker(iana_t *piana, cr_point_t *p3d)
{
	I32 res;

	res = iana_isInBunkerL(piana, p3d->x, p3d->y);

	if (s_InBunker == 1) {
		res = 1;
	} else if (s_InBunker == 2) {
		res = 1;
	} else {
		// don't change.. :P
	}

	return res;
}



I32 iana_isInBunkerL(iana_t *piana, double x, double y)
{
    I32 res;
    cr_rectL_t *prtL;

    if (*(piana->pRoughBunkerMat)) {
        prtL = piana->prectLBunker;
        if ((prtL->sx < x && x < prtL->ex) && (prtL->sy < y && y < prtL->ey)) {
            res = 1;
        } else {
            res = 0;
        }
    } else {
        res = 0;
    }
    return res;
}



/*!
    ********************************************************************************
    *	@brief      Is in Rough area?
    *
    *  @param[in]	piana
    *              IANA module handle
    *	@param[in]  x
    *				x, Local coordinate
    *	@param[in]  y
    *				y, Local coordinate
    *
    *  @return		1: In.
    *  			0: Out. (or Rough area is NOT activated)
    *
    *	@author	    yhsuk
    *  @date       2020/0924
    *******************************************************************************/
I32 iana_isInRough(iana_t *piana, cr_point_t *p3d)
{
    I32 res;

    res = iana_isInRoughL(piana, p3d->x, p3d->y);

    return res;
}



I32 iana_isInRoughL(iana_t *piana, double x, double y)
{
    I32 res;
    cr_rectL_t *prtL;

    if (*(piana->pRoughBunkerMat)) {
        prtL = piana->prectLRough;
        if ((prtL->sx < x && x < prtL->ex) && (prtL->sy < y && y < prtL->ey)) {
            res = 1;
        } else {
            res = 0;
        }
    } else {
        res = 0;
    }
    return res;
}




/*!
    ********************************************************************************
    *	@brief      Get Projection Plane
    *
    *  @param[in]	piana
    *              IANA module handle

    *  @return		1: In.
    *  			0: Out.
    *
    *	@author	    yhsuk
    *  @date       2018/08/03
    *******************************************************************************/
I32 iana_getProjectPlane(iana_t *piana)
{
    I32 res;

    res = iana_getProjectPlane1(piana);

    return res;
}


I32 iana_getProjectPlane0(iana_t *piana)
{
    I32 res;
    U32 camid;
    iana_cam_param_t *picp;
    cr_vector_t zvect;
    cr_vector_t Cam2Tee;

    //-----------
    zvect.x = zvect.y = zvect.z = 0;
    for (camid = 0; camid < MAXCAMCOUNT; camid++) {
        picp = &piana->pic[camid]->icp;

        // 1) cam pos
        // 2) get vector from cam pos to Tee pos (Origin)
        //cr_vector_sub(&zvect, &picp->CamPosL, &Cam2Tee);

        if (*piana->pCamPosEstimatedLValid) {
            cr_vector_sub(&zvect, &picp->CamPosEstimatedL, &Cam2Tee);
        } else {
            cr_vector_sub(&zvect, &picp->CamPosL, &Cam2Tee);
        }

        // 3) make plane.
        cr_plane_p0n(&zvect, &Cam2Tee, &picp->ProjectionPlane);	// Projection Plane
    }

    res = 1;
    return res;
}



I32 iana_getProjectPlane1(iana_t *piana)
{
    I32 res;
    U32 camid;
    iana_cam_param_t *picp;
    cr_vector_t zvect;
    cr_vector_t Cam2Tee;

    //-----------

    for (camid = 0; camid < MAXCAMCOUNT; camid++) {
        picp = &piana->pic[camid]->icp;

        // 1) cam pos
        // 2) get vector from cam pos to Tee pos (Origin)
        //cr_vector_sub(&zvect, &picp->CamPosL, &Cam2Tee);

        memcpy(&zvect, &picp->ProjectionPlaneCenter, sizeof(cr_point_t));

        if (*piana->pCamPosEstimatedLValid) {
            cr_vector_sub(&zvect, &picp->CamPosEstimatedL, &Cam2Tee);
        } else {
            cr_vector_sub(&zvect, &picp->CamPosL, &Cam2Tee);
        }

        // 3) make plane.
        cr_plane_p0n(&zvect, &Cam2Tee, &picp->ProjectionPlane);	// Projection Plane
    }

    res = 1;
    return res;
}




U32 iana_Ptr2TargetPlanePtr(
    cr_point_t *pPtr,			// Point (in)
    cr_point_t *pPtrTarget,		// Point on the Target
    cr_point_t *pCamPos, 		// Camera position
    cr_plane_t *pPlaneTarget	// Target Plane
)
{
    U32 res;
    cr_line_t  linec2ptr;			// line passes camera point and Ptr

    //----
    cr_line_p0p1(pCamPos, pPtr, &linec2ptr);
    cr_point_line_plane(&linec2ptr, pPlaneTarget, pPtrTarget);

    res = 1;

    return res;
}

#if 0
void testLP2(iana_t *piana)
{
    FILE *fp;
    char fn[128];
    U32 camid;
    U32 count;
    U32 i;

    static int s_testPL2 = 0;

    //static int s_testPL2 = 1;
#define DATALPCOUNT 1024
    point_t dataL[3][DATALPCOUNT];
    point_t dataP[3][DATALPCOUNT];
    int 	datacount[3];


    if (s_testPL2 == 0) {
        return;
    }


    for (camid = 0; camid < MAXCAMCOUNT; camid++) {
        datacount[camid] = 0;
        sprintf(fn, "ddd%d.txt", camid);
        fp = fopen(fn, "r");
        if (fp == NULL) {
            continue;
        }

        count = 0;
        for (i = 0; i < DATALPCOUNT; i++) {
            double xL, yL, xP, yP;
            I32 readcount;
            readcount = fscanf(fp, "%lf %lf %lf %lf", &xL, &yL, &xP, &yP);
            if (readcount != 4) {
                break;
            }
            dataL[camid][count].x = xL;
            dataL[camid][count].y = yL;

            dataP[camid][count].x = xP;
            dataP[camid][count].y = yP;
            count++;

            if (count >= DATALPCOUNT) {
                break;
            }
        }
        if (fp) {
            fclose(fp);
        }
        datacount[camid] = count;


        //-------------------------------------
        for (i = 0; i < count; i++) {
            point_t Lp0, Pp0;
            point_t Lp, Pp;
            Lp0.x = dataL[camid][i].x;
            Lp0.y = dataL[camid][i].y;
            Pp0.x = dataP[camid][i].x;
            Pp0.y = dataP[camid][i].y;

            iana_L2P2(piana, camid, &Lp0, &Pp);
            iana_P2L2(piana, camid, &Pp0, &Lp);
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,
                "[ %d %4d ] Lp0: ( %16lf %16lf ) Pp0: ( %16lf %16lf )   Lp: ( %16lf %16lf ) Pp: ( %16lf %16lf ) \n",
                camid, i,
                Lp0.x, Lp0.y,
                Pp0.x, Pp0.y,
                Lp.x, Lp.y,
                Pp.x, Pp.y);
        }
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");

    }

}
#endif

I32 iana_P2L3D_EP(
    iana_t *piana,
    U32 camid,
    double zvalue,
    point_t *pPp,
    cr_point_t *pLp3d)
{
    I32 res;
    U32 i, j;
    point_t uPp, iPp;
    double camPp[4];
    double wPp[4];

    //cameraintrinsic_t *pcip;
    cameraextrinsic_t *pcep;


    iana_cam_param_t *picp;
    //	camparam_t	*pcp;
    double *pcameraParameter;			// // fx, cx, fy, cy
    double *pdistCoeffs;				// k1, k2, p1, p2, k3	

    //------
    picp = &piana->pic[camid]->icp;
    {
        pcep = &picp->cep;

        pcameraParameter 	= picp->cameraintrinsic.cameraParameter;
        pdistCoeffs			= picp->cameraintrinsic.distCoeffs;
        cr_intrinsic_normalize(pPp, &uPp, pcameraParameter);
        cr_undistortPoint_normalized(&uPp, &iPp, pdistCoeffs);

    }
    // 1) pixel to image coordinate. undistort + Int^-1




    // 2) to Camera coord: Insert (and mult) zvalue and 1
    camPp[0] = iPp.x * zvalue;
    camPp[1] = iPp.y * zvalue;
    camPp[2] = zvalue;
    camPp[3] = 1.0;

    // 3) to World coord
    for (i = 0; i < 4; i++) {
        wPp[i] = 0;
        for (j = 0; j < 4; j++) {
            //wPp[i] += Invetp[i][j] * camPp[j];
            wPp[i] += pcep->InvextMat[i][j] * camPp[j];
        }
    }

    pLp3d->x = wPp[0];
    pLp3d->y = wPp[1];
    pLp3d->z = wPp[2];

    res = 1;
#if 0
    {
        double zvalue1;
        point_t Pp;
        double errx, erry, errall;
        double errzvalue;
        iana_L3D2P_EP(
            piana,
            camid,
            pLp3d,			// Input. World coordinate point
            &zvalue1,		// zvalue.
            &Pp);			// image point

        errx = pPp->x - Pp.x;
        erry = pPp->y - Pp.y;
        errall = sqrt(errx*errx + erry*erry);
        errzvalue = zvalue - zvalue1;

        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]  Pp: %10lf %10lf   ->    %10lf %10lf  (err: %10le %10le, %10le),   zvalue: %10lf ->  %10lf (err: %10le)\n", camid, pPp->x, pPp->y, Pp.x, Pp.y, errx, erry, errall,
            zvalue, zvalue1, errzvalue);

        // ERROR:  smaller than 1e-13.      
    }
#endif

    return res;
}


I32 iana_P2L_EP(iana_t *piana, U32 camid, double zvalue, point_t *pPp,
    point_t *pLp0)
{
    I32 res;
    cr_plane_t xyplane;

    cr_vector_t nv;
    cr_point_t origin;

    cr_point_t crwPp;
    cr_point_t camPos0;
    cr_point_t crLp;
    cr_point_t Lp3d;

    iana_cam_t *pic;
    iana_cam_param_t *picp;


    //--
    if (zvalue < 0.0) {
#define DEFAULT_ZVALUE_P2L 0.5
        zvalue = DEFAULT_ZVALUE_P2L;
    }
    iana_P2L3D_EP(piana, camid, zvalue, pPp, &Lp3d);


    //---------
    // 1) make xyplane:  Z = 0 plane
    nv.x = 0; nv.y = 0; nv.z = 1;
    origin.x = 0; origin.y = 0; origin.z = 0;
    cr_plane_p0n(&origin, &nv, &xyplane);

    crwPp.x = Lp3d.x;
    crwPp.y = Lp3d.y;
    crwPp.z = Lp3d.z;


    pic  = piana->pic[camid];
    picp = &pic->icp;


    camPos0.x = picp->CamPosL.x;
    camPos0.y = picp->CamPosL.y;
    camPos0.z = picp->CamPosL.z;

    iana_Ptr2TargetPlanePtr(&crwPp, &crLp, &camPos0, &xyplane);
    /*
       cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "        wLp: \t\t\t\t\t\t%lf %lf %lf, %lf\n",
       crLp.x, crLp.y, crLp.z);
       */

    pLp0->x = crLp.x;
    pLp0->y = crLp.y;

    res = 1;

    return res;
}



I32 iana_L3D2P_EP(
    iana_t *piana,
    U32 camid,
    cr_point_t *pLp3d,		// World coordinate point
    double *pzvalue,		// zvalue.   NULL: don't return.
    point_t *pPp)			// image point
{
    I32 res;
    U32 i, j;
    point_t uPp, iPp;
    point_t Pp;
    double camPp[4];
    double wPp[4];
    double zvalue;

    //cameraintrinsic_t *pcip;
    cameraextrinsic_t *pcep;


    iana_cam_param_t *picp;
    //	camparam_t	*pcp;
    double *pcameraParameter;			// // fx, cx, fy, cy
    double *pdistCoeffs;				// k1, k2, p1, p2, k3	

    //------
    picp = &piana->pic[camid]->icp;
    {
        pcep = &picp->cep;

        pcameraParameter 	= picp->cameraintrinsic.cameraParameter;
        pdistCoeffs			= picp->cameraintrinsic.distCoeffs;
        //		cr_intrinsic_normalize(pPp, &uPp, pcameraParameter);
        //		cr_undistortPoint_normalized(&uPp, &iPp, pdistCoeffs);
    }


    // 1) World coord to Camera coord.
    wPp[0] = pLp3d->x;
    wPp[1] = pLp3d->y;
    wPp[2] = pLp3d->z;
    wPp[3] = 1.0;

    for (i = 0; i < 4; i++) {
        camPp[i] = 0;
        for (j = 0; j < 4; j++) {
            //wPp[i] += Invetp[i][j] * camPp[j];
            camPp[i] += pcep->extMat[i][j] * wPp[j];
        }
    }
    // 2) get zvalue and to undistorted_normalzed image point.
    // check camPp[3] == 1.0
    zvalue = camPp[2];
    iPp.x = camPp[0] / zvalue;
    iPp.y = camPp[1] / zvalue;

    // 3) distort.. :P

    cr_distortPoint_normalized(&iPp, &uPp, pdistCoeffs);

    // 4) denormalization

    cr_intrinsic_denormalize(&uPp, &Pp, pcameraParameter);

    pPp->x = Pp.x;
    pPp->y = Pp.y;

    if (pzvalue) {
        *pzvalue = zvalue;
    }
    res = 1;

    return res;
}



I32 iana_L2P_EP(iana_t *piana, U32 camid, point_t *pLp, point_t *pPp, double *pzvalue)
{
    I32 res;
    cr_point_t Lp3d;

    //---
    Lp3d.x = pLp->x;
    Lp3d.y = pLp->y;
    Lp3d.z = 0;


    res = iana_L3D2P_EP(
        piana,
        camid,
        &Lp3d,		// World coordinate point
        pzvalue,		// zvalue.   NULL: don't return.
        pPp);			// image point

    res =1;
    return res;
}




/*!
********************************************************************************
*	@brief      Convert pixel coordinated point to camera normal coordinate
*               Camera Normal Plane is the plane perpendicular to the cam view direction of the distance 1
*   @param[in]	piana
*               IANA module handle
*   @param[in]	camid
*               camera id
*   @param[in]	distorted
*               0 if pixel coordinated point is already undistorted, positive if the pixel coordinated point is distorted
*	@param[in]  pPtPix
*				pointer to the pixel coordinated point
*	@param[out] pPtNormal
*				pointer to the camera normal coordinated point
*
*   @return		1: success to calculate
*  			    0: Fail to calculate
*
*	@author	    Hyeons Choi
*   @date       2021/02/03
*******************************************************************************/
I32 iana_P2N(iana_t *piana, 
    U32 camid,
    int distorted,
    point_t *pPtPix, 
    point_t *pPtNormal)
{
    I32 res;
    double *camParams, *distParmas;

    camParams = piana->pic[camid]->icp.cameraintrinsic.cameraParameter;
    distParmas = piana->pic[camid]->icp.cameraintrinsic.distCoeffs;

    res = cr_intrinsic_normalize(pPtPix, pPtNormal, camParams);
    if (distorted) {
        cr_undistortPoint_normalized(pPtNormal, pPtNormal, distParmas);
    }

    return res;
}


/*!
********************************************************************************
*	@brief      Convert camera normal coordinated point to pixel coordinated point.
*               Camera Normal Plane is the plane perpendicular to the cam view direction of the distance 1
*   @param[in]	piana
*               IANA module handle
*   @param[in]	camid
*               camera id
*   @param[in]	distorted
*               0: the result pixel coordinated point to be undistorted
*               positive: the pixel coordinated point to be distorted
*	@param[in]  pPtNormal
*				pointer to the camera normal coordinated point
*	@param[out] pPtPix
*				pointer to the pixel coordinated point
*
*   @return		1: success to calculate
*  			    0: Fail to calculate
*
*	@author	    Hyeons Choi
*   @date       2021/02/03
*******************************************************************************/
I32 iana_N2P(iana_t *piana,
    U32 camid,
    int distorted,
    point_t *pPtNormal, 
    point_t *pPtPix)
{
    I32 res;
    point_t distortNormal;
    double *camParams, *distParmas;

    camParams = piana->pic[camid]->icp.cameraintrinsic.cameraParameter;
    distParmas = piana->pic[camid]->icp.cameraintrinsic.distCoeffs;

    if (distorted) {
        cr_distortPoint_normalized(pPtNormal, &distortNormal, distParmas);
    }
    res = cr_intrinsic_denormalize(&distortNormal, pPtPix, camParams);

    return res;
}


/*!
********************************************************************************
*	@brief      Convert camera normal coordinated point to world coordinated point.
*               We assume the distance of object from camera is given.
*   @param[in]	piana
*               IANA module handle
*   @param[in]	camid
*               camera id
*   @param[in]	pPtNormal
*               pointer to the camera normal coordinated point
*	@param[in]  objDistCam
*				distance of object from camera
*	@param[out] pPtWorld
*				pointer to the world coordinated point
*
*   @return		1: success to calculate
*  			    0: Fail to calculate
*
*	@author	    Hyeons Choi
*   @date       2021/02/03
*******************************************************************************/
I32 iana_N2World(iana_t *piana,
    U32 camid,
    point_t *pPtNormal, 
    double objDistCam,
    cr_point_t *pPtWorld)
{
    I32 res;
    cr_point_t normPtWDist;
    double *extMat;
    double extmat33[3][3];

    normPtWDist.x = pPtNormal->x * objDistCam;
    normPtWDist.y = pPtNormal->y * objDistCam;
    normPtWDist.z = objDistCam;
    
    extMat = (double*) piana->pic[camid]->icp.cep.extMat;
    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {
            extmat33[i][j] = *(extMat+i*4+j);
        }
    }
    mat3v3mult(extmat33, (double*)&normPtWDist, (double*)pPtWorld);
    pPtWorld->x += extMat[3];
    pPtWorld->y += extMat[7];
    pPtWorld->z += extMat[11];

    res = 1;
    return res;
}

I32 iana_World2N2(iana_t *piana,
    U32 camid,
    cr_point_t *pPtWorld, 
    point_t *pPtNormal)
{
    I32 res;
    double *invExtMat;
    double invExtMat33[3][3];
    cr_point_t pPtRotated;
    
    invExtMat = (double*)piana->pic[camid]->icp.cep.InvextMat;
    for (int i=0; i<3; i++) {
        for (int j=0; j<3; j++) {
            invExtMat33[i][j] = *(invExtMat+i*4+j);
        }
    }
    mat3v3mult(invExtMat33, (double*)pPtWorld, (double*)&pPtRotated);
    pPtRotated.x += invExtMat[3];
    pPtRotated.y += invExtMat[7];
    pPtRotated.z += invExtMat[11];

    if (pPtRotated.z < 1e-5) {
        res = 0;
        return res;
    }

    pPtNormal->x = pPtRotated.x / pPtRotated.z;
    pPtNormal->y = pPtRotated.y / pPtRotated.z;
    
    res = 1;
    return res;

}
#if defined (__cplusplus)
}
#endif

