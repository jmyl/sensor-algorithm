/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2019 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_security_implement.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2019/04/08 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <ctype.h>

#include "cr_common.h"
#include "iana_security_implement.h"
#include "scamif.h"
#include "iana_security.h"
#include "sha256.h"



#if defined(_WIN32)
#define _WIN32_DCOM
#include <iostream>
using namespace std;
#include <comdef.h>
#include <Wbemidl.h>

# pragma comment(lib, "wbemuuid.lib")

#include <atlbase.h>
#include "WQL.h"
#endif

#include "cr_math.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
static int hddSerialRead0(U08 hddserial[], int drivenum, int maxlen);
#if defined(_WIN32)
static char * flipAndCodeBytes(const char * str, int pos, int flip, char * buf);
#endif
U32 pcinfocode_core(
		U08 code[32], 				// Output code.         32byte = 8 * 4byte
		U08 hash[32], 				// Input Hash code.     32byte = 8 * 4byte
		U32 cpuid[4]				// Input cpuid code.    16byte = 4 * 4byte
);
U32 runcode_core(U08 runcode[32], U08 pcinfocode[32], U32 cpusid[4]);

/*!
 ********************************************************************************
 *	@brief      Get pcinfo. (mainboard serial)
 *
 *  @param[in]	h
 *              IANA security module handle
 *  @param[out]	bdserial
 *              Motherboard Seiral String. max 32 bytes.
 *  @return		result.   1: GOOD. 0: FAIL.
 *
 *	@author	    yhsuk
 *  @date       2019/04/02
 *******************************************************************************/
U32 baseBoardSerialHddReadHash(U08 bdserial_[32], U08 hddserial_[32], U08 sha256[32])
{
	U32 res;
	U32 res0, res1;

//	U08 biosserial[40];
	U08 biosserial[1024];

	U08 bdserial[1024], hddserial[1024];
	U08 datain[1024];
	int len0, len1, len;

	//---
	memset(datain, 0, 1024);
	if (bdserial) {
		memset(bdserial, 0, 1024);
	}
	if (hddserial) {
		memset(hddserial, 0, 1024);
	}
	if (bdserial_) {
		memset(bdserial_, 0, 32);
	}
	if (hddserial_) {
		memset(hddserial_, 0, 32);
	}
	res0 = 0; len0 = 0;
	res1 = 0; len1 = 0;
	if (bdserial_) {
		res0 = baseBoardBiosSerialRead(bdserial, biosserial, 1000);
		if (res0 == 1) {
			len0 = (int)strlen((const char *)bdserial);
			//			if (len0 > 32) len0 = 32;
			if (len0 >= 32) {
				int i, c;
				for (i = 0; i < len0; i++) {
					if (i > 31) {
						c = (bdserial[i] + bdserial[i % 31]) & 0xFF;
						if (c == 0) {
							c = '~';
						}
						bdserial[i % 31] = (U08)(c&0xFF);
					}
				}
				len0 = 31;
				bdserial[31] = 0;
			}
			if (len0 > 0) {
				memcpy(datain, bdserial, len0);
				memcpy(bdserial_, bdserial, len0);
				bdserial_[len0] = 0;
			} else {
				len0 = 0;
			}
		} else {
			len0 = 0;
		}
	} else {
		res0 = 1;
		len0 = 0;
	}


	if (res0)  {
		if (hddserial_) {
			//res1 = hddSerialRead(hddserial, 0, 32);
			res1 = hddSerialRead(hddserial, 0, 100);
			if (res1 == 1) {
				len1 = (int)strlen((const char *)hddserial);
				//printf("len1: %d, `%s'\n", len1, hddserial);

				////if (len1 > 32) len0 = 32;
				//if (len1 >= 32) {
				//	len1 = 31;
				//	hddserial[31] = 0;
				//}
				//printf("len1: %d, `%s'\n", len1, hddserial);
				//if (len1 > 0) {
				//	memcpy(datain+len0, hddserial, len1);
				//} else {
				//	len1 = 0;
				//}

				if (len1 >= 32) {
					int i, c;
					for (i = 0; i < len1; i++) {
						if (i > 31) {
							c = (hddserial[i] + hddserial[i % 31]) & 0xFF;
							if (c == 0) {
								c = '~';
							}
							hddserial[i % 31] = (U08)(c & 0xFF);
						}
					}
					len1 = 31;
					hddserial[31] = 0;
					//printf("-> len1: %d, `%s'\n", len1, hddserial);
				}

				if (len1 > 0) {
					memcpy(datain+len0, hddserial, len1);
					memcpy(hddserial_, hddserial, len1);
				}
				else {
					len1 = 0;
				}
			} else {
				len1 = 0;
			}
		} else {
			res1 = 1;
			len1 = 0;
		}
	}

	if (res0 != 0 && res1 != 0) {
		if (sha256 != NULL) {
			HAND h;
			U32 i;
			U08 mycode[4];

			//---

#define HASH_CODE0		0x7F
#define HASH_CODE1		0x7E
#define HASH_CODE2		0x7D
#define HASH_CODE3		0x7C
			mycode[0] = HASH_CODE0;
			mycode[1] = HASH_CODE0;
			mycode[2] = HASH_CODE0;
			mycode[3] = HASH_CODE0;

			len = len0 + len1;
			for (i = 0; i < (U32)len; i++) {
				datain[i] = datain[i] ^ mycode[i%4];
			}

			memset(sha256, 0, 32);

			h = Sha256_create();
			if (h != NULL) {
				Sha256_Init(h);
				Sha256_Update(h, (U08 *)datain, len);
				Sha256_Final(h, (U08 *)sha256);
				Sha256_delete(h);
			}
		}
		res = 1;
	} else {
		res = 0;
	}
	return res;
}




/*!
 ********************************************************************************
 *	@brief      Get HDD serial string with drivenum
 *
 *  @param[out]	hddseiral
 *              HDD serial string buffer
 *  @param[in]	drivenum
 *              Driver Number
 *  @param[in]	maxlen
 *              string buffer maximum length
 *
 *  @return		0: FAIL, 1: GOOD.
 *
 *	@author	    yhsuk
 *  @date       2015/12/05
 *******************************************************************************/
int hddSerialRead(U08 hddserial[], int drivenum, int maxlen)
{
	int res;
	//U08 mycode[4];
	//int len0;
	//int i;

	//--------------
//	// HDDSERIAL XOR codes... for ZCAM
//#define HDDSERIAL_CODE0		0x7F
//#define HDDSERIAL_CODE1		0x7E
//#define HDDSERIAL_CODE2		0x7D
//#define HDDSERIAL_CODE3		0x7C
//	mycode[0] = HDDSERIAL_CODE0;
//	mycode[1] = HDDSERIAL_CODE1;
//	mycode[2] = HDDSERIAL_CODE2;
//	mycode[3] = HDDSERIAL_CODE3;

	res =  hddSerialRead0(hddserial, drivenum, maxlen);
	if (res == 0) {
		//fail.
	} else {
#if 0
		len0 = strlen((const char *)hddserial);
		for (i = 0; i < len0; i++) {
			hddserial[i] = hddserial[i] ^ mycode[i%4];
		}
#endif
	}

	return res;
}


static int hddSerialRead0(U08 hddserial[], int drivenum, int maxlen)
{
	int res;
//	int done = FALSE;
#if defined(_WIN32)
	HANDLE hPhysicalDriveIOCTL = 0;

	//  Try to get a handle to PhysicalDrive IOCTL, report failure
	//  and exit if can't.
	//char driveName [256];
	TCHAR driveName [256];

	//sprintf (driveName, "\\\\.\\PhysicalDrive%d", drive);
	//sprintf_s(driveName, _TRUNCATE, L"\\\\.\\PhysicalDrive%d", drivenum);
#pragma warning(disable:4996)
	swprintf(driveName, L"\\\\.\\PhysicalDrive%d", drivenum);
#pragma warning(default:4996)

	//  Windows NT, Windows 2000, Windows XP - admin rights not required
	hPhysicalDriveIOCTL = CreateFile (driveName, 0,
			FILE_SHARE_READ | FILE_SHARE_WRITE, NULL,
			OPEN_EXISTING, 0, NULL);

	if (hPhysicalDriveIOCTL == INVALID_HANDLE_VALUE) {
		res = 0;
		goto func_exit;
	} else {
		STORAGE_PROPERTY_QUERY query;
		DWORD cbBytesReturned = 0;
		char buffer [1024];

		memset ((void *) & query, 0, sizeof (query));
		query.PropertyId = StorageDeviceProperty;
		query.QueryType = PropertyStandardQuery;

		memset (buffer, 0, sizeof (buffer));

		if ( DeviceIoControl (hPhysicalDriveIOCTL, IOCTL_STORAGE_QUERY_PROPERTY,
					& query,
					sizeof (query),
					& buffer,
					sizeof (buffer),
					& cbBytesReturned, NULL) )
		{         
			STORAGE_DEVICE_DESCRIPTOR * descrip = (STORAGE_DEVICE_DESCRIPTOR *) & buffer;
			char serialNumber [1024];
			char modelNumber [1024];
			char vendorId [1024];
			//char productRevision [1000];

			flipAndCodeBytes (buffer,
					descrip -> VendorIdOffset,
					0, vendorId );
			flipAndCodeBytes (buffer,
					descrip -> ProductIdOffset,
					0, modelNumber );
			//flipAndCodeBytes (buffer,
			//            descrip -> ProductRevisionOffset,
			//            0, productRevision );
			flipAndCodeBytes (buffer,
					descrip -> SerialNumberOffset,
					1, serialNumber );

			serialNumber[sizeof(serialNumber)-1] = 0;
			{
				int len;

				len = sizeof(serialNumber);
				if (maxlen <= len) {
					len = maxlen;
				}
				memcpy(hddserial, serialNumber, len);
			}
			res = 1;
		} else {
			res = 0;
		}
		CloseHandle (hPhysicalDriveIOCTL);
	}
func_exit:
	return res;
#else
	// TODO: LINUX_PORT
	strcpy((char*)hddserial, "0123456789");
	res = 1;
	return res;
#endif
}



#if defined(_WIN32)
static char * flipAndCodeBytes(const char * str, int pos, int flip, char * buf)
{
   int i;
   int j = 0;
   int k = 0;

   buf [0] = '\0';
   if (pos <= 0)
      return buf;

   if ( ! j)
   {
      char p = 0;

      // First try to gather all characters representing hex digits only.
      j = 1;
      k = 0;
      buf[k] = 0;
      for (i = pos; j && str[i] != '\0'; ++i)
      {
	 char c = (char)tolower(str[i]);

	 if (isspace(c))
	    c = '0';

	 ++p;
	 buf[k] <<= 4;

	 if (c >= '0' && c <= '9')
	    buf[k] |= (unsigned char) (c - '0');
	 else if (c >= 'a' && c <= 'f')
	    buf[k] |= (unsigned char) (c - 'a' + 10);
	 else
	 {
	    j = 0;
	    break;
	 }

	 if (p == 2)
	 {
	    if (buf[k] != '\0' && ! isprint((int)(unsigned char)buf[k]))
	    {
	       j = 0;
	       break;
	    }
	    ++k;
	    p = 0;
	    buf[k] = 0;
	 }

      }
   }

   if ( ! j)
   {
      // There are non-digit characters, gather them as is.
      j = 1;
      k = 0;
      for (i = pos; j && str[i] != '\0'; ++i)
      {
	     char c = str[i];

	     if ( ! isprint(c))
	     {
	        j = 0;
	        break;
	     }

	     buf[k++] = c;
      }
   }

   if ( ! j)
   {
      // The characters are not there or are not printable.
      k = 0;
   }

   buf[k] = '\0';

   if (flip)
      // Flip adjacent characters
      for (j = 0; j < k; j += 2)
      {
	     char t = buf[j];
	     buf[j] = buf[j + 1];
	     buf[j + 1] = t;
      }

   // Trim any beginning and end space
   i = j = -1;
   for (k = 0; buf[k] != '\0'; ++k)
   {
      if (! isspace(buf[k]))
      {
	     if (i < 0)
	        i = k;
	     j = k;
      }
   }

   if ((i >= 0) && (j >= 0))
   {
      for (k = i; (k <= j) && (buf[k] != '\0'); ++k)
         buf[k - i] = buf[k];
      buf[k - i] = '\0';
   }

   return buf;
}
#endif

/*!
 ********************************************************************************
 *	@brief      Get baseboard serial and Bios serial string
 *
 *  @param[out]	baseboardserial
 *              base board (Mother board) serial string buffer
 *  @param[out]	biosserial
 *              BIOS serial string buffer
 *  @param[in]	maxlen
 *              string buffer maximum length
 *
 *  @return		0: FAIL, 1: GOOD.
 *
 *	@author	    yhsuk
 *  @date       2015/12/05
 *******************************************************************************/

int baseBoardBiosSerialRead(U08 baseboardserial[], U08 biosserial[], int maxlen)
{
	int res;
#if defined(_WIN32)	
	char *szBiosSN;
	CWQL myWQL;
	int len;

	//---

	
	
	
	//----
	
	if(!myWQL.connect(WMI_LOCALHOST, WMI_DEFAULTNAME, WMI_DEFAULTPW))
	{
		//Connection failed!
		res = 0;
		goto func_exit;
	}

	{
		CWQL::RowSet rs;
		if(!myWQL.getClassProperties(  L"Win32_Bios", rs ))
		{
			//WQL Execute failed!
			res = 0;
			goto func_exit;
		}
//#define MAXBIOSSN	1024
#define MAXBIOSSN	32
		szBiosSN = (char *) malloc(MAXBIOSSN);
		if (szBiosSN == NULL) {
			res = 0;
			goto func_exit;
		}
		if (maxlen > MAXBIOSSN) {
			len =  MAXBIOSSN;
		} else {
			len = maxlen;
		}
		memset(szBiosSN, 0, MAXBIOSSN);

#pragma warning(disable:4996)
		sprintf(szBiosSN,"%ls", (rs[0][L"SerialNumber"]).c_str());
		*(szBiosSN + MAXBIOSSN-1) = 0;			// NULL-terminated..
		memcpy(baseboardserial, szBiosSN, len);
		//printf("[BIOS SerialNumber] : %s\n", szBiosSN);

		if(!myWQL.getClassProperties(  L"Win32_BaseBoard", rs ))
		{
			//WQL Execute failed!
			free (szBiosSN);
			res = 0;
//			myWQL.close();
			goto func_exit;
		}
		memset(szBiosSN, 0, MAXBIOSSN);

		sprintf(szBiosSN,"%ls", (rs[0][L"SerialNumber"]).c_str());
		*(szBiosSN + MAXBIOSSN-1) = 0;			// NULL-terminated..
		memcpy(biosserial, szBiosSN, len);
		//printf("[Win32_BaseBoard] : %s\n", szBiosSN);
		free (szBiosSN);
		res = 1;
//		myWQL.close();
	}

func_exit:
	return res;

#pragma warning(default:4996)

#else
	// TODO: LINUX_PORT
	strcpy((char*)baseboardserial, "0123456789");
	strcpy((char*)biosserial, "0123456789");

	res = 1;
	return res;
#endif

}



U32  security_hboardinfo_read(HAND h, U08 *buf, I32 *pstatus)
{
	U32 res;
	iana_security_t *pas;

	//------
	if (h == NULL) {
		res = 0;		// BAD..
		goto func_exit;
	}

	pas = (iana_security_t *) h;

	res = scamif_hboardinfo_read(pas->hscamif, 0, (U32 *)buf, sizeof(hboardinfo_t));

	UNUSED(pstatus);
func_exit:
	return res;
}


U32  security_hboardinfo_write(HAND h, U08 *buf, I32 *pstatus)
{
	U32 res;
	iana_security_t *pas;

	//------
	if (h == NULL) {
		res = 0;		// BAD..
		goto func_exit;
	}

	pas = (iana_security_t *) h;

	res = scamif_hboardinfo_write(pas->hscamif, 0, (U32 *)buf, sizeof(hboardinfo_t));

	UNUSED(pstatus);
func_exit:
	return res;
}


U32  security_pcinfo_read(HAND h, void *buf)
{
	U32 res;
	iana_security_t *pas;

	//------
	if (h == NULL) {
		res = 0;		// BAD..
		goto func_exit;
	}

	pas = (iana_security_t *) h;

	res = scamif_pcinfo_read(pas->hscamif, 0, (U32 *)buf, sizeof(pcinfocode_t));

func_exit:
	return res;
}

U32  security_pcinfo_write(HAND h, void *buf)
{
	U32 res;
	iana_security_t *pas;

	//------
	if (h == NULL) {
		res = 0;		// BAD..
		goto func_exit;
	}

	pas = (iana_security_t *) h;

	res = scamif_pcinfo_write(pas->hscamif, 0, (U32 *)buf, sizeof(pcinfocode_t));

func_exit:
	return res;
}


U32  security_pcinfo_make(HAND h, void *buf)
{
	U32 res;

	pcinfocode_t pif;

	char bdserial[40];
	char hddserial[40];
	char sha256[32];

	//------
	res = 0;		// BAD..
	if (h == NULL) {
		goto func_exit;
	}

	if (baseBoardSerialHddReadHash((U08 *)bdserial, (U08 *)hddserial, (U08 *)sha256) == 1) {
		U32 cpuid[4];

		res = security_cpusid_read(h, cpuid);
		if (res) {
			U32 crcvalue;
			pcinfocode_core(&pif.code[0], (U08 *)sha256, cpuid);

			crcvalue = cr_crc32(0, (U08 *)&pif, sizeof(pcinfocode_t) - sizeof(int));
			pif.crc = crcvalue;
			memcpy(buf, &pif, sizeof(pcinfocode_t));
			res = 1;
		}
	}
func_exit:

	return res;
}


U32  security_runcode_read(HAND h, void *buf)
{
	U32 res;
	iana_security_t *pas;

	//------
	if (h == NULL) {
		res = 0;		// BAD..
		goto func_exit;
	}

	pas = (iana_security_t *) h;

	res = scamif_runcode_read(pas->hscamif, 0, (U32 *)buf, sizeof(runcode_t));

func_exit:
	return res;
}

U32  security_runcode_write(HAND h, void *buf)
{
	U32 res;
	iana_security_t *pas;

	//------
	if (h == NULL) {
		res = 0;		// BAD..
		goto func_exit;
	}

	pas = (iana_security_t *) h;

	res = scamif_runcode_write(pas->hscamif, 0, (U32 *)buf, sizeof(runcode_t));

func_exit:
	return res;
}


U32  security_runcode_check(HAND h, U32 *presult)
{
	U32 res;
	pcinfocode_t	pic;
	U32 cpuid[4];

	runcode_t	runcode0;
	runcode_t	runcode1;

	//---
	res = security_pcinfo_read(h, &pic);
	if (res) {
		res = security_cpusid_read(h, cpuid);
		if (res) {
			runcode_core(&runcode0.code[0], (U08 *)&pic.code[0], cpuid);
			res = security_runcode_read(h, &runcode1);
			if (res) {
				if (memcmp(&runcode0.code[0], &runcode1.code[0], sizeof(U08)*32) == 0) {
					*presult = 1;				// good. 
				} else {
					*presult = 0;				// BAD..
				}
			}
		}
	}

	return res;
}

U32 security_runcode_update(HAND h, U32 good1bad0)
{
	I32 res;
	pcinfocode_t	pic;
	U32 cpuid[4];
	runcode_t	rcd;

	//---
	res = 0;
	res = security_pcinfo_read(h, &pic);
	if (res) {
		res = security_cpusid_read(h, cpuid);
		if (res) {
			runcode_core(&rcd.code[0], (U08 *)&pic.code[0], cpuid);
			if (good1bad0 != 1) {
				U32 i;
				for (i = 0; i < 32; i++) {
					rcd.code[i] = (U08) (rand() & 0xFF);			// -_-;
				}
			}
			res = security_runcode_write(h, &rcd);
		}
	}

	return res;
}



U32  security_cpusid_read(HAND h, void *buf)
{
	U32 res;
	iana_security_t *pas;

	//------
	if (h == NULL) {
		res = 0;		// BAD..
		goto func_exit;
	}

	pas = (iana_security_t *) h;

	res = scamif_cpusid_read(pas->hscamif, 0, (U32 *)buf, sizeof(cpusid_t));

func_exit:
	return res;
}

U32  security_cpusid_write(HAND h, void *buf)
{
	U32 res;
	iana_security_t *pas;

	//------
	if (h == NULL) {
		res = 0;		// BAD..
		goto func_exit;
	}

	pas = (iana_security_t *) h;

	res = scamif_cpusid_write(pas->hscamif, 0, (U32 *)buf, sizeof(cpusid_t));

func_exit:
	return res;
}


U32  security_cpubdserial_read(HAND h, void *buf)
{
	U32 res;
	iana_security_t *pas;

	//------
	if (h == NULL) {
		res = 0;		// BAD..
		goto func_exit;
	}

	pas = (iana_security_t *) h;

	res = scamif_cpubdserial_read(pas->hscamif, 0, (U32 *)buf, 16);

func_exit:
	return res;
}





U32 pcinfocode_core(
		U08 code[32], 				// Output code.         32byte = 8 * 4byte
		U08 hash[32], 				// Input Hash code.     32byte = 8 * 4byte
		U32 cpuid[4]				// Input cpuid code.    16byte = 4 * 4byte
)
{
	U32 i;
	U32 *pout;
	U32 *pin0;
	U32 *pin1;
	
	//--
	pout = (U32 *) &code[0];
	pin0 = (U32 *) &hash[0];
	pin1 = (U32 *) &cpuid[0];
	for (i = 0; i < 8; i++) {
		*(pout+i) = *(pin0 + i) ^ *(pin1 + (i % 4));
	}

	return 1;
}

U32 runcode_core(U08 runcode[32], U08 pcinfocode[32], U32 cpusid[4]) 
{
	char bdserial[40];
	char hddserial[40];
	char sha256[32];
	char bytevalue;

	U32 i;
	U32 j;
	U32 *ptr0, *ptr1;
	ptr0 = (U32 *) &pcinfocode[0];
	ptr1 = (U32 *) &runcode[0];
	
	for (i = 0; i < 8; i++) {
		j = ((4 + 3 - i) ) % 4;
		*(ptr1+i) = *(ptr0+i) ^ cpusid[j];
	}
	baseBoardSerialHddReadHash((U08*)bdserial, (U08*)hddserial, (U08 *)sha256);

	bytevalue = 0;
	for (i = 0; i < 32; i++) {
		bytevalue ^= sha256[i];
	}

	for (i = 0; i < 32; i++) {
		runcode[i] ^= bytevalue;
	}

	return 1;
}




#if defined (__cplusplus)
}
#endif


