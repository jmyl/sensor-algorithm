/*!
 *******************************************************************************
                                                                                
                    Creatz Iana Interface
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2019 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_security_implement.h
	 @brief  
	 @author YongHo Suk
	 @date   2019/04/02 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_SECURITY_IMPLEMENT_H_)
#define		 _IANA_SECURITY_IMPLEMENT_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include "cr_common.h"
//#include "cr_thread.h"
#include "cr_osapi.h"
#include "crgev.h"
/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif
typedef struct _iana_security {
	U32 good;
	HAND hscamif;
} iana_security_t;


typedef struct _hboardinfo {
	U32 guid[4];			// 16 byte, 4 int  == Hboard serial
	U32 mcpuid[4];			// 16 byte, 4 int
	U08 barcode[32];		// 32 byte, 8 int
	U32 vendorId;			//  4 byte, 1 int
	U32 rsvd[2];			//  8 byte, 2 int
	U32 crc;				//  4 byte, 1 int
} hboardinfo_t;    // Total 20 int == 80 byte

typedef struct _pcinfocode {
 U08 code[32];   // 32 byte, 8 int
 U32 crc;    //  4 byte, 1 int
} pcinfocode_t;     // Total 9 int ==  36 byte

typedef struct _runcode {
 U08 code[32];   // 32 byte, 8 int
 U32 crc;    //  4 byte, 1 int
} runcode_t;     // Total 9 int ==  36 byte

typedef struct _cpusid {
	U32 mcpuid[4];			// 16 byte, 4 int
} cpusid_t;    // Total 4 int == 16 byte


typedef struct _boarddata {
	U32 sync;                   // 0x900dfee1.   
	U32 slotnum;              // slot number.
	hboardinfo_t hb;        
	U32 reserved1[10];			// 128 byte align.
	pcinfocode_t pic;
	U32 reserved2[7];			// 192 byte align.
	runcode_t rc;
	U32 reserved3[6];			// (256-4) = 252 byte align.
	U32 crc32;
} boarddata_t;				// sizeof (boarddata_t) = 256  :P

typedef struct _boarddatarsvd {
	U32 reserved1[10];			// 10 * 4
	U32 reserved2[7];			//  7 * 4
	U32 reserved3[6];			//  6 * 4
} boarddatarsvd_t;				// sizeof (boarddatarsvd_t) = (23 * 4 =  92) :P



/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
U32 baseBoardSerialHddReadHash(U08 bdserial[32], U08 hddserial[32], U08 sha256[32]);

int hddSerialRead(U08 hddserial[], int drivenum, int maxlen);
int baseBoardBiosSerialRead(U08 baseboardserial[], U08 biosserial[], int maxlen);

U32  security_hboardinfo_read(HAND h, U08 *buf, I32 *pstatus);
U32  security_hboardinfo_write(HAND h, U08 *buf, I32 *pstatus);

U32 security_pcinfo_read(HAND h, void *hhbi);
U32 security_pcinfo_write(HAND h, void *hhbi);
U32 security_pcinfo_make(HAND h, void *hpif);

U32  security_runcode_read(HAND h, void *buf);
U32  security_runcode_write(HAND h, void *buf);
U32  security_runcode_check(HAND h, U32 *presult);
U32  security_runcode_update(HAND h, U32 good1bad0);
U32  security_cpusid_read(HAND h, void *buf);
U32  security_cpubdserial_read(HAND h, void *buf);
#if defined (__cplusplus)
}
#endif

#endif		// _IANA_SECURITY_IMPLEMENT_H_

