/*!
 *******************************************************************************
                                                                                
                    Creatz Iana Tool
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_tool.h
	 @brief  
	 @author YongHo Suk
	 @date   2016/02/21 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_TOOL_H_)
#define		 _IANA_TOOL_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#define MYDOTGRAY(x, y, value, buf, width)		(*(U08 *)((buf) + (x) + (y) * (width)) = (value))

#define MKBGR(b,g,r)	(unsigned int)				\
	(												\
		(											\
			((unsigned int)(b) << 16) & 0xFF0000	\
		)											\
		|											\
		(											\
			((unsigned int)(g) <<  8) & 0x00FF00	\
		)											\
		|											\
		(											\
			((unsigned int)(r) <<  0) & 0x0000FF	\
		)											\
	)
/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

#if defined(__linux__) /* BMP header for Linux */
typedef struct tagBITMAPFILEHEADER { 
  WORD    bfType; 
  DWORD   bfSize; 
  WORD    bfReserved1; 
  WORD    bfReserved2; 
  DWORD   bfOffBits; 
} BITMAPFILEHEADER, *PBITMAPFILEHEADER;

typedef struct tagBITMAPINFOHEADER{
  DWORD  biSize; 
  LONG   biWidth; 
  LONG   biHeight; 
  WORD   biPlanes; 
  WORD   biBitCount; 
  DWORD  biCompression; 
  DWORD  biSizeImage; 
  LONG   biXPelsPerMeter; 
  LONG   biYPelsPerMeter; 
  DWORD  biClrUsed; 
  DWORD  biClrImportant; 
} BITMAPINFOHEADER, *LPBITMAPINFOHEADER, *PBITMAPINFOHEADER; 


typedef struct tagRGBQUAD {
  BYTE    rgbBlue; 
  BYTE    rgbGreen; 
  BYTE    rgbRed; 
  BYTE    rgbReserved; 
} RGBQUAD; 

#define BI_RGB 0
#endif

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

void mycircle(int xc, int yc, int r, int rgb, U08 *buf, int width);
void mycirclepixel(int xc,int yc,int x,int y, U08 rr, U08 gg, U08 bb, U08 *buf, int width);
void mycircleputpixel(int x,int y, U08 rr, U08 gg, U08 bb, U08 *buf, int width);
void mycircleGray(int xc, int yc, int r, U08 value, U08 *buf, int width);
void mycirclepixelGray(int xc, int yc, int x, int y, U08 value, U08 *buf, int width);
void mycircleputpixelGray(int x, int y, U08 value, U08 *buf, int width);

void mycircleGrayInv(int xc, int yc, int r, U08 *buf, int width);
void mycirclepixelGrayInv(int xc, int yc, int x, int y, U08 *buf, int width);
void mycircleputpixelGrayInv(int x, int y, U08 *buf, int width);


void mycircleGray2(int xc, int yc, int r, U08 value, U08 *buf, int width);					// Circle with Full-out
void mycircleGray3(int xc, int yc, int r0, int r1, U08 value, U08 *buf, int width);
void mycirclepixelGray2(int xc, int yc, int x, int y, int r, U08 value, U08 *buf, int width);
void mycircleputpixelGray2(int x, int yc, int y, int r, U08 value, U08 *buf, int width);

void mycircleGray4(int xc, int yc, int r, U08 value, U08 *buf, int width);					// Circle with Full-in
void mycirclepixelGray4(int xc, int yc, int x, int y, U08 value, U08 *buf, int width);
void mycircleputpixelGray4(int x, int yc, int y, U08 value, U08 *buf, int width);

void myrectpixelGray(int sx, int sy, int ex, int ey, U08 value, U08 *buf, int width);

void mydrawpolylineGray(int x[], int y[], int pointcount, U08 value, U08 *buf, int width);
void mydrawpolygonGray(int x[], int y[], int pointcount, U08 value, U08 *buf, int width);





void mycircleBGR(int xc, int yc, int r, U32 bgrvalue, U08 *buf, int width);
void mycirclepixelBGR(int xc, int yc, int x, int y, U32 bgrvalue, U08 *buf, int width);
void mycircleputpixelBGR(int x, int y, U32 bgrvalue, U08 *buf, int width);

void mycircleBGRInv(int xc, int yc, int r, U08 *buf, int width);
void mycirclepixelBGRInv(int xc, int yc, int x, int y, U08 *buf, int width);
void mycircleputpixelBGRInv(int x, int y, U08 *buf, int width);


void mycircleBGR2(int xc, int yc, int r, U32 bgrvalue, U08 *buf, int width);					// Circle with Full-out
void mycircleBGR3(int xc, int yc, int r0, int r1, U32 bgrvalue, U08 *buf, int width);
void mycirclepixelBGR2(int xc, int yc, int x, int y, int r, U32 bgrvalue, U08 *buf, int width);
void mycircleputpixelBGR2(int x, int yc, int y, int r, U32 bgrvalue, U08 *buf, int width);

void mycircleBGR4(int xc, int yc, int r, U32 bgrvalue, U08 *buf, int width);					// Circle with Full-in
void mycirclepixelBGR4(int xc, int yc, int x, int y, U32 bgrvalue, U08 *buf, int width);
void mycircleputpixelBGR4(int x, int yc, int y, U32 bgrvalue, U08 *buf, int width);


void mylineGray(int x1, int y1, int x2, int y2, U08 value, U08 *buf, int width);
void mylineGrayH(int x1, int y1, int x2, int y2, U08 value, U08 *buf, int width, int lineh);
void mylineGrayV(int x1, int y1, int x2, int y2, U08 value, U08 *buf, int width, int linev);
void myrectGray(int x1, int y1, int x2, int y2, U08 value, U08 *buf, int width);

void mylineBGR(int x1, int y1, int x2, int y2, U32 bgrvalue, U08 *buf, int width);
void mylineBGRH(int x1, int y1, int x2, int y2, U32 bgrvalue, U08 *buf, int width, int lineh);
void mylineBGRV(int x1, int y1, int x2, int y2, U32 bgrvalue, U08 *buf, int width, int lineV);
void myrectBGR(int x1, int y1, int x2, int y2, U32 bgrvalue, U08 *buf, int width);
//void mydotBGR(int x, int y, U32 bgrvalue, U08 *buf, int width);


//void mydotGray(int x, int y, U08 value, U08 *buf, int width);


void mycircleBGRAdd4(int xc, int yc, int r, U32 bgrvalue, U08 *buf, int width);					// Circle with Full-in
void mycirclepixelBGRAdd4(int xc, int yc, int x, int y, U32 bgrvalue, U08 *buf, int width);
void mycircleputpixelBGRAdd4(int x, int yc, int y, U32 bgrvalue, U08 *buf, int width);


void mydrawpolylineBGR(int x[], int y[], int pointcount, U32 bgrvalue, U08 *buf, int width);
void mydrawpolygonBGR(int x[], int y[], int pointcount, U32 bgrvalue, U08 *buf, int width);


//--
//void saveimageprepare(char filepath[1024], U32 goodshot, U32 checkme);
void saveimageprepare(char *dirpath, char *filepath, U32 goodshot, U32 checkme);
void saveimage(char path[], char *buf, int width, int height, int camid, int cnt);
void saveimage2(char path[], char *buf, int width, int height, int camid, int cnt);
void saveimage3(char path[], char prefix[], char *buf, int width, int height, int camid, int cnt, char *filename);
void saveimageJpeg(HAND hjpeg, U32 qvalue, char path[], char prefix[], char *buf, int width, int height, int camid, int cnt,  char *filename);
void saveimageJpeg2(HAND hjpeg, U32 qvalue, char path[], char filename[], char *buf, int width, int height);
void saveimageJpegRGB(HAND hjpeg, U32 qvalue, char path[], char filename[], char *rbgbuf, int width, int height);
void saveimageJpegBGR(HAND hjpeg, U32 qvalue, char path[], char filename[], char *rbgbuf, int width, int height);
void saveimageBGR(char path[], char *filename, char *buf, int width, int height);
void saveimageRGB(char path[], char *filename, char *buf, int width, int height);

void bmpout(char *fn, char *buf, int width, int height, int isgray, int isencrypt);
U32 bmpGrayLoad(char *fn, char *buf, int width, int height);
U32 jpegLoad(HAND hjpeg, char *fn, char *buf, int width, int height);		// Load and decode.
U32 jpegLoad2(HAND hjpeg, char *fn, char *buf, int *pwidth, int *pheight);		// Load and decode.

void histogram_equalization(U08 *bufsrc, U08 *bufdest, U32 width, U32 height);
void gamma_correction(U08 *bufsrc, U08 *bufdest, U32 width, U32 height);
//----------------------------------------------------------------------------------------


I32 getMulcScale(float fmulc, U32 *pmulc, U32 *pscale);

U32 iana_readinfo_filename(HAND hiana, const char *infofile);
U32 iana_simresult_filename(HAND hiana, const char *resultfile);

I32 iana_ballradius(iana_t *piana, U32 camid, cr_point_t *pBp, double *pPr, double *pLr);

#if defined (__cplusplus)
}
#endif

#endif		// _IANA_TOOL_H_

