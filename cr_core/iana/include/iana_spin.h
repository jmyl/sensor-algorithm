/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA shot
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_spin.h
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2016/02/26 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_SPIN_H_)
#define		 _IANA_SPIN_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#define     MARKSTATE_EMPTY		0x0000
#define     MARKSTATE_ALIVE		0x0001
#define     MARKSTATE_CLOSED	0x0002

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/
I32 iana_spin(iana_t *piana);
#if _MSC_VER == 1400									// visual studio 2005
I32 iana_spin_mark(iana_t *piana);
#endif

I32 iana_spin_check(iana_t *piana);
I32 iana_spin_mark_spin_check(iana_t *piana);
I32 iana_clubimage_fit(iana_t *piana);

#if defined (__cplusplus)
}
#endif

#endif		// _IANA_SPIN_H_
