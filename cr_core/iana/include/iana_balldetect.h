/*!
*******************************************************************************
                                                                                
                CREATZ IANA ball detect
                                                                                
  	@section copyright_notice COPYRIGHT NOTICE
  	Copyright (c) 2015 by Creatz Inc. 
  	All Rights Reserved. \n
  	Do not duplicate without prior written consent of Creatz Inc.

*******************************************************************************
  	@section file_information FILE CREATION INFORMATION
	@file   iana_balldetect.h
	@brief  detect ball-like objects using several circle detection algorithms
	@author Original: by yhsuk
	@date   2016/02/21 First Created

	@section checkin_information LATEST CHECK-IN INFORMATION
    $Rev$ 
    $Author$
    $Date$
*******************************************************************************/
#if !defined(_IANA_BALLDETECT_H_)
#define		 _IANA_BALLDETECT_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

I32 iana_getballcandidator(
		iana_t *piana, U32 camid, U08 *buf, 
		U32 width, U32 height, 
		U32 width2,
		iana_ballcandidate_t bc[MAXCANDIDATE], U32 *pbcount, 
		U32 maxth, U32 minth, U32 multv, U32 expn, double cermax);

I32 iana_getballcandidatorAdapt(
		iana_t *piana, U32 camid, U08 *buf, 
		U32 width, U32 height, 
		U32 width2,
		iana_ballcandidate_t bc[MAXCANDIDATE], U32 *pbcount, 
		U32 halfWinsize,
		U32 multv, U32 expn,
		double stdmult,
		double cermax
		, double cutmeanmult    //20190420, yhsuk.
		);

I32 iana_getballcandidatorCanny(
		iana_t *piana, U32 camid, U08 *buf, 
		U32 width, U32 height, 			// ROI width
		U32 width2,						// Image width for buf
		iana_ballcandidate_t bc[MAXCANDIDATE], U32 *pbcount,
		U32 maxth,
		U32 minth,
		U32 multv,
		U32 nexp,
		double cermax
		);

I32 iana_getballcandidator_cht(
		iana_t *piana, U32 camid, U08 *buf, 
		U32 width, U32 height, 			// ROI width
		U32 width2,						// Image width for buf
		iana_ballcandidate_t bc[MAXCANDIDATE], U32 *pbcount,
		U32 halfWinSize,
		U32 multv,
		U32 nexp,
		double stdmult,
		double cermax
		, double cutmeanmult    //20190420, yhsuk.
		);


double IANA_BALLDETECT_GetMaxCircleRatio(I32 camsensor_category, U32 camid);
double IANA_BALLDETECT_GetMaxCircleRatio2(I32 camsensor_category, U32 camid);
double IANA_BALLDETECT_GetMinCircleRatio(I32 camsensor_category, U32 camid);



#if defined (__cplusplus)
}
#endif

#endif		// _IANA_BALLDETECT_H_
