/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA shot
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_spin_rotation.h
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2016/02/27 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_SPIN_ROTATION_H_)
#define		 _IANA_SPIN_ROTATION_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/
I32 iana_spin_rotation(iana_t *piana);




I32 calcRodriguesMatrix(cr_point_t *pcposL, cr_point_t *pbposL, double rrm[][3], double rrma[][3], cr_vector_t *psrv, double     *psrt);
I32 calcRodriguesMatrixA(cr_point_t *pcposL, cr_point_t *pbposL, double rrm[][3], cr_vector_t *psrv, double     *psrt);
I32 calcRodriguesMatrix2(
	cr_point_t *pcposL,
	cr_point_t *pbposL,
	double rrm[][3],
	cr_vector_t *psrv,
	double     *psrt
	);

I32 rodriguesMatrix(cr_vector_t *pua, double theta, double rrm[][3]);
I32 rodriguesMatrixInv(double rrm[][3], cr_vector_t *pua, double *ptheta);
I32 rodriguesRotate(double rrm[][3], cr_vector_t *pvfrom, cr_vector_t *pvto);
I32 rotateV(double rrm[][3], cr_vector_t *pvfrom, cr_vector_t *pvto);
#if defined (__cplusplus)
}
#endif

#endif		// _IANA_SPIN_ROTATION_H_
