/*!
 *******************************************************************************
                                                                                
                   CREATZ IANA KALMAN filter.
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2020 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_kalman.h
	 @brief  
	 @author YongHo Suk
	 @date   2020/02/24 Spin calc.

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_KALMAN_H_)
#define		 _IANA_KALMAN_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#define 	KFTYPE_NULL	0
#define 	KFTYPE_CP	1					// Constant Position
#define 	KFTYPE_CV	2					// Constant Velocity
#define 	KFTYPE_CA	3					// Constant Acceleration


#define 	KFSTATE		9					// 3 (pos) + 3 (vel) + 3 (acc)
#define 	KFOBSERVE	3					// 3 (pos)

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/
typedef struct iana_kalman_ {
	U32 kftype;						// KALMAN FILTER TYPE.
	U32 type32_0_type64_1;
	double state[KFSTATE];			// current state vector    x_k|k
	double state_[KFSTATE];			// projected state vector  x_k|k-1
	double innovation[KFOBSERVE];	// innovation 
	double K[KFSTATE][KFOBSERVE];	// Kalman gain
	double P[KFSTATE][KFSTATE];		// covariance matrix       P_k|k
	double P_[KFSTATE][KFSTATE];	// projected covariance matrix   P_k|k-1
	double F[KFSTATE][KFSTATE];		// state transition matrix
	double H[KFOBSERVE][KFSTATE];	// Observation matrix
	double Q[KFSTATE][KFSTATE];		// state process noise matrix (NULL:?)
	double s2w;			// state process variance
	double R[KFOBSERVE][KFOBSERVE];			// Observation noise matrix	  (NULL:?)
	double s2v;			// observation noise variance
	double dt;
} iana_kalman_t;
/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif






HAND iana_kalman_create();

I32 iana_kalman_delete(HAND hkm);

I32 iana_kalman_init(
		HAND hkm, 
		U32 kftype,
		U32 type32_0_type64_1,	// 0: float, 1: double
		double *state0,		// initial state vector
		double *p0,				// initial covariance matrix
		double *F,				// initial state transition matrix
		double *H,				// initial Observation matrix
		double *Q,				// initial state process noise matrix (NULL:?)
		double s2w,				// initial state process variance
		double *R,				// initial Observation noise matrix	  (NULL:?)
		double s2v,				// initial observation noise variance
		double dt);

I32 iana_kalman_update(HAND hkm, double *observ, U32 bvalid);		// bvalid: observ vector is GOOD.
I32 iana_kalman_make_init(
				double pos[],
				double vel[],
				double acc[],
				double posseq[],
				U32    count,
				double state0[],
				double p0[]);

#if defined (__cplusplus)
}
#endif

#endif		// _IANA_KALMAN_H_

