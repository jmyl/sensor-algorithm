/*!
 *******************************************************************************
                                                                                
                    Creatz Iana Implement
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_implement.h
	 @brief  
	 @author YongHo Suk
	 @date   2015/12/20 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_IMPLEMENT_H_)
#define		 _IANA_IMPLEMENT_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#define	IANA_STATE_NULL				0x0000
#define	IANA_STATE_CREATED			0x0001
#define	IANA_STATE_INITED			0x0002
#define	IANA_STATE_STARTED			0x0003
#define	IANA_STATE_STOPPED			0x0004
#define	IANA_STATE_DELETED			0x0005


#define IANA_RUN_NULL				0x0000
#define IANA_RUN_EMPTY				0x0001
//#define IANA_RUN_PREPARECAM			0x0002
#define IANA_RUN_CAMCHECK			0x0002
#define IANA_RUN_PREPARED			0x0003
#define IANA_RUN_READY				0x0004
#define IANA_RUN_TRANSITORY			0x0005
#define IANA_RUN_GOODSHOT			0x0006
#define IANA_RUN_TRIALSHOT			0x0007
#define IANA_RUN_POWERSAVING		0x0008

//--- Executive Commands
#define		IANA_CMD_EXC_NULL				0x00010000

#define		IANA_CMD_EXC_AEC_SET			0x00010001 	// p0: index, p1: aec
#define		IANA_CMD_EXC_AEC_GET			0x00010002	// p0: index, p1: &aec
#define		IANA_CMD_EXC_EXPOSURE_SET		0x00010003	// p0: index, p1: exposure
#define		IANA_CMD_EXC_EXPOSURE_GET		0x00010004	// p0: index, p1: &exposure

#define		IANA_CMD_EXC_BARWIPEOUT_SET		0x00010005	// p0: index, p1: bar_wipeout(0:no, 1: yes)
#define		IANA_CMD_EXC_BARWIPEOUT_GET		0x00010006	// p0: index, p1: &bar_wipeout

#define		IANA_CMD_EXC_CAMCHECK_SET		0x00010007  // p0: Index, p1: Check camera and reinit if Error detected.. :)
#define		IANA_CMD_EXC_CAMCHECK_GET		0x00010008  // p0, p1, p2: &goodstate of cam0, cam1, cam2

#define		IANA_CMD_EXC_FLIP_SET			0x00010009	// p0: index, p1: flipmode // 0: Horizontal, 1: Vertical, 2: Both,    -1: NO
#define		IANA_CMD_EXC_FLIP_GET			0x0001000A	// p0: index, p1: &flipmode

#define		IANA_CMD_EXC_AGC_SET			0x00010011	// p0: index, p1: agc
#define		IANA_CMD_EXC_AGC_GET			0x00010012	// p0: index, p1: &agc
#define		IANA_CMD_EXC_GAIN_SET			0x00010013	// p0: index, p1: gain
#define		IANA_CMD_EXC_GAIN_GET			0x00010014	// p0: index, p1: &gain

#define		IANA_CMD_EXC_P2LMAT_SET			0x00010020	// p0: index, p1: &bAp2l
#define		IANA_CMD_EXC_L2PMAT_SET			0x00010021	// p0: index, p1: &bAl2p
#define		IANA_CMD_EXC_WRITEP2LL2PMAT		0x00010022	// p0: index
#define		IANA_CMD_EXC_HBOARDINFO_READ	0x00010023	// p0: buf		(size: 80 byte), p1: *pstatus		2015/1205
#define		IANA_CMD_EXC_HBOARDINFO_WRITE	0x00010024	// p0: buf      (size: 80 byte), p1: *pstatus		2015/1205
#define		IANA_CMD_EXC_PCINFOCODE_READ	0x00010025	// p0: pcinfocode output(size: 32 byte),			2016/0115
#define		IANA_CMD_EXC_PCINFOCODE_WRITE	0x00010026	// p0: pcinfocode input (size: 32 byte),			2016/0115
#define		IANA_CMD_EXC_PCINFOCODE_MAKE	0x00010027	// p0: pcinfocode output(size: 32 byte)             2016/0115
#define		IANA_CMD_EXC_RUNCODE_CHECK		0x00010028	// p0: *presult	 (output, 0: BAD, 1: GOOD)			2016/0115
#define		IANA_CMD_EXC_RUNCODE_UPDATE		0x00010029	// p0: runok	 (input,  0: NO RUN. 1: OK. RUN.)	2016/0115

#define		IANA_CMD_EXC_FPS_SET			0x00010030	// p0: index  p1: fps
#define		IANA_CMD_EXC_FPS_GET			0x00010031	// p0: index  &p1: fps

#define		IANA_CMD_EXC_PCINFO_MAINBOARD_SERIAL_READ	0x00010040	// p0: buf (size: 32byte)			2015/1205
#define		IANA_CMD_EXC_PCINFO_HDD_SERIAL_READ		0x00010041	// p0: buf (size; 32byte), p1: drivernum			2015/1205
#define		IANA_CMD_EXC_MAKEGUID						0x00010042	// p0: buf (size: 16byte)			2015/1205
#define		IANA_CMD_EXC_PCINFO_MB_HDD_HASH			0x00010043	// p0: mainboard buf (size; 32byte), p1: hdd serial buf (32byte), p2: HASH buf (32byte)


#define		IANA_CMD_EXC_IMAGE_BUFFER_GET	0x00010100  // p0: index, p1: &buf
#define		IANA_CMD_EXC_IMAGE_WIDTH_GET	0x00010101  // p0: index, p1: &width
#define		IANA_CMD_EXC_IMAGE_HEIGHT_GET	0x00010102  // p0: index, p1: &height
#define		IANA_CMD_EXC_IMAGE_RAWWIDTH_GET	0x00010103  // p0: index, p1: &rawwidth
#define		IANA_CMD_EXC_IMAGE_RAWHEIGHT_GET	0x00010104  // p0: index, p1: &rawheight

#define		IANA_CMD_EXC_IMAGE_DISPLAYBUFFER_GET	0x00010110  // p0: (0: l, 1: c, 2: r) , p1: &buf



#define		IANA_CMD_EXC_SENSOR_START		0x00010200  // 
#define		IANA_CMD_EXC_SENSOR_STOP		0x00010201  // 

#define		IANA_CMD_EXC_COMPRESS_START	0x00010211	// p0: index(x) , p1: (b7:b0) codec (0: NULL, 1: JPEG), (b15:b8): Q value
#define		IANA_CMD_EXC_COMPRESS_STOP		0x00010212	// p0: index(x)

#define		IANA_CMD_EXC_CAPTURE_START		0x00010213	// //////////p0: index(x)
#define		IANA_CMD_EXC_CAPTURE_STOP		0x00010214	// ////////
#define		IANA_CMD_EXC_CAPTURE_SAVE		0x00010215	// ////////

#define		IANA_CMD_EXC_REINIT			0x00010220	

#define		IANA_CMD_EXC_CAMID_GET			0x00010300  //	p0: &camid_left, p1: &camid_center, p2: &camid_right
#define		IANA_CMD_EXC_CAMCOUNT_GET		0x00010301  //  p0: &camcount
#define		IANA_CMD_EXC_TW_SET			0x00010310	// p0: index
#define		IANA_CMD_EXC_REWIND_FILE		0x00010311	// p0: index


#define		IANA_CMD_EXC_CPUIF_PWM			0x00010400	// p0: ch (0 ~ 3), p1: Duty rate (0 ~ 100)
#define		IANA_CMD_EXC_CPUIF_USBONOFF	0x00010401	// p0: ch (1 ~ 4), p1: on/off (0: off, 1: on)
#define		IANA_CMD_EXC_CPUIF_CPUCONNECTIONSTATE	0x00010402	// p0: &connnectionstate
#define		IANA_CMD_EXC_CPUIF_CPUSID_READ	0x00010410	// p0: buf		(len: 16 byte),	p1: *pstatus

#define		IANA_CMD_EXC_XBOARD_CPUSID_READ	IANA_CMD_EXC_CPUIF_CPUSID_READ						// 20190117


#define		IANA_CMD_EXC_CPUIF_SCPU_FLASH_READ		0x00010500	// p0: buf, p1: page_idx, p2: offset (Integer-count), p3: size (integer-count)
#define		IANA_CMD_EXC_CPUIF_SCPU_FLASH_WRITE	0x00010501	// p0: buf, p1: page_idx, p2: offset (Integer-count), p3: size (integer-count)
#define		IANA_CMD_EXC_CPUIF_SCPU_RESET			0x00010502	// p0: mdelay
#define		IANA_CMD_EXC_CPUIF_SCPU_FLASH_WRITE2	0x00010503	// Erase page.... Don't re-write previous data.!~!!!!
			   											// p0: buf, p1: page_idx, p2: offset (Integer-count), p3: size (integer-count)


#define		IANA_CMD_EXC_XBOARD_FLASH_READ		0x00010510	// Read data.                  p0: databuf,  p1: address, p2: size (Byte size)
#define		IANA_CMD_EXC_XBOARD_FLASH_WRITE		0x00010511	// Write data. Need pre-erase. p0: databuf,  p1: writebackbuf, p2: address, p3: size (Byte size)
#define		IANA_CMD_EXC_XBOARD_RESET			0x00010512	// Trivial..
#define		IANA_CMD_EXC_XBOARD_FLASH_WRITE2	0x00010513	// Write data. Don't need pre-erase. p0: databuf,  p1: writebackbuf, p2: address, p3: size (Byte size)
#define		IANA_CMD_EXC_XBOARD_FLASH_WRITE_WITHERASE	IANA_CMD_EXC_XBOARD_FLASH_WRITE2
#define		IANA_CMD_EXC_XBOARD_FLASH_ERASE		0x00010514	// Erase p0: address, p1: size (Byte size)


#define		IANA_CMD_EXC_CAM_RECEIVETIME_GET	0x00010600  // p0: camkind (0: Sidecam, 1: Centercam), p1: &timetick
#define		IANA_CMD_EXC_CAM_CONNECT		0x00010601  // p0: camkind (0: Sidecam, 1: Centercam)
#define		IANA_CMD_EXC_CAM_DISCONNECT	0x00010602  // p0: camkind (0: Sidecam, 1: Centercam)
#define		IANA_CMD_EXC_CAM_FPS_SET		0x00010603  // p0: camkind (0: Sidecam, 1: Centercam), p1: fps


#define		IANA_CMD_EXC_LANCLEAR			0x00010610	// Clear Lan connection... 2018/0608
#define		IANA_CMD_EXC_RESETSENSOR		0x00010611	// Reset Sensor... 20200730
#define		IANA_CMD_EXC_WORKMODE			0x00010620	// p0: (0: right, 1: left), p1: workmode (0: NULL,  1: NORMAL, 2: LOW), p2: fps

//----------------------------------------------------------------------------------------------
#define		IANA_CMD_EXC_IMAGE_CALLBACK		0x00010700	// p0: callback function handle, p1: user param




/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/







#if defined (__cplusplus)
}
#endif

#endif		// _IANA_IMPLEMENT_H_

