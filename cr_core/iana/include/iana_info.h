/*!
 *******************************************************************************
                                                                                
                    Creatz Iana info
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_info.h
	 @brief  shot info
	 @author YongHo Suk
	 @date   2016/03/21 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_INFO_H_)
#define		 _IANA_INFO_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

#define IANA_INFO_NO			0x0000
#define IANA_INFO_INFO_CHECK	0x0001
#define IANA_INFO_INFO_IMAGE	0x0002

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif
typedef struct _iana_info {
		U32 infomode;
		U32 guid[4];
		char infopath[1024];
		U32 fileformat;			// 0: BITMAP, 1: JPEG
		U32 qvalue;				// JPEG Q value  (0 ~ 100)
} iana_info_t;


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

I32 iana_info_init(HAND hiana, U32 goodshot, U32 checkme);
I32 iana_info_start(HAND hiana, U32 goodshot, U32 checkme);
I32 iana_info_camrunconfig(HAND hiana);
I32 iana_info_ready(HAND hiana);
I32 iana_info_shot(HAND hiana);
I32 iana_info_spin(HAND hiana);
I32 iana_info_feature(HAND hiana);
I32 iana_info_shotresult(HAND hiana);

I32 iana_info_end(HAND hiana);



I32 iana_info_feature2(HAND hiana);

void IANA_INFO_CreateFeatureXmlPath(HAND hIana, char *fileName, char *pathName);


#if defined (__cplusplus)
}
#endif

#endif		// _IANA_CAM_H_

