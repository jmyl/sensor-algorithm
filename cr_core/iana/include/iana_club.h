/*!
 *******************************************************************************
                                                                                
                   CREATZ IANA club calc.
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2017 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_club.h
	 @brief  
	 @author YongHo Suk
	 @date   2017/01/02 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
/** @defgroup   IANA_CLUB   IANA_CLUB
    This module detects features on club and computes club informations.
*/
#if !defined(_IANA_CLUB_H_)
#define		 _IANA_CLUB_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*!
********************************************************************************
*  @ingroup    IANA_CLUB
*	@brief      Estimates Club information from ball launch and ball spin informations
*
*  @param[in]	piana
*              IANA module handle
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2017/01/02
*******************************************************************************/
I32 iana_club_estimate(iana_t *piana);


/*!
********************************************************************************
*   @ingroup    IANA_CLUB
*	@brief      The main clubpath calculation function for P3V2. Only images from cam0 is used for calculate clubpath
*               Computes clubpath, clubspeed and then do some fine tuning.
*   @param[in]  piana
*               IANA module handle
*
*   @return		1: good, 0: no-good
*
*	@author	    yhsuk
*   @date       2020/03/20
*******************************************************************************/
I32 iana_club_calc(iana_t *piana);


/*!
******************************************************************************************
*   @ingroup    IANA_CLUB
*   @brief      Estimates ball spin from ball launch and club informations. Only for P3V2
*   @param      piana   IANA module handler
*   @param      camid   camera id
*   @param      angleoffset A paramter to give offset. Currently, 0 is used.
*   @return     1:good, 0:no-good
*   @author     yhsuk, Hyeonseok Choi
*   @date       unknown
********************************************************************************************/
I32 iana_club_spin_estimate(
		iana_t *piana,
		U32 camid,
		double angleoffset		//[degree]
		);

/*!  
*   @ingroup IANA_CLUB
*   @brief
*   @param
*   @return
*   @author
*   @date
*/
I32 IANA_CLUB_RemoveOutlier(double xx[], double yy[], U32 count, U32 *pexileindex);


#if defined (__cplusplus)
}
#endif

#endif		// _IANA_CLUB_H_

