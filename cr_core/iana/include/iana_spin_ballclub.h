/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA shot
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2017 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_spin_ballclub.h
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2017/04/05 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_SPIN_BALLCLUB_H_)
#define		 _IANA_SPIN_BALLCLUB_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/
I32 iana_spin_clubpath(iana_t *piana, U32 normalbulk);
I32 iana_spin_clubimage_overlap(iana_t *piana, U32 camid);
I32 iana_spin_ballclub_check(iana_t *piana);

I32 iana_spin_real_clubdata(iana_t *piana);
#if defined (__cplusplus)
}
#endif

#endif		// _IANA_SPIN_BALLCLUB_H_
