/*!
 *******************************************************************************
                                                                                
                    Creatz Iana cam implement
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_cam_implement.h
	 @brief  
	 @author YongHo Suk
	 @date   2015/12/20 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_IANA_CAM_IMPLEMENT_H_)
#define		 _IANA_CAM_IMPLEMENT_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/
#if 0
static iana_camconf_t s_icconf[IANA_CAMCONF] = 
{
	{ // [0]					: IANA_CAMCONF_EMPTY
		{{ // center
			1280, 	//  width;
			1024, 	// U32 height;
			0,		// U32 offset_x;
			0,		// U32 offset_y;
			2,		// U32 framerate;
			(10 * 256), // U32 gain;
			10,		// U32 exposure;
			1, 		// U32 skip;
			1, 		// U32 multitude;
			1 		// U32 syncdiv
		},
		{	// Side
			1280, 	//  width;
			1024, 	// U32 height;
			0,		// U32 offset_x;
			0,		// U32 offset_y;
			2,		// U32 framerate;
			(10 * 256), // U32 gain;
			10,		// U32 exposure;
			1, 		// U32 skip;
			1, 		// U32 multitude;
			2 		// U32 syncdiv
		}
		}
	},

	{ // [1]					: IANA_CAMCONF_PRERPARECAM
		{{ // center
			1280, 	//  width;
			1024, 	// U32 height;
			0,		// U32 offset_x;
			0,		// U32 offset_y;
			100,		// U32 framerate;
			(10 * 256), // U32 gain;
			20,		// U32 exposure;
			8, 		// U32 skip;
			1, 		// U32 multitude;
			1 		// U32 syncdiv
		},
		{	// Side
			1280, 	//  width;
			1024, 	// U32 height;
			0,		// U32 offset_x;
			0,		// U32 offset_y;
			1,		// U32 framerate;
			(10 * 256), // U32 gain;
			20,		// U32 exposure;
			2, 		// U32 skip;
			4, 		// U32 multitude;
			2 		// U32 syncdiv
		}
		}
	},
	
	{ // [2]					: IANA_CAMCONF_READY
		{{ // center
			320, 	//  width;
			200, 	// U32 height;
			0,		// U32 offset_x;
			0,		// U32 offset_y;
			3000,	// U32 framerate;
			(10 * 256), // U32 gain;
			10,		// U32 exposure;
			4, 		// U32 skip;
			MULTITUDE_READY_CENTER, 		// U32 multitude;
			1 		// U32 syncdiv
		},
		{	// Side
			320, 	//  width;
			200, 	// U32 height;
			0,		// U32 offset_x;
			0,		// U32 offset_y;
			3000,	// U32 framerate;
			(10 * 256), // U32 gain;
			10,		// U32 exposure;
			4, 		// U32 skip;
			MULTITUDE_READY_SIDE, 		// U32 multitude;
			2 		// U32 syncdiv
		}
		}
	},
};

#endif
	


#if defined (__cplusplus)
}
#endif

#endif		// _IANA_CAM_IMPLEMENT_H_

