/*!
 *******************************************************************************
                                                                                
                   CREATZ IANA club data using mark
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2019 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_club_mark.h
	 @brief  
	 @author YongHo Suk
	 @date   2019/12/10 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_CLUB_MARK_H_)
#define		 _IANA_CLUB_MARK_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

typedef enum _clubmark_state {
	CLUBMARK_NULL 		=0,
	CLUBMARK_BAR 		=1,
	CLUBMARK_DOT 		=2,
	CLUBMARK_MBAR	 	=3,
	CLUBMARK_BAR_DOT 	=4,
	CLUBMARK_UNDECIDED 	=5,
	CLUBMARK_EXILE 		=6,


	CLUBMARK_EXILE2		=7,			// FAKE..
	CLUBMARK_EXILE3 	=8			// FAKE..
} clubmark_state_t;

#if 0
#define CLUBMARK_NULL 		0
#define CLUBMARK_BAR 		1
#define CLUBMARK_DOT 		2
#define CLUBMARK_MBAR	 	3
#define CLUBMARK_BAR_DOT 	4
#define CLUBMARK_UNDECIDED 	5
#define CLUBMARK_EXILE 		6


#define CLUBMARK_EXILE2		7			// FAKE..
#define CLUBMARK_EXILE3 	8			// FAKE..
#endif

typedef enum _clubmark_exilewhy {
	CLUBMARK_EXILEWHY_NULL 					=0,
	CLUBMARK_EXILEWHY_TOOSMALL_RECT 		=1,
	CLUBMARK_EXILEWHY_TOUCHED_EDGE 			=2,
	CLUBMARK_EXILEWHY_TOOCLOSE_BALL 		=3,
	CLUBMARK_EXILEWHY_SMALLER_LOWERMAXY1 	=4,
	CLUBMARK_EXILEWHY_SMALLER_LOWERMAXY2 	=5,
	CLUBMARK_EXILEWHY_OUTLIER 				=6,

	CLUBMARK_EXILEWHY_DOT_PROHIBITED		=10,
	CLUBMARK_EXILEWHY_BAR_TOPBAR_SMALLY 	=20,
	CLUBMARK_EXILEWHY_BAR_TOUCHED_EDGE_X 	=21,
	CLUBMARK_EXILEWHY_BAR_TOUCHED_EDGE_Y 	=22,
	CLUBMARK_EXILEWHY_BAR_NOT_HORIZONTAL 	=24,
	CLUBMARK_EXILEWHY_BAR_NOT_VERTICAL 		=24,
	CLUBMARK_EXILEWHY_BAR_TOOCLOSE_BALL 	=25,
	CLUBMARK_EXILEWHY_BAR_OUTLIER			=26,
	CLUBMARK_EXILEWHY_BAR_YOUARERIGHT		=27,
	CLUBMARK_EXILEWHY_BAR_PROHIBITED		=28
} clubmark_exilewhy_t;






#define CLUBMARK_MAXLABEL	250


//#define CLUBMARK_BAR_CLASSCOUNT	5
//#define CLUBMARK_BAR_CLASSCOUNT	8
#define CLUBMARK_BAR_CLASSCOUNT	16
//#define CLUBMARK_BAR_CLASSCOUNT	


#define CLUBMARK_MARKCLASS_NULL			0				// undecided
#define CLUBMARK_MARKCLASS_VERTICAL		1				// vertical
#define CLUBMARK_MARKCLASS_HORIZONTAL	2				// horizontal
#define CLUBMARK_MARKCLASS_UNDECIDED	3				// what?


#define CLUBMARK_REGION_NULL		0
#define CLUBMARK_REGION_BAR			1
#define CLUBMARK_REGION_DOT			2
#define CLUBMARK_REGION_BAR_DOT		3

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif
typedef struct _rectObj {
	U32 pixelcount;				// pixel count inside  rectangluar.
	U32 pixelcount2;			// pixel count inside  region.
	double areav;				// area value.

	point_t p[4];
	cr_line_t vertex[4];		// vertex..
	double vertexlen[4];		// vertex length
	double vertexdist[4];		// distance vertex to next point
	U32 maxvertexlenIndex;
} rectObj_t;



typedef struct _clubmark_seg {
	clubmark_state_t state;					// 0: NULL, 1: BAR, 2: DOT, 3: BAR or DOT, 4: UNDECIDED, 5: EXILE
	clubmark_exilewhy_t exilewhy;						// reasone of exile..
	rectObj_t rectObj;
	imagesegment_t imgseg;
	U32 markclass;
	U32 ccvalid;
	cr_point_t ccP[CLUBMARK_BAR_CLASSCOUNT];		// center of class, P
	cr_point_t ccL[CLUBMARK_BAR_CLASSCOUNT];		// center of class, L

	double  xma[3];			// quad regression coefficient for L.x
	double  yma[3];			// quad regression coefficient for L.y

	double  xma2[2];		// Linear regression coefficient for L.x
	double  yma2[2];		// Linear regression coefficient for L.y

} clubmark_seg_t;

#define MAXCANDIDATE	16
typedef struct _clubmark_dot {
	U32 count;
//	U32 offset_x, offset_y;
	clubmark_seg_t	clm[MAXCANDIDATE];		// points without offset and xfact,yfact
} clubmark_dot_t;

typedef struct _clubmark_bar {
	U32 count;
//	U32 offset_x, offset_y;
	clubmark_seg_t	clm[MAXCANDIDATE];
} clubmark_bar_t;


typedef struct _clubmark_Lp {
	U32 	mavalid;		// regression coefficients are valid..
	double  xma[3];			// quad regression coefficient for L.x
	double  yma[3];			// quad regression coefficient for L.y

	double  xma2[2];		// Linear regression coefficient for L.x
	double  yma2[2];		// Linear regression coefficient for L.y


	double  thma[3];		// quad regression coefficient for atan(1/m) of bar line.
	U32 	valid[MARKSEQUENCELEN];
	point_t Lp[MARKSEQUENCELEN];

} clubmark_Lp_t;



typedef struct _clubmark_p3d {

	U32 	mavalid;		// regression coefficients are valid..
	double  xma[3];			// quad regression coefficient for L.x
	double  yma[3];			// quad regression coefficient for L.y
	double  zma[3];			// quad regression coefficient for L.y


	U32 	valid[MARKSEQUENCELEN];
	cr_point_t p3d[MARKSEQUENCELEN];

} clubmark_p3d_t;





//#define		NORMALBULK_NONE			0
//#define		NORMALBULK_NORMAL		1
//#define		NORMALBULK_BULK			2

typedef struct _clubmark_region {
	U32 valid;						// 0: EMPTY, 1: GOOD.
	point_t vertex[4];			
	point_t upperband[2];
	point_t lowerband[2];
//	U32 offset_x, offset_y;
} clubmark_region_t;


typedef struct _clubmark_feature {


	U32 offset_x, offset_y;
	double tsd;			// ts.. shift to cam1.
	double tsdiffd;		// shifted delta from shot time of cam1.
	point_t Pp;			// Ball position
	double  Pr;			// Ball radius

	clubmark_dot_t cmd;
	clubmark_bar_t cmb;

	clubmark_region_t crd;			// dot region
	clubmark_region_t crb;		// bar region
	clubmark_region_t crall;	// dot and bar region

} clubmark_feature_t;


typedef struct _clubmark_upperline {
	U32 valid;
	cr_line_t clineP;
	cr_line_t clineL;

	cr_point_t clineLp03d;		// Direct calc of 3d Point..
	cr_point_t clineLp03dE;		// estimated 3d Point..
} clubmark_upperline_t;

typedef struct _clubmark_lowerdot {
	U32 valid;
	cr_point_t cdotP;
	cr_point_t cdotL;
	cr_point_t cdotL3d;			// Direct calc of  3d Point..
	cr_point_t cdotL3dE;			// estimated 3d Point..
} clubmark_lowerdot_t;


typedef struct _clubmark_face {
	U32 valid;
	clubmark_upperline_t upperline;
	clubmark_lowerdot_t  lowerdot;
} clubmark_face_t;

typedef struct _clubmark_cam {
	U32	normalbulk;	// 0: NONE, 1: NORMAL, 2: BULK

	I32 indexshot;
	I32 index0;			// start
	I32 index1;			// last index

	double l1cm;
	double xfact;
	double yfact;

	/*
	clubmark_dot_t cmd[MARKSEQUENCELEN];
	clubmark_bar_t cmb[MARKSEQUENCELEN];

	double tsd[MARKSEQUENCELEN];			// ts.. shift to cam1.
	double tsdiffd[MARKSEQUENCELEN];		// shifted delta from shot time of cam1.
	point_t Pp[MARKSEQUENCELEN];		// Ball position
	double  Pr[MARKSEQUENCELEN];		// Ball radius
	*/


	clubmark_feature_t	cft[MARKSEQUENCELEN];	// Feautre.. 
	clubmark_face_t		face[MARKSEQUENCELEN];	// Face of clube..
		
	U32 	dotplanevalid;
	cr_plane_t dotplane;
	cr_line_t dotline;

	clubmark_Lp_t dotLp;
	clubmark_p3d_t dotp3d;
	clubmark_Lp_t barLp[4];			// 
	clubmark_p3d_t barp3d;
} clubmark_cam_t;

typedef struct _clubmark{
	U32		clubcalc_method;	

	double tsdiff;				// ts_of_cam0 - ts_of_cam1.  see  piana->bulkpairindex[indexc];
	double tsdshotcam[MAXCAMCOUNT];		// shot time
	clubmark_cam_t  cmc[MAXCAMCOUNT];

	cr_vector_t clubvect;
	cr_vector_t clubvect_dot;
	cr_vector_t clubvect_bar;


	double clubspeed_B;			// [m/s]
	double clubspeed_A;			// [m/s]
	double clubpath;			// [degree]
	double clubfaceangle;		// [degree]
	double clubattackangle;		// [degree]

	double clubloftangle;		// [degree]
	double clublieangle;		// [degree]

	double clubfaceimpactLateral;	// [mm]
	double clubfaceimpactVertical;	// [mm]


	int		Assurance_clubspeed_B;				// [0 ~ 100]
	int		Assurance_clubspeed_A;				// [0 ~ 100]
	int		Assurance_clubpath;					// [0 ~ 100]
	int		Assurance_clubfaceangle;			// [0 ~ 100]
	int		Assurance_clubattackangle;			// [0 ~ 100]
	int		Assurance_clubloftangle;			// [0 ~ 100]
	int		Assurance_clublieangle;				// [0 ~ 100]
	int		Assurance_clubfaceimpactLateral;	// [0 ~ 100]
	int		Assurance_clubfaceimpactVertical;	// [0 ~ 100]
	
} clubmark_t;

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
HAND iana_club_mark_create(HAND h);
I32 iana_club_mark_clear(HAND hcm);
I32 iana_club_mark_delete(HAND hcm);

I32 iana_club_mark(iana_t *piana, U32 clubcalc_method);

#if defined (__cplusplus)
}
#endif

#endif		// _IANA_CLUB_MARK_H_

