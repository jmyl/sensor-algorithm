/*!
 *******************************************************************************
                                                                                
                    Creatz Iana Configuration Read/Write
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_configure.h
	 @brief  
	 @author YongHo Suk
	 @date   2016/02/22 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_CONFIGURE_H_)
#define		 _IANA_CONFIGURE_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


I32 iana_config_read(iana_t *piana);
I32 iana_config_write(iana_t *piana);
I32 iana_config_read_INI(iana_t *piana);
I32 iana_config_write_INI(iana_t *piana);

//I32 iana_config_conversion(iana_t *piana, U32 right0left1);
I32 iana_config_conversion(iana_t *piana);
I32 iana_config_conversion2(iana_t *piana, point_t *pBallposL, U32 right0left1);

I32 iana_readTransMat(iana_t *piana, U32 right0left1);
I32 iana_readChessboardPixelPoint(iana_t *piana, U32 right0left1);
//I32 iana_checkChessboardPixelPoint(iana_t *piana, U32 right0left1);
I32 iana_checkChessboardPixelPoint2(iana_t *piana, U32 right0left1);
I32 iana_checkChessboardPixelPoint(iana_t *piana);


#if defined (__cplusplus)
}
#endif

#endif		// _IANA_CONFIGURE_H_

