/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA check ready
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_checkready.h
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2016/02/21 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_CHECKREADY_H_)
#define		 _IANA_CHECKREADY_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/



/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

typedef struct _ballcandidatepair {
	U32 state;				// 0: NULL, 1: OK.
	U32 index[2];
	iana_ballcandidate_t bc[2];
	cr_point_t b3d;
} ballcandidatepair_t;

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

I32 iana_checkready(iana_t *piana);
I32 IANA_Prepare4Ready(iana_t *piana);
I32 iana_readyarea(iana_t *piana, U32 camid, double lx, double ly, U32 *pox, U32 *poy);
I32 iana_checkpowersaving(iana_t *piana, U32 checkchange);

#if defined (__cplusplus)
}
#endif

#endif		// _IANA_CHECKREADY_H_
