/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA RGB LED
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2020 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_rgbled.h
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2020/03/31 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_RGBLED_H_)
#define		 _IANA_RGBLED_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

//U32 iana_rgbled_rgb0(iana_t *piana, U32 camcode, U08 r_, U08 g_, U08 b_);
//U32 iana_rgbled_rgb(iana_t *piana, U32 camcode, U08 r1_[12], U08 g1_[12], U08 b1_[12], U08 r2_[12], U08 g2_[12], U08 b2_[12]) ;
U32 iana_rgbled_rgb0(iana_t *piana, U32 right0left1, U32 camcode, U08 r_, U08 g_, U08 b_);
U32 iana_rgbled_rgb1(iana_t *piana, U32 right0left1, U32 camcode, U32 ledid, U08 r_, U08 g_, U08 b_);
U32 iana_rgbled_rgb2(iana_t *piana, U32 right0left1, U32 camcode, U08 r_[12], U08 g_[12], U08 b_[12]);
U32 iana_rgbled_rgb3(iana_t *piana, U32 right0left1, U32 camcode, U32 tablecode, U32 itercount, U32 direction);
U32 iana_rgbled_rgb4(iana_t *piana, U32 right0left1, U32 camcode, U32 rotationtype, U32 gradianttype, U32 animationspeed, U32 animationmode);
#if defined (__cplusplus)
}
#endif

#endif		// _IANA_RGBLED_H_

