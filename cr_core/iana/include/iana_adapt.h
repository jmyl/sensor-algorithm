/*!
 *******************************************************************************
                                                                                
                    Creatz Iana Interface
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_adapt.h
	 @brief  
	 @author YongHo Suk
	 @date   2016/02/20 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_IANA_ADAPT_H_)
#define		 _IANA_ADAPT_H_

#define IANA_STATICLIBRARY


/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#if defined(_WIN32)
#include <windows.h>
#endif

#if defined (__cplusplus)
extern "C" {
#define CPLUSCPLUS_EXTERN_C  //   extern "C"
#else
#define CPLUSCPLUS_EXTERN_C  
#endif

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

#if defined(_WIN32)

#if defined(IANA_STATICLIBRARY)
#define		IANA_EXPORT	

#else
#if defined(IANA_DLL_IMPLEMENT)
//#error 1
#define IANA_EXPORT CPLUSCPLUS_EXTERN_C __declspec(dllexport)
#else
//#error 2
#define IANA_EXPORT CPLUSCPLUS_EXTERN_C __declspec(dllimport)
#endif
#endif

#else

#define		IANA_EXPORT	

#endif

//---------- Result Code (CRG_result_t)
#define		IANA_OK						0x00000000

#define		IANA_ERR_GENERAL				0x80000000
#define		IANA_ERR_BADHANDLE				0x80000001
#define		IANA_ERR_UNSUPPORTED_CMD		0x80000010
#define		IANA_ERR_BADPARAM				0x80000011

#define		IANA_IS_RESULT_ERROR(x)	(IANA_ERR_GENERAL & (x))


//----
// See crkea_sensor.h
#define		IANA_SENSOR_STATE_NULL			0x0000
#define		IANA_SENSOR_STATE_CREATED		0x0001
#define		IANA_SENSOR_STATE_INITED		0x0002
#define		IANA_SENSOR_STATE_STARTED		0x0003			// but, ball area is empty.
#define		IANA_SENSOR_STATE_READYBALL0	0x0004
#define		IANA_SENSOR_STATE_READYBALL		0x0005
#define		IANA_SENSOR_STATE_GOODSHOT		0x0006
#define		IANA_SENSOR_STATE_SWING			0x0007
#define		IANA_SENSOR_STATE_STOPPED		0x0008
#define		IANA_SENSOR_STATE_DELETED		0x0009
#define		IANA_SENSOR_STATE_DISCONNECTED 	0x000A

//#define		IANA_SENSOR_STATE_FILE3DONE	0xFFFF0001

//---------- AREA Code
#define IANA_AREA_NULL			((U32)(-1))
#define IANA_AREA_TEE			0x0000
#define IANA_AREA_IRON			0x0001
#define IANA_AREA_PUTTER		0x0002

//---------- Command Code
#define		IANA_CMD_NULL					0x00000000
#define		IANA_CMD_DLLVERSION				0x00000001
#define		IANA_CMD_OPERATION_START		0x00000002
//#define		IANA_CMD_OPERATION_START2		0x00000040				// Caution!!  20191115
#define		IANA_CMD_OPERATION_START1		0x00000041				// Caution!!  20200513
#define		IANA_CMD_OPERATION_STOP			0x00000003
#define		IANA_CMD_OPERATION_PAUSE		0x00000004
#define		IANA_CMD_OPERATION_RESTART		0x00000005
#define		IANA_CMD_OPERATION_SETAREA		0x00000006
#define		IANA_CMD_OPERATION_AREARECT_SET	0x00000007
#define		IANA_CMD_OPERATION_AREARECT_GET	0x00000008
#define		IANA_CMD_OPERATION_SETWIND		0x00000010
#define		IANA_CMD_OPERATION_GETSTATUS	0x00000011
#define		IANA_CMD_OPERATION_ACTIVATE		0x00000012						// Activate ... 
#define		IANA_CMD_OPERATION_ACTIVATEVALID	0x00000013					// ActLevel run/don't run. p0: 0: Dont run, 1: run.
#define		IANA_CMD_OPERATION_AREAALLOW	0x00000014						
#define		IANA_CMD_OPERATION_BALLPOSITION	0x00000015						// p3 == 0:   Legacy BALLPOSION,  p3 == 1 : BALLPOSITION3D
#define		IANA_CMD_OPERATION_SERIAL_GET	0x00000016

#define		IANA_CMD_OPERATION_MATUSE_SET	0x00000017
#define		IANA_CMD_OPERATION_MATRECT_SET	0x00000018
#define		IANA_CMD_OPERATION_MATRECT_GET	0x00000019
#define		IANA_CMD_OPERATION_MAT2RECT_SET	0x0000001A					// L, mm
#define		IANA_CMD_OPERATION_MAT2RECT_GET	0x0000001B
#define		IANA_CMD_OPERATION_TEESTATE_SET	0x0000001C					// p0, Tee state is "DOWN" = 0 / "UP" = 1,   p1, 0: NO ball , 1: Ball on the TEE
#define		IANA_CMD_OPERATION_RIGHTLEFT_SET	0x0000001D				// p0, 0: Right-handed sensor, 1: Left-handed sensor
#define		IANA_CMD_OPERATION_SHOTRESULTEX	0x0000001E				// p0, 0: Right-handed sensor, 1: Left-handed sensor
#define		IANA_CMD_OPERATION_SHOTDATAEX	0x0000001F				// p0: iana_shotdataEX_t				// 20200409


#define		IANA_CMD_CALC_TRAJECTORY		0x00000020
#define		IANA_CMD_OPERATION_SET_AIRPRESSURE_PA		0x00000021	// p0: air pressure in Pa
#define		IANA_CMD_OPERATION_SET_ALTITUDE	0x00000022	// p0: altitutde in m
#define		IANA_CMD_OPERATION_SET_CLUBMODE	0x00000023	// p0: club mode.   0, 1: reverved, 2: NOMARKING CLUB,   3: CLUB mark with Bar and dot,  4: CLUB mark with TOPBAR


#define		IANA_CMD_OPERATION_SET_CALLBACK	0x00000028	// p0: callback function.   p1: callback function id.

#define		IANA_CMD_OPERATION_GETSWSPEC	0x00000030				// p0, U08 swspec[64+4]
#define		IANA_CMD_OPERATION_GETBARCODE	0x00000031				// p0, U08 barcode[64]

#define		IANA_CMD_OPERATION_RGB0_RGB		0x00000050				// Set RGB code. p0: 0x00: off ALL RGB LED. 0x01: CAM1, 0x02: CAM2, 0x03; CAM1 and CAM2. both. 
																	//				 p1: (((R << 16) & 0x00FF0000) | ((G << 8) & 0x0000FF00) | (B & 0x000000FF))

#define		IANA_CMD_OPERATION_RGB1_RGB		0x00000051				// Set RGB code. p0: 0x00: off ALL RGB LED. 0x01: CAM1, 0x02: CAM2, 0x03; CAM1 and CAM2. both. 
																	//				 p1: (((R << 16) & 0x00FF0000) | ((G << 8) & 0x0000FF00) | (B & 0x000000FF))
																	//               p2: ledid (0 ~ 11)					
#define		IANA_CMD_OPERATION_RGB2_RGB		0x00000052				// Set RGB code. p0: 0x00: off ALL RGB LED. 0x01: CAM1, 0x02: CAM2, 0x03; CAM1 and CAM2. both. 
																	//				 p1: (IANA_rgb2_t) data
#define		IANA_CMD_OPERATION_RGB3_RGB		0x00000053				// Set RGB tablecode. p0: cam. (0x00, 0x01, 0x02, 0x03)
																	//				 p1: table code   (0: rgb0.txt ~ 99: rgb99.txt)
																	//				 p2:  iteration count (0: STOP now),    p3: direction (0: normal, 1: reverse)				
#define		IANA_CMD_OPERATION_RGB4_RGB		0x00000054				// Set RGB animation code
																	//				 p1: rotation type. (0: STOP, 1: Clockwise, 2: Count-Clockwise)
																	//				 p2: animation speed. (0: Fastest ~ 15: Slowest)


//#define		IANA_CMD_OPERATION_START2		0x00000040			// Check me!!!

//---------- Command Code
#define		IANA_OPMODE_DEFAULT				0x0000
#define		IANA_OPMODE_CAM					0x0001
#define		IANA_OPMODE_FILE				0x0002

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

//---- Basic data type for IANA project
typedef	  signed char			I08;
typedef	  signed short			I16;
typedef	  signed int			I32;
typedef	  signed long			L32;
typedef	unsigned char			U08;
typedef	unsigned short			U16;
typedef	unsigned int			U32;
typedef	unsigned long			UL32;
typedef void*					HAND;

//---- Return code for IANA function
typedef		I32					IANA_result_t;

//---- Shot Result information



typedef		struct _IANA_shotdata {
	I32 ballspeedx1000;
	I32 sidespin;
	I32 backspin;
	I32 azimuthx1000;
	I32 inclinex1000;

	I32 clubspeed_Bx1000;
	I32 clubspeed_Ax1000;
	I32 clubpathx1000;
	I32 clubfaceanglex1000;
	
	I32 hitheightx1000;

	double axisx;
	double axisy;
	double axisz;
	double spinmag;
} IANA_shotdata_t;


typedef		struct IANA_shotdataEX1_t {
	CR_GUID shotguid;				// GUID of shot

	int category;					// Sensor category
	int rightlefthanded;			// 0: right-haned, 1: left-handed
	int xposx1000;					// Shot ball x position [mm]
	int yposx1000;					// Shot ball y position [mm]
	int zposx1000;					// Shot ball z position [mm]

	int ballspeedx1000;				// ball speed. [m/s] * 1000 = [mm/s]
	int inclineX1000;				// ball incline. [degree] * 1000
	int azimuthX1000;				// ball azimuth. [degree] * 1000

	int spincalc_method;			// Spin calc. method.  0: Using Ball/Club path. 1: Ball Marking based. 2: Dimple based.
	int assurance_spin;				// Spin calcuration assurance. 0-100
	int backspinX1000;				// backspin. [rpm] * 1000
	int sidespinX1000;				// sidespin. [rpm] * 1000
	int rollspinX1000;				// rollspin. [rpm] * 1000

	int clubcalc_method;			// club calc. method.  0: marking-less method, 1: another marking-less method, 2: bar-marking, 3: bar-marking and dot-marking
	int assurance_clubspeed;		
	int assurance_clubpath;
	int assurance_faceangle;
	int assurance_attackangle;
	int assurance_loftangle;
	int assurance_lieangle;
	int assurance_faceimpactLateral;
	int assurance_faceimpactVertical;
	int clubspeedX1000;				// club speed. [m/s] * 1000
	int clubpathX1000;				// club path angle. [degree] * 1000
	int faceangleX1000;				// face angle. [degree] * 1000
	int attackangleX1000;			// attack angle. [degree] * 1000
	int loftangleX1000;				// loftangle. [degree] * 1000
	int lieangleX1000;				// lie angle. [degree] * 1000
	int faceimpactLateralX1000;		// horizontal deviation of impact point from club center. [mm]
	int faceimpactVerticalX1000;	// vertical deviation of impact point from club center. [mm]

	int data[30];					// padding data. 

} IANA_shotdataEX1_t;



//---- Point data for trajectory
typedef		struct _IANA_point {
	I32 x;
	I32 y;
	I32 z;
	I32 t;
} IANA_point_t;


//---- Point data for trajectory
//#define IANA_TRAJ_MAXCOUNT 2000
#define IANA_TRAJ_MAXCOUNT 1000
/*
typedef		struct _IANA_trajectory {
	int count;
	IANA_point_t pts[IANA_TRAJ_MAXCOUNT];	// Unit of x, y, z: [cm],   t: [0.001sec == msec]
	int peakheight;							// [m]
	int carrydistance;						// [m]
	int sidedistance;						// [m]
	int flighttimeX1000;					// [msec]
} IANA_trajectory_t;
*/
//---- Velocity and rotation Vector for trajectory
typedef		struct _IANA_vs {
	int vx;				
	int vy;				
	int vz;
	int sidespin;
	int backspin;
	int t;				
} IANA_vs_t;

typedef		struct _IANA_trajectory {
	int count;
	IANA_point_t pts_mm[IANA_TRAJ_MAXCOUNT];	// Unit of x, y, z: [mm],   t: [0.001sec == msec]
	int peakheightx1000;						// [mm]
	int carrydistancex1000;						// [mm]
	int sidedistancex1000;						// [mm]
	int flighttimeX1000;						// [msec]
	IANA_vs_t vs[IANA_TRAJ_MAXCOUNT];	// Unit of vx, vy, vz: [mm/s]
                                        //       sidespin, backspin: [rpm]
                                        //       t: [0.001sec == msec]
	int udata[32];						// Reserved.
} IANA_trajectory_t;


typedef		struct _IANA_rect {
	int		sx;
	int		sy;
	int		ex;
	int		ey;
} IANA_rect_t;

typedef	struct _IANA_rgb2 {
	U08 r[12];
	U08 g[12];
	U08 b[12];
} IANA_rgb2_t;


//---- Call back fucntion for shot event
typedef int (CALLBACK IANA_CALLBACKFUNC) (HAND h, U32 status, IANA_shotdata_t *psd, PARAM_T userparam);
typedef int (CALLBACK IANA_CALLBACKFUNC2) (HAND h, U32 status, IANA_shotdata_t psd[], U32 shotdatacount, PARAM_T userparam);

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

IANA_EXPORT HAND IANA_create(U32 opmode, PARAM_T p0, PARAM_T p1, PARAM_T p2, PARAM_T p3);

IANA_EXPORT IANA_result_t IANA_delete(HAND h);
IANA_EXPORT IANA_result_t IANA_cmd(HAND h, U32 cmd, PARAM_T p0, PARAM_T p1, PARAM_T p2, PARAM_T p3);


#if defined (__cplusplus)
}
#endif


#endif		// _IANA_ADAPT_H_


