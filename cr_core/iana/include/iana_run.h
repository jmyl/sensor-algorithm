/*!
 *******************************************************************************
                                                                                
                    Creatz Iana run functions
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_run.h
	 @brief  
	 @author YongHo Suk
	 @date   2016/02/23 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_RUN_H_)
#define		 _IANA_RUN_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

I32 iana_run_start(iana_t *piana);
I32 iana_run_start_LITE(iana_t *piana);
I32 iana_run_stop(iana_t *piana);
I32 iana_run_pause(iana_t *piana);
I32 iana_run_restart(iana_t *piana);
I32 iana_run_restart2(iana_t *piana);
I32 iana_run_needrestart(iana_t *piana);
I32 iana_run_needrestart2(iana_t *piana);

void iana_setReadyCam(iana_t *piana);

#if defined (__cplusplus)
}
#endif

#endif		// _IANA_WORK_H_

