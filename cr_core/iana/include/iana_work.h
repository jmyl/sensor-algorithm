/*!
 *******************************************************************************
                                                                                
                    Creatz Iana working functions
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_work.h
	 @brief  
	 @author YongHo Suk
	 @date   2016/02/20 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_IANA_WORK_H_)
#define		 _IANA_WORK_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

I32 iana_cam_reconfig(iana_t *piana, U32 update);
I32 iana_setarea(iana_t *piana, U32 area);
I32 iana_setareaallow(iana_t *piana, U32 allowTee, U32 allowIron, U32 allowPutter);
I32 iana_shot_getts(iana_t *piana, U64 ts64to, 
		U64 *pts64Rfrom, U64 *pts64Rto, U32 *prindexR, 
		U64 *pts64Ofrom, U64 *pts64Oto, U32 *prindexO);
I32 iana_makeguid(char guid[]);

//U64 iana_ts64MToS(iana_t *piana, U64 ts64m);
//U64 iana_ts64SToM(iana_t *piana, U64 ts64s);
//U64 iana_ts64MeToM(iana_t *piana, U32 camid, U64 ts64);
//U64 iana_ts64MeToS(iana_t *piana, U32 camid, U64 ts64);
//U64 iana_ts64MeToO(iana_t *piana, U32 camidfrom, U32 camidto, U64 ts64from);

U64 iana_ts64_r2o(iana_t *piana, U64 ts64m);
U64 iana_ts64_o2r(iana_t *piana, U64 ts64s);
U64 iana_ts64_2r(iana_t *piana, U32 camid, U64 ts64);
U64 iana_ts64_2o(iana_t *piana, U32 camid, U64 ts64);
U64 iana_ts64_from2to(iana_t *piana, U32 camidfrom, U32 camidto, U64 ts64from);



U32 iana_getballposition(iana_t *piana, HAND p0, HAND p1, HAND p2, CR_BOOL threeDimension);				// 20200730
U32 iana_shotdata_ballEx(iana_t *piana, U32 clearresult, HAND p1);
U32 iana_shotdataEx(iana_t *piana, HAND p0);

U32 iana_shotplane_angle(iana_t *piana, double *pangle);

U32 iana_NetworkThrottlingIndex_Set(U32 value);				// 1: success, 0: fail
U32 iana_setRunBallsizeMinMax(iana_t *piana, U32 camid, cr_rectL_t *prectRunL, double sizeratio);
U32 iana_setReadyRunBallsizeMinMax(iana_t *piana, U32 camid, double sizeratio);
U32 iana_setRunRunBallsizeMinMax(iana_t *piana, U32 camid, cr_rect_t *prectRunP, double sizeratio);

U32 iana_setrightleft(iana_t *piana, U32 right0left1);
U32 iana_teepos_update(iana_t *piana);

#if defined (__cplusplus)
}
#endif

#endif		// _IANA_WORK_H_

