/*!
 *******************************************************************************
                                                                                
                    Creatz Iana xboard
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana.h
	 @brief  
	 @author YongHo Suk
	 @date   2016/12/21 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_XBOARD_H_)
#define		 _IANA_XBOARD_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"
#if defined(_WIN32)
#include <tchar.h>
#endif
/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

//#define XBRD_IP		"172.16.1.50"

#define XBRD_RS_IP		"172.16.1.50"
#define XBRD_LS_IP		"172.16.1.51"

#define XMINH_RS_IP		_T("172.16.1.50")
#define XMINH_LS_IP		_T("172.16.1.51")

#define XBRD_PORT	502


#define XBRD_PORT	502

#define XBRD_RGB_TABLECODE_COUNT	100   // (0 ~ 99
#define XBRD_RGB_LINE_COUNT			120		
/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif
typedef struct _iana_xbrd_rgb {
	U32		active;	// RGB operation active. 0: NO.
	U32		runstamp;
	U32		runstamp_required;
	U32		direction;		// 0: Normal, 1: Reverse
	U32		tablecode;
	U32		itercount;		// iteration count
	U32		current_iter;	// current iteration count
	U32		linecount;		// Total line count of RGB code
	U32		current_line;	// current line
	U08		r[XBRD_RGB_LINE_COUNT][12];
	U08		g[XBRD_RGB_LINE_COUNT][12];
	U08		b[XBRD_RGB_LINE_COUNT][12];
} iana_xbrd_rgb_t;
typedef struct _iana_xbrd
{
	HAND	hmutex;		// Mutex for IANA
	HAND	hth;		// thread handler

	HAND	hiana;		// HAND for IANA

	HAND	hXbrd;		// X Board handle 

	U32		right0left1;

	//----------------
	U32		category;	// XBOARD Category..

	iana_xbrd_rgb_t xrgb;

} iana_xboard_t;



//--
HAND iana_xboard_create(HAND hiana);
HAND iana_xboard_create2(HAND hiana, U32 right0left1);
HAND iana_xboard_create3(HAND hiana, U32 right0left1, U32 xboard_category);
I32 iana_xboard_delete(HAND h);

I32 iana_xboard_needbdcheck(HAND hiana);

U32 iana_xboard_cpusid_read(HAND hiana, U32 *pbuf);
U32 iana_xboard_flash_erase(HAND hiana, U32 addr, U32 length);
U32 iana_xboard_flash_read(HAND hiana, U08 *pbuf, U32 addr, U32 length);

U32 iana_xboard_flash_write(HAND hiana, U08 *pbuf, U08 *pwritebackbuf, U32 addr, U32 length);
U32 iana_xboard_flash_write_witherase(HAND hiana, U08 *pbuf, U08 *pwritebackbuf, U32 addr, U32 length);

U32 iana_xboard_sensor_rgbled_xminh(HAND hiana, HAND hxbrd, U32 cam1cam2, U32 tablecode, U32 itercount, U32 direction);
U32 iana_xboard_sensor_rgbled_xminh_inactive(HAND hiana, HAND hxbrd, U32 cam1cam2);

#if defined (__cplusplus)
}
#endif

#endif		// _IANA_XBOARD_H_

