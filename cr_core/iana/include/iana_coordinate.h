/*!
 *******************************************************************************
                   Creatz Coordinate

    @section copyright_notice COPYRIGHT NOTICE
    Copyright (c) 2011 by Creatz Inc.
    All Rights Reserved. \n
    Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
    @section file_information FILE CREATION INFORMATION
    @file   iana_coordinate.h
    @brief  Coordinate transform
    @author Hyeonseok Choi
    @date   2021/02/03 First Created

    @section checkin_information LATEST CHECK-IN INFORMATION
    $Rev$
    $Author$
    $Date$
 *******************************************************************************/

#if !defined(_IANA_COORDINATE_H_)
#define		 _IANA_COORDINATE_H_

/*----------------------------------------------------------------------------
    Description	: defines referenced header files
-----------------------------------------------------------------------------*/
#include "iana.h"
#include "cr_geometric.h"

/*----------------------------------------------------------------------------
    Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*   Description	: Type definition of structures and data type
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*   Description	: static variable declaration
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*   Description	: external and internal global variable
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*	Description	: declares the dll function prototype
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
*	Description	: declares the external function prototype
-----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif


I32 iana_P2L(iana_t *piana, U32 camid, point_t *pPp, point_t *pLp, U32 undistorted);
I32 iana_L2P(iana_t *piana, U32 camid, point_t *pLp, point_t *pPp, U32 undistorted);

I32 iana_P2L0(iana_t *piana, U32 camid, point_t *pPp, point_t *pLp, U32 undistorted);
I32 iana_L2P0(iana_t *piana, U32 camid, point_t *pLp, point_t *pPp, U32 undistorted);


I32 iana_L2P2(iana_t *piana, U32 camid, point_t *pLp, point_t *pPp);
I32 iana_LP2P2(iana_t *piana, U32 camid, point_t *pLp, cr_plane_t *pPt, point_t *pPp);
I32 iana_L2P2PP(iana_t *piana, U32 camid, cr_point_t *pLcpPP, point_t *pPp);
I32 iana_L2P_Ud(iana_t *piana, U32 camid, point_t *pLp, point_t *pPp);

I32 iana_P2L2(iana_t *piana, U32 camid, point_t *pPp, point_t *pLp);
I32 iana_P2LP2(iana_t *piana, U32 camid, point_t *pPp, point_t *pLp, cr_plane_t *pPt);
I32 iana_P2L2PP(iana_t *piana, U32 camid, point_t *pPp, cr_point_t *pLcpPP);
I32 iana_P2L_Ud(iana_t *piana, U32 camid, point_t *pLp, point_t *pPp);


U32 iana_Ptr2TargetPlanePtr(
    cr_point_t *pPtr,			// Point (in)
    cr_point_t *pPtrTarget,		// Point on the Target
    cr_point_t *pCamPos, 		// Camera position
    cr_plane_t *pPlaneTarget	// Target Plane
);


I32 iana_P2L3D_EP(
    iana_t *piana,
    U32 camid,
    double zvalue,
    point_t *pPp,
    cr_point_t *pLp3d);

I32 iana_P2L_EP(iana_t *piana, U32 camid, double zvalue, point_t *pPp,
    point_t *pLp0);



I32 iana_L3D2P_EP(
    iana_t *piana,
    U32 camid,
    cr_point_t *pLp3d,		// World coordinate point
    double *pzvalue,		// zvalue.
    point_t *pPp);			// image point


I32 iana_L2P_EP(iana_t *piana, U32 camid, point_t *pLp, point_t *pPp, double *pzvalue);




I32 iana_isInTee(iana_t *piana, cr_point_t *p3d);
I32 iana_isInIron(iana_t *piana, cr_point_t *p3d);
I32 iana_isInPutter(iana_t *piana, cr_point_t *p3d);

I32 iana_isInTeeL(iana_t *piana, double x, double y);
I32 iana_isInIronL(iana_t *piana, double x, double y);
I32 iana_isInPutterL(iana_t *piana, double x, double y);


I32 iana_isInBunker(iana_t *piana, cr_point_t *p3d);
I32 iana_isInBunkerL(iana_t *piana, double x, double y);

I32 iana_isInRough(iana_t *piana, cr_point_t *p3d);
I32 iana_isInRoughL(iana_t *piana, double x, double y);

I32 iana_getProjectPlane(iana_t *piana);
I32 iana_getProjectPlane0(iana_t *piana);
I32 iana_getProjectPlane1(iana_t *piana);



I32 iana_P2N(iana_t *piana, U32 camid, int distorted, point_t *pPtPix, point_t *pPtNormal);
I32 iana_N2P(iana_t *piana, U32 camid, int distorted, point_t *pPtNormal, point_t *pPtPix);
I32 iana_N2World(iana_t *piana, U32 camid, point_t *pPtNormal, double objDistCam, cr_point_t *pPtWorld);
I32 iana_World2N2(iana_t *piana, U32 camid, cr_point_t *pPtWorld, point_t *pPtNormal);


#if defined (__cplusplus)
}
#endif

#endif		// _IANA_COORDINATE_H_

