/*!
 *******************************************************************************
                                                                                
                    Creatz Iana Version
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana.h
	 @brief  
	 @author YongHo Suk
	 @date   2016/12/30 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_VERSION_H_)
#define		 _IANA_VERSION_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		2
//#define 	CAMSENSOR_BUILD		1

//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		7
////#define 	CAMSENSOR_BUILD		1
//#define 	CAMSENSOR_BUILD		99
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		8
//#define 	CAMSENSOR_BUILD		0
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		8
//#define 	CAMSENSOR_BUILD		1
//

//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		8
//#define 	CAMSENSOR_BUILD		2
//

//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		8
//#define 	CAMSENSOR_BUILD		3
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		8
//#define 	CAMSENSOR_BUILD		4
//

//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		9
//#define 	CAMSENSOR_BUILD		0
//

//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		9
//#define 	CAMSENSOR_BUILD		1

//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		9
//#define 	CAMSENSOR_BUILD		2
//
//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		0
//

//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		2
//

//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		1
//#define 	CAMSENSOR_BUILD		0
//

//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		1
//#define 	CAMSENSOR_BUILD		2		/*1*/
//
//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		2
//#define 	CAMSENSOR_BUILD		0

//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		3
//#define 	CAMSENSOR_BUILD		0
//
//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		3
//#define 	CAMSENSOR_BUILD		1

//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		3
//#define 	CAMSENSOR_BUILD		2
//

//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		3
//#define 	CAMSENSOR_BUILD		3
//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		3
//#define 	CAMSENSOR_BUILD		4
//
//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		3
//#define 	CAMSENSOR_BUILD		5
//
//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		3
//#define 	CAMSENSOR_BUILD		6
//
//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		3
//#define 	CAMSENSOR_BUILD		7
//
//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		3
//#define 	CAMSENSOR_BUILD		8
//
//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		3
//#define 	CAMSENSOR_BUILD		12
//
//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		3
//#define 	CAMSENSOR_BUILD		20
//

//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		3
////#define 	CAMSENSOR_BUILD		21
//#define 	CAMSENSOR_BUILD		999

//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		4
//#define 	CAMSENSOR_BUILD		0


//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		4
//#define 	CAMSENSOR_BUILD		2
//
//
//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		4
//#define 	CAMSENSOR_BUILD		3
//
//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		4
//#define 	CAMSENSOR_BUILD		4
//

//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		4
//#define 	CAMSENSOR_BUILD		5
//

//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		4
//#define 	CAMSENSOR_BUILD		99
//


//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		4
//#define 	CAMSENSOR_BUILD		999
//



//#define 	CAMSENSOR_BUILD		999
//


//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		0
//
//
//#define 	CAMSENSOR_MAJOR		1
//#define 	CAMSENSOR_MINOR		999
//#define 	CAMSENSOR_BUILD		999
//

//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		1
//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		2
//

//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		3
//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		4
//

//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		5
//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		1
//#define 	CAMSENSOR_BUILD		0
//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		1
//#define 	CAMSENSOR_BUILD		1
//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		1
//#define 	CAMSENSOR_BUILD		2
//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		1
//#define 	CAMSENSOR_BUILD		3
//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		1
//#define 	CAMSENSOR_BUILD		4
//

//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		2
//#define 	CAMSENSOR_BUILD		0
//

//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		2
//#define 	CAMSENSOR_BUILD		2

//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		2
//#define 	CAMSENSOR_BUILD		3
//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		2
//#define 	CAMSENSOR_BUILD		4
//
//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		2
//#define 	CAMSENSOR_BUILD		5
//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		2
//#define 	CAMSENSOR_BUILD		6
//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		2
//#define 	CAMSENSOR_BUILD		7
//


//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		2
//#define 	CAMSENSOR_BUILD		992
//

//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		3
//#define 	CAMSENSOR_BUILD		1
//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		3
////#define 	CAMSENSOR_BUILD		222
//#define 	CAMSENSOR_BUILD		333
//

//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		3
//#define 	CAMSENSOR_BUILD		2

//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		3
//#define 	CAMSENSOR_BUILD		3
//



//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		3
//#define 	CAMSENSOR_BUILD		444
//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		3
//#define 	CAMSENSOR_BUILD		555
//

//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		3
//#define 	CAMSENSOR_BUILD		666
//


#define 	CAMSENSOR_MAJOR		2
#define 	CAMSENSOR_MINOR		5
#define 	CAMSENSOR_BUILD		102



//
//#undef 	CAMSENSOR_MAJOR
//#undef 	CAMSENSOR_MINOR
//#undef 	CAMSENSOR_BUILD
//
//
//
//#define 	CAMSENSOR_MAJOR		2
//#define 	CAMSENSOR_MINOR		2
//#define 	CAMSENSOR_BUILD		999

//



//---------------------------------------------
//------ TEST for P3V2... 
#define P3V2_TEST

#if defined(P3V2_TEST)
#undef	CAMSENSOR_MAJOR
#undef	CAMSENSOR_MINOR
#undef	CAMSENSOR_BUILD


//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		1

//
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		2

//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		3
//


//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		4
//

//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		4
//


//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		6
//

//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		7
//


//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		8

//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		9
//

//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		10
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		11
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		12
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		13
//
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		14
//
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		15
//
//


//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		16
//

//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		17
//



//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		18
//

//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		19

//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		20
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		21
//

//
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		22
//
//

//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		23
//
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		24
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		25
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		26
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		27
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		28
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		29
//

//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		30
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		31
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		32
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		0
//#define 	CAMSENSOR_BUILD		33
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		1
//#define 	CAMSENSOR_BUILD		0
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		1
//#define 	CAMSENSOR_BUILD		1
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		1
//#define 	CAMSENSOR_BUILD		2
//

//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		1
//#define 	CAMSENSOR_BUILD		3
//
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		1
//#define 	CAMSENSOR_BUILD		4
//

//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		1
//#define 	CAMSENSOR_BUILD		5
//
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		1
//#define 	CAMSENSOR_BUILD		6
//
//
//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		1
//#define 	CAMSENSOR_BUILD		7

//
//#define 	CAMSENSOR_MAJOR		0
//#define 	CAMSENSOR_MINOR		1
//#define 	CAMSENSOR_BUILD		8

#define 	CAMSENSOR_MAJOR		0
#define 	CAMSENSOR_MINOR		1
#define 	CAMSENSOR_BUILD		9

#endif



//---------------------------------------------


/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

#endif		// _IANA_VERSION_H_

