/*!
 *******************************************************************************
                                                                                
                   Creatz Golf Camera sensor
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2016 by Creatz Inc.
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc. \n

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_traj.h
	 @brief  Golf bal Trajectory.
	 @author YongHo Suk                                 
	 @date   2016/03/01 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_IANA_TRAJ_H_)
#define		 _IANA_TRAJ_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
/*
             ^               
             |Z               
             |              .
             |          .       .
             |       .            .
             |     .               .
             |   .                  .
             | .                     .
             |------------------------.----->		Y
            /                          .
           /
		  /
		 V  X

*/

#define RADIAN2DEGREE(x)	((x) * 180. / M_PI)
#define DEGREE2RADIAN(x)	((x) * M_PI / 180.)

#define RADIAN2RPM(x)		((x) * 60./ (2. * M_PI))
#define RPM2RADIAN(x)		((x) * (2. * M_PI) / 60.)

#define KMH2MS(x)			((x) / 3.6)
#define MS2KMH(x)			((x) * 3.6)

/*----------------------------------------------------------------------------
 *	Description	: typedef
 -----------------------------------------------------------------------------*/


typedef struct _iana_traj_basic {
	double ballmass;
	double ballarea;
	double ballradius;
	double maxz;
	double airdensity;
} iana_traj_basic_t;


typedef struct _iana_traj_trick {
	double fd_mult;
	double fdx_mult;
	double fdy_mult;
	double fdz_mult;

	double fm_mult;
	double fmx_mult;
	double fmy_mult;
	double fmz_mult;

	double va_boundary;
	double va_hi_fx_mult;
	double va_hi_fy_mult;
	double va_hi_fz_mult;
	double va_low_fx_mult;
	double va_low_fy_mult;
	double va_low_fz_mult;

} iana_traj_trick_t;

typedef struct iana_traj {
	iana_traj_basic_t trajb;
	double windx;		// side wind element.
	double windy;		// 

//	double v[3];		// vx, vy, vz (m/s)
	double r[3];		// spin, wx, wy, wz (unit vector)
	double w;			// magnitude of spin (radian)
	double vy;			// magnitude of vy (m/s)

	double current_w;	// magnitude of current spin (radian)

	double t;			// current flight time, (sec)

#define IANA_TRAJ_RK_EQ_NUM		6
	double q[IANA_TRAJ_RK_EQ_NUM];		// vx, x, vy, y, vz, z

	double timedelta;
	double wc[4];		// spin decay coefficient.
	double vyc[4];		// vy decay coefficient.

	U32    index;
} iana_traj_t;


typedef struct iana_traj_data {
	double vmag;		// [m/s]
	double azimuth;		// [degree]
	double incline;		// [degree]
	double hitheight;	// [m]
	double backspin;	// [rpm]
	double sidespin;	// [rpm]
	double windmag;		// [m/s]
	double windangle;	// 0: to Y.  90: left to right, 180: against, 270: right to left
} iana_traj_data_t;


#define		IANA_RESULT_COUNT_MAX	IANA_TRAJ_MAXCOUNT
typedef struct iana_traj_result {
	double distance;	// distance from hit position, (m)
	double height;		// max height. (m)
	double side;		// max side deviation (m)

	double last_vmag;	// last velocity magninute
	double last_azimuth;	// last azimuth (degree)
	double last_incline;	// last incline (degree)

	double last_sidespin;	// last sidespin (rpm)
	double last_backspin;	// last backspin (rpm)

	double flighttime;	// flight time (sec)
	double timedelta;	// time resolution (sec)

	unsigned int    count;		// count of valid data.
	double t[IANA_RESULT_COUNT_MAX];		// time
	double x[IANA_RESULT_COUNT_MAX];		// x (side)
	double y[IANA_RESULT_COUNT_MAX];		// y (target)
	double z[IANA_RESULT_COUNT_MAX];		// z (height)

	double vx[IANA_RESULT_COUNT_MAX];		// x (side)
	double vy[IANA_RESULT_COUNT_MAX];		// y (target)
	double vz[IANA_RESULT_COUNT_MAX];		// z (height)


	double sidespin[IANA_RESULT_COUNT_MAX];
	double backspin[IANA_RESULT_COUNT_MAX];
	double rollspin[IANA_RESULT_COUNT_MAX];	// 2019/0719

	double wx[IANA_RESULT_COUNT_MAX];		// 2019/0719
	double wy[IANA_RESULT_COUNT_MAX];		// 2019/0719
	double wz[IANA_RESULT_COUNT_MAX];		// 2019/0719
} iana_traj_result_t;

/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/




/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

iana_traj_t *iana_traj_create(void);
void iana_traj_delete(HAND htraj);
void iana_traj(HAND htraj, double  timedelta, iana_traj_data_t *ptd, iana_traj_result_t *ptr);

iana_traj_t *iana_traj_create_2(void);
void iana_traj_delete_2(HAND htraj);
void iana_traj_2(HAND htraj, double  timedelta, iana_traj_data_t *ptd, iana_traj_result_t *ptr);


iana_traj_t *iana_traj_scapegoat_create(void);
void iana_traj_scapegoat_delete(HAND htraj);
void iana_traj_scapegoat(HAND htraj, double  timedelta, iana_traj_data_t *ptd, iana_traj_result_t *ptr);

I32 iana_traj_scapegoat_airpressure_psi(double airpressure_psi);
I32 iana_traj_scapegoat_airpressure_Pa(double airpressure_Pa);
I32 iana_traj_scapegoat_airpressure_altitude(double altitude_m);




void iana_traj_setmult(
		double fdmx_,
		double fdmy0_,
		double fdmy1_,
		double fdmz_,

		double flmx_,
		double flmy_,
		double flmz0_,
		double flmz1_);

#if defined (__cplusplus)
}
#endif

#endif					// _IANA_TRAJ_H_
