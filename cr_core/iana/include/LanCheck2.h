#pragma once
#if defined(_WIN32)
#include <winsock2.h>
#include <iphlpapi.h>
#include <icmpapi.h>
#endif

#define MAX_CHECK_IP	10
class CLanCheck2
{
public:
	CLanCheck2(void);
	~CLanCheck2(void);

//	CWinThread *m_pThread;
	void *m_pThread;
public:
	CR_BOOL	m_bIsConnect[MAX_CHECK_IP];
	int	m_iLanSpeed[MAX_CHECK_IP];
	int	m_iEnd;
	char  m_strIP[MAX_CHECK_IP][1024];
	CR_BOOL	m_bCheck[MAX_CHECK_IP];

	int		m_nErrCnt[MAX_CHECK_IP];
public:
	void start();
	void stop();

	CR_BOOL	setCheckIP(int index, char* strIP);
	char	*getCheckIP(int index);
	CR_BOOL	isrun(int index);

	CR_BOOL	getLanInfo(int index, int *pLan, int *pSpeed);
	CR_BOOL	removeCheckIP(int index);
	CR_BOOL	removeCheckIPAll();

	static void* checkThread(void *pParam);
	
	static int	isConnectLan(char *strIP);
	static int	getLanSpeed(char *strIP);

public:
	char 	m_strCheckIP[2][1024];

};

