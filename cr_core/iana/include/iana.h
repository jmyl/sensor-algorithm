/*!
 *******************************************************************************
                                                                                
                    Creatz Iana Interface
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana.h
	 @brief  
	 @author YongHo Suk
	 @date   2015/12/20 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_H_)
#define		 _IANA_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"
#include "cr_geometric.h"

#include "scamif.h"
#include "iana_cam.h"
#include "iana_info.h"
#include "iana_version.h"

//#include "iana_club_mark.h"
/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
//#define Z3_NEOCLUB
//#define Z3_25mmTEST



#define BALLDIAMETER			0.0428
#define BALLRADIUS				(BALLDIAMETER/2.0)


#define CRMINRATIO		0.7

#define CRMINRATIO_		0.8

#define CRMINRATIO_EYEXO		0.7

#define CRMAXRATIO				1.5
#define CRMAXRATIO_EYEXO		1.3

#define CRMAXRATIO_Z		1.5
#define CRMAXRATIO_EYEXO0	1.5

#define CRMAXRATIO_P3V2		1.4	
#define CRMAXRATIO_P3V2_0		1.2
#define CRMAXRATIO_P3V2_1		1.4
#define CRMAXRATIO_P3V2_1_0		1.5
#define CRMAXRATIO_P3V2_1_1		1.5

#define VMAGCALC_YVALUE	0.25


#define USEATTCKCAM		
#define SLAVECAM_ATTCAM_SYNC

//---------------------------------------------------------------------------
#define CAMSENSOR_NULL			0
#define CAMSENSOR_COMMON		1	
#define CAMSENSOR_VISION2		2
#define CAMSENSOR_VISION2CB		3
#define CAMSENSOR_ZCAM			4
#define CAMSENSOR_CBALL			5					// ZCAM2X
#define CAMSENSOR_CONSOLE1		6
#define CAMSENSOR_CBALL2		7
#define CAMSENSOR_TCAM			8
#define CAMSENSOR_Z3			9
#define CAMSENSOR_EYEXO			10
#define CAMSENSOR_P3V2			11


#define CAMSENSOR_LAST			11



//#define CAMSENSOR_CATEGORY		CAMSENSOR_COMMON	
//#define CAMSENSOR_CATEGORY		CAMSENSOR_VISION2	
//#define CAMSENSOR_CATEGORY		CAMSENSOR_VISION2CB
//#define CAMSENSOR_CATEGORY		CAMSENSOR_ZCAM	
//#define CAMSENSOR_CATEGORY		CAMSENSOR_Z3	
#define CAMSENSOR_CATEGORY		CAMSENSOR_CBALL	

#define CAMPROCESSMODE_NULL			0
#define CAMPROCESSMODE_BALLMARK		1
#define CAMPROCESSMODE_BALLCLUB		2
#define CAMPROCESSMODE_ATTACKCAM	3



//---------
#define XBOARD_CATEGORY_NULL			0
#define XBOARD_CATEGORY_XMINI		1
#define XBOARD_CATEGORY_XMINIHUB		2



//----------------------------
#define ALLOW_LEFTHANDED							// 2018/0823


//----------------------------------------


//#define ROUGHBUNKER_COMPENSATION				// not for now... 


#if defined(ZCAM_CONSOLE1)

#undef CAMSENSOR_CATEGORY
#define CAMSENSOR_CATEGORY		CAMSENSOR_CONSOLE1	

#endif


#if defined(ZCAM_TCAM)

#undef CAMSENSOR_CATEGORY
#define CAMSENSOR_CATEGORY		CAMSENSOR_TCAM	

#endif



#define SCAM_TSSCALE 	(1000000000LL)




//------------------------------------------------------------------------


#define	IANA_IMGCOUNT	5


#define IANA_AREA_NULL			((U32)(-1))
#define IANA_AREA_TEE			0x0000
#define IANA_AREA_IRON			0x0001
#define IANA_AREA_PUTTER		0x0002
#define IANA_AREA_ALL			0x0003




#define	FULLWIDTH				1280
#define	FULLHEIGHT				1024


//#define IPPI_INTER_WHATTHE		IPPI_INTER_LINEAR
//#define IPPI_INTER_WHATTHE		IPPI_INTER_CUBIC
//#define IPPI_INTER_WHATTHE		IPPI_INTER_NN
//#define IPPI_INTER_WHATTHE		(IPPI_SUBPIXEL_EDGE | IPPI_INTER_CUBIC)

#define	MAXCANDIDATE			16
#define MINLABEL				1
#define MAXLABEL				64
//#define MAXLABEL				128

#define MAXLABEL_NM				128			// for Non-marking processing.



#define IMAGESEGMENT_NULL		0x0000
#define IMAGESEGMENT_FULL		0x0001
#define IMAGESEGMENT_FULL_BALL	0x0002


#define	IANA_VETERAN_EMPTY		0x0000
#define	IANA_VETERAN_APPEAR		0x0001
#define	IANA_VETERAN_EXIST		0x0002
#define	IANA_VETERAN_SHOT		0x0003

#define	IANA_OPMODE_DEFAULT				0x0000
#define	IANA_OPMODE_CAM					0x0001
#define	IANA_OPMODE_FILE				0x0002


#define DDIST(x0,y0,x1,y1)  		sqrt(((x0)-(x1)) * ((x0)-(x1)) + ((y0)-(y1)) * ((y0)-(y1)))
#define DDIST3(x0,y0,z0,x1,y1,z1)  	sqrt(((x0)-(x1)) * ((x0)-(x1)) + ((y0)-(y1)) * ((y0)-(y1)) + ((z0)-(z1)) * ((z0)-(z1)))

#define LINEAR_SCALE(x,x0,y0,x1,y1) ( ( ((double) (((y1) - (y0)))) / ((double) ((x1) - (x0))) )*((x)-(x0)) + (y0) )

//#define BALLIMAGE_REF_SIZE	64
#define BALLIMAGE_REF_SIZE	128


//#define BALLSEQUENCELEN		8
//#define BALLSEQUENCELEN		16
										//#define BALLSEQUENCELEN		32
#define BALLSEQUENCELEN		64
#define BALLCCOUNT			4
//#define TS64FROM_OFFSET		(10 * 1000 * 1000LL)			// 5 msec
//#define TS64FROM_OFFSET		(5 * 1000 * 1000LL)			// 5 msec
//#define TS64FROM_OFFSET		(1 * 1000 * 1000LL)			// 1 msec
//#define TS64FROM_OFFSET		(0 * 1000 * 1000LL)			// 0 msec
//#define TS64FROM_OFFSET		(3 * 1000 * 1000LL)			// 3 msec
///#define BULK_PRESHOT_COUNT		6

//#define TS64FROM_OFFSET		((long long)(1.5 * 1000 * 1000LL))			// 2 msec
//#define BULK0_COUNT		60
//#define BULK0_COUNT		50


//#define MARKSEQUENCELEN		50
/////////////#define MARKSEQUENCELEN		40
//#define MARKSEQUENCELEN		20
//#define MARKSEQUENCELEN		30

#define MARKSEQUENCELEN		32					// 20170530



#define MARKSEQUENCELEN_EYEXO	24
///////#define MARKSEQUENCELEN_EYEXO	32


#define MARKCOUNT			32
#define MARKROTCOUNT		32

#define MARKSEQUENCELENATTCAM		50			// 20170530



//#define		THREADPRIORITY_IANA_MANAGER		0
//#define		THREADPRIORITY_IANA_CORE		1

#if defined(_WIN32)
#define		THREADPRIORITY_IANA_MANAGER		THREAD_PRIORITY_TIME_CRITICAL
#define		THREADPRIORITY_IANA_CORE		THREAD_PRIORITY_TIME_CRITICAL
#else
#define		THREADPRIORITY_IANA_MANAGER		MAX_USER_PRIORITY
#define		THREADPRIORITY_IANA_CORE		MAX_USER_PRIORITY
#endif
//#define		THREADPRIORITY_IANA_MANAGER		THREAD_PRIORITY_NORMAL
//#define		THREADPRIORITY_IANA_CORE		THREAD_PRIORITY_ABOVE_NORMAL


//#define		USE_SHOTAREARUNMODE



#define SPINASSURANCE_CROSSONLY		0.6
#define SPINASSURANCE_1				0.65
#define SPINASSURANCE_2				0.75
#define SPINASSURANCE_3				0.80
#define SPINASSURANCE_4				0.85
#define SPINASSURANCE_5				0.90
#define SPINASSURANCE_6				0.93

//#define	HEIGHT_CALI_FLAT_DEFAULT	0.007


#define		NORMALBULK_NONE			0
#define		NORMALBULK_NORMAL		1
#define		NORMALBULK_BULK			2



//#define		INFO_ALLCONDITION

//#define		SET_NETWORKTHROTTLINGINDEX


// 20191108. for Dimple processing.
#define MARKCOUNT2		128

//--- for ballgroup-RANSAC processing

#define BALLIDMAX		128

#define BALLGROUPMAX	8


//-------
#define INFOIMAGE_COUNT 24
//#define INFOIMAGE_PUTTER_COUNT 16
#define INFOIMAGE_PUTTER_COUNT 24

//-------------------------
#define MULTICANDIDATE_TEST





//-------------------------
#define MARK_KIND_EMPTY		0
#define MARK_KIND_UNDECIDED	1
#define MARK_KIND_DOT		2
#define MARK_KIND_BAR		3
#define MARK_KIND_OTHER		4









//#define CLUBFILEQVALUE	90
#define CLUBFILEQVALUE	100
#define CLUBFILENAME	"clubimage"
//#define CLUBFILENAME	"clubimage.jpg"

//#define MARKFILEQVALUE	90
#define MARKFILEQVALUE	100
/*----------------------------------------------------------------------------*/
#define		CR2CLUBCALC_DEFAULT				0
#define		CR2CLUBCALC_NOCALC				1
#define		CR2CLUBCALC_CLUB_NOMARK			2
#define		CR2CLUBCALC_CLUB_MARK1			3
#define		CR2CLUBCALC_CLUB_MARK2			4


#define CLUBCALC_NULL				0
#define CLUBCALC_NOCALC				1
#define CLUBCALC_CLUB				2
#define CLUBCALC_CLUB_MARK_BARDOT	3
#define CLUBCALC_CLUB_MARK_TOPBAR	4


/*----------------------------------------------------------------------------*/


#define WIDTHS		96							// 16*6... for CONSOLE1
#define HEIGHTS		96


#define WIDTHI	(4*WIDTHS)
#define HEIGHTI	(4*HEIGHTS)


#define WIDTHM	200
#define HEIGHTM	200



//// Width/Height for Dimple-only Processing
// see ana_spin_mark.cpp
//#define WIDTHD		128
//#define HEIGHTD		128




//#define V2000_TEST                                    // TEST!!!



#define BULKPAIRINDEX_NONE		((U32)-1)


/*----------------------------------------------------------------------------*/
//#define BMIXX
/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

extern double ggg_HEIGHT_CALI_FLAT_DEFAULT;


typedef		struct _IANA_shotdata IANA_shotdata_t;


typedef int (CALLBACK IANA_IMAGE_CALLBACKFUNC) (char *img, int width, int height, int p0, int p1, int p2, int p3, PARAM_T userparam);
typedef int (CALLBACK IANA_CALLBACKFUNC) (HAND h, U32 status, IANA_shotdata_t *psd, PARAM_T userparam);
typedef int (CALLBACK IANA_CALLBACKFUNC1) (HAND h, U32 status, HAND hsd, U32 cbfuncid, PARAM_T userparam);
//typedef int (CALLBACK IANA_CALLBACKFUNC2) (HAND h, U32 status, IANA_shotdata_t psd[], U32 shotdatacount, I32 userparam);


//--------------------------------
//--- for ballgroup-RANSAC processing
typedef struct _ballid {
	I32 groupid;
	U32 cexist;					// IANA_BALL_NULL, IANA_BALL_EXIST, IANA_BALL_EXILE
	U32 seq;
	U32 id;

} ballid_t;

typedef struct _ballgroup_param {
	U32 count;
	double m_;
	double b_;
} ballgroup_element_t;

typedef struct _ballgroup {
	U32 ballcount;
	ballid_t bid[BALLIDMAX];

	U32 elementcount;
	ballgroup_element_t element[BALLGROUPMAX];

} ballgroup_t;


/*!
*   @ingroup    IANA_CLUB
*   @brief      Club path계산에 필요한 값들을 모아둔 structure.\n !!mark를 이용한 club 계산은 이것을 사용하지 않습니다.
*/
typedef struct _clubpath_context {
	U32 index;
	U32 m;
	U32 shotindex;
	U32 shotm;

	U32 frame;
	U32 shotframe;

	I32 startindex;
	I32 endindex;

	double clubpath;		// Degree.			[degree]
	double clubfaceangle;	// Degree.			[degree]
	double clubspeedB;		// Club speed before shot [m/s]
	double clubspeedA;		// Club speed after  shot [m/s]

	double sidespin;
	double backspin;

	double geareffect;		// Geareffect for sidespin [degree]

	//-------------------------------
	U32 width;				// Image width, height
	U32 height;
	U32	offset_x;
	U32	offset_y;
	U32	gain;
	U32 exposure;
	U32 multitude;
	
	I32 startx;						// Processing Image
	I32 endx;
	I32 starty;
	I32 endy;
	U32 roiwidth;
	U32 roiheight;
	U32 imgoffset;

	U64 ts64shot;
	U64	ts64[MARKSEQUENCELEN];

	double	tsd[MARKSEQUENCELEN];			// (ts64[] - ts64shot) / TSSCALE_D

	U32 aftershot;
	U32 preshotgood;
	double preshot_m_;
	double preshot_b_;
	double preshot_myxa[3];

	double postshot_ma[3];

	double maiitt[3];				// frame number to time..

	double shotindexcombined;

	double shotframecombined;



	U32	preshot_mtxa_valid;
	U32	preshot_mtya_valid;
	double preshot_mtxa[3];
	double preshot_mtya[3];


	double postshot_myxa[3];

	U32	postshot_mtxa_valid;
	U32	postshot_mtya_valid;
	double postshot_mtxa[3];
	double postshot_mtya[3];


	I32 shotarea;						// 20200626.
	//-------------------------------
	I32	linecount[MARKSEQUENCELEN];
#define MAXLINECOUNT		4

	double theta_[MARKSEQUENCELEN][MAXLINECOUNT];
	double m_[MARKSEQUENCELEN][MAXLINECOUNT];
	double b_[MARKSEQUENCELEN][MAXLINECOUNT];

	I32		houghpvalue0[MARKSEQUENCELEN][FULLHEIGHT];
	I32		houghpvalue1[MARKSEQUENCELEN][FULLHEIGHT];
	I32		houghXpvalue[MARKSEQUENCELEN][FULLWIDTH];
	I32		houghYpvalue[MARKSEQUENCELEN][FULLHEIGHT];



	I32		hx[MARKSEQUENCELEN][MAXLINECOUNT];		// Hough-based header position
	I32		hy[MARKSEQUENCELEN][MAXLINECOUNT];

	I32		hlen[MARKSEQUENCELEN];			// [MAXLINECOUNT];

	I32		hpexist[MARKSEQUENCELEN][MAXLINECOUNT];

	I32		hsx[MARKSEQUENCELEN];			// Shape-based header position
	I32		hsy[MARKSEQUENCELEN];
	I32		hsexist[MARKSEQUENCELEN];

	I32		count;
	double	hlenmin;
	double	hlenmax;
	double	hlenEstimated;
	double	hlenEstimatedL;		// Local length of Header

	double  hitoffsetL;

	double	myha[3];

	I32		sx, ex;

	U32 bulkshotindex;
	U32 bulkindexFrom;
	U32 bulkindexTo;


	//-------------------------------

	U08 *diffimg;
	U08 *diffimgFilter;
	U08 *diffimgBinary;
	U08 *prefimgNoBall;

	//----------------------------------
#define CLUBDATACOUNT	32
	U32	   h2count;
	U32	   h2valid;

	U32	   h2state[CLUBDATACOUNT];

	double h2toexma[3];				// t vs toe x
	double h2toeyma[3];				// t vs toe y
	double h2toeshaftxma[3];				// t vs toeshaft x
	double h2toeshaftyma[3];				// t vs toeshaft y
	double h2heelxma[3];			// t vs heel x
	double h2heelyma[3];			// t vs heel y
	double h2xma[3];				// t vs x for calc.
	double h2yma[3];				// t vs y

	double h2clubspeedma[3];			// t vs clubspeed			// not used acutally

	double h2ts[CLUBDATACOUNT];
	point_t h2toepos[CLUBDATACOUNT];
	point_t h2toeshaftpos[CLUBDATACOUNT];

	point_t h2heelpos[CLUBDATACOUNT];
	point_t h2shaftpos[CLUBDATACOUNT];
	U32	   h2heel3dposvalid[CLUBDATACOUNT];
	cr_point_t h2heel3dpos[CLUBDATACOUNT];	// heel 3d position

	double h2shaftangle[CLUBDATACOUNT];		// shaft angle..
	double h2faceangle[CLUBDATACOUNT];	// faceangle..		// not used
	cr_line_t h2shaftline[CLUBDATACOUNT];	// shaft line. (fixed point: heel point)
	cr_line_t h2shaft3dline[CLUBDATACOUNT];	// shaft 3d line. (fixed point: heel point)


	double h2facelen;					// [m]

	double h2clubspeed_B;				// [m/s]
	double h2clubpath;

	point_t h2hitpos;					// ball hit position

	cr_line_t h2lineface;
	cr_line_t h2lineshot;
	double gearoffset;					// [m]


} clubpath_context_t;




typedef struct _imagesegment {
	U32 state;
	U32 label;
	U32 pixelcount;

	U32 ixl, iyu;			// left, upper
	U32 ixr, iyb;			// right, bottom

	U32 marginal;			// marginal position.

	U32 minx, miny;			// minimum distance position.
	double mind;			// min distance value from center

	double mx, my;			// mean x, mean y (cx, cy)
	double a_, b_, c_;			// ax + by + c = 0;   // yi = m * xi + b
} imagesegment_t;


typedef struct _ballpair {
	U32 valid;
	U32 index[2];			// pair index for cam0 and cam1
	double ts64d;
	point_t posL[2];
} ballpair_t;


typedef struct _markelement {
	U32 			state;					// 0: EMPTY, 1: ALIVE, 2: CLOSED
	U32				goodseq;
	I32 			birthindex;
	I32 			lastindex;
	imagesegment_t 	imsg[MARKSEQUENCELEN];
	
	cr_point_t		posP[MARKSEQUENCELEN];		// Pixel point
	cr_point_t		posPD[MARKSEQUENCELEN];		// Pixel point, Normalized, Center-relative.
	cr_point_t		posPD2[MARKSEQUENCELEN];	// Pixel point, PD -> cam compensation
	cr_point_t		posL[MARKSEQUENCELEN];		// Local point (m0 for NEWMARKPROCESSING)
	cr_point_t		posLD[MARKSEQUENCELEN];		// Pixel point, Normalized, Center-relative.
	cr_point_t		posLD2[MARKSEQUENCELEN];	// Pixel point, LD -> cam compensation

//--------------
	cr_point_t		posV3D[MARKSEQUENCELEN];	// mv for NEWMARKPROCESSING
	cr_point_t		posV3D_[MARKSEQUENCELEN];	// mv_ for NEWMARKPROCESSING

	cr_point_t		pos3D[MARKSEQUENCELEN];		// m for NEWMARKPROCESSING
	cr_point_t		pos3D_[MARKSEQUENCELEN];	// m_ for NEWMARKPROCESSING


	cr_point_t		pos3D_quadratic[MARKSEQUENCELEN];	// m_ for NEWMARKPROCESSING, quadratic regression.


	double 			angle[MARKSEQUENCELEN];
	cr_point_t		cop;		// center of plane
} markelement_t;



typedef struct _markelement2 {				// 20191108 for Dimple processing.
	U32 			state;					// 0: EMPTY, 1: ALIVE, 2: CLOSED
	U32				dotcount;				// dot count

	double			imagediameter;
	double			imagemultfactor;

	point_t			ballposP;
	cr_point_t		ballposL3D;


	point_t			Pimage[MARKCOUNT2];			// dot Position. image pixel
	point_t			Pabs[MARKCOUNT2];			// dot Position. absolute pixel
	cr_point_t		L3dabs[MARKCOUNT2];			// dot Position. global coord. 
	cr_point_t		L3bc[MARKCOUNT2];			// dot Position. with respect to ball center

} markelement2_t;



typedef struct _marksequence {
	int 			count;						// NON-EMPTY mark element count
	int 			seqnum;						// Sequence number

	U32				valid[MARKSEQUENCELEN];
	markelement_t	mke[MARKCOUNT];

	cr_point_t 		ballposL[MARKSEQUENCELEN];	// Ball position
	double     		ballrL[MARKSEQUENCELEN];		// Ball radius

	cr_point_t 		ballposL3D[MARKSEQUENCELEN];	// Ball position, 3D (Pb for NEWMARKPROCESSING)

	cr_point_t 		ballposP[MARKSEQUENCELEN];		// Ball position, absolute Pixel
	double     		ballrP[MARKSEQUENCELEN];		// Ball radius	, absolute Pixel

	U32				ballPosvalid[MARKSEQUENCELEN];

	point_t 		ballposL2[MARKSEQUENCELEN];		// Ball position, Local point
	double     		ballrL2[MARKSEQUENCELEN];		// Ball radius
	double     		ballrL3[MARKSEQUENCELEN];		// Ball radius, calc with Ball-Camera Distance

	point_t 		ballposP2[MARKSEQUENCELEN];		// Ball position, absolute Pixel
	double     		ballrP2[MARKSEQUENCELEN];		// Ball radius	, absolute Pixel
	double     		ballrP3[MARKSEQUENCELEN];		// Ball radius, calc with Ball-Camera Distance

	U32				ballPos2valid[MARKSEQUENCELEN];


	point_t 		ballposL2Raw[MARKSEQUENCELEN];		// Ball position, Local point
	double     		ballrL2Raw[MARKSEQUENCELEN];		// Ball radius
	double     		ballrL3Raw[MARKSEQUENCELEN];		// Ball radius, calc with Ball-Camera Distance

	point_t 		ballposP2Raw[MARKSEQUENCELEN];		// Ball position, absolute Pixel
	double     		ballrP2Raw[MARKSEQUENCELEN];		// Ball radius	, absolute Pixel
	double     		ballrP3Raw[MARKSEQUENCELEN];		// Ball radius, calc with Ball-Camera Distance

	double			cam2ball[MARKSEQUENCELEN];			// distance between cam and ball
	double			cam2ground[MARKSEQUENCELEN];		// distance between cam and ground


	double     		startx[MARKSEQUENCELEN];		// image start position
	double     		starty[MARKSEQUENCELEN];		// 

	U64 			ts64[MARKSEQUENCELEN];		

	//--------
	double 			rrm[MARKSEQUENCELEN][3][3];		// Rodrigues Matrix for Sight compensation


	//--------

	markelement2_t	mke2Dimple[MARKSEQUENCELEN];	// for Dimple processing.  20191108 
	markelement2_t	mke2Mark[MARKSEQUENCELEN];		// for Mark2  processing.  20191108

} marksequence_t;



typedef struct _shotdata_full {
	CR_GUID shotguid;					// GUID of shot result

	I32 goodshot;						// 1: Good shot, 0: NO shot.
	I32	ballspeedx1000;					// Ball speed	[mm/s]
	I32	azimuthx1000;					// Ball azimuth	[(1/1000) degree]
	I32	inclinex1000;					// Ball incline	[(1/1000) degree]

	I32	spindata_kind;					// 0: ball-marking result.  1: ball-club result	
	I32	sidespin;						// side spin. (+: slice, -: hook)	[rpm]
	I32	backspin;						// back spin (+: backspin, -: Topspin)	[rpm]
	I32	rollspin;						// Roll spin. (+: clock-wise, -: Counter clock-wize)	[rpm]

	I32	clubdatakind;					// 0: ball-marking result.  1: ball-club result	
	I32	clubspeed_Bx1000;				// Club speed Before shot	[mm/s]
	I32	clubspeed_Ax1000;				// Club speed After shot	[mm/s]
	I32	clubpathx1000;					// Club path. (+: right,  -: left)	[(1/1000) degree]
	I32	clubfaceanglex1000;				// Club face angle (+: right, -: left)	[(1/1000) degree]
	I32	clubfacelengthx1000;			// Club face length (0: discard this value)	[mm]
	I32	clubballhitpointx1000;			// Club ball hit point from club toe	[mm]
	I32	rsvd4;							// Reserved.  0x00000000	

	I32	mk_assurance;					// marking ball result, data quality assurance. 0 ~ 99
	I32	mk_sidespin;					// marking ball result: side spin. (+: slice, -: hook)	[rpm]
	I32	mk_backspin;					// marking ball result: back spin. (+: backspin, -: Topspin)	[rpm]
	I32	mk_rollspin;					// marking ball result: Roll spin. (+: clock-wise, -: Counter clock-wize)	[rpm]
	I32	mk_spinmagnitude;				// marking ball result: spin magnitude	
	I32	mk_axis_x_x1000000;				// marking ball result: spin axis vector, x value * 1000000
	I32	mk_axis_y_x1000000;				// marking ball result: spin axis vector, y value * 1000000	
	I32	mk_axis_z_x1000000;				// marking ball result: spin axis vector, z value * 1000000	
	I32	mk_clubspeed_Bx1000;			// marking ball result: Club speed Before shot	[mm/s]
	I32	mk_clubspeed_Ax1000;			// marking ball result: Club speed After shot	[mm/s]
	I32	mk_clubpathx1000;				// marking ball result: Club path. (+: right,  -: left)	[(1/1000) degree]
	I32	mk_clubfaceanglex1000;			// marking ball result: Club face angle (+: right, -: left)	[(1/1000) degree]

	I32	bc_assurance;					// Club - Ball result: data quality assurance. 0 ~ 99
	I32	bc_sidespin;					// Club - Ball result: side spin. (+: slice, -: hook)	[rpm]
	I32	bc_backspin;					// Club - Ball result: back spin (+: backspin, -: Topspin)	[rpm]
	I32	bc_rollspin;					// Club - Ball result: Fixed to 0.	[rpm]

	I32	bc_spinmagnitude;				// Club - Ball result: spin magnitude
	I32	bc_axis_x_x1000000;				// Club - Ball result: spin axis vector, x value * 1000000	
	I32	bc_axis_y_x1000000;				// Club - Ball result: spin axis vector. Fixed to 0x00000000	
	I32	bc_axis_z_x1000000;				// Club - Ball result: spin axis vector, z value * 1000000	
	I32	bc_clubspeed_Bx1000;			// Club - Ball result: Club speed Before shot	[mm/s]
	I32	bc_clubspeed_Ax1000;			// Club - Ball result: Club speed After  shot	[mm/s]
	I32	bc_clubpathx1000;				// Club - Ball result: Club path. (+: right,  -: left)	[(1/1000) degree]
	I32	bc_clubfaceanglex1000;			// Club - Ball resI32 iana_shot_store_shotresultfull(iana_t *piana)ult: Club face angle (+: right, -: left)	[(1/1000) degree]
	I32	bc_clubfacelengthx1000;			// Club face length (0: discard this value)	[mm]
	I32	bc_clubballhitpointx1000;		// Club ball hit point from club toe	[mm]

	I32	datapad[18];					// Data padding.   Fill with 0x00000000	
} shotdata_full_t;

typedef struct _iana_shotresult {
	int		valid;
	int	    readflag;					// 0: Not yet read, 1: read...

	//--
	double shotAssurance;

	double vmag;
	double incline;
	double azimuth;

	//--
	U32		spincalc_method;	// Spin calc. method.  0: Using Ball/Club path. 1: Ball Marking based. 2: Dimple based.
	double spinAssurance;

	double sidespin;
	double backspin;
	double rollspin;		// 

	cr_vector_t axis;
	double spinmag;

	double mx_[2], bx_[2];
	double my_[2], by_[2];
	double mxy_[2], bxy_[2];


	//----
	U32		clubcalc_method;	// club calc. method.  0: marking-less method, 1: another marking-less method, 2: bar-marking, 3: bar-marking and dot-marking
	double clubspeed_B;			// [m/s]
	double clubspeed_A;			// [m/s]
	double clubpath;			// [degree]
	double clubfaceangle;		// [degree]
//-- 20200104
	double clubattackangle;		// [degree]

	double clubloftangle;		// [degree]
	double clublieangle;		// [degree]

	double clubfaceimpactLateral;	// [mm]
	double clubfaceimpactVertical;	// [mm]


	U32		Assurance_clubspeed_B;				// [0 ~ 100]
	U32		Assurance_clubspeed_A;				// [0 ~ 100]
	U32		Assurance_clubpath;					// [0 ~ 100]
	U32		Assurance_clubfaceangle;			// [0 ~ 100]
	U32		Assurance_clubattackangle;			// [0 ~ 100]
	U32		Assurance_clubloftangle;			// [0 ~ 100]
	U32		Assurance_clublieangle;				// [0 ~ 100]
	U32		Assurance_clubfaceimpactLateral;	// [0 ~ 100]
	U32		Assurance_clubfaceimpactVertical;	// [0 ~ 100]
	
	
//--

	//-----------  BallClub
	double clubAssurance;		// 20190916
	double headerlenL;

	double hitoffsetL;			// Ballx - cx


	double geareffect;			// sidespin

	U32 width;
	U32 height;
} iana_shotresult_t;



typedef struct _iana_ballcandidate {
	U32     cexist;
	U32		tsindex;			// TIME stamp index.. from shot point
	U32		m;					// multitude index
	U32		rsved;				// trivial... for 64bit align... 
	U64		ts64;				// TS64 of me.


	//-- pixel point
	cr_point_t	P;
	double cr;
	double cer;


	//-- local point
	cr_point_t	L;
	double lr;	// Radius

	double mean,  stddev;		// Radius area.
	double meanH, stddevH;		// Half radius area.
} iana_ballcandidate_t;


typedef struct _iana_veteran {
	U32	state;			// State of veteran


	U32 count;

	//-- pixel point
	cr_point_t P;
	double cr;			// radius value
	double cer;			// error of LS

	//-- local point
	cr_point_t L;
	double lr;	// Radius

	cr_point_t b3d;				// Ball 3d position.. 

	double mean,  stddev;		// Radius area.				// 20200806
	double meanH, stddevH;		// Half radius area.
} iana_veteran_t;



//iana_veteran_t;



typedef struct _lpdatapair {	// Data pair for quad transform construction
	point_t	Lp;				// Local Point
	point_t	Pp;				// Pixel Point
} lpdatapair_t;


//#define LPCOUNTMAX	(32*32)
#define LPCOUNTMAX	(100*100)

typedef struct _iana_cam_param {		// Run parameter
	//----
	cr_point_t	CamPosL;
	cr_point_t	CamPosEstimatedL;		// Camera position, Estimated using Calibration data

	U32 bCamparamValid;				// 1: cameraParameter and distCoeffs are valid.
	cameraintrinsic_t cameraintrinsic;
	U32 CamExtrinsicValid;	
	cameraextrinsic_t cep;

	cr_plane_t	ProjectionPlane;			// Projection plane..
	cr_point_t	ProjectionPlaneCenter;		// 2018/1129



	biquad_t	bAp2l;		// Pixel to Local translate matrix
	biquad_t	bAl2p;		// Local to Pixel.

	biquad_t	bAp2l_raw;	// Pixel to Local translate matrix, without Calibration-flat height compensation
	biquad_t	bAl2p_raw;	// Local to Pixel.

	U32			quad_UdValid;		// 1: Undistorted translate matrix are valid. 0: invalid.

	biquad_t	bAp2l_Ud;		// Undistorted Pixel to Local translate matrix
	biquad_t	bAl2p_Ud;		// Local to Undistorted Pixel.

	biquad_t	bAp2l_Ud_raw;	// Undistorted Pixel to Local translate matrix, without Calibration-flat height compensation
	biquad_t	bAl2p_Ud_raw;	// Local to Undistorted Pixel.


	U32			lpdatapaircount;	
	lpdatapair_t lpdatapair[LPCOUNTMAX];

	double			height_cali_flat;

	//--- Processing Area.
	//cr_rect_t rectArea;
	cr_rect_t rectArea_;
	cr_rect_t rectAreaExt;

	cr_rect_t rectTee;
	cr_rect_t rectIron;
	cr_rect_t rectPutter;

	//--- Ball
	double 	CerBallexist;
	double 	CerBallshot;

	double 	BallsizeMax;
	double 	BallsizeMin;

	double	RunBallsizePMax;
	double	RunBallsizePMin;
	//--- Start shot position
	cr_point_t P;	// Start shot Pixel position

	double  cr;

	cr_point_t L;	// Start shot Local position

	cr_point_t b3d;	// Start shot 3d ball position

/////////	U32		gd1[10240];		//guard #1
	//----  Ballgroup for RANSAC
	ballgroup_t bgp;
/////////	U32		gd1[10240];		//guard #2
	//--- Shot geomtric parameters
	U32 ballcount;

	double maPx[3];
	double maPy[3];
	double maPr[3];

	double maLx[3];
	double maLy[3];
	double maLr[3];

	double mPxx_, bPxx_;
	double mPyy_, bPyy_;
	double mxy_, bxy_;

	cr_line_t 		LocLine;		// Local line
	cr_plane_t		ShotPlane;		// Shot Plane

	//--
	double	gainmult;

	int m_ledbrgt;
	int m_bright;
	int m_contrast;
	int m_gamma;

	//--
	double maxangleOnetick;		// Max rotaion for tick

	//--
	double mean, stddev;		// Radius area.
	double meanH, stddevH;		// Half radius area.

	double meanRH_min, meanRH_max, meanRH_last;
}	iana_cam_param_t;


typedef struct _markdistance {
	U32 id;
	double distance;

} markdistance_t;

typedef struct _markrotation {
	U32 valid;
	cr_vector_t	axis;
	double		angle;		// Degree
	double rrma[3][3];		// Axis rotation matrix
	U32		i0, i1, i2;
	double derr1, derr2;	// Estimation error
	double derrN;			// Estimation error for Next seq mark.
	U32		goodcount;
} markrotation_t;


typedef struct _markinfo {
	U32 kind;
	imagesegment_t 	imsg;	// Image segment information
	double angle;			// Mark image width, height, angle
	cr_point_t posP;			// P, Pixel position of mark
	cr_point_t rposP;			// P, Pixel Relative position with respect to ball center
	cr_point_t rposPTopview;	// P, Relative position with respect to ball center, Top View

	cr_point_t posL;		// 3D position of mark
	cr_point_t rposL;		// Relative position with respect to ball center
	cr_point_t rposLTopview;	// Relative position with respect to ball center, Top View

	cr_point_t rposLTopviewNext;	// Rotated postion.. 



//-------
	point_t NposP;		// P, Pixel position of mark
	point_t NposL;		// L position of mark
	cr_point_t Npos3D;		// position of mark, with P2L3D..   (On the line.. NOT absolute poision.)  (20181231.. )
	cr_point_t NposOnBP;	// 3D position of mark on Ball Plane	 (markOnBP)
	cr_point_t NposOnBPwrtBall;	// Relative 3D position of mark on Ball Plane w.r.t. Ball Position (markOnBPwrtBall)
	cr_point_t Npos3DwrtBall;	// Relative 3D position of mark on w.r.t. Ball Position (mark3DwrtBall)
	double  cofa;		// Coeffient of ua
	double  cofb;		// Coeffient of ub
	double  cofc;		// Coeffient of uc			

	cr_point_t Npos3DwrtBallNext;	// Relative 3D position of mark on w.r.t. Ball Position (mark3DwrtBall)




	markdistance_t mkd[MARKCOUNT];	// Mark distance between next seq.
} markinfo_t;


typedef struct _ballmarkinfo {
	U32 valid;
#if defined(BMIXX_NO)
#else
	U64 ts64;			// Time stamp...
#endif

	//---
	double rrm[3][3];		// Rodrigues Matrix, vector and angle
	cr_vector_t rv;			
	double rrma[3][3];		// Axis rotation and Rod. mat
	double      rt;


	cr_vector_t ua;			// normal vector    of BALL-PLANE
	cr_vector_t ub;			// 1st Basis vector of BALL-PLANE		 (Rodrigues vector)
	cr_vector_t uc;			// 2nd Basis vector of BALL-PLANE

	cr_plane_t BallPlane;	// Ball plane..

	//----
	
	U32 markcount;
	markinfo_t	markinfo[MARKCOUNT];

	U32 markrotationcount;
	markrotation_t mkr[MARKROTCOUNT];

} ballmarkinfo_t;


typedef struct _ballmarkimg {
	U32 valid;
	U08 *pimg;
	U08 *pimgRaw;
	U08 *pimgBW;
	I32 startx, starty;
	U32 width, height;
	double pr_, lr_;			// 
	double multfact;			// 2018/1128

} ballmarkimg_t;




typedef struct _bestmatchindex {
	I32    bestindex[MARKSEQUENCELEN][MARKCOUNT];
} bestmatchindex_t;



struct _iana;
typedef struct  _iana_cam {
	struct _iana 		*piana;		// IANA!!!!
	I32	camid;					// CAMID of my camkind.  (-1: Not avaliable)

	//---
	iana_camconf_t icf;

	U32 rungain0;				// Original rungain
	U32 rungain;				// Actual rungain
	//---
	U32		offset_x;			// Camera Image Start x, start y
	U32		offset_y;

	U32		runProcessingWidth;
	U32		runProcessingHeight;

	//--- Image
	U08 *pimg[IANA_IMGCOUNT];
	U08 *prefimg;
	U08 *pimgFull;

	U08 *pimgClubOverlap;

	//-- Ball Image backup
	cr_rect_t rectBallImg;
	U08 BallImg[BALLIMAGE_REF_SIZE * BALLIMAGE_REF_SIZE * 4];
	U32 biw, bih;

	iana_cam_param_t icp;

	//---
	iana_veteran_t	vtr[4];			// 0: Tee, 1: Iron, 2: Putter	   3: Total..
	iana_veteran_t	vtrPrev[4];		// Previous Veteran point.

	U32 rindexshot;
	U32 mshot;
	U32 rindexshotbulk;				// 
	U64 ts64shot;					// Ball disappear timeindex
	U64 ts64shot0;					// Frame-synced timeindex



	U32 bcseqcount;					// ball candidate count
	iana_ballcandidate_t bc[BALLSEQUENCELEN][BALLCCOUNT];		// ball candidate sequence;
	iana_ballcandidate_t bs[BALLSEQUENCELEN];					// selected ball sequence;
	cr_point_t 			 PointOnLine[BALLSEQUENCELEN];			// Point in the shot line
	double 			     maPrr[BALLSEQUENCELEN];				// Regression coeff. for P radius vs td		2020/0721
	double 			     maLrr[BALLSEQUENCELEN];				// Regression coeff. for L radius vs td		2020/0721


	//--- for spin calc
	imagesegment_t imgseg[MAXLABEL];
	double markangle[MAXLABEL];			// angle of mark
	marksequence_t	mks;


	//--- for spin calc, Non-marking basesd.
	imagesegment_t imgseg_NM[MAXLABEL_NM];			// For Non-Marking based,  2019/0107

	//--- club..
	clubpath_context_t	cpc;

	//-  New ball mark sequence .. 2016/11/29
	ballmarkinfo_t	bmi[MARKSEQUENCELEN];
	ballmarkinfo_t	bmiTest[MARKSEQUENCELEN];

	ballmarkimg_t	bmimg[MARKSEQUENCELEN];
	imagesegment_t imgseg_N[MAXLABEL];
	double markangle_N[MAXLABEL];			// angle of mark
	markrotation_t mkrSeqBest[MARKSEQUENCELEN];				// Best of each Sequence
	bestmatchindex_t matchIndexSeqBest[MARKSEQUENCELEN];	// best index..

	markrotation_t mkrBest;								// Best of all.
	bestmatchindex_t matchIndexbest;


#if 0	// not used  acutally
	//---  CLUB..
	U32 rowcolsumcount;
	U32 rowsum[MARKSEQUENCELEN][FULLHEIGHT];
	U32 rowsumfrom[MARKSEQUENCELEN];
	U32 rowsumto[MARKSEQUENCELEN];

	U32 colsum[MARKSEQUENCELEN][FULLWIDTH];
	U32 colsumfrom[MARKSEQUENCELEN];
	U32 colsumto[MARKSEQUENCELEN];

	U32 rowcolsumvalid[MARKSEQUENCELEN];
#endif /* 0 */


	//--- For Ball speed refine.
	U32	ballcount;
	double vmag2[MARKSEQUENCELEN];
	double TrajY[MARKSEQUENCELEN];

	U32 firstbulkindex;


	//--- Tee Position   20200903
	point_t teeCenterP;
	point_t teeCenterL;

	cr_point_t teeTop3d;

	point_t teeTopProjectionL;
	point_t teeTopProjectionP;

	//-------------
	U08 r[128];
	U08 g[128];
	U08 b[128];
} iana_cam_t;


typedef struct  _iana_cam_setup{
	U32 camintrinsicid;
	U32 masterslave;			// 0: MASTER, 1: SLAVE
	U32	zrtmode;				// 1: zrot, 0: NROT				// Center..
	U32 processmode;			// BALLMARK, BALLCLUB

	U32 run_width;
	U32 run_height;

	U32 run_processing_width;
	U32 run_processing_height;


	I32 offsetoffset_x;
	I32 offsetoffset_y;

	U32 run_framerate;
	U32 run_basegain;
	U32 run_exposure;
	U32 run_skip;
	U32 run_multitude;
	U32 run_syncdiv;

	U32 fullrange_exposure;
	U32 fullrange_basegain;

	U32 run_width_putter;
	U32 run_height_putter;

	I32 offsetoffset_x_putter;
	I32 offsetoffset_y_putter;

	U32 run_framerate_putter;
	U32 run_basegain_putter;
	U32 run_exposure_putter;
	U32 run_skip_putter;
	U32 run_multitude_putter;
	U32 run_syncdiv_putter;

	U32 bulk_width;
	U32 bulk_height;

	U32 bulk_preshot_count;
	U32 mult_for_bulk;

	I64 bulk_preshot_additional_time;
	I64 bulk_time_preshot;


	U32 exposure_post;			// FW default value: 0			// 2019-11/29
	U32 exposure_expiate;		// FW default value: 34

} iana_cam_setup_t;


typedef struct  _iana_setup {
	U32 camsensorcategory;
	iana_cam_setup_t icsetup[MAXCAMCOUNT];
} iana_setup_t;


#define SHOTRESULT_NOSHOT		0
#define SHOTRESULT_GOODSHOT		1


#define SHOTCHECKME_NO			0
#define SHOTCHECKME_YES			1

#define PATHBUFLEN		2048


typedef struct _datapath {
	U32	  mode;							// 0: Legacy mode,  1: datapath mode, 2: command set.
	CHR szDataDir[PATHBUFLEN];
	CHR szDataScDataDir[PATHBUFLEN];

	char sDataDir[PATHBUFLEN];
	char sDataScDataDir[PATHBUFLEN];

	CHR szEXEPath[PATHBUFLEN];
	CHR szDir[PATHBUFLEN];
	CHR szDrive[PATHBUFLEN];
	CHR szFileName[PATHBUFLEN];
	CHR szFileExt[PATHBUFLEN];

} datapath_t;

#if defined(_WIN32)			
#define DATAPATH	TEXT("c:\\ZCAM")
#else
#define DATAPATH	TEXT("./ZCAM")
#endif
#define DATAPATHTXT	"datapath.txt"


typedef struct _iana
{
	/* System / Handler */
	HAND	hmutex;		// Mutex for IANA
	HAND	hth;		// thread handler
	HAND	hth_core;	// core thread handler

	HAND	hjpeg;	

	HAND	hcm;							// 20200106	


	/* Timestamp / System Tick / Count */
	I64		ts64delta;	// t640(master) - t641(slave)
	double	tsdiff;							// timestamp difference in sec..  ts_of_cam0 - ts_of_cam1 20200304   	
	
	U32		resetcamtick;
	U32		resetsensortick;	
	U32		runstate_prepared_tick;	
	U32		goodreadytick;	
	U32		readystarttick;		
	U32		updategainTick;

	U32 	tsdiffbadcount;
	U32 	imagesuccessbadcount;
	U32 	restartcount;
	U32		tsdiffchecktick;

	U32		heartbeat_tick;
	
	U32		readygoodcount;
	U32		readygoodcountGOOD;

	U32		updategainCount;

	U32		runindexcount;
	U32		readymercycount;

	U32		checkreadypositioncount;	


	/* State flags */
	U32		state;		
	U32		runstate;
	U32		opmode;
	U32		activated;	// 0: NO activated, 1: Activated.

	U32		needrestart;
	U32		needrestart2;

	U32		needresetcam;				// Reset Camera 
	U32		needresetsensor;			// Reset Sensor	

	U32		needrunstateempty;			// Go to empty state and RE-start.. :P	

	U32		needxbdcheck;	


	/* CAM IF & Configuration */
	HAND	hscamif;

	HAND	RS_hscamif;
	HAND	LS_hscamif;

	iana_setup_t isetup;
	iana_cam_t	*pic[MAXCAMCOUNT];						// 20180825 추가  
	iana_cam_t	RS_ic[MAXCAMCOUNT];						// 20180825 추가	
	iana_cam_t	LS_ic[MAXCAMCOUNT];						// 20180825 추가	

	U32 	currentcamconf;		// Current camera conf mode 	

	U32		camidCheckReady;
	U32		camidOther;
	

	/* Ball Position, Index Processing */
	U32				ballpaircount;
	ballpair_t		ballpair[MARKSEQUENCELEN];
	cr_point_t 	ballpairPosL[MARKSEQUENCELEN];	// Ball position, reconstructed using ball pair information.

	U32		lastrindex[MAXCAMCOUNT];

	U32		bulkpairindex[MARKSEQUENCELEN];				// j = [i] : ith cam0 image matches jth cam1.
	U32		bulkshotindex[MAXCAMCOUNT];


	/* Spin Processing */
	U32 spinsimulmode;		// 0: NO, 1: Simulation 
	
	cr_vector_t simul_axis;
	double	simul_spinmag;
	
	double simul_backspin;		// Pitch, THETA_x
	double simul_sidespin;		// -Yaw, -THETA_z
	double simul_rollspin;		// Roll, THETA_y


	/* Axis / Area Processing */
	U32		allowedarea[3];		// 0: Tee, 1: Iron, 2: Putter area

	U32 	processarea;		// shot process area.
	U32 	shotarea;			// Primary shot area.

	U32		shotarearunmode;	// 1: Tee/Iron, both enable,  0: separated.

	cr_rectL_t *prectLArea;
	cr_rectL_t *prectLTee;
	cr_rectL_t *prectLIron;
	cr_rectL_t *prectLPutter;

	cr_rectL_t RS_rectLArea;
	cr_rectL_t RS_rectLTee;
	cr_rectL_t RS_rectLIron;
	cr_rectL_t RS_rectLPutter;

	cr_rectL_t LS_rectLArea;
	cr_rectL_t LS_rectLTee;
	cr_rectL_t LS_rectLIron;
	cr_rectL_t LS_rectLPutter;

	U32		*pRoughBunkerMat;		// 0: Don't use rough/bunker area,   1: Use.

	cr_rectL_t *prectLRough;
	cr_rectL_t *prectLBunker;

	U32		RS_RoughBunkerMat;		// 0: Don't use rough/bunker area,   1: Use.

	cr_rectL_t RS_rectLRough;
	cr_rectL_t RS_rectLBunker;

	U32		LS_RoughBunkerMat;		// 0: Don't use rough/bunker area,   1: Use.

	cr_rectL_t LS_rectLRough;
	cr_rectL_t LS_rectLBunker;

	cr_line_t ShotLine;	// Shot Line

	U32	ironAreaBallExistCount;					// 20201130,  
	U32 ironAreaCheckCount;						// 20201201


	/* Configuration / Mode / Info */	
	I32		camsensor_category;
	I32		camsensor_category_ini;
	I32		processmode[MAXCAMCOUNT];							// Camera processing mode
	
	U32		useLeftHanded;		// 0: Don't use left-handed (One Camera Sensor Mode), 1: Use Left-handed (Two Camera Sensor Mode).   <-- Use this!!!!
	U32		Right0Left1;		// 0: Right-Handed, 1: Left-Handed
	U32		leftHandedType;	// 0: Two Camera Sensor Set,   1: One Camera Sensor Set  -> Deprecated.. :P 
	U32		Right0Left1Required;
	U32		NeedChangeRight0Left1;

	U32		oneareamode;		// 0: Legacy mode,  1: One area mode.
	U32		teedotmode;			// 0: Legacy mode,  1: Tee dot mode.
	
	U32		framerate;					// Framerate 
	U32		framerate_putter;			// Framerate for Putter

	double	height_cali_flat;	
	
	I32 airpressure_PA;
	I32 airpressure_PA_valid;

	double airpressure_PSI;
	I32 airpressure_PSI_valid;

	I32 airpressure_altitude_m;
	I32 airpressure_altitude_m_valid;

	I32 airpressure_altitude_feet;
	I32 airpressure_altitude_feet_valid;

	U32		ransac_id;				// 0: deterministic.  1: rand() with timer seed setting. 2: rand() with seed setting.
	U32		ransac_seed;

	U32		clubcalc_method_required;			// 20200711.

	U32		drawpathmode;

	U32		trajmode;
	I32		windspeedx1000;
	I32		winddirectionx1000;	

	U32		infomode;		// 0: NO, 1: Info xml only, 2: Info xml and Image
	U32		info_saveimage;				// 0: no,  1: Save image
	iana_info_t	iif;
	
	iana_camconf_t	info_icc[MAXCAMCOUNT];			// camconf for info processing.  
	I32				info_runProcessingWidth[MAXCAMCOUNT];
	I32				info_runProcessingHeight[MAXCAMCOUNT];
	iana_camconf_t	info_icc_bulk[MAXCAMCOUNT];	// camconf for info processing. BULK
	cr_point_t		info_startPosL[MAXCAMCOUNT];
	cr_point_t		info_startPosP[MAXCAMCOUNT];
	cr_point_t		info_startPosB[MAXCAMCOUNT];
	U64				info_startts64[MAXCAMCOUNT];
	I32				info_startindex[MAXCAMCOUNT];
	I32				info_firstindex[MAXCAMCOUNT];
	I32				info_lastindex[MAXCAMCOUNT];
	I32				info_firstindex_bulk[MAXCAMCOUNT];
	I32				info_lastindex_bulk[MAXCAMCOUNT];
	I32				info_goodshot;
	iana_shotresult_t info_shotresult;
	I32				info_right0left1;

	U08		shotdatapath[1024];	

	U08		shottime[64];		// from info file.      20200426	  

	U32		*pCamPosEstimatedLValid;	// Estimated cam position is VALID....
	U32		*pUseProjectionPlane;

	U32		RS_CamPosEstimatedLValid;	// Estimated cam position is VALID....
	U32		RS_UseProjectionPlane;

	U32		LS_CamPosEstimatedLValid;	// Estimated cam position is VALID....
	U32		LS_UseProjectionPlane;

	//----------  20200714
	point_t 		ballposP2Info[MAXCAMCOUNT][MARKSEQUENCELEN];		// Ball position, absolute Pixel from info file. 20200714
	double     		ballrP2Info[MAXCAMCOUNT][MARKSEQUENCELEN];		// Ball radius	, absolute Pixel from info file. 20200714
	cr_point_t 		ballposL3DInfo[MAXCAMCOUNT][MARKSEQUENCELEN];	// Ball position, 3D (Pb for NEWMARKPROCESSING) from info file. 20200714

	markelement2_t	mke2DimpleInfo[MAXCAMCOUNT][MARKSEQUENCELEN];	// for Dimple processing from info file. 20200714
	markelement2_t	mke2MarkInfo[MAXCAMCOUNT][MARKSEQUENCELEN];		// for Mark2  processing from info file. 20200714	
	

	/* Post Shot Processing / Result */	
	CR_GUID	shotguid;			// shot guid
	
	U32		shotresultflag;				// 0: Bad, 1: Goodshot
	U32		shotresultcheckme;			// 0: soso. 1: checkme.....
	iana_shotresult_t shotresultdata;
	iana_shotresult_t shotresult[MAXCAMCOUNT];

	iana_shotresult_t clubball_shotresultdata;
	iana_shotresult_t clubball_shotresult[MAXCAMCOUNT];

	shotdata_full_t		shotresultfull;

	iana_shotresult_t shotresultdata2;	// 20191115
	iana_shotresult_t shotresult2[MAXCAMCOUNT];	


	/* XBoard */
	HAND	hxboard;
	HAND	RS_hxboard;
	HAND	LS_hxboard;
	U32		xboard_category;


	/* Power */
	U32		powersavingtick;
	U32		powersavingtime;
	U32		powersaving;


	/* Directory/File Path strings */
	CHR szEXEPath[PATHBUFLEN];
	
	CHR szDir[PATHBUFLEN];
	CHR szDrive[PATHBUFLEN];
	CHR szFileName[PATHBUFLEN];
	CHR szFileExt[PATHBUFLEN];
	
	CHR szImageDirRoot[PATHBUFLEN];
	CHR szImageDir[PATHBUFLEN];	

	char	readinfofile[1024];
	char	simresultfile[1024];


	/* Debug & Tools */
	HAND	hcbgt;		// handle of Bugtrap
	HAND	hcl;	

	U32		testmode;			// 0: NO Test.      1: Fake run.

	/* User defined */
	HAND	hcbfunc;
	PARAM_T		userparam;

	HAND	hcbfunc1;				// 20200513
	U32		cbfunc1_id;

	HAND    himgfunc;
	PARAM_T     imguserparam;
	

	/* Deprecated */
	HAND	hscamifAtC;		// attack camera....  20170601 -> Deprecated.	

	HAND	RS_hscamifAtC;		// -> Deprecated.	
	HAND	LS_hscamifAtC;			// -> Deprecated.	

	I64		ts64deltaAtC;	// t640(master) - t64_AttackCam  -> Deprecated.

	U32 	imagesuccessAtCbadcount;		// -> Deprecated.
	U32		restartAtCcount;				// -> Deprecated.
	U32 	needrestartAtC;				// -> Deprecated.
	U32 	tsdiffAtCbadcount;			// -> Deprecated.

	U32		*pUseAttackCam;		// 0: Don't use Attackcam,   1: Use. ==> Deprecated
	U32		RS_UseAttackCam;		// 0: Don't use Attackcam,   1: Use.	==> Deprecated
	U32		LS_UseAttackCam;		// 0: Don't use Attackcam,   1: Use.	==> Deprecated
} iana_t;




//---- Ball position 
typedef		struct _iana_ballposition {
	int ballexist;				// 0: Don't Exist, 1: Exist
	int shotresult;				// 0: Last shot was NOT me.  1: Last shot was me!!
	int x;						// X position [mm]
	int y;						// Y position [mm]
	int z;						// Z position [mm]
} iana_ballposition_t;


typedef		struct _iana_shotdata_ballEx {
	U32    valid;			// 0: result invalid or empty.  1: Valid.
	U32	   reserved;		// 0. reserved.
	double incline;			// Ball incline, [degree]
	double azimuth;			// Ball azimuth, [degree]
	double vmag;			// Ball speed, [m/s]
	double shotAssurance;	// Measurement assurance of speed, incline and azimuth
	double spinmag;			// Ball spin magnitude, [rpm: Round-per-Minute]
	double spinaxis[3];		// Ball spin axis,  [0]: x, [1]: y, [2]: z
	double spinAssurance;	// Measurement assurance of spin
} iana_shotdata_ballEx_t;


typedef		struct _iana_shotdataEX {				// see CR_shotdataEX_t
	I32 ballspeedx1000;					// [mm/s]
	I32 sidespin;						// [rpm]
	I32 backspin;						// [rpm]
	I32 azimuthx1000;					// [(1/1000) degree]
	I32 inclinex1000;					// [(1/1000) degree]

	I32 clubspeed_Bx1000;				// [mm/s]
	I32 clubspeed_Ax1000;				// [mm/s]
	I32 clubpathx1000;					// [(1/1000) degree]	
	I32 clubfaceanglex1000;				// [(1/1000) degree]
	
	I32 hitheightx1000;					// [mm] Discard it.
} iana_shotdataEX_t;


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

extern datapath_t g_datapath;

I32 iana_image_callback_set(HAND hiana, HAND hfunc, PARAM_T userparam);


I32 iana_runparam_init(iana_t *piana);
I32 iana_allocbuffer(iana_t *piana);
I32 iana_freebuffer(iana_t *piana);
#if defined (__cplusplus)
}
#endif

#endif		// _IANA_H_

