/* Previous functions - write matching functions here after changing the function's name */
#if !defined(_IANA_DEPRICATED_H_)
#define		 _IANA_DEPRICATED_H_

/* iana_club_mark.cpp */
#define iana_club_mark_recog 						RecognizeClubmark
#define iana_club_mark_threshold 					GetClubmarkThreshold
#define estimateballpos 							EstimateBallPosition
#define club_mark_binarization 						BinarizeClubmarkImage
#define club_mark_labeling 							LabelingClubmark

#define iana_club_mark_calc_bardot 					ComputeData
#define iana_club_mark_calc_bardot_dot 				ComputeData_Dot
#define iana_club_mark_calc_bardot_dot_2d 			ComputeData_Dot_2D
#define iana_club_mark_calc_bardot_dot_3d 			ComputeData_Dot_3D
#define iana_club_mark_calc_bardot_dot_p3dE 		ComputeData_Dot_P3dE
#define iana_club_mark_calc_bardot_bar 				ComputeData_Bar
#define iana_club_mark_calc_bardot_bar_2d 			ComputeData_Bar_2D
#define iana_club_mark_calc_bardot_bar_3d 			ComputeData_Bar_3D
#define iana_club_mark_calc_bardot_bar_p3dE 		ComputeData_Bar_P3dE
#define iana_club_mark_calc_bardot_upperline_3dot 	ComputeData_Upperline_3Dot
#define iana_club_mark_bardot_upperline_3dot_face 	ComputeData_Upperline_3Dot_Face

#define iana_club_mark_calc_bardot_extdata 			ComputeData_Extdata
#define iana_club_mark_calc_bardot_extdata2 		ComputeData_Extdata2
#define iana_club_mark_calc_bardot_extdata_index 	ComputeData_Extdata_Index
#define iana_club_mark_calc_topbar_extdata 			ComputeData_Topbar_Extdata
#define iana_club_mark_calc_topbar_extdata2 		ComputeData_Topbar_Extdata2
#define iana_club_mark_calc_topbar_extdata_index 	ComputeData_Topbar_Extdata_Index

#define iana_club_mark_ts 							GetClubmarkTimestamp
#define iana_club_mark_info_start 					OpenClubmarkInfoXml
#define iana_club_mark_info_end 					CloseClubmarkInfoXml
#define iana_club_mark_info_store 					StoreClubmarkInfoXml

#define club_mark_select 							SelectClubmark
#define club_mark_exile 							ExileClubmarkData

#define club_mark_regression 						RegressionClubmark
#define club_mark_regression_lowerdot(x,y,z) 			RegressionDot(x,y,z,CR_TRUE)
#define club_mark_regression_dot(x,y,z) 				RegressionDot(x,y,z,CR_FALSE)
#define club_mark_regression_upperbar 				RegressionUpperbar
#define club_mark_regression_bar 					RegressionBar
#define club_mark_regression_Mbar 					RegressionMbar
#define iana_club_mark_regression_lowerdot_3d 		RegressionLowerDot_3D
#define iana_club_mark_regression_upperbar_3d 		RegressionUpperbar_3D

#define display_bardot 								DisplayClubmarkBarDot



/* iana_club.cpp */
#define iana_club_estimate 							IANA_CLUB_EstimateClubData
#define iana_club_spin_estimate 					IANA_CLUB_EstimateClubDataForP3V2 /* currently used only for CAMSENSOR_P3V2 */
#define iana_club_calc			  					IANA_CLUB_CalcClubDataP3V2	/* currently used only for CAMSENSOR_P3V2 */
#define iana_clubpath_cam		  					GetClubPath				/* currently used only for CAMSENSOR_P3V2 */
#define iana_clubpath_cam_param		  				GetClubPath_Param		/* currently used only for CAMSENSOR_P3V2 */
#define iana_clubpath_cam_getpos		  			GetClubPath_Position		/* currently used only for CAMSENSOR_P3V2 */
#define iana_clubpath_cam_getpos_calc 				GetClubPath_Position_Computation		/* currently used only for CAMSENSOR_P3V2 */
#define iana_clubpath_cam_exile			   			GetClubPath_Position_Exile		/* currently used only for CAMSENSOR_P3V2 */
#define iana_clubpath_cam_regression				GetClubPath_Position_Regression		/* currently used only for CAMSENSOR_P3V2 */
#define removeoutlier				     			IANA_CLUB_RemoveOutlier	/* currently used only for CAMSENSOR_P3V2 */
#define modifyshot4p3v2								IANA_CLUB_ModifyShot4P3V2	/* currently used only for CAMSENSOR_P3V2 */
#define iana_clubpath_cam_display					DisplayClubPath				/* currently used only for CAMSENSOR_P3V2 */
#define check_isIron								IANA_CLUB_IsIron
#define iana_club_mark								IANA_CLUBMARK_Procedure or IANA_CLUB_GetClubmark
#define getp3d										"is this function placed in the correct place?" 
#define iana_club_mark_face							GetFaceData
#define club_mark_class_classfy						ClassifyClubmarkClass
#define club_mark_classification					GetBarDotCount
#define club_mark_region							GetClubmarkRegion
#define quadregression_errcalc						"is this function placed in the correct place?"
#define iana_compensateTransMat						CompensateTransMat
#define iana_compensateTransMat2					CompensateTransMat2
#define iana_config_conversion						IANA_ConverseConfiguration
#define iana_config_conversion2						IANA_ConverseConfiguration2

/* iana_checkready.cpp */
#define iana_toready								IANA_Prepare4Ready	


/* iana_core.cpp */
#define setcurrentdirstr	 						IANA_SetCurrentDir
#define iana_core_check_time						VerifyCamProcessingTime
#define iana_changerightleft						IANA_CORE_ChangeRightLeft
#define iana_changerightleft_LITE					IANA_CORE_ChangeRightLeft_LITE
#define updateLedLut								IANA_CAM_UpdateLedLut		/* in iana_cam.cpp */
#define updateGain									IANA_CAM_UpdateGain			/* in iana_cam.cpp */
#define iana_check_tsmatch							IANA_GetValidTsDiff
#define iana_do_goodshot							IANA_GoodShot_PostProcedure

/* iana_exile.cpp */
#define iana_exile									IANA_ExileBall 	 
#define iana_exile_cam2								IANA_ExileBallCandidate
#define iana_exile_cam_ransac						ExileBallCandidate_RANSAC
#define iana_exile_cam_ransac_id					ExileBallCandidate_RANSAC_ID
#define iana_exile_cam_overlaptime					CalcOverapTime

/* iana_shot.cpp */
#define iana_shot									IANA_SHOT_Procedure
#define iana_shot_refine							IANA_SHOT_RefineShotDirection
#define iana_shot_calc								IANA_SHOT_CalcShotDirection
#define iana_shot_candidate							GetShotCandidate
#define iana_shot_regression						RegressShotData
#define iana_shot_plane								IANA_SHOT_GetShotPlane
#define iana_shot_plane0							GetShotPlane0
#define iana_shot_plane3							GetShotPlane3
#define iana_shot_3Dtrajline						IANA_SHOT_Get3DTrajLine
#define iana_shot_allpointsonline					IANA_SHOT_GetAllPointsOnLine
#define iana_shot_pointonline						GetPointOnLine
#define iana_requestBulk							IANA_SHOT_RequestBulkImage
#define iana_requestBulk_TimeStamp					IANA_SHOT_RequestBulkImage_Timestamp
#define iana_putter_check							IANA_SHOT_SavePutterResult
#define iana_shot_check_readyball					CheckReadyBall
#define iana_shot_ballradius_3d						CalcBallRadiusUsing3DPositions
#define iana_shot_checkresult						CheckShotResult

/* iana_shot_recalc.cpp */
#define iana_shot_recalc							IANA_SHOT_ReCalcShotData

/* iana_simresult.cpp */
#define iana_simresult			 					IANA_WriteSimulationResult

/* iana_spin.cpp */
#define iana_spin 		 							IANA_SPIN_GetSpinData
#define iana_spin_check 	 						IANA_SPIN_Check
#define iana_spin_real_clubdata 		 			IANA_SPIN_GetFullShotData

/* iana_spin_ballclub.cpp */
#define iana_spin_clubpath							IANA_SPIN_GetClubPath
#define iana_spin_clubpath_cam3						GetClubPath3
#define iana_spin_clubpath_image3					ClubPath_ProcessImage3
#define iana_spin_clubpath_update_context		 	ClubPath_UpdateContext
#define iana_spin_clubpath_position_calc			ClubPath_CalcPosition
#define iana_spin_clubpath_preshot_filtering		ClubPath_FilterPreshot
#define iana_spin_clubpath_position_hough		 	ClubPath_CalcPosition_Hough
#define iana_spin_clubpath_position_shape		 	ClubPath_CalcPosition_Shape
#define iana_spin_clubpath_position_shape_refine	ClubPath_CalcPosition_RefineShape
#define iana_spin_clubpath_position_refine		  	ClubPath_RefinePosition
#define iana_spin_clubpath_getClubHposEst		 	ClubPath_GetClubHposEst
#define iana_spin_clubpath_getClubHposEst2		 	ClubPath_GetClubHposEst2
#define iana_spin_clubpath_position_regressionx		ClubPath_RegressPosition
#define iana_spin_clubpath_calcroi			 		ClubPath_CalcRoi
#define iana_spin_clubpath_calc			 			ClubPath_CalcClubData
#define iana_spin_clubimage_overlap			 		IANA_SPIN_GetClubImageOverlapped
#define iana_spin_draw_clubpath		 				DrawClubPath
#define iana_spin_ballclub_check					IANA_SPIN_SaveBallClubData 		 
#define iana_spin_ballclub_check_cam		 		SaveBallClubData
#define iana_spin_clubpath_spin		 				ClubPath_CalcSpin
#define iana_spin_clubpath_sidespin			 		ClubPath_CalcSpin_SideSpin
#define iana_spin_clubpath_backspin			 		ClubPath_CalcSpin_BackSpin
#define iana_clubimage_fit			 				IANA_SPIN_FitClubImage


/* iana_work.cpp */


		
				

#endif		// _IANA_DEPRICATED_H_


