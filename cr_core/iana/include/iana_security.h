/*!
 *******************************************************************************
                                                                                
                    Creatz Iana Interface
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2019 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana.h
	 @brief  
	 @author YongHo Suk
	 @date   2019/04/02 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_SECURITY_H_)
#define		 _IANA_SECURITY_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include "cr_common.h"

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/
HAND iana_security_create(HAND hscamif);
I32 iana_security_delete(HAND h); 
I32 iana_security_hscamif_set(HAND h, HAND hscamif);
I32 iana_security_hscamif_get(HAND h, HAND *phscamif);

I32 iana_security_pcinfo_mainboard_serial_read(HAND h, U08 bdserial[32]);
I32 iana_security_pcinfo_hdd_serial_read(HAND h, U08 hddserial[32]);
I32 iana_security_makeguid(HAND h, U32 guid[4]);
I32 iana_security_pcinfo_mb_hdd_hash(
	HAND h,
	U08 bdserial[32],
	U08 hddserial[32],
	U08 hashcode[32]);
I32 iana_security_hboardinfo_read(HAND h, void * /*hboardinfo_t	*/phbi);
I32 iana_security_hboardinfo_write(HAND h, void * /*hboardinfo_t	*/phbi);


I32 iana_security_pcinfo_read(HAND h, void *hhbi);
I32 iana_security_pcinfo_write(HAND h, void *hhbi);
I32 iana_security_pcinfo_make(HAND h, void *hpif);	
I32 iana_security_runcode_check(HAND h, U32 *presult);
I32 iana_security_runcode_update(HAND h, U32 good1bad0);
I32 iana_security_cpusid_read(HAND h, U32 cpusid[4]);

I32 iana_security_cpubdserial_read(HAND h, U32 bdserial[4]);
#if defined (__cplusplus)
}
#endif

#endif		// _IANA_SECURITY_H_

