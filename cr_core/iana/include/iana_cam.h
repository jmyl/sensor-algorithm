/*!
 *******************************************************************************
                                                                                
                    Creatz Iana cam
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_cam.h
	 @brief  
	 @author YongHo Suk
	 @date   2015/12/20 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_IANA_CAM_H_)
#define		 _IANA_CAM_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#define IANA_CAMCONF				4

#define IANA_CAMCONF_NULL			0
#define IANA_CAMCONF_BASE			1
#define IANA_CAMCONF_FULLRANGE		2
#define IANA_CAMCONF_RUN			3
#define IANA_CAMCONF_RUN_PUTTER		4
#define IANA_CAMCONF_POWERSAVING	5
#define IANA_CAMCONF_CHANGEME		((U32)(-1))



//--
/*
		{{ // center
			1280, 	//  width;
			1024, 	// U32 height;
			0,		// U32 offset_x;
			0,		// U32 offset_y;
			100,		// U32 framerate;
			(10 * 256), // U32 gain;
			20,		// U32 exposure;
			SKIP_PRERPARECAM_CENTER, 		// U32 skip;
			MULTITUDE_PRERPARECAM_CENTER, 		// U32 multitude;
			1 		// U32 syncdiv
*/

//
#define PRERPARECAM_CENTER_WIDTH		1280
#define PRERPARECAM_CENTER_HEIGHT		1024
#define PRERPARECAM_CENTER_FRAMERATE	100
#define PRERPARECAM_CENTER_GAIN			(10 * 256)
#define PRERPARECAM_CENTER_EXPOSURE		20
#define PRERPARECAM_CENTER_SKIP			4
#define PRERPARECAM_CENTER_MULTITUDE	4

#define PRERPARECAM_SIDE_WIDTH			1280
#define PRERPARECAM_SIDE_HEIGHT			1024
#define PRERPARECAM_SIDE_FRAMERATE		100
#define PRERPARECAM_SIDE_GAIN			(10 * 256)
#define PRERPARECAM_SIDE_EXPOSURE		20
#define PRERPARECAM_SIDE_SKIP			4
#define PRERPARECAM_SIDE_MULTITUDE		4



#define SKIP_PRERPARECAM_CENTER			4
#define MULTITUDE_PRERPARECAM_CENTER	1

#define SKIP_PRERPARECAM_SIDE			4
#define MULTITUDE_PRERPARECAM_SIDE		4

#define SKIP_READY_CENTER				4
#define MULTITUDE_READY_CENTER			1

#define SKIP_READY_SIDE					4
#define MULTITUDE_READY_SIDE			4



#define	CAMINTRINSIC_NULL				0
#define	CAMINTRINSIC_12mm0				1
#define	CAMINTRINSIC_8mm0				2
#define	CAMINTRINSIC_VARIFOCAL_3_8mm0	3
#define	CAMINTRINSIC_4mm0				4
//#define	CAMINTRINSIC_LAST				4

#define	CAMINTRINSIC_12mm1				5				// 20190320. Re-calc'ed.
#define	CAMINTRINSIC_8mm1				6				// 20190320. Re-calc'ed.

//#define	CAMINTRINSIC_LAST				6
#define	CAMINTRINSIC_25mm5MP			7				// 20191002

#define	CAMINTRINSIC_6mm5MP				8				// 20200317

#define	CAMINTRINSIC_12mm2				9				// 20201006, by hschoi


#define	CAMINTRINSIC_LAST				9


/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif
//typedef struct _iana_camconf_cam {
//	U32 width;
//	U32 height;
//	U32 offset_x;
//	U32 offset_y;
//	U32 framerate;
//	U32 gain;
//	U32 exposure;
//
//	//---
//	U32 skip;
//	U32 multitude;
//	U32 syncdiv;
//} iana_camconf_t;



typedef camimageinfo_t iana_camconf_t;



//iana_camconf_cam_t;

/*
typedef struct _iana_camconf {
	iana_camconf_cam_t	cconfc[2];
} iana_camconf_t;
*/

typedef struct _cameraintrinsic {
	U32 id;							// parameter set id.
	double cameraParameter[4];		// fx, cx, fy, cy
	double distCoeffs[5];			// k1, k2, p1, p2, k3
} cameraintrinsic_t;


typedef struct _cameraextrinsic {
	double rotationVector[3];		
	double translationVector[3];	
	double r[3][3];					// Rodridues matrix
	double extMat[4][4];			// [[R|t];[0 0 0 1]]
	double InvextMat[4][4];			// Inverse of extMat. 

} cameraextrinsic_t;
/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

I32 iana_cam_cconf(HAND hiana, U32 camconf);
I32 iana_cam_start(HAND hiana);
I32 iana_cam_restart(HAND hiana);
I32 iana_cam_stop(HAND hiana);

//I32 iana_cam_property(HAND hiana, U32 camid, iana_camconf_t *pccc);
I32 iana_cam_property(HAND hiana, U32 camid, HAND hscamif, iana_camconf_t *pccc);
//I32 iana_cam_property_BULK(HAND hiana, U32 camid, U32 width, U32 height, U32 offset_x, U32 offset_y);
I32 iana_cam_property_BULK(HAND hiana, U32 camid, HAND hscamif, U32 width, U32 height, U32 offset_x, U32 offset_y);
I32 iana_cam_fmcinit_config(HAND hiana, U32 camid, U32 fmcinit, U32 zrot, U32 autoinit);				// fmcinit: Do FMC Init or Not.    zrot: 1: ZROT mode,  0: NROT mode
I32 iana_cam_update(HAND hiana, HAND hscamif, U32 camid);
I32 iana_cam_update_all(HAND hiana, HAND hscamif);

//I32 iana_cam_runparam_init(HAND hiana);
I32 iana_cam_setprocessingarea(HAND hiana, U32 camid);


I32 iana_cam_getCamInterinsicParamAll(HAND hiana); 
I32 iana_cam_getCamInterinsicParam(HAND hiana, U32 paramid, cameraintrinsic_t *pcip);



I32 iana_sensor_setup(HAND hiana, U32 camsensorid);

I32 IANA_CAM_UpdateLedLut(HAND hiana, int forceupdate);
I32 IANA_CAM_UpdateGain(HAND hiana);



#if defined (__cplusplus)
}
#endif

#endif		// _IANA_CAM_H_

