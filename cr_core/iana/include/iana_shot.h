/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA shot
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_shot.h
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2016/02/24 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_IANA_SHOT_H_)
#define		 _IANA_SHOT_H_

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#define USE_IANA_BALL

#define IANA_BALL_EMPTY				0
#define IANA_BALL_EXIST				1
#define IANA_BALL_EXILE				2


#define IANA_CLUB_EMPTY				0
#define IANA_CLUB_EXIST				1
#define IANA_CLUB_EXILE				2




#define IANA_EXILE_NULL			0
#define IANA_EXILE_MAXDISTANCE	1
#define IANA_EXILE_TOOCLOSE		2
#define IANA_EXILE_SUMTD			3
#define IANA_EXILE_DIFFV			4
#define IANA_EXILE_YFARCHECK		5
#define IANA_EXILE_CLUBTOOCLOSE	6
#define IANA_EXILE_TOOFAR		7
#define IANA_EXILE_ONLYONE		8
#define IANA_EXILE_CERR			9
#define IANA_EXILE_RANSAC_XY			10
#define IANA_EXILE_RANSAC_YX			11
#define IANA_EXILE_RANSAC_TX			12
#define IANA_EXILE_RANSAC_TY			13
#define IANA_EXILE_RANSAC_THETA			14



#define IANA_EXILE_STARTPOINTTOOCLOSE	101
#define IANA_EXILE_TOOBIGY				102
#define IANA_EXILE_TOOSMALLY				103
#define IANA_EXILE_AREAOUT				104
#define IANA_EXILE_STARTPOINTCHECK		105
			


//#define READCOUNT	4
//#define READCOUNT	6			// 20160301..

//#define READCOUNT_CANDIDATE	12			// 20160630..
//#define READCOUNT_CANDIDATE	16			// 20161213
//#define READCOUNT_CANDIDATE	32			// 20161213
//#define READCOUNT_CANDIDATE	8			// 20170403
//#define READCOUNT_CANDIDATE	10			// 20170403
//#define READCOUNT_CANDIDATE	10			// 20170403
//#define READCOUNT_CANDIDATE	6			// 20171111
#define READCOUNT_CANDIDATE	14			// 20180501
//#define READCOUNT_CANDIDATE	20			// 20180501

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

I32 iana_shot(iana_t *piana);


I32 iana_shot_calc(iana_t *piana);
I32 iana_shot_plane(iana_t *piana, U32 camid);
I32 iana_shot_3Dtrajline(iana_t *piana);
I32 iana_shot_allpointsonline(iana_t *piana, U32 camid);
//I32 iana_shot_pointonline(iana_t *piana, U32 camid, cr_point_t *pongline, cr_point_t *ponline);

I32 iana_putter_check(iana_t *piana);


#if defined (__cplusplus)
}
#endif

#endif		// _IANA_SHOT_H_
