/*!
 *******************************************************************************

					CREATZ IANA shot calc.

	 @section copyright_notice COPYRIGHT NOTICE
		 Copyright (c) 2015 ~ 2016 by Creatz Inc.
		 All Rights Reserved. \n
		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
	 @section file_information FILE CREATION INFORMATION
	 @file   iana_spin_mark.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2015/12/26 Spin mark

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$
	$Author$
	$Date$
 *******************************************************************************/

 /*----------------------------------------------------------------------------
	 Description	: defines referenced header files
 -----------------------------------------------------------------------------*/
#include <math.h>
#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"
#include "cr_nanoxml.h"

#include <ipp.h>
#include <ippcc.h>

#include "scamif.h"
#include "scamif_buffer.h"

#include "iana.h"
#include "iana_work.h"
#include "iana_implement.h"
#include "iana_cam.h"
#include "iana_cam_implement.h"
#include "iana_balldetect.h"
#include "iana_shot.h"
#include "iana_tool.h"
#include "iana_spin.h"
#include "iana_spin_rotation.h"
#include "iana_spin_ballclub.h"
// #include "iana_club_attack.h"

#include "cr_circle.h"

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include "AK_algorithm.h"
#include "coord_translate.h"
#include "feature_define.h"

#include <vector>

#define BALL_RADIUS_WORLD 0.0214
#define FPS 2220.

using namespace cv;
using namespace Eigen;
using namespace std;

/*----------------------------------------------------------------------------
	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#pragma warning(disable:4456)

#ifdef __cplusplus
extern "C" {
#endif

/*----------------------------------------------------------------------------
*    Description	: Type definition of structures and data type
-----------------------------------------------------------------------------*/
#define REQUESTBULK_HERE

#define PIXELAREA_MIN	50  // 200, 100, 80, 50
#define PIXELAREA_MAX	1000// 1000,    

#define MROIOFFSET		2	

#define PIXELCOUNTMAX	(2*1024) // 1024
#define PIXELCOUNTMIN	20

#define MOREMORE	4

#define WINDOWRATIO	(1.0/3.0) // (1.0/4.0), (1.0/8.0), (1.0/2.0)
/*----------------------------------------------------------------------------
*    Description	: static variable declaration
-----------------------------------------------------------------------------*/

static int s_usebmi = 0;
static int s_use_rL3 = 1;

static double s_c0addx = 0;
static double s_c0addy = 0;
int s_sleep_mark = 10; // 100, 50, 10, 1
static int s_refineshot2 = 0; // 1
static int ddd_0 = 1;
static int eee_0 = 1;

static int s_pixelarea_min = PIXELAREA_MIN;

static int s_mo_edge_test = 0;
static int s_mesh_test = 0;

/*----------------------------------------------------------------------------
*    Description	: external and internal global variable
-----------------------------------------------------------------------------*/

extern int g_ccam_lite;
/*----------------------------------------------------------------------------
*	Description	: declares the external function prototype
-----------------------------------------------------------------------------*/
extern I32 iana_getballcandidator_set_minr_maxr(double minr, double maxr);
extern U32 markSeqD_old(iana_t *piana, U32 camid);				// !!!!! You are the No. 1!!!
extern U32 SetCrosshair(U08 *imgbufSrc, U32 width, U32 height, U32 thickness);
/*----------------------------------------------------------------------------
*	Description	: declares the function prototype
-----------------------------------------------------------------------------*/
I32 DetectDotMark(iana_t *piana, U32 camid, imagesegment_t imgseg[], double markangle[], U32 sx, U32 sy, point_t *pbcP, double radiusP, point_t *pcbL, double radiusL, U08 *buf, U08 *bufBW, double multfact);
I32 EqualizeBrightness(iana_t *piana, U32 cx, U32 cy, double radius, U32 width, U32 height, U08 *buf, U08 *buf2, U08 *buf3, U32 camid);

//--
U32 bulkFirstindex(iana_t *piana, U32 camid, U32 *pFirstindex);
U32 IANA_SHOT_RefineBallPos_Bulk(iana_t *piana, U32 camid);
U32 RefineBallPos_BulkRegression(iana_t *piana, U32 camid);
U32 RefineBallPos_BulkRegressionRANSAC(iana_t *piana, U32 camid);
U32	RegressionXY_RANSAC(double xarray[], double yarray[], U32 count, double coeff[3], U32 usequad);

U32 GetDotMarkSeq(iana_t *piana, U32 camid);
U32 GetDimpleMarkSeq_Legacy(iana_t *piana, U32 camid, vector<coord_t> &ftseqs,
	vector<size_t> &ftcnts);

U32 GetDimpleMarkSeq(iana_t *piana, U32 camid, vector<coord_t> &ftseqs,
	vector<size_t> &ftcnts);

U32 RefineShot_ShotPlaneProjection(iana_t *piana);
U32 RegressionShotLocalLine(iana_t *piana, U32 camid);
U32 PairingBall_Stereo(iana_t *piana);
U32 RefineShot_BulkStereo(iana_t *piana);

U32 markStore(iana_t *piana, U32 camid, U32 seqnum, U32 sx2, U32 sy2, imagesegment_t imgseg[], double markangle[], double multfact); // only for dotspin?

U32 markSpin(iana_t *piana, U32 camid); // only for dotspin?
U32 markNearest(iana_t *piana, U32 camid); // only for dotspin?
U32 markRotationVect(iana_t *piana, U32 camid); // only for dotspin?
U32 markRotationBest(iana_t *piana, U32 camid); // only for dotspin?
U32 refineSpin(cr_vector_t *paxis, double spinmag, double *pbackspin, double *psidespin); // modifyspin(fit to gcq , only for dotspin(markrotationbest)

I32 iana_requestBulk(iana_t *piana);
I32 iana_requestBulk_TimeStamp(iana_t *piana, U64 *pts64C, U64 *pts64S);

double calc_distance(markinfo_t *pmi0, markinfo_t *pmi1); // only for dotspin (marknearest)
U32 ballmark3D( // only for dotspin (markstore)
	iana_t *piana, 				//I
	cr_point_t *pcampos, 		//I
	cr_point_t *pballposL3D,	//I
	point_t *pmarkL,			//I
	cr_plane_t *pBallPlane,		//I
	cr_vector_t *pua, cr_vector_t *pub, cr_vector_t *puc, 		// I
	double lr_,					//I

	double *pcofa, double *pcofb, double *pcofc,		// O. Coefficient of ua,ub,uc
	cr_point_t *pmarkOnBP,			// O, Mark On Ball Plane
	cr_point_t *pmarkOnBPwrtBall,	// O, Mark On Ball Plane w.r.t. Ball
	cr_point_t *pmark3DwrtBall		// O, 3D position w.r.t. Ball
);

U32 ballmark3D2( // NOT only for dotspin (mainlt used for markstore, but also used for dimple to ballcoordi)
	iana_t *piana, 				//I
	cr_point_t *pcampos, 		//I
	cr_point_t *pballposL3D,	//I
	point_t *pmarkL,			//I
	double lr_,					//I
	cr_point_t *pmark3DwrtBall	// 3D position, wrt ball.
);

U32 ballmark3D3( // only for dotspin (markstore)
	iana_t *piana, 				//I
	U32 camid,
	cr_point_t *pcampos, 		//I
	cr_point_t *pballposL3D,	//I
	cr_point_t *pballposP,	//I
	point_t *pmarkP,			//I
	double lr_,					//I
	cr_point_t *pmark3DwrtBall	// 3D position, wrt ball.
);



U32 ballmarkLP(
	iana_t *piana, 				// I
	U32 camid,
	cr_point_t *pcampos, 		//I
	cr_point_t *pballposL3D,	// I,	Ball position
	cr_point_t *pmark3DwrtBall,	// I,	Mark 3D position w.r.t. Ball
	point_t	   *pmarkL,			// Mark L position
	point_t	   *pmarkP			// Mark P position
);



I32 markSpincheck(iana_t *piana, U32 camid);
I32 iana_spin_mark_make_simuldata(iana_t *piana);
I32 make_simuldata(cr_point_t *ppt, U32 maxcount, U32 *pmarkcount); // used only in iana_spin_mark_make_simuldata  

I32 mkrefimage(iana_t *piana, U32 camid); // make reference image?

double calcimageradius(iana_t *piana, U32 camid, U32 seqnum); // ?
U32 apparentBallRadius(iana_t *piana, U32 camid); // ?
U32 ballViewPlane(iana_t *piana, U32 camid, U32 seqnum); // ?

U32 modif_barnoise(U08* buf, I32 width, I32 height, I32 barStartX, I32 barEndX);

/*!
********************************************************************************
*	@brief      CAM detect mark
*
*  @param[in]	piana
*              IANA module handle
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/02/27
*******************************************************************************/
I32 DetectDotMark(iana_t *piana, U32 camid, imagesegment_t imgseg[], double markangle[], U32 sx, U32 sy, point_t *pbcP, double radiusP, point_t *pcbL, double radiusL, U08 *buf, U08 *bufBW, double multfact)
{ // TODO:  PH2_DIVIDE ? This function computes reference value to compute rotation, named markangle. That should be separated.
	I32 res;
	U32 widthI, heightI;
	U32 widthNet, heightNet;

	iana_cam_t	*pic;

	double thresholdmark;

    U08 *pimgsrc1;
	U08 *pimgdest;

	double rP;

    
    cv::Mat cvImgsrc, cvImgBall, cvImgBinary, cvImgMorph;
    cv::Mat cvMean, cvStdev, cvKernel(cv::Size(3, 3), CV_8UC1);
    cvKernel = 1;
    //---------------------------
#define MOME_TEST
#if defined(MOME_TEST)
	{
		FILE *fp;
//#define SPINSIMULFILE		"spinsimul.txt"
#pragma warning(disable:4996)
		fp = fopen("D:\\mome.txt", "r");
		if (fp) {
			fscanf(fp, "%d", &s_mo_edge_test);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
			fscanf(fp, "%d", &s_mesh_test);
			fclose(fp);
		}
#pragma warning(default:4996)
	}
#else
	s_mo_edge_test = 0;
	s_mesh_test = 0;
#endif

	pic = piana->pic[camid];

	widthI = WIDTHI;
	heightI = HEIGHTI;

    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, widthI, heightI, 3, 1, 2, 3, piana->imguserparam);
    }

	widthNet = (U32)(radiusP * multfact * 2);
	heightNet = (U32)(radiusP * multfact * 2);
	rP = radiusP * multfact;

    cvImgsrc = cv::Mat(cv::Size(WIDTHI, HEIGHTI), CV_8UC1, buf);
    cvImgBall = cvImgsrc(cv::Rect(0, 0, widthNet, heightNet));
    cv::meanStdDev(cvImgBall, cvMean, cvStdev);

#define MEAN2THRESHOLD	0.95 // 1.5, 1.0, 0.9~0.5, 0.3
#define STDDEV_RATIO	0.3 // 0.4, 0.5, 0.7, 1.0
	thresholdmark = cvMean.at<double>(0) - cvStdev.at<double>(0) * STDDEV_RATIO;

    cv::threshold(cvImgBall, cvImgBinary, thresholdmark, 255., cv::THRESH_BINARY_INV);

    cv::morphologyEx(cvImgBinary, cvImgMorph, cv::MORPH_ERODE, cvKernel, cv::Point(-1, -1), 1, cv::BORDER_ISOLATED);

	pimgsrc1 = buf;
	pimgdest = pic->pimg[0];

    cvImgsrc = 0;
    cvImgMorph.copyTo(cvImgsrc(cv::Rect(0, 0, widthNet, heightNet)));
    memcpy(pimgdest, cvImgsrc.data, widthI*heightI);

	if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgMorph.data, cvImgMorph.cols, cvImgMorph.rows, 4, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgsrc.data, cvImgsrc.cols, cvImgsrc.rows, 5, 1, 2, 3, piana->imguserparam);
	}

	// 2) Cut marginal.... r * 0.9
	{
		I32 cxi, cyi;
		I32 cri0, cri1;
		double discardrate;

#define DISCARDRATE	0.90 // 0.95, 0.93, 0.85, 0.8, 0.80, 0.75, 0.70
#define DISCARDRATE_CONSOLE1	0.95

		if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
			discardrate = DISCARDRATE;
		} else {
			discardrate = DISCARDRATE_CONSOLE1;
		}

		cri0 = (I32)(rP * discardrate);
		cri1 = (I32)(rP);
		cxi = (I32)(rP);
		cyi = (I32)(rP);

		mycircleGray3(cxi, cyi, cri0, cri1, (U08)0x00, pimgdest, widthI);
		// mycircleGray2(cxi, cyi, cri, (U08)0x00, pimgdest, widthI); // cri +- 1
	}

    memcpy(bufBW, pimgdest, widthI*heightI);			// backup BW-data.

	if (piana->himgfunc) {
		U08 *pimg;
		I32 cxi, cyi, cri1;

		//---
		((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)bufBW, widthI, heightI, 4, 1, 2, 3, piana->imguserparam);

		pimg = (U08 *)malloc((widthI + 16)*(heightI + 16));
		memcpy(pimg, pimgdest, widthI*heightI);

		cri1 = (I32)(rP);
		cxi = (I32)(rP + 0.5);
		cyi = (I32)(rP + 0.5);

		mycircleGray(cxi, cyi, cri1, 0xFF, pimg, widthI);
		mycircleGray(cxi, cyi, cri1 + 1, 0x00, pimg, widthI);
		((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimg, widthI, heightI, 4, 1, 2, 3, piana->imguserparam);
		free(pimg);
	}

	// 3) Labeling and segmentation
	{
		U08 *pimg;
		I32 i;
		imagesegment_t 	*pis;
		U08 segindex;

		if (s_mesh_test) {
#define SEGMENTWIDTH_M	10 // 16
            SetCrosshair(pimgdest, widthNet, heightNet, SEGMENTWIDTH_M);

			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgdest, widthI, heightI, 1, 1, 2, 3, piana->imguserparam);
			}
		}
        
        pimg = (U08*)malloc(sizeof(U08) * WIDTHI * HEIGHTI);
        memset(pimg, 0, sizeof(U08) * WIDTHI * HEIGHTI);

		{ 	//-- Labeling
            cv::Mat cvCrosshaired(cv::Size(WIDTHI, HEIGHTI), CV_8UC1, pimgdest), cvBallCrosshaired, cvLabelled, cvLabelledChar;
            cv::Mat cvStats, cvCents;
            int labelCnt;
            
            pimgsrc1 = pimgdest;
			pimgdest = pic->pimg[1];
			memcpy(pimgdest, pimgsrc1, widthI*heightI);

            cvBallCrosshaired = cvCrosshaired(cv::Rect(0, 0, widthNet, heightNet));
            labelCnt = cv::connectedComponentsWithStats(cvBallCrosshaired, cvLabelled, cvStats, cvCents, 8, CV_32S);
            cvLabelled.convertTo(cvLabelledChar, CV_8UC1);
            labelCnt = labelCnt < MAXLABEL ? labelCnt : MAXLABEL;

			//-- Segm.
			memset(imgseg, 0, sizeof(imagesegment_t) * MAXLABEL);
			for (i = 0; i < MAXLABEL; i++) {
				pis = &imgseg[i];
				pis->state = IMAGESEGMENT_NULL;
				pis->label = i;
			}

            for (i = 1; i < labelCnt; i++) {
                pis = &imgseg[i];

                pis->state = IMAGESEGMENT_FULL;
                pis->ixl = cvStats.at<int>(i, 0);
                pis->iyu = cvStats.at<int>(i, 1);
                pis->ixr = cvStats.at<int>(i, 0) + cvStats.at<int>(i, 2);
                pis->iyb = cvStats.at<int>(i, 1) + cvStats.at<int>(i, 3);
                pis->pixelcount = cvStats.at<int>(i, 4);

                pis->mx = cvCents.at<double>(i, 0);
                pis->my = cvCents.at<double>(i, 1);
            }

            memcpy(pimg, cvLabelledChar.data, sizeof(U08) * widthNet * heightNet);
            ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvLabelledChar.data, widthNet, heightNet, 0, 1, 2, 3, piana->imguserparam);
		}
        
		{ 	// Mark angle
			U32 ii, jj;
			U32 count;
			double xx[PIXELCOUNTMAX + 1], yy[PIXELCOUNTMAX + 1];
			double mx_, bx_;
			for (i = 0; i < MAXLABEL; i++) {
				pis = &imgseg[i];
				count = 0;
				if (pis->state == IMAGESEGMENT_FULL) {
					if (pis->pixelcount < PIXELCOUNTMAX) {
						if (pis->pixelcount >= PIXELCOUNTMIN) {
							U32 w, h;
							w = pis->ixr - pis->ixl + 1;
							h = pis->iyb - pis->iyu + 1;
							if (w*h < PIXELAREA_MIN || w * h > PIXELAREA_MAX) {
								pis->state = IMAGESEGMENT_NULL;
								continue;
							}

							for (ii = pis->iyu; ii <= pis->iyb; ii++) {				// X-axis
								for (jj = pis->ixl; jj <= pis->ixr; jj++) {
									segindex = *(pimg + jj + ii * widthNet);
									if (segindex == i) {
										xx[count] = jj;
										yy[count] = ii;
										count++;
										break;
									}
								}
								for (jj = pis->ixr; jj >= pis->ixl; jj--) {
									segindex = *(pimg + jj + ii * widthNet);
									if (segindex == i) {
										xx[count] = jj;
										yy[count] = ii;
										count++;
										break;
									}
								}
							}
							for (jj = pis->ixl; jj <= pis->ixr; jj++) {
								for (ii = pis->iyu; ii <= pis->iyb; ii++) {
									segindex = *(pimg + jj + ii * widthNet);
									if (segindex == i) {
										xx[count] = jj;
										yy[count] = ii;
										count++;
										break;
									}
								}
								for (ii = pis->iyb; ii <= pis->iyu; ii--) {
									segindex = *(pimg + jj + ii * widthI);
									if (segindex == i) {
										xx[count] = jj;
										yy[count] = ii;
										count++;
										break;
									}
								}
							}
							cr_regression(xx, yy, count, &mx_, &bx_);

							markangle[i] = RADIAN2DEGREE(atan(mx_));
							if (markangle[i] < 0.0) {
								markangle[i] += 180.0;
							}
						} else {
							pis->state = IMAGESEGMENT_NULL;			// Discard
						}
					} else {
						pis->state = IMAGESEGMENT_NULL;			// Discard too big segment..
					}
				}
			}
		}

		for (segindex = 0; segindex < MAXLABEL; segindex++) {
			pis = &imgseg[segindex];
			if (pis->state == IMAGESEGMENT_FULL) {
				I32 j, k;
				I32 dx, dy;
				I32 dist;
				I32 mind;
				I32 mj, mk;
				I32 cx, cy;

				mind = 100000;
				mj = pis->iyu; mk = pis->ixl;
				
                cx = (I32)(multfact * (pbcP->x - sx));
				cy = (I32)(multfact * (pbcP->y - sy));
				
                for (j = (I32)pis->iyu; j <= (I32)pis->iyb; j++) {
					for (k = (I32)pis->ixl; k <= (I32)pis->ixr; k++) {
						if (segindex == *(pimg + k + j * widthNet)) {
							dx = (I32)(k - cx);
							dy = (I32)(j - cy);
							dist = dx * dx + dy * dy;
							if (dist < mind) {
								mj = j;
								mk = k;
								mind = dist;
							}
						}
					}
				}
				
                pis->minx = mk;
				pis->miny = mj;
				pis->mind = sqrt((double)mind);
				//				cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]: %5d  %5d : %10lf (%5d %5d)\n", segindex, mk, mj, pis->mind, cx, cy);
			}
		}
	}
	piana; camid; sx; sy; pbcP; radiusP; pcbL; radiusL; buf;

	res = 1;
	return res;
}

//static 
void pseudoinv32(double u[3][2], double ud[2][3])
{
	int i, j, k;
	double ut[2][3];
	double utu[2][2];
	double utuInv[2][2];
	double det;

	//--
	/*
	utu[0][0] = ut[0][0] * u[0][0] + ut[0][1] * u[1][0] + ut[0][2] * u[2][0];
	utu[1][0] = ut[1][0] * u[0][0] + ut[1][1] * u[1][0] + ut[1][2] * u[2][0];

	utu[0][1] = ut[0][0] * u[0][1] + ut[0][1] * u[1][1] + ut[0][2] * u[2][1];
	utu[1][1] = ut[1][0] * u[0][1] + ut[1][1] * u[1][1] + ut[1][2] * u[2][1];
	*/


	for (i = 0; i < 3; i++) {			// Transpose
		for (j = 0; j < 2; j++) {
			ut[j][i] = u[i][j];
		}
	}

	for (i = 0; i < 2; i++) {
		for (j = 0; j < 2; j++) {
			utu[i][j] = 0.0;
			for (k = 0; k < 3; k++) {
				utu[i][j] += ut[i][k] * u[k][j];
				//utu[i][j] += u[k][i] * u[k][j];
			}
		}
	}

	//#define DET_TOOSMALL	(0.0000001)
#define DET_TOOSMALL	(1e-7)
	det = utu[0][0] * utu[1][1] - utu[0][1] * utu[1][0];
	if (det > -DET_TOOSMALL && det < DET_TOOSMALL) {		// BAD..
		for (i = 0; i < 2; i++) {
			for (j = 0; j < 3; j++) {
				ud[i][j] = 0.0;
			}
		}
	}
	else {
		utuInv[0][0] = utu[1][1] / det;
		utuInv[0][1] = -utu[1][0] / det;
		utuInv[1][0] = -utu[0][1] / det;
		utuInv[1][1] = utu[0][0] / det;
	}

	ud[0][0] = utuInv[0][0] * ut[0][0] + utuInv[0][1] * ut[1][0];
	ud[0][1] = utuInv[0][0] * ut[0][1] + utuInv[0][1] * ut[1][1];
	ud[0][2] = utuInv[0][0] * ut[0][2] + utuInv[0][1] * ut[1][2];

	ud[1][0] = utuInv[1][0] * ut[0][0] + utuInv[1][1] * ut[1][0];
	ud[1][1] = utuInv[1][0] * ut[0][1] + utuInv[1][1] * ut[1][1];
	ud[1][2] = utuInv[1][0] * ut[0][2] + utuInv[1][1] * ut[1][2];

	for (i = 0; i < 2; i++) {
		for (j = 0; j < 3; j++) {
			ud[i][j] = 0.0;
			for (k = 0; k < 2; k++) {
				ud[i][j] += utuInv[i][k] * ut[k][j];
			}
		}
	}
}

static U32 pseudoinv3N(cr_vector_t v1[], cr_vector_t v2[], double pinv[][3], U32 n)
{
	U32 i, j, k;
	double *v1m, *v2m;
	double *v1mT;
	double v1mv1mT[3][3];
	double v1mv1mTinv[3][3];
	double *v2mv1mT;
	double sum;
	double value1, value2;
	double det;

	U32 res;

	//--
	v1m = (double *)malloc(3 * n * sizeof(double));			// 3xn
	v2m = (double *)malloc(3 * n * sizeof(double));			// 3xn
	v1mT = (double *)malloc(3 * n * sizeof(double));			// nx3
	v2mv1mT = (double *)malloc(3 * 3 * sizeof(double));

	for (i = 0; i < n; i++) {
		*(v1m + n * 0 + i) = v1[i].x;
		*(v1m + n * 1 + i) = v1[i].y;
		*(v1m + n * 2 + i) = v1[i].z;

		*(v1mT + 0 + i * 3) = v1[i].x;
		*(v1mT + 1 + i * 3) = v1[i].y;
		*(v1mT + 2 + i * 3) = v1[i].z;

		*(v2m + n * 0 + i) = v2[i].x;
		*(v2m + n * 1 + i) = v2[i].y;
		*(v2m + n * 2 + i) = v2[i].z;
	}

	//-- V1 * V1T
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			sum = 0;
			for (k = 0; k < n; k++) {
				//sum = sum + a[i][k] * b[k][j];
				value1 = *(v1m + i * n + k);
				value2 = *(v1mT + k * 3 + j);
				sum = sum + value1 * value2;
			}
			//c[i][j] = sum;
			v1mv1mT[i][j] = sum;
		}
	}

	// INV(V1*V1T)
	det = mat3inv(v1mv1mT, v1mv1mTinv);

#define PINV_DET_MIN	(1e-17) // (1e-13)
	if (det > -PINV_DET_MIN && det < PINV_DET_MIN) {
		res = 0;
		goto func_exit;
	}
	// V2 * V1T
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			sum = 0;
			for (k = 0; k < n; k++) {
				value1 = *(v2m + i * n + k);
				value2 = *(v1mT + k * 3 + j);
				sum = sum + value1 * value2;
			}
			*(v2mv1mT + i * 3 + j) = sum;
		}
	}

	// (V2*V1T) * INV(V1*V1T)


	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
			sum = 0;
			for (k = 0; k < 3; k++) {
				value1 = *(v2mv1mT + i * 3 + k);
				value2 = v1mv1mTinv[k][j];
				sum = sum + value1 * value2;
			}
			pinv[i][j] = sum;
		}
	}
	res = 1;

func_exit:
	free(v1m);
	free(v2m);
	free(v1mT);
	free(v2mv1mT);

	return res;
}

/*!
********************************************************************************
*	@brief      CAM ball image brightness equalization3
*
*  @param[in]	pcbP
*              ball center position
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/11/10
*******************************************************************************/
I32 EqualizeBrightness(
	iana_t *piana,
	U32 cx, U32 cy, double radius,
	U32 width, U32 height,
	U08 *buf, U08 *buf2, U08 *buf3,
	U32 camid
)
{ // HOLDED: There's some unclear points on sx, ex, sy, ex and we cannot use try catch and classes with destructor. Work later...

	I32 res;
	I32 i, j;
	I32 ii, jj;

	U32 widthT, heightT;			// Target area width/height
	I32 sxT, syT, exT, eyT;
	U32 widthInteg, heightInteg;	// Integration area width/height
	I32 sxInteg, syInteg, exInteg, eyInteg;
	U32 halfWinsize;				// Window half size
	U32 IntegWinsize;				// Window half size
	IppiSize roiSizeInteg;
	Ipp32f *pInteg;
	float sum, value;

    cv::Mat cvImgBuf(cv::Size(width, height), CV_8UC1, buf), cvImgBordered_2integ, cvImgInteg, cvImgBin;
    int is, ie, js, je;
	// For Adaptive thresholding,
	// Get windowed mean and windowed std for each pixel
	// Window size is...   2*L
	// 1) Select area.    Target Area: (cx - widthT/2, cy - heightT/2) ~ (cx + widthT/2, cy+heightT/2)
	// 2) Integration area:  Target Area -L ~ Area + L
    
#define ADDMETHANKU	0.5 // 1
	widthT = (U32)(2 * (radius + ADDMETHANKU));
	heightT = (U32)(2 * (radius + ADDMETHANKU));

	halfWinsize = (U32)(radius * WINDOWRATIO);

	widthInteg = widthT + halfWinsize * 2;
	heightInteg = heightT + halfWinsize * 2;

	sxT = (I32)(cx - widthT / 2);
	exT = (I32)(cx + widthT / 2);

	syT = (I32)(cy - heightT / 2);
	eyT = (I32)(cy + heightT / 2);

	if (sxT < 0) {
        sxT = 0;
    }
	if (exT > (I32)width) {
        exT = (I32)width;
    }
	if (syT < 0) {
        syT = 0;
    }
	if (eyT > (I32)height) {
        eyT = (I32)height;
    }
    
    //cv::copyMakeBorder(
    //    cvImgBuf(cv::Rect(sxT, syT, exT-sxT, eyT-syT)), cvImgBordered_2integ,
    //    halfWinsize, halfWinsize, halfWinsize, halfWinsize,
    //    cv::BORDER_REPLICATE); // BORDER_REFLECT, BORDER_REFLECT101
    //cv::integral(cvImgBordered_2integ, cvImgInteg);

    cv::integral(cvImgBuf(cv::Rect(sxT, syT, exT-sxT, eyT-syT)), cvImgInteg);
    cv::copyMakeBorder(
        cvImgInteg, cvImgBordered_2integ,
        halfWinsize, halfWinsize, halfWinsize, halfWinsize,
        cv::BORDER_REPLICATE); // BORDER_REFLECT, BORDER_REFLECT101

    for (i = 0; i < (I32)heightT; i++) {
        is = i < (int)halfWinsize ? (int)halfWinsize : i;
        ie = i + (int)halfWinsize*2; ////////////////////////////////////////

        for (j = 0; j < (I32)widthT; j++) {
            js = j < (int)halfWinsize ? (int)halfWinsize : j;
            je = j < (int)halfWinsize ? (int)halfWinsize : j;
            sum = cvImgBordered_2integ.at<int>(j + halfWinsize*2, i + halfWinsize*2) 
                - cvImgBordered_2integ.at<int>(j + halfWinsize*2, i)
                - cvImgBordered_2integ.at<int>(j, i + halfWinsize*2)
                + cvImgBordered_2integ.at<int>(j, i);

            sum =
                *(pInteg + (ii - halfWinsize) * (IntegWinsize + 1) + (jj - halfWinsize))
                - *(pInteg + (ii - halfWinsize) * (IntegWinsize + 1) + (jj + halfWinsize))
                - *(pInteg + (ii + halfWinsize) * (IntegWinsize + 1) + (jj - halfWinsize))
                + *(pInteg + (ii + halfWinsize) * (IntegWinsize + 1) + (jj + halfWinsize));

            sum = sum / (halfWinsize*halfWinsize * 4);

#define SUMMULT	0.5f // 0.5f, 0.6f, 1.0f, 0.8f
#define SUMSUM	0x40 // 0x40, 0x40, 0x80, 0x60, 0x50
#define SUMMULT2 1.0f // 1,2f

            sum = sum * SUMMULT;
            value = ((float)(*(buf + j + width * i)) - (sum - SUMSUM));
            value *= SUMMULT2;

            if (value < 0) {
                value = 0;
            }
            if (value > 0xFF) {
                value = 0xFF;
            }

            *(buf2 + WIDTHS * (syT + i) + (sxT + j)) = (U08)value;
        }
    }

	sxInteg = sxT - halfWinsize;
	exInteg = exT + halfWinsize;

	syInteg = syT - halfWinsize;
	eyInteg = eyT + halfWinsize;

	if (exInteg > (I32)width) {
		res = 0;
		goto func_exit;
	}

	if (eyInteg > (I32)height) {
		res = 0;
		goto func_exit;
	}

	IntegWinsize = roiSizeInteg.width = exInteg - sxInteg + 1;
	roiSizeInteg.height = eyInteg - syInteg + 1;

	pInteg = (Ipp32f *)malloc((widthInteg + 10) * (heightInteg + 10) * sizeof(Ipp64f) * MOREMORE);

	__try { 				// __try / __except().. 20180407
		ippiIntegral_8u32f_C1R(
			buf + sxInteg + width * syInteg,
			width,
			pInteg,
			(IntegWinsize + 1) * sizeof(Ipp32f),
			roiSizeInteg, 0.0);

		//if (piana->himgfunc) {
		//	((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf, 		WIDTHS, HEIGHTS, 2, 1, 2, 3, piana->imguserparam);
		//	//((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimgeqz, 	WIDTHS, HEIGHTS, 3, 1, 2, 3, piana->imguserparam);
		//	((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf2, 	WIDTHS, HEIGHTS, 4, 1, 2, 3, piana->imguserparam);
		//}


		for (i = 0; i < (I32)heightT; i++) {
			for (j = 0; j < (I32)widthT; j++) {
				value = *(buf + j + width * i);
				*(buf3 + WIDTHS * (syT + i) + (sxT + j)) = (U08)value;
			}
		}

		if (piana->himgfunc) {
			((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf3, WIDTHS, HEIGHTS, 2, 1, 2, 3, piana->imguserparam);
		}

		for (i = 0; i < (I32)heightT; i++) {
			ii = i + halfWinsize;
			for (j = 0; j < (I32)widthT; j++) {
				jj = j + halfWinsize;
				sum =
					*(pInteg + (ii - halfWinsize) * (IntegWinsize + 1) + (jj - halfWinsize))
					- *(pInteg + (ii - halfWinsize) * (IntegWinsize + 1) + (jj + halfWinsize))
					- *(pInteg + (ii + halfWinsize) * (IntegWinsize + 1) + (jj - halfWinsize))
					+ *(pInteg + (ii + halfWinsize) * (IntegWinsize + 1) + (jj + halfWinsize));

				sum = sum / (halfWinsize*halfWinsize * 4);

#define SUMMULT	0.5f // 0.5f, 0.6f, 1.0f, 0.8f
#define SUMSUM	0x40 // 0x40, 0x40, 0x80, 0x60, 0x50
#define SUMMULT2 1.0f // 1,2f

				sum = sum * SUMMULT;
				value = ((float)(*(buf + j + width * i)) - (sum - SUMSUM));
				value *= SUMMULT2;
				
                if (value < 0) {
                    value = 0;
                }
				if (value > 0xFF) {
                    value = 0xFF;
                }

				*(buf2 + WIDTHS * (syT + i) + (sxT + j)) = (U08)value;
			}
		}
	}
	__except (GetExceptionCode() == EXCEPTION_ACCESS_VIOLATION
		|| GetExceptionCode() == EXCEPTION_ARRAY_BOUNDS_EXCEEDED
		) {
		// .... :)
	}

	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf2, WIDTHS, HEIGHTS, 3, 1, 2, 3, piana->imguserparam);
	}

	free(pInteg);

	res = 1;
	camid; buf2;
func_exit:
	return res;
}

/*!
********************************************************************************
*	@brief      get FirstIndex for bulk image buffer
*
*  @param[in]	piana
*              IANA module handle
*  @param[in]	camid
*              camera id
*  @param[out]	pFirstindex
*              First index for bulk image
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/0916
*******************************************************************************/
U32 bulkFirstindex(iana_t *piana, U32 camid, U32 *pFirstindex)
{
	U32 res;
	U32 rindex;
	U32 i;

	rindex = 0;
	res = 0;
	if (piana->opmode == IANA_OPMODE_FILE) {
		rindex = piana->info_firstindex_bulk[camid];
		res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, rindex, NULL, NULL);
	}
	else {
		for (i = 0; i < 10; i++) {
			res = scamif_imagebuf_read(piana->hscamif, camid, NORMALBULK_BULK, NULL, NULL, &rindex);
			if (res == 1) {
				break;
			}
			Sleep(1);
		}
	}

	*pFirstindex = rindex;
	return res;
}

/*!
********************************************************************************
*	@brief      Refined ball position
*
*  @param[in]	piana
*              IANA module handle
*  @param[in]	camid
*              camera id
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/0916
*******************************************************************************/
#define WIDTHSRB	128
#define HEIGHTSRB	128
static I32	widthsrb = WIDTHSRB;
static I32	heightsrb = HEIGHTSRB;
static U32 s_useEP_flag = 1;
U32 IANA_SHOT_RefineBallPos_Bulk(iana_t *piana, U32 camid)
{
	U32	i;
	U32 res;

	iana_cam_t			*pic;
	iana_cam_param_t 	*picp;
	camimageinfo_t     	*pcii;
	camif_buffer_info_t *pb;
	marksequence_t		*pmks;
	ballmarkinfo_t		*pbmi;
	U64 				ts64shot;

	U16 	*sumbuf16;
    cv::Mat cvSumBuf;
	U16 	*curbuf16;
	U32		sumbufwidth;
	U32		sumbufheight;
	U32		sumbufinited;
	U32		sumbufcount;
	U32		sumbufcount2;

	cv::Size buf16roisize;


	U32		itercount;
	U32		setrindexshotbulk;
	U32		rindexshotbulk;
	U08 	*buf;
	U08 	*bufOrg;

	U32 	useEP;

	U32 marksequencelen;
	U32		noballcount;
	U32 	goodballcount;
	U32		goodball;

	//---
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	}
	else {
		marksequencelen = MARKSEQUENCELEN;
	}

	pic = piana->pic[camid];
	picp = &pic->icp;
	pmks = &pic->mks;
	pbmi = &pic->bmi[0];
	ts64shot = pic->ts64shot;

	pb = NULL;
	memset(pmks->ballPos2valid, 0, sizeof(U32) * MARKSEQUENCELEN);
	memset(pmks->ballPosvalid, 0, sizeof(U32) * MARKSEQUENCELEN);
	memset(pmks->valid, 0, sizeof(U32) * MARKSEQUENCELEN);

	memset(pbmi, 0, sizeof(ballmarkinfo_t) * MARKSEQUENCELEN);

	for (i = 0; i < MARKSEQUENCELEN; i++) {
		pmks->ballposP2Raw[i].x = -999;
		pmks->ballposP2Raw[i].y = -999;
	}

	sumbuf16 = NULL;
	curbuf16 = NULL;
	sumbufwidth = 0;
	sumbufheight = 0;
	sumbufinited = 0;
	sumbufcount = 0;
	sumbufcount2 = 0;
	buf16roisize.width = 100;		// FAKE
	buf16roisize.height = 100;		// FAKE

	setrindexshotbulk = 0;
	rindexshotbulk = 0;

	if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
		useEP = 1;
	}
	else {
		useEP = 0;
	}

	if (s_useEP_flag == 0) {
		useEP = 0;
	}

	useEP = 0;
	if (piana->processmode[camid] == CAMPROCESSMODE_BALLCLUB) {
		//itercount = (MARKSEQUENCELEN * 2) / 3;
		//itercount = (MARKSEQUENCELEN * 3) / 4;
		itercount = marksequencelen;
	}
	else {
		itercount = marksequencelen;
	}

	buf = piana->pic[camid]->pimg[0];

	noballcount = 0;
	goodballcount = 0;

	for (i = 0; i < MARKSEQUENCELEN; i++) {
		pbmi[i].ts64 = 0;
	}

	for (i = 0; i < itercount; i++) {
		U32		width;
		U32		height;
		U32		offset_x;
		U32		offset_y;
		U32 	rindex;
		I32 	startx, starty;

		U64 	ts64;
		I64 	ts64delta;
		double 	ts64deltad;

		point_t bcL;	// Ball center, local
		double 	lr_;
		point_t bcP; 	// Ball center, pixel
		double 	pr_;

		point_t bcPEst; 	// Ball center, Estimated.
		double 	pr_Est;		// Ball radius, Estimated

		//----
		bufOrg = NULL;

		goodball = 0;
		rindex = pic->firstbulkindex + i;
		// 1) get image buffer
		if (piana->opmode == IANA_OPMODE_FILE) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, rindex, &pb, &bufOrg);
			if (res == 0) {
				break;
			}
		}
		else {
			U32 j;
			//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bulk, img read\n", i);
			res = 0;
			for (j = 0; j < 20; j++) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, rindex, &pb, &bufOrg);

                if (res == 1) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read done. (%d, %d, %d)\n", camid, i, j);
					break;
				}
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read .... nogood. (%d, %d, %d)\n", camid, i, j);
				Sleep(1);
			}

			if (j != 0) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read .... nogood. (%d, %d, %d)\n", camid, i, j);
			}
			if (res == 0) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail. No more. [%d, %d:%d]Break\n", camid, i, j);
				break;
			}
		}

		pcii = &pb->cii;
		width = pcii->width;
		height = pcii->height;
		offset_x = pcii->offset_x;
		offset_y = pcii->offset_y;

#define GAMMA_REFINEBALLPOS
#if defined(GAMMA_REFINEBALLPOS)
	    if (camid == 0) {
		    if (piana->camsensor_category == CAMSENSOR_EYEXO) {
				    memcpy(buf, bufOrg, width*height);
		    } else {
			    memcpy(buf, bufOrg, width*height);
		    }
	    } else {
		    if (piana->camsensor_category == CAMSENSOR_EYEXO) {
			    gamma_correction(bufOrg, buf, width, height);
		    } else {
			    gamma_correction(bufOrg, buf, width, height);
		    }
	    }
#endif

        if (sumbufinited == 0) {
			sumbufwidth = width;
			sumbufheight = height;
		
            sumbuf16 = (U16 *)malloc(width * height * sizeof(U16));
			curbuf16 = (U16 *)malloc(width * height * sizeof(U16));
			
            memset(sumbuf16, 0, width * height * sizeof(U16));
			memset(curbuf16, 0, width * height * sizeof(U16));
			
            cvSumBuf = cv::Mat(cv::Size(width, height), CV_16UC1);
            cvSumBuf = 0;

            sumbufinited = 1;
			sumbufcount = 0;
			sumbufcount2 = 0;

			buf16roisize.width = width;
			buf16roisize.height = height;
		}

		if (sumbufinited) {

#define SUMBUFCOUNTCALCCOUNT	2 // 8
			if (width == sumbufwidth && height == sumbufheight) {

				cv::Size buf16roisize2;
                cv::Mat cvBuf(buf16roisize, CV_8UC1, buf), cvBuf16;
				
                cvBuf.convertTo(cvBuf16, CV_16UC1);

				buf16roisize2.width = width / 2;
				buf16roisize2.height = height;
                
				if (sumbufcount < SUMBUFCOUNTCALCCOUNT) {
                    cv::Mat cvSumLeft = cvSumBuf(cv::Rect(0, 0, width/2, height)) + cvBuf16(cv::Rect(0, 0, width/2, height));
                    cvSumLeft.copyTo(cvSumBuf(cv::Rect(0, 0, width/2, height)));
					sumbufcount++;
				}

				if (i > marksequencelen - SUMBUFCOUNTCALCCOUNT * 3) {
					if (sumbufcount2 < SUMBUFCOUNTCALCCOUNT) {
                        cv::Mat cvSumRight = cvSumBuf(cv::Rect(width/2, 0, width/2, height)) + cvBuf16(cv::Rect(width/2, 0, width/2, height));
                        cvSumRight.copyTo(cvSumBuf(cv::Rect(width/2, 0, width/2, height)));
						sumbufcount2++;
					}
				}
			}
		}

		ts64 = MAKEU64(pb->ts_h, pb->ts_l);
		pmks->ts64[i] = ts64;
		pbmi[i].ts64 = ts64;
#define TS64DIFF_MIN	(1 * 1000000)		// nsec * 1000000 = msec
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%d] ts64: %016llx (%10lf)\n", camid, i, ts64, ts64/TSSCALE_D);

		//--------------------- 
		// 2) Ball center, radius
		{	// Estimation-based
			point_t L2; 	// for radius calc
			point_t P2; 	// for radius calc, pixel

			ballmarkinfo_t		*pbmi0;
			// #error ABCDEFG re-estimate ts64shot 
			ts64delta = (I64)(ts64 - ts64shot);
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "sm[%d]: %I64d\n", i, ts64delta);
			if (ts64delta < 0) {
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BEFORE SHOT!\n");
				continue;
			}

			if (setrindexshotbulk == 0) {
				rindexshotbulk = rindex;
				setrindexshotbulk = 1;
			}
			
            ts64deltad = ts64delta / TSSCALE_D;
			bcL.x = picp->maLx[2] * ts64deltad*ts64deltad + picp->maLx[1] * ts64deltad + picp->maLx[0];
			bcL.y = picp->maLy[2] * ts64deltad*ts64deltad + picp->maLy[1] * ts64deltad + picp->maLy[0];
			lr_ = picp->maLr[2] * ts64deltad*ts64deltad + picp->maLr[1] * ts64deltad + picp->maLr[0];

			{
				double ma[3];
				double td;

				//--
				td = ts64deltad;
				memcpy(&ma[0], &picp->maPx[0], sizeof(double) * 3);
				bcP.x = ma[2] * td*td + ma[1] * td + ma[0];
				memcpy(&ma[0], &picp->maPy[0], sizeof(double) * 3);
				bcP.y = ma[2] * td*td + ma[1] * td + ma[0];
				memcpy(&ma[0], &picp->maPr[0], sizeof(double) * 3);
				pr_ = ma[2] * td*td + ma[1] * td + ma[0];
			}

			L2; P2;

#define OFFSETXY	4
			startx = (I32)(bcP.x - widthsrb / 2) - offset_x - OFFSETXY;
			starty = (I32)(bcP.y - heightsrb / 2) - offset_y - OFFSETXY;

			if (startx < 0) startx = 0;
			if (starty < 0) starty = 0;

			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%d]P<%lf %lf : %lf>, L<%lf %lf %lf>  Start(%d %d)\n",camid, i,
			//		bcP.x, bcP.y, pr_, 
			//		bcL.x, bcL.y, lr_,
			//		startx, starty);

			memcpy(&bcPEst, &bcP, sizeof(point_t));
			pr_Est = pr_;

#define DIPLAY_CIRCLE_HERE
#if defined(DIPLAY_CIRCLE_HERE)
			if (piana->opmode == IANA_OPMODE_FILE) {
				I32 x, y, r;
				U08 *pimgbuf;

				x = (I32)(bcP.x - offset_x);
				y = (I32)(bcP.y - offset_y);

				r = (I32)pr_;
				pimgbuf = bufOrg;

				if (piana->himgfunc) {
					((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimgbuf, width, height, 0, 1, 2, 3, piana->imguserparam);
				}
			}
#endif

#define PR_MARGIN	10
			if((bcP.x < 0 || bcP.x >= FULLWIDTH - (pr_ * 1 + PR_MARGIN)) || 
                (bcP.y < 0 || bcP.y >= FULLHEIGHT - (pr_ * 1 + PR_MARGIN)) || 
                (startx < 0 || startx >= FULLWIDTH - (pr_ * 1 + PR_MARGIN)) || 
                (starty < 0 || starty >= FULLHEIGHT - (pr_ * 1 + PR_MARGIN)) ) {
				continue;
			}

			pbmi0 = &pic->bmi[i];
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d]P<%lf %lf : %lf>, L<%lf %lf %lf>  Start(%d %d)\n",i,
			//		bcP.x, bcP.y, pr_, 
			//		bcL.x, bcL.y, lr_,
			//		startx, starty);
		}

		{ 	// real image processing
			iana_ballcandidate_t bc[MAXCANDIDATE];
			U32 bcount;
			point_t P2; 	// for radius calc, pixel
			point_t L2; 	// for radius calc

			U32 ii;
			I32 index;
			U32 minth;
			U32 maxth;
			double cermax;
			double crmin, crmax;

#define THY0	0.20
#define THY1	0.30
#define THY2	0.30

#define MAXTH0	70 // 70, 70
#define MAXTH1	30 // 20, 60
#define MAXTH2	30 // 20, 40
#define MINTH	10 // 10, 30

			if (useEP) {
				iana_P2L_EP(piana, camid, 0.5, &bcP, &L2);
			}
			else {
				iana_P2L(piana, camid, &bcP, &L2, 0);
			}

			minth = MINTH;
			if (L2.y < THY0) {
				maxth = MAXTH0;
			}
			if (L2.y < THY1) {
				maxth = MAXTH1;
			}
			else {
				maxth = MAXTH2;
			}

#define REFINE_MULTV	3 // 1, 1
#define REFINE_EXPN		3 // 1, 2
#define CERMAX_MULT		0.5 // 1, 1.2, 1.5
			cermax = piana->pic[camid]->icp.CerBallshot;
			cermax *= CERMAX_MULT;
			{
				double crminratio;
				double crmaxratio;
				cr_rect_t rectRunP;

				rectRunP.left = offset_x;
				rectRunP.top = offset_y;
				rectRunP.right = offset_x + widthsrb;
				rectRunP.bottom = offset_y + heightsrb;

				iana_setRunRunBallsizeMinMax(piana, camid, &rectRunP, 1.0);

				if (piana->camsensor_category == CAMSENSOR_EYEXO) {
					crminratio = CRMINRATIO_EYEXO;
					crmaxratio = CRMAXRATIO_EYEXO;
				}
				else {
					crminratio = CRMINRATIO;
					crmaxratio = CRMAXRATIO;
				}
				crmin = (piana->pic[camid]->icp.RunBallsizePMin / 2.0) * crminratio;
				crmax = (piana->pic[camid]->icp.RunBallsizePMax / 2.0) * crmaxratio;
			}

#define SHOT_HALFWINSIZE	8
#define THSTDMULT_		3 // 0.7, 1

#define REFINE_MULTVA	1
#define REFINE_EXPNA	0 // 1

#define REFINE_MULTV_	3 // 1, 1
#define REFINE_EXPN_	3 // 1, 2

			bcount = 0;
			if (piana->camsensor_category != CAMSENSOR_EYEXO) {

				res = iana_getballcandidator(
					piana,
					camid,
					(U08 *)buf + startx + starty * width,
					widthsrb, heightsrb, width,
					&bc[0],
					&bcount,
					maxth, minth,
					1, 1, // 1, 0, 		// REFINE_MULTV_, REFINE_EXPN_, 
					cermax
				);

				//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%d]bcount: %d   Refined Ball.. .%10lf %10lf   (r:%10lf e:%10lf)\n", 
				//	camid, __LINE__, bcount, bc[0].P.x, bc[0].P.y,
				//	bc[0].cr, bc[0].cer
				//	);
			}

			if (bcount == 0) {
				U32 multv, expv;
				U32 shot_halfwinsize;
				double thstdmult;
				double cutmeanmult;

#define SHOT_HALFWINSIZE_Z3_1	10
#define THSTDMULT_Z3_0	1
#define THSTDMULT_Z3_1	0.5

				if (piana->camsensor_category == CAMSENSOR_EYEXO) {
					if (camid == 0) {
						shot_halfwinsize = SHOT_HALFWINSIZE;
						thstdmult = THSTDMULT_Z3_0;
						multv = 1;
						expv = 1;
					} else {
						shot_halfwinsize = SHOT_HALFWINSIZE_Z3_1;
						thstdmult = THSTDMULT_Z3_1;
						multv = 1;
						expv = 0;
					}

					cutmeanmult = 0.0005;
				} else {
					multv = 1; // 2
					expv = 0;
					if (camid == 0) {
						multv = 1;
						expv = 0;
					} else if (camid == 1) {
						multv = 1;
						expv = 0;
					}

					thstdmult = THSTDMULT_;
					shot_halfwinsize = SHOT_HALFWINSIZE;
					cutmeanmult = 0;
				}

				if (piana->camsensor_category == CAMSENSOR_EYEXO) {
					double maPrr[3];
					double td;
					double prr;

					//--
					td = ts64deltad;
					memcpy(&maPrr[0], &pic->maPrr[0], sizeof(double) * 3);

					prr = maPrr[2]*td*td + maPrr[1]*td + maPrr[0];

#define MINPRR		5.0
					if (prr > MINPRR) {
						double minr, maxr;

#define PRR2MINPRR	(0.9) // 0.98, 0.8
#define PRR2MAXPRR  (1.10) // 1.02, 1.2
						minr = prr * PRR2MINPRR;
						maxr = prr * PRR2MAXPRR;
						iana_getballcandidator_set_minr_maxr(minr, maxr);
					}

					res = iana_getballcandidator_cht (
						piana,
						camid,
						(U08 *)buf + startx + starty * width,
						widthsrb, heightsrb, width,
						&bc[0],
						&bcount,

						SHOT_HALFWINSIZE / 2,
						multv, expv,			// 1, 0, //REFINE_MULTVA, REFINE_EXPNA,
						thstdmult,
						cermax, 
                        cutmeanmult				// CUTMEAN zero..
						);

				} else {
					res = iana_getballcandidator_cht(
						piana,
						camid,
						(U08 *)buf + startx + starty * width,
						widthsrb, heightsrb, width,
						&bc[0],
						&bcount,

						SHOT_HALFWINSIZE / 2,
						multv, expv,			// 1, 0, //REFINE_MULTVA, REFINE_EXPNA,
						thstdmult,
						cermax, 
                        cutmeanmult				// CUTMEAN zero..
						);
				}
				//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] Refined Ball.. With Adaptive Threshold.%10lf %10lf\n", 
				//		camid, bc[0].P.x, bc[0].P.y
				//		);
			}
			
			if (piana->camsensor_category == CAMSENSOR_EYEXO) {
				// 
			}
			else {
				if (bcount == 0) {
					res = iana_getballcandidator(
						piana,
						camid,
						(U08 *)buf + startx + starty * width,
						widthsrb, heightsrb, width,
						&bc[0],
						&bcount,
						maxth, minth,
						REFINE_MULTV, REFINE_EXPN,
						cermax
					);
				}
			}

            if (bcount == 0) {
				if (piana->camsensor_category == CAMSENSOR_EYEXO) {
					pbmi[i].valid = 0;
					pbmi[i].ts64 = ts64;
				} else {
					pbmi[i].valid = 1;
					pbmi[i].ts64 = ts64;
				}
			}

#define FILE_SLEEP	1
#if defined(FILE_SLEEP)
			if (piana->opmode == IANA_OPMODE_FILE) {
				Sleep(FILE_SLEEP);
			}
#endif
			//				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "count: %d\n", bcount);
			index = -1;
			for (ii = 0; ii < bcount; ii++) {
				if (bc[ii].cexist == IANA_BALL_EXIST) {
					if (bc[ii].cer > cermax || bc[ii].cr < crmin || bc[ii].cr > crmax) {	// Filtering..
						bc[ii].cexist = IANA_BALL_EMPTY;
						continue;
					} else {
						double sx, sy;
						double dx, dy;
						double distStart;
						double radius_rate;
						
                        //--
						pr_ = bc[ii].cr;

#define RADIUS_RATE	1.0 // 1.05, 1.1, 1.2   // �̰��� �߿�.
#define RADIUS_RATE_CONSOLE1	1.0 // 1.2, 1.1
//#define RADIUS_RATE_CONSOLE1	RADIUS_RATE
						if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
							radius_rate = RADIUS_RATE_CONSOLE1;
						} else {
							radius_rate = RADIUS_RATE;
						}

						pr_ *= radius_rate;
						
						sx = piana->pic[piana->camidCheckReady]->icp.L.x;
						sy = piana->pic[piana->camidCheckReady]->icp.L.y;
						
                        //--- Raw Ball position.
						pmks->ballposP2Raw[i].x = pmks->ballposP2[i].x = bc[ii].P.x + startx + offset_x;
						pmks->ballposP2Raw[i].y = pmks->ballposP2[i].y = bc[ii].P.y + starty + offset_y;
						pmks->ballrP2Raw[i] = pmks->ballrP2[i] = pr_;

#define POSTESTVALUE	1 // 5, 2, 0.5
#if defined(POSTESTVALUE)
						
						pmks->ballposP2Raw[i].x = pmks->ballposP2[i].x = bc[ii].P.x + startx + offset_x + POSTESTVALUE;
						pmks->ballposP2Raw[i].y = pmks->ballposP2[i].y = bc[ii].P.y + starty + offset_y + POSTESTVALUE;
						pmks->ballrP2Raw[i] = pmks->ballrP2[i] = pr_;
#endif

						P2.x = pmks->ballposP2[i].x;
						P2.y = pmks->ballposP2[i].y;

						if (useEP) {
							iana_P2L_EP(piana, camid, 0.5, &P2, &L2);
						} else {
							iana_P2L(piana, camid, &P2, &L2, 0);
						}

						pmks->ballposL2Raw[i].x = pmks->ballposL2[i].x = L2.x;
						pmks->ballposL2Raw[i].y = pmks->ballposL2[i].y = L2.y;

						dx = L2.x - sx;
						dy = L2.y - sy;
						distStart = sqrt(dx*dx + dy * dy);

#define MINSTARTDIST_EYEXO	0.005
#define MINSTARTDIST		0.01 // 0.03
						if (piana->camsensor_category == CAMSENSOR_EYEXO) {
							if (distStart < MINSTARTDIST_EYEXO) {
								break;
							}
						} else {
							if (distStart < MINSTARTDIST) {
								break;
							}
						}

						{
							point_t L1, L2, P1, P2;
							double dx, dy, r_;

							P1.x = bc[ii].P.x + startx + offset_x;
							P1.y = bc[ii].P.y + starty + offset_y;

							if (useEP) {
								iana_P2L_EP(piana, camid, 0.5, &P1, &L1);
							} else {
								iana_P2L(piana, camid, &P1, &L1, 0);
							}

							P2.x = P1.x + pr_;
							P2.y = P1.y;

							if (useEP) {
								iana_P2L_EP(piana, camid, 0.5, &P2, &L2);
							} else {
								iana_P2L(piana, camid, &P2, &L2, 0);
							}

							dx = L1.x - L2.x;
							dy = L1.y - L2.y;

							r_ = sqrt(dx*dx + dy * dy);

							pmks->ballrL2Raw[i] = pmks->ballrL2[i] = r_;
						}


						{
							double cx, cy;
							double dx, dy;
							double dist;
							double toobigdist;

							cx = pmks->ballposL2Raw[i].x;
							cy = pmks->ballposL2Raw[i].y;

							dx = cx - bcL.x;
							dy = cy - bcL.y;
							dist = sqrt(dx*dx + dy * dy);

							//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "cx cy: %10lf %10lf   vs bcL x y : %10lf %10lf dist: %10lf\n",
							//	cx, cy, bcL.x, bcL.y, dist);
#define TOOBIGDIST				0.02
#define TOOBIGDIST_EYEXO		0.01
							if (piana->camsensor_category == CAMSENSOR_EYEXO) {
								toobigdist = TOOBIGDIST_EYEXO;
							} else {
								toobigdist = TOOBIGDIST;
							}

							if (dist > toobigdist) {
								cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Distance TOO BIG.. %lf > %lf\n", dist, toobigdist);
								bc[ii].cexist = IANA_BALL_EMPTY;
								continue;
							}
						}

						pmks->ballPos2valid[i] = 1;
						pmks->ballPosvalid[i] = 1;
						pbmi[i].valid = 1;
						
                        //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] %10lf %10lf\n", i, cx[i], cy[i]);
						index = (I32)i;
						goodball = 1;
						break;
					} 	// if (bc[ii].cer > cermax || bc[ii].cr < crmin || bc[ii].cr > crmax) 	// Filtering.
				}
			} 	// for (ii = 0; ii < bcount; ii++) 
			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n\n");
		} 	// real image processing

		if (piana->opmode == IANA_OPMODE_FILE) {
			if (goodball) {
				U08 *pimg;
				I32 x, y, r;

				pimg = (U08 *)malloc((width + 16)*(height + 16));
				memcpy(pimg, buf, width*height);
				x = (I32)pmks->ballposP2Raw[i].x - offset_x;
				y = (I32)pmks->ballposP2Raw[i].y - offset_y;
				r = (I32)pmks->ballrP2Raw[i];

				if (x + r < (I32)(width - 2) && y + r < (I32)(height - 2)) {
					mycircleGray(x, y, r, 0xFF, pimg, width);
					mycircleGray(x, y, r + 1, 0x00, pimg, width);
				}

				if (piana->himgfunc) {
					((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimg, width, height, 2 /*0*/, 1, 2, 3, piana->imguserparam);
				}
				free(pimg);
			}
		}

		// IMGCALLBACK for refineBallPos
		if (ts64delta >= 0) {
			if (goodball) {
				goodballcount++;
				noballcount = 0;
			} else {
				noballcount++;
			}

#define NOBALLCOUNTMAX	8
#define GOODBALLCOUNT	16 // 8, 32
#define GOODBALLCOUNT2	5
#define NOBALLCOUNTMAX2	4
			if (noballcount >= NOBALLCOUNTMAX) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] loop break #1 with noballcount: %d\n", camid, noballcount);
				break;					// No more..
			}

			if (goodballcount > GOODBALLCOUNT) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] loop break #2 with goodballcount: %d\n", camid, goodballcount);
				break;
			}

			if (goodballcount > GOODBALLCOUNT2) {
				if (noballcount >= NOBALLCOUNTMAX2) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] loop break #3 with noballcount: %d, goodballcount: %d\n", camid, noballcount, goodballcount);
					break;					// No more..
				}
			}
		}
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] loop bottom %d\n", camid, i);
	} 		// for (i = 0; i < MARKSEQUENCELEN; i++) 


	if (sumbufcount > 0) {
        cv::Mat cvBufDiv, cvBufDiv_8u;
        cvBufDiv = cvSumBuf / sumbufcount;
        cvBufDiv.convertTo(cvBufDiv_8u, CV_8UC1);
        memcpy(pic->prefimg, cvBufDiv_8u.data, sizeof(U08) * cvBufDiv_8u.cols * cvBufDiv_8u.rows);

		if (piana->himgfunc) {
			((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pic->prefimg, sumbufwidth, sumbufheight, 0, 1, 2, 3, piana->imguserparam);
		}

        //ippiDivC_16u_C1IRSfs((Ipp16u)sumbufcount, sumbuf16, sumbufwidth * 2, buf16roisize, 0);
        //ippiConvert_16u8u_C1R(sumbuf16, sumbufwidth * 2, pic->prefimg, sumbufwidth, buf16roisize);
        //if (piana->himgfunc) {
        //    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pic->prefimg, sumbufwidth, sumbufheight, 1, 1, 2, 3, piana->imguserparam);
        //}

		free(curbuf16);
		free(sumbuf16);
	}

	//----
    RefineBallPos_BulkRegressionRANSAC(piana, camid);

#define DISPLAY_REFINED_BALLPOS
#if defined(DISPLAY_REFINED_BALLPOS)
	if (piana->opmode == IANA_OPMODE_FILE) {
		U32		width;
		U32		height;
		U32		offset_x;
		U32		offset_y;
		U32 	rindex;
		I32 	startx, starty;
		I32 	x, y, r;

		for (i = 0; i < marksequencelen; i++) {
			res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, i, &pb, &bufOrg);

			if (res == 0) {
				continue;
			}

			pcii = &pb->cii;
			width = pcii->width;
			height = pcii->height;
			offset_x = pcii->offset_x;
			offset_y = pcii->offset_y;

			startx; starty; rindex;

			if (piana->himgfunc) {
				memcpy(buf, bufOrg, width*height);

				x = (I32)pmks->ballposP2Raw[i].x - offset_x;
				y = (I32)pmks->ballposP2Raw[i].y - offset_y;
				r = (I32)pmks->ballrP2Raw[i];
				mycircleGray(x, y, r, 0xFF, buf, width);

				x = (I32)pmks->ballposP[i].x - offset_x;
				y = (I32)pmks->ballposP[i].y - offset_y;
				r = (I32)pmks->ballrP[i];
				mycircleGray(x, y, r, 0x80, buf, width);

				{
					I32 startx, endx;
					res = scamif_imagebuf_zrot_barnoise_position(
						piana->hscamif, camid,
						NORMALBULK_BULK, i,
						&startx, &endx);

					if (res) {
						mylineGray(startx, 0, startx, 32, 0xFF, buf, width);
						mylineGray(endx, 0, endx, 32, 0xFF, buf, width);
					}
				}

				if (pbmi[i].valid) {
					((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, width, height, 0, 1, 2, 3, piana->imguserparam);
				}
				else {
					((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, width, height, 1, 1, 2, 3, piana->imguserparam);
				}
			}
			// DISPLAY_DISPLAY_2
			//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "GOOD GOOD  [%d] %d %d %d \n",
			//	i, 
			//	pbmi[i].valid,
			//	pmks->ballPosvalid[i],
			//	pmks->ballPos2valid[i]
			//	);
			{
				I64		ts64delta;
				ts64delta = pbmi[i].ts64 - ts64shot;
				if (ts64delta > 0) {
					pbmi[i].valid = 1;
				}
			}
		}
	}
#endif

	{	// check with pre-calc'ed data.
		double maPx[3];
		double maPy[3];
		double maPr[3];
		double maLr[3];

		point_t P0[MARKSEQUENCELEN], L0[MARKSEQUENCELEN];
		double  Pr[MARKSEQUENCELEN], Lr[MARKSEQUENCELEN];

		double td;
		double distsum, distmean;
		U32 count;

		//--
		memcpy(&maPx[0], &picp->maPx[0], sizeof(double) * 3);
		memcpy(&maPy[0], &picp->maPy[0], sizeof(double) * 3);
		memcpy(&maPr[0], &picp->maPr[0], sizeof(double) * 3);

		memcpy(&maLr[0], &picp->maLr[0], sizeof(double) * 3);

		distsum = 0;
		count = 0;
		for (i = 0; i < marksequencelen; i++) {
			double dx, dy, dist;
			I64 ts64i;

			//--
			ts64i = (I64)pbmi[i].ts64;
			if (ts64i == 0) {
				continue;
			}
			td = (ts64i - (I64)ts64shot) / TSSCALE_D;

			P0[i].x = maPx[2] * td*td + maPx[1] * td + maPx[0];
			P0[i].y = maPy[2] * td*td + maPy[1] * td + maPy[0];
			Pr[i] = maPr[2] * td*td + maPr[1] * td + maPr[0];
			Lr[i] = maLr[2] * td*td + maLr[1] * td + maLr[0];

			iana_P2L(piana, camid, &P0[i], &L0[i], 0);

			dx = pmks->ballposL[i].x - L0[i].x;
			dy = pmks->ballposL[i].y - L0[i].y;

			dist = sqrt(dx*dx + dy * dy);
			distsum = distsum + dist;
			count++;
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] %10lf dist: %10lf distsum: %10lf  distmean: %10lf\n",
				i, td,
				dist, distsum, distsum / count);
		}
		distmean = distsum / count;

#define MISTMEAN_MIN	0.02 // 0.5
		if (distmean > MISTMEAN_MIN) {
			for (i = 0; i < marksequencelen; i++) {
				pmks->ballposP[i].x = pmks->ballposP2[i].x = P0[i].x;
				pmks->ballposP[i].y = pmks->ballposP2[i].y = P0[i].y;
				pmks->ballposP[i].z = 0.0;
				pmks->ballrP[i] = pmks->ballrP2[i] = Pr[i];

				pmks->ballposL[i].x = pmks->ballposL2[i].x = L0[i].x;
				pmks->ballposL[i].y = pmks->ballposL2[i].y = L0[i].y;
				pmks->ballposL[i].z = 0.0;
				pmks->ballrL[i] = pmks->ballrL2[i] = Lr[i];
			}
		}
	}

	pic->rindexshotbulk = rindexshotbulk;
	res = 1;
	return res;
}



U32 RefineBallPos_BulkRegression(iana_t *piana, U32 camid)
{
	U32	i;
	U32 res;

	iana_cam_t			*pic;
	iana_cam_param_t 	*picp;
	marksequence_t		*pmks;
	ballmarkinfo_t		*pbmi;
	U64 				ts64shot;

	U32 	useEP;
	U32 marksequencelen;

	//---
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	}
	else {
		marksequencelen = MARKSEQUENCELEN;
	}

	pic = piana->pic[camid];
	picp = &pic->icp;
	pmks = &pic->mks;
	pbmi = &pic->bmi[0];
	ts64shot = pic->ts64shot;

	useEP = 0;

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "refine, camid: %d\n", camid);
	{ 		// Filtering center position x, y
		double 	xx[MARKSEQUENCELEN], yy[MARKSEQUENCELEN];
		double 	ii[MARKSEQUENCELEN];
		double	dd[MARKSEQUENCELEN];
		double	cmm[MARKSEQUENCELEN];
		I32 prevvalid = -1;
		I32 firstid;
		double mindist = 1e10;
		double r2;
		I32 count;
		point_t Pp, Lp;

		count = 0;
		for (i = 0; i < marksequencelen; i++) {				// TOO
			if (pmks->ballPos2valid[i] == 1) {

#define BALLPOS2VALID_COUNTCHECK	15
#define BALLPOS2VALID_LYCHECK		0.75
				Pp.x = pmks->ballposP2Raw[i].x;
				Pp.y = pmks->ballposP2Raw[i].y;

				if (useEP) {
					iana_P2L_EP(piana, camid, 0.5, &Pp, &Lp);
				} else {
					iana_P2L(piana, camid, &Pp, &Lp, 0);
				}

				if (count < BALLPOS2VALID_COUNTCHECK || Lp.y < BALLPOS2VALID_LYCHECK) {
					count++;
				} else {
					pmks->ballPos2valid[i] = 0;
					pmks->ballPosvalid[i] = 0;
				}
			}
		}

		firstid = -1;
		for (i = 0; i < marksequencelen; i++) {
			dd[i] = 1e100;
		}

		for (i = 0; i < marksequencelen; i++) {
			if (pmks->ballPos2valid[i] == 1) {
				double dx, dy;
				if (prevvalid == -1) {
					prevvalid = i;
					firstid = i;
					continue;
				}
				dx = pmks->ballposP2Raw[i].x - pmks->ballposP2Raw[prevvalid].x;
				dy = pmks->ballposP2Raw[i].y - pmks->ballposP2Raw[prevvalid].y;

				dd[i] = sqrt(dx*dx + dy * dy);
				prevvalid = i;
				if (mindist > dd[i]) {
					mindist = dd[i];
				}
			}
		}

		if (firstid >= 0 && mindist < 1e10) {
			double xma[3];
			double yma[3];
			double rma[3];
			double ixma[3];
			double iyma[3];

			double mmm;
			U32    immm;
			U32    immmsum;
			U32 index;
			U32 success;
			I32 regres;

			//---
			success = 1;

			for (i = 0; i < marksequencelen; i++) {
				cmm[i] = (double)((int)i - (int)firstid);
			}

			index = 0;
			immmsum = 0;
			xma[2] = xma[1] = xma[0] = 0;
			yma[2] = yma[1] = yma[0] = 0;
			rma[2] = rma[1] = rma[0] = 0;
			for (i = firstid; i < marksequencelen; i++) {
				if (pmks->ballPos2valid[i] == 1) {
					index++;
					if (i == (U32)firstid) {
						mmm = 0;
					} else {
						mmm = dd[i] / mindist;
						//						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] mmm: %lf\n", i, mmm);
					}
					immm = (U32)(mmm + 0.20);
					immmsum += immm;
					cmm[i] = immmsum;
				}
			}

#define MININDEXCOUNT	4
			if (index < MININDEXCOUNT) {			// FAIL.
				success = 0;
			}

			if (success) { // get estimated X parameter
				//--- ESTIMATED X-Y.. :P
				index = 0;
				for (i = 0; i < marksequencelen; i++) {
					if (pmks->ballPos2valid[i] == 1) {
						xx[index] = (double)pmks->ballposP2[i].x;
						yy[index] = (double)pmks->ballposP2[i].y;
						ii[index] = (double)i;
						index++;
					}
				}
				regres = caw_quadraticregression(ii, xx, index, &ixma[2], &ixma[1], &ixma[0], &r2);
				
                if (regres == 0) {
					regres = cr_regression2(ii, xx, index, &ixma[1], &ixma[0], &r2);
					ixma[2] = 0;
				}
				
                regres = caw_quadraticregression(ii, yy, index, &iyma[2], &iyma[1], &iyma[0], &r2);
				if (regres == 0) {
					regres = cr_regression2(ii, yy, index, &iyma[1], &iyma[0], &r2);
					iyma[2] = 0;
				}

				index = 0;
				for (i = 0; i < marksequencelen; i++) {
					if (pmks->ballPos2valid[i] == 1) {
						xx[index] = (double)cmm[i];
						yy[index] = (double)pmks->ballposP2[i].x;
						ii[index] = (double)i;
						index++;
					}
				}

				//--- ESTIMATED X .. :P
#define MINR2		0.9
				regres = caw_quadraticregression(xx, yy, index, &xma[2], &xma[1], &xma[0], &r2);
				if (regres == 0) {
					regres = cr_regression2(xx, yy, index, &xma[1], &xma[0], &r2);
					xma[2] = 0;
				}

				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
				//		"index: %d, xma2_: %lf xma1_: %lf xma2_: %lf r2: %lf\n", 
				//		index, xma[2], xma[1], xma[0], r2);
				
                if (regres == 1) {
					if (r2 < MINR2) {				// TODO: Introduce Other measure. r2 is too sensitive... :P
						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "index: %d, xma2_: %lf xma1_: %lf xma2_: %lf r2: %lf\n", index, xma[2], xma[1], xma[0], r2);
						xma[2] = 0;
						regres = cr_regression2(xx, yy, index, &xma[1], &xma[0], &r2);
						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "--->  xma2_: %lf xma1_: %lf xma2_: %lf r2: %lf\n", xma[2], xma[1], xma[0], r2);
						if (r2 < MINR2) {
							success = 0;
						}
					}
				} else {
					success = 0;
				}
			}

			if (success) { // get estimated Y parameter
				index = 0;
				for (i = 0; i < marksequencelen; i++) {
					if (pmks->ballPos2valid[i] == 1) {
						xx[index] = (double)cmm[i];
						yy[index] = (double)pmks->ballposP2[i].y;
						index++;
					}
				}

#define MINR2		0.9
				regres = caw_quadraticregression(xx, yy, index, &yma[2], &yma[1], &yma[0], &r2);
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
				//		"index: %d, yma2_: %lf yma1_: %lf yma2_: %lf r2: %lf\n", 
				//		index, yma[2], yma[1], yma[0], r2);

				if (regres == 0) {
					regres = cr_regression2(xx, yy, index, &yma[1], &yma[0], &r2);
					yma[2] = 0;
				}
				if (regres == 1) {
					if (r2 < MINR2) {
						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "index: %d, yma2_: %lf yma1_: %lf yma2_: %lf r2: %lf\n", index, yma[2], yma[1], yma[0], r2);
						yma[2] = 0;
						regres = cr_regression2(xx, yy, index, &yma[1], &yma[0], &r2);
						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "--->  yma2_: %lf yma1_: %lf yma2_: %lf r2: %lf\n", yma[2], yma[1], yma[0], r2);
					}
				} else {
					success = 0;
				}
			}


			if (success) { // get estimated r parameter
				double 	rr[MARKSEQUENCELEN];
				double minr, maxr;
				U32 minrI, maxrI;
				U32 valid[MARKSEQUENCELEN];

				//--
				index = 0;
				minr = 99999; minrI = marksequencelen;
				maxr = 0.0;   maxrI = marksequencelen;
				for (i = 0; i < marksequencelen; i++) {
					if (pmks->ballPos2valid[i] == 1) {
						valid[i] = 1;
						rr[i] = pmks->ballrP2[i];
				
                        if (minr > rr[i]) {
							minr = rr[i];
							minrI = i;
						}
						
                        if (maxr < rr[i]) {
							maxr = rr[i];
							maxrI = i;
						}
						index++;
					} else {
						valid[i] = 0;
					}
				}

				if (index >= MININDEXCOUNT + 2) {			// Discard maximum and minimum..
					valid[minrI] = 0;
					valid[maxrI] = 0;
				}

				index = 0;
				for (i = 0; i < marksequencelen; i++) {
					if (valid[i] == 1) {
						xx[index] = (double)cmm[i];
						yy[index] = (double)rr[i];
						index++;

#define RRCOUNTGOOD		MARKSEQUENCELEN
						if (index >= RRCOUNTGOOD) {
							break;
						}
					}
				}

                regres = caw_quadraticregression(xx, yy, index, &rma[2], &rma[1], &rma[0], &r2);
				if (regres == 0) {
					regres = cr_regression2(xx, yy, index, &rma[1], &rma[0], &r2);
					rma[2] = 0;
				}

				if (regres == 1) {
					success = 1;
				} else {
					success = 0;
				}
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, 
				//		"index: %d, rma2_: %lf rma1_: %lf rma2_: %lf r2: %lf\n", 
				//		index, rma[2], rma[1], rma[0], r2);
			}

			for (i = 0; i < marksequencelen; i++) {
				double x, y, r;
				point_t P0, L0;
				point_t Pp, Lp;
				double dx, dy;
				if (i < (U32)firstid) {
					x = ixma[2] * i*i + ixma[1] * i + ixma[0];
					y = iyma[2] * i*i + iyma[1] * i + iyma[0];

					pmks->ballposP2[i].x = x;
					pmks->ballposP2[i].y = y;

					Pp.x = x;
					Pp.y = y;
					if (useEP) {
						iana_P2L_EP(piana, camid, 0.5, &Pp, &Lp);
					} else {
						iana_P2L(piana, camid, &Pp, &Lp, 0);
					}
					pmks->ballposL2[i].x = Lp.x;
					pmks->ballposL2[i].y = Lp.y;
				} else {
					//--
					if (success) { // get estimated X, Y and r
						x = xma[2] * cmm[i] * cmm[i] + xma[1] * cmm[i] + xma[0];
						y = yma[2] * cmm[i] * cmm[i] + yma[1] * cmm[i] + yma[0];
						r = rma[2] * cmm[i] * cmm[i] + rma[1] * cmm[i] + rma[0];

						if (r < 5) {
							success = 0;
						} else {
							pmks->ballposP2[i].x = x;
							pmks->ballposP2[i].y = y;
							pmks->ballrP2[i] = r;

							Pp.x = pmks->ballposP2[i].x;
							Pp.y = pmks->ballposP2[i].y;
							if (useEP) {
								iana_P2L_EP(piana, camid, 0.5, &Pp, &Lp);
							} else {
								iana_P2L(piana, camid, &Pp, &Lp, 0);
							}
							pmks->ballposL2[i].x = Lp.x;
							pmks->ballposL2[i].y = Lp.y;

							//-- Radius calc.
							P0.x = Pp.x + pmks->ballrP2[i];
							P0.y = Pp.y;
							if (useEP) {
								iana_P2L_EP(piana, camid, 0.5, &P0, &L0);
							} else {
								iana_P2L(piana, camid, &P0, &L0, 0);
							}

							dx = L0.x - Lp.x;
							dy = L0.y - Lp.y;
							pmks->ballrL2[i] = sqrt(dx*dx + dy * dy);
						}
					} 
				}
			}
		} else {
			for (i = 0; i < marksequencelen; i++) {
				pbmi[i].valid = 0;
			}
		}
	}

	for (i = 0; i < marksequencelen; i++) {
		pmks->ballposP[i].x = pmks->ballposP2[i].x;
		pmks->ballposP[i].y = pmks->ballposP2[i].y;
		pmks->ballposP[i].z = 0.0;
		pmks->ballposL[i].x = pmks->ballposL2[i].x;
		pmks->ballposL[i].y = pmks->ballposL2[i].y;
		pmks->ballposL[i].z = 0.0;


		pmks->ballrP[i] = pmks->ballrP2[i];
		pmks->ballrL[i] = pmks->ballrL2[i];
	}

	res = 1;

	return  res;
}


U32 RefineBallPos_BulkRegressionRANSAC(iana_t *piana, U32 camid)
{
	U32	i;
	U32 res;

	iana_cam_t			*pic;
	iana_cam_param_t 	*picp;
	marksequence_t		*pmks;
	ballmarkinfo_t		*pbmi;
	U64 				ts64shot;
	I32 index;

#define XY_RANSAC_MAXCOUNT	128
	double 	xx[XY_RANSAC_MAXCOUNT], yy[XY_RANSAC_MAXCOUNT];
	double 	rr[XY_RANSAC_MAXCOUNT];
	double 	ii[XY_RANSAC_MAXCOUNT];
	double coeffX[3], coeffY[3], coeffR[3];
	U32 marksequencelen;

	//---
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}

	pic = piana->pic[camid];
	picp = &pic->icp;
	pmks = &pic->mks;
	pbmi = &pic->bmi[0];
	ts64shot = pic->ts64shot;

	//---
	index = 0;
	for (i = 0; i < marksequencelen; i++) {
		if (pmks->ballPos2valid[i]) {
			xx[index] = (double)pmks->ballposP2Raw[i].x;
			yy[index] = (double)pmks->ballposP2Raw[i].y;
			rr[index] = (double)pmks->ballrP2Raw[i];

			ii[index] = (double)i;
			index++;
		}
	}

	// RANSAC.. for X
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
        RegressionXY_RANSAC(ii, xx, index, &coeffX[0], 0);
        RegressionXY_RANSAC(ii, yy, index, &coeffY[0], 0);
	} else {
        RegressionXY_RANSAC(ii, xx, index, &coeffX[0], 1);
        RegressionXY_RANSAC(ii, yy, index, &coeffY[0], 1);
	}
    RegressionXY_RANSAC(ii, rr, index, &coeffR[0], 0);

	for (i = 0; i < marksequencelen; i++) {
		point_t P0, L0;
		point_t Pp, Lp;
		double dx, dy;
		double rl;
		double x, y, r;

		//--
		x = coeffX[2] * i*i + coeffX[1] * i + coeffX[0];
		y = coeffY[2] * i*i + coeffY[1] * i + coeffY[0];
		r = coeffR[2] * i*i + coeffR[1] * i + coeffR[0];


		pmks->ballposP[i].x = pmks->ballposP2[i].x = x;
		pmks->ballposP[i].y = pmks->ballposP2[i].y = y;
		pmks->ballposP[i].z = 0.0;
		pmks->ballrP[i] = pmks->ballrP2[i] = r;

		//-
		Pp.x = x; Pp.y = y;
		iana_P2L(piana, camid, &Pp, &Lp, 0);


		pmks->ballposL[i].x = pmks->ballposL2[i].x = Lp.x;
		pmks->ballposL[i].y = pmks->ballposL2[i].y = Lp.y;
		pmks->ballposL[i].z = 0.0;



		P0.x = Pp.x + r;
		P0.y = Pp.y;
		iana_P2L(piana, camid, &P0, &L0, 0);

		dx = L0.x - Lp.x;
		dy = L0.y - Lp.y;
		rl = sqrt(dx*dx + dy * dy);

		pmks->ballrL[i] = pmks->ballrL2[i] = rl;

		pmks->ballPos2valid[i] = 1;
		pmks->ballPosvalid[i] = 1;
	}

	res = 1;

	return res;
}


U32	RegressionXY_RANSAC(double xarray[], double yarray[], U32 count, double coeff[3], U32 usequad)
{
	U32 res;
	U32 i, j;
	U32 n;
	U32 stepvalue;
#define XY_RANSAC_MAXCOUNT	128
	double 	xx[XY_RANSAC_MAXCOUNT], yy[XY_RANSAC_MAXCOUNT];
	double r2;

	double dx, dy;
	double m, b;

	I32 scorecount;
	I32 scMAX;
	I32 scMAXIndex;
	double scMAXm, scMAXb;

	I32 index;


	//--
	res = 0;
#define XY_RANSAC_COUNT	4
	coeff[0] = coeff[1] = coeff[2] = 0;
	if (count < XY_RANSAC_COUNT) {
		if (count < 2) {
			res = 0;
		} else {
			for (i = 0; i < count; i++) {
				xx[i] = xarray[i];
				yy[i] = yarray[i];

				cr_regression2(xx, yy, count, &coeff[1], &coeff[0], &r2);
				res = 1;
			}
		}
		goto func_exit;
	}

	stepvalue = count / 3;

	scMAX = 0;
	scMAXIndex = -999;

	scMAXm = 0;
	scMAXb = 0;
	for (i = 0; i < count; i++) {
		double y_;
		double evalue;

		j = i + stepvalue;
		j = j % count;

		dx = xarray[i] - xarray[j];
		dy = yarray[i] - yarray[j];

#define XY_RANSAC_DOUBLEMIN	(1e-6)
		if (dy > -XY_RANSAC_DOUBLEMIN && dy < XY_RANSAC_DOUBLEMIN) {
			continue;
		}

		// get candidate parameter
		m = dy / dx;
		b = yarray[i] - m * xarray[i];

		// measure score..
		scorecount = 0;
		for (n = 0; n < count; n++) {
			y_ = m * xarray[n] + b;

			evalue = y_ - yarray[n];
			if (evalue < 0) {
				evalue = -evalue;
			}
#define XY_RANSAC_EVALUE_MAX 2
			if (evalue < XY_RANSAC_EVALUE_MAX) {
				scorecount++;
			}
		}

		if (scMAX < scorecount) {
			scMAX = scorecount;
			scMAXIndex = i;
			scMAXm = m;
			scMAXb = b;
		}
	}
#define XY_RANSAC_SCORE_MIN		5
	if (scMAX >= XY_RANSAC_SCORE_MIN && usequad) {
		double y_;
		double evalue;

		index = 0;
		for (i = 0; i < count; i++) {
			y_ = scMAXm * xarray[i] + scMAXb;

			evalue = y_ - yarray[i];
			if (evalue < 0) {
				evalue = -evalue;
			}

			if (evalue < XY_RANSAC_EVALUE_MAX) {
				xx[index] = xarray[i];
				yy[index] = yarray[i];
				index++;
			}
		}

		caw_quadraticregression(xx, yy, index, &coeff[2], &coeff[1], &coeff[0], &r2);
	} else {
		for (i = 0; i < count; i++) {
			xx[i] = xarray[i];
			yy[i] = yarray[i];
		}
		cr_regression2(xx, yy, count, &coeff[1], &coeff[0], &r2);
	}

	res = 1;

func_exit:
	return res;
}


/*!
********************************************************************************
*	@brief      Mark Sequence processing
*
*  @param[in]	piana
*              IANA module handle
*  @param[in]	camid
*              camera id
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/0917
*******************************************************************************/
static int s_useProjPl_flag = 1;
U32 apparentBallRadius(iana_t *piana, U32 camid)
{
	U32 res;

	iana_cam_t	*pic;
	iana_cam_param_t *picp;
	camimageinfo_t     *pcii;
	camif_buffer_info_t *pb;
	marksequence_t	*pmks;

	U64 	ts64;
	U64		ts64pb;
	cr_point_t PbPrev;

	double lr_;
	double pr_;

	U32		width;
	U32		height;
	U32		offset_x;
	U32		offset_y;
	U08 	*buf;
	U32 	rindex;

	U32 i, j;

	cr_point_t Pb0;	// Ball center, Local
	cr_point_t Pb;	// Ball center, 3D
	cr_vector_t uw;	// Sight from CAM to ball. unit-vector
	cr_line_t   lw;	// line with uw and P_{b,0} : ball position, local coord.
	cr_point_t campos;	// Camera position

	U32    useProjPl;		// Use Projection Plane

	U32 marksequencelen;

	//-----
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}

	pic = piana->pic[camid];
	picp = &pic->icp;
	pmks = &pic->mks;

	pic->ballcount = 0;

	if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
		useProjPl = 1;
	} else {
		useProjPl = 0;
	}

	if (s_useProjPl_flag == 0) {
		useProjPl = 0;
	}

	{
#define RAWMULT	5 // 1, 10
		double 	Lxx[MARKSEQUENCELEN*RAWMULT], Lyy[MARKSEQUENCELEN*RAWMULT], Lzz[MARKSEQUENCELEN*RAWMULT];
		double	tt[MARKSEQUENCELEN*RAWMULT];

		point_t bcL1;	// Ball center, local
		point_t bcP1; 	// Ball center, pixel
		point_t bcL2;	// Ball center, local
		point_t bcP2; 	// Ball center, pixel
		ballmarkinfo_t		*pbmi;

		U32 count;
		U32 israw;
		U32 israw_checked;

		//--
		pbmi = &pic->bmi[0];

		pic->ballcount = 0;

		res = 0;
		buf = NULL;
		pb = NULL;
		PbPrev.x = 0; PbPrev.y = 0; PbPrev.z = 0;
		ts64pb = 0;

		israw = 0;
		israw_checked = 0;
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]  PosL   PosL2\n", camid);

		for (i = 0; i < marksequencelen; i++) {
			pmks->cam2ball[i] = 0;
			pmks->cam2ground[i] = 0;
			pmks->ballrL3Raw[i] = 0;
			pmks->ballrL3[i] = 0;
			pmks->ballrP3Raw[i] = 0;
			pmks->ballrP3[i] = 0;
		}
		count = 0;

		for (i = 0; i < marksequencelen; i++) {
			if (pbmi[i].valid == 0) {
				//	continue;
			}
			{
				double dx, dy, dxy;
				double lx, ly;

				//--
				dx = pmks->ballposL2Raw[i].x - pmks->ballposL2[i].x;
				dy = pmks->ballposL2Raw[i].y - pmks->ballposL2[i].y;

				dxy = sqrt(dx*dx + dy * dy);

#define RAW_L_DIFF	(0.005) // 0.02, 0.0005, 0.001, 0.00005
				if (dxy > RAW_L_DIFF /* *0 */) {
					lx = pmks->ballposL2[i].x;
					ly = pmks->ballposL2[i].y;
				} else {
					lx = pmks->ballposL2Raw[i].x;
					ly = pmks->ballposL2Raw[i].y;
				}

				bcL2.x = lx;
				bcL2.y = ly;
				bcP2.x = pmks->ballposP2[i].x;
				bcP2.y = pmks->ballposP2[i].y;

				dx = pmks->ballposL2Raw[i].x - pmks->ballposL[i].x;
				dy = pmks->ballposL2Raw[i].y - pmks->ballposL[i].y;

				dxy = sqrt(dx*dx + dy * dy);
				if (dxy > RAW_L_DIFF /* *0 */) {
					lx = pmks->ballposL[i].x;
					ly = pmks->ballposL[i].y;
					israw = 0;
				} else {
					lx = pmks->ballposL2Raw[i].x;
					ly = pmks->ballposL2Raw[i].y;
					israw = 1;
					israw_checked = 1;
				}

				bcL1.x = lx;
				bcL1.y = ly;
				bcP1.x = pmks->ballposP[i].x;
				bcP1.y = pmks->ballposP[i].y;
			}

			rindex = pic->firstbulkindex + i;

#define BALLSTARTY_MARGIN	0.01
			if (piana->camsensor_category != CAMSENSOR_EYEXO) {
				if (bcL2.y < pic->icp.L.y + BALLSTARTY_MARGIN) {
					continue;
				}
			}

			// 1) get image buffer
			if (piana->opmode == IANA_OPMODE_FILE) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, rindex, &pb, &buf);
				if (res == 0) {
					break;
				}
			}
			else {
				//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bulk, img read\n", i);
				for (j = 0; j < 20; j++) {
					res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, rindex, &pb, &buf);
					if (res == 1) {
						break;
					}

					Sleep(1);
				}
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read [%d:%d]\n", i, j);
				if (res == 0) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", i, j);
					break;
				}
			}

			pcii = &pb->cii;
			width = pcii->width;
			height = pcii->height;
			offset_x = pcii->offset_x;
			offset_y = pcii->offset_y;


			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, width, height, 1, 1, 2, 3, piana->imguserparam);
			}

			ts64 = MAKEU64(pb->ts_h, pb->ts_l);

			if (ts64 < piana->pic[camid]->ts64shot) {		// 20200305
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%d before shot... \n", i);
				continue;
			}
			
            //--------------------- 
			// 2) Ball center, radius. Brightness equalization
			{
				//-----------------------------------------------------
				// NEWMARKPROCESSING [1]: Sight Vector from CAM to Ball
				//-----------------------------------------------------
				{
					iana_cam_t *pic;

					Pb0.x = bcL1.x;
					Pb0.y = bcL1.y;
					Pb0.z = 0.0;

					// Cam position
					pic = piana->pic[camid];
					memcpy(&campos, &pic->icp.CamPosL, sizeof(cr_point_t));
					// v3 = v1 - v2
					cr_vector_sub(&Pb0, &campos, &uw);		// uw = Pb0 - campos
					cr_vector_normalization(&uw, &uw);
					cr_line_p0m(&campos, &uw, &lw);
				}

				//-----------------------------------------------------
				// NEWMARKPROCESSING [2]: Ball 3D position
				//-----------------------------------------------------
				{
					// Intersection point between Lw and Ball-Traj Line
					double dx, dy, dz;
					cr_point_t PbTrj;

					cr_point_line_line(
						&lw, 				// Cam-ball sight line.
						&piana->ShotLine,	// Ball Traj line.
						&Pb,				// Intersection point on the Cam-ball sight line.
						&PbTrj);			// Intersection point on the Ball Traj. line.

					{
						Lxx[count] = PbTrj.x;
						Lyy[count] = PbTrj.y;
						Lzz[count] = PbTrj.z;
						tt[count] = (ts64 - piana->pic[camid]->ts64shot) / TSSCALE_D;
						count++;
					}

					memcpy(&pmks->ballposL3D[i], &Pb, sizeof(cr_point_t));

					if (pic->ballcount != 0) {
						double ddist;
						double ddist0;
						double dts;
						double vvv;
						double vvv0;

						dx = (PbTrj.x - PbPrev.x);
						dy = (PbTrj.y - PbPrev.y);
						dz = (PbTrj.z - PbPrev.z);

						ddist = cr_vector_distance(&PbPrev, &PbTrj);

						ddist0 = sqrt(dx*dx + dy * dy);
						dts = (ts64 - ts64pb) / TSSCALE_D;
						{
							U32 framerate;
							U32 syncdiv;

							//--
							pcii = &pb->cii;
							framerate = pcii->framerate;
							syncdiv = pcii->syncdiv;

							if (syncdiv == 0) {
								syncdiv = 1;
							}
							dts = 1.0 / (framerate / syncdiv);
						}

						vvv = ddist / dts;
						vvv0 = ddist0 / dts;
						pic->vmag2[pic->ballcount - 1] = vvv;
						pic->TrajY[pic->ballcount - 1] = PbTrj.y;
						
                        {
							double dxy;
							double incline;
							double azimuth;

							dxy = sqrt(dx*dx + dy * dy);
							incline = RADIAN2DEGREE(atan2(dz, dxy));
							azimuth = RADIAN2DEGREE(atan2(dx, dy));
							cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "incline: %lf,  azimuth: %lf\n", incline, azimuth);
						}

					}
					PbPrev.x = PbTrj.x;
					PbPrev.y = PbTrj.y;
					PbPrev.z = PbTrj.z;
					ts64pb = ts64;
					pic->ballcount++;
				}

				//-----------------------------------------------------
				// NEWMARKPROCESSING [3]: Apparent radius of Ball.
				//-----------------------------------------------------
				{
					double l1, l2;
					double r2;
					cr_point_t vtmp;

					cr_vector_sub(&Pb, &campos, &vtmp);
					l1 = cr_vector_norm(&vtmp);				// distance between cam and Ball 3D point

					cr_vector_sub(&Pb0, &campos, &vtmp);
					l2 = cr_vector_norm(&vtmp);				// distance between cam and Ball local point

					r2 = BALLRADIUS * (l2 / l1);
					pmks->cam2ball[i] = l1;
					pmks->cam2ground[i] = l2;

					{
						lr_ = pmks->ballrL2[i];
						pr_ = pmks->ballrP2[i];
						pmks->ballrL3Raw[i] = r2;
						{
							point_t L1, L2; 	// for radius calc
							point_t P1, P2; 	// for radius calc, pixel
							double dx, dy, dlen;
							
							{
								double dx, dy, dxy;
								double lx, ly;

								//--
								dx = pmks->ballposL2Raw[i].x - pmks->ballposL[i].x;
								dy = pmks->ballposL2Raw[i].y - pmks->ballposL[i].y;

								dxy = sqrt(dx*dx + dy * dy);
								if (dxy > RAW_L_DIFF) {
									lx = pmks->ballposL[i].x;
									ly = pmks->ballposL[i].y;
								} else {
									lx = pmks->ballposL2Raw[i].x;
									ly = pmks->ballposL2Raw[i].y;
								}
								L1.x = lx;
								L1.y = ly;
							}

							L2.x = L1.x;
							L2.y = L1.y + r2;

							iana_L2P(piana, camid, &L1, &P1, 0);
							iana_L2P(piana, camid, &L2, &P2, 0);
							dx = P1.x - P2.x;
							dy = P1.y - P2.y;
							dlen = sqrt(dx*dx + dy * dy);

							//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " dx dy dlen: %lf %lf %lf\n", dx, dy, dlen);

							pmks->ballrP3Raw[i] = dlen;

							if (useProjPl) {
								dlen = calcimageradius(piana, camid, i);
								pmks->ballrP3Raw[i] = dlen;
							}
						}
					}
				}
			} 			// 2) Ball center, radius. Brightness equalization
		} 	// for (i = 0; i < MARKSEQUENCELEN; i++) 


		if (count > 10) {
			double td, td_1;
			double x_, y_, z_;
			double x_1, y_1, z_1;
			double maLx[3], maLy[3], maLz[3];
			double r2;
			U32 ii;
			I32 first;

			double vvv;
			double incline, azimuth;

            //--
			r2;
			first = 1;

			{
				iana_shotresult_t *psr;
				psr = &piana->shotresultdata;
				azimuth = psr->azimuth;
				incline = psr->incline;
				vvv = psr->vmag;
			}

#define TD_START 0
#define TD_LAST	0.005
#define TD_STEP	0.0005
			ii;
			td = TD_START;
			x_1 = maLx[2] * td*td + maLx[1] * td + maLx[0];
			y_1 = maLy[2] * td*td + maLy[1] * td + maLy[0];
			z_1 = maLz[2] * td*td + maLz[1] * td + maLz[0];
			td_1 = td;

			for (td = TD_STEP; td < 0.005; td += TD_STEP) {
				double dx, dy, dz, dxy, dxyz;
				double vmag;

				x_ = maLx[2] * td*td + maLx[1] * td + maLx[0];
				y_ = maLy[2] * td*td + maLy[1] * td + maLy[0];
				z_ = maLz[2] * td*td + maLz[1] * td + maLz[0];

				dx = x_ - x_1;
				dy = y_ - y_1;
				dz = z_ - z_1;
				dxy = sqrt(dx*dx + dy * dy);
				dxyz = sqrt(dx*dx + dy * dy + dz * dz);
				vmag = dxyz / (td - td_1);

				x_1 = x_;
				y_1 = y_;
				z_1 = z_;
				td_1 = td;
				vvv = vmag;

				azimuth = RADIAN2DEGREE(atan2(dx, dy));
				incline = RADIAN2DEGREE(atan2(dz, dxy)); 		// Theta = tan-1( h / d)

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%10lf> XYZ_ (%10lf %10lf %10lf) dXYZ_ (%10lf %10lf %10lf at %10lf)  vmag: %10lf (dxyz: %10lf) incline: %lf  azimuth: %lf\n",
					td,
					x_, y_, z_,
					dx, dy, dz, (td - td_1),
					vmag, dxyz,
					incline, azimuth
				);


			}

			{
				U32 iii;
				iana_cam_t *pic;
				iana_shotresult_t *psr;

				pic = piana->pic[camid];

				for (iii = 0; iii < pic->ballcount; iii++) {
					pic->vmag2[iii] = vvv;
				}
				psr = &piana->shotresultdata;
				psr->azimuth = azimuth;
				psr->incline = incline;
			}
		}
	}

	res = 1;

	return res;
}


U32 ballViewPlane(iana_t *piana, U32 camid, U32 seqnum)
{
	U32 res;
	U32 useProjPl;
	cr_point_t Pb;
	cr_point_t campos;	// Camera position

	ballmarkinfo_t	*pbmi;
	iana_cam_t			*pic;
	marksequence_t		*pmks;

	//---
	useProjPl = 0;

	pic = piana->pic[camid];
	pbmi = &pic->bmi[seqnum];
	pmks = &pic->mks;

	if (useProjPl) {
		point_t PbP;
		memcpy(&PbP, &pmks->ballposP[seqnum], sizeof(point_t));
		iana_P2L3D_EP(piana, camid,
			pmks->cam2ball[seqnum],			// Camera to Ball distance.
			&PbP, &Pb);
	} else {
		memcpy(&Pb, &pmks->ballposL3D[seqnum], sizeof(cr_point_t));
	}

	/*
		for example, Pb from ballposL3D:      (-0.148884085, 0.100919003, 0.010320747)
		Pb from iana_P2L3D_EP(): (-0.150469869, 0.101329841, 0.009579867)
Distance: 0.001798   (under 2mm)
*/

	memcpy(&campos, &pic->icp.CamPosL, sizeof(cr_point_t));

	// Calc View rotation angle (Z-axis to Ball_to_Cam_Sight)
	calcRodriguesMatrix(
		&campos,
		&Pb,
		pbmi->rrm,
		pbmi->rrma,
		&pbmi->rv,
		&pbmi->rt);

	{	// BALL PLANE.
		cr_vector_t ua, ub, uc;
		cr_vector_t vtmp;
		double nv;

		// Get Ball to Camera vector
		cr_vector_sub(&campos, &Pb, &vtmp); 	// v1 - v2. ball to Cam Vector
		nv = cr_vector_normalization(&vtmp, &ua);	// Ball to camera vector.. and normalize
		//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "nv of ua: %lf\n", nv);

		// Rod.
		memcpy(&ub, &pbmi->rv, sizeof(cr_vector_t));	// 

		// ...
		//cr_vector_cross(&ua, &pbmi[i].ub, &vtmp);
		cr_vector_cross(&ua, &ub, &vtmp);
		nv = cr_vector_normalization(&vtmp, &uc);	//
		//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "nv of uc: %lf\n", nv);

		memcpy(&pbmi->ua, &ua, sizeof(cr_vector_t));
		memcpy(&pbmi->ub, &ub, sizeof(cr_vector_t));
		memcpy(&pbmi->uc, &uc, sizeof(cr_vector_t));

		cr_plane_p0n(&Pb, &ua, &pbmi->BallPlane);	// Ball Plane
	}

	res = 1;

	return res;
}



/*!
********************************************************************************
*	@brief      Refined Shot
*
*  @param[in]	piana
*              IANA module handle
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/0917
*******************************************************************************/
U32 RefineShot_ShotPlaneProjection(iana_t *piana)
{
	U32 res;
	U32 camid;

	res = 1;
	for (camid = 0; camid < 2; camid++) {
		res = RegressionShotLocalLine(piana, camid);
		if (res == 0) break;
	}

	if (res == 0) {
		goto func_exit;
	}

	{
		iana_shot_plane(piana, 0);
		iana_shot_plane(piana, 1);

		{
			U32 res;
			double angle;

			res = iana_shotplane_angle(piana, &angle);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"{%d} Plane angle, res: %d, angle: %lf\n", __LINE__, res, angle);

		}

		iana_shot_3Dtrajline(piana);

		iana_shot_allpointsonline(piana, 0);
		iana_shot_allpointsonline(piana, 1);
	}


	{	// Calc incline and azimuth
		double mx, my, mz;
		double ml;
		double azimuth;
		double incline;
		iana_shotresult_t *psr;

		mx = piana->ShotLine.m.x;
		my = piana->ShotLine.m.y;
		mz = piana->ShotLine.m.z;

		ml = sqrt(mx*mx + my * my);
		azimuth = RADIAN2DEGREE(atan2(mx, my));

		if (azimuth > +90) {
			azimuth = azimuth - 180;
		}
		if (azimuth < -90) {
			azimuth = azimuth + 180;
		}

		incline = RADIAN2DEGREE(atan2(mz, ml)); 		// Theta = tan-1( h / d)
		psr = &piana->shotresultdata;

		psr->azimuth = azimuth;
	}

func_exit:
	return res;
}

/*!
********************************************************************************
*	@brief      Refined Shot, 2nd.
*
*  @param[in]	piana
*              IANA module handle
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/0922
*******************************************************************************/
U32 RefineShot_BulkStereo(iana_t *piana)
{
	U32 res;
	U32 i;
	U32 camid;
	U32 index[2];
	iana_cam_t			*pic;
	iana_cam_param_t 	*picp;
	marksequence_t		*pmks;
	ballpair_t *pbp;

	cr_point_t *pCamPosL[2];
	double posdistance[MARKSEQUENCELEN];

	U32 marksequencelen;

	//---
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}

	// 1) Line btw cam0 and ball.
	// 2) Line btw cam1 and ball.
	// 3) Cross point.   -> Ball on Trj.
	for (camid = 0; camid < 2; camid++) {
		pic = piana->pic[camid];
		picp = &pic->icp;
		pCamPosL[camid] = &picp->CamPosL;
	}

	//	if (piana->ballpaircount > MARKSEQUENCELEN - 1) 
	if (piana->ballpaircount > marksequencelen) {
		res = 0;
		goto func_exit;
	}

	__try {			// 2017/12/07...    Crash removal..
		for (i = 0; i < piana->ballpaircount; i++) {
			cr_point_t ballpos[2];
			cr_point_t ballposTrj[2];
			cr_line_t  ballCamLine[2];

			pbp = &piana->ballpair[i];
			for (camid = 0; camid < 2; camid++) {
				U32 idx;
				pic = piana->pic[camid];
				picp = &pic->icp;
				pmks = &pic->mks;

				idx; index;

				ballpos[camid].x = pbp->posL[camid].x;
				ballpos[camid].y = pbp->posL[camid].y;
				if (camid == 0) {
					ballpos[camid].x += s_c0addx;
					ballpos[camid].y += s_c0addy;
				}
				ballpos[camid].z = 0;

				cr_line_p0p1(&ballpos[camid], pCamPosL[camid], &ballCamLine[camid]);
			}

			cr_point_line_line(&ballCamLine[0], &ballCamLine[1], &ballposTrj[0], &ballposTrj[1]);

			{
				double dx, dy, dz;
				dx = ballposTrj[0].x - ballposTrj[1].x;
				dy = ballposTrj[0].y - ballposTrj[1].y;
				dz = ballposTrj[0].z - ballposTrj[1].z;
				posdistance[i] = sqrt(dx*dx + dy * dy + dz * dz);
			}
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BALL PAIR KKK [%2d] %10lf %10lf   (%lf %lf %lf:%lf %lf %lf)   %10lf %10lf (%lf %lf %lf:%lf %lf %lf)      %10lf %10lf %10lf      %10lf %10lf %10lf : %lf\n",
				i,
				ballpos[0].x, ballpos[0].y,
				ballCamLine[0].m.x, ballCamLine[0].m.y, ballCamLine[0].m.z,
				ballCamLine[0].p0.x, ballCamLine[0].p0.y, ballCamLine[0].p0.z,

				ballpos[1].x, ballpos[1].y,
				ballCamLine[1].m.x, ballCamLine[1].m.y, ballCamLine[1].m.z,
				ballCamLine[1].p0.x, ballCamLine[1].p0.y, ballCamLine[1].p0.z,

				ballposTrj[0].x, ballposTrj[0].y, ballposTrj[0].z,
				ballposTrj[1].x, ballposTrj[1].y, ballposTrj[1].z,
				posdistance[i]
			);

			memcpy(&piana->ballpairPosL[i], &ballposTrj[0], sizeof(cr_point_t));
			if (i != 0) {
				double dx, dy, dz;
				double ddist;
				double ts64diff;
				double vvv;

				//--
				dx = piana->ballpairPosL[i].x - piana->ballpairPosL[i - 1].x;
				dy = piana->ballpairPosL[i].y - piana->ballpairPosL[i - 1].y;
				dz = piana->ballpairPosL[i].z - piana->ballpairPosL[i - 1].z;

				//ddist = sqrt(dx*dx+ dy*dy + dz*dz);
				ddist = cr_vector_distance(&piana->ballpairPosL[i], &piana->ballpairPosL[i - 1]);
				ts64diff = piana->ballpair[i].ts64d - piana->ballpair[i - 1].ts64d;
				vvv = ddist / ts64diff;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d] vmag_3: %10lf\n", i, vvv);

			}
		}

        {
			if (piana->ballpaircount > 4) {
				U32 i;
				U32 count, count2;
				double azimuth, incline, incline2;
				double xv, yv, zv, lv;
				cr_point_t p0, p1;
				cr_point_t *pp;
				cr_vector_t v0;

                double xx[MARKSEQUENCELEN];
				double yy[MARKSEQUENCELEN];
				double zz[MARKSEQUENCELEN];
				double ii[MARKSEQUENCELEN];
				
                double mx_, my_, mz_;
				double bx_, by_, bz_;

				double dy;

				iana_shotresult_t *psr;
				double ts64d0;//, ts64d;

				//--
				incline2; lv; p0; yv; p1; zv; xv; v0;

				psr = &piana->shotresultdata;

				count = piana->ballpaircount;
				count2 = count;
				ts64d0 = piana->ballpair[0].ts64d;
				for (i = 0; i < count; i++) {
					pbp = &piana->ballpair[i];
					pp = &piana->ballpairPosL[i];
					xx[i] = (double)pp->x;
					yy[i] = (double)pp->y;
					zz[i] = (double)pp->z;
					//					ii[i] = (double) i;
					ii[i] = pbp->ts64d - ts64d0;

					dy = yy[i] - yy[0];
					if (dy < 0) dy = -dy;

#define MAX_DY		0.2
#define MIN_COUNT	5
					if ((i <= MIN_COUNT) || (dy < MAX_DY)) {
						count2 = i;
					}
				}

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "----------\n");
				for (i = 0; i < count; i++) {
					pbp = &piana->ballpair[i];
					pp = &piana->ballpairPosL[i];
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%10lf\n", pp->x);
				}
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
				for (i = 0; i < count; i++) {
					pbp = &piana->ballpair[i];
					pp = &piana->ballpairPosL[i];
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%10lf\n", pp->y);
				}
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");
				for (i = 0; i < count; i++) {
					pbp = &piana->ballpair[i];
					pp = &piana->ballpairPosL[i];
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%10lf\n", pp->z);
				}

				cr_regression(ii, xx, count, &mx_, &bx_);			// x vs t - t0
				cr_regression(ii, yy, count, &my_, &by_);			// y vs t - t0
				cr_regression(ii, zz, count, &mz_, &bz_);			// z vs t - t0

				incline = RADIAN2DEGREE(atan2(mz_, my_)); 		// Theta = tan-1( h / d)
				azimuth = RADIAN2DEGREE(atan2(mx_, my_)); 		// Theta = tan-1( h / d)

				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "REFINED incline 3: %10lf  -> %10lf (%d)\n", psr->incline, incline, count2);
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "REFINED azimuth 3: %10lf  -> %10lf (%d)\n", psr->azimuth, azimuth, count2);

				for (i = 0; i < piana->ballpaircount; i++) {
					memcpy(&p0, &piana->ballpairPosL[i], sizeof(cr_point_t));
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%10d   %10.6lf %10.6lf %10.6lf\n", i, p0.x, p0.y, p0.z);
				}

				{
					cr_point_t sum;
					cr_point_t mean;
					cr_vector_t m;


					memset(&sum, 0, sizeof(cr_point_t));
					for (i = 0; i < piana->ballpaircount; i++) {
						cr_vector_add(&piana->ballpairPosL[i], &sum, &sum);
					}
					cr_vector_scalar(&sum, (1.0 / (double)piana->ballpaircount), &mean);

					m.x = mx_;
					m.y = my_;
					m.z = mz_;
					cr_vector_normalization(&m, &m);

					cr_line_p0m(&mean, &m, &piana->ShotLine);

					//-------------
					iana_shot_allpointsonline(piana, 0);
					iana_shot_allpointsonline(piana, 1);

					{	// Calc incline and azimuth
						double mx, my, mz;
						double azimuth;
						double incline;
						iana_shotresult_t *psr;

						mx = piana->ShotLine.m.x;
						my = piana->ShotLine.m.y;
						mz = piana->ShotLine.m.z;

						azimuth = RADIAN2DEGREE(atan2(mx, my));
						incline = RADIAN2DEGREE(atan2(mz, my)); 		// Theta = tan-1( h / d)

						psr = &piana->shotresultdata;

						psr->azimuth = azimuth;

						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "incline: %lf, azimuth: %lf\n", incline, azimuth);
					}
				}
			}
		}
		res = 1;
	}
	__except (GetExceptionCode() == EXCEPTION_ACCESS_VIOLATION
		|| GetExceptionCode() == EXCEPTION_ARRAY_BOUNDS_EXCEEDED
		|| GetExceptionCode() == EXCEPTION_INT_DIVIDE_BY_ZERO
		|| GetExceptionCode() == EXCEPTION_FLT_DIVIDE_BY_ZERO
		) {
		res = 0;
	}

func_exit:

	return res;
}

/*!
********************************************************************************
*	@brief      shotRegression
*
*  @param[in]	piana
*              IANA module handle
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/0917
*******************************************************************************/
U32 s_israw_on = 0;
U32 RegressionShotLocalLine(iana_t *piana, U32 camid)
{
	U32 res;

	iana_cam_t			*pic;
	iana_cam_param_t 	*picp;
	marksequence_t		*pmks;

	double Lxx[MARKSEQUENCELEN];
	double Lyy[MARKSEQUENCELEN];
	double tt[MARKSEQUENCELEN];
	double Lrr[MARKSEQUENCELEN];
	point_t firstL;

	U32 count;
	U32 isFirst;
	U64 ts64shot;

	U32 i;
	ballmarkinfo_t		*pbmi;
	U32 marksequencelen;

	//----
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}
	pic = piana->pic[camid];
	picp = &pic->icp;
	pmks = &pic->mks;
	pbmi = &pic->bmi[0];

	count = 0;
	isFirst = 1;
	ts64shot = pic->ts64shot;
	for (i = 0; i < marksequencelen; i++) {
		U32 israw;
		if (pmks->ballPosvalid[i]) {
			double dx, dy, dxy;
			double lx, ly;

			dx = pmks->ballposL2Raw[i].x - pmks->ballposL[i].x;
			dy = pmks->ballposL2Raw[i].y - pmks->ballposL[i].y;

			dxy = sqrt(dx*dx + dy * dy);
			if (dxy > RAW_L_DIFF) {
				lx = pmks->ballposL[i].x;
				ly = pmks->ballposL[i].y;
				israw = 0;
			} else {
				lx = pmks->ballposL2Raw[i].x;
				ly = pmks->ballposL2Raw[i].y;
				israw = 1;
			}

			if (s_israw_on == 0) {
				israw = 1;
			}

			if (israw) {
				if (isFirst) {
					firstL.x = lx;
					firstL.y = ly;
					isFirst = 0;
				} else {
					Lxx[count] = lx;
					Lyy[count] = ly;
					tt[count] = (double)((I64)pmks->ts64[i] - (I64)ts64shot) / TSSCALE_D;
					Lrr[count] = pmks->ballrL[i];

					if (tt[count] > 0) {			// 20200305
						count++;
					}
				}
			}

		}
	}

#define BALLCOUNT_MIN		5
#define BALLCOUNT_START 	0 // 2
#define BALLCOUNT_LEN		7 // 5, 10
	if (count >= BALLCOUNT_MIN) {
		double maLx[3], maLy[3], maLr[3];
		double mxy_, bxy_;

		double r2;

		I32		startindex;
		I32		calccount;

		startindex = BALLCOUNT_START;
		if (count >= BALLCOUNT_START + BALLCOUNT_LEN) {
			calccount = BALLCOUNT_LEN;
		}
		else {
			calccount = count - startindex;
		}

        caw_quadraticregression(tt + startindex, Lxx + startindex, calccount, &maLx[2], &maLx[1], &maLx[0], &r2);
		caw_quadraticregression(tt + startindex, Lyy + startindex, calccount, &maLy[2], &maLy[1], &maLy[0], &r2);
		caw_quadraticregression(tt + startindex, Lrr + startindex, calccount, &maLr[2], &maLr[1], &maLr[0], &r2);

		cr_regression(Lyy + startindex, Lxx + startindex, calccount, &mxy_, &bxy_);

		memcpy(&picp->maLx[0], &maLx[0], sizeof(double) * 3);
		memcpy(&picp->maLy[0], &maLy[0], sizeof(double) * 3);
		memcpy(&picp->maLr[0], &maLr[0], sizeof(double) * 3);
		picp->mxy_ = mxy_; picp->bxy_ = bxy_;

		res = 1;
	}
	else {
		res = 0;
	}
	return res;
}


/*!
********************************************************************************
*	@brief      Store mark..
*
*  @param[in]	piana
*              IANA module handle
*  @param[in]	camid
*
*  @param[in]	seqnum
*
*  @param[in]	imgseg
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/1130
*******************************************************************************/
static int s_NNspin = 1;
static int s_usemxmy = 1;
U32 markStore(iana_t *piana, U32 camid, U32 seqnum, U32 sx2, U32 sy2, imagesegment_t imgseg[], double markangle[], double multfact)
{
	int i;

	iana_cam_t		*pic;
	imagesegment_t 	*pis;
	U32 count;
	ballmarkinfo_t	*pbmi;
	markinfo_t		*pmi;
	cr_point_t Pb;				//	Ball Position, 3D.
	point_t   PbL;				//	Ball Position.
	double     lr_;
	double     pr_;
	U64 ts64shot;
	I64 ts64delta;
	double ts64deltad;

	marksequence_t *pmks;

	U32    useProjPl;		// Use Projection Plane

	//---
	pic = piana->pic[camid];
	pmks = &pic->mks;

	pbmi = &pic->bmi[seqnum];
	count = 0;

	ts64shot = pic->ts64shot;

	if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
		useProjPl = 1;
	} else {
		useProjPl = 0;
	}

	if (s_useProjPl_flag == 0) {
		useProjPl = 0;
	}

	useProjPl = 0;

	{
		memcpy(&Pb, &pmks->ballposL3D[seqnum], sizeof(cr_point_t));
		lr_ = pmks->ballrL2[seqnum];
		pr_ = pmks->ballrP2[seqnum];

		memcpy(&PbL, &pmks->ballposL2[seqnum], sizeof(point_t));
		ts64delta = (I64)(pmks->ts64[seqnum] - ts64shot);
	}

	ts64deltad = (double)(ts64delta / TSSCALE_D);

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "seq: %d ------- (%10lf %10lf %10lf) (%lld %lld %lld %lf)\n", seqnum, Pb.x, Pb.y, Pb.z,
	//		ts64shot, 
	//		pbmi->ts64,
	//		ts64delta, ts64deltad );

	for (i = 0; i < MARKCOUNT; i++) {
		pis = &imgseg[i];
		pmi = &pbmi->markinfo[count];

		memcpy(&pmi->imsg, pis, sizeof(imagesegment_t));
		if (pis->state == IMAGESEGMENT_FULL) {
			point_t Pp, Lp;
			U32 w, h;
			U32 cx, cy;
			U32 area;
			U32 kind;
			double whrate;
			double pixelrate;

			//---
			kind = MARK_KIND_EMPTY;

			w = pis->ixr - pis->ixl + 1;
			h = pis->iyb - pis->iyu + 1;

			if (s_usemxmy) {
				cx = (U32)(pis->mx + 0.5);
				cy = (U32)(pis->my + 0.5);
			} else {
				cx = (pis->ixr + pis->ixl) / 2;
				cy = (pis->iyb + pis->iyu) / 2;
			}

			whrate = (double)w / (double)h;
			area = w * h;
			pixelrate = (double)pis->pixelcount / (double)area;

#define WHRATE_MIN 0.7 // 0.6
#define WHRATE_MAX 1.4 // 1.6
#define AREARATE	0.5
#define MARKDOTSIZE_MAX	25
#define MARKBARSIZE_MIN	13

			kind = MARK_KIND_BAR;
			if (w < MARKDOTSIZE_MAX && h < MARKDOTSIZE_MAX) {
				if (whrate < WHRATE_MAX && whrate > WHRATE_MIN) {
					if (pixelrate > AREARATE) {
						kind = MARK_KIND_DOT;
					}
				}
			}
			if (w < MARKBARSIZE_MIN && h < MARKBARSIZE_MIN) {
				kind = MARK_KIND_DOT;
			}

			//--
			Pp.x = cx / multfact + sx2;
			Pp.y = cy / multfact + sy2;
			pmi->posP.x = Pp.x; pmi->posP.y = Pp.y;
			pmi->posP.z = 0;  // FAKE

			{
				{
					double dd;
					double x, y, z;
					double r2;

					x = pmi->rposP.x;
					y = pmi->rposP.y;
					r2 = pr_ * pr_;

					dd = x * x + y * y;
					if (dd >= r2) {
						z = 0;
					}
					else {
						z = sqrt(r2 - dd);
					}
					pmi->rposP.z = z;
				}

				iana_P2L(piana, camid, &Pp, &Lp, 0);	// to Local point
				if (useProjPl) {
					iana_P2L3D_EP(piana, camid, 0.5, &Pp, &pmi->Npos3D);
					iana_P2L_EP(piana, camid, 0.5, &Pp, &Lp);
				}

				memcpy(&pmi->NposP, &Pp, sizeof(point_t));
				memcpy(&pmi->NposL, &Lp, sizeof(point_t));
			}

            pmi->kind = kind;
			pmi->angle = markangle[i];

			//-- 
			{
				U32 res;
				point_t markL, markP;
				//---
				
				if (s_use_rL3) {
					//
					cr_point_t Npos3DwrtBall2;
					if (useProjPl) {
						res = ballmark3D2(
							piana, 			//I
							&pic->icp.CamPosL, 		//I
							&pmks->ballposL3D[seqnum],	//I
							&pmi->NposL,			//I
							BALLRADIUS,					//I
							&Npos3DwrtBall2	// 3D position
						);
						res = ballmark3D3(
							piana, 			//I
							camid,
							&pic->icp.CamPosL, 		//I
							&pmks->ballposL3D[seqnum],	//I
							&pmks->ballposP[seqnum],	//I
							&pmi->NposP,			//I
							BALLRADIUS,					//I
							&Npos3DwrtBall2	// 3D position
						);
					} else {
						res = ballmark3D2(
							piana, 			//I
							&pic->icp.CamPosL, 		//I
							&pmks->ballposL3D[seqnum],	//I
							&pmi->NposL,			//I
							BALLRADIUS,					//I
							&Npos3DwrtBall2	// 3D position
						);
					}
					memcpy(&pmi->Npos3DwrtBall, &Npos3DwrtBall2, sizeof(cr_vector_t));

					if (piana->spinsimulmode) {
						cr_point_t *pcampos;
						cr_point_t *pballPos;
						cr_point_t Npos3D2;
						cr_line_t cmline;	// Line between Camera and mark
						cr_plane_t xyplane;
						point_t markL, markP;
						cr_point_t markL3D;

						// make line between Cam and mark L position
						pcampos = &pic->icp.CamPosL;
						pballPos = &pmks->ballposL3D[seqnum];

						cr_vector_add(&pic->bmiTest[seqnum].markinfo[count].Npos3DwrtBall, pballPos, &Npos3D2);
						cr_line_p0p1(pcampos, &Npos3D2, &cmline);


						{	// Z = 0 plane (XY plane)
							cr_vector_t nv;
							cr_point_t origin;

							nv.x = 0; nv.y = 0; nv.z = 1;
							origin.x = 0; origin.y = 0; origin.z = 0;
							cr_plane_p0n(&origin, &nv, &xyplane);
						}

						// get intersection point between line and z=0 plane.
						res = cr_point_line_plane(&cmline, &xyplane, &markL3D); 		// Mark Position on the z=0 Plane
						markL.x = markL3D.x; markL.y = markL3D.y;
						iana_L2P(piana, camid, &markL, &markP, 0);
						//memcpy(&pmi->Npos3DwrtBall, &pic->bmiTest[seqnum].markinfo[count].Npos3DwrtBall, sizeof(cr_vector_t));
						memcpy(&pmi->NposL, &markL, sizeof(point_t));

						if (useProjPl) {
							res = ballmark3D3(
								piana, 			//I
								camid,
								&pic->icp.CamPosL, 		//I
								&pmks->ballposL3D[seqnum],	//I
								&pmks->ballposP[seqnum],	//I
								&pmi->NposP,			//I
								BALLRADIUS,					//I
								&Npos3DwrtBall2	// 3D position
							);
						} else {
							res = ballmark3D2(
								piana, 			//I
								&pic->icp.CamPosL, 		//I
								&pmks->ballposL3D[seqnum],	//I
								&pmi->NposL,			//I
								BALLRADIUS,					//I
								&Npos3DwrtBall2	// 3D position
							);
						}
						
                        if (pic->bmiTest[seqnum].markinfo[count].Npos3DwrtBall.z > 0) {
							memcpy(&pmi->Npos3DwrtBall, &Npos3DwrtBall2, sizeof(cr_vector_t));
						} else {
							memcpy(&pmi->Npos3DwrtBall, &pic->bmiTest[seqnum].markinfo[count].Npos3DwrtBall, sizeof(cr_vector_t));
						}
					}
				} else {
					res = ballmark3D(
						piana, 			//I
						&pic->icp.CamPosL, 		//I
						&pmks->ballposL3D[seqnum],	//I
						&pmi->NposL,			//I
						&pbmi->BallPlane,		//I
						&pbmi->ua, &pbmi->ub, &pbmi->uc, 		// I
						lr_,					//I

						&pmi->cofa, &pmi->cofb, &pmi->cofc,
						&pmi->NposOnBP,	// Mark On Ball Plane
						&pmi->NposOnBPwrtBall,	// Mark On Ball Plane
						&pmi->Npos3DwrtBall	// 3D position
					);

				}

				ballmarkLP(
					piana, 						// I
					camid,
					&pic->icp.CamPosL, 			// I,   Cam  position
					&pmks->ballposL3D[seqnum],	// I,	Ball position
					&pmi->Npos3DwrtBall,	// I,	Mark 3D position w.r.t. Ball
					&markL,			// Mark L position
					&markP
				);

				/*    Error of L : ~ 0.0001,    P: ~ 0.15 */
				if (s_NNspin) {
					pmi->rposLTopview.x = pmi->Npos3DwrtBall.x;
					pmi->rposLTopview.y = pmi->Npos3DwrtBall.y;
					pmi->rposLTopview.z = pmi->Npos3DwrtBall.z;
				}
			}

			count++;
		} else {
			pmi->kind = MARK_KIND_EMPTY;
		}
	}

	pbmi->markcount = count;
	return 1;
}


/*!
********************************************************************************
*	@brief      Ball mark P position to 3Dmark
*
*  @param[in]	piana
*              IANA module handle
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2017/0523
*******************************************************************************/
U32 ballmark3D(
	iana_t *piana, 				//I
	cr_point_t *pcampos, 		//I
	cr_point_t *pballposL3D,	//I
	point_t *pmarkL,			//I
	cr_plane_t *pBallPlane,		//I
	cr_vector_t *pua, cr_vector_t *pub, cr_vector_t *puc, 		// I
	double lr_,					//I

	double *pcofa, double *pcofb, double *pcofc,
	cr_point_t *pmarkOnBP,	// Mark On Ball Plane
	cr_point_t *pmarkOnBPwrtBall,	// Mark On Ball Plane
	cr_point_t *pmark3DwrtBall	// 3D position
)

{
	U32 res;
	cr_line_t cmline;	// Line between Camera and mark L position
	cr_point_t markL;
	double cofa, cofb, cofc;
	double dtmp;
	double x, y, z;

	//---

	markL.x = pmarkL->x;
	markL.y = pmarkL->y;
	markL.z = 0.0;
	// make line between Cam and mark L position
	cr_line_p0p1(pcampos, &markL, &cmline);

	// Get line - BallPlane intersection point
	res = cr_point_line_plane(&cmline, pBallPlane, pmarkOnBP); 		// Mark Position on the Ball Plane

	// Get Relative position w.r.t. Ball3D position
	cr_vector_sub(pmarkOnBP, pballposL3D, pmarkOnBPwrtBall);

	cofa = cr_vector_inner(pmarkOnBPwrtBall, pua);			// TRIVIAL... near zero. discard it!
	cofb = cr_vector_inner(pmarkOnBPwrtBall, pub);
	cofc = cr_vector_inner(pmarkOnBPwrtBall, puc);

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "cofa: %10lf cofb: %10lf cofc: %10lf\n",
	//		cofa, cofb, cofc);

	dtmp = (cofb*cofb + cofc * cofc);
	dtmp = lr_ * lr_ - dtmp;
	if (dtmp > 0.0) {
		cofa = sqrt(dtmp);
	}
	else {
		cofa = 0.0;
	}

	x = cofa * pua->x + cofb * pub->x + cofc * puc->x;
	y = cofa * pua->y + cofb * pub->y + cofc * puc->y;
	z = cofa * pua->z + cofb * pub->z + cofc * puc->z;

	pmark3DwrtBall->x = cofa * pua->x + cofb * pub->x + cofc * puc->x;
	pmark3DwrtBall->y = cofa * pua->y + cofb * pub->y + cofc * puc->y;
	pmark3DwrtBall->z = cofa * pua->z + cofb * pub->z + cofc * puc->z;
	
    *pcofa = cofa;
	*pcofb = cofb;
	*pcofc = cofc;

	res = 1;

	piana;
	return res;
}



/*!
********************************************************************************
*	@brief      Ball mark P position to 3Dmark, using line-sphere interconnection
*
*  @param[in]	piana
*              IANA module handle
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2017/0711
*******************************************************************************/
U32 ballmark3D2(
	iana_t *piana, 				//I
	cr_point_t *pcampos, 		//I
	cr_point_t *pballposL3D,	//I
	point_t *pmarkL,			//I
	double lr_,					//I
	cr_point_t *pmark3DwrtBall	// 3D position, wrt ball.
)
{
	U32 res;
	cr_line_t cmline;	// Line between Camera and mark L position
	cr_point_t markL;
	cr_sphere_t sp;

	cr_point_t p[2];
	cr_point_t mark3D;
	U32 count;
	double det;
	//---

	markL.x = pmarkL->x;
	markL.y = pmarkL->y;
	markL.z = 0.0;

	// make line between Cam and mark L position
	cr_line_p0p1(pcampos, &markL, &cmline);

	// Make Ball sphere. :)
	cr_sphere_make(pballposL3D, lr_, &sp);

	// Get 3D position
	res = cr_line_sphere_mild(&cmline, &sp, &p[0], &count, &det);

	if (res != 0) {
		if (count >= 1) {
			memcpy(&mark3D, &p[0], sizeof(cr_point_t));
		}
		else {
			res = 0;
		}
	}
	if (res != 0) {
		cr_vector_sub(&mark3D, pballposL3D, pmark3DwrtBall);
	}
	piana;
	return res;
}



/*!
********************************************************************************
*	@brief      Ball mark P position to 3Dmark, using line-sphere interconnection
*	            and projection plane.
*
*  @param[in]	piana
*              IANA module handle
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2018/1231
*******************************************************************************/
U32 ballmark3D3(
	iana_t *piana, 				//I
	U32 camid,
	cr_point_t *pcampos, 		//I
	cr_point_t *pballposL3D,	//I
	cr_point_t *pballposP,	//I
	point_t *pmarkP,			//I
	double lr_,					//I
	cr_point_t *pmark3DwrtBall	// 3D position, wrt ball.
)
{
	U32 res;
	cr_line_t cmline;	// Line between Camera and mark L position
	cr_sphere_t sp;

	cr_point_t p[2];
	cr_point_t mark3D;
	U32 count;
	double det;

	cr_point_t markL3D;
	cr_point_t ballposL3D_EP;

	double dx, dy, dz;
	double camtoballdistance;

	point_t ballposP;

    //---
	dx = pcampos->x - pballposL3D->x;
	dy = pcampos->y - pballposL3D->y;
	dz = pcampos->z - pballposL3D->z;
	camtoballdistance = sqrt(dx*dx + dy * dy + dz * dz);

	ballposP.x = pballposP->x;
	ballposP.y = pballposP->y;
	iana_P2L3D_EP(piana, camid, camtoballdistance, pmarkP, &markL3D);
	iana_P2L3D_EP(piana, camid, camtoballdistance, &ballposP, &ballposL3D_EP);

	// make line between Cam and mark L position
	cr_line_p0p1(pcampos, &markL3D, &cmline);

	// Make Ball sphere. :)
	cr_sphere_make(&ballposL3D_EP, lr_, &sp);

	// Get 3D position
	res = cr_line_sphere_mild(&cmline, &sp, &p[0], &count, &det);

	if (res != 0) {
		if (count == 1) {
			mark3D.x = p[0].x; mark3D.y = p[0].y; mark3D.z = p[0].z;
		} else if (count == 2) {
			if (p[0].z > p[1].z) {
				mark3D.x = p[0].x; mark3D.y = p[0].y; mark3D.z = p[0].z;
			} else {
				mark3D.x = p[1].x; mark3D.y = p[1].y; mark3D.z = p[1].z;
			}
		} else {
			res = 0;
		}
	}
	if (res != 0) {
		cr_vector_sub(&mark3D, pballposL3D, pmark3DwrtBall);
	}

	piana;
	return res;
}


/*!
********************************************************************************
*	@brief      Ball mark 3Dmark to L/P position
*
*  @param[in]	piana
*              IANA module handle
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2017/0523
*******************************************************************************/
U32 ballmarkLP(
	iana_t *piana, 				// I
	U32 camid,
	cr_point_t *pcampos, 		//I
	cr_point_t *pballposL3D,	// I,	Ball position
	cr_point_t *pmark3DwrtBall,	// I,	Mark 3D position w.r.t. Ball
	point_t	   *pmarkL,			// Mark L position
	point_t	   *pmarkP			// Mark P position
)
{
	U32 res;
	cr_line_t cmline;	// Line between Camera and mark
	cr_point_t mark3D;	// Mark 3D position
	cr_point_t markL;	// Mark L position
	cr_plane_t xyplane;		//I,

	//---
	// 1) get P3D of mark.
	// 2) Get line passes P3D and Cam.
	// 3) get intersection point between line and z=0 (xy) plane. This is L point of given mark.
	// 4) Convert to P point.

	{	// Z = 0 plane (XY plane)
		cr_vector_t nv;
		cr_point_t origin;

		nv.x = 0; nv.y = 0; nv.z = 1;
		origin.x = 0; origin.y = 0; origin.z = 0;
		cr_plane_p0n(&origin, &nv, &xyplane);
	}

	// 1) get P3D of mark.
	cr_vector_add(pmark3DwrtBall, pballposL3D, &mark3D); 		// mark3D = pmark3DwrtBall + pballposL3D;

	// 2) Get line passes Cam and mark3D point.
	cr_line_p0p1(pcampos, &mark3D, &cmline);

	// 3) get intersection point between line and z=0 plane.
	res = cr_point_line_plane(&cmline, &xyplane, &markL); 		// Mark Position on the z=0 Plane

	pmarkL->x = markL.x;
	pmarkL->y = markL.y;

	iana_L2P(piana, camid, pmarkL, pmarkP, 0);

	return res;
}


/*!
********************************************************************************
*	@brief      enumerate Ballpair
*
*  @param[in]	piana
*              IANA module handle
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/0922
*******************************************************************************/
U32 PairingBall_Stereo(iana_t *piana)
{
	U32 res;
	iana_cam_t	*pic;
	U64 ts64shot;
	double ts64dd;
	U32 ballpaircount;
	ballpair_t *pbp;
	U32 camid;
	U32 count, counts[2];
	U32 i, j;

#define STARTPOSITIONMULT	0 // 5
	double xx[MARKSEQUENCELEN * 2 + STARTPOSITIONMULT], yy[MARKSEQUENCELEN * 2 + STARTPOSITIONMULT], tt[MARKSEQUENCELEN * 2 + STARTPOSITIONMULT];
	double ax2_[2], ax1_[2], ax0_[2];
	double ay2_[2], ay1_[2], ay0_[2];
	double r2;

	double ts64shotd0;
	double ts64shotd1;
	U32    tscount;
	marksequence_t *pmks[2];
	U32 marksequencelen;

	//======
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}

	for (camid = 0; camid < 2; camid++) {
		pic = piana->pic[camid];
		pmks[camid] = &pic->mks;
	}

	//--
	ballpaircount = 0;
	memset(&piana->ballpair[0], 0, sizeof(ballpair_t) * MARKSEQUENCELEN);
	ts64shot = piana->pic[0]->ts64shot;

	counts[0] = counts[1] = 0;
	tscount = 0;
	ts64shotd0 = 0;
	ts64shotd1 = 0;
	
    for (camid = 0; camid < 2; camid++) {
		pic = piana->pic[camid];
		count = 0;
		for (i = 0; i < STARTPOSITIONMULT; i++) {
			xx[count] = pic->icp.L.x;
			yy[count] = pic->icp.L.y;
			tt[count] = 0;
			count++;
		}

		for (i = 0; i < marksequencelen; i++) {
			for (j = 0; j < BALLCCOUNT; j++) {
				iana_ballcandidate_t *pbc;
				pbc = &pic->bc[i][j];
				if (pbc->cexist == IANA_BALL_EXIST) {
					ts64dd = ((I64)(pbc->ts64 - ts64shot)) / TSSCALE_D;
					xx[count] = pbc->L.x;
					yy[count] = pbc->L.y;
					tt[count] = ts64dd * 1000;

					if (camid == 0) {
						if (tscount == 0) {
							tscount = 0;
							ts64shotd0 = ts64dd;
							ts64shotd1 = ts64dd;
						}
						else {
							ts64shotd1 = ts64dd;
						}
						tscount++;
					}
					count++;
					break;
				}
			}
		}

		for (i = 0; i < count; i++) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "qqq [%2d:%4d] %lf %lf %lf\n",
				camid, i,
				xx[i],
				yy[i],
				tt[i]);
		}
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");

#define	GOOD4REGRESSION	4
		if (count >= GOOD4REGRESSION + STARTPOSITIONMULT) {
			I32 regres;
			I32 regoffset;

			regoffset = 0;
			ax2_[camid] = ax1_[camid] = ax0_[camid] = 0;
		
            regres = caw_quadraticregression(tt + regoffset, xx + regoffset, count - regoffset, &ax2_[camid], &ax1_[camid], &ax0_[camid], &r2);
			if (regres == 0 || r2 < 0.8) {
				ax2_[camid] = ax1_[camid] = ax0_[camid] = 0;
				cr_regression2(tt + regoffset, xx + regoffset, count - regoffset, &ax1_[camid], &ax0_[camid], &r2);
			}

			ay2_[camid] = ay1_[camid] = ay0_[camid] = 0;
			regres = caw_quadraticregression(tt + regoffset, yy + regoffset, count - regoffset, &ay2_[camid], &ay1_[camid], &ay0_[camid], &r2);
			
            if (regres == 0 || r2 < 0.8) {
				ay2_[camid] = ay1_[camid] = ay0_[camid] = 0;
				cr_regression2(tt + regoffset, yy + regoffset, count - regoffset, &ay1_[camid], &ay0_[camid], &r2);
			}
		}
		counts[camid] = count;
	}

	if (counts[0] >= GOOD4REGRESSION + STARTPOSITIONMULT && counts[1] >= GOOD4REGRESSION + STARTPOSITIONMULT) {
		double ts64ddm;
		ts64dd = ts64shotd0;
		I32 iter;
		iter = 0;
		for (i = 0; i < marksequencelen; i++) {
			pbp = &piana->ballpair[i];
			pbp->index[0] = 0;
			pbp->index[1] = 0;

			ts64dd = ts64shotd0 + i * ((ts64shotd1 - ts64shotd0) / tscount);
			if (i >= tscount) {
				break;
			}

			ts64ddm = ts64dd * 1000;

			pbp->ts64d = ts64shot / TSSCALE_D + ts64dd;
			pbp->valid = 1;


			for (camid = 0; camid < 2; camid++) {
				pbp->posL[camid].x = ax2_[camid] * ts64ddm * ts64ddm + ax1_[camid] * ts64ddm + ax0_[camid];
				pbp->posL[camid].y = ay2_[camid] * ts64ddm * ts64ddm + ay1_[camid] * ts64ddm + ay0_[camid];
			}
			iter++;
		}
		piana->ballpaircount = iter;;

		for (camid = 0; camid < 2; camid++) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%d   X %lf %lf %lf  Y %lf %lf %lf\n", camid,
				ax2_[camid], ax1_[camid], ax0_[camid],
				ay2_[camid], ay1_[camid], ay0_[camid]);

			for (i = 0; i < piana->ballpaircount; i++) {
				pbp = &piana->ballpair[i];

				pbp->posL[camid].x;
				pbp->posL[camid].y;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%4d] %lf %lf %lf\n",
					camid, i,
					pbp->posL[camid].x,
					pbp->posL[camid].y,
					pbp->ts64d - (ts64shot / TSSCALE_D));
			}
		}

		res = 1;
	} else {
		res = 0;
	}

	return res;
}

/*!
********************************************************************************
*	@brief      Mark Rotation calc.
*
*  @param[in]	piana
*              IANA module handle
*  @param[in]	camid
*              camera id
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/1202
*******************************************************************************/
U32 markSpin(iana_t *piana, U32 camid)
{
	U32 res;

	U32 i, j;
	iana_cam_t	*pic;
	ballmarkinfo_t	*pbmi;
	markinfo_t		*pmi;

	U32 marksequencelen;

	//--
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	}
	else {
		marksequencelen = MARKSEQUENCELEN;
	}
	pic = piana->pic[camid];

	for (i = 0; i < marksequencelen; i++) {
		pbmi = &pic->bmi[i];

        if (pbmi->valid) {
			for (j = 0; j < MARKCOUNT; j++) {
				pmi = &pbmi->markinfo[j];
			}
		}
	}

	// Get N-nearest marks.
	markNearest(piana, camid);

	// Get rotation vector for each mark.. 
	markRotationVect(piana, camid);

	//	// Get best rotation vector
	//	markRotationBest(piana, camid);

	res = 1;
	return res;
}




static int mkdcompare(const void *arg1, const void *arg2)
{					// smallest element is first... 
	int res;
	markdistance_t *mkd1, *mkd2;
	double v1, v2;

	mkd1 = (markdistance_t *)arg1;
	mkd2 = (markdistance_t *)arg2;

	v1 = mkd1->distance;
	v2 = mkd2->distance;
	if (v1 < v2) {
		res = -1;
	} else if (v1 == v2) {
		res = 0;
	} else {
		res = 1;
	}

	return res;
}

/*!
********************************************************************************
*	@brief      Mark Get nearest mark.
*
*  @param[in]	piana
*              IANA module handle
*  @param[in]	camid
*              camera id
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/1204
*******************************************************************************/
U32 markNearest(iana_t *piana, U32 camid)
{
	U32 res;

	U32 i, j, k;
	ballmarkinfo_t	*pbmi0, *pbmi1;
	markinfo_t		*pmi0, *pmi1;
	iana_cam_t		*pic;
	markdistance_t	*pmkd;
	U32 marksequencelen;

	//--
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}

	pic = piana->pic[camid];
	
    // 1) Calc distance between marks of ith seq. and marks of i+1th seq.
	for (i = 0; i < marksequencelen - 1; i++) {
		pbmi0 = &pic->bmi[i];
		pbmi1 = &pic->bmi[i + 1];

		for (j = 0; j < MARKCOUNT; j++) {
			pmi0 = &pbmi0->markinfo[j];
			if (pmi0->kind != MARK_KIND_EMPTY) {
				pmkd = &pmi0->mkd[0];
				
                for (k = 0; k < MARKCOUNT; k++) {
					pmi1 = &pbmi1->markinfo[k];
					pmkd[k].id = k;

					if (pmi0->kind == pmi1->kind) {
						pbmi0->valid = 1;
						pbmi1->valid = 1;
						pmkd[k].distance = calc_distance(pmi0, pmi1);
					} else {
						pmkd[k].distance = 1.0e10;
					}
				}	// k
				qsort((void *)pmkd, MARKCOUNT, sizeof(markdistance_t), mkdcompare);
			}
		}	// j
	} // i


	res = 1;
	return res;
}


double calc_distance(markinfo_t *pmi0, markinfo_t *pmi1)
{
	double distance;
	double dx, dy, dz;
	dx = pmi0->rposLTopview.x - pmi1->rposLTopview.x;
	dy = pmi0->rposLTopview.y - pmi1->rposLTopview.y;
	dz = pmi0->rposLTopview.z - pmi1->rposLTopview.z;
	//	distance = sqrt(dx*dx + dy*dy + dz*dz);

	distance = cr_vector_distance(&pmi0->rposLTopview, &pmi1->rposLTopview);
	return distance;
}


/*!
********************************************************************************
*	@brief      Mark Rotation vector
*
*  @param[in]	piana
*              IANA module handle
*  @param[in]	camid
*              camera id
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/1204
*******************************************************************************/
U32 markRotationVect(iana_t *piana, U32 camid)
{
	U32 res;

	U32 seq;
	U32 i0, i1, i2;
	U32 j, k;
	iana_cam_param_t *picp;

	markrotation_t 	*pmkr;
	U32 markrotationcount;
	ballmarkinfo_t	*pbmi0, *pbmi1, *pbmi2;
	markinfo_t		*pmi0, *pmi1, *pmi2;
	markdistance_t	*pmkd0, *pmkd1;
	iana_cam_t		*pic;
	cr_vector_t 	ax;
	double			angle;
	double			maxdistance;
	double			maxangle;
	double			lr_;

	marksequence_t	*pmks;

	U32 marksequencelen;

	//--
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}
	pic = piana->pic[camid];
	picp = &pic->icp;
	pmks = &pic->mks;

	maxangle = picp->maxangleOnetick;
	for (seq = 0; seq < marksequencelen - 2; seq++) {
		pbmi0 = &pic->bmi[seq];
		pbmi1 = &pic->bmi[seq + 1];
		pbmi2 = &pic->bmi[seq + 2];

		lr_ = pmks->ballrL2[seq];

		maxdistance = lr_ * maxangle * (2 * M_PI / 360.0);

		pmkr = &pbmi0->mkr[0];
		pbmi0->markrotationcount = 0;
		if (pbmi0->valid && pbmi1->valid && pbmi2->valid) {
			markrotationcount = 0;
			for (i0 = 0; i0 < MARKCOUNT; i0++) {
				pmi0 = &pbmi0->markinfo[i0];
				if (pmi0->kind != MARK_KIND_EMPTY) {
					pmkd0 = &pmi0->mkd[0];

#define NNEAREST	3
					for (j = 0; j < NNEAREST; j++) {
						if (pmkd0[j].distance > maxdistance) {
							continue;
						}
		
                        i1 = pmkd0[j].id;				// Next seq's mark id.
						if (i1 >= MARKCOUNT) {
							continue;
						}
						pmi1 = &pbmi1->markinfo[i1];
						pmkd1 = &pmi1->mkd[0];
						//
						
                        for (k = 0; k < NNEAREST; k++) {
							if (pmkd1[k].distance >= maxdistance) {
								//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "1[%d] : %lf  too big.\n", k, pmkd1[k].distance);
								continue;
							}
							//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "1[%d] : %lf\n", k, pmkd1[k].distance);
							i2 = pmkd1[k].id;				// Next seq's mark id.
							if (i2 >= MARKCOUNT) {			// what?
								continue;
							}
							pmi2 = &pbmi2->markinfo[i2];

							if (pmi0->kind == pmi1->kind && pmi1->kind == pmi2->kind) {
								cr_vector_t v0, v1, v2;
								cr_vector_t d0, d1;
								cr_vector_t v0ax, v1ax, v2ax;
								cr_vector_t v0nax, v1nax, v2nax;
								cr_vector_t	vv;
								double vdotv;
								double s0, s1, s2;

								//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[[[1]]]\n");
								// Calc rotation vector with 3 position.
								//memcpy(&v0, &pmi0->rposLTopview.x, sizeof(cr_vector_t));
								//memcpy(&v1, &pmi1->rposLTopview.x, sizeof(cr_vector_t));
								//memcpy(&v2, &pmi2->rposLTopview.x, sizeof(cr_vector_t));
								memcpy(&v0, &pmi0->rposLTopview, sizeof(cr_vector_t));
								memcpy(&v1, &pmi1->rposLTopview, sizeof(cr_vector_t));
								memcpy(&v2, &pmi2->rposLTopview, sizeof(cr_vector_t));

								cr_vector_sub(&v1, &v0, &d0);
								cr_vector_sub(&v2, &v1, &d1);

								cr_vector_cross(&d0, &d1, &ax);
								cr_vector_normalization(&ax, &ax);

								//---- Rotation angle.
								vdotv = cr_vector_inner(&ax, &v0);
								cr_vector_scalar(&ax, vdotv, &v0ax); 		// v0ax = (ax . v0) vax;
								cr_vector_sub(&v0, &v0ax, &v0nax); 			// v0nax = v0 - v0ax;
								s0 = cr_vector_norm(&v0nax);

								vdotv = cr_vector_inner(&ax, &v2);
								cr_vector_scalar(&ax, vdotv, &v2ax); 		// v2ax = (ax . v2) vax;
								cr_vector_sub(&v2, &v2ax, &v2nax); 			// v2nax = v2 - v2ax;
								s2 = cr_vector_norm(&v2nax);

								vdotv = cr_vector_inner(&v0nax, &v2nax);
								angle = RADIAN2DEGREE(acos(vdotv / (s0*s2)));						// theta = acos(v0nax . v2nax / |v0nax | * |v2nax |);

								cr_vector_cross(&v0nax, &v2nax, &vv); 		// vv = v0ax x v1ax;							
								vdotv = cr_vector_inner(&vv, &ax);
								if (vdotv < 0) {			// opposite direction
									angle *= -1;
								}

								angle = angle / 2;

								{						// Angle value is too errous
									double angle0;
									double angle1;
									double angle2;
									double angle3;
									double dtmp;
									double dinner;
									double v0_, v1_, v2_;

									dinner = cr_vector_inner(&v0, &v1);
									v0_ = cr_vector_norm(&v0);
									v1_ = cr_vector_norm(&v1);
									dtmp = dinner / (v0_*v1_);
									angle1 = RADIAN2DEGREE(acos(dtmp));

									dinner = cr_vector_inner(&v1, &v2);
									v1_ = cr_vector_norm(&v1);
									v2_ = cr_vector_norm(&v2);
									dtmp = dinner / (v1_*v2_);
									angle2 = RADIAN2DEGREE(acos(dtmp));



									dinner = cr_vector_inner(&v0, &v2);
									v0_ = cr_vector_norm(&v0);
									v2_ = cr_vector_norm(&v2);
									dtmp = dinner / (v0_*v2_);
									angle3 = RADIAN2DEGREE(acos(dtmp));

									angle0 = (angle1 * 2 + angle2 * 2 + angle3 * 1) / (2 + 2 + 1);

									//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "angle %10lf vs angle123: %lf %lf %lf -> %lf\n",
									//		angle, angle1, angle2, angle3, angle0);

									if (angle * angle0 > 0) {
										angle = angle0;			// Same sign
									}
									else {
										angle = -angle0;		// Opposite sign
									}
								}

								if (angle < -maxangle || angle > maxangle) {
									//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[[[2]]], angle: %lf\n", angle);
									angle = -999;
									memset(&ax, 0, sizeof(cr_vector_t));
									//fail.
								} else {
									double dx, dy, dz;
									double derr1, derr2;
									cr_vector_t v1_, v2_;

									//--
									if (angle < 0) {
										angle *= -1;
										cr_vector_scalar(&ax, -1, &ax);
									}

									pmkr[markrotationcount].angle = angle;
									memcpy(&pmkr[markrotationcount].axis, &ax, sizeof(cr_vector_t));
									pmkr[markrotationcount].i0 = i0;
									pmkr[markrotationcount].i1 = i1;
									pmkr[markrotationcount].i2 = i2;
									rodriguesMatrix(&ax, DEGREE2RADIAN(angle), pmkr[markrotationcount].rrma);

									rotateV(pmkr[markrotationcount].rrma, &v0, &v1_);
									rotateV(pmkr[markrotationcount].rrma, &v1_, &v2_);

									dx = v1.x - v1_.x; dy = v1.y - v1_.y; dz = v1.z - v1_.z;
									derr1 = cr_vector_distance(&v1, &v1_);
									pmkr[markrotationcount].derr1 = derr1;

									dx = v2.x - v2_.x; dy = v2.y - v2_.y; dz = v2.z - v2_.z;
									derr2 = cr_vector_distance(&v2, &v2_);
									pmkr[markrotationcount].derr2 = derr2;

#define DERR_MIN	(0.002) // 0.001, 0.005, 0.0015
#define DERR_MIN_CONSOLE1	(0.010) // 0.005
//#define DERR_MIN_CONSOLE1	DERR_MIN
									double derr_min;
									if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
										derr_min = DERR_MIN_CONSOLE1;
									} else {
										derr_min = DERR_MIN;
									}

									if (derr1 < derr_min && derr2 < derr_min) {			// Validate...
										//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[[[4]]]\n");
										pmkr[markrotationcount].valid = 1;
										markrotationcount++;
										pbmi0->markrotationcount = markrotationcount;
									}
								}
								v1ax; v1nax; s1;
							} else {
								angle = 0;
								memset(&ax, 0, sizeof(cr_vector_t));
							}

							if (markrotationcount >= MARKROTCOUNT) {
								break;
							}
						}	// for (k = 0; k < NNEAREST; k++) 

						if (markrotationcount >= MARKROTCOUNT) {
							break;
						}
					}		// for (j = 0; j < NNEAREST; j++) 
				} 		// if (pmi0->kind != MARK_KIND_EMPTY) 

				if (markrotationcount >= MARKROTCOUNT) {
					break;
				}
			} 				// for (i0 = 0; i0 < MARKCOUNT; i0++) 
		} // if (pbmi0->valid && pbmi1->valid && pbmi2->valid)
	} 		// for (seq = 0; seq < MARKSEQUENCELEN-2; seq++) 
	res = 1;
	return res;
}


/*!
********************************************************************************
*	@brief      Mark Best Rotation
*
*  @param[in]	piana
*              IANA module handle
*  @param[in]	camid
*              camera id
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/1204
*******************************************************************************/
#if 1
U32 markRotationBest(iana_t *piana, U32 camid)
{
	U32 i, j, k;
	U32 rotid;
	U32 res;
	ballmarkinfo_t	*pbmi0, *pbmi1, *pbmi2;
	markinfo_t	    *pmi0, *pmi1, *pmi2;
	markrotation_t 	*pmkr0;
	//	markrotation_t 	*pmkr0, *pmkr1, *pmkr2;
	bestmatchindex_t *pbestmi;
	iana_cam_t		*pic;

	U32 marksequencelen;
	//--
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}
	pic = piana->pic[camid];

	for (i = 0; i < marksequencelen; i++) {
		pic->mkrSeqBest[i].valid = 0;
	}

	for (i = 0; i < marksequencelen; i++) {
		pbestmi = &pic->matchIndexSeqBest[i];
		for (j = 0; j < marksequencelen; j++) {
			for (k = 0; k < MARKCOUNT; k++) {
				pbestmi->bestindex[j][k] = -1;
			}
		}
	}

	pbestmi = &pic->matchIndexbest;
	for (j = 0; j < marksequencelen; j++) {
		for (k = 0; k < MARKCOUNT; k++) {
			pbestmi->bestindex[j][k] = -1;
		}
	}

	//--- Select best rotation for each seq.
	for (i = 0; i < marksequencelen - 2; i++) {
		pbmi0 = &pic->bmi[i];
		pbmi1 = &pic->bmi[i + 1];
		pbmi2 = &pic->bmi[i + 2];
		
        for (j = 0; j < pbmi0->markrotationcount; j++) {
			pmkr0 = &pbmi0->mkr[j];
			if (pmkr0->valid) {
				{
					cr_vector_t v0, v1, v1_;
					U32 i0, i1;
					U32 goodcount;
					markdistance_t	*pmkd0;

					pbmi0 = &pic->bmi[i];

					goodcount = 0;
					pmkr0->derrN = 0;
					for (i0 = 0; i0 < MARKCOUNT; i0++) {
						double dist;
						double mindist;
						U32 mindistindex;
						double mindist_good;

#define NNEARESTROTATION	5
						pmi0 = &pbmi0->markinfo[i0];
						//						pmkd0 = &pmi0->mkd[0];
						if (pbmi0->valid == 0) {
							continue;
						}
						memcpy(&v0, &pmi0->rposLTopview, sizeof(cr_vector_t));

#define MINDISTMAX	1.0
						mindist = MINDISTMAX;
						for (k = 0; k < NNEARESTROTATION; k++) {
							pmkd0 = &pmi0->mkd[k];
							if (pmkd0->distance > MINDISTMAX) {
								continue;
							}
							i1 = pmkd0->id;
							if (i1 >= MARKCOUNT) {
								continue;
							}
							pmi1 = &pbmi1->markinfo[i1];
							memcpy(&v1, &pmi1->rposLTopview, sizeof(cr_vector_t));

							rotateV(pmkr0->rrma, &v0, &v1_);
							dist = cr_vector_distance(&v1, &v1_);

							if (mindist > dist) {
								mindist = dist;
								mindistindex = i1;
							}
						}

#define MINDIST_GOOD	0.005 // 0.001, 0.01
#define MINDIST_GOOD_CONSOLE1	0.003 // 0.01
//#define MINDIST_GOOD_CONSOLE1	MINDIST_GOOD
						if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
							mindist_good = MINDIST_GOOD_CONSOLE1;
						}
						else {
							mindist_good = MINDIST_GOOD;
						}
						if (mindist < mindist_good) {
							goodcount++;
							pmkr0->derrN += mindist;
						}
					}
					pmkr0->goodcount = goodcount;

				}
			} 		// if (pmkr0->valid) 
		} 			// for (j = 0; j < pbmi0->markrotationcount; j++) 

		{
			U32 maxgoodcount;
			U32 maxgoodcountid;
			double minderrN;
			maxgoodcount = 0;
			maxgoodcountid = (U32)-1;
			minderrN = 1e10;
			for (j = 0; j < pbmi0->markrotationcount; j++) {
				pmkr0 = &pbmi0->mkr[j];
				if (maxgoodcount < pmkr0->goodcount) {
					maxgoodcount = pmkr0->goodcount;
					maxgoodcountid = j;
					minderrN = pmkr0->derrN;
				}
				else if (maxgoodcount == pmkr0->goodcount) {
					if (minderrN > pmkr0->derrN) {
						maxgoodcountid = j;
						minderrN = pmkr0->derrN;
					}
				}
			}

			if (maxgoodcount > 0) {
				double derrNmean;
				double totalrpm;

				//--
				pmkr0 = &pbmi0->mkr[maxgoodcountid];
				memcpy(&pic->mkrSeqBest[i], pmkr0, sizeof(markrotation_t));

				derrNmean = minderrN / maxgoodcount;
				
                totalrpm = (pmkr0->angle / 360.) * piana->framerate * 60;
			}
		}
	} 				// for (i = 0; i < MARKSEQUENCELEN-2; i++) 

	//--- Select best rotation 
#define ROTJUMP		3 // 5, 2
#define ROTJUMP_CONSOLE1	2
//#define ROTJUMP_CONSOLE1	ROTJUMP
	{
		U32 rotjump;
		
        if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
			rotjump = ROTJUMP_CONSOLE1;
		} else {
			rotjump = ROTJUMP;
		}

		for (rotid = 0; rotid < marksequencelen - 1; rotid++) {
			U32 goodcount;
			U32 goodcounttoosmall;
			double maxdist;

			pmkr0 = &pic->mkrSeqBest[rotid];
			maxdist = 0;
			if (pmkr0->valid) {
				pmkr0->derrN = 0;
				pmkr0->goodcount = 0;
				goodcount = 0;
				goodcounttoosmall = 0;
				pbestmi = &pic->matchIndexSeqBest[rotid];

				for (i = 0; i < marksequencelen - rotjump; i++) {
					U32 i0, i1, i2;
					cr_vector_t v0;
					cr_vector_t v1, v1_;
					cr_vector_t v2, v2_;

					//-
					pbmi0 = &pic->bmi[i];
					pbmi1 = &pic->bmi[i + rotjump];

					pmi2; v1; v1_; i2;
					if (pbmi0->valid == 0 || pbmi1->valid == 0) {
						continue;
					}

					for (i0 = 0; i0 < MARKCOUNT; i0++) {		// For each point..
						double dist;
						double dist0;
						double mindist;
						U32 mindistindex;

						//--
						pmi0 = &pbmi0->markinfo[i0];
						if (pmi0->kind == MARK_KIND_EMPTY) {
							continue;
						}
						memcpy(&v0, &pmi0->rposLTopview, sizeof(cr_vector_t));

#define MINDISTMAX	1.0
						mindist = MINDISTMAX;
						mindistindex = 0;
						for (i1 = 0; i1 < MARKCOUNT; i1++) {
							cr_vector_t vi;
							pmi1 = &pbmi1->markinfo[i1];
							if (pmi0->kind != pmi1->kind) {
								continue;
							}
							memcpy(&v2, &pmi1->rposLTopview, sizeof(cr_vector_t));
							memcpy(&vi, &v0, sizeof(cr_vector_t));
						
                            for (i2 = 0; i2 < rotjump; i2++) {
								rotateV(pmkr0->rrma, &vi, &v2_);
								memcpy(&vi, &v2_, sizeof(cr_vector_t));
							}
							dist = cr_vector_distance(&v2, &v2_);
							dist0 = cr_vector_distance(&v0, &v2_);

#define MINDIST_TOOSMALL			0.001 // 0.0005
                            if (mindist > dist) {
								mindist = dist;
								mindistindex = i1;
							}

							if (maxdist < dist0) {
								maxdist = dist0;
							}
							
                            if (dist0 < MINDIST_TOOSMALL) {
								goodcounttoosmall++;
							}
						}

						double mindist_good1;

#define MINDIST_GOOD1	0.003 // 0.001, 0.004, 0.005, 0.002
//#define MINDIST_GOOD1_CONSOLE1	0.002, 0.007, 0.006, 0.005
#define MINDIST_GOOD1_CONSOLE1	MINDIST_GOOD1
						if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
							mindist_good1 = MINDIST_GOOD1_CONSOLE1;
						} else {
							mindist_good1 = MINDIST_GOOD1;
						}

						//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] rotid: %d, <%4d:%4d> mindist: %lf, goodcount: %d\n", camid, rotid,i, i0, mindist, goodcount); 
						if (mindist < mindist_good1) {
							goodcount++;
							pmkr0->derrN += mindist;
							pbestmi->bestindex[i][i0] = mindistindex;
							//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-----> [%d] rotid: %d, <%4d: %4d -> %4d> mindist: %lf, goodcount: %d\n", camid, rotid, i, i0, mindistindex, mindist, goodcount); 
						}
					}
				}

#define MAXDIST 0.01 // 1
				if (maxdist > MAXDIST) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "maxdist:%lf.  goodcount: %d -> %d \n",
						maxdist, goodcount, goodcount - goodcounttoosmall);
					if (goodcount >= goodcounttoosmall) {
						goodcount = goodcount - goodcounttoosmall;
					} else {
						goodcount = 0;
					}
				}

				if (goodcount > 0) {
					double derrNmean;
					double totalrpm;
					double backspin;
					double sidespin;

					pmkr0->goodcount = goodcount;
					totalrpm = (pmkr0->angle / 360.) * piana->framerate * 60;
					derrNmean = pmkr0->derrN / goodcount;
					backspin = pmkr0->axis.x * totalrpm;
					sidespin = -pmkr0->axis.z * totalrpm;
				}
			}
		} 	// for (rotid = 0; rotid < MARKSEQUENCELEN-1; rotid++) 

		{
			U32 maxgoodcount;
			U32 maxgoodcountid;
			double minderrN;

			iana_shotresult_t *psr;
			cr_vector_t axis;
			double totalrpm;
			double backspin;
			double sidespin;

			//----------
			axis.x = 1; axis.y = 0; axis.z = 0;
			totalrpm = 500;
			backspin = 500; sidespin = 0;

			psr = &piana->shotresult[camid];
			//--
			maxgoodcount = 0;
			maxgoodcountid = (U32)-1;
			minderrN = 1;

			for (rotid = 0; rotid < marksequencelen - 1; rotid++) {
				pmkr0 = &pic->mkrSeqBest[rotid];
				if (pmkr0->valid) {
					if (maxgoodcount < pmkr0->goodcount) {
						maxgoodcount = pmkr0->goodcount;
						maxgoodcountid = rotid;
						minderrN = pmkr0->derrN;
					} else if (maxgoodcount == pmkr0->goodcount) {
						if (minderrN > pmkr0->derrN) {
							maxgoodcountid = rotid;
							minderrN = pmkr0->derrN;
						}
					}
				}
			}

#define MAXGOODCOUNT_MIN	2 // 8, 4, 3
			psr->spinAssurance = 0.20;

			if (maxgoodcount > MARKCOUNT) {
				maxgoodcount = MARKCOUNT;
			}

			if (maxgoodcount >= MAXGOODCOUNT_MIN) {					// Good!  Re-estimate rotation matrix and axis, angle.
				U32 success;

				cr_vector_t *pv0, *pv1;
				U32 vcount;

				//--
				pv0 = (cr_vector_t *)malloc(sizeof(cr_vector_t) * maxgoodcount);
				pv1 = (cr_vector_t *)malloc(sizeof(cr_vector_t) * maxgoodcount);

				pmkr0 = &pic->mkrSeqBest[maxgoodcountid];

				{
					psr->spinAssurance = 0.85;
					memcpy(&axis, &pmkr0->axis, sizeof(cr_vector_t));
					totalrpm = (pmkr0->angle / 360.) * piana->framerate * 60;
					backspin = pmkr0->axis.x * totalrpm;
					sidespin = -pmkr0->axis.z * totalrpm;
				}

				memcpy(&pic->mkrBest, pmkr0, sizeof(markrotation_t));
				memcpy(&pic->matchIndexbest, &pic->matchIndexSeqBest[maxgoodcountid], sizeof(bestmatchindex_t));

				pbestmi = &pic->matchIndexbest;
				vcount = 0;
				for (rotid = 0; rotid < marksequencelen - rotjump; rotid++) {
					pbmi0 = &pic->bmi[rotid];
					pbmi1 = &pic->bmi[rotid + rotjump];		// Next sequence.. 
					if (pbmi0->valid && pbmi1->valid) {
						for (i = 0; i < MARKCOUNT; i++) {
							I32 inext;		// next index..

							if (vcount >= maxgoodcount) {
								break;
							}

							inext = pbestmi->bestindex[rotid][i];

							if (inext >= 0 && inext < MARKCOUNT) {
								pmi0 = &pbmi0->markinfo[i];
								pmi1 = &pbmi1->markinfo[inext];
								if (pmi0->kind != MARK_KIND_EMPTY && (pmi0->kind == pmi1->kind)) {

									memcpy(pv0 + vcount, &pmi0->rposLTopview, sizeof(cr_vector_t));
									memcpy(pv1 + vcount, &pmi1->rposLTopview, sizeof(cr_vector_t));
									vcount++;
									if (vcount >= maxgoodcount) {
										break;
									}
								}
							}
						}
						if (vcount >= maxgoodcount) {
							break;
						}
					}
				}
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "maxgoodcountid: %d, maxgoodcount: %d, vcount: %d\n",
					maxgoodcountid,
					maxgoodcount, vcount);
				success = 0;
			
                if (vcount >= MAXGOODCOUNT_MIN) {
					double det;
					double value;
					double rrma2[3][3];
					double theta, angle;

                    //---------
					// 1) Re-estimate Rotation matrix, using Pseudo inverse
					success = pseudoinv3N(pv0, pv1, rrma2, vcount);

					if (success) {
						// 2) Normalize determinant to 1.
						det = mat3det(rrma2);
						if (det > DET_TOOSMALL) {

#define DET_3_		(0.999)
#define DET_TRY		10
							for (i = 0; i < DET_TRY; i++) {
								value = pow(det, -(1.0 / 3.0));
								
                                if (det > DET_3_) {
									value = value * DET_3_;
								}
								
                                mat3scalar(rrma2, value, rrma2);
								det = mat3det(rrma2);
								
                                if (det < DET_3_) {
									break;
								}
							}
						}

						if (det > DET_TOOSMALL) {
                            success = rodriguesMatrixInv(rrma2, &axis, &theta);
						
                            if (success) {
								theta /= rotjump;
								angle = RADIAN2DEGREE(theta);

								rodriguesMatrix(&axis, theta, rrma2);

								totalrpm = (angle / 360.) * piana->framerate * 60;
								backspin = axis.x * totalrpm;
								sidespin = -axis.z * totalrpm;

								//-- Update mkrBest..
								memcpy(&pic->mkrBest.axis, &axis, sizeof(cr_vector_t));
								pic->mkrBest.angle = angle;
								memcpy(&pic->mkrBest.rrma[0][0], &rrma2[0][0], sizeof(double) * (3 * 3));
							}
						} else {
							success = 0;
						}
					} else {
						success = 0;
					}
				}
				
				if (success) {
					double spinassurance;
#define ASRMAXGOODCOUNT7	0.95
#define ASRMAXGOODCOUNT5	0.90
#define ASRMAXGOODCOUNT4	0.86
#define ASRMAXGOODCOUNT3	0.80
					if (maxgoodcount >= 7) {
						spinassurance = ASRMAXGOODCOUNT7;
					} else if (maxgoodcount >= 5) {
						spinassurance = ASRMAXGOODCOUNT5;
					} else if (maxgoodcount >= 4) {
						spinassurance = ASRMAXGOODCOUNT4;
					} else {
						spinassurance = ASRMAXGOODCOUNT3;
					}

					if (camid == 0) {
						psr->spinAssurance = spinassurance;		// FAKE.. for Center CAM
					} else {
						psr->spinAssurance = spinassurance - 0.02;	// FAKE.. for Side Cam.
					}

				} else {
					psr->spinAssurance = 0.20;
				}

				free(pv0); free(pv1);
			} else { 	// if (maxgoodcount > 0)
				psr->spinAssurance = 0.10;
				pic->mkrBest.valid = 0;
			}

			if (psr->spinAssurance < ASRMAXGOODCOUNT3) {
#define FAKE_SIDESPIN	222
#define FAKE_BACKSPIN	888
				backspin = FAKE_BACKSPIN; sidespin = FAKE_SIDESPIN;

				axis.x = backspin;
				axis.y = 0;
				axis.z = -sidespin;
				totalrpm = cr_vector_normalization(&axis, &axis);

				piana->shotresultcheckme = SHOTCHECKME_YES;
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " Checkme. 888 spin\n");
			} else {
				if (piana->camsensor_category == CAMSENSOR_Z3 || piana->camsensor_category == CAMSENSOR_EYEXO) {
					refineSpin(&axis, totalrpm, &backspin, &sidespin);

				}
			}

			//-- Store shot result..
			memcpy(&psr->axis, &axis, sizeof(cr_vector_t));
			psr->spinmag = totalrpm;
			psr->backspin = backspin;
			psr->sidespin = sidespin;
		}

		{	// Rotate mark vector.
			I32 i, j;
			ballmarkinfo_t	*pbmi;
			markinfo_t	    *pmi;
			for (i = 0; i < (I32)marksequencelen - 1; i++) {
				pbmi = &pic->bmi[i];
				if (pbmi->valid) {
					for (j = 0; j < (I32)pbmi->markcount; j++) {
						pmi = &pbmi->markinfo[j];
						rotateV(pic->mkrBest.rrma, &pmi->rposLTopview, &pmi->rposLTopviewNext);
						rotateV(pic->mkrBest.rrma, &pmi->Npos3DwrtBall, &pmi->Npos3DwrtBallNext);
					}
				}
			}
		}
	}

	res = 1;

	return res;
}
#endif



/*!
********************************************************************************
*	@brief      Check spin ..
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2017/0532
*******************************************************************************/
I32 iana_spin_mark_spin_check(
	iana_t *piana)
{
	I32 res;
	U32 camid;

	res = 1;
	for (camid = 0; camid < 2; camid++) {
		if (piana->processmode[camid] == CAMPROCESSMODE_BALLMARK) {
			res = markSpincheck(piana, camid);
		}
	}

	return res;
}


/*!
********************************************************************************
*	@brief      Check spin ..
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/0532
*******************************************************************************/
I32 markSpincheck(
	iana_t *piana,
	U32 camid)
{
	I32 res;
	I32 i, j;
	I32 count;
	point_t Pp, Lp;

    U08 *pimg;
	U08 *pimg2;
	I32 x, y;
	I32 x0, y0;
	I32 startx, starty;

	iana_cam_t	*pic;
	markinfo_t		*pmi;
	ballmarkinfo_t		*pbmi;
	ballmarkinfo_t		*pbmi0;
	marksequence_t		*pmks;

	int len = PATHBUFLEN;
	char dirpath[PATHBUFLEN];
	char filename[PATHBUFLEN];

	int width;
	int height;

	int imagefiletype;					// 0: JPEG, 1: BMP


	cv::Size roisize;
	U32 imgoffset;
	I32 sx, sy;
	cv::Size roisizeT;
	U32 imgoffsetT;
	I32 sxT, syT;
	U32 widthT, heightT;
	U08 *pimgT;
	I32 ipr_;

	U32 displaycount;

	U32 imagedata;
	U32 drawspinline;

	U32 marksequencelen;

	//-------------

	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}

#define RECTSIZE		3 // 2
#define RECTSIZENEXT	2 // 4
#define RECTSIZE0		1
	imagefiletype = 0; 					// 0: JPEG, 1: BMP
	width = WIDTHI;
	height = HEIGHTI;
	widthT = height;
	heightT = width;

	WideCharToMultiByte(CP_ACP, 0, (LPCWSTR)piana->szImageDir, len, dirpath, len, NULL, NULL);

	pimg = (U08 *)malloc(WIDTHI * HEIGHTI * 3);
	pimg2 = (U08 *)malloc(WIDTHI * HEIGHTI * 3);
	pimgT = (U08 *)malloc(WIDTHI * HEIGHTI * 3);

	pic = piana->pic[camid];
	pmks = &pic->mks;
	Lp.x = Lp.y = 0;

	memset(pimg2, 0, WIDTHI * HEIGHTI * 3);

	displaycount = 0;

	imagedata = 1;
	nxmlie(1, "<camera id=\"%d\">", camid);

#define ASRMAXGOODCOUNT5	0.90
	if (piana->shotresultdata.spinAssurance > ASRMAXGOODCOUNT5) {
		drawspinline = 1;
	} else {
		drawspinline = 0;
	}

	for (i = 1; i < (I32)marksequencelen; i++) {
		pbmi0 = &pic->bmi[i - 1];					// Previous frame..
		pbmi = &pic->bmi[i];

#define DISPLAYCOUNT	100 // 10
		if (displaycount > DISPLAYCOUNT) {
			continue;
		}

#pragma warning(default:4996)

#define USERAWIMAGE
		if (pic->bmimg[i].valid) {
			double multfact;
			double multfact4;

			startx = pic->bmimg[i].startx;
			starty = pic->bmimg[i].starty;

			multfact = pic->bmimg[i].multfact;

			multfact4 = multfact;
#define MINMULTFACT 1.0
#define MAXMULTFACT 10.0
			if (multfact < MINMULTFACT) {
				multfact = MINMULTFACT;
			}

			if (multfact > MAXMULTFACT) {
				multfact = MAXMULTFACT;
			}

			for (j = 0; j < WIDTHI * HEIGHTI; j++) {
				U08 c;
				c = *(pic->bmimg[i].pimgRaw + j);
				*(pimg + j * 3 + 0) = c;			// R
				*(pimg + j * 3 + 1) = c;			// G
				*(pimg + j * 3 + 2) = c;			// B
			}

			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pic->bmimg[i].pimgRaw, WIDTHI, HEIGHTI, 0, 1, 2, 3, piana->imguserparam);
			}

			if (pbmi0->valid) {
				count = pbmi0->markcount;
				for (j = 0; j < count; j++) {		// Mark count in Ball image
					pmi = &pbmi0->markinfo[j];
					ballmarkLP(
						piana,
						camid,
						&pic->icp.CamPosL,			// I
						&pmks->ballposL3D[i], 		// I,	Ball position. current frame.
						&pmi->Npos3DwrtBallNext, 	// I,	Mark 3D position w.r.t. Ball. Previous frame
						&Lp,			// Mark L position
						&Pp			// Mark P position
					);

                    x = (I32)((Pp.x - startx + 0.0) * multfact4);
					y = (I32)((Pp.y - starty + 0.0) * multfact4);
						
                    // Spin rotation line.
					if (x < (RECTSIZE + 1) || 
                        x >= WIDTHI - (RECTSIZE + 1) || 
                        y < (RECTSIZE + 1) || 
                        y > HEIGHTI - (RECTSIZE + 1)) {
						continue;
					}

					ballmarkLP(
						piana, 				// I
						camid,
						&pic->icp.CamPosL,			// I
						&pmks->ballposL3D[i], 		// I,	Ball position. current frame.
						&pmi->Npos3DwrtBall, 	// I,	Mark 3D position w.r.t. Ball. Previous frame
						&Lp,			// Mark L position
						&Pp			// Mark P position
					);

                    x0 = (I32)((Pp.x - startx + 0.0) * multfact4);
					y0 = (I32)((Pp.y - starty + 0.0) * multfact4);
						
                    if (x < (RECTSIZE + 1) || 
                        x >= WIDTHI - (RECTSIZE + 1) || 
                        y < (RECTSIZE + 1) || 
                        y > HEIGHTI - (RECTSIZE + 1)) {
						continue;
					}

					myrectBGR(x - RECTSIZENEXT, y - RECTSIZENEXT, x + RECTSIZENEXT, y + RECTSIZENEXT, MKBGR(0x00, 0x00, 0xFF), pimg, width /*WIDTHI*/);			// B G R
					myrectBGR(x0 - RECTSIZE0, y0 - RECTSIZE0, x0 + RECTSIZE0, y0 + RECTSIZE0, MKBGR(0x00, 0xFF, 0xFF), pimg, width /*WIDTHI*/);
					if (drawspinline) {
						mylineBGR(x0, y0, x, y, MKBGR(0x00, 0x00, 0xFF), pimg, width /*WIDTHI*/);
					}
						
                    mycircleBGR4(x, y, 2, MKBGR(0x00, 0x00, 0xFF), pimg, width /*WIDTHI*/);			// B G R
					mycircleBGR4(x0, y0, 2, MKBGR(0x00, 0xFF, 0xFF), pimg, width /*WIDTHI*/);			// B G R
				}

				{
					count = pbmi->markcount;


					for (j = 0; j < count; j++) {		// Mark count in Ball image
						pmi = &pbmi->markinfo[j];
						Pp.x = pmi->NposP.x; Pp.y = pmi->NposP.y;

						x = (I32)((Pp.x - startx + 0.0) * multfact4);
						y = (I32)((Pp.y - starty + 0.0) * multfact4);
						myrectBGR(x - RECTSIZE, y - RECTSIZE, x + RECTSIZE, y + RECTSIZE, MKBGR(0xFF, 0xFF, 0x00), pimg, width /*WIDTHI*/);

						if (piana->himgfunc) {
							((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimg, WIDTHI, HEIGHTI, 0, 4, 2, 3, piana->imguserparam);
						}
					} 	// for (j = 0; j < count; j++) 		// Mark count in Ball image
				}
			} 	// if (pbmi0->valid) 

#define BALLPOS2VALID_LYCHECK		0.75
			if (Lp.y < BALLPOS2VALID_LYCHECK) {
				if (piana->himgfunc) {
					//((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimg, WIDTHI, HEIGHTI, 0, 1, 2, 3, piana->imguserparam);
					((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimg, WIDTHI, HEIGHTI, 0, 4, 2, 3, piana->imguserparam);
				}
			}

			{	// Rotate = Transpose + Vmirror
				//--

				ipr_ = (I32)(pic->bmimg[i].pr_ * multfact);
			
                if (ipr_ >= 128) ipr_ = 127;

				roisize.width = ipr_ * 2;
				roisize.height = ipr_ * 2;

				sx = 0;
				sy = 0;

                imgoffset = (sx + sy * WIDTHI) * 3;
				widthT = HEIGHTI;
				heightT = WIDTHI;

				roisizeT.width = ipr_ * 2;
				roisizeT.height = ipr_ * 2;
				sxT = (widthT - ipr_ * 2) / 2;
				syT = (heightT - ipr_ * 2) / 2;
				imgoffsetT = (sxT + syT * widthT) * 3;

				memset(pimgT, 0, widthT * heightT * sizeof(U08) * 3);
                
                {
                    cv::Mat cvImg(cv::Size(width, height), CV_8UC3, pimg), cvImgCrop, cvImgCropT, cvImgCropMirror, cvImgT(cv::Size(height, width), CV_8UC3);

                    cvImgCrop = cvImg(cv::Rect(sx, sy, roisize.width, roisize.height));
                    cv::transpose(cvImgCrop, cvImgCropT);
                    cv::flip(cvImgCropT, cvImgCropMirror, 0);
                    cvImgT = 0;
                    cvImgCropMirror.copyTo(cvImgT(cv::Rect(sxT, syT, roisize.width, roisize.height)));
				    memcpy(pimgT, cvImgT.data, sizeof(U08) * width * height * 3);
                }

				{
					int rrr;
					x = (I32)widthT / 2 - 1;
					y = (I32)heightT / 2 - 1;
					rrr = ipr_ + 1;
					mycircleBGR3(x, y, rrr, rrr + 4, MKBGR(0, 0, 0), pimgT, widthT /*WIDTHI*/);
					mycircleBGR(x, y, rrr, MKBGR(0x80, 0xA0, 0xA0), pimgT, widthT /*WIDTHI*/);
				}
			}

            {
				I32 m;
				I32 sxm, sym;

				sxm = (widthT - WIDTHM) / 2;
				sym = (heightT - HEIGHTM) / 2;

				for (m = 0; m < HEIGHTM; m++) {
					memcpy(
						(pimg + (0 + (m + 0)*WIDTHM) * 3),
						(pimgT + (sxm + (m + sym)*widthT) * 3),
						WIDTHM * 3);
				}
			}

			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimg, WIDTHM, HEIGHTM, 2, 4, 2, 3, piana->imguserparam);
			}

			if (imagedata == 1) {
				nxmlie(1, "<imagedata>");

                {
					U64 ts64shot;
					iana_cam_param_t *picp;
					iana_shotresult_t *pcsr;;

					picp = &pic->icp;
					pcsr = &piana->clubball_shotresult[camid];

					ts64shot = pic->ts64shot;

					nxmlie(0, "<processmode>%d</processmode>", piana->processmode[camid]);
					nxmlie(0, "<width>%d</width>", WIDTHM);
					nxmlie(0, "<height>%d</height>", HEIGHTM);
					nxmlie(0, "<shotts64>%llu</shotts64>", ts64shot);
					nxmlie(0, "<ballposition>%d %d</ballposition>", 0, 0);
					nxmlie(0, "<balldirection>%10.6lf</balldirection>", 0);
					nxmlie(0, "<clubdirection>%10.6lf</clubdirection>", 0);


				}
				nxmlie(-1, "</imagedata>");
				imagedata = 0;
				nxmlie(1, "<images>");
			}

#pragma warning(disable:4996)
			if (imagefiletype == 0) 					// 0: JPEG, 1: BMP
			{
				sprintf(filename, "m%d_%04d.jpg", camid, displaycount);
			} else {		// 1: BMP
				sprintf(filename, "m%d_%04d.bmp", camid, displaycount);
			}

			nxmlie(1, "<image>");
			
            {
				nxmlie(0, "<filename>%s</filename>", filename);
			}
			nxmlie(-1, "</image>");

			if (imagefiletype == 0) 					// 0: JPEG, 1: BMP
			{

#define MARKFILEQVALUE0		100 // 90, 95
				saveimageJpegBGR(piana->hjpeg,
					MARKFILEQVALUE0, //MARKFILEQVALUE,
					dirpath,
					filename,
					(char *)pimg,
					WIDTHM,
					HEIGHTM);
			} else {

				saveimageBGR(
					dirpath,
					filename,
					(char *)pimg,
					WIDTHM,
					HEIGHTM);
			}
			displaycount++;
		}
	} 	// for (i = 0; i < MARKSEQUENCELEN; i++) 

	free(pimgT);

	if (piana->himgfunc) {
		((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimg2, WIDTHI, HEIGHTI, 2, 4, 2, 3, piana->imguserparam);
	}

	free(pimg2);
	free(pimg);
	res = 1;

	nxmlie(-1, "</images>");
	nxmlie(-1, "</camera>");
	return res;

}

/*!
********************************************************************************
*	@brief      Make simul data
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2017/0711
*******************************************************************************/
I32 iana_spin_mark_make_simuldata(iana_t *piana)
{
	I32 res;

	U32 camid;
	U32 i, j;
	iana_cam_t	*pic;

	cr_vector_t axis;
	double	spinmag;
	double theta;
	double rrm[3][3];

	ballmarkinfo_t 	*pbmiTest;
	markinfo_t		*pmi;
	cr_point_t	simuldata[MARKCOUNT];

    U32 markcount;
	U32 marksequencelen;

	//----
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}

	memcpy(&axis, &piana->simul_axis, sizeof(cr_vector_t));
	spinmag = piana->simul_spinmag;		// RPM
	theta = spinmag * (1.0 / 60) * (2 * M_PI) / piana->framerate;

	make_simuldata(&simuldata[0], MARKCOUNT, &markcount);

	for (camid = 0; camid < 2; camid++) {
		pic = piana->pic[camid];

		//-- Make Simultion points
		memset(&pic->bmiTest[0], 0, sizeof(ballmarkinfo_t) * MARKSEQUENCELEN);
		for (i = 0; i < marksequencelen; i++) {
			pbmiTest = &pic->bmiTest[i];
			pbmiTest->markcount = markcount;
			rodriguesMatrix(&axis, (theta * i), rrm);			// ith..
			for (j = 0; j < markcount; j++) {
				pmi = &pbmiTest->markinfo[j];
				rodriguesRotate(rrm, &simuldata[j], &pmi->Npos3DwrtBall);
			}
		}
	}

	res = 1;
	return res;
}

I32 make_simuldata(cr_point_t *ppt, U32 maxcount, U32 *pmarkcount)
{
	U32 count;
	I32 i, j, k;
	double r = BALLRADIUS;
	double ir2, ir3;
	I32 res;

	//---
	count = 0;
	ir2 = 1.0 / sqrt(2.0);
	ir3 = 1.0 / sqrt(3.0);
	//1) (+-1, 0, 0), (0,+-1, 0), (0, 0, +-1)
	for (i = -1; i <= 1; i += 2) {
		ppt[count + 0].x = i * r; ppt[count + 0].y = 0; ppt[count + 0].z = 0;
		ppt[count + 1].x = 0; ppt[count + 1].y = i * r; ppt[count + 1].z = 0;
		ppt[count + 2].x = 0; ppt[count + 2].y = 0; ppt[count + 2].z = i * r;
		count += 3;
	}

	//2) (+-1/r(2), +-1/r(2), 0), (+-1/r(2), 0, +-1/r(2), 0) (0, +-1/r(2), +-1/r(2))
	for (i = -1; i <= 1; i += 2) {
		for (j = -1; j <= 1; j += 2) {
			ppt[count + 0].x = i * ir2; ppt[count + 0].y = j * ir2; ppt[count + 0].z = 0;
			ppt[count + 1].x = i * ir2; ppt[count + 1].y = 0; ppt[count + 1].z = j * ir2;
			ppt[count + 2].x = 0; ppt[count + 2].y = i * ir2; ppt[count + 2].z = j * ir2;
			count += 3;
		}
	}

	//3) (+-1/r(3), +-1/r(3), +-1/r(3))
	for (i = -1; i <= 1; i += 2) {
		for (j = -1; j <= 1; j += 2) {
			for (k = -1; k <= 1; k += 2) {
				ppt[count + 0].x = i * ir3; ppt[count + 0].y = j * ir3; ppt[count + 0].z = k * ir3;
				count++;
			}
		}
	}

	*pmarkcount = count;

	maxcount;
	res = 1;
	return res;
}

/*!
********************************************************************************
*	@brief      Make Reference Image
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2017/0910
*******************************************************************************/
I32 mkrefimage(iana_t *piana, U32 camid)
{
	I32		res;
	I32		i;

	iana_cam_t			*pic;
	camimageinfo_t     	*pcii;
	camif_buffer_info_t *pb;
	U32		width;
	U32		height;
	U32 	multitude;

	U32		sumbufinited;
	U32		sumbufcount;

    cv::Size bufSize, sumbufSize;
    cv::Mat cvSumbuf16, cvSumbuf;
	//---
	sumbufinited = 0;
	sumbufcount = 0;

	pic = piana->pic[camid];
	res = 1;

#define REFIMAGECOUNT	10
	for (i = 0; i < REFIMAGECOUNT; i++) {
		I32		rindex0;

		U32		offset_x;
		U32		offset_y;
		U08 	*buf;
		U32 	rindex;

#define BEFORESHOTCOUNT	5
		rindex0 = (I32)pic->rindexshot + i - BEFORESHOTCOUNT;
		res = scamif_imagebuf_trimindex2(piana->hscamif, camid, NORMALBULK_NORMAL, rindex0, &rindex);
		res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_NORMAL, rindex, &pb, &buf);
		if (res == 0 || buf == NULL || pb == NULL) {
			continue;
		}

		pcii = &pb->cii;
		width = pcii->width;
		height = pcii->height;
		offset_x = pcii->offset_x;
		offset_y = pcii->offset_y;
		multitude = pcii->multitude;
        
        bufSize.width = pcii->width;
        bufSize.height= pcii->height;

		if (sumbufinited == 0) {
            cvSumbuf16 = cv::Mat(bufSize, CV_16UC1);
            cvSumbuf16 = 0;

            sumbufSize.width = width;
            sumbufSize.height = height;

			sumbufinited = 1;
			sumbufcount = 0;
		}

		if (res) {
			if (width == (U32)sumbufSize.width && height == (U32)sumbufSize.height) {
                cv::Mat cvCurBuf(bufSize, CV_8UC1, buf), cvCurBuf16;
                cvCurBuf.convertTo(cvCurBuf16, CV_16UC1);

#define SUMBUFCOUNTCALCCOUNT	2
				if (sumbufcount < SUMBUFCOUNTCALCCOUNT) {
                    cvSumbuf16 += cvCurBuf16;
					sumbufcount++;
				} else {
					break;
				}
			}
		}
	}

	if (sumbufcount > 0) {
        cvSumbuf16 /= sumbufcount;
        cvSumbuf16 .convertTo(cvSumbuf, CV_8UC1);
        memcpy(pic->prefimg, cvSumbuf.data, sizeof(U08) * cvSumbuf.cols * cvSumbuf.rows);
	}

	return res;
}

double calcimageradius(iana_t *piana, U32 camid, U32 seqnum)
{
	double ldist;
	cr_point_t b3d;
	cr_point_t b3d3;
	point_t L1;
	point_t P1, P2;
	double dx, dy, dz;
	double ratio;

	marksequence_t		*pmks;
	iana_cam_t	*pic;

	//-----------------------------
	pic = piana->pic[camid];
	pmks = &pic->mks;

	// Ball P pos
	L1.x = pmks->ballposL[seqnum].x;
	L1.y = pmks->ballposL[seqnum].y;
	iana_L2P(piana, camid, &L1, &P1, 0);
	
    // get 3d position
	iana_P2L3D_EP(piana, camid,
		pmks->cam2ball[seqnum],			// Camera to Ball distance.
		&P1, &b3d);

#define DLEN0	30.0
	P2.x = P1.x + DLEN0;
	P2.y = P1.y;

	iana_P2L3D_EP(piana, camid,
		pmks->cam2ball[seqnum],			// Camera to Ball distance.
		&P2, &b3d3);

	dx = b3d3.x - b3d.x;
	dy = b3d3.y - b3d.y;
	dz = b3d3.z - b3d.z;

	ldist = sqrt(dx*dx + dy * dy + dz * dz);

	ratio = BALLRADIUS / ldist;

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " 3) dx dy dz %lf %lf %lf     ldist: %lf (ratio: %lf)\n", dx, dy, dz, ldist, ratio);

	ldist = DLEN0 * ratio;

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " Image distance: %lf (vs DLEN0: %lf)\n", ldist, DLEN0);

	return ldist;
}

U32 refineSpin(cr_vector_t *paxis, double spinmag, double *pbackspin, double *psidespin)
{
	// 20190406, match to GCQ. 
	cr_vector_t axis;

	//---
	memcpy(&axis, paxis, sizeof(cr_vector_t));

#define ADDZ_DEG	(2.0)
#define ADDZ_RADIAN (DEGREE2RADIAN(ADDZ_DEG))
	axis.z = axis.z + ADDZ_RADIAN;
	cr_vector_normalization(&axis, paxis);

	*psidespin = -paxis->z * spinmag;
	*pbackspin = paxis->x * spinmag;

	return 1;
}

#ifdef __cplusplus
}
#endif

//static int s_msD = 1;
static int s_msD = 2;
I32 iana_spin_mark(iana_t *piana, vector<coord_t> &ftseqs,
	vector<size_t> &ftcnts) {
	I32 res;
	iana_cam_t *pic;
	U32 camid;

	U32 rindex;

#if defined(REQUESTBULK_HERE)
	U64 ts64[2];
	I64 ts64tmp;
#endif
	U32 camidlist[2];

	U32 i;
	iana_setup_t *pisetup;
	iana_cam_setup_t *picsetup;

	U32 marksequencelen;

	//------------------------------------------
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}

	pisetup = &piana->isetup;
	res = 0;

#if defined(REQUESTBULK_HERE)
	ts64[0] = ts64[1] = 0;
	
    if (piana->opmode == IANA_OPMODE_FILE) {
		//
	} else {
		iana_requestBulk_TimeStamp(piana, &ts64[0], &ts64[1]);
		
        ts64tmp = (I64)ts64[0];
		ts64tmp += pisetup->icsetup[0].bulk_preshot_additional_time;

		ts64[0] = (U64)ts64tmp;
		ts64tmp = (I64)ts64[1];
		
        ts64tmp += pisetup->icsetup[1].bulk_preshot_additional_time;
		ts64[1] = (U64)ts64tmp;
	}

	//---
	scamif_stop(piana->hscamif);

#endif

	{		// Read spin mode
#pragma warning(disable:4996)
		int len = PATHBUFLEN;
		char filename[PATHBUFLEN];
		char pathstr[PATHBUFLEN];
		char drivestr[PATHBUFLEN];

		FILE *fp;

		int mode;					// Simul mode
		int datamode;				// 0: Use backspin, sidespin, Rollpin,    1: Use axis and spin magnitude 

		cr_vector_t simul_axis;
		double	simul_spinmag;

		double simul_backspin;		// Roll, THETA_x
		double simul_sidespin;		// -Yaw, -THETA_z
		double simul_rollspin;		// Roll, THETA_y

		//--
#define SPINSIMULFILE		"spinsimul.txt"
		WideCharToMultiByte(CP_ACP, 0, (LPCWSTR)piana->szDrive, len, drivestr, len, NULL, NULL);
		WideCharToMultiByte(CP_ACP, 0, (LPCWSTR)piana->szDir, len, pathstr, len, NULL, NULL);
#pragma warning(disable:4996)
		sprintf(filename, "%s%s\\%s", drivestr, pathstr, SPINSIMULFILE);
		fp = fopen(filename, "r");
		mode = 0;
		if (fp) {
			fscanf(fp, "%d", &mode);
			fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
			if (mode == 1) {
				fscanf(fp, "%d", &datamode);
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

				fscanf(fp, "%lf", &simul_backspin);					// backspin
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

				fscanf(fp, "%lf", &simul_sidespin);					// sidespin
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

				fscanf(fp, "%lf", &simul_rollspin);				// rollspin
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

				fscanf(fp, "%lf", &simul_axis.x);					// ax
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

				fscanf(fp, "%lf", &simul_axis.y);					// ay
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

				fscanf(fp, "%lf", &simul_axis.z);					// az
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

				fscanf(fp, "%lf", &simul_spinmag);					// Spin magnitude
				fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

				if (datamode == 0) {				// use spin values
					simul_axis.x = simul_backspin;
					simul_axis.y = simul_rollspin;
					simul_axis.z = -simul_sidespin;
					simul_spinmag = cr_vector_normalization(&simul_axis, &simul_axis);
				}
				else if (datamode == 1) {
					cr_vector_normalization(&simul_axis, &simul_axis);
					simul_backspin = simul_axis.x * simul_spinmag;
					simul_rollspin = simul_axis.y * simul_spinmag;
					simul_sidespin = -simul_axis.z * simul_spinmag;
				}

				memcpy(&piana->simul_axis, &simul_axis, sizeof(cr_vector_t));
				piana->simul_spinmag = simul_spinmag;
				piana->simul_backspin = simul_backspin;
				piana->simul_sidespin = simul_sidespin;
				piana->simul_rollspin = simul_rollspin;
			}
			else {
				mode = 0;
			}
			fclose(fp);
		}
		piana->spinsimulmode = mode;
	}

	if (piana->spinsimulmode) {
		iana_spin_mark_make_simuldata(piana);
	}

	{
		I32 processmode0, processmode1;
		processmode0 = piana->processmode[0];
		processmode1 = piana->processmode[1];

		if ((processmode0 == CAMPROCESSMODE_BALLCLUB) && (processmode1 == CAMPROCESSMODE_BALLMARK)) {
			camidlist[0] = 1;
			camidlist[1] = 0;
		} else { 
			camidlist[0] = 0;
			camidlist[1] = 1;
		}
	}

	for (i = 0; i < 2; i++) {
		camid = camidlist[i];
		picsetup = &pisetup->icsetup[camid];

		pic = piana->pic[camid];

		memset(&pic->bmi[0], 0, sizeof(ballmarkinfo_t) * MARKSEQUENCELEN);

		pic->ballcount = 0;
		memset(&pic->vmag2[0], 0, sizeof(double) * MARKSEQUENCELEN);
		memset(&pic->TrajY[0], 0, sizeof(double) * MARKSEQUENCELEN);

#if defined(REQUESTBULK_HERE)
		if (piana->opmode == IANA_OPMODE_FILE) {
			// DO NOTHING..
		} else {
			U32 readframe;

			U64 ts64shot;
			U32		offset_x;
			U32		offset_y;

			I32 axx1024;
			I32 bxx1024;

			I32 ayx1024;
			I32 byx1024;

			iana_cam_param_t *picp;
			U32 bulkmult;
			iana_cam_t	*pic;

			//--
			pic = piana->pic[camid];
			offset_x = pic->offset_x;
			offset_y = pic->offset_y;

			readframe = marksequencelen;

			bulkmult = picsetup->mult_for_bulk;
			scamif_image_property_BULK(piana->hscamif, camid,
				picsetup->bulk_width,
				picsetup->bulk_height,
				offset_x, offset_y);


			scamif_imagebuf_init(piana->hscamif, camid, 2, NULL);

			ts64shot = piana->pic[camid]->ts64shot;
			picp = &piana->pic[camid]->icp;

			if (bulkmult == 1 || bulkmult == 0) {
				axx1024 = 0;
				bxx1024 = 0;

				ayx1024 = 0;
				byx1024 = 0;
			} else {
				axx1024 = (I32)(picp->mPxx_ * 1024);
				bxx1024 = (I32)(picp->bPxx_ * 1024);

				ayx1024 = (I32)(picp->mPyy_ * 1024);
				byx1024 = (I32)(picp->bPyy_ * 1024);

			}
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] Before BULK request, tsfrom: %lld(%llx), tsshot: %lld(%llx) zzz\n", camid,
				ts64[camid], ts64[camid],
				ts64shot, ts64shot);

			{
#pragma warning(disable:4996)
				FILE *fp;
				int mode;
				fp = fopen("bulkmult.txt", "r");
				if (fp) {
					fscanf(fp, "%d", &mode);
					fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'

					if (mode == 1) {
						fscanf(fp, "%d", &bulkmult);
						fscanf(fp, "%*[^\n]"); // SKIP to '\n' and '\r'
					}
					fclose(fp);
				}
#pragma warning(default:4996)
			}

			scamif_cam_BULK64(piana->hscamif,
				camid,   	// BULK transfer.. Center
				ts64[camid],
				readframe,
				0,			// wait time
				0, //1,			// continue after bulk
				bulkmult,	// mult for bulk
				1				// skip for bulk
				// --
				, ts64shot,
				axx1024,
				bxx1024,
				ayx1024,
				byx1024
			);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] After BULK request qqq\n", camid);
		}
#endif
		{
			U32 doRefineBallpos;
			U32 doneRefineBallpos;
			doRefineBallpos = 1;
			doneRefineBallpos = 0;
			
            if (piana->processmode[camid] == CAMPROCESSMODE_BALLCLUB) {
				doRefineBallpos = 0;
			}
			
            pic = piana->pic[camid];
			pic->firstbulkindex = (U32)-1;
			res = bulkFirstindex(piana, camid, &rindex);
			if (res == 1) {
				pic->firstbulkindex = rindex;
				if (doRefineBallpos) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] Before refineBallPos request\n", camid);
					res = IANA_SHOT_RefineBallPos_Bulk(piana, camid);
					doneRefineBallpos = 1;
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] After refineBallPos request\n", camid);
				} else { 
					mkrefimage(piana, camid);
				}
			}
		}
	}

#if defined(REQUESTBULK_HERE)
	if (*piana->pUseAttackCam) {
		camid = 2;
		pic = piana->pic[camid];
		if (piana->opmode == IANA_OPMODE_FILE) {
			// DO NOTHING..
		} else {
			U32 readframe;

			U64 	ts64shot, ts64from, preshot;
			U32		offset_x;
			U32		offset_y;

			I32 axx1024;
			I32 bxx1024;

			I32 ayx1024;
			I32 byx1024;

			iana_cam_param_t *picp;
			U32 bulkmult;
			iana_cam_t	*pic;

			//--
			picsetup = &pisetup->icsetup[camid];
			pic = piana->pic[camid];
			offset_x = pic->offset_x;
			offset_y = pic->offset_y;

			readframe = MARKSEQUENCELENATTCAM;

			bulkmult = picsetup->mult_for_bulk;
			scamif_image_property_BULK(piana->hscamifAtC, camid - 2, picsetup->bulk_width, picsetup->bulk_height, offset_x, offset_y);

			scamif_imagebuf_init(piana->hscamifAtC, camid - 2, 2, NULL);

			ts64shot = piana->pic[camid]->ts64shot;
			picp = &piana->pic[camid]->icp;

			axx1024 = 0;
			bxx1024 = 0;

			ayx1024 = 0;
			byx1024 = 0;

			preshot = picsetup->bulk_time_preshot;
			ts64from = ts64shot - preshot;

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] Before BULK request, tsfrom: %lld(%llx), tsshot: %lld(%llx) zzz\n", camid,
				ts64from, ts64from,
				ts64shot, ts64shot);
			
            scamif_cam_BULK64(piana->hscamifAtC,
				camid - 2,   	// BULK transfer.. Center
				ts64from,
				readframe,
				0,			// wait time
				0, //1,			// continue after bulk
				bulkmult,	// mult for bulk
				1				// skip for bulk
				// --
				, ts64shot,
				axx1024,
				bxx1024,
				ayx1024,
				byx1024
			);

			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] After BULK request qqq\n", camid);
		}
	}
#endif

	{
        IANA_SHOT_RefineBallPos_Bulk(piana, 0);
	}

	if (s_refineshot2) {
		res = PairingBall_Stereo(piana);
        if (res) {
			res = RefineShot_BulkStereo(piana);
		}
        if (res == 0) {
			res = RefineShot_ShotPlaneProjection(piana);				// -_-; 
		}
	} else {
		res = RefineShot_ShotPlaneProjection(piana);
	}


	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		// what? 
	} else {
		for (camid = 0; camid < 2; camid++) {
			I32 i;
			memcpy(&piana->shotresult[camid], &piana->shotresultdata, sizeof(iana_shotresult_t));
			pic = piana->pic[camid];

			for (i = 0; i < (I32)marksequencelen; i++) {
				pic->bmimg[i].valid = 0;
			}

			if (pic->firstbulkindex != (U32)-1) {
				cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]processmode = %d\n", camid, piana->processmode[camid]);
				if (piana->processmode[camid] == CAMPROCESSMODE_BALLMARK) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]markSeq\n", camid);

#define APPBALL_OTHERFUNC
#if defined(APPBALL_OTHERFUNC)
					res = apparentBallRadius(piana, camid);
					res = ballViewPlane(piana, camid, i);
#endif
					res = GetDotMarkSeq(piana, camid);
					if (piana->camsensor_category == CAMSENSOR_EYEXO) {

						if (s_msD == 2) {
							res = GetDimpleMarkSeq(piana, camid, ftseqs, ftcnts);
						} else {
							res = GetDimpleMarkSeq(piana, camid, ftseqs, ftcnts);
						}
					}
				}
			}
		}
	}


	if (piana->camsensor_category == CAMSENSOR_P3V2) {
		// what? 
	} else {
		for (camid = 0; camid < 2; camid++) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]Before markSpin\n", camid);
			markSpin(piana, camid);
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]After markSpin\n", camid);
		}
	}

	return res;
}


static int s_use_histogram_equalization = 0;
U32 GetDotMarkSeq(iana_t *piana, U32 camid)
{
	U32 res;

	iana_cam_t	*pic;
	iana_cam_param_t *picp;
	camimageinfo_t     *pcii;
	camif_buffer_info_t *pb;
	marksequence_t	*pmks;



	U64 	ts64;
	U64		ts64pb;
	cr_point_t PbPrev;

	double lr_;
	point_t bcP; 	// Ball center, pixel
	double pr_;
	double multfact;

	U32		width;
	U32		height;
	U32		offset_x;
	U32		offset_y;
	U08 	*buf;
	U32 	rindex;

	double startxD, startyD;
	I32 startx2, starty2;
	double startxD2, startyD2;

	U32 i, j;
	U08		*imgbufS;
	U08		*imgbufSRaw;
	U08		*imgbufS0;
	U08		*imgbufI;
	U08		*imgbufIRaw;


	cr_point_t Pb0;	// Ball center, Local
	cr_vector_t uw;	// Sight from CAM to ball. unit-vector
	cr_line_t   lw;	// line with uw and P_{b,0} : ball position, local coord.
	cr_point_t campos;	// Camera position

	U32    useProjPl;		// Use Projection Plane

	U32 marksequencelen;

	//-----
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}

	pic = piana->pic[camid];
	picp = &pic->icp;
	pmks = &pic->mks;

	imgbufS = (U08*)malloc(FULLWIDTH * FULLHEIGHT* MOREMORE);
	imgbufSRaw = (U08*)malloc(FULLWIDTH * FULLHEIGHT* MOREMORE);

	imgbufS0 = (U08*)malloc(FULLWIDTH * FULLHEIGHT);
	imgbufI = (U08*)malloc(WIDTHI * HEIGHTI* MOREMORE);
	imgbufIRaw = (U08*)malloc(WIDTHI * HEIGHTI* MOREMORE);
	
	if (piana->camsensor_category == CAMSENSOR_CONSOLE1) {
		useProjPl = 1;
	} else {
		useProjPl = 0;
	}

	if (s_useProjPl_flag == 0) {
		useProjPl = 0;
	}

	{
		point_t bcL1;	// Ball center, local
		point_t bcP1; 	// Ball center, pixel
		point_t bcL2;	// Ball center, local
		point_t bcP2; 	// Ball center, pixel
		ballmarkinfo_t		*pbmi;

        cv::Mat cvImgBuf;

		pbmi = &pic->bmi[0];

		res = 0;
		buf = NULL;
		pb = NULL;
		PbPrev.x = 0; PbPrev.y = 0; PbPrev.z = 0;
		ts64pb = 0;

		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]  PosL   PosL2\n", camid);

		for (i = 0; i < marksequencelen; i++) {
			if (pbmi[i].valid == 0) {
				continue;
			}

			bcL2.x = pmks->ballposL2[i].x;
			bcL2.y = pmks->ballposL2[i].y;
			bcP2.x = pmks->ballposP2[i].x;
			bcP2.y = pmks->ballposP2[i].y;

			bcL1.x = pmks->ballposL[i].x;
			bcL1.y = pmks->ballposL[i].y;
			bcP1.x = pmks->ballposP[i].x;
			bcP1.y = pmks->ballposP[i].y;


			//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "<%2d>  %10lf %10lf     %10lf %10lf\n", 
			//		i,
			//		bcL1.x,
			//		bcL1.y,
			//		bcL2.x,
			//		bcL2.y);

			rindex = pic->firstbulkindex + i;

#define BALLSTARTY_MARGIN	0.01
			if (bcL2.y < pic->icp.L.y + BALLSTARTY_MARGIN) { 
				continue;
			}

			// 1) get image buffer
			if (piana->opmode == IANA_OPMODE_FILE) {
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, rindex, &pb, &buf);
				if (res == 0) {
					break;
				}
			} else {
				for (j = 0; j < 20; j++) {
					res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, rindex, &pb, &buf);
					if (res == 1) {
						break;
					}
					Sleep(1);
				}
				
                if (res == 0) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", i, j);
					break;
					//continue;
				}
			}

			pcii = &pb->cii;
			width = pcii->width;
			height = pcii->height;
			offset_x = pcii->offset_x;
			offset_y = pcii->offset_y;

            cvImgBuf = cv::Mat(cv::Size(width, height), CV_8UC1, buf);

			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, width, height, 1, 1, 2, 3, piana->imguserparam);
			}

			ts64 = MAKEU64(pb->ts_h, pb->ts_l);
			
			//--------------------- 
			// 2) Ball center, radius. Brightness equalization
			{
				{ 	// real image processing
					//-----------------------------------------------------
					// NEWMARKPROCESSING [1]: Sight Vector from CAM to Ball
					//-----------------------------------------------------
					{
						iana_cam_t *pic;

						Pb0.x = bcL1.x;
						Pb0.y = bcL1.y;
						Pb0.z = 0.0;

						// Cam position
						pic = piana->pic[camid];
						memcpy(&campos, &pic->icp.CamPosL, sizeof(cr_point_t));
						cr_vector_sub(&Pb0, &campos, &uw);		// uw = Pb0 - campos
						cr_vector_normalization(&uw, &uw);
						cr_line_p0m(&campos, &uw, &lw);
					}

					lr_ = pmks->ballrL2[i];
					pr_ = pmks->ballrP2[i];

					if (piana->camsensor_category == CAMSENSOR_EYEXO) {
						multfact = (WIDTHS / pr_) * 0.95;
					} else {
						multfact = (128 / pr_) * 0.9;
					}

#define MAXMULTIFACT	3.5
					if (multfact > MAXMULTIFACT) {
						multfact = MAXMULTIFACT;
					}

					startxD = (bcP2.x - pr_) - offset_x;
					startyD = (bcP2.y - pr_) - offset_y;

#define MINPR_ 5
					if (pr_ < MINPR_) {
						continue;
					}

					if (startxD < 1 || startxD > (U32)(width - pr_) - 1) {
						continue;
					}

					if (startyD < 1 || startyD > (U32)(height - pr_) - 1) {
						continue;
					}

					startxD2 = startxD + offset_x;			// absolute coordinate
					startyD2 = startyD + offset_y;
					startx2 = (U32)startxD2; 
					starty2 = (U32)startyD2;
				}

				{ // Ball brightness equalization
					double cxP, cyP, crP;
					U32 doGammacorrection;

					//---
					cxP = (bcP2.x - startxD2);
					cyP = (bcP2.y - startyD2);
					crP = pr_;

					memset(imgbufS, 0, WIDTHS * HEIGHTS* MOREMORE);
					memset(imgbufSRaw, 0, WIDTHS * HEIGHTS* MOREMORE);

					{
                        // compute mean brigheness 1/sqrt2 of radius so that no background involve.
                        // the window shounld be square so, we should not see outside of 1/sqrt2 of radius.
                        cv::Mat sqrt2Ball;
                        double sqrt2R = sqrt(2.0) * crP;
                        cv::Rect sqrtBallRect;

                        cv::Scalar cvMean;

                        sqrtBallRect.x = (int)(cxP - sqrt2R/2);
                        sqrtBallRect.y = (int)(cyP - sqrt2R/2);
                        sqrtBallRect.width = sqrtBallRect.height = (int)sqrt2R;
                        
                        sqrt2Ball = cvImgBuf(sqrtBallRect);

                        cvMean = cv::mean(cvImgBuf);
                        
#define DOGAMMA_MEAN	200
						if (cvMean[0] < DOGAMMA_MEAN) {
							doGammacorrection = 1;
						} else { 
							doGammacorrection = 0;
						}
						//cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mean: %lf, stdev: %lf   doGamma: %d\n", mean, stdev, doGammacorrection);
					}

                    if (s_use_histogram_equalization) {
						if (doGammacorrection) {
							U08 *buf0;
							buf0 = (U08 *)malloc(width*height);

							gamma_correction(buf, buf0, width, height);
							histogram_equalization(buf0, imgbufS0, width, height);
							free(buf0);
						} else {
							histogram_equalization(buf, imgbufS0, width, height);
						}

						{
							res = EqualizeBrightness(piana, (U32)cxP, (U32)cyP, crP,
								width, height,
								imgbufS0 + (U32)(startxD + ((U32)startyD) * width),
								imgbufS,
								imgbufSRaw,
								camid);
						}
					} else {
						if (doGammacorrection) {
							gamma_correction(buf, imgbufS0, width, height);

							if (piana->himgfunc) {
								((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, width, height, 0, 1, 2, 3, piana->imguserparam);
								((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)imgbufS0, width, height, 1, 1, 2, 3, piana->imguserparam);
							}

                            {
								if (startxD < crP * WINDOWRATIO) {
									startxD = crP * WINDOWRATIO;
								}
							
                                if (startyD < crP * WINDOWRATIO) {
									startyD = crP * WINDOWRATIO;
								}
								
                                res = EqualizeBrightness(piana, (U32)cxP, (U32)cyP, crP,
									width, height,
									imgbufS0 + (U32)(startxD + ((U32)startyD) * width),
									imgbufS,
									imgbufSRaw,
									camid);
							}
						} else {
							res = EqualizeBrightness(piana, (U32)cxP, (U32)cyP, crP,
								width, height,
								buf + (U32)(startxD + ((U32)startyD) * width),
								imgbufS,
								imgbufSRaw,
								camid);
						}
					}
				}
			} 			// 2) Ball center, radius. Brightness equalization

			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, width, height, 2, 1, 2, 3, piana->imguserparam);
				((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)imgbufS, width, height, 2, 1, 2, 3, piana->imguserparam);
			}
			//--------------------- 
			//3)  Resize 4x4

			{			// 2017/12/07...    Crash removal..
                cv::Mat cvBufBefResize, cvBufAftResize,
                    cvBufRawBefResize, cvBufRawAftResize;
				cv::Size ssize;
				cv::Rect sroi, droi;
				double xfactI, yfactI;

				//------
				xfactI = multfact;
				yfactI = multfact;

				ssize.width = width;
				ssize.height = height;

				sroi.x = 0; // startx;
				sroi.y = 0; // starty;
				sroi.width = WIDTHS;
				sroi.height = HEIGHTS;

				droi.x = 0;
				droi.y = 0;
				droi.width = (int)(sroi.width * xfactI);
				droi.height = (int)(sroi.height * yfactI);

                cvBufBefResize = cv::Mat(ssize, CV_8UC1, imgbufS);
                cvBufRawBefResize = cv::Mat(ssize, CV_8UC1, imgbufSRaw);

                cv::resize(cvBufBefResize, cvBufAftResize, droi.size(), xfactI, yfactI, cv::INTER_CUBIC);
                cv::resize(cvBufRawBefResize, cvBufRawAftResize, droi.size(), xfactI, yfactI, cv::INTER_CUBIC);

				if (piana->himgfunc) {
					((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, width, height, 1, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvBufAftResize.data, cvBufAftResize.cols, cvBufAftResize.rows, 2, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvBufRawAftResize.data, cvBufRawAftResize.cols, cvBufRawAftResize.rows, 3, 1, 2, 3, piana->imguserparam);
				}

                {
                    cv::Size imgSize = (cv::Size)cvBufAftResize.size();
                    cv::Mat imgBordered1, imgBordered2;
                    if (imgSize.width < WIDTHI) {
                        cv::copyMakeBorder(cvBufAftResize, imgBordered1, 0, 0, 0, WIDTHI - imgSize.width, cv::BORDER_REPLICATE);
                    } else {
                        imgBordered1 = cvBufAftResize(cv::Rect(0, 0, WIDTHI, imgSize.height));
                    }

                    if (imgSize.height < HEIGHTI) {
                        cv::copyMakeBorder(imgBordered1, imgBordered2, 0, HEIGHTI - imgSize.height, 0, 0, cv::BORDER_REPLICATE);
                    } else {
                        imgBordered2 = imgBordered1(cv::Rect(0, 0, WIDTHI, HEIGHTI));
                    }
                    memcpy(imgbufI, imgBordered2.data, sizeof(U08) * WIDTHI * HEIGHTI);
                }

                {
                    cv::Size imgSize = (cv::Size)cvBufRawAftResize.size();
                    cv::Mat imgBordered1, imgBordered2;
                    if (imgSize.width < WIDTHI) {
                        cv::copyMakeBorder(cvBufRawAftResize, imgBordered1, 0, 0, 0, WIDTHI - imgSize.width, cv::BORDER_REPLICATE);
                    } else {
                        imgBordered1 = cvBufRawAftResize(cv::Rect(0, 0, WIDTHS, imgSize.height));
                    }

                    if (imgSize.height< HEIGHTI) {
                        cv::copyMakeBorder(imgBordered1, imgBordered2, 0, HEIGHTI - imgSize.height, 0, 0, cv::BORDER_REPLICATE);
                    } else {
                        imgBordered2 = imgBordered1(cv::Rect(0, 0, WIDTHI, HEIGHTI));
                    }
                    memcpy(imgbufIRaw, imgBordered2.data, sizeof(U08) * WIDTHI * HEIGHTI);
                }

                if (piana->himgfunc) {
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)imgbufS, WIDTHS, HEIGHTS, 2, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)imgbufI, WIDTHI, HEIGHTI, 3, 1, 2, 3, piana->imguserparam);
                }
			}

			//--------------------- 
			//4)  ball mark detection;
			{
				// mark detection
				res = DetectDotMark(piana, camid, pic->imgseg_N, pic->markangle_N, startx2, starty2, &bcP, pr_, &bcL2, lr_, imgbufI, pic->bmimg[i].pimgBW, multfact);
				memcpy(pic->bmimg[i].pimg, imgbufI, WIDTHI*HEIGHTI);
				memcpy(pic->bmimg[i].pimgRaw, imgbufIRaw, WIDTHI*HEIGHTI);
				pic->bmimg[i].multfact = multfact;

				pic->bmimg[i].startx = startx2;
				pic->bmimg[i].starty = starty2;
				pic->bmimg[i].lr_ = lr_;
				pic->bmimg[i].pr_ = pr_;
				pic->bmimg[i].valid = 1;
				if (piana->himgfunc) {
					((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pic->bmimg[i].pimgBW, WIDTHI, HEIGHTI, 1, 1, 2, 3, piana->imguserparam);
					((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pic->bmimg[i].pimg, WIDTHI, HEIGHTI, 2, 1, 2, 3, piana->imguserparam);
					((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pic->bmimg[i].pimgRaw, WIDTHI, HEIGHTI, 4, 1, 2, 3, piana->imguserparam);
				}
			}

			if (piana->himgfunc) {
				U08 *pimg;
				I32 cxi, cyi, cri1;
				int sx, sy, ex, ey;

                //-----
				pimg = (U08 *)malloc((WIDTHI + 16)*(HEIGHTI + 16));
				memcpy(pimg, imgbufI, WIDTHI*HEIGHTI);

				sx = (int)((bcP2.x - startxD2) * multfact);
				sy = (int)((bcP2.y - startyD2) * multfact);
				ex = (int)((bcP2.x - startxD2) * multfact);
				ey = (int)((bcP2.y - startyD2) * multfact);

				cxi = (int)((bcP2.x - startxD2 + 0.0) * multfact);
				cyi = (int)((bcP2.y - startyD2 + 0.0) * multfact);

				cri1 = (int)((pr_ + 0.0) * multfact);

				mycircleGray(cxi, cyi, cri1, 0xFF, pimg, WIDTHI);
				mycircleGray(cxi, cyi, cri1 + 1, 0x00, pimg, WIDTHI);

				mycircleGray(cxi, cyi, cri1 + 10, 0xFF, pimg, WIDTHI);
				mycircleGray(cxi, cyi, cri1 + 10 + 1, 0x00, pimg, WIDTHI);

				//mycircleGray(cxi, cyi, cri1+ 5    , 0xFF, pimg, WIDTHI);
				//mycircleGray(cxi, cyi, cri1+ 5 + 1, 0x00, pimg, WIDTHI);

				((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)pimg, WIDTHI, HEIGHTI, 5, 1, 2, 3, piana->imguserparam);
				free(pimg);
			}

            //--------------------- 
			//5)  ball mark store and processing
			markStore(piana, camid, i, startx2, starty2, pic->imgseg_N, pic->markangle_N, multfact);

			if (piana->opmode == IANA_OPMODE_FILE) {
				Sleep(s_sleep_mark);
			}
		} 		// for (i = 0; i < MARKSEQUENCELEN; i++) 
	}

	free(imgbufS);
	free(imgbufSRaw);
	free(imgbufS0);
	free(imgbufI);
	free(imgbufIRaw);

	res = 1;

	return res;
}

bool get_image_and_info(Mat &src, U64& time_stamp, Rect2i& rough_crop_info,
	U32 opmode, HAND hscamif, U32 camid, U32 index) {
	camif_buffer_info_t *pb;
	U08 *buf;
	U32 res;
	if (opmode == IANA_OPMODE_FILE) {
		//rindex = pic->firstbulkindex + i;
		res = scamif_imagebuf_randomaccess(hscamif, camid, NORMALBULK_BULK, index, &pb, &buf);
		if (res == 0) {
			return false;
		}
	}
	else {
		//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bulk, img read\n", i);
		int j;
		for (j = 0; j < 20; j++) {
			res = scamif_imagebuf_randomaccess(hscamif, camid, NORMALBULK_BULK, index, &pb, &buf);
			if (res == 1) {
				break;						// -> 20200516.. 
				//return false;
			}
			Sleep(1);
		}
		//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read [%d:%d]\n", i, j);
		if (res == 0) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", index, j);
			return false;
			//continue;
		}
	}

	camimageinfo_t *pcii = &pb->cii;

	src = Mat(pcii->height, pcii->width, CV_8UC1, buf);

	time_stamp = MAKEU64(pb->ts_h, pb->ts_l);

	rough_crop_info.x = pcii->offset_x;
	rough_crop_info.y = pcii->offset_y;
	rough_crop_info.width = pcii->width;
	rough_crop_info.height = pcii->height;

	return true;
}

static bool crop_image(Mat &src, Point2i &origin, const Rect2i &rough_crop_info,
	const Point2d &ball_pos_2d, double ball_radius_2d) {
	Mat backup = src.clone();
	int crop_size = 2 * ball_radius_2d + 2;
	Rect2i crop_info(ball_pos_2d.x - ball_radius_2d - rough_crop_info.x,
		ball_pos_2d.y - ball_radius_2d - rough_crop_info.y, //
		crop_size, crop_size);

	if (crop_info.x < 0 || crop_info.y < 0 ||
		crop_info.x + crop_info.width >= src.size().width ||
		crop_info.y + crop_info.height >= src.size().height)
		return false;

	// Crop the full image to that image contained by the rectangle crop_info
	// Note that this doesn't copy the data
	src = backup(crop_info);
	origin =
		Point2i(rough_crop_info.x + crop_info.x, rough_crop_info.y + crop_info.y);
	return true;
}

Matrix4d get_extrinsic_matrix(iana_cam_param_t *picp) {
	return Map<Matrix4d>(picp->cep.extMat[0]).transpose();
}

Matrix3d get_intrinsic_matrix(iana_cam_param_t *picp) {
	double* pcameraParameter = picp->cameraintrinsic.cameraParameter;
	Matrix3d intrinsic;
	intrinsic << pcameraParameter[0], 0., pcameraParameter[1], 0.,
		pcameraParameter[2], pcameraParameter[3], 0., 0., 1.;
	return intrinsic;
}


//static int s_ni = 2;
static int s_ni = 3;
static int s_use_mean_adaptive = 0;
//static int s_use_mean_adaptive = 1;

U32 GetDimpleMarkSeq_Legacy(iana_t *piana, U32 camid, vector<coord_t> &ftseqs,
	vector<size_t> &ftcnts) 
{
	iana_cam_t *pic = piana->pic[camid];
	iana_cam_param_t *picp = &pic->icp;
	marksequence_t *pmks = &pic->mks;
	U64 ts64shot = pic->ts64shot;

	int marksequencelen = (piana->camsensor_category == CAMSENSOR_EYEXO)
		? MARKSEQUENCELEN_EYEXO
		: MARKSEQUENCELEN;

	Matrix4d extrinsic = get_extrinsic_matrix(picp);
	Matrix3d intrinsic = get_intrinsic_matrix(picp);

	point_t bcL2; // Ball center, local
	ballmarkinfo_t *pbmi;
	pbmi = &pic->bmi[0];

	for (int i = 0; i < marksequencelen; i++) {
		if (pbmi[i].valid == 0) {
			continue;
		}

		bcL2.x = pmks->ballposL2[i].x;
		bcL2.y = pmks->ballposL2[i].y;

		Vector3d pos_world = Map<Vector3d>(pmks->ballposL3D[i].v);
		Point2d ball_pos_2d;
		double ball_radius_2d;

		ball_info_world_to_local(ball_pos_2d, ball_radius_2d, pos_world,
			BALL_RADIUS_WORLD, intrinsic, extrinsic);

		if (piana->camsensor_category == CAMSENSOR_EYEXO) {
			//
		}
		else {
#define BALLSTARTY_MARGIN1	0.005
			if (bcL2.y < pic->icp.L.y + BALLSTARTY_MARGIN1) {
				continue;
			}
		}
		// 1) get image buffer
		Mat src;
		U64 time_stamp;
		Rect2i rough_crop_info;
		int index = pic->firstbulkindex + i;
		if (!get_image_and_info(src, time_stamp, rough_crop_info,
			piana->opmode, piana->hscamif,
			camid, index)) {
			break;
		}
		if (piana->camsensor_category == CAMSENSOR_EYEXO) {
			if (time_stamp < ts64shot) {
				continue;
			}
		}

		if (piana->himgfunc) {
			((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)src.data, src.cols, src.rows, 1, 1, 2, 3, piana->imguserparam);
		}

		// 20200812 denoising zrot mod bar
		{
			I32 barStartx, barEndx;
			U32 barDetectRes;

			barDetectRes = scamif_imagebuf_zrot_barnoise_position(piana->hscamif, camid, NORMALBULK_BULK, i, &barStartx, &barEndx);
			if (barDetectRes == 1) {
				modif_barnoise((U08 *)src.data, src.cols, src.rows, barStartx, barEndx);
			}
		}



		// NOTE: Local radius is converted from fixed world radius. 
		// So I delete exception handling codes for small radius cases.

		Point2i origin;
		if (!crop_image(src, origin, rough_crop_info, ball_pos_2d, ball_radius_2d)) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Crop failed[%d:%d]\n", i, index);
			continue;
		}

		Mat denoised_img;
		// feature_define.get_denoised_img(cropped, denoised_img);
		FeatureDefine::get_denoised_img(src, denoised_img);

		if (piana->himgfunc) {
			((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)(
				(char *)src.data, src.cols, src.rows,
				0, 1, 2, 3, piana->imguserparam);

			((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)(
				(char *)denoised_img.data, denoised_img.cols, denoised_img.rows,
				1, 1, 2, 3, piana->imguserparam);
		}

		Mat binarized_img;
		FeatureDefine::get_binarized_img(denoised_img, binarized_img);

		 //if (piana->himgfunc) {
		 //    ((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufD, WIDTHD, HEIGHTD, 0, 1, 2, 3, piana->imguserparam);
		 //    //((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufB, (WIDTHD+bordersize), (HEIGHTD+bordersize), 1, 1, 2, 3, piana->imguserparam);
		 //    ((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufF, WIDTHD, HEIGHTD, 1, 1, 2, 3, piana->imguserparam);
		 //    ((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufB, WIDTHD, HEIGHTD, 2, 1, 2, 3, piana->imguserparam);
		 //    ((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufG, WIDTHD, HEIGHTD, 3, 1, 2, 3, piana->imguserparam);
		 //}

		vector<Point2d> features_2d;
		FeatureDefine::get_center_of_features(
			binarized_img, origin, src.rows, features_2d);

		FeatureDefine::refine_features(
			features_2d, ball_pos_2d, ball_radius_2d, src.rows, denoised_img, origin);

		// if (piana->himgfunc) {
		// 	((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufF, WIDTHD, HEIGHTD, 3, 1, 2, 3, piana->imguserparam);
		// 	((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufB, WIDTHD, HEIGHTD, 4, 1, 2, 3, piana->imguserparam);
		// 	//((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufG, WIDTHD, HEIGHTD, 5, 1, 2, 3, piana->imguserparam);
		// 	((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufH, WIDTHD, HEIGHTD, 5, 1, 2, 3, piana->imguserparam);
		// }

		vector<coord_t> features;
		Vector3d camera_pos_world = Map<Vector3d>(pic->icp.CamPosL.v);
		coord_translate_L2P(features_2d, features, extrinsic.inverse(),
			intrinsic, camera_pos_world,
			pos_world, BALL_RADIUS_WORLD);

#define MARKCOUNT_MIN	10
#define MARKCOUNT_MIN_STRICT	30 // 20, 40
		if (features.size() >= MARKCOUNT_MIN_STRICT) {
			ftcnts.push_back(features.size());
			ftseqs.insert(ftseqs.end(), features.begin(), features.end());
		} else {					// 20200713
			ftcnts.push_back(0);
		}
	}
	return 1;
}

static int s_useInfoFeature = 0;
U32 GetDimpleMarkSeq(iana_t *piana, U32 camid,
	vector<coord_t> &ftseqs,
	vector<size_t> &ftcnts) 
{
	iana_cam_t *pic = piana->pic[camid];
	marksequence_t *pmks = &pic->mks;
	int i, j;

	int marksequencelen = (piana->camsensor_category == CAMSENSOR_EYEXO)
		? MARKSEQUENCELEN_EYEXO
		: MARKSEQUENCELEN;

	//--------
	markSeqD_old(piana, camid);

	for (i = 0; i < (int)marksequencelen; i++) {
		markelement2_t	*pmke2;
		vector<coord_t> features;

        pmke2 = &pmks->mke2Dimple[i];
		if (piana->opmode == IANA_OPMODE_FILE) {
			if (s_useInfoFeature) {
				pmke2 = &piana->mke2DimpleInfo[camid][i];
			}
		}

		for (j = 0; j < (int)pmke2->dotcount; j++) {
			coord_t p = {{
					pmke2->L3bc[j].x,
					pmke2->L3bc[j].y,
					pmke2->L3bc[j].z}};

			features.push_back(p);
		}
		
        if (features.size() >= MARKCOUNT_MIN_STRICT) {
			ftcnts.push_back(features.size());
			ftseqs.insert(ftseqs.end(), features.begin(), features.end());
		} else {					// 20200713.. 
					ftcnts.push_back(0);
		}
	}

	return 1;
}


U32 dimpleSpin(iana_t *piana, U32 camid, std::vector<coord_t> &ftseqs,
	std::vector<size_t> &ftcnts)
{
	U32 res;
	int    *features_counts_othermark;
	double *feature_list_othermark;
	int nf_o;

	iana_cam_t	*pic;
	marksequence_t	*pmks;
	markelement2_t	*pmke2;

	int i, j;

	int indexcount;
	int addcount;

	int foundit;
	int fcount;

	int nonzero;
	int nonzerocount;
	U32 marksequencelen;

	//--
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	}
	else {
		marksequencelen = MARKSEQUENCELEN;
	}
	pic = piana->pic[camid];
	pmks = &pic->mks;

	features_counts_othermark = (int *)malloc(sizeof(int)*MARKSEQUENCELEN);
	feature_list_othermark = (double *)malloc(sizeof(double)*MARKSEQUENCELEN*MARKCOUNT2 * 3);

	/* make othermark feature stucture */
	indexcount = 0;
	addcount = 0;
	nonzero = 0;
	nonzerocount = 0;
	for (i = 0; i < (I32)marksequencelen; i++) {
		foundit = 0;
		fcount = 0;
		pmke2 = &pmks->mke2Mark[i];


		if (pmke2->state) {
			for (j = 0; j < (int)pmke2->dotcount; j++) {

				feature_list_othermark[addcount * 3 + 0] = pmke2->L3bc[j].x;
				feature_list_othermark[addcount * 3 + 1] = pmke2->L3bc[j].y;
				feature_list_othermark[addcount * 3 + 2] = pmke2->L3bc[j].z;
				addcount++;
				foundit = 1;
				fcount++;
				if (fcount >= MARKCOUNT2) {
					break;
				}
			}
		}
		/* Better to add 0 fcount if fails to find any feature? */
		if (foundit) {
			nonzero = 1;
		}

		if (nonzero) {
			features_counts_othermark[indexcount] = fcount;
			indexcount++;
		}

		if (foundit) {
			nonzerocount = indexcount;
		}
	}

	indexcount = nonzerocount;
	nf_o = indexcount;

	Vector3d rotation;
	res = AK_get_rotated_angle((double *)ftseqs.data(), ftcnts.size(),
		ftcnts.data(), feature_list_othermark, nf_o, (size_t *)features_counts_othermark,
		rotation.data(), BALL_RADIUS_WORLD);

	{
		iana_shotresult_t *psr, *psr2;
		double r_x, r_y, r_z;

		cr_vector_t axis;
		double spinmag;
		double backspin;
		double sidespin;

        psr = &piana->shotresult[camid];
        psr2 = &piana->shotresult2[camid];

#define FAKE_SIDESPIN	222
#define FAKE_BACKSPIN	888
        if (res & _BV(AK_SUMMARY_FAULT)) {		// FAULT..
			backspin = FAKE_BACKSPIN; sidespin = FAKE_SIDESPIN;

			axis.x = backspin;
			axis.y = 0;
			axis.z = -sidespin;
			spinmag = cr_vector_normalization(&axis, &axis);

		} else {
			Vector3d rotation_rpm = rotation * (piana->framerate * 60 / 2 / M_PI);				// 20200605, yhsuk
            double a = psr->azimuth * 3.14159165 / 180.;
            double i = psr->incline* 3.14159165 / 180.;

            double ca = cos(a), sa = sin(a), ci = cos(i), si = sin(i);

            r_x = rotation_rpm(0);
			r_y = rotation_rpm(1);
			r_z = rotation_rpm(2);

            // rotation in balldirection aspect
            axis.x = r_x*ca - r_y*sa; 
            axis.y = r_x*ci*sa + r_y*ci*ca + r_z*si; 
            axis.z = -r_x*si*sa - r_y*si*ca + r_z*ci;
            
            backspin = axis.x;
            sidespin = -axis.z;
            spinmag = cr_vector_normalization(&axis, &axis);

            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n\n\n");
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "AK AK AK AK   %10lf %10lf %10lf   (bs: %10lf ss: %10lf)\n",
                axis.x, axis.y, axis.z,
                axis.x, -axis.z);
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n\n\n");
        }

		memcpy(psr2, psr, sizeof(iana_shotresult_t));

		memcpy(&psr2->axis, &axis, sizeof(cr_vector_t));
		psr2->spinmag = spinmag;
		psr2->backspin = backspin;
		psr2->sidespin = sidespin;
	}

	free(features_counts_othermark);
	free(feature_list_othermark);

	return res;
}


U32 modif_barnoise(U08* buf, I32 width, I32 height, I32 barStartX, I32 barEndX) {
	U32 res;
	I32 i, j;
	I32 brightnessCorrVal;

	// collect pixel brightness information
	{
		I32 inBarBrightness, outBarBrightness, inBarBrightnessCnt, outBarBrightnessCnt;
		I32 wstart, wend, hstart, hend;
		
		// init values
		inBarBrightness = 0;
		outBarBrightness = 0;
		inBarBrightnessCnt = 0;
		outBarBrightnessCnt = 0;

		// restrict pixel collecting area indices
		if (barStartX < 4) { wstart = 0; } else { wstart = barStartX - 4; }
		if (barEndX > width - 4) { wend = width; } else { wend = barEndX + 4; }
		hstart = 0;
		hend = height - 1; // The barnoise doen't affect last row of image. See bottom of any barnoised image

		for (i = wstart; i < wend; i++) {
			for (j = hstart; j < hend; j++) {
				if (barStartX <= i && i <= barEndX) {
					inBarBrightness += buf[j * width + i];
					inBarBrightnessCnt++;
				} else {
					outBarBrightness += buf[j * width + i];
					outBarBrightnessCnt++;
				}
			}
		}
		if (inBarBrightnessCnt == 0 || outBarBrightnessCnt == 0) {
			return 0;
		}
		brightnessCorrVal = (int)((double)inBarBrightness / inBarBrightnessCnt - (double)outBarBrightness / outBarBrightnessCnt);
	}

	//correct brightness of barnoise
	{
		U08 lookuptable[256];
		for (i = 0; i < 256; i++) {
			U08 corr;
			corr = (U08)(int)(brightnessCorrVal * pow((1. - (double)i / 255.0), 0.3));
			if (i < corr) { lookuptable[i] = 0; } else { lookuptable[i] = i - corr; }
		}
		for (i = barStartX; i <= barEndX; i++) {
			for (j = 0; j < height; j++) {
				buf[j * width + i] = lookuptable[buf[j * width + i]];
			}
		}
	}
	res = 1;
	return res;
}
//----------------------------------------------------------------------------

#pragma warning(default:4456)



#if 0

I32 brightequalization(iana_t *piana, U32 cx, U32 cy, double radius, U32 width, U32 height, U08 *buf, U08 *buf2);
I32 brightequalization2(iana_t *piana, U32 cx, U32 cy, double radius, U32 width, U32 height, U08 *buf, U08 *buf2, U32 camid);

U32 enumerateBallpair(iana_t *piana);
U32 enumerateBallpair2(iana_t *piana);


U32 selectdimple2(imagesegment_t *pimgseg, U32 maxlabelcount, U32 radius);

U32 updateVmag(iana_t *piana, U32 camid);
U32 shotRegression_NEW_SPINCALC(iana_t *piana, U32 camid); // Almst same with shotRegression(Now, RegressionShotLocalLine)

/*!
********************************************************************************
*	@brief      CAM ball image brightness equalization
*
*  @param[in]	pcbP
*              ball center position
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/02/27
*******************************************************************************/
I32 brightequalization(
    iana_t *piana,
    U32 cx, U32 cy, double radius,
    U32 width, U32 height,
    U08 *buf, U08 *buf2)
{
    I32 res;
    //U32 i, j;
    I32 i, j;
    double rrr;
    U32 value;
    double m1;
    double m21;
    double m22;
    double m2;
    double rate1;
    double rate21;
    double rate22;
    double d21, d22;

    Ipp64f mean;				// Ipp64f == double
    Ipp64f stdev;
    IppiSize roisizeCenter;

    U32 widthS, heightI;
    U32 widthD, heightD;
    U32 startxD, startyD;

    U08 *pimgeqz;

    //----------------------------------------
    pimgeqz = (U08*)malloc(WIDTHS * HEIGHTS * MOREMORE);		// Equalization buffer

    widthS = WIDTHS;
    heightI = HEIGHTS;

#define RATE1	(2*0.25)
#define RATE21	(2*0.6)
#define RATE22	(2*0.8)
    rate1 = RATE1;
    rate21 = RATE21;
    rate22 = RATE22;

    widthD = (U32)(radius * rate1);
    heightD = widthD;
    startxD = (U32)(cx - widthD / 2);
    startyD = (U32)(cy - heightD / 2);
    roisizeCenter.width = widthD;
    roisizeCenter.height = heightD;
    ippiMean_StdDev_8u_C1R(buf + startxD + width * startyD, width, roisizeCenter, &mean, &stdev);
    m1 = mean;

    d21 = widthD = (U32)(radius * rate21);
    heightD = widthD;
    startxD = (U32)(cx - widthD / 2);
    startyD = (U32)(cy - heightD / 2);
    roisizeCenter.width = widthD;
    roisizeCenter.height = heightD;

    ippiMean_StdDev_8u_C1R(buf + startxD + width * startyD, width, roisizeCenter, &mean, &stdev);
    m21 = mean;

    d22 = widthD = (U32)(radius * rate22);
    heightD = widthD;
    startxD = (U32)(cx - widthD / 2);
    startyD = (U32)(cy - heightD / 2);
    roisizeCenter.width = widthD;
    roisizeCenter.height = heightD;
    ippiMean_StdDev_8u_C1R(buf + startxD + width * startyD, width, roisizeCenter, &mean, &stdev);
    m22 = mean;

    m2 = (m22 * (d22*d22) / 4.0 - m21 * (d21*d21) / 4.0) / (d22*d22 / 4.0 - d21 * d21 / 4.0);


    memset(pimgeqz, 0, (WIDTHS * HEIGHTS* MOREMORE));

    for (i = 0; i < (I32)(radius * 2); i++) {
        for (j = 0; j < (I32)(radius * 2); j++) {
            rrr = DDIST(radius, radius, (double)i, (double)j) / radius;
            value = (U32)(256 * ((rrr * rrr) * (1 - m2 / m1) + m2 / m1));

            if (value > 255) {
                value = 255;
            }
            pimgeqz[i + widthS * j] = (U08)value;
        }
    }

    {
        double mfact;
        IppiSize multroisize;
        multroisize.width = (U32)(radius * 2);
        multroisize.height = (U32)(radius * 2);

        memset(buf2, 0x80, widthS*heightI);
        //cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,
        //		"c: (%lf, %lf:%lf)\n", cx2, cy2, cr2);
        ippiMulScale_8u_C1R(
            buf + (U32)(cx - radius + ((U32)(cy - radius)) * width), width,
            pimgeqz, widthS,
            buf2 + (U32)(cx - radius + ((U32)(cy - radius)) * widthS), widthS,
            multroisize);

#define SUBFACT		0.2
#define MULFACT		0.0
        ippiSubC_8u_C1IRSfs(
            (Ipp8u)(m2*SUBFACT),
            buf2 + (U32)(cx - radius + ((U32)(cy - radius)) * widthS), widthS,
            multroisize, 0);
        mfact = (256 / m2) * (1 + MULFACT);
        ippiMulC_8u_C1IRSfs(
            (Ipp8u)mfact,
            buf2 + (U32)(cx - radius + ((U32)(cy - radius)) * widthS), widthS,
            multroisize,
            0);
    }

    /*
    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf, 		WIDTHS, HEIGHTS, 2, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimgeqz, 	WIDTHS, HEIGHTS, 3, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf2, 	WIDTHS, HEIGHTS, 4, 1, 2, 3, piana->imguserparam);
    }
    */
    free(pimgeqz);

    res = 1;

    piana;
    height;
    return res;
}

/*!
********************************************************************************
*	@brief      CAM ball image brightness equalization2
*
*  @param[in]	pcbP
*              ball center position
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/02/27
*******************************************************************************/
I32 brightequalization2(
    iana_t *piana,
    U32 cx, U32 cy, double radius,
    U32 width, U32 height,
    U08 *buf, U08 *buf2,
    U32 camid
)
{
    I32 res;
    U32 i, j;
    double rrr;
    U32 value;
    double m1;
    double m21;
    double m22;
    double m2;
    double rate1;
    double rate21;
    double rate22;
    double d21, d22;

    Ipp64f mean;				// Ipp64f == double
    Ipp64f stdev;
    IppiSize roisizeCenter;

    U32 widthS, heightI;
    U32 widthD, heightD;
    U32 startxD, startyD;

    U08 *pimgeqz;
    double mx, my;

    //----------------------------------------

    pimgeqz = (U08*)malloc(WIDTHS * HEIGHTS* MOREMORE);		// Equalization buffer

    widthS = WIDTHS;
    heightI = HEIGHTS;

#define RATE1	(2*0.25)
#define RATE21	(2*0.6)
#define RATE22	(2*0.8)
    rate1 = RATE1;
    rate21 = RATE21;
    rate22 = RATE22;

    widthD = (U32)(radius * rate1);
    heightD = widthD;
    startxD = (U32)(cx - widthD / 2);
    startyD = (U32)(cy - heightD / 2);
    roisizeCenter.width = widthD;
    roisizeCenter.height = heightD;
    ippiMean_StdDev_8u_C1R(buf + startxD + width * startyD, width, roisizeCenter, &mean, &stdev);
    m1 = mean;

    d21 = widthD = (U32)(radius * rate21);
    heightD = widthD;
    startxD = (U32)(cx - widthD / 2);
    startyD = (U32)(cy - heightD / 2);
    roisizeCenter.width = widthD;
    roisizeCenter.height = heightD;

    ippiMean_StdDev_8u_C1R(buf + startxD + width * startyD, width, roisizeCenter, &mean, &stdev);
    m21 = mean;

    d22 = widthD = (U32)(radius * rate22);
    heightD = widthD;
    startxD = (U32)(cx - widthD / 2);
    startyD = (U32)(cy - heightD / 2);
    roisizeCenter.width = widthD;
    roisizeCenter.height = heightD;
    ippiMean_StdDev_8u_C1R(buf + startxD + width * startyD, width, roisizeCenter, &mean, &stdev);
    m22 = mean;

    m2 = (m22 * (d22*d22) / 4.0 - m21 * (d21*d21) / 4.0) / (d22*d22 / 4.0 - d21 * d21 / 4.0);


    memset(pimgeqz, 0, (WIDTHS * HEIGHTS* MOREMORE));

    mx = radius;
    my = radius;

    camid;


    for (i = 0; i < (U32)(radius * 2); i++) {
        for (j = 0; j < (U32)(radius * 2); j++) {
            rrr = DDIST(mx, my, (double)i, (double)j) / radius;
            value = (U32)(256 * ((rrr * rrr) * (1 - m2 / m1) + m2 / m1));
            if (value > 255) {
                value = 255;
            }
            pimgeqz[i + widthS * j] = (U08)value;
        }
    }


    {
        double mfact;
        IppiSize multroisize;
        multroisize.width = (U32)(radius * 2);
        multroisize.height = (U32)(radius * 2);

        memset(buf2, 0x80, widthS*heightI);
        //cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,
        //		"c: (%lf, %lf:%lf)\n", cx2, cy2, cr2);
        ippiMulScale_8u_C1R(
            buf + (U32)(cx - radius + ((U32)(cy - radius)) * width), width,
            pimgeqz, widthS,
            buf2 + (U32)(cx - radius + ((U32)(cy - radius)) * widthS), widthS,
            multroisize);

#define SUBFACT		0.2
#define MULFACT		0.0
        ippiSubC_8u_C1IRSfs(
            (Ipp8u)(m2*SUBFACT),
            buf2 + (U32)(cx - radius + ((U32)(cy - radius)) * widthS), widthS,
            multroisize, 0);
        mfact = (256 / m2) * (1 + MULFACT);
        ippiMulC_8u_C1IRSfs(
            (Ipp8u)mfact,
            buf2 + (U32)(cx - radius + ((U32)(cy - radius)) * widthS), widthS,
            multroisize,
            0);

    }
    /*
    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf, 		WIDTHS, HEIGHTS, 2, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimgeqz, 	WIDTHS, HEIGHTS, 3, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf2, 	WIDTHS, HEIGHTS, 4, 1, 2, 3, piana->imguserparam);
    }
    */
    free(pimgeqz);

    res = 1;

    piana;
    height;
    return res;
}

U32 enumerateBallpair(iana_t *piana)
{
    U32 res;
    iana_cam_t	*pic0, *pic1;
    marksequence_t *pmks0, *pmks1;
    U64 ts640, ts641;
    double ts64d0, ts64d1;

    U32 ballpaircount;
    ballpair_t *pbp;
    U32 i, j, k, m;
    U32 jstart;
    U32 found;
    point_t posL[2];

    U32 marksequencelen;

    //--
    if (piana->camsensor_category == CAMSENSOR_EYEXO) {
        marksequencelen = MARKSEQUENCELEN_EYEXO;
    } else {
        marksequencelen = MARKSEQUENCELEN;
    }

    pic0 = piana->pic[0];
    pmks0 = &pic0->mks;
    pic1 = piana->pic[1];
    pmks1 = &pic1->mks;

    ballpaircount = 0;
    memset(&piana->ballpair[0], 0, sizeof(ballpair_t) * MARKSEQUENCELEN);
    ts640 = 0; ts64d0 = 0;
    ts641 = 0; ts64d1 = 0;

    jstart = 0;

#define TS64RANGE	(0.0002) // (0.0005)
    i = k = j = m = 0;
    for (i = 0; i < marksequencelen; i++) {
        iana_ballcandidate_t *pbc0, *pbc1;
        for (k = 0; k < BALLCCOUNT; k++) {
            pbc0 = &pic0->bc[i][k];
            if (pbc0->cexist == IANA_BALL_EXIST) {
                ts640 = pbc0->ts64;
                ts64d0 = ts640 / TSSCALE_D;
                posL[0].x = pbc0->L.x;
                posL[0].y = pbc0->L.y;

                found = 0;
                for (j = 0; j < marksequencelen; j++) {
                    for (m = 0; m < BALLCCOUNT; m++) {
                        pbc1 = &pic1->bc[j][m];

                        if (pbc1->cexist == IANA_BALL_EXIST) {
                            ts641 = pbc1->ts64;
                            ts64d1 = ts641 / TSSCALE_D;
                            if (ts64d0 > ts64d1 - TS64RANGE && ts64d0 < ts64d1 + TS64RANGE) {		// Found it!!;
                                found = 1;
                                posL[1].x = pbc1->L.x;
                                posL[1].y = pbc1->L.y;
                                break;
                            } else {
                                if (ts64d0 < ts64d1) {
                                    break;
                                }
                            }
                        }
                    }
                    if (found) {
                        break;
                    }
                }

                if (found) {
                    pbp = &piana->ballpair[ballpaircount];
                    pbp->index[0] = i;
                    pbp->index[1] = j;
                    pbp->ts64d = ts64d0;
                    pbp->valid = 1;
                    pbp->posL[0].x = posL[0].x;
                    pbp->posL[0].y = posL[0].y;

                    pbp->posL[1].x = posL[1].x;
                    pbp->posL[1].y = posL[1].y;

                    ballpaircount++;
                    //jstart = j+1;
                    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d %2d vs %2d %2d]: %10lf  %10lf\n", i, k, j, m, ts64d0, ts64d1);
                }
            }
        } 		// for (k = 0; k < MARKSEQUENCELEN; k++) 
    } 			// for (i = 0; i < MARKSEQUENCELEN; i++) 
    if (ballpaircount > marksequencelen - 1) {
        ballpaircount = marksequencelen - 1;
    }

    piana->ballpaircount = ballpaircount;

    //cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ballpaircount: %d\n", ballpaircount);
    for (i = 0; i < ballpaircount; i++) {
        double tsdiff;
        pbp = &piana->ballpair[i];
        ts64d0 = pmks0->ts64[pbp->index[0]] / TSSCALE_D;
        ts64d1 = pmks1->ts64[pbp->index[1]] / TSSCALE_D;
        tsdiff = ts64d0 - ts64d1;
        //cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%2d: %2d %2d] %10lf  %10lf : %lg\n", i, 
        // pbp->index[0], pbp->index[1], ts64d0, ts64d1, tsdiff);
    }
    res = 1;
    return res;
}




/*!
********************************************************************************
*	@brief      enumerate Ballpair
*
*  @param[in]	piana
*              IANA module handle
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/0922
*******************************************************************************/
U32 enumerateBallpair2(iana_t *piana)
{
    U32 res;
    iana_cam_t	*pic;
    U64 ts64shot;
    double ts64dd;
    U32 ballpaircount;
    ballpair_t *pbp;
    U32 camid;
    U32 count, counts[2];
    U32 i, j;

#define STARTPOSITIONMULT	0 // 5
    double xx[MARKSEQUENCELEN + STARTPOSITIONMULT], yy[MARKSEQUENCELEN + STARTPOSITIONMULT], tt[MARKSEQUENCELEN + STARTPOSITIONMULT];
    //	double mx_[2], bx_[2];
    //	double my_[2], by_[2];
    double ax2_[2], ax1_[2], ax0_[2];
    double ay2_[2], ay1_[2], ay0_[2];
    double r2;

    U32 marksequencelen;

    //--
    if (piana->camsensor_category == CAMSENSOR_EYEXO) {
        marksequencelen = MARKSEQUENCELEN_EYEXO;
    } else {
        marksequencelen = MARKSEQUENCELEN;
    }
    ballpaircount = 0;
    //	piana->ballpaircount = 0;
    memset(&piana->ballpair[0], 0, sizeof(ballpair_t) * MARKSEQUENCELEN);
    ts64shot = piana->pic[0]->ts64shot;

    counts[0] = counts[1] = 0;
    for (camid = 0; camid < 2; camid++) {
        pic = piana->pic[camid];
        count = 0;
        for (i = 0; i < STARTPOSITIONMULT; i++) {
            xx[count] = pic->icp.L.x;
            yy[count] = pic->icp.L.y;
            tt[count] = 0;
            count++;
        }
        for (i = 0; i < marksequencelen; i++) {
            for (j = 0; j < BALLCCOUNT; j++) {
                iana_ballcandidate_t *pbc;
                pbc = &pic->bc[i][j];
                if (pbc->cexist == IANA_BALL_EXIST) {
                    ts64dd = ((I64)(pbc->ts64 - ts64shot)) / TSSCALE_D;
                    xx[count] = pbc->L.x;
                    yy[count] = pbc->L.y;
                    tt[count] = ts64dd * 1000;

                    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "qqq [%2d:%4d:%4d] %lf %lf %lf\n",
                        camid, i, j,
                        xx[count],
                        yy[count],
                        tt[count]);

                    count++;
                    break;
                }
            }
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "\n");

        }


#define	GOOD4REGRESSION	4
        if (count >= GOOD4REGRESSION + STARTPOSITIONMULT) {
            I32 regres;

            ax2_[camid] = ax1_[camid] = ax0_[camid] = 0;
            regres = caw_quadraticregression(tt + 1, xx + 1, count - 1, &ax2_[camid], &ax1_[camid], &ax0_[camid], &r2);
            if (regres == 0 || r2 < 0.8) {
                ax2_[camid] = ax1_[camid] = ax0_[camid] = 0;
                cr_regression2(tt + 1, xx + 1, count - 1, &ax1_[camid], &ax0_[camid], &r2);
            }

            ay2_[camid] = ay1_[camid] = ay0_[camid] = 0;
            regres = caw_quadraticregression(tt + 1, yy + 1, count - 1, &ay2_[camid], &ay1_[camid], &ay0_[camid], &r2);
            if (regres == 0 || r2 < 0.8) {
                ay2_[camid] = ay1_[camid] = ay0_[camid] = 0;
                cr_regression2(tt + 1, yy + 1, count - 1, &ay1_[camid], &ay0_[camid], &r2);
            }
            //			cr_regression2(tt, xx, count, &mx_[camid], &bx_[camid], &r2);
            //			cr_regression2(tt, yy, count, &my_[camid], &by_[camid], &r2);
        }
        counts[camid] = count;
    }

#define TS64D		(0.001) // (0.0001)
    if (counts[0] >= GOOD4REGRESSION + STARTPOSITIONMULT && counts[1] >= GOOD4REGRESSION + STARTPOSITIONMULT) {
        double ts64ddm;
        I32 iter;
        //for (i = 0; i < MARKSEQUENCELEN; i++) 
        iter = 0;
        for (i = 0; i < 10; i++) {
            pbp = &piana->ballpair[i];
            pbp->index[0] = 0;
            pbp->index[1] = 0;

            ts64dd = i * (TS64D);
            ts64ddm = ts64dd * 1000;

            pbp->ts64d = ts64shot / TSSCALE_D + ts64dd;

            pbp->valid = 1;


            for (camid = 0; camid < 2; camid++) {
                //pbp->posL[camid].x = mx_[camid] * ts64dd + bx_[camid];
                //pbp->posL[camid].y = my_[camid] * ts64dd + by_[camid];


                //pbp->posL[camid].x = ax2_[camid] * ts64dd * ts64dd + ax1_[camid] * ts64dd + ax0_[camid];
                //pbp->posL[camid].y = ay2_[camid] * ts64dd * ts64dd + ay1_[camid] * ts64dd + ay0_[camid];
                pbp->posL[camid].x = ax2_[camid] * ts64ddm * ts64ddm + ax1_[camid] * ts64ddm + ax0_[camid];
                pbp->posL[camid].y = ay2_[camid] * ts64ddm * ts64ddm + ay1_[camid] * ts64ddm + ay0_[camid];
            }
            iter++;
        }
        piana->ballpaircount = iter;;



        for (camid = 0; camid < 2; camid++) {
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%d   X %lf %lf %lf  Y %lf %lf %lf\n", camid,
                ax2_[camid], ax1_[camid], ax0_[camid],
                ay2_[camid], ay1_[camid], ay0_[camid]);

            for (i = 0; i < piana->ballpaircount; i++) {
                pbp = &piana->ballpair[i];

                pbp->posL[camid].x;
                pbp->posL[camid].y;
                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d:%4d] %lf %lf %lf\n",
                    camid, i,
                    pbp->posL[camid].x,
                    pbp->posL[camid].y,
                    pbp->ts64d - (ts64shot / TSSCALE_D));

            }
        }


        res = 1;
    } else {
        res = 0;
    }

    return res;
}




U32 selectdimple2(
    imagesegment_t *pimgseg,
    U32 maxlabelcount,
    U32 radius
)
{
    U32 res;
    I32 i;

    I32 meanDimple;
    I32 minw, maxw;
    I32 minh, maxh;
    I32 mincount, maxcount;
    I32 segW, segH;

    double dx, dy, d2;
    double minratio, maxratio;
    double segratio;





    I32 maxd2, mind2;

    imagesegment_t 	*pis;

    double dimple_area;
    double dimple_radius;
    double seg_area;
    I32 badreason;
    //--

    meanDimple = radius / 8;

    minw = (meanDimple / 4);
    maxw = (U32)(meanDimple * 1.5);

    minh = minw;
    maxh = maxw;
    mincount = minw * minw;
    maxcount = maxw * maxw;

    //minratio = 0.5;
    //maxratio = 2.0;

    minratio = 1.0 / 3.0;
    maxratio = 3.0;




    maxd2 = radius * radius;
    mind2 = meanDimple * meanDimple;

    //-
    dimple_radius = (radius / 10.0);
    dimple_area = dimple_radius * dimple_radius * M_PI;


    //-
    for (i = 0; i < (I32)maxlabelcount; i++) {
        pis = pimgseg + i;
        if (pis->state == IMAGESEGMENT_FULL) {
            if (pis->pixelcount < (U32)mincount || pis->pixelcount >(U32)maxcount) {
                pis->state = IMAGESEGMENT_NULL;
                continue;
            }

            // check count;


            // Check Size
            segW = pis->ixr - pis->ixl + 1;
            segH = pis->iyb - pis->iyu + 1;


            //seg_area = segW * segH;
            seg_area = pis->pixelcount;
            badreason = 0;
            if (seg_area < dimple_area / 15.0) {
                badreason = 1;
            } else if (seg_area > dimple_area) {
                badreason = 2;
            } else if ((double)segW > dimple_radius * 2) {
                badreason = 3;
            } else if ((double)segH > dimple_radius * 2) {
                badreason = 4;
            } else if ((double)segW > segH * 2) {
                badreason = 5;
            } else if ((double)segH > segW * 2) {
                badreason = 6;
            } else if (seg_area < (segH * segW * 0.5)) {
                badreason = 6;
            }

            if (badreason != 0) {
                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, " badreason: %d\n", badreason);
                continue;
            }

            if (segW < minw || segH < minw) {
                pis->state = IMAGESEGMENT_NULL;
                continue;
            }

            segratio = (double)segW / (double)segH;

            // check ratio
            if (segratio < minratio || maxratio > maxratio) {
                pis->state = IMAGESEGMENT_NULL;
                continue;
            }

            // Check in-circle
            dx = pis->mx - radius;
            dy = pis->my - radius;
            d2 = dx * dx + dy * dy;
            if (d2 > maxd2) {
                pis->state = IMAGESEGMENT_NULL;
                continue;
            }
            if (d2 < mind2) {
                pis->state = IMAGESEGMENT_NULL;
                continue;
            }

            if (badreason != 0) {
                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "     DON'T filter'ed..\n");
            }
        }
    }


    res = 1;

    return res;
}


/*!
********************************************************************************
*	@brief      Updata vmag
*
*  @param[in]	piana
*              IANA module handle
*  @param[in]	camid
*              camera id
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/0917
*******************************************************************************/
U32 updateVmag(iana_t *piana, U32 camid)
{
    U32 res;
    I32 i;

    int vvvcount;
    double vvv;
    double vvvsum;
    double vmm;
    double vmax, vmin;
    iana_shotresult_t *psr;
    int isfirst;				// Discard first ball..
    int starti, endi;

    int usevreg;
    double vreg;

    iana_cam_t	*pic;

    //--

    pic = piana->pic[camid];
    psr = &piana->shotresult[camid];

    memcpy(psr, &piana->shotresultdata, sizeof(iana_shotresult_t));

#define MINVVV0	0 // 3
#define MINVVV1	4 // 6, 3
#define VMINRATE 0.8 // 0.9
#define VMAXRATE 1.1
#define MINVVVCOUNT	2
#define MAXVVV0	20 // 12
    vmin = psr->vmag * VMINRATE;
    vmax = psr->vmag * VMAXRATE;

    usevreg = 0;
    vreg = 0.0;
    if (pic->ballcount >= MINVVV1) {
        double vv[PIXELCOUNTMAX + 1], yy[PIXELCOUNTMAX + 1];
        int count0;
        double y0;

        double m_, b_;
        double yc;

        y0 = piana->pic[camid]->icp.L.y;
        y0 = 0; // -_-;

        if (piana->camsensor_category == CAMSENSOR_EYEXO) {
#define VMAGCALC_YVALUE_25mm	0.15
            yc = (y0 + VMAGCALC_YVALUE_25mm);
        } else {
#define VMAGCALC_YVALUE_	0.15
            yc = (y0 + VMAGCALC_YVALUE_);
        }

        count0 = 0;
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "CAM: %d\n", camid);
        if (pic->TrajY[0] > yc) {
            yc = pic->TrajY[0];
        }
        for (i = MINVVV0; i < (I32)pic->ballcount - 1; i++) {
            if (pic->vmag2[i] > vmin && pic->vmag2[i] < vmax) {
                vv[count0] = pic->vmag2[i];
                yy[count0] = pic->TrajY[i];
                count0++;
                cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,
                    " (%2d) vmag: %10lf  y: %10lf \n", i, pic->vmag2[i], pic->TrajY[i]);

                if (count0 > 4) {
                    if (pic->TrajY[i] > yc) {
                    } else {
                        yc = pic->TrajY[i];
                    }
                    break;
                }
            }
        }

        if (count0 > 3) {
            I32 count1;
            count1 = count0;

            cr_regression(yy, vv, count1, &m_, &b_);
            vreg = (m_ * yc + b_);

#define MAXM_ (-0.5)
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "vreg %lf at yc: %lf\n", vreg, yc);
            if (/*m_ < MAXM_ && */vreg < vmax && vreg > vmin) {
                usevreg = 1;
            }
        } else {
            usevreg = 0;
        }
    }

    if (pic->ballcount >= MINVVV1) {
        vmm = 0;
        vvvcount = 0;
        for (i = MINVVV0; i < (I32)(pic->ballcount - 1); i++) {
            //cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,
            //	" (%2d) vmag: %lf  y: %lf (%lf)\n", i, pic->vmag2[i], pic->TrajY[i], pic->TrajY[i] - pic->TrajY[0]);

#define MAXTRAJYDIFF	0.5 // 0.2
            if (pic->vmag2[i] > vmin && pic->vmag2[i] < vmax && (pic->TrajY[i] - pic->TrajY[0] < MAXTRAJYDIFF)) {
                vmm += pic->vmag2[i];
                vvvcount++;
            }
        }

        vmm = vmin;
        vvv = pic->vmag2[MINVVV0];
        vvvsum = 0;
        vvvcount = 0;
        isfirst = 1;

        endi = pic->ballcount - 1;
        if (endi > MAXVVV0) {
            endi = MAXVVV0;
        }

        if (pic->ballcount == MINVVV1) {
            starti = 1;
        } else {
            starti = 2;
        }

        for (i = starti; i <= endi; i++) {
            if (pic->vmag2[i] > vmin && pic->vmag2[i] < vmax && (pic->TrajY[i] - pic->TrajY[0] < MAXTRAJYDIFF)) {
                if (pic->vmag2[i] > vmm) {
                    if (isfirst) {
                        isfirst = 0;		// Discard 1st ball.
                        continue;
                    }
                    vvvsum += pic->vmag2[i];
                    vvvcount++;

#define VVVCOUNT	20 // 10, 8, 3
                    if (vvvcount >= VVVCOUNT) {
                        break;
                    }
                }
            }
        }

        if (vvvcount > 0) {
            vvv = vvvsum / vvvcount;
            cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN,
                " vmag: %lf -> %lf:%d (%d ~ %d) (%lf)\n",
                psr->vmag, vvv, vvvcount, starti, endi, vmm);

            //#define LINEAR_SCALE(x,x0,y0,x1,y1) ( ( ((double) (((y1) - (y0)))) / ((double) ((x1) - (x0))) )*((x)-(x0)) + (y0) )
            psr->vmag = vvv;
        }
    }

    if (usevreg) {
        cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "Use Vreg. %lf -> %lf\n", psr->vmag, vreg);
        psr->vmag = vreg;
    }

    res = 1;


    return res;
}


/*!
********************************************************************************
*	@brief      shotRegression
*
*  @param[in]	piana
*              IANA module handle
*
*  @return		1: good, 0: no-good
*
*	@author	    yhsuk
*  @date       2016/0917
*******************************************************************************/
U32 shotRegression_NEW_SPINCALC(iana_t *piana, U32 camid)
{
    U32 res;

    iana_cam_t			*pic;
    iana_cam_param_t 	*picp;
    marksequence_t		*pmks;

    double Lxx[MARKSEQUENCELEN];
    double Lyy[MARKSEQUENCELEN];
    double tt[MARKSEQUENCELEN];
    double Lrr[MARKSEQUENCELEN];

    U32 count;
    U32 isFirst;
    U64 ts64shot;

    U32 i;
    ballmarkinfo_t		*pbmi;
    U32 marksequencelen;

    //----
    if (piana->camsensor_category == CAMSENSOR_EYEXO) {
        marksequencelen = MARKSEQUENCELEN_EYEXO;
    } else {
        marksequencelen = MARKSEQUENCELEN;
    }
    pic = piana->pic[camid];
    picp = &pic->icp;
    pmks = &pic->mks;
    pbmi = &pic->bmi[0];

    count = 0;
    isFirst = 1;
    ts64shot = pic->ts64shot;

    for (i = 0; i < marksequencelen; i++) {
        I64 t64delta;
        double t64deltad;
        if (pbmi[i].valid) {
            t64delta = (I64)(pmks->ts64[i] - ts64shot);

            if (t64delta < 0) {
                continue;
            }

            t64deltad = (double)(t64delta / TSSCALE_D);

            {
                double dx, dy, dxy;
                double lx, ly;

                dx = pmks->ballposL2Raw[i].x - pmks->ballposL2[i].x;
                dy = pmks->ballposL2Raw[i].y - pmks->ballposL2[i].y;

                dxy = sqrt(dx*dx + dy * dy);
                if (dxy > RAW_L_DIFF) {
                    lx = pmks->ballposL2[i].x;
                    ly = pmks->ballposL2[i].y;
                } else {
                    lx = pmks->ballposL2Raw[i].x;
                    ly = pmks->ballposL2Raw[i].y;
                }

                Lxx[count] = lx;
                Lyy[count] = ly;
                Lrr[count] = pmks->ballrL2[i];
            }

            tt[count] = t64deltad;
            if (isFirst) {
                isFirst = 0;
            } else {
                count++;
            }
        }
    }

#define BALLCOUNT_MIN_NEW		3
    if (count >= BALLCOUNT_MIN_NEW) {
        double maLx[3], maLy[3], maLr[3];
        double mxy_, bxy_;

        double r2;

        caw_quadraticregression(tt, Lxx, count, &maLx[2], &maLx[1], &maLx[0], &r2);
        caw_quadraticregression(tt, Lyy, count, &maLy[2], &maLy[1], &maLy[0], &r2);
        caw_quadraticregression(tt, Lrr, count, &maLr[2], &maLr[1], &maLr[0], &r2);

        cr_regression(Lyy, Lxx, count, &mxy_, &bxy_);

        memcpy(&picp->maLx[0], &maLx[0], sizeof(double) * 3);
        memcpy(&picp->maLy[0], &maLy[0], sizeof(double) * 3);
        memcpy(&picp->maLr[0], &maLr[0], sizeof(double) * 3);
        picp->mxy_ = mxy_; picp->bxy_ = bxy_;

        res = 1;
    } else {
        res = 0;
    }

    return res;
}

#endif