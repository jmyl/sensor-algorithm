#ifndef __IANA_SPIN_MARK_H__
#define __IANA_SPIN_MARK_H__

#include "iana.h"

I32 iana_spin_mark(iana_t *piana, std::vector<coord_t> &ftseqs,
	std::vector<size_t> &ftcnts);
U32 dimpleSpin(iana_t *piana, U32 camid, std::vector<coord_t> &ftseqs,
	std::vector<size_t> &ftcnts);

#endif //__IANA_SPIN_MARK_H__
