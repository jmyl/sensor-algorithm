#pragma once

#include "iana.h"

namespace SpinCalc {
	void init();
    void run(iana_t *piana);
}
