#include "spincalc.h"

#include "AK_algorithm.h"
#include "feature_define.h"
#include "iana_spin_mark.h"
#include "iana_spin_rotation.h"
#include <vector>
#include <algorithm>					// 20200927
#include "akspinsrc/headers/numpy.h"

using namespace std;

static int checkSpin(iana_shotresult_t *psr);

void SpinCalc::init() {
	AK_init(30, 30);
	FeatureDefine::init(FeatureDefine::params_list(0, 9, 39, 30, 3, 18, 19, 11, 0,
		27, 25, 5, 1, 3, 3)); 
}
void SpinCalc::run(iana_t *piana) {
    vector<coord_t> ftseqs;
    vector<size_t> ftcnts;
    // 1) get mark element and tracking them.
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "before iana_spin_mark()\n");
	iana_spin_mark(piana, ftseqs, ftcnts);
	// 2) get rotation axis and magnitude
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "before iana_spin_rotation()\n");
	iana_spin_rotation(piana);
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "after  iana_spin_rotation()\n");
    
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {
		int camid;
		for (camid = 0; camid < 2; camid++) {
			if (piana->processmode[camid] == CAMPROCESSMODE_BALLMARK) 
			{
				cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]Before dimpleSpin\n", camid);
				dimpleSpin(piana, camid, ftseqs, ftcnts);

				{
					iana_shotresult_t sr2, sr2R;
					int isSpinGood, isSpinGoodR;

					memcpy(&sr2, &piana->shotresult2[camid], sizeof(iana_shotresult_t));
					isSpinGood = checkSpin(&sr2);
					if (isSpinGood == 0) {
						vector<coord_t> ftBackseqs;
						vector<size_t> ftBackcnts;

						ftBackseqs = ftseqs;
						ftBackcnts = ftcnts;

						std::reverse(ftBackseqs.begin(), ftBackseqs.end());
						std::reverse(ftBackcnts.begin(), ftBackcnts.end());

						dimpleSpin(piana, camid, ftBackseqs, ftBackcnts);

						memcpy(&sr2R, &piana->shotresult2[camid], sizeof(iana_shotresult_t));

						sr2R.backspin = -sr2R.backspin;
						sr2R.sidespin = -sr2R.sidespin;
						sr2R.rollspin = -sr2R.rollspin;

						sr2R.axis.x = -sr2R.axis.x;	sr2R.axis.y = -sr2R.axis.y;	sr2R.axis.z = -sr2R.axis.z;

						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bs: %lf ss: %lf axis: %lf %lf %lf    vs bs: %lf ss: %lf axis: %lf %lf %lf (%lf %lf)\n", camid,
							sr2.backspin, sr2.sidespin, sr2.axis.x, sr2.axis.y, sr2.axis.z,
							sr2R.backspin, sr2R.sidespin, sr2R.axis.x, sr2R.axis.y, sr2R.axis.z,
							sr2R.backspin - sr2.backspin, sr2R.sidespin - sr2.sidespin);

						isSpinGoodR = checkSpin(&sr2R);
						if (isSpinGoodR) {
							memcpy(&piana->shotresult2[camid], &sr2R, sizeof(iana_shotresult_t));
							cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]Change Spin!!\n");
						} else {
							memcpy(&piana->shotresult2[camid], &sr2, sizeof(iana_shotresult_t));
						}
					}
				}
				cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]After dimpleSpin\n", camid);
			}
		}
	}

}

static int checkSpin(iana_shotresult_t *psr)
{
	int res;

	double spinaxis2D;			// spinaxis with backspin and sidespin
	double rollspinv;

	//--
	spinaxis2D = atan2(psr->sidespin, psr->backspin);
	spinaxis2D = spinaxis2D * 180.0 / 3.141592;			// radian to degree.

	rollspinv = psr->axis.y;  // rollspin.. :P

//#define SPINAXIS2D_TOOLARGE	10.0
//#define ROLLSPINV_TOOLARGE	0.15
#define SPINAXIS2D_TOOLARGE	20
#define ROLLSPINV_TOOLARGE	0.2
	if (spinaxis2D > SPINAXIS2D_TOOLARGE || spinaxis2D < -SPINAXIS2D_TOOLARGE) {
		res = 0;
	} else if (rollspinv > ROLLSPINV_TOOLARGE || rollspinv < -ROLLSPINV_TOOLARGE) {
		res = 0;
	} else {
		res = 1;
	}

	return res;
}