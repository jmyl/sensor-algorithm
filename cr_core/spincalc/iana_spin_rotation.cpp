/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA shot spin rotation calc.
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_spin_rotation.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2016/02/28 Spin mark

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <math.h>
#if defined(__linux__)
#include <string.h>
#endif
#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"

#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#endif

#include "scamif.h"
//#include "scamif_main.h"
#include "scamif_buffer.h"

#include "iana.h"
#include "iana_work.h"
#include "iana_implement.h"
#include "iana_cam.h"
#include "iana_cam_implement.h"
#include "iana_balldetect.h"
#include "iana_shot.h"
#include "iana_tool.h"
#include "iana_spin.h"
#include "iana_spin_rotation.h"

#include "iana_coordinate.h"
#include "cr_regression.h"
#include "cr_matrix.h"

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif
#if !defined(LINEAR_SCALE)
#define LINEAR_SCALE(x,x0,y0,x1,y1) ( ( ((double) (((y1) - (y0)))) / ((double) ((x1) - (x0))) )*((x)-(x0)) + (y0) )
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/
//I32 calcRodriguesMatrix(cr_point_t *pcposL, cr_point_t *pbposL, 
//		double rrm[][3], cr_vector_t *psrv, double     *psrt);
I32 calcRodriguesMatrix2(
	cr_point_t *pcposL,
	cr_point_t *pbposL,
	double rrm[][3],
	cr_vector_t *psrv,
	double     *psrt
	);


I32 rodriguesMatrix(cr_vector_t *pua, double theta, double rrm[][3]);
I32 rodriguesMatrixInv(double rrm[][3], cr_vector_t *pua, double *ptheta);
I32 rodriguesRotate(double rrm[][3], cr_vector_t *pvfrom, cr_vector_t *pvto);

U32 getplane(cr_point_t *array, U32 arraylen, cr_plane_t *pplane);
U32 getplane0(cr_point_t *array, U32 arraylen, cr_plane_t *pplane);
U32 getplane1(cr_point_t *array, U32 arraylen, cr_plane_t *pplane);
U32 getplane2(cr_point_t *array, U32 arraylen, cr_plane_t *pplane);
U32 getplane3(cr_point_t *array, U32 arraylen, cr_plane_t *pplane);


I32 calcaxis(cr_vector_t planenormal[], U32 planenormalvalid[], U32 planenormallength[], cr_vector_t *paxis0, cr_vector_t *paxis);
extern U32 markRotationBest(iana_t *piana, U32 camid);


double weightForCalc(double x, double y, double r)
{
	double w;
	double d;

#define MINR4WEIGHT	0.01
	if (r < MINR4WEIGHT) {
		w = 1.0;
	} else {
		d = sqrt(x*x+y*y);
		w = 1.0-d/r;
		if (w < 0.0) {
			w = 0.0;
		}
		if (w > 1.0) {
			w = 1.0;
		}
	}
#define MINW	0.3			// 0, 0.3
#define MAXW	1.0			// 1, 1.0

	w = ((MAXW - MINW) / (1.0 - 0.0))  * w + MINW;

	return w;
}


/*!
 ********************************************************************************
 *	@brief      CAM get rotation axis and magnitude
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/02/28
 *******************************************************************************/
//I32 iana_spin_rotation_NEWMARKINGPROCESS(iana_t *piana, U32 camid);

I32 iana_spin_rotation(iana_t *piana)
{
	I32 res;
	U32 camid;

	res = 0;
	for (camid = 0; camid < 2; camid++) {
		if (piana->processmode[camid] == CAMPROCESSMODE_BALLMARK) {
			res = markRotationBest(piana, camid);
		}
	}
	return res;
}



int rpmcompare(const void *arg1, const void *arg2)
{
	int res;
	double v1, v2;

	v1 = *(double *) arg1;
	v2 = *(double *) arg2;
	if (v1 < v2) {
		res = 1;
	} else if (v1 == v2) {
		res = 0;
	} else {
		res = -1;
	}

	return res;
}


#pragma warning(disable:4101)


/*!
 ********************************************************************************
 *	@brief      CAM get RodriguesMatrix
 *
 *  @param[in]	pcposL
 *              Camera position
 *  @param[in]	pbposL
 *              Ball position
 *
 *  @param[out]	rrm
 *              R matrix result
 *  @param[out]	psrv
 *              Sight Rotation vector
 *  @param[out]	psrt
 *              Sight Rotation angle
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/02/28
 *******************************************************************************/
#if 0
I32 calcRodriguesMatrix(
	cr_point_t *pcposL,
	cr_point_t *pbposL,
	double rrm[][3],
	cr_vector_t *psrv,
	double     *psrt
	)
{
	I32 res;
	cr_vector_t ucs;	// Cam sight unit-vector
	cr_vector_t uu;		// Up unit-vector
	cr_vector_t ua;		// axis unit-vector
	double thetaC_;
	double dtmp;

	//--------
	//1. Get Camera sight vector
	cr_vector_sub(pbposL, pcposL, &ucs); // v1 - v2. Cam sight vector

	dtmp = cr_vector_norm(&ucs);
	dtmp = 1 / dtmp;
	cr_vector_scalar(&ucs, dtmp, &ucs); // Cam sight unit vector

//			// DO NOT use D-coordindate.. :P		20160228
//	//change to d-coordinate ... 3. unit vector
//	dtmp  =   ucs.x;
//	ucs.x = - ucs.y;
//	ucs.y = dtmp;

	//4. Up unit vector
	uu.x = 0.0;
	uu.y = 0.0;
	uu.z = 1.0;

	//5. Axis vector.   a_ = uu x uc
	cr_vector_cross(&uu, &ucs, &ua);

	dtmp = cr_vector_norm(&ua);	
	dtmp = 1 / dtmp;
	cr_vector_scalar(&ua, dtmp, &ua); // Axis unit vector

	//6. get rotation angle
	dtmp = cr_vector_inner(&ucs, &uu);
	thetaC_ = M_PI - acos(dtmp);

	//7. Rodrigues rotation... 

	memcpy(psrv, &ua, sizeof(cr_vector_t));
	*psrt = -thetaC_;
	res = rodriguesMatrix(&ua, -thetaC_, rrm);

	return res;
}
#endif
I32 calcRodriguesMatrix(			// More instructive processing... (Same result)
	cr_point_t *pcposL,
	cr_point_t *pbposL,
	double rrm[][3],
	double rrma[][3],
	cr_vector_t *psrv,
	double     *psrt
	)
{
	I32 res;
	cr_vector_t ucs;	// Ball to Cam unit-vector
	cr_vector_t ux, uy, uz;		// unit-vector
	cr_vector_t ua;		// axis unit-vector
	double thetaC_;
	double dtmp;

	//--------
	//1. Get Ball to Camera vector
	cr_vector_sub(pcposL, pbposL, &ucs); // v1 - v2. ball to Cam Vector

	dtmp = cr_vector_norm(&ucs);
	dtmp = 1 / dtmp;
	cr_vector_scalar(&ucs, dtmp, &ucs); //Ball to Ca unit vector


	//4. unit vector
	ux.x = 1.0; ux.y = 0.0; ux.z = 0.0;
	uy.x = 0.0; uy.y = 1.0; uy.z = 0.0;
	uz.x = 0.0; uz.y = 0.0; uz.z = 1.0;

	//5. Axis vector.   a_ = uz x uc
	cr_vector_cross(&uz, &ucs, &ua);

	dtmp = cr_vector_norm(&ua);	
	dtmp = 1 / dtmp;
	cr_vector_scalar(&ua, dtmp, &ua); // Axis unit vector

	//6. get rotation angle
	dtmp = cr_vector_inner(&ucs, &uz);
	thetaC_ = acos(dtmp);

	//7. Rodrigues rotation... 

	memcpy(psrv, &ua, sizeof(cr_vector_t));
	*psrt = thetaC_;
	res = rodriguesMatrix(&ua, thetaC_, rrm);

#if 0
	{
		cr_vector_t aaa;
		double thetaaa;

		rodriguesMatrixInv(rrm, &aaa, &thetaaa);

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "(%10lf %10lf %10lf:%10lf<%10lf>) -> (%10lf %10lf %10lf:%10lf<%10lf>)\n", 
				ua.x, ua.y, ua.z, thetaC_, RADIAN2DEGREE(thetaC_),
				aaa.x, aaa.y, aaa.z, thetaaa, RADIAN2DEGREE(thetaaa)
				);
	}
#endif


	//8. Get axis-rotation matrix
	{
		cr_vector_t ur;
		cr_vector_t uaxy;
		double thetaR_;
		double ra[3][3];
		double ra_[3][3];
		double s, c;
		double rrma_[3][3];

		//get rotation angle
		uaxy.x = ua.x;
		uaxy.y = ua.y;
		uaxy.z = 0;


		cr_vector_normalization(
		&uaxy, &uaxy);


		dtmp = cr_vector_inner(&ux, &uaxy);
		thetaR_ = acos(dtmp);

		cr_vector_cross(&ux, &uaxy, &ur);
		if (ur.z < 0) {
			thetaR_ = - thetaR_;
		}


//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "theTaR_, angle: %lf\n", (thetaR_ * 180 / 3.14) );





		s = sin(thetaR_);
		c = cos(thetaR_);
		ra[0][0] = c; ra[0][1] = -s; ra[0][2] = 0;
		ra[1][0] = s; ra[1][1] =  c; ra[1][2] = 0;
		ra[2][0] = 0; ra[2][1] =  0; ra[2][2] = 1;

		ra_[0][0] = c;  ra_[0][1] =  s; ra_[0][2] = 0;
		ra_[1][0] = -s; ra_[1][1] =  c; ra_[1][2] = 0;
		ra_[2][0] = 0;  ra_[2][1] =  0; ra_[2][2] = 1;

#if 0
		mat33mult(rrm, ra, rrma);
#endif
#if 0
		mat33mult(ra, rrm, rrma);
#endif
#if 0
		mat33mult(rrm, ra, rrma_);
		mat33mult(ra_, rrma_, rrma);
#endif


#if 0
		mat33mult(ra_, rrm, rrma);
#endif
#if 0
		mat33mult(rrm, ra_, rrma);
#endif

#if 1
		// �׳� �̰ɷ� ���� ���ڱ���... ������.
		{
			int i, j;
			for (i = 0; i < 3; i++) {
				for (j = 0; j < 3; j++) {
					rrma[i][j] = rrm[i][j];
				}
			}
		}
#endif
	}
#if 0
	{
		double rrm0[3][3];
		cr_vector_t v1, v2;

		rodriguesMatrix(&ua, -thetaC_, rrm0);
		rodriguesRotate(rrm, &uz, &v1);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "(%10lf %10lf %10lf) -> (%10lf %10lf %10lf)\n", uz.x, uz.y, uz.z, v1.x, v1.y, v1.z);
		rodriguesRotate(rrm0, &v1, &v2);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "(%10lf %10lf %10lf) -> (%10lf %10lf %10lf)\n", v1.x, v1.y, v1.z, v2.x, v2.y, v2.z);


		mat3inv(rrma, rrm0);
		rodriguesRotate(rrma, &uz, &v1);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "(%10lf %10lf %10lf) -> (%10lf %10lf %10lf)\n", uz.x, uz.y, uz.z, v1.x, v1.y, v1.z);
		rodriguesRotate(rrm0, &v1, &v2);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "(%10lf %10lf %10lf) -> (%10lf %10lf %10lf)\n", v1.x, v1.y, v1.z, v2.x, v2.y, v2.z);
	}
#endif
	return res;
}







/*!
 ********************************************************************************
 *	@brief      CAM get RodriguesMatrix,   X-axis only
 *
 *  @param[in]	pcposL
 *              Camera position
 *  @param[in]	pbposL
 *              Ball position
 *
 *  @param[out]	rrm
 *              R matrix result
 *  @param[out]	psrv
 *              Sight Rotation vector
 *  @param[out]	psrt
 *              Sight Rotation angle
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/02/28
 *******************************************************************************/
I32 calcRodriguesMatrix2(
	cr_point_t *pcposL,
	cr_point_t *pbposL,
	double rrm[][3],
	cr_vector_t *psrv,
	double     *psrt
	)
{
	I32 res;
	cr_vector_t ucs;	// Cam sight unit-vector
	cr_vector_t uu;		// Up unit-vector
	cr_vector_t ua;		// axis unit-vector
	double thetaC_;
	double dtmp;

	//--------
	// Up unit vector
	uu.x = 0.0;
	uu.y = 0.0;
	uu.z = 1.0;

	//--------
	// Get Camera sight vector
	cr_vector_sub(pbposL, pcposL, &ucs); // v1 - v2. Cam sight vector

	dtmp = cr_vector_norm(&ucs);
	dtmp = 1 / dtmp;
	cr_vector_scalar(&ucs, dtmp, &ucs); // Cam sight unit vector


	// Axis vector... X only.
	ua.x = 1.0;
	ua.y = 0.0;
	ua.z = 0.0;

	//6. get rotation angle
	dtmp = cr_vector_inner(&ucs, &uu);
	thetaC_ = M_PI - acos(dtmp);

	//7. Rodrigues rotation... 

	memcpy(psrv, &ua, sizeof(cr_vector_t));
	*psrt = -thetaC_;
	res = rodriguesMatrix(&ua, -thetaC_, rrm);

	return res;
}

/*!
 ********************************************************************************
 *	@brief      CAM  RodriguesMatrix with rotation axis and angle
 *
 *  @param[in]	pua
 *              axis vector of rotation, unit vector
 *  @param[in]	theta
 *              rotation angle
 *  @param[out]	rrm
 *              R matrix result
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/02/28
 *******************************************************************************/
I32 rodriguesMatrix(cr_vector_t *pua, double theta, double rrm[][3])
{
	I32 res;
	double x, y, z;				// axis
	double c, s;				// c = cos(theta), s = sin(theta)

	double x2, y2, z2;
	double xy, yz, zx;

	//--
	x = pua->x;
	y = pua->y;
	z = pua->z;

	c = cos(theta);
	s = sin(theta);

	x2 = x*x; y2 = y*y; z2 = z*z;
	xy = x*y; yz = y*z; zx = z*x;
	// See https://en.wikipedia.org/wiki/Rotation_matrix#Rotation_matrix_from_axis_and_angle
	rrm[0][0] = x2*(1-c) +   c; 	rrm[0][1] = xy*(1-c) - z*s;		rrm[0][2] = zx*(1-c) + y*s;
	rrm[1][0] = xy*(1-c) + z*s; 	rrm[1][1] = y2*(1-c) +   c;		rrm[1][2] = yz*(1-c) - x*s;
	rrm[2][0] = zx*(1-c) - y*s; 	rrm[2][1] = yz*(1-c) + x*s;		rrm[2][2] = z2*(1-c) +   c;

	res = 1;

	//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "ua: %10lf %10lf %10lf    ang: %10lf\n", x, y, z, (theta*180/3.1415));
	return res;
}

/*!
 ********************************************************************************
 *	@brief      CAM  Rotation axis and angle with RodriguesMatrix
 *
 *  @param[in]	rrm
 *              R matrix result
 *  @param[out]	paxis
 *              axis vector of rotation, unit vector
 *  @param[out]	ptheta
 *              rotation angle
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/12/10
 *******************************************************************************/
I32 rodriguesMatrixInv(double rrm[][3], cr_vector_t *paxis, double *ptheta)
{
	double theta;

	double traceR;
	double dtmp;
	double sinTheta;
	I32 res;

	//--
	// see https://en.wikipedia.org/wiki/Axis%E2%80%93angle_representation#Log_map_from_SO.283.29_to_so.283.29
	traceR = rrm[0][0] + rrm[1][1] + rrm[2][2];		// Trace of (R)
	dtmp = (traceR - 1.0) / 2.0;
	//if (dtmp > 1.0)  { dtmp =  1.0; }
	//if (dtmp < -1.0) { dtmp = -1.0; }

#define DTMP_MAX	(1.001)
//#define DTMP_MAX	(1.05)
	if (dtmp > 1/DTMP_MAX && dtmp < DTMP_MAX) {
		//dtmp = 1.0;
		dtmp = 1/DTMP_MAX;
	}
	if (dtmp > 1.0 || dtmp < -1.0) {
		res = 0;
		goto func_exit;
	}

	theta = acos(dtmp);

	sinTheta = sin(theta);
	*ptheta = theta;
#define MINVALUE	(1e-10)
	if (sinTheta > -MINVALUE  && sinTheta < MINVALUE) {
		paxis->x = 1; paxis->y = 0; paxis->z = 0;
	} else {
		paxis->x = (rrm[2][1] - rrm[1][2]) / (2*sinTheta);
		paxis->y = (rrm[0][2] - rrm[2][0]) / (2*sinTheta);
		paxis->z = (rrm[1][0] - rrm[0][1]) / (2*sinTheta);
	}

	cr_vector_normalization(paxis, paxis);

	res = 1;
func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      CAM  Rodrigues rotation
 *
 *  @param[in]	rrm
 *              R matrix 
 *  @param[in]	pvfrom
 *              input vector
 *  @param[out]	pvto
 *              output vector
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/02/29
 *******************************************************************************/
I32 rodriguesRotate(double rrm[][3], cr_vector_t *pvfrom, cr_vector_t *pvto)
{
	I32 res;
	I32 i, j;
	cr_vector_t v;

	//pvto->v[0] = 0.0;
	//pvto->v[1] = 0.0;
	//pvto->v[2] = 0.0;
	//for (i = 0; i < 3; i++) {
	//	for (j = 0; j < 3; j++) {
	//		pvto->v[i] += rrm[i][j] * pvfrom->v[j];
	//	}
	//}

//	mat3v3mult(rrm, pvfrom->v, v.v) ;
	mat3v3mult(rrm, pvfrom->v, pvto->v) ;

	res = 1;

	return res;
}


I32 rotateV(double rrm[][3], cr_vector_t *pvfrom, cr_vector_t *pvto)
{
	I32 res;
	I32 i, j;
	cr_vector_t v;

	mat3v3mult(rrm, pvfrom->v, pvto->v) ;

	res = 1;

	return res;
}

//static int s_getplane_ = 0;
//static int s_getplane_ = 1;
//static int s_getplane_ = 2;
static int s_getplane_ = 3;


//------
U32 getplane(cr_point_t *array, U32 arraylen, cr_plane_t *pplane)
{
	U32 res;

	if (s_getplane_ == 0) {
		res = getplane0(array, arraylen, pplane);
	} else if (s_getplane_ == 1) {
		res = getplane1(array, arraylen, pplane);
	} else if (s_getplane_ == 2) {
		res = getplane2(array, arraylen, pplane);
	} else if (s_getplane_ == 3) {
		res = getplane3(array, arraylen, pplane);
	} else {
		res = 0;
	}

	return res;
}

U32 getplane0(cr_point_t *array, U32 arraylen, cr_plane_t *pplane)
{
	U32 res;
	U32 i, j, k;

	double x, y, z;
	double *pa;
	double *pb;

	double AtA[3][3];
	double X[3][3];
	double Y[3];
	double Z[3];

	double a_, b_, c_, d_;
	double sumx, sumy, sumz;

	double det;


	pa = (double *) malloc(3 * sizeof(double) * arraylen);
	pb = (double *) malloc(1 * sizeof(double) * arraylen);


	sumx = 0.0;
	sumy = 0.0;
	sumz = 0.0;
	for (i = 0; i < arraylen; i++) {
		x = (array+i)->x;
		y = (array+i)->y;
		z = (array+i)->z;

		*(pa + i * 3 + 0) = x - y;
		*(pa + i * 3 + 1) = z;
		*(pa + i * 3 + 2) = 1.0;

		*(pb + i + 0) = -y;

		sumx += x;
		sumy += y;
		sumz += z;
	}

	// AT * A
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
//			AtA[i][j] = sum(k = 0 ~ arraylen-1) (a[k][i] * a[k][j]);
			AtA[i][j] = 0.0;
			for (k = 0; k < arraylen; k++) {
				//AtA[i][j] += a[k][i] * a[k][j];
				AtA[i][j] += (*(pa + k * 3 + i)) * (*(pa + k * 3 + j));
			}
		}
	}

	// X = inv(AT*A)
	det =  mat3inv(
		AtA, 		// input
		X		// output
		);

#define VERYSMALL	(1e-15)
	if (det < VERYSMALL && det > -VERYSMALL) {
		free(pa);
		free(pb);
		res = 0;
		goto func_exit;
	}
	// AT * B = Y

	for (i = 0; i < 3; i++) {

		Y[i] = 0;
		for (k = 0; k < arraylen; k++) {
			//Y[i] += a[k][i] * bk;
			Y[i] += (*(pa + k * 3 + i)) * (*(pb + k + 0));
		}
	}

	// Z = X * Y 

	for (i = 0; i < 3; i++) {
		Z[i] = 0;
		for (k = 0; k < 3; k++) {
			Z[i] += X[i][k] * Y[k];
		}
	}

	a_ = Z[0];			
	b_ = 1 - Z[0];		// b = 1 - a;
	c_ = Z[1];			
	d_ = Z[2];			

	if (a_ < 0) {		//..
		a_ = -a_;
		b_ = -b_;
		c_ = -c_;
		d_ = -d_;
	}

	pplane->n.x = a_;
	pplane->n.y = b_;
	pplane->n.z = c_;

	cr_vector_normalization(&pplane->n, &pplane->n);



	//-------
	pplane->p0.x = sumx / arraylen;
	pplane->p0.y = sumy / arraylen;
	pplane->p0.z = sumz / arraylen;

	free(pa);
	free(pb);
	res = 1;
func_exit:
	return res;
}

/*!
 ********************************************************************************
 *	@brief      Get Plane..
 *
 *  @param[in]	array
 *              array of point
 *  @param[in]	arraylen
 *              count of array
 *  @param[out]	pplane
 *              plane
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/08/02
 *******************************************************************************/
U32 getplane1(cr_point_t *array, U32 arraylen, cr_plane_t *pplane)
{
	U32 res;
	U32 i, j, k;

	double x, y, z;
	double *pa;
	double *pb;

	double AtA[3][3];
	double X[3][3];
	double Y[3];
	double Z[3];

	double a_, b_, c_, d_;
	double a__, b__, c__;

	double det;


	pa = (double *) malloc(3 * sizeof(double) * arraylen);
	pb = (double *) malloc(1 * sizeof(double) * arraylen);


	for (i = 0; i < arraylen; i++) {
		x = (array+i)->x;
		y = (array+i)->y;
		z = (array+i)->z;

		*(pa + i * 3 + 0) = x - y;
		*(pa + i * 3 + 1) = z;
		*(pa + i * 3 + 2) = 1.0;

		*(pb + i + 0) = -y;
	}

	// AT * A
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
//			AtA[i][j] = sum(k = 0 ~ arraylen-1) (a[k][i] * a[k][j]);
			AtA[i][j] = 0.0;
			for (k = 0; k < arraylen; k++) {
				//AtA[i][j] += a[k][i] * a[k][j];
				AtA[i][j] += (*(pa + k * 3 + i)) * (*(pa + k * 3 + j));
			}
		}
	}

	// X = inv(AT*A)
	det =  mat3inv(
		AtA, 		// input
		X		// output
		);

#define VERYSMALL	(1e-15)
	if (det < VERYSMALL && det > -VERYSMALL) {
		free(pa);
		free(pb);
		res = 0;
		goto func_exit;
	}
	// AT * B = Y

	for (i = 0; i < 3; i++) {

		Y[i] = 0;
		for (k = 0; k < arraylen; k++) {
			//Y[i] += a[k][i] * bk;
			Y[i] += (*(pa + k * 3 + i)) * (*(pb + k + 0));
		}
	}

	// Z = X * Y 

	for (i = 0; i < 3; i++) {
		Z[i] = 0;
		for (k = 0; k < 3; k++) {
			Z[i] += X[i][k] * Y[k];
		}
	}

	a_ = Z[0];			
	b_ = 1 - Z[0];		// b = 1 - a;
	c_ = Z[1];			
	d_ = Z[2];			

	if (a_ < 0) {		//..
		a_ = -a_;
		b_ = -b_;
		c_ = -c_;
		d_ = -d_;
	}

	pplane->n.x = a_;
	pplane->n.y = b_;
	pplane->n.z = c_;

	cr_vector_normalization(&pplane->n, &pplane->n);

	a__ = a_;
	b__ = b_;
	c__ = c_;
	if (a__ < 0) { a__ = - a__; }
	if (b__ < 0) { b__ = - b__; }
	if (c__ < 0) { c__ = - c__; }

	pplane->p0.x = pplane->p0.y = pplane->p0.z = 0.0;
	if (a__ < VERYSMALL && b__ < VERYSMALL && c__ < VERYSMALL) {
		// what?
	} else {
		if (a__ <= c__ && b__ <= c__ ) {
				pplane->p0.z = -d_/ c_;
		} else {
			if (a__ <= b__) {
				pplane->p0.y = -d_/ b_;
			} else {
				pplane->p0.x = -d_/ a_;
			}
		}
	}

	free(pa);
	free(pb);
	res = 1;
func_exit:
	return res;
}


/*!
 ********************************************************************************
 *	@brief      Get Plane, 2
 *
 *  @param[in]	array
 *              array of point
 *  @param[in]	arraylen
 *              count of array
 *  @param[out]	pplane
 *              plane
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/08/22
 *******************************************************************************/
U32 getplane2(cr_point_t *array, U32 arraylen, cr_plane_t *pplane)
{
	U32 res;
	U32 i, j, k;

	double x, y, z;
	double *pa;
	double *pb;

	double AtA[3][3];
	double X[3][3];
	double Y[3];
	double Z[3];

	double a_, b_, c_, d_;
	double a__, b__, c__;

	double det;


	pa = (double *) malloc(3 * sizeof(double) * arraylen);
	pb = (double *) malloc(1 * sizeof(double) * arraylen);


	for (i = 0; i < arraylen; i++) {
		x = (array+i)->x;
		y = (array+i)->y;
		z = (array+i)->z;

		//*(pa + i * 3 + 0) = x - y;
		//*(pa + i * 3 + 1) = z;
		*(pa + i * 3 + 0) = x;
		*(pa + i * 3 + 1) = y - z;
		*(pa + i * 3 + 2) = 1.0;

		//*(pb + i + 0) = -y;
		*(pb + i + 0) = -z;
	}

	// AT * A
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
//			AtA[i][j] = sum(k = 0 ~ arraylen-1) (a[k][i] * a[k][j]);
			AtA[i][j] = 0.0;
			for (k = 0; k < arraylen; k++) {
				//AtA[i][j] += a[k][i] * a[k][j];
				AtA[i][j] += (*(pa + k * 3 + i)) * (*(pa + k * 3 + j));
			}
		}
	}

	// X = inv(AT*A)
	det =  mat3inv(
		AtA, 		// input
		X		// output
		);

#define VERYSMALL	(1e-15)
	if (det < VERYSMALL && det > -VERYSMALL) {
		free(pa);
		free(pb);
		res = 0;
		goto func_exit;
	}
	// AT * B = Y

	for (i = 0; i < 3; i++) {

		Y[i] = 0;
		for (k = 0; k < arraylen; k++) {
			//Y[i] += a[k][i] * bk;
			Y[i] += (*(pa + k * 3 + i)) * (*(pb + k + 0));
		}
	}

	// Z = X * Y 

	for (i = 0; i < 3; i++) {
		Z[i] = 0;
		for (k = 0; k < 3; k++) {
			Z[i] += X[i][k] * Y[k];
		}
	}

	a_ = Z[0];			
	b_ = Z[1];		
	c_ = 1 - Z[1];	// c = 1 - b;

	//b_ = 1 - Z[0];		// b = 1 - a;
	//c_ = Z[1];			
	d_ = Z[2];			

	if (a_ < 0) {		//..
		a_ = -a_;
		b_ = -b_;
		c_ = -c_;
		d_ = -d_;
	}

	pplane->n.x = a_;
	pplane->n.y = b_;
	pplane->n.z = c_;

	cr_vector_normalization(&pplane->n, &pplane->n);

	a__ = a_;
	b__ = b_;
	c__ = c_;
	if (a__ < 0) { a__ = - a__; }
	if (b__ < 0) { b__ = - b__; }
	if (c__ < 0) { c__ = - c__; }

	pplane->p0.x = pplane->p0.y = pplane->p0.z = 0.0;
	if (a__ < VERYSMALL && b__ < VERYSMALL && c__ < VERYSMALL) {
		// what?
	} else {
		if (a__ <= c__ && b__ <= c__ ) {
				pplane->p0.z = -d_/ c_;
		} else {
			if (a__ <= b__) {
				pplane->p0.y = -d_/ b_;
			} else {
				pplane->p0.x = -d_/ a_;
			}
		}
	}

	free(pa);
	free(pb);
	res = 1;
func_exit:
	return res;
}




/*!
 ********************************************************************************
 *	@brief      Get Plane, 3
 *
 *  @param[in]	array
 *              array of point
 *  @param[in]	arraylen
 *              count of array
 *  @param[out]	pplane
 *              plane
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2016/08/22
 *******************************************************************************/
U32 getplane3(cr_point_t *array, U32 arraylen, cr_plane_t *pplane)
{
	U32 res;
	U32 i, j, k;

	double x, y, z;
	double *pa;
	double *pb;

	double AtA[3][3];
	double X[3][3];
	double Y[3];
	double Z[3];

	double a_, b_, c_, d_;
	double a__, b__, c__;

	double det;


	pa = (double *) malloc(3 * sizeof(double) * arraylen);
	pb = (double *) malloc(1 * sizeof(double) * arraylen);


	for (i = 0; i < arraylen; i++) {
		x = (array+i)->x;
		y = (array+i)->y;
		z = (array+i)->z;

		//*(pa + i * 3 + 0) = x - y;
		//*(pa + i * 3 + 1) = z;
		*(pa + i * 3 + 0) = x;
		*(pa + i * 3 + 1) = y - z;
		*(pa + i * 3 + 2) = 1.0;

		//*(pb + i + 0) = -y;
		*(pb + i + 0) = -z;
	}

	// AT * A
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 3; j++) {
//			AtA[i][j] = sum(k = 0 ~ arraylen-1) (a[k][i] * a[k][j]);
			AtA[i][j] = 0.0;
			for (k = 0; k < arraylen; k++) {
				//AtA[i][j] += a[k][i] * a[k][j];
				AtA[i][j] += (*(pa + k * 3 + i)) * (*(pa + k * 3 + j));
			}
		}
	}

	// X = inv(AT*A)
	det =  mat3inv(
		AtA, 		// input
		X		// output
		);

#define VERYSMALL	(1e-15)
	if (det < VERYSMALL && det > -VERYSMALL) {
		free(pa);
		free(pb);
		res = 0;
		goto func_exit;
	}
	// AT * B = Y

	for (i = 0; i < 3; i++) {

		Y[i] = 0;
		for (k = 0; k < arraylen; k++) {
			//Y[i] += a[k][i] * bk;
			Y[i] += (*(pa + k * 3 + i)) * (*(pb + k + 0));
		}
	}

	// Z = X * Y 

	for (i = 0; i < 3; i++) {
		Z[i] = 0;
		for (k = 0; k < 3; k++) {
			Z[i] += X[i][k] * Y[k];
		}
	}

	a_ = Z[0];			
	b_ = Z[1];		
	c_ = 1 - Z[1];	// c = 1 - b;

	//b_ = 1 - Z[0];		// b = 1 - a;
	//c_ = Z[1];			
	d_ = Z[2];			

	if (a_ < 0) {		//..
		a_ = -a_;
		b_ = -b_;
		c_ = -c_;
		d_ = -d_;
	}

	pplane->n.x = a_;
	pplane->n.y = b_;
	pplane->n.z = c_;

	cr_vector_normalization(&pplane->n, &pplane->n);


	//--
	pplane->p0.x = (array)->x;			// First Point.. (CAM!!!!)
	pplane->p0.y = (array)->y;
	pplane->p0.z = (array)->z;

	free(pa);
	free(pb);
	res = 1;
func_exit:
	return res;
}


I32 calcaxis(cr_vector_t planenormal[], U32 planenormalvalid[], U32 planenormallength[], cr_vector_t *paxis0, cr_vector_t *paxis)
{
	I32 res;
	U32 i;
	I32 count;
	I32 count2;
	//double distance;
	cr_vector_t vmean;

	U32 valid[MARKCOUNT];
	cr_vector_t planenormalDirection[MARKCOUNT];

	count = 0;
	memset(&valid[0], 0, sizeof(U32) * MARKCOUNT);
	for (i = 0; i < MARKCOUNT; i++) {
		valid[i] = planenormalvalid[i];
		if (valid[i]) {
			double dtmp;
			count++;
			memcpy(&planenormalDirection[i], &planenormal[i], sizeof(cr_vector_t));
			dtmp = cr_vector_inner(&planenormal[i], paxis0);
			if (dtmp < 0) {
				planenormalDirection[i].x *= -1;
				planenormalDirection[i].y *= -1;
				planenormalDirection[i].z *= -1;
			}
		}
	}

	res = 0;
	if (count == 0) {
		res = count;
		goto func_exit;
	}

	while(count > 2) {
		double maxd;
		U32 maxi;
		vmean.x = vmean.y = vmean.z = 0.0;
		count = 0;
		count2 = 0;
		for (i = 0; i < MARKCOUNT; i++) {
			if (valid[i]) {
				vmean.x += planenormalDirection[i].x * planenormallength[i];
				vmean.y += planenormalDirection[i].y * planenormallength[i];
				vmean.z += planenormalDirection[i].z * planenormallength[i];
				count ++;
				count2 +=  planenormallength[i];
			}
		}

		cr_vector_normalization(&vmean, &vmean);
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "--------\n");
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%d   %10lf %10lf %10lf\n", count, vmean.x, vmean.y, vmean.z);
		if (count == 2) {
			paxis->x = vmean.x;
			paxis->y = vmean.y;
			paxis->z = vmean.z;
			break;
		}

		maxd = 0.0;
		maxi = 0;
		for (i = 0; i < MARKCOUNT; i++) {
			if (valid[i]) {
				double dx, dy, dz;
				double dv;

				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d]%10lf %10lf %10lf\n", i, planenormalDirection[i].x, planenormalDirection[i].y, planenormalDirection[i].z);
				dx = vmean.x - planenormalDirection[i].x;
				dy = vmean.y - planenormalDirection[i].y;
				dz = vmean.z - planenormalDirection[i].z;

				dv = sqrt(dx*dx + dy*dy + dz*dz);

				if (dv >= maxd) {
					maxd = dv;
					maxi = i;
				}
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "         dv: %lf (maxd: %lf, maxi: %d)\n", dv, maxd, maxi);
			}
		}
//#define MAXDVALUE	0.5
#define MAXDVALUE	0.3
		if (maxd > MAXDVALUE) {
			valid[maxi] = 0;
		} else {
			paxis->x = vmean.x;
			paxis->y = vmean.y;
			paxis->z = vmean.z;
			break;
		}
	}


//#define ZMULT1	(0.2)
//#define ZMULT2	(0.8)

#if 0
#define ZMULT1	(0.6)
#define ZMULT2	(0.8)

	if (count == 1) {
		paxis->z *= ZMULT1;
	} else if (count == 2) {
		paxis->z *= ZMULT2;
	}
#endif
	{
		int zsign;
		double zabs;

		if (paxis->z >= 0) {
			zsign = 1;
			zabs = paxis->z;
		} else {
			zsign = -1;
			zabs = -paxis->z;
		}
		
		if (count == 1) {
			// 0      ->  0
			// 0.1    ->  0.10
			// 0.2    ->  0.20
			// 0.3    ->  0.25
			// 0.6    ->  0.40
			// 1.0    ->  0.50
			if (zabs < 0.1) {
				zabs = LINEAR_SCALE(zabs, 0, 0, 0.1, 0.1);
			} else if (zabs < 0.2) {
				zabs = LINEAR_SCALE(zabs, 0.10, 0.10, 0.20, 0.20);
			} else if (zabs < 0.3) {
				zabs = LINEAR_SCALE(zabs, 0.20, 0.20, 0.30, 0.25);
			} else if (zabs < 0.6) {
				zabs = LINEAR_SCALE(zabs, 0.30, 0.25, 0.60, 0.40);
			} else if (zabs < 1.0) {
				zabs = LINEAR_SCALE(zabs, 0.60, 0.40, 1.00, 0.50);
			} else {
				zabs = 0.5;
			}
		} else if (count == 2) {
			// 0      ->  0
			// 0.3    ->  0.30
			// 0.6    ->  0.50
			// 1.0    ->  0.80
			if (zabs < 0.3) {
				zabs = LINEAR_SCALE(zabs, 0, 0, 0.3, 0.3);
			} else if (zabs < 0.6) {
				zabs = LINEAR_SCALE(zabs, 0.30, 0.30, 0.60, 0.50);
			} else if (zabs < 1.0) {
				zabs = LINEAR_SCALE(zabs, 0.60, 0.50, 1.00, 0.80);
			} else {
				zabs = 0.80;
			}
		} else {
			//
		}

		paxis->z = zsign * zabs;
	}

	cr_vector_normalization(paxis, paxis);

	memcpy(&planenormalvalid[0], &valid[0], sizeof(U32) * MARKCOUNT);
	res = count;
func_exit:
	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__" res: %d,  %lf %lf %lf\n", 
			res,
			paxis->x,
			paxis->y,
			paxis->z
			);
	return res;
}

/****

General solution: For each pixel in the destination image, take the pixel in the source image with coordinates of the destination pixel, rotated in the opposite direction.

Enhancement to solution: The rotation usually won't give exact pixel coordinates. Do a weighted average of the source pixel with its neighbors, according to the percentage it overlaps them.

Faster solution for binary images: Convert the image into "runs" of consecutive foreground pixels. Then rotate the endpoints of these lines and draw them into the destination.

Normally this will produce slight gaps due to integer roundoff, so when one or both endpoints are more than 10% away from an integer, patch by drawing TWO lines for the single source line, using the integer coordinates rounded up and down.

If one endpoint is within 10% and the other isn't, the two lines will form a 'V' shape. If both are off by more than 10%, the two lines will form an 'X' shape.

This can be done with respect to the X axis or the Y axis . Use the one with the smallest angle between the axis and the rotation angle. (I.e. if the rotation angle is between 45 and -45, use the X axis.)

Still faster solution for binary images: If there are fewer background pixels than foreground pixels, fill the destination with foreground, and follow the above algorithm with background pixels.


https://stackoverflow.com/questions/484573/image-rotation-algorithm

*****/
#if defined (__cplusplus)
}
#endif
