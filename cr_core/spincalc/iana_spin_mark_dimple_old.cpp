/*!
 *******************************************************************************
                                                                                
                    CREATZ IANA shot calc.
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2015 ~ 2016 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   iana_spin_mark.cpp
	 @brief  IANA
	 @author Original: by yhsuk
	 @date   2015/12/26 Spin mark

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#include <math.h>
#include "cr_common.h"
#include "cr_thread.h"
#include "cr_osapi.h"
#include "cr_nanoxml.h"

#ifdef IPP_SUPPORT
#include <ipp.h>
#include <ippcc.h>
#endif

#include "scamif.h"
//#include "scamif_main.h"
#include "scamif_buffer.h"

#include "iana.h"
#include "iana_work.h"
#include "iana_implement.h"
#include "iana_cam.h"
#include "iana_cam_implement.h"
#include "iana_balldetect.h"
#include "iana_shot.h"
#include "iana_tool.h"
#include "iana_spin.h"
#include "iana_spin_rotation.h"
#include "iana_spin_ballclub.h"
// #include "iana_club_attack.h"

#include "iana_coordinate.h"
#include "cr_regression.h"
#include "cr_matrix.h"


#include <opencv2/imgproc.hpp>
/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#pragma warning(disable:4456)
#if defined (__cplusplus)
extern "C" {
#endif

	

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/
static int s_usebmi = 0;	
static int s_use_rL3 = 1;

static double s_c0addx = 0;
static double s_c0addy = 0;
/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the function prototype 
 -----------------------------------------------------------------------------*/

static U32 AdjustContrast(
		U08 *imgbufD,
		U08 *imgbufB,
		U08 *imgbufF);

static I32 GetBinDimpleImg_Legacy2(
		U08 *imgbufSrcA,
		U08 *imgbufSrcB,
		U08 *imgbufResultA,
		U08 *imgbufResultB,
		U32 radius,
		I32 adaptive_threshold_constantA,
		I32 adaptive_threshold_constantB
		);

static I32 GetBinDimpleImg_Legacy3(
		iana_t *piana,

		U08 *imgbufSrcA,
		U08 *imgbufSrcB,
		U08 *imgbufResultA,
		U08 *imgbufResultB,
		U32 radius,
		I32 adaptive_threshold_constantA,
		I32 adaptive_threshold_constantB
		);

static void GetBinDimpleImg(
    iana_t *piana,
    U08 *imgbufSrc,
    U08 *imgOthermark,
    U08 *imgbufResult,
    I32 adaptive_threshold_constant);

static U32 CalcLabelSegment(
		U08 *imgSrc,
		U08 *imgLabel,
		U32 maxlabelcount,
		imagesegment_t *pimgseg,
		U32 *segcount);
#if 0
static U32 SelectDimple_Legacy(
	imagesegment_t *pimgseg,
	U32 maxlabelcount,
	U32 radius
	);
#endif
static U32 SelectDimple(
    imagesegment_t *pimgseg,
    U32 maxlabelcount,
    U32 radius);

static U32 RefineDimpleWithLightRay(
		imagesegment_t imgsegOrg[],
		imagesegment_t imgsegResult[],
		double kvalue,
		U32 maxlabels,
		U32 diameter);

U32 SetCrosshair(
    U08 *imgbufSrc,
    U32 width, U32 height,
    U32 thickness);

static U32 ConvertCoordiImageToWorld_DimpleOthermark(iana_t *piana, U32 camid, 
				imagesegment_t imgsegDimple[],
				imagesegment_t imgsegMark[],
				U32 seqnum,
				U32 maxlabelcount,
				double multfact,
				double diameter);

static U32 ConvertCoordiImageToWorld(iana_t *piana, U32 camid,
				imagesegment_t imgseg[],
				U32 seqnum,
				U32 maxlabelcount,
				double multfact,
				double diameter,
				markelement2_t	*pmke2		// O
				);

static U32 ConvertCoordiLocalToBall(
		iana_t *piana, 				//I
		cr_point_t *pcampos, 		//I
		cr_point_t *pballposL3D,	//I
		point_t *pmarkL,			//I
		double lr_,					//I
		cr_point_t *pmark3DwrtBall	// 3D position, wrt ball.
		);

static U32 SelectOthermark(
	imagesegment_t *pimgseg,
	U32 maxlabelcount,
	U32 radius,
	U32 dvalue			// divided value
	);

static U32 recalcDimpleWithLightRay(
		imagesegment_t *pisOrg,
		imagesegment_t *pisRes,
		double kvalue,
		U32 diameter);

extern U32 modif_barnoise(U08* buf, I32 width, I32 height, I32 barStartX, I32 barEndX);

#if 1
/*!
 ********************************************************************************
 *	@brief      Ball mark P position to 3Dmark, using line-sphere interconnection
 *
 *  @param[in]	piana
 *              IANA module handle
 *
 *  @return		1: good, 0: no-good
 *
 *	@author	    yhsuk
 *  @date       2017/0711
 *******************************************************************************/
static U32 ConvertCoordiLocalToBall(
		iana_t *piana, 				//I
		cr_point_t *pcampos, 		//I
		cr_point_t *pballposL3D,	//I
		point_t *pmarkL,			//I
		double lr_,					//I
		cr_point_t *pmark3DwrtBall	// 3D position, wrt ball.
		)
{
	U32 res;
	cr_line_t cmline;	// Line between Camera and mark L position
	cr_point_t markL;
	cr_sphere_t sp;

	cr_point_t p[2];
	cr_point_t mark3D;
	U32 count;
	double det;
	//---

	markL.x = pmarkL->x;
	markL.y = pmarkL->y;
	markL.z = 0.0;

	// make line between Cam and mark L position
	cr_line_p0p1(pcampos, &markL, &cmline);

	// Make Ball sphere. :)
	cr_sphere_make(pballposL3D, lr_, &sp);

	// Get 3D position
	res = cr_line_sphere_mild(&cmline, &sp, &p[0], &count, &det);

	if (res != 0) {
		if (count == 1) {
			mark3D.x = p[0].x; mark3D.y = p[0].y; mark3D.z = p[0].z;
		} else if (count == 2) {
			int i;
			double dx, dy, dz;
			double dist2[2];

			for (i = 0; i < 2; i++) {
				dx = pcampos->x - p[i].x;			// p[0].x -> p[i].x   Error corrected. 
				dy = pcampos->y - p[i].y;
				dz = pcampos->z - p[i].z;

				dist2[i] = dx*dx + dy*dy + dz*dz;
			}

			if (dist2[0] < dist2[1]) {		// select near point.
				memcpy(&mark3D, &p[0], sizeof(cr_point_t));
			} else {
				memcpy(&mark3D, &p[1], sizeof(cr_point_t));
			}
#if 0
			if (p[0].z > p[1].z) {
				mark3D.x = p[0].x; mark3D.y = p[0].y; mark3D.z = p[0].z;
			} else {
				mark3D.x = p[1].x; mark3D.y = p[1].y; mark3D.z = p[1].z;
			}
#endif
		} else {
			res = 0;
		}
	}
	if (res != 0) {
		cr_vector_sub(&mark3D, pballposL3D, pmark3DwrtBall);
	}
#if 0
	{
		double x, y, z;
		double mag;
		x = pmark3DwrtBall->x;
		y = pmark3DwrtBall->y;
		z = pmark3DwrtBall->z;
		mag = sqrt(x*x + y*y + z*z);
		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mark3D2 %10lf %10lf %10lf  (mag: %lf)\n", 
				x,
				y,
				z,
				mag);
	}
#endif
	piana;
	return res;
}
#endif




/*
 *
----------------------------
1-Line		(-0.10)
----------------------------
44.13	2.87	44.46	3.40
46.45	5.24	47.37	4.33
45.33	15.96	45.86	14.98
45.27	15.60	45.60	15.39
47.89	7.09	48.95	6.24


11.24	33.73	11.16	32.42
11.87	34.65	11.77	34.24
11.03	33.02	10.97	33.04
10.55	33.30	10.53	32.53
11.09	31.11	11.03	29.92

----------------------------
2-Line		(0.00)
----------------------------

54.59	4.20	55.09	4.64
55.02	15.35	55.44	15.17
57.88	8.74	58.34	8.81
56.40	10.98	56.92	10.81
55.00	10.01	55.42	9.98


57.15	8.43	57.60	8.14
51.04	17.02	51.61	16.33
55.06	5.47	55.60	5.43



33.65	15.27	36.95	14.46
41.29	10.67	41.74	10.23
45.08	9.23	45.45	9.44
47.26	14.01	47.87	13.51
46.30	16.12	46.61	15.55
46.29	8.12	46.73	7.89

Driver
47.16	22.17	47.33	22.27
48.87	18.35	49.10	18.91
48.15	21.08	48.39	20.83
47.65	20.88	47.96	20.77
47.21	6.38	47.70	6.18

11.70	34.13	11.70	34.21	
17.32	34.70	17.20	34.03
20.45	33.95	20.29	33.85
17.92	40.38	17.81	40.22
18.02	37.16	17.91	36.69

28.38	31.99	28.29	31.79
30.83	31.99	30.78	31.78
30.65	31.71	30.10	31.41
30.76	27.57	30.28	27.33
29.03	31.08	28.63	31.14


3.31	34.83	3.34	34.42
3.16	34.15	3.06	34.17
6.74	38.05	6.65	35.13
5.09	36.78	5.11	38.94
5.76	36.45	5.72	36.31










----------------------------
4-Line	(+0.20)
----------------------------
45.75	7.55	45.57	7.53
47.85	16.05	47.53	15.32
45.90	5.03	45.76	4.32
46.96	0.74	46.78	0.81
46.63	13.35	46.23	12.65



30.78	23.10	30.40	22.99






21.76	28.09	21.49	28.17
20.36	27.84	20.17	28.26
18.72	29.98	18.60	30.77
22.29	22.81	21.98	21.97
19.99	24.87	19.74	24.82

4.37	22.84	4.34	24.38
7.79	23.22	7.56	24.34
5.94	17.73	5.84	16.56
4.25	19.45	4.25	20.90
3.80	20.05	3.59	20.31

 */

//----------------------------------------------------------------------------
#define ADWSIZE	19
static const 
/*float */
double 
adjust_const_filter_double[ADWSIZE*ADWSIZE] = {
-0.0277008 ,-0.02770065,-0.02770004,-0.02769793,-0.02769211,-0.02767937,-0.02765761,-0.02762958,-0.02760465,-0.02759453,-0.02760465,-0.02762958,-0.02765761,-0.02767937,-0.02769211,-0.02769793,-0.02770004,-0.02770065,-0.0277008 ,
-0.02770065,-0.02769986,-0.0276965 ,-0.02768493,-0.02765307,-0.02758335,-0.02746426,-0.0273108 ,-0.02717434,-0.02711897,-0.02717434,-0.0273108 ,-0.02746426,-0.02758335,-0.02765307,-0.02768493,-0.0276965 ,-0.02769986,-0.02770065,
-0.02770004,-0.0276965 ,-0.02768141,-0.02762958,-0.02748678,-0.02717434,-0.02664061,-0.02595282,-0.02534126,-0.0250931 ,-0.02534126,-0.02595282,-0.02664061,-0.02717434,-0.02748678,-0.02762958,-0.02768141,-0.0276965 ,-0.02770004,
-0.02769793,-0.02768493,-0.02762958,-0.02743938,-0.0269154 ,-0.02576898,-0.02381055,-0.02128685,-0.01904286,-0.01813229,-0.01904286,-0.02128685,-0.02381055,-0.02576898,-0.0269154 ,-0.02743938,-0.02762958,-0.02768493,-0.02769793,
-0.02769211,-0.02765307,-0.02748678,-0.0269154 ,-0.02534126,-0.02189722,-0.01601379,-0.00843216,-0.00169084, 0.00104465,-0.00169084,-0.00843216,-0.01601379,-0.02189722,-0.02534126,-0.0269154 ,-0.02748678,-0.02765307,-0.02769211,
-0.02767937,-0.02758335,-0.02717434,-0.02576898,-0.02189722,-0.01342625, 0.00104465, 0.01969246, 0.03627342, 0.04300165, 0.03627342, 0.01969246, 0.00104465,-0.01342625,-0.02189722,-0.02576898,-0.02717434,-0.02758335,-0.02767937, 
-0.02765761,-0.02746426,-0.02664061,-0.02381055,-0.01601379, 0.00104465, 0.03018547, 0.06773754, 0.10112749, 0.11467648, 0.10112749, 0.06773754, 0.03018547, 0.00104465,-0.01601379,-0.02381055,-0.02664061,-0.02746426,-0.02765761, 
-0.02762958,-0.0273108 ,-0.02595282,-0.02128685,-0.00843216, 0.01969246, 0.06773754, 0.12965044, 0.18470117, 0.20703968, 0.18470117, 0.12965044, 0.06773754, 0.01969246,-0.00843216,-0.02128685,-0.02595282,-0.0273108 ,-0.02762958,  
-0.02760465,-0.02717434,-0.02534126,-0.01904286,-0.00169084, 0.03627342, 0.10112749, 0.18470117, 0.25901187, 0.28916571, 0.25901187, 0.18470117, 0.10112749, 0.03627342,-0.00169084,-0.01904286,-0.02534126,-0.02717434,-0.02760465, 
-0.02759453,-0.02711897,-0.0250931 ,-0.01813229, 0.00104465, 0.04300165, 0.11467648, 0.20703968, 0.28916571, 0.32249086, 0.28916571, 0.20703968, 0.11467648, 0.04300165, 0.00104465,-0.01813229,-0.0250931 ,-0.02711897,-0.02759453, 
-0.02760465,-0.02717434,-0.02534126,-0.01904286,-0.00169084, 0.03627342, 0.10112749, 0.18470117, 0.25901187, 0.28916571, 0.25901187, 0.18470117, 0.10112749, 0.03627342,-0.00169084,-0.01904286,-0.02534126,-0.02717434,-0.02760465, 
-0.02762958,-0.0273108 ,-0.02595282,-0.02128685,-0.00843216, 0.01969246, 0.06773754, 0.12965044, 0.18470117, 0.20703968, 0.18470117, 0.12965044, 0.06773754, 0.01969246,-0.00843216,-0.02128685,-0.02595282,-0.0273108 ,-0.02762958,  
-0.02765761,-0.02746426,-0.02664061,-0.02381055,-0.01601379, 0.00104465, 0.03018547, 0.06773754, 0.10112749, 0.11467648, 0.10112749, 0.06773754, 0.03018547, 0.00104465,-0.01601379,-0.02381055,-0.02664061,-0.02746426,-0.02765761, 
-0.02767937,-0.02758335,-0.02717434,-0.02576898,-0.02189722,-0.01342625, 0.00104465, 0.01969246, 0.03627342, 0.04300165, 0.03627342, 0.01969246, 0.00104465,-0.01342625,-0.02189722,-0.02576898,-0.02717434,-0.02758335,-0.02767937, 
-0.02769211,-0.02765307,-0.02748678,-0.0269154 ,-0.02534126,-0.02189722,-0.01601379,-0.00843216,-0.00169084, 0.00104465,-0.00169084,-0.00843216,-0.01601379,-0.02189722,-0.02534126,-0.0269154 ,-0.02748678,-0.02765307,-0.02769211,
-0.02769793,-0.02768493,-0.02762958,-0.02743938,-0.0269154 ,-0.02576898,-0.02381055,-0.02128685,-0.01904286,-0.01813229,-0.01904286,-0.02128685,-0.02381055,-0.02576898,-0.0269154 ,-0.02743938,-0.02762958,-0.02768493,-0.02769793,
-0.02770004,-0.0276965 ,-0.02768141,-0.02762958,-0.02748678,-0.02717434,-0.02664061,-0.02595282,-0.02534126,-0.0250931 ,-0.02534126,-0.02595282,-0.02664061,-0.02717434,-0.02748678,-0.02762958,-0.02768141,-0.0276965 ,-0.02770004, 
-0.02770065,-0.02769986,-0.0276965 ,-0.02768493,-0.02765307,-0.02758335,-0.02746426,-0.0273108 ,-0.02717434,-0.02711897,-0.02717434,-0.0273108 ,-0.02746426,-0.02758335,-0.02765307,-0.02768493,-0.0276965 ,-0.02769986,-0.02770065, 
-0.0277008 ,-0.02770065,-0.02770004,-0.02769793,-0.02769211,-0.02767937,-0.02765761,-0.02762958,-0.02760465,-0.02759453,-0.02760465,-0.02762958,-0.02765761,-0.02767937,-0.02769211,-0.02769793,-0.02770004,-0.02770065,-0.0277008
};


static int zzz = 0;
#ifdef IPP_SUPPORT
static Ipp32s adjust_const_filter[ADWSIZE*ADWSIZE];
#else
static int adjust_const_filter[ADWSIZE*ADWSIZE];
#endif



#define ADWSIZE1212	15
static const double adjust_const_filter1212_double[ADWSIZE1212*ADWSIZE1212] = {
	-0.06221126, -0.06217643, -0.06206867, -0.06180898, -0.06132971, -0.06067528, -0.06007047, -0.05982027, -0.06007047, -0.06067528, -0.06132971, -0.06180898, -0.06206867, -0.06217643, -0.06221126,
	-0.06217643, -0.06203089, -0.06158058, -0.0604954 , -0.0584927 , -0.05575802, -0.05323072, -0.0521852 , -0.05323072, -0.05575802, -0.0584927 , -0.0604954 , -0.06158058, -0.06203089, -0.06217643,
	-0.06206867, -0.06158058, -0.06007047, -0.05643137, -0.04971533, -0.04054461, -0.03206935, -0.02856323, -0.03206935, -0.04054461, -0.04971533, -0.05643137, -0.06007047, -0.06158058, -0.06206867,
	-0.06180898, -0.0604954 , -0.05643137, -0.04663767, -0.02856323, -0.00388268,  0.01892623,  0.02836201,  0.01892623, -0.00388268, -0.02856323, -0.04663767, -0.05643137, -0.0604954 , -0.06180898,
	-0.06132971, -0.0584927 , -0.04971533, -0.02856323,  0.01047333,  0.06377756,  0.11303946,  0.13341855,  0.11303946,  0.06377756,  0.01047333, -0.02856323, -0.04971533, -0.0584927 , -0.06132971,
	-0.06067528, -0.05575802, -0.04054461, -0.00388268,  0.06377756,  0.15616728,  0.24155062,  0.27687273,  0.24155062,  0.15616728,  0.06377756, -0.00388268, -0.04054461, -0.05575802, -0.06067528,
	-0.06007047, -0.05323072, -0.03206935,  0.01892623,  0.11303946,  0.24155062,  0.36031611,  0.40944805,  0.36031611,  0.24155062,  0.11303946,  0.01892623, -0.03206935, -0.05323072, -0.06007047,
	-0.05982027, -0.0521852 , -0.02856323,  0.02836201,  0.13341855,  0.27687273,  0.40944805,  0.46429296,  0.40944805,  0.27687273,  0.13341855,  0.02836201, -0.02856323, -0.0521852 , -0.05982027,
	-0.06007047, -0.05323072, -0.03206935,  0.01892623,  0.11303946,  0.24155062,  0.36031611,  0.40944805,  0.36031611,  0.24155062,  0.11303946,  0.01892623, -0.03206935, -0.05323072, -0.06007047,
	-0.06067528, -0.05575802, -0.04054461, -0.00388268,  0.06377756,  0.15616728,  0.24155062,  0.27687273,  0.24155062,  0.15616728,  0.06377756, -0.00388268, -0.04054461, -0.05575802, -0.06067528,
	-0.06132971, -0.0584927 , -0.04971533, -0.02856323,  0.01047333,  0.06377756,  0.11303946,  0.13341855,  0.11303946,  0.06377756,  0.01047333, -0.02856323, -0.04971533, -0.0584927 , -0.06132971,
	-0.06180898, -0.0604954 , -0.05643137, -0.04663767, -0.02856323, -0.00388268,  0.01892623,  0.02836201,  0.01892623, -0.00388268, -0.02856323, -0.04663767, -0.05643137, -0.0604954 , -0.06180898,
	-0.06206867, -0.06158058, -0.06007047, -0.05643137, -0.04971533, -0.04054461, -0.03206935, -0.02856323, -0.03206935, -0.04054461, -0.04971533, -0.05643137, -0.06007047, -0.06158058, -0.06206867,
	-0.06217643, -0.06203089, -0.06158058, -0.0604954 , -0.0584927 , -0.05575802, -0.05323072, -0.0521852 , -0.05323072, -0.05575802, -0.0584927 , -0.0604954 , -0.06158058, -0.06203089, -0.06217643,
	-0.06221126, -0.06217643, -0.06206867, -0.06180898, -0.06132971, -0.06067528, -0.06007047, -0.05982027, -0.06007047, -0.06067528, -0.06132971, -0.06180898, -0.06206867, -0.06217643, -0.06221126
};

#ifdef IPP_SUPPORT
static Ipp32s adjust_const_filter1212[ADWSIZE1212*ADWSIZE1212];
#else
static int adjust_const_filter1212[ADWSIZE1212*ADWSIZE1212];
#endif

#define WIDTHD		256
#define HEIGHTD		256

#define		FILTER_MULTIFLIER	128

#define BLURPADDING 4
static int s_ni = 4;
static int s_use_mean_adaptive = 0;

U32 markSeqD_old(iana_t *piana, U32 camid)				// !!!!! You are the No. 1!!!
{
#ifdef IPP_SUPPORT
	U32 res;

	iana_cam_t	*pic;
	iana_cam_param_t *picp;
	camimageinfo_t     *pcii;
	camif_buffer_info_t *pb;
	marksequence_t	*pmks;


	U08 *pimgG;
	U08 *pimgA;
	U08 *pimgL;
	U08 *pimgC;

	double lr_;
	double pr_;
	double multfact;


	U32		width;
	U32		height;
	U32		offset_x;
	U32		offset_y;
	U08 	*buf;
	U32 	rindex;
	Ipp8u 	*resizebuf;
	int 	bufsize;

	U32 i, j;

	U32 useProjPl;

	I32 startx, starty;
	I32 widthxy;

	IppiSize roisize;
	IppStatus status;

	I32 startxAbs, startyAbs;
	
	U08		*imgbufS;
    U08		*imgbufD;
    U08		*imgbufBlurFrame, *imgbufBlur, *imgbufEq;
	U08		*imgbufB;
	U08		*imgbufF;
	U08		*imgbufG;
	U08		*imgbufH;

	int bordersize;
	U64 ts64;
	U64 ts64shot;

	U32 marksequencelen;

	//------------------------------------------
	if (piana->camsensor_category == CAMSENSOR_EYEXO) {	
		marksequencelen = MARKSEQUENCELEN_EYEXO;
	} else {
		marksequencelen = MARKSEQUENCELEN;
	}
	
	
	for (i = 0; i < ADWSIZE*ADWSIZE; i++) {
		adjust_const_filter[i] = (Ipp32s) (adjust_const_filter_double[i] * FILTER_MULTIFLIER + 0.5);
	}


	for (i = 0; i < ADWSIZE1212*ADWSIZE1212; i++) {
		adjust_const_filter1212[i] = (Ipp32s) (adjust_const_filter1212_double[i] * FILTER_MULTIFLIER + 0.5);
	}

	pic 		= piana->pic[camid];
	picp 		= &pic->icp;
	pmks 		= &pic->mks;

	imgbufS 		= (U08* )malloc(FULLWIDTH * FULLHEIGHT);
	imgbufD 		= (U08* )malloc(WIDTHD * HEIGHTD);
    imgbufBlurFrame = (U08* )malloc((WIDTHD  + BLURPADDING *2+16) * (HEIGHTD + BLURPADDING * 2+16));
    imgbufBlur      = (U08* )malloc(WIDTHD * HEIGHTD);
    imgbufEq        = (U08* )malloc(WIDTHD * HEIGHTD);
	imgbufB 		= (U08* )malloc((WIDTHD+(ADWSIZE+16)) * (HEIGHTD+(ADWSIZE+16)));
	imgbufF 		= (U08* )malloc(WIDTHD * HEIGHTD);
	imgbufG 		= (U08* )malloc(WIDTHD * HEIGHTD);
	imgbufH 		= (U08* )malloc(WIDTHD * HEIGHTD);


	//resizebuf = NULL;

	ts64shot = piana->pic[camid]->ts64shot;
	useProjPl = 1;
	{
		point_t bcL1;	// Ball center, local
		point_t bcP1; 	// Ball center, pixel
		point_t bcL2;	// Ball center, local
		point_t bcP2; 	// Ball center, pixel
		ballmarkinfo_t		*pbmi;
		pbmi		= &pic->bmi[0];
		

		res = 0;
		buf = NULL;
		pb = NULL;
		//PbPrev.x = 0; PbPrev.y = 0; PbPrev.z = 0;
		//ts64pb = 0;

		/*
		for (i = 0; i < MARKSEQUENCELEN; i++) {
			pmks->cam2ball[i] = 0;
			pmks->cam2ground[i] = 0;
			pmks->ballrL3Raw[i] = 0;
			pmks->ballrL3[i] = 0;
			pmks->ballrP3Raw[i] = 0;
			pmks->ballrP3[i] = 0;
		}
		*/

		for (i = 0; i < marksequencelen; i++) {
			if (pbmi[i].valid == 0) {
				continue;
			}
			
			bcL2.x = pmks->ballposL2[i].x;
			bcL2.y = pmks->ballposL2[i].y;
			bcP2.x = pmks->ballposP2[i].x;
			bcP2.y = pmks->ballposP2[i].y;

			bcL1.x = pmks->ballposL[i].x;
			bcL1.y = pmks->ballposL[i].y;
			bcP1.x = pmks->ballposP[i].x;
			bcP1.y = pmks->ballposP[i].y;


			rindex = pic->firstbulkindex + i;
			if (piana->camsensor_category == CAMSENSOR_EYEXO) {
				//
			} else {
#define BALLSTARTY_MARGIN1	0.005
				if (bcL2.y < pic->icp.L.y + BALLSTARTY_MARGIN1) {
					continue;
				}
			}
			// 1) get image buffer
			if (piana->opmode == IANA_OPMODE_FILE) {
				//rindex = pic->firstbulkindex + i;
				res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, rindex, &pb, &buf);
				if (res == 0) {
					break;
				}
			} else {
				//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bulk, img read\n", i);
				for (j = 0; j < 20; j++) {
					res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, rindex, &pb, &buf);
					if (res == 1) {
						break;
					}
					cr_sleep(1);
				}
				//cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read [%d:%d]\n", i, j);
				if (res == 0) {
					cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", i, j);
					break;
					//continue;
				}
			}

			pcii 		= &pb->cii;
			width 		= pcii->width;
			height 		= pcii->height;
			offset_x 	= pcii->offset_x;
			offset_y 	= pcii->offset_y;

			ts64 		= MAKEU64(pb->ts_h, pb->ts_l);
			if (piana->camsensor_category == CAMSENSOR_EYEXO) {
				if (ts64 < ts64shot) {
					continue;
				}
			}
//IMGCALLBACK
			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf, width, height, 1, 1, 2, 3, piana->imguserparam);
			}

			//ts64 = MAKEU64(pb->ts_h, pb->ts_l);

			//--------------------- 
			// 2) Ball center, radius
			{ 
				{ 	// real image processing
					//-----------------------------------------------------
					// NEWMARKPROCESSING [3]: Apparent radius of Ball.
					//-----------------------------------------------------
					{
						lr_ = pmks->ballrL2[i];
						pr_ = pmks->ballrP2[i];
					}

#define MINPR_		5
					if (pr_ < MINPR_) {
						continue;
					}


					startx = (I32) ((bcP2.x - pr_) - offset_x);
					starty = (I32) ((bcP2.y - pr_) - offset_y);
					widthxy = (I32) (pr_ * 2);

					multfact = (double)WIDTHD / (double)widthxy;

					//if (startx < (I32) pr_ || startx > (I32) (width - widthxy) - 1) {
					//	continue;
					//}

					//if (starty < (I32) pr_ || starty > (I32) (height - widthxy) - 1) {
					//	continue;
					//}

					if (startx < 0+1 || startx > (I32) (width - pr_) - 1) {
						continue;
					}

					if (starty < 0+1 || starty > (I32) (height - pr_) - 1) {
						continue;
					}


					startxAbs = startx + offset_x;			// absolute coordinate
					startyAbs = starty + offset_y;
				}
			} 			// 2) Ball center, radius. 

			// 20200812 denoising zrot mod bar
			{
				I32 barStartx, barEndx;
				U32 barDetectRes;

				barDetectRes = scamif_imagebuf_zrot_barnoise_position(piana->hscamif, camid, NORMALBULK_BULK, i, &barStartx, &barEndx);
				if (barDetectRes == 1) {
					modif_barnoise(buf, width, height, barStartx, barEndx);
				}
			}


			//--------------------- 
			//3)  Resize 
			
			resizebuf = NULL;
			__try {		
				IppiSize ssize;
				IppiRect sroi, droi;
				int sstep, dstep;
				IppStatus status;
				double xfactD, yfactD;

				//IppiSize dsize;
				//int bordersize;



				//			double rr;

				//------
				sroi.x 			= 0;
				sroi.y 			= 0;
				sroi.width 		= widthxy;
				sroi.height 	= widthxy;

				droi.x 			= 0;
				droi.y 			= 0;

				droi.width 		= WIDTHD;
				droi.height 	= HEIGHTD;

//#define IPPI_INTER_USEME	IPPI_INTER_CUBIC
#define IPPI_INTER_USEME	IPPI_INTER_LANCZOS

				ippiResizeGetBufSize(sroi, droi, 1, IPPI_INTER_USEME, &bufsize);

				resizebuf = (U08 *)malloc(bufsize);

				xfactD = multfact;
				yfactD = multfact;

				ssize.width		= widthxy;
				ssize.height	= widthxy;


				sroi.x = 0; // startx;
				sroi.y = 0; // starty;
				sroi.width 		= widthxy;
				sroi.height 	= widthxy;

				droi.x 			= 0;
				droi.y 			= 0;

				droi.width 		= WIDTHD;
				droi.height 	= HEIGHTD;
//				droi.width 		= (int) (sroi.width * multfact);
//				droi.height 	= (int) (sroi.height *  multfact);

				sstep 			= width;
				dstep 			= WIDTHD;

				status = ippiResizeSqrPixel_8u_C1R(
						buf + startx + starty * width,
						ssize,
						sstep,
						sroi,
						//
						(Ipp8u*) imgbufD,
						dstep,
						droi,
						//
						xfactD,
						yfactD,
						0,			// shiftx
						0,			// shifty
						IPPI_INTER_USEME,
						(Ipp8u*) resizebuf
						);

				if (piana->himgfunc) {
					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)buf + startx + starty * width, width, widthxy, 
						0, 1, 2, 3, piana->imguserparam);

					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufD,	WIDTHD, HEIGHTD,		
						1, 1, 2, 3, piana->imguserparam);
				}

			} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
					|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
					){
				// .... :)
			}

			if (resizebuf != NULL) {
				free (resizebuf);
				resizebuf = NULL;
			}
			bordersize = ADWSIZE+1;

            histogram_equalization(imgbufD, imgbufEq, WIDTHD, HEIGHTD);

            {
                IppiSize blurRoiSize, sRoiSize;
                IppiSize maskSize;
                IppiPoint blurAnchor;

                sRoiSize.width = WIDTHD;
                sRoiSize.height = HEIGHTD;

                blurRoiSize.width = WIDTHD + BLURPADDING * 2;
                blurRoiSize.height = HEIGHTD + BLURPADDING * 2;

                maskSize.width = 7;
                maskSize.height = 7;

                blurAnchor.x = 3;
                blurAnchor.y = 3;

                ippiCopyReplicateBorder_8u_C1R(
                    imgbufEq, WIDTHD, sRoiSize,
                    imgbufBlurFrame, WIDTHD + BLURPADDING * 2, blurRoiSize,
                    BLURPADDING, BLURPADDING);

                //-
                ippiFilterBox_8u_C1R(
                    imgbufBlurFrame + (WIDTHD + BLURPADDING * 2) * BLURPADDING + BLURPADDING,
                    WIDTHD + BLURPADDING * 2,
                    imgbufF, WIDTHD, sRoiSize, maskSize, blurAnchor);
            }
            // histogram equalization //
            //gamma_correction(imgbufBlur, imgbufF, WIDTHD, HEIGHTD);

            if (piana->himgfunc) {
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)imgbufD, WIDTHD, HEIGHTD, 0, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)imgbufEq, WIDTHD, HEIGHTD, 1, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)imgbufBlurFrame, WIDTHD + BLURPADDING * 2, HEIGHTD + BLURPADDING * 2, 2, 1, 2, 3, piana->imguserparam);
                // ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)imgbufBlur, WIDTHD, HEIGHTD, 3, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)imgbufF, WIDTHD, HEIGHTD, 4, 1, 2, 3, piana->imguserparam);
            }

			if (s_ni == 2) {
                GetBinDimpleImg_Legacy2(
					imgbufF,
					imgbufD,
					imgbufB,
					imgbufG,
					(WIDTHD/2),
					15 // 10 // 20  // 30 // 50// 10 // -10
					, -15 // -20 // -30  // -50
					);
			} else if (s_ni == 3) {
                GetBinDimpleImg_Legacy3(
						piana,
						imgbufF,
						imgbufD,
						imgbufB,
						imgbufG,
						(WIDTHD/2),
					20 // 15 // 20 //15//23
					, -25
					//20, -25
                );
            } else if (s_ni == 4) {
                GetBinDimpleImg(
                    piana,
                    imgbufF,
                    imgbufG,
                    imgbufB,
                    -5);
            }


			if (piana->himgfunc) {
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufD, WIDTHD, HEIGHTD, 0, 1, 2, 3, piana->imguserparam);
				//((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufB, (WIDTHD+bordersize), (HEIGHTD+bordersize), 1, 1, 2, 3, piana->imguserparam);
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufF, WIDTHD, HEIGHTD, 1, 1, 2, 3, piana->imguserparam);
				((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufB, WIDTHD, HEIGHTD, 2, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufG, WIDTHD, HEIGHTD, 3, 1, 2, 3, piana->imguserparam);
			}

			{
#define MPROIOFFSET 	3
#define MPROIOFFSET5 	5
				U32 mproioffset;
				IppiSize mproisize;

				Ipp8u Mask[5*5] = {
					1, 1, 1, 1, 1,
					1, 1, 1, 1, 1,
					1, 1, 1, 1, 1,
					1, 1, 1, 1, 1,
					1, 1, 1, 1, 1
				};

				IppiSize maskSize;
				IppiPoint anchor;

				//---
				// Open, 5x5
				maskSize.height = 5;
				maskSize.width = 5;

				anchor.x = 2;
				anchor.y = 2;

				mproisize.width 	= WIDTHD - 2 * MPROIOFFSET5;
				mproisize.height 	= HEIGHTD - 2 * MPROIOFFSET5;
				mproioffset			= MPROIOFFSET  * WIDTHD + MPROIOFFSET5;

				//-
				ippiErode_8u_C1IR(
						imgbufB + mproioffset,
						WIDTHD,
						mproisize,
						Mask, 
						maskSize, 
							anchor);

				ippiDilate_8u_C1IR(
						imgbufB + mproioffset,
						WIDTHD,
						mproisize,
						Mask, 
						maskSize, 
						anchor);

				// Open, 3x3
				mproisize.width 	= WIDTHD - 2 * MPROIOFFSET;
				mproisize.height 	= HEIGHTD - 2 * MPROIOFFSET;
				mproioffset			= MPROIOFFSET  * WIDTHD + MPROIOFFSET;
                for (int iii = 0; iii < 3; iii++) {
                    ippiErode3x3_8u_C1IR(imgbufB + mproioffset, WIDTHD, mproisize);			// 3x3
                    ippiDilate3x3_8u_C1IR(imgbufB + mproioffset, WIDTHD, mproisize);
                }

				//-
				ippiErode3x3_8u_C1IR(imgbufG + mproioffset, WIDTHD, mproisize);			// 3x3

				ippiErode3x3_8u_C1R(
						imgbufG + mproioffset, WIDTHD,		// src
						imgbufH + mproioffset, WIDTHD,		// dst
						mproisize
						);	

				ippiSub_8u_C1RSfs(								// C = A - B
						imgbufH + mproioffset, WIDTHD, // source,		// B
						imgbufG + mproioffset, WIDTHD, // source,		// A
						imgbufH + mproioffset, WIDTHD,		// C, edge result.
						mproisize, 0);	

			}

			{
#define DP_MAXLABEL				250
				U32 			segcount;
				imagesegment_t imgseg[DP_MAXLABEL], *pis;
				imagesegment_t imgseg2[DP_MAXLABEL], *pis2;	// 
				imagesegment_t imgseg3[DP_MAXLABEL], *pis3;	// light-ray refined dimples
				imagesegment_t imgsegMark[DP_MAXLABEL], *pisMark;
				U32			ii;

                CalcLabelSegment(
						imgbufB,
						imgbufB, //imgbufF,
						DP_MAXLABEL,
						imgseg,
						&segcount
						);

				memcpy(imgseg2, imgseg, sizeof(imagesegment_t) * DP_MAXLABEL);
                SelectDimple(imgseg2, DP_MAXLABEL, WIDTHD/2);

#define K_FOR_REFINELIGHTRAY	0.5
                RefineDimpleWithLightRay(imgseg2, imgseg3, K_FOR_REFINELIGHTRAY, DP_MAXLABEL, WIDTHD);

#define CROSSHAIRVALUE	16 // 4, 8, 12
                SetCrosshair(imgbufH, WIDTHD, HEIGHTD, CROSSHAIRVALUE);
                CalcLabelSegment(
						imgbufH,
						imgbufG, //imgbufF,
						DP_MAXLABEL,
						imgsegMark,
						&segcount
						);
                SelectOthermark(imgsegMark, DP_MAXLABEL, WIDTHD/2, CROSSHAIRVALUE);

				for (ii = 1; ii < DP_MAXLABEL; ii++) {
					pis = &imgseg[ii];
					pis2 = &imgseg2[ii];
					pis3 = &imgseg3[ii];
					if (pis->state == IMAGESEGMENT_FULL) {
						if (pis2->state == IMAGESEGMENT_FULL) {
							//mycircleGray((int)pis->mx, (int)pis->my, 5, 0x44, imgbufF, WIDTHD);
							mycircleGray((int)pis3->mx, (int)pis3->my, 2, 0x10, imgbufF, WIDTHD);
						} else {
							//mycircleGray((int)pis->mx, (int)pis->my, 1, 0x10, imgbufF, WIDTHD);
						}
					}
				}

				for (ii = 1; ii < DP_MAXLABEL; ii++) {
					pisMark = &imgsegMark[ii];
					if (pisMark->state == IMAGESEGMENT_FULL) {
					mycircleGray((int)pisMark->mx, (int)pisMark->my, 4, 0xFF, imgbufF, WIDTHD);
					}
				}

				if (piana->himgfunc) {
					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufF, WIDTHD, HEIGHTD, 2, 1, 2, 3, piana->imguserparam);
					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufB, WIDTHD, HEIGHTD, 3, 1, 2, 3, piana->imguserparam);
					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufG, WIDTHD, HEIGHTD, 4, 1, 2, 3, piana->imguserparam);
					((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufH, WIDTHD, HEIGHTD, 5, 1, 2, 3, piana->imguserparam);
				}
//DISPLAY_DISPLAY 3
// IMGCALLBACK for Feature.
				//--------------------------------------------------------------------------------------------------------------
				// dimple point axis.. image to global.
                ConvertCoordiImageToWorld_DimpleOthermark(
						piana, 
						camid, 
						&imgseg3[0], 
						&imgsegMark[0],
						i, 
						DP_MAXLABEL, 
						multfact, 
						(double) WIDTHD);

				//--------------------------------------------------------------------------------------------------------------
			}

			roisize;
			pimgL;
			pimgA;
			pimgG;
			status;
			pimgC;
		}
	}

	free(imgbufS);
	free(imgbufD);
    free(imgbufBlurFrame);
    free(imgbufBlur);
    free(imgbufEq);
	free(imgbufB);
	free(imgbufF);
	free(imgbufG);
	free(imgbufH);

	res = 1;

	status;
	return res;
       
#else
    U32 res;

    iana_cam_t	*pic;
    iana_cam_param_t *picp;
    camimageinfo_t     *pcii;
    camif_buffer_info_t *pb;
    marksequence_t	*pmks;

    double lr_;
    double pr_;
    double multfact;

    U32		width;
    U32		height;
    U32		offset_x;
    U32		offset_y;
    U08 	*buf;
    U32 	rindex;
    cv::Size 	bufImgSize;

    U32 i, j;

    U32 useProjPl;

    I32 startx, starty;
    I32 widthxy;

    cv::Size roisize;

    cv::Mat cvImgBuf, cvImgResized, cvImgEq, cvImgBlur;
    cv::Mat cvImgBin, cvImgMorph1, cvImgMorph2, cvImgLabelDimple;
    cv::Mat cvImgBinOthermark, cvImgShrink1, cvImgShrink2, cvImgBoundary, cvImgLabelOthermark;

    U64 ts64;
    U64 ts64shot;

    U32 marksequencelen;

    //------------------------------------------
    if (piana->camsensor_category == CAMSENSOR_EYEXO) {
        marksequencelen = MARKSEQUENCELEN_EYEXO;
    } else {
        marksequencelen = MARKSEQUENCELEN;
    }


    for (i = 0; i < ADWSIZE*ADWSIZE; i++) {
        adjust_const_filter[i] = (int)(adjust_const_filter_double[i] * FILTER_MULTIFLIER + 0.5);
    }


    for (i = 0; i < ADWSIZE1212*ADWSIZE1212; i++) {
        adjust_const_filter1212[i] = (int)(adjust_const_filter1212_double[i] * FILTER_MULTIFLIER + 0.5);
    }

    pic 		= piana->pic[camid];
    picp 		= &pic->icp;
    pmks 		= &pic->mks;

    cvImgEq             = cv::Mat(cv::Size(WIDTHD, HEIGHTD), CV_8UC1);
    cvImgBlur           = cv::Mat(cv::Size(WIDTHD, HEIGHTD), CV_8UC1);

    cvImgBin            = cv::Mat(cv::Size(WIDTHD, HEIGHTD), CV_8UC1);
    cvImgMorph1         = cv::Mat(cv::Size(WIDTHD, HEIGHTD), CV_8UC1); 
    cvImgMorph2         = cv::Mat(cv::Size(WIDTHD, HEIGHTD), CV_8UC1);
    cvImgLabelDimple    = cv::Mat(cv::Size(WIDTHD, HEIGHTD), CV_8UC1);
    
    cvImgBinOthermark   = cv::Mat(cv::Size(WIDTHD, HEIGHTD), CV_8UC1);
    cvImgShrink1        = cv::Mat(cv::Size(WIDTHD, HEIGHTD), CV_8UC1);
    cvImgShrink2        = cv::Mat(cv::Size(WIDTHD, HEIGHTD), CV_8UC1);
    cvImgBoundary       = cv::Mat(cv::Size(WIDTHD, HEIGHTD), CV_8UC1);
    cvImgLabelOthermark = cv::Mat(cv::Size(WIDTHD, HEIGHTD), CV_8UC1);

    ts64shot = piana->pic[camid]->ts64shot;
    useProjPl = 1;
    {
        point_t bcL1;	// Ball center, local
        point_t bcP1; 	// Ball center, pixel
        point_t bcL2;	// Ball center, local
        point_t bcP2; 	// Ball center, pixel
        ballmarkinfo_t		*pbmi;
        pbmi		= &pic->bmi[0];


        res = 0;
        buf = NULL;
        pb = NULL;

        //for (i = 0; i < MARKSEQUENCELEN; i++) {
        //    pmks->cam2ball[i] = 0;
        //    pmks->cam2ground[i] = 0;
        //    pmks->ballrL3Raw[i] = 0;
        //    pmks->ballrL3[i] = 0;
        //    pmks->ballrP3Raw[i] = 0;
        //    pmks->ballrP3[i] = 0;
        //}

        for (i = 0; i < marksequencelen; i++) {
            if (pbmi[i].valid == 0) {
                continue;
            }

            bcL2.x = pmks->ballposL2[i].x;
            bcL2.y = pmks->ballposL2[i].y;
            bcP2.x = pmks->ballposP2[i].x;
            bcP2.y = pmks->ballposP2[i].y;

            bcL1.x = pmks->ballposL[i].x;
            bcL1.y = pmks->ballposL[i].y;
            bcP1.x = pmks->ballposP[i].x;
            bcP1.y = pmks->ballposP[i].y;


            rindex = pic->firstbulkindex + i;
            if (piana->camsensor_category == CAMSENSOR_EYEXO) {
                //
            } else {
#define BALLSTARTY_MARGIN1	0.005
                if (bcL2.y < pic->icp.L.y + BALLSTARTY_MARGIN1) {
                    continue;
                }
            }

            // 1) get image buffer
            if (piana->opmode == IANA_OPMODE_FILE) {
                //rindex = pic->firstbulkindex + i;
                res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, rindex, &pb, &buf);
                if (res == 0) {
                    break;
                }
            } else {
                //		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] bulk, img read\n", i);
                for (j = 0; j < 20; j++) {
                    res = scamif_imagebuf_randomaccess(piana->hscamif, camid, NORMALBULK_BULK, rindex, &pb, &buf);
                    if (res == 1) {
                        break;
                    }
                    cr_sleep(1);
                }
                //cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read [%d:%d]\n", i, j);
                if (res == 0) {
                    cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "BULK read Fail.. [%d:%d]Break\n", i, j);
                    break;
                    //continue;
                }
            }

            pcii 		= &pb->cii;
            width 		= pcii->width;
            height 		= pcii->height;
            bufImgSize = cv::Size(width, height);
            offset_x 	= pcii->offset_x;
            offset_y 	= pcii->offset_y;

            ts64 		= MAKEU64(pb->ts_h, pb->ts_l);
            if (piana->camsensor_category == CAMSENSOR_EYEXO) {
                if (ts64 < ts64shot) {
                    continue;
                }
            }

            if (piana->himgfunc) {
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, width, height, 1, 1, 2, 3, piana->imguserparam);
            }

            //--------------------- 
            // 2) Ball center, radius
            { 	// real image processing
                //-----------------------------------------------------
                // NEWMARKPROCESSING [3]: Apparent radius of Ball.
                //-----------------------------------------------------
                {
                    lr_ = pmks->ballrL2[i];
                    pr_ = pmks->ballrP2[i];
                }

#define MINPR_		5
                if (pr_ < MINPR_) {
                    continue;
                }

                startx = (I32)((bcP2.x - pr_) - offset_x);
                starty = (I32)((bcP2.y - pr_) - offset_y);
                widthxy = (I32)(pr_ * 2);

                multfact = (double)WIDTHD / (double)widthxy;

                // 시작지점과 끝 지점이 공을 모두 포함하는 조건으로 갈 것인지, 일부만 포함해도 되는 조건으로 갈 것인지 확실히 해야 함.
                // 여기서 끝 지점이 width, height에서 반지름의 2배(widthxy)만큼을 들어와야 하는데 pr_만큼만 빼서 try-catch를 써야 했음.
                if (startx < 0 || startx > (I32)(width - widthxy)) {
                    continue;
                }

                if (starty < 0 || starty > (I32)(height - widthxy)) {
                    continue;
                }
            } // 2) Ball center, radius. 


            // 20200812 denoising zrot mod bar
            {
                I32 barStartx, barEndx;
                U32 barDetectRes;

                barDetectRes = scamif_imagebuf_zrot_barnoise_position(piana->hscamif, camid, NORMALBULK_BULK, i, &barStartx, &barEndx);
                if (barDetectRes == 1) {
                    modif_barnoise(buf, width, height, barStartx, barEndx);
                }
            }

            //--------------------- 
            //3)  Resize 
            {
                cv::Size ssize, dsize;
                cv::Rect sroi;
                double xfactD, yfactD;
                cv::Mat cvImgCropped;

                //------
                sroi.x 			= startx;
                sroi.y 			= starty;
                sroi.width 		= widthxy;
                sroi.height 	= widthxy;

                ssize = sroi.size();

                dsize.width 	= WIDTHD;
                dsize.height 	= HEIGHTD;

                xfactD = multfact;
                yfactD = multfact;

                cvImgBuf = cv::Mat(bufImgSize, CV_8UC1, buf);
                cvImgCropped = cvImgBuf(sroi);
                cv::resize(cvImgCropped, cvImgResized, dsize, xfactD, yfactD, cv::INTER_LANCZOS4);

                if (piana->himgfunc) {
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)buf, width, height, 0, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(cvImgCropped.data), widthxy, widthxy, 0, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(cvImgResized.data), WIDTHD, HEIGHTD, 2, 1, 2, 3, piana->imguserparam);
                }
            }

            histogram_equalization((U08*)(cvImgResized.data), cvImgEq.data, WIDTHD, HEIGHTD);

            cv::boxFilter(cvImgEq, cvImgBlur, -1, cv::Size(7, 7), cv::Point(-1, -1), true, cv::BORDER_REPLICATE);

            if (piana->himgfunc) {
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(cvImgResized.data), WIDTHD, HEIGHTD, 0, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(cvImgEq.data), WIDTHD, HEIGHTD, 1, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(cvImgBlur.data), WIDTHD, HEIGHTD, 2, 1, 2, 3, piana->imguserparam);
            }

            if (s_ni == 2) {
                GetBinDimpleImg_Legacy2(
                    cvImgBlur.data,
                    cvImgResized.data,
                    cvImgBin.data,
                    cvImgBinOthermark.data,
                    (WIDTHD/2),
                    15 // 10 // 20  // 30 // 50// 10 // -10
                    , -15 // -20 // -30  // -50
                );
            } else if (s_ni == 3) {
                GetBinDimpleImg_Legacy3(
                    piana,
                    cvImgBlur.data,
                    cvImgResized.data,
                    cvImgBin.data,
                    cvImgBinOthermark.data,
                    (WIDTHD/2),
                    20 // 15 // 20 //15//23
                    , -25
                    //20, -25
                );
            } else if (s_ni == 4) {
                GetBinDimpleImg(
                    piana,
                    cvImgBlur.data,
                    cvImgBinOthermark.data, // othermark
                    cvImgBin.data, // binarized
                    -5);
            }


            if (piana->himgfunc) {
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgResized.data, WIDTHD, HEIGHTD, 0, 1, 2, 3, piana->imguserparam);
                //((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufB, (WIDTHD+bordersize), (HEIGHTD+bordersize), 1, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgEq.data, WIDTHD, HEIGHTD, 1, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgBin.data, WIDTHD, HEIGHTD, 2, 1, 2, 3, piana->imguserparam);
                ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgBinOthermark.data, WIDTHD, HEIGHTD, 3, 1, 2, 3, piana->imguserparam);
            }

            {
#define MPROIOFFSET 	3
#define MPROIOFFSET5 	5
                cv::Mat rectKer3, rectKer5;

                rectKer3 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3), cv::Point(-1, -1));
                rectKer5 = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5, 5), cv::Point(-1, -1));

                //---
                // Open, 5x5
                cv::morphologyEx(cvImgBin, cvImgMorph1, cv::MORPH_OPEN, 
                    rectKer5, cv::Point(-1, -1), 1, cv::BORDER_ISOLATED);
                
                // Open, 3x3
                cv::morphologyEx(cvImgMorph1, cvImgMorph2, cv::MORPH_OPEN,
                    rectKer3, cv::Point(-1, -1), 3, cv::BORDER_ISOLATED);

                cv::morphologyEx(cvImgBinOthermark, cvImgShrink1, cv::MORPH_ERODE,
                    rectKer3, cv::Point(-1, -1), 1, cv::BORDER_ISOLATED);

                cv::morphologyEx(cvImgShrink1, cvImgShrink2, cv::MORPH_ERODE,
                        rectKer3, cv::Point(-1, -1), 1, cv::BORDER_ISOLATED);

                cv::subtract(cvImgShrink1, cvImgShrink2, cvImgBoundary);
            }

            {
#define DP_MAXLABEL				250
                U32 			segcount;
                imagesegment_t imgseg[DP_MAXLABEL], *pis;
                imagesegment_t imgseg2[DP_MAXLABEL], *pis2;	// 
                imagesegment_t imgseg3[DP_MAXLABEL], *pis3;	// light-ray refined dimples
                imagesegment_t imgsegMark[DP_MAXLABEL], *pisMark;
                U32			ii;

                CalcLabelSegment(
                    cvImgMorph2.data,
                    cvImgLabelDimple.data, //imgbufF,
                    DP_MAXLABEL,
                    imgseg,
                    &segcount
                );

                memcpy(imgseg2, imgseg, sizeof(imagesegment_t) * DP_MAXLABEL);
                SelectDimple(imgseg2, DP_MAXLABEL, WIDTHD/2);

#define K_FOR_REFINELIGHTRAY	0.5
                RefineDimpleWithLightRay(imgseg2, imgseg3, K_FOR_REFINELIGHTRAY, DP_MAXLABEL, WIDTHD);

#define CROSSHAIRVALUE	16 // 4, 8, 12
                SetCrosshair(cvImgBoundary.data, WIDTHD, HEIGHTD, CROSSHAIRVALUE);
                CalcLabelSegment(
                    cvImgBoundary.data,
                    cvImgLabelOthermark.data, //imgbufF,
                    DP_MAXLABEL,
                    imgsegMark,
                    &segcount
                );
                SelectOthermark(imgsegMark, DP_MAXLABEL, WIDTHD/2, CROSSHAIRVALUE);

                for (ii = 1; ii < DP_MAXLABEL; ii++) {
                    pis = &imgseg[ii];
                    pis2 = &imgseg2[ii];
                    pis3 = &imgseg3[ii];
                    if (pis->state == IMAGESEGMENT_FULL) {
                        if (pis2->state == IMAGESEGMENT_FULL) {
                            //mycircleGray((int)pis->mx, (int)pis->my, 5, 0x44, imgbufF, WIDTHD);
                            mycircleGray((int)pis3->mx, (int)pis3->my, 2, 0x10, cvImgBlur.data, WIDTHD);
                        } else {
                            //mycircleGray((int)pis->mx, (int)pis->my, 1, 0x10, imgbufF, WIDTHD);
                        }
                    }
                }

                for (ii = 1; ii < DP_MAXLABEL; ii++) {
                    pisMark = &imgsegMark[ii];
                    if (pisMark->state == IMAGESEGMENT_FULL) {
                        mycircleGray((int)pisMark->mx, (int)pisMark->my, 4, 0xFF, cvImgBlur.data, WIDTHD);
                    }
                }

                if (piana->himgfunc) {
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(cvImgBlur.data), WIDTHD, HEIGHTD, 2, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(cvImgBin.data), WIDTHD, HEIGHTD, 3, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(cvImgBinOthermark.data), WIDTHD, HEIGHTD, 4, 1, 2, 3, piana->imguserparam);
                    ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)(cvImgBoundary.data), WIDTHD, HEIGHTD, 5, 1, 2, 3, piana->imguserparam);
                }
                //DISPLAY_DISPLAY 3
                // IMGCALLBACK for Feature.
                //--------------------------------------------------------------------------------------------------------------
                // dimple point axis.. image to global.
                ConvertCoordiImageToWorld_DimpleOthermark(
                    piana,
                    camid,
                    &imgseg3[0],
                    &imgsegMark[0],
                    i,
                    DP_MAXLABEL,
                    multfact,
                    (double)WIDTHD);

                //--------------------------------------------------------------------------------------------------------------
            }
        }
    }
    res = 1;


    return res;
#endif
}


static U32 AdjustContrast(
		U08 *imgbufD,
		U08 *imgbufB,
		U08 *imgbufF)
{
#ifdef IPP_SUPPORT
	IppiSize ssize;
	IppiSize dsize;
	int bordersize;
	IppiSize kernelSize;
	IppiPoint anchor;
	if (s_use_mean_adaptive) {
		bordersize = ADWSIZE1212+1;

		ssize.width = WIDTHD;
		ssize.height = HEIGHTD;

		dsize.width = (WIDTHD+bordersize);
		dsize.height = (HEIGHTD+bordersize);

		ippiCopyReplicateBorder_8u_C1R(
				imgbufD,
				WIDTHD,
				ssize,

				imgbufB,
				WIDTHD+bordersize,
				dsize,

				bordersize/2,
				bordersize/2);


		kernelSize.height = ADWSIZE1212;
		kernelSize.width = ADWSIZE1212;

		anchor.x = ADWSIZE1212/2;
		anchor.y = ADWSIZE1212/2;

		ippiFilter_8u_C1R (
				imgbufB + (WIDTHD+bordersize) * (bordersize/2) + (bordersize/2),
				WIDTHD+bordersize,
				imgbufF,
				WIDTHD,
				ssize, 
				adjust_const_filter1212,
				kernelSize, 
				anchor,
				//(int) (FILTER_MULTIFLIER * 3.125)			// 	400
				(int) (FILTER_MULTIFLIER * 2)			// 	400
				);

	} else {
		bordersize = ADWSIZE+1;

		ssize.width = WIDTHD;
		ssize.height = HEIGHTD;

		dsize.width = (WIDTHD+bordersize);
		dsize.height = (HEIGHTD+bordersize);

		ippiCopyReplicateBorder_8u_C1R(
				imgbufD,
				WIDTHD,
				ssize,

				imgbufB,
				WIDTHD+bordersize,
				dsize,

				bordersize/2,
				bordersize/2);


		kernelSize.height = ADWSIZE;
		kernelSize.width = ADWSIZE;

		anchor.x = ADWSIZE/2;
		anchor.y = ADWSIZE/2;

		ippiFilter_8u_C1R (
				imgbufB + (WIDTHD+bordersize) * (bordersize/2) + (bordersize/2),
				WIDTHD+bordersize,
				imgbufF,
				WIDTHD,
				ssize, 
				adjust_const_filter,
				kernelSize, 
				anchor,
				(int) (FILTER_MULTIFLIER * 3.125)			// 	400
				);
	}
/*
	{
		int ii;
		int sum0, sum1;

		sum0 = 0; 
		sum1 = 0;
		for (ii = 0; ii < HEIGHTD*WIDTHD; ii++) {
			sum0 += *(imgbufD+ii);
			sum1 += *(imgbufF+ii);
		}

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "sum0: %d, sum1: %d, ratio: %lf\n", sum0, sum1, (double)sum1 / double(sum0));

	}
*/




/*
if (piana->himgfunc) {
	((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufD, WIDTHD, HEIGHTD, 0, 1, 2, 3, piana->imguserparam);
	((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufB, (WIDTHD+bordersize), (HEIGHTD+bordersize), 1, 1, 2, 3, piana->imguserparam);
	((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufF, WIDTHD, HEIGHTD, 2, 1, 2, 3, piana->imguserparam);

}
*/

	return 1;
#else
	cv::Size ssize;
	cv::Size dsize;
	cv::Size kernelSize;
	cv::Point anchor;

    cv::Mat cvImgIn, cvImgOut;
    cv::Mat kernel;

    cvImgIn = cv::Mat(cv::Size(WIDTHD, HEIGHTD), CV_8UC1, imgbufD);
	if (s_use_mean_adaptive) {
        kernelSize.height = ADWSIZE1212;
        kernelSize.width = ADWSIZE1212;

        cv::Mat(kernelSize, CV_64FC1, (double*)adjust_const_filter1212_double).convertTo(kernel, CV_32FC1);
        
        cv::filter2D(cvImgIn, cvImgOut, -1, kernel, cv::Point(-1, -1), 0.0, cv::BORDER_REPLICATE);
        memcpy(imgbufF, cvImgOut.data, sizeof(U08) * WIDTHD * HEIGHTD);
        // convert to a filtering with integer kernel?
	} else {
        kernelSize.height = ADWSIZE;
        kernelSize.width = ADWSIZE;

        cv::Mat(kernelSize, CV_64FC1, (double*)adjust_const_filter_double).convertTo(kernel, CV_32FC1);

        cv::filter2D(cvImgIn, cvImgOut, -1, kernel, cv::Point(-1, -1), 0.0, cv::BORDER_REPLICATE);
        memcpy(imgbufF, cvImgOut.data, sizeof(U08) * WIDTHD * HEIGHTD);
	}
    imgbufB; // This was for bordered image, malloc-ed/free-ed in funcion 'GetBinDimpleImage'
	
    return 1;
#endif
}

static I32 GetBinDimpleImg_Legacy2(
		U08 *imgbufSrcA,
		U08 *imgbufSrcB,
		U08 *imgbufResultA,
		U08 *imgbufResultB,
		U32 radius,
		I32 adaptive_threshold_constantA,
		I32 adaptive_threshold_constantB
		)
{
	I32 res;
	I32 diameter;
	I32 bin_window_size, bin_window_half;
	int i, j, m, n;
	I32 fromi, toi, fromj, toj;
	I32 targetoffset;
	I32 sumv, count, meanv;
	I32 value;


	diameter = radius * 2;
	bin_window_size = diameter / 8;

	if ((bin_window_size & 1) == 0) {		// check even?
		bin_window_size--;
	}

	bin_window_half = bin_window_size/2;
	//--
	sumv = 0;
	count = 0;
	meanv = 0;

	for (i = 0; i < HEIGHTD; i++) {
		fromi = i - bin_window_half;
		toi = i + bin_window_half;

		if (fromi < 0) fromi = 0;
		if (toi > HEIGHTD-1) toi = HEIGHTD-1;
		for (j = 0; j < WIDTHD; j++) {
			fromj = j - bin_window_half;
			toj = j + bin_window_half;

			if (fromj < 0) fromj = 0;
			if (toj > WIDTHD-1) toj = HEIGHTD-1;

			sumv = 0;
			count = 0;
			for (m = fromi; m <= toi; m++) {
				targetoffset = m * WIDTHD;
				for (n = fromj; n <= toj; n++) {
					sumv += (U32) *(imgbufSrcA + targetoffset+n);
					count++;
				}
			}
			meanv = sumv / count;
			*(imgbufResultA + i * WIDTHD + j) = (U08)meanv;
		}
	}

	for (i = 0; i < HEIGHTD; i++) {
		targetoffset = i * WIDTHD;
		for (j = 0; j < WIDTHD; j++) {
			meanv = *(imgbufResultA + targetoffset+j);
			value = *(imgbufSrcA + targetoffset+j) - meanv;
			if (value < adaptive_threshold_constantA) {
				value = 0;
			} else {
				value = 0xFF;
			}
			*(imgbufResultA + targetoffset+j) = (U08) value;

			if (imgbufSrcB) {
				value = *(imgbufSrcB + targetoffset+j) - meanv;

				if (value > adaptive_threshold_constantB) {
					value = 0;
				} else {
					value = 0xFF;
				}
				*(imgbufResultB + targetoffset+j) = (U08) value;
			}
		}
	}

	res = 1;
	return res;
}


static I32 GetBinDimpleImg_Legacy3(
		iana_t *piana,
		U08 *imgbufSrcA,
		U08 *imgbufSrcB,
		U08 *imgbufResultA,
		U08 *imgbufResultB,
		U32 radius,
		I32 adaptive_threshold_constantA,
		I32 adaptive_threshold_constantB
		)
{
#ifdef IPP_SUPPORT
	I32 res;
	I32 widthwithBorder;
	I32 heightwithBorder;

	IppiSize roisize;
	IppiSize roisizewithBorder;
	U08 *imgbufSrcAwithBorder;
	Ipp32f *pInteg;
	Ipp32f *pimgWindowedMean;
	Ipp16s *pimgWindowedMean16;
	Ipp16s *pimgWork;

	I32 diameter;
	I32 bin_window_size, bin_window_half;

	I32 offsetA, offsetB;
	I32 calcarea;

	//--
	diameter = radius * 2;
	bin_window_size = diameter / 8;

	if ((bin_window_size & 1) == 0) {		// check even?
		bin_window_size--;
	}

	bin_window_half			= bin_window_size/2;

	if (bin_window_half & 0x01) {
		//bin_window_half--;
		bin_window_half++;
	}
	
	widthwithBorder			= WIDTHD + bin_window_half + bin_window_half;
	heightwithBorder		= HEIGHTD + bin_window_half + bin_window_half;
	
	roisize.width			= WIDTHD;
	roisize.height			= HEIGHTD;
	roisizewithBorder.width = widthwithBorder;
	roisizewithBorder.height = heightwithBorder;

	imgbufSrcAwithBorder 	= (U08 *)malloc(widthwithBorder * heightwithBorder);
	pInteg 					= (Ipp32f *) malloc((widthwithBorder+1) * (heightwithBorder+1) * sizeof(Ipp32f));
	pimgWindowedMean 		= (Ipp32f *) malloc(WIDTHD * HEIGHTD * sizeof(Ipp32f));
	pimgWindowedMean16 		= (Ipp16s *) malloc(WIDTHD * HEIGHTD * sizeof(Ipp16s));
	pimgWork 				= (Ipp16s *) malloc(WIDTHD * HEIGHTD * sizeof(Ipp16s));

	//---
	ippiCopyReplicateBorder_8u_C1R(
			imgbufSrcA,
			WIDTHD, 
			roisize,

			imgbufSrcAwithBorder, 
			widthwithBorder,
			roisizewithBorder,
			
			bin_window_half, 
			bin_window_half);


	//if (piana->himgfunc) {
	//	((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufSrcA, WIDTHD, HEIGHTD, 0, 1, 2, 3, piana->imguserparam);

	//	((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)imgbufSrcAwithBorder, widthwithBorder, heightwithBorder, 1, 1, 2, 3, piana->imguserparam);
	//}
	


	ippiIntegral_8u32f_C1R(
			imgbufSrcAwithBorder,
			widthwithBorder, 
			pInteg,
			(widthwithBorder+1) * sizeof(Ipp32f), 
			roisizewithBorder, 0);
	//if (piana->himgfunc) {
	//	int i, j;
	//	float vd;
	//	for (i = 0; i < HEIGHTD; i++) {
	//		for (j = 0; j < WIDTHD; j++) {
	//			vd = (float)*(pInteg + i * (widthwithBorder+1) + j);
	//			*( ((U08 *)pimgWork) + i * WIDTHD + j ) = (U08) (vd/100000);
	//		}
	//	}
	//	((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimgWork, WIDTHD, HEIGHTD, 0, 1, 2, 3, piana->imguserparam);
	//}

	// SUM = integ(i+wh, j+hh) - integ(i+wh, j-hh) - integ(i-wh, j+hh) + integ(i-wh,j-hh)
	//
	// AA =  integ(i+wh, i+hh) - integ(i+wh, j-hh)		// B - A
	offsetA = (((bin_window_half) 		+ bin_window_half) * (heightwithBorder+1)) 
				+ ((bin_window_half) 	- bin_window_half);
	offsetB = (((bin_window_half) 		+ bin_window_half) * (heightwithBorder+1)) 
				+ ((bin_window_half) 	+ bin_window_half);
	ippiSub_32f_C1R(													// C = B - A
			pInteg 	+ offsetA, (widthwithBorder+1)	*sizeof(Ipp32f),	// A
			pInteg 	+ offsetB, (widthwithBorder+1)	*sizeof(Ipp32f),	// B
			pimgWindowedMean , WIDTHD 				*sizeof(Ipp32f),	// C
			roisize);
	
	// BB = AA - integ(i-wh, j+hh)					// B - A
	offsetA = (((bin_window_half) 		- bin_window_half) * (heightwithBorder+1)) 
				+ ((bin_window_half) 	+ bin_window_half); // A
	offsetB = 0;											// B
	ippiSub_32f_C1IR(						// B = B - A
			pInteg 	+ offsetA, (widthwithBorder+1)	*sizeof(Ipp32f),	// A
			pimgWindowedMean , WIDTHD				*sizeof(Ipp32f), 	// B		SrcDst
			roisize);	

	// CC = BB + integ(i-wh, j-hh) = SUM
	offsetA = (((bin_window_half) 		- bin_window_half) * (heightwithBorder+1)) 
				+ ((bin_window_half) 	- bin_window_half); // A
	offsetB = 0;											// B
	ippiAdd_32f_C1IR(						// B = B + A
			pInteg 	+ offsetA, (widthwithBorder+1)	*sizeof(Ipp32f),	// A
			pimgWindowedMean , WIDTHD				*sizeof(Ipp32f), 	// B		SrcDst
			roisize);	



	//if (piana->himgfunc) {
	//	int i, j;
	//	float vd;
	//	for (i = 0; i < HEIGHTD; i++) {
	//		for (j = 0; j < WIDTHD; j++) {
	//			vd = (float)*(pimgWindowedMean + i * WIDTHD + j);
	//			*( ((U08 *)pimgWork) + i * WIDTHD + j ) = (U08) (vd/(bin_window_half*bin_window_half*4));
	//		}
	//	}
	//	((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimgWork, WIDTHD, HEIGHTD, 0, 1, 2, 3, piana->imguserparam);
	//}

	// get Mean
	calcarea = ((bin_window_half*2+1)*(bin_window_half*2+1));	
	ippiDivC_32f_C1IR(
			(Ipp32f)calcarea, 
			pimgWindowedMean, WIDTHD*sizeof(Ipp32f),
			roisize);

	//if (piana->himgfunc) {
	//	int i, j;
	//	float vd;
	//	for (i = 0; i < HEIGHTD; i++) {
	//		for (j = 0; j < WIDTHD; j++) {
	//			vd = (float)*(pimgWindowedMean + i * WIDTHD + j);
	//			*( ((U08 *)pimgWork) + i * WIDTHD + j ) = (U08) vd;
	//		}
	//	}
	//	((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimgWork, WIDTHD, HEIGHTD, 1, 1, 2, 3, piana->imguserparam);
	//}



	// get Mean with 16s

	ippiConvert_32f16s_C1RSfs (
			pimgWindowedMean, WIDTHD * sizeof(Ipp32f),
			pimgWindowedMean16, WIDTHD * sizeof(Ipp16s),
			roisize, ippRndZero,
			0);
	
	//if (piana->himgfunc) {
	//	int i, j;
	//	Ipp16s v;

	//	for (i = 0; i < HEIGHTD; i++) {
	//		for (j = 0; j < WIDTHD; j++) {
	//			v = (Ipp16s)*(pimgWindowedMean16 + i * WIDTHD + j);
	//			*( ((U08 *)pimgWork) + i * WIDTHD + j ) = (U08) v;
	//		}
	//	}
	//	((IANA_IMAGE_CALLBACKFUNC *) piana->himgfunc)((char *)pimgWork, WIDTHD, HEIGHTD, 2, 1, 2, 3, piana->imguserparam);
	//}

	//Result of A
	ippiConvert_8u16s_C1R(
			imgbufSrcA, WIDTHD * sizeof(Ipp8u),
			pimgWork, WIDTHD * sizeof(Ipp16s),
			roisize);

	ippiSub_16s_C1IRSfs(						// B = B - A
			pimgWindowedMean16, WIDTHD*sizeof(Ipp16s),				// A
			pimgWork, WIDTHD * sizeof(Ipp16s),
			roisize, 0);	

	ippiThreshold_LTValGTVal_16s_C1IR(
			pimgWork, WIDTHD * sizeof(Ipp16s),	// source
			roisize,
			(Ipp16s) adaptive_threshold_constantA,(Ipp16s) 0,
			(Ipp16s) adaptive_threshold_constantA,(Ipp16s) 255
			);

	ippiConvert_16s8u_C1R(
			pimgWork, WIDTHD * sizeof(Ipp16s),	// source
			imgbufResultA, WIDTHD * sizeof(Ipp8u),
			roisize);

	//Result of B
	ippiConvert_8u16s_C1R(
			imgbufSrcB, WIDTHD * sizeof(Ipp8u),
			pimgWork, WIDTHD * sizeof(Ipp16s),
			roisize);

	ippiSub_16s_C1IRSfs(						// B = B - A
			pimgWindowedMean16, WIDTHD*sizeof(Ipp16s),				// A
			pimgWork, WIDTHD * sizeof(Ipp16s),
			roisize, 0);	

	ippiThreshold_LTValGTVal_16s_C1IR(
			pimgWork, WIDTHD * sizeof(Ipp16s),	// source
			roisize,
			(Ipp16s) adaptive_threshold_constantB,(Ipp16s) 0, 
			(Ipp16s) adaptive_threshold_constantB,(Ipp16s) 255
			);

	ippiConvert_16s8u_C1R(
			pimgWork, WIDTHD * sizeof(Ipp16s),	// source
			imgbufResultB, WIDTHD * sizeof(Ipp8u),
			roisize);

	free(imgbufSrcAwithBorder);
	free(pInteg);
	free(pimgWindowedMean);
	free(pimgWindowedMean16);
	free(pimgWork);

	piana;

	res = 1;
	return res;
#else

    cv::Mat cvImgSrcA, cvImgSrcB, cvSrcA16, cvSrcB16, cvImgResA, cvImgResB;
    cv::Mat cvImgBorder, cvIntegral, cvMeanF32, cvMean16, cvImgRelDiff;
    cv::Mat cvImgBinA, cvImgBinB;
    cv::Size srcSize;
    I32 res;

    I32 diameter;
    I32 bin_window_size, bin_window_half;

    srcSize = cv::Size(WIDTHD, HEIGHTD);
    cvImgSrcA = cv::Mat(srcSize, CV_8UC1, imgbufSrcA);
    cvImgSrcB = cv::Mat(srcSize, CV_8UC1, imgbufSrcB);

    //--
    diameter = radius * 2;
    bin_window_size = diameter / 8;

    if ((bin_window_size & 1) == 0) {		// check even?
        bin_window_size--;
    }

    bin_window_half			= bin_window_size/2;

    cv::copyMakeBorder(cvImgSrcA, cvImgBorder, 
        bin_window_half, bin_window_half, bin_window_half, bin_window_half, 
        cv::BORDER_REPLICATE);

    cv::integral(cvImgBorder, cvIntegral, CV_32FC1);

    // i) memory direct access
    {}
    // ii) memory access with pointer(at)
    {}
    // iii) crop suitablly and image operation
    {
        cv::Mat cropUL, cropBL, cropUR, cropBR, sum;
        int windowArea;
        cropUL = cvIntegral(cv::Rect(0, 0, WIDTHD, HEIGHTD));
        cropBL = cvIntegral(cv::Rect(bin_window_size, 0, WIDTHD, HEIGHTD)); 
        cropUR = cvIntegral(cv::Rect(0, bin_window_size, WIDTHD, HEIGHTD)); 
        cropBR = cvIntegral(cv::Rect(bin_window_size, bin_window_size, WIDTHD, HEIGHTD));
        sum = cropBR - cropBL - cropUR + cropUL;
        windowArea = bin_window_size * bin_window_size;
        cvMeanF32 /= (float)windowArea;
    }

    cvMeanF32.convertTo(cvMean16, CV_16SC1);
    cvImgSrcA.convertTo(cvSrcA16, CV_16SC1);
    cv::compare(cvSrcA16, cvMean16 + (I16)adaptive_threshold_constantA,
        cvImgBinA, cv::CMP_GE);
    memcpy(imgbufResultA, cvImgBinA.data, sizeof(U08) * WIDTHD * HEIGHTD);

    //Result of B
    cvImgSrcB.convertTo(cvSrcB16, CV_16SC1);
    cv::compare(cvSrcA16, cvMean16 + (I16)adaptive_threshold_constantB,
        cvImgBinB, cv::CMP_GE);
    memcpy(imgbufResultB, cvImgBinB.data, sizeof(U08) * WIDTHD * HEIGHTD);

    piana;

    res = 1;
    return res;
#endif
}

static void GetBinDimpleImg(
    iana_t *piana,
    U08 *imgbufSrc,
    U08 *imgOthermark,
    U08 *imgbufResult,
    I32 adaptive_threshold_constant) {

    cv::Mat cvImgBuf(cv::Size(HEIGHTD, WIDTHD), CV_8UC1, imgbufSrc), 
        cvAvgImg(cv::Size(HEIGHTD, WIDTHD), CV_8UC1),
        cvImgMask1(cv::Size(HEIGHTD, WIDTHD), CV_8UC1),
        cvImgMask2(cv::Size(HEIGHTD, WIDTHD), CV_8UC1),
        cvImgMask3(cv::Size(HEIGHTD, WIDTHD), CV_8UC1),
        cvImgNonMaskedArea(cv::Size(HEIGHTD, WIDTHD), CV_8UC1),
        cvImgMasked(cv::Size(HEIGHTD, WIDTHD), CV_8UC1),
        cvContrastAdjusted(cv::Size(HEIGHTD, WIDTHD), CV_8UC1),
        cvImgRes(cv::Size(HEIGHTD, WIDTHD), CV_8UC1);

    cv::Mat kernel;

    U08		*imgbufB;
    U08		*imgbufF;

    imgbufB = (U08*)malloc((WIDTHD + (ADWSIZE + 16)) * (HEIGHTD + (ADWSIZE + 16)));
    imgbufF = (U08*)malloc(WIDTHD * HEIGHTD);

    cv::blur(cvImgBuf, cvAvgImg, cv::Size(31, 31), cv::Point(-1,-1), cv::BORDER_REPLICATE);
    
#if 1 // the first flow of the algorithm
    kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3), cv::Point(1, 1));
    cv::adaptiveThreshold(cvImgBuf, cvImgMask1, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, 85, 0);

    memcpy(imgOthermark, cvImgMask1.data, cvImgMask1.rows*cvImgMask1.cols); // return markd area for othermark-match mode

    // extend mark area a little bit.
    cv::morphologyEx(cvImgMask1, cvImgMask2, cv::MORPH_ERODE, kernel);
#endif

#if 0 // the second flow of the algorithm
    kernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(5, 5), cv::Point(1, 1));
        
    // optimization paramter here, the kernel size and the shift constant (now, 10)
    // the constant 10 stops detecting the 'dimple-like marks'
    cv::adaptiveThreshold(cvImgBuf, cvImgMask1, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, 85, 10); 

    // extend mark area a little bit.
    cv::morphologyEx(cvImgMask1, cvImgMask2, cv::MORPH_ERODE, kernel);

    memcpy(imgOthermark, cvImgMask2.data, cvImgMask2.rows*cvImgMask2.cols); // return markd area for othermark-match mode
#endif

    // select non-mask area of the input image
    cv::bitwise_and(cvImgMask2, cvImgBuf, cvImgNonMaskedArea);

    // excange black and white area
    cv::bitwise_not(cvImgMask2, cvImgMask2);

    // select mask area of the averaged input image
    cv::bitwise_and(cvImgMask2, cvAvgImg, cvImgMask3);

    // original image on non-masked area and averaged image on masked area
    // So, weaken the effect of othermarks(the black ones such as markings and logos)
    cv::add(cvImgNonMaskedArea, cvImgMask3, cvImgMasked);

    // additional operation smooths border line and emphasizes(amplifies) the contrast difference
    AdjustContrast((U08*)cvImgMasked.data, imgbufB, imgbufF);
    cvContrastAdjusted = cv::Mat(cv::Size(HEIGHTD, WIDTHD), CV_8UC1, imgbufF);

    // binarize.
    cv::adaptiveThreshold(cvContrastAdjusted, cvImgRes, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, 31, adaptive_threshold_constant);
    
    if (piana->himgfunc) {
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgBuf.data, WIDTHD, HEIGHTD, 0, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgMask2.data, WIDTHD, HEIGHTD, 1, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgMask3.data, WIDTHD, HEIGHTD, 2, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgNonMaskedArea.data, WIDTHD, HEIGHTD, 3, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvImgMasked.data, WIDTHD, HEIGHTD, 4, 1, 2, 3, piana->imguserparam);
        ((IANA_IMAGE_CALLBACKFUNC *)piana->himgfunc)((char *)cvContrastAdjusted.data, WIDTHD, HEIGHTD, 5, 1, 2, 3, piana->imguserparam);
    }

    memcpy(imgbufResult, cvImgRes.data, cvImgRes.rows*cvImgRes.cols); // to return
    free(imgbufB);
    free(imgbufF);

}

static U32 CalcLabelSegment(
		U08 *imgSrc,
		U08 *imgLabel,
		U32 maxlabelcount,
		imagesegment_t *pimgseg,
		U32 *segcount
		)
{
#ifdef IPP_SUPPORT
	U32 res;

	I32 lbbuffersize;
	U08 *lbbuffer;
	//I32 segmentcount;

	I32 i, j;
	imagesegment_t 	*pis;
	U32 segindex;

	U32 lbroioffset;
	IppiSize lbroisize;
	IppStatus status;


	//--

#define LABELING_MARGIN	3
	lbroisize.width 	= WIDTHD 	- LABELING_MARGIN * 2;
	lbroisize.height	= HEIGHTD 	- LABELING_MARGIN * 2;
	lbroioffset 		= (LABELING_MARGIN) * WIDTHD + (LABELING_MARGIN);

	ippiLabelMarkersGetBufferSize_8u_C1R(lbroisize, &lbbuffersize);	// Get buffersize
	lbbuffer = (U08 *)malloc(lbbuffersize);

	memcpy(imgLabel, imgSrc, WIDTHD*HEIGHTD);

	status = ippiLabelMarkers_8u_C1IR(
			imgLabel, WIDTHD, lbroisize, 
			1, maxlabelcount-1, 
			ippiNormInf,
			(int *)segcount,
			lbbuffer);

	free(lbbuffer);

	memset(pimgseg, 0, sizeof(imagesegment_t) * maxlabelcount);
	for (i = 0; i < (I32)maxlabelcount; i++) {
		pis = pimgseg + i;
		pis->state = IMAGESEGMENT_NULL;
		pis->label = i;
	}

	for (i = LABELING_MARGIN; i < (I32)HEIGHTD - LABELING_MARGIN; i++) {
		for (j = LABELING_MARGIN; j < (I32)WIDTHD - LABELING_MARGIN; j++) {
			segindex = *(imgLabel + j + i * WIDTHD);
			if (segindex >= 1 && segindex < maxlabelcount) {
				pis = pimgseg + segindex;
				if (pis->state == IMAGESEGMENT_NULL) {
					pis->state  = IMAGESEGMENT_FULL;
                    pis->ixl = j;
                    pis->iyu = i;
                    pis->ixr = j;
                    pis->iyb = i;

					pis->mx = 0;
					pis->my = 0;
				}
			
                pis->mx += j;
				pis->my += i;
				pis->pixelcount++;
                
                if ((I32)pis->ixr < j) {		// xr is biggest..
                    pis->ixr = j;
                }
                if ((I32)pis->ixl > j) {		// xl is smallest..
                    pis->ixl = j;
                }
                pis->iyb = i;

			}
		}
	}
	*segcount = 0;
	for (i = 0; i < (I32)maxlabelcount; i++) {
		pis = pimgseg + i;
		if (pis->state == IMAGESEGMENT_FULL && pis->pixelcount > 0) {
			(*segcount)++;
			pis->mx /= pis->pixelcount;
			pis->my /= pis->pixelcount;
		} else {
			pis->mx = 0;
			pis->my = 0;
		}
	}

	res = 1;
	return res;
#else
    cv::Mat cvImgIn, cvImgLabel;
    cv::Mat cvStats, cvCents;
    cv::Size imgSize;
    int labelCnt;

    I32 i;
    imagesegment_t 	*pis;
    
    U32 res;
    
    imgSize = cv::Size(WIDTHD, HEIGHTD);
    cvImgIn = cv::Mat(imgSize, CV_8UC1, imgSrc);
    labelCnt = cv::connectedComponentsWithStats(cvImgIn, cvImgLabel, cvStats, cvCents, 8, CV_32SC1);

    labelCnt = labelCnt < (int)maxlabelcount ? labelCnt : (int)maxlabelcount;
    
    memset(pimgseg, 0, sizeof(imagesegment_t) * maxlabelcount);
    for (i = 0; i < (I32)maxlabelcount; i++) {
        pis = pimgseg + i;
        pis->state = IMAGESEGMENT_NULL;
        pis->label = i;
    }

    for (i = 1; i < labelCnt; i++) {
        pis = pimgseg + i;
        
        pis->label = i;
        pis->ixl = cvStats.at<int>(i, 0);
        pis->iyu = cvStats.at<int>(i, 1);
        pis->ixr = cvStats.at<int>(i, 0) + cvStats.at<int>(i, 2);
        pis->iyb = cvStats.at<int>(i, 1) + cvStats.at<int>(i, 3);
        pis->pixelcount = cvStats.at<int>(i, 4);

        pis->mx = cvCents.at<double>(i, 0);
        pis->my = cvCents.at<double>(i, 1);

        if (pis->pixelcount > 0) {
            (*segcount)++;
            pis->state = IMAGESEGMENT_FULL;
        }
    }

    memcpy(imgLabel, cvImgLabel.data, sizeof(U08) * WIDTHD * HEIGHTD);
    res = 1;
    return res;
#endif
}


#if 0
static U32 SelectDimple_Legacy(
	imagesegment_t *pimgseg,
	U32 maxlabelcount,
	U32 radius
	)
{
	U32 res;
	I32 i;

	I32 meanDimple;
	I32 minw, maxw;
	I32 minh, maxh;
	I32 mincount, maxcount;
	I32 segW,segH;

	double dx, dy, d2;
	double minratio, maxratio;
	double segratio;
	I32 maxd2, mind2;

	imagesegment_t 	*pis;
	//--

	meanDimple = radius / 8;

	minw = (meanDimple/4);
	maxw = (U32)(meanDimple * 1.5);

	minh = minw;
	maxh = maxw;
	mincount = minw * minw;
	maxcount = maxw * maxw;

	//minratio = 0.5;
	//maxratio = 2.0;

	minratio = 1.0/3.0;
	maxratio = 3.0;




	maxd2 = radius * radius;
	mind2 = meanDimple * meanDimple;

	for (i = 0; i < (I32)maxlabelcount; i++) {
		pis = pimgseg + i;
		if (pis->state == IMAGESEGMENT_FULL) {
			if (pis->pixelcount < (U32)mincount || pis->pixelcount > (U32)maxcount) {
				pis->state = IMAGESEGMENT_NULL;
				continue;
			}
			
			// check count;

			
			// Check Size
			segW = pis->ixr - pis->ixl + 1;
			segH = pis->iyb - pis->iyu + 1;
			if (segW   < minw || segH < minw) {
				pis->state = IMAGESEGMENT_NULL;
				continue;
			}

            // check enough dimple area into the frame
            if (pis->pixelcount < (U32)segH*segW / 2) {
                pis->state = IMAGESEGMENT_NULL;
                continue;
            }

			segratio = (double)segW / (double)segH;

			// check ratio
			if (segratio < minratio || maxratio > maxratio) {
				pis->state = IMAGESEGMENT_NULL;
				continue;
			}

			// Check in-circle
			dx = pis->mx - radius;
			dy = pis->my - radius;
			d2 = dx*dx + dy*dy;
			if (d2 > maxd2) {
				pis->state = IMAGESEGMENT_NULL;
				continue;
			}
			if (d2 < mind2) {
				pis->state = IMAGESEGMENT_NULL;
				continue;
			}
		}
	}


	res = 1;

	return res;
}
#endif

static U32 SelectDimple(
    imagesegment_t *pimgseg,
    U32 maxlabelcount,
    U32 radius
) {
    U32 res;
    I32 i;

    I32 minw, maxw;
    I32 minh, maxh;
    I32 mincount, maxcount;
    I32 segW, segH;

    double dimpleRad, dimpleArea;
    double dx, dy, d2;
    double minratio, maxratio;
    double segratio;
    I32 maxd2, mind2;

    imagesegment_t 	*pis;
    //--

    dimpleRad = (double)radius * 4 / 42.8;
    dimpleArea = dimpleRad * dimpleRad * 3.14;
    mincount = (I32)(dimpleArea / 15);
    maxcount = (I32)(dimpleArea * 1.5);

    minw = (I32)(dimpleRad / 4);
    maxw = (I32)(dimpleRad * 1.5);
    minh = minw;
    maxh = maxw;

    //minratio = 0.5;
    //maxratio = 2.0;

    minratio = 1.0 / 3.0;
    maxratio = 3.0;

    maxd2 = radius * radius;
    mind2 = (I32)(dimpleRad * dimpleRad);

    for (i = 0; i < (I32)maxlabelcount; i++) {
        pis = pimgseg + i;
        if (pis->state == IMAGESEGMENT_FULL) {
            // check count;
            if (pis->pixelcount < (U32)mincount || pis->pixelcount >(U32)maxcount) {
                pis->state = IMAGESEGMENT_NULL;
                continue;
            }

            // Check Size
            segW = pis->ixr - pis->ixl + 1;
            segH = pis->iyb - pis->iyu + 1;
            if (segW < minw || segH < minw) {
                pis->state = IMAGESEGMENT_NULL;
                continue;
            }

            // check enough dimple area into the frame
            if (pis->pixelcount < (U32)segH*segW / 2) {
                pis->state = IMAGESEGMENT_NULL;
                continue;
            }

            segratio = (double)segW / (double)segH;

            // check ratio
            if (segratio < minratio || maxratio > maxratio) {
                pis->state = IMAGESEGMENT_NULL;
                continue;
            }

            // Check in-circle
            dx = pis->mx - radius;
            dy = pis->my - radius;
            d2 = dx * dx + dy * dy;
            if (d2 > maxd2) {
                pis->state = IMAGESEGMENT_NULL;
                continue;
            }
            if (d2 < mind2) {
                pis->state = IMAGESEGMENT_NULL;
                continue;
            }
        }
    }


    res = 1;

    return res;
}


static U32 SelectOthermark(
    imagesegment_t *pimgseg,
    U32 maxlabelcount,
    U32 radius,
    U32 dvalue			// divided value
)
{
    U32 res;
    I32 i;

    I32 minw, maxw;
    I32 minh, maxh;
    I32 mincount, maxcount;
    // I32 segW, segH;

    double dx, dy, d2;
    //	double minratio, maxratio;
    //	double segratio;
    I32 maxd2;
    I32 mind2;

    imagesegment_t 	*pis;
    //--

    minw = dvalue / 4;
    maxw = (U32)(dvalue * 1.5);

    minh = minw;
    maxh = maxw;
    mincount = minw;
    maxcount = maxw * maxw;

    maxd2 = radius * radius;
    maxd2 = (I32)(maxd2 * 0.8);

    mind2 = dvalue;
    for (i = 0; i < (I32)maxlabelcount; i++) {
        pis = pimgseg + i;
        if (pis->state == IMAGESEGMENT_FULL) {
            if (pis->pixelcount < (U32)mincount || pis->pixelcount >(U32)maxcount) {
                pis->state = IMAGESEGMENT_NULL;
                continue;
            }

            //segW = pis->ixr - pis->ixl + 1;
            //segH = pis->iyb - pis->iyu + 1;
            //if (segW < minw || segH < minw) {
            //    pis->state = IMAGESEGMENT_NULL;
            //    continue;
            //}

            // Check in-circle
            dx = pis->mx - radius;
            dy = pis->my - radius;
            d2 = dx * dx + dy * dy;
            if (d2 > maxd2) {
                pis->state = IMAGESEGMENT_NULL;
                continue;
            }
            if (d2 < mind2) {
                pis->state = IMAGESEGMENT_NULL;
                continue;
            }
        }
    }


    res = 1;

    return res;
}

#if 1
U32 SetCrosshair(
	U08 *imgbufSrc, 
    U32 width, U32 height,
	U32 thickness)
{
	U32 res;
	U32 posvalue;
	U32 i, j;

	for (i = 0; i < height; i++) {
		posvalue = i * width;
		for (j = 0; j < width; j += thickness) {
			*(imgbufSrc + posvalue + j) = 0x00;
		}
	}

	for (i = 0; i < height; i += thickness) {
		posvalue = i * width;
		for (j = 0; j < width; j++) {
			*(imgbufSrc + posvalue + j) = 0x00;
		}
	}

	res = 1;
	return res;
}
#endif


#if 1
static U32 RefineDimpleWithLightRay(
		imagesegment_t imgsegOrg[],
		imagesegment_t imgsegResult[],
		double kvalue,
		U32 maxlabels,
		U32 diameter)
{
	U32 res;
	imagesegment_t *pisOrg;
	imagesegment_t *pisResult;
	U32 i;

	//--
	memcpy(imgsegResult, imgsegOrg, sizeof(imagesegment_t) * maxlabels);

	for (i = 0; i < (I32)maxlabels; i++) {
		pisOrg = &imgsegOrg[i];
		if (pisOrg->state == IMAGESEGMENT_FULL) {
			pisResult = &imgsegResult[i];
			recalcDimpleWithLightRay(pisOrg, pisResult, kvalue, diameter);
		}
	}
	res = 1;
	return res;
}
#endif

static U32 recalcDimpleWithLightRay( // TODO: combine with RefineDimpleWithLightRay to a single function.
    imagesegment_t *pisOrg,
    imagesegment_t *pisRes,
    double kvalue,
    U32 diameter)
{
    U32 res;
    double radius;
    double dx, dy;
    double dx2, dy2;
    double value;
    double delta;
    double r, r2;

    //--
    radius = diameter / 2.0;

    dx = (pisOrg->mx - radius) / radius;
    dy = (pisOrg->my - radius) / radius;

    r2 = dx * dx + dy * dy;
    value = (r2 * (1.0 - r2));
    if (value > 0.0) {
        delta = (kvalue * (2 * sqrt(value)) / 1000.0) / 0.042;
    } else {
        delta = 0;
    }

    r = sqrt(r2);
    if (r2 <= 0.0 || delta > r) {
        dx2 = dx;
        dy2 = dy;
    } else {
        dx2 = dx * (r - delta) / r;
        dy2 = dy * (r - delta) / r;

    }

    pisRes->mx = (dx2 * radius) + radius;
    pisRes->my = (dy2 * radius) + radius;

    res = 1;

    return res;
}

#if 1
static U32 ConvertCoordiImageToWorld_DimpleOthermark(iana_t *piana, U32 camid,
				imagesegment_t imgsegDimple[],
				imagesegment_t imgsegMark[],
				U32 seqnum,
				U32 maxlabelcount,
				double multfact,
				double diameter)
{
	U32 res;
	iana_cam_t		*pic;
	marksequence_t	*pmks;
	markelement2_t	*pmke2;		// O

	//--
	pic 		= piana->pic[camid];
	pmks 		= &pic->mks;

	pmke2 = &pmks->mke2Dimple[seqnum];
	res = ConvertCoordiImageToWorld(piana, camid,
				imgsegDimple,
				seqnum,
				maxlabelcount,
				multfact,
				diameter,
				&pmks->mke2Dimple[seqnum]
				);

//#define MARKCOUNT_MIN	30
#define MARKCOUNT_MIN	10
//#define MARKCOUNT_MIN	1
	if (pmke2->dotcount < MARKCOUNT_MIN) {
		pmke2->state = 0;		// Empty. 
	}

	res = ConvertCoordiImageToWorld(piana, camid,
				imgsegMark,
				seqnum,
				maxlabelcount,
				multfact,
				diameter,
				&pmks->mke2Mark[seqnum]
				);
#if 0
	{
		int i;
		markelement2_t	*pmke2;
		double x, y, z;
		double xr, yr, zr;
		double rr;


		pmke2 = &pmks->mke2Dimple[seqnum];
		for (i = 0; i < (int)pmke2->dotcount; i++) {
			x = pmke2->L3bc[i].x; y = pmke2->L3bc[i].y; z = pmke2->L3bc[i].z;
			xr = x / BALLRADIUS; yr = y / BALLRADIUS; zr = z / BALLRADIUS;
			rr = xr*xr + yr*yr + zr*zr;
			rr = sqrt(rr);
			cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%3d %10lf %10lf %10lf    %10lf %10lf %10lf   %10lf\n",
					i, 
					x, y, z,
					xr, yr, zr,
					rr);
		}

		cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "-----------------------\n");

		pmke2 = &pmks->mke2Mark[seqnum];
		for (i = 0; i < (int)pmke2->dotcount; i++) {
			x = pmke2->L3bc[i].x; y = pmke2->L3bc[i].y; z = pmke2->L3bc[i].z;
			xr = x / BALLRADIUS; yr = y / BALLRADIUS; zr = z / BALLRADIUS;
			rr = xr*xr + yr*yr + zr*zr;
			rr = sqrt(rr);
			cr_trace( CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%3d %10lf %10lf %10lf    %10lf %10lf %10lf   %10lf\n",
					i, 
					x, y, z,
					xr, yr, zr,
					rr);
		}



	}
#endif
	res = 1;

	return res;
}
#endif


#if 1
static U32 ConvertCoordiImageToWorld(iana_t *piana, U32 camid,
				imagesegment_t imgseg[],
				U32 seqnum,
				U32 maxlabelcount,
				double multfact,
				double diameter,
				markelement2_t	*pmke2		// O
				)
{
	U32 res;
	U32 i;
	iana_cam_t		*pic;
	marksequence_t	*pmks;
	point_t			*pPb;
	cr_point_t		*pL3db;
	point_t			Pp;
	point_t			Lp;
	cr_point_t		L3dd;

	imagesegment_t *pis;
	U32 count;

	//--
	pic 		= piana->pic[camid];
	pmks 		= &pic->mks;

	//
	pPb = &pmks->ballposP2[seqnum];			// Ball P position
	pL3db = &pmks->ballposL3D[seqnum];

	//
	pmke2->state 				= 0;		// Empty
	pmke2->dotcount 			= 0;
	pmke2->imagediameter 		= diameter;
	pmke2->imagemultfactor 		= multfact;

	memcpy(&pmke2->ballposP, 	pPb, 	sizeof(point_t));
	memcpy(&pmke2->ballposL3D,	pL3db, sizeof(cr_point_t));

	count = 0;
	for (i = 0; i < maxlabelcount; i++) {
		if (count < MARKCOUNT2) {
			pis = imgseg + i;
			if (pis->state == IMAGESEGMENT_FULL) {
				Pp.x = pis->mx; Pp.x = (Pp.x - diameter/2.0) / multfact; Pp.x = Pp.x + pPb->x;			// pixel to absolute pixel
				Pp.y = pis->my; Pp.y = (Pp.y - diameter/2.0) / multfact; Pp.y = Pp.y + pPb->y;

				pmke2->Pimage[count].x = pis->mx;
				pmke2->Pimage[count].y = pis->my;
				pmke2->Pabs[count].x = Pp.x;
				pmke2->Pabs[count].y = Pp.y;


				iana_P2L(piana, camid, &Pp, &Lp, 0);
				res = ConvertCoordiLocalToBall(
						piana, 				//I
						&pic->icp.CamPosL,	//I				Cam  position
						pL3db, 				//I				ball position
						&Lp,				//I				dimple position
						BALLRADIUS,			//I
						&L3dd				//O, 			3D position, wrt ball.
						);

				//checkl3dd
				{
					double mag;
					
					mag = sqrt(L3dd.x * L3dd.x + L3dd.y * L3dd.y + L3dd.z * L3dd.z);

#define BALLRATIO_MIN	0.9
#define BALLRATIO_MAX	1.1
					if (mag < BALLRADIUS * BALLRATIO_MIN || mag > BALLRADIUS * BALLRATIO_MAX) {
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "mag: %lf\n  .. BAD\n", mag);
						continue;
					}
				}

				if (res) {
					cr_point_t		L3dabs;
					L3dabs.x = L3dd.x + pL3db->x; L3dabs.y = L3dd.y + pL3db->y; L3dabs.z = L3dd.z + pL3db->z;
					memcpy(&pmke2->L3bc[count], 	&L3dd, 		sizeof(cr_point_t));
					memcpy(&pmke2->L3dabs[count], 	&L3dabs, 	sizeof(cr_point_t));
					count++;
				}
			}
		}

		if (count >= MARKCOUNT2) {
			break;
		}
	}


	if (count > 0) {
		pmke2->state = 1;		// ALIVE... not empty
		pmke2->dotcount = count;		// ALIVE... not empty
	}
	res = 1;

	return res;
}
#endif

extern int AK_get_rotated_angle(double *feature_sequence_dimple,
                         size_t num_frames_dimple,
                         size_t *features_counts_dimple,
                         double *feature_sequence_othermark,
                         size_t num_frames_othermark,
                         size_t *features_counts_othermark,
                         double *result_rotation);
extern void AK_init(int sampling_number_frame_1, int sampling_number_frame_2);
		
//----------------------------------------------------------------------------

#if defined (__cplusplus)
}
#endif
#pragma warning(default:4456)

