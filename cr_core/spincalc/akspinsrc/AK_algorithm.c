/***************************************************************************/
/*!
    @file   AK_algorithm.c
    @brief  Rotation computation module using dimple and othermark features

    @section copyright_notice COPYRIGHT NOTICE
    Copyright (c) 2019~ by Creatz Inc. \n
    All Rights Reserved. \n
    Do not duplicate without prior written consent of Creatz Inc.

    @section file_information FILE CREATION INFORMATION
    @author Hyeonseok Choi, Sungsik Nam
    @date   2019/10/21 First Created

    @section	checkin_information LATEST CHECK-IN INFORMATION
    @author		Hyeonseok Choi
    @date		2020/07/19
    @rev		2020/07/22
 */
/***************************************************************************/

/*----------------------------------------------------------------------------
        Description	: defines referenced header files
-----------------------------------------------------------------------------*/

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define __PRIVATE_INCLUDE  // this should come before include AK_algorithm.h
#include "AK_algorithm.h"

#include "cr_dbgmsg.h"
/*----------------------------------------------------------------------------
        Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

#define _USE_MATH_DEFINES
#define number_feature_used 120

/*!
    @brief      threshold for searching closest dimple to match
                Equation: 2 * sin(radians(dimple_distance_in_degrees / 2)/2) +
   feature_error feature error: 0.02 as distance on unit sphere 0.95 in degrees
   0.017 in distances, optimized test dimple_distance_in_degree	10
    @author	    Hyeonseok Choi
 */
#define closest_feature_threshold 0.104238774730672

/*!
    @brief      our distances are mesured between unit vectors
                                will use 'distance reference' instead of
   distance itself distance reference is the inner product reference = (2 -
   distance^2)/2 distance = sqrt(2 - 2 * reference) larger reference <=> closer
   vectors
    @author	    Hyeonseok Choi
 */
#define distance_threshold_ref \
  ((2 - closest_feature_threshold * closest_feature_threshold) / 2)

/* (searching_range * margin * convert_radian) ^2 */
/* (30 * 1.1 * PI / 180) ^ 2 */
#define SEARCHING_RANGE_IN_DEGREES 35 // 30
#define SEARCHING_ANGLE_THRESH_SQUARE 0.451519170478848821381710626
// #define SEARCHING_ANGLE_THRESH_SQUARE 0.33172837014772566468860372527362
#define MINIMUM_DISTANCE_BETWEEN_FEATURES 0.5
#define MAXIMUM_DISTANCE_BETWEEN_FEATURES 0.8
#define FARTHEST_FRAME_COMPARE_DISTANCE_RADIANS_COMPUTE_ERROR \
  0.87266462599716478846184538424431 /* 45, in degrees */
#define FARTHEST_FRAME_COMPARE_DISTANCE_RADIANS_REFINE \
  0.6981317007977318307694763073954 /* 40, in degrees */

/* 0.6981317007977318307694763073954 40, in degrees */
/* 0.7853981633974483096156608458198 45, in degrees */
/* 0.8726646259971647884618453842443 50, in degrees */
/* 0.9599310885968812673080299226687 55, in degrees */
/* 1.0471975511965977461542144610932 60, in degrees */

struct member_var self;

/*----------------------------------------------------------------------------
        Description	: static variable declaration
 -----------------------------------------------------------------------------*/

//static int restrict_topspin_mode = RESTRICT_TOPSPIN_MODE_CALCULATE_ALL;
static int restrict_topspin_mode = RESTRICT_TOPSPIN_MODE_CONDITIONAL;
//static int restrict_topspin_mode = RESTRICT_TOPSPIN_MODE_RESTRICT_ALL;
static BOOL restrict_topspin = FALSE;
//static BOOL restrict_topspin = TRUE;

/*----------------------------------------------------------------------------
        Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declare and implement functions
 -----------------------------------------------------------------------------*/

/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
int compare_indexinfo(const void *ii1, const void **ii2) {
  pairing_index_info_t *info1 = (pairing_index_info_t *)ii1,
                       *info2 = (pairing_index_info_t *)ii2;
  return (int)(info2->indexRef - info1->indexRef);
}
/* number_feature_used, the sampling rate for pairing features.
 * sampling rate can be set manually when call init  */

/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
void AK_init(size_t sampling_number_frame_1, size_t sampling_number_frame_2) {
  size_t i, j, k;
  int area_diff_1D[3] = {-1, 0, 1};
  int area_diff_2D[3][3];
  int A[3] = {0, 1, -1};
  for (i = 0; i < 3; i++) {
    int *p = area_diff_2D[i];
    for (j = 0; j < 3; j++) {
      *p++ = num_of_index_grid * A[i] + area_diff_1D[j];
    }
  }

  for (i = 0; i < 3; i++) {
    int *p = *self.area_diff_3D[i];
    for (j = 0; j < 3; j++)
      for (k = 0; k < 3; k++) {
        *p++ =
            num_of_index_grid * num_of_index_grid * A[i] + area_diff_2D[j][k];
      }
  }

  self.sampling_number_frame_1 = (int)sampling_number_frame_1;
  self.sampling_number_frame_2 = (int)sampling_number_frame_2;
}

/***************************************************************************/
/*!
    @brief      Check features are vaild or not, and make features into
                appropriate form for matching
    @param[in]	feature_sequence		original feature sequences
    @param[in]	num_frames		number of frame(images) of features
    @param[in]	features_counts			sequence of number of features
    @param[in]	feature_vector_length	reference length of feature
    @param[in]	min_features_ref		min number of feature threshold
    @param[out]	normalized_feature_sequence	reformed sequence of features
    @param[out]	normalized_features_counts	number of reformed features
    @param[out]	organized_num_frames	number of reformed frames
    @return		average of number of reformed features
    @author	    Hyeonseok Choi
 */
/***************************************************************************/
int AK_check_and_normalize_feature_sequence(
    const double *feature_sequence, const size_t num_frames,
    const size_t *features_counts, const double feature_vector_length,
    const size_t min_features_ref, double *normalized_feature_sequence,
    size_t *normalized_features_counts, size_t *organized_num_frames) {
  // double *normalized_feature_sequence;
  // size_t *normalized_features_counts;

  size_t i, j, feature_index_travel, feature_index_save;
  int average_features_count = 0;
  int effective_frame_count = 0;

  feature_index_travel = 0;
  feature_index_save = 0;

  // normalized_feature_sequence =
  //    (double *)malloc(sizeof(double) * 256 * num_frames * 3);
  // normalized_features_counts = (size_t *)malloc(sizeof(size_t) * num_frames);

  /* check length of feature vector */
  for (i = 0; i < num_frames; i++) {
    size_t features_count, normalized_features_count;
    features_count = features_counts[i];
    normalized_features_count = 0;

    for (j = 0; j < features_count; j++) {
      double x, y, z, vlen;
      x = feature_sequence[3 * feature_index_travel];
      y = feature_sequence[3 * feature_index_travel + 1];
      z = feature_sequence[3 * feature_index_travel + 2];
      feature_index_travel++;
      vlen = sqrt(x * x + y * y + z * z);

      if (fabs(vlen - feature_vector_length) > 1e-8) continue;
      normalized_feature_sequence[feature_index_save * 3] = x / vlen;
      normalized_feature_sequence[feature_index_save * 3 + 1] = y / vlen;
      normalized_feature_sequence[feature_index_save * 3 + 2] = z / vlen;
      feature_index_save++;
      normalized_features_count++;
    }
    if (normalized_features_count > min_features_ref) {
      normalized_features_counts[i] = normalized_features_count;
      effective_frame_count++;
    } else {
      normalized_features_counts[i] = 0;
      feature_index_save -= normalized_features_count;
    }
  }
  if (effective_frame_count == 0) {
    average_features_count = 0;
  } else {
    average_features_count = (int)(feature_index_save / effective_frame_count);
  }
  *organized_num_frames = num_frames;
  /* select best position of sequence */

  //{
  //  size_t start, start_best, end_best;
  //  size_t org_feature_index_from, org_feature_index_count;
  //  int started;
  //  double sequence_reference, sequence_reference_best;

  //  start = 0;
  //  start_best = 0;
  //  end_best = num_frames;
  //  sequence_reference_best = 1.;
  //  started = 0;

  //  sequence_reference = 1;
  //  for (i = 0; i < num_frames; i++) {
  //    if (normalized_features_counts[i] != 0) {
  //      if (started) {
  //        sequence_reference /= normalized_features_counts[i];
  //      } else {
  //        start = i;
  //        sequence_reference = 1. / normalized_features_counts[i];
  //        started = 1;
  //      }
  //    } else {
  //      if (started) {
  //        if (sequence_reference < sequence_reference_best) {
  //          start_best = start;
  //          end_best = i;
  //          started = 0;
  //          sequence_reference_best = sequence_reference;
  //        }
  //      }
  //    }
  //  }

  //  if (started) {
  //    if (sequence_reference < sequence_reference_best) {
  //      start_best = start;
  //      end_best = num_frames;
  //    }
  //  }

  //  org_feature_index_from = 0;
  //  org_feature_index_count = 0;

  //  for (i = 0; i < start_best; i++) {
  //    org_feature_index_from += normalized_features_counts[i];
  //  }
  //  for (i = start_best; i < end_best; i++) {
  //    org_feature_index_count += normalized_features_counts[i];
  //  }

  //  *organized_num_frames = end_best - start_best;
  //  if (*organized_num_frames < 2) {
  //    // return with code TooFewFramesCannotMatch?
  //    goto AK_CHECK_AND_NORMALIZE_FEATURE_SEQUENCE_EXIT;
  //  }
  //  memcpy(organized_features_counts, normalized_features_counts + start_best,
  //         *organized_num_frames * sizeof(size_t));
  //  memcpy(organized_feature_sequence,
  //         normalized_feature_sequence + org_feature_index_from * 3,
  //         org_feature_index_count * 3 * sizeof(double));

  //  average_features_count =
  //      (int)(org_feature_index_count / (*organized_num_frames));
  //}
  // AK_CHECK_AND_NORMALIZE_FEATURE_SEQUENCE_EXIT:
  //  free(normalized_feature_sequence);
  //  free(normalized_features_counts);

  return average_features_count;
}

/***************************************************************************/
/*!
    @brief      Main function of calculating rotation of golfball
    @param[in]	feature_sequence_dimple		sequence of dimple features
    @param[in]	num_frames_dimple		number of frames of dimple ftrs
    @param[in]	features_counts_dimple	seq of numders of dimple ftrs
    @param[in]	feature_sequence_othermark	sequence of othermark ftrs
    @param[in]	num_frames_othermark	number of frames of othermark ftrs
    @param[in]	features_counts_othermark	seq of numders of dimple ftrs
    @param[out]	result_rotation		resulted rotation
    @param[in]	feature_vector_length	reference length of features
    @return		encoded performace of the calculation. good, soso, etc
    @author	    Hyeonseok Choi
 */
/***************************************************************************/
unsigned int AK_get_rotated_angle(
    double *feature_sequence_dimple, size_t num_frames_dimple, size_t *features_counts_dimple,
     double *feature_sequence_othermark, size_t num_frames_othermark, size_t *features_counts_othermark,    
    double *result_rotation, double feature_vector_length) {
  double *feature_sequence;
  size_t *features_counts;
  size_t num_frames;
  size_t num_pairing_frames;
  struct feature_info *sequence_feature_info;
  matrix_t min_error_matrix;
  matrix_t refined_rotation;
  pairing_index_info_t *paring_index_infos;
  size_t pairing_info_count;

  unsigned int return_code = 0;

  double *normalized_feature_list_dimple =
      (double *)malloc(sizeof(double) * 256 * num_frames_dimple * 3);
  double *normalized_feature_list_othermark =
      (double *)malloc(sizeof(double) * 256 * num_frames_othermark * 3);
  size_t *normalized_features_counts_dimple =
      (size_t *)malloc(sizeof(size_t) * num_frames_dimple);
  size_t *normalized_features_counts_othermark =
      (size_t *)malloc(sizeof(size_t) * num_frames_othermark);

  { /* SELECT MODE */
    size_t average_dimples_count, average_othermarks_count;
    average_dimples_count = AK_check_and_normalize_feature_sequence(
        feature_sequence_dimple, num_frames_dimple, features_counts_dimple,
        feature_vector_length, IGNORE_FTRS_THRESH_DIMPLE,
        normalized_feature_list_dimple, normalized_features_counts_dimple,
        &num_frames_dimple);
    average_othermarks_count = AK_check_and_normalize_feature_sequence(
        feature_sequence_othermark, num_frames_othermark,
        features_counts_othermark, feature_vector_length,
        IGNORE_FTRS_THRESH_OTHERMK, normalized_feature_list_othermark,
        normalized_features_counts_othermark, &num_frames_othermark);

    if (average_dimples_count < AVG_NUM_FTRS_THRESH_DIMPLE) {
      if (average_othermarks_count < AVG_NUM_FTRS_THRESH_OTHERMK) {
        if (average_dimples_count < AVG_NUM_FTRS_THRESH_OTHERMK) {
          /* both features are too few so that cannot compute rotation */
          return_code |= _BV(AK_ERROR_TooSmallFeaturesCannotMatch);
          return_code |= _BV(AK_MATCHINGMODE_NONE);
          return_code |= _BV(AK_SUMMARY_FAULT);
          goto EXIT2;

        } else {
          /* both features are too few but will compute with small num of
           * features */
          return_code |= _BV(AK_MATCHINGMODE_SMALLDIMPLES);
          feature_sequence = normalized_feature_list_dimple;
          features_counts = normalized_features_counts_dimple;
          num_frames = num_frames_dimple;
        }

      } else {
        return_code |= _BV(AK_MATCHINGMODE_OTHERMAKRS);
        feature_sequence = normalized_feature_list_othermark;
        features_counts = normalized_features_counts_othermark;
        num_frames = num_frames_othermark;
      }

    } else {
      return_code |= _BV(AK_MATCHINGMODE_DIMPES);
      feature_sequence = normalized_feature_list_dimple;
      features_counts = normalized_features_counts_dimple;
      num_frames = num_frames_dimple;
    }
  } /* SELECT MODE FIN. */
  if (restrict_topspin_mode == RESTRICT_TOPSPIN_MODE_CALCULATE_ALL) {
    restrict_topspin = FALSE;
  } else if (restrict_topspin_mode == RESTRICT_TOPSPIN_MODE_RESTRICT_ALL ||
             get_len_longest_non0_succ_ftr_seq(features_counts, num_frames) <
                 4) {
    restrict_topspin = TRUE;
  }
  sequence_feature_info = malloc(num_frames * sizeof(struct feature_info));
  paring_index_infos =
      (pairing_index_info_t *)malloc(sizeof(pairing_index_info_t) * num_frames);

#define MAX_NUM_FRAMES_4PAIRING 4
  num_pairing_frames = num_frames < MAX_NUM_FRAMES_4PAIRING
                           ? num_frames
                           : MAX_NUM_FRAMES_4PAIRING;
  get_best_pairing_position(features_counts, num_frames, num_pairing_frames,
                            paring_index_infos, &pairing_info_count);

  init_feature_infos((coord_t *)feature_sequence, num_frames, features_counts,
                     sequence_feature_info);

  /* Search the minimum error rotation by
   * 1. pairing
   * 2. applying candidate rotations to all the sequence of images
   */
  return_code |= get_rough_rotate_angle(
      num_frames, sequence_feature_info, num_pairing_frames, paring_index_infos,
      pairing_info_count, SEARCHING_RANGE_IN_DEGREES,
      SEARCHING_ANGLE_THRESH_SQUARE, MINIMUM_DISTANCE_BETWEEN_FEATURES,
      MAXIMUM_DISTANCE_BETWEEN_FEATURES, &min_error_matrix);

  free(paring_index_infos);

  if (return_code & (_BV(AK_ERROR_NoMatchedRotationCandidates) |
                     _BV(AK_ERROR_TooSmallFeaturesCannotMatch))) {
    return_code |= _BV(AK_SUMMARY_FAULT);
    goto EXIT1;
  }

  { /* Refine the computed rotation */
    coord_t min_error_vector;
    double min_error_rotation_angle;
    size_t max_refine_gap, refine_gap;

    Rodrigues_mat2vec(&min_error_matrix, &min_error_vector);
    min_error_rotation_angle = get_norm(&min_error_vector);
    max_refine_gap = (int)(FARTHEST_FRAME_COMPARE_DISTANCE_RADIANS_REFINE /
                           min_error_rotation_angle);
    max_refine_gap =
        max_refine_gap < num_frames - 2 ? max_refine_gap : num_frames - 2;

    for (refine_gap = 1; refine_gap <= max_refine_gap; refine_gap++) {
      refine_rotated_angle(sequence_feature_info, num_frames, refine_gap,
                           &min_error_matrix, &refined_rotation);
      memcpy(&min_error_matrix, &refined_rotation, sizeof(matrix_t));
    }
  }

  Rodrigues_mat2vec(&refined_rotation, (coord_t *)result_rotation);
  
  /* APPLY RESULT CODES to RESULTED ROTATION */
  if (result_rotation[0] < 0) {
    return_code |= _BV(AK_SPINTYPE_TopSpin);
  }
  if (fabs(result_rotation[1]) > 5000) {
    return_code |= _BV(AK_SPINTYPE_LargeRollSpin);
  }
  if (fabs(result_rotation[2] / result_rotation[0]) > 1) {
    return_code |= _BV(AK_SPINTYPE_LargeSideSpin);
  }

EXIT1:
  fin_feature_infos(num_frames, sequence_feature_info);
  free(sequence_feature_info);
EXIT2:
  if (!(return_code & _BV(AK_SUMMARY_FAULT)))
    return_code |= _BV(AK_SUMMARY_GOOD);
  free(normalized_feature_list_dimple);
  free(normalized_feature_list_othermark);
  free(normalized_features_counts_dimple);
  free(normalized_features_counts_othermark);
  return return_code;
}

/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
unsigned int get_rough_rotate_angle(
    size_t num_frames,
    struct feature_info *sequence_feature_info /* len: num_frames */,
    size_t num_pairing_frames, const pairing_index_info_t *pairing_index_infos,
    const size_t pairing_index_count, int searching_range_in_degrees,
    double searching_angle_thresh_square,
    double minimum_distance_between_features,
    double maximum_distance_between_features, matrix_t *min_error_matrix) {
  /* Get possible, fixed number of rotations, by comparing adjacent frames.
   * get_rotation_candidiates is consist with pairing, convert to rotation,
   * evaluating error, get min errored rotation.
   **/
  const int candidate_num_for_each_frame = 10;
  const int max_rotation_candidates_count =
      candidate_num_for_each_frame * (int)num_pairing_frames;
  matrix_t *candidate_rotations =
      malloc(max_rotation_candidates_count * sizeof(matrix_t));

  //memset(candidate_rotations, 0, max_rotation_candidates_count * sizeof(matrix_t));


  unsigned rotation_candidates_count = 0;
  unsigned int return_code_rough_angle = 0;
  size_t i, j;

  for (i = 0; i < pairing_index_count; i++) {
    size_t frame_from, frame_to;
    frame_from = pairing_index_infos[i].indexFrom;
    frame_to = pairing_index_infos[i].indexFrom + 1;
    if (frame_from < 0 || frame_from > num_frames - 1 || frame_to < 0 ||
        frame_to > num_frames - 1) {
      continue;
    }
	int count;

    //rotation_candidates_count += get_rotation_candidiates(
    //    sequence_feature_info + frame_from, sequence_feature_info + frame_to,
    //    searching_range_in_degrees, searching_angle_thresh_square,
    //    minimum_distance_between_features, maximum_distance_between_features,
    //    candidate_num_for_each_frame,
    //    candidate_rotations + rotation_candidates_count);


	count = (int)get_rotation_candidiates(
		sequence_feature_info + frame_from, sequence_feature_info + frame_to,
		searching_range_in_degrees, searching_angle_thresh_square,
		minimum_distance_between_features, maximum_distance_between_features,
		candidate_num_for_each_frame,
		candidate_rotations + rotation_candidates_count);
/*
	int jj;
	for (jj = 0; jj < count; jj++) {
		double sum0, sum1, sum2;
		double sumsum;
		int ii;
		matrix_t * pm = candidate_rotations + rotation_candidates_count+jj;

		sum0 = 0;
		sum1 = 0;
		sum2 = 0;
		for (ii = 0; ii < 3; ii++) {
			sum0 = sum0 + pm->entry[0][ii] * pm->entry[0][ii];
			sum1 = sum1 + pm->entry[1][ii] * pm->entry[1][ii];
			sum2 = sum2 + pm->entry[2][ii] * pm->entry[2][ii];
		}
		sumsum = sqrt(sum0 + sum1 + sum2);

		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "[%d] sum: %lf %lf %lf  => %lf\n", rotation_candidates_count + jj, sum0, sum1, sum2, sumsum);
	}
*/
	rotation_candidates_count = rotation_candidates_count + count;

  }

  if (rotation_candidates_count == 0)
    return_code_rough_angle |= _BV(AK_ERROR_NoMatchedRotationCandidates);
  else {
    if (rotation_candidates_count < 20)
      return_code_rough_angle |= _BV(AK_WARNING_NotEnoughRotationCandidates);

    matrix_t *rotation_matrix = candidate_rotations;
    double max_error_ref = 0; /* matching error is always smaller than 1. */
    /* to search minimum value, it should be initialized with sufficient big
     * number, 1.*/
    double cur_max_matching_cnt_ref = 0;
    for (i = 0; i < rotation_candidates_count; i++, rotation_matrix++) {
      double average_error = 0;
      size_t total_matching_count = 0;
      coord_t rot_vec;
      matrix_t amplified_rot_mat;
      double rot_mag_rad, total_matching_count_ref;
      size_t max_diff, diff;
      Rodrigues_mat2vec(rotation_matrix, &rot_vec);
      rot_mag_rad = get_norm(&rot_vec);
      max_diff = (size_t)(
          FARTHEST_FRAME_COMPARE_DISTANCE_RADIANS_COMPUTE_ERROR / rot_mag_rad);

      max_diff = max_diff < num_frames - 1 ? max_diff : num_frames - 1;
      if (max_diff == 0) continue;

      memcpy(&amplified_rot_mat, rotation_matrix, sizeof(matrix_t));

      for (diff = 1; diff <= max_diff; diff++) {
        matrix_t temp;

        for (j = 0; j < num_frames - diff; j++) {
          int features_from_count = (int)sequence_feature_info[j].feature_num;
          coord_t *rotated_features =
              malloc(sizeof(coord_t) * features_from_count);
          match_result_t error;

          rotate_points(sequence_feature_info[j].feature_num,
                        sequence_feature_info[j].sorted_features,
                        &amplified_rot_mat, rotated_features);

          error.matched_point_from =
              malloc(sizeof(coord_t) * features_from_count);
          error.matched_point_to =
              malloc(sizeof(coord_t) * features_from_count);

          matching_features(features_from_count, rotated_features,
                            sequence_feature_info[j + diff].indexed_features,
                            &error);

          free(error.matched_point_from);
          free(error.matched_point_to);

          free(rotated_features);

          average_error += error.square_mean_error * error.matched_points_count;
          total_matching_count += error.matched_points_count;
        }
        matrix_dot(&amplified_rot_mat, rotation_matrix, &temp);
        memcpy(&amplified_rot_mat, &temp, sizeof(matrix_t));
      }

      if (total_matching_count == 0)
        continue;
      else
        average_error /= total_matching_count;
      total_matching_count_ref =
          total_matching_count /
          ((2.23953903 - 2 * rot_mag_rad) * (2.23953903 - 2 * rot_mag_rad));
      if (average_error > max_error_ref) {
        if (cur_max_matching_cnt_ref * 0.9 > total_matching_count_ref) {
          continue;
        }
        cur_max_matching_cnt_ref = total_matching_count_ref;
        max_error_ref = average_error;
        *min_error_matrix = *rotation_matrix;
      }
    }
    if (max_error_ref == 0)
      return_code_rough_angle |= _BV(AK_ERROR_NoMatchedRotationCandidates);
    else if (max_error_ref < 0.999)
      return_code_rough_angle |= _BV(AK_WARNING_LargeMatchingDifference);
  }
  free(candidate_rotations);
  return return_code_rough_angle;
}

/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
size_t get_rotation_candidiates(const struct feature_info *feature_info_from,
                                const struct feature_info *feature_info_to,
                                const double searching_range_in_degrees,
                                const double searching_angle_thresh_square,
                                const double minimum_distance_between_features,
                                const double maximum_distance_between_features,
                                size_t max_num_of_candidate,
                                matrix_t *candidate_rotations) {
  coord_t *features_list_from = feature_info_from->sorted_features;
  coord_t *features_list_to = feature_info_to->sorted_features;
  index_stack_t *indexed_list_to = feature_info_to->indexed_features;
  size_t m = feature_info_from->feature_num, n = feature_info_to->feature_num;

  double searching_range_threshold_distance =
      sin(searching_range_in_degrees * 0.55 * M_PI / 180) *
      2; /* TODO: replace it with constant.*/
#define max_pair_num 150000

  coord_t(*two_points_1)[2] = malloc(sizeof(coord_t) * 2 * max_pair_num);
  coord_t(*two_points_2)[2] = malloc(sizeof(coord_t) * 2 * max_pair_num);
/*
  memset(two_points_1, 0, sizeof(coord_t) * 2 * max_pair_num);
  memset(two_points_2, 0, sizeof(coord_t) * 2 * max_pair_num);
*/
  size_t result_count;
  {
    BOOL *feature_pairability_matrix = malloc(m * n * sizeof(BOOL));
    memset(feature_pairability_matrix, 0, m * n * sizeof(BOOL));

    get_pairability_matrix_between_frames(
        m, features_list_from, n, features_list_to,
        searching_range_threshold_distance, feature_pairability_matrix);

    result_count = get_paired_points(
        feature_info_from, feature_info_to, feature_pairability_matrix,
        minimum_distance_between_features, maximum_distance_between_features,
        self.sampling_number_frame_1, self.sampling_number_frame_2, 0.034,
        max_pair_num, two_points_1, two_points_2);
    free(feature_pairability_matrix);
  }
  {
    matrix_t *converted_rotations = malloc(sizeof(matrix_t) * result_count);
	//memset(converted_rotations, 0, sizeof(matrix_t) * result_count);

    result_count = convert_pairs_to_rotations(
        result_count, two_points_1, two_points_2, searching_angle_thresh_square,
        converted_rotations);

    result_count = get_small_error_rotations(
        m, features_list_from, indexed_list_to, result_count,
        converted_rotations, max_num_of_candidate, candidate_rotations);
    free(converted_rotations);
  }
  free(two_points_1);
  free(two_points_2);

  return result_count;
}

/***************************************************************************/
/*!
    @brief      Get the best index pairs for pairing features
    @param[in]	features_counts	array(pointer) of numbers of features
    @param[in]	num_total_frames	number of input frames
    @param[in]	num_pairing_frames	num of frame pairs for pairing
    @param[out]	pairing_infos		set of pairing info
    @param[out]	paring_index_count	number of returning index pairs
    @return		none
    @author	    Hyeonseok Choi
 */
/***************************************************************************/
void get_best_pairing_position(const size_t *features_counts,
                               const size_t num_total_frames,
                               const size_t num_pairing_frames,
                               pairing_index_info_t *pairing_infos,
                               size_t *paring_index_count) {
  size_t saved_pairing_index_count, i;
  saved_pairing_index_count = 0;
  for (i = 0; i < num_total_frames - 1; i++) {
    if (features_counts[i] != 0 && features_counts[i + 1] != 0) {
      pairing_infos[saved_pairing_index_count].indexFrom = i;
      pairing_infos[saved_pairing_index_count].indexRef =
          features_counts[i] * features_counts[i + 1];
      saved_pairing_index_count++;
    }
  }

  if (saved_pairing_index_count <= num_pairing_frames) {
    *paring_index_count = saved_pairing_index_count;
  } else {
    qsort(pairing_infos, saved_pairing_index_count,
          sizeof(pairing_index_info_t), compare_indexinfo);
    *paring_index_count = num_pairing_frames;
  }
}

/* - Change data structures.
 * - Calculated fixed values, which can be used on 'rotation calculation
 *   process'.
 * */
/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
void init_feature_infos(
    coord_t *feature_sequence, size_t num_frames,
    size_t *features_counts /* [num_frames] */,
    struct feature_info *sequence_feature_info /* [num_frames] */
) {
  size_t i;
  for (i = 0; i < num_frames; i++) {
    size_t feature_len = (features_counts[i] < number_feature_used)
                             ? features_counts[i]
                             : number_feature_used;
    sequence_feature_info[i].feature_num = feature_len;
    sequence_feature_info[i].sorted_features = feature_sequence;
    feature_sequence += features_counts[i];
    sort_features(feature_len, sequence_feature_info[i].sorted_features);

    sequence_feature_info[i].indexed_features =
        malloc(sizeof(index_stack_t) * max_index);
    indexing_initialize(sequence_feature_info[i].indexed_features);

    /* Often called. spend some time. reduce computations if possible
     * deepest module of function call stack, no dependency */
    indexing_run(feature_len, sequence_feature_info[i].sorted_features,
                 sequence_feature_info[i].indexed_features, 27,
                 self.area_diff_3D[0][0]);
    sequence_feature_info[i].feature_distance_matrix =
        malloc(feature_len * feature_len * sizeof(double));

    /* Distances of features-pair */
    get_feature_distance_matrix(
        feature_len, sequence_feature_info[i].sorted_features,
        sequence_feature_info[i].feature_distance_matrix);
  }
}

/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
void fin_feature_infos(
    size_t num_frames,
    struct feature_info *sequence_feature_info /* [num_frames] */
) {
  size_t i;
  for (i = 0; i < num_frames; i++) {
    free(sequence_feature_info[i].indexed_features);
    free(sequence_feature_info[i].feature_distance_matrix);
  }
}

/* Because values of two_points will be modified, make sure the original
 * features(points) keep unchanged Do not just link reference of points to
 * two_points. copy and pass. */
/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
size_t get_paired_points(const struct feature_info *feature_info_from,
                         const struct feature_info *feature_info_to,
                         const BOOL *feature_pairability_matrix,
                         const double minimum_distance_between_features,
                         const double maximum_distance_between_features,
                         const int sampling_number_frame_1,
                         const int sampling_number_frame_2,
                         const double permitted_feature_distance_error,
                         size_t pair_num,
                         coord_t (*two_points_1)[2] /* [pair_num][2] */,
                         coord_t (*two_points_2)[2] /* [pair_num][2] */
) {
  coord_t *features_list_from = feature_info_from->sorted_features;
  coord_t *features_list_to = feature_info_to->sorted_features;
  size_t m = feature_info_from->feature_num, n = feature_info_to->feature_num;

  size_t point_pairs_count = 0;
  size_t i1, i2, j1, j2;

  size_t i_jump, j_jump;
  size_t i_index_limit, j_index_limit;
  if (sampling_number_frame_1 == 0) return 0;

  i_jump = m / sampling_number_frame_1;
  j_jump = n / sampling_number_frame_2;

  if (i_jump == 0) i_jump = 1;
  if (j_jump == 0) j_jump = 1;

  i_index_limit = m * i_jump;
  j_index_limit = n * j_jump;
  i_index_limit = i_index_limit < m ? i_index_limit : m;
  j_index_limit = j_index_limit < n ? i_index_limit : n;
  for (i1 = 0; i1 < i_index_limit; i1 += i_jump)
    for (i2 = i1 + i_jump; i2 < i_index_limit; i2 += i_jump) {
      double feature_distance1 =
          feature_info_from->feature_distance_matrix[i1 * m + i2];
      if (feature_distance1 < maximum_distance_between_features &&
          minimum_distance_between_features < feature_distance1) {
        for (j1 = 0; j1 < j_index_limit; j1 += j_jump)
          for (j2 = j1 + j_jump; j2 < j_index_limit; j2 += j_jump) {
            double feature_distance2 =
                feature_info_to->feature_distance_matrix[j1 * n + j2];
            if (feature_distance2 < maximum_distance_between_features &&
                minimum_distance_between_features < feature_distance2) {
              if (fabs(feature_distance1 - feature_distance2) <
                  permitted_feature_distance_error) {
                if (feature_pairability_matrix[i1 * n + j1] &&
                    feature_pairability_matrix[i2 * n + j2]) {
                  two_points_1[point_pairs_count][0] = features_list_from[i1];
                  two_points_1[point_pairs_count][1] = features_list_from[i2];
                  two_points_2[point_pairs_count][0] = features_list_to[j1];
                  two_points_2[point_pairs_count][1] = features_list_to[j2];
                  point_pairs_count++;
                  if (point_pairs_count >= pair_num) return point_pairs_count;
                }
                if (feature_pairability_matrix[i1 * n + j2] &&
                    feature_pairability_matrix[i2 * n + j1]) {
                  two_points_1[point_pairs_count][0] = features_list_from[i1];
                  two_points_1[point_pairs_count][1] = features_list_from[i2];
                  two_points_2[point_pairs_count][0] = features_list_to[j2];
                  two_points_2[point_pairs_count][1] = features_list_to[j1];
                  point_pairs_count++;
                  if (point_pairs_count >= pair_num) return point_pairs_count;
                }
              }
            }
          }
      }
    }
  return point_pairs_count;
}

/* Compare two 'rotation matrixes'
 * Caution: It's input must be 'rotation matrix', ofcourse with determinent 1.
 */
/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
static BOOL is_similar_rotation(const matrix_t *a, const matrix_t *b) {
  const double e = 1e-8;
  /* To reduce computational time, compare 3 elements only, because the
   * determinant of rotation matrix is always 1. */
  return (fabs(a->entry[0][0] - b->entry[0][0]) < e &&
          fabs(a->entry[0][1] - b->entry[0][1]) < e &&
          fabs(a->entry[1][0] - b->entry[1][0]) < e);
  /* Compare every elements.
    size_t i;
    for(i=0; i<3*3; i++)
        if( fabs(a->entry[0][i] - b->entry[0][i] ) >= e )
            return FALSE;
    return TRUE;     */
}

/*
 * Remove similar rotations and find 'k' rotations, which has small error.
 *
 * - 'result' must be with size 'k' array.
 * - The number of results could be smaller than k.
 * - The number of results is returned as a "function return value".
 */

/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
size_t get_best_k_rotations(size_t n, errored_rotation_t *r /* [n] */,
                            size_t max_k, matrix_t *result /* [max_k] */) {
  size_t i, j, cnt = 0;
  for (i = 0; i < n; i++) {
    BOOL unique = TRUE;
    for (j = 0; j < cnt; j++) {
      if (is_similar_rotation(result + j, &r[i].rot_mat)) unique = FALSE;
    }
    if (unique) {
      result[cnt++] = r[i].rot_mat;
      if (cnt >= max_k) break;
    }
  }
  return cnt;
}

size_t get_small_error_rotations(
    size_t feature_from_len,
    const coord_t *features_list_from /* [feature_from_len] */,
    const index_stack_t *indexed_list_to, size_t candidate_rotations_count,
    const matrix_t
        *candidate_rotation_matrices /* [candidate_rotations_count] */,
    size_t max_num_of_candidate, matrix_t *small_error_rotations) {
  errored_rotation_t *rotation_with_error_list =
      malloc(candidate_rotations_count * sizeof(errored_rotation_t));
  size_t result_num;
  size_t i, j, refined_candidates_count;

  refined_candidates_count = 0;
  for (i = 0; i < candidate_rotations_count;
       i++, candidate_rotation_matrices++) {
    coord_t *rotated_points = malloc(sizeof(coord_t) * feature_from_len);
    match_result_t matched_result;

    rotate_points(feature_from_len, features_list_from,
                  candidate_rotation_matrices, rotated_points);

    matched_result.matched_point_from =
        malloc(sizeof(coord_t) * feature_from_len);
    matched_result.matched_point_to =
        malloc(sizeof(coord_t) * feature_from_len);

    matching_features(feature_from_len, rotated_points, indexed_list_to,
                      &matched_result);

    if (matched_result.matched_points_count < 3)
      goto EXIT_GET_SMALL_ERROR_ROTATIONS;

    { /* match featured frames near the rotation from pairing */
      size_t matched_points_count = matched_result.matched_points_count;
      matrix_t temp_inner_mtx, svd_U, svd_V, additional_rot, refined_rot;
      coord_t svd_S;
      coord_t *refined_points = malloc(sizeof(coord_t) * matched_points_count);

      matrix_dot_n(matched_result.matched_point_from,
                   matched_result.matched_point_to,
                   matched_result.matched_points_count, &temp_inner_mtx);
      svd(&svd_U, &svd_S, &svd_V, &temp_inner_mtx);
      transpose(&svd_U, &svd_U);
      matrix_dot(&svd_V, &svd_U, &additional_rot);
      matrix_dot(&additional_rot, candidate_rotation_matrices, &refined_rot);
      if (!is_valid_rotation_(&refined_rot, SEARCHING_ANGLE_THRESH_SQUARE)) {
          free(refined_points);
        goto EXIT_GET_SMALL_ERROR_ROTATIONS;
      }
      rotate_points(matched_result.matched_points_count,
                    matched_result.matched_point_from, &additional_rot,
                    refined_points);

      { /* compute matching error under refined rotation */
        double point_difference_ref_avg = 0, inner;

        for (j = 0; j < matched_points_count; j++) {
          inner = vector_dot(refined_points + j,
                             matched_result.matched_point_to + j);
          point_difference_ref_avg += ((inner < 1) ? inner : 1);
        }
        point_difference_ref_avg /= matched_points_count;

		//        rotation_with_error_list[i].rot_mat = refined_rot;
		//        rotation_with_error_list[i].error = point_difference_ref_avg;
		rotation_with_error_list[refined_candidates_count].rot_mat = refined_rot;		// 20201013.. Index Bug Fix by hschoi
		rotation_with_error_list[refined_candidates_count].error = point_difference_ref_avg;	// 20201013.. Index Bug Fix by hschoi
		refined_candidates_count++;
      }
      free(refined_points);
    }
  EXIT_GET_SMALL_ERROR_ROTATIONS:
    free(matched_result.matched_point_from);
    free(matched_result.matched_point_to);
    free(rotated_points);
  }

  qsort(&(rotation_with_error_list[0]), refined_candidates_count,
        sizeof(errored_rotation_t), compare_rotation);

  result_num =
      get_best_k_rotations(refined_candidates_count, rotation_with_error_list,
                           max_num_of_candidate, small_error_rotations);

  free(rotation_with_error_list);
  return result_num;
}

/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
static size_t get_index(const coord_t *f) {
  size_t index = _GET_INDEX((f->f.x + 1) * half_num_of_side_mesh,
                            (f->f.y + 1) * half_num_of_side_mesh,
                            (f->f.z + 1) * half_num_of_side_mesh);
  /* if (index >= max_index)
    printf("x, y, z: %f, %f, %f\n", f->f.x, f->f.y, f->f.z); */
  assert(index < max_index);
  return index;
}

void matching_features(
    size_t features_from_count,
    const coord_t *rotated_features /* [features_from_count] */,
    const index_stack_t *indexed_features, match_result_t *return_result) {
  int matched_points_count = 0;
  double error_ref_sum = 0;

  size_t i, j;
  for (i = 0; i < features_from_count; i++, rotated_features++) {
	  //    size_t cur_index = get_index(rotated_features);
	  volatile int  cur_index = (int)get_index(rotated_features);
/*
	  {
		  double x, y, z;
		  double nnn;

		  x = rotated_features->f.x;
		  y = rotated_features->f.y;
		  z = rotated_features->f.z;
		  nnn = sqrt(x*x + y * y + z * z);
#define SMALL_SMALL	(1e-5)
		  if (nnn < 1 - SMALL_SMALL || nnn > 1 + SMALL_SMALL) {
			  cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "rf: %lf %lf %lf,    norm: %lf\n", x, y, z, nnn);
		  }
	  }
*/

	  if (cur_index < 0 || cur_index > 512-1) {     // bad cur_index.. 
		  //volatile int jjj = cur_index + 1;
		  //jjj++;
		  continue;
	  }
	  double max_ref = distance_threshold_ref;
    coord_t *matched_point = NULL;
    index_stack_t searching_area_features = indexed_features[cur_index];
    coord_t *comp_freature = searching_area_features.coord;
    for (j = 0; j < searching_area_features.len; j++, comp_freature++) {
      double inner = vector_dot(rotated_features, comp_freature);
      if (inner > max_ref) {
        matched_point = comp_freature;
        max_ref = inner;
      }
    }

    if (matched_point != NULL) {
      return_result->matched_point_from[matched_points_count] =
          *rotated_features;
      return_result->matched_point_to[matched_points_count] = *matched_point;
      matched_points_count++;
      error_ref_sum += (max_ref < 1 ? max_ref : 1);
    }
  }
  return_result->matched_points_count = matched_points_count;
  if (matched_points_count == 0)
    return_result->square_mean_error = distance_threshold_ref;
  else
    return_result->square_mean_error = error_ref_sum / matched_points_count;
}

/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
void indexing_initialize(index_stack_t *indexed_features) {
  size_t i;
  for (i = 0; i < num_of_index_grid * num_of_index_grid * num_of_index_grid;
       i++) {
    indexed_features++->len = 0;
  }
}

/* double (*test)[3] = (double (*)[3])c; */
/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
static void index_push(const coord_t *c, index_stack_t *st) {
  size_t l = st->len;
  if (l >= INDEX_DEPTH) {
    return;
  }
  memcpy(st->coord + l, c, sizeof(*c));
  st->len = l + 1;
}

/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
void indexing_run(const size_t feature_num, const coord_t *sorted_features,
                  index_stack_t *indexed_features, size_t neighbor_num,
                  int *neighbor_offset /* [neighbor_num] */
) {
  size_t i, j;
  for (i = 0; i < feature_num; i++, sorted_features++) {
    int center_index = (int)get_index(sorted_features);
    for (j = 0; j < neighbor_num; j++) {
      int neighbor_index = center_index + neighbor_offset[j];
      if (neighbor_index >= max_index || neighbor_index < 0) continue;
      index_push(sorted_features, indexed_features + neighbor_index);
    }
  }
}

/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
static int compare_x(const void *p1, const void *p2) {
  double x_1 = ((coord_t *)p1)->f.x;
  double x_2 = ((coord_t *)p2)->f.x;
  if (x_1 < x_2) return -1;
  if (x_1 > x_2) return 1;
  return 0;
}

/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
void sort_features(size_t len, coord_t *ca) {
  qsort(ca, len, sizeof(ca[0]), compare_x);
}

/* comfirmed. */
/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
void get_matrix_from_pair(const coord_t *point1, const coord_t *point2,
                          matrix_t *matrix) {
  coord_t *sum = matrix->row;
  coord_t *cross = matrix->row + 1;
  coord_t *third = matrix->row + 2;

  add(point1, point2, sum);
  normalize(sum, sum);

  cross_prod(point1, point2, cross);
  normalize(cross, cross);

  cross_prod(sum, cross, third);

  transpose(matrix, matrix);
}

/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
size_t convert_pairs_to_rotations(
    size_t pair_num, coord_t (*const pair1)[2] /* [pair_num][2] */,
    coord_t (*const pair2)[2] /* [pair_num][2] */,
    double searching_angle_thresh_square,
    matrix_t *converted_matrices) { /*searching_range_in_degrees) */

  int converted_count = 0;
  coord_t rot_vec;
  matrix_t matrix1, matrix2, rot_mat;
  size_t i;

  for (i = 0; i < pair_num; i++) {
    double rot_x, rot_y, rot_z;
    get_matrix_from_pair(&pair1[i][0], &pair1[i][1], &matrix1);
    get_matrix_from_pair(&pair2[i][0], &pair2[i][1], &matrix2);

    transpose(&matrix1, &matrix1);
    matrix_dot(&matrix2, &matrix1, &rot_mat);
    Rodrigues_mat2vec(&rot_mat, &rot_vec);
    /* print_coordinate(&rot_vec); */
    /* NOTE: Exclude topspin case. */
    /* if(rot_vec.f.x > 0) { */
    /* NOTE: Check it's smaller than 30 + epsilon. */
    rot_x = rot_vec.f.x;
    rot_y = rot_vec.f.y;
    rot_z = rot_vec.f.z;

    if (is_valid_rotation(rot_x, rot_y, rot_z, searching_angle_thresh_square)) {
      memcpy(converted_matrices + converted_count, &rot_mat, sizeof(matrix_t));
      converted_count++;
    }
  }
  return converted_count;
}

/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
BOOL is_valid_rotation(double rot_x, double rot_y, double rot_z,
                       double threshAmp) {
  double rotNormSq;
  rotNormSq = rot_x * rot_x + rot_y * rot_y + rot_z * rot_z;
  if (rotNormSq > threshAmp) {
    return 0;
  }
  if (restrict_topspin && rot_x < 0 && fabs(rot_z / rot_x) < 3.0) {
    return 0;
  }
  return 1;
}

/***************************************************************************/
/*!
    @brief      Function description
    @param[in]		param1		input parameter description
    @param[in, out]	param2		in-place parameter description
    @param[out]		param3		output parameter description
    @return		return description
    @author	    $(author)
    @date       $(date)
 */
/***************************************************************************/
BOOL is_valid_rotation_(matrix_t *rot, double threshAmp) {
  coord_t vec;
  Rodrigues_mat2vec(rot, &vec);
  if (restrict_topspin) {
    return is_valid_rotation(vec.f.x, vec.f.y, vec.f.z, threshAmp) &&
           (vec.f.x > 0.0);
  } else {
    return is_valid_rotation(vec.f.x, vec.f.y, vec.f.z, threshAmp);
  }
}

/***************************************************************************/
/*!
    @brief      comparing errored_rotations for sorting
    @param[in]		r1	errored rotation1
    @param[in]		r2	errored rotation2
    @return		1: error1 < error2, -1: error1 > error2, 0:same error
    @author	    Hyeonseok Choi
 */
/***************************************************************************/
int compare_rotation(const void *r1, const void *r2) {
  errored_rotation_t *rot1 = (errored_rotation_t *)r1;
  errored_rotation_t *rot2 = (errored_rotation_t *)r2;

  double x_1 = rot1->error, x_2 = rot2->error;
  if (x_1 < x_2) return 1;
  if (x_1 > x_2) return -1;
  return 0;
}

/***************************************************************************/
/*!
    @brief      Construct matrix whose entries are the parities, whether the
                corresponding pair of features are pairable or not. we do
                pairing only if the distance of features is in some 'good'
                range. The candidate of pairings are repeatedly compared, so we
                need to precalculate and save their distance parities
    @param[in]	m	The number of features in former frame
    @param[in]	features_list_from	the features in former frame
    @param[in]	n	The number of features in latter frame
    @param[in]	features_list_to 	the features in latter frame
    @param[in]	searching_range_threshold_distance	'good' distance
   threshold
    @param[out]	feature_pairability_matrix	resulted pairable parity matrix
    @return		none
    @author	    Hyeonseok Choi
 */
/***************************************************************************/
void get_pairability_matrix_between_frames(
    size_t m, const coord_t *features_list_from /* [m] */, size_t n,
    const coord_t *features_list_to /* [n] */,
    const double searching_range_threshold_distance,
    BOOL *feature_pairability_matrix /* [][n] */
) {
  double distence_reference = (2 - searching_range_threshold_distance *
                                       searching_range_threshold_distance) /
                              2;
  size_t previous_starting_index = 0;
  size_t i, j;
  for (i = 0; i < m; i++) {
    const coord_t *feature_from = features_list_from + i;
    double x1 = feature_from->f.x;
    for (j = previous_starting_index; j < n; j++) {
      const coord_t *feature_to = features_list_to + j;
      double x2 = feature_to->f.x;
      if (x1 - x2 > searching_range_threshold_distance) {
        previous_starting_index++;
        continue;
      } else if (x2 - x1 > searching_range_threshold_distance) {
        break;
      } else {
        feature_pairability_matrix[i * n + j] =
            (vector_dot(feature_from, feature_to) > distence_reference);
      }
    }
  }
}

/***************************************************************************/
/*!
    @brief      Calculate and save the distances of all pairs of features. Our
   pairing is done only if the corresponding pairs of features are of similar
   diatances. The candidates of pairings are repeatedly compared, so need to
   precalculate and save the distances
    @param[in]		features_count the number of features
    @param[in]		sorted_features sorted features. the features should be
sorted because not to compare unnecessary features ,
    @param[out]		feature_distance_matrix resulted diatence matrix
    @return		none
    @author	    Hyeonseok Choi
 */
/***************************************************************************/
void get_feature_distance_matrix(
    size_t features_count,
    const coord_t *sorted_features /* [features_count] */,
    double *feature_distance_matrix) {
  size_t i, j;
  for (i = 0; i < features_count; i++) {
    const coord_t *point1 = sorted_features + i;
    for (j = i + 1; j < features_count; j++) {
      const coord_t *point2 = sorted_features + j;
      double inner = vector_dot(point1, point2);
      /* TODO: Could we save powered value and use it? For speed sake.
          -> maybe not: the difference of distance is not competible with the
         difference of squre of distance. So, we should gonna take sqrt every
         comparison which is inefficient*/
      if (inner < 1)
        feature_distance_matrix[i * features_count + j] = sqrt(2 - 2 * inner);
      else
        feature_distance_matrix[i * features_count + j] = 0;
    }
  }
}

/***************************************************************************/
/*!
    @brief      rotate points respect to the origin under given rotation mtx
    @param[in]	len		the number of points
    @param[in]	points	points, will be rotated
    @param[in]	rotation_matrix	the given rotation matrix. we're not gonna check
   this is rotation or not.
    @param[out]	rotated_points	rotated points
    @return		none
    @author	    Hyeonseok Choi
 */
/***************************************************************************/
void rotate_points(size_t len, const coord_t *points,
                   const matrix_t *rotation_matrix, coord_t *rotated_points) {
  size_t i;
  for (i = 0; i < len; i++) {
    mv_dot(rotation_matrix, points++, rotated_points++);
  }
}

/***************************************************************************/
/*!
    @brief      Refine rotation by fine matching between features on a sphere of
   fixed gap. The gap is something like amplification index reference. For
   example, frame gap=2, then we're gonna match features in image indexed i and
   i+2. So rotate features with 2*(expected rotation) and then fine tune and
   update rotation.
    @param[in]	sequence_feature_info	sequence of feature info
    @param[in]	num_frames	the number feature sequences
    @param[in]	frame_gap
    @param[in]	expected_rotation_matrix
    @param[out]	refined_rotation
    @return		none
    @author	    Hyeonseok Choi
 */
/***************************************************************************/
void refine_rotated_angle(struct feature_info *sequence_feature_info,
                          const size_t num_frames, const size_t frame_gap,
                          const matrix_t *expected_rotation_matrix,
                          matrix_t *refined_rotation) {
  size_t i, whole_feautures_count = 0;
  int matched_features_count = 0;
  coord_t rot_vec;
  matrix_t amplified_rotation_mat, refined_amplified_rotation_mat;
  coord_t *whole_features_from;
  coord_t *whole_features_to;

  for (i = 0; i < num_frames; i++) {
    whole_feautures_count += sequence_feature_info[i].feature_num;
  }

  whole_features_from = malloc(whole_feautures_count * sizeof(coord_t));
  whole_features_to = malloc(whole_feautures_count * sizeof(coord_t));

  Rodrigues_mat2vec(expected_rotation_matrix, &rot_vec);
  vector_scalar_multi(&rot_vec, frame_gap, &rot_vec);
  Rodrigues_vec2mat(&amplified_rotation_mat, &rot_vec);

  for (i = 0; i < num_frames - frame_gap; i++) {
    int features_from_count = (int)sequence_feature_info[i].feature_num;
    coord_t *rotated_points = malloc(sizeof(coord_t) * features_from_count);
    match_result_t result;
    rotate_points(features_from_count, sequence_feature_info[i].sorted_features,
                  &amplified_rotation_mat, rotated_points);

    result.matched_point_from = malloc(sizeof(coord_t) * features_from_count);
    result.matched_point_to = malloc(sizeof(coord_t) * features_from_count);
    matching_features(features_from_count, rotated_points,
                      sequence_feature_info[i + frame_gap].indexed_features,
                      &result);
    memcpy(whole_features_from + matched_features_count,
           result.matched_point_from,
           sizeof(coord_t) * result.matched_points_count);
    memcpy(whole_features_to + matched_features_count, result.matched_point_to,
           sizeof(coord_t) * result.matched_points_count);
    matched_features_count += (int)result.matched_points_count;

    free(result.matched_point_from);
    free(result.matched_point_to);
    free(rotated_points);
  }
  {
    matrix_t svd_U, svd_V, svd_A, additional_mtx;
    coord_t svd_S, additional_vec;
    matrix_dot_n(whole_features_from, whole_features_to, matched_features_count,
                 &svd_A);
    svd(&svd_U, &svd_S, &svd_V, &svd_A);
    transpose(&svd_U, &svd_U);
    matrix_dot(&svd_V, &svd_U, &additional_mtx);
    Rodrigues_mat2vec(&additional_mtx, &additional_vec);
    vector_scalar_multi(&additional_vec, 180.0 / 3.14159265, &additional_vec);
    vector_scalar_multi(&additional_vec, 1. / frame_gap, &additional_vec);
    matrix_dot(&additional_mtx, &amplified_rotation_mat,
               &refined_amplified_rotation_mat);
    Rodrigues_mat2vec(&refined_amplified_rotation_mat, &rot_vec);
    vector_scalar_multi(&rot_vec, 1. / frame_gap, &rot_vec);
    Rodrigues_vec2mat(refined_rotation, &rot_vec);
  }
  free(whole_features_from);
  free(whole_features_to);
}

size_t get_len_longest_non0_succ_ftr_seq(const size_t *features_counts,
                                         const size_t num_frames) {
  size_t i, len_longest_non0_succ_ftr_seq, len_cur_non0_succ_ftr_seq;
  len_longest_non0_succ_ftr_seq = 0;
  len_cur_non0_succ_ftr_seq = 0;
  for (i = 0; i < num_frames; i++) {
    if (features_counts[i] != 0) {
      len_cur_non0_succ_ftr_seq++;
    } else {
      if (len_longest_non0_succ_ftr_seq < len_cur_non0_succ_ftr_seq) {
        len_longest_non0_succ_ftr_seq = len_cur_non0_succ_ftr_seq;
      }
      len_cur_non0_succ_ftr_seq = 0;
    }
  }
  if (len_longest_non0_succ_ftr_seq < len_cur_non0_succ_ftr_seq) {
    len_longest_non0_succ_ftr_seq = len_cur_non0_succ_ftr_seq;
  }
  return len_longest_non0_succ_ftr_seq;
}
