#ifdef _WIN32
#pragma warning(disable : 4244)
#endif

#define CV_NO_BACKWARD_COMPATIBILITY

#include <iostream>
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <stdlib.h>

#define _USE_MATH_DEFINES
#include <math.h>

#define __PRIVATE_INCLUDE
#include "feature_define.h"

using namespace cv;

#define RESIZE_RADIUS 127.5 // resized image: 256x256

#define TO_ODD(x) (((((x)) >> 1) << 1) + 1)

static int reduce_highlight(cv::Mat &mat, int pixel_num, int minimum_value,
                            int highlight_roi);
static void blur(const cv::Mat &src, cv::Mat &dst, int blur_type,
                 int kernel_size);
static void invert_marker(cv::Mat &mat, int block_size, int constant_offset,
                          int dilation_size, int clip_val);
void make_adjust_constast_kernel(Mat &kernel, int strength, int kernel_size,
                                 int sigma_x10);
static void adjust_contrast(const Mat &src, Mat &dst, const Mat &kernel);
static void adaptive_threshold(const cv::Mat &src, cv::Mat &dst, int type,
                               int window_size, int C);
static void open(const Mat &src, Mat &dst, int kernel_size, int iteration_num);
static int labeling(const cv::Mat &src, cv::Mat &labels, cv::Mat &stats,
                    cv::Mat &centroids);
// static int refine_and_filter_features(int num, coord_t *features);
static bool is_weird(int w, int h, int A, double dimple_radius,
                     double single_dimple_area);

/*
 */
void FeatureDefine::init(const params_t &_params) {
  params = _params;

  make_adjust_constast_kernel(adjust_contrast_kernel,
                              MAX(params.contrast_adjust_strength, 1),
                              TO_ODD(params.contrast_adjust_kernel_size),
                              MAX(params.contrast_adjust_sigma_x10, 1));

  for (int i = 0; i < TILT_CORRECTION_COEFF_BOOSTER; i++) {
    tilt_correction_coeff_table[i] =
        sin(2 * asin((double)i / TILT_CORRECTION_COEFF_BOOSTER));
    // std::cout << tilt_correction_coeff_table[i] << std::endl;
  }
}


void FeatureDefine::get_denoised_img(const cv::Mat &src, cv::Mat &trg,
                                     bool save_images, const std::string &path,
                                     const std::string &basename) {
  resize(src, trg, Size(2 * RESIZE_RADIUS + 1, 2 * RESIZE_RADIUS + 1), 0, 0,
         INTER_CUBIC);
  if (save_images)
    cv::imwrite(path + "01_resize_" + basename, trg);

  blur(trg, trg, params.blur_kernel_type, TO_ODD(params.blur_kernel_size));

  if (save_images) {
    cv::imwrite(path + "02_blur_" + basename, trg);
  }
}

void FeatureDefine::get_binarized_img(const cv::Mat &src, cv::Mat &trg,
                                      bool save_images, const std::string &path,
                                      const std::string &basename) {
  trg = src.clone();
  int clip_val =
      reduce_highlight(trg, 500 /* params.reduce_highlight_pixel_num */,
                       150 /* params.reduce_highlight_minimum_value */, 84);

  invert_marker(trg, TO_ODD(params.get_marker_block_size),
                params.get_marker_constant_offset,
                params.get_marker_dilation_size, clip_val);

  adjust_contrast(trg, trg, adjust_contrast_kernel);

  if (save_images)
    cv::imwrite(path + "03_adjust_contrast_" + basename, trg);

  adaptive_threshold(trg, trg, params.adaptive_threshold_type,
                     TO_ODD(params.adaptive_threshold_window_size),
                     -params.adaptive_threshold_constant_offset);
  if (save_images) {
    cv::imwrite(path + "04_adaptive_threshold_" + basename, trg);
  }
  open(trg, trg, TO_ODD(params.open_1_kernel_size),
       MAX(params.open_1_iteration_num, 1));
  if (save_images)
    cv::imwrite(path + "05_open_1_" + basename, trg);

  open(trg, trg, TO_ODD(params.open_2_kernel_size),
       MAX(params.open_2_iteration_num, 1));
  if (save_images)
    cv::imwrite(path + "06_open_2_" + basename, trg);
}

void FeatureDefine::get_center_of_features(
    const cv::Mat &src, const cv::Point2d &origin, int org_image_size,
    std::vector<Point2d> &features, bool save_images, const std::string &path,
    const std::string &basename) {
  Mat trg;
  if (save_images)
    trg = src.clone();
  else
    (void)(trg);
  Mat labels;
  Mat stats;
  Mat centroids;

  // int label_num =
  labeling(src, labels, stats, centroids);

  // coord_t *features_array = (coord_t *)malloc(label_num * sizeof(coord_t));

  // TODO: Set it as a member variable.
  double dimple_radius =
      (double)RESIZE_RADIUS * DIMPLE_DIAMETER / GOLF_BALL_DIAMETER;
  double single_dimple_area = M_PI * dimple_radius * dimple_radius;

  //   // # -1: area, 2: width, 3: height
  for (int i = 0; i < stats.rows; i++) {
    if (!is_weird(stats.at<int>(Point(2, i)), stats.at<int>(Point(3, i)),
                  stats.at<int>(Point(4, i)), dimple_radius,
                  single_dimple_area)) {
      // std::cout << "is not weird. "
      //           << "x: " << centroids.at<double>(i, 0) << std::endl;
      Point2d p(centroids.at<double>(i, 0), centroids.at<double>(i, 1));

      if (save_images) {
        circle(trg, p, 3, Scalar(100, 100, 100), 1, LINE_AA);
        circle(trg, p, 3, Scalar(100, 100, 100), 1, LINE_AA);
      }

      p = (p * org_image_size) / (2 * RESIZE_RADIUS + 1) + origin;

      features.push_back(p);
      // points.append(self.get_normalized_point(centroids[i], center,
      // radius_2d));
    }
  }

  if (save_images) {
    resize(trg, trg, Size(), 3, 3, INTER_LANCZOS4);

    // Uncomment below if you want to see every results.
    // namedWindow("image", WINDOW_AUTOSIZE);
    // imshow("image", blured_img);
    // waitKey(30);

    cv::imwrite(path + "07_label_" + basename, trg);
  }
}

void FeatureDefine::refine_features(std::vector<Point2d> &features,
                                    const cv::Point2d &ball_pos_2d,
                                    double radius_2d, // around 38.81
                                    int org_image_size, const cv::Mat &img,
                                    const cv::Point2d &origin, bool save_images,
                                    const std::string &path,
                                    const std::string &basename) {
  std::vector<Point2d> input_features = features;
  features.clear();
  Mat trg = img.clone();

  for (std::vector<Point2d>::iterator f = input_features.begin();
       f != input_features.end(); f++) {

    if (save_images) {
      Point2d p = (*f - origin) * (2 * RESIZE_RADIUS + 1) / org_image_size;
      circle(trg, p, 3, Scalar(100, 100, 100), 1, LINE_AA);
      circle(trg, p, 3, Scalar(100, 100, 100), 1, LINE_AA);
    }
    Point2d p = *f - ball_pos_2d;
    // Normalized domain
    double dimple_diameter_d4 = DIMPLE_DIAMETER / GOLF_BALL_DIAMETER / 2;
    double r = sqrt(p.x * p.x + p.y * p.y) / radius_2d;
    // NOTE: (int)((1-epsilon) * TILT_CORRECTION_COEFF_BOOSTER) == 255
    double tilt_correction = (r >= 1)
                                 ? 0
                                 : tilt_correction_coeff_table[(
                                       int)(r * TILT_CORRECTION_COEFF_BOOSTER)];

    // double tilt_correction_original = sin(2 * ((r > 1) ? M_PI / 2 :
    // asin(r))); std::cout << tilt_correction << "," <<
    // tilt_correction_original
    //           << std::endl;
    double new_r = r - dimple_diameter_d4 * tilt_correction;

    if (0.15 < new_r && new_r < 0.9) {
      p *= new_r / r;
      p += ball_pos_2d;
      features.push_back(p);
    }

    if (save_images) {
      Point2d tp = (p - origin) * (2 * RESIZE_RADIUS + 1) / org_image_size;
      circle(trg, tp, 1, Scalar(10, 10, 10), 1, LINE_AA);
      circle(trg, tp, 1, Scalar(10, 10, 10), 1, LINE_AA);
    }
  }

  if (save_images) {
    Mat resized_img;
    resize(trg, resized_img, Size(), 3, 3, INTER_LANCZOS4);

    // Uncomment below if you want to see every results.
    // namedWindow("image", WINDOW_AUTOSIZE);
    // imshow("image", resized_img);
    // waitKey(30);

    cv::imwrite(path + "20_result_" + basename, resized_img);
  }
}

int reduce_highlight(Mat &mat, int pixel_num, int minimum_value,
                     int highlight_roi) {
  // // Select ROI
  // Rect2d r = selectROI(im);

  // // Crop image
  // Mat imCrop = im(r);

  int hist[256] = {
      0,
  };
  int ll = int(RESIZE_RADIUS - highlight_roi / 2);
  int ul = int(RESIZE_RADIUS + highlight_roi / 2);
  for (int i = ll; i <= ul; i++) {
    for (int j = ll; j <= ul; j++) {
      hist[mat.at<uchar>(j, i)]++;
    }
  }

  int clip_thd = minimum_value;
  int accum = 0;
  for (int i = 255; i > minimum_value; i--) {
    accum += hist[i];
    if (accum > pixel_num) {
      clip_thd = i;
      break;
    }
  }

  for (int i = ll; i <= ul; i++) {
    for (int j = ll; j <= ul; j++) {
      if (mat.at<uchar>(j, i) > clip_thd)
        mat.at<uchar>(j, i) = clip_thd;
    }
  }
  return clip_thd;
}

void blur(const Mat &src, Mat &dst, int blur_type, int kernel_size) {
  assert(kernel_size % 2 == 1 && "Check it's odd.");
  switch (blur_type) {
  case FeatureDefine::BLUR_HOMOGENEOUS:
    cv::blur(src, dst, Size(kernel_size, kernel_size),
             Point(-1, -1) /*Anchor: kernel center*/);
    break;
  case FeatureDefine::BLUR_GUASSIAN:
    GaussianBlur(src, dst, Size(kernel_size, kernel_size), 0, 0);
    break;
  case FeatureDefine::BLUR_MEDIAN:
    medianBlur(src, dst, kernel_size);
    break;
  case FeatureDefine::BLUR_BILATERAL:
    bilateralFilter(src, dst, kernel_size, kernel_size * 2, kernel_size / 2);
    break;
  default:
    assert(false && "blur_type is weird.");
  }
}

void invert_marker(Mat &mat, int block_size, int constant_offset,
                   int dilation_size, int clip_val) {
  Mat logos;
  cv::adaptiveThreshold(mat, logos, 255, ADAPTIVE_THRESH_MEAN_C,
                        THRESH_BINARY_INV, block_size, constant_offset);
  Mat element = getStructuringElement(
      MORPH_ELLIPSE, Size(2 * dilation_size + 1, 2 * dilation_size + 1),
      Point(dilation_size, dilation_size));
  cv::dilate(logos, logos, element);

  for (int r = 0; r < mat.rows; ++r) {
    unsigned char *plogos = logos.ptr<unsigned char>(r);
    unsigned char *pmat = mat.ptr<unsigned char>(r);
    for (int c = 0; c < mat.cols; ++c) {
      if (plogos[c] == 255)
        pmat[c] = clip_val;
    }
  }
}

void make_adjust_constast_kernel(Mat &kernel, int strength, int kernel_size,
                                 int sigma_x10) {
  assert(kernel_size % 2 == 1 && "Check it's odd.");
  assert(strength >= 1);

  // Mat kernel = getGaussianKernel(kernel_size, (double)sigma_x10 / 10,
  // CV_64F); kernel *= strength; kernel -= ((double)(strength - 1) /
  // (kernel_size)); sepFilter2D(src, dst, -1, kernel, kernel);

  // TODO: Change it to fixed table or on-time-calculation structure.
  Mat kernel_1d =
      getGaussianKernel(kernel_size, (double)sigma_x10 / 10, CV_64F);
  kernel = kernel_1d * kernel_1d.t();
  kernel *= strength;
  kernel -= ((double)(strength - 1) / (kernel_size * kernel_size));
  assert(fabs(sum(kernel)[0] - 1) < FLT_EPSILON);
}

void adjust_contrast(const Mat &src, Mat &dst, const Mat &kernel) {
  filter2D(src, dst, -1, kernel);
}

void adaptive_threshold(const Mat &src, Mat &dst, int type, int window_size,
                        int C) {
  adaptiveThreshold(src, dst, 255, type, THRESH_BINARY, window_size, C);
}
// open(src, dst, 5, 1);
// open(src, dst, 3, 3);
void open(const Mat &src, Mat &dst, int kernel_size, int iteration_num) {
  Mat mask =
      getStructuringElement(MORPH_ELLIPSE, Size(kernel_size, kernel_size));
  morphologyEx(src, dst, MORPH_OPEN, mask, Point(-1, -1), iteration_num);
}

static bool is_weird(int w, int h, int A, double dimple_radius,
                     double single_dimple_area) {

  return (A < single_dimple_area / 15 || A < (w * h) / 2 || w > 2 * h ||
          h > 2 * w || w > 2.6 * dimple_radius || h > 2.6 * dimple_radius ||
          A > 1.3 * single_dimple_area);
}

int labeling(const Mat &src, Mat &labels, Mat &stats, Mat &centroids) {
  int label_num = connectedComponentsWithStats(src, labels, stats, centroids, 4,
                                               CV_16U, CCL_DEFAULT);
  return label_num;
}
