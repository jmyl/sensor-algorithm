#include "rodrigues.h"
#include <opencv2/calib3d.hpp>
#include <opencv2/core/eigen.hpp>

using namespace cv;
using namespace Eigen;

Matrix3d get_rodrigues_matrix(Vector3d rotation_vector) {
  cv::Mat in(3, 1, CV_64FC1);
  eigen2cv(rotation_vector, in);
  cv::Mat out(3, 3, CV_64FC1);
  Rodrigues(in, out);
  Matrix3d rodrigues_form;
  cv2eigen(out, rodrigues_form);
  return rodrigues_form;
}

Vector3d get_rodrigues_vector(const Matrix3d &rotation_matrix) {
  cv::Mat in(3, 3, CV_64FC1);
  eigen2cv(rotation_matrix, in);
  cv::Mat out(3, 1, CV_64FC1);
  Rodrigues(in, out);
  Vector3d rodrigues_form;
  cv2eigen(out, rodrigues_form);
  return rodrigues_form;
}
