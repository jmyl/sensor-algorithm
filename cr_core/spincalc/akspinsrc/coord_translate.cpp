#include "cr_geometric.h"
#include "coord_translate.h"
#include "feature_define.h"
#include "rodrigues.h"
#if defined(_WIN32)
#include <Eigen/Dense>
#else
#include <eigen3/Eigen/Dense>
#endif
#include <iostream>
#include <math.h> /* sqrt */
#include <opencv2/calib3d.hpp>

using namespace std;
using namespace Eigen;
using namespace cv;
using Eigen::MatrixXd;

void ball_info_world_to_local(Point2d &pos_l, double &radius_l,
                              const Vector3d &pos_w, double radius_w,
                              Matrix3Xd intrinsic, Matrix4d extrinsic) {
  Vector4d pos_homocoord;
  pos_homocoord << pos_w, 1;
  Vector4d X = extrinsic * pos_homocoord;

  radius_l = (radius_w / X(2)) // on the normalized image plane
             * (intrinsic(0, 0) + intrinsic(1, 1)) / 2; // focal length in pixel

  intrinsic.conservativeResize(intrinsic.rows(), intrinsic.cols() + 1);
  intrinsic.col(intrinsic.cols() - 1) << 0, 0, 0;
  Vector3d pos_2d_homocoord = intrinsic * X;
  pos_l.x = pos_2d_homocoord(0) / pos_2d_homocoord(2);
  pos_l.y = pos_2d_homocoord(1) / pos_2d_homocoord(2);
}

Matrix3d get_aspect_conversion_matrix(const Vector3d &from,
                                      const Vector3d &to) {
  if (from == to)
    return Matrix3d::Identity();

  Vector3d from_norm = from.normalized();
  Vector3d to_norm = to.normalized();

  Vector3d rotational_axis = -from.cross(to.transpose());
  Vector3d rotational_axis_norm = rotational_axis.normalized();
  double rotational_angle = acos(from_norm.transpose() * to_norm);

  Vector3d rotation_vector = rotational_axis_norm * rotational_angle;
  return get_rodrigues_matrix(rotation_vector);
}

void coord_translate_L2P(vector<Point2d> &ft_input, vector<coord_t> &ft,
                         Matrix4d full_inv_ext_mat, Matrix3d int_mat,
                         Vector3d cam_position, Vector3d ball_position,
                         double ball_radius) {
  cr_sphere_t ball;
  cr_sphere_make((cr_point_t *)(&ball_position), ball_radius, &ball);

  for (vector<Point2d>::iterator f = ft_input.begin(); f != ft_input.end();
       f++) {
    cr_line_t cam_feature_line;
    cr_point_t ballray_intersections[2];
    U32 pcount, ballfeature_res;
    double pdter;

    // Vector3d featurePix2;
    // featurePix2 << f->entry[0] + rough_crop_info.x + crop_info.x,
    //     f->entry[1] + rough_crop_info.y + crop_info.y, 1.;
    // cout << "featurePix2:" << endl << featurePix2 << endl;

    Vector3d featurePix;
    featurePix << f->x, f->y, 1.;
    // cout << "featurePix:" << endl << featurePix << endl;

    Vector4d featureNCam;
    featureNCam << int_mat.inverse() * featurePix, 1.;

    Vector4d featureNCamWld = full_inv_ext_mat * featureNCam;
    cr_point_t featureNCamWld3d;
    featureNCamWld3d.x = featureNCamWld[0];
    featureNCamWld3d.y = featureNCamWld[1];
    featureNCamWld3d.z = featureNCamWld[2];

    cr_line_p0p1((cr_point_t *)(&cam_position), &featureNCamWld3d,
                 &cam_feature_line);
    ballfeature_res = cr_line_sphere_mild(
        &cam_feature_line, &ball, ballray_intersections, &pcount, &pdter);
    if (ballfeature_res == 1) {
      coord_t p = {{ballray_intersections[0].x - ball_position(0),
                    ballray_intersections[0].y - ball_position(1),
                    ballray_intersections[0].z - ball_position(2)}};
      ft.push_back(p);
    }

    // Matrix3d flip_matrix;
    // // clang-format off
    // flip_matrix <<
    //     1,  0,  0,
    //     0, -1,  0,
    //     0,  0, -1;
    // // clang-format on
    // Matrix3d ball_pos_corr_mat = get_aspect_conversion_matrix(
    //     ball_position - cam_position, -cam_position);

    // Matrix3d feature_conversion_matrix_from_image_to_world =
    //     ball_pos_corr_mat * full_inv_ext_mat * flip_matrix;

    // Vector3d feature(f->entry);
    // feature = feature_conversion_matrix_from_image_to_world * feature;
    // f->entry[0] = feature(0);
    // f->entry[1] = feature(1);
    // f->entry[2] = feature(2);
  }
}

// clang-format off
// features = []
// for image_name, (cropped_image, c_x, c_y, r) in sorted(cropped_images_with_center.items()):
//     resized_image = cv2.resize(cropped_image, (256,256), interpolation = cv2.INTER_LINEAR)
//     feature_list = feature_detector.get_features_single_image(resized_image, (128, 128), 128)


//     feature_list_world = MatchingAlgorithm.rotate_points(feature_list, feature_conversion_matrix_from_image_to_world)
//     features.append(feature_list_world)
// return features
// clang-format on
