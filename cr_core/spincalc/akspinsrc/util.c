#include <math.h>
#include <stdlib.h>
#include <string.h>

#define __PRIVATE_INCLUDE
#include "util.h"

#define EPSILON 0.000001

double radians(double degree) { return degree * M_PI / 180.0; }

/* In our case, we don't need to cover every angles.
 * This function could only convert the rotations, which is constraint in a
 * certain range. */
void Rodrigues_mat2vec(const matrix_t *rot_mat, coord_t *rot_vec) {
  double trace =
      rot_mat->entry[0][0] + rot_mat->entry[1][1] + rot_mat->entry[2][2];
  if (3 - 2 * EPSILON < trace) {
    rot_vec->f.x = 0;
    rot_vec->f.y = 0;
    rot_vec->f.z = 0;

  } else if (-1 + 2 * EPSILON > trace) {
    if (rot_mat->entry[0][0] < -0.3) {
      memcpy(rot_vec, rot_mat->row, sizeof(coord_t));
    } else if (rot_mat->entry[1][1] < -0.3) {
      memcpy(rot_vec, rot_mat->row + 1, sizeof(coord_t));
    } else {
      memcpy(rot_vec, rot_mat->row + 2, sizeof(coord_t));
    }
    normalize(rot_vec, rot_vec);
    rot_vec->f.x = rot_vec->f.x * M_PI_2;
    rot_vec->f.y = rot_vec->f.y * M_PI_2;
    rot_vec->f.z = rot_vec->f.z * M_PI_2;

  } else {
    double ctheta = (trace - 1) / 2;
    coord_t rho;
    double stheta;
    double theta;
    rho.f.x = rot_mat->entry[2][1] - rot_mat->entry[1][2];
    rho.f.y = rot_mat->entry[0][2] - rot_mat->entry[2][0];
    rho.f.z = rot_mat->entry[1][0] - rot_mat->entry[0][1];

    stheta = get_norm(&rho) / 2;
    normalize(&rho, &rho);
    theta = atan2(stheta, ctheta);
    rot_vec->f.x = rho.f.x * theta;
    rot_vec->f.y = rho.f.y * theta;
    rot_vec->f.z = rho.f.z * theta;
  }
}

#define INIT_MATRIX(mx, e1, e2, e3, e4, e5, e6, e7, e8, e9)                    \
  do {                                                                         \
    mx.entry[0][0] = (e1);                                                     \
    mx.entry[0][1] = (e2);                                                     \
    mx.entry[0][2] = (e3);                                                     \
    mx.entry[1][0] = (e4);                                                     \
    mx.entry[1][1] = (e5);                                                     \
    mx.entry[1][2] = (e6);                                                     \
    mx.entry[2][0] = (e7);                                                     \
    mx.entry[2][1] = (e8);                                                     \
    mx.entry[2][2] = (e9);                                                     \
  } while (0)

void Rodrigues_vec2mat(matrix_t *rot_mat, const coord_t *rot_vec) {

  double theta = get_norm(rot_vec);
  double entry[3];
  double ctheta, stheta;
  matrix_t cid;
  matrix_t ucross;
  size_t i, j;

  if (theta < EPSILON) {
    rot_mat->entry[0][0] = 1;
    rot_mat->entry[0][1] = 0;
    rot_mat->entry[0][2] = 0;
    rot_mat->entry[1][0] = 0;
    rot_mat->entry[1][1] = 1;
    rot_mat->entry[1][2] = 0;
    rot_mat->entry[2][0] = 0;
    rot_mat->entry[2][1] = 0;
    rot_mat->entry[2][2] = 1;
    return;
  }

  entry[0] = (rot_vec->f.x) / theta;
  entry[1] = (rot_vec->f.y) / theta;
  entry[2] = (rot_vec->f.z) / theta;
  ctheta = cos(theta), stheta = sin(theta);
  INIT_MATRIX(cid,
              /* clang-format off */
    ctheta,0,0,
    0,ctheta,0,
    0,0,ctheta
              /* clang-format on */
  );
  INIT_MATRIX(ucross,
              /* clang-format off */
    0,-entry[2],entry[1],
    entry[2],0,-entry[0],
    -entry[1],entry[0],0
              /* clang-format on */
  );

  for (i = 0; i < 3; i++)
    for (j = 0; j < 3; j++) {
      rot_mat->entry[i][j] = cid.entry[i][j] +
                             entry[i] * entry[j] * (1 - ctheta) +
                             ucross.entry[i][j] * stheta;
      /*                          I cos(theta) +      uuT (1 - cos(theta)) +
       * u_x sin(theta) */
    }
}
