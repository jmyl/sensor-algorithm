#include "svd3.h"

#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>
#include <string.h>

#define __PRIVATE_INCLUDE
#include "numpy.h"

#define X_ 0
#define Y_ 1
#define Z_ 2

void add(const coord_t *p1, const coord_t *p2, coord_t *sum) {
  (sum->f.x) = (p1->f.x) + (p2->f.x);
  (sum->f.y) = (p1->f.y) + (p2->f.y);
  (sum->f.z) = (p1->f.z) + (p2->f.z);
}

void subtract(const coord_t *p1, const coord_t *p2, coord_t *subtract) {
  (subtract->f.x) = (p1->f.x) - (p2->f.x);
  (subtract->f.y) = (p1->f.y) - (p2->f.y);
  (subtract->f.z) = (p1->f.z) - (p2->f.z);
}

void vector_scalar_multi(const coord_t *vector, const double scalar,
                         coord_t *res_vec) {
  res_vec->f.x = vector->f.x * scalar;
  res_vec->f.y = vector->f.y * scalar;
  res_vec->f.z = vector->f.z * scalar;
}

#define DOUBLE_SWAP(x, y)                                                      \
  do {                                                                         \
    double __SWAP = *(x);                                                      \
    *(x) = *(y);                                                               \
    *(y) = __SWAP;                                                             \
  } while (0)

/*
 * You could simply send same parameter to input and output.
 */
void transpose(const matrix_t *in, matrix_t *out) {
  if (in != out)
    memcpy(out, in, sizeof(*in));

  DOUBLE_SWAP(&out->entry[0][1], &out->entry[1][0]);
  DOUBLE_SWAP(&out->entry[0][2], &out->entry[2][0]);
  DOUBLE_SWAP(&out->entry[1][2], &out->entry[2][1]);
}

void matrix_dot(const matrix_t *matrix1, const matrix_t *matrix2,
                matrix_t *dot_res) {
  size_t i, j, k;
  for (i = 0; i < 3; i++) {
    for (j = 0; j < 3; j++) {
      dot_res->entry[i][j] = 0;
      for (k = 0; k < 3; k++) {
        dot_res->entry[i][j] += matrix1->entry[i][k] * matrix2->entry[k][j];
      }
    }
  }
}

void cross_prod(const coord_t *vec1, const coord_t *vec2, coord_t *cross) {
  cross->f.x = vec1->f.y * vec2->f.z - vec2->f.y * vec1->f.z;
  cross->f.y = vec1->f.z * vec2->f.x - vec2->f.z * vec1->f.x;
  cross->f.z = vec1->f.x * vec2->f.y - vec2->f.x * vec1->f.y;
}

double get_norm(const coord_t *vec) {
  return sqrt(vec->f.x * vec->f.x + vec->f.y * vec->f.y + vec->f.z * vec->f.z);
}

void normalize(const coord_t *vec_in, coord_t *vec_out) {
  double norm = get_norm(vec_in);

  vec_out->f.x = vec_in->f.x / norm;
  vec_out->f.y = vec_in->f.y / norm;
  vec_out->f.z = vec_in->f.z / norm;
}

void mv_dot(const matrix_t *matrix, const coord_t *vec, coord_t *res_vec) {
  res_vec->f.x = vector_dot(matrix->row + 0, vec);
  res_vec->f.y = vector_dot(matrix->row + 1, vec);
  res_vec->f.z = vector_dot(matrix->row + 2, vec);
}

void matrix_dot_n(const coord_t *left_n_matrix, const coord_t *right_n_matrix,
                  const size_t matrix_length, matrix_t *result_33_matrix) {
  size_t i, j, k;
  for (i = 0; i < 3; i++) {
    for (j = 0; j < 3; j++) {
      result_33_matrix->entry[i][j] = 0;
      for (k = 0; k < matrix_length; k++) {
        result_33_matrix->entry[i][j] +=
            left_n_matrix[k].entry[i] * right_n_matrix[k].entry[j];
      }
    }
  }
}

void svd(matrix_t *U, coord_t *S, matrix_t *V, matrix_t *A) {
  /* svd3.c treats martices as column major but we do row major
   * diagonal matrix does not matter.
   * Not to transpost every time, just change multiplication order of U and V
   * then transpose once at the very last */
  svd3((double *)V->entry, (double *)S->entry, (double *)U->entry,
       (double *)A->entry);
  transpose(U, U);
  transpose(V, V);
}

double get_distance_between_normalized_points(const coord_t *point1,
                                              const coord_t *point2) {
  return sqrt(2 - 2 * vector_dot(point1, point2));
}

double determinant(const matrix_t *matrix) {

  const double(*entry)[3] = matrix->entry;
  return entry[0][0] * (entry[1][1] * entry[2][2] - entry[1][2] * entry[2][1]) +
         entry[0][1] * (entry[1][2] * entry[2][0] - entry[1][0] * entry[2][2]) +
         entry[0][2] * (entry[1][0] * entry[2][1] - entry[1][1] * entry[2][0]);
}
