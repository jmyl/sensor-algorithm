#ifndef __NUMPY_H__
#define __NUMPY_H__

#include <stddef.h>

typedef union {
  double entry[3];
  struct {
    double x;
    double y;
    double z;
  } f;
} coord_t, point3_t;

typedef union {
  double entry[3][3];
  coord_t row[3];
} matrix_t;
/* TODO: Add demention information to the name.*/

#ifdef __PRIVATE_INCLUDE

void add(const coord_t *p1, const coord_t *p2, coord_t *sum);
void subtract(const coord_t *p1, const coord_t *p2, coord_t *subtract);
void vector_scalar_multi(const coord_t *vector, const double scalar,
                         coord_t *res_vec);
void transpose(const matrix_t *in, matrix_t *out);
#define vector_dot(vec1, vec2)                                                 \
  ((vec1)->f.x * (vec2)->f.x + (vec1)->f.y * (vec2)->f.y +                     \
   (vec1)->f.z * (vec2)->f.z)

void matrix_dot(const matrix_t *matrix1, const matrix_t *matrix2,
                matrix_t *dot_res);
void mv_dot(const matrix_t *matrix, const coord_t *vec, coord_t *res_vec);
void cross_prod(const coord_t *vec1, const coord_t *vec2, coord_t *cross);
double get_norm(const coord_t *vec);
void normalize(const coord_t *vec_in, coord_t *vec_out);
void svd(matrix_t *U, coord_t *S, matrix_t *V, matrix_t *A);
void matrix_dot_n(const coord_t *left_n_matrix, const coord_t *right_n_matrix,
                  const size_t matrix_length, matrix_t *result_33_matrix);
double determinant(const matrix_t *matrix);
double get_distance_between_normalized_points(const coord_t *point1,
                                              const coord_t *point2);

#endif

#endif /* __NUMPY_H__ */
