/***************************************************************************/
/*!
    @file   AK_algorithm.h
    @brief  $Rotation computation module using dimple and othermark features

    @section copyright_notice COPYRIGHT NOTICE
    Copyright (c) 2019~ by Creatz Inc. \n
    All Rights Reserved. \n
    Do not duplicate without prior written consent of Creatz Inc.

    @section file_information FILE CREATION INFORMATION
    @author Hyeonseok Choi, Sungsik Nam
    @date   2019/10/21 First Created

    @section checkin_information LATEST CHECK-IN INFORMATION
    @author	Hyeonseok Choi
    @date	2020/07/19
    @rev	2020/07/22
 */
/***************************************************************************/
#ifndef __AK_ALGORITHM_H__
#define __AK_ALGORITHM_H__

#ifdef __cplusplus
extern "C" {
#endif

/*----------------------------------------------------------------------------
        Description	: defines referenced header files
-----------------------------------------------------------------------------*/

#include <stddef.h>

#ifdef __PRIVATE_INCLUDE
#include "numpy.h"
#include "util.h"
#if (_MSC_VER >= 1400)
#include <wtypes.h>
#else
#include <stdbool.h>
typedef bool BOOL;
#define TRUE true
#define FALSE false
#endif  // _MSC_VER >= 1400
#endif  // __PRIVATE_INCLUDE

/*----------------------------------------------------------------------------
        Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

#define AVERAGE_DIMPLE_FEATURE_COUNT_THRESHOLD 30
#define AVERAGE_OTHERMARK_FEATURE_COUNT_THRESHOLD 10

#define IGNORE_FTRS_THRESH_OTHERMK 5
#define IGNORE_FTRS_THRESH_DIMPLE 10
#define AVG_NUM_FTRS_THRESH_OTHERMK 10
#define AVG_NUM_FTRS_THRESH_DIMPLE 30

#define RESTRICT_TOPSPIN_MODE_CALCULATE_ALL 0
#define RESTRICT_TOPSPIN_MODE_CONDITIONAL 1
#define RESTRICT_TOPSPIN_MODE_RESTRICT_ALL 2

#define _BV(x) (1 << (x))

#ifdef __PRIVATE_INCLUDE
#define half_num_of_side_mesh 4
#define num_of_side_mesh (half_num_of_side_mesh * 2)
#define num_of_index_grid (num_of_side_mesh /*+2*/)
#define max_index (num_of_index_grid * num_of_index_grid * num_of_index_grid)

#define _GET_INDEX(i, j, k)                            \
  ((size_t)(i)*num_of_index_grid * num_of_index_grid + \
   (size_t)(j)*num_of_index_grid + (size_t)(k))

#define INDEX_DEPTH (5 * 3 * 3)

#endif  // __PRIVATE_INCLUDE

/*----------------------------------------------------------------------------
        Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/

typedef enum {
  AK_SUMMARY_GOOD,
  AK_SUMMARY_SOSO,
  AK_SUMMARY_BAD,
  AK_SUMMARY_FAULT,

  AK_SPINTYPE_TopSpin,
  AK_SPINTYPE_LargeRollSpin,
  AK_SPINTYPE_LargeSideSpin,
  AK_SPINTYPE_Undefined2,

  AK_MATCHINGMODE_DIMPES,
  AK_MATCHINGMODE_OTHERMAKRS,
  AK_MATCHINGMODE_SMALLDIMPLES,
  AK_MATCHINGMODE_NONE,

  AK_ERROR_TooSmallFeaturesCannotMatch,
  AK_ERROR_NoMatchedRotationCandidates,
  AK_ERROR_Unclassified1,
  AK_ERROR_Unclassified2,

  AK_WARNING_NotEnoughDimples,
  AK_WARNING_NotEnoughRotationCandidates,
  AK_WARNING_LargeMatchingDifference,
  AK_WARNING_Undefined,

  AK_RESULT_LIMIT
} ak_result_fields_t;

/*!
    @typedef	pairing_index_info_t
    @brief		type for selecting the best pairing position. Pairing is
                designed between adjacent frames so enough to know index of
                former frame(indexFrom). And to select the best pairing index,
                which have the most num of features, define indexRef.
*/
typedef struct {
  size_t indexFrom;
  size_t indexRef;
} pairing_index_info_t;

#ifdef __PRIVATE_INCLUDE
struct member_var {
  int area_diff_3D[3][3][3];
  /* int number_feature_used; */
  int sampling_number_frame_1;
  int sampling_number_frame_2;
  int min_dimple_features_count;
  int min_othermark_features_count;
};
extern struct member_var self;
typedef struct {
  matrix_t rot_mat;
  double error;
} errored_rotation_t;
typedef struct {
  size_t len;
  coord_t coord[INDEX_DEPTH];
} index_stack_t;
struct feature_info {
  size_t feature_num;
  coord_t *sorted_features;
  index_stack_t *indexed_features;
  double *feature_distance_matrix;
  /*int timestamp; */
};
typedef struct {
  coord_t *matched_point_from;
  coord_t *matched_point_to;
  size_t matched_points_count;
  double square_mean_error;
} match_result_t;
#endif  // __PRIVATE_INCLUDE

#ifdef AK_DECLARE_FOR_ERROR_ANALYSIS

#ifndef _LEN
#define _LEN(x) (sizeof(x) / sizeof(x[0]))
#endif
const char *ak_result_field_names[] = {
    "AK_SUMMARY_GOOD",
    "AK_SUMMARY_SOSO",
    "AK_SUMMARY_BAD",
    "AK_SUMMARY_FAULT",

    "AK_SPINTYPE_TopSpin",
    "AK_SPINTYPE_LargeRollSpin",
    "AK_SPINTYPE_LargeSideSpin",
    "AK_SPINTYPE_Undefined2",

    "AK_MATCHINGMODE_DIMPES",
    "AK_MATCHINGMODE_OTHERMAKRS",
    "AK_MATCHINGMODE_SMALLDIMPLES",
    "AK_MATCHINGMODE_NONE",

    "AK_ERROR_TooSmallFeaturesCannotMatch",
    "AK_ERROR_NoMatchedRotationCandidates",
    "AK_ERROR_Unclassified1",
    "AK_ERROR_Unclassified2",

    "AK_WARNING_NotEnoughDimples",
    "AK_WARNING_NotEnoughRotationCandidates",
    "AK_WARNING_LargeMatchingDifference",
    "AK_WARNING_Undefined",
};
#endif

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype
 -----------------------------------------------------------------------------*/

void AK_init(size_t sampling_number_frame_1, size_t sampling_number_frame_2);

unsigned int AK_get_rotated_angle(
    double *feature_sequence_dimple, size_t num_frames_dimple, size_t *features_counts_dimple, 
    double *feature_sequence_othermark, size_t num_frames_othermark, size_t *features_counts_othermark,
    double *result_rotation, double feature_vector_length);

/*----------------------------------------------------------------------------
 *	Description	: declares the all function prototype
 -----------------------------------------------------------------------------*/
#ifdef __PRIVATE_INCLUDE
int AK_check_and_normalize_feature_sequence(
    const double *feature_sequence, const size_t num_frames,
    const size_t *features_counts, const double feature_vector_length,
    const size_t min_features_ref, double *normalized_feature_sequence,
    size_t *normalized_features_counts, size_t *organized_num_frames);

unsigned int get_rough_rotate_angle(
    size_t num_frames,
    struct feature_info *sequence_feature_info /* len: num_frames */,
    size_t num_pairing_frames, const pairing_index_info_t *pairing_index_infos,
    const size_t pairing_index_count, int searching_range_in_degrees,
    double searching_angle_thresh_square,
    double minimum_distance_between_features,
    double maximum_distance_between_features, matrix_t *min_error_matrix);

size_t get_rotation_candidiates(const struct feature_info *feature_info_from,
                                const struct feature_info *feature_info_to,
                                const double searching_range_in_degrees,
                                const double searching_angle_thresh_square,
                                const double minimum_distance_between_features,
                                const double maximum_distance_between_features,
                                size_t max_num_of_candidate,
                                matrix_t *candidate_rotations);

void get_best_pairing_position(const size_t *features_counts,
                               const size_t num_total_frames,
                               const size_t num_pairing_frames,
                               pairing_index_info_t *pairing_infos,
                               size_t *paring_index_count);
void init_feature_infos(
    coord_t *feature_sequence, size_t num_frames,
    size_t *features_counts /* [num_frames] */,
    struct feature_info *sequence_feature_info /* [num_frames] */
);
void fin_feature_infos(
    size_t num_frames,
    struct feature_info *sequence_feature_info /* [num_frames] */
);
size_t get_paired_points(const struct feature_info *feature_info_from,
                         const struct feature_info *feature_info_to,
                         const BOOL *feature_pairability_matrix,
                         const double minimum_distance_between_features,
                         const double maximum_distance_between_features,
                         const int sampling_number_frame_1,
                         const int sampling_number_frame_2,
                         const double permitted_feature_distance_error,
                         size_t pair_num,
                         coord_t (*two_points_1)[2] /* [pair_num][2] */,
                         coord_t (*two_points_2)[2] /* [pair_num][2] */
);
size_t get_best_k_rotations(size_t n, errored_rotation_t *r /* [n] */,
                            size_t max_k, matrix_t *result /* [max_k] */);
size_t get_small_error_rotations(
    size_t feature_from_len,
    const coord_t *features_list_from /* [feature_from_len] */,
    const index_stack_t *indexed_list_to, size_t candidate_rotations_count,
    const matrix_t
        *candidate_rotation_matrices /* [candidate_rotations_count] */,
    size_t max_num_of_candidate, matrix_t *small_error_rotations);
void matching_features(
    size_t features_from_count,
    const coord_t *rotated_features /* [features_from_count] */,
    const index_stack_t *indexed_features, match_result_t *return_result);
void indexing_initialize(index_stack_t *indexed_features);
void indexing_run(const size_t feature_num, const coord_t *sorted_features,
                  index_stack_t *indexed_features, size_t neighbor_num,
                  int *neighbor_offset /* [neighbor_num] */
);
void sort_features(size_t len, coord_t *ca);
void get_matrix_from_pair(const coord_t *point1, const coord_t *point2,
                          matrix_t *matrix);
size_t convert_pairs_to_rotations(size_t pair_num,
                                  coord_t (*const pair1)[2] /* [pair_num][2] */,
                                  coord_t (*const pair2)[2] /* [pair_num][2] */,
                                  double searching_angle_thresh_square,
                                  matrix_t *converted_matrices);
int compare_rotation(const void *r1, const void *r2);
void get_pairability_matrix_between_frames(
    size_t m, const coord_t *features_list_from /* [m] */, size_t n,
    const coord_t *features_list_to /* [n] */,
    const double searching_range_threshold_distance,
    BOOL *feature_pairability_matrix /* [][n] */
);
void get_feature_distance_matrix(
    size_t features_count,
    const coord_t *sorted_features /* [features_count] */,
    double *feature_distance_matrix);
void rotate_points(size_t len, const coord_t *points,
                   const matrix_t *rotation_matrix, coord_t *rotated_points);
void refine_rotated_angle(struct feature_info *sequence_feature_info,
                          const size_t num_frames, const size_t frame_gap,
                          const matrix_t *expected_rotation_matrix,
                          matrix_t *refined_rotation);

BOOL is_valid_rotation(double rot_x, double rot_y, double rot_z,
                       double threshAmp);
BOOL is_valid_rotation_(matrix_t *rot, double threshAmp);
size_t get_len_longest_non0_succ_ftr_seq(const size_t *features_counts,
                                         const size_t num_frames);
#endif /* __PRIVATE */

#ifdef __cplusplus
}
#endif

#endif /* !__AK_ALGORITHM_H__ */
