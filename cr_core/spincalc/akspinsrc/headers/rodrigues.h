#ifndef ROADRIGUES_H
#define ROADRIGUES_H

#if defined(_WIN32)
#include <Eigen/Dense>
#else
#include <eigen3/Eigen/Dense>
#endif
#include <opencv2/core.hpp>

Eigen::Matrix3d get_rodrigues_matrix(Eigen::Vector3d rotation_vector);
Eigen::Vector3d get_rodrigues_vector(const Eigen::Matrix3d &rotation_matrix);

#endif
