#ifndef __UTIL_H__
#define __UTIL_H__

#define _USE_MATH_DEFINES
#include "numpy.h"
#include <math.h>

#ifndef M_PI
#define M_PI 3.1415926535
#endif
#ifndef M_PI_2
#define M_PI_2 1.570796327
#endif
#ifndef M_PI_4
#define M_PI_4 0.785398163
#endif

double radians(double degree);
void Rodrigues_mat2vec(const matrix_t *rot_mat, coord_t *rot_vec);
void Rodrigues_vec2mat(matrix_t *rot_mat, const coord_t *rot_vec);

#endif /* !__UTIL_H__ */
