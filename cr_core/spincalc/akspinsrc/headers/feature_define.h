/**
 */

#ifndef __FEATURE_DEFINE_H__
#define __FEATURE_DEFINE_H__

// #include "opencv2/imgproc/imgproc.hpp"
#include "numpy.h"
#include <opencv2/imgproc.hpp>
#include <stddef.h>

namespace FeatureDefine {

#define GOLF_BALL_DIAMETER 42.8 // unit : mm
#define DIMPLE_DIAMETER 4       // unit : mm

typedef struct {
  int blur_kernel_type;                   // "box"
  int blur_kernel_size;                   // 9,
  int get_marker_block_size;              //
  int get_marker_constant_offset;         //
  int get_marker_dilation_size;           //
  int contrast_adjust_strength;           // 15,
  int contrast_adjust_kernel_size;        // 15,
  int contrast_adjust_sigma_x10;          // 11
  int adaptive_threshold_type;            // ADAPTIVE_THRESH_MEAN_C /
                                          // ADAPTIVE_THRESH_GAUSSIAN_C
  int adaptive_threshold_window_size;     // 25,
  int adaptive_threshold_constant_offset; // 28,
  int open_1_kernel_size;                 // 5
  int open_1_iteration_num;               // 1
  int open_2_kernel_size;                 // 3
  int open_2_iteration_num;               // 3
} params_t;

static params_t params;
static cv::Mat adjust_contrast_kernel;
#define TILT_CORRECTION_COEFF_BOOSTER 512
static double tilt_correction_coeff_table[TILT_CORRECTION_COEFF_BOOSTER];

/* Public Interfaces */
enum BlurTypes { BLUR_HOMOGENEOUS, BLUR_GUASSIAN, BLUR_MEDIAN, BLUR_BILATERAL };

template <class... T> params_t params_list(T... args) {
  return params_t{args...};
}
void init(const params_t &params);
void get_denoised_img(const cv::Mat &src, cv::Mat &trg,
                      bool save_images = false, const std::string &path = (""),
                      const std::string &basename = (""));
void get_binarized_img(const cv::Mat &src, cv::Mat &trg,
                       bool save_images = false, const std::string &path = (""),
                       const std::string &basename = (""));
void get_center_of_features(const cv::Mat &src, const cv::Point2d &origin,
                            int org_image_size,
                            std::vector<cv::Point2d> &features,
                            bool save_images = false,
                            const std::string &path = (""),
                            const std::string &basename = (""));
void refine_features(std::vector<cv::Point2d> &features,
                     const cv::Point2d &ball_pos_2d, double radius,
                     int org_image_size, const cv::Mat &img,
                     const cv::Point2d &origin, bool save_images = false,
                     const std::string &path = (""),
                     const std::string &basename = (""));
} // namespace FeatureDefine

#endif /* !__FEATURE_DEFINE_H__ */
