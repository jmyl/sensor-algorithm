#ifndef __COORD_TRANSLATE_H__
#define __COORD_TRANSLATE_H__

#include "cr_geometric.h"
#include "numpy.h"
#if defined(_WIN32)
#include <Eigen/Dense>
#else
#include <eigen3/Eigen/Dense>
#endif
#include <opencv2/core.hpp>
#include <vector>

void ball_info_world_to_local(cv::Point2d &pos_l, double &radius_l,
                              const Eigen::Vector3d &pos_w, double radius_w,
                              Eigen::Matrix3Xd intrinsic,
                              Eigen::Matrix4d extrinsic);

void coord_translate_L2P(std::vector<cv::Point2d> &ft_input,
                         std::vector<coord_t> &ft,
                         Eigen::Matrix4d full_inv_ext_mat,
                         Eigen::Matrix3d int_mat, Eigen::Vector3d cam_position,
                         Eigen::Vector3d ball_position, double ball_radius);

Eigen::Matrix3d get_aspect_conversion_matrix(const Eigen::Vector3d &from,
                                             const Eigen::Vector3d &to);

#endif // __COORD_TRANSLATE_H__
