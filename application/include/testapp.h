#if !defined(_TESTAPP_H_)
#define		 _TESTAPP_H_


#define INTER_TESTAPP_IANA_FILE			0
#define INTER_TESTAPP_OPENCV		1
#define INTER_TESTAPP_CAM_CTRL		2
#define INTER_TESTAPP_INI			3
#define INTER_TESTAPP_DEFAULT		9999


void TESTAPP_IANA_File(char *filename, int image_display, int club);
void TESTAPP_OpenCV(char *filename);
void TESTAPP_CAM_Ctrl(char *filename);
int TESTAPP_Libini (int argc, char *argv[]);




#endif

