add_library(opencv STATIC
    opencv4_test.cpp
    )

target_include_directories(opencv
    PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}"
    )

find_package( OpenCV REQUIRED )

target_link_libraries(opencv
    PRIVATE
        cr_common
        cr_api
        iana
        spincalc
        # gigev
        # pcie
        scamif
        cr_osal
        iana
        ${OpenCV_LIBS}
)

