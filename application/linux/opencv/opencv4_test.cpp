#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "cr_common.h"

#include "cr_thread.h"
#include "cr_osapi.h"


#include "iana.h"

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

#define LOCAL_DEBUG_ON 1
#define LOCAL_ERROR_TRACE_ON 1
#include "cr_dbg_on.h"


using namespace cv;

/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

	

#if defined (__cplusplus)
extern "C" {
#endif

void TESTAPP_OpenCV(char *filename)
{
	 Mat rawimg;
	 rawimg = imread(filename, IMREAD_COLOR);

	 LOCAL_DEBUG(("TESTAPP_OpenCV: %s\n", filename));
}


#if defined (__cplusplus)
}
#endif


