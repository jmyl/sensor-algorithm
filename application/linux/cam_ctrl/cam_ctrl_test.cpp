/*
 * cam_ctrl_test.cpp
 *
 * Copyright 2021 Creatz
 *
 * CAM Viewer test application
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "cr_common.h"

#include "cr_thread.h"
#include "cr_osapi.h"

#include "scamif.h"
#include "scamif_buffer.h"

#include "iana.h"

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

using namespace cv;


#define LOCAL_DEBUG_ON 1
#define LOCAL_ERROR_TRACE_ON 1
#include "cr_dbg_on.h"



/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

#define MAX_CAM_NUM		4
//#define NUM_CAM			2

#define SCREEN_WIDTH	1920
#define SCREEN_HEIGHT	1200
#define SCREEN_X_START	100	
#define SCREEN_Y_START	100



#define MAX_WIDTH		1280
#define MAX_HEIGHT		1024
#define WIN_WIDTH		((SCREEN_WIDTH-200)/NUM_CAM )
#define WIN_HEIGHT		((SCREEN_HEIGHT-200)/NUM_CAM)

#define IMAGE_TEST

#define MAX_STATE	5

enum {
	STATE_STOP = 1,
	STATE_START = 2,	
	STATE_SEARCH,
	STATE_READY,
	STATE_LAUNCH,	
};

String CamWindowName[MAX_CAM_NUM] = {"CAM0", "CAM1", "CAM2", "CAM3"};
String TestBtnName = "Test";
String StateBtnName[MAX_STATE] = {"Stop", "Start", "Search", "Ready", "Lanuch"};


Point prevPt[MAX_CAM_NUM];
Point newPt[MAX_CAM_NUM];

int TestState;
int PrevTestState;

int RunState;
int PrevRunState;

int PrevLedBrightness[MAX_CAM_NUM];
int PrevGain[MAX_CAM_NUM];

HAND ImageMutext;

Mat CamImage[MAX_CAM_NUM];

static HAND hScamif;


static void  CallbackButtonTest(int state, void *userdata)
{
	// do your test
	LOCAL_DEBUG(("%s: btn state=%d\n", __func__,  state));
	TestState = state;
	
}

static void  CallbackButtonRunState(int state, void *userdata)
{

	int i;
	String *btn = (String *)userdata;

	for(i=0; i < MAX_STATE; i++)
	{
		if(*btn == StateBtnName[i]) {
			LOCAL_DEBUG(("%s: run state=%d btn state=%d\n", __func__, i, state));
			break;
		}
	}

	if(i>=MAX_STATE) {
		LOCAL_TRACE_ERR(("%s: not  found valid state for this button\n", __func__));
		return;
	}

	if(state) {
		RunState = i;
	}
	
}



static void CallbackOnMouse( int event, int x, int y, int flags, void *userdata )
{
	int i;
	String *win = (String *)userdata;

	for(i=0; i < NUM_CAM; i++)
	{
		if(*win == CamWindowName[i]) {
		//	printf("%s mouse event\n", win->c_str());
			break;
		}
	}
	
	if(i>=NUM_CAM) {
		LOCAL_TRACE_ERR(("%s: no CAM found for this mouse callback\n", __func__));
		return;
	}

	if( event == EVENT_LBUTTONUP  )
	{	
		newPt[i] = Point(x,y);
	}
    else if( event == EVENT_LBUTTONDOWN )
    {
        prevPt[i] = newPt[i] = Point(x,y);
		
    }
    else if( event == EVENT_MOUSEMOVE && (flags & EVENT_FLAG_LBUTTON) )
    {	

    }

}


static int scamif_fnCB(
	void *pData, 
	I32 nSize,
	U32 ts_h, U32 ts_l,
	I32 width, I32 height,
	I32 offset_x, I32 offset_y,
	U32 normalbulk, U32 skip, U32 multitude,
	U32 camid,						
	void *pParam)
{
	return 1;
}


static void ChangeCamState(void)
{
	LOCAL_DEBUG(("%s: value=%d\n", __func__, RunState));
		
	if(RunState == STATE_START) {
		scamif_start(hScamif);
	} else if(RunState == STATE_STOP) {
		scamif_stop(hScamif);		
	} else  {
		HAL_SCAMIF_SetRunState(hScamif, RunState-1);	
	}	
}	

static void ChangeGain(int cam_id, int gain)
{
	LOCAL_DEBUG(("%s: cam_id=%d value=%d\n", __func__, cam_id, gain));

	scamif_image_property_gain(hScamif, cam_id, (U32)gain);	
	scamif_image_update_gain(hScamif, cam_id);
}

static void ChangeLedBrightness(int cam_id, int value)
{
	LOCAL_DEBUG(("%s: cam_id=%d value=%d\n", __func__, cam_id, value));

	scamif_image_property_LEDbright(hScamif, cam_id, (U32)value);
	scamif_image_update_LEDbright(hScamif, cam_id);

}

static void ChangeRoi(int cam_id)
{
	int x_start;
	int y_start;
	int x_size;
	int y_size;

	x_start = (newPt[cam_id].x < prevPt[cam_id].x)? newPt[cam_id].x:prevPt[cam_id].x;
	y_start = (newPt[cam_id].y < prevPt[cam_id].y)? newPt[cam_id].y:prevPt[cam_id].y;
	x_size = abs(newPt[cam_id].x - prevPt[cam_id].x);
	y_size = abs(newPt[cam_id].y - prevPt[cam_id].y);

	LOCAL_DEBUG(("%s:cam_id=%d x=%d y=%d w=%d h=%d\n", __func__, cam_id, x_start, y_start, x_size, y_size));

	HAL_SCAMIF_UpdateRoi(hScamif,cam_id, x_start, y_start, x_size, y_size );
}

CR_BOOL NewRoiSelected(Point pre_pt, Point new_pt)
{
#define MIN_W	100
#define MIN_H	100

	CR_BOOL new_pos = CR_FALSE;

	if(pre_pt != new_pt) {
		if(abs(new_pt.x - pre_pt.x) > MIN_W) {
			if(abs(new_pt.y - pre_pt.y) > MIN_W) {
				new_pos = CR_TRUE;
			}
		}
	}
	return new_pos;
}

/* CAM Viewer INIT section */
#define HEAD_GLOBAL_CONF		"global"

#define KEY_SYNC_PERIOD_SEARCH	"sync_search"
#define KEY_SYNC_PERIOD_READY	"sync_ready"
#define KEY_SYNC_PERIOD_LAUNCH	"sync_launch"
#define ZROT	"zrot"
#define NROT	"nrot"


#define HEAD_LED_0_TIMING		"led_0_timing"
#define HEAD_LED_1_TIMING		"led_1_timing"
#define HEAD_LED_2_TIMING		"led_2_timing"
#define HEAD_LED_3_TIMING		"led_3_timing"

#define RISING_TIME_0_SEARCH	"rising_search_0"
#define RISING_TIME_1_SEARCH	"rising_search_1"
#define RISING_TIME_2_SEARCH	"rising_search_2"
#define RISING_TIME_3_SEARCH	"rising_search_3"

#define FALLING_TIME_0_SEARCH	"falling_search_0"
#define FALLING_TIME_1_SEARCH	"falling_search_1"
#define FALLING_TIME_2_SEARCH	"falling_search_2"
#define FALLING_TIME_3_SEARCH	"falling_search_3"

// TODO: rest of all.....

#define HEAD_SENSOR_0_CONF		"sensor_0"
#define HEAD_SENSOR_1_CONF		"sensor_1"
#define HEAD_SENSOR_2_CONF		"sensor_2"
#define HEAD_SENSOR_3_CONF		"sensor_3"

#define RESIZE	"resize"

static void ParseIniAndSetParams(char *filename)
{
	// TODO: read ini file and set proper init values

	int zrot = OSAL_FILE_GetIniInteger(HEAD_GLOBAL_CONF, ZROT,  0, filename);

}


void TESTAPP_CAM_Ctrl(char *filename)
{
	int i;
    int led_brightness[MAX_CAM_NUM];
    int gain[MAX_CAM_NUM];

	U08 *ip[NUM_CAM];
	U32 port[NUM_CAM];
	U32 masterslave[NUM_CAM];
	U32 streaming_channel_mode = 0;
	camif_buffer_info_t *pb;
	camimageinfo_t	*pcii;
	U08 *buf;
	I32 res;
	U32 rindex;

	LOCAL_DEBUG(("TESTAPP_CAM_Ctrl: ini_file=%s\n", filename));

	ParseIniAndSetParams(filename);

	/* CAM Window */
	for(i=0; i< NUM_CAM; i++)
	{
		namedWindow(CamWindowName[i], WINDOW_NORMAL|WINDOW_GUI_EXPANDED|WINDOW_KEEPRATIO);
		resizeWindow(CamWindowName[i], WIN_WIDTH, WIN_HEIGHT 	);
		if(i < 2) {
			moveWindow(CamWindowName[i], SCREEN_X_START+(i*(WIN_WIDTH+50)), SCREEN_Y_START 	);
		} else {
			moveWindow(CamWindowName[i], SCREEN_X_START+((i-2)*(WIN_WIDTH+50)), SCREEN_Y_START + WIN_HEIGHT +50	);		
		}
		led_brightness[i] = 50;
		gain[i] = 0;
		createTrackbar( "LED", CamWindowName[i], &led_brightness[i], 100,  NULL);
		PrevLedBrightness[i] = led_brightness[i];
		createTrackbar( "Gain", CamWindowName[i], &gain[i], 100,  NULL);	
		PrevGain[i] = gain[i];		
		setMouseCallback( CamWindowName[i], CallbackOnMouse, &CamWindowName[i] );


		ip[i] = (U08*)cr_malloc(64);

		ImageMutext = cr_mutex_create();
	}
	
	/* Control panel */ 
	createButton(TestBtnName,CallbackButtonTest,&TestBtnName, QT_CHECKBOX,0);		
	createButton(StateBtnName[0],CallbackButtonRunState,&StateBtnName[0], QT_RADIOBOX,1);		    
	for(i=1; i< 5; i++)
	{		
		createButton(StateBtnName[i],CallbackButtonRunState,&StateBtnName[i], QT_RADIOBOX,0);		    	
	}

	hScamif	= scamif_create2((HAND)scamif_fnCB, CAMWORKMODE_SENSING, 0, 0);

	masterslave[0]	= 0;	// Master: 0, Slave: 1
	masterslave[1] 	= 1;	// Master: 0, Slave: 1
	
	scamif_cam_start2(hScamif,
			NUM_CAM,				// camera count
			ip,				// camera IP
			port,			// Port for Receiving Image
			masterslave,		// Master: 0, Slave: 1
			streaming_channel_mode
			);	


    while( waitKey(100) != 27 )
    {
		for (i = 0; i < NUM_CAM; i++) {
			U32 ts_h; U32 ts_l;
			I32 width; I32 height;
			I32 offset_x; I32 offset_y;
			U32 normalbulk;
			U32 multitude;
		
			normalbulk = 1;

#ifndef IMAGE_TEST
			res = scamif_imagebuf_read_latest(hScamif, i, normalbulk, &pb, &buf, &rindex);
			if (res == 0) {
				cr_mutex_release(ImageMutext);
				continue;
			}

			ts_h = pb->ts_h;
			ts_l = pb->ts_l;
			pcii = &pb->cii;
		
			width = pcii->width;
			height = pcii->height;
			offset_x = pcii->offset_x;
			offset_y = pcii->offset_y;
			multitude = pcii->multitude;

			CamImage[i] =  Mat(height,width, CV_8UC1 , buf);	

#else
			CamImage[i] = imread("test.jpg");
#endif

			
			if(led_brightness[i] != PrevLedBrightness[i]) {
				ChangeLedBrightness(i, led_brightness[i]);
				PrevLedBrightness[i] = led_brightness[i];
			}
			if(gain[i] != PrevGain[i]) {
				ChangeGain(i, gain[i]);
				PrevGain[i] = gain[i];
			}

			cr_mutex_wait(ImageMutext, -1);

			imshow(CamWindowName[i], CamImage[i]);	

			if(NewRoiSelected(prevPt[i],newPt[i]) == CR_TRUE) {
				ChangeRoi(i);
				rectangle(CamImage[i], prevPt[i], newPt[i], Scalar::all(255), 1, 8, 0); 	
				newPt[i] = prevPt[i];
				imshow(CamWindowName[i], CamImage[i]);	
				waitKey(500);	// delay for image rect display
			}			
			
			cr_mutex_release(ImageMutext);			
		}	

		if(RunState != PrevRunState) {
			ChangeCamState();
			PrevRunState = RunState;
		}
		
		if(TestState != PrevTestState) {
			// Do your test 
			PrevTestState = TestState;
		}		

    }
	
    destroyAllWindows();

	scamif_stop(hScamif);
	scamif_delete(hScamif);

	for (i = 0; i < NUM_CAM; i++) {
		cr_free(ip[i]);		
	}
	cr_mutex_delete(ImageMutext);

}



#if defined (__cplusplus)
}
#endif




