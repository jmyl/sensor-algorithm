/***************************************************************************
                          ini.cpp  -  command line tool for reading/
                                      modifying ini files.
                             -------------------
    begin                : Fri Apr 21 2000
    copyright            : (C) 2000 by Simon White
    email                : s_a_white@email.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libini.h"
#include "cr_common.h"
#include "cr_osapi.h"

#if defined (__cplusplus)
extern "C" {
#endif


static void parameters (char *name)
{
    printf ("Syntax: %s [-<options>...] <file> [section] [key] [data]\n", name);
}



int TESTAPP_Libini (int argc, char *argv[])
{
    int  inifile = 0, heading = 0, key = 0, data = 0;
    char buffer[300], *p = buffer;
    int  length = 0;
	int i;
    ini_fd_t fd;

    // Parse command line arguments.
    for (i = 1; i < argc; i++)
    {
        if (inifile == 0)
            inifile = i;
        else if (heading == 0)
            heading = i;
        else if (key == 0)
            key = i;
        else if (data == 0)
            data = i;
        else
        {
            parameters (argv[0]);
            return 0;
        }
    }

    if (!inifile)
    {
        fprintf (stderr, "ERROR: INI filename not supplied.\n");
        return -1;
    }

    // NULL strings can be entered using "".  A NULL string for key
    // is not always allowed
    if (key)
    {
        if (*argv[key] == '\0')
            key = 0;
    }
    
    // Check for Microsoft things we don't support    
     if (!heading)
      {
          fprintf (stderr, "ERROR: Listing of INI file sections not supported.\n");
          return -1;
      }
      else if (!key)
      {
          fprintf (stderr, "ERROR: Listing of INI file keys in section \"%s\" not supported.\n", argv[heading]);
          return -1;
      }
    

    do
    {  
        // Read/Write the data
        if (data)
        {   // Write
            OSAL_FILE_SetIniString (argv[heading], argv[key],argv[data], argv[inifile]);            
        }
        else
        {   // Read
//#define GET_INT_TEST        
#ifdef GET_INT_TEST
            int ret = OSAL_FILE_GetIniInteger(argv[heading], argv[key],  1, argv[inifile]);
			printf("%d\n", ret);
#else
			unsigned long ret = OSAL_FILE_GetIniString(argv[heading], argv[key], "def", buffer, 16, argv[inifile]);
			if (ret == 0)
			{
				fprintf (stderr, "ERROR: Unable to read key \"%s\".", argv[key]);
				goto main_error;
			}
			printf("%s\n", buffer);
#endif

        }
    } while (0);

return 0;

main_error:
    return -1;
}



#if defined (__cplusplus)
}
#endif


