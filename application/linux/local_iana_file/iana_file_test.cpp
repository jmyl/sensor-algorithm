/*
 * iana_file_test.c
 *
 * Copyright 2021 Creatz
 *
 * IANA computation test tool 
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "cr_common.h"

#include "cr_thread.h"
#include "cr_osapi.h"


#include "iana.h"
#include "iana_adapt.h"
#include "iana_tool.h"
#include "iana_implement.h"

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

using namespace cv;


#define LOCAL_DEBUG_ON 1
#define LOCAL_ERROR_TRACE_ON 1
#include "cr_dbg_on.h"


/*----------------------------------------------------------------------------
 	Description	: MACROs and definitions
-----------------------------------------------------------------------------*/
#define TESTAPP_NAME			"IANA_FILE_TESTAPP"
#define RESULT_FILE_PATH		"result.xml"

#define TEST_STATUS_IDLE			0
#define TEST_STATUS_START			1
#define TEST_STATUS_RUN				2
#define TEST_STATUS_END				3
#define TEST_STATUS_ERROR			4



#if defined (__cplusplus)
extern "C" {
#endif

static void * hIana = NULL;
static BOOL TestStatus = TEST_STATUS_IDLE;
static int PicureCount= 0;


static int InitTest(void)
{
	LOCAL_DEBUG(("[%s]%s: BEGIN\n", TESTAPP_NAME, __func__));	
	
	hIana =  IANA_create(IANA_OPMODE_FILE, 0, 0, 0, 0);
	TestStatus = TEST_STATUS_START;
 
	LOCAL_DEBUG(("[%s]%s: END\n",  TESTAPP_NAME,__func__));	
	return 0;
}

static int CloseTest(void)
{
	LOCAL_DEBUG(("[%s]%s: BEGIN\n", TESTAPP_NAME,  __func__));	
	
	if (hIana != NULL) {
		IANA_cmd(hIana, IANA_CMD_OPERATION_STOP, 0, 0, 0, 0);
		IANA_delete(hIana);
		hIana = NULL;
	}
	TestStatus = TEST_STATUS_IDLE;
	LOCAL_DEBUG(("[%s]%s: END\n",  TESTAPP_NAME, __func__));	
	return 0;
}

// Display callback example - openCV
static int DisplayImg(unsigned char* img, int width, int height, int sx, int sy, int colorspace)
{
	char window_name[64];
	Mat image(height,width, CV_8UC1 , img);
	
    if(image.empty())
    {
        std::cout << "Could not read the image: "  << std::endl;
        return 1;
    }

	memset(window_name, 0 , 64);
#if 0		// TODO: if you want, implement multiple display/window, but you should consider system load and memory
	sprintf(window_name, "disp window %d", PicureCount++);
#else
	sprintf(window_name, "disp window");
#endif
	
    imshow(window_name, image);

	// TODO: if you want, implement display position control
	// TODO: if you want, implement window position control
	namedWindow(window_name, WINDOW_NORMAL);
	resizeWindow(window_name, 512, 512);


	// TODO: if you want, implement display period control
	waitKey(1);	// TODO: by default for initial test

	PicureCount++;
	
	return 0;
}

static int DisplayCallback(char *img, int width, int height, int p0, int p1, int p2, int p3, PARAM_T userparam) 		// p1, 1: GRAY, 2: YUV422, 3: YUV420, 4: RGB
{
	int sx, sy;
	int colorspace;

	//LOCAL_DEBUG(("[%s]%s\n",TESTAPP_NAME,  __func__));	

	sx = 0; sy = 0;
	if (p0 == 0) {
		sx= 0;
		sy= 0;
	} else if (p0 == 1) {
		sx = 512;
		sy = 0;
	} else if (p0 == 2) {
		sx = 0;
		sy = 256;
	} else if (p0 == 3) {
		sx = 512;
		sy = 256;
	} else if (p0 == 4) {
		sx = 0;
		sy = 512;
	} else if (p0 == 5) {
		sx = 512;
		sy = 512;
	}

	if (p1 >= 1 && p1 <= 4) { 	// p1, 1: GRAY, 2: YUV422, 3: YUV420, 4: RGB
		colorspace = p1;
	} else {
		colorspace = 1;
	}

	DisplayImg((unsigned char*) img,
			width, height,
			sx, sy,
			colorspace				// 1: GRAY, 2: YUV422, 3: YUV420, 4: RGB
	);


	return 1;
}

static int ResultCallback (
	HAND h, 
	U32 status, 
	IANA_shotdata_t *psd, 
	I32 userparam)

{

	printf("[Shot Reslt] \n");
	printf("\tstatus:\t\t0x%0x\n", (U32) status);
	printf("\tuserparam:\t0x%0x\n", (U32) userparam);

	if (status == IANA_SENSOR_STATE_GOODSHOT) {
		printf("Good shot..\n");

		printf(" vmag %lf, inc %lf, azimuth %lf, %lf, %lf, %lf, %lf, backspin %d, sidespin %d, %lf, %lf, %lf, %lf\n",
					psd->ballspeedx1000 / 1000.0,
					psd->inclinex1000 / 1000.0,
					psd->azimuthx1000 / 1000.0,

					psd->clubpathx1000 / 1000.0,
					psd->clubfaceanglex1000 / 1000.0,
					psd->clubspeed_Bx1000 / 1000.0,
					psd->clubspeed_Ax1000 / 1000.0,
					psd->backspin,
					psd->sidespin,

					psd->spinmag,

					psd->axisx,
					psd->axisy,
					psd->axisz);

	} else {
		printf("Bad shot..\n");
	}

	TestStatus = TEST_STATUS_END;

	PicureCount= 0;
	destroyAllWindows();

	return 0;
}



// OK: return 0
static int RunTest(char *info_file, const char* result_file, int image_display, int club)
{
	LOCAL_DEBUG(("[%s]%s: BEGIN\n",  TESTAPP_NAME,  __func__));	

	if (hIana != NULL) {

#if 0
		if (club == 0) {
			IANA_cmd(hIana, IANA_CMD_OPERATION_SETAREA, IANA_AREA_TEE, 0, 0, 0);
			IANA_cmd(hIana, IANA_CMD_OPERATION_AREAALLOW, 	1, 1, 0, 		0);
		} else if (club == 1) {
			IANA_cmd(hIana, IANA_CMD_OPERATION_SETAREA, IANA_AREA_IRON, 0, 0, 0);
			IANA_cmd(hIana, IANA_CMD_OPERATION_AREAALLOW, 	1, 1, 0, 		0);
		} else if (club == 2) {
			IANA_cmd(hIana, IANA_CMD_OPERATION_SETAREA, IANA_AREA_PUTTER, 0, 0, 0);
			IANA_cmd(hIana, IANA_CMD_OPERATION_AREAALLOW, 	0, 0, 1, 		0);
		}	
#endif /* 0 */

		iana_readinfo_filename(hIana, info_file);			// input  (ex: info.xml)
		iana_simresult_filename(hIana, result_file);	// result (ex: result.xml)
		
		IANA_cmd(hIana, IANA_CMD_OPERATION_START, (I64)ResultCallback, 0, 0, 0);
		if (image_display) {
			IANA_cmd(hIana, IANA_CMD_EXC_IMAGE_CALLBACK, (I64) DisplayCallback, 0,  0, 0);
		} else {
			IANA_cmd(hIana, IANA_CMD_EXC_IMAGE_CALLBACK, 0, 0, 0, 0); 		// NO DISPLAY
		}
	}
	TestStatus = TEST_STATUS_RUN;
	LOCAL_DEBUG(("[%s]%s: END\n",  TESTAPP_NAME, __func__));	
	return 0;
}

static int VerifyResult(int result)
{
	if(result == 0) 	{
		LOCAL_DEBUG(("TESTAPP_IANA_File: PASSED\n"));	
	}
	else {
		LOCAL_TRACE_ERR(("TESTAPP_IANA_File: FAILED\n"));			
	}

	return 0;
}


void TESTAPP_IANA_File_Usage(void)
{
	printf("Usage: ./vulkan input_info_file image_display [club (0:Tee, 1: Iron, 2: Putter)] \n");
}

void TESTAPP_IANA_File(char *filename, int image_display, int club)
{
	int result;
	int test_timeout = 100000; // msec
	
	LOCAL_DEBUG(("TESTAPP_IANA_File: %s\n", filename));	

	InitTest();

	result = RunTest(filename, RESULT_FILE_PATH, image_display, club);

	LOCAL_DEBUG(("Second run! @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ TESTAPP_IANA_File: %s\n", filename));	

  result = RunTest(filename, RESULT_FILE_PATH, image_display, club);

	while(test_timeout) {
		if(TestStatus == TEST_STATUS_END || TestStatus == TEST_STATUS_IDLE) {
			LOCAL_DEBUG(("TESTAPP_IANA_File: Test completed\n"));	
			break;
		}
		cr_sleep(100);
		test_timeout -= 100;
	}

	VerifyResult(result);			

	CloseTest();
}


#if defined (__cplusplus)
}
#endif



