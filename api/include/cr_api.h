/*!
 *******************************************************************************
                                                                                
                    Creatz API header
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2021  by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_api.h
	 @brief  
	 @author 
	 @date   2021/01/08 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/
#if !defined(_CR_API_H_)
#define		 _CR_API_H_

#include "cr_common.h"

I32 CR_API_SendInternalCommand(HAND h, U32 cmd, PARAM_T p0, PARAM_T p1, PARAM_T p2, PARAM_T p3);
I32 CR_API_StartHostAPI(void);
I32 CR_API_StopHostAPI(void);


#endif

