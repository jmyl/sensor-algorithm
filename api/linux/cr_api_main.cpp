/*!
 *******************************************************************************
                                                                                
                   Creatz API main code
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2021  by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_api_main.cpp
	 @brief  Wrapper of OS File System services
	 @author 
	 @date   2021/01/08 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>

 
#include "cr_common.h"
#include "cr_osapi.h"
#include "cr_thread.h"


/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
 #define MAIN_API_TASK_PRIORITY 				MAX_USER_PRIORITY


/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/ 
 static cr_thread_t ApiThreadContext;

/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*!
 ********************************************************************************
 *	@brief      Send each command to IANA from Host API
 *
 *	@param[in]  
 *				
 *	@param[in]  
 *				
 *  @return		
 *
 *	@author	   
 *  @date       
 *******************************************************************************/
I32 CR_API_SendInternalCommand(HAND h, U32 cmd, PARAM_T p0, PARAM_T p1, PARAM_T p2, PARAM_T p3)
{
	return 0;
}


/*!
 ********************************************************************************
 *	@brief      Main Host API thread - tcp/ip socket API --> switch(command) --> IANA_Cmd() 
 *				
 *	@param[in]  startcontext
 *				
 *  @return		
 *
 *	@author	   
 *  @date       
 *******************************************************************************/
void *MainApiThread(void *startcontext)
{
	cr_thread_t *pt;
	
	volatile U32	loop;
	U32				u0;
	U32 camid;


	//--------------------------------------- 
	// Change state..
	pt = &ApiThreadContext;
	pt->ustate = CR_THREAD_STATE_INITED;


	//---------------------------------------
	pt->ustate = CR_THREAD_STATE_RUN;
	loop = 1;


	while(loop) {


		// TODO:

		
#define CHECK_PERIOD	100				// 100 Hz
		u0 = cr_event_wait (pt->hevent, CHECK_PERIOD);
		if (pt->ustate == CR_THREAD_STATE_NEEDSTOP) {
			cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%s: I would be killed...\n", __func__);
			break;
		}
	}

	pt->ustate = CR_THREAD_STATE_STOPPING;
	//--------------------------------------- 

	return 0;
}

/*!
 ********************************************************************************
 *	@brief      Start Host API process
 *
 *	@param			None
 *				 
 *  @return			SUCCESS: 0
 *
 *	@author	   
 *  @date       
 *******************************************************************************/
I32 CR_API_StartHostAPI(void)
{
	ApiThreadContext.ustate = CR_THREAD_STATE_NULL;
	ApiThreadContext.hevent = cr_event_create();
	ApiThreadContext.hthread = (cr_thread_t *) cr_thread_create(MainApiThread, NULL);
	cr_thread_setpriority(ApiThreadContext.hthread, MAIN_API_TASK_PRIORITY);	

	return 0;
}

/*!
 ********************************************************************************
 *	@brief      Stop Host API process
 *
 *	@param			None
 *				 
 *  @return			SUCCESS: 0
 *
 *	@author	   
 *  @date       
 *******************************************************************************/
I32 CR_API_StopHostAPI(void)
{	
	ApiThreadContext.ustate = CR_THREAD_STATE_NEEDSTOP;	
	cr_event_set(ApiThreadContext.hevent);
	
	cr_thread_join(ApiThreadContext.hthread); 
	cr_event_delete(ApiThreadContext.hevent);

	return 0;
}

#if defined (__cplusplus)
 }
#endif


