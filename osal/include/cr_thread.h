/*!
 *******************************************************************************
                                                                                
                    Creatz Thread model
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 ~ 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_thread.h
	 @brief  
	 @author YongHo Suk
	 @date   2011/03/22 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_CR_THREAD_H_)
#define		 _CR_THREAD_H_

#include "cr_common.h"
/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#if defined(__linux__)
#define MAX_USER_PRIORITY    99
#define NORMAL_USER_PRIORITY    50
#define MIN_USER_PRIORITY    1
#elif defined(_WIN32)
#define MAX_USER_PRIORITY    THREAD_PRIORITY_TIME_CRITICAL
#define NORMAL_USER_PRIORITY    THREAD_PRIORITY_ABOVE_NORMAL
#define MIN_USER_PRIORITY    THREAD_PRIORITY_LOWEST
#endif

/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/
typedef enum _cr_thread_state {
	CR_THREAD_STATE_NULL = 0,
	CR_THREAD_STATE_INITED,
	CR_THREAD_STATE_RUN,
	CR_THREAD_STATE_NEEDSTOP,
	CR_THREAD_STATE_STOPPING
} cr_tread_state_t;


typedef struct _cr_thread {
	HAND		hthread;			//!< Handle of thread
	U32			ustate;				
	HAND		hevent;				//!< Handle of Event of thread	
} cr_thread_t;

/*----------------------------------------------------------------------------
 	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 	Description	: external and internal global variable
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the dll function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif


#if defined (__cplusplus)
}
#endif


#endif		// _CR_THREAD_H_

