/*!
 *******************************************************************************
                                                                                
                    OS API 
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 ~ 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_osapi.h
	 @brief  
	 @author YongHo Suk
	 @date   2011/03/14 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

#if !defined(_CR_OSAPI_H_)
#define		 _CR_OSAPI_H_

#include "cr_common.h"
/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
-----------------------------------------------------------------------------*/
#if defined(__linux__)
#define INFINITE		(-1)
#endif

#define MAX_PATH_LEN		2048


/*----------------------------------------------------------------------------
 	Description	: Type definition of structures and data type
 -----------------------------------------------------------------------------*/
typedef struct _CR_Time_t {
	I32 wYear;
	I32 wMonth;
	I32 wDayOfWeek;
	I32 wDay;
	I32 wHour;
	I32 wMinute;
	I32 wSecond;
	I32 wMilliseconds;
} CR_Time_t;

enum {
	FILE_OP_COPY=0,
	FILE_OP_DELETE,		
	FILE_OP_MOVE,	
};

typedef struct _CR_FileOperation_t {
	U32 command;
	CHR *dst;
	CHR *src;
	CR_BOOL recursive;
	CR_BOOL file_only;
} CR_FileOperation_t;



/*----------------------------------------------------------------------------
 *	Description	: function prototypes 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif
typedef void *(*pfuncThreadMain_t)(void *pParam);

/*----------------------------------------------------------------------------
 *	MUTEX / SEMAPHORE
 -----------------------------------------------------------------------------*/
HAND cr_mutex_create (void);
U32 cr_mutex_wait(HAND pmutex, I32 timeout);
U32 cr_mutex_release (HAND pmutex);
U32 cr_mutex_delete (HAND pmutex);
HAND cr_sem_create (I32 initcnt, I32 maxcnt);
U32 cr_sem_get(HAND psema, I32 timeout);
I32 cr_sem_release (HAND psema);
I32 cr_sem_count (HAND psema);
U32 cr_sem_delete (HAND psema);


/*----------------------------------------------------------------------------
 *	EVENT
 -----------------------------------------------------------------------------*/
HAND cr_event_create (void);
HAND cr_event_create_with_name(char *eventname);
HAND cr_event_open(char *eventname);
U32 cr_event_set(HAND pevent);
U32 cr_event_wait (HAND pevent, I32 timeout);
U32 cr_event_delete (HAND pevent);


/*----------------------------------------------------------------------------
 *	SYSTEM
 -----------------------------------------------------------------------------*/
UL32 cr_gettickcount (void);
void cr_sleep (U32 ticks);
void OSAL_SYS_MicroSleep(U32 usec);
void OSAL_SYS_GetLocalTime(CR_Time_t *localTime);
void OSAL_SYS_GenerateGUID(char *guid);
I32 OSAL_SYS_GetCpuUsage(void);



/*----------------------------------------------------------------------------
 *	THREAD / SCHEDULING
 -----------------------------------------------------------------------------*/
HAND cr_thread_create (void * (*pfunc)(void *pParam), HAND param);
HAND cr_thread_get_id (void);
void cr_thread_join (HAND pthread);
void cr_thread_setpriority (HAND pthread, I32 priority);

void OSAL_THREAD_Delete(HAND hthread);

/*----------------------------------------------------------------------------
 *	MEMORY
 -----------------------------------------------------------------------------*/
void * cr_malloc( unsigned int size );
void * cr_calloc( unsigned int numberOfElement, unsigned int sizePerElement );
void cr_free( void *ptr );
void cr_memmove(void *dst, void *src, unsigned int size);
void *cr_memcpy(void *dest, void *src, U32  len);
void *cr_memset(void *ptr, I32 value, U32 num);

/*----------------------------------------------------------------------------
 *	STRING / Character Encoding
 -----------------------------------------------------------------------------*/
I08 *cr_strcpy(I08 *dest, I08 *src);
void OSAL_STR_ConvertUnicode2MultiByte(CHR *inputStr, char *outputStr, int len);
void OSAL_STR_ConvertMultiByte2Unicode(char *inputStr, CHR *outputStr, int len);


#if defined(_WIN32)

#define cr_sprintf wsprintf
#define cr_sscanf swscanf

#elif defined(__linux__)

#define cr_sprintf sprintf
#define cr_sscanf sscanf

#else

#error "not supported"

#endif


/*----------------------------------------------------------------------------
 *	FILE SYSTEM
 -----------------------------------------------------------------------------*/
FILE *cr_fopen(const char *pathname, const char *mode); 
int cr_fclose(FILE *fp); 
#if 0
size_t cr_fread( void * ptr, size_t size, size_t count, FILE * stream );
size_t cr_fwrite( const void * ptr, size_t size, size_t count, FILE * stream );
#else
#define cr_fread fread
#define cr_fwrite fwrite
#endif
#define cr_fflush fflush
#define cr_fprintf fprintf
#define cr_fseek fseek
#define cr_fscanf fscanf

int cr_mkdir(const char *dirname);
int cr_wmkdir(const CHR *dirname);
int cr_rmdir( const char *dirname );

void OSAL_FILE_GetCurrentExecDir( U32 size, CHR* buf );
void OSAL_FILE_GetCurrentWorkDir( U32 size, char* buf );
void OSAL_FILE_GetDirPath( char* path, char* dir );


int OSAL_FILE_Copy(CHR *dst, CHR *src);
int OSAL_FILE_DeleteFiles(CHR *dst, CR_BOOL fileOnly);
int OSAL_FILE_Move(CHR *dst, CHR *src);


unsigned long OSAL_FILE_GetIniString(CHR *sectionName, CHR *keyName, CHR *defaultStr, CHR *retBuf, unsigned long size, CHR* iniFileName);
CR_BOOL OSAL_FILE_SetIniString(CHR *sectionName, CHR *keyName, CHR *string, CHR* iniFileName);
int OSAL_FILE_GetIniInteger(CHR *sectionName, CHR *keyName, int defaultVal, CHR* iniFileName);



#if defined (__cplusplus)
}
#endif


#endif		// _CR_OSAPI_H_

