/*!
 *******************************************************************************
                                                                                
                   Creatz OS Wrapper - Thread
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 ~ 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_osal_thread.c
	 @brief  Wrapper of OS Thread/Context services
	 @author YongHo Suk                                 
	 @date   2011/03/14 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#include "cr_common.h"
#include "cr_osapi.h"
#if defined(__linux__)
#include <pthread.h>
#include <sched.h>
#include <errno.h>
#endif
#include "cr_thread.h"


/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/


#if defined (__cplusplus)
extern "C" {
#endif

/*!
 ********************************************************************************
 *	@brief      Create Thread
 *	@param[in]  pfunc
 *					start-up function of thread
 *	@param[in]  param
 *					paramter for start-up function
 *					
 *  @return		handle of thread
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/

HAND cr_thread_create (void *(*pfunc)(void *pParam), HAND param)
{
#if defined(_WIN32)
	
	HAND hthread = NULL;
	UL32 threadid;

	hthread = CreateThread (NULL, 0, (LPTHREAD_START_ROUTINE)pfunc, param, 0, &threadid);
	return hthread;
#elif defined(__linux__)
	pthread_t pthread;
	int err;

	//err = pthread_create(&pthread, NULL, (void* (*)(void*))pfunc, param);
	err = pthread_create(&pthread, NULL, pfunc, param);
	if (err != 0) {
	//	printf("\ncan't create thread :[%s]", strerror(err));
	} else {
	//	printf("\n Thread created successfully\n");
	}

	return (HAND) pthread;
#else
#error what!
#endif

}


/*!
 ********************************************************************************
 *	@brief      Get id of current thread
 *  @return		id of current thread
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/
HAND cr_thread_get_id (void)
{
#if defined(_WIN32)
	
	HAND hthread;
#if defined(_WIN64)
	hthread = (HAND) (U64)GetCurrentThreadId();
#else
	hthread = (HAND) GetCurrentThreadId();
#endif
		// Retrieves the thread identifier of the calling thread.

	return hthread; 

#elif defined(__linux__)
	HAND hthread;
	hthread = (HAND) pthread_self();

	return hthread;
#else
#error what!
#endif
}





/*!
 ********************************************************************************
 *	@brief      Join Thread
 *	@param[in]  pthread
 *					handle of thread
 *  @return		None
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/
void cr_thread_join (HAND hthread)
{
#if defined(_WIN32)
	WaitForSingleObject(hthread, INFINITE);
	return; 


#elif defined(__linux__)
	pthread_t pthread;
	pthread = (pthread_t) hthread;
	pthread_join(pthread, NULL);
	return; 
#else
#error what!
#endif
}

/*!
 ********************************************************************************
 *	@brief      Delete Thread
 *	@param[in]  pthread
 *					handle of thread
 *  @return		None
 *
 *	@author	    
 *  @date       
 *******************************************************************************/

void OSAL_THREAD_Delete(HAND hthread)
{
#if defined(_WIN32)

	TerminateThread(hthread, 0);	
	
#elif defined(__linux__)

	pthread_t pthread;
	pthread = (pthread_t) hthread;

	pthread_join(pthread, NULL);
	pthread_cancel(pthread);

#endif
} 


/*!
 ********************************************************************************
 *	@brief      Set priority of Thread
 *	@param[in]  pthread 
 *					handle of thread
 *	@param[in]  priority 
 *					priority of thread
 *  @return		void
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/

void cr_thread_setpriority (HAND pthread, I32 priority)
{
#if defined(_WIN32)
	
	HAND hthread = pthread;
	SetThreadPriority(hthread, priority); 
	return;

#elif defined(__linux__)
    int policy;
    struct sched_param param;

    pthread_getschedparam(pthread_self(), &policy, &param);
    param.sched_priority = priority;
    pthread_setschedparam(pthread_self(), policy, &param);

    return;

#else
#error what!
#endif

}


#if defined (__cplusplus)
}
#endif
