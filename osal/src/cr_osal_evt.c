/*!
 *******************************************************************************
                                                                                
                   Creatz OS Wrapper - Event
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 ~ 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_osal_evt.c
	 @brief  Wrapper of OS Event services
	 @author YongHo Suk                                 
	 @date   2011/03/14 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#include "cr_common.h"
#include "cr_osapi.h"

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/


#if defined (__cplusplus)
extern "C" {
#endif


/*!
 ********************************************************************************
 *	@brief      Create Event
 *
 *  @return		handle of event
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/
HAND cr_event_create (void)
{
#if defined(_WIN32)
	HAND hevent;

	hevent = CreateEvent(
			NULL,				// the handle cannot be inherited by child processes
			FALSE,				// manulreset is FALSE == Automatic reset.		
			FALSE,				// non-signal'ed.
			NULL
			);

	return hevent;
#elif defined(__linux__)
	HAND hevent;

	hevent = cr_sem_create (0, 1);

	return hevent;
#else
#error what!
#endif
}


/*!
 ********************************************************************************
 *	@brief      Create Event with event name
 *
 *  @return		handle of event
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/
HAND cr_event_create_with_name (char *eventname)
{
#if defined(_WIN32)
	HAND hevent;

	hevent = CreateEventA(
			NULL,				// the handle cannot be inherited by child processes
			FALSE,				// manulreset is FALSE == Automatic reset.		
			FALSE,				// non-signal'ed.
			eventname
			);

	return hevent;
#elif defined(__linux__)
	HAND hevent;

	hevent = cr_sem_create (0, 1);

	return hevent;
#else
#error what!
#endif
}


/*!
 ********************************************************************************
 *	@brief      Open exist event 
 *
 *  @return		handle of event
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/
HAND cr_event_open (char *eventname)
{
#if defined(_WIN32)
	HAND hevent;

	hevent = OpenEventA(
			EVENT_ALL_ACCESS,
			FALSE,				// manulreset is FALSE == Automatic reset.		
			eventname
			);

	return hevent;
#elif defined(__linux__)
	HAND hevent;

	hevent = cr_sem_create (0, 1);

	return hevent;
#else
#error what!
#endif
}





/*!
 ********************************************************************************
 *	@brief      set event
 *	@param[in]  pevent 
 *	                handle of event
 *  @return		1: success of getting mutex 
 *              0: fail
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/
U32 cr_event_set(HAND pevent)
{
#if defined(_WIN32)
	U32 res = 0;			// default is FAIL.
	HAND hevent = pevent;
	if (hevent) {
		SetEvent(hevent);
		res = 1;
		// 1: success, 0: fail
	} 
	return res;
#elif defined(__linux__)
	U32 res;
	res = cr_sem_release (pevent);
	return res;
#else
#error what!
#endif
}

/*!
 ********************************************************************************
 *	@brief      wait for event
 *	@param[in]  pevent 
 *	                handle of event
 *	@param[in]  timeout 
 *	                timeout value (msec). 0xFFFFFFFF == INFINITE (See winbase.h)
 *  @return		1: success of getting event
 *              0: fail (timeout?)
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/

U32 cr_event_wait (HAND pevent, I32 timeout)
{
#if defined(_WIN32)
	U32 res = 0;			// default is FAIL.
	HAND hevent = pevent;
	if (hevent) {
		res = ((WaitForSingleObject (hevent,  timeout) == WAIT_OBJECT_0) ? 1 : 0); 
	//	res = WaitForSingleObject (hevent,  timeout);
		// 1: success, 0: fail
	} 
	return res;
#elif defined(__linux__)
	U32 res;

	res = cr_sem_get(pevent, timeout);

	return res;
#else
#error what!
#endif
}

/*!
 ********************************************************************************
 *	@brief      Delete event
 *	@param[in]  pevent 
 *	                handle of event
 *  @return		1: success of deleting event 
 *              0: fail 
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/

U32 cr_event_delete (HAND pevent)
{
#if defined(_WIN32)
	U32 res = 0;			// default is FAIL.

	HAND hevent = pevent;
	if (hevent) {	
		if (CloseHandle (pevent)) {
			res = 1;		// YES!! success!!
		} 
	}
	return res;
#elif defined(__linux__)
	U32 res;

	res = cr_sem_delete (pevent);

	return res;

#else
#error what!
#endif
}


#if defined (__cplusplus)
}
#endif
