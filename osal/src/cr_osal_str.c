/*!
 *******************************************************************************
                                                                                
                   Creatz OS Wrapper - String
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 ~ 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_osal_str.c
	 @brief  Wrapper of OS String services
	 @author YongHo Suk                                 
	 @date   2011/03/14 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#if defined(_WIN32)
#include <tchar.h>
#endif
#include <string.h>

#include "cr_common.h"
#include "cr_osapi.h"


/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#define PATHBUFLEN		2048


/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/


#if defined (__cplusplus)
extern "C" {
#endif



/*!
 ********************************************************************************
 *	@brief      Fill block of memory (memset)
 *	@param[out] dest
 *	              ptr to output string
 *	@param[in] src
 *	              ptr to input string
 *  @return		dest is returned.
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/
I08 *cr_strcpy(		// 
	I08 *dest,
	I08 *src
)
{
   const I08 *p;
   I08 *q; 
#define CR_STRCPY_MAXLENGTH	(1024 * 16)
#if defined(CREATZ_STRCPY_MAXLENGTH)
   U32 maxcnt = 0;
#endif
 
   for(p = src, q = dest; *p != '\0'; p++, q++) {
	   *q = *p;
#if defined(CREATZ_STRCPY_MAXLENGTH)
	   if (++maxcnt >= CREATZ_STRCPY_MAXLENGTH) {
		   break;
	   }
#endif
   }
 
   *q = '\0';
 
   return dest;
}

/*!
 ********************************************************************************
 *	@brief      convert unicode string to multibyte string
 *	@param[in] inputStr
 *	              unicode string pointer
 *	@param[in] len
 *	              string size
 *	@param[out] outputStr
 *	              multibyte string pointer
 *
 *	@author	    kjlee
 *  @date       2020/11/20
 *******************************************************************************/
void OSAL_STR_ConvertUnicode2MultiByte(CHR *inputStr, char *outputStr, int len)
{
#if defined(_WIN32)
	WideCharToMultiByte(CP_ACP, 0, inputStr, len, outputStr, len, NULL, NULL);
#else
	// implement if needed
	strcpy(outputStr, inputStr);
#endif
}

/*!
 ********************************************************************************
 *	@brief      convert multibyte string to unicode string
 *	@param[in] inputStr
 *	              multibyte string pointer
 *	@param[in] len
 *	              string size 
 *	@param[out] outputStr
 *	              unicode string pointer
 *
 *	@author	    kjlee
 *  @date       2020/11/23
 *******************************************************************************/
void OSAL_STR_ConvertMultiByte2Unicode(char *inputStr, CHR *outputStr, int len)
{
#if defined(_WIN32)
	MultiByteToWideChar(CP_ACP, 0, inputStr, len, outputStr, len);
#else
	// implement if needed
	strcpy(outputStr, inputStr);
#endif
}



#if defined (__cplusplus)
}
#endif
