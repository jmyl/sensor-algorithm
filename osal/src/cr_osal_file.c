/*!
 *******************************************************************************
                                                                                
                   Creatz OS Wrapper - File System
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 ~ 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_osal_file.c
	 @brief  Wrapper of OS File System services
	 @author YongHo Suk                                 
	 @date   2011/03/14 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>

#if defined(_WIN32)
#include <tchar.h>
#include <direct.h>
#else
#include <unistd.h>
#include <fcntl.h>
#include <libgen.h>
#include "libini.h"
#endif
#include "cr_common.h"
#include "cr_osapi.h"

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#define RETRY_CNT	10

#define PATHBUFLEN		2048

/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/

#if defined (__cplusplus)
extern "C" {
#endif

/*!
 ********************************************************************************
 *	@brief      Open a file
 *	@param[in]  pathname 
 *					file path 
 *	@param[in]  mode 
 *					file operation mode
 *  @return		If success, return file descriptor. Otherwise return NULL
 *
 *	@author	   
 *  @date       
 *******************************************************************************/
FILE *cr_fopen(const char *pathname, const char *mode)
{
	return fopen(pathname, mode);
}

/*!
 ********************************************************************************
 *	@brief      Close a file
 *	@param[in]  fp 
 *					file descriptor 
 *  @return		If success, return 0. Otherwise return -1
 *
 *	@author	   
 *  @date       
 *******************************************************************************/
int cr_fclose(FILE *fp)
{
	return fclose(fp);
}

#if 0
/*!
 ********************************************************************************
 *	@brief      read data from file
 *	@param[in]  ptr 
 *					buffer pointer
 *	@param[in]  size 
 *					byte count for one data
 *	@param[in]  count 
 *					data size to be read 
 *	@param[in]  stream 
 *					file descriptor  
 *  @return		data count read
 *       
 *******************************************************************************/
size_t cr_fread( void * ptr, size_t size, size_t count, FILE * stream )
{
	return fread(ptr, size, count, stream);
}

/*!
 ********************************************************************************
 *	@brief      write data to file
 *	@param[in]  ptr 
 *					buffer pointer
 *	@param[in]  size 
 *					byte count for one data
 *	@param[in]  count 
 *					data size to be written 
 *	@param[in]  stream 
 *					file descriptor  
 *  @return		data count written
 *     
 *******************************************************************************/
size_t cr_fwrite( const void * ptr, size_t size, size_t count, FILE * stream )
{
	return fwrite(ptr,  size,  count,  stream );
}
#endif

/*!
 ********************************************************************************
 *	@brief      get current execution directory path
 *	@param[in]  size
 *					path length
 *	@param[out]  buf
 *					absolute path
 *  @return	
 *  
 *******************************************************************************/
void OSAL_FILE_GetCurrentExecDir( U32 size, CHR* buf )
{
#if defined(_WIN32)

#pragma warning(disable:4996)
	TCHAR exe_path[MAX_PATH_LEN];
	TCHAR drive[_MAX_DRIVE];
	TCHAR dir[_MAX_DIR];
	TCHAR fname[_MAX_FNAME];
	TCHAR ext[_MAX_EXT];

	size = size;

	GetModuleFileName( NULL, exe_path, MAX_PATH_LEN );
	_tsplitpath(exe_path, drive, dir, fname, ext);
	wsprintf(buf, _T("%s%s"), drive, dir);
#pragma warning(default:4996)	

#elif defined(__linux__)	

	getcwd(buf, size);	

#endif

}

/*!
 ********************************************************************************
 *	@brief      get current work directory path
 *	@param[in]  size
 *					path length
 *	@param[out]  buf
 *					absolute path
 *  @return	
 * 
 *******************************************************************************/
void OSAL_FILE_GetCurrentWorkDir( U32 size, char* buf )
{
#if defined(_WIN32)
	
	GetCurrentDirectoryA(size, buf);

#elif defined(__linux__)	

	getcwd(buf, size);	

#endif

}

/*!
 ********************************************************************************
 *	@brief      get directory path from full path 
 *	@param[in]  size
 *					path length
 *	@param[out]  buf
 *					directory path
 *  @return	
 *
 *******************************************************************************/
void OSAL_FILE_GetDirPath( char* path, char* dir )
{
#define BUFLEN		2048

#if defined(_WIN32)
#pragma warning(disable:4996)
	char file[BUFLEN];
	char ext[BUFLEN];
	char drive[BUFLEN];
	char dirTmp[BUFLEN];	

	_splitpath(path, drive, dirTmp, file, ext);
	sprintf(dir, "%s%s", drive, dirTmp);
#pragma warning(default:4996)

#elif defined(__linux__)	

	char pathTmp[BUFLEN];	
	strcpy(pathTmp , path);
	strcpy(dir, dirname(pathTmp));

#endif

}



/*!
 ********************************************************************************
 *	@brief      make a directory
 *	@param[in]  dirname
 *					directory name
 *  @return		OK:0 / FAIL:-1
 *  
 *******************************************************************************/
int cr_mkdir(const char *dirname)
{
#if defined(_WIN32)
	return _mkdir(dirname);	
#elif defined(__linux__)
	return( mkdir( dirname, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP| S_IXOTH ) );
#endif
}

/*!
 ********************************************************************************
 *	@brief      make a directory
 *	@param[in]  dirname(unicode string)
 *					directory name
 *  @return		OK:0 / FAIL:-1
 *     
 *******************************************************************************/
 // TODO: change to use unified mkdir 
int cr_wmkdir(const CHR *dirname)
{
#if defined(_WIN32)
	return _wmkdir(dirname);	
#elif defined(__linux__)
	return( mkdir( dirname, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP| S_IXOTH ) );
#endif
}



/*!
 ********************************************************************************
 *	@brief      remove a directory
 *	@param[in]  dirname
 *					directory name
 *  @return		OK:0 / FAIL:-1
 *     
 *******************************************************************************/
int cr_rmdir( const char *dirname )
{
#if defined(_WIN32)
	return( _rmdir( dirname ) );
#elif defined(__linux__)
	return( rmdir( dirname ) );
#endif
	
}

#if defined(_WIN32)
static int FileOperation(CR_FileOperation_t *fileOp)
{
	SHFILEOPSTRUCT s = { 0 };
	I32 tryiter;
	I32 dogood = 0;	

	s.hwnd = NULL;
	s.pTo =  fileOp->dst;		// TODO: must have two NULL characters at the end
	s.pFrom = fileOp->src;		// TODO: must have two NULL characters at the end	
	if(fileOp->command == FILE_OP_COPY) {
		s.wFunc = FO_COPY;
		s.fFlags = FOF_SILENT;
	} else if(fileOp->command == FILE_OP_DELETE) {
		s.wFunc = FO_DELETE;
		s.fFlags = FOF_SILENT | FOF_NOCONFIRMATION | FOF_NOERRORUI;
	} else if(fileOp->command == FILE_OP_MOVE) {
		s.wFunc = FO_MOVE;
		s.fFlags = FOF_SILENT;		
	}	

	if(fileOp->recursive == CR_FALSE) {
		s.fFlags |= FOF_NORECURSION;	
	}
	
	if(fileOp->file_only == CR_TRUE) {
		s.fFlags |= FOF_FILESONLY;	
	}	
	
	for (tryiter = 0; tryiter < RETRY_CNT; tryiter++) {
		dogood = 0; 

		__try {
				SHFileOperation( &s );

				dogood = 1;
		} __except (GetExceptionCode()==EXCEPTION_ACCESS_VIOLATION
					|| GetExceptionCode()== EXCEPTION_ARRAY_BOUNDS_EXCEEDED
					|| GetExceptionCode()== EXCEPTION_INT_DIVIDE_BY_ZERO
					|| GetExceptionCode()== EXCEPTION_FLT_DIVIDE_BY_ZERO	){
						cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, __FUNCTION__"(%d) : __except !!!\n", __LINE__);
					}


		if (dogood) {
			break;
		}

	}

	if(dogood) {
		return CR_OK;
	}
	return CR_ERR_GENERAL;	
}
#endif

#if defined(__linux__)
/*!
 ********************************************************************************
 *	@brief      Copy file
 *	@param[in]  old_path
 *					source file path
 *	@param[in]  new_path
 *					destination file path
 *  @return		OK:0 / FAIL:-1
 *
 *******************************************************************************/
int OSAL_FILE_CopyFile( char *old_path, char *new_path )
{	
    int src_fd, dst_fd; 
    struct stat srcstat; 
    int n; 
    char buf[1024] = {0x00,}; 
    if( (src_fd = open(old_path, O_RDONLY)) <0 ) 
    { 
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s: (%d) : ERROR: Unable to open %s\n", __func__, __LINE__, old_path);
		return CR_ERROR;
    } 

    fstat(src_fd, &srcstat); 

    if( access( new_path, F_OK ) == 0 )
    { 
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%s: (%d) : WARN: dest file(%s) exist but new creation! \n", __func__, __LINE__, new_path);
    } 
 
    // create new file
    if( (dst_fd = open(new_path, O_WRONLY|O_CREAT|O_TRUNC, srcstat.st_mode)) < 0 ) 
    { 
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%s: (%d) : WARN: Unable to open  %s \n", __func__, __LINE__, new_path);
		close( src_fd );
		return CR_ERROR;
    }	

    while( (n = read(src_fd, buf, 1024)) > 0 ) 
    { 
        write( dst_fd, buf, n ); 
    }  

    fchown( dst_fd, srcstat.st_uid, srcstat.st_gid ); 
	
    close( src_fd ); 
    close( dst_fd ); 
 
    return CR_OK; 
}
#endif

/*!
 ********************************************************************************
 *	@brief      Copy file or directory
 *	@param[in]  dst
 *					data pointer for the destination file or directory
 *	@param[in]  src
 *					data pointer for the source file or directory
 *  @return		OK:0 / FAIL:-1
 *
 *	@author	   
 *  @date       
 *******************************************************************************/
int OSAL_FILE_Copy(CHR *dst, CHR *src)
{
#if defined(_WIN32)
	CR_FileOperation_t fileOp;
	fileOp.command = FILE_OP_COPY;
	fileOp.dst = dst;	
	fileOp.src = src;		
	fileOp.recursive = CR_TRUE;			
	fileOp.file_only = CR_FALSE;		
	return FileOperation(&fileOp);
#elif defined(__linux__)
	// TODO: LINUX_PORT - let's use system command until the precise operation is needed
	char command[1024];

	sprintf(command, "cp -a %s %s", src, dst);

	system(command);
	return 0;
#endif
}


/*!
 ********************************************************************************
 *	@brief      Delete files
 *	@param[in]  src
 *					data pointer for the destination file or directory
 *  @return		OK:0 / FAIL:-1
 *
 *******************************************************************************/
int OSAL_FILE_DeleteFiles(CHR *src, CR_BOOL fileOnly)
{	
#if defined(_WIN32)
	CR_FileOperation_t fileOp;
	fileOp.command = FILE_OP_DELETE;
	fileOp.dst = NULL;	
	fileOp.src = src;		
	fileOp.recursive = CR_FALSE;		
	fileOp.file_only = fileOnly;	
	return FileOperation(&fileOp);
#elif defined(__linux__)
// TODO: LINUX_PORT - let's use system command until the precise operation is needed
	char command[1024];

	sprintf(command, "rm -rf %s", src);

	system(command);

	return 0;
#endif
}

/*!
 ********************************************************************************
 *	@brief      Move file & directory
 *	@param[in]  dst
 *					data pointer for the destination file or directory
 *	@param[in]  src
 *					data pointer for the source file or directory
 *  @return		OK:0 / FAIL:-1
 * 
 *******************************************************************************/
int OSAL_FILE_Move(CHR *dst, CHR *src)
{
#if defined(_WIN32)
#if 0
	CR_FileOperation_t fileOp;
	fileOp.command = FILE_OP_MOVE;
	fileOp.dst = dst;	
	fileOp.src = src;		
	fileOp.recursive = CR_TRUE;			
	fileOp.file_only = CR_FALSE;		
	return FileOperation(&fileOp);
#else
	if(MoveFileEx(src, dst, MOVEFILE_COPY_ALLOWED | MOVEFILE_REPLACE_EXISTING)) {
		return CR_OK; 
	}		
	return CR_ERR_GENERAL;	
#endif
#elif defined(__linux__)
// TODO: LINUX_PORT - let's use system command until the precise operation is needed
	char command[1024];

	sprintf(command, "mv %s %s", src, dst);

	system(command);

	return 0;
#endif

}

#if defined(__linux__)
static CR_BOOL FindKey(ini_fd_t fd, CHR *sectionName, CHR *keyName)
{
	int ret;
	ret = ini_locateHeading(fd, sectionName);
	if(ret < 0) 	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s: (%d) : ERROR: Unable to locate section(%s)\n", __func__, __LINE__, sectionName);
		return CR_FALSE;
	}
	ret = ini_locateKey (fd, keyName);
	if(ret < 0) 	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s: (%d) : ERROR: Unable to locate key(%s)\n", __func__, __LINE__, keyName);
		return CR_FALSE;
	}

	return CR_TRUE;
}
#endif

/*!
 ********************************************************************************
 *	@brief      get data(string) in ini file
 *	@param[in]  sectionName
 *					ini section name
 *	@param[in]  keyName
 *					ini key name 
 *	@param[in]  defaultStr
 *					default string if there is no matched key data
 *	@param[out]  retBuf
 *					output string if there is a matched key
 *	@param[in]  size
 *					string size   
 *	@param[in]  iniFileName
 *					ini file name  
 *  @return		OK:0 / FAIL:-1
 * 
 *******************************************************************************/
unsigned long OSAL_FILE_GetIniString(CHR *sectionName, CHR *keyName, CHR *defaultStr, CHR *retBuf, unsigned long size, CHR* iniFileName)
{
#if defined(_WIN32)
	return GetPrivateProfileString(sectionName, keyName, defaultStr, retBuf, size,  iniFileName);
#else	
	ini_fd_t fd;
	int ret;

	fd = ini_open (iniFileName, "r", ";");
	if (fd == NULL) 	{
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%s: (%d) : ERROR: Unable to open or create an INI file(%s)\n", __func__, __LINE__, iniFileName);
		strncpy(retBuf , defaultStr, size);
		return 0;
	}
	if(FindKey(fd, sectionName, keyName) != CR_TRUE) 	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s: (%d) : ERROR: Unable to find key!\n", __func__, __LINE__);
		goto main_error;
	}

	ret = ini_readString (fd, retBuf, size);
	if(ret < 0) 	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s: (%d) : ERROR: Unable to  read key(%s)\n", __func__, __LINE__, keyName);
		goto main_error;
	}	

	ini_close (fd);
	return ret;

main_error:
    ini_close (fd);
	strncpy(retBuf , defaultStr, size);	
    return 0;

#endif

}

/*!
 ********************************************************************************
 *	@brief      set data(string) in ini file
 *	@param[in]  sectionName
 *					ini section name
 *	@param[in]  keyName
 *					ini key name 
 *	@param[in]  string
 *					string to be set
 *	@param[in]  iniFileName
 *					ini file name  
 *  @return		OK:0 / FAIL:-1
 * 
 *******************************************************************************/
CR_BOOL OSAL_FILE_SetIniString(CHR *sectionName, CHR *keyName, CHR *string, CHR* iniFileName)
{
#if defined(_WIN32)
	return WritePrivateProfileString(sectionName, keyName, string, iniFileName);
#else
	ini_fd_t fd;
	int ret;

	fd = ini_open (iniFileName, "w", ";");
	if (fd == NULL) 	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s: (%d) : ERROR: Unable to open or create an INI file(%s)\n", __func__, __LINE__, iniFileName);
		return 0;
	}

	if(FindKey(fd, sectionName, keyName) != CR_TRUE) 	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s: (%d) : ERROR: Unable to find key!\n", __func__, __LINE__);		
		goto main_error;
	}

	ret = ini_writeString (fd, string);
	if(ret < 0) 	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s: (%d) : ERROR: Unable to  write key(%s)\n", __func__, __LINE__, keyName);
		goto main_error;
	}	

	ini_close (fd);
	return ret;

main_error:
    ini_close (fd);
    return 0;

#endif

}

/*!
 ********************************************************************************
 *	@brief      get data(integer) in ini file
 *	@param[in]  sectionName
 *					ini section name
 *	@param[in]  keyName
 *					ini key name 
 *	@param[in]  defaultVal
 *					default value if there is no matched key data
 *	@param[in]  iniFileName
 *					ini file name  
 *  @return		OK:0 / FAIL:-1
 * 
 *******************************************************************************/
int OSAL_FILE_GetIniInteger(CHR *sectionName, CHR *keyName, int defaultVal, CHR* iniFileName)
{
#if defined(_WIN32)
	return GetPrivateProfileInt(sectionName, keyName, defaultVal, iniFileName);
#else
	ini_fd_t fd;
	int ret;
#define MAX_INT_STR_SIZE	512
	char strBuf[MAX_INT_STR_SIZE];

	fd = ini_open (iniFileName, "r", ";");
	if (fd == NULL) 	{
//		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_WARN, "%s: (%d) : ERROR: Unable to open or create an INI file(%s)\n", __func__, __LINE__, iniFileName);
		return defaultVal;
	}
	
	if(FindKey(fd, sectionName, keyName) != CR_TRUE) 	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s: (%d) : ERROR: Unable to find key!\n", __func__, __LINE__);		
		goto main_error;
	}	

	ret = ini_readString (fd, strBuf, MAX_INT_STR_SIZE);
	if(ret < 0) 	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s: (%d) : ERROR: Unable to  read key(%s)\n", __func__, __LINE__, keyName);
		goto main_error;
	}	

	ini_close (fd);
	return atoi(strBuf);

main_error:
    ini_close (fd);
    return defaultVal;

#endif

}





#if defined (__cplusplus)
}
#endif
