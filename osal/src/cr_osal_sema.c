/*!
 *******************************************************************************
                                                                                
                   Creatz OS Wrapper - Semaphore/Mutex
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 ~ 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_osal_sema.c
	 @brief  Wrapper of OS Semaphore/Mutex services
	 @author YongHo Suk                                 
	 @date   2011/03/14 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#include "cr_common.h"
#include "cr_osapi.h"
#if defined(__linux__)
#include <pthread.h>
#include <sys/time.h>
#include <errno.h>
#endif


/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/
#if defined(__linux__)
	typedef struct _cr_pmutex
	{	pthread_cond_t		m_Cond;
		pthread_mutex_t 	m_Mutex;
		I32 				m_nCount;
	} cr_pmutex_t; 
	
	typedef struct _cr_psemaphore
	{	pthread_cond_t		m_Cond;
		pthread_mutex_t 	m_Mutex;
		I32 				m_nCount;
		I32 				m_nMaxCount;
	} cr_psemaphore_t;	
#endif


/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/


#if defined (__cplusplus)
extern "C" {
#endif


/*!
 ********************************************************************************
 *	@brief      Create semaphore
 *  @return		handle of mutex
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/
HAND cr_mutex_create (void)
{
#if defined(_WIN32)
	HAND hmutex;
	hmutex = CreateMutex (NULL,  FALSE,  NULL);
	return hmutex;
#elif defined(__linux__)
	cr_pmutex_t *pMutex;
	pMutex = (cr_pmutex_t *)malloc(sizeof(cr_pmutex_t));
	if (pMutex) {
		pMutex->m_nCount = 1;
		pthread_cond_init  (&pMutex->m_Cond,  NULL);
		pthread_mutex_init (&pMutex->m_Mutex, NULL);
	}
	return (HAND)pMutex;    
#else
#error whaT!?
#endif
}


/*!
 ********************************************************************************
 *	@brief      Wait mutex
 *	@param[in]  pmutex 
 *	                handle of mutex
 *	@param[in]  timeout 
 *	                timeout value (msec). (-1) == INFINITE (See winbase.h)
 *
 *  @return		1: success of getting mutex 
 *              0: fail (timeout?)
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/
U32 cr_mutex_wait(HAND pmutex, I32 timeout)
{
#if defined(_WIN32)
	U32 res = 0;			// default is FAIL.
	DWORD rvalue;
	HAND hmutex = pmutex;
	if (hmutex) {
		//res = ((WaitForSingleObject (hmutex,  timeout) == WAIT_OBJECT_0) ? 1 : 0); 
		rvalue = WaitForSingleObject (hmutex,  timeout);

		if (rvalue ==  WAIT_OBJECT_0) {
			res = 1;
		} else {
			res = 0;
		}
		// 1: success, 0: fail
	} 

	return res;
#elif defined(__linux__)
	U32 res = 0;			// default is FAIL.
	cr_pmutex_t *pMutex = (cr_pmutex_t*)pmutex;

	if (pMutex) {   
		if (pthread_mutex_lock (&pMutex->m_Mutex)) {
			goto func_exit;			// fail.
		}

		if (timeout == INFINITE) {   
			while (pMutex->m_nCount == 0) {   
				if (pthread_cond_wait (&pMutex->m_Cond, &pMutex->m_Mutex)) {   
					pthread_mutex_unlock (&pMutex->m_Mutex);
					goto func_exit;			// fail.
				}
			}
		} else { 	
			struct timeval  currenttime;
			struct timespec timeoutTS;

			gettimeofday (&currenttime, NULL);
//			if (timeout > APP_TIMER_GRANUALITY)
//				timeout -= APP_TIMER_GRANUALITY;	

			timeoutTS.tv_nsec  = (U32)(currenttime.tv_usec*1000 + timeout*1000000);
			timeoutTS.tv_sec   = currenttime.tv_sec + timeoutTS.tv_nsec / 1000000000;
			timeoutTS.tv_nsec %= 1000000000;

			while (pMutex->m_nCount == 0) {	
				if (pthread_cond_timedwait (&pMutex->m_Cond, 
							&pMutex->m_Mutex, 
							&timeoutTS)) { 	
					pthread_mutex_unlock (&pMutex->m_Mutex);
					goto func_exit;			// fail.
				}
			}
		}
		if (--pMutex->m_nCount < 0) {
			pMutex->m_nCount = 0;
		}
		pthread_mutex_unlock (&pMutex->m_Mutex);
		res = 1;
	}
func_exit:
	return res;
#else
#error whaT!?
#endif
}

/*!
 ********************************************************************************
 *	@brief      Release Mutex
 *	@param[in]  pmutex 
 *	                handle of mutex
 *  @return		1: success of release mutex 
 *              0: fail
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/

U32 cr_mutex_release (HAND pmutex)
{
#if defined(_WIN32)
	U32 res = 0;			// default is FAIL.
	
	HAND hmutex = pmutex;
	if (ReleaseMutex (hmutex)) {
		res = 1;		//success
	}

	return res;
#elif defined(__linux__)
	U32 res = 0;			// default is FAIL.
	cr_pmutex_t *pMutex = (cr_pmutex_t*)pmutex;

	if (pMutex) {	
		if (++pMutex->m_nCount > 1)
			pMutex->m_nCount = 1;
		pthread_cond_signal  (&pMutex->m_Cond);
		pthread_mutex_unlock (&pMutex->m_Mutex);
		res = 1;
	}

	return res;
#else
#error whaT!?
#endif
}

/*!
 ********************************************************************************
 *	@brief      Delete Mutex
 *	@param[in]  pmutex 
 *	                handle of mutex
 *  @return		1: success of deleting mutex 
 *              0: fail 
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/
U32 cr_mutex_delete (HAND pmutex)
{
#if defined(_WIN32)
	U32 res = 0;			// default is FAIL.

	HAND hmutex = pmutex;
	if (hmutex) {	
		if (CloseHandle (hmutex)) {
			res = 1;		// YES!! success!!
		} 
	}
	return res;

#elif defined(__linux__)
	U32 res = 0;			// default is FAIL.
	cr_pmutex_t *pMutex = (cr_pmutex_t*)pmutex;

	if (pMutex) {	
		while (pthread_cond_destroy (&pMutex->m_Cond) == EBUSY) { 
			pthread_cond_signal (&pMutex->m_Cond); 
		}
		pthread_mutex_unlock  (&pMutex->m_Mutex);
		pthread_mutex_destroy (&pMutex->m_Mutex);
		free (pMutex);   
		res = 1;
	}

	return res;
#else
#error whaT!?
#endif
}



/*!
 ********************************************************************************
 *	@brief      Create semaphore
 *
 *	@param[in]  initcnt 
 *	                Initial count of semaphore
 *	@param[in]  maxcnt 
 *	                Maximum count of semaphore
 *
 *  @return		handle of semaphore
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/
HAND cr_sem_create (I32 initcnt, I32 maxcnt)
{
#if defined(_WIN32)
	HAND hsema;

	if (initcnt < 0) {
		initcnt = maxcnt;
	}

	hsema = CreateSemaphore(NULL, initcnt, maxcnt, NULL);
	return hsema;
#elif defined(__linux__)
	cr_psemaphore_t   *pSema;

	if (initcnt < 0) {
		initcnt = maxcnt;
	}
	pSema = (cr_psemaphore_t*)malloc(sizeof(cr_psemaphore_t));

	if (pSema != NULL) {
		pSema->m_nCount = initcnt;
		pSema->m_nMaxCount = maxcnt;
		pthread_cond_init  (&pSema->m_Cond,  NULL);
		pthread_mutex_init (&pSema->m_Mutex, NULL);    
	}

	return (HAND)pSema;       
#else
#error whaT!?
#endif
}


/*!
 ********************************************************************************
 *	@brief      get semaphore
 *	@param[in]  psema 
 *	                handle of semaphore
 *	@param[in]  timeout 
 *	                timeout value (msec). 0xFFFFFFFF == INFINITE (See winbase.h)
 *
 *  @return		1: success of getting mutex 
 *              0: fail (timeout?)
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/
U32 cr_sem_get(HAND hsema, I32 timeout)
{
#if defined(_WIN32)
	U32 res = 0;			// default is FAIL.
	if (hsema) {
		res = ((WaitForSingleObject (hsema,  timeout) == WAIT_OBJECT_0) ? 1 : 0); 
		// 1: success, 0: fail
	} 
	return res;
#elif defined(__linux__)
	U32 res = 0;			// default is FAIL.
	cr_psemaphore_t   *pSema = (cr_psemaphore_t*)hsema;

	if (pSema) {   
		if (pthread_mutex_lock (&pSema->m_Mutex)) {
			goto func_exit;
		}

		if (timeout == INFINITE) {   
			while (pSema->m_nCount == 0) {	
				if (pthread_cond_wait (&pSema->m_Cond, &pSema->m_Mutex)) {   
					pthread_mutex_unlock (&pSema->m_Mutex);
					goto func_exit;
				}
			}            
		} else {	
			struct timeval  currenttime;
			struct timespec timeoutTS;

			gettimeofday (&currenttime, NULL);
//			if (timeout > APP_TIMER_GRANUALITY) {
//				timeout -= APP_TIMER_GRANUALITY;	
//			}

			timeoutTS.tv_nsec  = (U32)(currenttime.tv_usec*1000 + timeout*1000000);
			timeoutTS.tv_sec   = currenttime.tv_sec + timeoutTS.tv_nsec / 1000000000;
			timeoutTS.tv_nsec %= 1000000000;

			while (pSema->m_nCount == 0) {	
				switch (pthread_cond_timedwait (&pSema->m_Cond, &pSema->m_Mutex, &timeoutTS)) {
					default:
					case ETIMEDOUT:
						pthread_mutex_unlock (&pSema->m_Mutex);
						goto func_exit;
						break;
					case 0:
						break;
				}
			}
		}

		if (--pSema->m_nCount < 0)
			pSema->m_nCount = 0;

		pthread_mutex_unlock(&pSema->m_Mutex);
		res = 1;
	}
func_exit:
	return res;
#else

#error what?
#endif
}

/*!
 ********************************************************************************
 *	@brief      Release semaphore (increase count of semaphore)
 *	@param[in]  psema 
 *	                handle of semaphore
 *  @return		count of semaphore, after release
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/

I32 cr_sem_release (HAND hsema)
{
#if defined(_WIN32)
	L32 cnt = 0;
	
	if (ReleaseSemaphore (hsema, 1, &cnt)) {
		cnt ++;
		if (cnt < 1) {
			cnt = 1;
		}
	}
	return cnt;
#elif defined(__linux__)
	I32 cnt = 0;
	cr_psemaphore_t   *pSema = (cr_psemaphore_t*)hsema;
	if (pSema) {   
		if (pthread_mutex_lock (&pSema->m_Mutex)) {
			goto func_exit;
		}

		if (pSema->m_nCount == 0) {
			pthread_cond_signal (&pSema->m_Cond);
		}
		if (++pSema->m_nCount >= pSema->m_nMaxCount) {
			pSema->m_nCount = pSema->m_nMaxCount;
		}

		pthread_mutex_unlock (&pSema->m_Mutex);
	}
func_exit:
	return cnt;

#else
#error what?
#endif
}

/*!
 ********************************************************************************
 *	@brief      get count semaphore
 *	@param[in]  psema 
 *	                handle of semaphore
 *  @return		current count of semaphore
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/
I32 cr_sem_count (HAND psema)
{
#if defined(_WIN32)
	L32 cnt = 0;			
	
	I32 res = 0;			// default is FAIL.
	HAND hsema = psema;
	if (hsema) {
		res = WaitForSingleObject(hsema, 0);
		if (res == WAIT_TIMEOUT) {
			cnt = 0;
		} else {
			ReleaseSemaphore (hsema, 1, &cnt);
			cnt++;
		}
	}
	return cnt;
#elif defined(__linux__)
	return 0; 					// fake..
#else
#error what!
#endif
}



/*!
 ********************************************************************************
 *	@brief      Delete semaphore
 *	@param[in]  psema 
 *	                handle of semaphore
 *  @return		1: success of deleting semaphore 
 *              0: fail 
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/

U32 cr_sem_delete (HAND hsema)
{
#if defined(_WIN32)
	U32 res = 0;			// default is FAIL.

	HAND hsem = hsema;
	if (hsem) {	
		if (CloseHandle (hsem)) {
			res = 1;		// YES!! success!!
		} 
	}
	return res;

#elif defined(__linux__)
	U32 res = 0;			// default is FAIL.
	cr_psemaphore_t   *pSema = (cr_psemaphore_t*)hsema;

	if (pSema) {   
		while (pthread_cond_destroy (&pSema->m_Cond) == EBUSY) { 
			pthread_cond_signal (&pSema->m_Cond); 
		} 
		pthread_mutex_unlock  (&pSema->m_Mutex); 
		pthread_mutex_destroy (&pSema->m_Mutex);
		free (pSema);   
		res = 1;
	}

	return res;
#else
#error what!
#endif

}


#if defined (__cplusplus)
}
#endif
