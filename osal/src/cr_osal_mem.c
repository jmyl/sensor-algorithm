/*!
 *******************************************************************************
                                                                                
                   Creatz OS Wrapper - Memory
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 ~ 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_osal_mem.c
	 @brief  Wrapper of OS Memory services
	 @author YongHo Suk                                 
	 @date   2011/03/14 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#include <string.h> 
#include "cr_common.h"
#include "cr_osapi.h"

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/


#if defined (__cplusplus)
extern "C" {
#endif



/*!
 ********************************************************************************
 *	@brief      allocate copy
 *	@param[in] size
 *	              memory size
 *  @return		memory ptr
 *
 *	@author	    
 *  @date       
 *******************************************************************************/
void * cr_malloc( unsigned int size )
{
	void*	ptr ;

	if ( size == 0 )
	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s: alloc size zero!", __func__);
		return NULL ;
	}

	ptr = malloc( size ) ;
	if ( ptr != NULL )
	{		
		return ptr ;
	}

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s: malloc failed!", __func__);

	return NULL ;
}


/*!
 ********************************************************************************
 *	@brief      allocate and clean memory
 *	@param[in] numberOfElement
 *	              memory size
 *	@param[in] sizePerElement
 *	               byte size per data
 *  @return		memory ptr
 *
 *	@author	    
 *  @date       
 *******************************************************************************/
void * cr_calloc( unsigned int numberOfElement, unsigned int sizePerElement )
{
	void* ptr ;

	if ( sizePerElement == 0 )
	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s: alloc size zero!", __func__);
		return NULL ;
	}

	ptr = calloc( numberOfElement, sizePerElement ) ;
	if ( ptr != NULL )
	{
		return ptr ;
	}

	cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s: calloc failed!", __func__);

	return NULL ;
} 



/*!
 ********************************************************************************
 *	@brief      free memory
 *	@param[in] ptr
 *	              memory ptr
 *  @return		none
 *
 *	@author	    
 *  @date       
 *******************************************************************************/
void cr_free( void *ptr )
{
	if ( ptr == NULL )
	{
		cr_trace(CR_MSG_AREA_GENERAL, CR_MSG_LEVEL_ERR, "%s: null ptr!", __func__);
		return;
	}

	free( ptr );
} 


/*!
 ********************************************************************************
 *	@brief      memory move
 *	@param[in] dst
 *	              destination memory ptr
 *	@param[in] src
 *	              soruce memory ptr
 *	@param[in] size
 *	              copy length
 *  @return		none
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/
void cr_memmove(void *dst, void *src, unsigned int size)
{
	memmove(dst, src, size);
}



/*!
 ********************************************************************************
 *	@brief      memory copy (memcpy)
 *	@param[in] dest
 *	              destination memory ptr
 *	@param[in] src
 *	              soruce memory ptr
 *	@param[in] len
 *	              copy length
 *  @return		destination memory ptr
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/
void *cr_memcpy(		// 
	void *dest,
	void *src,
	U32  len
)
{
	return memcpy(dest, src, len);
}


/*!
 ********************************************************************************
 *	@brief      Fill block of memory (memset)
 *	@param[in] ptr
 *	              Pointer to the block of memory to fill. 
 *	@param[in] src
 *	              Value to be set. 
 *	              The value is passed as an int, but the function fills the block 
 *	              of memory using the unsigned char conversion of this value. 
 *	@param[in] num 
 *	              Number of bytes to be set to the value.
 *  @return		ptr is returned.
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/
void *cr_memset(		// 
	void *ptr,
	I32 value,
	U32 num
)
{
	return memset(ptr, value, num);
}


#if defined (__cplusplus)
}
#endif
