/*!
 *******************************************************************************
                                                                                
                   Creatz OS Wrapper - System
                                                                                
  	 @section copyright_notice COPYRIGHT NOTICE
  	     Copyright (c) 2011 ~ 2015 by Creatz Inc. 
  		 All Rights Reserved. \n
  		 Do not duplicate without prior written consent of Creatz Inc.

 *******************************************************************************
  	 @section file_information FILE CREATION INFORMATION
	 @file   cr_osal_sys.cpp
	 @brief  Wrapper of OS System services
	 @author YongHo Suk                                 
	 @date   2011/03/14 First Created

	 @section checkin_information LATEST CHECK-IN INFORMATION
	$Rev$ 
	$Author$
	$Date$
 *******************************************************************************/

/*----------------------------------------------------------------------------
 	Description	: defines referenced header files  
 -----------------------------------------------------------------------------*/
#include "cr_common.h"
#include "cr_osapi.h"
#if defined(__linux__)
#include <stdio.h> 
#include <unistd.h> 
#include <time.h>
#include <sys/time.h>
#include <errno.h>
#include <uuid/uuid.h>
#endif
#include <string.h>

#define LOCAL_DEBUG_ON 1
#define LOCAL_ERROR_TRACE_ON 1
#include "cr_dbg_on.h"

/*----------------------------------------------------------------------------
 *	Description	: defines Macros and definitions
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 *	Description	: static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: external and internal global & static variable declaration  
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the external function prototype 
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 *	Description	: declares the static function prototype 
 -----------------------------------------------------------------------------*/


#if defined (__cplusplus)
extern "C" {
#endif

#if defined(_WIN32)
#define __USE_TIMEGETTIME__
#if defined(__USE_TIMEGETTIME__)
#pragma comment(lib, "winmm.lib")
#endif
#endif
/*!
 ********************************************************************************
 *	@brief      Get tickcount of system (msec)
 *  @return		tick count
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/

UL32 cr_gettickcount (void)
{
#if defined(_WIN32)
	
	UL32 ticks = 0;

#if defined(__USE_TIMEGETTIME__)
// resolution of GetTickCount(): 16 msec
// resolution of timeGetTime(): 1 msec
// for using timeGetTime(), winmm.lib must be linked.
	ticks = timeGetTime();
#else
	ticks = GetTickCount();
#endif

	return ticks;
#elif defined(__linux__)

	UL32 ticks = 0;
	timeval tv;

    gettimeofday(&tv, NULL);
    ticks = tv.tv_sec * 1000 + tv.tv_usec / 1000;

	return ticks;
#else
#error what!
#endif

}


/*!
 ********************************************************************************
 *	@brief      Sleep (msec)
 *	@param[in]  ticks 
 *					sleep tick (msec)
 *  @return		void
 *
 *	@author	    yhsuk
 *  @date       2011/03/14
 *******************************************************************************/

void cr_sleep (U32 ticks)
{
#if defined(_WIN32)
	Sleep (ticks);
#elif defined(__linux__)
	U32 uticks;

	uticks = ticks * 1000;
	usleep(uticks);

	return;
#else
#error what!
#endif


}

/*!
 ********************************************************************************
 *	@brief      Sleep (usec). Currently used for Linux platform only.
 *	@param[in]  ticks 
 *					sleep tick (usec)
 *  @return		void
 * 
 *******************************************************************************/
void OSAL_SYS_MicroSleep(U32 usec)
{
#if defined(_WIN32)
	// TODO: if you need it
	UNUSED(usec);
#elif defined(__linux__)
	usleep(usec);
#endif

}


/*!
 ********************************************************************************
 *	@brief      get current local time(year/month/day/hour/minute/second/milisecond
 *	@param[out]  localTime 
 *					CR_Time_t
 *  @return		void
 * 
 *******************************************************************************/
void OSAL_SYS_GetLocalTime(CR_Time_t *localTime)
{
#if defined(_WIN32)
	SYSTEMTIME st;
	
	GetLocalTime( &st);

	localTime->wYear = st.wYear;
	localTime->wMonth = st.wMonth;
	localTime->wDay = st.wDay;
	localTime->wHour = st.wHour;
	localTime->wMinute = st.wMinute;
	localTime->wSecond = st.wSecond;
	localTime->wMilliseconds = st.wMilliseconds;	
	
#elif defined(__linux__)

	time_t tmpTimer;
	struct tm st;
	timeval tv;

	localtime_r( &tmpTimer, &st );

	localTime->wYear = st.tm_year + 1900;
	localTime->wMonth = st.tm_mon;
	localTime->wDay = st.tm_mday;
	localTime->wHour = st.tm_hour;
	localTime->wMinute = st.tm_min;
	localTime->wSecond = st.tm_sec;	

	gettimeofday(&tv, NULL);
	localTime->wMilliseconds = tv.tv_usec / 1000;	
	
#endif
}

/*!
 ********************************************************************************
 *	@brief      get current GUID
 *	@param[out]  guid 
 *					16 bytes buffer 
 *  @return		void
 * 
 *******************************************************************************/
void OSAL_SYS_GenerateGUID(char *guid)
{
#if defined(_WIN32)
	CoCreateGuid((GUID *)guid);
#elif defined(__linux__)

	uuid_t out;

	uuid_generate(out);
	memcpy(guid, out, 16);

	CR_GUID *guidTrace;
	guidTrace = (CR_GUID *) out; 
	LOCAL_DEBUG(("OSAL_SYS_GenerateGUID: GUID = %08X-%04X-%04X-%02X%02X-%02X%02X%02X%02X%02X%02X\n",
			guidTrace->Data1,	
			guidTrace->Data2,	
			guidTrace->Data3,	
			guidTrace->Data4[0],	
			guidTrace->Data4[1],	
			guidTrace->Data4[2],	
			guidTrace->Data4[3],	
			guidTrace->Data4[4],	
			guidTrace->Data4[5],	
			guidTrace->Data4[6],	
			guidTrace->Data4[7]));

#endif
}


#if defined(_WIN32)
static I64 SubtractTime(const FILETIME& ftA, const FILETIME& ftB)
{
	LARGE_INTEGER a, b;
	a.LowPart = ftA.dwLowDateTime;
	a.HighPart = ftA.dwHighDateTime;

	b.LowPart = ftB.dwLowDateTime;
	b.HighPart = ftB.dwHighDateTime;

	return a.QuadPart - b.QuadPart;
}
#endif

I32 OSAL_SYS_GetCpuUsage(void)
{
#if defined(_WIN32)
	//system total times
	static FILETIME s_ftPrevSysKernel;
	static FILETIME s_ftPrevSysUser;

	//process times
	static FILETIME s_ftPrevProcKernel;
	static FILETIME s_ftPrevProcUser;

	static short s_nCpuUsage;
	static U64 s_dwLastRun;

	volatile static LONG s_lRunCount;	
	static U32 s_inited = 0;

	//--
	short nCpuCopy;
	U64 currentTime;
	//----------------------------

	if (s_inited == 0) {
		ZeroMemory(&s_ftPrevSysKernel, sizeof(FILETIME));
		ZeroMemory(&s_ftPrevSysUser, sizeof(FILETIME));

		ZeroMemory(&s_ftPrevProcKernel, sizeof(FILETIME));
		ZeroMemory(&s_ftPrevProcUser, sizeof(FILETIME));

		s_nCpuUsage = 0;
		s_dwLastRun = 0;
		s_lRunCount = 0;

		s_inited = 1;
	}


	nCpuCopy = s_nCpuUsage;
	{
		/*
		   If this is called too often, the measurement itself will greatly 
		   affect the results.
		 */
#if 0
		if (!EnoughTimePassed())
		{
			::InterlockedDecrement(&s_lRunCount);
			return nCpuCopy;
		}
#endif

		currentTime = cr_gettickcount();
#define MINIMUMTIME4USAGE 250
		if (currentTime < s_dwLastRun + MINIMUMTIME4USAGE) {
			return nCpuCopy;
		}
	}

	{
		FILETIME ftSysIdle, ftSysKernel, ftSysUser;
		FILETIME ftProcCreation, ftProcExit, ftProcKernel, ftProcUser;


		if (!GetSystemTimes(&ftSysIdle, &ftSysKernel, &ftSysUser) ||
				!GetProcessTimes(GetCurrentProcess(), &ftProcCreation,
					&ftProcExit, &ftProcKernel, &ftProcUser))
		{
		//	::InterlockedDecrement(&s_lRunCount);
			return nCpuCopy;
		}

		//if (!IsFirstRun())
		if (s_dwLastRun != 0) 
		{
			/*
			   CPU usage is calculated by getting the total amount of time 
			   the system has operated since the last measurement 
			   (made up of kernel + user) and the total
			   amount of time the process has run (kernel + user).
			 */
			U64 ftSysKernelDiff =
				SubtractTime(ftSysKernel, s_ftPrevSysKernel);
			I64 ftSysUserDiff =
				SubtractTime(ftSysUser, s_ftPrevSysUser);

			I64 ftProcKernelDiff =
				SubtractTime(ftProcKernel, s_ftPrevProcKernel);
			I64 ftProcUserDiff =
				SubtractTime(ftProcUser, s_ftPrevProcUser);

			I64 nTotalSys =  ftSysKernelDiff + ftSysUserDiff;
			I64 nTotalProc = ftProcKernelDiff + ftProcUserDiff;

			if (nTotalSys > 0)
			{
				s_nCpuUsage = (short)((100.0 * nTotalProc) / nTotalSys);
			}
			if (s_nCpuUsage < 0) {
				s_nCpuUsage = 0;
			}
			if (s_nCpuUsage > 100) {
				s_nCpuUsage = 100;
			}
		}

		s_ftPrevSysKernel = ftSysKernel;
		s_ftPrevSysUser = ftSysUser;
		s_ftPrevProcKernel = ftProcKernel;
		s_ftPrevProcUser = ftProcUser;

		//s_dwLastRun = GetTickCount64();
		s_dwLastRun = cr_gettickcount();

		nCpuCopy = s_nCpuUsage;
	}

	return nCpuCopy;
#elif defined(__linux__)
		// TODO: if needed
	return 0;	
#endif

}


#if defined (__cplusplus)
}
#endif
