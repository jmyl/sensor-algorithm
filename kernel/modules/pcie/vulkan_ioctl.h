#if !defined(_VULKAN_IOCTL_H_)
#define		 _VULKAN_IOCTL_H_


#if defined (__cplusplus)
extern "C" {
#endif

/* flag to use ioctl for DMA read, if it is not set, read() is used by default */
#define VULKAN_DMA_IOCTL	0


#define VULKAN_IOCTL_MAGIC 'v'

#define SET_USR_BUF_ADDR	_IOW(VULKAN_IOCTL_MAGIC, 0, unsigned long)
#define COPY_DMA_BUF		_IOW(VULKAN_IOCTL_MAGIC, 1, unsigned long)
#define TEST_IOCTL			_IOW(VULKAN_IOCTL_MAGIC, 1, unsigned long)



#if defined (__cplusplus)
}
#endif

#endif


