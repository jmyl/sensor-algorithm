/*
 * vulkan_pcie_drv.c
 *
 * Copyright 2021 Creatz
 *
 * Driver for PCI Express.
 */

#include <linux/module.h>
#include <linux/pci.h>
#include <linux/slab.h>
#include <linux/spinlock.h>
#include <linux/poll.h> 
#include <asm/io.h>
#include <linux/uaccess.h>
#include <linux/cdev.h>
#include <linux/init.h>
#include <linux/delay.h>

#include <asm/byteorder.h>
#include <asm/io.h>



#include "vulkan_ioctl.h"
#include "vulkan_pcie_map.h"


MODULE_DESCRIPTION("PCIe device driver for Vulkan");
MODULE_AUTHOR("Creatz");
MODULE_VERSION("0.01");
MODULE_ALIAS("vulkan_pcie");

// TODO: License issue - use LGPL? Create device in the system init process instead of device runtime

MODULE_LICENSE("GPL v2");	


#define DMA_WAIT_TIMEOUT	100

#define PCI_VENDOR_ID_CREATZ		0x10ee
#define PCI_DEVICE_ID_VULKAN		0xd011

#define LOCAL_DEBUG(x)		printk x
#define LOCAL_TRACE_ERR(x)		printk x

#define MAX_CAM			4		/* Max capable CAMs */
#define NUM_OF_CAMS		2		/* Real attached CAMs */


typedef struct VulkanPciDev_ {
	int device_ready;
	int bar_index[MAX_BARS];
	void __iomem *bar_virt_addr[MAX_BARS];
	unsigned long bar_start[MAX_BARS];
	unsigned long bar_end[MAX_BARS];	
	unsigned long bar_size[MAX_BARS];
	unsigned int current_frame_num[MAX_CAM];
	dma_addr_t dma;
	void *dma_buffer;
	spinlock_t spinlock;
	wait_queue_head_t dma_wait_queue;
	wait_queue_head_t frame_wait_queue;	
	unsigned long dma_read_count;
	unsigned long frame_event_count;	
	int msi_count;
} VulkanPciDev_t;

static const char vulkan_dev_name[] = "vulkan";

static const struct pci_device_id vulkan_pci_ids[] = {
	{PCI_DEVICE(PCI_VENDOR_ID_CREATZ, PCI_DEVICE_ID_VULKAN), },
	{ 0, /* End: all zeroes */ }
};

static VulkanPciDev_t VulkanPciDev;

#if VULKAN_DMA_IOCTL
static unsigned long buf_userspace;
#endif

static irqreturn_t vulkan_irq_dma_handler(int irq, void *arg)
{
//	unsigned long flags;
	
	volatile unsigned int *dma_status_addr = ((volatile unsigned int *)VulkanPciDev.bar_virt_addr[0]) + BAR_0_DMA_STATUS_OFFSET;
	volatile unsigned int *dma_ctrl_addr = ((volatile unsigned int *)VulkanPciDev.bar_virt_addr[0]) + BAR_0_DMA_STATUS_OFFSET;	


	if(*dma_status_addr == DMA_STATUS_DONE) {
		// Copy DMA buffer to user buffer

//		spin_lock_irqsave(&VulkanPciDev.spinlock, flags);

		VulkanPciDev.dma_read_count++;
		*dma_ctrl_addr = DMA_STATUS_STOP;
		
//		spin_unlock_irqrestore(&VulkanPciDev.spinlock, flags);	

		wake_up_interruptible(&VulkanPciDev.dma_wait_queue);
	}

	return 0;
}

static irqreturn_t vulkan_irq_frame_handler(int irq, void *arg)
{
	int i;
	int frame_updated = 0;
	volatile unsigned int *cur_frame_num_addr; 

	for(i=0; i < NUM_OF_CAMS; i++)
	{
		cur_frame_num_addr = ((volatile unsigned int *)VulkanPciDev.bar_virt_addr[0]) + CAM_REG_OFFSET(i, CUR_FRAME_NUM_OFFSET);	
		if(*cur_frame_num_addr != VulkanPciDev.current_frame_num[i]) {
			frame_updated = 1;
		}
		VulkanPciDev.current_frame_num[i] = *cur_frame_num_addr;
	}	
	if(frame_updated) {
		wake_up_interruptible(&VulkanPciDev.frame_wait_queue);
	}
	return 0;
}

static irqreturn_t vulkan_irq_handler(int irq, void *arg)
{
	int i;
	int frame_updated = 0;
//	unsigned long flags;
	
	volatile unsigned int *dma_status_addr = ((volatile unsigned int *)VulkanPciDev.bar_virt_addr[0]) + BAR_0_DMA_STATUS_OFFSET;
	volatile unsigned int *dma_ctrl_addr = ((volatile unsigned int *)VulkanPciDev.bar_virt_addr[0]) + BAR_0_DMA_STATUS_OFFSET;	
	volatile unsigned int *cur_frame_num_addr; 

	if(*dma_status_addr == DMA_STATUS_DONE) {
		// Copy DMA buffer to user buffer

//		spin_lock_irqsave(&VulkanPciDev.spinlock, flags);

		VulkanPciDev.dma_read_count++;
		*dma_ctrl_addr = DMA_STATUS_STOP;
		
//		spin_unlock_irqrestore(&VulkanPciDev.spinlock, flags);	

		wake_up_interruptible(&VulkanPciDev.dma_wait_queue);
	}

	for(i=0; i < NUM_OF_CAMS; i++)
	{
		cur_frame_num_addr = ((volatile unsigned int *)VulkanPciDev.bar_virt_addr[0]) + CAM_REG_OFFSET(i, CUR_FRAME_NUM_OFFSET);	
		if(*cur_frame_num_addr != VulkanPciDev.current_frame_num[i]) {
			frame_updated = 1;
		}
		VulkanPciDev.current_frame_num[i] = *cur_frame_num_addr;
	}	
	if(frame_updated) {
		VulkanPciDev.frame_event_count++;
		wake_up_interruptible(&VulkanPciDev.frame_wait_queue);
	}
	return 0;
}


static int vulkan_probe(struct pci_dev *pdev,
		       const struct pci_device_id *ent)
{
	int rc, i;
	int msi_max_count = 0;

	LOCAL_DEBUG(("vulkan_probe\n"));

	rc = pcim_enable_device(pdev);
	if (rc) {
		LOCAL_TRACE_ERR(("[%s] pcim_enable_device() failed. Aborting.\n", __func__));
		return rc;
	}

	rc = pci_request_regions(pdev, vulkan_dev_name);
	if (rc) {
		LOCAL_TRACE_ERR(("[%s] Cannot request region\n", __func__));
		return -ENOMEM;
	}


	pci_set_master(pdev);

	for(i=0 ;i < MAX_BARS; i++)
	{
		VulkanPciDev.bar_size[i] = pci_resource_len(pdev, i*2);
		VulkanPciDev.bar_start[i] = pci_resource_start(pdev, i*2);
		VulkanPciDev.bar_end[i] = pci_resource_end(pdev, i*2);		
		VulkanPciDev.bar_virt_addr[i] = pcim_iomap(pdev, i*2, 0);
		if (!VulkanPciDev.bar_virt_addr[i]) {
			LOCAL_TRACE_ERR(("[%s] Cannot pcim_iomap for bar(BAR%d)\n", __func__, i));
			return -ENOMEM;
		}
		
		LOCAL_DEBUG(("BAR%d mapping virt addr=0x%lx(start=0x%lx end=0x%lx size=0x%lx)\n", i*2, (unsigned long)VulkanPciDev.bar_virt_addr[i], VulkanPciDev.bar_start[i], VulkanPciDev.bar_end[i], VulkanPciDev.bar_size[i] ));

		if ((pci_resource_flags(pdev, i*2) & IORESOURCE_MEM) == 0) {
			LOCAL_TRACE_ERR(("[%s] no memory in bar %d\n", __func__, i*2));	
			return -ENODEV;
		}
	}

	for(i=0 ;i < MAX_CAM; i++)
	{
		VulkanPciDev.current_frame_num[i] = 0;
	}

#if 0	// TODO: DMA memory pool
	VulkanPciDev.dma_buffer = dma_alloc_coherent(&pdev->dev, PCIE_IMG_BUF_SIZE, &VulkanPciDev.dma, GFP_KERNEL);
#else
	VulkanPciDev.dma_buffer = kmalloc(PCIE_IMG_BUF_SIZE, GFP_KERNEL);	
#endif /* 0 */

	if (!VulkanPciDev.dma_buffer) {
		LOCAL_TRACE_ERR(("[%s] Failed to dma_alloc_coherent!\n", __func__));
		return -ENODEV;
	}	
	

	spin_lock_init(&VulkanPciDev.spinlock);
	init_waitqueue_head(&VulkanPciDev.dma_wait_queue);
	init_waitqueue_head(&VulkanPciDev.frame_wait_queue);	
	VulkanPciDev.dma_read_count = 0;
	VulkanPciDev.frame_event_count = 0;
	VulkanPciDev.msi_count = 0;

	
	msi_max_count = pci_msi_vec_count(pdev);
	if (msi_max_count >= 2) {
		VulkanPciDev.msi_count  = pci_alloc_irq_vectors(pdev, 1, 2, PCI_IRQ_MSI);
		if (VulkanPciDev.msi_count <= 0) {
			LOCAL_TRACE_ERR(("[%s] MSI max count=%d. Use Legacy IRQ!! \n", __func__, msi_max_count));			
		} else {
			LOCAL_DEBUG(("[%s] MSI max count=%d. Use MSI!! \n", __func__, msi_max_count));
		}
		
		
	} else {
		VulkanPciDev.msi_count = 0;
		LOCAL_TRACE_ERR(("[%s] MSI max count=%d. Use Legacy IRQ!! \n", __func__, msi_max_count));
	}	

	if(VulkanPciDev.msi_count > 1) {
		rc = devm_request_irq(&pdev->dev, pci_irq_vector(pdev, 0), vulkan_irq_dma_handler, IRQF_SHARED, vulkan_dev_name, &VulkanPciDev);
		if (rc) {
			LOCAL_TRACE_ERR(("[%s] Failed to register MSI handler. Aborting.\n", __func__));
			return -ENODEV;
		}
		rc = devm_request_irq(&pdev->dev, pci_irq_vector(pdev, 1), vulkan_irq_frame_handler, IRQF_SHARED, vulkan_dev_name, &VulkanPciDev);
		if (rc) {
			LOCAL_TRACE_ERR(("[%s] Failed to register MSI handler. Aborting.\n", __func__));
			return -ENODEV;
		}	
	} else {

//	rc = devm_request_irq(&pdev->dev, pdev->irq, vulkan_irq_handler, IRQF_DISABLED, vulkan_dev_name, &VulkanPciDev);
		rc = devm_request_irq(&pdev->dev, pdev->irq, vulkan_irq_handler, IRQF_SHARED, vulkan_dev_name, &VulkanPciDev);
		if (rc) {
			LOCAL_TRACE_ERR(("[%s] Failed to register MSI handler. Aborting.\n", __func__));
			return -ENODEV;
		}
	}

	/*  All PCI-X and PCIe compliant devices must call pci_set_dma_mask() as they are 64-bit DMA devices.*/	
	if (pci_set_dma_mask(pdev, DMA_BIT_MASK(64))) {
		LOCAL_TRACE_ERR(("[%s] Failed to set DMA mask. Aborting.\n", __func__));
		return -ENODEV;
	} else {
		if(pci_set_consistent_dma_mask(pdev, DMA_BIT_MASK(64)))  {
			LOCAL_TRACE_ERR(("[%s] Failed to pci_set_consistent_dma_mask. Aborting.\n", __func__));
			return -ENODEV;
		}
	}	

	VulkanPciDev.device_ready = 1;

	return 0;
}

static void vulkan_remove(struct pci_dev *pdev)
{
	int i;
	
	LOCAL_DEBUG(("vulkan_remove\n"));

	for(i=0 ;i < MAX_BARS; i++)
	{
		VulkanPciDev.bar_virt_addr[i] = NULL;
	}	

	if(VulkanPciDev.msi_count > 1) {
		devm_free_irq(&pdev->dev, pci_irq_vector(pdev, 0), &VulkanPciDev);
		devm_free_irq(&pdev->dev, pci_irq_vector(pdev, 1), &VulkanPciDev);		
		pci_free_irq_vectors(pdev);
	} else {
		devm_free_irq(&pdev->dev, pdev->irq, &VulkanPciDev);
	}

//	dma_free_coherent(&pdev->dev, PCIE_IMG_BUF_SIZE, VulkanPciDev.dma_buffer, VulkanPciDev.dma);
	kfree(VulkanPciDev.dma_buffer);
	VulkanPciDev.device_ready = 0;
}

MODULE_DEVICE_TABLE(pci, vulkan_pci_ids);

static struct pci_driver vulkan_pci_driver = {
	.name = vulkan_dev_name,
	.id_table = vulkan_pci_ids,
	.probe = vulkan_probe,
	.remove = vulkan_remove,	
};

static dev_t dev_id;
static struct cdev cdev;
struct class *vulkan_class;

static long vulkan_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	int rc = 0;

	
	if(!VulkanPciDev.device_ready) {
		return -1;
	}

	switch (cmd) {	
#if VULKAN_DMA_IOCTL	
	case SET_USR_BUF_ADDR:
		buf_userspace = arg;
		*(((volatile unsigned long *)VulkanPciDev.bar_virt_addr[0]) + BAR_0_DMA_DST_ADDR_OFFSET) = (unsigned long)virt_to_phys(VulkanPciDev.dma_buffer);
		LOCAL_DEBUG(("%s: SET_USER_ADDR %lx \n", __func__, arg));
		break;
		

	case COPY_DMA_BUF:		
		if(VulkanPciDev.dma_read_count) {
			rc = copy_to_user((void __user *) buf_userspace, (void *) VulkanPciDev.dma_buffer, arg);	
			VulkanPciDev.dma_read_count--;
		} else {
			LOCAL_TRACE_ERR(("%s: read count mismatch[%ld]!!\n", __func__, VulkanPciDev.dma_read_count));
			rc = -1;
		}
		break;		
#endif
	case TEST_IOCTL:
		/* Do any test you want */
		printk("BAR 2(offset 0x%x) =0x%lx\n", arg, readl(VulkanPciDev.bar_virt_addr[1]+arg));
		break;
	default:
		LOCAL_TRACE_ERR(("%s: Unsupported command[%lx]!!\n", __func__, arg));
		rc = -1;
		break;
	}
	return rc;
}

/* Blocking read */
static ssize_t vulkan_read(struct file *filp, char __user *userbuf,
			     size_t count, loff_t *f_pos)
{
	ssize_t read_size = 0;
	int ret;

	if(!VulkanPciDev.device_ready) {
		return 0;
	}

	/* Start DMA */
	*(((volatile unsigned long *)VulkanPciDev.bar_virt_addr[0]) + BAR_0_DMA_DST_ADDR_OFFSET) = (unsigned long)virt_to_phys(VulkanPciDev.dma_buffer);
	*(((volatile unsigned long *)VulkanPciDev.bar_virt_addr[0]) + BAR_0_DMA_SIZE_OFFSET) = (unsigned long)count;	
	*(((volatile unsigned long *)VulkanPciDev.bar_virt_addr[0]) + BAR_0_DMA_CTRL_OFFSET) = DMA_RUN;	


	/* Wait until DMA is done */
	ret = wait_event_interruptible_timeout(VulkanPciDev.dma_wait_queue,
							 VulkanPciDev.dma_read_count,
							 DMA_WAIT_TIMEOUT);
	if (ret > 0) {
		if(VulkanPciDev.dma_read_count) {
			read_size = copy_to_user((void __user *) userbuf, (void *) VulkanPciDev.dma_buffer, count);	
			VulkanPciDev.dma_read_count--;
		} else {
			LOCAL_TRACE_ERR(("%s: read count mismatch[%ld]!!\n", __func__, VulkanPciDev.dma_read_count));
			read_size = 0;
		}
	}

	return read_size;
}



static __poll_t vulkan_poll(struct file *filp, poll_table *wait)
{
	__poll_t mask = 0;

	if(!VulkanPciDev.device_ready) {
		return POLLERR;
	}

	poll_wait(filp, &VulkanPciDev.frame_wait_queue, wait);

	if(VulkanPciDev.frame_event_count > 0) {
		mask |= POLLIN;
		VulkanPciDev.frame_event_count--;
	}

	return mask;
}



static const struct file_operations Fops = {
        .owner = THIS_MODULE,
        .open = NULL,
        .release = NULL,
        .read = vulkan_read,
        .write = NULL,
        .unlocked_ioctl = vulkan_ioctl,
        .llseek = NULL,
        .poll       = vulkan_poll,
};


static int __init vulkan_init(void)
{
	int rc = 0;
	struct device *dev;	
	

	rc = alloc_chrdev_region(&dev_id, 0, 1, vulkan_dev_name);
	if(rc) {
		LOCAL_TRACE_ERR(("%s: alloc_chrdev_region error.\n", __func__));
		return rc;
	}
	
	cdev_init(&cdev, &Fops);
	cdev.owner = THIS_MODULE;

	rc = cdev_add(&cdev, dev_id, 1);
	if(rc) {
		LOCAL_TRACE_ERR(("%s: cdev_add error.\n", __func__));
		unregister_chrdev_region(dev_id, 1);
		return rc;
	}

	vulkan_class = class_create(THIS_MODULE, vulkan_dev_name);
	if(IS_ERR(vulkan_class)) {
		rc = PTR_ERR(vulkan_class);
		LOCAL_TRACE_ERR(("%s: class_create error.\n", __func__));
		cdev_del(&cdev);
		unregister_chrdev_region(dev_id, 1);
		return rc;
	}

	dev = device_create(vulkan_class, NULL, dev_id, NULL, vulkan_dev_name);
	if(IS_ERR(dev)) {
		rc = PTR_ERR(dev);
		LOCAL_TRACE_ERR(("%s: class_create error.\n", __func__));
		class_destroy(vulkan_class);
		cdev_del(&cdev);
		unregister_chrdev_region(dev_id, 1);
		return rc;
	}

	rc = pci_register_driver(&vulkan_pci_driver);

	
	LOCAL_DEBUG(("Vulkan driver successfully initialized.\n"));

	return rc;
}

static void __exit vulkan_exit(void)
{
	pci_unregister_driver(&vulkan_pci_driver);

	device_destroy(vulkan_class, dev_id);
	class_destroy(vulkan_class);
	cdev_del(&cdev);
	unregister_chrdev_region(dev_id, 1);
}

module_init(vulkan_init);
module_exit(vulkan_exit);

